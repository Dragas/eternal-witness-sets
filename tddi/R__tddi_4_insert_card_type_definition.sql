insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Koth of the Hammer Emblem'),
        (select types.id from types where name = 'Emblem')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Koth of the Hammer Emblem'),
        (select types.id from types where name = 'Koth')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Venser, the Sojourner Emblem'),
        (select types.id from types where name = 'Emblem')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Venser, the Sojourner Emblem'),
        (select types.id from types where name = 'Venser')
    ) 
 on conflict do nothing;
