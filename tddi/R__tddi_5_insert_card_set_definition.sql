insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Koth of the Hammer Emblem'),
    (select id from sets where short_name = 'tddi'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Venser, the Sojourner Emblem'),
    (select id from sets where short_name = 'tddi'),
    '1',
    'common'
) 
 on conflict do nothing;
