insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'itp'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Monster'),
    (select id from sets where short_name = 'itp'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'itp'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sorceress Queen'),
    (select id from sets where short_name = 'itp'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'itp'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'itp'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reverse Damage'),
    (select id from sets where short_name = 'itp'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Durkwood Boars'),
    (select id from sets where short_name = 'itp'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'itp'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk of the Pearl Trident'),
    (select id from sets where short_name = 'itp'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Archers'),
    (select id from sets where short_name = 'itp'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ironclaw Orcs'),
    (select id from sets where short_name = 'itp'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elven Riders'),
    (select id from sets where short_name = 'itp'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'itp'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lost Soul'),
    (select id from sets where short_name = 'itp'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Sink'),
    (select id from sets where short_name = 'itp'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rod of Ruin'),
    (select id from sets where short_name = 'itp'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whirling Dervish'),
    (select id from sets where short_name = 'itp'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mesa Pegasus'),
    (select id from sets where short_name = 'itp'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'itp'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murk Dwellers'),
    (select id from sets where short_name = 'itp'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'itp'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Artillery'),
    (select id from sets where short_name = 'itp'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Oriflamme'),
    (select id from sets where short_name = 'itp'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pearled Unicorn'),
    (select id from sets where short_name = 'itp'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = 'itp'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Healing Salve'),
    (select id from sets where short_name = 'itp'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winter Blast'),
    (select id from sets where short_name = 'itp'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alabaster Potion'),
    (select id from sets where short_name = 'itp'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grizzly Bears'),
    (select id from sets where short_name = 'itp'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = 'itp'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clockwork Beast'),
    (select id from sets where short_name = 'itp'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'itp'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'itp'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'itp'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Twiddle'),
    (select id from sets where short_name = 'itp'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jayemdae Tome'),
    (select id from sets where short_name = 'itp'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Untamed Wilds'),
    (select id from sets where short_name = 'itp'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Bone'),
    (select id from sets where short_name = 'itp'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'itp'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'War Mammoth'),
    (select id from sets where short_name = 'itp'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Wraith'),
    (select id from sets where short_name = 'itp'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Black'),
    (select id from sets where short_name = 'itp'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scathe Zombies'),
    (select id from sets where short_name = 'itp'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Energy Flux'),
    (select id from sets where short_name = 'itp'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'itp'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glasses of Urza'),
    (select id from sets where short_name = 'itp'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bog Imp'),
    (select id from sets where short_name = 'itp'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mons''s Goblin Raiders'),
    (select id from sets where short_name = 'itp'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warp Artifact'),
    (select id from sets where short_name = 'itp'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Battering Ram'),
    (select id from sets where short_name = 'itp'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disintegrate'),
    (select id from sets where short_name = 'itp'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Detonate'),
    (select id from sets where short_name = 'itp'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cursed Land'),
    (select id from sets where short_name = 'itp'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zephyr Falcon'),
    (select id from sets where short_name = 'itp'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weakness'),
    (select id from sets where short_name = 'itp'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feedback'),
    (select id from sets where short_name = 'itp'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyrotechnics'),
    (select id from sets where short_name = 'itp'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = 'itp'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = 'itp'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'itp'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Bats'),
    (select id from sets where short_name = 'itp'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scryb Sprites'),
    (select id from sets where short_name = 'itp'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'itp'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'itp'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'itp'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hill Giant'),
    (select id from sets where short_name = 'itp'),
    '33',
    'common'
) 
 on conflict do nothing;
