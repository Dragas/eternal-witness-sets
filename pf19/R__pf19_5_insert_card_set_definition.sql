insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pf19'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pf19'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pf19'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pf19'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pf19'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'pf19'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'pf19'),
    '1',
    'rare'
) 
 on conflict do nothing;
