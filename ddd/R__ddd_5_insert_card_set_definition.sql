insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Liliana Vess'),
    (select id from sets where short_name = 'ddd'),
    '32',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vine Trellis'),
    (select id from sets where short_name = 'ddd'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rude Awakening'),
    (select id from sets where short_name = 'ddd'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blastoderm'),
    (select id from sets where short_name = 'ddd'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghost-Lit Stalker'),
    (select id from sets where short_name = 'ddd'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddd'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddd'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urborg Syphon-Mage'),
    (select id from sets where short_name = 'ddd'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Macabre'),
    (select id from sets where short_name = 'ddd'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddd'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddd'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Genju of the Fens'),
    (select id from sets where short_name = 'ddd'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Albino Troll'),
    (select id from sets where short_name = 'ddd'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddd'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nature''s Lore'),
    (select id from sets where short_name = 'ddd'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plated Slagwurm'),
    (select id from sets where short_name = 'ddd'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hideous End'),
    (select id from sets where short_name = 'ddd'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk Wildspeaker'),
    (select id from sets where short_name = 'ddd'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vicious Hunger'),
    (select id from sets where short_name = 'ddd'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Bats'),
    (select id from sets where short_name = 'ddd'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddd'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sign in Blood'),
    (select id from sets where short_name = 'ddd'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Mongrel'),
    (select id from sets where short_name = 'ddd'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harmonize'),
    (select id from sets where short_name = 'ddd'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddd'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'ddd'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Bone'),
    (select id from sets where short_name = 'ddd'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Twisted Abomination'),
    (select id from sets where short_name = 'ddd'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tendrils of Corruption'),
    (select id from sets where short_name = 'ddd'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Invigorate'),
    (select id from sets where short_name = 'ddd'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Rager'),
    (select id from sets where short_name = 'ddd'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skeletal Vampire'),
    (select id from sets where short_name = 'ddd'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drudge Skeletons'),
    (select id from sets where short_name = 'ddd'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ichor Slick'),
    (select id from sets where short_name = 'ddd'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Indrik Stomphowler'),
    (select id from sets where short_name = 'ddd'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Corrupt'),
    (select id from sets where short_name = 'ddd'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elephant Guide'),
    (select id from sets where short_name = 'ddd'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ravenous Rats'),
    (select id from sets where short_name = 'ddd'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathgreeter'),
    (select id from sets where short_name = 'ddd'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bad Moon'),
    (select id from sets where short_name = 'ddd'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Polluted Mire'),
    (select id from sets where short_name = 'ddd'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howling Banshee'),
    (select id from sets where short_name = 'ddd'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rancor'),
    (select id from sets where short_name = 'ddd'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windstorm'),
    (select id from sets where short_name = 'ddd'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slippery Karst'),
    (select id from sets where short_name = 'ddd'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serrated Arrows'),
    (select id from sets where short_name = 'ddd'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast Attack'),
    (select id from sets where short_name = 'ddd'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keening Banshee'),
    (select id from sets where short_name = 'ddd'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wirewood Savage'),
    (select id from sets where short_name = 'ddd'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lignify'),
    (select id from sets where short_name = 'ddd'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravenous Baloth'),
    (select id from sets where short_name = 'ddd'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enslave'),
    (select id from sets where short_name = 'ddd'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stampeding Wildebeests'),
    (select id from sets where short_name = 'ddd'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mutilate'),
    (select id from sets where short_name = 'ddd'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snuff Out'),
    (select id from sets where short_name = 'ddd'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basking Rootwalla'),
    (select id from sets where short_name = 'ddd'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rise from the Grave'),
    (select id from sets where short_name = 'ddd'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddd'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treetop Village'),
    (select id from sets where short_name = 'ddd'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krosan Tusker'),
    (select id from sets where short_name = 'ddd'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fleshbag Marauder'),
    (select id from sets where short_name = 'ddd'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overrun'),
    (select id from sets where short_name = 'ddd'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Genju of the Cedars'),
    (select id from sets where short_name = 'ddd'),
    '13',
    'uncommon'
) 
 on conflict do nothing;
