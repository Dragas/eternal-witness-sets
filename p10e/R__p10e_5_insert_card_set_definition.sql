insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Reya Dawnbringer'),
    (select id from sets where short_name = 'p10e'),
    '35',
    'rare'
) 
 on conflict do nothing;
