insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Vampire'),
    (select id from sets where short_name = 'txln'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dinosaur'),
    (select id from sets where short_name = 'txln'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure'),
    (select id from sets where short_name = 'txln'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pirate'),
    (select id from sets where short_name = 'txln'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ixalan Checklist'),
    (select id from sets where short_name = 'txln'),
    'CH1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure'),
    (select id from sets where short_name = 'txln'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plant'),
    (select id from sets where short_name = 'txln'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure'),
    (select id from sets where short_name = 'txln'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk'),
    (select id from sets where short_name = 'txln'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illusion'),
    (select id from sets where short_name = 'txln'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure'),
    (select id from sets where short_name = 'txln'),
    '10',
    'common'
) 
 on conflict do nothing;
