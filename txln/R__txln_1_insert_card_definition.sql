    insert into mtgcard(name) values ('Vampire') on conflict do nothing;
    insert into mtgcard(name) values ('Dinosaur') on conflict do nothing;
    insert into mtgcard(name) values ('Treasure') on conflict do nothing;
    insert into mtgcard(name) values ('Pirate') on conflict do nothing;
    insert into mtgcard(name) values ('Ixalan Checklist') on conflict do nothing;
    insert into mtgcard(name) values ('Plant') on conflict do nothing;
    insert into mtgcard(name) values ('Merfolk') on conflict do nothing;
    insert into mtgcard(name) values ('Illusion') on conflict do nothing;
