insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Sightless Ghoul'),
    (select id from sets where short_name = 'dka'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormbound Geist'),
    (select id from sets where short_name = 'dka'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grim Backwoods'),
    (select id from sets where short_name = 'dka'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hollowhenge Beast'),
    (select id from sets where short_name = 'dka'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scorch the Fields'),
    (select id from sets where short_name = 'dka'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gavony Ironwright'),
    (select id from sets where short_name = 'dka'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death''s Caress'),
    (select id from sets where short_name = 'dka'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thraben Doomsayer'),
    (select id from sets where short_name = 'dka'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Niblis of the Breath'),
    (select id from sets where short_name = 'dka'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Niblis of the Mist'),
    (select id from sets where short_name = 'dka'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tragic Slip'),
    (select id from sets where short_name = 'dka'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chill of Foreboding'),
    (select id from sets where short_name = 'dka'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hollowhenge Spirit'),
    (select id from sets where short_name = 'dka'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Young Wolf'),
    (select id from sets where short_name = 'dka'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diregraf Captain'),
    (select id from sets where short_name = 'dka'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hellrider'),
    (select id from sets where short_name = 'dka'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shattered Perception'),
    (select id from sets where short_name = 'dka'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Niblis of the Urn'),
    (select id from sets where short_name = 'dka'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Markov Blademaster'),
    (select id from sets where short_name = 'dka'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dawntreader Elk'),
    (select id from sets where short_name = 'dka'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolfbitten Captive // Krallenhorde Killer'),
    (select id from sets where short_name = 'dka'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vorapede'),
    (select id from sets where short_name = 'dka'),
    '131',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Village Survivors'),
    (select id from sets where short_name = 'dka'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Increasing Confusion'),
    (select id from sets where short_name = 'dka'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curse of Echoes'),
    (select id from sets where short_name = 'dka'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Helvault'),
    (select id from sets where short_name = 'dka'),
    '151',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nephalia Seakite'),
    (select id from sets where short_name = 'dka'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Midnight Guard'),
    (select id from sets where short_name = 'dka'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadly Allure'),
    (select id from sets where short_name = 'dka'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skirsdag Flayer'),
    (select id from sets where short_name = 'dka'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sorin, Lord of Innistrad'),
    (select id from sets where short_name = 'dka'),
    '142',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Griptide'),
    (select id from sets where short_name = 'dka'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Retrieval'),
    (select id from sets where short_name = 'dka'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Increasing Savagery'),
    (select id from sets where short_name = 'dka'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skillful Lunge'),
    (select id from sets where short_name = 'dka'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grafdigger''s Cage'),
    (select id from sets where short_name = 'dka'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'dka'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Somberwald Dryad'),
    (select id from sets where short_name = 'dka'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Havengul Lich'),
    (select id from sets where short_name = 'dka'),
    '139',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Undying Evil'),
    (select id from sets where short_name = 'dka'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vault of the Archangel'),
    (select id from sets where short_name = 'dka'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiend of the Shadows'),
    (select id from sets where short_name = 'dka'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gravepurge'),
    (select id from sets where short_name = 'dka'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Screeching Skaab'),
    (select id from sets where short_name = 'dka'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shriekgeist'),
    (select id from sets where short_name = 'dka'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burning Oil'),
    (select id from sets where short_name = 'dka'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gravecrawler'),
    (select id from sets where short_name = 'dka'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Divination'),
    (select id from sets where short_name = 'dka'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reap the Seagraf'),
    (select id from sets where short_name = 'dka'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrack with Madness'),
    (select id from sets where short_name = 'dka'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Markov Warlord'),
    (select id from sets where short_name = 'dka'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Requiem Angel'),
    (select id from sets where short_name = 'dka'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silverclaw Griffin'),
    (select id from sets where short_name = 'dka'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lost in the Woods'),
    (select id from sets where short_name = 'dka'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tracker''s Instincts'),
    (select id from sets where short_name = 'dka'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stromkirk Captain'),
    (select id from sets where short_name = 'dka'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Falkenrath Torturer'),
    (select id from sets where short_name = 'dka'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Feud'),
    (select id from sets where short_name = 'dka'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forge Devil'),
    (select id from sets where short_name = 'dka'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faith''s Shield'),
    (select id from sets where short_name = 'dka'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gravetiller Wurm'),
    (select id from sets where short_name = 'dka'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elbrus, the Binding Blade // Withengar Unbound'),
    (select id from sets where short_name = 'dka'),
    '147',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Geralf''s Mindcrusher'),
    (select id from sets where short_name = 'dka'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kessig Recluse'),
    (select id from sets where short_name = 'dka'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravenous Demon // Archdemon of Greed'),
    (select id from sets where short_name = 'dka'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fires of Undeath'),
    (select id from sets where short_name = 'dka'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chant of the Skifsang'),
    (select id from sets where short_name = 'dka'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scorned Villager // Moonscarred Werewolf'),
    (select id from sets where short_name = 'dka'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jar of Eyeballs'),
    (select id from sets where short_name = 'dka'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Predator Ooze'),
    (select id from sets where short_name = 'dka'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Executioner''s Hood'),
    (select id from sets where short_name = 'dka'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erdwal Ripper'),
    (select id from sets where short_name = 'dka'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Altar of the Lost'),
    (select id from sets where short_name = 'dka'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chosen of Markov // Markov''s Servant'),
    (select id from sets where short_name = 'dka'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nearheath Stalker'),
    (select id from sets where short_name = 'dka'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mikaeus, the Unhallowed'),
    (select id from sets where short_name = 'dka'),
    '70',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thalia, Guardian of Thraben'),
    (select id from sets where short_name = 'dka'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Haunted Fengraf'),
    (select id from sets where short_name = 'dka'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Increasing Devotion'),
    (select id from sets where short_name = 'dka'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thought Scour'),
    (select id from sets where short_name = 'dka'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lingering Souls'),
    (select id from sets where short_name = 'dka'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heavy Mattock'),
    (select id from sets where short_name = 'dka'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saving Grasp'),
    (select id from sets where short_name = 'dka'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moonveil Dragon'),
    (select id from sets where short_name = 'dka'),
    '99',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Immerwolf'),
    (select id from sets where short_name = 'dka'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lambholt Elder // Silverpelt Werewolf'),
    (select id from sets where short_name = 'dka'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sanctuary Cat'),
    (select id from sets where short_name = 'dka'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyreheart Wolf'),
    (select id from sets where short_name = 'dka'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avacyn''s Collar'),
    (select id from sets where short_name = 'dka'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feed the Pack'),
    (select id from sets where short_name = 'dka'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Farbog Boneflinger'),
    (select id from sets where short_name = 'dka'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harrowing Journey'),
    (select id from sets where short_name = 'dka'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gather the Townsfolk'),
    (select id from sets where short_name = 'dka'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chalice of Life // Chalice of Death'),
    (select id from sets where short_name = 'dka'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Curse of Bloodletting'),
    (select id from sets where short_name = 'dka'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ulvenwald Bear'),
    (select id from sets where short_name = 'dka'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Break of Day'),
    (select id from sets where short_name = 'dka'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Briarpack Alpha'),
    (select id from sets where short_name = 'dka'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Faithless Looting'),
    (select id from sets where short_name = 'dka'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curse of Thirst'),
    (select id from sets where short_name = 'dka'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Afflicted Deserter // Werewolf Ransacker'),
    (select id from sets where short_name = 'dka'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghoultree'),
    (select id from sets where short_name = 'dka'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sudden Disappearance'),
    (select id from sets where short_name = 'dka'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wild Hunger'),
    (select id from sets where short_name = 'dka'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Huntmaster of the Fells // Ravager of the Fells'),
    (select id from sets where short_name = 'dka'),
    '140',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Drogskol Reaver'),
    (select id from sets where short_name = 'dka'),
    '137',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mondronen Shaman // Tovolar''s Magehunter'),
    (select id from sets where short_name = 'dka'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relentless Skaabs'),
    (select id from sets where short_name = 'dka'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hunger of the Howlpack'),
    (select id from sets where short_name = 'dka'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Russet Wolves'),
    (select id from sets where short_name = 'dka'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bar the Door'),
    (select id from sets where short_name = 'dka'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hinterland Hermit // Hinterland Scourge'),
    (select id from sets where short_name = 'dka'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dungeon Geists'),
    (select id from sets where short_name = 'dka'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curse of Exhaustion'),
    (select id from sets where short_name = 'dka'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drogskol Captain'),
    (select id from sets where short_name = 'dka'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Cat'),
    (select id from sets where short_name = 'dka'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strangleroot Geist'),
    (select id from sets where short_name = 'dka'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Geralf''s Messenger'),
    (select id from sets where short_name = 'dka'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elgaud Inquisitor'),
    (select id from sets where short_name = 'dka'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Falkenrath Aristocrat'),
    (select id from sets where short_name = 'dka'),
    '138',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bone to Ash'),
    (select id from sets where short_name = 'dka'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curse of Misfortunes'),
    (select id from sets where short_name = 'dka'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crushing Vines'),
    (select id from sets where short_name = 'dka'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Increasing Ambition'),
    (select id from sets where short_name = 'dka'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Secrets of the Dead'),
    (select id from sets where short_name = 'dka'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tower Geist'),
    (select id from sets where short_name = 'dka'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Headless Skaab'),
    (select id from sets where short_name = 'dka'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loyal Cathar // Unhallowed Cathar'),
    (select id from sets where short_name = 'dka'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fling'),
    (select id from sets where short_name = 'dka'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vengeful Vampire'),
    (select id from sets where short_name = 'dka'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gruesome Discovery'),
    (select id from sets where short_name = 'dka'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Increasing Vengeance'),
    (select id from sets where short_name = 'dka'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Counterlash'),
    (select id from sets where short_name = 'dka'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Torch Fiend'),
    (select id from sets where short_name = 'dka'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burden of Guilt'),
    (select id from sets where short_name = 'dka'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ray of Revelation'),
    (select id from sets where short_name = 'dka'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolfhunter''s Quiver'),
    (select id from sets where short_name = 'dka'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Favor of the Woods'),
    (select id from sets where short_name = 'dka'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Artful Dodge'),
    (select id from sets where short_name = 'dka'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Havengul Runebinder'),
    (select id from sets where short_name = 'dka'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Séance'),
    (select id from sets where short_name = 'dka'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clinging Mists'),
    (select id from sets where short_name = 'dka'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beguiler of Wills'),
    (select id from sets where short_name = 'dka'),
    '28',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Deranged Outcast'),
    (select id from sets where short_name = 'dka'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alpha Brawl'),
    (select id from sets where short_name = 'dka'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grim Flowering'),
    (select id from sets where short_name = 'dka'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heckling Fiends'),
    (select id from sets where short_name = 'dka'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spiteful Shadows'),
    (select id from sets where short_name = 'dka'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Call to the Kindred'),
    (select id from sets where short_name = 'dka'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Seizer // Ghastly Haunting'),
    (select id from sets where short_name = 'dka'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wakedancer'),
    (select id from sets where short_name = 'dka'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thraben Heretic'),
    (select id from sets where short_name = 'dka'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Talons of Falkenrath'),
    (select id from sets where short_name = 'dka'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archangel''s Light'),
    (select id from sets where short_name = 'dka'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Highborn Ghoul'),
    (select id from sets where short_name = 'dka'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warden of the Wall'),
    (select id from sets where short_name = 'dka'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zombie Apocalypse'),
    (select id from sets where short_name = 'dka'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flayer of the Hatebound'),
    (select id from sets where short_name = 'dka'),
    '89',
    'rare'
) 
 on conflict do nothing;
