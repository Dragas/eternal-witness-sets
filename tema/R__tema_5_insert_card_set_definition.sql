insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tema'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tema'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Soldier'),
    (select id from sets where short_name = 'tema'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elf Warrior'),
    (select id from sets where short_name = 'tema'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tema'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall'),
    (select id from sets where short_name = 'tema'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carnivore'),
    (select id from sets where short_name = 'tema'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tema'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurm'),
    (select id from sets where short_name = 'tema'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tema'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serf'),
    (select id from sets where short_name = 'tema'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tema'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tema'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dack Fayden Emblem'),
    (select id from sets where short_name = 'tema'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elephant'),
    (select id from sets where short_name = 'tema'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tema'),
    '3',
    'common'
) 
 on conflict do nothing;
