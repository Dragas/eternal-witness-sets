insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tddj'),
    '1',
    'common'
) 
 on conflict do nothing;
