insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Altar''s Reap'),
    (select id from sets where short_name = 'mb1'),
    '563',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sparkspitter'),
    (select id from sets where short_name = 'mb1'),
    '1064',
    'common'
) ,
(
    (select id from mtgcard where name = 'Propaganda'),
    (select id from sets where short_name = 'mb1'),
    '462',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shenanigans'),
    (select id from sets where short_name = 'mb1'),
    '1057',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dusk Charger'),
    (select id from sets where short_name = 'mb1'),
    '643',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harrow'),
    (select id from sets where short_name = 'mb1'),
    '1235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Danitha Capashen, Paragon'),
    (select id from sets where short_name = 'mb1'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Die Young'),
    (select id from sets where short_name = 'mb1'),
    '626',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grotesque Mutation'),
    (select id from sets where short_name = 'mb1'),
    '681',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lifespring Druid'),
    (select id from sets where short_name = 'mb1'),
    '1260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mortarpod'),
    (select id from sets where short_name = 'mb1'),
    '1612',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Suspicious Bookcase'),
    (select id from sets where short_name = 'mb1'),
    '1637',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Torch Courier'),
    (select id from sets where short_name = 'mb1'),
    '1085',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Revolution'),
    (select id from sets where short_name = 'mb1'),
    '884',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thraben Foulbloods'),
    (select id from sets where short_name = 'mb1'),
    '793',
    'common'
) ,
(
    (select id from mtgcard where name = 'Decision Paralysis'),
    (select id from sets where short_name = 'mb1'),
    '344',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrive'),
    (select id from sets where short_name = 'mb1'),
    '1357',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wishful Merfolk'),
    (select id from sets where short_name = 'mb1'),
    '554',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avacyn''s Pilgrim'),
    (select id from sets where short_name = 'mb1'),
    '1127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scatter the Seeds'),
    (select id from sets where short_name = 'mb1'),
    '1325',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadeye Tormentor'),
    (select id from sets where short_name = 'mb1'),
    '615',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghost Ship'),
    (select id from sets where short_name = 'mb1'),
    '390',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Market'),
    (select id from sets where short_name = 'mb1'),
    '575',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nighthowler'),
    (select id from sets where short_name = 'mb1'),
    '722',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Helm of Awakening'),
    (select id from sets where short_name = 'mb1'),
    '1592',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Knight'),
    (select id from sets where short_name = 'mb1'),
    '574',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Untamed Hunger'),
    (select id from sets where short_name = 'mb1'),
    '806',
    'common'
) ,
(
    (select id from mtgcard where name = 'Root Out'),
    (select id from sets where short_name = 'mb1'),
    '1317',
    'common'
) ,
(
    (select id from mtgcard where name = 'Violent Ultimatum'),
    (select id from sets where short_name = 'mb1'),
    '1506',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghitu Lavarunner'),
    (select id from sets where short_name = 'mb1'),
    '947',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boggart Brute'),
    (select id from sets where short_name = 'mb1'),
    '865',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beneath the Sands'),
    (select id from sets where short_name = 'mb1'),
    '1136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gore Swine'),
    (select id from sets where short_name = 'mb1'),
    '964',
    'common'
) ,
(
    (select id from mtgcard where name = 'Peema Outrider'),
    (select id from sets where short_name = 'mb1'),
    '1289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Monastery Loremaster'),
    (select id from sets where short_name = 'mb1'),
    '436',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure Mage'),
    (select id from sets where short_name = 'mb1'),
    '532',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Renegade''s Getaway'),
    (select id from sets where short_name = 'mb1'),
    '753',
    'common'
) ,
(
    (select id from mtgcard where name = 'Martial Glory'),
    (select id from sets where short_name = 'mb1'),
    '1451',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snapping Drake'),
    (select id from sets where short_name = 'mb1'),
    '499',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bone Splinters'),
    (select id from sets where short_name = 'mb1'),
    '583',
    'common'
) ,
(
    (select id from mtgcard where name = 'Samut''s Sprint'),
    (select id from sets where short_name = 'mb1'),
    '1050',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lignify'),
    (select id from sets where short_name = 'mb1'),
    '1261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ochran Assassin'),
    (select id from sets where short_name = 'mb1'),
    '1462',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelic Destiny'),
    (select id from sets where short_name = 'mb1'),
    '16',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ravenous Chupacabra'),
    (select id from sets where short_name = 'mb1'),
    '745',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Built to Smash'),
    (select id from sets where short_name = 'mb1'),
    '878',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exultant Skymarcher'),
    (select id from sets where short_name = 'mb1'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deep Freeze'),
    (select id from sets where short_name = 'mb1'),
    '346',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serendib Efreet'),
    (select id from sets where short_name = 'mb1'),
    '484',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fill with Fright'),
    (select id from sets where short_name = 'mb1'),
    '661',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keldon Halberdier'),
    (select id from sets where short_name = 'mb1'),
    '988',
    'common'
) ,
(
    (select id from mtgcard where name = 'Augury Owl'),
    (select id from sets where short_name = 'mb1'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rootborn Defenses'),
    (select id from sets where short_name = 'mb1'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tectonic Rift'),
    (select id from sets where short_name = 'mb1'),
    '1080',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darkblast'),
    (select id from sets where short_name = 'mb1'),
    '609',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Highspire Mantis'),
    (select id from sets where short_name = 'mb1'),
    '1436',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seal of Strength'),
    (select id from sets where short_name = 'mb1'),
    '1326',
    'common'
) ,
(
    (select id from mtgcard where name = 'Expose Evil'),
    (select id from sets where short_name = 'mb1'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daretti, Scrap Savant'),
    (select id from sets where short_name = 'mb1'),
    '898',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Smiting Helix'),
    (select id from sets where short_name = 'mb1'),
    '777',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Monument'),
    (select id from sets where short_name = 'mb1'),
    '1577',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rite of the Serpent'),
    (select id from sets where short_name = 'mb1'),
    '757',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azra Bladeseeker'),
    (select id from sets where short_name = 'mb1'),
    '846',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disposal Mummy'),
    (select id from sets where short_name = 'mb1'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urban Evolution'),
    (select id from sets where short_name = 'mb1'),
    '1504',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deadbridge Shaman'),
    (select id from sets where short_name = 'mb1'),
    '614',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gleam of Resistance'),
    (select id from sets where short_name = 'mb1'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thraben Inspector'),
    (select id from sets where short_name = 'mb1'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baloth Gorger'),
    (select id from sets where short_name = 'mb1'),
    '1129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plaguecrafter'),
    (select id from sets where short_name = 'mb1'),
    '735',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kiora''s Follower'),
    (select id from sets where short_name = 'mb1'),
    '1443',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prowling Caracal'),
    (select id from sets where short_name = 'mb1'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riverwheel Aerialists'),
    (select id from sets where short_name = 'mb1'),
    '474',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancient Brontodon'),
    (select id from sets where short_name = 'mb1'),
    '1120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Candlelight Vigil'),
    (select id from sets where short_name = 'mb1'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Man-o''-War'),
    (select id from sets where short_name = 'mb1'),
    '421',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ana Sanctuary'),
    (select id from sets where short_name = 'mb1'),
    '1118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Fury'),
    (select id from sets where short_name = 'mb1'),
    '1194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carnivorous Moss-Beast'),
    (select id from sets where short_name = 'mb1'),
    '1155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coat with Venom'),
    (select id from sets where short_name = 'mb1'),
    '599',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodrite Invoker'),
    (select id from sets where short_name = 'mb1'),
    '582',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Leak'),
    (select id from sets where short_name = 'mb1'),
    '420',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Renewal'),
    (select id from sets where short_name = 'mb1'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Watcher in the Web'),
    (select id from sets where short_name = 'mb1'),
    '1369',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancient Ziggurat'),
    (select id from sets where short_name = 'mb1'),
    '1653',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Returned Centaur'),
    (select id from sets where short_name = 'mb1'),
    '754',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disowned Ancestor'),
    (select id from sets where short_name = 'mb1'),
    '630',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Roughrider'),
    (select id from sets where short_name = 'mb1'),
    '960',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crystal Chimes'),
    (select id from sets where short_name = 'mb1'),
    '1569',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Adanto Vanguard'),
    (select id from sets where short_name = 'mb1'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woolly Thoctar'),
    (select id from sets where short_name = 'mb1'),
    '1513',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arachnus Web'),
    (select id from sets where short_name = 'mb1'),
    '1122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blastfire Bolt'),
    (select id from sets where short_name = 'mb1'),
    '855',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anger of the Gods'),
    (select id from sets where short_name = 'mb1'),
    '840',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Take Vengeance'),
    (select id from sets where short_name = 'mb1'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murder of Crows'),
    (select id from sets where short_name = 'mb1'),
    '438',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tinker'),
    (select id from sets where short_name = 'mb1'),
    '527',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Briarhorn'),
    (select id from sets where short_name = 'mb1'),
    '1148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Talrand, Sky Summoner'),
    (select id from sets where short_name = 'mb1'),
    '513',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whiplash Trap'),
    (select id from sets where short_name = 'mb1'),
    '546',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunrise Seeker'),
    (select id from sets where short_name = 'mb1'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wildfire Emissary'),
    (select id from sets where short_name = 'mb1'),
    '1103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Summons'),
    (select id from sets where short_name = 'mb1'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bounding Krasis'),
    (select id from sets where short_name = 'mb1'),
    '1404',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Star of Extinction'),
    (select id from sets where short_name = 'mb1'),
    '1068',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Palladium Myr'),
    (select id from sets where short_name = 'mb1'),
    '1616',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Shatter'),
    (select id from sets where short_name = 'mb1'),
    '714',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cliffside Lookout'),
    (select id from sets where short_name = 'mb1'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Genju of the Spires'),
    (select id from sets where short_name = 'mb1'),
    '945',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampire Hexmage'),
    (select id from sets where short_name = 'mb1'),
    '811',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sailor of Means'),
    (select id from sets where short_name = 'mb1'),
    '476',
    'common'
) ,
(
    (select id from mtgcard where name = 'Venom Sliver'),
    (select id from sets where short_name = 'mb1'),
    '1367',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circular Logic'),
    (select id from sets where short_name = 'mb1'),
    '317',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghost Quarter'),
    (select id from sets where short_name = 'mb1'),
    '1671',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woodborn Behemoth'),
    (select id from sets where short_name = 'mb1'),
    '1376',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Larger Than Life'),
    (select id from sets where short_name = 'mb1'),
    '1256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancestral Vengeance'),
    (select id from sets where short_name = 'mb1'),
    '565',
    'common'
) ,
(
    (select id from mtgcard where name = 'Embodiment of Spring'),
    (select id from sets where short_name = 'mb1'),
    '362',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunder Drake'),
    (select id from sets where short_name = 'mb1'),
    '524',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boompile'),
    (select id from sets where short_name = 'mb1'),
    '1553',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Humongulus'),
    (select id from sets where short_name = 'mb1'),
    '403',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental Uprising'),
    (select id from sets where short_name = 'mb1'),
    '1191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thraben Standard Bearer'),
    (select id from sets where short_name = 'mb1'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manamorphose'),
    (select id from sets where short_name = 'mb1'),
    '1526',
    'common'
) ,
(
    (select id from mtgcard where name = 'Labyrinth Guardian'),
    (select id from sets where short_name = 'mb1'),
    '416',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sealock Monster'),
    (select id from sets where short_name = 'mb1'),
    '481',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Explore'),
    (select id from sets where short_name = 'mb1'),
    '1202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ambitious Aetherborn'),
    (select id from sets where short_name = 'mb1'),
    '564',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drudge Sentinel'),
    (select id from sets where short_name = 'mb1'),
    '639',
    'common'
) ,
(
    (select id from mtgcard where name = 'Suppression Bonds'),
    (select id from sets where short_name = 'mb1'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Acidic Slime'),
    (select id from sets where short_name = 'mb1'),
    '1109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Siegecraft'),
    (select id from sets where short_name = 'mb1'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Befuddle'),
    (select id from sets where short_name = 'mb1'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Dragon'),
    (select id from sets where short_name = 'mb1'),
    '454',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prophetic Prism'),
    (select id from sets where short_name = 'mb1'),
    '1622',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lotus Petal'),
    (select id from sets where short_name = 'mb1'),
    '1601',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crippling Blight'),
    (select id from sets where short_name = 'mb1'),
    '605',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruesome Fate'),
    (select id from sets where short_name = 'mb1'),
    '682',
    'common'
) ,
(
    (select id from mtgcard where name = 'Path of Peace'),
    (select id from sets where short_name = 'mb1'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tendrils of Corruption'),
    (select id from sets where short_name = 'mb1'),
    '789',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Egg'),
    (select id from sets where short_name = 'mb1'),
    '908',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glacial Crasher'),
    (select id from sets where short_name = 'mb1'),
    '391',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weapons Trainer'),
    (select id from sets where short_name = 'mb1'),
    '1510',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beetleback Chief'),
    (select id from sets where short_name = 'mb1'),
    '852',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wrench Mind'),
    (select id from sets where short_name = 'mb1'),
    '828',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thought Collapse'),
    (select id from sets where short_name = 'mb1'),
    '521',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Sculpt'),
    (select id from sets where short_name = 'mb1'),
    '429',
    'common'
) ,
(
    (select id from mtgcard where name = 'Child of Night'),
    (select id from sets where short_name = 'mb1'),
    '598',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fierce Empath'),
    (select id from sets where short_name = 'mb1'),
    '1212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vraska''s Finisher'),
    (select id from sets where short_name = 'mb1'),
    '817',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treacherous Terrain'),
    (select id from sets where short_name = 'mb1'),
    '1500',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soulmender'),
    (select id from sets where short_name = 'mb1'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thornweald Archer'),
    (select id from sets where short_name = 'mb1'),
    '1355',
    'common'
) ,
(
    (select id from mtgcard where name = 'Storm Sculptor'),
    (select id from sets where short_name = 'mb1'),
    '506',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Visionary'),
    (select id from sets where short_name = 'mb1'),
    '1195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exsanguinate'),
    (select id from sets where short_name = 'mb1'),
    '651',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inquisitor''s Ox'),
    (select id from sets where short_name = 'mb1'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Mirari Conjecture'),
    (select id from sets where short_name = 'mb1'),
    '431',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bear''s Companion'),
    (select id from sets where short_name = 'mb1'),
    '1397',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dusk Legion Zealot'),
    (select id from sets where short_name = 'mb1'),
    '644',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nettle Sentinel'),
    (select id from sets where short_name = 'mb1'),
    '1278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quiet Disrepair'),
    (select id from sets where short_name = 'mb1'),
    '1301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flesh to Dust'),
    (select id from sets where short_name = 'mb1'),
    '663',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quest for the Gravelord'),
    (select id from sets where short_name = 'mb1'),
    '741',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reality Scramble'),
    (select id from sets where short_name = 'mb1'),
    '1034',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scroll Thief'),
    (select id from sets where short_name = 'mb1'),
    '479',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dominus of Fealty'),
    (select id from sets where short_name = 'mb1'),
    '1520',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gifted Aetherborn'),
    (select id from sets where short_name = 'mb1'),
    '669',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Akoum Refuge'),
    (select id from sets where short_name = 'mb1'),
    '1651',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Call to Heel'),
    (select id from sets where short_name = 'mb1'),
    '306',
    'common'
) ,
(
    (select id from mtgcard where name = 'Memory Erosion'),
    (select id from sets where short_name = 'mb1'),
    '424',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arrest'),
    (select id from sets where short_name = 'mb1'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archaeomancer'),
    (select id from sets where short_name = 'mb1'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Renegade Map'),
    (select id from sets where short_name = 'mb1'),
    '1623',
    'common'
) ,
(
    (select id from mtgcard where name = 'Palace Sentinels'),
    (select id from sets where short_name = 'mb1'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talons of Wildwood'),
    (select id from sets where short_name = 'mb1'),
    '1349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'mb1'),
    '1633',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gilt-Leaf Palace'),
    (select id from sets where short_name = 'mb1'),
    '1672',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Solemn Simulacrum'),
    (select id from sets where short_name = 'mb1'),
    '1632',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Weathered Wayfarer'),
    (select id from sets where short_name = 'mb1'),
    '274',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodlust Inciter'),
    (select id from sets where short_name = 'mb1'),
    '859',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kruphix, God of Horizons'),
    (select id from sets where short_name = 'mb1'),
    '1446',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Arbor Armament'),
    (select id from sets where short_name = 'mb1'),
    '1123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Firehoof Cavalry'),
    (select id from sets where short_name = 'mb1'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Concentrate'),
    (select id from sets where short_name = 'mb1'),
    '327',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Fireslinger'),
    (select id from sets where short_name = 'mb1'),
    '953',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pentarch Ward'),
    (select id from sets where short_name = 'mb1'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nine-Tail White Fox'),
    (select id from sets where short_name = 'mb1'),
    '445',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ondu Giant'),
    (select id from sets where short_name = 'mb1'),
    '1283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Messenger Jays'),
    (select id from sets where short_name = 'mb1'),
    '427',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Haven Medic'),
    (select id from sets where short_name = 'mb1'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hyena Pack'),
    (select id from sets where short_name = 'mb1'),
    '976',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grim Discovery'),
    (select id from sets where short_name = 'mb1'),
    '679',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blossoming Sands'),
    (select id from sets where short_name = 'mb1'),
    '1659',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marauding Boneslasher'),
    (select id from sets where short_name = 'mb1'),
    '705',
    'common'
) ,
(
    (select id from mtgcard where name = 'All Is Dust'),
    (select id from sets where short_name = 'mb1'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gnarlid Pack'),
    (select id from sets where short_name = 'mb1'),
    '1223',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pulse of Murasa'),
    (select id from sets where short_name = 'mb1'),
    '1300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Protector'),
    (select id from sets where short_name = 'mb1'),
    '1217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancestral Mask'),
    (select id from sets where short_name = 'mb1'),
    '1119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zealous Strike'),
    (select id from sets where short_name = 'mb1'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Soulgorger'),
    (select id from sets where short_name = 'mb1'),
    '1619',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oreskos Swiftclaw'),
    (select id from sets where short_name = 'mb1'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cartouche of Solidarity'),
    (select id from sets where short_name = 'mb1'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple of the False God'),
    (select id from sets where short_name = 'mb1'),
    '1691',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nagging Thoughts'),
    (select id from sets where short_name = 'mb1'),
    '442',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blazing Volley'),
    (select id from sets where short_name = 'mb1'),
    '856',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormchaser Chimera'),
    (select id from sets where short_name = 'mb1'),
    '1490',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jwar Isle Avenger'),
    (select id from sets where short_name = 'mb1'),
    '412',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greater Gargadon'),
    (select id from sets where short_name = 'mb1'),
    '968',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kargan Dragonlord'),
    (select id from sets where short_name = 'mb1'),
    '987',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Oracle'),
    (select id from sets where short_name = 'mb1'),
    '480',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fervent Strike'),
    (select id from sets where short_name = 'mb1'),
    '922',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glaring Aegis'),
    (select id from sets where short_name = 'mb1'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pestilence'),
    (select id from sets where short_name = 'mb1'),
    '729',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nature''s Claim'),
    (select id from sets where short_name = 'mb1'),
    '1275',
    'common'
) ,
(
    (select id from mtgcard where name = 'Totally Lost'),
    (select id from sets where short_name = 'mb1'),
    '528',
    'common'
) ,
(
    (select id from mtgcard where name = 'Supreme Verdict'),
    (select id from sets where short_name = 'mb1'),
    '1493',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frontline Devastator'),
    (select id from sets where short_name = 'mb1'),
    '939',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cast Out'),
    (select id from sets where short_name = 'mb1'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aura Shards'),
    (select id from sets where short_name = 'mb1'),
    '1392',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feral Abomination'),
    (select id from sets where short_name = 'mb1'),
    '657',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drag Under'),
    (select id from sets where short_name = 'mb1'),
    '357',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enthralling Victor'),
    (select id from sets where short_name = 'mb1'),
    '916',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Protection'),
    (select id from sets where short_name = 'mb1'),
    '256',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Niblis of Dusk'),
    (select id from sets where short_name = 'mb1'),
    '444',
    'common'
) ,
(
    (select id from mtgcard where name = 'Absorb Vis'),
    (select id from sets where short_name = 'mb1'),
    '558',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carrion Feeder'),
    (select id from sets where short_name = 'mb1'),
    '592',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sadistic Hypnotist'),
    (select id from sets where short_name = 'mb1'),
    '761',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hunter of Eyeblights'),
    (select id from sets where short_name = 'mb1'),
    '688',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aid the Fallen'),
    (select id from sets where short_name = 'mb1'),
    '560',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon''s Presence'),
    (select id from sets where short_name = 'mb1'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Burrows'),
    (select id from sets where short_name = 'mb1'),
    '1673',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bartizan Bats'),
    (select id from sets where short_name = 'mb1'),
    '571',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'mb1'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon''s Eye Savants'),
    (select id from sets where short_name = 'mb1'),
    '356',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scrounger of Souls'),
    (select id from sets where short_name = 'mb1'),
    '763',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fiery Hellhound'),
    (select id from sets where short_name = 'mb1'),
    '924',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Manipulation'),
    (select id from sets where short_name = 'mb1'),
    '1488',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Catacomb Slug'),
    (select id from sets where short_name = 'mb1'),
    '595',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curse of the Nightly Hunt'),
    (select id from sets where short_name = 'mb1'),
    '897',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skullclamp'),
    (select id from sets where short_name = 'mb1'),
    '1630',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prosperous Pirates'),
    (select id from sets where short_name = 'mb1'),
    '463',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coldsteel Heart'),
    (select id from sets where short_name = 'mb1'),
    '1562',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reviving Dose'),
    (select id from sets where short_name = 'mb1'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcane Denial'),
    (select id from sets where short_name = 'mb1'),
    '288',
    'common'
) ,
(
    (select id from mtgcard where name = 'Calculated Dismissal'),
    (select id from sets where short_name = 'mb1'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moment of Craving'),
    (select id from sets where short_name = 'mb1'),
    '716',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strength in Numbers'),
    (select id from sets where short_name = 'mb1'),
    '1343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bitter Revelation'),
    (select id from sets where short_name = 'mb1'),
    '572',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relic Crush'),
    (select id from sets where short_name = 'mb1'),
    '1310',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hidden Stockpile'),
    (select id from sets where short_name = 'mb1'),
    '1435',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ringwarden Owl'),
    (select id from sets where short_name = 'mb1'),
    '470',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kiss of the Amesha'),
    (select id from sets where short_name = 'mb1'),
    '1444',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zealot of the God-Pharaoh'),
    (select id from sets where short_name = 'mb1'),
    '1107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tireless Tracker'),
    (select id from sets where short_name = 'mb1'),
    '1361',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Martyr''s Bond'),
    (select id from sets where short_name = 'mb1'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kathari Remnant'),
    (select id from sets where short_name = 'mb1'),
    '1441',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Assault'),
    (select id from sets where short_name = 'mb1'),
    '950',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vandalize'),
    (select id from sets where short_name = 'mb1'),
    '1093',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frontier Mastodon'),
    (select id from sets where short_name = 'mb1'),
    '1215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sejiri Refuge'),
    (select id from sets where short_name = 'mb1'),
    '1687',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Star-Crowned Stag'),
    (select id from sets where short_name = 'mb1'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fierce Invocation'),
    (select id from sets where short_name = 'mb1'),
    '923',
    'common'
) ,
(
    (select id from mtgcard where name = 'Surrakar Banisher'),
    (select id from sets where short_name = 'mb1'),
    '510',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = 'mb1'),
    '713',
    'common'
) ,
(
    (select id from mtgcard where name = 'Filigree Familiar'),
    (select id from sets where short_name = 'mb1'),
    '1582',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hammer Dropper'),
    (select id from sets where short_name = 'mb1'),
    '1434',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desert Twister'),
    (select id from sets where short_name = 'mb1'),
    '1181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shoulder to Shoulder'),
    (select id from sets where short_name = 'mb1'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wight of Precinct Six'),
    (select id from sets where short_name = 'mb1'),
    '825',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yeva''s Forcemage'),
    (select id from sets where short_name = 'mb1'),
    '1381',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thornscape Battlemage'),
    (select id from sets where short_name = 'mb1'),
    '1354',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thought Erasure'),
    (select id from sets where short_name = 'mb1'),
    '1496',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Divine Favor'),
    (select id from sets where short_name = 'mb1'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leopard-Spotted Jiao'),
    (select id from sets where short_name = 'mb1'),
    '1000',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lashknife Barrier'),
    (select id from sets where short_name = 'mb1'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Run Amok'),
    (select id from sets where short_name = 'mb1'),
    '1047',
    'common'
) ,
(
    (select id from mtgcard where name = 'Turntimber Basilisk'),
    (select id from sets where short_name = 'mb1'),
    '1365',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wayfaring Temple'),
    (select id from sets where short_name = 'mb1'),
    '1509',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Secrets of the Golden City'),
    (select id from sets where short_name = 'mb1'),
    '482',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gift of Orzhova'),
    (select id from sets where short_name = 'mb1'),
    '1524',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cragganwick Cremator'),
    (select id from sets where short_name = 'mb1'),
    '892',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raging Swordtooth'),
    (select id from sets where short_name = 'mb1'),
    '1471',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Voldaren Duelist'),
    (select id from sets where short_name = 'mb1'),
    '1099',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lord of the Accursed'),
    (select id from sets where short_name = 'mb1'),
    '703',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Costly Plunder'),
    (select id from sets where short_name = 'mb1'),
    '602',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ivy Lane Denizen'),
    (select id from sets where short_name = 'mb1'),
    '1244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cabal Therapy'),
    (select id from sets where short_name = 'mb1'),
    '587',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ahn-Crop Crasher'),
    (select id from sets where short_name = 'mb1'),
    '833',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Fodder'),
    (select id from sets where short_name = 'mb1'),
    '909',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crystal Shard'),
    (select id from sets where short_name = 'mb1'),
    '1570',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Borrowed Grace'),
    (select id from sets where short_name = 'mb1'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time Sieve'),
    (select id from sets where short_name = 'mb1'),
    '1497',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Bauble'),
    (select id from sets where short_name = 'mb1'),
    '1610',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boiling Earth'),
    (select id from sets where short_name = 'mb1'),
    '866',
    'common'
) ,
(
    (select id from mtgcard where name = 'Purphoros, God of the Forge'),
    (select id from sets where short_name = 'mb1'),
    '1029',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Firebolt'),
    (select id from sets where short_name = 'mb1'),
    '927',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aerie Bowmasters'),
    (select id from sets where short_name = 'mb1'),
    '1111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frost Lynx'),
    (select id from sets where short_name = 'mb1'),
    '388',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cobblebrute'),
    (select id from sets where short_name = 'mb1'),
    '890',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ninth Bridge Patrol'),
    (select id from sets where short_name = 'mb1'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Guildmage'),
    (select id from sets where short_name = 'mb1'),
    '1532',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ruin Rat'),
    (select id from sets where short_name = 'mb1'),
    '759',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mire''s Malice'),
    (select id from sets where short_name = 'mb1'),
    '715',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gaseous Form'),
    (select id from sets where short_name = 'mb1'),
    '389',
    'common'
) ,
(
    (select id from mtgcard where name = 'Makindi Sliderunner'),
    (select id from sets where short_name = 'mb1'),
    '1007',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winged Shepherd'),
    (select id from sets where short_name = 'mb1'),
    '277',
    'common'
) ,
(
    (select id from mtgcard where name = 'Territorial Baloth'),
    (select id from sets where short_name = 'mb1'),
    '1352',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cunning Breezedancer'),
    (select id from sets where short_name = 'mb1'),
    '1413',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Windcaller Aven'),
    (select id from sets where short_name = 'mb1'),
    '548',
    'common'
) ,
(
    (select id from mtgcard where name = 'River Boa'),
    (select id from sets where short_name = 'mb1'),
    '1315',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Walking Corpse'),
    (select id from sets where short_name = 'mb1'),
    '819',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rubblebelt Maaka'),
    (select id from sets where short_name = 'mb1'),
    '1044',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mardu Warshrieker'),
    (select id from sets where short_name = 'mb1'),
    '1008',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhet-Crop Spearmaster'),
    (select id from sets where short_name = 'mb1'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wind Strider'),
    (select id from sets where short_name = 'mb1'),
    '552',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Wurm'),
    (select id from sets where short_name = 'mb1'),
    '1036',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eternal Witness'),
    (select id from sets where short_name = 'mb1'),
    '1200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Commit // Memory'),
    (select id from sets where short_name = 'mb1'),
    '1537',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bladebrand'),
    (select id from sets where short_name = 'mb1'),
    '576',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spreading Rot'),
    (select id from sets where short_name = 'mb1'),
    '779',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alchemist''s Greeting'),
    (select id from sets where short_name = 'mb1'),
    '836',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Griffin'),
    (select id from sets where short_name = 'mb1'),
    '275',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spawning Grounds'),
    (select id from sets where short_name = 'mb1'),
    '1338',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reclaim'),
    (select id from sets where short_name = 'mb1'),
    '1307',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pelakka Wurm'),
    (select id from sets where short_name = 'mb1'),
    '1290',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skeletal Scrying'),
    (select id from sets where short_name = 'mb1'),
    '774',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'New Benalia'),
    (select id from sets where short_name = 'mb1'),
    '1681',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sewer Nemesis'),
    (select id from sets where short_name = 'mb1'),
    '767',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cathartic Reunion'),
    (select id from sets where short_name = 'mb1'),
    '882',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frontier Bivouac'),
    (select id from sets where short_name = 'mb1'),
    '1669',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blossom Dryad'),
    (select id from sets where short_name = 'mb1'),
    '1144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seek the Wilds'),
    (select id from sets where short_name = 'mb1'),
    '1329',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sulfurous Blast'),
    (select id from sets where short_name = 'mb1'),
    '1072',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Murder'),
    (select id from sets where short_name = 'mb1'),
    '717',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'mb1'),
    '1001',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crash Through'),
    (select id from sets where short_name = 'mb1'),
    '893',
    'common'
) ,
(
    (select id from mtgcard where name = 'Debtors'' Knell'),
    (select id from sets where short_name = 'mb1'),
    '1519',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blindblast'),
    (select id from sets where short_name = 'mb1'),
    '857',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cower in Fear'),
    (select id from sets where short_name = 'mb1'),
    '604',
    'common'
) ,
(
    (select id from mtgcard where name = 'Creeping Mold'),
    (select id from sets where short_name = 'mb1'),
    '1170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blastoderm'),
    (select id from sets where short_name = 'mb1'),
    '1142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marked by Honor'),
    (select id from sets where short_name = 'mb1'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tower of Eons'),
    (select id from sets where short_name = 'mb1'),
    '1643',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avalanche Riders'),
    (select id from sets where short_name = 'mb1'),
    '844',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clear the Mind'),
    (select id from sets where short_name = 'mb1'),
    '320',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Retriever'),
    (select id from sets where short_name = 'mb1'),
    '1613',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Invigorate'),
    (select id from sets where short_name = 'mb1'),
    '1243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unburden'),
    (select id from sets where short_name = 'mb1'),
    '804',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grim Affliction'),
    (select id from sets where short_name = 'mb1'),
    '678',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ondu Greathorn'),
    (select id from sets where short_name = 'mb1'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anger'),
    (select id from sets where short_name = 'mb1'),
    '839',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wayward Giant'),
    (select id from sets where short_name = 'mb1'),
    '1101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Renegade Demon'),
    (select id from sets where short_name = 'mb1'),
    '752',
    'common'
) ,
(
    (select id from mtgcard where name = 'March of the Drowned'),
    (select id from sets where short_name = 'mb1'),
    '706',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shining Armor'),
    (select id from sets where short_name = 'mb1'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sultai Charm'),
    (select id from sets where short_name = 'mb1'),
    '1491',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul-Strike Technique'),
    (select id from sets where short_name = 'mb1'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gonti, Lord of Luxury'),
    (select id from sets where short_name = 'mb1'),
    '671',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wheel of Fate'),
    (select id from sets where short_name = 'mb1'),
    '1102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Metamorph'),
    (select id from sets where short_name = 'mb1'),
    '456',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Condescend'),
    (select id from sets where short_name = 'mb1'),
    '328',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keldon Overseer'),
    (select id from sets where short_name = 'mb1'),
    '989',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dictate of Erebos'),
    (select id from sets where short_name = 'mb1'),
    '625',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rith, the Awakener'),
    (select id from sets where short_name = 'mb1'),
    '1476',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Charbelcher'),
    (select id from sets where short_name = 'mb1'),
    '1588',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Farmstead Gleaner'),
    (select id from sets where short_name = 'mb1'),
    '1581',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Earth Elemental'),
    (select id from sets where short_name = 'mb1'),
    '914',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cartouche of Zeal'),
    (select id from sets where short_name = 'mb1'),
    '881',
    'common'
) ,
(
    (select id from mtgcard where name = 'Devilthorn Fox'),
    (select id from sets where short_name = 'mb1'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Game'),
    (select id from sets where short_name = 'mb1'),
    '954',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skitter Eel'),
    (select id from sets where short_name = 'mb1'),
    '493',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sigiled Starfish'),
    (select id from sets where short_name = 'mb1'),
    '488',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krenko, Mob Boss'),
    (select id from sets where short_name = 'mb1'),
    '996',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dawnglare Invoker'),
    (select id from sets where short_name = 'mb1'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Animar, Soul of Elements'),
    (select id from sets where short_name = 'mb1'),
    '1387',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aura Gnarlid'),
    (select id from sets where short_name = 'mb1'),
    '1126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shimmerscale Drake'),
    (select id from sets where short_name = 'mb1'),
    '486',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fortify'),
    (select id from sets where short_name = 'mb1'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mahamoti Djinn'),
    (select id from sets where short_name = 'mb1'),
    '419',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Warden'),
    (select id from sets where short_name = 'mb1'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akroan Sergeant'),
    (select id from sets where short_name = 'mb1'),
    '835',
    'common'
) ,
(
    (select id from mtgcard where name = 'Call of the Nightwing'),
    (select id from sets where short_name = 'mb1'),
    '1405',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'mb1'),
    '674',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fatal Push'),
    (select id from sets where short_name = 'mb1'),
    '655',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Might of the Masses'),
    (select id from sets where short_name = 'mb1'),
    '1270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Isolation Zone'),
    (select id from sets where short_name = 'mb1'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Geist of the Moors'),
    (select id from sets where short_name = 'mb1'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emerge Unscathed'),
    (select id from sets where short_name = 'mb1'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Champion'),
    (select id from sets where short_name = 'mb1'),
    '809',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Pridemate'),
    (select id from sets where short_name = 'mb1'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meddling Mage'),
    (select id from sets where short_name = 'mb1'),
    '1453',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cartouche of Knowledge'),
    (select id from sets where short_name = 'mb1'),
    '309',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faithbearer Paladin'),
    (select id from sets where short_name = 'mb1'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savage Punch'),
    (select id from sets where short_name = 'mb1'),
    '1324',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beastmaster Ascension'),
    (select id from sets where short_name = 'mb1'),
    '1133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Questing Phelddagrif'),
    (select id from sets where short_name = 'mb1'),
    '1469',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imperious Perfect'),
    (select id from sets where short_name = 'mb1'),
    '1242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Outnumber'),
    (select id from sets where short_name = 'mb1'),
    '1023',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = 'mb1'),
    '1680',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dynacharge'),
    (select id from sets where short_name = 'mb1'),
    '913',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightform'),
    (select id from sets where short_name = 'mb1'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tarfire'),
    (select id from sets where short_name = 'mb1'),
    '1078',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cairn Wanderer'),
    (select id from sets where short_name = 'mb1'),
    '590',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infest'),
    (select id from sets where short_name = 'mb1'),
    '692',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Recruiter of the Guard'),
    (select id from sets where short_name = 'mb1'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cauldron of Souls'),
    (select id from sets where short_name = 'mb1'),
    '1558',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Natural Connection'),
    (select id from sets where short_name = 'mb1'),
    '1273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Map the Wastes'),
    (select id from sets where short_name = 'mb1'),
    '1268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alesha''s Vanguard'),
    (select id from sets where short_name = 'mb1'),
    '561',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rivals'' Duel'),
    (select id from sets where short_name = 'mb1'),
    '1041',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fogwalker'),
    (select id from sets where short_name = 'mb1'),
    '383',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kiora''s Dambreaker'),
    (select id from sets where short_name = 'mb1'),
    '413',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dread Return'),
    (select id from sets where short_name = 'mb1'),
    '636',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daggerback Basilisk'),
    (select id from sets where short_name = 'mb1'),
    '1177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Elemental'),
    (select id from sets where short_name = 'mb1'),
    '929',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blinding Souleater'),
    (select id from sets where short_name = 'mb1'),
    '1549',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Helix'),
    (select id from sets where short_name = 'mb1'),
    '1448',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Elder'),
    (select id from sets where short_name = 'mb1'),
    '1379',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Deathraiders'),
    (select id from sets where short_name = 'mb1'),
    '1431',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gift of Growth'),
    (select id from sets where short_name = 'mb1'),
    '1220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tithe Drinker'),
    (select id from sets where short_name = 'mb1'),
    '1498',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desperate Ravings'),
    (select id from sets where short_name = 'mb1'),
    '903',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Locksmith'),
    (select id from sets where short_name = 'mb1'),
    '955',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seek the Horizon'),
    (select id from sets where short_name = 'mb1'),
    '1328',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chartooth Cougar'),
    (select id from sets where short_name = 'mb1'),
    '887',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prodigal Sorcerer'),
    (select id from sets where short_name = 'mb1'),
    '461',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gravepurge'),
    (select id from sets where short_name = 'mb1'),
    '675',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inquisition of Kozilek'),
    (select id from sets where short_name = 'mb1'),
    '694',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Contingency Plan'),
    (select id from sets where short_name = 'mb1'),
    '330',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stallion of Ashmouth'),
    (select id from sets where short_name = 'mb1'),
    '781',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voracious Null'),
    (select id from sets where short_name = 'mb1'),
    '816',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giantbaiting'),
    (select id from sets where short_name = 'mb1'),
    '1523',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burst Lightning'),
    (select id from sets where short_name = 'mb1'),
    '879',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Mechanist'),
    (select id from sets where short_name = 'mb1'),
    '373',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhys the Redeemed'),
    (select id from sets where short_name = 'mb1'),
    '1530',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Great-Horn Krushok'),
    (select id from sets where short_name = 'mb1'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pit Keeper'),
    (select id from sets where short_name = 'mb1'),
    '734',
    'common'
) ,
(
    (select id from mtgcard where name = 'Refocus'),
    (select id from sets where short_name = 'mb1'),
    '465',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blade Instructor'),
    (select id from sets where short_name = 'mb1'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Relic-Warder'),
    (select id from sets where short_name = 'mb1'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unflinching Courage'),
    (select id from sets where short_name = 'mb1'),
    '1502',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Reclamation'),
    (select id from sets where short_name = 'mb1'),
    '733',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yavimaya''s Embrace'),
    (select id from sets where short_name = 'mb1'),
    '1514',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lead by Example'),
    (select id from sets where short_name = 'mb1'),
    '1258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Typhoid Rats'),
    (select id from sets where short_name = 'mb1'),
    '803',
    'common'
) ,
(
    (select id from mtgcard where name = 'Timberwatch Elf'),
    (select id from sets where short_name = 'mb1'),
    '1359',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llanowar Empath'),
    (select id from sets where short_name = 'mb1'),
    '1263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hammerhand'),
    (select id from sets where short_name = 'mb1'),
    '971',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunlance'),
    (select id from sets where short_name = 'mb1'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zealous Persecution'),
    (select id from sets where short_name = 'mb1'),
    '1516',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nirkana Assassin'),
    (select id from sets where short_name = 'mb1'),
    '724',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smelt'),
    (select id from sets where short_name = 'mb1'),
    '1062',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hideous End'),
    (select id from sets where short_name = 'mb1'),
    '685',
    'common'
) ,
(
    (select id from mtgcard where name = 'Recoup'),
    (select id from sets where short_name = 'mb1'),
    '1037',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Expropriate'),
    (select id from sets where short_name = 'mb1'),
    '370',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Subtle Strike'),
    (select id from sets where short_name = 'mb1'),
    '785',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cleansing Screech'),
    (select id from sets where short_name = 'mb1'),
    '889',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cultivate'),
    (select id from sets where short_name = 'mb1'),
    '1176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weldfast Wingsmith'),
    (select id from sets where short_name = 'mb1'),
    '543',
    'common'
) ,
(
    (select id from mtgcard where name = 'Timely Reinforcements'),
    (select id from sets where short_name = 'mb1'),
    '262',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragonlord Ojutai'),
    (select id from sets where short_name = 'mb1'),
    '1418',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nyx-Fleece Ram'),
    (select id from sets where short_name = 'mb1'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azorius Charm'),
    (select id from sets where short_name = 'mb1'),
    '1393',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Epicure of Blood'),
    (select id from sets where short_name = 'mb1'),
    '646',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cosmotronic Wave'),
    (select id from sets where short_name = 'mb1'),
    '891',
    'common'
) ,
(
    (select id from mtgcard where name = 'Screamreach Brawler'),
    (select id from sets where short_name = 'mb1'),
    '1052',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghoulcaller''s Accomplice'),
    (select id from sets where short_name = 'mb1'),
    '668',
    'common'
) ,
(
    (select id from mtgcard where name = 'Essence Warden'),
    (select id from sets where short_name = 'mb1'),
    '1199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seismic Stomp'),
    (select id from sets where short_name = 'mb1'),
    '1054',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistmeadow Witch'),
    (select id from sets where short_name = 'mb1'),
    '1527',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blighted Fen'),
    (select id from sets where short_name = 'mb1'),
    '1658',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anticipate'),
    (select id from sets where short_name = 'mb1'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kalastria Nightwatch'),
    (select id from sets where short_name = 'mb1'),
    '696',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'mb1'),
    '611',
    'common'
) ,
(
    (select id from mtgcard where name = 'Formless Nurturing'),
    (select id from sets where short_name = 'mb1'),
    '1214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steady Progress'),
    (select id from sets where short_name = 'mb1'),
    '504',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fledgling Mawcor'),
    (select id from sets where short_name = 'mb1'),
    '379',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Torment of Venom'),
    (select id from sets where short_name = 'mb1'),
    '796',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opt'),
    (select id from sets where short_name = 'mb1'),
    '451',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreadwaters'),
    (select id from sets where short_name = 'mb1'),
    '358',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death by Dragons'),
    (select id from sets where short_name = 'mb1'),
    '899',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wind Drake'),
    (select id from sets where short_name = 'mb1'),
    '549',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harmonize'),
    (select id from sets where short_name = 'mb1'),
    '1234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chatter of the Squirrel'),
    (select id from sets where short_name = 'mb1'),
    '1161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windrider Eel'),
    (select id from sets where short_name = 'mb1'),
    '551',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heavy Infantry'),
    (select id from sets where short_name = 'mb1'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curiosity'),
    (select id from sets where short_name = 'mb1'),
    '340',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snake Umbra'),
    (select id from sets where short_name = 'mb1'),
    '1336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Young Pyromancer'),
    (select id from sets where short_name = 'mb1'),
    '1105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Paladin of the Bloodstained'),
    (select id from sets where short_name = 'mb1'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desert Cerodon'),
    (select id from sets where short_name = 'mb1'),
    '902',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Hub'),
    (select id from sets where short_name = 'mb1'),
    '1650',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Renewed Faith'),
    (select id from sets where short_name = 'mb1'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Djinn of Wishes'),
    (select id from sets where short_name = 'mb1'),
    '354',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Erratic Explosion'),
    (select id from sets where short_name = 'mb1'),
    '917',
    'common'
) ,
(
    (select id from mtgcard where name = 'Djeru''s Resolve'),
    (select id from sets where short_name = 'mb1'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seal of Cleansing'),
    (select id from sets where short_name = 'mb1'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dismember'),
    (select id from sets where short_name = 'mb1'),
    '629',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Umbral Mantle'),
    (select id from sets where short_name = 'mb1'),
    '1646',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serrated Arrows'),
    (select id from sets where short_name = 'mb1'),
    '1626',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weirded Vampire'),
    (select id from sets where short_name = 'mb1'),
    '824',
    'common'
) ,
(
    (select id from mtgcard where name = 'Qasali Pridemage'),
    (select id from sets where short_name = 'mb1'),
    '1467',
    'common'
) ,
(
    (select id from mtgcard where name = 'Peace of Mind'),
    (select id from sets where short_name = 'mb1'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hedron Crab'),
    (select id from sets where short_name = 'mb1'),
    '398',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flame Jab'),
    (select id from sets where short_name = 'mb1'),
    '930',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thalia''s Lancers'),
    (select id from sets where short_name = 'mb1'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shaper Parasite'),
    (select id from sets where short_name = 'mb1'),
    '485',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bituminous Blast'),
    (select id from sets where short_name = 'mb1'),
    '1399',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doomgape'),
    (select id from sets where short_name = 'mb1'),
    '1521',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drana''s Emissary'),
    (select id from sets where short_name = 'mb1'),
    '1419',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Etched Oracle'),
    (select id from sets where short_name = 'mb1'),
    '1580',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flame-Kin Zealot'),
    (select id from sets where short_name = 'mb1'),
    '1426',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brimstone Dragon'),
    (select id from sets where short_name = 'mb1'),
    '873',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Contraband Kingpin'),
    (select id from sets where short_name = 'mb1'),
    '1410',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight of Sorrows'),
    (select id from sets where short_name = 'mb1'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Contagion Clasp'),
    (select id from sets where short_name = 'mb1'),
    '1564',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Satyr Enchanter'),
    (select id from sets where short_name = 'mb1'),
    '1479',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tavern Swindler'),
    (select id from sets where short_name = 'mb1'),
    '788',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Celestial Flare'),
    (select id from sets where short_name = 'mb1'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'River Hoopoe'),
    (select id from sets where short_name = 'mb1'),
    '1477',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Implement of Malice'),
    (select id from sets where short_name = 'mb1'),
    '1597',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wellwisher'),
    (select id from sets where short_name = 'mb1'),
    '1370',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teferi, Temporal Archmage'),
    (select id from sets where short_name = 'mb1'),
    '515',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blessed Spirits'),
    (select id from sets where short_name = 'mb1'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Growth'),
    (select id from sets where short_name = 'mb1'),
    '1371',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bow of Nylea'),
    (select id from sets where short_name = 'mb1'),
    '1147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystic Confluence'),
    (select id from sets where short_name = 'mb1'),
    '440',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Portent'),
    (select id from sets where short_name = 'mb1'),
    '458',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kavu Climber'),
    (select id from sets where short_name = 'mb1'),
    '1247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Territorial Hammerskull'),
    (select id from sets where short_name = 'mb1'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Crowd Goes Wild'),
    (select id from sets where short_name = 'mb1'),
    '1173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Capture Sphere'),
    (select id from sets where short_name = 'mb1'),
    '308',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rishadan Footpad'),
    (select id from sets where short_name = 'mb1'),
    '471',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guard Gomazoa'),
    (select id from sets where short_name = 'mb1'),
    '395',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jungle Delver'),
    (select id from sets where short_name = 'mb1'),
    '1245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Laboratory Maniac'),
    (select id from sets where short_name = 'mb1'),
    '415',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thought Vessel'),
    (select id from sets where short_name = 'mb1'),
    '1639',
    'common'
) ,
(
    (select id from mtgcard where name = 'Act of Treason'),
    (select id from sets where short_name = 'mb1'),
    '831',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gwyllion Hedge-Mage'),
    (select id from sets where short_name = 'mb1'),
    '1525',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Generator Servant'),
    (select id from sets where short_name = 'mb1'),
    '944',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Dragon'),
    (select id from sets where short_name = 'mb1'),
    '1097',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ill-Tempered Cyclops'),
    (select id from sets where short_name = 'mb1'),
    '977',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vigor'),
    (select id from sets where short_name = 'mb1'),
    '1368',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Millikin'),
    (select id from sets where short_name = 'mb1'),
    '1606',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Price of Progress'),
    (select id from sets where short_name = 'mb1'),
    '1026',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wake the Reflections'),
    (select id from sets where short_name = 'mb1'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doomed Dissenter'),
    (select id from sets where short_name = 'mb1'),
    '631',
    'common'
) ,
(
    (select id from mtgcard where name = 'Collar the Culprit'),
    (select id from sets where short_name = 'mb1'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Search for Tomorrow'),
    (select id from sets where short_name = 'mb1'),
    '1327',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beastbreaker of Bala Ged'),
    (select id from sets where short_name = 'mb1'),
    '1132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Write into Being'),
    (select id from sets where short_name = 'mb1'),
    '556',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clutch of Currents'),
    (select id from sets where short_name = 'mb1'),
    '325',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'mb1'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Intrusive Packbeast'),
    (select id from sets where short_name = 'mb1'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Combo Attack'),
    (select id from sets where short_name = 'mb1'),
    '1165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sparring Mummy'),
    (select id from sets where short_name = 'mb1'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mask of Memory'),
    (select id from sets where short_name = 'mb1'),
    '1604',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Encircling Fissure'),
    (select id from sets where short_name = 'mb1'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snapping Sailback'),
    (select id from sets where short_name = 'mb1'),
    '1337',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Runeclaw Bear'),
    (select id from sets where short_name = 'mb1'),
    '1320',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Bear'),
    (select id from sets where short_name = 'mb1'),
    '453',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirran Crusader'),
    (select id from sets where short_name = 'mb1'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thorn of the Black Rose'),
    (select id from sets where short_name = 'mb1'),
    '792',
    'common'
) ,
(
    (select id from mtgcard where name = 'Douse in Gloom'),
    (select id from sets where short_name = 'mb1'),
    '632',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enchanted Evening'),
    (select id from sets where short_name = 'mb1'),
    '1522',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gurmag Drowner'),
    (select id from sets where short_name = 'mb1'),
    '396',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daring Skyjek'),
    (select id from sets where short_name = 'mb1'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Frost'),
    (select id from sets where short_name = 'mb1'),
    '539',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arrester''s Zeal'),
    (select id from sets where short_name = 'mb1'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Academy Journeymage'),
    (select id from sets where short_name = 'mb1'),
    '281',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wildsize'),
    (select id from sets where short_name = 'mb1'),
    '1374',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bomber Corps'),
    (select id from sets where short_name = 'mb1'),
    '868',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grasp of Phantoms'),
    (select id from sets where short_name = 'mb1'),
    '394',
    'common'
) ,
(
    (select id from mtgcard where name = 'Revel in Riches'),
    (select id from sets where short_name = 'mb1'),
    '755',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'mb1'),
    '1262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pouncing Cheetah'),
    (select id from sets where short_name = 'mb1'),
    '1296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancient Den'),
    (select id from sets where short_name = 'mb1'),
    '1652',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crow of Dark Tidings'),
    (select id from sets where short_name = 'mb1'),
    '606',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prowling Pangolin'),
    (select id from sets where short_name = 'mb1'),
    '739',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trinket Mage'),
    (select id from sets where short_name = 'mb1'),
    '533',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beacon of Immortality'),
    (select id from sets where short_name = 'mb1'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Catalog'),
    (select id from sets where short_name = 'mb1'),
    '311',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Invaders'),
    (select id from sets where short_name = 'mb1'),
    '372',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodfire Expert'),
    (select id from sets where short_name = 'mb1'),
    '858',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daze'),
    (select id from sets where short_name = 'mb1'),
    '342',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silverchase Fox'),
    (select id from sets where short_name = 'mb1'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magus of the Moat'),
    (select id from sets where short_name = 'mb1'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Courser of Kruphix'),
    (select id from sets where short_name = 'mb1'),
    '1169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Millstone'),
    (select id from sets where short_name = 'mb1'),
    '1607',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tandem Tactics'),
    (select id from sets where short_name = 'mb1'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancient Grudge'),
    (select id from sets where short_name = 'mb1'),
    '838',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fade into Antiquity'),
    (select id from sets where short_name = 'mb1'),
    '1205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guardian Shield-Bearer'),
    (select id from sets where short_name = 'mb1'),
    '1231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snubhorn Sentry'),
    (select id from sets where short_name = 'mb1'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Collective Brutality'),
    (select id from sets where short_name = 'mb1'),
    '600',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jeskai Sage'),
    (select id from sets where short_name = 'mb1'),
    '410',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakshasa''s Secret'),
    (select id from sets where short_name = 'mb1'),
    '744',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baleful Ammit'),
    (select id from sets where short_name = 'mb1'),
    '569',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Domesticated Hydra'),
    (select id from sets where short_name = 'mb1'),
    '1184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Bell Monk'),
    (select id from sets where short_name = 'mb1'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gush'),
    (select id from sets where short_name = 'mb1'),
    '397',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infernal Scarring'),
    (select id from sets where short_name = 'mb1'),
    '691',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloud Elemental'),
    (select id from sets where short_name = 'mb1'),
    '322',
    'common'
) ,
(
    (select id from mtgcard where name = 'Belligerent Brontodon'),
    (select id from sets where short_name = 'mb1'),
    '1398',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scuttling Death'),
    (select id from sets where short_name = 'mb1'),
    '764',
    'common'
) ,
(
    (select id from mtgcard where name = 'Draco'),
    (select id from sets where short_name = 'mb1'),
    '1574',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = 'mb1'),
    '1055',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stoic Builder'),
    (select id from sets where short_name = 'mb1'),
    '1342',
    'common'
) ,
(
    (select id from mtgcard where name = 'Errant Ephemeron'),
    (select id from sets where short_name = 'mb1'),
    '366',
    'common'
) ,
(
    (select id from mtgcard where name = 'Penumbra Spider'),
    (select id from sets where short_name = 'mb1'),
    '1291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampaging Cyclops'),
    (select id from sets where short_name = 'mb1'),
    '1033',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temporal Mastery'),
    (select id from sets where short_name = 'mb1'),
    '517',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Countless Gears Renegade'),
    (select id from sets where short_name = 'mb1'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic of the Hidden Way'),
    (select id from sets where short_name = 'mb1'),
    '441',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gideon''s Lawkeeper'),
    (select id from sets where short_name = 'mb1'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corrupted Conscience'),
    (select id from sets where short_name = 'mb1'),
    '335',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arrow Storm'),
    (select id from sets where short_name = 'mb1'),
    '842',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dungrove Elder'),
    (select id from sets where short_name = 'mb1'),
    '1187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crystal Ball'),
    (select id from sets where short_name = 'mb1'),
    '1568',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grapple with the Past'),
    (select id from sets where short_name = 'mb1'),
    '1224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Repulse'),
    (select id from sets where short_name = 'mb1'),
    '466',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dictate of Heliod'),
    (select id from sets where short_name = 'mb1'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dual Shot'),
    (select id from sets where short_name = 'mb1'),
    '912',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mimic Vat'),
    (select id from sets where short_name = 'mb1'),
    '1608',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Encampment Keeper'),
    (select id from sets where short_name = 'mb1'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skirk Prospector'),
    (select id from sets where short_name = 'mb1'),
    '1060',
    'common'
) ,
(
    (select id from mtgcard where name = 'Universal Automaton'),
    (select id from sets where short_name = 'mb1'),
    '1647',
    'common'
) ,
(
    (select id from mtgcard where name = 'Durkwood Baloth'),
    (select id from sets where short_name = 'mb1'),
    '1188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = 'mb1'),
    '766',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spikeshot Goblin'),
    (select id from sets where short_name = 'mb1'),
    '1066',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grasp of the Hieromancer'),
    (select id from sets where short_name = 'mb1'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alley Evasion'),
    (select id from sets where short_name = 'mb1'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sultai Runemark'),
    (select id from sets where short_name = 'mb1'),
    '786',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fling'),
    (select id from sets where short_name = 'mb1'),
    '934',
    'common'
) ,
(
    (select id from mtgcard where name = 'Haakon, Stromgald Scourge'),
    (select id from sets where short_name = 'mb1'),
    '684',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fire // Ice'),
    (select id from sets where short_name = 'mb1'),
    '1538',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rain of Thorns'),
    (select id from sets where short_name = 'mb1'),
    '1302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Return to the Earth'),
    (select id from sets where short_name = 'mb1'),
    '1311',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terashi''s Grasp'),
    (select id from sets where short_name = 'mb1'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Apostle''s Blessing'),
    (select id from sets where short_name = 'mb1'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Stone'),
    (select id from sets where short_name = 'mb1'),
    '1609',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrun, the Last Troll'),
    (select id from sets where short_name = 'mb1'),
    '1358',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Death-Hood Cobra'),
    (select id from sets where short_name = 'mb1'),
    '1179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrill of Possibility'),
    (select id from sets where short_name = 'mb1'),
    '1083',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloak of Mists'),
    (select id from sets where short_name = 'mb1'),
    '321',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dead Reveler'),
    (select id from sets where short_name = 'mb1'),
    '617',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ballynock Cohort'),
    (select id from sets where short_name = 'mb1'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prophetic Ravings'),
    (select id from sets where short_name = 'mb1'),
    '1028',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shipwreck Looter'),
    (select id from sets where short_name = 'mb1'),
    '487',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunter''s Ambush'),
    (select id from sets where short_name = 'mb1'),
    '1239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iroas''s Champion'),
    (select id from sets where short_name = 'mb1'),
    '1438',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Center Soul'),
    (select id from sets where short_name = 'mb1'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maximize Altitude'),
    (select id from sets where short_name = 'mb1'),
    '423',
    'common'
) ,
(
    (select id from mtgcard where name = 'Experiment One'),
    (select id from sets where short_name = 'mb1'),
    '1201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Expedite'),
    (select id from sets where short_name = 'mb1'),
    '918',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nest Invader'),
    (select id from sets where short_name = 'mb1'),
    '1277',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shamanic Revelation'),
    (select id from sets where short_name = 'mb1'),
    '1331',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Bombardment'),
    (select id from sets where short_name = 'mb1'),
    '952',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mephitic Vapors'),
    (select id from sets where short_name = 'mb1'),
    '709',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ninja of the Deep Hours'),
    (select id from sets where short_name = 'mb1'),
    '446',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dread Drone'),
    (select id from sets where short_name = 'mb1'),
    '635',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vapor Snag'),
    (select id from sets where short_name = 'mb1'),
    '537',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battle Rampart'),
    (select id from sets where short_name = 'mb1'),
    '850',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kozilek''s Predator'),
    (select id from sets where short_name = 'mb1'),
    '1251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghostly Changeling'),
    (select id from sets where short_name = 'mb1'),
    '667',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thieving Magpie'),
    (select id from sets where short_name = 'mb1'),
    '518',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wojek Bodyguard'),
    (select id from sets where short_name = 'mb1'),
    '1104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crumbling Necropolis'),
    (select id from sets where short_name = 'mb1'),
    '1661',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Benthic Giant'),
    (select id from sets where short_name = 'mb1'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krosan Tusker'),
    (select id from sets where short_name = 'mb1'),
    '1255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Citanul Woodreaders'),
    (select id from sets where short_name = 'mb1'),
    '1162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Triumph of the Hordes'),
    (select id from sets where short_name = 'mb1'),
    '1363',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dream Cache'),
    (select id from sets where short_name = 'mb1'),
    '359',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sudden Demise'),
    (select id from sets where short_name = 'mb1'),
    '1071',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Artful Maneuver'),
    (select id from sets where short_name = 'mb1'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sultai Soothsayer'),
    (select id from sets where short_name = 'mb1'),
    '1492',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tajuru Pathwarden'),
    (select id from sets where short_name = 'mb1'),
    '1346',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dregscape Zombie'),
    (select id from sets where short_name = 'mb1'),
    '637',
    'common'
) ,
(
    (select id from mtgcard where name = 'Adventurous Impulse'),
    (select id from sets where short_name = 'mb1'),
    '1110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Balloon Brigade'),
    (select id from sets where short_name = 'mb1'),
    '951',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hulking Devil'),
    (select id from sets where short_name = 'mb1'),
    '975',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormblood Berserker'),
    (select id from sets where short_name = 'mb1'),
    '1070',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Acrobatic Maneuver'),
    (select id from sets where short_name = 'mb1'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shattering Spree'),
    (select id from sets where short_name = 'mb1'),
    '1056',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hightide Hermit'),
    (select id from sets where short_name = 'mb1'),
    '400',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hanweir Lancer'),
    (select id from sets where short_name = 'mb1'),
    '972',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curse of Opulence'),
    (select id from sets where short_name = 'mb1'),
    '896',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Archangel'),
    (select id from sets where short_name = 'mb1'),
    '1449',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Geomancer''s Gambit'),
    (select id from sets where short_name = 'mb1'),
    '946',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ainok Survivalist'),
    (select id from sets where short_name = 'mb1'),
    '1115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Charging Rhino'),
    (select id from sets where short_name = 'mb1'),
    '1160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kavu Primarch'),
    (select id from sets where short_name = 'mb1'),
    '1248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faithless Looting'),
    (select id from sets where short_name = 'mb1'),
    '919',
    'common'
) ,
(
    (select id from mtgcard where name = 'Healer''s Hawk'),
    (select id from sets where short_name = 'mb1'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magma Spray'),
    (select id from sets where short_name = 'mb1'),
    '1006',
    'common'
) ,
(
    (select id from mtgcard where name = 'Esper Charm'),
    (select id from sets where short_name = 'mb1'),
    '1421',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Okiba-Gang Shinobi'),
    (select id from sets where short_name = 'mb1'),
    '726',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cauldron Dance'),
    (select id from sets where short_name = 'mb1'),
    '1407',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lonesome Unicorn // Rider in Need'),
    (select id from sets where short_name = 'mb1'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Whelp'),
    (select id from sets where short_name = 'mb1'),
    '911',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eel Umbra'),
    (select id from sets where short_name = 'mb1'),
    '361',
    'common'
) ,
(
    (select id from mtgcard where name = 'Midnight Guard'),
    (select id from sets where short_name = 'mb1'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fblthp, the Lost'),
    (select id from sets where short_name = 'mb1'),
    '377',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savannah Lions'),
    (select id from sets where short_name = 'mb1'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sparktongue Dragon'),
    (select id from sets where short_name = 'mb1'),
    '1065',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ash Barrens'),
    (select id from sets where short_name = 'mb1'),
    '1656',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fiery Temper'),
    (select id from sets where short_name = 'mb1'),
    '925',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rotfeaster Maggot'),
    (select id from sets where short_name = 'mb1'),
    '758',
    'common'
) ,
(
    (select id from mtgcard where name = 'Torment of Hailfire'),
    (select id from sets where short_name = 'mb1'),
    '795',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meteorite'),
    (select id from sets where short_name = 'mb1'),
    '1605',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fragmentize'),
    (select id from sets where short_name = 'mb1'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'mb1'),
    '336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreadship Reef'),
    (select id from sets where short_name = 'mb1'),
    '1664',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fascination'),
    (select id from sets where short_name = 'mb1'),
    '375',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Reckoner'),
    (select id from sets where short_name = 'mb1'),
    '1518',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Belbe''s Portal'),
    (select id from sets where short_name = 'mb1'),
    '1548',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aethersnipe'),
    (select id from sets where short_name = 'mb1'),
    '282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Displace'),
    (select id from sets where short_name = 'mb1'),
    '351',
    'common'
) ,
(
    (select id from mtgcard where name = 'Defense of the Heart'),
    (select id from sets where short_name = 'mb1'),
    '1180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Certain Death'),
    (select id from sets where short_name = 'mb1'),
    '597',
    'common'
) ,
(
    (select id from mtgcard where name = 'Furnace Whelp'),
    (select id from sets where short_name = 'mb1'),
    '941',
    'common'
) ,
(
    (select id from mtgcard where name = 'Irontread Crusher'),
    (select id from sets where short_name = 'mb1'),
    '1598',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kazandu Refuge'),
    (select id from sets where short_name = 'mb1'),
    '1678',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plagued Rusalka'),
    (select id from sets where short_name = 'mb1'),
    '736',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirror Entity'),
    (select id from sets where short_name = 'mb1'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloom Tender'),
    (select id from sets where short_name = 'mb1'),
    '1143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lead the Stampede'),
    (select id from sets where short_name = 'mb1'),
    '1259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mycoloth'),
    (select id from sets where short_name = 'mb1'),
    '1272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mardu Hordechief'),
    (select id from sets where short_name = 'mb1'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu'),
    (select id from sets where short_name = 'mb1'),
    '932',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demolish'),
    (select id from sets where short_name = 'mb1'),
    '901',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kolaghan''s Command'),
    (select id from sets where short_name = 'mb1'),
    '1445',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sacred Cat'),
    (select id from sets where short_name = 'mb1'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ojutai''s Breath'),
    (select id from sets where short_name = 'mb1'),
    '448',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon''s Grasp'),
    (select id from sets where short_name = 'mb1'),
    '622',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guttersnipe'),
    (select id from sets where short_name = 'mb1'),
    '970',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bellows Lizard'),
    (select id from sets where short_name = 'mb1'),
    '853',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deepglow Skate'),
    (select id from sets where short_name = 'mb1'),
    '347',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yuriko, the Tiger''s Shadow'),
    (select id from sets where short_name = 'mb1'),
    '1515',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chronostutter'),
    (select id from sets where short_name = 'mb1'),
    '316',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyward Eye Prophets'),
    (select id from sets where short_name = 'mb1'),
    '1486',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dreadbringer Lampads'),
    (select id from sets where short_name = 'mb1'),
    '634',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demonic Vigor'),
    (select id from sets where short_name = 'mb1'),
    '621',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eater of Days'),
    (select id from sets where short_name = 'mb1'),
    '1576',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Explosive Vegetation'),
    (select id from sets where short_name = 'mb1'),
    '1203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fountain of Renewal'),
    (select id from sets where short_name = 'mb1'),
    '1586',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kor Sky Climber'),
    (select id from sets where short_name = 'mb1'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aggressive Urge'),
    (select id from sets where short_name = 'mb1'),
    '1114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krumar Bond-Kin'),
    (select id from sets where short_name = 'mb1'),
    '697',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thought Scour'),
    (select id from sets where short_name = 'mb1'),
    '522',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Warchief'),
    (select id from sets where short_name = 'mb1'),
    '961',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scoured Barrens'),
    (select id from sets where short_name = 'mb1'),
    '1686',
    'common'
) ,
(
    (select id from mtgcard where name = 'Defiant Strike'),
    (select id from sets where short_name = 'mb1'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foundry Street Denizen'),
    (select id from sets where short_name = 'mb1'),
    '936',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rune-Scarred Demon'),
    (select id from sets where short_name = 'mb1'),
    '760',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Congregate'),
    (select id from sets where short_name = 'mb1'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rhystic Study'),
    (select id from sets where short_name = 'mb1'),
    '468',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gustcloak Skirmisher'),
    (select id from sets where short_name = 'mb1'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Felidar Umbra'),
    (select id from sets where short_name = 'mb1'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = 'mb1'),
    '1274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conviction'),
    (select id from sets where short_name = 'mb1'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ondu Champion'),
    (select id from sets where short_name = 'mb1'),
    '1020',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fertile Ground'),
    (select id from sets where short_name = 'mb1'),
    '1211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chancellor of the Annex'),
    (select id from sets where short_name = 'mb1'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twins of Maurer Estate'),
    (select id from sets where short_name = 'mb1'),
    '802',
    'common'
) ,
(
    (select id from mtgcard where name = 'Font of Mythos'),
    (select id from sets where short_name = 'mb1'),
    '1584',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruul Signet'),
    (select id from sets where short_name = 'mb1'),
    '1589',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dispel'),
    (select id from sets where short_name = 'mb1'),
    '350',
    'common'
) ,
(
    (select id from mtgcard where name = 'Asceticism'),
    (select id from sets where short_name = 'mb1'),
    '1125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Precursor Golem'),
    (select id from sets where short_name = 'mb1'),
    '1621',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unclaimed Territory'),
    (select id from sets where short_name = 'mb1'),
    '1693',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Topan Freeblade'),
    (select id from sets where short_name = 'mb1'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sedraxis Specter'),
    (select id from sets where short_name = 'mb1'),
    '1482',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Valley Dasher'),
    (select id from sets where short_name = 'mb1'),
    '1092',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foundry Inspector'),
    (select id from sets where short_name = 'mb1'),
    '1585',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reclusive Artificer'),
    (select id from sets where short_name = 'mb1'),
    '1472',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whispersilk Cloak'),
    (select id from sets where short_name = 'mb1'),
    '1649',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wolfkin Bond'),
    (select id from sets where short_name = 'mb1'),
    '1375',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shriekmaw'),
    (select id from sets where short_name = 'mb1'),
    '771',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bitterbow Sharpshooters'),
    (select id from sets where short_name = 'mb1'),
    '1140',
    'common'
) ,
(
    (select id from mtgcard where name = 'New Horizons'),
    (select id from sets where short_name = 'mb1'),
    '1279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riparian Tiger'),
    (select id from sets where short_name = 'mb1'),
    '1314',
    'common'
) ,
(
    (select id from mtgcard where name = 'Choking Tethers'),
    (select id from sets where short_name = 'mb1'),
    '315',
    'common'
) ,
(
    (select id from mtgcard where name = 'Farseek'),
    (select id from sets where short_name = 'mb1'),
    '1206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lieutenants of the Guard'),
    (select id from sets where short_name = 'mb1'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Copper Carapace'),
    (select id from sets where short_name = 'mb1'),
    '1565',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simic Locket'),
    (select id from sets where short_name = 'mb1'),
    '1629',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wander in Death'),
    (select id from sets where short_name = 'mb1'),
    '821',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tibalt''s Rager'),
    (select id from sets where short_name = 'mb1'),
    '1084',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ravenous Leucrocota'),
    (select id from sets where short_name = 'mb1'),
    '1306',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feed the Clan'),
    (select id from sets where short_name = 'mb1'),
    '1207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Catacomb Crocodile'),
    (select id from sets where short_name = 'mb1'),
    '594',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demonic Tutor'),
    (select id from sets where short_name = 'mb1'),
    '620',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Court Hussar'),
    (select id from sets where short_name = 'mb1'),
    '337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azra Oddsmaker'),
    (select id from sets where short_name = 'mb1'),
    '1394',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightwalker'),
    (select id from sets where short_name = 'mb1'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Rush'),
    (select id from sets where short_name = 'mb1'),
    '1098',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Mongrel'),
    (select id from sets where short_name = 'mb1'),
    '1372',
    'common'
) ,
(
    (select id from mtgcard where name = 'Resurrection'),
    (select id from sets where short_name = 'mb1'),
    '209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rush of Adrenaline'),
    (select id from sets where short_name = 'mb1'),
    '1048',
    'common'
) ,
(
    (select id from mtgcard where name = 'Palace Jailer'),
    (select id from sets where short_name = 'mb1'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shrouded Lore'),
    (select id from sets where short_name = 'mb1'),
    '772',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zendikar''s Roil'),
    (select id from sets where short_name = 'mb1'),
    '1382',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pillage'),
    (select id from sets where short_name = 'mb1'),
    '1024',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brimstone Mage'),
    (select id from sets where short_name = 'mb1'),
    '874',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glade Watcher'),
    (select id from sets where short_name = 'mb1'),
    '1222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thresher Lizard'),
    (select id from sets where short_name = 'mb1'),
    '1082',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cadaver Imp'),
    (select id from sets where short_name = 'mb1'),
    '589',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deep Analysis'),
    (select id from sets where short_name = 'mb1'),
    '345',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bastion Inventor'),
    (select id from sets where short_name = 'mb1'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ingot Chewer'),
    (select id from sets where short_name = 'mb1'),
    '983',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brazen Wolves'),
    (select id from sets where short_name = 'mb1'),
    '872',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Piledriver'),
    (select id from sets where short_name = 'mb1'),
    '959',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wake of Vultures'),
    (select id from sets where short_name = 'mb1'),
    '818',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grapeshot'),
    (select id from sets where short_name = 'mb1'),
    '966',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overgrown Battlement'),
    (select id from sets where short_name = 'mb1'),
    '1286',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arc Trail'),
    (select id from sets where short_name = 'mb1'),
    '841',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Baleful Strix'),
    (select id from sets where short_name = 'mb1'),
    '1395',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghitu War Cry'),
    (select id from sets where short_name = 'mb1'),
    '948',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mulch'),
    (select id from sets where short_name = 'mb1'),
    '1271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bulwark Giant'),
    (select id from sets where short_name = 'mb1'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gorehorn Minotaurs'),
    (select id from sets where short_name = 'mb1'),
    '963',
    'common'
) ,
(
    (select id from mtgcard where name = 'Citadel Castellan'),
    (select id from sets where short_name = 'mb1'),
    '1408',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragonscale Boon'),
    (select id from sets where short_name = 'mb1'),
    '1185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Valakut Invoker'),
    (select id from sets where short_name = 'mb1'),
    '1090',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sakashima the Impostor'),
    (select id from sets where short_name = 'mb1'),
    '477',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benevolent Ancestor'),
    (select id from sets where short_name = 'mb1'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Covenant of Blood'),
    (select id from sets where short_name = 'mb1'),
    '603',
    'common'
) ,
(
    (select id from mtgcard where name = 'Court Street Denizen'),
    (select id from sets where short_name = 'mb1'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kiln Fiend'),
    (select id from sets where short_name = 'mb1'),
    '992',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cinder Hellion'),
    (select id from sets where short_name = 'mb1'),
    '888',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enlightened Maniac'),
    (select id from sets where short_name = 'mb1'),
    '364',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flameshot'),
    (select id from sets where short_name = 'mb1'),
    '931',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Akroan Hoplite'),
    (select id from sets where short_name = 'mb1'),
    '1386',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crown-Hunter Hireling'),
    (select id from sets where short_name = 'mb1'),
    '895',
    'common'
) ,
(
    (select id from mtgcard where name = 'Epic Confrontation'),
    (select id from sets where short_name = 'mb1'),
    '1198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Singing Bell Strike'),
    (select id from sets where short_name = 'mb1'),
    '491',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leaping Master'),
    (select id from sets where short_name = 'mb1'),
    '999',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = 'mb1'),
    '1303',
    'common'
) ,
(
    (select id from mtgcard where name = 'Macabre Waltz'),
    (select id from sets where short_name = 'mb1'),
    '704',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Specter'),
    (select id from sets where short_name = 'mb1'),
    '689',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wind-Kin Raiders'),
    (select id from sets where short_name = 'mb1'),
    '550',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Opportunity'),
    (select id from sets where short_name = 'mb1'),
    '450',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grixis Slavedriver'),
    (select id from sets where short_name = 'mb1'),
    '680',
    'common'
) ,
(
    (select id from mtgcard where name = 'Court Homunculus'),
    (select id from sets where short_name = 'mb1'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cathodion'),
    (select id from sets where short_name = 'mb1'),
    '1557',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krenko''s Command'),
    (select id from sets where short_name = 'mb1'),
    '997',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seraph of the Suns'),
    (select id from sets where short_name = 'mb1'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Graypelt Refuge'),
    (select id from sets where short_name = 'mb1'),
    '1674',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tectonic Edge'),
    (select id from sets where short_name = 'mb1'),
    '1690',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alhammarret''s Archive'),
    (select id from sets where short_name = 'mb1'),
    '1543',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ember Weaver'),
    (select id from sets where short_name = 'mb1'),
    '1197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smash to Smithereens'),
    (select id from sets where short_name = 'mb1'),
    '1061',
    'common'
) ,
(
    (select id from mtgcard where name = 'Go for the Throat'),
    (select id from sets where short_name = 'mb1'),
    '670',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Convolute'),
    (select id from sets where short_name = 'mb1'),
    '332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ojutai Interceptor'),
    (select id from sets where short_name = 'mb1'),
    '447',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Arena'),
    (select id from sets where short_name = 'mb1'),
    '730',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flashfreeze'),
    (select id from sets where short_name = 'mb1'),
    '378',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sandstorm Charger'),
    (select id from sets where short_name = 'mb1'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vigean Graftmage'),
    (select id from sets where short_name = 'mb1'),
    '538',
    'common'
) ,
(
    (select id from mtgcard where name = 'Festercreep'),
    (select id from sets where short_name = 'mb1'),
    '658',
    'common'
) ,
(
    (select id from mtgcard where name = 'Revenant'),
    (select id from sets where short_name = 'mb1'),
    '756',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Peace Strider'),
    (select id from sets where short_name = 'mb1'),
    '1617',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sparkmage Apprentice'),
    (select id from sets where short_name = 'mb1'),
    '1063',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weight of the Underworld'),
    (select id from sets where short_name = 'mb1'),
    '823',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blessing of Belzenlok'),
    (select id from sets where short_name = 'mb1'),
    '577',
    'common'
) ,
(
    (select id from mtgcard where name = 'Khalni Heart Expedition'),
    (select id from sets where short_name = 'mb1'),
    '1249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darksteel Garrison'),
    (select id from sets where short_name = 'mb1'),
    '1571',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Energy Field'),
    (select id from sets where short_name = 'mb1'),
    '363',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rhox War Monk'),
    (select id from sets where short_name = 'mb1'),
    '1474',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rage Reflection'),
    (select id from sets where short_name = 'mb1'),
    '1032',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cathar''s Companion'),
    (select id from sets where short_name = 'mb1'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stunt Double'),
    (select id from sets where short_name = 'mb1'),
    '509',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Artificer''s Assistant'),
    (select id from sets where short_name = 'mb1'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aetherflux Reservoir'),
    (select id from sets where short_name = 'mb1'),
    '1539',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lawless Broker'),
    (select id from sets where short_name = 'mb1'),
    '698',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crossroads Consecrator'),
    (select id from sets where short_name = 'mb1'),
    '1172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Skyfisher'),
    (select id from sets where short_name = 'mb1'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alloy Myr'),
    (select id from sets where short_name = 'mb1'),
    '1544',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rancor'),
    (select id from sets where short_name = 'mb1'),
    '1304',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jeering Homunculus'),
    (select id from sets where short_name = 'mb1'),
    '409',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prey Upon'),
    (select id from sets where short_name = 'mb1'),
    '1298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forbidden Alchemy'),
    (select id from sets where short_name = 'mb1'),
    '385',
    'common'
) ,
(
    (select id from mtgcard where name = 'Khenra Scrapper'),
    (select id from sets where short_name = 'mb1'),
    '990',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon''s Eye Sentry'),
    (select id from sets where short_name = 'mb1'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Artist'),
    (select id from sets where short_name = 'mb1'),
    '581',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loyal Sentry'),
    (select id from sets where short_name = 'mb1'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Longshot Squad'),
    (select id from sets where short_name = 'mb1'),
    '1264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lay of the Land'),
    (select id from sets where short_name = 'mb1'),
    '1257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kaervek''s Torch'),
    (select id from sets where short_name = 'mb1'),
    '986',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyrotechnics'),
    (select id from sets where short_name = 'mb1'),
    '1030',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blur of Blades'),
    (select id from sets where short_name = 'mb1'),
    '864',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reclaiming Vines'),
    (select id from sets where short_name = 'mb1'),
    '1308',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jubilant Mascot'),
    (select id from sets where short_name = 'mb1'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stab Wound'),
    (select id from sets where short_name = 'mb1'),
    '780',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urborg Uprising'),
    (select id from sets where short_name = 'mb1'),
    '808',
    'common'
) ,
(
    (select id from mtgcard where name = 'Preyseizer Dragon'),
    (select id from sets where short_name = 'mb1'),
    '1025',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Underworld Coinsmith'),
    (select id from sets where short_name = 'mb1'),
    '1501',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thornhide Wolves'),
    (select id from sets where short_name = 'mb1'),
    '1353',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kin-Tree Warden'),
    (select id from sets where short_name = 'mb1'),
    '1250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faith''s Fetters'),
    (select id from sets where short_name = 'mb1'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blue Elemental Blast'),
    (select id from sets where short_name = 'mb1'),
    '299',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frontline Rebel'),
    (select id from sets where short_name = 'mb1'),
    '940',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tatyova, Benthic Druid'),
    (select id from sets where short_name = 'mb1'),
    '1494',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Charge'),
    (select id from sets where short_name = 'mb1'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mercurial Geists'),
    (select id from sets where short_name = 'mb1'),
    '1454',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crashing Tide'),
    (select id from sets where short_name = 'mb1'),
    '338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thornwood Falls'),
    (select id from sets where short_name = 'mb1'),
    '1692',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merciless Resolve'),
    (select id from sets where short_name = 'mb1'),
    '710',
    'common'
) ,
(
    (select id from mtgcard where name = 'Salivating Gremlins'),
    (select id from sets where short_name = 'mb1'),
    '1049',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elves of Deep Shadow'),
    (select id from sets where short_name = 'mb1'),
    '1193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naya Charm'),
    (select id from sets where short_name = 'mb1'),
    '1458',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Parry'),
    (select id from sets where short_name = 'mb1'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windgrace Acolyte'),
    (select id from sets where short_name = 'mb1'),
    '827',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mortify'),
    (select id from sets where short_name = 'mb1'),
    '1457',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lone Missionary'),
    (select id from sets where short_name = 'mb1'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Atarka Efreet'),
    (select id from sets where short_name = 'mb1'),
    '843',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steamflogger Boss'),
    (select id from sets where short_name = 'mb1'),
    '1069',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Borrowed Hostility'),
    (select id from sets where short_name = 'mb1'),
    '869',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudreader Sphinx'),
    (select id from sets where short_name = 'mb1'),
    '324',
    'common'
) ,
(
    (select id from mtgcard where name = 'River Darter'),
    (select id from sets where short_name = 'mb1'),
    '472',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Shrieker'),
    (select id from sets where short_name = 'mb1'),
    '1003',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seal of Doom'),
    (select id from sets where short_name = 'mb1'),
    '765',
    'common'
) ,
(
    (select id from mtgcard where name = 'Empyrial Armor'),
    (select id from sets where short_name = 'mb1'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Martyr''s Cause'),
    (select id from sets where short_name = 'mb1'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mogg Flunkies'),
    (select id from sets where short_name = 'mb1'),
    '1014',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = 'mb1'),
    '1219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shambling Attendants'),
    (select id from sets where short_name = 'mb1'),
    '769',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drana, Kalastria Bloodchief'),
    (select id from sets where short_name = 'mb1'),
    '633',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ranger''s Guile'),
    (select id from sets where short_name = 'mb1'),
    '1305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lingering Souls'),
    (select id from sets where short_name = 'mb1'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Street Wraith'),
    (select id from sets where short_name = 'mb1'),
    '783',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Renegade Tactics'),
    (select id from sets where short_name = 'mb1'),
    '1040',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cackling Imp'),
    (select id from sets where short_name = 'mb1'),
    '588',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fact or Fiction'),
    (select id from sets where short_name = 'mb1'),
    '371',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selvala, Heart of the Wilds'),
    (select id from sets where short_name = 'mb1'),
    '1330',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Defeat'),
    (select id from sets where short_name = 'mb1'),
    '619',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tragic Slip'),
    (select id from sets where short_name = 'mb1'),
    '799',
    'common'
) ,
(
    (select id from mtgcard where name = 'Adorned Pouncer'),
    (select id from sets where short_name = 'mb1'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blightning'),
    (select id from sets where short_name = 'mb1'),
    '1401',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Distemper of the Blood'),
    (select id from sets where short_name = 'mb1'),
    '906',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eyeblight''s Ending'),
    (select id from sets where short_name = 'mb1'),
    '652',
    'common'
) ,
(
    (select id from mtgcard where name = 'Triton Tactics'),
    (select id from sets where short_name = 'mb1'),
    '534',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Noxious Dragon'),
    (select id from sets where short_name = 'mb1'),
    '725',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jungle Wayfinder'),
    (select id from sets where short_name = 'mb1'),
    '1246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Call the Scions'),
    (select id from sets where short_name = 'mb1'),
    '1153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winding Constrictor'),
    (select id from sets where short_name = 'mb1'),
    '1512',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Time to Feed'),
    (select id from sets where short_name = 'mb1'),
    '1360',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aminatou''s Augury'),
    (select id from sets where short_name = 'mb1'),
    '285',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Altar'),
    (select id from sets where short_name = 'mb1'),
    '1546',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Champion of the Parish'),
    (select id from sets where short_name = 'mb1'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knight of Cliffhaven'),
    (select id from sets where short_name = 'mb1'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trespasser''s Curse'),
    (select id from sets where short_name = 'mb1'),
    '800',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chasm Skulker'),
    (select id from sets where short_name = 'mb1'),
    '313',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sarkhan''s Rage'),
    (select id from sets where short_name = 'mb1'),
    '1051',
    'common'
) ,
(
    (select id from mtgcard where name = 'Night''s Whisper'),
    (select id from sets where short_name = 'mb1'),
    '723',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silvergill Adept'),
    (select id from sets where short_name = 'mb1'),
    '490',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunset Pyramid'),
    (select id from sets where short_name = 'mb1'),
    '1636',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Caustic Tar'),
    (select id from sets where short_name = 'mb1'),
    '596',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Elder'),
    (select id from sets where short_name = 'mb1'),
    '1322',
    'common'
) ,
(
    (select id from mtgcard where name = 'Distortion Strike'),
    (select id from sets where short_name = 'mb1'),
    '352',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Draconic Disciple'),
    (select id from sets where short_name = 'mb1'),
    '1416',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Caged Sun'),
    (select id from sets where short_name = 'mb1'),
    '1556',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jushi Apprentice // Tomoya the Revealer'),
    (select id from sets where short_name = 'mb1'),
    '411',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Farbog Revenant'),
    (select id from sets where short_name = 'mb1'),
    '654',
    'common'
) ,
(
    (select id from mtgcard where name = 'Engineered Might'),
    (select id from sets where short_name = 'mb1'),
    '1420',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Executioner''s Capsule'),
    (select id from sets where short_name = 'mb1'),
    '650',
    'common'
) ,
(
    (select id from mtgcard where name = 'River Serpent'),
    (select id from sets where short_name = 'mb1'),
    '473',
    'common'
) ,
(
    (select id from mtgcard where name = 'Send to Sleep'),
    (select id from sets where short_name = 'mb1'),
    '483',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grasp of Fate'),
    (select id from sets where short_name = 'mb1'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hijack'),
    (select id from sets where short_name = 'mb1'),
    '974',
    'common'
) ,
(
    (select id from mtgcard where name = 'Champion of Arashin'),
    (select id from sets where short_name = 'mb1'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mulldrifter'),
    (select id from sets where short_name = 'mb1'),
    '437',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divination'),
    (select id from sets where short_name = 'mb1'),
    '353',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hexplate Golem'),
    (select id from sets where short_name = 'mb1'),
    '1594',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ethercaste Knight'),
    (select id from sets where short_name = 'mb1'),
    '1422',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tidy Conclusion'),
    (select id from sets where short_name = 'mb1'),
    '794',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crosis''s Charm'),
    (select id from sets where short_name = 'mb1'),
    '1412',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pollenbright Wings'),
    (select id from sets where short_name = 'mb1'),
    '1465',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Bounty'),
    (select id from sets where short_name = 'mb1'),
    '1344',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace''s Phantasm'),
    (select id from sets where short_name = 'mb1'),
    '408',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wren''s Run Vanquisher'),
    (select id from sets where short_name = 'mb1'),
    '1378',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Carrion Imp'),
    (select id from sets where short_name = 'mb1'),
    '593',
    'common'
) ,
(
    (select id from mtgcard where name = 'Become Immense'),
    (select id from sets where short_name = 'mb1'),
    '1135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bombard'),
    (select id from sets where short_name = 'mb1'),
    '867',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Tradewinds'),
    (select id from sets where short_name = 'mb1'),
    '283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barrage of Boulders'),
    (select id from sets where short_name = 'mb1'),
    '849',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gut Shot'),
    (select id from sets where short_name = 'mb1'),
    '969',
    'common'
) ,
(
    (select id from mtgcard where name = 'Healing Hands'),
    (select id from sets where short_name = 'mb1'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charging Monstrosaur'),
    (select id from sets where short_name = 'mb1'),
    '886',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chart a Course'),
    (select id from sets where short_name = 'mb1'),
    '312',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blistergrub'),
    (select id from sets where short_name = 'mb1'),
    '580',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tidal Warrior'),
    (select id from sets where short_name = 'mb1'),
    '525',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gelectrode'),
    (select id from sets where short_name = 'mb1'),
    '1428',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stave Off'),
    (select id from sets where short_name = 'mb1'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Failed Inspection'),
    (select id from sets where short_name = 'mb1'),
    '374',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudkin Seer'),
    (select id from sets where short_name = 'mb1'),
    '323',
    'common'
) ,
(
    (select id from mtgcard where name = 'Priest of Titania'),
    (select id from sets where short_name = 'mb1'),
    '1299',
    'common'
) ,
(
    (select id from mtgcard where name = 'War Behemoth'),
    (select id from sets where short_name = 'mb1'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hot Soup'),
    (select id from sets where short_name = 'mb1'),
    '1595',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hound of the Farbogs'),
    (select id from sets where short_name = 'mb1'),
    '687',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bala Ged Scorpion'),
    (select id from sets where short_name = 'mb1'),
    '568',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inferno Jet'),
    (select id from sets where short_name = 'mb1'),
    '982',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darksteel Mutation'),
    (select id from sets where short_name = 'mb1'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gods Willing'),
    (select id from sets where short_name = 'mb1'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sword of the Animist'),
    (select id from sets where short_name = 'mb1'),
    '1638',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Crypt'),
    (select id from sets where short_name = 'mb1'),
    '1603',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Loxodon Warhammer'),
    (select id from sets where short_name = 'mb1'),
    '1602',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reckless Spite'),
    (select id from sets where short_name = 'mb1'),
    '750',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Mask'),
    (select id from sets where short_name = 'mb1'),
    '1575',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Centaur Glade'),
    (select id from sets where short_name = 'mb1'),
    '1159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Omens'),
    (select id from sets where short_name = 'mb1'),
    '270',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Challenger'),
    (select id from sets where short_name = 'mb1'),
    '1403',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Miasmic Mummy'),
    (select id from sets where short_name = 'mb1'),
    '711',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bring Low'),
    (select id from sets where short_name = 'mb1'),
    '875',
    'common'
) ,
(
    (select id from mtgcard where name = 'Misdirection'),
    (select id from sets where short_name = 'mb1'),
    '432',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Defiant Ogre'),
    (select id from sets where short_name = 'mb1'),
    '900',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iona''s Judgment'),
    (select id from sets where short_name = 'mb1'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heavy Arbalest'),
    (select id from sets where short_name = 'mb1'),
    '1591',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gone Missing'),
    (select id from sets where short_name = 'mb1'),
    '393',
    'common'
) ,
(
    (select id from mtgcard where name = 'Genju of the Fens'),
    (select id from sets where short_name = 'mb1'),
    '666',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ainok Tracker'),
    (select id from sets where short_name = 'mb1'),
    '834',
    'common'
) ,
(
    (select id from mtgcard where name = 'Breaker of Armies'),
    (select id from sets where short_name = 'mb1'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gurmag Angler'),
    (select id from sets where short_name = 'mb1'),
    '683',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pathrazer of Ulamog'),
    (select id from sets where short_name = 'mb1'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Induce Despair'),
    (select id from sets where short_name = 'mb1'),
    '690',
    'common'
) ,
(
    (select id from mtgcard where name = 'Release the Ants'),
    (select id from sets where short_name = 'mb1'),
    '1038',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lure'),
    (select id from sets where short_name = 'mb1'),
    '1265',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barging Sergeant'),
    (select id from sets where short_name = 'mb1'),
    '848',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kraul Foragers'),
    (select id from sets where short_name = 'mb1'),
    '1252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akroan Horse'),
    (select id from sets where short_name = 'mb1'),
    '1541',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balustrade Spy'),
    (select id from sets where short_name = 'mb1'),
    '570',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Spectacle'),
    (select id from sets where short_name = 'mb1'),
    '949',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abzan Runemark'),
    (select id from sets where short_name = 'mb1'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ezuri''s Archers'),
    (select id from sets where short_name = 'mb1'),
    '1204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shining Aerosaur'),
    (select id from sets where short_name = 'mb1'),
    '227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Youthful Scholar'),
    (select id from sets where short_name = 'mb1'),
    '557',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Greater Sandwurm'),
    (select id from sets where short_name = 'mb1'),
    '1227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Juggernaut'),
    (select id from sets where short_name = 'mb1'),
    '1599',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gray Merchant of Asphodel'),
    (select id from sets where short_name = 'mb1'),
    '677',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fog'),
    (select id from sets where short_name = 'mb1'),
    '1213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dirge of Dread'),
    (select id from sets where short_name = 'mb1'),
    '628',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thran Dynamo'),
    (select id from sets where short_name = 'mb1'),
    '1640',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Motivator'),
    (select id from sets where short_name = 'mb1'),
    '957',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terrain Elemental'),
    (select id from sets where short_name = 'mb1'),
    '1351',
    'common'
) ,
(
    (select id from mtgcard where name = 'Affa Protector'),
    (select id from sets where short_name = 'mb1'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mardu Roughrider'),
    (select id from sets where short_name = 'mb1'),
    '1450',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Breeding Pit'),
    (select id from sets where short_name = 'mb1'),
    '585',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wandering Champion'),
    (select id from sets where short_name = 'mb1'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woolly Loxodon'),
    (select id from sets where short_name = 'mb1'),
    '1377',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wee Dragonauts'),
    (select id from sets where short_name = 'mb1'),
    '1511',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Perilous Myr'),
    (select id from sets where short_name = 'mb1'),
    '1618',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dismal Backwater'),
    (select id from sets where short_name = 'mb1'),
    '1663',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reliquary Tower'),
    (select id from sets where short_name = 'mb1'),
    '1683',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skeleton Archer'),
    (select id from sets where short_name = 'mb1'),
    '775',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moonlit Strider'),
    (select id from sets where short_name = 'mb1'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Decommission'),
    (select id from sets where short_name = 'mb1'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thoughtcast'),
    (select id from sets where short_name = 'mb1'),
    '520',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evincar''s Justice'),
    (select id from sets where short_name = 'mb1'),
    '649',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slither Blade'),
    (select id from sets where short_name = 'mb1'),
    '497',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Scrying'),
    (select id from sets where short_name = 'mb1'),
    '1345',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Roar of the Wurm'),
    (select id from sets where short_name = 'mb1'),
    '1316',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coralhelm Guide'),
    (select id from sets where short_name = 'mb1'),
    '333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abzan Falconer'),
    (select id from sets where short_name = 'mb1'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Molten Rain'),
    (select id from sets where short_name = 'mb1'),
    '1016',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Annihilate'),
    (select id from sets where short_name = 'mb1'),
    '567',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Direct Current'),
    (select id from sets where short_name = 'mb1'),
    '905',
    'common'
) ,
(
    (select id from mtgcard where name = 'Take Down'),
    (select id from sets where short_name = 'mb1'),
    '1348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Retraction Helix'),
    (select id from sets where short_name = 'mb1'),
    '467',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodmad Vampire'),
    (select id from sets where short_name = 'mb1'),
    '860',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crushing Canopy'),
    (select id from sets where short_name = 'mb1'),
    '1175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Release the Gremlins'),
    (select id from sets where short_name = 'mb1'),
    '1039',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infantry Veteran'),
    (select id from sets where short_name = 'mb1'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swiftwater Cliffs'),
    (select id from sets where short_name = 'mb1'),
    '1689',
    'common'
) ,
(
    (select id from mtgcard where name = 'Excoriate'),
    (select id from sets where short_name = 'mb1'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Battle Priest'),
    (select id from sets where short_name = 'mb1'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krenko''s Enforcer'),
    (select id from sets where short_name = 'mb1'),
    '998',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snap'),
    (select id from sets where short_name = 'mb1'),
    '498',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bear Cub'),
    (select id from sets where short_name = 'mb1'),
    '1131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zada''s Commando'),
    (select id from sets where short_name = 'mb1'),
    '1106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of Old Benalia'),
    (select id from sets where short_name = 'mb1'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Village Bell-Ringer'),
    (select id from sets where short_name = 'mb1'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Walk the Plank'),
    (select id from sets where short_name = 'mb1'),
    '820',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampire Envoy'),
    (select id from sets where short_name = 'mb1'),
    '810',
    'common'
) ,
(
    (select id from mtgcard where name = 'Floodgate'),
    (select id from sets where short_name = 'mb1'),
    '381',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thopter Foundry'),
    (select id from sets where short_name = 'mb1'),
    '1535',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Groundswell'),
    (select id from sets where short_name = 'mb1'),
    '1230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx''s Tutelage'),
    (select id from sets where short_name = 'mb1'),
    '502',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Roast'),
    (select id from sets where short_name = 'mb1'),
    '1042',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra''s Embrace'),
    (select id from sets where short_name = 'mb1'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nissa, Voice of Zendikar'),
    (select id from sets where short_name = 'mb1'),
    '1281',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'mb1'),
    '1665',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elephant Guide'),
    (select id from sets where short_name = 'mb1'),
    '1192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunt the Weak'),
    (select id from sets where short_name = 'mb1'),
    '1240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Retreat to Emeria'),
    (select id from sets where short_name = 'mb1'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seismic Shift'),
    (select id from sets where short_name = 'mb1'),
    '1053',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meandering Towershell'),
    (select id from sets where short_name = 'mb1'),
    '1269',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodbraid Elf'),
    (select id from sets where short_name = 'mb1'),
    '1402',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dolmen Gate'),
    (select id from sets where short_name = 'mb1'),
    '1573',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blanchwood Armor'),
    (select id from sets where short_name = 'mb1'),
    '1141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fallen Angel'),
    (select id from sets where short_name = 'mb1'),
    '653',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gideon Jura'),
    (select id from sets where short_name = 'mb1'),
    '118',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Desolation Twin'),
    (select id from sets where short_name = 'mb1'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eddytrail Hawk'),
    (select id from sets where short_name = 'mb1'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stalking Tiger'),
    (select id from sets where short_name = 'mb1'),
    '1341',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skarrg, the Rage Pits'),
    (select id from sets where short_name = 'mb1'),
    '1688',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deny Reality'),
    (select id from sets where short_name = 'mb1'),
    '1415',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skittering Crustacean'),
    (select id from sets where short_name = 'mb1'),
    '494',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crib Swap'),
    (select id from sets where short_name = 'mb1'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dune Beetle'),
    (select id from sets where short_name = 'mb1'),
    '641',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inspired Charge'),
    (select id from sets where short_name = 'mb1'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viashino Sandstalker'),
    (select id from sets where short_name = 'mb1'),
    '1096',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sprouting Thrinax'),
    (select id from sets where short_name = 'mb1'),
    '1489',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stalwart Aven'),
    (select id from sets where short_name = 'mb1'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rummaging Goblin'),
    (select id from sets where short_name = 'mb1'),
    '1046',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Rager'),
    (select id from sets where short_name = 'mb1'),
    '732',
    'common'
) ,
(
    (select id from mtgcard where name = 'Impending Disaster'),
    (select id from sets where short_name = 'mb1'),
    '979',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Doorkeeper'),
    (select id from sets where short_name = 'mb1'),
    '355',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coral Trickster'),
    (select id from sets where short_name = 'mb1'),
    '334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vengeful Rebirth'),
    (select id from sets where short_name = 'mb1'),
    '1505',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kor Hookmaster'),
    (select id from sets where short_name = 'mb1'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Incorrigible Youths'),
    (select id from sets where short_name = 'mb1'),
    '980',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Short Sword'),
    (select id from sets where short_name = 'mb1'),
    '1627',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meditation Puzzle'),
    (select id from sets where short_name = 'mb1'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mantle of Webs'),
    (select id from sets where short_name = 'mb1'),
    '1267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kiki-Jiki, Mirror Breaker'),
    (select id from sets where short_name = 'mb1'),
    '991',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shambling Remains'),
    (select id from sets where short_name = 'mb1'),
    '1483',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feral Prowler'),
    (select id from sets where short_name = 'mb1'),
    '1209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tandem Lookout'),
    (select id from sets where short_name = 'mb1'),
    '514',
    'common'
) ,
(
    (select id from mtgcard where name = 'Contradict'),
    (select id from sets where short_name = 'mb1'),
    '331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Athreos, God of Passage'),
    (select id from sets where short_name = 'mb1'),
    '1391',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Purple-Crystal Crab'),
    (select id from sets where short_name = 'mb1'),
    '464',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sigil of Valor'),
    (select id from sets where short_name = 'mb1'),
    '1628',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sanctum Gargoyle'),
    (select id from sets where short_name = 'mb1'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nemesis of Reason'),
    (select id from sets where short_name = 'mb1'),
    '1459',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corpsejack Menace'),
    (select id from sets where short_name = 'mb1'),
    '1411',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eternal Thirst'),
    (select id from sets where short_name = 'mb1'),
    '648',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Bladewhirl'),
    (select id from sets where short_name = 'mb1'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Tithe'),
    (select id from sets where short_name = 'mb1'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Syr Elenora, the Discerning'),
    (select id from sets where short_name = 'mb1'),
    '512',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doomed Traveler'),
    (select id from sets where short_name = 'mb1'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Driver of the Dead'),
    (select id from sets where short_name = 'mb1'),
    '638',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Shrine'),
    (select id from sets where short_name = 'mb1'),
    '1677',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battle Mastery'),
    (select id from sets where short_name = 'mb1'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marsh Hulk'),
    (select id from sets where short_name = 'mb1'),
    '708',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brine Elemental'),
    (select id from sets where short_name = 'mb1'),
    '303',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Rage'),
    (select id from sets where short_name = 'mb1'),
    '1089',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Miner''s Bane'),
    (select id from sets where short_name = 'mb1'),
    '1011',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'mb1'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Commune with the Gods'),
    (select id from sets where short_name = 'mb1'),
    '1167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bottle Gnomes'),
    (select id from sets where short_name = 'mb1'),
    '1554',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Archangel'),
    (select id from sets where short_name = 'mb1'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wargate'),
    (select id from sets where short_name = 'mb1'),
    '1508',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trading Post'),
    (select id from sets where short_name = 'mb1'),
    '1644',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Impact Tremors'),
    (select id from sets where short_name = 'mb1'),
    '978',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure Cruise'),
    (select id from sets where short_name = 'mb1'),
    '530',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Mercy'),
    (select id from sets where short_name = 'mb1'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murderous Compulsion'),
    (select id from sets where short_name = 'mb1'),
    '718',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pitfall Trap'),
    (select id from sets where short_name = 'mb1'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Two-Headed Giant'),
    (select id from sets where short_name = 'mb1'),
    '1086',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Recover'),
    (select id from sets where short_name = 'mb1'),
    '751',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desperate Sentry'),
    (select id from sets where short_name = 'mb1'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Byway Courier'),
    (select id from sets where short_name = 'mb1'),
    '1152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lawmage''s Binding'),
    (select id from sets where short_name = 'mb1'),
    '1447',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin War Paint'),
    (select id from sets where short_name = 'mb1'),
    '962',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abundant Growth'),
    (select id from sets where short_name = 'mb1'),
    '1108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fury Charm'),
    (select id from sets where short_name = 'mb1'),
    '942',
    'common'
) ,
(
    (select id from mtgcard where name = 'Djeru''s Renunciation'),
    (select id from sets where short_name = 'mb1'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pillory of the Sleepless'),
    (select id from sets where short_name = 'mb1'),
    '1463',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Windborne Charge'),
    (select id from sets where short_name = 'mb1'),
    '276',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Gitrog Monster'),
    (select id from sets where short_name = 'mb1'),
    '1430',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Warden of the Eye'),
    (select id from sets where short_name = 'mb1'),
    '1507',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Sapherd'),
    (select id from sets where short_name = 'mb1'),
    '1380',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fires of Yavimaya'),
    (select id from sets where short_name = 'mb1'),
    '1425',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Borderland Explorer'),
    (select id from sets where short_name = 'mb1'),
    '1145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raff Capashen, Ship''s Mage'),
    (select id from sets where short_name = 'mb1'),
    '1470',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coat of Arms'),
    (select id from sets where short_name = 'mb1'),
    '1561',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pinion Feast'),
    (select id from sets where short_name = 'mb1'),
    '1294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loxodon Partisan'),
    (select id from sets where short_name = 'mb1'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Barrier'),
    (select id from sets where short_name = 'mb1'),
    '1440',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desperate Castaways'),
    (select id from sets where short_name = 'mb1'),
    '623',
    'common'
) ,
(
    (select id from mtgcard where name = 'Festering Newt'),
    (select id from sets where short_name = 'mb1'),
    '659',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mortal''s Ardor'),
    (select id from sets where short_name = 'mb1'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crenellated Wall'),
    (select id from sets where short_name = 'mb1'),
    '1567',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oracle of Nectars'),
    (select id from sets where short_name = 'mb1'),
    '1529',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bomat Bazaar Barge'),
    (select id from sets where short_name = 'mb1'),
    '1550',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fleeting Distraction'),
    (select id from sets where short_name = 'mb1'),
    '380',
    'common'
) ,
(
    (select id from mtgcard where name = 'Titanic Growth'),
    (select id from sets where short_name = 'mb1'),
    '1362',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lazotep Behemoth'),
    (select id from sets where short_name = 'mb1'),
    '699',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coveted Jewel'),
    (select id from sets where short_name = 'mb1'),
    '1566',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shipwreck Singer'),
    (select id from sets where short_name = 'mb1'),
    '1485',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kraul Warrior'),
    (select id from sets where short_name = 'mb1'),
    '1253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Chant'),
    (select id from sets where short_name = 'mb1'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Uncaged Fury'),
    (select id from sets where short_name = 'mb1'),
    '1087',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nature''s Lore'),
    (select id from sets where short_name = 'mb1'),
    '1276',
    'common'
) ,
(
    (select id from mtgcard where name = 'Perish'),
    (select id from sets where short_name = 'mb1'),
    '728',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Containment Membrane'),
    (select id from sets where short_name = 'mb1'),
    '329',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Pyrohelix'),
    (select id from sets where short_name = 'mb1'),
    '883',
    'common'
) ,
(
    (select id from mtgcard where name = 'Caught in the Brights'),
    (select id from sets where short_name = 'mb1'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unlicensed Disintegration'),
    (select id from sets where short_name = 'mb1'),
    '1503',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nimble Mongoose'),
    (select id from sets where short_name = 'mb1'),
    '1280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Welkin Tern'),
    (select id from sets where short_name = 'mb1'),
    '544',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Firewalker'),
    (select id from sets where short_name = 'mb1'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Migratory Route'),
    (select id from sets where short_name = 'mb1'),
    '1456',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spire Monitor'),
    (select id from sets where short_name = 'mb1'),
    '503',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insolent Neonate'),
    (select id from sets where short_name = 'mb1'),
    '984',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Sire'),
    (select id from sets where short_name = 'mb1'),
    '1614',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temporal Fissure'),
    (select id from sets where short_name = 'mb1'),
    '516',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brazen Buccaneers'),
    (select id from sets where short_name = 'mb1'),
    '871',
    'common'
) ,
(
    (select id from mtgcard where name = 'Herald''s Horn'),
    (select id from sets where short_name = 'mb1'),
    '1593',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avarax'),
    (select id from sets where short_name = 'mb1'),
    '845',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coiling Oracle'),
    (select id from sets where short_name = 'mb1'),
    '1409',
    'common'
) ,
(
    (select id from mtgcard where name = 'Queen Marchesa'),
    (select id from sets where short_name = 'mb1'),
    '1468',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gateway Plaza'),
    (select id from sets where short_name = 'mb1'),
    '1670',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warteye Witch'),
    (select id from sets where short_name = 'mb1'),
    '822',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crop Rotation'),
    (select id from sets where short_name = 'mb1'),
    '1171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Benthic Infiltrator'),
    (select id from sets where short_name = 'mb1'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Destructive Tampering'),
    (select id from sets where short_name = 'mb1'),
    '904',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blightsoil Druid'),
    (select id from sets where short_name = 'mb1'),
    '579',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragonsoul Knight'),
    (select id from sets where short_name = 'mb1'),
    '910',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aura of Silence'),
    (select id from sets where short_name = 'mb1'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bartered Cow'),
    (select id from sets where short_name = 'mb1'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lunarch Mantle'),
    (select id from sets where short_name = 'mb1'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fen Hauler'),
    (select id from sets where short_name = 'mb1'),
    '656',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fall of the Hammer'),
    (select id from sets where short_name = 'mb1'),
    '921',
    'common'
) ,
(
    (select id from mtgcard where name = 'Roots'),
    (select id from sets where short_name = 'mb1'),
    '1318',
    'common'
) ,
(
    (select id from mtgcard where name = 'Everdream'),
    (select id from sets where short_name = 'mb1'),
    '368',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Riptide Crab'),
    (select id from sets where short_name = 'mb1'),
    '1475',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodstone Goblin'),
    (select id from sets where short_name = 'mb1'),
    '862',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oakgnarl Warrior'),
    (select id from sets where short_name = 'mb1'),
    '1282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Gift'),
    (select id from sets where short_name = 'mb1'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soothsaying'),
    (select id from sets where short_name = 'mb1'),
    '501',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sandsteppe Citadel'),
    (select id from sets where short_name = 'mb1'),
    '1685',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Compelling Argument'),
    (select id from sets where short_name = 'mb1'),
    '326',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Hollow'),
    (select id from sets where short_name = 'mb1'),
    '1676',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gift of Estates'),
    (select id from sets where short_name = 'mb1'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hooded Brawler'),
    (select id from sets where short_name = 'mb1'),
    '1236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bone Saw'),
    (select id from sets where short_name = 'mb1'),
    '1551',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Matron'),
    (select id from sets where short_name = 'mb1'),
    '956',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hardy Veteran'),
    (select id from sets where short_name = 'mb1'),
    '1233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inferno Fist'),
    (select id from sets where short_name = 'mb1'),
    '981',
    'common'
) ,
(
    (select id from mtgcard where name = 'Felidar Guardian'),
    (select id from sets where short_name = 'mb1'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'mb1'),
    '926',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Predict'),
    (select id from sets where short_name = 'mb1'),
    '459',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Regrowth'),
    (select id from sets where short_name = 'mb1'),
    '1309',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leapfrog'),
    (select id from sets where short_name = 'mb1'),
    '418',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sandstone Oracle'),
    (select id from sets where short_name = 'mb1'),
    '1625',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brilliant Spectrum'),
    (select id from sets where short_name = 'mb1'),
    '302',
    'common'
) ,
(
    (select id from mtgcard where name = 'Metallic Rebuke'),
    (select id from sets where short_name = 'mb1'),
    '428',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hypothesizzle'),
    (select id from sets where short_name = 'mb1'),
    '1437',
    'common'
) ,
(
    (select id from mtgcard where name = 'Siege Wurm'),
    (select id from sets where short_name = 'mb1'),
    '1333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Omenspeaker'),
    (select id from sets where short_name = 'mb1'),
    '449',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conifer Strider'),
    (select id from sets where short_name = 'mb1'),
    '1168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greenbelt Rampager'),
    (select id from sets where short_name = 'mb1'),
    '1228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grasping Scoundrel'),
    (select id from sets where short_name = 'mb1'),
    '672',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alpine Grizzly'),
    (select id from sets where short_name = 'mb1'),
    '1116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jackal Pup'),
    (select id from sets where short_name = 'mb1'),
    '985',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakdos Drake'),
    (select id from sets where short_name = 'mb1'),
    '743',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fathom Seer'),
    (select id from sets where short_name = 'mb1'),
    '376',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unyielding Krumar'),
    (select id from sets where short_name = 'mb1'),
    '807',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Fireweaver'),
    (select id from sets where short_name = 'mb1'),
    '1035',
    'common'
) ,
(
    (select id from mtgcard where name = 'Searing Light'),
    (select id from sets where short_name = 'mb1'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exclude'),
    (select id from sets where short_name = 'mb1'),
    '369',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Great Furnace'),
    (select id from sets where short_name = 'mb1'),
    '1675',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Ogre'),
    (select id from sets where short_name = 'mb1'),
    '861',
    'common'
) ,
(
    (select id from mtgcard where name = 'Invisibility'),
    (select id from sets where short_name = 'mb1'),
    '406',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whelming Wave'),
    (select id from sets where short_name = 'mb1'),
    '545',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silkweaver Elite'),
    (select id from sets where short_name = 'mb1'),
    '1335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darksteel Citadel'),
    (select id from sets where short_name = 'mb1'),
    '1662',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warden of Evos Isle'),
    (select id from sets where short_name = 'mb1'),
    '540',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skulking Ghost'),
    (select id from sets where short_name = 'mb1'),
    '776',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swift Kick'),
    (select id from sets where short_name = 'mb1'),
    '1077',
    'common'
) ,
(
    (select id from mtgcard where name = 'Act on Impulse'),
    (select id from sets where short_name = 'mb1'),
    '832',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reassembling Skeleton'),
    (select id from sets where short_name = 'mb1'),
    '748',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Amass the Components'),
    (select id from sets where short_name = 'mb1'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baloth Null'),
    (select id from sets where short_name = 'mb1'),
    '1396',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gravitic Punch'),
    (select id from sets where short_name = 'mb1'),
    '967',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lethal Sting'),
    (select id from sets where short_name = 'mb1'),
    '700',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rogue''s Passage'),
    (select id from sets where short_name = 'mb1'),
    '1684',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bonesplitter'),
    (select id from sets where short_name = 'mb1'),
    '1552',
    'common'
) ,
(
    (select id from mtgcard where name = 'Impulse'),
    (select id from sets where short_name = 'mb1'),
    '404',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana, Death''s Majesty'),
    (select id from sets where short_name = 'mb1'),
    '701',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Enduring Victory'),
    (select id from sets where short_name = 'mb1'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyspear Cavalry'),
    (select id from sets where short_name = 'mb1'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Humble'),
    (select id from sets where short_name = 'mb1'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Plaguelord'),
    (select id from sets where short_name = 'mb1'),
    '731',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dazzling Lights'),
    (select id from sets where short_name = 'mb1'),
    '343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glint-Sleeve Artisan'),
    (select id from sets where short_name = 'mb1'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Denied'),
    (select id from sets where short_name = 'mb1'),
    '618',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shrewd Hatchling'),
    (select id from sets where short_name = 'mb1'),
    '1533',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Emmessi Tome'),
    (select id from sets where short_name = 'mb1'),
    '1579',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Monastery Swiftspear'),
    (select id from sets where short_name = 'mb1'),
    '1017',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Preordain'),
    (select id from sets where short_name = 'mb1'),
    '460',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dirgur Nemesis'),
    (select id from sets where short_name = 'mb1'),
    '349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sagu Archer'),
    (select id from sets where short_name = 'mb1'),
    '1321',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk Looter'),
    (select id from sets where short_name = 'mb1'),
    '426',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krosan Druid'),
    (select id from sets where short_name = 'mb1'),
    '1254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mark of Mutiny'),
    (select id from sets where short_name = 'mb1'),
    '1009',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swashbuckling'),
    (select id from sets where short_name = 'mb1'),
    '1075',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wretched Gryff'),
    (select id from sets where short_name = 'mb1'),
    '555',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foil'),
    (select id from sets where short_name = 'mb1'),
    '384',
    'common'
) ,
(
    (select id from mtgcard where name = 'Staggershock'),
    (select id from sets where short_name = 'mb1'),
    '1067',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mist Raven'),
    (select id from sets where short_name = 'mb1'),
    '434',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Devastator'),
    (select id from sets where short_name = 'mb1'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hurricane'),
    (select id from sets where short_name = 'mb1'),
    '1241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Canopy Spider'),
    (select id from sets where short_name = 'mb1'),
    '1154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravecrawler'),
    (select id from sets where short_name = 'mb1'),
    '673',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quakefoot Cyclops'),
    (select id from sets where short_name = 'mb1'),
    '1031',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flayer Husk'),
    (select id from sets where short_name = 'mb1'),
    '1583',
    'common'
) ,
(
    (select id from mtgcard where name = 'Promise of Bunrei'),
    (select id from sets where short_name = 'mb1'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fencing Ace'),
    (select id from sets where short_name = 'mb1'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voice of the Provinces'),
    (select id from sets where short_name = 'mb1'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Madcap Skills'),
    (select id from sets where short_name = 'mb1'),
    '1005',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mogg Fanatic'),
    (select id from sets where short_name = 'mb1'),
    '1013',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bristling Boar'),
    (select id from sets where short_name = 'mb1'),
    '1149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudshift'),
    (select id from sets where short_name = 'mb1'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hyena Umbra'),
    (select id from sets where short_name = 'mb1'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stinkweed Imp'),
    (select id from sets where short_name = 'mb1'),
    '782',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chillbringer'),
    (select id from sets where short_name = 'mb1'),
    '314',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wave-Wing Elemental'),
    (select id from sets where short_name = 'mb1'),
    '542',
    'common'
) ,
(
    (select id from mtgcard where name = 'Revive'),
    (select id from sets where short_name = 'mb1'),
    '1312',
    'common'
) ,
(
    (select id from mtgcard where name = 'Turn Aside'),
    (select id from sets where short_name = 'mb1'),
    '535',
    'common'
) ,
(
    (select id from mtgcard where name = 'Master Transmuter'),
    (select id from sets where short_name = 'mb1'),
    '422',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blades of Velis Vel'),
    (select id from sets where short_name = 'mb1'),
    '854',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daring Demolition'),
    (select id from sets where short_name = 'mb1'),
    '608',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elixir of Immortality'),
    (select id from sets where short_name = 'mb1'),
    '1578',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Youthful Knight'),
    (select id from sets where short_name = 'mb1'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erg Raiders'),
    (select id from sets where short_name = 'mb1'),
    '647',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrashing Brontodon'),
    (select id from sets where short_name = 'mb1'),
    '1356',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Uncomfortable Chill'),
    (select id from sets where short_name = 'mb1'),
    '536',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ferocious Zheng'),
    (select id from sets where short_name = 'mb1'),
    '1210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blow Your House Down'),
    (select id from sets where short_name = 'mb1'),
    '863',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dukhara Scavenger'),
    (select id from sets where short_name = 'mb1'),
    '640',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skirk Commando'),
    (select id from sets where short_name = 'mb1'),
    '1059',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frenzied Raptor'),
    (select id from sets where short_name = 'mb1'),
    '937',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Fire'),
    (select id from sets where short_name = 'mb1'),
    '1100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sheer Drop'),
    (select id from sets where short_name = 'mb1'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Cat'),
    (select id from sets where short_name = 'mb1'),
    '573',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ensoul Artifact'),
    (select id from sets where short_name = 'mb1'),
    '365',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Claustrophobia'),
    (select id from sets where short_name = 'mb1'),
    '319',
    'common'
) ,
(
    (select id from mtgcard where name = 'Assemble the Legion'),
    (select id from sets where short_name = 'mb1'),
    '1390',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stromkirk Patrol'),
    (select id from sets where short_name = 'mb1'),
    '784',
    'common'
) ,
(
    (select id from mtgcard where name = 'God-Pharaoh''s Faithful'),
    (select id from sets where short_name = 'mb1'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ior Ruin Expedition'),
    (select id from sets where short_name = 'mb1'),
    '407',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hornet Nest'),
    (select id from sets where short_name = 'mb1'),
    '1238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Toxin Sliver'),
    (select id from sets where short_name = 'mb1'),
    '798',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ethereal Ambush'),
    (select id from sets where short_name = 'mb1'),
    '1423',
    'common'
) ,
(
    (select id from mtgcard where name = 'Caustic Caterpillar'),
    (select id from sets where short_name = 'mb1'),
    '1157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cancel'),
    (select id from sets where short_name = 'mb1'),
    '307',
    'common'
) ,
(
    (select id from mtgcard where name = 'Celestial Crusader'),
    (select id from sets where short_name = 'mb1'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tormod''s Crypt'),
    (select id from sets where short_name = 'mb1'),
    '1642',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Castaway''s Despair'),
    (select id from sets where short_name = 'mb1'),
    '310',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kird Ape'),
    (select id from sets where short_name = 'mb1'),
    '993',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unwavering Initiate'),
    (select id from sets where short_name = 'mb1'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stitched Drake'),
    (select id from sets where short_name = 'mb1'),
    '505',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Spellbomb'),
    (select id from sets where short_name = 'mb1'),
    '1540',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ephemeral Shields'),
    (select id from sets where short_name = 'mb1'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crowd''s Favor'),
    (select id from sets where short_name = 'mb1'),
    '894',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plaxcaster Frogling'),
    (select id from sets where short_name = 'mb1'),
    '1464',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fiend Hunter'),
    (select id from sets where short_name = 'mb1'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Consulate Dreadnought'),
    (select id from sets where short_name = 'mb1'),
    '1563',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight of Dawn'),
    (select id from sets where short_name = 'mb1'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Icy Manipulator'),
    (select id from sets where short_name = 'mb1'),
    '1596',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rosethorn Halberd'),
    (select id from sets where short_name = 'mb1'),
    '1319',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trepanation Blade'),
    (select id from sets where short_name = 'mb1'),
    '1645',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plummet'),
    (select id from sets where short_name = 'mb1'),
    '1295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Talons'),
    (select id from sets where short_name = 'mb1'),
    '1004',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Ingester'),
    (select id from sets where short_name = 'mb1'),
    '455',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alley Strangler'),
    (select id from sets where short_name = 'mb1'),
    '562',
    'common'
) ,
(
    (select id from mtgcard where name = 'Living Death'),
    (select id from sets where short_name = 'mb1'),
    '702',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oust'),
    (select id from sets where short_name = 'mb1'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savage Knuckleblade'),
    (select id from sets where short_name = 'mb1'),
    '1480',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sweatworks Brawler'),
    (select id from sets where short_name = 'mb1'),
    '1076',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seeker of the Way'),
    (select id from sets where short_name = 'mb1'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pegasus Courser'),
    (select id from sets where short_name = 'mb1'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mark of the Vampire'),
    (select id from sets where short_name = 'mb1'),
    '707',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raptor Companion'),
    (select id from sets where short_name = 'mb1'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Read the Bones'),
    (select id from sets where short_name = 'mb1'),
    '746',
    'common'
) ,
(
    (select id from mtgcard where name = 'Odric, Lunarch Marshal'),
    (select id from sets where short_name = 'mb1'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Skirmisher'),
    (select id from sets where short_name = 'mb1'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guardians of Meletis'),
    (select id from sets where short_name = 'mb1'),
    '1590',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shape the Sands'),
    (select id from sets where short_name = 'mb1'),
    '1332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Refurbish'),
    (select id from sets where short_name = 'mb1'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hooting Mandrills'),
    (select id from sets where short_name = 'mb1'),
    '1237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Oriflamme'),
    (select id from sets where short_name = 'mb1'),
    '1022',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overrun'),
    (select id from sets where short_name = 'mb1'),
    '1287',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overgrown Armasaur'),
    (select id from sets where short_name = 'mb1'),
    '1285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plague Wight'),
    (select id from sets where short_name = 'mb1'),
    '737',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hinterland Drake'),
    (select id from sets where short_name = 'mb1'),
    '401',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wirewood Lodge'),
    (select id from sets where short_name = 'mb1'),
    '1694',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Galvanic Blast'),
    (select id from sets where short_name = 'mb1'),
    '943',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reflector Mage'),
    (select id from sets where short_name = 'mb1'),
    '1473',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Virulent Swipe'),
    (select id from sets where short_name = 'mb1'),
    '815',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crush Dissent'),
    (select id from sets where short_name = 'mb1'),
    '339',
    'common'
) ,
(
    (select id from mtgcard where name = 'Queen''s Agent'),
    (select id from sets where short_name = 'mb1'),
    '740',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sleep'),
    (select id from sets where short_name = 'mb1'),
    '495',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'mb1'),
    '443',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hired Blade'),
    (select id from sets where short_name = 'mb1'),
    '686',
    'common'
) ,
(
    (select id from mtgcard where name = 'Survive the Night'),
    (select id from sets where short_name = 'mb1'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Memory Lapse'),
    (select id from sets where short_name = 'mb1'),
    '425',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pondering Mage'),
    (select id from sets where short_name = 'mb1'),
    '457',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Warrior'),
    (select id from sets where short_name = 'mb1'),
    '1196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Expedition Raptor'),
    (select id from sets where short_name = 'mb1'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dawn''s Reflection'),
    (select id from sets where short_name = 'mb1'),
    '1178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Looming Altisaur'),
    (select id from sets where short_name = 'mb1'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dismantling Blow'),
    (select id from sets where short_name = 'mb1'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eldritch Evolution'),
    (select id from sets where short_name = 'mb1'),
    '1190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bitterblade Warrior'),
    (select id from sets where short_name = 'mb1'),
    '1139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadly Tempest'),
    (select id from sets where short_name = 'mb1'),
    '616',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fog Bank'),
    (select id from sets where short_name = 'mb1'),
    '382',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dinosaur Hunter'),
    (select id from sets where short_name = 'mb1'),
    '627',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silhana Ledgewalker'),
    (select id from sets where short_name = 'mb1'),
    '1334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slash of Talons'),
    (select id from sets where short_name = 'mb1'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coalition Honor Guard'),
    (select id from sets where short_name = 'mb1'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistform Shrieker'),
    (select id from sets where short_name = 'mb1'),
    '433',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frilled Deathspitter'),
    (select id from sets where short_name = 'mb1'),
    '938',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greater Basilisk'),
    (select id from sets where short_name = 'mb1'),
    '1226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thornbow Archer'),
    (select id from sets where short_name = 'mb1'),
    '791',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chromatic Star'),
    (select id from sets where short_name = 'mb1'),
    '1560',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forge Devil'),
    (select id from sets where short_name = 'mb1'),
    '935',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hieroglyphic Illumination'),
    (select id from sets where short_name = 'mb1'),
    '399',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Breath'),
    (select id from sets where short_name = 'mb1'),
    '907',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zulaport Chainmage'),
    (select id from sets where short_name = 'mb1'),
    '830',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earthen Arms'),
    (select id from sets where short_name = 'mb1'),
    '1189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of the Tusk'),
    (select id from sets where short_name = 'mb1'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pack''s Favor'),
    (select id from sets where short_name = 'mb1'),
    '1288',
    'common'
) ,
(
    (select id from mtgcard where name = 'Citywatch Sphinx'),
    (select id from sets where short_name = 'mb1'),
    '318',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lay Claim'),
    (select id from sets where short_name = 'mb1'),
    '417',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Approach of the Second Sun'),
    (select id from sets where short_name = 'mb1'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Stirrings'),
    (select id from sets where short_name = 'mb1'),
    '1121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Join Shields'),
    (select id from sets where short_name = 'mb1'),
    '1439',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Cannonade'),
    (select id from sets where short_name = 'mb1'),
    '1021',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gust Walker'),
    (select id from sets where short_name = 'mb1'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Nacatl'),
    (select id from sets where short_name = 'mb1'),
    '1373',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whir of Invention'),
    (select id from sets where short_name = 'mb1'),
    '547',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mogg War Marshal'),
    (select id from sets where short_name = 'mb1'),
    '1015',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armadillo Cloak'),
    (select id from sets where short_name = 'mb1'),
    '1388',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firebrand Archer'),
    (select id from sets where short_name = 'mb1'),
    '928',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eyes in the Skies'),
    (select id from sets where short_name = 'mb1'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Borrowing 100,000 Arrows'),
    (select id from sets where short_name = 'mb1'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathreap Ritual'),
    (select id from sets where short_name = 'mb1'),
    '1414',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantom Centaur'),
    (select id from sets where short_name = 'mb1'),
    '1292',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sapphire Charm'),
    (select id from sets where short_name = 'mb1'),
    '478',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ondu War Cleric'),
    (select id from sets where short_name = 'mb1'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Painful Lesson'),
    (select id from sets where short_name = 'mb1'),
    '727',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ephemerate'),
    (select id from sets where short_name = 'mb1'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tar Snare'),
    (select id from sets where short_name = 'mb1'),
    '787',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slave of Bolas'),
    (select id from sets where short_name = 'mb1'),
    '1534',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Borderland Ranger'),
    (select id from sets where short_name = 'mb1'),
    '1146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temur Battle Rage'),
    (select id from sets where short_name = 'mb1'),
    '1081',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pilgrim''s Eye'),
    (select id from sets where short_name = 'mb1'),
    '1620',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Peel from Reality'),
    (select id from sets where short_name = 'mb1'),
    '452',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strategic Planning'),
    (select id from sets where short_name = 'mb1'),
    '507',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief Invoker'),
    (select id from sets where short_name = 'mb1'),
    '1284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Syncopate'),
    (select id from sets where short_name = 'mb1'),
    '511',
    'common'
) ,
(
    (select id from mtgcard where name = 'Claim // Fame'),
    (select id from sets where short_name = 'mb1'),
    '1536',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sliver Hivelord'),
    (select id from sets where short_name = 'mb1'),
    '1487',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Alchemist''s Vial'),
    (select id from sets where short_name = 'mb1'),
    '1542',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Eldest Reborn'),
    (select id from sets where short_name = 'mb1'),
    '645',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grazing Gladehart'),
    (select id from sets where short_name = 'mb1'),
    '1225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Broken Bond'),
    (select id from sets where short_name = 'mb1'),
    '1150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Animate Dead'),
    (select id from sets where short_name = 'mb1'),
    '566',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spider Spawning'),
    (select id from sets where short_name = 'mb1'),
    '1339',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inkfathom Divers'),
    (select id from sets where short_name = 'mb1'),
    '405',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prey''s Vengeance'),
    (select id from sets where short_name = 'mb1'),
    '1297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sun-Crowned Hunters'),
    (select id from sets where short_name = 'mb1'),
    '1074',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burnished Hart'),
    (select id from sets where short_name = 'mb1'),
    '1555',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shardless Agent'),
    (select id from sets where short_name = 'mb1'),
    '1484',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ornithopter'),
    (select id from sets where short_name = 'mb1'),
    '1615',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of One Thousand Cuts'),
    (select id from sets where short_name = 'mb1'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bonds of Faith'),
    (select id from sets where short_name = 'mb1'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terminate'),
    (select id from sets where short_name = 'mb1'),
    '1495',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tower Gargoyle'),
    (select id from sets where short_name = 'mb1'),
    '1499',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tempt with Discovery'),
    (select id from sets where short_name = 'mb1'),
    '1350',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel of the Dire Hour'),
    (select id from sets where short_name = 'mb1'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curio Vendor'),
    (select id from sets where short_name = 'mb1'),
    '341',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savage Twister'),
    (select id from sets where short_name = 'mb1'),
    '1481',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flamewave Invoker'),
    (select id from sets where short_name = 'mb1'),
    '933',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Healing Grace'),
    (select id from sets where short_name = 'mb1'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diminish'),
    (select id from sets where short_name = 'mb1'),
    '348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Browbeat'),
    (select id from sets where short_name = 'mb1'),
    '876',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glint'),
    (select id from sets where short_name = 'mb1'),
    '392',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armament Corps'),
    (select id from sets where short_name = 'mb1'),
    '1389',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beast Within'),
    (select id from sets where short_name = 'mb1'),
    '1134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yargle, Glutton of Urborg'),
    (select id from sets where short_name = 'mb1'),
    '829',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mystical Teachings'),
    (select id from sets where short_name = 'mb1'),
    '439',
    'common'
) ,
(
    (select id from mtgcard where name = 'Falkenrath Reaver'),
    (select id from sets where short_name = 'mb1'),
    '920',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clip Wings'),
    (select id from sets where short_name = 'mb1'),
    '1163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trial of Ambition'),
    (select id from sets where short_name = 'mb1'),
    '801',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon-Scarred Bear'),
    (select id from sets where short_name = 'mb1'),
    '1186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Universal Solvent'),
    (select id from sets where short_name = 'mb1'),
    '1648',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nantuko Husk'),
    (select id from sets where short_name = 'mb1'),
    '720',
    'common'
) ,
(
    (select id from mtgcard where name = 'Field of Ruin'),
    (select id from sets where short_name = 'mb1'),
    '1667',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cursed Minotaur'),
    (select id from sets where short_name = 'mb1'),
    '607',
    'common'
) ,
(
    (select id from mtgcard where name = 'Granitic Titan'),
    (select id from sets where short_name = 'mb1'),
    '965',
    'common'
) ,
(
    (select id from mtgcard where name = 'Caligo Skin-Witch'),
    (select id from sets where short_name = 'mb1'),
    '591',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vastwood Gorger'),
    (select id from sets where short_name = 'mb1'),
    '1366',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tukatongue Thallid'),
    (select id from sets where short_name = 'mb1'),
    '1364',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archetype of Imagination'),
    (select id from sets where short_name = 'mb1'),
    '290',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guided Strike'),
    (select id from sets where short_name = 'mb1'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Valakut Predator'),
    (select id from sets where short_name = 'mb1'),
    '1091',
    'common'
) ,
(
    (select id from mtgcard where name = 'Butcher''s Glee'),
    (select id from sets where short_name = 'mb1'),
    '586',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frogmite'),
    (select id from sets where short_name = 'mb1'),
    '1587',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arbor Elf'),
    (select id from sets where short_name = 'mb1'),
    '1124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obelisk Spider'),
    (select id from sets where short_name = 'mb1'),
    '1461',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Broodmother'),
    (select id from sets where short_name = 'mb1'),
    '1417',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sorcerer''s Broom'),
    (select id from sets where short_name = 'mb1'),
    '1634',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wing Shards'),
    (select id from sets where short_name = 'mb1'),
    '278',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Agony Warp'),
    (select id from sets where short_name = 'mb1'),
    '1385',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horseshoe Crab'),
    (select id from sets where short_name = 'mb1'),
    '402',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maverick Thopterist'),
    (select id from sets where short_name = 'mb1'),
    '1452',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bojuka Bog'),
    (select id from sets where short_name = 'mb1'),
    '1660',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nin, the Pain Artist'),
    (select id from sets where short_name = 'mb1'),
    '1460',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diamond Mare'),
    (select id from sets where short_name = 'mb1'),
    '1572',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Amphin Pathmage'),
    (select id from sets where short_name = 'mb1'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diabolic Edict'),
    (select id from sets where short_name = 'mb1'),
    '624',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steadfast Sentinel'),
    (select id from sets where short_name = 'mb1'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kin-Tree Invocation'),
    (select id from sets where short_name = 'mb1'),
    '1442',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Augur of Bolas'),
    (select id from sets where short_name = 'mb1'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armillary Sphere'),
    (select id from sets where short_name = 'mb1'),
    '1545',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'mb1'),
    '642',
    'common'
) ,
(
    (select id from mtgcard where name = 'First-Sphere Gargantua'),
    (select id from sets where short_name = 'mb1'),
    '662',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pierce the Sky'),
    (select id from sets where short_name = 'mb1'),
    '1293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silumgar Butcher'),
    (select id from sets where short_name = 'mb1'),
    '773',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chromatic Lantern'),
    (select id from sets where short_name = 'mb1'),
    '1559',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fusion Elemental'),
    (select id from sets where short_name = 'mb1'),
    '1427',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rosheen Meanderer'),
    (select id from sets where short_name = 'mb1'),
    '1531',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Emrakul''s Hatcher'),
    (select id from sets where short_name = 'mb1'),
    '915',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Nighthawk'),
    (select id from sets where short_name = 'mb1'),
    '813',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Saproling Migration'),
    (select id from sets where short_name = 'mb1'),
    '1323',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrummingbird'),
    (select id from sets where short_name = 'mb1'),
    '523',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boon of Emrakul'),
    (select id from sets where short_name = 'mb1'),
    '584',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruinous Gremlin'),
    (select id from sets where short_name = 'mb1'),
    '1045',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rosemane Centaur'),
    (select id from sets where short_name = 'mb1'),
    '1478',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pressure Point'),
    (select id from sets where short_name = 'mb1'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Dabbling'),
    (select id from sets where short_name = 'mb1'),
    '610',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghor-Clan Rampager'),
    (select id from sets where short_name = 'mb1'),
    '1429',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'mb1'),
    '253',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skymarcher Aspirant'),
    (select id from sets where short_name = 'mb1'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crowned Ceratok'),
    (select id from sets where short_name = 'mb1'),
    '1174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spectral Gateguards'),
    (select id from sets where short_name = 'mb1'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vessel of Malignity'),
    (select id from sets where short_name = 'mb1'),
    '814',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guided Passage'),
    (select id from sets where short_name = 'mb1'),
    '1433',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vessel of Volatility'),
    (select id from sets where short_name = 'mb1'),
    '1095',
    'common'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = 'mb1'),
    '1138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prickleboar'),
    (select id from sets where short_name = 'mb1'),
    '1027',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hardened Berserker'),
    (select id from sets where short_name = 'mb1'),
    '973',
    'common'
) ,
(
    (select id from mtgcard where name = 'Decree of Justice'),
    (select id from sets where short_name = 'mb1'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knight of the Skyward Eye'),
    (select id from sets where short_name = 'mb1'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Withering'),
    (select id from sets where short_name = 'mb1'),
    '612',
    'common'
) ,
(
    (select id from mtgcard where name = 'Extract from Darkness'),
    (select id from sets where short_name = 'mb1'),
    '1424',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Colossal Dreadmaw'),
    (select id from sets where short_name = 'mb1'),
    '1164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Will-o''-the-Wisp'),
    (select id from sets where short_name = 'mb1'),
    '826',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Accursed Spirit'),
    (select id from sets where short_name = 'mb1'),
    '559',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhonas''s Monument'),
    (select id from sets where short_name = 'mb1'),
    '1624',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thornwind Faeries'),
    (select id from sets where short_name = 'mb1'),
    '519',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rolling Thunder'),
    (select id from sets where short_name = 'mb1'),
    '1043',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brute Strength'),
    (select id from sets where short_name = 'mb1'),
    '877',
    'common'
) ,
(
    (select id from mtgcard where name = 'Built to Last'),
    (select id from sets where short_name = 'mb1'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyscanner'),
    (select id from sets where short_name = 'mb1'),
    '1631',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blasted Landscape'),
    (select id from sets where short_name = 'mb1'),
    '1657',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grim Contest'),
    (select id from sets where short_name = 'mb1'),
    '1432',
    'common'
) ,
(
    (select id from mtgcard where name = 'Centaur Courser'),
    (select id from sets where short_name = 'mb1'),
    '1158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Essence Scatter'),
    (select id from sets where short_name = 'mb1'),
    '367',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shambling Goblin'),
    (select id from sets where short_name = 'mb1'),
    '770',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Sentry'),
    (select id from sets where short_name = 'mb1'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Excavation Elephant'),
    (select id from sets where short_name = 'mb1'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Canyon Lurkers'),
    (select id from sets where short_name = 'mb1'),
    '880',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fretwork Colony'),
    (select id from sets where short_name = 'mb1'),
    '664',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grave Titan'),
    (select id from sets where short_name = 'mb1'),
    '676',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Destructor Dragon'),
    (select id from sets where short_name = 'mb1'),
    '1182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fungal Infection'),
    (select id from sets where short_name = 'mb1'),
    '665',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Javelin'),
    (select id from sets where short_name = 'mb1'),
    '1002',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knollspine Dragon'),
    (select id from sets where short_name = 'mb1'),
    '994',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rabid Bloodsucker'),
    (select id from sets where short_name = 'mb1'),
    '742',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vent Sentinel'),
    (select id from sets where short_name = 'mb1'),
    '1094',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcane Sanctum'),
    (select id from sets where short_name = 'mb1'),
    '1654',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelsong'),
    (select id from sets where short_name = 'mb1'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dream Twist'),
    (select id from sets where short_name = 'mb1'),
    '360',
    'common'
) ,
(
    (select id from mtgcard where name = 'Never Happened'),
    (select id from sets where short_name = 'mb1'),
    '721',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moonglove Extract'),
    (select id from sets where short_name = 'mb1'),
    '1611',
    'common'
) ,
(
    (select id from mtgcard where name = 'Putrefy'),
    (select id from sets where short_name = 'mb1'),
    '1466',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Somber Hoverguard'),
    (select id from sets where short_name = 'mb1'),
    '500',
    'common'
) ,
(
    (select id from mtgcard where name = 'Touch of Moonglove'),
    (select id from sets where short_name = 'mb1'),
    '797',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tajuru Warcaller'),
    (select id from sets where short_name = 'mb1'),
    '1347',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mnemonic Wall'),
    (select id from sets where short_name = 'mb1'),
    '435',
    'common'
) ,
(
    (select id from mtgcard where name = 'Caller of Gales'),
    (select id from sets where short_name = 'mb1'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Campaign of Vengeance'),
    (select id from sets where short_name = 'mb1'),
    '1406',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Oriflamme'),
    (select id from sets where short_name = 'mb1'),
    '958',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Broodhunter Wurm'),
    (select id from sets where short_name = 'mb1'),
    '1151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elesh Norn, Grand Cenobite'),
    (select id from sets where short_name = 'mb1'),
    '88',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Skaab Goliath'),
    (select id from sets where short_name = 'mb1'),
    '492',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fetid Imp'),
    (select id from sets where short_name = 'mb1'),
    '660',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mutiny'),
    (select id from sets where short_name = 'mb1'),
    '1018',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Greaves'),
    (select id from sets where short_name = 'mb1'),
    '1600',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silent Observer'),
    (select id from sets where short_name = 'mb1'),
    '489',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Imp'),
    (select id from sets where short_name = 'mb1'),
    '749',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undercity''s Embrace'),
    (select id from sets where short_name = 'mb1'),
    '805',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Spring'),
    (select id from sets where short_name = 'mb1'),
    '430',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Laboratory Brute'),
    (select id from sets where short_name = 'mb1'),
    '414',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'mb1'),
    '1058',
    'common'
) ,
(
    (select id from mtgcard where name = 'Veteran Swordsmith'),
    (select id from sets where short_name = 'mb1'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arch of Orazca'),
    (select id from sets where short_name = 'mb1'),
    '1655',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nimble-Blade Khenra'),
    (select id from sets where short_name = 'mb1'),
    '1019',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corpsehatch'),
    (select id from sets where short_name = 'mb1'),
    '601',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reaper of Night // Harvest Fear'),
    (select id from sets where short_name = 'mb1'),
    '747',
    'common'
) ,
(
    (select id from mtgcard where name = 'Affectionate Indrik'),
    (select id from sets where short_name = 'mb1'),
    '1112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Taurean Mauler'),
    (select id from sets where short_name = 'mb1'),
    '1079',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Basilica'),
    (select id from sets where short_name = 'mb1'),
    '1682',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manglehorn'),
    (select id from sets where short_name = 'mb1'),
    '1266',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krosan Verge'),
    (select id from sets where short_name = 'mb1'),
    '1679',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zhur-Taa Druid'),
    (select id from sets where short_name = 'mb1'),
    '1517',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dissenter''s Deliverance'),
    (select id from sets where short_name = 'mb1'),
    '1183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Righteous Cause'),
    (select id from sets where short_name = 'mb1'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sorin Markov'),
    (select id from sets where short_name = 'mb1'),
    '778',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Summit Prowler'),
    (select id from sets where short_name = 'mb1'),
    '1073',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basking Rootwalla'),
    (select id from sets where short_name = 'mb1'),
    '1130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evra, Halcyon Witness'),
    (select id from sets where short_name = 'mb1'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'mb1'),
    '1218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wishcoin Crab'),
    (select id from sets where short_name = 'mb1'),
    '553',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sensor Splicer'),
    (select id from sets where short_name = 'mb1'),
    '222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Path to Exile'),
    (select id from sets where short_name = 'mb1'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boulder Salvo'),
    (select id from sets where short_name = 'mb1'),
    '870',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ainok Bond-Kin'),
    (select id from sets where short_name = 'mb1'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forsake the Worldly'),
    (select id from sets where short_name = 'mb1'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Artisan of Kozilek'),
    (select id from sets where short_name = 'mb1'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trail of Evidence'),
    (select id from sets where short_name = 'mb1'),
    '529',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Faerie Conclave'),
    (select id from sets where short_name = 'mb1'),
    '1666',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shadowcloak Vampire'),
    (select id from sets where short_name = 'mb1'),
    '768',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rally the Peasants'),
    (select id from sets where short_name = 'mb1'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Instill Infection'),
    (select id from sets where short_name = 'mb1'),
    '695',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forgotten Cave'),
    (select id from sets where short_name = 'mb1'),
    '1668',
    'common'
) ,
(
    (select id from mtgcard where name = 'Watercourser'),
    (select id from sets where short_name = 'mb1'),
    '541',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undying Rage'),
    (select id from sets where short_name = 'mb1'),
    '1088',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enlightened Ascetic'),
    (select id from sets where short_name = 'mb1'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bladewing the Risen'),
    (select id from sets where short_name = 'mb1'),
    '1400',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feral Krushok'),
    (select id from sets where short_name = 'mb1'),
    '1208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mizzix''s Mastery'),
    (select id from sets where short_name = 'mb1'),
    '1012',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bewilder'),
    (select id from sets where short_name = 'mb1'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aggressive Instinct'),
    (select id from sets where short_name = 'mb1'),
    '1113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blighted Bat'),
    (select id from sets where short_name = 'mb1'),
    '578',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stream of Thought'),
    (select id from sets where short_name = 'mb1'),
    '508',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gift of Paradise'),
    (select id from sets where short_name = 'mb1'),
    '1221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spy Kit'),
    (select id from sets where short_name = 'mb1'),
    '1635',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Backwoods Survivalists'),
    (select id from sets where short_name = 'mb1'),
    '1128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Lacerator'),
    (select id from sets where short_name = 'mb1'),
    '812',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dauthi Mindripper'),
    (select id from sets where short_name = 'mb1'),
    '613',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battle-Rattle Shaman'),
    (select id from sets where short_name = 'mb1'),
    '851',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feat of Resistance'),
    (select id from sets where short_name = 'mb1'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhox Maulers'),
    (select id from sets where short_name = 'mb1'),
    '1313',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tidal Wave'),
    (select id from sets where short_name = 'mb1'),
    '526',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bestial Menace'),
    (select id from sets where short_name = 'mb1'),
    '1137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra Disciple'),
    (select id from sets where short_name = 'mb1'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sage of Lat-Nam'),
    (select id from sets where short_name = 'mb1'),
    '475',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scarab Feast'),
    (select id from sets where short_name = 'mb1'),
    '762',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghostblade Eidolon'),
    (select id from sets where short_name = 'mb1'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abzan Guide'),
    (select id from sets where short_name = 'mb1'),
    '1384',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Purge'),
    (select id from sets where short_name = 'mb1'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ambassador Oak'),
    (select id from sets where short_name = 'mb1'),
    '1117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Commune with Nature'),
    (select id from sets where short_name = 'mb1'),
    '1166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volunteer Reserves'),
    (select id from sets where short_name = 'mb1'),
    '268',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Innocent Blood'),
    (select id from sets where short_name = 'mb1'),
    '693',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mother of Runes'),
    (select id from sets where short_name = 'mb1'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Momentary Blink'),
    (select id from sets where short_name = 'mb1'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thran Golem'),
    (select id from sets where short_name = 'mb1'),
    '1641',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abzan Charm'),
    (select id from sets where short_name = 'mb1'),
    '1383',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dauntless Cathar'),
    (select id from sets where short_name = 'mb1'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lotus-Eye Mystics'),
    (select id from sets where short_name = 'mb1'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nucklavee'),
    (select id from sets where short_name = 'mb1'),
    '1528',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Rake'),
    (select id from sets where short_name = 'mb1'),
    '712',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alesha, Who Smiles at Death'),
    (select id from sets where short_name = 'mb1'),
    '837',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nameless Inversion'),
    (select id from sets where short_name = 'mb1'),
    '719',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure Hunt'),
    (select id from sets where short_name = 'mb1'),
    '531',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frantic Search'),
    (select id from sets where short_name = 'mb1'),
    '386',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Blessing'),
    (select id from sets where short_name = 'mb1'),
    '1216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Caravan Escort'),
    (select id from sets where short_name = 'mb1'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Felidar Sovereign'),
    (select id from sets where short_name = 'mb1'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hamlet Captain'),
    (select id from sets where short_name = 'mb1'),
    '1232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Basilisk Collar'),
    (select id from sets where short_name = 'mb1'),
    '1547',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Riftwing Cloudskate'),
    (select id from sets where short_name = 'mb1'),
    '469',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chaos Warp'),
    (select id from sets where short_name = 'mb1'),
    '885',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meren of Clan Nel Toth'),
    (select id from sets where short_name = 'mb1'),
    '1455',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Prakhata Club Security'),
    (select id from sets where short_name = 'mb1'),
    '738',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squirrel Wrangler'),
    (select id from sets where short_name = 'mb1'),
    '1340',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maximize Velocity'),
    (select id from sets where short_name = 'mb1'),
    '1010',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kolaghan Stormsinger'),
    (select id from sets where short_name = 'mb1'),
    '995',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carpet of Flowers'),
    (select id from sets where short_name = 'mb1'),
    '1156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Greenwood Sentinel'),
    (select id from sets where short_name = 'mb1'),
    '1229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balduvian Horde'),
    (select id from sets where short_name = 'mb1'),
    '847',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thallid Omnivore'),
    (select id from sets where short_name = 'mb1'),
    '790',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slipstream Eel'),
    (select id from sets where short_name = 'mb1'),
    '496',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frilled Sea Serpent'),
    (select id from sets where short_name = 'mb1'),
    '387',
    'common'
) 
 on conflict do nothing;
