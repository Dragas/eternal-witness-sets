insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tisd'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ooze'),
    (select id from sets where short_name = 'tisd'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire'),
    (select id from sets where short_name = 'tisd'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spider'),
    (select id from sets where short_name = 'tisd'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon'),
    (select id from sets where short_name = 'tisd'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'tisd'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tisd'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Innistrad Checklist'),
    (select id from sets where short_name = 'tisd'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'tisd'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'tisd'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tisd'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tisd'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Homunculus'),
    (select id from sets where short_name = 'tisd'),
    '3',
    'common'
) 
 on conflict do nothing;
