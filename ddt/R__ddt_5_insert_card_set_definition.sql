insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Battle Squadron'),
    (select id from sets where short_name = 'ddt'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Essence Scatter'),
    (select id from sets where short_name = 'ddt'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Glory Chaser'),
    (select id from sets where short_name = 'ddt'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cold-Eyed Selkie'),
    (select id from sets where short_name = 'ddt'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Master of Waves'),
    (select id from sets where short_name = 'ddt'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hordeling Outburst'),
    (select id from sets where short_name = 'ddt'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Charbelcher'),
    (select id from sets where short_name = 'ddt'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tidal Warrior'),
    (select id from sets where short_name = 'ddt'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddt'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Chieftain'),
    (select id from sets where short_name = 'ddt'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghostfire'),
    (select id from sets where short_name = 'ddt'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddt'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tidal Courier'),
    (select id from sets where short_name = 'ddt'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aquitect''s Will'),
    (select id from sets where short_name = 'ddt'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Ringleader'),
    (select id from sets where short_name = 'ddt'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Spring'),
    (select id from sets where short_name = 'ddt'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blighted Cataract'),
    (select id from sets where short_name = 'ddt'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddt'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk Looter'),
    (select id from sets where short_name = 'ddt'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Diplomats'),
    (select id from sets where short_name = 'ddt'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddt'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Engulf the Shore'),
    (select id from sets where short_name = 'ddt'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tarfire'),
    (select id from sets where short_name = 'ddt'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddt'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Concentrate'),
    (select id from sets where short_name = 'ddt'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forgotten Cave'),
    (select id from sets where short_name = 'ddt'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddt'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Wardriver'),
    (select id from sets where short_name = 'ddt'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wake Thrasher'),
    (select id from sets where short_name = 'ddt'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inkfathom Divers'),
    (select id from sets where short_name = 'ddt'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brute Strength'),
    (select id from sets where short_name = 'ddt'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Tunneler'),
    (select id from sets where short_name = 'ddt'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Razerunners'),
    (select id from sets where short_name = 'ddt'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gempalm Incinerator'),
    (select id from sets where short_name = 'ddt'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harbinger of the Tides'),
    (select id from sets where short_name = 'ddt'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddt'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cleaver Riot'),
    (select id from sets where short_name = 'ddt'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tidal Wave'),
    (select id from sets where short_name = 'ddt'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krenko, Mob Boss'),
    (select id from sets where short_name = 'ddt'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Goon'),
    (select id from sets where short_name = 'ddt'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krenko''s Command'),
    (select id from sets where short_name = 'ddt'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foundry Street Denizen'),
    (select id from sets where short_name = 'ddt'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warren Instigator'),
    (select id from sets where short_name = 'ddt'),
    '32',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scroll Thief'),
    (select id from sets where short_name = 'ddt'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk Wayfinder'),
    (select id from sets where short_name = 'ddt'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddt'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Grenade'),
    (select id from sets where short_name = 'ddt'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lonely Sandbar'),
    (select id from sets where short_name = 'ddt'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Claustrophobia'),
    (select id from sets where short_name = 'ddt'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Master of the Pearl Trident'),
    (select id from sets where short_name = 'ddt'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ember Hauler'),
    (select id from sets where short_name = 'ddt'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Rabblemaster'),
    (select id from sets where short_name = 'ddt'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Misdirection'),
    (select id from sets where short_name = 'ddt'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relentless Assault'),
    (select id from sets where short_name = 'ddt'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boggart Brute'),
    (select id from sets where short_name = 'ddt'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brittle Effigy'),
    (select id from sets where short_name = 'ddt'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merfolk Sovereign'),
    (select id from sets where short_name = 'ddt'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Triton Tactics'),
    (select id from sets where short_name = 'ddt'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rootwater Hunter'),
    (select id from sets where short_name = 'ddt'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blighted Gorge'),
    (select id from sets where short_name = 'ddt'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tidebinder Mage'),
    (select id from sets where short_name = 'ddt'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merrow Reejerey'),
    (select id from sets where short_name = 'ddt'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Streambed Aquitects'),
    (select id from sets where short_name = 'ddt'),
    '18',
    'common'
) 
 on conflict do nothing;
