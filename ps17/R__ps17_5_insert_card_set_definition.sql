insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Jace, Unraveler of Secrets'),
    (select id from sets where short_name = 'ps17'),
    '69',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nissa, Steward of Elements'),
    (select id from sets where short_name = 'ps17'),
    '204',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gideon of the Trials'),
    (select id from sets where short_name = 'ps17'),
    '14',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chandra, Torch of Defiance'),
    (select id from sets where short_name = 'ps17'),
    '110',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Liliana, Death''s Majesty'),
    (select id from sets where short_name = 'ps17'),
    '97',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, God-Pharaoh'),
    (select id from sets where short_name = 'ps17'),
    '140',
    'mythic'
) 
 on conflict do nothing;
