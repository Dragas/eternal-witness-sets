    insert into mtgcard(name) values ('Jace, Unraveler of Secrets') on conflict do nothing;
    insert into mtgcard(name) values ('Nissa, Steward of Elements') on conflict do nothing;
    insert into mtgcard(name) values ('Gideon of the Trials') on conflict do nothing;
    insert into mtgcard(name) values ('Chandra, Torch of Defiance') on conflict do nothing;
    insert into mtgcard(name) values ('Liliana, Death''s Majesty') on conflict do nothing;
    insert into mtgcard(name) values ('Nicol Bolas, God-Pharaoh') on conflict do nothing;
