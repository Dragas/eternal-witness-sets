insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Mirrored Depths'),
    (select id from sets where short_name = 'phop'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stairs to Infinity'),
    (select id from sets where short_name = 'phop'),
    'P1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tazeem'),
    (select id from sets where short_name = 'phop'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Horizon Boughs'),
    (select id from sets where short_name = 'phop'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tember City'),
    (select id from sets where short_name = 'phop'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestine Reef'),
    (select id from sets where short_name = 'phop'),
    '42',
    'rare'
) 
 on conflict do nothing;
