    insert into mtgcard(name) values ('Mirrored Depths') on conflict do nothing;
    insert into mtgcard(name) values ('Stairs to Infinity') on conflict do nothing;
    insert into mtgcard(name) values ('Tazeem') on conflict do nothing;
    insert into mtgcard(name) values ('Horizon Boughs') on conflict do nothing;
    insert into mtgcard(name) values ('Tember City') on conflict do nothing;
    insert into mtgcard(name) values ('Celestine Reef') on conflict do nothing;
