insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Mirrored Depths'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mirrored Depths'),
        (select types.id from types where name = 'Karsus')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stairs to Infinity'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stairs to Infinity'),
        (select types.id from types where name = 'Xerex')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tazeem'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tazeem'),
        (select types.id from types where name = 'Zendikar')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Horizon Boughs'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Horizon Boughs'),
        (select types.id from types where name = 'Pyrulea')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tember City'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tember City'),
        (select types.id from types where name = 'Kinshala')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Celestine Reef'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Celestine Reef'),
        (select types.id from types where name = 'Luvion')
    ) 
 on conflict do nothing;
