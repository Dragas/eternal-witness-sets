insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ashaya, the Awoken World'),
    (select id from sets where short_name = 'tori'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon'),
    (select id from sets where short_name = 'tori'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight'),
    (select id from sets where short_name = 'tori'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace, Telepath Unbound Emblem'),
    (select id from sets where short_name = 'tori'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra, Roaring Flame Emblem'),
    (select id from sets where short_name = 'tori'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elf Warrior'),
    (select id from sets where short_name = 'tori'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter'),
    (select id from sets where short_name = 'tori'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'tori'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tori'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tori'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter'),
    (select id from sets where short_name = 'tori'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana, Defiant Necromancer Emblem'),
    (select id from sets where short_name = 'tori'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tori'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tori'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magic Origins Checklist'),
    (select id from sets where short_name = 'tori'),
    '0',
    'common'
) 
 on conflict do nothing;
