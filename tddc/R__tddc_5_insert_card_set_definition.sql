insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Thrull'),
    (select id from sets where short_name = 'tddc'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon'),
    (select id from sets where short_name = 'tddc'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tddc'),
    '1',
    'common'
) 
 on conflict do nothing;
