insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Wall of Roots'),
    (select id from sets where short_name = 'f08'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thirst for Knowledge'),
    (select id from sets where short_name = 'f08'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tendrils of Agony'),
    (select id from sets where short_name = 'f08'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tormod''s Crypt'),
    (select id from sets where short_name = 'f08'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Resurrection'),
    (select id from sets where short_name = 'f08'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Isochron Scepter'),
    (select id from sets where short_name = 'f08'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desert'),
    (select id from sets where short_name = 'f08'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eternal Witness'),
    (select id from sets where short_name = 'f08'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Remand'),
    (select id from sets where short_name = 'f08'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serrated Arrows'),
    (select id from sets where short_name = 'f08'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pendelhaven'),
    (select id from sets where short_name = 'f08'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shrapnel Blast'),
    (select id from sets where short_name = 'f08'),
    '12',
    'rare'
) 
 on conflict do nothing;
