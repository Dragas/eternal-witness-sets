insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Bubbling Beebles'),
    (select id from sets where short_name = 'uds'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slinking Skirge'),
    (select id from sets where short_name = 'uds'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tormented Angel'),
    (select id from sets where short_name = 'uds'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ivy Seer'),
    (select id from sets where short_name = 'uds'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hulking Ogre'),
    (select id from sets where short_name = 'uds'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temporal Adept'),
    (select id from sets where short_name = 'uds'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Capashen Templar'),
    (select id from sets where short_name = 'uds'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wake of Destruction'),
    (select id from sets where short_name = 'uds'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eradicate'),
    (select id from sets where short_name = 'uds'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Momentum'),
    (select id from sets where short_name = 'uds'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Masons'),
    (select id from sets where short_name = 'uds'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archery Training'),
    (select id from sets where short_name = 'uds'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Capashen Standard'),
    (select id from sets where short_name = 'uds'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rapid Decay'),
    (select id from sets where short_name = 'uds'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Braidwood Sextant'),
    (select id from sets where short_name = 'uds'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Voice of Duty'),
    (select id from sets where short_name = 'uds'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Colos Yearling'),
    (select id from sets where short_name = 'uds'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chime of Night'),
    (select id from sets where short_name = 'uds'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Metalworker'),
    (select id from sets where short_name = 'uds'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fodder Cannon'),
    (select id from sets where short_name = 'uds'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reliquary Monk'),
    (select id from sets where short_name = 'uds'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Compost'),
    (select id from sets where short_name = 'uds'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dying Wail'),
    (select id from sets where short_name = 'uds'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thieving Magpie'),
    (select id from sets where short_name = 'uds'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tethered Griffin'),
    (select id from sets where short_name = 'uds'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wild Colos'),
    (select id from sets where short_name = 'uds'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brine Seer'),
    (select id from sets where short_name = 'uds'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Extruder'),
    (select id from sets where short_name = 'uds'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Elder'),
    (select id from sets where short_name = 'uds'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Capashen Knight'),
    (select id from sets where short_name = 'uds'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plated Spider'),
    (select id from sets where short_name = 'uds'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opalescence'),
    (select id from sets where short_name = 'uds'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thran Dynamo'),
    (select id from sets where short_name = 'uds'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Academy Rector'),
    (select id from sets where short_name = 'uds'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kingfisher'),
    (select id from sets where short_name = 'uds'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Negator'),
    (select id from sets where short_name = 'uds'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Hollow'),
    (select id from sets where short_name = 'uds'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disease Carriers'),
    (select id from sets where short_name = 'uds'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancient Silverback'),
    (select id from sets where short_name = 'uds'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Powder Keg'),
    (select id from sets where short_name = 'uds'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Piper'),
    (select id from sets where short_name = 'uds'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Monitor'),
    (select id from sets where short_name = 'uds'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thran Golem'),
    (select id from sets where short_name = 'uds'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scent of Nightshade'),
    (select id from sets where short_name = 'uds'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Multani''s Decree'),
    (select id from sets where short_name = 'uds'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blizzard Elemental'),
    (select id from sets where short_name = 'uds'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mark of Fury'),
    (select id from sets where short_name = 'uds'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marker Beetles'),
    (select id from sets where short_name = 'uds'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mental Discipline'),
    (select id from sets where short_name = 'uds'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Glare'),
    (select id from sets where short_name = 'uds'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'False Prophet'),
    (select id from sets where short_name = 'uds'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treachery'),
    (select id from sets where short_name = 'uds'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quash'),
    (select id from sets where short_name = 'uds'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rescue'),
    (select id from sets where short_name = 'uds'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Twisted Experiment'),
    (select id from sets where short_name = 'uds'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Metathran Soldier'),
    (select id from sets where short_name = 'uds'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fatigue'),
    (select id from sets where short_name = 'uds'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Sting'),
    (select id from sets where short_name = 'uds'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Marshal'),
    (select id from sets where short_name = 'uds'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rofellos, Llanowar Emissary'),
    (select id from sets where short_name = 'uds'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plague Dogs'),
    (select id from sets where short_name = 'uds'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mask of Law and Grace'),
    (select id from sets where short_name = 'uds'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bubbling Muck'),
    (select id from sets where short_name = 'uds'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Apprentice Necromancer'),
    (select id from sets where short_name = 'uds'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Private Research'),
    (select id from sets where short_name = 'uds'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Iridescent Drake'),
    (select id from sets where short_name = 'uds'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scrying Glass'),
    (select id from sets where short_name = 'uds'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Telepathic Spies'),
    (select id from sets where short_name = 'uds'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Storage Matrix'),
    (select id from sets where short_name = 'uds'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magnify'),
    (select id from sets where short_name = 'uds'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Gardener'),
    (select id from sets where short_name = 'uds'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Donate'),
    (select id from sets where short_name = 'uds'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Illuminated Wings'),
    (select id from sets where short_name = 'uds'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Replenish'),
    (select id from sets where short_name = 'uds'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skittering Horror'),
    (select id from sets where short_name = 'uds'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carnival of Souls'),
    (select id from sets where short_name = 'uds'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Incendiary'),
    (select id from sets where short_name = 'uds'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Taunting Elf'),
    (select id from sets where short_name = 'uds'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yawgmoth''s Bargain'),
    (select id from sets where short_name = 'uds'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aura Thief'),
    (select id from sets where short_name = 'uds'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fledgling Osprey'),
    (select id from sets where short_name = 'uds'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rayne, Academy Chancellor'),
    (select id from sets where short_name = 'uds'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reckless Abandon'),
    (select id from sets where short_name = 'uds'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Masticore'),
    (select id from sets where short_name = 'uds'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hunting Moa'),
    (select id from sets where short_name = 'uds'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Festering Wound'),
    (select id from sets where short_name = 'uds'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodshot Cyclops'),
    (select id from sets where short_name = 'uds'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Feast'),
    (select id from sets where short_name = 'uds'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Opposition'),
    (select id from sets where short_name = 'uds'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heart Warden'),
    (select id from sets where short_name = 'uds'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thorn Elemental'),
    (select id from sets where short_name = 'uds'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lurking Jackals'),
    (select id from sets where short_name = 'uds'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mantis Engine'),
    (select id from sets where short_name = 'uds'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Emperor Crocodile'),
    (select id from sets where short_name = 'uds'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thran Foundry'),
    (select id from sets where short_name = 'uds'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pattern of Rebirth'),
    (select id from sets where short_name = 'uds'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trumpet Blast'),
    (select id from sets where short_name = 'uds'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plow Under'),
    (select id from sets where short_name = 'uds'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sigil of Sleep'),
    (select id from sets where short_name = 'uds'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Berserker'),
    (select id from sets where short_name = 'uds'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rofellos''s Gift'),
    (select id from sets where short_name = 'uds'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flame Jet'),
    (select id from sets where short_name = 'uds'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scent of Ivy'),
    (select id from sets where short_name = 'uds'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fend Off'),
    (select id from sets where short_name = 'uds'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jasmine Seer'),
    (select id from sets where short_name = 'uds'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ravenous Rats'),
    (select id from sets where short_name = 'uds'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Caltrops'),
    (select id from sets where short_name = 'uds'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Metathran Elite'),
    (select id from sets where short_name = 'uds'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Festival'),
    (select id from sets where short_name = 'uds'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keldon Champion'),
    (select id from sets where short_name = 'uds'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gamekeeper'),
    (select id from sets where short_name = 'uds'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Field Surgeon'),
    (select id from sets where short_name = 'uds'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disappear'),
    (select id from sets where short_name = 'uds'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Incubator'),
    (select id from sets where short_name = 'uds'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scent of Cinder'),
    (select id from sets where short_name = 'uds'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scent of Jasmine'),
    (select id from sets where short_name = 'uds'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Landslide'),
    (select id from sets where short_name = 'uds'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Braidwood Cup'),
    (select id from sets where short_name = 'uds'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Junk Diver'),
    (select id from sets where short_name = 'uds'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brass Secretary'),
    (select id from sets where short_name = 'uds'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Solidarity'),
    (select id from sets where short_name = 'uds'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Covetous Dragon'),
    (select id from sets where short_name = 'uds'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Body Snatcher'),
    (select id from sets where short_name = 'uds'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Enchantress'),
    (select id from sets where short_name = 'uds'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scour'),
    (select id from sets where short_name = 'uds'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra Advocate'),
    (select id from sets where short_name = 'uds'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sanctimony'),
    (select id from sets where short_name = 'uds'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Repercussion'),
    (select id from sets where short_name = 'uds'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scent of Brine'),
    (select id from sets where short_name = 'uds'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Impatience'),
    (select id from sets where short_name = 'uds'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cinder Seer'),
    (select id from sets where short_name = 'uds'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Lookout'),
    (select id from sets where short_name = 'uds'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sowing Salt'),
    (select id from sets where short_name = 'uds'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Master Healer'),
    (select id from sets where short_name = 'uds'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightshade Seer'),
    (select id from sets where short_name = 'uds'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Attrition'),
    (select id from sets where short_name = 'uds'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Squirming Mass'),
    (select id from sets where short_name = 'uds'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Splinter'),
    (select id from sets where short_name = 'uds'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Voice of Reason'),
    (select id from sets where short_name = 'uds'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keldon Vandals'),
    (select id from sets where short_name = 'uds'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flicker'),
    (select id from sets where short_name = 'uds'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goliath Beetle'),
    (select id from sets where short_name = 'uds'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Encroach'),
    (select id from sets where short_name = 'uds'),
    '59',
    'uncommon'
) 
 on conflict do nothing;
