    insert into mtgcard(name) values ('Reciprocate') on conflict do nothing;
    insert into mtgcard(name) values ('Fireball') on conflict do nothing;
    insert into mtgcard(name) values ('Oxidize') on conflict do nothing;
    insert into mtgcard(name) values ('Terror') on conflict do nothing;
    insert into mtgcard(name) values ('Psychatog') on conflict do nothing;
    insert into mtgcard(name) values ('Mana Leak') on conflict do nothing;
