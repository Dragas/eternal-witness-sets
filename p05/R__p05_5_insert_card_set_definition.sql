insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Reciprocate'),
    (select id from sets where short_name = 'p05'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'p05'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oxidize'),
    (select id from sets where short_name = 'p05'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = 'p05'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Psychatog'),
    (select id from sets where short_name = 'p05'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Leak'),
    (select id from sets where short_name = 'p05'),
    '5',
    'rare'
) 
 on conflict do nothing;
