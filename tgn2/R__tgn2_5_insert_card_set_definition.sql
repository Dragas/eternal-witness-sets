insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tgn2'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dinosaur'),
    (select id from sets where short_name = 'tgn2'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tgn2'),
    '2',
    'common'
) 
 on conflict do nothing;
