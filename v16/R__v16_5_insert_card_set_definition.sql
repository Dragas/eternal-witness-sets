insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Umezawa''s Jitte'),
    (select id from sets where short_name = 'v16'),
    '14',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Marit Lage'),
    (select id from sets where short_name = 'v16'),
    '16',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Unmask'),
    (select id from sets where short_name = 'v16'),
    '15',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Processor'),
    (select id from sets where short_name = 'v16'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Helvault'),
    (select id from sets where short_name = 'v16'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mind''s Desire'),
    (select id from sets where short_name = 'v16'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Obliterate'),
    (select id from sets where short_name = 'v16'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cabal Ritual'),
    (select id from sets where short_name = 'v16'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tolaria West'),
    (select id from sets where short_name = 'v16'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Beseech the Queen'),
    (select id from sets where short_name = 'v16'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Memnarch'),
    (select id from sets where short_name = 'v16'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Near-Death Experience'),
    (select id from sets where short_name = 'v16'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dark Depths'),
    (select id from sets where short_name = 'v16'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Conflux'),
    (select id from sets where short_name = 'v16'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Glissa, the Traitor'),
    (select id from sets where short_name = 'v16'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Momir Vig, Simic Visionary'),
    (select id from sets where short_name = 'v16'),
    '9',
    'mythic'
) 
 on conflict do nothing;
