insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ooze'),
    (select id from sets where short_name = 'trna'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure'),
    (select id from sets where short_name = 'trna'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'trna'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Domri, Chaos Bringer Emblem'),
    (select id from sets where short_name = 'trna'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'trna'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frog Lizard'),
    (select id from sets where short_name = 'trna'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Human'),
    (select id from sets where short_name = 'trna'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Centaur'),
    (select id from sets where short_name = 'trna'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'trna'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter'),
    (select id from sets where short_name = 'trna'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'trna'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illusion'),
    (select id from sets where short_name = 'trna'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx'),
    (select id from sets where short_name = 'trna'),
    '9',
    'common'
) 
 on conflict do nothing;
