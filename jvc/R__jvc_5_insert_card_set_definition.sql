insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ophidian'),
    (select id from sets where short_name = 'jvc'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guile'),
    (select id from sets where short_name = 'jvc'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Waterspout Djinn'),
    (select id from sets where short_name = 'jvc'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Martyr of Frost'),
    (select id from sets where short_name = 'jvc'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soulbright Flamekin'),
    (select id from sets where short_name = 'jvc'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'jvc'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magma Jet'),
    (select id from sets where short_name = 'jvc'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seal of Fire'),
    (select id from sets where short_name = 'jvc'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'jvc'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'jvc'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancestral Vision'),
    (select id from sets where short_name = 'jvc'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oxidda Golem'),
    (select id from sets where short_name = 'jvc'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brine Elemental'),
    (select id from sets where short_name = 'jvc'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flamekin Brawler'),
    (select id from sets where short_name = 'jvc'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra Nalaar'),
    (select id from sets where short_name = 'jvc'),
    '34',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'jvc'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flame Javelin'),
    (select id from sets where short_name = 'jvc'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyre Charger'),
    (select id from sets where short_name = 'jvc'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fledgling Mawcor'),
    (select id from sets where short_name = 'jvc'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demonfire'),
    (select id from sets where short_name = 'jvc'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fireblast'),
    (select id from sets where short_name = 'jvc'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Willbender'),
    (select id from sets where short_name = 'jvc'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firebolt'),
    (select id from sets where short_name = 'jvc'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Deceit'),
    (select id from sets where short_name = 'jvc'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Repulse'),
    (select id from sets where short_name = 'jvc'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inner-Flame Acolyte'),
    (select id from sets where short_name = 'jvc'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ingot Chewer'),
    (select id from sets where short_name = 'jvc'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'jvc'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riftwing Cloudskate'),
    (select id from sets where short_name = 'jvc'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fireslinger'),
    (select id from sets where short_name = 'jvc'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'jvc'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terrain Generator'),
    (select id from sets where short_name = 'jvc'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'jvc'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daze'),
    (select id from sets where short_name = 'jvc'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mulldrifter'),
    (select id from sets where short_name = 'jvc'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace Beleren'),
    (select id from sets where short_name = 'jvc'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fact or Fiction'),
    (select id from sets where short_name = 'jvc'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slith Firewalker'),
    (select id from sets where short_name = 'jvc'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Stone'),
    (select id from sets where short_name = 'jvc'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quicksilver Dragon'),
    (select id from sets where short_name = 'jvc'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Errant Ephemeron'),
    (select id from sets where short_name = 'jvc'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Man-o''-War'),
    (select id from sets where short_name = 'jvc'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'jvc'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cone of Flame'),
    (select id from sets where short_name = 'jvc'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spire Golem'),
    (select id from sets where short_name = 'jvc'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Furnace Whelp'),
    (select id from sets where short_name = 'jvc'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flamewave Invoker'),
    (select id from sets where short_name = 'jvc'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Pit Dragon'),
    (select id from sets where short_name = 'jvc'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chartooth Cougar'),
    (select id from sets where short_name = 'jvc'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'jvc'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu'),
    (select id from sets where short_name = 'jvc'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gush'),
    (select id from sets where short_name = 'jvc'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keldon Megaliths'),
    (select id from sets where short_name = 'jvc'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'jvc'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fathom Seer'),
    (select id from sets where short_name = 'jvc'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bottle Gnomes'),
    (select id from sets where short_name = 'jvc'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hostility'),
    (select id from sets where short_name = 'jvc'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = 'jvc'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Condescend'),
    (select id from sets where short_name = 'jvc'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aethersnipe'),
    (select id from sets where short_name = 'jvc'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voidmage Apprentice'),
    (select id from sets where short_name = 'jvc'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = 'jvc'),
    '13',
    'uncommon'
) 
 on conflict do nothing;
