insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ornithopter'),
    (select id from sets where short_name = '5ed'),
    '393',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Psychic Venom'),
    (select id from sets where short_name = '5ed'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nature''s Lore'),
    (select id from sets where short_name = '5ed'),
    '316',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dingus Egg'),
    (select id from sets where short_name = '5ed'),
    '364',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Time Elemental'),
    (select id from sets where short_name = '5ed'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = '5ed'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Forces'),
    (select id from sets where short_name = '5ed'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bird Maiden'),
    (select id from sets where short_name = '5ed'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feldon''s Cane'),
    (select id from sets where short_name = '5ed'),
    '368',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thrull Retainer'),
    (select id from sets where short_name = '5ed'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dwarven Ruins'),
    (select id from sets where short_name = '5ed'),
    '415',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '5ed'),
    '446',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ice Floe'),
    (select id from sets where short_name = '5ed'),
    '420',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Vault'),
    (select id from sets where short_name = '5ed'),
    '388',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disrupting Scepter'),
    (select id from sets where short_name = '5ed'),
    '365',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Craw Giant'),
    (select id from sets where short_name = '5ed'),
    '285',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Word of Blasting'),
    (select id from sets where short_name = '5ed'),
    '276',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jayemdae Tome'),
    (select id from sets where short_name = '5ed'),
    '384',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scaled Wurm'),
    (select id from sets where short_name = '5ed'),
    '322',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = '5ed'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea Spirit'),
    (select id from sets where short_name = '5ed'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '5ed'),
    '436',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tsunami'),
    (select id from sets where short_name = '5ed'),
    '334',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sacred Boon'),
    (select id from sets where short_name = '5ed'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brainwash'),
    (select id from sets where short_name = '5ed'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Torture'),
    (select id from sets where short_name = '5ed'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serpent Generator'),
    (select id from sets where short_name = '5ed'),
    '397',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fyndhorn Elder'),
    (select id from sets where short_name = '5ed'),
    '297',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armor of Faith'),
    (select id from sets where short_name = '5ed'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Initiates of the Ebon Hand'),
    (select id from sets where short_name = '5ed'),
    '169s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '5ed'),
    '449',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chub Toad'),
    (select id from sets where short_name = '5ed'),
    '283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '5ed'),
    '439',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time Bomb'),
    (select id from sets where short_name = '5ed'),
    '404',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Game of Chaos'),
    (select id from sets where short_name = '5ed'),
    '232†',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unstable Mutation'),
    (select id from sets where short_name = '5ed'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drudge Skeletons'),
    (select id from sets where short_name = '5ed'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '5ed'),
    '447',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Terrain'),
    (select id from sets where short_name = '5ed'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: White'),
    (select id from sets where short_name = '5ed'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ivory Cup'),
    (select id from sets where short_name = '5ed'),
    '380',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death Ward'),
    (select id from sets where short_name = '5ed'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primal Clay'),
    (select id from sets where short_name = '5ed'),
    '395',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rod of Ruin'),
    (select id from sets where short_name = '5ed'),
    '396',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyroblast'),
    (select id from sets where short_name = '5ed'),
    '262',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Touch of Death'),
    (select id from sets where short_name = '5ed'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Conscripts'),
    (select id from sets where short_name = '5ed'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paralyze'),
    (select id from sets where short_name = '5ed'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pikemen'),
    (select id from sets where short_name = '5ed'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = '5ed'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Craw Wurm'),
    (select id from sets where short_name = '5ed'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howl from Beyond'),
    (select id from sets where short_name = '5ed'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Ravel'),
    (select id from sets where short_name = '5ed'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hurricane'),
    (select id from sets where short_name = '5ed'),
    '303',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Oriflamme'),
    (select id from sets where short_name = '5ed'),
    '257',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Hero'),
    (select id from sets where short_name = '5ed'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feedback'),
    (select id from sets where short_name = '5ed'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = '5ed'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vodalian Soldiers'),
    (select id from sets where short_name = '5ed'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Animate Dead'),
    (select id from sets where short_name = '5ed'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tundra Wolves'),
    (select id from sets where short_name = '5ed'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elder Druid'),
    (select id from sets where short_name = '5ed'),
    '290',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carrion Ants'),
    (select id from sets where short_name = '5ed'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necropotence'),
    (select id from sets where short_name = '5ed'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Svyelunite Temple'),
    (select id from sets where short_name = '5ed'),
    '425',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Carapace'),
    (select id from sets where short_name = '5ed'),
    '281',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '5ed'),
    '441',
    'common'
) ,
(
    (select id from mtgcard where name = 'Imposing Visage'),
    (select id from sets where short_name = '5ed'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prismatic Ward'),
    (select id from sets where short_name = '5ed'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jandor''s Saddlebags'),
    (select id from sets where short_name = '5ed'),
    '383',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fellwar Stone'),
    (select id from sets where short_name = '5ed'),
    '369',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coral Helm'),
    (select id from sets where short_name = '5ed'),
    '359',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conquer'),
    (select id from sets where short_name = '5ed'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abbey Gargoyles'),
    (select id from sets where short_name = '5ed'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eternal Warrior'),
    (select id from sets where short_name = '5ed'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Growth'),
    (select id from sets where short_name = '5ed'),
    '342',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flood'),
    (select id from sets where short_name = '5ed'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abyssal Specter'),
    (select id from sets where short_name = '5ed'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Blue'),
    (select id from sets where short_name = '5ed'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reef Pirates'),
    (select id from sets where short_name = '5ed'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steal Artifact'),
    (select id from sets where short_name = '5ed'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disintegrate'),
    (select id from sets where short_name = '5ed'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evil Presence'),
    (select id from sets where short_name = '5ed'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '5ed'),
    '440',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Speakers'),
    (select id from sets where short_name = '5ed'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Net'),
    (select id from sets where short_name = '5ed'),
    '400',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glasses of Urza'),
    (select id from sets where short_name = '5ed'),
    '374',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Benalish Hero'),
    (select id from sets where short_name = '5ed'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit Link'),
    (select id from sets where short_name = '5ed'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shanodin Dryads'),
    (select id from sets where short_name = '5ed'),
    '325',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ironclaw Curse'),
    (select id from sets where short_name = '5ed'),
    '244†',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clay Statue'),
    (select id from sets where short_name = '5ed'),
    '355',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greater Werewolf'),
    (select id from sets where short_name = '5ed'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crusade'),
    (select id from sets where short_name = '5ed'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Lust'),
    (select id from sets where short_name = '5ed'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Clash'),
    (select id from sets where short_name = '5ed'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Breeding Pit'),
    (select id from sets where short_name = '5ed'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fungusaur'),
    (select id from sets where short_name = '5ed'),
    '296',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winds of Change'),
    (select id from sets where short_name = '5ed'),
    '275',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '5ed'),
    '432',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aspect of Wolf'),
    (select id from sets where short_name = '5ed'),
    '278',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karplusan Forest'),
    (select id from sets where short_name = '5ed'),
    '421',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cursed Land'),
    (select id from sets where short_name = '5ed'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winter Orb'),
    (select id from sets where short_name = '5ed'),
    '408',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magus of the Unseen'),
    (select id from sets where short_name = '5ed'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lord of the Pit'),
    (select id from sets where short_name = '5ed'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Bomb'),
    (select id from sets where short_name = '5ed'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = '5ed'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sleight of Mind'),
    (select id from sets where short_name = '5ed'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hollow Trees'),
    (select id from sets where short_name = '5ed'),
    '418',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Green'),
    (select id from sets where short_name = '5ed'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Rats'),
    (select id from sets where short_name = '5ed'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ball Lightning'),
    (select id from sets where short_name = '5ed'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flashfires'),
    (select id from sets where short_name = '5ed'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drain Life'),
    (select id from sets where short_name = '5ed'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloak of Confusion'),
    (select id from sets where short_name = '5ed'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hecatomb'),
    (select id from sets where short_name = '5ed'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meekstone'),
    (select id from sets where short_name = '5ed'),
    '389',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '5ed'),
    '442',
    'common'
) ,
(
    (select id from mtgcard where name = 'Regeneration'),
    (select id from sets where short_name = '5ed'),
    '321',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crumble'),
    (select id from sets where short_name = '5ed'),
    '287',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyrotechnics'),
    (select id from sets where short_name = '5ed'),
    '263',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = '5ed'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hill Giant'),
    (select id from sets where short_name = '5ed'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crystal Rod'),
    (select id from sets where short_name = '5ed'),
    '361',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sea Sprite'),
    (select id from sets where short_name = '5ed'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ambush Party'),
    (select id from sets where short_name = '5ed'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crimson Manticore'),
    (select id from sets where short_name = '5ed'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghazbán Ogre'),
    (select id from sets where short_name = '5ed'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Amulet of Kroog'),
    (select id from sets where short_name = '5ed'),
    '347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Master'),
    (select id from sets where short_name = '5ed'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thicket Basilisk'),
    (select id from sets where short_name = '5ed'),
    '331',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feroz''s Ban'),
    (select id from sets where short_name = '5ed'),
    '370',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dance of Many'),
    (select id from sets where short_name = '5ed'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Weakness'),
    (select id from sets where short_name = '5ed'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enervate'),
    (select id from sets where short_name = '5ed'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primal Order'),
    (select id from sets where short_name = '5ed'),
    '318',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anti-Magic Aura'),
    (select id from sets where short_name = '5ed'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gauntlets of Chaos'),
    (select id from sets where short_name = '5ed'),
    '373',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mesa Pegasus'),
    (select id from sets where short_name = '5ed'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ivory Guardians'),
    (select id from sets where short_name = '5ed'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = '5ed'),
    '413',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Adarkar Wastes'),
    (select id from sets where short_name = '5ed'),
    '410',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Warrens'),
    (select id from sets where short_name = '5ed'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glacial Wall'),
    (select id from sets where short_name = '5ed'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drain Power'),
    (select id from sets where short_name = '5ed'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = '5ed'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Dead'),
    (select id from sets where short_name = '5ed'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'An-Havva Constable'),
    (select id from sets where short_name = '5ed'),
    '277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ley Druid'),
    (select id from sets where short_name = '5ed'),
    '308',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Wraith'),
    (select id from sets where short_name = '5ed'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Divine Transformation'),
    (select id from sets where short_name = '5ed'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Divine Offering'),
    (select id from sets where short_name = '5ed'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wyluli Wolf'),
    (select id from sets where short_name = '5ed'),
    '345',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatterstorm'),
    (select id from sets where short_name = '5ed'),
    '266',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Detonate'),
    (select id from sets where short_name = '5ed'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merfolk of the Pearl Trident'),
    (select id from sets where short_name = '5ed'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Energy Flux'),
    (select id from sets where short_name = '5ed'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '5ed'),
    '431',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = '5ed'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Spears'),
    (select id from sets where short_name = '5ed'),
    '407',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Avenger'),
    (select id from sets where short_name = '5ed'),
    '405',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ironclaw Orcs'),
    (select id from sets where short_name = '5ed'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Throne of Bone'),
    (select id from sets where short_name = '5ed'),
    '403',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = '5ed'),
    '313',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = '5ed'),
    '267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Johtull Wurm'),
    (select id from sets where short_name = '5ed'),
    '306',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sorceress Queen'),
    (select id from sets where short_name = '5ed'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evil Eye of Orms-by-Gore'),
    (select id from sets where short_name = '5ed'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sand Silos'),
    (select id from sets where short_name = '5ed'),
    '423',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fallen Angel'),
    (select id from sets where short_name = '5ed'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Durkwood Boars'),
    (select id from sets where short_name = '5ed'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Healing Salve'),
    (select id from sets where short_name = '5ed'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Caribou Range'),
    (select id from sets where short_name = '5ed'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inferno'),
    (select id from sets where short_name = '5ed'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grizzly Bears'),
    (select id from sets where short_name = '5ed'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = '5ed'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gloom'),
    (select id from sets where short_name = '5ed'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Killer Bees'),
    (select id from sets where short_name = '5ed'),
    '307',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bad Moon'),
    (select id from sets where short_name = '5ed'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '5ed'),
    '438',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ironroot Treefolk'),
    (select id from sets where short_name = '5ed'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = '5ed'),
    '427',
    'common'
) ,
(
    (select id from mtgcard where name = 'Binding Grasp'),
    (select id from sets where short_name = '5ed'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '5ed'),
    '430',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zephyr Falcon'),
    (select id from sets where short_name = '5ed'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jokulhaups'),
    (select id from sets where short_name = '5ed'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wanderlust'),
    (select id from sets where short_name = '5ed'),
    '339',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Iron Star'),
    (select id from sets where short_name = '5ed'),
    '379',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Icatian Town'),
    (select id from sets where short_name = '5ed'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ray of Command'),
    (select id from sets where short_name = '5ed'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dust to Dust'),
    (select id from sets where short_name = '5ed'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ironclaw Curse'),
    (select id from sets where short_name = '5ed'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brassclaw Orcs'),
    (select id from sets where short_name = '5ed'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '5ed'),
    '435',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mole Worms'),
    (select id from sets where short_name = '5ed'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pox'),
    (select id from sets where short_name = '5ed'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone Spirit'),
    (select id from sets where short_name = '5ed'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jade Monolith'),
    (select id from sets where short_name = '5ed'),
    '381',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spell Blast'),
    (select id from sets where short_name = '5ed'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashes to Ashes'),
    (select id from sets where short_name = '5ed'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Wretched'),
    (select id from sets where short_name = '5ed'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stream of Life'),
    (select id from sets where short_name = '5ed'),
    '328',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashes to Ashes'),
    (select id from sets where short_name = '5ed'),
    '141s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Murk Dwellers'),
    (select id from sets where short_name = '5ed'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Soldier'),
    (select id from sets where short_name = '5ed'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Maze'),
    (select id from sets where short_name = '5ed'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crown of the Ages'),
    (select id from sets where short_name = '5ed'),
    '360',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Artifacts'),
    (select id from sets where short_name = '5ed'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dwarven Hold'),
    (select id from sets where short_name = '5ed'),
    '414',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aether Storm'),
    (select id from sets where short_name = '5ed'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deflection'),
    (select id from sets where short_name = '5ed'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Updraft'),
    (select id from sets where short_name = '5ed'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akron Legionnaire'),
    (select id from sets where short_name = '5ed'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Undoing'),
    (select id from sets where short_name = '5ed'),
    '392',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '5ed'),
    '443',
    'common'
) ,
(
    (select id from mtgcard where name = 'Portent'),
    (select id from sets where short_name = '5ed'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lord of Atlantis'),
    (select id from sets where short_name = '5ed'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone Giant'),
    (select id from sets where short_name = '5ed'),
    '269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Homarid Warrior'),
    (select id from sets where short_name = '5ed'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Black'),
    (select id from sets where short_name = '5ed'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Broken Visage'),
    (select id from sets where short_name = '5ed'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fog'),
    (select id from sets where short_name = '5ed'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mons''s Goblin Raiders'),
    (select id from sets where short_name = '5ed'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shrink'),
    (select id from sets where short_name = '5ed'),
    '326',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seasinger'),
    (select id from sets where short_name = '5ed'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cockatrice'),
    (select id from sets where short_name = '5ed'),
    '284',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sulfurous Springs'),
    (select id from sets where short_name = '5ed'),
    '424',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Squatters'),
    (select id from sets where short_name = '5ed'),
    '258',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Bauble'),
    (select id from sets where short_name = '5ed'),
    '406',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Hive'),
    (select id from sets where short_name = '5ed'),
    '402',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necrite'),
    (select id from sets where short_name = '5ed'),
    '181s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = '5ed'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hydroblast'),
    (select id from sets where short_name = '5ed'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hungry Mist'),
    (select id from sets where short_name = '5ed'),
    '302',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = '5ed'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howling Mine'),
    (select id from sets where short_name = '5ed'),
    '377',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Living Artifact'),
    (select id from sets where short_name = '5ed'),
    '311',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fire Drake'),
    (select id from sets where short_name = '5ed'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Skycaptain'),
    (select id from sets where short_name = '5ed'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diabolic Machine'),
    (select id from sets where short_name = '5ed'),
    '363',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elven Riders'),
    (select id from sets where short_name = '5ed'),
    '291',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Order of the White Shield'),
    (select id from sets where short_name = '5ed'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Animate Wall'),
    (select id from sets where short_name = '5ed'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krovikan Fetish'),
    (select id from sets where short_name = '5ed'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Digging Team'),
    (select id from sets where short_name = '5ed'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Memory Lapse'),
    (select id from sets where short_name = '5ed'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Flare'),
    (select id from sets where short_name = '5ed'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Joven''s Tools'),
    (select id from sets where short_name = '5ed'),
    '386',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '5ed'),
    '448',
    'common'
) ,
(
    (select id from mtgcard where name = 'Millstone'),
    (select id from sets where short_name = '5ed'),
    '390',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Bone'),
    (select id from sets where short_name = '5ed'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deathgrip'),
    (select id from sets where short_name = '5ed'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sibilant Spirit'),
    (select id from sets where short_name = '5ed'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warp Artifact'),
    (select id from sets where short_name = '5ed'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alabaster Potion'),
    (select id from sets where short_name = '5ed'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = '5ed'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Game of Chaos'),
    (select id from sets where short_name = '5ed'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = '5ed'),
    '391',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stasis'),
    (select id from sets where short_name = '5ed'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Helm of Chatzuk'),
    (select id from sets where short_name = '5ed'),
    '376',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Funeral March'),
    (select id from sets where short_name = '5ed'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Untamed Wilds'),
    (select id from sets where short_name = '5ed'),
    '335',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Panic'),
    (select id from sets where short_name = '5ed'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Twiddle'),
    (select id from sets where short_name = '5ed'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dandân'),
    (select id from sets where short_name = '5ed'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grapeshot Catapult'),
    (select id from sets where short_name = '5ed'),
    '375',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin War Drums'),
    (select id from sets where short_name = '5ed'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shield Bearer'),
    (select id from sets where short_name = '5ed'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = '5ed'),
    '429',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keldon Warlord'),
    (select id from sets where short_name = '5ed'),
    '247',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azure Drake'),
    (select id from sets where short_name = '5ed'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unholy Strength'),
    (select id from sets where short_name = '5ed'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Knight'),
    (select id from sets where short_name = '5ed'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shapeshifter'),
    (select id from sets where short_name = '5ed'),
    '398',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heal'),
    (select id from sets where short_name = '5ed'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pentagram of the Ages'),
    (select id from sets where short_name = '5ed'),
    '394',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruins of Trokair'),
    (select id from sets where short_name = '5ed'),
    '422',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Artillery'),
    (select id from sets where short_name = '5ed'),
    '253',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Stone'),
    (select id from sets where short_name = '5ed'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra Paladin'),
    (select id from sets where short_name = '5ed'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leviathan'),
    (select id from sets where short_name = '5ed'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primordial Ooze'),
    (select id from sets where short_name = '5ed'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pestilence'),
    (select id from sets where short_name = '5ed'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Juxtapose'),
    (select id from sets where short_name = '5ed'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Colossus of Sardia'),
    (select id from sets where short_name = '5ed'),
    '358',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fountain of Youth'),
    (select id from sets where short_name = '5ed'),
    '372',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Strength'),
    (select id from sets where short_name = '5ed'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = '5ed'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scryb Sprites'),
    (select id from sets where short_name = '5ed'),
    '324',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greater Realm of Preservation'),
    (select id from sets where short_name = '5ed'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Lands'),
    (select id from sets where short_name = '5ed'),
    '312',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flare'),
    (select id from sets where short_name = '5ed'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Sink'),
    (select id from sets where short_name = '5ed'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = '5ed'),
    '227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Radjan Spirit'),
    (select id from sets where short_name = '5ed'),
    '320',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blessed Wine'),
    (select id from sets where short_name = '5ed'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lifetap'),
    (select id from sets where short_name = '5ed'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stromgald Cabal'),
    (select id from sets where short_name = '5ed'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blight'),
    (select id from sets where short_name = '5ed'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cat Warriors'),
    (select id from sets where short_name = '5ed'),
    '282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aysen Bureaucrats'),
    (select id from sets where short_name = '5ed'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Derelor'),
    (select id from sets where short_name = '5ed'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ebon Stronghold'),
    (select id from sets where short_name = '5ed'),
    '416',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nether Shadow'),
    (select id from sets where short_name = '5ed'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Repentant Blacksmith'),
    (select id from sets where short_name = '5ed'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desert Twister'),
    (select id from sets where short_name = '5ed'),
    '288',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Library'),
    (select id from sets where short_name = '5ed'),
    '329',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaseous Form'),
    (select id from sets where short_name = '5ed'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brothers of Fire'),
    (select id from sets where short_name = '5ed'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Personal Incarnation'),
    (select id from sets where short_name = '5ed'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Engine'),
    (select id from sets where short_name = '5ed'),
    '366',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smoke'),
    (select id from sets where short_name = '5ed'),
    '268',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Segovian Leviathan'),
    (select id from sets where short_name = '5ed'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Errantry'),
    (select id from sets where short_name = '5ed'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flame Spirit'),
    (select id from sets where short_name = '5ed'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hurkyl''s Recall'),
    (select id from sets where short_name = '5ed'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reverse Damage'),
    (select id from sets where short_name = '5ed'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wind Spirit'),
    (select id from sets where short_name = '5ed'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Righteousness'),
    (select id from sets where short_name = '5ed'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pirate Ship'),
    (select id from sets where short_name = '5ed'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knight of Stromgald'),
    (select id from sets where short_name = '5ed'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necrite'),
    (select id from sets where short_name = '5ed'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jalum Tome'),
    (select id from sets where short_name = '5ed'),
    '382',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = '5ed'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = '5ed'),
    '267†',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Force Spike'),
    (select id from sets where short_name = '5ed'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arenson''s Aura'),
    (select id from sets where short_name = '5ed'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marsh Viper'),
    (select id from sets where short_name = '5ed'),
    '315',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mindstab Thrull'),
    (select id from sets where short_name = '5ed'),
    '178s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sengir Autocrat'),
    (select id from sets where short_name = '5ed'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Swords'),
    (select id from sets where short_name = '5ed'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Royal Guard'),
    (select id from sets where short_name = '5ed'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Justice'),
    (select id from sets where short_name = '5ed'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dancing Scimitar'),
    (select id from sets where short_name = '5ed'),
    '362',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Castle'),
    (select id from sets where short_name = '5ed'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Archers'),
    (select id from sets where short_name = '5ed'),
    '292',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Underground River'),
    (select id from sets where short_name = '5ed'),
    '426',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mesa Falcon'),
    (select id from sets where short_name = '5ed'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Imp'),
    (select id from sets where short_name = '5ed'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magical Hack'),
    (select id from sets where short_name = '5ed'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantom Monster'),
    (select id from sets where short_name = '5ed'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dwarven Catapult'),
    (select id from sets where short_name = '5ed'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'War Mammoth'),
    (select id from sets where short_name = '5ed'),
    '340',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island Sanctuary'),
    (select id from sets where short_name = '5ed'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flying Carpet'),
    (select id from sets where short_name = '5ed'),
    '371',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tranquility'),
    (select id from sets where short_name = '5ed'),
    '333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prodigal Sorcerer'),
    (select id from sets where short_name = '5ed'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flight'),
    (select id from sets where short_name = '5ed'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karma'),
    (select id from sets where short_name = '5ed'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orgg'),
    (select id from sets where short_name = '5ed'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Initiates of the Ebon Hand'),
    (select id from sets where short_name = '5ed'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '5ed'),
    '434',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bottomless Vault'),
    (select id from sets where short_name = '5ed'),
    '411',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampire Bats'),
    (select id from sets where short_name = '5ed'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fear'),
    (select id from sets where short_name = '5ed'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Bestiary'),
    (select id from sets where short_name = '5ed'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plague Rats'),
    (select id from sets where short_name = '5ed'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = '5ed'),
    '428',
    'common'
) ,
(
    (select id from mtgcard where name = 'Firebreathing'),
    (select id from sets where short_name = '5ed'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Venom'),
    (select id from sets where short_name = '5ed'),
    '336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frozen Shade'),
    (select id from sets where short_name = '5ed'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Order of the Sacred Torch'),
    (select id from sets where short_name = '5ed'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inferno'),
    (select id from sets where short_name = '5ed'),
    '243†',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rag Man'),
    (select id from sets where short_name = '5ed'),
    '190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Instill Energy'),
    (select id from sets where short_name = '5ed'),
    '304',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '5ed'),
    '444',
    'common'
) ,
(
    (select id from mtgcard where name = 'Havenwood Battleground'),
    (select id from sets where short_name = '5ed'),
    '417',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pearled Unicorn'),
    (select id from sets where short_name = '5ed'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elkin Bottle'),
    (select id from sets where short_name = '5ed'),
    '367',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kismet'),
    (select id from sets where short_name = '5ed'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Library of Leng'),
    (select id from sets where short_name = '5ed'),
    '387',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tarpan'),
    (select id from sets where short_name = '5ed'),
    '330',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Warriors'),
    (select id from sets where short_name = '5ed'),
    '222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clockwork Steed'),
    (select id from sets where short_name = '5ed'),
    '357',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eye for an Eye'),
    (select id from sets where short_name = '5ed'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mindstab Thrull'),
    (select id from sets where short_name = '5ed'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Brute'),
    (select id from sets where short_name = '5ed'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'D''Avenant Archer'),
    (select id from sets where short_name = '5ed'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Verduran Enchantress'),
    (select id from sets where short_name = '5ed'),
    '337',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aladdin''s Ring'),
    (select id from sets where short_name = '5ed'),
    '346',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pradesh Gypsies'),
    (select id from sets where short_name = '5ed'),
    '317',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '5ed'),
    '437',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barbed Sextant'),
    (select id from sets where short_name = '5ed'),
    '351',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Scout'),
    (select id from sets where short_name = '5ed'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Air'),
    (select id from sets where short_name = '5ed'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Recall'),
    (select id from sets where short_name = '5ed'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin King'),
    (select id from sets where short_name = '5ed'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Erg Raiders'),
    (select id from sets where short_name = '5ed'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aurochs'),
    (select id from sets where short_name = '5ed'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Truce'),
    (select id from sets where short_name = '5ed'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Warp'),
    (select id from sets where short_name = '5ed'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Fire'),
    (select id from sets where short_name = '5ed'),
    '273',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = '5ed'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Barrier'),
    (select id from sets where short_name = '5ed'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jester''s Cap'),
    (select id from sets where short_name = '5ed'),
    '385',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea Serpent'),
    (select id from sets where short_name = '5ed'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '5ed'),
    '445',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ankh of Mishra'),
    (select id from sets where short_name = '5ed'),
    '348',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whirling Dervish'),
    (select id from sets where short_name = '5ed'),
    '341',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brushland'),
    (select id from sets where short_name = '5ed'),
    '412',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Icatian Store'),
    (select id from sets where short_name = '5ed'),
    '419',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skull Catapult'),
    (select id from sets where short_name = '5ed'),
    '399',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battering Ram'),
    (select id from sets where short_name = '5ed'),
    '353',
    'common'
) ,
(
    (select id from mtgcard where name = 'Force of Nature'),
    (select id from sets where short_name = '5ed'),
    '294',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Brambles'),
    (select id from sets where short_name = '5ed'),
    '338',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krovikan Sorcerer'),
    (select id from sets where short_name = '5ed'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = '5ed'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Altar'),
    (select id from sets where short_name = '5ed'),
    '349',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Foxfire'),
    (select id from sets where short_name = '5ed'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = '5ed'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sabretooth Tiger'),
    (select id from sets where short_name = '5ed'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barl''s Cage'),
    (select id from sets where short_name = '5ed'),
    '352',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clockwork Beast'),
    (select id from sets where short_name = '5ed'),
    '356',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scathe Zombies'),
    (select id from sets where short_name = '5ed'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boomerang'),
    (select id from sets where short_name = '5ed'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Samite Healer'),
    (select id from sets where short_name = '5ed'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Transmogrant'),
    (select id from sets where short_name = '5ed'),
    '350',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hurloon Minotaur'),
    (select id from sets where short_name = '5ed'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tawnos''s Weaponry'),
    (select id from sets where short_name = '5ed'),
    '401',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '5ed'),
    '433',
    'common'
) ,
(
    (select id from mtgcard where name = 'White Knight'),
    (select id from sets where short_name = '5ed'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shield Wall'),
    (select id from sets where short_name = '5ed'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Atog'),
    (select id from sets where short_name = '5ed'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bottle of Suleiman'),
    (select id from sets where short_name = '5ed'),
    '354',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hipparion'),
    (select id from sets where short_name = '5ed'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winter Blast'),
    (select id from sets where short_name = '5ed'),
    '343',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seraph'),
    (select id from sets where short_name = '5ed'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lure'),
    (select id from sets where short_name = '5ed'),
    '314',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wooden Sphere'),
    (select id from sets where short_name = '5ed'),
    '409',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Manabarbs'),
    (select id from sets where short_name = '5ed'),
    '250',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rabid Wombat'),
    (select id from sets where short_name = '5ed'),
    '319',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blinking Spirit'),
    (select id from sets where short_name = '5ed'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = '5ed'),
    '280',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Titania''s Song'),
    (select id from sets where short_name = '5ed'),
    '332',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Holy Strength'),
    (select id from sets where short_name = '5ed'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lifeforce'),
    (select id from sets where short_name = '5ed'),
    '310',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Labyrinth Minotaur'),
    (select id from sets where short_name = '5ed'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolverine Pack'),
    (select id from sets where short_name = '5ed'),
    '344',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Icatian Phalanx'),
    (select id from sets where short_name = '5ed'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lhurgoyf'),
    (select id from sets where short_name = '5ed'),
    '309',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = '5ed'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cave People'),
    (select id from sets where short_name = '5ed'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Farmer'),
    (select id from sets where short_name = '5ed'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infinite Hourglass'),
    (select id from sets where short_name = '5ed'),
    '378',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain Goat'),
    (select id from sets where short_name = '5ed'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forget'),
    (select id from sets where short_name = '5ed'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stampede'),
    (select id from sets where short_name = '5ed'),
    '327',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Xenic Poltergeist'),
    (select id from sets where short_name = '5ed'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Remove Soul'),
    (select id from sets where short_name = '5ed'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zur''s Weirding'),
    (select id from sets where short_name = '5ed'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pit Scorpion'),
    (select id from sets where short_name = '5ed'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Wraith'),
    (select id from sets where short_name = '5ed'),
    '147s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Captain'),
    (select id from sets where short_name = '5ed'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angry Mob'),
    (select id from sets where short_name = '5ed'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scavenger Folk'),
    (select id from sets where short_name = '5ed'),
    '323',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leshrac''s Rite'),
    (select id from sets where short_name = '5ed'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lost Soul'),
    (select id from sets where short_name = '5ed'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manabarbs'),
    (select id from sets where short_name = '5ed'),
    '250†',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boomerang'),
    (select id from sets where short_name = '5ed'),
    '75s',
    'common'
) 
 on conflict do nothing;
