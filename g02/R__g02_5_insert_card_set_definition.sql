insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Hammer of Bogardan'),
    (select id from sets where short_name = 'g02'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tradewind Rider'),
    (select id from sets where short_name = 'g02'),
    '2',
    'rare'
) 
 on conflict do nothing;
