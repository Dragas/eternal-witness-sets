insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ephemerate // Ephemerate'),
    (select id from sets where short_name = 'amh1'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravenous Giant // Ravenous Giant'),
    (select id from sets where short_name = 'amh1'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza, Lord High Artificer // Urza, Lord High Artificer'),
    (select id from sets where short_name = 'amh1'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Face of Divinity // Face of Divinity'),
    (select id from sets where short_name = 'amh1'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rebuild // Rebuild'),
    (select id from sets where short_name = 'amh1'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Morophon, the Boundless // Morophon, the Boundless'),
    (select id from sets where short_name = 'amh1'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lancer Sliver // Lancer Sliver'),
    (select id from sets where short_name = 'amh1'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Everdream // Everdream'),
    (select id from sets where short_name = 'amh1'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prismatic Vista // Prismatic Vista'),
    (select id from sets where short_name = 'amh1'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yawgmoth, Thran Physician // Yawgmoth, Thran Physician'),
    (select id from sets where short_name = 'amh1'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lesser Masticore // Lesser Masticore'),
    (select id from sets where short_name = 'amh1'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dregscape Sliver // Dregscape Sliver'),
    (select id from sets where short_name = 'amh1'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrenn and Six // Wrenn and Six'),
    (select id from sets where short_name = 'amh1'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Waterlogged Grove // Waterlogged Grove'),
    (select id from sets where short_name = 'amh1'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zhalfirin Decoy // Zhalfirin Decoy'),
    (select id from sets where short_name = 'amh1'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Endling // Endling'),
    (select id from sets where short_name = 'amh1'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ingenious Infiltrator // Ingenious Infiltrator'),
    (select id from sets where short_name = 'amh1'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Throes of Chaos // Throes of Chaos'),
    (select id from sets where short_name = 'amh1'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warteye Witch // Warteye Witch'),
    (select id from sets where short_name = 'amh1'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Headless Specter // Headless Specter'),
    (select id from sets where short_name = 'amh1'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smoke Shroud // Smoke Shroud'),
    (select id from sets where short_name = 'amh1'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Matron // Goblin Matron'),
    (select id from sets where short_name = 'amh1'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'First Sliver''s Chosen // First Sliver''s Chosen'),
    (select id from sets where short_name = 'amh1'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silent Clearing // Silent Clearing'),
    (select id from sets where short_name = 'amh1'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sisay, Weatherlight Captain // Sisay, Weatherlight Captain'),
    (select id from sets where short_name = 'amh1'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ranger-Captain of Eos // Ranger-Captain of Eos'),
    (select id from sets where short_name = 'amh1'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Skelemental // Lightning Skelemental'),
    (select id from sets where short_name = 'amh1'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chillerpillar // Chillerpillar'),
    (select id from sets where short_name = 'amh1'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcum''s Astrolabe // Arcum''s Astrolabe'),
    (select id from sets where short_name = 'amh1'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enduring Sliver // Enduring Sliver'),
    (select id from sets where short_name = 'amh1'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Force of Despair // Force of Despair'),
    (select id from sets where short_name = 'amh1'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scour All Possibilities // Scour All Possibilities'),
    (select id from sets where short_name = 'amh1'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ninja of the New Moon // Ninja of the New Moon'),
    (select id from sets where short_name = 'amh1'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra the Benevolent // Serra the Benevolent'),
    (select id from sets where short_name = 'amh1'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Rake // Mind Rake'),
    (select id from sets where short_name = 'amh1'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Umezawa''s Charm // Umezawa''s Charm'),
    (select id from sets where short_name = 'amh1'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spinehorn Minotaur // Spinehorn Minotaur'),
    (select id from sets where short_name = 'amh1'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirrodin Besieged // Mirrodin Besieged'),
    (select id from sets where short_name = 'amh1'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winds of Abandon // Winds of Abandon'),
    (select id from sets where short_name = 'amh1'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Echo of Eons // Echo of Eons'),
    (select id from sets where short_name = 'amh1'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sword of Truth and Justice // Sword of Truth and Justice'),
    (select id from sets where short_name = 'amh1'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ayula, Queen Among Bears // Ayula, Queen Among Bears'),
    (select id from sets where short_name = 'amh1'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Watcher for Tomorrow // Watcher for Tomorrow'),
    (select id from sets where short_name = 'amh1'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Throatseeker // Throatseeker'),
    (select id from sets where short_name = 'amh1'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of One Thousand Cuts // Wall of One Thousand Cuts'),
    (select id from sets where short_name = 'amh1'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Answered Prayers // Answered Prayers'),
    (select id from sets where short_name = 'amh1'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hogaak, Arisen Necropolis // Hogaak, Arisen Necropolis'),
    (select id from sets where short_name = 'amh1'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bazaar Trademage // Bazaar Trademage'),
    (select id from sets where short_name = 'amh1'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plague Engineer // Plague Engineer'),
    (select id from sets where short_name = 'amh1'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soulherder // Soulherder'),
    (select id from sets where short_name = 'amh1'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pashalik Mons // Pashalik Mons'),
    (select id from sets where short_name = 'amh1'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mox Tantalite // Mox Tantalite'),
    (select id from sets where short_name = 'amh1'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Excavating Anurid // Excavating Anurid'),
    (select id from sets where short_name = 'amh1'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sword of Sinew and Steel // Sword of Sinew and Steel'),
    (select id from sets where short_name = 'amh1'),
    '47',
    'common'
) 
 on conflict do nothing;
