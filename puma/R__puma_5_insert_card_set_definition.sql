insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Celestial Colonnade'),
    (select id from sets where short_name = 'puma'),
    'U33',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mana Vault'),
    (select id from sets where short_name = 'puma'),
    'U29',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Liliana of the Veil'),
    (select id from sets where short_name = 'puma'),
    'U10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Karn Liberated'),
    (select id from sets where short_name = 'puma'),
    'U2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bitterblossom'),
    (select id from sets where short_name = 'puma'),
    'U7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Life from the Loam'),
    (select id from sets where short_name = 'puma'),
    'U17',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ancient Tomb'),
    (select id from sets where short_name = 'puma'),
    'U31',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Demonic Tutor'),
    (select id from sets where short_name = 'puma'),
    'U8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goryo''s Vengeance'),
    (select id from sets where short_name = 'puma'),
    'U9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Emrakul, the Aeons Torn'),
    (select id from sets where short_name = 'puma'),
    'U1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Eternal Witness'),
    (select id from sets where short_name = 'puma'),
    'U16',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mikaeus, the Unhallowed'),
    (select id from sets where short_name = 'puma'),
    'U11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Raging Ravine'),
    (select id from sets where short_name = 'puma'),
    'U38',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Balefire Dragon'),
    (select id from sets where short_name = 'puma'),
    'U14',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Pulse'),
    (select id from sets where short_name = 'puma'),
    'U24',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stirring Wildwood'),
    (select id from sets where short_name = 'puma'),
    'U39',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sigarda, Host of Herons'),
    (select id from sets where short_name = 'puma'),
    'U25',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Through the Breach'),
    (select id from sets where short_name = 'puma'),
    'U15',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Engineered Explosives'),
    (select id from sets where short_name = 'puma'),
    'U28',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gaddock Teeg'),
    (select id from sets where short_name = 'puma'),
    'U21',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kitchen Finks'),
    (select id from sets where short_name = 'puma'),
    'U27',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Noble Hierarch'),
    (select id from sets where short_name = 'puma'),
    'U18',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Temporal Manipulation'),
    (select id from sets where short_name = 'puma'),
    'U6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lavaclaw Reaches'),
    (select id from sets where short_name = 'puma'),
    'U37',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Platinum Emperion'),
    (select id from sets where short_name = 'puma'),
    'U30',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tasigur, the Golden Fang'),
    (select id from sets where short_name = 'puma'),
    'U13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tarmogoyf'),
    (select id from sets where short_name = 'puma'),
    'U19',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vengevine'),
    (select id from sets where short_name = 'puma'),
    'U20',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lord of Extinction'),
    (select id from sets where short_name = 'puma'),
    'U23',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Reanimate'),
    (select id from sets where short_name = 'puma'),
    'U12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cavern of Souls'),
    (select id from sets where short_name = 'puma'),
    'U32',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ulamog, the Infinite Gyre'),
    (select id from sets where short_name = 'puma'),
    'U4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Leovold, Emissary of Trest'),
    (select id from sets where short_name = 'puma'),
    'U22',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fulminator Mage'),
    (select id from sets where short_name = 'puma'),
    'U26',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Creeping Tar Pit'),
    (select id from sets where short_name = 'puma'),
    'U34',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dark Depths'),
    (select id from sets where short_name = 'puma'),
    'U35',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Snapcaster Mage'),
    (select id from sets where short_name = 'puma'),
    'U5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kozilek, Butcher of Truth'),
    (select id from sets where short_name = 'puma'),
    'U3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Karakas'),
    (select id from sets where short_name = 'puma'),
    'U36',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Urborg, Tomb of Yawgmoth'),
    (select id from sets where short_name = 'puma'),
    'U40',
    'mythic'
) 
 on conflict do nothing;
