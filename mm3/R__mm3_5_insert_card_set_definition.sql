insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Sphinx''s Revelation'),
    (select id from sets where short_name = 'mm3'),
    '187',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mist Raven'),
    (select id from sets where short_name = 'mm3'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cackling Counterpart'),
    (select id from sets where short_name = 'mm3'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Path to Exile'),
    (select id from sets where short_name = 'mm3'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyknight Legionnaire'),
    (select id from sets where short_name = 'mm3'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cower in Fear'),
    (select id from sets where short_name = 'mm3'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seal of Primordium'),
    (select id from sets where short_name = 'mm3'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thundersong Trumpeter'),
    (select id from sets where short_name = 'mm3'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wingcrafter'),
    (select id from sets where short_name = 'mm3'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Broodmate Dragon'),
    (select id from sets where short_name = 'mm3'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Denial'),
    (select id from sets where short_name = 'mm3'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slime Molding'),
    (select id from sets where short_name = 'mm3'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talon Trooper'),
    (select id from sets where short_name = 'mm3'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Druid''s Deliverance'),
    (select id from sets where short_name = 'mm3'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Ransom'),
    (select id from sets where short_name = 'mm3'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyromancer Ascension'),
    (select id from sets where short_name = 'mm3'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cavern of Souls'),
    (select id from sets where short_name = 'mm3'),
    '232',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pitfall Trap'),
    (select id from sets where short_name = 'mm3'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simic Signet'),
    (select id from sets where short_name = 'mm3'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Might of Old Krosa'),
    (select id from sets where short_name = 'mm3'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Verdant Catacombs'),
    (select id from sets where short_name = 'mm3'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wayfaring Temple'),
    (select id from sets where short_name = 'mm3'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coiling Oracle'),
    (select id from sets where short_name = 'mm3'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battle-Rattle Shaman'),
    (select id from sets where short_name = 'mm3'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urbis Protector'),
    (select id from sets where short_name = 'mm3'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Revive'),
    (select id from sets where short_name = 'mm3'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Hookmaster'),
    (select id from sets where short_name = 'mm3'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rootborn Defenses'),
    (select id from sets where short_name = 'mm3'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul War Chant'),
    (select id from sets where short_name = 'mm3'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serum Visions'),
    (select id from sets where short_name = 'mm3'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mudbutton Torchrunner'),
    (select id from sets where short_name = 'mm3'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hellrider'),
    (select id from sets where short_name = 'mm3'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zur the Enchanter'),
    (select id from sets where short_name = 'mm3'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arachnus Web'),
    (select id from sets where short_name = 'mm3'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cyclonic Rift'),
    (select id from sets where short_name = 'mm3'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azorius Guildgate'),
    (select id from sets where short_name = 'mm3'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Shatter'),
    (select id from sets where short_name = 'mm3'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Falkenrath Noble'),
    (select id from sets where short_name = 'mm3'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aethermage''s Touch'),
    (select id from sets where short_name = 'mm3'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tarmogoyf'),
    (select id from sets where short_name = 'mm3'),
    '141',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Past in Flames'),
    (select id from sets where short_name = 'mm3'),
    '105',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Orzhov Signet'),
    (select id from sets where short_name = 'mm3'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Carnage Gladiator'),
    (select id from sets where short_name = 'mm3'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Séance'),
    (select id from sets where short_name = 'mm3'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mistmeadow Witch'),
    (select id from sets where short_name = 'mm3'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Electromancer'),
    (select id from sets where short_name = 'mm3'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sensor Splicer'),
    (select id from sets where short_name = 'mm3'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sundering Growth'),
    (select id from sets where short_name = 'mm3'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savage Lands'),
    (select id from sets where short_name = 'mm3'),
    '243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seal of Doom'),
    (select id from sets where short_name = 'mm3'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Entomber Exarch'),
    (select id from sets where short_name = 'mm3'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tattermunge Witch'),
    (select id from sets where short_name = 'mm3'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stoic Angel'),
    (select id from sets where short_name = 'mm3'),
    '190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dimir Guildgate'),
    (select id from sets where short_name = 'mm3'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vanish into Memory'),
    (select id from sets where short_name = 'mm3'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Grudge'),
    (select id from sets where short_name = 'mm3'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kathari Bomber'),
    (select id from sets where short_name = 'mm3'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tandem Lookout'),
    (select id from sets where short_name = 'mm3'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corpse Connoisseur'),
    (select id from sets where short_name = 'mm3'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slaughterhorn'),
    (select id from sets where short_name = 'mm3'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obzedat, Ghost Council'),
    (select id from sets where short_name = 'mm3'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Momentary Blink'),
    (select id from sets where short_name = 'mm3'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Traitorous Instinct'),
    (select id from sets where short_name = 'mm3'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Youthful Knight'),
    (select id from sets where short_name = 'mm3'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kraken Hatchling'),
    (select id from sets where short_name = 'mm3'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Assault'),
    (select id from sets where short_name = 'mm3'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burning-Tree Emissary'),
    (select id from sets where short_name = 'mm3'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opportunity'),
    (select id from sets where short_name = 'mm3'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gifts Ungiven'),
    (select id from sets where short_name = 'mm3'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruul Signet'),
    (select id from sets where short_name = 'mm3'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snapcaster Mage'),
    (select id from sets where short_name = 'mm3'),
    '50',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ulvenwald Tracker'),
    (select id from sets where short_name = 'mm3'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Niv-Mizzet, Dracogenius'),
    (select id from sets where short_name = 'mm3'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Banishing Stroke'),
    (select id from sets where short_name = 'mm3'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Signet'),
    (select id from sets where short_name = 'mm3'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giantbaiting'),
    (select id from sets where short_name = 'mm3'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Falkenrath Aristocrat'),
    (select id from sets where short_name = 'mm3'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spell Pierce'),
    (select id from sets where short_name = 'mm3'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crippling Chill'),
    (select id from sets where short_name = 'mm3'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Guide'),
    (select id from sets where short_name = 'mm3'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Summoning Trap'),
    (select id from sets where short_name = 'mm3'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyrewild Shaman'),
    (select id from sets where short_name = 'mm3'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scourge Devil'),
    (select id from sets where short_name = 'mm3'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Genesis'),
    (select id from sets where short_name = 'mm3'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Extractor Demon'),
    (select id from sets where short_name = 'mm3'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shimmering Grotto'),
    (select id from sets where short_name = 'mm3'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scavenging Ooze'),
    (select id from sets where short_name = 'mm3'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Familiar''s Ruse'),
    (select id from sets where short_name = 'mm3'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunhome Guildmage'),
    (select id from sets where short_name = 'mm3'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azorius Signet'),
    (select id from sets where short_name = 'mm3'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dinrova Horror'),
    (select id from sets where short_name = 'mm3'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Delirium Skeins'),
    (select id from sets where short_name = 'mm3'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Outrage'),
    (select id from sets where short_name = 'mm3'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boros Guildgate'),
    (select id from sets where short_name = 'mm3'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marsh Flats'),
    (select id from sets where short_name = 'mm3'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Guildgate'),
    (select id from sets where short_name = 'mm3'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Penumbra Spider'),
    (select id from sets where short_name = 'mm3'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wing Splicer'),
    (select id from sets where short_name = 'mm3'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abyssal Specter'),
    (select id from sets where short_name = 'mm3'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unburial Rites'),
    (select id from sets where short_name = 'mm3'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grafdigger''s Cage'),
    (select id from sets where short_name = 'mm3'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lone Missionary'),
    (select id from sets where short_name = 'mm3'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Shrine'),
    (select id from sets where short_name = 'mm3'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Guildgate'),
    (select id from sets where short_name = 'mm3'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mizzium Mortars'),
    (select id from sets where short_name = 'mm3'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Ranger'),
    (select id from sets where short_name = 'mm3'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Signet'),
    (select id from sets where short_name = 'mm3'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana of the Veil'),
    (select id from sets where short_name = 'mm3'),
    '76',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Temporal Mastery'),
    (select id from sets where short_name = 'mm3'),
    '54',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bone Splinters'),
    (select id from sets where short_name = 'mm3'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Damnation'),
    (select id from sets where short_name = 'mm3'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grixis Slavedriver'),
    (select id from sets where short_name = 'mm3'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ranger of Eos'),
    (select id from sets where short_name = 'mm3'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fists of Ironwood'),
    (select id from sets where short_name = 'mm3'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul Guildgate'),
    (select id from sets where short_name = 'mm3'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Agent of Masks'),
    (select id from sets where short_name = 'mm3'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ground Assault'),
    (select id from sets where short_name = 'mm3'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spike Jester'),
    (select id from sets where short_name = 'mm3'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Aristocrat'),
    (select id from sets where short_name = 'mm3'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baloth Cage Trap'),
    (select id from sets where short_name = 'mm3'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Signet'),
    (select id from sets where short_name = 'mm3'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thunderous Wrath'),
    (select id from sets where short_name = 'mm3'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fiery Justice'),
    (select id from sets where short_name = 'mm3'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seaside Citadel'),
    (select id from sets where short_name = 'mm3'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grasp of Phantoms'),
    (select id from sets where short_name = 'mm3'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakdos Guildgate'),
    (select id from sets where short_name = 'mm3'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desecration Demon'),
    (select id from sets where short_name = 'mm3'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Centaur Healer'),
    (select id from sets where short_name = 'mm3'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magma Jet'),
    (select id from sets where short_name = 'mm3'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terminate'),
    (select id from sets where short_name = 'mm3'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sin Collector'),
    (select id from sets where short_name = 'mm3'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harmonize'),
    (select id from sets where short_name = 'mm3'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Germination'),
    (select id from sets where short_name = 'mm3'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blood Moon'),
    (select id from sets where short_name = 'mm3'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stony Silence'),
    (select id from sets where short_name = 'mm3'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Augur of Bolas'),
    (select id from sets where short_name = 'mm3'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wort, the Raidmother'),
    (select id from sets where short_name = 'mm3'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Entreat the Angels'),
    (select id from sets where short_name = 'mm3'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Primal Command'),
    (select id from sets where short_name = 'mm3'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Venser, Shaper Savant'),
    (select id from sets where short_name = 'mm3'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Sky Swallower'),
    (select id from sets where short_name = 'mm3'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sever the Bloodline'),
    (select id from sets where short_name = 'mm3'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Call of the Herd'),
    (select id from sets where short_name = 'mm3'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mortician Beetle'),
    (select id from sets where short_name = 'mm3'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voice of Resurgence'),
    (select id from sets where short_name = 'mm3'),
    '200',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Urban Evolution'),
    (select id from sets where short_name = 'mm3'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Basilisk Collar'),
    (select id from sets where short_name = 'mm3'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Night Terrors'),
    (select id from sets where short_name = 'mm3'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Intangible Virtue'),
    (select id from sets where short_name = 'mm3'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blade Splicer'),
    (select id from sets where short_name = 'mm3'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Domri Rade'),
    (select id from sets where short_name = 'mm3'),
    '161',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Death''s Shadow'),
    (select id from sets where short_name = 'mm3'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terminus'),
    (select id from sets where short_name = 'mm3'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sprouting Thrinax'),
    (select id from sets where short_name = 'mm3'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pit Keeper'),
    (select id from sets where short_name = 'mm3'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thragtusk'),
    (select id from sets where short_name = 'mm3'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abrupt Decay'),
    (select id from sets where short_name = 'mm3'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Fodder'),
    (select id from sets where short_name = 'mm3'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcane Sanctum'),
    (select id from sets where short_name = 'mm3'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Anthem'),
    (select id from sets where short_name = 'mm3'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dynacharge'),
    (select id from sets where short_name = 'mm3'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Charm'),
    (select id from sets where short_name = 'mm3'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deputy of Acquittals'),
    (select id from sets where short_name = 'mm3'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moroii'),
    (select id from sets where short_name = 'mm3'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cruel Ultimatum'),
    (select id from sets where short_name = 'mm3'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Frost'),
    (select id from sets where short_name = 'mm3'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Torrent of Souls'),
    (select id from sets where short_name = 'mm3'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grisly Spectacle'),
    (select id from sets where short_name = 'mm3'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Olivia Voldaren'),
    (select id from sets where short_name = 'mm3'),
    '177',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Soul Manipulation'),
    (select id from sets where short_name = 'mm3'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teleportal'),
    (select id from sets where short_name = 'mm3'),
    '193',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Restoration Angel'),
    (select id from sets where short_name = 'mm3'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystical Teachings'),
    (select id from sets where short_name = 'mm3'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Advent of the Wurm'),
    (select id from sets where short_name = 'mm3'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Recover'),
    (select id from sets where short_name = 'mm3'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Madcap Skills'),
    (select id from sets where short_name = 'mm3'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyroclasm'),
    (select id from sets where short_name = 'mm3'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lingering Souls'),
    (select id from sets where short_name = 'mm3'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arachnus Spinner'),
    (select id from sets where short_name = 'mm3'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rhox War Monk'),
    (select id from sets where short_name = 'mm3'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orzhov Guildgate'),
    (select id from sets where short_name = 'mm3'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strength in Numbers'),
    (select id from sets where short_name = 'mm3'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aethertow'),
    (select id from sets where short_name = 'mm3'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghor-Clan Rampager'),
    (select id from sets where short_name = 'mm3'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Craterhoof Behemoth'),
    (select id from sets where short_name = 'mm3'),
    '122',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Linvala, Keeper of Silence'),
    (select id from sets where short_name = 'mm3'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forbidden Alchemy'),
    (select id from sets where short_name = 'mm3'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pilfered Plans'),
    (select id from sets where short_name = 'mm3'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Misty Rainforest'),
    (select id from sets where short_name = 'mm3'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampire Nighthawk'),
    (select id from sets where short_name = 'mm3'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selesnya Guildgate'),
    (select id from sets where short_name = 'mm3'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Explore'),
    (select id from sets where short_name = 'mm3'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scorched Rusalka'),
    (select id from sets where short_name = 'mm3'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Agony Warp'),
    (select id from sets where short_name = 'mm3'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bronzebeak Moa'),
    (select id from sets where short_name = 'mm3'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hungry Spriggan'),
    (select id from sets where short_name = 'mm3'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arid Mesa'),
    (select id from sets where short_name = 'mm3'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Signet'),
    (select id from sets where short_name = 'mm3'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Reckoner'),
    (select id from sets where short_name = 'mm3'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Griselbrand'),
    (select id from sets where short_name = 'mm3'),
    '72',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bonfire of the Damned'),
    (select id from sets where short_name = 'mm3'),
    '91',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Woolly Thoctar'),
    (select id from sets where short_name = 'mm3'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vital Splicer'),
    (select id from sets where short_name = 'mm3'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thornscape Battlemage'),
    (select id from sets where short_name = 'mm3'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Rotwurm'),
    (select id from sets where short_name = 'mm3'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death-Hood Cobra'),
    (select id from sets where short_name = 'mm3'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Guildgate'),
    (select id from sets where short_name = 'mm3'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Compulsive Research'),
    (select id from sets where short_name = 'mm3'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sedraxis Specter'),
    (select id from sets where short_name = 'mm3'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kor Skyfisher'),
    (select id from sets where short_name = 'mm3'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Damping Matrix'),
    (select id from sets where short_name = 'mm3'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skirsdag Cultist'),
    (select id from sets where short_name = 'mm3'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Izzet Signet'),
    (select id from sets where short_name = 'mm3'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Warden'),
    (select id from sets where short_name = 'mm3'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ogre Jailbreaker'),
    (select id from sets where short_name = 'mm3'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gift of Orzhova'),
    (select id from sets where short_name = 'mm3'),
    '209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Graceful Reprieve'),
    (select id from sets where short_name = 'mm3'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dregscape Zombie'),
    (select id from sets where short_name = 'mm3'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hanweir Lancer'),
    (select id from sets where short_name = 'mm3'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vithian Stinger'),
    (select id from sets where short_name = 'mm3'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mogg Flunkies'),
    (select id from sets where short_name = 'mm3'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flickerwisp'),
    (select id from sets where short_name = 'mm3'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gideon''s Lawkeeper'),
    (select id from sets where short_name = 'mm3'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zealous Conscripts'),
    (select id from sets where short_name = 'mm3'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eyes in the Skies'),
    (select id from sets where short_name = 'mm3'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Oracle'),
    (select id from sets where short_name = 'mm3'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Call of the Conclave'),
    (select id from sets where short_name = 'mm3'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azure Mage'),
    (select id from sets where short_name = 'mm3'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wake the Reflections'),
    (select id from sets where short_name = 'mm3'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Image'),
    (select id from sets where short_name = 'mm3'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inquisition of Kozilek'),
    (select id from sets where short_name = 'mm3'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avacyn''s Pilgrim'),
    (select id from sets where short_name = 'mm3'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Attended Knight'),
    (select id from sets where short_name = 'mm3'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molten Rain'),
    (select id from sets where short_name = 'mm3'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rewind'),
    (select id from sets where short_name = 'mm3'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boros Signet'),
    (select id from sets where short_name = 'mm3'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rubblebelt Maaka'),
    (select id from sets where short_name = 'mm3'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tower Gargoyle'),
    (select id from sets where short_name = 'mm3'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scalding Tarn'),
    (select id from sets where short_name = 'mm3'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deadeye Navigator'),
    (select id from sets where short_name = 'mm3'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Master Splicer'),
    (select id from sets where short_name = 'mm3'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spire Monitor'),
    (select id from sets where short_name = 'mm3'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evil Twin'),
    (select id from sets where short_name = 'mm3'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gnawing Zombie'),
    (select id from sets where short_name = 'mm3'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crumbling Necropolis'),
    (select id from sets where short_name = 'mm3'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghostly Flicker'),
    (select id from sets where short_name = 'mm3'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unflinching Courage'),
    (select id from sets where short_name = 'mm3'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Putrefy'),
    (select id from sets where short_name = 'mm3'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Auger Spree'),
    (select id from sets where short_name = 'mm3'),
    '151',
    'common'
) 
 on conflict do nothing;
