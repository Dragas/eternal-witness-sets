insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'evg'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Messenger'),
    (select id from sets where short_name = 'evg'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Sledder'),
    (select id from sets where short_name = 'evg'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wirewood Lodge'),
    (select id from sets where short_name = 'evg'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mogg Fanatic'),
    (select id from sets where short_name = 'evg'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forgotten Cave'),
    (select id from sets where short_name = 'evg'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wildsize'),
    (select id from sets where short_name = 'evg'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wirewood Symbiote'),
    (select id from sets where short_name = 'evg'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Ringleader'),
    (select id from sets where short_name = 'evg'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Akki Coalflinger'),
    (select id from sets where short_name = 'evg'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Promenade'),
    (select id from sets where short_name = 'evg'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tar Pitcher'),
    (select id from sets where short_name = 'evg'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mudbutton Torchrunner'),
    (select id from sets where short_name = 'evg'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flamewave Invoker'),
    (select id from sets where short_name = 'evg'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'evg'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Imperious Perfect'),
    (select id from sets where short_name = 'evg'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clickslither'),
    (select id from sets where short_name = 'evg'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raging Goblin'),
    (select id from sets where short_name = 'evg'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moonglove Extract'),
    (select id from sets where short_name = 'evg'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stonewood Invoker'),
    (select id from sets where short_name = 'evg'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spitting Earth'),
    (select id from sets where short_name = 'evg'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'evg'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gempalm Strider'),
    (select id from sets where short_name = 'evg'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Warchief'),
    (select id from sets where short_name = 'evg'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ambush Commander'),
    (select id from sets where short_name = 'evg'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tarfire'),
    (select id from sets where short_name = 'evg'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Timberwatch Elf'),
    (select id from sets where short_name = 'evg'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tranquil Thicket'),
    (select id from sets where short_name = 'evg'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Harbinger'),
    (select id from sets where short_name = 'evg'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gempalm Incinerator'),
    (select id from sets where short_name = 'evg'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'evg'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slate of Ancestry'),
    (select id from sets where short_name = 'evg'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'evg'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mogg War Marshal'),
    (select id from sets where short_name = 'evg'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Matron'),
    (select id from sets where short_name = 'evg'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Allosaurus Rider'),
    (select id from sets where short_name = 'evg'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voice of the Woods'),
    (select id from sets where short_name = 'evg'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'evg'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emberwilde Augur'),
    (select id from sets where short_name = 'evg'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harmonize'),
    (select id from sets where short_name = 'evg'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skirk Prospector'),
    (select id from sets where short_name = 'evg'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skirk Fire Marshal'),
    (select id from sets where short_name = 'evg'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Siege-Gang Commander'),
    (select id from sets where short_name = 'evg'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boggart Shenanigans'),
    (select id from sets where short_name = 'evg'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wirewood Herald'),
    (select id from sets where short_name = 'evg'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lys Alana Huntmaster'),
    (select id from sets where short_name = 'evg'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'evg'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Warrior'),
    (select id from sets where short_name = 'evg'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'evg'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skirk Drill Sergeant'),
    (select id from sets where short_name = 'evg'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Burrows'),
    (select id from sets where short_name = 'evg'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wren''s Run Vanquisher'),
    (select id from sets where short_name = 'evg'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'evg'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Eulogist'),
    (select id from sets where short_name = 'evg'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heedless One'),
    (select id from sets where short_name = 'evg'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reckless One'),
    (select id from sets where short_name = 'evg'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'evg'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ib Halfheart, Goblin Tactician'),
    (select id from sets where short_name = 'evg'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skirk Shaman'),
    (select id from sets where short_name = 'evg'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wood Elves'),
    (select id from sets where short_name = 'evg'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Cohort'),
    (select id from sets where short_name = 'evg'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wellwisher'),
    (select id from sets where short_name = 'evg'),
    '15',
    'common'
) 
 on conflict do nothing;
