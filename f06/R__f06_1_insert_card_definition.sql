    insert into mtgcard(name) values ('Fire // Ice') on conflict do nothing;
    insert into mtgcard(name) values ('Wild Mongrel') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Warchief') on conflict do nothing;
    insert into mtgcard(name) values ('Astral Slide') on conflict do nothing;
    insert into mtgcard(name) values ('Circular Logic') on conflict do nothing;
    insert into mtgcard(name) values ('Life // Death') on conflict do nothing;
    insert into mtgcard(name) values ('Lobotomy') on conflict do nothing;
    insert into mtgcard(name) values ('Terminate') on conflict do nothing;
    insert into mtgcard(name) values ('Armadillo Cloak') on conflict do nothing;
    insert into mtgcard(name) values ('Arrogant Wurm') on conflict do nothing;
    insert into mtgcard(name) values ('Chainer''s Edict') on conflict do nothing;
    insert into mtgcard(name) values ('Elves of Deep Shadow') on conflict do nothing;
