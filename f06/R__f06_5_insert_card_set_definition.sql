insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Fire // Ice'),
    (select id from sets where short_name = 'f06'),
    '12a',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wild Mongrel'),
    (select id from sets where short_name = 'f06'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Warchief'),
    (select id from sets where short_name = 'f06'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Astral Slide'),
    (select id from sets where short_name = 'f06'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circular Logic'),
    (select id from sets where short_name = 'f06'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Life // Death'),
    (select id from sets where short_name = 'f06'),
    '11a',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lobotomy'),
    (select id from sets where short_name = 'f06'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terminate'),
    (select id from sets where short_name = 'f06'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armadillo Cloak'),
    (select id from sets where short_name = 'f06'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arrogant Wurm'),
    (select id from sets where short_name = 'f06'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chainer''s Edict'),
    (select id from sets where short_name = 'f06'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elves of Deep Shadow'),
    (select id from sets where short_name = 'f06'),
    '1',
    'rare'
) 
 on conflict do nothing;
