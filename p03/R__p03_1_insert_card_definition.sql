    insert into mtgcard(name) values ('Demon') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin') on conflict do nothing;
    insert into mtgcard(name) values ('Voidmage Prodigy') on conflict do nothing;
    insert into mtgcard(name) values ('Bear') on conflict do nothing;
    insert into mtgcard(name) values ('Insect') on conflict do nothing;
    insert into mtgcard(name) values ('Rukh') on conflict do nothing;
    insert into mtgcard(name) values ('Sliver') on conflict do nothing;
