insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Demon'),
    (select id from sets where short_name = 'p03'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'p03'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voidmage Prodigy'),
    (select id from sets where short_name = 'p03'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bear'),
    (select id from sets where short_name = 'p03'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insect'),
    (select id from sets where short_name = 'p03'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rukh'),
    (select id from sets where short_name = 'p03'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sliver'),
    (select id from sets where short_name = 'p03'),
    '3',
    'common'
) 
 on conflict do nothing;
