insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Gilded Light'),
    (select id from sets where short_name = 'scg'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Tyrant'),
    (select id from sets where short_name = 'scg'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tendrils of Agony'),
    (select id from sets where short_name = 'scg'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eternal Dragon'),
    (select id from sets where short_name = 'scg'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trap Digger'),
    (select id from sets where short_name = 'scg'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Final Punishment'),
    (select id from sets where short_name = 'scg'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dispersal Shield'),
    (select id from sets where short_name = 'scg'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divergent Growth'),
    (select id from sets where short_name = 'scg'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Shadow'),
    (select id from sets where short_name = 'scg'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Consumptive Goo'),
    (select id from sets where short_name = 'scg'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faces of the Past'),
    (select id from sets where short_name = 'scg'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Aberration'),
    (select id from sets where short_name = 'scg'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Breath'),
    (select id from sets where short_name = 'scg'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daru Spiritualist'),
    (select id from sets where short_name = 'scg'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frontline Strategist'),
    (select id from sets where short_name = 'scg'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zealous Inquisitor'),
    (select id from sets where short_name = 'scg'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Form of the Dragon'),
    (select id from sets where short_name = 'scg'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Upwelling'),
    (select id from sets where short_name = 'scg'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'One with Nature'),
    (select id from sets where short_name = 'scg'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cabal Interrogator'),
    (select id from sets where short_name = 'scg'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daru Warchief'),
    (select id from sets where short_name = 'scg'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Accelerated Mutation'),
    (select id from sets where short_name = 'scg'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Extra Arms'),
    (select id from sets where short_name = 'scg'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bonethorn Valesk'),
    (select id from sets where short_name = 'scg'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Call to the Grave'),
    (select id from sets where short_name = 'scg'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bladewing the Risen'),
    (select id from sets where short_name = 'scg'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unspeakable Symbol'),
    (select id from sets where short_name = 'scg'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stifle'),
    (select id from sets where short_name = 'scg'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treetop Scout'),
    (select id from sets where short_name = 'scg'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Day of the Dragons'),
    (select id from sets where short_name = 'scg'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sprouting Vines'),
    (select id from sets where short_name = 'scg'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancient Ooze'),
    (select id from sets where short_name = 'scg'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sliver Overlord'),
    (select id from sets where short_name = 'scg'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind''s Desire'),
    (select id from sets where short_name = 'scg'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Titanic Bulvox'),
    (select id from sets where short_name = 'scg'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rain of Blades'),
    (select id from sets where short_name = 'scg'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krosan Warchief'),
    (select id from sets where short_name = 'scg'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Twisted Abomination'),
    (select id from sets where short_name = 'scg'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Farseer'),
    (select id from sets where short_name = 'scg'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skulltap'),
    (select id from sets where short_name = 'scg'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wirewood Symbiote'),
    (select id from sets where short_name = 'scg'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forgotten Ancient'),
    (select id from sets where short_name = 'scg'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonspeaker Shaman'),
    (select id from sets where short_name = 'scg'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woodcloaker'),
    (select id from sets where short_name = 'scg'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Brigand'),
    (select id from sets where short_name = 'scg'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragonstorm'),
    (select id from sets where short_name = 'scg'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Root Elemental'),
    (select id from sets where short_name = 'scg'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Psychopath'),
    (select id from sets where short_name = 'scg'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kurgadon'),
    (select id from sets where short_name = 'scg'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fatal Mutation'),
    (select id from sets where short_name = 'scg'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragonstalker'),
    (select id from sets where short_name = 'scg'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyrostatic Pillar'),
    (select id from sets where short_name = 'scg'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thundercloud Elemental'),
    (select id from sets where short_name = 'scg'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brain Freeze'),
    (select id from sets where short_name = 'scg'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Decree of Justice'),
    (select id from sets where short_name = 'scg'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Astral Steel'),
    (select id from sets where short_name = 'scg'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rush of Knowledge'),
    (select id from sets where short_name = 'scg'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wing Shards'),
    (select id from sets where short_name = 'scg'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Warchief'),
    (select id from sets where short_name = 'scg'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guilty Conscience'),
    (select id from sets where short_name = 'scg'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Xantid Swarm'),
    (select id from sets where short_name = 'scg'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noble Templar'),
    (select id from sets where short_name = 'scg'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chartooth Cougar'),
    (select id from sets where short_name = 'scg'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coast Watcher'),
    (select id from sets where short_name = 'scg'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carbonize'),
    (select id from sets where short_name = 'scg'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nefashu'),
    (select id from sets where short_name = 'scg'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clutch of Undeath'),
    (select id from sets where short_name = 'scg'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Decree of Annihilation'),
    (select id from sets where short_name = 'scg'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Force Bubble'),
    (select id from sets where short_name = 'scg'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ambush Commander'),
    (select id from sets where short_name = 'scg'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exiled Doomsayer'),
    (select id from sets where short_name = 'scg'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Misguided Rage'),
    (select id from sets where short_name = 'scg'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riptide Survivor'),
    (select id from sets where short_name = 'scg'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reaping the Graves'),
    (select id from sets where short_name = 'scg'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Wings'),
    (select id from sets where short_name = 'scg'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Mage'),
    (select id from sets where short_name = 'scg'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sulfuric Vortex'),
    (select id from sets where short_name = 'scg'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vengeful Dead'),
    (select id from sets where short_name = 'scg'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aphetto Runecaster'),
    (select id from sets where short_name = 'scg'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Long-Term Plans'),
    (select id from sets where short_name = 'scg'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Carrion Feeder'),
    (select id from sets where short_name = 'scg'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death''s-Head Buzzard'),
    (select id from sets where short_name = 'scg'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunting Pack'),
    (select id from sets where short_name = 'scg'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skirk Volcanist'),
    (select id from sets where short_name = 'scg'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Scales'),
    (select id from sets where short_name = 'scg'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rock Jockey'),
    (select id from sets where short_name = 'scg'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chill Haunting'),
    (select id from sets where short_name = 'scg'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Putrid Raptor'),
    (select id from sets where short_name = 'scg'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karona''s Zealot'),
    (select id from sets where short_name = 'scg'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Siege-Gang Commander'),
    (select id from sets where short_name = 'scg'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silver Knight'),
    (select id from sets where short_name = 'scg'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Fangs'),
    (select id from sets where short_name = 'scg'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temporal Fissure'),
    (select id from sets where short_name = 'scg'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raven Guild Master'),
    (select id from sets where short_name = 'scg'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frozen Solid'),
    (select id from sets where short_name = 'scg'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pemmin''s Aura'),
    (select id from sets where short_name = 'scg'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Metamorphose'),
    (select id from sets where short_name = 'scg'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Recuperate'),
    (select id from sets where short_name = 'scg'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistform Warchief'),
    (select id from sets where short_name = 'scg'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mercurial Kite'),
    (select id from sets where short_name = 'scg'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Liberator'),
    (select id from sets where short_name = 'scg'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stabilizer'),
    (select id from sets where short_name = 'scg'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Claws of Wirewood'),
    (select id from sets where short_name = 'scg'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unburden'),
    (select id from sets where short_name = 'scg'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raven Guild Initiate'),
    (select id from sets where short_name = 'scg'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Edgewalker'),
    (select id from sets where short_name = 'scg'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ageless Sentinels'),
    (select id from sets where short_name = 'scg'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scattershot'),
    (select id from sets where short_name = 'scg'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wipe Clean'),
    (select id from sets where short_name = 'scg'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Proteus Machine'),
    (select id from sets where short_name = 'scg'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fierce Empath'),
    (select id from sets where short_name = 'scg'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enrage'),
    (select id from sets where short_name = 'scg'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Temple of the False God'),
    (select id from sets where short_name = 'scg'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Parallel Thoughts'),
    (select id from sets where short_name = 'scg'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uncontrolled Infestation'),
    (select id from sets where short_name = 'scg'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hindering Touch'),
    (select id from sets where short_name = 'scg'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mischievous Quanar'),
    (select id from sets where short_name = 'scg'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin War Strike'),
    (select id from sets where short_name = 'scg'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karona, False God'),
    (select id from sets where short_name = 'scg'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grip of Chaos'),
    (select id from sets where short_name = 'scg'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reward the Faithful'),
    (select id from sets where short_name = 'scg'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Decree of Pain'),
    (select id from sets where short_name = 'scg'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Decree of Savagery'),
    (select id from sets where short_name = 'scg'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Undead Warchief'),
    (select id from sets where short_name = 'scg'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wirewood Guardian'),
    (select id from sets where short_name = 'scg'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krosan Drover'),
    (select id from sets where short_name = 'scg'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primitive Etchings'),
    (select id from sets where short_name = 'scg'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cabal Conditioning'),
    (select id from sets where short_name = 'scg'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Collector'),
    (select id from sets where short_name = 'scg'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shoreline Ranger'),
    (select id from sets where short_name = 'scg'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bladewing''s Thrall'),
    (select id from sets where short_name = 'scg'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lingering Death'),
    (select id from sets where short_name = 'scg'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dimensional Breach'),
    (select id from sets where short_name = 'scg'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Decree of Silence'),
    (select id from sets where short_name = 'scg'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ark of Blight'),
    (select id from sets where short_name = 'scg'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spark Spray'),
    (select id from sets where short_name = 'scg'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lethal Vapors'),
    (select id from sets where short_name = 'scg'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Break Asunder'),
    (select id from sets where short_name = 'scg'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alpha Status'),
    (select id from sets where short_name = 'scg'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dawn Elemental'),
    (select id from sets where short_name = 'scg'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zombie Cutthroat'),
    (select id from sets where short_name = 'scg'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scornful Egotist'),
    (select id from sets where short_name = 'scg'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Torrent of Fire'),
    (select id from sets where short_name = 'scg'),
    '107',
    'common'
) 
 on conflict do nothing;
