insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Chainer''s Edict'),
    (select id from sets where short_name = 'tor'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deep Analysis'),
    (select id from sets where short_name = 'tor'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frantic Purification'),
    (select id from sets where short_name = 'tor'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hydromorph Gull'),
    (select id from sets where short_name = 'tor'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krosan Restorer'),
    (select id from sets where short_name = 'tor'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crackling Club'),
    (select id from sets where short_name = 'tor'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crippling Fatigue'),
    (select id from sets where short_name = 'tor'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slithery Stalker'),
    (select id from sets where short_name = 'tor'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hypnox'),
    (select id from sets where short_name = 'tor'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arrogant Wurm'),
    (select id from sets where short_name = 'tor'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aquamoeba'),
    (select id from sets where short_name = 'tor'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liquify'),
    (select id from sets where short_name = 'tor'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teroh''s Vanguard'),
    (select id from sets where short_name = 'tor'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Basking Rootwalla'),
    (select id from sets where short_name = 'tor'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Putrid Imp'),
    (select id from sets where short_name = 'tor'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pardic Arsonist'),
    (select id from sets where short_name = 'tor'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mortal Combat'),
    (select id from sets where short_name = 'tor'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anurid Scavenger'),
    (select id from sets where short_name = 'tor'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = 'tor'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rancid Earth'),
    (select id from sets where short_name = 'tor'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cephalid Vandal'),
    (select id from sets where short_name = 'tor'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Radiate'),
    (select id from sets where short_name = 'tor'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boneshard Slasher'),
    (select id from sets where short_name = 'tor'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cabal Surgeon'),
    (select id from sets where short_name = 'tor'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cabal Coffers'),
    (select id from sets where short_name = 'tor'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Compulsion'),
    (select id from sets where short_name = 'tor'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nantuko Shade'),
    (select id from sets where short_name = 'tor'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carrion Rats'),
    (select id from sets where short_name = 'tor'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Equal Treatment'),
    (select id from sets where short_name = 'tor'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Major Teroh'),
    (select id from sets where short_name = 'tor'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Acorn Harvest'),
    (select id from sets where short_name = 'tor'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plagiarize'),
    (select id from sets where short_name = 'tor'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cephalid Snitch'),
    (select id from sets where short_name = 'tor'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hypochondria'),
    (select id from sets where short_name = 'tor'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Sludge'),
    (select id from sets where short_name = 'tor'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Carrion Wurm'),
    (select id from sets where short_name = 'tor'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kamahl''s Sledge'),
    (select id from sets where short_name = 'tor'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'False Memories'),
    (select id from sets where short_name = 'tor'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flash of Defiance'),
    (select id from sets where short_name = 'tor'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chainer, Dementia Master'),
    (select id from sets where short_name = 'tor'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faceless Butcher'),
    (select id from sets where short_name = 'tor'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hydromorph Guardian'),
    (select id from sets where short_name = 'tor'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temporary Insanity'),
    (select id from sets where short_name = 'tor'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nantuko Blightcutter'),
    (select id from sets where short_name = 'tor'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cephalid Sage'),
    (select id from sets where short_name = 'tor'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stern Judge'),
    (select id from sets where short_name = 'tor'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shade''s Form'),
    (select id from sets where short_name = 'tor'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vengeful Dreams'),
    (select id from sets where short_name = 'tor'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Insist'),
    (select id from sets where short_name = 'tor'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Scourge'),
    (select id from sets where short_name = 'tor'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Floating Shield'),
    (select id from sets where short_name = 'tor'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tainted Peak'),
    (select id from sets where short_name = 'tor'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cabal Ritual'),
    (select id from sets where short_name = 'tor'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Possessed Barbarian'),
    (select id from sets where short_name = 'tor'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghostly Wings'),
    (select id from sets where short_name = 'tor'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Possessed Nomad'),
    (select id from sets where short_name = 'tor'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grim Lavamancer'),
    (select id from sets where short_name = 'tor'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crazed Firecat'),
    (select id from sets where short_name = 'tor'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Possessed Centaur'),
    (select id from sets where short_name = 'tor'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mutilate'),
    (select id from sets where short_name = 'tor'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Organ Grinder'),
    (select id from sets where short_name = 'tor'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Restless Dreams'),
    (select id from sets where short_name = 'tor'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alter Reality'),
    (select id from sets where short_name = 'tor'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Accelerate'),
    (select id from sets where short_name = 'tor'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyromania'),
    (select id from sets where short_name = 'tor'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grotesque Hybrid'),
    (select id from sets where short_name = 'tor'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nostalgic Dreams'),
    (select id from sets where short_name = 'tor'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seton''s Scout'),
    (select id from sets where short_name = 'tor'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Parallel Evolution'),
    (select id from sets where short_name = 'tor'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Laquatus''s Champion'),
    (select id from sets where short_name = 'tor'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Petradon'),
    (select id from sets where short_name = 'tor'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tainted Field'),
    (select id from sets where short_name = 'tor'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Strength of Isolation'),
    (select id from sets where short_name = 'tor'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dwell on the Past'),
    (select id from sets where short_name = 'tor'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Waste Away'),
    (select id from sets where short_name = 'tor'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obsessive Search'),
    (select id from sets where short_name = 'tor'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Trooper'),
    (select id from sets where short_name = 'tor'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Retribution'),
    (select id from sets where short_name = 'tor'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hell-Bent Raider'),
    (select id from sets where short_name = 'tor'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stupefying Touch'),
    (select id from sets where short_name = 'tor'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zombie Trailblazer'),
    (select id from sets where short_name = 'tor'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Transcendence'),
    (select id from sets where short_name = 'tor'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Militant Monk'),
    (select id from sets where short_name = 'tor'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ambassador Laquatus'),
    (select id from sets where short_name = 'tor'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skullscorch'),
    (select id from sets where short_name = 'tor'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Psychotic Haze'),
    (select id from sets where short_name = 'tor'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fiery Temper'),
    (select id from sets where short_name = 'tor'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skywing Aven'),
    (select id from sets where short_name = 'tor'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sickening Dreams'),
    (select id from sets where short_name = 'tor'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pay No Heed'),
    (select id from sets where short_name = 'tor'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ichorid'),
    (select id from sets where short_name = 'tor'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sonic Seizure'),
    (select id from sets where short_name = 'tor'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Possessed Aven'),
    (select id from sets where short_name = 'tor'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pitchstone Wall'),
    (select id from sets where short_name = 'tor'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nantuko Cultivator'),
    (select id from sets where short_name = 'tor'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Devastating Dreams'),
    (select id from sets where short_name = 'tor'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tainted Wood'),
    (select id from sets where short_name = 'tor'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Breakthrough'),
    (select id from sets where short_name = 'tor'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Morningtide'),
    (select id from sets where short_name = 'tor'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Centaur Veteran'),
    (select id from sets where short_name = 'tor'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Centaur Chieftain'),
    (select id from sets where short_name = 'tor'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llawan, Cephalid Empress'),
    (select id from sets where short_name = 'tor'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pardic Collaborator'),
    (select id from sets where short_name = 'tor'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coral Net'),
    (select id from sets where short_name = 'tor'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravegouger'),
    (select id from sets where short_name = 'tor'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overmaster'),
    (select id from sets where short_name = 'tor'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strength of Lunacy'),
    (select id from sets where short_name = 'tor'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mystic Familiar'),
    (select id from sets where short_name = 'tor'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tainted Isle'),
    (select id from sets where short_name = 'tor'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reborn Hero'),
    (select id from sets where short_name = 'tor'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shambling Swarm'),
    (select id from sets where short_name = 'tor'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mesmeric Fiend'),
    (select id from sets where short_name = 'tor'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unhinge'),
    (select id from sets where short_name = 'tor'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barbarian Outcast'),
    (select id from sets where short_name = 'tor'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dawn of the Dead'),
    (select id from sets where short_name = 'tor'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gloomdrifter'),
    (select id from sets where short_name = 'tor'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Churning Eddy'),
    (select id from sets where short_name = 'tor'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Far Wanderings'),
    (select id from sets where short_name = 'tor'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cephalid Aristocrat'),
    (select id from sets where short_name = 'tor'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Longhorn Firebeast'),
    (select id from sets where short_name = 'tor'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teroh''s Faithful'),
    (select id from sets where short_name = 'tor'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Retraced Image'),
    (select id from sets where short_name = 'tor'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nantuko Calmer'),
    (select id from sets where short_name = 'tor'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mortiphobia'),
    (select id from sets where short_name = 'tor'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circular Logic'),
    (select id from sets where short_name = 'tor'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Last Laugh'),
    (select id from sets where short_name = 'tor'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Turbulent Dreams'),
    (select id from sets where short_name = 'tor'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enslaved Dwarf'),
    (select id from sets where short_name = 'tor'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cephalid Illusionist'),
    (select id from sets where short_name = 'tor'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balshan Collaborator'),
    (select id from sets where short_name = 'tor'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balthor the Stout'),
    (select id from sets where short_name = 'tor'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Insidious Dreams'),
    (select id from sets where short_name = 'tor'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cabal Torturer'),
    (select id from sets where short_name = 'tor'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Invigorating Falls'),
    (select id from sets where short_name = 'tor'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Narcissism'),
    (select id from sets where short_name = 'tor'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spirit Flare'),
    (select id from sets where short_name = 'tor'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krosan Constrictor'),
    (select id from sets where short_name = 'tor'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pardic Lancer'),
    (select id from sets where short_name = 'tor'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gurzigost'),
    (select id from sets where short_name = 'tor'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flaming Gambit'),
    (select id from sets where short_name = 'tor'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cleansing Meditation'),
    (select id from sets where short_name = 'tor'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Violent Eruption'),
    (select id from sets where short_name = 'tor'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Petravark'),
    (select id from sets where short_name = 'tor'),
    '109',
    'common'
) 
 on conflict do nothing;
