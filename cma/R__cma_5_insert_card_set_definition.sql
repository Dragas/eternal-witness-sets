insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cma'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistmeadow Witch'),
    (select id from sets where short_name = 'cma'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cma'),
    '290',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana-Charged Dragon'),
    (select id from sets where short_name = 'cma'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wash Out'),
    (select id from sets where short_name = 'cma'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Greaves'),
    (select id from sets where short_name = 'cma'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unexpectedly Absent'),
    (select id from sets where short_name = 'cma'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyward Eye Prophets'),
    (select id from sets where short_name = 'cma'),
    '193',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cma'),
    '303',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakdos Carnarium'),
    (select id from sets where short_name = 'cma'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'cma'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orzhov Basilica'),
    (select id from sets where short_name = 'cma'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dungeon Geists'),
    (select id from sets where short_name = 'cma'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grisly Salvage'),
    (select id from sets where short_name = 'cma'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vow of Duty'),
    (select id from sets where short_name = 'cma'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Caller of the Pack'),
    (select id from sets where short_name = 'cma'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cma'),
    '313',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eternal Witness'),
    (select id from sets where short_name = 'cma'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diabolic Servitude'),
    (select id from sets where short_name = 'cma'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sulfurous Blast'),
    (select id from sets where short_name = 'cma'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Restore'),
    (select id from sets where short_name = 'cma'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armillary Sphere'),
    (select id from sets where short_name = 'cma'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Song of the Dryads'),
    (select id from sets where short_name = 'cma'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loreseeker''s Stone'),
    (select id from sets where short_name = 'cma'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silklash Spider'),
    (select id from sets where short_name = 'cma'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Syphon Flesh'),
    (select id from sets where short_name = 'cma'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Guildgate'),
    (select id from sets where short_name = 'cma'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thief of Blood'),
    (select id from sets where short_name = 'cma'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Path to Exile'),
    (select id from sets where short_name = 'cma'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rupture Spire'),
    (select id from sets where short_name = 'cma'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wave of Vitriol'),
    (select id from sets where short_name = 'cma'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meren of Clan Nel Toth'),
    (select id from sets where short_name = 'cma'),
    '186',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rakdos Signet'),
    (select id from sets where short_name = 'cma'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wellwisher'),
    (select id from sets where short_name = 'cma'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Priest of Titania'),
    (select id from sets where short_name = 'cma'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Imperious Perfect'),
    (select id from sets where short_name = 'cma'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Timberwatch Elf'),
    (select id from sets where short_name = 'cma'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corpse Augur'),
    (select id from sets where short_name = 'cma'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crystal Vein'),
    (select id from sets where short_name = 'cma'),
    '246',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rampaging Baloths'),
    (select id from sets where short_name = 'cma'),
    '139',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Moss Diamond'),
    (select id from sets where short_name = 'cma'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Altar''s Reap'),
    (select id from sets where short_name = 'cma'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Blossoms'),
    (select id from sets where short_name = 'cma'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Extractor Demon'),
    (select id from sets where short_name = 'cma'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zoetic Cavern'),
    (select id from sets where short_name = 'cma'),
    '284',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'cma'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Putrefy'),
    (select id from sets where short_name = 'cma'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Voice of All'),
    (select id from sets where short_name = 'cma'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'cma'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bladewing the Risen'),
    (select id from sets where short_name = 'cma'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jungle Hollow'),
    (select id from sets where short_name = 'cma'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dread Cacodemon'),
    (select id from sets where short_name = 'cma'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Return to Dust'),
    (select id from sets where short_name = 'cma'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightkeeper of Emeria'),
    (select id from sets where short_name = 'cma'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquil Thicket'),
    (select id from sets where short_name = 'cma'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcane Denial'),
    (select id from sets where short_name = 'cma'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Satyr Wayfinder'),
    (select id from sets where short_name = 'cma'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Masked Admirers'),
    (select id from sets where short_name = 'cma'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evincar''s Justice'),
    (select id from sets where short_name = 'cma'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bojuka Bog'),
    (select id from sets where short_name = 'cma'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eater of Hope'),
    (select id from sets where short_name = 'cma'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Haunted Fengraf'),
    (select id from sets where short_name = 'cma'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Nantuko'),
    (select id from sets where short_name = 'cma'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selesnya Signet'),
    (select id from sets where short_name = 'cma'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seer''s Sundial'),
    (select id from sets where short_name = 'cma'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mulch'),
    (select id from sets where short_name = 'cma'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Essence Warden'),
    (select id from sets where short_name = 'cma'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bonehoard'),
    (select id from sets where short_name = 'cma'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cma'),
    '318',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curse of Inertia'),
    (select id from sets where short_name = 'cma'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cma'),
    '311',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deceiver Exarch'),
    (select id from sets where short_name = 'cma'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Simic Signet'),
    (select id from sets where short_name = 'cma'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molten Slagheap'),
    (select id from sets where short_name = 'cma'),
    '260',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barter in Blood'),
    (select id from sets where short_name = 'cma'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azorius Guildgate'),
    (select id from sets where short_name = 'cma'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desert Twister'),
    (select id from sets where short_name = 'cma'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Derevi, Empyrial Tactician'),
    (select id from sets where short_name = 'cma'),
    '176',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Collective Unconscious'),
    (select id from sets where short_name = 'cma'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thunderstaff'),
    (select id from sets where short_name = 'cma'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reiver Demon'),
    (select id from sets where short_name = 'cma'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lu Xun, Scholar General'),
    (select id from sets where short_name = 'cma'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primordial Sage'),
    (select id from sets where short_name = 'cma'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Grip'),
    (select id from sets where short_name = 'cma'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'cma'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golgari Charm'),
    (select id from sets where short_name = 'cma'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orzhov Signet'),
    (select id from sets where short_name = 'cma'),
    '223',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boros Guildmage'),
    (select id from sets where short_name = 'cma'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Ranger'),
    (select id from sets where short_name = 'cma'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cma'),
    '320',
    'common'
) ,
(
    (select id from mtgcard where name = 'Korozda Guildmage'),
    (select id from sets where short_name = 'cma'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blood Bairn'),
    (select id from sets where short_name = 'cma'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pathbreaker Ibex'),
    (select id from sets where short_name = 'cma'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Siege Behemoth'),
    (select id from sets where short_name = 'cma'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Centaur Vinecrasher'),
    (select id from sets where short_name = 'cma'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Guildgate'),
    (select id from sets where short_name = 'cma'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolfcaller''s Howl'),
    (select id from sets where short_name = 'cma'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Predator, Flagship'),
    (select id from sets where short_name = 'cma'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of the False God'),
    (select id from sets where short_name = 'cma'),
    '277',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mortify'),
    (select id from sets where short_name = 'cma'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jungle Basin'),
    (select id from sets where short_name = 'cma'),
    '258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'cma'),
    '307',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viridian Zealot'),
    (select id from sets where short_name = 'cma'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azorius Chancery'),
    (select id from sets where short_name = 'cma'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lifeblood Hydra'),
    (select id from sets where short_name = 'cma'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Signet'),
    (select id from sets where short_name = 'cma'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Borrowing 100,000 Arrows'),
    (select id from sets where short_name = 'cma'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cloudthresher'),
    (select id from sets where short_name = 'cma'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = 'cma'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Acidic Slime'),
    (select id from sets where short_name = 'cma'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Presence of Gond'),
    (select id from sets where short_name = 'cma'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kaalia of the Vast'),
    (select id from sets where short_name = 'cma'),
    '180',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Duergar Hedge-Mage'),
    (select id from sets where short_name = 'cma'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selesnya Guildmage'),
    (select id from sets where short_name = 'cma'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Command Tower'),
    (select id from sets where short_name = 'cma'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leafdrake Roost'),
    (select id from sets where short_name = 'cma'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selesnya Sanctuary'),
    (select id from sets where short_name = 'cma'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Safekeeper'),
    (select id from sets where short_name = 'cma'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wolfbriar Elemental'),
    (select id from sets where short_name = 'cma'),
    '168',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mother of Runes'),
    (select id from sets where short_name = 'cma'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overrun'),
    (select id from sets where short_name = 'cma'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flickerform'),
    (select id from sets where short_name = 'cma'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiend Hunter'),
    (select id from sets where short_name = 'cma'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grim Flowering'),
    (select id from sets where short_name = 'cma'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Primal Growth'),
    (select id from sets where short_name = 'cma'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bane of Progress'),
    (select id from sets where short_name = 'cma'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thornweald Archer'),
    (select id from sets where short_name = 'cma'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivid Meadow'),
    (select id from sets where short_name = 'cma'),
    '283',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Havenwood Battleground'),
    (select id from sets where short_name = 'cma'),
    '256',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kirtar''s Wrath'),
    (select id from sets where short_name = 'cma'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Opal Palace'),
    (select id from sets where short_name = 'cma'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Charm'),
    (select id from sets where short_name = 'cma'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Basandra, Battle Seraph'),
    (select id from sets where short_name = 'cma'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Titania, Protector of Argoth'),
    (select id from sets where short_name = 'cma'),
    '157',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fresh Meat'),
    (select id from sets where short_name = 'cma'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angelic Arbiter'),
    (select id from sets where short_name = 'cma'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Butcher of Malakir'),
    (select id from sets where short_name = 'cma'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beastmaster Ascension'),
    (select id from sets where short_name = 'cma'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harrow'),
    (select id from sets where short_name = 'cma'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cma'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Syphon Mind'),
    (select id from sets where short_name = 'cma'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Verdant Force'),
    (select id from sets where short_name = 'cma'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Malfegor'),
    (select id from sets where short_name = 'cma'),
    '184',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'cma'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grave Sifter'),
    (select id from sets where short_name = 'cma'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Congregate'),
    (select id from sets where short_name = 'cma'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wood Elves'),
    (select id from sets where short_name = 'cma'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spider Spawning'),
    (select id from sets where short_name = 'cma'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death by Dragons'),
    (select id from sets where short_name = 'cma'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orim''s Thunder'),
    (select id from sets where short_name = 'cma'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunderfoot Baloth'),
    (select id from sets where short_name = 'cma'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conjurer''s Closet'),
    (select id from sets where short_name = 'cma'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief, the Vastwood'),
    (select id from sets where short_name = 'cma'),
    '263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Secluded Steppe'),
    (select id from sets where short_name = 'cma'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darksteel Mutation'),
    (select id from sets where short_name = 'cma'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cma'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ambition''s Cost'),
    (select id from sets where short_name = 'cma'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aethermage''s Touch'),
    (select id from sets where short_name = 'cma'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seaside Citadel'),
    (select id from sets where short_name = 'cma'),
    '269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Rot Farm'),
    (select id from sets where short_name = 'cma'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'cma'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Control Magic'),
    (select id from sets where short_name = 'cma'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whirlwind'),
    (select id from sets where short_name = 'cma'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Whelp'),
    (select id from sets where short_name = 'cma'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghost Quarter'),
    (select id from sets where short_name = 'cma'),
    '251',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terminate'),
    (select id from sets where short_name = 'cma'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Farhaven Elf'),
    (select id from sets where short_name = 'cma'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swiftfoot Boots'),
    (select id from sets where short_name = 'cma'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Surveyor''s Scope'),
    (select id from sets where short_name = 'cma'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'cma'),
    '306',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fallen Angel'),
    (select id from sets where short_name = 'cma'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Offering'),
    (select id from sets where short_name = 'cma'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cleansing Beam'),
    (select id from sets where short_name = 'cma'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tariel, Reckoner of Souls'),
    (select id from sets where short_name = 'cma'),
    '194',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vivid Marsh'),
    (select id from sets where short_name = 'cma'),
    '282',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drove of Elves'),
    (select id from sets where short_name = 'cma'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Garrison'),
    (select id from sets where short_name = 'cma'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Rager'),
    (select id from sets where short_name = 'cma'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Djinn of Infinite Deceits'),
    (select id from sets where short_name = 'cma'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faerie Conclave'),
    (select id from sets where short_name = 'cma'),
    '248',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul of the Harvest'),
    (select id from sets where short_name = 'cma'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hada Spy Patrol'),
    (select id from sets where short_name = 'cma'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tempt with Glory'),
    (select id from sets where short_name = 'cma'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darksteel Ingot'),
    (select id from sets where short_name = 'cma'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tainted Wood'),
    (select id from sets where short_name = 'cma'),
    '276',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cma'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scourge of Nel Toth'),
    (select id from sets where short_name = 'cma'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Victimize'),
    (select id from sets where short_name = 'cma'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lotleth Troll'),
    (select id from sets where short_name = 'cma'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shriekmaw'),
    (select id from sets where short_name = 'cma'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tornado Elemental'),
    (select id from sets where short_name = 'cma'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Signet'),
    (select id from sets where short_name = 'cma'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrecking Ball'),
    (select id from sets where short_name = 'cma'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curse of the Forsaken'),
    (select id from sets where short_name = 'cma'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grim Backwoods'),
    (select id from sets where short_name = 'cma'),
    '254',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blue Sun''s Zenith'),
    (select id from sets where short_name = 'cma'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rise from the Grave'),
    (select id from sets where short_name = 'cma'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'cma'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Visionary'),
    (select id from sets where short_name = 'cma'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Skysweeper'),
    (select id from sets where short_name = 'cma'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akroma, Angel of Fury'),
    (select id from sets where short_name = 'cma'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barren Moor'),
    (select id from sets where short_name = 'cma'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Despair'),
    (select id from sets where short_name = 'cma'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wren''s Run Packmaster'),
    (select id from sets where short_name = 'cma'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kessig Cagebreakers'),
    (select id from sets where short_name = 'cma'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bant Panorama'),
    (select id from sets where short_name = 'cma'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akoum Refuge'),
    (select id from sets where short_name = 'cma'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tribute to the Wild'),
    (select id from sets where short_name = 'cma'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vulturous Zombie'),
    (select id from sets where short_name = 'cma'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myriad Landscape'),
    (select id from sets where short_name = 'cma'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azorius Keyrune'),
    (select id from sets where short_name = 'cma'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cma'),
    '302',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slippery Karst'),
    (select id from sets where short_name = 'cma'),
    '275',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cma'),
    '315',
    'common'
) ,
(
    (select id from mtgcard where name = 'High Market'),
    (select id from sets where short_name = 'cma'),
    '257',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aerie Mystics'),
    (select id from sets where short_name = 'cma'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thought Vessel'),
    (select id from sets where short_name = 'cma'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Titania''s Chosen'),
    (select id from sets where short_name = 'cma'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyrohemia'),
    (select id from sets where short_name = 'cma'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skullclamp'),
    (select id from sets where short_name = 'cma'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kazandu Tuskcaller'),
    (select id from sets where short_name = 'cma'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mazirek, Kraul Death Priest'),
    (select id from sets where short_name = 'cma'),
    '185',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cma'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Banshee of the Dread Choir'),
    (select id from sets where short_name = 'cma'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karmic Guide'),
    (select id from sets where short_name = 'cma'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archangel of Strife'),
    (select id from sets where short_name = 'cma'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cma'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Commander''s Sphere'),
    (select id from sets where short_name = 'cma'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thousand-Year Elixir'),
    (select id from sets where short_name = 'cma'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cma'),
    '309',
    'common'
) ,
(
    (select id from mtgcard where name = 'Indrik Stomphowler'),
    (select id from sets where short_name = 'cma'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vow of Malice'),
    (select id from sets where short_name = 'cma'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Praetor''s Counsel'),
    (select id from sets where short_name = 'cma'),
    '134',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Murkfiend Liege'),
    (select id from sets where short_name = 'cma'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Basalt Monolith'),
    (select id from sets where short_name = 'cma'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cma'),
    '310',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Finality'),
    (select id from sets where short_name = 'cma'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cma'),
    '317',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winged Coatl'),
    (select id from sets where short_name = 'cma'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Righteous Cause'),
    (select id from sets where short_name = 'cma'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Assault Suit'),
    (select id from sets where short_name = 'cma'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selesnya Guildgate'),
    (select id from sets where short_name = 'cma'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wretched Confluence'),
    (select id from sets where short_name = 'cma'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Archdruid'),
    (select id from sets where short_name = 'cma'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirror Entity'),
    (select id from sets where short_name = 'cma'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Transguild Promenade'),
    (select id from sets where short_name = 'cma'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunting Triad'),
    (select id from sets where short_name = 'cma'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mycoloth'),
    (select id from sets where short_name = 'cma'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'cma'),
    '308',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stonecloaker'),
    (select id from sets where short_name = 'cma'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Elder'),
    (select id from sets where short_name = 'cma'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jarad, Golgari Lich Lord'),
    (select id from sets where short_name = 'cma'),
    '179',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wonder'),
    (select id from sets where short_name = 'cma'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pilgrim''s Eye'),
    (select id from sets where short_name = 'cma'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Snare'),
    (select id from sets where short_name = 'cma'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Freyalise, Llanowar''s Fury'),
    (select id from sets where short_name = 'cma'),
    '111',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Saltcrusted Steppe'),
    (select id from sets where short_name = 'cma'),
    '268',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rubinia Soulsinger'),
    (select id from sets where short_name = 'cma'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Plaguelord'),
    (select id from sets where short_name = 'cma'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cma'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cma'),
    '319',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bathe in Light'),
    (select id from sets where short_name = 'cma'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sword of the Paruns'),
    (select id from sets where short_name = 'cma'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thornwind Faeries'),
    (select id from sets where short_name = 'cma'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sejiri Refuge'),
    (select id from sets where short_name = 'cma'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lys Alana Huntmaster'),
    (select id from sets where short_name = 'cma'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reclamation Sage'),
    (select id from sets where short_name = 'cma'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Monument'),
    (select id from sets where short_name = 'cma'),
    '216',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bloodspore Thrinax'),
    (select id from sets where short_name = 'cma'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diabolic Tutor'),
    (select id from sets where short_name = 'cma'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Akroma''s Vengeance'),
    (select id from sets where short_name = 'cma'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Comet Storm'),
    (select id from sets where short_name = 'cma'),
    '79',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Curse of Predation'),
    (select id from sets where short_name = 'cma'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orzhov Guildmage'),
    (select id from sets where short_name = 'cma'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terastodon'),
    (select id from sets where short_name = 'cma'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stranglehold'),
    (select id from sets where short_name = 'cma'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cma'),
    '288',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anger'),
    (select id from sets where short_name = 'cma'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Razorjaw Oni'),
    (select id from sets where short_name = 'cma'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oni of Wild Places'),
    (select id from sets where short_name = 'cma'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leonin Bladetrap'),
    (select id from sets where short_name = 'cma'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ezuri, Renegade Leader'),
    (select id from sets where short_name = 'cma'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cma'),
    '316',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shattered Angel'),
    (select id from sets where short_name = 'cma'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cma'),
    '314',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'cma'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Mystic'),
    (select id from sets where short_name = 'cma'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cma'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Creeperhulk'),
    (select id from sets where short_name = 'cma'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viridian Emissary'),
    (select id from sets where short_name = 'cma'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cma'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cma'),
    '312',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gwyllion Hedge-Mage'),
    (select id from sets where short_name = 'cma'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vivid Grove'),
    (select id from sets where short_name = 'cma'),
    '281',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diviner Spirit'),
    (select id from sets where short_name = 'cma'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flickerwisp'),
    (select id from sets where short_name = 'cma'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skullwinder'),
    (select id from sets where short_name = 'cma'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forgotten Cave'),
    (select id from sets where short_name = 'cma'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Champion of Stray Souls'),
    (select id from sets where short_name = 'cma'),
    '51',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Great Oak Guardian'),
    (select id from sets where short_name = 'cma'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overwhelming Stampede'),
    (select id from sets where short_name = 'cma'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cma'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cma'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emerald Medallion'),
    (select id from sets where short_name = 'cma'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avatar of Slaughter'),
    (select id from sets where short_name = 'cma'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Polluted Mire'),
    (select id from sets where short_name = 'cma'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'cma'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dread Summons'),
    (select id from sets where short_name = 'cma'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gargoyle Castle'),
    (select id from sets where short_name = 'cma'),
    '250',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'cma'),
    '278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Roon of the Hidden Realm'),
    (select id from sets where short_name = 'cma'),
    '190',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vow of Lightning'),
    (select id from sets where short_name = 'cma'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Joraga Warcaller'),
    (select id from sets where short_name = 'cma'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sever the Bloodline'),
    (select id from sets where short_name = 'cma'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Immaculate Magistrate'),
    (select id from sets where short_name = 'cma'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cma'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azami, Lady of Scrolls'),
    (select id from sets where short_name = 'cma'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oros, the Avenger'),
    (select id from sets where short_name = 'cma'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Furnace Whelp'),
    (select id from sets where short_name = 'cma'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Master Warcraft'),
    (select id from sets where short_name = 'cma'),
    '202',
    'rare'
) 
 on conflict do nothing;
