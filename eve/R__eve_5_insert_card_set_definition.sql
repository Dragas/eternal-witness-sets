insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Twinblade Slasher'),
    (select id from sets where short_name = 'eve'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Canker Abomination'),
    (select id from sets where short_name = 'eve'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nip Gwyllion'),
    (select id from sets where short_name = 'eve'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necroskitter'),
    (select id from sets where short_name = 'eve'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marshdrinker Giant'),
    (select id from sets where short_name = 'eve'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Snuffers'),
    (select id from sets where short_name = 'eve'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flame Jab'),
    (select id from sets where short_name = 'eve'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Suture Spirit'),
    (select id from sets where short_name = 'eve'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hobgoblin Dragoon'),
    (select id from sets where short_name = 'eve'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glen Elendra Archmage'),
    (select id from sets where short_name = 'eve'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merrow Bonegnawer'),
    (select id from sets where short_name = 'eve'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mindwrack Liege'),
    (select id from sets where short_name = 'eve'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gilder Bairn'),
    (select id from sets where short_name = 'eve'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balefire Liege'),
    (select id from sets where short_name = 'eve'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Figure of Destiny'),
    (select id from sets where short_name = 'eve'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fetid Heath'),
    (select id from sets where short_name = 'eve'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unnerving Assault'),
    (select id from sets where short_name = 'eve'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overbeing of Myth'),
    (select id from sets where short_name = 'eve'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rekindled Flame'),
    (select id from sets where short_name = 'eve'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quillspike'),
    (select id from sets where short_name = 'eve'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Batwing Brume'),
    (select id from sets where short_name = 'eve'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doomgape'),
    (select id from sets where short_name = 'eve'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wilderness Hypnotist'),
    (select id from sets where short_name = 'eve'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slippery Bogle'),
    (select id from sets where short_name = 'eve'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scarecrone'),
    (select id from sets where short_name = 'eve'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harvest Gwyllion'),
    (select id from sets where short_name = 'eve'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wistful Selkie'),
    (select id from sets where short_name = 'eve'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hag Hedge-Mage'),
    (select id from sets where short_name = 'eve'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hatchet Bully'),
    (select id from sets where short_name = 'eve'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Umbra Stalker'),
    (select id from sets where short_name = 'eve'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primalcrux'),
    (select id from sets where short_name = 'eve'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Needle Specter'),
    (select id from sets where short_name = 'eve'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Syphon Life'),
    (select id from sets where short_name = 'eve'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shorecrasher Mimic'),
    (select id from sets where short_name = 'eve'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crag Puca'),
    (select id from sets where short_name = 'eve'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drain the Well'),
    (select id from sets where short_name = 'eve'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crackleburr'),
    (select id from sets where short_name = 'eve'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Double Cleave'),
    (select id from sets where short_name = 'eve'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swirling Spriggan'),
    (select id from sets where short_name = 'eve'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Endless Horizons'),
    (select id from sets where short_name = 'eve'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unmake'),
    (select id from sets where short_name = 'eve'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashling, the Extinguisher'),
    (select id from sets where short_name = 'eve'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Banishing Knack'),
    (select id from sets where short_name = 'eve'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cold-Eyed Selkie'),
    (select id from sets where short_name = 'eve'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hateflayer'),
    (select id from sets where short_name = 'eve'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jawbone Skulkin'),
    (select id from sets where short_name = 'eve'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beckon Apparition'),
    (select id from sets where short_name = 'eve'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Puncture Blast'),
    (select id from sets where short_name = 'eve'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Creakwood Ghoul'),
    (select id from sets where short_name = 'eve'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crumbling Ashes'),
    (select id from sets where short_name = 'eve'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stillmoon Cavalier'),
    (select id from sets where short_name = 'eve'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cankerous Thirst'),
    (select id from sets where short_name = 'eve'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Noggle Hedge-Mage'),
    (select id from sets where short_name = 'eve'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evershrike'),
    (select id from sets where short_name = 'eve'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Impelled Giant'),
    (select id from sets where short_name = 'eve'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dream Fracture'),
    (select id from sets where short_name = 'eve'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duergar Hedge-Mage'),
    (select id from sets where short_name = 'eve'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flooded Grove'),
    (select id from sets where short_name = 'eve'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dominus of Fealty'),
    (select id from sets where short_name = 'eve'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nucklavee'),
    (select id from sets where short_name = 'eve'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fire at Will'),
    (select id from sets where short_name = 'eve'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Recumbent Bliss'),
    (select id from sets where short_name = 'eve'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rise of the Hobgoblins'),
    (select id from sets where short_name = 'eve'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oona''s Grace'),
    (select id from sets where short_name = 'eve'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Noggle Bridgebreaker'),
    (select id from sets where short_name = 'eve'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loyal Gyrfalcon'),
    (select id from sets where short_name = 'eve'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Noggle Bandit'),
    (select id from sets where short_name = 'eve'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stream Hopper'),
    (select id from sets where short_name = 'eve'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Groundling Pouncer'),
    (select id from sets where short_name = 'eve'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Monstrify'),
    (select id from sets where short_name = 'eve'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flickerwisp'),
    (select id from sets where short_name = 'eve'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stalker Hag'),
    (select id from sets where short_name = 'eve'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selkie Hedge-Mage'),
    (select id from sets where short_name = 'eve'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trapjaw Kelpie'),
    (select id from sets where short_name = 'eve'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Indigo Faerie'),
    (select id from sets where short_name = 'eve'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spirit of the Hearth'),
    (select id from sets where short_name = 'eve'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Regal Force'),
    (select id from sets where short_name = 'eve'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grazing Kelpie'),
    (select id from sets where short_name = 'eve'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scourge of the Nobilis'),
    (select id from sets where short_name = 'eve'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Light from Within'),
    (select id from sets where short_name = 'eve'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ballynock Trapper'),
    (select id from sets where short_name = 'eve'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathbringer Liege'),
    (select id from sets where short_name = 'eve'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nobilis of War'),
    (select id from sets where short_name = 'eve'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightsky Mimic'),
    (select id from sets where short_name = 'eve'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kithkin Spellduster'),
    (select id from sets where short_name = 'eve'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Belligerent Hatchling'),
    (select id from sets where short_name = 'eve'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hotheaded Giant'),
    (select id from sets where short_name = 'eve'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desecrator Hag'),
    (select id from sets where short_name = 'eve'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Call the Skybreaker'),
    (select id from sets where short_name = 'eve'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kithkin Zealot'),
    (select id from sets where short_name = 'eve'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Worm Harvest'),
    (select id from sets where short_name = 'eve'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Idle Thoughts'),
    (select id from sets where short_name = 'eve'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sapling of Colfenor'),
    (select id from sets where short_name = 'eve'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clout of the Dominus'),
    (select id from sets where short_name = 'eve'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightmare Incursion'),
    (select id from sets where short_name = 'eve'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raven''s Crime'),
    (select id from sets where short_name = 'eve'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanity Grinding'),
    (select id from sets where short_name = 'eve'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snakeform'),
    (select id from sets where short_name = 'eve'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deity of Scars'),
    (select id from sets where short_name = 'eve'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smoldering Butcher'),
    (select id from sets where short_name = 'eve'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dream Thief'),
    (select id from sets where short_name = 'eve'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glamerdye'),
    (select id from sets where short_name = 'eve'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phosphorescent Feast'),
    (select id from sets where short_name = 'eve'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Waves of Aggression'),
    (select id from sets where short_name = 'eve'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soot Imp'),
    (select id from sets where short_name = 'eve'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Noxious Hatchling'),
    (select id from sets where short_name = 'eve'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Outrage Shaman'),
    (select id from sets where short_name = 'eve'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duskdale Wurm'),
    (select id from sets where short_name = 'eve'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Murkfiend Liege'),
    (select id from sets where short_name = 'eve'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tilling Treefolk'),
    (select id from sets where short_name = 'eve'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Creakwood Liege'),
    (select id from sets where short_name = 'eve'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gwyllion Hedge-Mage'),
    (select id from sets where short_name = 'eve'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wake Thrasher'),
    (select id from sets where short_name = 'eve'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Favor of the Overbeing'),
    (select id from sets where short_name = 'eve'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Helix Pinnacle'),
    (select id from sets where short_name = 'eve'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hearthfire Hobgoblin'),
    (select id from sets where short_name = 'eve'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ward of Bones'),
    (select id from sets where short_name = 'eve'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spitting Image'),
    (select id from sets where short_name = 'eve'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inside Out'),
    (select id from sets where short_name = 'eve'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duergar Assailant'),
    (select id from sets where short_name = 'eve'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cauldron Haze'),
    (select id from sets where short_name = 'eve'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cenn''s Enlistment'),
    (select id from sets where short_name = 'eve'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archon of Justice'),
    (select id from sets where short_name = 'eve'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wickerbough Elder'),
    (select id from sets where short_name = 'eve'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talara''s Battalion'),
    (select id from sets where short_name = 'eve'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unwilling Recruit'),
    (select id from sets where short_name = 'eve'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aerie Ouphes'),
    (select id from sets where short_name = 'eve'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Patrol Signaler'),
    (select id from sets where short_name = 'eve'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Talonrend'),
    (select id from sets where short_name = 'eve'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cinder Pyromancer'),
    (select id from sets where short_name = 'eve'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voracious Hatchling'),
    (select id from sets where short_name = 'eve'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savage Conception'),
    (select id from sets where short_name = 'eve'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rendclaw Trow'),
    (select id from sets where short_name = 'eve'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Odious Trow'),
    (select id from sets where short_name = 'eve'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riverfall Mimic'),
    (select id from sets where short_name = 'eve'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talara''s Bane'),
    (select id from sets where short_name = 'eve'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fable of Wolf and Owl'),
    (select id from sets where short_name = 'eve'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Springjack Pasture'),
    (select id from sets where short_name = 'eve'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shell Skulkin'),
    (select id from sets where short_name = 'eve'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chaotic Backlash'),
    (select id from sets where short_name = 'eve'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fiery Bombardment'),
    (select id from sets where short_name = 'eve'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cascade Bluffs'),
    (select id from sets where short_name = 'eve'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hallowed Burial'),
    (select id from sets where short_name = 'eve'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodied Ghost'),
    (select id from sets where short_name = 'eve'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moonhold'),
    (select id from sets where short_name = 'eve'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Antler Skulkin'),
    (select id from sets where short_name = 'eve'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stigma Lasher'),
    (select id from sets where short_name = 'eve'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Divinity of Pride'),
    (select id from sets where short_name = 'eve'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shrewd Hatchling'),
    (select id from sets where short_name = 'eve'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inundate'),
    (select id from sets where short_name = 'eve'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thunderblust'),
    (select id from sets where short_name = 'eve'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Invert the Skies'),
    (select id from sets where short_name = 'eve'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloom Tender'),
    (select id from sets where short_name = 'eve'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leering Emblem'),
    (select id from sets where short_name = 'eve'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woodlurker Mimic'),
    (select id from sets where short_name = 'eve'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battlegate Mimic'),
    (select id from sets where short_name = 'eve'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Restless Apparition'),
    (select id from sets where short_name = 'eve'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Endure'),
    (select id from sets where short_name = 'eve'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Springjack Shepherd'),
    (select id from sets where short_name = 'eve'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mirror Sheen'),
    (select id from sets where short_name = 'eve'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Reap'),
    (select id from sets where short_name = 'eve'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merrow Levitator'),
    (select id from sets where short_name = 'eve'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rugged Prairie'),
    (select id from sets where short_name = 'eve'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sturdy Hatchling'),
    (select id from sets where short_name = 'eve'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Noggle Ransacker'),
    (select id from sets where short_name = 'eve'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duergar Mine-Captain'),
    (select id from sets where short_name = 'eve'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hoof Skulkin'),
    (select id from sets where short_name = 'eve'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Razorfin Abolisher'),
    (select id from sets where short_name = 'eve'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gift of the Deity'),
    (select id from sets where short_name = 'eve'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heartlash Cinder'),
    (select id from sets where short_name = 'eve'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duergar Cave-Guard'),
    (select id from sets where short_name = 'eve'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lingering Tormentor'),
    (select id from sets where short_name = 'eve'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nettle Sentinel'),
    (select id from sets where short_name = 'eve'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyrrhic Revival'),
    (select id from sets where short_name = 'eve'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cache Raiders'),
    (select id from sets where short_name = 'eve'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Altar Golem'),
    (select id from sets where short_name = 'eve'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spitemare'),
    (select id from sets where short_name = 'eve'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fang Skulkin'),
    (select id from sets where short_name = 'eve'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Twilight Mire'),
    (select id from sets where short_name = 'eve'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Edge of the Divinity'),
    (select id from sets where short_name = 'eve'),
    '87',
    'common'
) 
 on conflict do nothing;
