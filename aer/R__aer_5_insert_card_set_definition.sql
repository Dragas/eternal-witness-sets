insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Destructive Tampering'),
    (select id from sets where short_name = 'aer'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'aer'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whir of Invention'),
    (select id from sets where short_name = 'aer'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Consulate Crackdown'),
    (select id from sets where short_name = 'aer'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Narnam Renegade'),
    (select id from sets where short_name = 'aer'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Highspire Infusion'),
    (select id from sets where short_name = 'aer'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Submerged Boneyard'),
    (select id from sets where short_name = 'aer'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Defiant Salvager'),
    (select id from sets where short_name = 'aer'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quicksmith Spy'),
    (select id from sets where short_name = 'aer'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winding Constrictor'),
    (select id from sets where short_name = 'aer'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scrapper Champion'),
    (select id from sets where short_name = 'aer'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Consulate Dreadnought'),
    (select id from sets where short_name = 'aer'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Outland Boar'),
    (select id from sets where short_name = 'aer'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Watchful Automaton'),
    (select id from sets where short_name = 'aer'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Peacewalker Colossus'),
    (select id from sets where short_name = 'aer'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Implement of Combustion'),
    (select id from sets where short_name = 'aer'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prey Upon'),
    (select id from sets where short_name = 'aer'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trophy Mage'),
    (select id from sets where short_name = 'aer'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daredevil Dragster'),
    (select id from sets where short_name = 'aer'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Metallic Mimic'),
    (select id from sets where short_name = 'aer'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Greenwheel Liberator'),
    (select id from sets where short_name = 'aer'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sram, Senior Edificer'),
    (select id from sets where short_name = 'aer'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Decommission'),
    (select id from sets where short_name = 'aer'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spire Patrol'),
    (select id from sets where short_name = 'aer'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyship Plunderer'),
    (select id from sets where short_name = 'aer'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghirapur Osprey'),
    (select id from sets where short_name = 'aer'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Racer'),
    (select id from sets where short_name = 'aer'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mechanized Production'),
    (select id from sets where short_name = 'aer'),
    '38',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ajani Unyielding'),
    (select id from sets where short_name = 'aer'),
    '127',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Disallow'),
    (select id from sets where short_name = 'aer'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Night Market Aeronaut'),
    (select id from sets where short_name = 'aer'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Midnight Entourage'),
    (select id from sets where short_name = 'aer'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shipwreck Moray'),
    (select id from sets where short_name = 'aer'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Chaser'),
    (select id from sets where short_name = 'aer'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Monstrous Onslaught'),
    (select id from sets where short_name = 'aer'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Invigorated Rampage'),
    (select id from sets where short_name = 'aer'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Implement of Improvement'),
    (select id from sets where short_name = 'aer'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quicksmith Rebel'),
    (select id from sets where short_name = 'aer'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Implement of Malice'),
    (select id from sets where short_name = 'aer'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tranquil Expanse'),
    (select id from sets where short_name = 'aer'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Universal Solvent'),
    (select id from sets where short_name = 'aer'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Revolution'),
    (select id from sets where short_name = 'aer'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glint-Sleeve Siphoner'),
    (select id from sets where short_name = 'aer'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Herald of Anguish'),
    (select id from sets where short_name = 'aer'),
    '64',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Walking Ballista'),
    (select id from sets where short_name = 'aer'),
    '181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Resourceful Return'),
    (select id from sets where short_name = 'aer'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lathnu Sailback'),
    (select id from sets where short_name = 'aer'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Audacious Infiltrator'),
    (select id from sets where short_name = 'aer'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aethertide Whale'),
    (select id from sets where short_name = 'aer'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unbridled Growth'),
    (select id from sets where short_name = 'aer'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Embraal Gear-Smasher'),
    (select id from sets where short_name = 'aer'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leave in the Dust'),
    (select id from sets where short_name = 'aer'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tezzeret''s Touch'),
    (select id from sets where short_name = 'aer'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Precise Strike'),
    (select id from sets where short_name = 'aer'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enraged Giant'),
    (select id from sets where short_name = 'aer'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inspiring Statuary'),
    (select id from sets where short_name = 'aer'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aethersphere Harvester'),
    (select id from sets where short_name = 'aer'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hinterland Drake'),
    (select id from sets where short_name = 'aer'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ironclad Revolutionary'),
    (select id from sets where short_name = 'aer'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Felidar Guardian'),
    (select id from sets where short_name = 'aer'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Salvage Scuttler'),
    (select id from sets where short_name = 'aer'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Airdrop Aeronauts'),
    (select id from sets where short_name = 'aer'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Renegade''s Getaway'),
    (select id from sets where short_name = 'aer'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hungry Flames'),
    (select id from sets where short_name = 'aer'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Caught in the Brights'),
    (select id from sets where short_name = 'aer'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inspiring Roar'),
    (select id from sets where short_name = 'aer'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conviction'),
    (select id from sets where short_name = 'aer'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weldfast Engineer'),
    (select id from sets where short_name = 'aer'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pia''s Revolution'),
    (select id from sets where short_name = 'aer'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verdant Automaton'),
    (select id from sets where short_name = 'aer'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aid from the Cowl'),
    (select id from sets where short_name = 'aer'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tezzeret the Schemer'),
    (select id from sets where short_name = 'aer'),
    '137',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Solemn Recruit'),
    (select id from sets where short_name = 'aer'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Comrade'),
    (select id from sets where short_name = 'aer'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cruel Finality'),
    (select id from sets where short_name = 'aer'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deft Dismissal'),
    (select id from sets where short_name = 'aer'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rishkar''s Expertise'),
    (select id from sets where short_name = 'aer'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dispersal Technician'),
    (select id from sets where short_name = 'aer'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heart of Kiran'),
    (select id from sets where short_name = 'aer'),
    '153',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wrangle'),
    (select id from sets where short_name = 'aer'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baral, Chief of Compliance'),
    (select id from sets where short_name = 'aer'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rogue Refiner'),
    (select id from sets where short_name = 'aer'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Restoration Specialist'),
    (select id from sets where short_name = 'aer'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lifecraft Awakening'),
    (select id from sets where short_name = 'aer'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lifecrafter''s Gift'),
    (select id from sets where short_name = 'aer'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scrap Trawler'),
    (select id from sets where short_name = 'aer'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Renegade Wheelsmith'),
    (select id from sets where short_name = 'aer'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gonti''s Machinations'),
    (select id from sets where short_name = 'aer'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cogwork Assembler'),
    (select id from sets where short_name = 'aer'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vengeful Rebel'),
    (select id from sets where short_name = 'aer'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aetherstream Leopard'),
    (select id from sets where short_name = 'aer'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hope of Ghirapur'),
    (select id from sets where short_name = 'aer'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Aid'),
    (select id from sets where short_name = 'aer'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kari Zev, Skyship Raider'),
    (select id from sets where short_name = 'aer'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fourth Bridge Prowler'),
    (select id from sets where short_name = 'aer'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lifecrafter''s Bestiary'),
    (select id from sets where short_name = 'aer'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Battle at the Bridge'),
    (select id from sets where short_name = 'aer'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scrounging Bandar'),
    (select id from sets where short_name = 'aer'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ajani, Valiant Protector'),
    (select id from sets where short_name = 'aer'),
    '185',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sram''s Expertise'),
    (select id from sets where short_name = 'aer'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barricade Breaker'),
    (select id from sets where short_name = 'aer'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dawnfeather Eagle'),
    (select id from sets where short_name = 'aer'),
    '14†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rishkar, Peema Renegade'),
    (select id from sets where short_name = 'aer'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Augmenting Automaton'),
    (select id from sets where short_name = 'aer'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tezzeret''s Simulacrum'),
    (select id from sets where short_name = 'aer'),
    '193',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aethergeode Miner'),
    (select id from sets where short_name = 'aer'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Irontread Crusher'),
    (select id from sets where short_name = 'aer'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spire of Industry'),
    (select id from sets where short_name = 'aer'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ridgescale Tusker'),
    (select id from sets where short_name = 'aer'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bastion Inventor'),
    (select id from sets where short_name = 'aer'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Welder Automaton'),
    (select id from sets where short_name = 'aer'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bastion Enforcer'),
    (select id from sets where short_name = 'aer'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Night Market Guard'),
    (select id from sets where short_name = 'aer'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reservoir Walker'),
    (select id from sets where short_name = 'aer'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pacification Array'),
    (select id from sets where short_name = 'aer'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aether Swooper'),
    (select id from sets where short_name = 'aer'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hidden Stockpile'),
    (select id from sets where short_name = 'aer'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Filigree Crawler'),
    (select id from sets where short_name = 'aer'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prizefighter Construct'),
    (select id from sets where short_name = 'aer'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heroic Intervention'),
    (select id from sets where short_name = 'aer'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Renegade Rallier'),
    (select id from sets where short_name = 'aer'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merchant''s Dockhand'),
    (select id from sets where short_name = 'aer'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oath of Ajani'),
    (select id from sets where short_name = 'aer'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Planar Bridge'),
    (select id from sets where short_name = 'aer'),
    '171',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Crackdown Construct'),
    (select id from sets where short_name = 'aer'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treasure Keeper'),
    (select id from sets where short_name = 'aer'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tezzeret''s Betrayal'),
    (select id from sets where short_name = 'aer'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gonti''s Aether Heart'),
    (select id from sets where short_name = 'aer'),
    '152',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hidden Herbalists'),
    (select id from sets where short_name = 'aer'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deadeye Harpooner'),
    (select id from sets where short_name = 'aer'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alley Strangler'),
    (select id from sets where short_name = 'aer'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter Arrest'),
    (select id from sets where short_name = 'aer'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daring Demolition'),
    (select id from sets where short_name = 'aer'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Druid of the Cowl'),
    (select id from sets where short_name = 'aer'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Implement of Examination'),
    (select id from sets where short_name = 'aer'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illusionist''s Stratagem'),
    (select id from sets where short_name = 'aer'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maulfist Revolutionary'),
    (select id from sets where short_name = 'aer'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kari Zev''s Expertise'),
    (select id from sets where short_name = 'aer'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gifted Aetherborn'),
    (select id from sets where short_name = 'aer'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Efficient Construction'),
    (select id from sets where short_name = 'aer'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Renegade Map'),
    (select id from sets where short_name = 'aer'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrangle'),
    (select id from sets where short_name = 'aer'),
    '101†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Servo Schematic'),
    (select id from sets where short_name = 'aer'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lifecraft Cavalry'),
    (select id from sets where short_name = 'aer'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Take into Custody'),
    (select id from sets where short_name = 'aer'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sweatworks Brawler'),
    (select id from sets where short_name = 'aer'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gremlin Infestation'),
    (select id from sets where short_name = 'aer'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fatal Push'),
    (select id from sets where short_name = 'aer'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fen Hauler'),
    (select id from sets where short_name = 'aer'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frontline Rebel'),
    (select id from sets where short_name = 'aer'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aerial Modification'),
    (select id from sets where short_name = 'aer'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alley Evasion'),
    (select id from sets where short_name = 'aer'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Implement of Ferocity'),
    (select id from sets where short_name = 'aer'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ice Over'),
    (select id from sets where short_name = 'aer'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sly Requisitioner'),
    (select id from sets where short_name = 'aer'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Countless Gears Renegade'),
    (select id from sets where short_name = 'aer'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yahenni, Undying Partisan'),
    (select id from sets where short_name = 'aer'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aether Poisoner'),
    (select id from sets where short_name = 'aer'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maverick Thopterist'),
    (select id from sets where short_name = 'aer'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'aer'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravenous Intruder'),
    (select id from sets where short_name = 'aer'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Exquisite Archangel'),
    (select id from sets where short_name = 'aer'),
    '18',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Alley Strangler'),
    (select id from sets where short_name = 'aer'),
    '52†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silkweaver Elite'),
    (select id from sets where short_name = 'aer'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pendulum of Patterns'),
    (select id from sets where short_name = 'aer'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baral''s Expertise'),
    (select id from sets where short_name = 'aer'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Release the Gremlins'),
    (select id from sets where short_name = 'aer'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reverse Engineer'),
    (select id from sets where short_name = 'aer'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Freejam Regent'),
    (select id from sets where short_name = 'aer'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tezzeret, Master of Metal'),
    (select id from sets where short_name = 'aer'),
    '190',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Secret Salvage'),
    (select id from sets where short_name = 'aer'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mobile Garrison'),
    (select id from sets where short_name = 'aer'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Untethered Express'),
    (select id from sets where short_name = 'aer'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Intimations'),
    (select id from sets where short_name = 'aer'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ornithopter'),
    (select id from sets where short_name = 'aer'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Consulate Turret'),
    (select id from sets where short_name = 'aer'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Siege Modification'),
    (select id from sets where short_name = 'aer'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shielded Aether Thief'),
    (select id from sets where short_name = 'aer'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Runner'),
    (select id from sets where short_name = 'aer'),
    '90',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aegis Automaton'),
    (select id from sets where short_name = 'aer'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foundry Assembler'),
    (select id from sets where short_name = 'aer'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Herder'),
    (select id from sets where short_name = 'aer'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Indomitable Creativity'),
    (select id from sets where short_name = 'aer'),
    '85',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Call for Unity'),
    (select id from sets where short_name = 'aer'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Perilous Predicament'),
    (select id from sets where short_name = 'aer'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yahenni''s Expertise'),
    (select id from sets where short_name = 'aer'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Greenbelt Rampager'),
    (select id from sets where short_name = 'aer'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aether Inspector'),
    (select id from sets where short_name = 'aer'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foundry Hornet'),
    (select id from sets where short_name = 'aer'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dawnfeather Eagle'),
    (select id from sets where short_name = 'aer'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Metallic Rebuke'),
    (select id from sets where short_name = 'aer'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aetherwind Basker'),
    (select id from sets where short_name = 'aer'),
    '104',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wind-Kin Raiders'),
    (select id from sets where short_name = 'aer'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Paradox Engine'),
    (select id from sets where short_name = 'aer'),
    '169',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Peema Aether-Seer'),
    (select id from sets where short_name = 'aer'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aeronaut Admiral'),
    (select id from sets where short_name = 'aer'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Natural Obsolescence'),
    (select id from sets where short_name = 'aer'),
    '118',
    'common'
) 
 on conflict do nothing;
