insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Fury Sliver'),
    (select id from sets where short_name = 'tsp'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Momentary Blink'),
    (select id from sets where short_name = 'tsp'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drudge Reavers'),
    (select id from sets where short_name = 'tsp'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'tsp'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wheel of Fate'),
    (select id from sets where short_name = 'tsp'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sprite Noble'),
    (select id from sets where short_name = 'tsp'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urborg Syphon-Mage'),
    (select id from sets where short_name = 'tsp'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evangelize'),
    (select id from sets where short_name = 'tsp'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benalish Cavalry'),
    (select id from sets where short_name = 'tsp'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gauntlet of Power'),
    (select id from sets where short_name = 'tsp'),
    '255',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pentarch Ward'),
    (select id from sets where short_name = 'tsp'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glass Asp'),
    (select id from sets where short_name = 'tsp'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paradise Plume'),
    (select id from sets where short_name = 'tsp'),
    '260',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clockspinning'),
    (select id from sets where short_name = 'tsp'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stuffy Doll'),
    (select id from sets where short_name = 'tsp'),
    '264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sudden Death'),
    (select id from sets where short_name = 'tsp'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smallpox'),
    (select id from sets where short_name = 'tsp'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sedge Sliver'),
    (select id from sets where short_name = 'tsp'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stonewood Invocation'),
    (select id from sets where short_name = 'tsp'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Axe'),
    (select id from sets where short_name = 'tsp'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feebleness'),
    (select id from sets where short_name = 'tsp'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chromatic Star'),
    (select id from sets where short_name = 'tsp'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Venser''s Sliver'),
    (select id from sets where short_name = 'tsp'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fathom Seer'),
    (select id from sets where short_name = 'tsp'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Children of Korlis'),
    (select id from sets where short_name = 'tsp'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gaze of Justice'),
    (select id from sets where short_name = 'tsp'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psionic Sliver'),
    (select id from sets where short_name = 'tsp'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swarmyard'),
    (select id from sets where short_name = 'tsp'),
    '278',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ophidian Eye'),
    (select id from sets where short_name = 'tsp'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampiric Sliver'),
    (select id from sets where short_name = 'tsp'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'tsp'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saltcrusted Steppe'),
    (select id from sets where short_name = 'tsp'),
    '277',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'tsp'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightshade Assassin'),
    (select id from sets where short_name = 'tsp'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tolarian Sentinel'),
    (select id from sets where short_name = 'tsp'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ignite Memories'),
    (select id from sets where short_name = 'tsp'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sporesower Thallid'),
    (select id from sets where short_name = 'tsp'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Two-Headed Sliver'),
    (select id from sets where short_name = 'tsp'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Screeching Sliver'),
    (select id from sets where short_name = 'tsp'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magus of the Jar'),
    (select id from sets where short_name = 'tsp'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pulmonic Sliver'),
    (select id from sets where short_name = 'tsp'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spirit Loop'),
    (select id from sets where short_name = 'tsp'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weatherseed Totem'),
    (select id from sets where short_name = 'tsp'),
    '268',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Think Twice'),
    (select id from sets where short_name = 'tsp'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Totem'),
    (select id from sets where short_name = 'tsp'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Looter il-Kor'),
    (select id from sets where short_name = 'tsp'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wipe Away'),
    (select id from sets where short_name = 'tsp'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ib Halfheart, Goblin Tactician'),
    (select id from sets where short_name = 'tsp'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brass Gnat'),
    (select id from sets where short_name = 'tsp'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'tsp'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Errant Ephemeron'),
    (select id from sets where short_name = 'tsp'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zealot il-Vec'),
    (select id from sets where short_name = 'tsp'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coal Stoker'),
    (select id from sets where short_name = 'tsp'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scryb Ranger'),
    (select id from sets where short_name = 'tsp'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living End'),
    (select id from sets where short_name = 'tsp'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi, Mage of Zhalfir'),
    (select id from sets where short_name = 'tsp'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temporal Eddy'),
    (select id from sets where short_name = 'tsp'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fool''s Demise'),
    (select id from sets where short_name = 'tsp'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sengir Nosferatu'),
    (select id from sets where short_name = 'tsp'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pull from Eternity'),
    (select id from sets where short_name = 'tsp'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Withering'),
    (select id from sets where short_name = 'tsp'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Verdant Embrace'),
    (select id from sets where short_name = 'tsp'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mindstab'),
    (select id from sets where short_name = 'tsp'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squall Line'),
    (select id from sets where short_name = 'tsp'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'tsp'),
    '290',
    'common'
) ,
(
    (select id from mtgcard where name = 'Assassinate'),
    (select id from sets where short_name = 'tsp'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barbed Shocker'),
    (select id from sets where short_name = 'tsp'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viashino Bladescout'),
    (select id from sets where short_name = 'tsp'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kaervek the Merciless'),
    (select id from sets where short_name = 'tsp'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathspore Thallid'),
    (select id from sets where short_name = 'tsp'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basal Sliver'),
    (select id from sets where short_name = 'tsp'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vesuvan Shapeshifter'),
    (select id from sets where short_name = 'tsp'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foriysian Totem'),
    (select id from sets where short_name = 'tsp'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Return to Dust'),
    (select id from sets where short_name = 'tsp'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trespasser il-Vec'),
    (select id from sets where short_name = 'tsp'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magus of the Mirror'),
    (select id from sets where short_name = 'tsp'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reiterate'),
    (select id from sets where short_name = 'tsp'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drifter il-Dal'),
    (select id from sets where short_name = 'tsp'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Cannonade'),
    (select id from sets where short_name = 'tsp'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grapeshot'),
    (select id from sets where short_name = 'tsp'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thallid Shell-Dweller'),
    (select id from sets where short_name = 'tsp'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undying Rage'),
    (select id from sets where short_name = 'tsp'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prismatic Lens'),
    (select id from sets where short_name = 'tsp'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Crier'),
    (select id from sets where short_name = 'tsp'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liege of the Pit'),
    (select id from sets where short_name = 'tsp'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clockwork Hydra'),
    (select id from sets where short_name = 'tsp'),
    '253',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Havenwood Wurm'),
    (select id from sets where short_name = 'tsp'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Search for Tomorrow'),
    (select id from sets where short_name = 'tsp'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel''s Grace'),
    (select id from sets where short_name = 'tsp'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bogardan Hellkite'),
    (select id from sets where short_name = 'tsp'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Grip'),
    (select id from sets where short_name = 'tsp'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chronatog Totem'),
    (select id from sets where short_name = 'tsp'),
    '252',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thick-Skinned Goblin'),
    (select id from sets where short_name = 'tsp'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Truth or Tale'),
    (select id from sets where short_name = 'tsp'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'tsp'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Firewake Sliver'),
    (select id from sets where short_name = 'tsp'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Psychotic Episode'),
    (select id from sets where short_name = 'tsp'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunder Totem'),
    (select id from sets where short_name = 'tsp'),
    '265',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magus of the Scroll'),
    (select id from sets where short_name = 'tsp'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Errant Doomsayers'),
    (select id from sets where short_name = 'tsp'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Telekinetic Sliver'),
    (select id from sets where short_name = 'tsp'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ground Rift'),
    (select id from sets where short_name = 'tsp'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Traitor''s Clutch'),
    (select id from sets where short_name = 'tsp'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pendelhaven Elder'),
    (select id from sets where short_name = 'tsp'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghostflame Sliver'),
    (select id from sets where short_name = 'tsp'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flickering Spirit'),
    (select id from sets where short_name = 'tsp'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greater Gargadon'),
    (select id from sets where short_name = 'tsp'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Durkwood Baloth'),
    (select id from sets where short_name = 'tsp'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strangling Soot'),
    (select id from sets where short_name = 'tsp'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sulfurous Blast'),
    (select id from sets where short_name = 'tsp'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mwonvuli Acid-Moss'),
    (select id from sets where short_name = 'tsp'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hivestone'),
    (select id from sets where short_name = 'tsp'),
    '256',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sprout'),
    (select id from sets where short_name = 'tsp'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Penumbra Spider'),
    (select id from sets where short_name = 'tsp'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fallen Ideal'),
    (select id from sets where short_name = 'tsp'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chameleon Blur'),
    (select id from sets where short_name = 'tsp'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plague Sliver'),
    (select id from sets where short_name = 'tsp'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bonesplitter Sliver'),
    (select id from sets where short_name = 'tsp'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quilled Sliver'),
    (select id from sets where short_name = 'tsp'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ivory Giant'),
    (select id from sets where short_name = 'tsp'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lotus Bloom'),
    (select id from sets where short_name = 'tsp'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Opaline Sliver'),
    (select id from sets where short_name = 'tsp'),
    '244',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unyaro Bees'),
    (select id from sets where short_name = 'tsp'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Riftwing Cloudskate'),
    (select id from sets where short_name = 'tsp'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plunder'),
    (select id from sets where short_name = 'tsp'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Might of Old Krosa'),
    (select id from sets where short_name = 'tsp'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bewilder'),
    (select id from sets where short_name = 'tsp'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sudden Shock'),
    (select id from sets where short_name = 'tsp'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Watcher Sliver'),
    (select id from sets where short_name = 'tsp'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Celestial Crusader'),
    (select id from sets where short_name = 'tsp'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tendrils of Corruption'),
    (select id from sets where short_name = 'tsp'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vesuva'),
    (select id from sets where short_name = 'tsp'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tromp the Domains'),
    (select id from sets where short_name = 'tsp'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Foriysian Interceptor'),
    (select id from sets where short_name = 'tsp'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurmcalling'),
    (select id from sets where short_name = 'tsp'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viscid Lemures'),
    (select id from sets where short_name = 'tsp'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudchaser Kestrel'),
    (select id from sets where short_name = 'tsp'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plated Pegasus'),
    (select id from sets where short_name = 'tsp'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rift Bolt'),
    (select id from sets where short_name = 'tsp'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pit Keeper'),
    (select id from sets where short_name = 'tsp'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancient Grudge'),
    (select id from sets where short_name = 'tsp'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'tsp'),
    '288',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Skimmer'),
    (select id from sets where short_name = 'tsp'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mogg War Marshal'),
    (select id from sets where short_name = 'tsp'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Might Sliver'),
    (select id from sets where short_name = 'tsp'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Amrou Seekers'),
    (select id from sets where short_name = 'tsp'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molder'),
    (select id from sets where short_name = 'tsp'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brine Elemental'),
    (select id from sets where short_name = 'tsp'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ith, High Arcanist'),
    (select id from sets where short_name = 'tsp'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deep-Sea Kraken'),
    (select id from sets where short_name = 'tsp'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sage of Epityr'),
    (select id from sets where short_name = 'tsp'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nantuko Shaman'),
    (select id from sets where short_name = 'tsp'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of the Holy Nimbus'),
    (select id from sets where short_name = 'tsp'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Opal Guardian'),
    (select id from sets where short_name = 'tsp'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghitu Firebreathing'),
    (select id from sets where short_name = 'tsp'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nether Traitor'),
    (select id from sets where short_name = 'tsp'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thelon of Havenwood'),
    (select id from sets where short_name = 'tsp'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aetherflame Wall'),
    (select id from sets where short_name = 'tsp'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flamecore Elemental'),
    (select id from sets where short_name = 'tsp'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gemstone Caverns'),
    (select id from sets where short_name = 'tsp'),
    '274',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Empty the Warrens'),
    (select id from sets where short_name = 'tsp'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dementia Sliver'),
    (select id from sets where short_name = 'tsp'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stonebrow, Krosan Hero'),
    (select id from sets where short_name = 'tsp'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magus of the Candelabra'),
    (select id from sets where short_name = 'tsp'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Triskelavus'),
    (select id from sets where short_name = 'tsp'),
    '266',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'tsp'),
    '283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primal Forcemage'),
    (select id from sets where short_name = 'tsp'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weathered Bodyguards'),
    (select id from sets where short_name = 'tsp'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fungal Reaches'),
    (select id from sets where short_name = 'tsp'),
    '273',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashcoat Bear'),
    (select id from sets where short_name = 'tsp'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phthisis'),
    (select id from sets where short_name = 'tsp'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tectonic Fiend'),
    (select id from sets where short_name = 'tsp'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jhoira''s Timebug'),
    (select id from sets where short_name = 'tsp'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Herd Gnarr'),
    (select id from sets where short_name = 'tsp'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Walk the Aeons'),
    (select id from sets where short_name = 'tsp'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Avenger'),
    (select id from sets where short_name = 'tsp'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flowstone Channeler'),
    (select id from sets where short_name = 'tsp'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divine Congregation'),
    (select id from sets where short_name = 'tsp'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sudden Spoiling'),
    (select id from sets where short_name = 'tsp'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystical Teachings'),
    (select id from sets where short_name = 'tsp'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dream Stalker'),
    (select id from sets where short_name = 'tsp'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jaya Ballard, Task Mage'),
    (select id from sets where short_name = 'tsp'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ixidron'),
    (select id from sets where short_name = 'tsp'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corpulent Corpse'),
    (select id from sets where short_name = 'tsp'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mangara of Corondor'),
    (select id from sets where short_name = 'tsp'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Paradox Haze'),
    (select id from sets where short_name = 'tsp'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'tsp'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skulking Knight'),
    (select id from sets where short_name = 'tsp'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savage Thallid'),
    (select id from sets where short_name = 'tsp'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keldon Halberdier'),
    (select id from sets where short_name = 'tsp'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sangrophage'),
    (select id from sets where short_name = 'tsp'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gorgon Recluse'),
    (select id from sets where short_name = 'tsp'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'tsp'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thallid Germinator'),
    (select id from sets where short_name = 'tsp'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flagstones of Trokair'),
    (select id from sets where short_name = 'tsp'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pardic Dragon'),
    (select id from sets where short_name = 'tsp'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Awakening'),
    (select id from sets where short_name = 'tsp'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Academy Ruins'),
    (select id from sets where short_name = 'tsp'),
    '269',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sidewinder Sliver'),
    (select id from sets where short_name = 'tsp'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ironclaw Buzzardiers'),
    (select id from sets where short_name = 'tsp'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Factory'),
    (select id from sets where short_name = 'tsp'),
    '280',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Locket of Yesterdays'),
    (select id from sets where short_name = 'tsp'),
    '258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sarpadian Empires, Vol. VII'),
    (select id from sets where short_name = 'tsp'),
    '263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dralnu, Lich Lord'),
    (select id from sets where short_name = 'tsp'),
    '237',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aspect of Mongoose'),
    (select id from sets where short_name = 'tsp'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Durkwood Tracker'),
    (select id from sets where short_name = 'tsp'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dreadship Reef'),
    (select id from sets where short_name = 'tsp'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cancel'),
    (select id from sets where short_name = 'tsp'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'tsp'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lim-Dûl the Necromancer'),
    (select id from sets where short_name = 'tsp'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slipstream Serpent'),
    (select id from sets where short_name = 'tsp'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancestral Vision'),
    (select id from sets where short_name = 'tsp'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Careful Consideration'),
    (select id from sets where short_name = 'tsp'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thelonite Hermit'),
    (select id from sets where short_name = 'tsp'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duskrider Peregrine'),
    (select id from sets where short_name = 'tsp'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Saffi Eriksdotter'),
    (select id from sets where short_name = 'tsp'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gustcloak Cavalier'),
    (select id from sets where short_name = 'tsp'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Restore Balance'),
    (select id from sets where short_name = 'tsp'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Skycutter'),
    (select id from sets where short_name = 'tsp'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fledgling Mawcor'),
    (select id from sets where short_name = 'tsp'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Detainment Spell'),
    (select id from sets where short_name = 'tsp'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fungus Sliver'),
    (select id from sets where short_name = 'tsp'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Draining Whelk'),
    (select id from sets where short_name = 'tsp'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spiketail Drakeling'),
    (select id from sets where short_name = 'tsp'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moonlace'),
    (select id from sets where short_name = 'tsp'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fortune Thief'),
    (select id from sets where short_name = 'tsp'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantom Wurm'),
    (select id from sets where short_name = 'tsp'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Voidmage Husher'),
    (select id from sets where short_name = 'tsp'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Strength in Numbers'),
    (select id from sets where short_name = 'tsp'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blazing Blade Askari'),
    (select id from sets where short_name = 'tsp'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spike Tiller'),
    (select id from sets where short_name = 'tsp'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'tsp'),
    '282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viscerid Deepwalker'),
    (select id from sets where short_name = 'tsp'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Web'),
    (select id from sets where short_name = 'tsp'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pentarch Paladin'),
    (select id from sets where short_name = 'tsp'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'tsp'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molten Slagheap'),
    (select id from sets where short_name = 'tsp'),
    '276',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skittering Monstrosity'),
    (select id from sets where short_name = 'tsp'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Faceless Devourer'),
    (select id from sets where short_name = 'tsp'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crookclaw Transmuter'),
    (select id from sets where short_name = 'tsp'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wormwood Dryad'),
    (select id from sets where short_name = 'tsp'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'tsp'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curse of the Cabal'),
    (select id from sets where short_name = 'tsp'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Castle Raptors'),
    (select id from sets where short_name = 'tsp'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Firemaw Kavu'),
    (select id from sets where short_name = 'tsp'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mishra, Artificer Prodigy'),
    (select id from sets where short_name = 'tsp'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chronosavant'),
    (select id from sets where short_name = 'tsp'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Word of Seizing'),
    (select id from sets where short_name = 'tsp'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spectral Force'),
    (select id from sets where short_name = 'tsp'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dread Return'),
    (select id from sets where short_name = 'tsp'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hypergenesis'),
    (select id from sets where short_name = 'tsp'),
    '201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spinneret Sliver'),
    (select id from sets where short_name = 'tsp'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Endrek Sahr, Master Breeder'),
    (select id from sets where short_name = 'tsp'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'tsp'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'tsp'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kher Keep'),
    (select id from sets where short_name = 'tsp'),
    '275',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eternity Snare'),
    (select id from sets where short_name = 'tsp'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snapback'),
    (select id from sets where short_name = 'tsp'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'D''Avenant Healer'),
    (select id from sets where short_name = 'tsp'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stronghold Overseer'),
    (select id from sets where short_name = 'tsp'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Assembly-Worker'),
    (select id from sets where short_name = 'tsp'),
    '248',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'tsp'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Griffin Guide'),
    (select id from sets where short_name = 'tsp'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jedit''s Dragoons'),
    (select id from sets where short_name = 'tsp'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'tsp'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Outrider en-Kor'),
    (select id from sets where short_name = 'tsp'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'tsp'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Haunting Hymn'),
    (select id from sets where short_name = 'tsp'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thrill of the Hunt'),
    (select id from sets where short_name = 'tsp'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Premature Burial'),
    (select id from sets where short_name = 'tsp'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stormcloud Djinn'),
    (select id from sets where short_name = 'tsp'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Amrou Scout'),
    (select id from sets where short_name = 'tsp'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temporal Isolation'),
    (select id from sets where short_name = 'tsp'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bogardan Rager'),
    (select id from sets where short_name = 'tsp'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scarwood Treefolk'),
    (select id from sets where short_name = 'tsp'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Candles of Leng'),
    (select id from sets where short_name = 'tsp'),
    '250',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scion of the Ur-Dragon'),
    (select id from sets where short_name = 'tsp'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conflagrate'),
    (select id from sets where short_name = 'tsp'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gemhide Sliver'),
    (select id from sets where short_name = 'tsp'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demonic Collusion'),
    (select id from sets where short_name = 'tsp'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Dryad'),
    (select id from sets where short_name = 'tsp'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Greenseeker'),
    (select id from sets where short_name = 'tsp'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trickbind'),
    (select id from sets where short_name = 'tsp'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Basalt Gargoyle'),
    (select id from sets where short_name = 'tsp'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tivadar of Thorn'),
    (select id from sets where short_name = 'tsp'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Norin the Wary'),
    (select id from sets where short_name = 'tsp'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Calciform Pools'),
    (select id from sets where short_name = 'tsp'),
    '270',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harmonic Sliver'),
    (select id from sets where short_name = 'tsp'),
    '240',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cavalry Master'),
    (select id from sets where short_name = 'tsp'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mindlash Sliver'),
    (select id from sets where short_name = 'tsp'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Subterranean Shambler'),
    (select id from sets where short_name = 'tsp'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spell Burst'),
    (select id from sets where short_name = 'tsp'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'tsp'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Call to the Netherworld'),
    (select id from sets where short_name = 'tsp'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cyclopean Giant'),
    (select id from sets where short_name = 'tsp'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shadow Sliver'),
    (select id from sets where short_name = 'tsp'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evil Eye of Urborg'),
    (select id from sets where short_name = 'tsp'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fortify'),
    (select id from sets where short_name = 'tsp'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'tsp'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magus of the Disk'),
    (select id from sets where short_name = 'tsp'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coral Trickster'),
    (select id from sets where short_name = 'tsp'),
    '54',
    'common'
) 
 on conflict do nothing;
