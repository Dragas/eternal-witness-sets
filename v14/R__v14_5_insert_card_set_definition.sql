insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Decree of Annihilation'),
    (select id from sets where short_name = 'v14'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cataclysm'),
    (select id from sets where short_name = 'v14'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Martial Coup'),
    (select id from sets where short_name = 'v14'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = 'v14'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Virtue''s Ruin'),
    (select id from sets where short_name = 'v14'),
    '14',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Upheaval'),
    (select id from sets where short_name = 'v14'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Child of Alara'),
    (select id from sets where short_name = 'v14'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Firespout'),
    (select id from sets where short_name = 'v14'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'v14'),
    '15',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Living Death'),
    (select id from sets where short_name = 'v14'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fracturing Gust'),
    (select id from sets where short_name = 'v14'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rolling Earthquake'),
    (select id from sets where short_name = 'v14'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Terminus'),
    (select id from sets where short_name = 'v14'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Smokestack'),
    (select id from sets where short_name = 'v14'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Burning of Xinye'),
    (select id from sets where short_name = 'v14'),
    '2',
    'mythic'
) 
 on conflict do nothing;
