    insert into mtgcard(name) values ('Decree of Annihilation') on conflict do nothing;
    insert into mtgcard(name) values ('Cataclysm') on conflict do nothing;
    insert into mtgcard(name) values ('Martial Coup') on conflict do nothing;
    insert into mtgcard(name) values ('Armageddon') on conflict do nothing;
    insert into mtgcard(name) values ('Virtue''s Ruin') on conflict do nothing;
    insert into mtgcard(name) values ('Upheaval') on conflict do nothing;
    insert into mtgcard(name) values ('Child of Alara') on conflict do nothing;
    insert into mtgcard(name) values ('Firespout') on conflict do nothing;
    insert into mtgcard(name) values ('Wrath of God') on conflict do nothing;
    insert into mtgcard(name) values ('Living Death') on conflict do nothing;
    insert into mtgcard(name) values ('Fracturing Gust') on conflict do nothing;
    insert into mtgcard(name) values ('Rolling Earthquake') on conflict do nothing;
    insert into mtgcard(name) values ('Terminus') on conflict do nothing;
    insert into mtgcard(name) values ('Smokestack') on conflict do nothing;
    insert into mtgcard(name) values ('Burning of Xinye') on conflict do nothing;
