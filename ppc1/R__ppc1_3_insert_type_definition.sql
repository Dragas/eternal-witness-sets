    insert into types (name) values('Legendary') on conflict do nothing;
    insert into types (name) values('Planeswalker') on conflict do nothing;
    insert into types (name) values('Garruk') on conflict do nothing;
    insert into types (name) values('Token') on conflict do nothing;
    insert into types (name) values('Creature') on conflict do nothing;
    insert into types (name) values('Wolf') on conflict do nothing;
