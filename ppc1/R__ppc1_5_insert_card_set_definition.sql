insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Garruk the Slayer'),
    (select id from sets where short_name = 'ppc1'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'ppc1'),
    'T1',
    'common'
) 
 on conflict do nothing;
