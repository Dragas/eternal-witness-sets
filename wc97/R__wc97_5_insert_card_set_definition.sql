insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Cloud Elemental'),
    (select id from sets where short_name = 'wc97'),
    'pm29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dissipate'),
    (select id from sets where short_name = 'wc97'),
    'pm61sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lhurgoyf'),
    (select id from sets where short_name = 'wc97'),
    'sg309',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hydroblast'),
    (select id from sets where short_name = 'wc97'),
    'js94sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Political Trickery'),
    (select id from sets where short_name = 'wc97'),
    'jk81sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Svend Geertsen Bio'),
    (select id from sets where short_name = 'wc97'),
    'sg0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Impulse'),
    (select id from sets where short_name = 'wc97'),
    'jk34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paul McCabe Bio'),
    (select id from sets where short_name = 'wc97'),
    'pm0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emerald Charm'),
    (select id from sets where short_name = 'wc97'),
    'sg106sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyroblast'),
    (select id from sets where short_name = 'wc97'),
    'pm262sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'wc97'),
    'js413',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abeyance'),
    (select id from sets where short_name = 'wc97'),
    'jk1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'wc97'),
    'jk431',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc97'),
    'js438',
    'common'
) ,
(
    (select id from mtgcard where name = 'River Boa'),
    (select id from sets where short_name = 'wc97'),
    'sg118sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'wc97'),
    'pm77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gemstone Mine'),
    (select id from sets where short_name = 'wc97'),
    'js164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc97'),
    'sg447',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bounty of the Hunt'),
    (select id from sets where short_name = 'wc97'),
    'sg85sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc97'),
    'sg446',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc97'),
    'pm444',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'wc97'),
    'sg299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forsaken Wastes'),
    (select id from sets where short_name = 'wc97'),
    'js125sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyroblast'),
    (select id from sets where short_name = 'wc97'),
    'jk262sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Uktabi Orangutan'),
    (select id from sets where short_name = 'wc97'),
    'sg123sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spectral Bears'),
    (select id from sets where short_name = 'wc97'),
    'sg98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc97'),
    'jk435',
    'common'
) ,
(
    (select id from mtgcard where name = 'Choking Sands'),
    (select id from sets where short_name = 'wc97'),
    'js113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc97'),
    'pm436',
    'common'
) ,
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = 'wc97'),
    'jk242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necratog'),
    (select id from sets where short_name = 'wc97'),
    'js76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc97'),
    'pm435',
    'common'
) ,
(
    (select id from mtgcard where name = 'City of Solitude'),
    (select id from sets where short_name = 'wc97'),
    'sg102sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ebony Charm'),
    (select id from sets where short_name = 'wc97'),
    'js120sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Man-o''-War'),
    (select id from sets where short_name = 'wc97'),
    'js37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quirion Ranger'),
    (select id from sets where short_name = 'wc97'),
    'sg117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abduction'),
    (select id from sets where short_name = 'wc97'),
    'pm30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wildfire Emissary'),
    (select id from sets where short_name = 'wc97'),
    'jk203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disrupt'),
    (select id from sets where short_name = 'wc97'),
    'pm37a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hammer of Bogardan'),
    (select id from sets where short_name = 'wc97'),
    'jk181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc97'),
    'jk443',
    'common'
) ,
(
    (select id from mtgcard where name = 'Janosch Kühn Decklist 1997'),
    (select id from sets where short_name = 'wc97'),
    'jk0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ophidian'),
    (select id from sets where short_name = 'wc97'),
    'pm45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'wc97'),
    'jk26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Honorable Passage'),
    (select id from sets where short_name = 'wc97'),
    'js7sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whirling Dervish'),
    (select id from sets where short_name = 'wc97'),
    'sg341',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc97'),
    'jk434',
    'common'
) ,
(
    (select id from mtgcard where name = 'Underground River'),
    (select id from sets where short_name = 'wc97'),
    'js426',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyrokinesis'),
    (select id from sets where short_name = 'wc97'),
    'pm78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Suq''Ata Lancer'),
    (select id from sets where short_name = 'wc97'),
    'pm96',
    'common'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'wc97'),
    'jk413',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Black'),
    (select id from sets where short_name = 'wc97'),
    'jk17sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crumble'),
    (select id from sets where short_name = 'wc97'),
    'sg287sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = '1997 World Championships Ad'),
    (select id from sets where short_name = 'wc97'),
    '0',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc97'),
    'pm442',
    'common'
) ,
(
    (select id from mtgcard where name = 'Svend Geertsen Decklist'),
    (select id from sets where short_name = 'wc97'),
    'sg0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wildfire Emissary'),
    (select id from sets where short_name = 'wc97'),
    'pm203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'wc97'),
    'jk77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc97'),
    'jk436',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = 'wc97'),
    'jk223sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc97'),
    'pm445',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frenetic Efreet'),
    (select id from sets where short_name = 'wc97'),
    'jk264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hydroblast'),
    (select id from sets where short_name = 'wc97'),
    'pm94sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harvest Wurm'),
    (select id from sets where short_name = 'wc97'),
    'sg130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of Stromgald'),
    (select id from sets where short_name = 'wc97'),
    'js171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = 'wc97'),
    'pm242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghazbán Ogre'),
    (select id from sets where short_name = 'wc97'),
    'sg298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nekrataal'),
    (select id from sets where short_name = 'wc97'),
    'js66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc97'),
    'pm437',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Furnace'),
    (select id from sets where short_name = 'wc97'),
    'pm155sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = 'wc97'),
    'js242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyroblast'),
    (select id from sets where short_name = 'wc97'),
    'js262sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shadow Guildmage'),
    (select id from sets where short_name = 'wc97'),
    'js140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Knight'),
    (select id from sets where short_name = 'wc97'),
    'js143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyrokinesis'),
    (select id from sets where short_name = 'wc97'),
    'pm78sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jolrael''s Centaur'),
    (select id from sets where short_name = 'wc97'),
    'sg222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frenetic Efreet'),
    (select id from sets where short_name = 'wc97'),
    'pm264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Contagion'),
    (select id from sets where short_name = 'wc97'),
    'js45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'wc97'),
    'jk54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fallen Askari'),
    (select id from sets where short_name = 'wc97'),
    'js59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Force of Will'),
    (select id from sets where short_name = 'wc97'),
    'pm28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc97'),
    'sg449',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc97'),
    'jk445',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc97'),
    'jk442',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc97'),
    'js440',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undiscovered Paradise'),
    (select id from sets where short_name = 'wc97'),
    'js167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc97'),
    'js439',
    'common'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'wc97'),
    'pm413',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc97'),
    'pm434',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disintegrate'),
    (select id from sets where short_name = 'wc97'),
    'jk219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heart of Yavimaya'),
    (select id from sets where short_name = 'wc97'),
    'sg138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = 'wc97'),
    'pm391sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dystopia'),
    (select id from sets where short_name = 'wc97'),
    'js47sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blank Card'),
    (select id from sets where short_name = 'wc97'),
    '00',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pillage'),
    (select id from sets where short_name = 'wc97'),
    'pm76sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serrated Arrows'),
    (select id from sets where short_name = 'wc97'),
    'pm110sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc97'),
    'jk444',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'wc97'),
    'jk433',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of the Mists'),
    (select id from sets where short_name = 'wc97'),
    'pm36sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'wc97'),
    'jk26sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc97'),
    'pm443',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exile'),
    (select id from sets where short_name = 'wc97'),
    'js3sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rogue Elephant'),
    (select id from sets where short_name = 'wc97'),
    'sg139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bounty of the Hunt'),
    (select id from sets where short_name = 'wc97'),
    'sg85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc97'),
    'sg448',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pillage'),
    (select id from sets where short_name = 'wc97'),
    'jk76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = 'wc97'),
    'js223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'wc97'),
    'jk430',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc97'),
    'js441',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Stone'),
    (select id from sets where short_name = 'wc97'),
    'pm153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paul McCabe Decklist'),
    (select id from sets where short_name = 'wc97'),
    'pm0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Janosch Kühn Bio 1997'),
    (select id from sets where short_name = 'wc97'),
    'jk0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'wc97'),
    'jk432',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undiscovered Paradise'),
    (select id from sets where short_name = 'wc97'),
    'pm167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc97'),
    'jk437',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whirling Dervish'),
    (select id from sets where short_name = 'wc97'),
    'sg341sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thawing Glaciers'),
    (select id from sets where short_name = 'wc97'),
    'jk144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sulfurous Springs'),
    (select id from sets where short_name = 'wc97'),
    'js424',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Outpost'),
    (select id from sets where short_name = 'wc97'),
    'jk139sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'wc97'),
    'js26sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fyndhorn Elves'),
    (select id from sets where short_name = 'wc97'),
    'sg244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Man-o''-War'),
    (select id from sets where short_name = 'wc97'),
    'pm37b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undiscovered Paradise'),
    (select id from sets where short_name = 'wc97'),
    'jk167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uktabi Orangutan'),
    (select id from sets where short_name = 'wc97'),
    'sg123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Force of Will'),
    (select id from sets where short_name = 'wc97'),
    'jk28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Uktabi Orangutan'),
    (select id from sets where short_name = 'wc97'),
    'js123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disintegrate'),
    (select id from sets where short_name = 'wc97'),
    'pm219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = 'wc97'),
    'jk20sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winter Orb'),
    (select id from sets where short_name = 'wc97'),
    'sg408',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jakub Šlemr Bio'),
    (select id from sets where short_name = 'wc97'),
    'js0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jakub Šlemr Decklist'),
    (select id from sets where short_name = 'wc97'),
    'js0b',
    'common'
) 
 on conflict do nothing;
