insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Booster Tutor'),
    (select id from sets where short_name = 'und'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Haberdasher'),
    (select id from sets where short_name = 'und'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin S.W.A.T. Team'),
    (select id from sets where short_name = 'und'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magic Word'),
    (select id from sets where short_name = 'und'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'und'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'und'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dirty Rat'),
    (select id from sets where short_name = 'und'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'und'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'und'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Water Gun Balloon Game'),
    (select id from sets where short_name = 'und'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hoisted Hireling'),
    (select id from sets where short_name = 'und'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'B-I-N-G-O'),
    (select id from sets where short_name = 'und'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duh'),
    (select id from sets where short_name = 'und'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Painiac'),
    (select id from sets where short_name = 'und'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Common Courtesy'),
    (select id from sets where short_name = 'und'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Timmy, Power Gamer'),
    (select id from sets where short_name = 'und'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frankie Peanuts'),
    (select id from sets where short_name = 'und'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wild Crocodile'),
    (select id from sets where short_name = 'und'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bronze Calendar'),
    (select id from sets where short_name = 'und'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spirit of the Season'),
    (select id from sets where short_name = 'und'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Common Iguana'),
    (select id from sets where short_name = 'und'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infernal Spawn of Evil'),
    (select id from sets where short_name = 'und'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Look at Me, I''m the DCI'),
    (select id from sets where short_name = 'und'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infernius Spawnington III, Esq.'),
    (select id from sets where short_name = 'und'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strategy, Schmategy'),
    (select id from sets where short_name = 'und'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Richard Garfield, Ph.D.'),
    (select id from sets where short_name = 'und'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slaying Mantis'),
    (select id from sets where short_name = 'und'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stinging Scorpion'),
    (select id from sets where short_name = 'und'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Six-y Beast'),
    (select id from sets where short_name = 'und'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'und'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'und'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boomstacker'),
    (select id from sets where short_name = 'und'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ordinary Pony'),
    (select id from sets where short_name = 'und'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Humming-'),
    (select id from sets where short_name = 'und'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Fortune'),
    (select id from sets where short_name = 'und'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rock Lobster'),
    (select id from sets where short_name = 'und'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'B.O.B. (Bevy of Beebles)'),
    (select id from sets where short_name = 'und'),
    '18',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Krark''s Other Thumb'),
    (select id from sets where short_name = 'und'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Entirely Normal Armchair'),
    (select id from sets where short_name = 'und'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight of the Hokey Pokey'),
    (select id from sets where short_name = 'und'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avatar of Me'),
    (select id from sets where short_name = 'und'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mother Kangaroo'),
    (select id from sets where short_name = 'und'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'und'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snickering Squirrel'),
    (select id from sets where short_name = 'und'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alexander Clamilton'),
    (select id from sets where short_name = 'und'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'AWOL'),
    (select id from sets where short_name = 'und'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blast from the Past'),
    (select id from sets where short_name = 'und'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Paper Tiger'),
    (select id from sets where short_name = 'und'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Growth Spurt'),
    (select id from sets where short_name = 'und'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pointy Finger of Doom'),
    (select id from sets where short_name = 'und'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Topsy Turvy'),
    (select id from sets where short_name = 'und'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Look at Me, I''m R&D'),
    (select id from sets where short_name = 'und'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Tutor'),
    (select id from sets where short_name = 'und'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flavor Judge'),
    (select id from sets where short_name = 'und'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Half-Squirrel, Half-'),
    (select id from sets where short_name = 'und'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Syr Cadian, Knight Owl'),
    (select id from sets where short_name = 'und'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bat-'),
    (select id from sets where short_name = 'und'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Impersonators'),
    (select id from sets where short_name = 'und'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Surgeon General Commander'),
    (select id from sets where short_name = 'und'),
    '72',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rings a Bell'),
    (select id from sets where short_name = 'und'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sword of Dungeons & Dragons'),
    (select id from sets where short_name = 'und'),
    '84',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Abstract Iguanart'),
    (select id from sets where short_name = 'und'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jack-in-the-Mox'),
    (select id from sets where short_name = 'und'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'und'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Super-Duper Death Ray'),
    (select id from sets where short_name = 'und'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Free-Range Chicken'),
    (select id from sets where short_name = 'und'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emcee'),
    (select id from sets where short_name = 'und'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Squirrel Farm'),
    (select id from sets where short_name = 'und'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Old Fogey'),
    (select id from sets where short_name = 'und'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Adorable Kitten'),
    (select id from sets where short_name = 'und'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carnivorous Death-Parrot'),
    (select id from sets where short_name = 'und'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'GO TO JAIL'),
    (select id from sets where short_name = 'und'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stet, Draconic Proofreader'),
    (select id from sets where short_name = 'und'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Underdome'),
    (select id from sets where short_name = 'und'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enter the Dungeon'),
    (select id from sets where short_name = 'und'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Poultrygeist'),
    (select id from sets where short_name = 'und'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jumbo Imp'),
    (select id from sets where short_name = 'und'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Old Guard'),
    (select id from sets where short_name = 'und'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infernal Spawn of Infernal Spawn of Evil'),
    (select id from sets where short_name = 'und'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inhumaniac'),
    (select id from sets where short_name = 'und'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'und'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pippa, Duchess of Dice'),
    (select id from sets where short_name = 'und'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yet Another Aether Vortex'),
    (select id from sets where short_name = 'und'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Acornelia, Fashionable Filcher'),
    (select id from sets where short_name = 'und'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cheatyface'),
    (select id from sets where short_name = 'und'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mer Man'),
    (select id from sets where short_name = 'und'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'und'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strutting Turkey'),
    (select id from sets where short_name = 'und'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chicken à la King'),
    (select id from sets where short_name = 'und'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skull Saucer'),
    (select id from sets where short_name = 'und'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Johnny, Combo Player'),
    (select id from sets where short_name = 'und'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infinity Elemental'),
    (select id from sets where short_name = 'und'),
    '54',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scissors Lizard'),
    (select id from sets where short_name = 'und'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time Out'),
    (select id from sets where short_name = 'und'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Staying Power'),
    (select id from sets where short_name = 'und'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Who // What // When // Where // Why'),
    (select id from sets where short_name = 'und'),
    '75',
    'rare'
) 
 on conflict do nothing;
