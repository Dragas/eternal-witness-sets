insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'I Delight in Your Convulsions'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Look Skyward and Despair'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'May Civilization Collapse'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'All in Good Time'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'My Undead Horde Awakens'),
        (select types.id from types where name = 'Ongoing')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'My Undead Horde Awakens'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'My Wish Is Your Command'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'I Know All, I See All'),
        (select types.id from types where name = 'Ongoing')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'I Know All, I See All'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Behold the Power of Destruction'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Your Will Is Not Your Own'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Into the Earthen Maw'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Fate of the Flammable'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Only Blood Ends Your Nightmares'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Embrace My Diabolical Vision'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Your Puny Minds Cannot Fathom'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tooth, Claw, and Tail'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Surrender Your Thoughts'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rotted Ones, Lay Siege'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nothing Can Stop Me Now'),
        (select types.id from types where name = 'Ongoing')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nothing Can Stop Me Now'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Iron Guardian Stirs'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Pieces Are Coming Together'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Approach My Molten Realm'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Every Hope Shall Vanish'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ignite the Cloneforge!'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Realms Befitting My Majesty'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nature Demands an Offering'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nature Shields Its Own'),
        (select types.id from types where name = 'Ongoing')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nature Shields Its Own'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Which of You Burns Brightest?'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Introductions Are in Order'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Very Soil Shall Shake'),
        (select types.id from types where name = 'Ongoing')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Very Soil Shall Shake'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Your Fate Is Thrice Sealed'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mortal Flesh Is Weak'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Roots of All Evil'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'All Shall Smolder in My Wake'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'A Display of My Dark Power'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Every Last Vestige Shall Rot'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Evil Comes to Fruition'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'I Bask in Your Silent Awe'),
        (select types.id from types where name = 'Ongoing')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'I Bask in Your Silent Awe'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'I Call on the Ancient Magics'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dance, Pathetic Marionette'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'My Genius Knows No Bounds'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'My Crushing Masterstroke'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Dead Shall Serve'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Know Naught but Fire'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Choose Your Champion'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Feed the Machine'),
        (select types.id from types where name = 'Scheme')
    ) 
 on conflict do nothing;
