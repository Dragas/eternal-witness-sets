insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'I Delight in Your Convulsions'),
    (select id from sets where short_name = 'oarc'),
    '17★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Look Skyward and Despair'),
    (select id from sets where short_name = 'oarc'),
    '24★',
    'common'
) ,
(
    (select id from mtgcard where name = 'May Civilization Collapse'),
    (select id from sets where short_name = 'oarc'),
    '25★',
    'common'
) ,
(
    (select id from mtgcard where name = 'All in Good Time'),
    (select id from sets where short_name = 'oarc'),
    '1★',
    'common'
) ,
(
    (select id from mtgcard where name = 'My Undead Horde Awakens'),
    (select id from sets where short_name = 'oarc'),
    '29★',
    'common'
) ,
(
    (select id from mtgcard where name = 'My Wish Is Your Command'),
    (select id from sets where short_name = 'oarc'),
    '30★',
    'common'
) ,
(
    (select id from mtgcard where name = 'I Know All, I See All'),
    (select id from sets where short_name = 'oarc'),
    '18★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Behold the Power of Destruction'),
    (select id from sets where short_name = 'oarc'),
    '4★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Your Will Is Not Your Own'),
    (select id from sets where short_name = 'oarc'),
    '45★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Into the Earthen Maw'),
    (select id from sets where short_name = 'oarc'),
    '20★',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Fate of the Flammable'),
    (select id from sets where short_name = 'oarc'),
    '13★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Only Blood Ends Your Nightmares'),
    (select id from sets where short_name = 'oarc'),
    '34★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Embrace My Diabolical Vision'),
    (select id from sets where short_name = 'oarc'),
    '9★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Your Puny Minds Cannot Fathom'),
    (select id from sets where short_name = 'oarc'),
    '44★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tooth, Claw, and Tail'),
    (select id from sets where short_name = 'oarc'),
    '40★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Surrender Your Thoughts'),
    (select id from sets where short_name = 'oarc'),
    '39★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rotted Ones, Lay Siege'),
    (select id from sets where short_name = 'oarc'),
    '38★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nothing Can Stop Me Now'),
    (select id from sets where short_name = 'oarc'),
    '33★',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Iron Guardian Stirs'),
    (select id from sets where short_name = 'oarc'),
    '22★',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Pieces Are Coming Together'),
    (select id from sets where short_name = 'oarc'),
    '35★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Approach My Molten Realm'),
    (select id from sets where short_name = 'oarc'),
    '3★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Every Hope Shall Vanish'),
    (select id from sets where short_name = 'oarc'),
    '10★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ignite the Cloneforge!'),
    (select id from sets where short_name = 'oarc'),
    '19★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Realms Befitting My Majesty'),
    (select id from sets where short_name = 'oarc'),
    '36★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nature Demands an Offering'),
    (select id from sets where short_name = 'oarc'),
    '31★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nature Shields Its Own'),
    (select id from sets where short_name = 'oarc'),
    '32★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Which of You Burns Brightest?'),
    (select id from sets where short_name = 'oarc'),
    '42★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Introductions Are in Order'),
    (select id from sets where short_name = 'oarc'),
    '21★',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Very Soil Shall Shake'),
    (select id from sets where short_name = 'oarc'),
    '41★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Your Fate Is Thrice Sealed'),
    (select id from sets where short_name = 'oarc'),
    '43★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mortal Flesh Is Weak'),
    (select id from sets where short_name = 'oarc'),
    '26★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Roots of All Evil'),
    (select id from sets where short_name = 'oarc'),
    '37★',
    'common'
) ,
(
    (select id from mtgcard where name = 'All Shall Smolder in My Wake'),
    (select id from sets where short_name = 'oarc'),
    '2★',
    'common'
) ,
(
    (select id from mtgcard where name = 'A Display of My Dark Power'),
    (select id from sets where short_name = 'oarc'),
    '8★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Every Last Vestige Shall Rot'),
    (select id from sets where short_name = 'oarc'),
    '11★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evil Comes to Fruition'),
    (select id from sets where short_name = 'oarc'),
    '12★',
    'common'
) ,
(
    (select id from mtgcard where name = 'I Bask in Your Silent Awe'),
    (select id from sets where short_name = 'oarc'),
    '15★',
    'common'
) ,
(
    (select id from mtgcard where name = 'I Call on the Ancient Magics'),
    (select id from sets where short_name = 'oarc'),
    '16★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dance, Pathetic Marionette'),
    (select id from sets where short_name = 'oarc'),
    '6★',
    'common'
) ,
(
    (select id from mtgcard where name = 'My Genius Knows No Bounds'),
    (select id from sets where short_name = 'oarc'),
    '28★',
    'common'
) ,
(
    (select id from mtgcard where name = 'My Crushing Masterstroke'),
    (select id from sets where short_name = 'oarc'),
    '27★',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Dead Shall Serve'),
    (select id from sets where short_name = 'oarc'),
    '7★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Know Naught but Fire'),
    (select id from sets where short_name = 'oarc'),
    '23★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Choose Your Champion'),
    (select id from sets where short_name = 'oarc'),
    '5★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feed the Machine'),
    (select id from sets where short_name = 'oarc'),
    '14★',
    'common'
) 
 on conflict do nothing;
