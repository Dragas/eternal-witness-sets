insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tddh'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Griffin'),
    (select id from sets where short_name = 'tddh'),
    '1',
    'common'
) 
 on conflict do nothing;
