insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Gabriel Nassif Bio'),
    (select id from sets where short_name = 'wc04'),
    'gn0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aeo Paquette Bio'),
    (select id from sets where short_name = 'wc04'),
    'ap0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gabriel Nassif Decklist'),
    (select id from sets where short_name = 'wc04'),
    'gn0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc04'),
    'jn347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Condescend'),
    (select id from sets where short_name = 'wc04'),
    'mb27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'wc04'),
    'gn33sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seething Song'),
    (select id from sets where short_name = 'wc04'),
    'ap104sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Annul'),
    (select id from sets where short_name = 'wc04'),
    'ap29sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'wc04'),
    'gn331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Great Furnace'),
    (select id from sets where short_name = 'wc04'),
    'mb282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windswept Heath'),
    (select id from sets where short_name = 'wc04'),
    'jn328',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pentad Prism'),
    (select id from sets where short_name = 'wc04'),
    'mb143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudpost'),
    (select id from sets where short_name = 'wc04'),
    'gn280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Incubator'),
    (select id from sets where short_name = 'wc04'),
    'mb212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'wc04'),
    'jn333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Julien Nuijten Decklist'),
    (select id from sets where short_name = 'wc04'),
    'jn0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fabricate'),
    (select id from sets where short_name = 'wc04'),
    'mb35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Talisman of Dominance'),
    (select id from sets where short_name = 'wc04'),
    'mb253',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Temple of the False God'),
    (select id from sets where short_name = 'wc04'),
    'gn143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disciple of the Vault'),
    (select id from sets where short_name = 'wc04'),
    'ap62a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aeo Paquette Decklist'),
    (select id from sets where short_name = 'wc04'),
    'ap0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thoughtcast'),
    (select id from sets where short_name = 'wc04'),
    'mb54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akroma''s Vengeance'),
    (select id from sets where short_name = 'wc04'),
    'gn2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Welding Jar'),
    (select id from sets where short_name = 'wc04'),
    'ap274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rude Awakening'),
    (select id from sets where short_name = 'wc04'),
    'jn92sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = 'wc04'),
    'jn274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Leak'),
    (select id from sets where short_name = 'wc04'),
    'mb89sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Great Furnace'),
    (select id from sets where short_name = 'wc04'),
    'ap282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc04'),
    'gn337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'wc04'),
    'gn58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exalted Angel'),
    (select id from sets where short_name = 'wc04'),
    'gn28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viridian Shaman'),
    (select id from sets where short_name = 'wc04'),
    'jn139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Decree of Justice'),
    (select id from sets where short_name = 'wc04'),
    'jn8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frogmite'),
    (select id from sets where short_name = 'wc04'),
    'ap172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Secluded Steppe'),
    (select id from sets where short_name = 'wc04'),
    'jn324',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wayfarer''s Bauble'),
    (select id from sets where short_name = 'wc04'),
    'gn165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ornithopter'),
    (select id from sets where short_name = 'wc04'),
    'ap224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc04'),
    'jn348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Somber Hoverguard'),
    (select id from sets where short_name = 'wc04'),
    'ap51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancient Den'),
    (select id from sets where short_name = 'wc04'),
    'mb278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'wc04'),
    'mb60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquil Thicket'),
    (select id from sets where short_name = 'wc04'),
    'jn326',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manuel Bevand Decklist'),
    (select id from sets where short_name = 'wc04'),
    'mb0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plow Under'),
    (select id from sets where short_name = 'wc04'),
    'jn272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blank Card'),
    (select id from sets where short_name = 'wc04'),
    '00',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seat of the Synod'),
    (select id from sets where short_name = 'wc04'),
    'mb283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc04'),
    'gn335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krark-Clan Ironworks'),
    (select id from sets where short_name = 'wc04'),
    'mb134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Leak'),
    (select id from sets where short_name = 'wc04'),
    'gn89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc04'),
    'jn350',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eternal Dragon'),
    (select id from sets where short_name = 'wc04'),
    'jn12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scrabbling Claws'),
    (select id from sets where short_name = 'wc04'),
    'gn237sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Julien Nuijten Bio'),
    (select id from sets where short_name = 'wc04'),
    'jn0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stifle'),
    (select id from sets where short_name = 'wc04'),
    'gn52sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'wc04'),
    'jn332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eternal Dragon'),
    (select id from sets where short_name = 'wc04'),
    'gn12a',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scrabbling Claws'),
    (select id from sets where short_name = 'wc04'),
    'jn237sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oxidize'),
    (select id from sets where short_name = 'wc04'),
    'jn79sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serum Visions'),
    (select id from sets where short_name = 'wc04'),
    'ap36sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rewind'),
    (select id from sets where short_name = 'wc04'),
    'gn96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Astral Slide'),
    (select id from sets where short_name = 'wc04'),
    'jn4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'wc04'),
    'gn332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Condescend'),
    (select id from sets where short_name = 'wc04'),
    'gn27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Annul'),
    (select id from sets where short_name = 'wc04'),
    'gn29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wing Shards'),
    (select id from sets where short_name = 'wc04'),
    'jn25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcbound Worker'),
    (select id from sets where short_name = 'wc04'),
    'ap104a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Renewed Faith'),
    (select id from sets where short_name = 'wc04'),
    'jn50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vault of Whispers'),
    (select id from sets where short_name = 'wc04'),
    'ap286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vault of Whispers'),
    (select id from sets where short_name = 'wc04'),
    'mb286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thoughtcast'),
    (select id from sets where short_name = 'wc04'),
    'ap54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relic Barrier'),
    (select id from sets where short_name = 'wc04'),
    'gn147sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shrapnel Blast'),
    (select id from sets where short_name = 'wc04'),
    'ap106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Furnace Dragon'),
    (select id from sets where short_name = 'wc04'),
    'mb62sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chrome Mox'),
    (select id from sets where short_name = 'wc04'),
    'ap152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tree of Tales'),
    (select id from sets where short_name = 'wc04'),
    'mb285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glimmervoid'),
    (select id from sets where short_name = 'wc04'),
    'ap281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Decree of Justice'),
    (select id from sets where short_name = 'wc04'),
    'gn8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serum Visions'),
    (select id from sets where short_name = 'wc04'),
    'mb36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = 'wc04'),
    'jn13sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eternal Witness'),
    (select id from sets where short_name = 'wc04'),
    'jn86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seething Song'),
    (select id from sets where short_name = 'wc04'),
    'mb104sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Furnace Dragon'),
    (select id from sets where short_name = 'wc04'),
    'ap62sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akroma''s Vengeance'),
    (select id from sets where short_name = 'wc04'),
    'jn2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flooded Strand'),
    (select id from sets where short_name = 'wc04'),
    'gn316',
    'rare'
) ,
(
    (select id from mtgcard where name = '2004 World Championships Ad'),
    (select id from sets where short_name = 'wc04'),
    '0',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'wc04'),
    'jn334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Charbelcher'),
    (select id from sets where short_name = 'wc04'),
    'mb176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chrome Mox'),
    (select id from sets where short_name = 'wc04'),
    'mb152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcbound Ravager'),
    (select id from sets where short_name = 'wc04'),
    'ap100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thirst for Knowledge'),
    (select id from sets where short_name = 'wc04'),
    'gn53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyroclasm'),
    (select id from sets where short_name = 'wc04'),
    'mb210sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cranial Plating'),
    (select id from sets where short_name = 'wc04'),
    'ap113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc04'),
    'gn336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blinkmoth Nexus'),
    (select id from sets where short_name = 'wc04'),
    'ap163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thirst for Knowledge'),
    (select id from sets where short_name = 'wc04'),
    'mb53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'wc04'),
    'gn333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talisman of Progress'),
    (select id from sets where short_name = 'wc04'),
    'mb256',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seat of the Synod'),
    (select id from sets where short_name = 'wc04'),
    'ap283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manuel Bevand Bio'),
    (select id from sets where short_name = 'wc04'),
    'mb0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'wc04'),
    'jn58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plow Under'),
    (select id from sets where short_name = 'wc04'),
    'jn272sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darksteel Citadel'),
    (select id from sets where short_name = 'wc04'),
    'mb164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Purge'),
    (select id from sets where short_name = 'wc04'),
    'gn12sbb',
    'uncommon'
) 
 on conflict do nothing;
