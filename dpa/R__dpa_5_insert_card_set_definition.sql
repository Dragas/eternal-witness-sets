insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = 'dpa'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Sky Raider'),
    (select id from sets where short_name = 'dpa'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Consume Spirit'),
    (select id from sets where short_name = 'dpa'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drove of Elves'),
    (select id from sets where short_name = 'dpa'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jagged-Scar Archers'),
    (select id from sets where short_name = 'dpa'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = 'dpa'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Troll Ascetic'),
    (select id from sets where short_name = 'dpa'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rage Reflection'),
    (select id from sets where short_name = 'dpa'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Talara''s Battalion'),
    (select id from sets where short_name = 'dpa'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Natural Spring'),
    (select id from sets where short_name = 'dpa'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Severed Legion'),
    (select id from sets where short_name = 'dpa'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dpa'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blanchwood Armor'),
    (select id from sets where short_name = 'dpa'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evacuation'),
    (select id from sets where short_name = 'dpa'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'dpa'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Megrim'),
    (select id from sets where short_name = 'dpa'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Eulogist'),
    (select id from sets where short_name = 'dpa'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravenous Rats'),
    (select id from sets where short_name = 'dpa'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hill Giant'),
    (select id from sets where short_name = 'dpa'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = 'dpa'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Imperious Perfect'),
    (select id from sets where short_name = 'dpa'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'dpa'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = 'dpa'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurm''s Tooth'),
    (select id from sets where short_name = 'dpa'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Denizen of the Deep'),
    (select id from sets where short_name = 'dpa'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duskdale Wurm'),
    (select id from sets where short_name = 'dpa'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dpa'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earth Elemental'),
    (select id from sets where short_name = 'dpa'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nature''s Spiral'),
    (select id from sets where short_name = 'dpa'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = 'dpa'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Control'),
    (select id from sets where short_name = 'dpa'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = 'dpa'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runeclaw Bear'),
    (select id from sets where short_name = 'dpa'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'River Boa'),
    (select id from sets where short_name = 'dpa'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Warrior'),
    (select id from sets where short_name = 'dpa'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enrage'),
    (select id from sets where short_name = 'dpa'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Essence Scatter'),
    (select id from sets where short_name = 'dpa'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Shatter'),
    (select id from sets where short_name = 'dpa'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Act of Treason'),
    (select id from sets where short_name = 'dpa'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cinder Pyromancer'),
    (select id from sets where short_name = 'dpa'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elven Riders'),
    (select id from sets where short_name = 'dpa'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Wood'),
    (select id from sets where short_name = 'dpa'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon''s Horn'),
    (select id from sets where short_name = 'dpa'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unholy Strength'),
    (select id from sets where short_name = 'dpa'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molimo, Maro-Sorcerer'),
    (select id from sets where short_name = 'dpa'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Counterbore'),
    (select id from sets where short_name = 'dpa'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = 'dpa'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'dpa'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'dpa'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'dpa'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howl of the Night Pack'),
    (select id from sets where short_name = 'dpa'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drudge Skeletons'),
    (select id from sets where short_name = 'dpa'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloud Sprite'),
    (select id from sets where short_name = 'dpa'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prodigal Pyromancer'),
    (select id from sets where short_name = 'dpa'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dpa'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mahamoti Djinn'),
    (select id from sets where short_name = 'dpa'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dpa'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coat of Arms'),
    (select id from sets where short_name = 'dpa'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'dpa'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eyeblight''s Ending'),
    (select id from sets where short_name = 'dpa'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dusk Imp'),
    (select id from sets where short_name = 'dpa'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boomerang'),
    (select id from sets where short_name = 'dpa'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Underworld Dreams'),
    (select id from sets where short_name = 'dpa'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Visionary'),
    (select id from sets where short_name = 'dpa'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abyssal Specter'),
    (select id from sets where short_name = 'dpa'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Immaculate Magistrate'),
    (select id from sets where short_name = 'dpa'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crowd of Cinders'),
    (select id from sets where short_name = 'dpa'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = 'dpa'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = 'dpa'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overrun'),
    (select id from sets where short_name = 'dpa'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Spears'),
    (select id from sets where short_name = 'dpa'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'dpa'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Warrior'),
    (select id from sets where short_name = 'dpa'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'dpa'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = 'dpa'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dpa'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Herald'),
    (select id from sets where short_name = 'dpa'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodmark Mentor'),
    (select id from sets where short_name = 'dpa'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Civic Wayfinder'),
    (select id from sets where short_name = 'dpa'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cancel'),
    (select id from sets where short_name = 'dpa'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loxodon Warhammer'),
    (select id from sets where short_name = 'dpa'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vigor'),
    (select id from sets where short_name = 'dpa'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spined Wurm'),
    (select id from sets where short_name = 'dpa'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'dpa'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snapping Drake'),
    (select id from sets where short_name = 'dpa'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Champion'),
    (select id from sets where short_name = 'dpa'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kraken''s Eye'),
    (select id from sets where short_name = 'dpa'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Furnace of Rath'),
    (select id from sets where short_name = 'dpa'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Promenade'),
    (select id from sets where short_name = 'dpa'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kamahl, Pit Fighter'),
    (select id from sets where short_name = 'dpa'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verdant Force'),
    (select id from sets where short_name = 'dpa'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deluge'),
    (select id from sets where short_name = 'dpa'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trained Armodon'),
    (select id from sets where short_name = 'dpa'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dpa'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Banefire'),
    (select id from sets where short_name = 'dpa'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mortivore'),
    (select id from sets where short_name = 'dpa'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moonglove Winnower'),
    (select id from sets where short_name = 'dpa'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lys Alana Huntmaster'),
    (select id from sets where short_name = 'dpa'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Piker'),
    (select id from sets where short_name = 'dpa'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dpa'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon''s Claw'),
    (select id from sets where short_name = 'dpa'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Roughshod Mentor'),
    (select id from sets where short_name = 'dpa'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Spring'),
    (select id from sets where short_name = 'dpa'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Elemental'),
    (select id from sets where short_name = 'dpa'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'dpa'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thieving Magpie'),
    (select id from sets where short_name = 'dpa'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Rack'),
    (select id from sets where short_name = 'dpa'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ascendant Evincar'),
    (select id from sets where short_name = 'dpa'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blaze'),
    (select id from sets where short_name = 'dpa'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Essence Drain'),
    (select id from sets where short_name = 'dpa'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'dpa'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greenweaver Druid'),
    (select id from sets where short_name = 'dpa'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dpa'),
    '109',
    'common'
) 
 on conflict do nothing;
