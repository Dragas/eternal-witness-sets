insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Finale of Promise'),
    (select id from sets where short_name = 'pwar'),
    '127s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mizzium Tank'),
    (select id from sets where short_name = 'pwar'),
    '138s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Finale of Devastation'),
    (select id from sets where short_name = 'pwar'),
    '160s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Time Wipe'),
    (select id from sets where short_name = 'pwar'),
    '223p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vraska, Swarm''s Eminence'),
    (select id from sets where short_name = 'pwar'),
    '236s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karn''s Bastion'),
    (select id from sets where short_name = 'pwar'),
    '248s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feather, the Redeemed'),
    (select id from sets where short_name = 'pwar'),
    '197p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Elderspell'),
    (select id from sets where short_name = 'pwar'),
    '89p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ugin, the Ineffable'),
    (select id from sets where short_name = 'pwar'),
    '2s★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tomik, Distinguished Advokist'),
    (select id from sets where short_name = 'pwar'),
    '34p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fblthp, the Lost'),
    (select id from sets where short_name = 'pwar'),
    '50p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saheeli, Sublime Artificer'),
    (select id from sets where short_name = 'pwar'),
    '234s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ignite the Beacon'),
    (select id from sets where short_name = 'pwar'),
    '18s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mizzium Tank'),
    (select id from sets where short_name = 'pwar'),
    '138p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra, Fire Artisan'),
    (select id from sets where short_name = 'pwar'),
    '119p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Living Twister'),
    (select id from sets where short_name = 'pwar'),
    '203p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kasmina, Enigmatic Mentor'),
    (select id from sets where short_name = 'pwar'),
    '56s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'God-Eternal Bontu'),
    (select id from sets where short_name = 'pwar'),
    '92s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Soul Diviner'),
    (select id from sets where short_name = 'pwar'),
    '218s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tibalt, Rakish Instigator'),
    (select id from sets where short_name = 'pwar'),
    '146s★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sorin, Vengeful Bloodlord'),
    (select id from sets where short_name = 'pwar'),
    '217p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'God-Eternal Oketra'),
    (select id from sets where short_name = 'pwar'),
    '16s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mobilized District'),
    (select id from sets where short_name = 'pwar'),
    '249s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bolas''s Citadel'),
    (select id from sets where short_name = 'pwar'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'God-Eternal Kefnet'),
    (select id from sets where short_name = 'pwar'),
    '53s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jiang Yanggu, Wildcrafter'),
    (select id from sets where short_name = 'pwar'),
    '164s★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ral, Storm Conduit'),
    (select id from sets where short_name = 'pwar'),
    '211s★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Augur of Bolas'),
    (select id from sets where short_name = 'pwar'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kaya, Bane of the Dead'),
    (select id from sets where short_name = 'pwar'),
    '231s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Twister'),
    (select id from sets where short_name = 'pwar'),
    '203s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Domri, Anarch of Bolas'),
    (select id from sets where short_name = 'pwar'),
    '191s★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Solar Blaze'),
    (select id from sets where short_name = 'pwar'),
    '216p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Awakening of Vitu-Ghazi'),
    (select id from sets where short_name = 'pwar'),
    '152s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreadhorde Invasion'),
    (select id from sets where short_name = 'pwar'),
    '86s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa, Who Shakes the World'),
    (select id from sets where short_name = 'pwar'),
    '169p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Finale of Devastation'),
    (select id from sets where short_name = 'pwar'),
    '160p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, Dragon-God'),
    (select id from sets where short_name = 'pwar'),
    '207p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Karn, the Great Creator'),
    (select id from sets where short_name = 'pwar'),
    '1p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaya, Bane of the Dead'),
    (select id from sets where short_name = 'pwar'),
    '231s★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jace, Wielder of Mysteries'),
    (select id from sets where short_name = 'pwar'),
    '54p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oath of Kaya'),
    (select id from sets where short_name = 'pwar'),
    '209p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dovin, Hand of Control'),
    (select id from sets where short_name = 'pwar'),
    '229s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bioessence Hydra'),
    (select id from sets where short_name = 'pwar'),
    '186s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jiang Yanggu, Wildcrafter'),
    (select id from sets where short_name = 'pwar'),
    '164s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Storrev, Devkarin Lich'),
    (select id from sets where short_name = 'pwar'),
    '219s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Narset''s Reversal'),
    (select id from sets where short_name = 'pwar'),
    '62s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Narset, Parter of Veils'),
    (select id from sets where short_name = 'pwar'),
    '61s★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ugin, the Ineffable'),
    (select id from sets where short_name = 'pwar'),
    '2s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enter the God-Eternals'),
    (select id from sets where short_name = 'pwar'),
    '196s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krenko, Tin Street Kingpin'),
    (select id from sets where short_name = 'pwar'),
    '137p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sarkhan the Masterless'),
    (select id from sets where short_name = 'pwar'),
    '143s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sorin, Vengeful Bloodlord'),
    (select id from sets where short_name = 'pwar'),
    '217s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreadhorde Arcanist'),
    (select id from sets where short_name = 'pwar'),
    '125s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nahiri, Storm of Stone'),
    (select id from sets where short_name = 'pwar'),
    '233s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Solar Blaze'),
    (select id from sets where short_name = 'pwar'),
    '216s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Wanderer'),
    (select id from sets where short_name = 'pwar'),
    '37s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nissa, Who Shakes the World'),
    (select id from sets where short_name = 'pwar'),
    '169s★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreadhorde Arcanist'),
    (select id from sets where short_name = 'pwar'),
    '125p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ral, Storm Conduit'),
    (select id from sets where short_name = 'pwar'),
    '211s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivien, Champion of the Wilds'),
    (select id from sets where short_name = 'pwar'),
    '180s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Finale of Eternity'),
    (select id from sets where short_name = 'pwar'),
    '91p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ajani, the Greathearted'),
    (select id from sets where short_name = 'pwar'),
    '184s★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jace, Wielder of Mysteries'),
    (select id from sets where short_name = 'pwar'),
    '54s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deliver Unto Evil'),
    (select id from sets where short_name = 'pwar'),
    '85s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jaya, Venerated Firemage'),
    (select id from sets where short_name = 'pwar'),
    '135s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enter the God-Eternals'),
    (select id from sets where short_name = 'pwar'),
    '196p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Davriel, Rogue Shadowmage'),
    (select id from sets where short_name = 'pwar'),
    '83s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, Dragon-God'),
    (select id from sets where short_name = 'pwar'),
    '207s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Casualties of War'),
    (select id from sets where short_name = 'pwar'),
    '187s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivien, Champion of the Wilds'),
    (select id from sets where short_name = 'pwar'),
    '180p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angrath, Captain of Chaos'),
    (select id from sets where short_name = 'pwar'),
    '227s★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Time Wipe'),
    (select id from sets where short_name = 'pwar'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana, Dreadhorde General'),
    (select id from sets where short_name = 'pwar'),
    '97s★',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Teferi, Time Raveler'),
    (select id from sets where short_name = 'pwar'),
    '221p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Huatli, the Sun''s Heart'),
    (select id from sets where short_name = 'pwar'),
    '230s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karn''s Bastion'),
    (select id from sets where short_name = 'pwar'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karn, the Great Creator'),
    (select id from sets where short_name = 'pwar'),
    '1s★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Paradise Druid'),
    (select id from sets where short_name = 'pwar'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tamiyo, Collector of Tales'),
    (select id from sets where short_name = 'pwar'),
    '220p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mobilized District'),
    (select id from sets where short_name = 'pwar'),
    '249p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani, the Greathearted'),
    (select id from sets where short_name = 'pwar'),
    '184p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi, Time Raveler'),
    (select id from sets where short_name = 'pwar'),
    '221s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Diviner'),
    (select id from sets where short_name = 'pwar'),
    '218p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kasmina, Enigmatic Mentor'),
    (select id from sets where short_name = 'pwar'),
    '56s★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Massacre Girl'),
    (select id from sets where short_name = 'pwar'),
    '99s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Parhelion II'),
    (select id from sets where short_name = 'pwar'),
    '24s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Domri, Anarch of Bolas'),
    (select id from sets where short_name = 'pwar'),
    '191s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani, the Greathearted'),
    (select id from sets where short_name = 'pwar'),
    '184s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feather, the Redeemed'),
    (select id from sets where short_name = 'pwar'),
    '197s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dovin, Hand of Control'),
    (select id from sets where short_name = 'pwar'),
    '229s★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Single Combat'),
    (select id from sets where short_name = 'pwar'),
    '30s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ravnica at War'),
    (select id from sets where short_name = 'pwar'),
    '28s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arlinn, Voice of the Pack'),
    (select id from sets where short_name = 'pwar'),
    '150s★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Finale of Eternity'),
    (select id from sets where short_name = 'pwar'),
    '91s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nahiri, Storm of Stone'),
    (select id from sets where short_name = 'pwar'),
    '233s★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana, Dreadhorde General'),
    (select id from sets where short_name = 'pwar'),
    '97s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Samut, Tyrant Smasher'),
    (select id from sets where short_name = 'pwar'),
    '235s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jaya, Venerated Firemage'),
    (select id from sets where short_name = 'pwar'),
    '135s★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Widespread Brutality'),
    (select id from sets where short_name = 'pwar'),
    '226s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivien''s Arkbow'),
    (select id from sets where short_name = 'pwar'),
    '181s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Elderspell'),
    (select id from sets where short_name = 'pwar'),
    '89s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Commence the Endgame'),
    (select id from sets where short_name = 'pwar'),
    '45s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jace, Wielder of Mysteries'),
    (select id from sets where short_name = 'pwar'),
    '54s★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fblthp, the Lost'),
    (select id from sets where short_name = 'pwar'),
    '50s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tomik, Distinguished Advokist'),
    (select id from sets where short_name = 'pwar'),
    '34s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra, Fire Artisan'),
    (select id from sets where short_name = 'pwar'),
    '119s★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Finale of Glory'),
    (select id from sets where short_name = 'pwar'),
    '12s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Narset, Parter of Veils'),
    (select id from sets where short_name = 'pwar'),
    '61s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karn, the Great Creator'),
    (select id from sets where short_name = 'pwar'),
    '1s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreadhorde Invasion'),
    (select id from sets where short_name = 'pwar'),
    '86p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Huatli, the Sun''s Heart'),
    (select id from sets where short_name = 'pwar'),
    '230s★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gideon Blackblade'),
    (select id from sets where short_name = 'pwar'),
    '13s★',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tibalt, Rakish Instigator'),
    (select id from sets where short_name = 'pwar'),
    '146s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Storrev, Devkarin Lich'),
    (select id from sets where short_name = 'pwar'),
    '219p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Triumph'),
    (select id from sets where short_name = 'pwar'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angrath, Captain of Chaos'),
    (select id from sets where short_name = 'pwar'),
    '227s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis, the Hate-Twisted'),
    (select id from sets where short_name = 'pwar'),
    '100s★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Wanderer'),
    (select id from sets where short_name = 'pwar'),
    '37s★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Time Wipe'),
    (select id from sets where short_name = 'pwar'),
    '223s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Samut, Tyrant Smasher'),
    (select id from sets where short_name = 'pwar'),
    '235s★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tolsimir, Friend to Wolves'),
    (select id from sets where short_name = 'pwar'),
    '224s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silent Submersible'),
    (select id from sets where short_name = 'pwar'),
    '66s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ral, Storm Conduit'),
    (select id from sets where short_name = 'pwar'),
    '211p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Niv-Mizzet Reborn'),
    (select id from sets where short_name = 'pwar'),
    '208s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Widespread Brutality'),
    (select id from sets where short_name = 'pwar'),
    '226p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bolas''s Citadel'),
    (select id from sets where short_name = 'pwar'),
    '79s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivien, Champion of the Wilds'),
    (select id from sets where short_name = 'pwar'),
    '180s★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spark Double'),
    (select id from sets where short_name = 'pwar'),
    '68s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arlinn, Voice of the Pack'),
    (select id from sets where short_name = 'pwar'),
    '150s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tamiyo, Collector of Tales'),
    (select id from sets where short_name = 'pwar'),
    '220s★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Domri, Anarch of Bolas'),
    (select id from sets where short_name = 'pwar'),
    '191p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tolsimir, Friend to Wolves'),
    (select id from sets where short_name = 'pwar'),
    '224p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ugin, the Ineffable'),
    (select id from sets where short_name = 'pwar'),
    '2p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saheeli, Sublime Artificer'),
    (select id from sets where short_name = 'pwar'),
    '234s★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chandra, Fire Artisan'),
    (select id from sets where short_name = 'pwar'),
    '119s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ilharg, the Raze-Boar'),
    (select id from sets where short_name = 'pwar'),
    '133s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kiora, Behemoth Beckoner'),
    (select id from sets where short_name = 'pwar'),
    '232s★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bolas''s Citadel'),
    (select id from sets where short_name = 'pwar'),
    '79p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashiok, Dream Render'),
    (select id from sets where short_name = 'pwar'),
    '228s★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Casualties of War'),
    (select id from sets where short_name = 'pwar'),
    '187p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana, Dreadhorde General'),
    (select id from sets where short_name = 'pwar'),
    '97p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dreadhorde Butcher'),
    (select id from sets where short_name = 'pwar'),
    '194p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tamiyo, Collector of Tales'),
    (select id from sets where short_name = 'pwar'),
    '220s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blast Zone'),
    (select id from sets where short_name = 'pwar'),
    '244p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oath of Kaya'),
    (select id from sets where short_name = 'pwar'),
    '209s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dovin''s Veto'),
    (select id from sets where short_name = 'pwar'),
    '193',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teyo, the Shieldmage'),
    (select id from sets where short_name = 'pwar'),
    '32s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Role Reversal'),
    (select id from sets where short_name = 'pwar'),
    '214s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Narset''s Reversal'),
    (select id from sets where short_name = 'pwar'),
    '62p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roalesk, Apex Hybrid'),
    (select id from sets where short_name = 'pwar'),
    '213s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Davriel, Rogue Shadowmage'),
    (select id from sets where short_name = 'pwar'),
    '83s★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kiora, Behemoth Beckoner'),
    (select id from sets where short_name = 'pwar'),
    '232s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gideon Blackblade'),
    (select id from sets where short_name = 'pwar'),
    '13s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Krenko, Tin Street Kingpin'),
    (select id from sets where short_name = 'pwar'),
    '137s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sorin, Vengeful Bloodlord'),
    (select id from sets where short_name = 'pwar'),
    '217s★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis, the Hate-Twisted'),
    (select id from sets where short_name = 'pwar'),
    '100s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vraska, Swarm''s Eminence'),
    (select id from sets where short_name = 'pwar'),
    '236s★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blast Zone'),
    (select id from sets where short_name = 'pwar'),
    '244s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Planewide Celebration'),
    (select id from sets where short_name = 'pwar'),
    '172s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karn''s Bastion'),
    (select id from sets where short_name = 'pwar'),
    '248p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashiok, Dream Render'),
    (select id from sets where short_name = 'pwar'),
    '228s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nissa, Who Shakes the World'),
    (select id from sets where short_name = 'pwar'),
    '169s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'God-Eternal Rhonas'),
    (select id from sets where short_name = 'pwar'),
    '163s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Teferi, Time Raveler'),
    (select id from sets where short_name = 'pwar'),
    '221s★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, Dragon-God'),
    (select id from sets where short_name = 'pwar'),
    '207s★',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Teyo, the Shieldmage'),
    (select id from sets where short_name = 'pwar'),
    '32s★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Command the Dreadhorde'),
    (select id from sets where short_name = 'pwar'),
    '82s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Neheb, Dreadhorde Champion'),
    (select id from sets where short_name = 'pwar'),
    '140s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Commence the Endgame'),
    (select id from sets where short_name = 'pwar'),
    '45p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sarkhan the Masterless'),
    (select id from sets where short_name = 'pwar'),
    '143s★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spark Double'),
    (select id from sets where short_name = 'pwar'),
    '68p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gideon Blackblade'),
    (select id from sets where short_name = 'pwar'),
    '13p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Finale of Revelation'),
    (select id from sets where short_name = 'pwar'),
    '51s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dreadhorde Butcher'),
    (select id from sets where short_name = 'pwar'),
    '194s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Finale of Revelation'),
    (select id from sets where short_name = 'pwar'),
    '51p',
    'mythic'
) 
 on conflict do nothing;
