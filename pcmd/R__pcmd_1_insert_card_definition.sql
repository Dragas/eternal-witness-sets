    insert into mtgcard(name) values ('Skullbriar, the Walking Grave') on conflict do nothing;
    insert into mtgcard(name) values ('Vish Kal, Blood Arbiter') on conflict do nothing;
    insert into mtgcard(name) values ('Nin, the Pain Artist') on conflict do nothing;
    insert into mtgcard(name) values ('Basandra, Battle Seraph') on conflict do nothing;
    insert into mtgcard(name) values ('Edric, Spymaster of Trest') on conflict do nothing;
