insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Skullbriar, the Walking Grave'),
    (select id from sets where short_name = 'pcmd'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vish Kal, Blood Arbiter'),
    (select id from sets where short_name = 'pcmd'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nin, the Pain Artist'),
    (select id from sets where short_name = 'pcmd'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Basandra, Battle Seraph'),
    (select id from sets where short_name = 'pcmd'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Edric, Spymaster of Trest'),
    (select id from sets where short_name = 'pcmd'),
    '196',
    'rare'
) 
 on conflict do nothing;
