    insert into mtgcard(name) values ('Dromoka, the Eternal') on conflict do nothing;
    insert into mtgcard(name) values ('Siege Rhino') on conflict do nothing;
    insert into mtgcard(name) values ('Honored Hierarch') on conflict do nothing;
    insert into mtgcard(name) values ('Sandsteppe Citadel') on conflict do nothing;
    insert into mtgcard(name) values ('Seeker of the Way') on conflict do nothing;
    insert into mtgcard(name) values ('Valorous Stance') on conflict do nothing;
