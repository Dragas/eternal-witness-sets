insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Dromoka, the Eternal'),
    (select id from sets where short_name = 'cp3'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Siege Rhino'),
    (select id from sets where short_name = 'cp3'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Honored Hierarch'),
    (select id from sets where short_name = 'cp3'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sandsteppe Citadel'),
    (select id from sets where short_name = 'cp3'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seeker of the Way'),
    (select id from sets where short_name = 'cp3'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Valorous Stance'),
    (select id from sets where short_name = 'cp3'),
    '3',
    'uncommon'
) 
 on conflict do nothing;
