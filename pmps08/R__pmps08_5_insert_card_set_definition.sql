insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pmps08'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pmps08'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jaya Ballard, Task Mage'),
    (select id from sets where short_name = 'pmps08'),
    'a1 2007',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pmps08'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pmps08'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pmps08'),
    '1',
    'common'
) 
 on conflict do nothing;
