insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Fire Elemental'),
    (select id from sets where short_name = '4bb'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghost Ship'),
    (select id from sets where short_name = '4bb'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Twiddle'),
    (select id from sets where short_name = '4bb'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Master'),
    (select id from sets where short_name = '4bb'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lord of the Pit'),
    (select id from sets where short_name = '4bb'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flight'),
    (select id from sets where short_name = '4bb'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Craw Wurm'),
    (select id from sets where short_name = '4bb'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunglasses of Urza'),
    (select id from sets where short_name = '4bb'),
    '347',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aladdin''s Ring'),
    (select id from sets where short_name = '4bb'),
    '292',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantom Monster'),
    (select id from sets where short_name = '4bb'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seeker'),
    (select id from sets where short_name = '4bb'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crystal Rod'),
    (select id from sets where short_name = '4bb'),
    '311',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crumble'),
    (select id from sets where short_name = '4bb'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rebirth'),
    (select id from sets where short_name = '4bb'),
    '267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Forces'),
    (select id from sets where short_name = '4bb'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sea Serpent'),
    (select id from sets where short_name = '4bb'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prodigal Sorcerer'),
    (select id from sets where short_name = '4bb'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frozen Shade'),
    (select id from sets where short_name = '4bb'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cyclopean Mummy'),
    (select id from sets where short_name = '4bb'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Energy Flux'),
    (select id from sets where short_name = '4bb'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eternal Warrior'),
    (select id from sets where short_name = '4bb'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armageddon Clock'),
    (select id from sets where short_name = '4bb'),
    '295',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Flare'),
    (select id from sets where short_name = '4bb'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uncle Istvan'),
    (select id from sets where short_name = '4bb'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Titania''s Song'),
    (select id from sets where short_name = '4bb'),
    '276',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steal Artifact'),
    (select id from sets where short_name = '4bb'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scryb Sprites'),
    (select id from sets where short_name = '4bb'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Regeneration'),
    (select id from sets where short_name = '4bb'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Will-o''-the-Wisp'),
    (select id from sets where short_name = '4bb'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Howling Mine'),
    (select id from sets where short_name = '4bb'),
    '325',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = '4bb'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychic Venom'),
    (select id from sets where short_name = '4bb'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blessing'),
    (select id from sets where short_name = '4bb'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Ice'),
    (select id from sets where short_name = '4bb'),
    '283',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battering Ram'),
    (select id from sets where short_name = '4bb'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rag Man'),
    (select id from sets where short_name = '4bb'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stasis'),
    (select id from sets where short_name = '4bb'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drain Power'),
    (select id from sets where short_name = '4bb'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Detonate'),
    (select id from sets where short_name = '4bb'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jandor''s Saddlebags'),
    (select id from sets where short_name = '4bb'),
    '330',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Terrain'),
    (select id from sets where short_name = '4bb'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diabolic Machine'),
    (select id from sets where short_name = '4bb'),
    '314',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hill Giant'),
    (select id from sets where short_name = '4bb'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Warriors'),
    (select id from sets where short_name = '4bb'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ley Druid'),
    (select id from sets where short_name = '4bb'),
    '256',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jayemdae Tome'),
    (select id from sets where short_name = '4bb'),
    '331',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brass Man'),
    (select id from sets where short_name = '4bb'),
    '302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '4bb'),
    '366',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leviathan'),
    (select id from sets where short_name = '4bb'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Untamed Wilds'),
    (select id from sets where short_name = '4bb'),
    '279',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Whelp'),
    (select id from sets where short_name = '4bb'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Water'),
    (select id from sets where short_name = '4bb'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mishra''s War Machine'),
    (select id from sets where short_name = '4bb'),
    '337',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ironroot Treefolk'),
    (select id from sets where short_name = '4bb'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Leak'),
    (select id from sets where short_name = '4bb'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Black'),
    (select id from sets where short_name = '4bb'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yotian Soldier'),
    (select id from sets where short_name = '4bb'),
    '360',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Engine'),
    (select id from sets where short_name = '4bb'),
    '317',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fissure'),
    (select id from sets where short_name = '4bb'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fellwar Stone'),
    (select id from sets where short_name = '4bb'),
    '319',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aladdin''s Lamp'),
    (select id from sets where short_name = '4bb'),
    '291',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lure'),
    (select id from sets where short_name = '4bb'),
    '262',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sandstorm'),
    (select id from sets where short_name = '4bb'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tsunami'),
    (select id from sets where short_name = '4bb'),
    '278',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Xenic Poltergeist'),
    (select id from sets where short_name = '4bb'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Onulet'),
    (select id from sets where short_name = '4bb'),
    '340',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pikemen'),
    (select id from sets where short_name = '4bb'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Piety'),
    (select id from sets where short_name = '4bb'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Animate Artifact'),
    (select id from sets where short_name = '4bb'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fog'),
    (select id from sets where short_name = '4bb'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Control Magic'),
    (select id from sets where short_name = '4bb'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Spears'),
    (select id from sets where short_name = '4bb'),
    '356',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karma'),
    (select id from sets where short_name = '4bb'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disrupting Scepter'),
    (select id from sets where short_name = '4bb'),
    '316',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = '4bb'),
    '361',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = '4bb'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spell Blast'),
    (select id from sets where short_name = '4bb'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Killer Bees'),
    (select id from sets where short_name = '4bb'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Avenger'),
    (select id from sets where short_name = '4bb'),
    '355',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Artillery'),
    (select id from sets where short_name = '4bb'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fortified Area'),
    (select id from sets where short_name = '4bb'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '4bb'),
    '376',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sorceress Queen'),
    (select id from sets where short_name = '4bb'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eye for an Eye'),
    (select id from sets where short_name = '4bb'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = '4bb'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cave People'),
    (select id from sets where short_name = '4bb'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '4bb'),
    '369',
    'common'
) ,
(
    (select id from mtgcard where name = 'Benalish Hero'),
    (select id from sets where short_name = '4bb'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '4bb'),
    '364',
    'common'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = '4bb'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Black Ward'),
    (select id from sets where short_name = '4bb'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unholy Strength'),
    (select id from sets where short_name = '4bb'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'White Knight'),
    (select id from sets where short_name = '4bb'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '4bb'),
    '377',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greed'),
    (select id from sets where short_name = '4bb'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cockatrice'),
    (select id from sets where short_name = '4bb'),
    '237',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Purelace'),
    (select id from sets where short_name = '4bb'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winter Blast'),
    (select id from sets where short_name = '4bb'),
    '290',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drain Life'),
    (select id from sets where short_name = '4bb'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Radjan Spirit'),
    (select id from sets where short_name = '4bb'),
    '266',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Righteousness'),
    (select id from sets where short_name = '4bb'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ironclaw Orcs'),
    (select id from sets where short_name = '4bb'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lost Soul'),
    (select id from sets where short_name = '4bb'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Eruption'),
    (select id from sets where short_name = '4bb'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Brambles'),
    (select id from sets where short_name = '4bb'),
    '282',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Northern Paladin'),
    (select id from sets where short_name = '4bb'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thicket Basilisk'),
    (select id from sets where short_name = '4bb'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Murk Dwellers'),
    (select id from sets where short_name = '4bb'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crimson Manticore'),
    (select id from sets where short_name = '4bb'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Earth Elemental'),
    (select id from sets where short_name = '4bb'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Iron Star'),
    (select id from sets where short_name = '4bb'),
    '326',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Brute'),
    (select id from sets where short_name = '4bb'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Bone'),
    (select id from sets where short_name = '4bb'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meekstone'),
    (select id from sets where short_name = '4bb'),
    '335',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oasis'),
    (select id from sets where short_name = '4bb'),
    '362',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crusade'),
    (select id from sets where short_name = '4bb'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angry Mob'),
    (select id from sets where short_name = '4bb'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Channel'),
    (select id from sets where short_name = '4bb'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keldon Warlord'),
    (select id from sets where short_name = '4bb'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = '4bb'),
    '338',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = '4bb'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Instill Energy'),
    (select id from sets where short_name = '4bb'),
    '252',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marsh Gas'),
    (select id from sets where short_name = '4bb'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marsh Viper'),
    (select id from sets where short_name = '4bb'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Specter'),
    (select id from sets where short_name = '4bb'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winter Orb'),
    (select id from sets where short_name = '4bb'),
    '358',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hurr Jackal'),
    (select id from sets where short_name = '4bb'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magnetic Mountain'),
    (select id from sets where short_name = '4bb'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blue Ward'),
    (select id from sets where short_name = '4bb'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = '4bb'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bronze Tablet'),
    (select id from sets where short_name = '4bb'),
    '303',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stream of Life'),
    (select id from sets where short_name = '4bb'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primal Clay'),
    (select id from sets where short_name = '4bb'),
    '342',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Lust'),
    (select id from sets where short_name = '4bb'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brainwash'),
    (select id from sets where short_name = '4bb'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evil Presence'),
    (select id from sets where short_name = '4bb'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scavenging Ghoul'),
    (select id from sets where short_name = '4bb'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = '4bb'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '4bb'),
    '371',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erosion'),
    (select id from sets where short_name = '4bb'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '4bb'),
    '365',
    'common'
) ,
(
    (select id from mtgcard where name = 'Uthden Troll'),
    (select id from sets where short_name = '4bb'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Bomb'),
    (select id from sets where short_name = '4bb'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gloom'),
    (select id from sets where short_name = '4bb'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Web'),
    (select id from sets where short_name = '4bb'),
    '287',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Manabarbs'),
    (select id from sets where short_name = '4bb'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Rack'),
    (select id from sets where short_name = '4bb'),
    '352',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Hive'),
    (select id from sets where short_name = '4bb'),
    '351',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inferno'),
    (select id from sets where short_name = '4bb'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savannah Lions'),
    (select id from sets where short_name = '4bb'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unstable Mutation'),
    (select id from sets where short_name = '4bb'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Strength'),
    (select id from sets where short_name = '4bb'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Healing Salve'),
    (select id from sets where short_name = '4bb'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Surge'),
    (select id from sets where short_name = '4bb'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Osai Vultures'),
    (select id from sets where short_name = '4bb'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Blue'),
    (select id from sets where short_name = '4bb'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shapeshifter'),
    (select id from sets where short_name = '4bb'),
    '345',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Divine Transformation'),
    (select id from sets where short_name = '4bb'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Vise'),
    (select id from sets where short_name = '4bb'),
    '299',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Red Elemental Blast'),
    (select id from sets where short_name = '4bb'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flashfires'),
    (select id from sets where short_name = '4bb'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jump'),
    (select id from sets where short_name = '4bb'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blight'),
    (select id from sets where short_name = '4bb'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = '4bb'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desert Twister'),
    (select id from sets where short_name = '4bb'),
    '240',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Colossus of Sardia'),
    (select id from sets where short_name = '4bb'),
    '308',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: White'),
    (select id from sets where short_name = '4bb'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tundra Wolves'),
    (select id from sets where short_name = '4bb'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paralyze'),
    (select id from sets where short_name = '4bb'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Library of Leng'),
    (select id from sets where short_name = '4bb'),
    '333',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Red Mana Battery'),
    (select id from sets where short_name = '4bb'),
    '343',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '4bb'),
    '373',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feedback'),
    (select id from sets where short_name = '4bb'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Air'),
    (select id from sets where short_name = '4bb'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Apprentice Wizard'),
    (select id from sets where short_name = '4bb'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Ward'),
    (select id from sets where short_name = '4bb'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pit Scorpion'),
    (select id from sets where short_name = '4bb'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit Shackle'),
    (select id from sets where short_name = '4bb'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = '4bb'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashes to Ashes'),
    (select id from sets where short_name = '4bb'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lord of Atlantis'),
    (select id from sets where short_name = '4bb'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pradesh Gypsies'),
    (select id from sets where short_name = '4bb'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird Maiden'),
    (select id from sets where short_name = '4bb'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Swords'),
    (select id from sets where short_name = '4bb'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Fire'),
    (select id from sets where short_name = '4bb'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Time Elemental'),
    (select id from sets where short_name = '4bb'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathlace'),
    (select id from sets where short_name = '4bb'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Net'),
    (select id from sets where short_name = '4bb'),
    '346',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hurkyl''s Recall'),
    (select id from sets where short_name = '4bb'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '4bb'),
    '368',
    'common'
) ,
(
    (select id from mtgcard where name = 'Venom'),
    (select id from sets where short_name = '4bb'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Mana Battery'),
    (select id from sets where short_name = '4bb'),
    '298',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hurloon Minotaur'),
    (select id from sets where short_name = '4bb'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = '4bb'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hurricane'),
    (select id from sets where short_name = '4bb'),
    '251',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Green Mana Battery'),
    (select id from sets where short_name = '4bb'),
    '323',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balance'),
    (select id from sets where short_name = '4bb'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Erg Raiders'),
    (select id from sets where short_name = '4bb'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lifetap'),
    (select id from sets where short_name = '4bb'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'El-Hajjâj'),
    (select id from sets where short_name = '4bb'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nafs Asp'),
    (select id from sets where short_name = '4bb'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mahamoti Djinn'),
    (select id from sets where short_name = '4bb'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathgrip'),
    (select id from sets where short_name = '4bb'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = '4bb'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bad Moon'),
    (select id from sets where short_name = '4bb'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Energy Tap'),
    (select id from sets where short_name = '4bb'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Knight'),
    (select id from sets where short_name = '4bb'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Carrion Ants'),
    (select id from sets where short_name = '4bb'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = '4bb'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk of the Pearl Trident'),
    (select id from sets where short_name = '4bb'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Personal Incarnation'),
    (select id from sets where short_name = '4bb'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = '4bb'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = '4bb'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warp Artifact'),
    (select id from sets where short_name = '4bb'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = '4bb'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strip Mine'),
    (select id from sets where short_name = '4bb'),
    '363',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Library'),
    (select id from sets where short_name = '4bb'),
    '273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whirling Dervish'),
    (select id from sets where short_name = '4bb'),
    '288',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '4bb'),
    '378',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clay Statue'),
    (select id from sets where short_name = '4bb'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conservator'),
    (select id from sets where short_name = '4bb'),
    '309',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Verduran Enchantress'),
    (select id from sets where short_name = '4bb'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clockwork Avian'),
    (select id from sets where short_name = '4bb'),
    '306',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chaoslace'),
    (select id from sets where short_name = '4bb'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Power Sink'),
    (select id from sets where short_name = '4bb'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Helm of Chatzuk'),
    (select id from sets where short_name = '4bb'),
    '324',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Force of Nature'),
    (select id from sets where short_name = '4bb'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Stone'),
    (select id from sets where short_name = '4bb'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '4bb'),
    '372',
    'common'
) ,
(
    (select id from mtgcard where name = 'Triskelion'),
    (select id from sets where short_name = '4bb'),
    '354',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Immolation'),
    (select id from sets where short_name = '4bb'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pirate Ship'),
    (select id from sets where short_name = '4bb'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Throne of Bone'),
    (select id from sets where short_name = '4bb'),
    '353',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Water Elemental'),
    (select id from sets where short_name = '4bb'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elven Riders'),
    (select id from sets where short_name = '4bb'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jade Monolith'),
    (select id from sets where short_name = '4bb'),
    '329',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smoke'),
    (select id from sets where short_name = '4bb'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tranquility'),
    (select id from sets where short_name = '4bb'),
    '277',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tawnos''s Weaponry'),
    (select id from sets where short_name = '4bb'),
    '349',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Clash'),
    (select id from sets where short_name = '4bb'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wild Growth'),
    (select id from sets where short_name = '4bb'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lifelace'),
    (select id from sets where short_name = '4bb'),
    '258',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kormus Bell'),
    (select id from sets where short_name = '4bb'),
    '332',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island Sanctuary'),
    (select id from sets where short_name = '4bb'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animate Wall'),
    (select id from sets where short_name = '4bb'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Holy Strength'),
    (select id from sets where short_name = '4bb'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plague Rats'),
    (select id from sets where short_name = '4bb'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = '4bb'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'White Mana Battery'),
    (select id from sets where short_name = '4bb'),
    '357',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Amrou Kithkin'),
    (select id from sets where short_name = '4bb'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunken City'),
    (select id from sets where short_name = '4bb'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clockwork Beast'),
    (select id from sets where short_name = '4bb'),
    '307',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burrowing'),
    (select id from sets where short_name = '4bb'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = '4bb'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = '4bb'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flying Carpet'),
    (select id from sets where short_name = '4bb'),
    '320',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tunnel'),
    (select id from sets where short_name = '4bb'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firebreathing'),
    (select id from sets where short_name = '4bb'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Rock Sled'),
    (select id from sets where short_name = '4bb'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simulacrum'),
    (select id from sets where short_name = '4bb'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Wood'),
    (select id from sets where short_name = '4bb'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ankh of Mishra'),
    (select id from sets where short_name = '4bb'),
    '294',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Millstone'),
    (select id from sets where short_name = '4bb'),
    '336',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ali Baba'),
    (select id from sets where short_name = '4bb'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cosmic Horror'),
    (select id from sets where short_name = '4bb'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ball Lightning'),
    (select id from sets where short_name = '4bb'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Junún Efreet'),
    (select id from sets where short_name = '4bb'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = '4bb'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '4bb'),
    '374',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Battle Gear'),
    (select id from sets where short_name = '4bb'),
    '296',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = '4bb'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conversion'),
    (select id from sets where short_name = '4bb'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grapeshot Catapult'),
    (select id from sets where short_name = '4bb'),
    '322',
    'common'
) ,
(
    (select id from mtgcard where name = 'Holy Armor'),
    (select id from sets where short_name = '4bb'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coral Helm'),
    (select id from sets where short_name = '4bb'),
    '310',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sleight of Mind'),
    (select id from sets where short_name = '4bb'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cursed Rack'),
    (select id from sets where short_name = '4bb'),
    '312',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ebony Horse'),
    (select id from sets where short_name = '4bb'),
    '318',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tetravus'),
    (select id from sets where short_name = '4bb'),
    '350',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Living Lands'),
    (select id from sets where short_name = '4bb'),
    '260',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lifeforce'),
    (select id from sets where short_name = '4bb'),
    '257',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disintegrate'),
    (select id from sets where short_name = '4bb'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin King'),
    (select id from sets where short_name = '4bb'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Amulet of Kroog'),
    (select id from sets where short_name = '4bb'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Castle'),
    (select id from sets where short_name = '4bb'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fear'),
    (select id from sets where short_name = '4bb'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shanodin Dryads'),
    (select id from sets where short_name = '4bb'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Timber Wolves'),
    (select id from sets where short_name = '4bb'),
    '275',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Artifacts'),
    (select id from sets where short_name = '4bb'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kismet'),
    (select id from sets where short_name = '4bb'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Siren''s Call'),
    (select id from sets where short_name = '4bb'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nether Shadow'),
    (select id from sets where short_name = '4bb'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Living Artifact'),
    (select id from sets where short_name = '4bb'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wanderlust'),
    (select id from sets where short_name = '4bb'),
    '285',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brothers of Fire'),
    (select id from sets where short_name = '4bb'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Green Ward'),
    (select id from sets where short_name = '4bb'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sindbad'),
    (select id from sets where short_name = '4bb'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glasses of Urza'),
    (select id from sets where short_name = '4bb'),
    '321',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rod of Ruin'),
    (select id from sets where short_name = '4bb'),
    '344',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wooden Sphere'),
    (select id from sets where short_name = '4bb'),
    '359',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cursed Land'),
    (select id from sets where short_name = '4bb'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Dust'),
    (select id from sets where short_name = '4bb'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = '4bb'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Howl from Beyond'),
    (select id from sets where short_name = '4bb'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ornithopter'),
    (select id from sets where short_name = '4bb'),
    '341',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fungusaur'),
    (select id from sets where short_name = '4bb'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abomination'),
    (select id from sets where short_name = '4bb'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bog Imp'),
    (select id from sets where short_name = '4bb'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Word of Binding'),
    (select id from sets where short_name = '4bb'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Backfire'),
    (select id from sets where short_name = '4bb'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dancing Scimitar'),
    (select id from sets where short_name = '4bb'),
    '313',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zephyr Falcon'),
    (select id from sets where short_name = '4bb'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '4bb'),
    '370',
    'common'
) ,
(
    (select id from mtgcard where name = 'Land Tax'),
    (select id from sets where short_name = '4bb'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Weakness'),
    (select id from sets where short_name = '4bb'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pearled Unicorn'),
    (select id from sets where short_name = '4bb'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tawnos''s Wand'),
    (select id from sets where short_name = '4bb'),
    '348',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alabaster Potion'),
    (select id from sets where short_name = '4bb'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pestilence'),
    (select id from sets where short_name = '4bb'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aspect of Wolf'),
    (select id from sets where short_name = '4bb'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magical Hack'),
    (select id from sets where short_name = '4bb'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Psionic Entity'),
    (select id from sets where short_name = '4bb'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Green'),
    (select id from sets where short_name = '4bb'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '4bb'),
    '367',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Twist'),
    (select id from sets where short_name = '4bb'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grizzly Bears'),
    (select id from sets where short_name = '4bb'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit Link'),
    (select id from sets where short_name = '4bb'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Liege'),
    (select id from sets where short_name = '4bb'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Royal Assassin'),
    (select id from sets where short_name = '4bb'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thoughtlace'),
    (select id from sets where short_name = '4bb'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestial Prism'),
    (select id from sets where short_name = '4bb'),
    '304',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mons''s Goblin Raiders'),
    (select id from sets where short_name = '4bb'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sisters of the Flame'),
    (select id from sets where short_name = '4bb'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dingus Egg'),
    (select id from sets where short_name = '4bb'),
    '315',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Obsianus Golem'),
    (select id from sets where short_name = '4bb'),
    '339',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Durkwood Boars'),
    (select id from sets where short_name = '4bb'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island Fish Jasconius'),
    (select id from sets where short_name = '4bb'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Samite Healer'),
    (select id from sets where short_name = '4bb'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drudge Skeletons'),
    (select id from sets where short_name = '4bb'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = '4bb'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blue Mana Battery'),
    (select id from sets where short_name = '4bb'),
    '300',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Creature Bond'),
    (select id from sets where short_name = '4bb'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Archers'),
    (select id from sets where short_name = '4bb'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyrotechnics'),
    (select id from sets where short_name = '4bb'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Oriflamme'),
    (select id from sets where short_name = '4bb'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Short'),
    (select id from sets where short_name = '4bb'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Balloon Brigade'),
    (select id from sets where short_name = '4bb'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gaseous Form'),
    (select id from sets where short_name = '4bb'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Animate Dead'),
    (select id from sets where short_name = '4bb'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Vault'),
    (select id from sets where short_name = '4bb'),
    '334',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tempest Efreet'),
    (select id from sets where short_name = '4bb'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Morale'),
    (select id from sets where short_name = '4bb'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Land Leeches'),
    (select id from sets where short_name = '4bb'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scathe Zombies'),
    (select id from sets where short_name = '4bb'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Bats'),
    (select id from sets where short_name = '4bb'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = '4bb'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mesa Pegasus'),
    (select id from sets where short_name = '4bb'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '4bb'),
    '375',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reverse Damage'),
    (select id from sets where short_name = '4bb'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gray Ogre'),
    (select id from sets where short_name = '4bb'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Giant'),
    (select id from sets where short_name = '4bb'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flood'),
    (select id from sets where short_name = '4bb'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Tortoise'),
    (select id from sets where short_name = '4bb'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ivory Tower'),
    (select id from sets where short_name = '4bb'),
    '328',
    'rare'
) ,
(
    (select id from mtgcard where name = 'War Mammoth'),
    (select id from sets where short_name = '4bb'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relic Bind'),
    (select id from sets where short_name = '4bb'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carnivorous Plant'),
    (select id from sets where short_name = '4bb'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Visions'),
    (select id from sets where short_name = '4bb'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = '4bb'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Red Ward'),
    (select id from sets where short_name = '4bb'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blue Elemental Blast'),
    (select id from sets where short_name = '4bb'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elder Land Wurm'),
    (select id from sets where short_name = '4bb'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bottle of Suleiman'),
    (select id from sets where short_name = '4bb'),
    '301',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Segovian Leviathan'),
    (select id from sets where short_name = '4bb'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ivory Cup'),
    (select id from sets where short_name = '4bb'),
    '327',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winds of Change'),
    (select id from sets where short_name = '4bb'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bog Wraith'),
    (select id from sets where short_name = '4bb'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'White Ward'),
    (select id from sets where short_name = '4bb'),
    '57',
    'uncommon'
) 
 on conflict do nothing;
