insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Genesis'),
    (select id from sets where short_name = 'j13'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golem'),
    (select id from sets where short_name = 'j13'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crucible of Worlds'),
    (select id from sets where short_name = 'j13'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vindicate'),
    (select id from sets where short_name = 'j13'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bribery'),
    (select id from sets where short_name = 'j13'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Show and Tell'),
    (select id from sets where short_name = 'j13'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overwhelming Forces'),
    (select id from sets where short_name = 'j13'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imperial Recruiter'),
    (select id from sets where short_name = 'j13'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'j13'),
    '1',
    'rare'
) 
 on conflict do nothing;
