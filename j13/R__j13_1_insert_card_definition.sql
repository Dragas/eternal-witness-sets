    insert into mtgcard(name) values ('Genesis') on conflict do nothing;
    insert into mtgcard(name) values ('Golem') on conflict do nothing;
    insert into mtgcard(name) values ('Crucible of Worlds') on conflict do nothing;
    insert into mtgcard(name) values ('Vindicate') on conflict do nothing;
    insert into mtgcard(name) values ('Bribery') on conflict do nothing;
    insert into mtgcard(name) values ('Show and Tell') on conflict do nothing;
    insert into mtgcard(name) values ('Overwhelming Forces') on conflict do nothing;
    insert into mtgcard(name) values ('Imperial Recruiter') on conflict do nothing;
    insert into mtgcard(name) values ('Swords to Plowshares') on conflict do nothing;
