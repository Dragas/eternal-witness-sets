insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Shahrazad'),
    (select id from sets where short_name = 'arn'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serendib Djinn'),
    (select id from sets where short_name = 'arn'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Tortoise'),
    (select id from sets where short_name = 'arn'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desert Twister'),
    (select id from sets where short_name = 'arn'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abu Ja''far'),
    (select id from sets where short_name = 'arn'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moorish Cavalry'),
    (select id from sets where short_name = 'arn'),
    '7†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wyluli Wolf'),
    (select id from sets where short_name = 'arn'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Tortoise'),
    (select id from sets where short_name = 'arn'),
    '15†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elephant Graveyard'),
    (select id from sets where short_name = 'arn'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Khabál Ghoul'),
    (select id from sets where short_name = 'arn'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brass Man'),
    (select id from sets where short_name = 'arn'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wyluli Wolf'),
    (select id from sets where short_name = 'arn'),
    '55†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dancing Scimitar'),
    (select id from sets where short_name = 'arn'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desert'),
    (select id from sets where short_name = 'arn'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flying Men'),
    (select id from sets where short_name = 'arn'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drop of Honey'),
    (select id from sets where short_name = 'arn'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Erg Raiders'),
    (select id from sets where short_name = 'arn'),
    '25†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eye for an Eye'),
    (select id from sets where short_name = 'arn'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ali Baba'),
    (select id from sets where short_name = 'arn'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merchant Ship'),
    (select id from sets where short_name = 'arn'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Singing Tree'),
    (select id from sets where short_name = 'arn'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oubliette'),
    (select id from sets where short_name = 'arn'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Juzám Djinn'),
    (select id from sets where short_name = 'arn'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Erg Raiders'),
    (select id from sets where short_name = 'arn'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Army of Allah'),
    (select id from sets where short_name = 'arn'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nafs Asp'),
    (select id from sets where short_name = 'arn'),
    '52†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dandân'),
    (select id from sets where short_name = 'arn'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ali from Cairo'),
    (select id from sets where short_name = 'arn'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Erhnam Djinn'),
    (select id from sets where short_name = 'arn'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flying Carpet'),
    (select id from sets where short_name = 'arn'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'King Suleiman'),
    (select id from sets where short_name = 'arn'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oubliette'),
    (select id from sets where short_name = 'arn'),
    '31†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird Maiden'),
    (select id from sets where short_name = 'arn'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Junún Efreet'),
    (select id from sets where short_name = 'arn'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Repentant Blacksmith'),
    (select id from sets where short_name = 'arn'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oasis'),
    (select id from sets where short_name = 'arn'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bird Maiden'),
    (select id from sets where short_name = 'arn'),
    '37†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jandor''s Ring'),
    (select id from sets where short_name = 'arn'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sandstorm'),
    (select id from sets where short_name = 'arn'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'War Elephant'),
    (select id from sets where short_name = 'arn'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cuombajj Witches'),
    (select id from sets where short_name = 'arn'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sindbad'),
    (select id from sets where short_name = 'arn'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island Fish Jasconius'),
    (select id from sets where short_name = 'arn'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Piety'),
    (select id from sets where short_name = 'arn'),
    '8†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sandals of Abdallah'),
    (select id from sets where short_name = 'arn'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aladdin''s Lamp'),
    (select id from sets where short_name = 'arn'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sorceress Queen'),
    (select id from sets where short_name = 'arn'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'City in a Bottle'),
    (select id from sets where short_name = 'arn'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magnetic Mountain'),
    (select id from sets where short_name = 'arn'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nafs Asp'),
    (select id from sets where short_name = 'arn'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Army of Allah'),
    (select id from sets where short_name = 'arn'),
    '2†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guardian Beast'),
    (select id from sets where short_name = 'arn'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ebony Horse'),
    (select id from sets where short_name = 'arn'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rukh Egg'),
    (select id from sets where short_name = 'arn'),
    '43†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hasran Ogress'),
    (select id from sets where short_name = 'arn'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unstable Mutation'),
    (select id from sets where short_name = 'arn'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rukh Egg'),
    (select id from sets where short_name = 'arn'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jihad'),
    (select id from sets where short_name = 'arn'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aladdin''s Ring'),
    (select id from sets where short_name = 'arn'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jandor''s Saddlebags'),
    (select id from sets where short_name = 'arn'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jeweled Bird'),
    (select id from sets where short_name = 'arn'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fishliver Oil'),
    (select id from sets where short_name = 'arn'),
    '13†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ifh-Bíff Efreet'),
    (select id from sets where short_name = 'arn'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'arn'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bottle of Suleiman'),
    (select id from sets where short_name = 'arn'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'El-Hajjâj'),
    (select id from sets where short_name = 'arn'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone-Throwing Devils'),
    (select id from sets where short_name = 'arn'),
    '33†',
    'common'
) ,
(
    (select id from mtgcard where name = 'War Elephant'),
    (select id from sets where short_name = 'arn'),
    '11†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serendib Efreet'),
    (select id from sets where short_name = 'arn'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Old Man of the Sea'),
    (select id from sets where short_name = 'arn'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone-Throwing Devils'),
    (select id from sets where short_name = 'arn'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyramids'),
    (select id from sets where short_name = 'arn'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mijae Djinn'),
    (select id from sets where short_name = 'arn'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hasran Ogress'),
    (select id from sets where short_name = 'arn'),
    '27†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aladdin'),
    (select id from sets where short_name = 'arn'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fishliver Oil'),
    (select id from sets where short_name = 'arn'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Camel'),
    (select id from sets where short_name = 'arn'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desert Nomads'),
    (select id from sets where short_name = 'arn'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diamond Valley'),
    (select id from sets where short_name = 'arn'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kird Ape'),
    (select id from sets where short_name = 'arn'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Library of Alexandria'),
    (select id from sets where short_name = 'arn'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ydwen Efreet'),
    (select id from sets where short_name = 'arn'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island of Wak-Wak'),
    (select id from sets where short_name = 'arn'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cyclone'),
    (select id from sets where short_name = 'arn'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hurr Jackal'),
    (select id from sets where short_name = 'arn'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'arn'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Piety'),
    (select id from sets where short_name = 'arn'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moorish Cavalry'),
    (select id from sets where short_name = 'arn'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghazbán Ogre'),
    (select id from sets where short_name = 'arn'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Metamorphosis'),
    (select id from sets where short_name = 'arn'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ring of Ma''rûf'),
    (select id from sets where short_name = 'arn'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bazaar of Baghdad'),
    (select id from sets where short_name = 'arn'),
    '70',
    'uncommon'
) 
 on conflict do nothing;
