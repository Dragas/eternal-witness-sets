    insert into mtgcard(name) values ('Shahrazad') on conflict do nothing;
    insert into mtgcard(name) values ('Serendib Djinn') on conflict do nothing;
    insert into mtgcard(name) values ('Giant Tortoise') on conflict do nothing;
    insert into mtgcard(name) values ('Desert Twister') on conflict do nothing;
    insert into mtgcard(name) values ('Abu Ja''far') on conflict do nothing;
    insert into mtgcard(name) values ('Moorish Cavalry') on conflict do nothing;
    insert into mtgcard(name) values ('Wyluli Wolf') on conflict do nothing;
    insert into mtgcard(name) values ('Elephant Graveyard') on conflict do nothing;
    insert into mtgcard(name) values ('Khabál Ghoul') on conflict do nothing;
    insert into mtgcard(name) values ('Brass Man') on conflict do nothing;
    insert into mtgcard(name) values ('Dancing Scimitar') on conflict do nothing;
    insert into mtgcard(name) values ('Desert') on conflict do nothing;
    insert into mtgcard(name) values ('Flying Men') on conflict do nothing;
    insert into mtgcard(name) values ('Drop of Honey') on conflict do nothing;
    insert into mtgcard(name) values ('Erg Raiders') on conflict do nothing;
    insert into mtgcard(name) values ('Eye for an Eye') on conflict do nothing;
    insert into mtgcard(name) values ('Ali Baba') on conflict do nothing;
    insert into mtgcard(name) values ('Merchant Ship') on conflict do nothing;
    insert into mtgcard(name) values ('Singing Tree') on conflict do nothing;
    insert into mtgcard(name) values ('Oubliette') on conflict do nothing;
    insert into mtgcard(name) values ('Juzám Djinn') on conflict do nothing;
    insert into mtgcard(name) values ('Army of Allah') on conflict do nothing;
    insert into mtgcard(name) values ('Nafs Asp') on conflict do nothing;
    insert into mtgcard(name) values ('Dandân') on conflict do nothing;
    insert into mtgcard(name) values ('Ali from Cairo') on conflict do nothing;
    insert into mtgcard(name) values ('Erhnam Djinn') on conflict do nothing;
    insert into mtgcard(name) values ('Flying Carpet') on conflict do nothing;
    insert into mtgcard(name) values ('King Suleiman') on conflict do nothing;
    insert into mtgcard(name) values ('Bird Maiden') on conflict do nothing;
    insert into mtgcard(name) values ('Junún Efreet') on conflict do nothing;
    insert into mtgcard(name) values ('Repentant Blacksmith') on conflict do nothing;
    insert into mtgcard(name) values ('Oasis') on conflict do nothing;
    insert into mtgcard(name) values ('Jandor''s Ring') on conflict do nothing;
    insert into mtgcard(name) values ('Sandstorm') on conflict do nothing;
    insert into mtgcard(name) values ('War Elephant') on conflict do nothing;
    insert into mtgcard(name) values ('Cuombajj Witches') on conflict do nothing;
    insert into mtgcard(name) values ('Sindbad') on conflict do nothing;
    insert into mtgcard(name) values ('Island Fish Jasconius') on conflict do nothing;
    insert into mtgcard(name) values ('Piety') on conflict do nothing;
    insert into mtgcard(name) values ('Sandals of Abdallah') on conflict do nothing;
    insert into mtgcard(name) values ('Aladdin''s Lamp') on conflict do nothing;
    insert into mtgcard(name) values ('Sorceress Queen') on conflict do nothing;
    insert into mtgcard(name) values ('City in a Bottle') on conflict do nothing;
    insert into mtgcard(name) values ('Magnetic Mountain') on conflict do nothing;
    insert into mtgcard(name) values ('Guardian Beast') on conflict do nothing;
    insert into mtgcard(name) values ('Ebony Horse') on conflict do nothing;
    insert into mtgcard(name) values ('Rukh Egg') on conflict do nothing;
    insert into mtgcard(name) values ('Hasran Ogress') on conflict do nothing;
    insert into mtgcard(name) values ('Unstable Mutation') on conflict do nothing;
    insert into mtgcard(name) values ('Jihad') on conflict do nothing;
    insert into mtgcard(name) values ('Aladdin''s Ring') on conflict do nothing;
    insert into mtgcard(name) values ('Jandor''s Saddlebags') on conflict do nothing;
    insert into mtgcard(name) values ('Jeweled Bird') on conflict do nothing;
    insert into mtgcard(name) values ('Fishliver Oil') on conflict do nothing;
    insert into mtgcard(name) values ('Ifh-Bíff Efreet') on conflict do nothing;
    insert into mtgcard(name) values ('Mountain') on conflict do nothing;
    insert into mtgcard(name) values ('Bottle of Suleiman') on conflict do nothing;
    insert into mtgcard(name) values ('El-Hajjâj') on conflict do nothing;
    insert into mtgcard(name) values ('Stone-Throwing Devils') on conflict do nothing;
    insert into mtgcard(name) values ('Serendib Efreet') on conflict do nothing;
    insert into mtgcard(name) values ('Old Man of the Sea') on conflict do nothing;
    insert into mtgcard(name) values ('Pyramids') on conflict do nothing;
    insert into mtgcard(name) values ('Mijae Djinn') on conflict do nothing;
    insert into mtgcard(name) values ('Aladdin') on conflict do nothing;
    insert into mtgcard(name) values ('Camel') on conflict do nothing;
    insert into mtgcard(name) values ('Desert Nomads') on conflict do nothing;
    insert into mtgcard(name) values ('Diamond Valley') on conflict do nothing;
    insert into mtgcard(name) values ('Kird Ape') on conflict do nothing;
    insert into mtgcard(name) values ('Library of Alexandria') on conflict do nothing;
    insert into mtgcard(name) values ('Ydwen Efreet') on conflict do nothing;
    insert into mtgcard(name) values ('Island of Wak-Wak') on conflict do nothing;
    insert into mtgcard(name) values ('Cyclone') on conflict do nothing;
    insert into mtgcard(name) values ('Hurr Jackal') on conflict do nothing;
    insert into mtgcard(name) values ('City of Brass') on conflict do nothing;
    insert into mtgcard(name) values ('Ghazbán Ogre') on conflict do nothing;
    insert into mtgcard(name) values ('Metamorphosis') on conflict do nothing;
    insert into mtgcard(name) values ('Ring of Ma''rûf') on conflict do nothing;
    insert into mtgcard(name) values ('Bazaar of Baghdad') on conflict do nothing;
