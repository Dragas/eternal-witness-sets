    insert into mtgcard(name) values ('Ral, Izzet Viceroy Emblem') on conflict do nothing;
    insert into mtgcard(name) values ('Bird Illusion') on conflict do nothing;
    insert into mtgcard(name) values ('Insect') on conflict do nothing;
    insert into mtgcard(name) values ('Soldier') on conflict do nothing;
    insert into mtgcard(name) values ('Elf Knight') on conflict do nothing;
    insert into mtgcard(name) values ('Angel') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin') on conflict do nothing;
    insert into mtgcard(name) values ('Vraska, Golgari Queen Emblem') on conflict do nothing;
