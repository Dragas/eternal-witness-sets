insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ral, Izzet Viceroy Emblem'),
    (select id from sets where short_name = 'tgrn'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird Illusion'),
    (select id from sets where short_name = 'tgrn'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insect'),
    (select id from sets where short_name = 'tgrn'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tgrn'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elf Knight'),
    (select id from sets where short_name = 'tgrn'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'tgrn'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tgrn'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vraska, Golgari Queen Emblem'),
    (select id from sets where short_name = 'tgrn'),
    '8',
    'common'
) 
 on conflict do nothing;
