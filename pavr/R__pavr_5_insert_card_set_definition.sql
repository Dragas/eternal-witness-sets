insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Moonsilver Spear'),
    (select id from sets where short_name = 'pavr'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Latch Seeker'),
    (select id from sets where short_name = 'pavr'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silverblade Paladin'),
    (select id from sets where short_name = 'pavr'),
    '*36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Restoration Angel'),
    (select id from sets where short_name = 'pavr'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Killing Wave'),
    (select id from sets where short_name = 'pavr'),
    '111',
    'rare'
) 
 on conflict do nothing;
