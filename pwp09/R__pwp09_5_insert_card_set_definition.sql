insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Rise from the Grave'),
    (select id from sets where short_name = 'pwp09'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Control'),
    (select id from sets where short_name = 'pwp09'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Path to Exile'),
    (select id from sets where short_name = 'pwp09'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampire Nighthawk'),
    (select id from sets where short_name = 'pwp09'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hellspark Elemental'),
    (select id from sets where short_name = 'pwp09'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slave of Bolas'),
    (select id from sets where short_name = 'pwp09'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kor Duelist'),
    (select id from sets where short_name = 'pwp09'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marisi''s Twinclaws'),
    (select id from sets where short_name = 'pwp09'),
    '26',
    'rare'
) 
 on conflict do nothing;
