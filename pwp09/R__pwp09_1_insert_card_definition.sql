    insert into mtgcard(name) values ('Rise from the Grave') on conflict do nothing;
    insert into mtgcard(name) values ('Mind Control') on conflict do nothing;
    insert into mtgcard(name) values ('Path to Exile') on conflict do nothing;
    insert into mtgcard(name) values ('Vampire Nighthawk') on conflict do nothing;
    insert into mtgcard(name) values ('Hellspark Elemental') on conflict do nothing;
    insert into mtgcard(name) values ('Slave of Bolas') on conflict do nothing;
    insert into mtgcard(name) values ('Kor Duelist') on conflict do nothing;
    insert into mtgcard(name) values ('Marisi''s Twinclaws') on conflict do nothing;
