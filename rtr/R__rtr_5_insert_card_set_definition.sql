insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Cremate'),
    (select id from sets where short_name = 'rtr'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Isperia''s Skywatch'),
    (select id from sets where short_name = 'rtr'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jarad, Golgari Lich Lord'),
    (select id from sets where short_name = 'rtr'),
    '174',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dramatic Rescue'),
    (select id from sets where short_name = 'rtr'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Havoc Festival'),
    (select id from sets where short_name = 'rtr'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos, Lord of Riots'),
    (select id from sets where short_name = 'rtr'),
    '187',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rakdos Shred-Freak'),
    (select id from sets where short_name = 'rtr'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oak Street Innkeeper'),
    (select id from sets where short_name = 'rtr'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Centaur''s Herald'),
    (select id from sets where short_name = 'rtr'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dispel'),
    (select id from sets where short_name = 'rtr'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sewer Shambler'),
    (select id from sets where short_name = 'rtr'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savage Surge'),
    (select id from sets where short_name = 'rtr'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Judge''s Familiar'),
    (select id from sets where short_name = 'rtr'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Charm'),
    (select id from sets where short_name = 'rtr'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rites of Reaping'),
    (select id from sets where short_name = 'rtr'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lyev Skyknight'),
    (select id from sets where short_name = 'rtr'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantom General'),
    (select id from sets where short_name = 'rtr'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knightly Valor'),
    (select id from sets where short_name = 'rtr'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Growing Ranks'),
    (select id from sets where short_name = 'rtr'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunspire Griffin'),
    (select id from sets where short_name = 'rtr'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'rtr'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sundering Growth'),
    (select id from sets where short_name = 'rtr'),
    '223',
    'common'
) ,
(
    (select id from mtgcard where name = 'Racecourse Fury'),
    (select id from sets where short_name = 'rtr'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selesnya Keyrune'),
    (select id from sets where short_name = 'rtr'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pack Rat'),
    (select id from sets where short_name = 'rtr'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Izzet Staticaster'),
    (select id from sets where short_name = 'rtr'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terrus Wurm'),
    (select id from sets where short_name = 'rtr'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azorius Arrester'),
    (select id from sets where short_name = 'rtr'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Underworld Connections'),
    (select id from sets where short_name = 'rtr'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grim Roustabout'),
    (select id from sets where short_name = 'rtr'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chronic Flooding'),
    (select id from sets where short_name = 'rtr'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overgrown Tomb'),
    (select id from sets where short_name = 'rtr'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skull Rend'),
    (select id from sets where short_name = 'rtr'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Charm'),
    (select id from sets where short_name = 'rtr'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ash Zealot'),
    (select id from sets where short_name = 'rtr'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cyclonic Rift'),
    (select id from sets where short_name = 'rtr'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Guildgate'),
    (select id from sets where short_name = 'rtr'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Martial Law'),
    (select id from sets where short_name = 'rtr'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wayfaring Temple'),
    (select id from sets where short_name = 'rtr'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chorus of Might'),
    (select id from sets where short_name = 'rtr'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Revenant'),
    (select id from sets where short_name = 'rtr'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Voidwielder'),
    (select id from sets where short_name = 'rtr'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azorius Keyrune'),
    (select id from sets where short_name = 'rtr'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'rtr'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azorius Charm'),
    (select id from sets where short_name = 'rtr'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dreg Mangler'),
    (select id from sets where short_name = 'rtr'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Civic Saber'),
    (select id from sets where short_name = 'rtr'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seller of Songbirds'),
    (select id from sets where short_name = 'rtr'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dryad Militant'),
    (select id from sets where short_name = 'rtr'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Perilous Shadow'),
    (select id from sets where short_name = 'rtr'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'rtr'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Korozda Monitor'),
    (select id from sets where short_name = 'rtr'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stealer of Secrets'),
    (select id from sets where short_name = 'rtr'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx of the Chimes'),
    (select id from sets where short_name = 'rtr'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soulsworn Spirit'),
    (select id from sets where short_name = 'rtr'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'rtr'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pursuit of Flight'),
    (select id from sets where short_name = 'rtr'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rest in Peace'),
    (select id from sets where short_name = 'rtr'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urban Burgeoning'),
    (select id from sets where short_name = 'rtr'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Explosive Impact'),
    (select id from sets where short_name = 'rtr'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abrupt Decay'),
    (select id from sets where short_name = 'rtr'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'rtr'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lotleth Troll'),
    (select id from sets where short_name = 'rtr'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grove of the Guardian'),
    (select id from sets where short_name = 'rtr'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Paralyzing Grasp'),
    (select id from sets where short_name = 'rtr'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx''s Revelation'),
    (select id from sets where short_name = 'rtr'),
    '200',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Epic Experiment'),
    (select id from sets where short_name = 'rtr'),
    '159',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Slime Molding'),
    (select id from sets where short_name = 'rtr'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tenement Crasher'),
    (select id from sets where short_name = 'rtr'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gobbling Ooze'),
    (select id from sets where short_name = 'rtr'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'rtr'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tavern Swindler'),
    (select id from sets where short_name = 'rtr'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grave Betrayal'),
    (select id from sets where short_name = 'rtr'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Charm'),
    (select id from sets where short_name = 'rtr'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hellhole Flailer'),
    (select id from sets where short_name = 'rtr'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arrest'),
    (select id from sets where short_name = 'rtr'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Street Sweeper'),
    (select id from sets where short_name = 'rtr'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drainpipe Vermin'),
    (select id from sets where short_name = 'rtr'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cobblebrute'),
    (select id from sets where short_name = 'rtr'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Supreme Verdict'),
    (select id from sets where short_name = 'rtr'),
    '201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volatile Rig'),
    (select id from sets where short_name = 'rtr'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armada Wurm'),
    (select id from sets where short_name = 'rtr'),
    '143',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Golgari Decoy'),
    (select id from sets where short_name = 'rtr'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rubbleback Rhino'),
    (select id from sets where short_name = 'rtr'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = 'rtr'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inaction Injunction'),
    (select id from sets where short_name = 'rtr'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'rtr'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Catacomb Slug'),
    (select id from sets where short_name = 'rtr'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Collective Blessing'),
    (select id from sets where short_name = 'rtr'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Launch Party'),
    (select id from sets where short_name = 'rtr'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Worldspine Wurm'),
    (select id from sets where short_name = 'rtr'),
    '140',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gore-House Chainwalker'),
    (select id from sets where short_name = 'rtr'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chromatic Lantern'),
    (select id from sets where short_name = 'rtr'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyline Predator'),
    (select id from sets where short_name = 'rtr'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vandalblast'),
    (select id from sets where short_name = 'rtr'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Common Bond'),
    (select id from sets where short_name = 'rtr'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychic Spiral'),
    (select id from sets where short_name = 'rtr'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brushstrider'),
    (select id from sets where short_name = 'rtr'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doorkeeper'),
    (select id from sets where short_name = 'rtr'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tower Drake'),
    (select id from sets where short_name = 'rtr'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bellows Lizard'),
    (select id from sets where short_name = 'rtr'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gatecreeper Vine'),
    (select id from sets where short_name = 'rtr'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Electrickery'),
    (select id from sets where short_name = 'rtr'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Precinct Captain'),
    (select id from sets where short_name = 'rtr'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Cackler'),
    (select id from sets where short_name = 'rtr'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skymark Roc'),
    (select id from sets where short_name = 'rtr'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spawn of Rix Maadi'),
    (select id from sets where short_name = 'rtr'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azor''s Elocutors'),
    (select id from sets where short_name = 'rtr'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fall of the Gavel'),
    (select id from sets where short_name = 'rtr'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keening Apparition'),
    (select id from sets where short_name = 'rtr'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Righteous Authority'),
    (select id from sets where short_name = 'rtr'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loxodon Smiter'),
    (select id from sets where short_name = 'rtr'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Izzet Guildgate'),
    (select id from sets where short_name = 'rtr'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avenging Arrow'),
    (select id from sets where short_name = 'rtr'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'New Prahv Guildmage'),
    (select id from sets where short_name = 'rtr'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deadbridge Goliath'),
    (select id from sets where short_name = 'rtr'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tablet of the Guilds'),
    (select id from sets where short_name = 'rtr'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Ringleader'),
    (select id from sets where short_name = 'rtr'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyroconvergence'),
    (select id from sets where short_name = 'rtr'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trestle Troll'),
    (select id from sets where short_name = 'rtr'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slum Reaper'),
    (select id from sets where short_name = 'rtr'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deathrite Shaman'),
    (select id from sets where short_name = 'rtr'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Daggerdrome Imp'),
    (select id from sets where short_name = 'rtr'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Axebane Guardian'),
    (select id from sets where short_name = 'rtr'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'rtr'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runewing'),
    (select id from sets where short_name = 'rtr'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Korozda Guildmage'),
    (select id from sets where short_name = 'rtr'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ethereal Armor'),
    (select id from sets where short_name = 'rtr'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Tithe'),
    (select id from sets where short_name = 'rtr'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'rtr'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pithing Needle'),
    (select id from sets where short_name = 'rtr'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'rtr'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trained Caracal'),
    (select id from sets where short_name = 'rtr'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Electromancer'),
    (select id from sets where short_name = 'rtr'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'rtr'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stab Wound'),
    (select id from sets where short_name = 'rtr'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sluiceway Scorpion'),
    (select id from sets where short_name = 'rtr'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horncaller''s Chant'),
    (select id from sets where short_name = 'rtr'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Splatter Thug'),
    (select id from sets where short_name = 'rtr'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Impostor'),
    (select id from sets where short_name = 'rtr'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desecration Demon'),
    (select id from sets where short_name = 'rtr'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Centaur Healer'),
    (select id from sets where short_name = 'rtr'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Druid''s Deliverance'),
    (select id from sets where short_name = 'rtr'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Keyrune'),
    (select id from sets where short_name = 'rtr'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mercurial Chemister'),
    (select id from sets where short_name = 'rtr'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hover Barrier'),
    (select id from sets where short_name = 'rtr'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vraska the Unseen'),
    (select id from sets where short_name = 'rtr'),
    '208',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blood Crypt'),
    (select id from sets where short_name = 'rtr'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crosstown Courier'),
    (select id from sets where short_name = 'rtr'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'rtr'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guild Feud'),
    (select id from sets where short_name = 'rtr'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Codex Shredder'),
    (select id from sets where short_name = 'rtr'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Guildgate'),
    (select id from sets where short_name = 'rtr'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cryptborn Horror'),
    (select id from sets where short_name = 'rtr'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dead Reveler'),
    (select id from sets where short_name = 'rtr'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Transguild Promenade'),
    (select id from sets where short_name = 'rtr'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golgari Keyrune'),
    (select id from sets where short_name = 'rtr'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Counterflux'),
    (select id from sets where short_name = 'rtr'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Auger Spree'),
    (select id from sets where short_name = 'rtr'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'rtr'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azorius Guildgate'),
    (select id from sets where short_name = 'rtr'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heroes'' Reunion'),
    (select id from sets where short_name = 'rtr'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Street Spasm'),
    (select id from sets where short_name = 'rtr'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ogre Jailbreaker'),
    (select id from sets where short_name = 'rtr'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Sentry'),
    (select id from sets where short_name = 'rtr'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Firemind''s Foresight'),
    (select id from sets where short_name = 'rtr'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Search the City'),
    (select id from sets where short_name = 'rtr'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trostani, Selesnya''s Voice'),
    (select id from sets where short_name = 'rtr'),
    '206',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nivix Guildmage'),
    (select id from sets where short_name = 'rtr'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guttersnipe'),
    (select id from sets where short_name = 'rtr'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azorius Justiciar'),
    (select id from sets where short_name = 'rtr'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Risen Sanctuary'),
    (select id from sets where short_name = 'rtr'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stonefare Crocodile'),
    (select id from sets where short_name = 'rtr'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasured Find'),
    (select id from sets where short_name = 'rtr'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fencing Ace'),
    (select id from sets where short_name = 'rtr'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Beastmaster'),
    (select id from sets where short_name = 'rtr'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'rtr'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Survey the Wreckage'),
    (select id from sets where short_name = 'rtr'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Batterhorn'),
    (select id from sets where short_name = 'rtr'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conjured Currency'),
    (select id from sets where short_name = 'rtr'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreadbore'),
    (select id from sets where short_name = 'rtr'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodfray Giant'),
    (select id from sets where short_name = 'rtr'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swift Justice'),
    (select id from sets where short_name = 'rtr'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Charm'),
    (select id from sets where short_name = 'rtr'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Essence Backlash'),
    (select id from sets where short_name = 'rtr'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrill-Kill Assassin'),
    (select id from sets where short_name = 'rtr'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'rtr'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zanikev Locust'),
    (select id from sets where short_name = 'rtr'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Annihilating Fire'),
    (select id from sets where short_name = 'rtr'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aquus Steed'),
    (select id from sets where short_name = 'rtr'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hallowed Fountain'),
    (select id from sets where short_name = 'rtr'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Detention Sphere'),
    (select id from sets where short_name = 'rtr'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bazaar Krovod'),
    (select id from sets where short_name = 'rtr'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'rtr'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nivmagus Elemental'),
    (select id from sets where short_name = 'rtr'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Isperia, Supreme Judge'),
    (select id from sets where short_name = 'rtr'),
    '171',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Corpsejack Menace'),
    (select id from sets where short_name = 'rtr'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necropolis Regent'),
    (select id from sets where short_name = 'rtr'),
    '71',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Seek the Horizon'),
    (select id from sets where short_name = 'rtr'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Temple Garden'),
    (select id from sets where short_name = 'rtr'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lobber Crew'),
    (select id from sets where short_name = 'rtr'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frostburn Weird'),
    (select id from sets where short_name = 'rtr'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Syncopate'),
    (select id from sets where short_name = 'rtr'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Ragemutt'),
    (select id from sets where short_name = 'rtr'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Destroy the Evidence'),
    (select id from sets where short_name = 'rtr'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'rtr'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'rtr'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eyes in the Skies'),
    (select id from sets where short_name = 'rtr'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slaughter Games'),
    (select id from sets where short_name = 'rtr'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viashino Racketeer'),
    (select id from sets where short_name = 'rtr'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archon of the Triumvirate'),
    (select id from sets where short_name = 'rtr'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Axebane Stag'),
    (select id from sets where short_name = 'rtr'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armory Guard'),
    (select id from sets where short_name = 'rtr'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Niv-Mizzet, Dracogenius'),
    (select id from sets where short_name = 'rtr'),
    '183',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jarad''s Orders'),
    (select id from sets where short_name = 'rtr'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Towering Indrik'),
    (select id from sets where short_name = 'rtr'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Call of the Conclave'),
    (select id from sets where short_name = 'rtr'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chaos Imps'),
    (select id from sets where short_name = 'rtr'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphere of Safety'),
    (select id from sets where short_name = 'rtr'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'rtr'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blistercoil Weird'),
    (select id from sets where short_name = 'rtr'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ultimate Price'),
    (select id from sets where short_name = 'rtr'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Longlegs'),
    (select id from sets where short_name = 'rtr'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Traitorous Instinct'),
    (select id from sets where short_name = 'rtr'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mizzium Mortars'),
    (select id from sets where short_name = 'rtr'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jace, Architect of Thought'),
    (select id from sets where short_name = 'rtr'),
    '44',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Search Warrant'),
    (select id from sets where short_name = 'rtr'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trostani''s Judgment'),
    (select id from sets where short_name = 'rtr'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakdos''s Return'),
    (select id from sets where short_name = 'rtr'),
    '188',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mana Bloom'),
    (select id from sets where short_name = 'rtr'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carnival Hellsteed'),
    (select id from sets where short_name = 'rtr'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thoughtflare'),
    (select id from sets where short_name = 'rtr'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slitherhead'),
    (select id from sets where short_name = 'rtr'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mizzium Skin'),
    (select id from sets where short_name = 'rtr'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blustersquall'),
    (select id from sets where short_name = 'rtr'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chemister''s Trick'),
    (select id from sets where short_name = 'rtr'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grisly Salvage'),
    (select id from sets where short_name = 'rtr'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hussar Patrol'),
    (select id from sets where short_name = 'rtr'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'rtr'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steam Vents'),
    (select id from sets where short_name = 'rtr'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rootborn Defenses'),
    (select id from sets where short_name = 'rtr'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vassal Soul'),
    (select id from sets where short_name = 'rtr'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shrieking Affliction'),
    (select id from sets where short_name = 'rtr'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'rtr'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deviant Glee'),
    (select id from sets where short_name = 'rtr'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minotaur Aggressor'),
    (select id from sets where short_name = 'rtr'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inspiration'),
    (select id from sets where short_name = 'rtr'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teleportal'),
    (select id from sets where short_name = 'rtr'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drudge Beetle'),
    (select id from sets where short_name = 'rtr'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Rally'),
    (select id from sets where short_name = 'rtr'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vitu-Ghazi Guildmage'),
    (select id from sets where short_name = 'rtr'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dynacharge'),
    (select id from sets where short_name = 'rtr'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Security Blockade'),
    (select id from sets where short_name = 'rtr'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'rtr'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Downsize'),
    (select id from sets where short_name = 'rtr'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rix Maadi Guildmage'),
    (select id from sets where short_name = 'rtr'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aerial Predation'),
    (select id from sets where short_name = 'rtr'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'rtr'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'rtr'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hypersonic Dragon'),
    (select id from sets where short_name = 'rtr'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coursers'' Accord'),
    (select id from sets where short_name = 'rtr'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Concordia Pegasus'),
    (select id from sets where short_name = 'rtr'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Serenity'),
    (select id from sets where short_name = 'rtr'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'rtr'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Utvara Hellkite'),
    (select id from sets where short_name = 'rtr'),
    '110',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rogue''s Passage'),
    (select id from sets where short_name = 'rtr'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Keyrune'),
    (select id from sets where short_name = 'rtr'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Assassin''s Strike'),
    (select id from sets where short_name = 'rtr'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'rtr'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archweaver'),
    (select id from sets where short_name = 'rtr'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death''s Presence'),
    (select id from sets where short_name = 'rtr'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cancel'),
    (select id from sets where short_name = 'rtr'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Palisade Giant'),
    (select id from sets where short_name = 'rtr'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selesnya Guildgate'),
    (select id from sets where short_name = 'rtr'),
    '246',
    'common'
) 
 on conflict do nothing;
