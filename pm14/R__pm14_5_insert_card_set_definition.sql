insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ratchet Bomb'),
    (select id from sets where short_name = 'pm14'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hive Stirrings'),
    (select id from sets where short_name = 'pm14'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Colossal Whale'),
    (select id from sets where short_name = 'pm14'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Megantic Sliver'),
    (select id from sets where short_name = 'pm14'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Diplomats'),
    (select id from sets where short_name = 'pm14'),
    '141',
    'rare'
) 
 on conflict do nothing;
