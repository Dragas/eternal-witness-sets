insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Lightning Rager'),
    (select id from sets where short_name = 'tcm2'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tuktuk the Returned'),
    (select id from sets where short_name = 'tcm2'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Experience'),
    (select id from sets where short_name = 'tcm2'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird'),
    (select id from sets where short_name = 'tcm2'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr'),
    (select id from sets where short_name = 'tcm2'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurm'),
    (select id from sets where short_name = 'tcm2'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tcm2'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tcm2'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Germ'),
    (select id from sets where short_name = 'tcm2'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daretti, Scrap Savant Emblem'),
    (select id from sets where short_name = 'tcm2'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goat'),
    (select id from sets where short_name = 'tcm2'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight'),
    (select id from sets where short_name = 'tcm2'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental Shaman'),
    (select id from sets where short_name = 'tcm2'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Triskelavite'),
    (select id from sets where short_name = 'tcm2'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pentavite'),
    (select id from sets where short_name = 'tcm2'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shapeshifter'),
    (select id from sets where short_name = 'tcm2'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tcm2'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tcm2'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurm'),
    (select id from sets where short_name = 'tcm2'),
    '17',
    'common'
) 
 on conflict do nothing;
