insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Snake'),
    (select id from sets where short_name = 'tc19'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhino'),
    (select id from sets where short_name = 'tc19'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Assassin'),
    (select id from sets where short_name = 'tc19'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird'),
    (select id from sets where short_name = 'tc19'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurm'),
    (select id from sets where short_name = 'tc19'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tc19'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird'),
    (select id from sets where short_name = 'tc19'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tc19'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horror'),
    (select id from sets where short_name = 'tc19'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sculpture'),
    (select id from sets where short_name = 'tc19'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drake'),
    (select id from sets where short_name = 'tc19'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tc19'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Centaur'),
    (select id from sets where short_name = 'tc19'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Human'),
    (select id from sets where short_name = 'tc19'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tc19'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tc19'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Sanctions'),
    (select id from sets where short_name = 'tc19'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis Reignited Emblem'),
    (select id from sets where short_name = 'tc19'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heart-Piercer Manticore'),
    (select id from sets where short_name = 'tc19'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gargoyle'),
    (select id from sets where short_name = 'tc19'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Egg'),
    (select id from sets where short_name = 'tc19'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi'),
    (select id from sets where short_name = 'tc19'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tc19'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plant'),
    (select id from sets where short_name = 'tc19'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manifest'),
    (select id from sets where short_name = 'tc19'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure'),
    (select id from sets where short_name = 'tc19'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Morph'),
    (select id from sets where short_name = 'tc19'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pegasus'),
    (select id from sets where short_name = 'tc19'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tc19'),
    '19',
    'common'
) 
 on conflict do nothing;
