insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Nin, the Pain Artist'),
    (select id from sets where short_name = 'c17'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kess, Dissident Mage'),
    (select id from sets where short_name = 'c17'),
    '39',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Corpse Augur'),
    (select id from sets where short_name = 'c17'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fell the Mighty'),
    (select id from sets where short_name = 'c17'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Herald''s Horn'),
    (select id from sets where short_name = 'c17'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Damnable Pact'),
    (select id from sets where short_name = 'c17'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loxodon Warhammer'),
    (select id from sets where short_name = 'c17'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Guildgate'),
    (select id from sets where short_name = 'c17'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krosan Verge'),
    (select id from sets where short_name = 'c17'),
    '260',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mystic Monastery'),
    (select id from sets where short_name = 'c17'),
    '263',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azami, Lady of Scrolls'),
    (select id from sets where short_name = 'c17'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirari''s Wake'),
    (select id from sets where short_name = 'c17'),
    '181',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blade of the Bloodchief'),
    (select id from sets where short_name = 'c17'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scoured Barrens'),
    (select id from sets where short_name = 'c17'),
    '276',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merciless Eviction'),
    (select id from sets where short_name = 'c17'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merchant of Secrets'),
    (select id from sets where short_name = 'c17'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Niv-Mizzet, the Firemind'),
    (select id from sets where short_name = 'c17'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crux of Fate'),
    (select id from sets where short_name = 'c17'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dismal Backwater'),
    (select id from sets where short_name = 'c17'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opulent Palace'),
    (select id from sets where short_name = 'c17'),
    '267',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c17'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saltcrusted Steppe'),
    (select id from sets where short_name = 'c17'),
    '273',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Into the Roil'),
    (select id from sets where short_name = 'c17'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silumgar''s Command'),
    (select id from sets where short_name = 'c17'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c17'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frontier Bivouac'),
    (select id from sets where short_name = 'c17'),
    '251',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Niv-Mizzet, Dracogenius'),
    (select id from sets where short_name = 'c17'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Captivating Vampire'),
    (select id from sets where short_name = 'c17'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'c17'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elfhame Palace'),
    (select id from sets where short_name = 'c17'),
    '247',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grixis Panorama'),
    (select id from sets where short_name = 'c17'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fellwar Stone'),
    (select id from sets where short_name = 'c17'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kindred Summons'),
    (select id from sets where short_name = 'c17'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jazal Goldmane'),
    (select id from sets where short_name = 'c17'),
    '62',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Exotic Orchard'),
    (select id from sets where short_name = 'c17'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chaos Warp'),
    (select id from sets where short_name = 'c17'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sanguine Bond'),
    (select id from sets where short_name = 'c17'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Izzet Boilerworks'),
    (select id from sets where short_name = 'c17'),
    '256',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vein Drinker'),
    (select id from sets where short_name = 'c17'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Well of Lost Dreams'),
    (select id from sets where short_name = 'c17'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akoum Refuge'),
    (select id from sets where short_name = 'c17'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunscorch Regent'),
    (select id from sets where short_name = 'c17'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Guildgate'),
    (select id from sets where short_name = 'c17'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunspear Shikari'),
    (select id from sets where short_name = 'c17'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fist of Suns'),
    (select id from sets where short_name = 'c17'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crackling Doom'),
    (select id from sets where short_name = 'c17'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bladewing the Risen'),
    (select id from sets where short_name = 'c17'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temur Sabertooth'),
    (select id from sets where short_name = 'c17'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drana, Kalastria Bloodchief'),
    (select id from sets where short_name = 'c17'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ramos, Dragon Engine'),
    (select id from sets where short_name = 'c17'),
    '55',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Curse of Bounty'),
    (select id from sets where short_name = 'c17'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c17'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodhusk Ritualist'),
    (select id from sets where short_name = 'c17'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vindictive Lich'),
    (select id from sets where short_name = 'c17'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skeletal Vampire'),
    (select id from sets where short_name = 'c17'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Monastery Siege'),
    (select id from sets where short_name = 'c17'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zendikar Resurgent'),
    (select id from sets where short_name = 'c17'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Utvara Hellkite'),
    (select id from sets where short_name = 'c17'),
    '144',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Magus of the Mind'),
    (select id from sets where short_name = 'c17'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tranquil Expanse'),
    (select id from sets where short_name = 'c17'),
    '286',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tithe Drinker'),
    (select id from sets where short_name = 'c17'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Tribute'),
    (select id from sets where short_name = 'c17'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nivix Guildmage'),
    (select id from sets where short_name = 'c17'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forsaken Sanctuary'),
    (select id from sets where short_name = 'c17'),
    '250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disrupt Decorum'),
    (select id from sets where short_name = 'c17'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonlord''s Servant'),
    (select id from sets where short_name = 'c17'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sangromancer'),
    (select id from sets where short_name = 'c17'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kindred Boon'),
    (select id from sets where short_name = 'c17'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Territorial Hellkite'),
    (select id from sets where short_name = 'c17'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Go for the Throat'),
    (select id from sets where short_name = 'c17'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c17'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kindred Charge'),
    (select id from sets where short_name = 'c17'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rout'),
    (select id from sets where short_name = 'c17'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Qasali Slingers'),
    (select id from sets where short_name = 'c17'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodline Necromancer'),
    (select id from sets where short_name = 'c17'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c17'),
    '307',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orator of Ojutai'),
    (select id from sets where short_name = 'c17'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cultivate'),
    (select id from sets where short_name = 'c17'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Shikari'),
    (select id from sets where short_name = 'c17'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grasslands'),
    (select id from sets where short_name = 'c17'),
    '252',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Comet Storm'),
    (select id from sets where short_name = 'c17'),
    '132',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kemba, Kha Regent'),
    (select id from sets where short_name = 'c17'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archaeomancer'),
    (select id from sets where short_name = 'c17'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c17'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Taj-Nar Swordsmith'),
    (select id from sets where short_name = 'c17'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selesnya Guildgate'),
    (select id from sets where short_name = 'c17'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sword of Vengeance'),
    (select id from sets where short_name = 'c17'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swiftfoot Boots'),
    (select id from sets where short_name = 'c17'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Traverse the Outlands'),
    (select id from sets where short_name = 'c17'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cauldron Dance'),
    (select id from sets where short_name = 'c17'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul''s Majesty'),
    (select id from sets where short_name = 'c17'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serendib Sorcerer'),
    (select id from sets where short_name = 'c17'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jwar Isle Refuge'),
    (select id from sets where short_name = 'c17'),
    '258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quietus Spike'),
    (select id from sets where short_name = 'c17'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boneyard Scourge'),
    (select id from sets where short_name = 'c17'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scion of the Ur-Dragon'),
    (select id from sets where short_name = 'c17'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Portal Mage'),
    (select id from sets where short_name = 'c17'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armillary Sphere'),
    (select id from sets where short_name = 'c17'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Memory Plunder'),
    (select id from sets where short_name = 'c17'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seht''s Tiger'),
    (select id from sets where short_name = 'c17'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Commander''s Sphere'),
    (select id from sets where short_name = 'c17'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Taigam, Sidisi''s Hand'),
    (select id from sets where short_name = 'c17'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Decree of Pain'),
    (select id from sets where short_name = 'c17'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balan, Wandering Knight'),
    (select id from sets where short_name = 'c17'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Baron of Vizkopa'),
    (select id from sets where short_name = 'c17'),
    '164',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Frontier Siege'),
    (select id from sets where short_name = 'c17'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Oracle'),
    (select id from sets where short_name = 'c17'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vela the Night-Clad'),
    (select id from sets where short_name = 'c17'),
    '201',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Savage Lands'),
    (select id from sets where short_name = 'c17'),
    '275',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deathbringer Regent'),
    (select id from sets where short_name = 'c17'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = 'c17'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hunter''s Prowess'),
    (select id from sets where short_name = 'c17'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Licia, Sanguine Tribune'),
    (select id from sets where short_name = 'c17'),
    '40',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wayfarer''s Bauble'),
    (select id from sets where short_name = 'c17'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c17'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivid Creek'),
    (select id from sets where short_name = 'c17'),
    '290',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skeletal Scrying'),
    (select id from sets where short_name = 'c17'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquil Thicket'),
    (select id from sets where short_name = 'c17'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Shrine'),
    (select id from sets where short_name = 'c17'),
    '257',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Carnarium'),
    (select id from sets where short_name = 'c17'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sword of the Animist'),
    (select id from sets where short_name = 'c17'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hero''s Blade'),
    (select id from sets where short_name = 'c17'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakish Heir'),
    (select id from sets where short_name = 'c17'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nihil Spellbomb'),
    (select id from sets where short_name = 'c17'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Path of Ancestry'),
    (select id from sets where short_name = 'c17'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental Bond'),
    (select id from sets where short_name = 'c17'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vivid Crag'),
    (select id from sets where short_name = 'c17'),
    '289',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crumbling Necropolis'),
    (select id from sets where short_name = 'c17'),
    '244',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scourge of Valkas'),
    (select id from sets where short_name = 'c17'),
    '142',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Etherium-Horn Sorcerer'),
    (select id from sets where short_name = 'c17'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Body Double'),
    (select id from sets where short_name = 'c17'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stromkirk Captain'),
    (select id from sets where short_name = 'c17'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Door of Destinies'),
    (select id from sets where short_name = 'c17'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Secluded Steppe'),
    (select id from sets where short_name = 'c17'),
    '278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rogue''s Passage'),
    (select id from sets where short_name = 'c17'),
    '272',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c17'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Sanctuary'),
    (select id from sets where short_name = 'c17'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Market'),
    (select id from sets where short_name = 'c17'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Protection'),
    (select id from sets where short_name = 'c17'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blind Obedience'),
    (select id from sets where short_name = 'c17'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Impostor'),
    (select id from sets where short_name = 'c17'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Painful Truths'),
    (select id from sets where short_name = 'c17'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curse of Verbosity'),
    (select id from sets where short_name = 'c17'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Myriad Landscape'),
    (select id from sets where short_name = 'c17'),
    '262',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inalla, Archmage Ritualist'),
    (select id from sets where short_name = 'c17'),
    '38',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hellkite Charger'),
    (select id from sets where short_name = 'c17'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unstable Obelisk'),
    (select id from sets where short_name = 'c17'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodlord of Vaasgoth'),
    (select id from sets where short_name = 'c17'),
    '102',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'The Ur-Dragon'),
    (select id from sets where short_name = 'c17'),
    '48',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ojutai, Soul of Winter'),
    (select id from sets where short_name = 'c17'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Graypelt Refuge'),
    (select id from sets where short_name = 'c17'),
    '253',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abundance'),
    (select id from sets where short_name = 'c17'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Farseek'),
    (select id from sets where short_name = 'c17'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kabira Crossroads'),
    (select id from sets where short_name = 'c17'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirri, Weatherlight Duelist'),
    (select id from sets where short_name = 'c17'),
    '43',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mercurial Chemister'),
    (select id from sets where short_name = 'c17'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steel Hellkite'),
    (select id from sets where short_name = 'c17'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arahbo, Roar of the World'),
    (select id from sets where short_name = 'c17'),
    '35',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Phantom Nishoba'),
    (select id from sets where short_name = 'c17'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirror of the Forebears'),
    (select id from sets where short_name = 'c17'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Return to Dust'),
    (select id from sets where short_name = 'c17'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodfell Caves'),
    (select id from sets where short_name = 'c17'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necromantic Selection'),
    (select id from sets where short_name = 'c17'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leonin Arbiter'),
    (select id from sets where short_name = 'c17'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blossoming Sands'),
    (select id from sets where short_name = 'c17'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kolaghan, the Storm''s Fury'),
    (select id from sets where short_name = 'c17'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreamstone Hedron'),
    (select id from sets where short_name = 'c17'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raksha Golden Cub'),
    (select id from sets where short_name = 'c17'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mairsil, the Pretender'),
    (select id from sets where short_name = 'c17'),
    '41',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rakdos Guildgate'),
    (select id from sets where short_name = 'c17'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spellbound Dragon'),
    (select id from sets where short_name = 'c17'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Divine Reckoning'),
    (select id from sets where short_name = 'c17'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'O-Kagachi, Vengeful Kami'),
    (select id from sets where short_name = 'c17'),
    '45',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Edgar Markov'),
    (select id from sets where short_name = 'c17'),
    '36',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Crushing Vines'),
    (select id from sets where short_name = 'c17'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darksteel Ingot'),
    (select id from sets where short_name = 'c17'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nazahn, Revered Bladesmith'),
    (select id from sets where short_name = 'c17'),
    '44',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rakdos Signet'),
    (select id from sets where short_name = 'c17'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shifting Shadow'),
    (select id from sets where short_name = 'c17'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spirit of the Hearth'),
    (select id from sets where short_name = 'c17'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crimson Honor Guard'),
    (select id from sets where short_name = 'c17'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c17'),
    '306',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kindred Dominance'),
    (select id from sets where short_name = 'c17'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sandsteppe Citadel'),
    (select id from sets where short_name = 'c17'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stirring Wildwood'),
    (select id from sets where short_name = 'c17'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swiftwater Cliffs'),
    (select id from sets where short_name = 'c17'),
    '283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = 'c17'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Consuming Vapors'),
    (select id from sets where short_name = 'c17'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Galecaster Colossus'),
    (select id from sets where short_name = 'c17'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alms Collector'),
    (select id from sets where short_name = 'c17'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Qasali Pridemage'),
    (select id from sets where short_name = 'c17'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jareth, Leonine Titan'),
    (select id from sets where short_name = 'c17'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Condemn'),
    (select id from sets where short_name = 'c17'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mathas, Fiend Seeker'),
    (select id from sets where short_name = 'c17'),
    '42',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rakdos Charm'),
    (select id from sets where short_name = 'c17'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kodama''s Reach'),
    (select id from sets where short_name = 'c17'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ryusei, the Falling Star'),
    (select id from sets where short_name = 'c17'),
    '141',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cinder Barrens'),
    (select id from sets where short_name = 'c17'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heirloom Blade'),
    (select id from sets where short_name = 'c17'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spelltwine'),
    (select id from sets where short_name = 'c17'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skullclamp'),
    (select id from sets where short_name = 'c17'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jedit Ojanen of Efrava'),
    (select id from sets where short_name = 'c17'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Tempest'),
    (select id from sets where short_name = 'c17'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grappling Hook'),
    (select id from sets where short_name = 'c17'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rain of Thorns'),
    (select id from sets where short_name = 'c17'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stone Quarry'),
    (select id from sets where short_name = 'c17'),
    '282',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fractured Identity'),
    (select id from sets where short_name = 'c17'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Basilica'),
    (select id from sets where short_name = 'c17'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opportunity'),
    (select id from sets where short_name = 'c17'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Taigam, Ojutai Master'),
    (select id from sets where short_name = 'c17'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teneb, the Harvester'),
    (select id from sets where short_name = 'c17'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crosis''s Charm'),
    (select id from sets where short_name = 'c17'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vivid Marsh'),
    (select id from sets where short_name = 'c17'),
    '292',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodforged Battle-Axe'),
    (select id from sets where short_name = 'c17'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dromoka, the Eternal'),
    (select id from sets where short_name = 'c17'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Outpost Siege'),
    (select id from sets where short_name = 'c17'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivid Grove'),
    (select id from sets where short_name = 'c17'),
    '291',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crosis, the Purger'),
    (select id from sets where short_name = 'c17'),
    '168',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Malakir Bloodwitch'),
    (select id from sets where short_name = 'c17'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crucible of the Spirit Dragon'),
    (select id from sets where short_name = 'c17'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonspeaker Shaman'),
    (select id from sets where short_name = 'c17'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hungry Lynx'),
    (select id from sets where short_name = 'c17'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oreskos Explorer'),
    (select id from sets where short_name = 'c17'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Curse of Vitality'),
    (select id from sets where short_name = 'c17'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Puppeteer Clique'),
    (select id from sets where short_name = 'c17'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seaside Citadel'),
    (select id from sets where short_name = 'c17'),
    '277',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ambition''s Cost'),
    (select id from sets where short_name = 'c17'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Relic Crush'),
    (select id from sets where short_name = 'c17'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Signet'),
    (select id from sets where short_name = 'c17'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'c17'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urborg Volcano'),
    (select id from sets where short_name = 'c17'),
    '288',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Opal Palace'),
    (select id from sets where short_name = 'c17'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'c17'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Behemoth Sledge'),
    (select id from sets where short_name = 'c17'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scalelord Reckoner'),
    (select id from sets where short_name = 'c17'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leonin Relic-Warder'),
    (select id from sets where short_name = 'c17'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reality Shift'),
    (select id from sets where short_name = 'c17'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Worn Powerstone'),
    (select id from sets where short_name = 'c17'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silumgar, the Drifting Death'),
    (select id from sets where short_name = 'c17'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marchesa, the Black Rose'),
    (select id from sets where short_name = 'c17'),
    '177',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Boros Garrison'),
    (select id from sets where short_name = 'c17'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Intet, the Dreamer'),
    (select id from sets where short_name = 'c17'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa''s Pilgrimage'),
    (select id from sets where short_name = 'c17'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Greaves'),
    (select id from sets where short_name = 'c17'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'White Sun''s Zenith'),
    (select id from sets where short_name = 'c17'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curse of Disturbance'),
    (select id from sets where short_name = 'c17'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Syphon Mind'),
    (select id from sets where short_name = 'c17'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magus of the Abyss'),
    (select id from sets where short_name = 'c17'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c17'),
    '302',
    'common'
) ,
(
    (select id from mtgcard where name = 'Command Tower'),
    (select id from sets where short_name = 'c17'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Argentum Armor'),
    (select id from sets where short_name = 'c17'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Izzet Chronarch'),
    (select id from sets where short_name = 'c17'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Underworld Connections'),
    (select id from sets where short_name = 'c17'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Falkenrath Noble'),
    (select id from sets where short_name = 'c17'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Apprentice Necromancer'),
    (select id from sets where short_name = 'c17'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Haven of the Spirit Dragon'),
    (select id from sets where short_name = 'c17'),
    '255',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of the False God'),
    (select id from sets where short_name = 'c17'),
    '284',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terminate'),
    (select id from sets where short_name = 'c17'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wing Shards'),
    (select id from sets where short_name = 'c17'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blighted Woodland'),
    (select id from sets where short_name = 'c17'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fortunate Few'),
    (select id from sets where short_name = 'c17'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'c17'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c17'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pawn of Ulamog'),
    (select id from sets where short_name = 'c17'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vivid Meadow'),
    (select id from sets where short_name = 'c17'),
    '293',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Butcher of Malakir'),
    (select id from sets where short_name = 'c17'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Palace Siege'),
    (select id from sets where short_name = 'c17'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shadowmage Infiltrator'),
    (select id from sets where short_name = 'c17'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c17'),
    '309',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Chemister'),
    (select id from sets where short_name = 'c17'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c17'),
    '308',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c17'),
    '303',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodsworn Steward'),
    (select id from sets where short_name = 'c17'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curse of Opulence'),
    (select id from sets where short_name = 'c17'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Patron of the Vein'),
    (select id from sets where short_name = 'c17'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anowon, the Ruin Sage'),
    (select id from sets where short_name = 'c17'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bojuka Bog'),
    (select id from sets where short_name = 'c17'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harbinger of the Tides'),
    (select id from sets where short_name = 'c17'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fleecemane Lion'),
    (select id from sets where short_name = 'c17'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tyrant''s Familiar'),
    (select id from sets where short_name = 'c17'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kheru Mind-Eater'),
    (select id from sets where short_name = 'c17'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c17'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Atarka, World Render'),
    (select id from sets where short_name = 'c17'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcanis the Omnipotent'),
    (select id from sets where short_name = 'c17'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Artist'),
    (select id from sets where short_name = 'c17'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Broodmate Dragon'),
    (select id from sets where short_name = 'c17'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wind-Scarred Crag'),
    (select id from sets where short_name = 'c17'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crucible of Fire'),
    (select id from sets where short_name = 'c17'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dimir Aqueduct'),
    (select id from sets where short_name = 'c17'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Havengul Lich'),
    (select id from sets where short_name = 'c17'),
    '173',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Clone Legion'),
    (select id from sets where short_name = 'c17'),
    '84',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Read the Bones'),
    (select id from sets where short_name = 'c17'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kindred Discovery'),
    (select id from sets where short_name = 'c17'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Signet'),
    (select id from sets where short_name = 'c17'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mosswort Bridge'),
    (select id from sets where short_name = 'c17'),
    '261',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hedron Archive'),
    (select id from sets where short_name = 'c17'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mortify'),
    (select id from sets where short_name = 'c17'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampire Nighthawk'),
    (select id from sets where short_name = 'c17'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Polymorphist''s Jest'),
    (select id from sets where short_name = 'c17'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'New Blood'),
    (select id from sets where short_name = 'c17'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcane Sanctum'),
    (select id from sets where short_name = 'c17'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hammer of Nazahn'),
    (select id from sets where short_name = 'c17'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystifying Maze'),
    (select id from sets where short_name = 'c17'),
    '264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savage Ventmaw'),
    (select id from sets where short_name = 'c17'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stalking Leonin'),
    (select id from sets where short_name = 'c17'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wasitora, Nekoru Queen'),
    (select id from sets where short_name = 'c17'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Staff of Nin'),
    (select id from sets where short_name = 'c17'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nomad Outpost'),
    (select id from sets where short_name = 'c17'),
    '265',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harmonize'),
    (select id from sets where short_name = 'c17'),
    '151',
    'uncommon'
) 
 on conflict do nothing;
