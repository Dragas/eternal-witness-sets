insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Nin, the Pain Artist'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nin, the Pain Artist'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nin, the Pain Artist'),
        (select types.id from types where name = 'Vedalken')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nin, the Pain Artist'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kess, Dissident Mage'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kess, Dissident Mage'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kess, Dissident Mage'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kess, Dissident Mage'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Corpse Augur'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Corpse Augur'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Corpse Augur'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fell the Mighty'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Herald''s Horn'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Damnable Pact'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Loxodon Warhammer'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Loxodon Warhammer'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Boros Guildgate'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Boros Guildgate'),
        (select types.id from types where name = 'Gate')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Krosan Verge'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystic Monastery'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Azami, Lady of Scrolls'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Azami, Lady of Scrolls'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Azami, Lady of Scrolls'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Azami, Lady of Scrolls'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mirari''s Wake'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Blade of the Bloodchief'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Blade of the Bloodchief'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Scoured Barrens'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Merciless Eviction'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Merchant of Secrets'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Merchant of Secrets'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Merchant of Secrets'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Niv-Mizzet, the Firemind'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Niv-Mizzet, the Firemind'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Niv-Mizzet, the Firemind'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Niv-Mizzet, the Firemind'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crux of Fate'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dismal Backwater'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Opulent Palace'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Mountain')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Saltcrusted Steppe'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Into the Roil'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Silumgar''s Command'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Plains')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Frontier Bivouac'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Niv-Mizzet, Dracogenius'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Niv-Mizzet, Dracogenius'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Niv-Mizzet, Dracogenius'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Niv-Mizzet, Dracogenius'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Captivating Vampire'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Captivating Vampire'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Evolving Wilds'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elfhame Palace'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grixis Panorama'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fellwar Stone'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kindred Summons'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jazal Goldmane'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jazal Goldmane'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jazal Goldmane'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jazal Goldmane'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Exotic Orchard'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chaos Warp'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sanguine Bond'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Izzet Boilerworks'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vein Drinker'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vein Drinker'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Well of Lost Dreams'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Akoum Refuge'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sunscorch Regent'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sunscorch Regent'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Orzhov Guildgate'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Orzhov Guildgate'),
        (select types.id from types where name = 'Gate')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sunspear Shikari'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sunspear Shikari'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sunspear Shikari'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fist of Suns'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crackling Doom'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bladewing the Risen'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bladewing the Risen'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bladewing the Risen'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bladewing the Risen'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Temur Sabertooth'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Temur Sabertooth'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Drana, Kalastria Bloodchief'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Drana, Kalastria Bloodchief'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Drana, Kalastria Bloodchief'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Drana, Kalastria Bloodchief'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ramos, Dragon Engine'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ramos, Dragon Engine'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ramos, Dragon Engine'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ramos, Dragon Engine'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Curse of Bounty'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Curse of Bounty'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Curse of Bounty'),
        (select types.id from types where name = 'Curse')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Plains')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodhusk Ritualist'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodhusk Ritualist'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodhusk Ritualist'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vindictive Lich'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vindictive Lich'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vindictive Lich'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skeletal Vampire'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skeletal Vampire'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skeletal Vampire'),
        (select types.id from types where name = 'Skeleton')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Monastery Siege'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zendikar Resurgent'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Utvara Hellkite'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Utvara Hellkite'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Magus of the Mind'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Magus of the Mind'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Magus of the Mind'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tranquil Expanse'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tithe Drinker'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tithe Drinker'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Blood Tribute'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nivix Guildmage'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nivix Guildmage'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nivix Guildmage'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forsaken Sanctuary'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Disrupt Decorum'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dragonlord''s Servant'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dragonlord''s Servant'),
        (select types.id from types where name = 'Goblin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dragonlord''s Servant'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sangromancer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sangromancer'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sangromancer'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kindred Boon'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Territorial Hellkite'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Territorial Hellkite'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Go for the Throat'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Swamp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kindred Charge'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rout'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Qasali Slingers'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Qasali Slingers'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Qasali Slingers'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodline Necromancer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodline Necromancer'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodline Necromancer'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Forest')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Orator of Ojutai'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Orator of Ojutai'),
        (select types.id from types where name = 'Bird')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Orator of Ojutai'),
        (select types.id from types where name = 'Monk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cultivate'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Leonin Shikari'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Leonin Shikari'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Leonin Shikari'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grasslands'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Comet Storm'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kemba, Kha Regent'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kemba, Kha Regent'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kemba, Kha Regent'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kemba, Kha Regent'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Archaeomancer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Archaeomancer'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Archaeomancer'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Island')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Taj-Nar Swordsmith'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Taj-Nar Swordsmith'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Taj-Nar Swordsmith'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Selesnya Guildgate'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Selesnya Guildgate'),
        (select types.id from types where name = 'Gate')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sword of Vengeance'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sword of Vengeance'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swiftfoot Boots'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swiftfoot Boots'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Traverse the Outlands'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cauldron Dance'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soul''s Majesty'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Serendib Sorcerer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Serendib Sorcerer'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Serendib Sorcerer'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jwar Isle Refuge'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Quietus Spike'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Quietus Spike'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Boneyard Scourge'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Boneyard Scourge'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Boneyard Scourge'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Scion of the Ur-Dragon'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Scion of the Ur-Dragon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Scion of the Ur-Dragon'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Scion of the Ur-Dragon'),
        (select types.id from types where name = 'Avatar')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Portal Mage'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Portal Mage'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Portal Mage'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Armillary Sphere'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Memory Plunder'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Seht''s Tiger'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Seht''s Tiger'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Commander''s Sphere'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Taigam, Sidisi''s Hand'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Taigam, Sidisi''s Hand'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Taigam, Sidisi''s Hand'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Taigam, Sidisi''s Hand'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Decree of Pain'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Balan, Wandering Knight'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Balan, Wandering Knight'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Balan, Wandering Knight'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Balan, Wandering Knight'),
        (select types.id from types where name = 'Knight')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Blood Baron of Vizkopa'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Blood Baron of Vizkopa'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Frontier Siege'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sea Gate Oracle'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sea Gate Oracle'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sea Gate Oracle'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vela the Night-Clad'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vela the Night-Clad'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vela the Night-Clad'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vela the Night-Clad'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Savage Lands'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Deathbringer Regent'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Deathbringer Regent'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Earthquake'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hunter''s Prowess'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Licia, Sanguine Tribune'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Licia, Sanguine Tribune'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Licia, Sanguine Tribune'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Licia, Sanguine Tribune'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wayfarer''s Bauble'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Island')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vivid Creek'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skeletal Scrying'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tranquil Thicket'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jungle Shrine'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rakdos Carnarium'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sword of the Animist'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sword of the Animist'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sword of the Animist'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hero''s Blade'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hero''s Blade'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rakish Heir'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rakish Heir'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nihil Spellbomb'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Path of Ancestry'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elemental Bond'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vivid Crag'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crumbling Necropolis'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Scourge of Valkas'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Scourge of Valkas'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Etherium-Horn Sorcerer'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Etherium-Horn Sorcerer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Etherium-Horn Sorcerer'),
        (select types.id from types where name = 'Minotaur')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Etherium-Horn Sorcerer'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Body Double'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Body Double'),
        (select types.id from types where name = 'Shapeshifter')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stromkirk Captain'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stromkirk Captain'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stromkirk Captain'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Door of Destinies'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Secluded Steppe'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rogue''s Passage'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Mountain')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Selesnya Sanctuary'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Black Market'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Teferi''s Protection'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Blind Obedience'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dark Impostor'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dark Impostor'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dark Impostor'),
        (select types.id from types where name = 'Assassin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Painful Truths'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Curse of Verbosity'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Curse of Verbosity'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Curse of Verbosity'),
        (select types.id from types where name = 'Curse')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Myriad Landscape'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Inalla, Archmage Ritualist'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Inalla, Archmage Ritualist'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Inalla, Archmage Ritualist'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Inalla, Archmage Ritualist'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hellkite Charger'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hellkite Charger'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Unstable Obelisk'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodlord of Vaasgoth'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodlord of Vaasgoth'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodlord of Vaasgoth'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Ur-Dragon'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Ur-Dragon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Ur-Dragon'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Ur-Dragon'),
        (select types.id from types where name = 'Avatar')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ojutai, Soul of Winter'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ojutai, Soul of Winter'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ojutai, Soul of Winter'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Graypelt Refuge'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Abundance'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Farseek'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kabira Crossroads'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mirri, Weatherlight Duelist'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mirri, Weatherlight Duelist'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mirri, Weatherlight Duelist'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mirri, Weatherlight Duelist'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mercurial Chemister'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mercurial Chemister'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mercurial Chemister'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Steel Hellkite'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Steel Hellkite'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Steel Hellkite'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Arahbo, Roar of the World'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Arahbo, Roar of the World'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Arahbo, Roar of the World'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Arahbo, Roar of the World'),
        (select types.id from types where name = 'Avatar')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Phantom Nishoba'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Phantom Nishoba'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Phantom Nishoba'),
        (select types.id from types where name = 'Beast')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Phantom Nishoba'),
        (select types.id from types where name = 'Spirit')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mirror of the Forebears'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Return to Dust'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodfell Caves'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Necromantic Selection'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Leonin Arbiter'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Leonin Arbiter'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Leonin Arbiter'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Blossoming Sands'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kolaghan, the Storm''s Fury'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kolaghan, the Storm''s Fury'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kolaghan, the Storm''s Fury'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dreamstone Hedron'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Raksha Golden Cub'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Raksha Golden Cub'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Raksha Golden Cub'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Raksha Golden Cub'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mairsil, the Pretender'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mairsil, the Pretender'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mairsil, the Pretender'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mairsil, the Pretender'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rakdos Guildgate'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rakdos Guildgate'),
        (select types.id from types where name = 'Gate')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spellbound Dragon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spellbound Dragon'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Divine Reckoning'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'O-Kagachi, Vengeful Kami'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'O-Kagachi, Vengeful Kami'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'O-Kagachi, Vengeful Kami'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'O-Kagachi, Vengeful Kami'),
        (select types.id from types where name = 'Spirit')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Edgar Markov'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Edgar Markov'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Edgar Markov'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Edgar Markov'),
        (select types.id from types where name = 'Knight')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crushing Vines'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Darksteel Ingot'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nazahn, Revered Bladesmith'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nazahn, Revered Bladesmith'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nazahn, Revered Bladesmith'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nazahn, Revered Bladesmith'),
        (select types.id from types where name = 'Artificer')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rakdos Signet'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shifting Shadow'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shifting Shadow'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spirit of the Hearth'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spirit of the Hearth'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spirit of the Hearth'),
        (select types.id from types where name = 'Spirit')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crimson Honor Guard'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crimson Honor Guard'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crimson Honor Guard'),
        (select types.id from types where name = 'Knight')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Mountain')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kindred Dominance'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sandsteppe Citadel'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stirring Wildwood'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swiftwater Cliffs'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nevinyrral''s Disk'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Consuming Vapors'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Galecaster Colossus'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Galecaster Colossus'),
        (select types.id from types where name = 'Giant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Galecaster Colossus'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Alms Collector'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Alms Collector'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Alms Collector'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Qasali Pridemage'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Qasali Pridemage'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Qasali Pridemage'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jareth, Leonine Titan'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jareth, Leonine Titan'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jareth, Leonine Titan'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jareth, Leonine Titan'),
        (select types.id from types where name = 'Giant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Condemn'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mathas, Fiend Seeker'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mathas, Fiend Seeker'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mathas, Fiend Seeker'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rakdos Charm'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kodama''s Reach'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kodama''s Reach'),
        (select types.id from types where name = 'Arcane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ryusei, the Falling Star'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ryusei, the Falling Star'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ryusei, the Falling Star'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ryusei, the Falling Star'),
        (select types.id from types where name = 'Spirit')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cinder Barrens'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Heirloom Blade'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Heirloom Blade'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spelltwine'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skullclamp'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skullclamp'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jedit Ojanen of Efrava'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jedit Ojanen of Efrava'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jedit Ojanen of Efrava'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jedit Ojanen of Efrava'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dragon Tempest'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grappling Hook'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grappling Hook'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rain of Thorns'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stone Quarry'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fractured Identity'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Orzhov Basilica'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Opportunity'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Taigam, Ojutai Master'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Taigam, Ojutai Master'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Taigam, Ojutai Master'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Taigam, Ojutai Master'),
        (select types.id from types where name = 'Monk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Teneb, the Harvester'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Teneb, the Harvester'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Teneb, the Harvester'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crosis''s Charm'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vivid Marsh'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodforged Battle-Axe'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodforged Battle-Axe'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dromoka, the Eternal'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dromoka, the Eternal'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dromoka, the Eternal'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Outpost Siege'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vivid Grove'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crosis, the Purger'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crosis, the Purger'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crosis, the Purger'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Malakir Bloodwitch'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Malakir Bloodwitch'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Malakir Bloodwitch'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crucible of the Spirit Dragon'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dragonspeaker Shaman'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dragonspeaker Shaman'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dragonspeaker Shaman'),
        (select types.id from types where name = 'Barbarian')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dragonspeaker Shaman'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hungry Lynx'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hungry Lynx'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Oreskos Explorer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Oreskos Explorer'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Oreskos Explorer'),
        (select types.id from types where name = 'Scout')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Curse of Vitality'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Curse of Vitality'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Curse of Vitality'),
        (select types.id from types where name = 'Curse')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Puppeteer Clique'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Puppeteer Clique'),
        (select types.id from types where name = 'Faerie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Puppeteer Clique'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Seaside Citadel'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ambition''s Cost'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Relic Crush'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Boros Signet'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sol Ring'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Urborg Volcano'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Opal Palace'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Terramorphic Expanse'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Behemoth Sledge'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Behemoth Sledge'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Scalelord Reckoner'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Scalelord Reckoner'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Leonin Relic-Warder'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Leonin Relic-Warder'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Leonin Relic-Warder'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Reality Shift'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Worn Powerstone'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Silumgar, the Drifting Death'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Silumgar, the Drifting Death'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Silumgar, the Drifting Death'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Marchesa, the Black Rose'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Marchesa, the Black Rose'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Marchesa, the Black Rose'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Marchesa, the Black Rose'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Boros Garrison'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Intet, the Dreamer'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Intet, the Dreamer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Intet, the Dreamer'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nissa''s Pilgrimage'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lightning Greaves'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lightning Greaves'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'White Sun''s Zenith'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Curse of Disturbance'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Curse of Disturbance'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Curse of Disturbance'),
        (select types.id from types where name = 'Curse')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Syphon Mind'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Magus of the Abyss'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Magus of the Abyss'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Magus of the Abyss'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Swamp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Command Tower'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Argentum Armor'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Argentum Armor'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Izzet Chronarch'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Izzet Chronarch'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Izzet Chronarch'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Underworld Connections'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Underworld Connections'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Falkenrath Noble'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Falkenrath Noble'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Falkenrath Noble'),
        (select types.id from types where name = 'Noble')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Apprentice Necromancer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Apprentice Necromancer'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Apprentice Necromancer'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Haven of the Spirit Dragon'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Temple of the False God'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Terminate'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wing Shards'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Blighted Woodland'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fortunate Few'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swords to Plowshares'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Island')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pawn of Ulamog'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pawn of Ulamog'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pawn of Ulamog'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vivid Meadow'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Butcher of Malakir'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Butcher of Malakir'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Butcher of Malakir'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Palace Siege'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shadowmage Infiltrator'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shadowmage Infiltrator'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shadowmage Infiltrator'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Forest')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Izzet Chemister'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Izzet Chemister'),
        (select types.id from types where name = 'Goblin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Izzet Chemister'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Forest')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Swamp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodsworn Steward'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodsworn Steward'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodsworn Steward'),
        (select types.id from types where name = 'Knight')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Curse of Opulence'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Curse of Opulence'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Curse of Opulence'),
        (select types.id from types where name = 'Curse')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Patron of the Vein'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Patron of the Vein'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Patron of the Vein'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Anowon, the Ruin Sage'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Anowon, the Ruin Sage'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Anowon, the Ruin Sage'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Anowon, the Ruin Sage'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bojuka Bog'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Harbinger of the Tides'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Harbinger of the Tides'),
        (select types.id from types where name = 'Merfolk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Harbinger of the Tides'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fleecemane Lion'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fleecemane Lion'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tyrant''s Familiar'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tyrant''s Familiar'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kheru Mind-Eater'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kheru Mind-Eater'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Plains')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Atarka, World Render'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Atarka, World Render'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Atarka, World Render'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Arcanis the Omnipotent'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Arcanis the Omnipotent'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Arcanis the Omnipotent'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Blood Artist'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Blood Artist'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Broodmate Dragon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Broodmate Dragon'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wind-Scarred Crag'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crucible of Fire'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dimir Aqueduct'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Havengul Lich'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Havengul Lich'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Havengul Lich'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Clone Legion'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Read the Bones'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kindred Discovery'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Orzhov Signet'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mosswort Bridge'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hedron Archive'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mortify'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vampire Nighthawk'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vampire Nighthawk'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vampire Nighthawk'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Polymorphist''s Jest'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'New Blood'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Arcane Sanctum'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hammer of Nazahn'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hammer of Nazahn'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hammer of Nazahn'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystifying Maze'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Savage Ventmaw'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Savage Ventmaw'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stalking Leonin'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stalking Leonin'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stalking Leonin'),
        (select types.id from types where name = 'Archer')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wasitora, Nekoru Queen'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wasitora, Nekoru Queen'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wasitora, Nekoru Queen'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wasitora, Nekoru Queen'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Staff of Nin'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nomad Outpost'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Harmonize'),
        (select types.id from types where name = 'Sorcery')
    ) 
 on conflict do nothing;
