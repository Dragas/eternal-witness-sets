insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tm14'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tm14'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'tm14'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goat'),
    (select id from sets where short_name = 'tm14'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sliver'),
    (select id from sets where short_name = 'tm14'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'tm14'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tm14'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tm14'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk, Caller of Beasts Emblem'),
    (select id from sets where short_name = 'tm14'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat'),
    (select id from sets where short_name = 'tm14'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tm14'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana of the Dark Realms Emblem'),
    (select id from sets where short_name = 'tm14'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tm14'),
    '8',
    'common'
) 
 on conflict do nothing;
