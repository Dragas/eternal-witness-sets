insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Kithkin Soldier'),
    (select id from sets where short_name = 'tshm'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tshm'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rat'),
    (select id from sets where short_name = 'tshm'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elf Warrior'),
    (select id from sets where short_name = 'tshm'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Warrior'),
    (select id from sets where short_name = 'tshm'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spider'),
    (select id from sets where short_name = 'tshm'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tshm'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'tshm'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Rogue'),
    (select id from sets where short_name = 'tshm'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elf Warrior'),
    (select id from sets where short_name = 'tshm'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tshm'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Warrior'),
    (select id from sets where short_name = 'tshm'),
    '11',
    'common'
) 
 on conflict do nothing;
