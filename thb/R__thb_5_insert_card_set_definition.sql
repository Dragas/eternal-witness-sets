insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Stinging Lionfish'),
    (select id from sets where short_name = 'thb'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heliod, Sun-Crowned'),
    (select id from sets where short_name = 'thb'),
    '18',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ironscale Hydra'),
    (select id from sets where short_name = 'thb'),
    '296',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eidolon of Obstruction'),
    (select id from sets where short_name = 'thb'),
    '299',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'thb'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pharika''s Libation'),
    (select id from sets where short_name = 'thb'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Underworld Dreams'),
    (select id from sets where short_name = 'thb'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arasta of the Endless Web'),
    (select id from sets where short_name = 'thb'),
    '325',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kroxa, Titan of Death''s Hunger'),
    (select id from sets where short_name = 'thb'),
    '340',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Woe Strider'),
    (select id from sets where short_name = 'thb'),
    '317',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nyxborn Brute'),
    (select id from sets where short_name = 'thb'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Entrancing Lyre'),
    (select id from sets where short_name = 'thb'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gallia of the Endless Dance'),
    (select id from sets where short_name = 'thb'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skola Grovedancer'),
    (select id from sets where short_name = 'thb'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Agonizing Remorse'),
    (select id from sets where short_name = 'thb'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alseid of Life''s Bounty'),
    (select id from sets where short_name = 'thb'),
    '353',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mire''s Grasp'),
    (select id from sets where short_name = 'thb'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stern Dismissal'),
    (select id from sets where short_name = 'thb'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bronze Sword'),
    (select id from sets where short_name = 'thb'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Underworld Breach'),
    (select id from sets where short_name = 'thb'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skophos Maze-Warden'),
    (select id from sets where short_name = 'thb'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elspeth''s Devotee'),
    (select id from sets where short_name = 'thb'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oread of Mountain''s Blaze'),
    (select id from sets where short_name = 'thb'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daxos, Blessed by the Sun'),
    (select id from sets where short_name = 'thb'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nyxborn Courser'),
    (select id from sets where short_name = 'thb'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Underworld Fires'),
    (select id from sets where short_name = 'thb'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enigmatic Incarnation'),
    (select id from sets where short_name = 'thb'),
    '337',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pheres-Band Brawler'),
    (select id from sets where short_name = 'thb'),
    '193',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pious Wayfarer'),
    (select id from sets where short_name = 'thb'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashiok, Nightmare Muse'),
    (select id from sets where short_name = 'thb'),
    '208',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Calix, Destiny''s Hand'),
    (select id from sets where short_name = 'thb'),
    '257',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Warbriar Blessing'),
    (select id from sets where short_name = 'thb'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deny the Divine'),
    (select id from sets where short_name = 'thb'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Staggering Insight'),
    (select id from sets where short_name = 'thb'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shadowspear'),
    (select id from sets where short_name = 'thb'),
    '345',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Captivating Unicorn'),
    (select id from sets where short_name = 'thb'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thassa''s Oracle'),
    (select id from sets where short_name = 'thb'),
    '308',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tymaret, Chosen from Death'),
    (select id from sets where short_name = 'thb'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mantle of the Wolf'),
    (select id from sets where short_name = 'thb'),
    '327',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Purphoros, Bronze-Blooded'),
    (select id from sets where short_name = 'thb'),
    '265',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Underworld Breach'),
    (select id from sets where short_name = 'thb'),
    '324',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tymaret, Chosen from Death'),
    (select id from sets where short_name = 'thb'),
    '263',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flummoxed Cyclops'),
    (select id from sets where short_name = 'thb'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rage-Scarred Berserker'),
    (select id from sets where short_name = 'thb'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightmare Shepherd'),
    (select id from sets where short_name = 'thb'),
    '315',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archon of Sun''s Grace'),
    (select id from sets where short_name = 'thb'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thirst for Meaning'),
    (select id from sets where short_name = 'thb'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Threnody Singer'),
    (select id from sets where short_name = 'thb'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eat to Extinction'),
    (select id from sets where short_name = 'thb'),
    '312',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Commanding Presence'),
    (select id from sets where short_name = 'thb'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Furious Rise'),
    (select id from sets where short_name = 'thb'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alirios, Enraptured'),
    (select id from sets where short_name = 'thb'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aspect of Lamprey'),
    (select id from sets where short_name = 'thb'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thassa, Deep-Dwelling'),
    (select id from sets where short_name = 'thb'),
    '261',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Enemy of Enlightenment'),
    (select id from sets where short_name = 'thb'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Akroan War'),
    (select id from sets where short_name = 'thb'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nylea, Keen-Eyed'),
    (select id from sets where short_name = 'thb'),
    '266',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Temple of Abandon'),
    (select id from sets where short_name = 'thb'),
    '347',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nylea''s Huntmaster'),
    (select id from sets where short_name = 'thb'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Siona, Captain of the Pyleas'),
    (select id from sets where short_name = 'thb'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Renata, Called to the Hunt'),
    (select id from sets where short_name = 'thb'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kiora Bests the Sea God'),
    (select id from sets where short_name = 'thb'),
    '52',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thassa''s Intervention'),
    (select id from sets where short_name = 'thb'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nylea''s Forerunner'),
    (select id from sets where short_name = 'thb'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrill of Possibility'),
    (select id from sets where short_name = 'thb'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eidolon of Obstruction'),
    (select id from sets where short_name = 'thb'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunlit Hoplite'),
    (select id from sets where short_name = 'thb'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thryx, the Sudden Storm'),
    (select id from sets where short_name = 'thb'),
    '309',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Funeral Rites'),
    (select id from sets where short_name = 'thb'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sentinel''s Eyes'),
    (select id from sets where short_name = 'thb'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'thb'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terror of Mount Velus'),
    (select id from sets where short_name = 'thb'),
    '295',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woe Strider'),
    (select id from sets where short_name = 'thb'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatter the Sky'),
    (select id from sets where short_name = 'thb'),
    '302',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treacherous Blessing'),
    (select id from sets where short_name = 'thb'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashiok, Sculptor of Fears'),
    (select id from sets where short_name = 'thb'),
    '274',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dryad of the Ilysian Grove'),
    (select id from sets where short_name = 'thb'),
    '326',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alseid of Life''s Bounty'),
    (select id from sets where short_name = 'thb'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elspeth''s Nightmare'),
    (select id from sets where short_name = 'thb'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Setessan Petitioner'),
    (select id from sets where short_name = 'thb'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ox of Agonas'),
    (select id from sets where short_name = 'thb'),
    '318',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Loathsome Chimera'),
    (select id from sets where short_name = 'thb'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heliod''s Punishment'),
    (select id from sets where short_name = 'thb'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Medomai''s Prophecy'),
    (select id from sets where short_name = 'thb'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Field of Ruin'),
    (select id from sets where short_name = 'thb'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Idyllic Tutor'),
    (select id from sets where short_name = 'thb'),
    '301',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mire Triton'),
    (select id from sets where short_name = 'thb'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sleep of the Dead'),
    (select id from sets where short_name = 'thb'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravebreaker Lamia'),
    (select id from sets where short_name = 'thb'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'thb'),
    '278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sage of Mysteries'),
    (select id from sets where short_name = 'thb'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thryx, the Sudden Storm'),
    (select id from sets where short_name = 'thb'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Haktos the Unscarred'),
    (select id from sets where short_name = 'thb'),
    '339',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Labyrinth of Skophos'),
    (select id from sets where short_name = 'thb'),
    '346',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treeshaker Chimera'),
    (select id from sets where short_name = 'thb'),
    '297',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Setessan Skirmisher'),
    (select id from sets where short_name = 'thb'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Binding of the Titans'),
    (select id from sets where short_name = 'thb'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Indomitable Will'),
    (select id from sets where short_name = 'thb'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unknown Shores'),
    (select id from sets where short_name = 'thb'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Triumph of Anax'),
    (select id from sets where short_name = 'thb'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightmare Shepherd'),
    (select id from sets where short_name = 'thb'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The First Iroan Games'),
    (select id from sets where short_name = 'thb'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm''s Wrath'),
    (select id from sets where short_name = 'thb'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nylea''s Intervention'),
    (select id from sets where short_name = 'thb'),
    '329',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Klothys, God of Destiny'),
    (select id from sets where short_name = 'thb'),
    '268',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rumbling Sentry'),
    (select id from sets where short_name = 'thb'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nessian Wanderer'),
    (select id from sets where short_name = 'thb'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hydra''s Growth'),
    (select id from sets where short_name = 'thb'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Devourer of Memory'),
    (select id from sets where short_name = 'thb'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sweet Oblivion'),
    (select id from sets where short_name = 'thb'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cling to Dust'),
    (select id from sets where short_name = 'thb'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunmane Pegasus'),
    (select id from sets where short_name = 'thb'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eidolon of Inspiration'),
    (select id from sets where short_name = 'thb'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'thb'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reverent Hoplite'),
    (select id from sets where short_name = 'thb'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wavebreak Hippocamp'),
    (select id from sets where short_name = 'thb'),
    '310',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Irreverent Revelers'),
    (select id from sets where short_name = 'thb'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arasta of the Endless Web'),
    (select id from sets where short_name = 'thb'),
    '352',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soulreaper of Mogis'),
    (select id from sets where short_name = 'thb'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fateful End'),
    (select id from sets where short_name = 'thb'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Impending Doom'),
    (select id from sets where short_name = 'thb'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demon of Loathing'),
    (select id from sets where short_name = 'thb'),
    '292',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dalakos, Crafter of Wonders'),
    (select id from sets where short_name = 'thb'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Traveler''s Amulet'),
    (select id from sets where short_name = 'thb'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skophos Warleader'),
    (select id from sets where short_name = 'thb'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flicker of Fate'),
    (select id from sets where short_name = 'thb'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aphemia, the Cacophony'),
    (select id from sets where short_name = 'thb'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Klothys, God of Destiny'),
    (select id from sets where short_name = 'thb'),
    '220',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nyx Lotus'),
    (select id from sets where short_name = 'thb'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phalanx Tactics'),
    (select id from sets where short_name = 'thb'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Temple of Plenty'),
    (select id from sets where short_name = 'thb'),
    '351',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashiok''s Erasure'),
    (select id from sets where short_name = 'thb'),
    '304',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Satyr''s Cunning'),
    (select id from sets where short_name = 'thb'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Witness of Tomorrows'),
    (select id from sets where short_name = 'thb'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'thb'),
    '282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fruit of Tizerus'),
    (select id from sets where short_name = 'thb'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Repeal'),
    (select id from sets where short_name = 'thb'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brine Giant'),
    (select id from sets where short_name = 'thb'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tymaret Calls the Dead'),
    (select id from sets where short_name = 'thb'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Starlit Mantle'),
    (select id from sets where short_name = 'thb'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nexus Wardens'),
    (select id from sets where short_name = 'thb'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erebos, Bleak-Hearted'),
    (select id from sets where short_name = 'thb'),
    '262',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Callaphe, Beloved of the Sea'),
    (select id from sets where short_name = 'thb'),
    '260',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'thb'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heliod''s Intervention'),
    (select id from sets where short_name = 'thb'),
    '300',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mindwrack Harpy'),
    (select id from sets where short_name = 'thb'),
    '276',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dryad of the Ilysian Grove'),
    (select id from sets where short_name = 'thb'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Plenty'),
    (select id from sets where short_name = 'thb'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tectonic Giant'),
    (select id from sets where short_name = 'thb'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm''s Wrath'),
    (select id from sets where short_name = 'thb'),
    '322',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Haktos the Unscarred'),
    (select id from sets where short_name = 'thb'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phoenix of Ash'),
    (select id from sets where short_name = 'thb'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Omen of the Forge'),
    (select id from sets where short_name = 'thb'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thassa''s Oracle'),
    (select id from sets where short_name = 'thb'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wolfwillow Haven'),
    (select id from sets where short_name = 'thb'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Protean Thaumaturge'),
    (select id from sets where short_name = 'thb'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relentless Pursuit'),
    (select id from sets where short_name = 'thb'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ox of Agonas'),
    (select id from sets where short_name = 'thb'),
    '147',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Triumphant Surge'),
    (select id from sets where short_name = 'thb'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gift of Strength'),
    (select id from sets where short_name = 'thb'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nadir Kraken'),
    (select id from sets where short_name = 'thb'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashiok''s Forerunner'),
    (select id from sets where short_name = 'thb'),
    '277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blight-Breath Catoblepas'),
    (select id from sets where short_name = 'thb'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thaumaturge''s Familiar'),
    (select id from sets where short_name = 'thb'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple of Enlightenment'),
    (select id from sets where short_name = 'thb'),
    '349',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gray Merchant of Asphodel'),
    (select id from sets where short_name = 'thb'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arasta of the Endless Web'),
    (select id from sets where short_name = 'thb'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul-Guide Lantern'),
    (select id from sets where short_name = 'thb'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treacherous Blessing'),
    (select id from sets where short_name = 'thb'),
    '316',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ilysian Caryatid'),
    (select id from sets where short_name = 'thb'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phoenix of Ash'),
    (select id from sets where short_name = 'thb'),
    '319',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dream Trawler'),
    (select id from sets where short_name = 'thb'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Omen of the Hunt'),
    (select id from sets where short_name = 'thb'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple of Abandon'),
    (select id from sets where short_name = 'thb'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hero of the Winds'),
    (select id from sets where short_name = 'thb'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dalakos, Crafter of Wonders'),
    (select id from sets where short_name = 'thb'),
    '335',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Victory''s Envoy'),
    (select id from sets where short_name = 'thb'),
    '289',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Omen of the Dead'),
    (select id from sets where short_name = 'thb'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple of Deceit'),
    (select id from sets where short_name = 'thb'),
    '348',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hateful Eidolon'),
    (select id from sets where short_name = 'thb'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elspeth, Sun''s Nemesis'),
    (select id from sets where short_name = 'thb'),
    '14',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hero of the Pride'),
    (select id from sets where short_name = 'thb'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nessian Hornbeetle'),
    (select id from sets where short_name = 'thb'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Protean Thaumaturge'),
    (select id from sets where short_name = 'thb'),
    '306',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elite Instructor'),
    (select id from sets where short_name = 'thb'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mantle of the Wolf'),
    (select id from sets where short_name = 'thb'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infuriate'),
    (select id from sets where short_name = 'thb'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Calix, Destiny''s Hand'),
    (select id from sets where short_name = 'thb'),
    '211',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Omen of the Sun'),
    (select id from sets where short_name = 'thb'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nyx Herald'),
    (select id from sets where short_name = 'thb'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thassa''s Intervention'),
    (select id from sets where short_name = 'thb'),
    '307',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vexing Gull'),
    (select id from sets where short_name = 'thb'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Athreos, Shroud-Veiled'),
    (select id from sets where short_name = 'thb'),
    '269',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chainweb Aracnir'),
    (select id from sets where short_name = 'thb'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Polukranos, Unchained'),
    (select id from sets where short_name = 'thb'),
    '224',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Karametra''s Blessing'),
    (select id from sets where short_name = 'thb'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erebos''s Intervention'),
    (select id from sets where short_name = 'thb'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thrill of Possibility'),
    (select id from sets where short_name = 'thb'),
    '356',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glory Bearers'),
    (select id from sets where short_name = 'thb'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nyxborn Colossus'),
    (select id from sets where short_name = 'thb'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Towering-Wave Mystic'),
    (select id from sets where short_name = 'thb'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Aspirant'),
    (select id from sets where short_name = 'thb'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Polukranos, Unchained'),
    (select id from sets where short_name = 'thb'),
    '342',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Final Death'),
    (select id from sets where short_name = 'thb'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heliod''s Intervention'),
    (select id from sets where short_name = 'thb'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bronzehide Lion'),
    (select id from sets where short_name = 'thb'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Escape Velocity'),
    (select id from sets where short_name = 'thb'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drag to the Underworld'),
    (select id from sets where short_name = 'thb'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'thb'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shadowspear'),
    (select id from sets where short_name = 'thb'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Malice'),
    (select id from sets where short_name = 'thb'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea God''s Scorn'),
    (select id from sets where short_name = 'thb'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Renata, Called to the Hunt'),
    (select id from sets where short_name = 'thb'),
    '267',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hero of the Nyxborn'),
    (select id from sets where short_name = 'thb'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'One with the Stars'),
    (select id from sets where short_name = 'thb'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nyxborn Seaguard'),
    (select id from sets where short_name = 'thb'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Revoke Existence'),
    (select id from sets where short_name = 'thb'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Venomous Hierophant'),
    (select id from sets where short_name = 'thb'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whirlwind Denial'),
    (select id from sets where short_name = 'thb'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Underworld Rage-Hound'),
    (select id from sets where short_name = 'thb'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anax, Hardened in the Forge'),
    (select id from sets where short_name = 'thb'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Uro, Titan of Nature''s Wrath'),
    (select id from sets where short_name = 'thb'),
    '229',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Eat to Extinction'),
    (select id from sets where short_name = 'thb'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nessian Boar'),
    (select id from sets where short_name = 'thb'),
    '328',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Acolyte of Affliction'),
    (select id from sets where short_name = 'thb'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Banishing Light'),
    (select id from sets where short_name = 'thb'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gravebreaker Lamia'),
    (select id from sets where short_name = 'thb'),
    '314',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grasping Giant'),
    (select id from sets where short_name = 'thb'),
    '288',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tectonic Giant'),
    (select id from sets where short_name = 'thb'),
    '323',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slaughter-Priest of Mogis'),
    (select id from sets where short_name = 'thb'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pharika''s Spawn'),
    (select id from sets where short_name = 'thb'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Atris, Oracle of Half-Truths'),
    (select id from sets where short_name = 'thb'),
    '333',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nyxbloom Ancient'),
    (select id from sets where short_name = 'thb'),
    '190',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Return to Nature'),
    (select id from sets where short_name = 'thb'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serpent of Yawning Depths'),
    (select id from sets where short_name = 'thb'),
    '291',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moss Viper'),
    (select id from sets where short_name = 'thb'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erebos''s Intervention'),
    (select id from sets where short_name = 'thb'),
    '313',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Omen of the Sea'),
    (select id from sets where short_name = 'thb'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inevitable End'),
    (select id from sets where short_name = 'thb'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kunoros, Hound of Athreos'),
    (select id from sets where short_name = 'thb'),
    '341',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aphemia, the Cacophony'),
    (select id from sets where short_name = 'thb'),
    '311',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plummet'),
    (select id from sets where short_name = 'thb'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Discordant Piper'),
    (select id from sets where short_name = 'thb'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Taranika, Akroan Veteran'),
    (select id from sets where short_name = 'thb'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Transcendent Envoy'),
    (select id from sets where short_name = 'thb'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'thb'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Memory Drain'),
    (select id from sets where short_name = 'thb'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nessian Boar'),
    (select id from sets where short_name = 'thb'),
    '181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Iroas''s Blessing'),
    (select id from sets where short_name = 'thb'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'thb'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple of Enlightenment'),
    (select id from sets where short_name = 'thb'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Purphoros, Bronze-Blooded'),
    (select id from sets where short_name = 'thb'),
    '150',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Storm Herald'),
    (select id from sets where short_name = 'thb'),
    '321',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scavenging Harpy'),
    (select id from sets where short_name = 'thb'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Klothys''s Design'),
    (select id from sets where short_name = 'thb'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inspire Awe'),
    (select id from sets where short_name = 'thb'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple of Abandon'),
    (select id from sets where short_name = 'thb'),
    '347★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrap in Flames'),
    (select id from sets where short_name = 'thb'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shatter the Sky'),
    (select id from sets where short_name = 'thb'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grim Physician'),
    (select id from sets where short_name = 'thb'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'thb'),
    '281',
    'common'
) ,
(
    (select id from mtgcard where name = 'Setessan Training'),
    (select id from sets where short_name = 'thb'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Triton Waverider'),
    (select id from sets where short_name = 'thb'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Portent of Betrayal'),
    (select id from sets where short_name = 'thb'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Destiny Spinner'),
    (select id from sets where short_name = 'thb'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Careless Celebrant'),
    (select id from sets where short_name = 'thb'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anax, Hardened in the Forge'),
    (select id from sets where short_name = 'thb'),
    '264',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hyrax Tower Scout'),
    (select id from sets where short_name = 'thb'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Uro, Titan of Nature''s Wrath'),
    (select id from sets where short_name = 'thb'),
    '343',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nylea, Keen-Eyed'),
    (select id from sets where short_name = 'thb'),
    '185',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Elspeth, Undaunted Hero'),
    (select id from sets where short_name = 'thb'),
    '270',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ashiok, Nightmare Muse'),
    (select id from sets where short_name = 'thb'),
    '256',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Glimpse of Freedom'),
    (select id from sets where short_name = 'thb'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eutropia the Twice-Favored'),
    (select id from sets where short_name = 'thb'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stampede Rider'),
    (select id from sets where short_name = 'thb'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lagonna-Band Storyteller'),
    (select id from sets where short_name = 'thb'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'thb'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Allure of the Unknown'),
    (select id from sets where short_name = 'thb'),
    '332',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Callaphe, Beloved of the Sea'),
    (select id from sets where short_name = 'thb'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bronzehide Lion'),
    (select id from sets where short_name = 'thb'),
    '334',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Malice'),
    (select id from sets where short_name = 'thb'),
    '350',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thassa, Deep-Dwelling'),
    (select id from sets where short_name = 'thb'),
    '71',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nyxbloom Ancient'),
    (select id from sets where short_name = 'thb'),
    '330',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Heliod''s Pilgrim'),
    (select id from sets where short_name = 'thb'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riptide Turtle'),
    (select id from sets where short_name = 'thb'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thundering Chariot'),
    (select id from sets where short_name = 'thb'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hero of the Games'),
    (select id from sets where short_name = 'thb'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minion''s Return'),
    (select id from sets where short_name = 'thb'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kroxa, Titan of Death''s Hunger'),
    (select id from sets where short_name = 'thb'),
    '221',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Deathbellow War Cry'),
    (select id from sets where short_name = 'thb'),
    '294',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rise to Glory'),
    (select id from sets where short_name = 'thb'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wolfwillow Haven'),
    (select id from sets where short_name = 'thb'),
    '357',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Archon of Falling Stars'),
    (select id from sets where short_name = 'thb'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'thb'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eidolon of Philosophy'),
    (select id from sets where short_name = 'thb'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashiok''s Erasure'),
    (select id from sets where short_name = 'thb'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphinx Mindbreaker'),
    (select id from sets where short_name = 'thb'),
    '290',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gray Merchant of Asphodel'),
    (select id from sets where short_name = 'thb'),
    '355',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Purphoros''s Intervention'),
    (select id from sets where short_name = 'thb'),
    '320',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Birth of Meletis'),
    (select id from sets where short_name = 'thb'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Underworld Sentinel'),
    (select id from sets where short_name = 'thb'),
    '293',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wavebreak Hippocamp'),
    (select id from sets where short_name = 'thb'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nylea''s Intervention'),
    (select id from sets where short_name = 'thb'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'thb'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Atris, Oracle of Half-Truths'),
    (select id from sets where short_name = 'thb'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kunoros, Hound of Athreos'),
    (select id from sets where short_name = 'thb'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'thb'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naiad of Hidden Coves'),
    (select id from sets where short_name = 'thb'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enigmatic Incarnation'),
    (select id from sets where short_name = 'thb'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mogis''s Favor'),
    (select id from sets where short_name = 'thb'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heroes of the Revel'),
    (select id from sets where short_name = 'thb'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Storm Herald'),
    (select id from sets where short_name = 'thb'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arena Trickster'),
    (select id from sets where short_name = 'thb'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heliod, Sun-Crowned'),
    (select id from sets where short_name = 'thb'),
    '259',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Archon of Sun''s Grace'),
    (select id from sets where short_name = 'thb'),
    '298',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leonin of the Lost Pride'),
    (select id from sets where short_name = 'thb'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Altar of the Pantheon'),
    (select id from sets where short_name = 'thb'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreadful Apathy'),
    (select id from sets where short_name = 'thb'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erebos, Bleak-Hearted'),
    (select id from sets where short_name = 'thb'),
    '93',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Temple of Deceit'),
    (select id from sets where short_name = 'thb'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wings of Hubris'),
    (select id from sets where short_name = 'thb'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ichthyomorphosis'),
    (select id from sets where short_name = 'thb'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'thb'),
    '283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirror Shield'),
    (select id from sets where short_name = 'thb'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Setessan Champion'),
    (select id from sets where short_name = 'thb'),
    '331',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lampad of Death''s Vigil'),
    (select id from sets where short_name = 'thb'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dream Trawler'),
    (select id from sets where short_name = 'thb'),
    '336',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elspeth Conquers Death'),
    (select id from sets where short_name = 'thb'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Incendiary Oracle'),
    (select id from sets where short_name = 'thb'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swimmer in Nightmares'),
    (select id from sets where short_name = 'thb'),
    '275',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shoal Kraken'),
    (select id from sets where short_name = 'thb'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Purphoros''s Intervention'),
    (select id from sets where short_name = 'thb'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreamstalker Manticore'),
    (select id from sets where short_name = 'thb'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Taranika, Akroan Veteran'),
    (select id from sets where short_name = 'thb'),
    '303',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voracious Typhon'),
    (select id from sets where short_name = 'thb'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Idyllic Tutor'),
    (select id from sets where short_name = 'thb'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Favored of Iroas'),
    (select id from sets where short_name = 'thb'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nadir Kraken'),
    (select id from sets where short_name = 'thb'),
    '305',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Final Flare'),
    (select id from sets where short_name = 'thb'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Underworld Charger'),
    (select id from sets where short_name = 'thb'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shimmerwing Chimera'),
    (select id from sets where short_name = 'thb'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nyx Lotus'),
    (select id from sets where short_name = 'thb'),
    '344',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Daxos, Blessed by the Sun'),
    (select id from sets where short_name = 'thb'),
    '258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aspect of Manticore'),
    (select id from sets where short_name = 'thb'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreamshaper Shaman'),
    (select id from sets where short_name = 'thb'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gallia of the Endless Dance'),
    (select id from sets where short_name = 'thb'),
    '338',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Allure of the Unknown'),
    (select id from sets where short_name = 'thb'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dawn Evangel'),
    (select id from sets where short_name = 'thb'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mischievous Chimera'),
    (select id from sets where short_name = 'thb'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Setessan Champion'),
    (select id from sets where short_name = 'thb'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Daybreak Chimera'),
    (select id from sets where short_name = 'thb'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chain to Memory'),
    (select id from sets where short_name = 'thb'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thirst for Meaning'),
    (select id from sets where short_name = 'thb'),
    '354',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple Thief'),
    (select id from sets where short_name = 'thb'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nyxborn Marauder'),
    (select id from sets where short_name = 'thb'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elspeth, Sun''s Nemesis'),
    (select id from sets where short_name = 'thb'),
    '255',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Labyrinth of Skophos'),
    (select id from sets where short_name = 'thb'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warden of the Chained'),
    (select id from sets where short_name = 'thb'),
    '230',
    'uncommon'
) 
 on conflict do nothing;
