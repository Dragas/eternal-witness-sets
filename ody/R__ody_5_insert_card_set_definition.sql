insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Afflict'),
    (select id from sets where short_name = 'ody'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infected Vermin'),
    (select id from sets where short_name = 'ody'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Refresh'),
    (select id from sets where short_name = 'ody'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ody'),
    '332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ody'),
    '345',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dematerialize'),
    (select id from sets where short_name = 'ody'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Springing Tiger'),
    (select id from sets where short_name = 'ody'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shadowblood Egg'),
    (select id from sets where short_name = 'ody'),
    '308',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Predict'),
    (select id from sets where short_name = 'ody'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Millikin'),
    (select id from sets where short_name = 'ody'),
    '302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scorching Missile'),
    (select id from sets where short_name = 'ody'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pianna, Nomad Captain'),
    (select id from sets where short_name = 'ody'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aven Cloudchaser'),
    (select id from sets where short_name = 'ody'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Auramancer'),
    (select id from sets where short_name = 'ody'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Charge'),
    (select id from sets where short_name = 'ody'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rotting Giant'),
    (select id from sets where short_name = 'ody'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krosan Avenger'),
    (select id from sets where short_name = 'ody'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Last Rites'),
    (select id from sets where short_name = 'ody'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cabal Patriarch'),
    (select id from sets where short_name = 'ody'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frenetic Ogre'),
    (select id from sets where short_name = 'ody'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crystal Quarry'),
    (select id from sets where short_name = 'ody'),
    '318',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Patron Wizard'),
    (select id from sets where short_name = 'ody'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Childhood Horror'),
    (select id from sets where short_name = 'ody'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Still Life'),
    (select id from sets where short_name = 'ody'),
    '275',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aboshan''s Desire'),
    (select id from sets where short_name = 'ody'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cephalid Shrine'),
    (select id from sets where short_name = 'ody'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deep Reconnaissance'),
    (select id from sets where short_name = 'ody'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soulcatcher'),
    (select id from sets where short_name = 'ody'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Delaying Shield'),
    (select id from sets where short_name = 'ody'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cease-Fire'),
    (select id from sets where short_name = 'ody'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Engulfing Flames'),
    (select id from sets where short_name = 'ody'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stalking Bloodsucker'),
    (select id from sets where short_name = 'ody'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pardic Firecat'),
    (select id from sets where short_name = 'ody'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Face of Fear'),
    (select id from sets where short_name = 'ody'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barbarian Ring'),
    (select id from sets where short_name = 'ody'),
    '313',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Squirrel Mob'),
    (select id from sets where short_name = 'ody'),
    '273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bog Wreckage'),
    (select id from sets where short_name = 'ody'),
    '314',
    'common'
) ,
(
    (select id from mtgcard where name = 'Epicenter'),
    (select id from sets where short_name = 'ody'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cephalid Looter'),
    (select id from sets where short_name = 'ody'),
    '72†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Zealot'),
    (select id from sets where short_name = 'ody'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rites of Spring'),
    (select id from sets where short_name = 'ody'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tireless Tribe'),
    (select id from sets where short_name = 'ody'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wayward Angel'),
    (select id from sets where short_name = 'ody'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mossfire Egg'),
    (select id from sets where short_name = 'ody'),
    '304',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magnivore'),
    (select id from sets where short_name = 'ody'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Holistic Wisdom'),
    (select id from sets where short_name = 'ody'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Muscle Burst'),
    (select id from sets where short_name = 'ody'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deluge'),
    (select id from sets where short_name = 'ody'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Squirrel Nest'),
    (select id from sets where short_name = 'ody'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sandstone Deadfall'),
    (select id from sets where short_name = 'ody'),
    '307',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ember Beast'),
    (select id from sets where short_name = 'ody'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hallowed Healer'),
    (select id from sets where short_name = 'ody'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darkwater Catacombs'),
    (select id from sets where short_name = 'ody'),
    '319',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ody'),
    '339',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Crusader'),
    (select id from sets where short_name = 'ody'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aven Flock'),
    (select id from sets where short_name = 'ody'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreamwinder'),
    (select id from sets where short_name = 'ody'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Repentant Vampire'),
    (select id from sets where short_name = 'ody'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demoralize'),
    (select id from sets where short_name = 'ody'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tarnished Citadel'),
    (select id from sets where short_name = 'ody'),
    '329',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Patriarch''s Desire'),
    (select id from sets where short_name = 'ody'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ody'),
    '349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bearscape'),
    (select id from sets where short_name = 'ody'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Execute'),
    (select id from sets where short_name = 'ody'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woodland Druid'),
    (select id from sets where short_name = 'ody'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Malevolent Awakening'),
    (select id from sets where short_name = 'ody'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Embolden'),
    (select id from sets where short_name = 'ody'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Caustic Tar'),
    (select id from sets where short_name = 'ody'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ody'),
    '343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirari'),
    (select id from sets where short_name = 'ody'),
    '303',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aether Burst'),
    (select id from sets where short_name = 'ody'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sacred Rites'),
    (select id from sets where short_name = 'ody'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whipkeeper'),
    (select id from sets where short_name = 'ody'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Standstill'),
    (select id from sets where short_name = 'ody'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancestral Tribute'),
    (select id from sets where short_name = 'ody'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nantuko Disciple'),
    (select id from sets where short_name = 'ody'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Painbringer'),
    (select id from sets where short_name = 'ody'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moment''s Peace'),
    (select id from sets where short_name = 'ody'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Call of the Herd'),
    (select id from sets where short_name = 'ody'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Recoup'),
    (select id from sets where short_name = 'ody'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gorilla Titan'),
    (select id from sets where short_name = 'ody'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bamboozle'),
    (select id from sets where short_name = 'ody'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pardic Swordsmith'),
    (select id from sets where short_name = 'ody'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Concentrate'),
    (select id from sets where short_name = 'ody'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Morbid Hunger'),
    (select id from sets where short_name = 'ody'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nantuko Mentor'),
    (select id from sets where short_name = 'ody'),
    '255',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystic Visionary'),
    (select id from sets where short_name = 'ody'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abandoned Outpost'),
    (select id from sets where short_name = 'ody'),
    '312',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mine Layer'),
    (select id from sets where short_name = 'ody'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Atogatog'),
    (select id from sets where short_name = 'ody'),
    '286',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pardic Miner'),
    (select id from sets where short_name = 'ody'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nantuko Shrine'),
    (select id from sets where short_name = 'ody'),
    '256',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphere of Duty'),
    (select id from sets where short_name = 'ody'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Molten Influence'),
    (select id from sets where short_name = 'ody'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Traumatize'),
    (select id from sets where short_name = 'ody'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aven Archer'),
    (select id from sets where short_name = 'ody'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Braids, Cabal Minion'),
    (select id from sets where short_name = 'ody'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thought Eater'),
    (select id from sets where short_name = 'ody'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampiric Dragon'),
    (select id from sets where short_name = 'ody'),
    '296',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volley of Boulders'),
    (select id from sets where short_name = 'ody'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steamclaw'),
    (select id from sets where short_name = 'ody'),
    '310',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chamber of Manipulation'),
    (select id from sets where short_name = 'ody'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zombify'),
    (select id from sets where short_name = 'ody'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ody'),
    '333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ody'),
    '335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balshan Griffin'),
    (select id from sets where short_name = 'ody'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savage Firecat'),
    (select id from sets where short_name = 'ody'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shifty Doppelganger'),
    (select id from sets where short_name = 'ody'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coffin Purge'),
    (select id from sets where short_name = 'ody'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anarchist'),
    (select id from sets where short_name = 'ody'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minotaur Explorer'),
    (select id from sets where short_name = 'ody'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blessed Orator'),
    (select id from sets where short_name = 'ody'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Might'),
    (select id from sets where short_name = 'ody'),
    '277',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chance Encounter'),
    (select id from sets where short_name = 'ody'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Battle of Wits'),
    (select id from sets where short_name = 'ody'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aven Smokeweaver'),
    (select id from sets where short_name = 'ody'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ody'),
    '341',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Fisher'),
    (select id from sets where short_name = 'ody'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Mongrel'),
    (select id from sets where short_name = 'ody'),
    '283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nantuko Elder'),
    (select id from sets where short_name = 'ody'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balshan Beguiler'),
    (select id from sets where short_name = 'ody'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gravestorm'),
    (select id from sets where short_name = 'ody'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Resilient Wanderer'),
    (select id from sets where short_name = 'ody'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whispering Shade'),
    (select id from sets where short_name = 'ody'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Cannibal'),
    (select id from sets where short_name = 'ody'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treetop Sentinel'),
    (select id from sets where short_name = 'ody'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scrivener'),
    (select id from sets where short_name = 'ody'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pilgrim of Justice'),
    (select id from sets where short_name = 'ody'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thermal Blast'),
    (select id from sets where short_name = 'ody'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Filthy Cur'),
    (select id from sets where short_name = 'ody'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seton, Krosan Protector'),
    (select id from sets where short_name = 'ody'),
    '267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Piper''s Melody'),
    (select id from sets where short_name = 'ody'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nomad Stadium'),
    (select id from sets where short_name = 'ody'),
    '322',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flame Burst'),
    (select id from sets where short_name = 'ody'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ody'),
    '350',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earnest Fellowship'),
    (select id from sets where short_name = 'ody'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Screams of the Damned'),
    (select id from sets where short_name = 'ody'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chainflinger'),
    (select id from sets where short_name = 'ody'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pedantic Learning'),
    (select id from sets where short_name = 'ody'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Psychatog'),
    (select id from sets where short_name = 'ody'),
    '292',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Repel'),
    (select id from sets where short_name = 'ody'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bash to Bits'),
    (select id from sets where short_name = 'ody'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantatog'),
    (select id from sets where short_name = 'ody'),
    '291',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shadowblood Ridge'),
    (select id from sets where short_name = 'ody'),
    '326',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magma Vein'),
    (select id from sets where short_name = 'ody'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dwarven Recruiter'),
    (select id from sets where short_name = 'ody'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beast Attack'),
    (select id from sets where short_name = 'ody'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mossfire Valley'),
    (select id from sets where short_name = 'ody'),
    '321',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barbarian Lunatic'),
    (select id from sets where short_name = 'ody'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Wall'),
    (select id from sets where short_name = 'ody'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cephalid Scout'),
    (select id from sets where short_name = 'ody'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Grunt'),
    (select id from sets where short_name = 'ody'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rites of Initiation'),
    (select id from sets where short_name = 'ody'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seafloor Debris'),
    (select id from sets where short_name = 'ody'),
    '325†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charmed Pendant'),
    (select id from sets where short_name = 'ody'),
    '298',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seton''s Desire'),
    (select id from sets where short_name = 'ody'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sungrass Egg'),
    (select id from sets where short_name = 'ody'),
    '311',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spellbane Centaur'),
    (select id from sets where short_name = 'ody'),
    '271',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thought Nibbler'),
    (select id from sets where short_name = 'ody'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ody'),
    '347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Otarian Juggernaut'),
    (select id from sets where short_name = 'ody'),
    '305',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sarcatog'),
    (select id from sets where short_name = 'ody'),
    '293',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sphere of Reason'),
    (select id from sets where short_name = 'ody'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cephalid Retainer'),
    (select id from sets where short_name = 'ody'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Price of Glory'),
    (select id from sets where short_name = 'ody'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Simplify'),
    (select id from sets where short_name = 'ody'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diligent Farmhand'),
    (select id from sets where short_name = 'ody'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mad Dog'),
    (select id from sets where short_name = 'ody'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sungrass Prairie'),
    (select id from sets where short_name = 'ody'),
    '328',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphere of Truth'),
    (select id from sets where short_name = 'ody'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balancing Act'),
    (select id from sets where short_name = 'ody'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Buried Alive'),
    (select id from sets where short_name = 'ody'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Persuasion'),
    (select id from sets where short_name = 'ody'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cognivore'),
    (select id from sets where short_name = 'ody'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tremble'),
    (select id from sets where short_name = 'ody'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ody'),
    '334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kirtar''s Desire'),
    (select id from sets where short_name = 'ody'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Escape Artist'),
    (select id from sets where short_name = 'ody'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Life Burst'),
    (select id from sets where short_name = 'ody'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Puppeteer'),
    (select id from sets where short_name = 'ody'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cartographer'),
    (select id from sets where short_name = 'ody'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aboshan, Cephalid Emperor'),
    (select id from sets where short_name = 'ody'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ody'),
    '342',
    'common'
) ,
(
    (select id from mtgcard where name = 'Need for Speed'),
    (select id from sets where short_name = 'ody'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chatter of the Squirrel'),
    (select id from sets where short_name = 'ody'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cabal Pit'),
    (select id from sets where short_name = 'ody'),
    '315',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dwarven Shrine'),
    (select id from sets where short_name = 'ody'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Psionic Gift'),
    (select id from sets where short_name = 'ody'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obstinate Familiar'),
    (select id from sets where short_name = 'ody'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mindslicer'),
    (select id from sets where short_name = 'ody'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tattoo Ward'),
    (select id from sets where short_name = 'ody'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unifying Theory'),
    (select id from sets where short_name = 'ody'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liquid Fire'),
    (select id from sets where short_name = 'ody'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overeager Apprentice'),
    (select id from sets where short_name = 'ody'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Think Tank'),
    (select id from sets where short_name = 'ody'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Devoted Caretaker'),
    (select id from sets where short_name = 'ody'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Amugaba'),
    (select id from sets where short_name = 'ody'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cursed Monstrosity'),
    (select id from sets where short_name = 'ody'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bomb Squad'),
    (select id from sets where short_name = 'ody'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Divert'),
    (select id from sets where short_name = 'ody'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ody'),
    '340',
    'common'
) ,
(
    (select id from mtgcard where name = 'Acceptable Losses'),
    (select id from sets where short_name = 'ody'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Twigwalker'),
    (select id from sets where short_name = 'ody'),
    '279',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Decimate'),
    (select id from sets where short_name = 'ody'),
    '287',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demolish'),
    (select id from sets where short_name = 'ody'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Decompose'),
    (select id from sets where short_name = 'ody'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantom Whelp'),
    (select id from sets where short_name = 'ody'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shadowmage Infiltrator'),
    (select id from sets where short_name = 'ody'),
    '294',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Morgue Theft'),
    (select id from sets where short_name = 'ody'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone-Tongue Basilisk'),
    (select id from sets where short_name = 'ody'),
    '276',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ody'),
    '348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skull Fracture'),
    (select id from sets where short_name = 'ody'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Graceful Antelope'),
    (select id from sets where short_name = 'ody'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aegis of Honor'),
    (select id from sets where short_name = 'ody'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Werebear'),
    (select id from sets where short_name = 'ody'),
    '282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overrun'),
    (select id from sets where short_name = 'ody'),
    '260',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ground Seal'),
    (select id from sets where short_name = 'ody'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dirty Wererat'),
    (select id from sets where short_name = 'ody'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Spray'),
    (select id from sets where short_name = 'ody'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Extract'),
    (select id from sets where short_name = 'ody'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Confessor'),
    (select id from sets where short_name = 'ody'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Shrine'),
    (select id from sets where short_name = 'ody'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sadistic Hypnotist'),
    (select id from sets where short_name = 'ody'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seize the Day'),
    (select id from sets where short_name = 'ody'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Howling Gale'),
    (select id from sets where short_name = 'ody'),
    '244',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mudhole'),
    (select id from sets where short_name = 'ody'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ray of Distortion'),
    (select id from sets where short_name = 'ody'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nimble Mongoose'),
    (select id from sets where short_name = 'ody'),
    '258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burning Sands'),
    (select id from sets where short_name = 'ody'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ody'),
    '346',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shelter'),
    (select id from sets where short_name = 'ody'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Upheaval'),
    (select id from sets where short_name = 'ody'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seafloor Debris'),
    (select id from sets where short_name = 'ody'),
    '325',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Windreader'),
    (select id from sets where short_name = 'ody'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nomad Decoy'),
    (select id from sets where short_name = 'ody'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dwarven Strike Force'),
    (select id from sets where short_name = 'ody'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Touch of Invisibility'),
    (select id from sets where short_name = 'ody'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Luminous Guardian'),
    (select id from sets where short_name = 'ody'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thaumatog'),
    (select id from sets where short_name = 'ody'),
    '295',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Catalyst Stone'),
    (select id from sets where short_name = 'ody'),
    '297',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Haunting Echoes'),
    (select id from sets where short_name = 'ody'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leaf Dancer'),
    (select id from sets where short_name = 'ody'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krosan Beast'),
    (select id from sets where short_name = 'ody'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Second Thoughts'),
    (select id from sets where short_name = 'ody'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cephalid Broker'),
    (select id from sets where short_name = 'ody'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shower of Coals'),
    (select id from sets where short_name = 'ody'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elephant Ambush'),
    (select id from sets where short_name = 'ody'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dogged Hunter'),
    (select id from sets where short_name = 'ody'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Limestone Golem'),
    (select id from sets where short_name = 'ody'),
    '301',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cantivore'),
    (select id from sets where short_name = 'ody'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kirtar''s Wrath'),
    (select id from sets where short_name = 'ody'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Halberdier'),
    (select id from sets where short_name = 'ody'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Syncopate'),
    (select id from sets where short_name = 'ody'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ody'),
    '337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thought Devourer'),
    (select id from sets where short_name = 'ody'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tombfire'),
    (select id from sets where short_name = 'ody'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ravaged Highlands'),
    (select id from sets where short_name = 'ody'),
    '324',
    'common'
) ,
(
    (select id from mtgcard where name = 'Verdant Succession'),
    (select id from sets where short_name = 'ody'),
    '280',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pilgrim of Virtue'),
    (select id from sets where short_name = 'ody'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zoologist'),
    (select id from sets where short_name = 'ody'),
    '285',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ody'),
    '338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Words of Wisdom'),
    (select id from sets where short_name = 'ody'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cabal Inquisitor'),
    (select id from sets where short_name = 'ody'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Burst'),
    (select id from sets where short_name = 'ody'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karmic Justice'),
    (select id from sets where short_name = 'ody'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terravore'),
    (select id from sets where short_name = 'ody'),
    '278',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zombie Assassin'),
    (select id from sets where short_name = 'ody'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chlorophant'),
    (select id from sets where short_name = 'ody'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tainted Pact'),
    (select id from sets where short_name = 'ody'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lithatog'),
    (select id from sets where short_name = 'ody'),
    '289',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aura Graft'),
    (select id from sets where short_name = 'ody'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skycloud Expanse'),
    (select id from sets where short_name = 'ody'),
    '327',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'ody'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Junk Golem'),
    (select id from sets where short_name = 'ody'),
    '300',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kamahl''s Desire'),
    (select id from sets where short_name = 'ody'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Centaur Garden'),
    (select id from sets where short_name = 'ody'),
    '316',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crypt Creeper'),
    (select id from sets where short_name = 'ody'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cephalid Looter'),
    (select id from sets where short_name = 'ody'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Infestation'),
    (select id from sets where short_name = 'ody'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lava Blister'),
    (select id from sets where short_name = 'ody'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Patchwork Gnomes'),
    (select id from sets where short_name = 'ody'),
    '306',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spark Mage'),
    (select id from sets where short_name = 'ody'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fledgling Imp'),
    (select id from sets where short_name = 'ody'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dedicated Martyr'),
    (select id from sets where short_name = 'ody'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deserted Temple'),
    (select id from sets where short_name = 'ody'),
    '320',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Innocent Blood'),
    (select id from sets where short_name = 'ody'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pulsating Illusion'),
    (select id from sets where short_name = 'ody'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steam Vines'),
    (select id from sets where short_name = 'ody'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Traveling Plague'),
    (select id from sets where short_name = 'ody'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Time Stretch'),
    (select id from sets where short_name = 'ody'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cultural Exchange'),
    (select id from sets where short_name = 'ody'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphere of Grace'),
    (select id from sets where short_name = 'ody'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cephalid Coliseum'),
    (select id from sets where short_name = 'ody'),
    '317',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firebolt'),
    (select id from sets where short_name = 'ody'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hint of Insanity'),
    (select id from sets where short_name = 'ody'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frightcrawler'),
    (select id from sets where short_name = 'ody'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gallantry'),
    (select id from sets where short_name = 'ody'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nefarious Lich'),
    (select id from sets where short_name = 'ody'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Impulsive Maneuvers'),
    (select id from sets where short_name = 'ody'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghastly Demise'),
    (select id from sets where short_name = 'ody'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Laquatus''s Creativity'),
    (select id from sets where short_name = 'ody'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodcurdler'),
    (select id from sets where short_name = 'ody'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Timberland Ruins'),
    (select id from sets where short_name = 'ody'),
    '330',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cabal Shrine'),
    (select id from sets where short_name = 'ody'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roar of the Wurm'),
    (select id from sets where short_name = 'ody'),
    '266',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Careful Study'),
    (select id from sets where short_name = 'ody'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beloved Chaplain'),
    (select id from sets where short_name = 'ody'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krosan Archer'),
    (select id from sets where short_name = 'ody'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ody'),
    '344',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crashing Centaur'),
    (select id from sets where short_name = 'ody'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Patrol Hound'),
    (select id from sets where short_name = 'ody'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Druid''s Call'),
    (select id from sets where short_name = 'ody'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Earth Rift'),
    (select id from sets where short_name = 'ody'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Animal Boneyard'),
    (select id from sets where short_name = 'ody'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Famished Ghoul'),
    (select id from sets where short_name = 'ody'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lieutenant Kirtar'),
    (select id from sets where short_name = 'ody'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skycloud Egg'),
    (select id from sets where short_name = 'ody'),
    '309',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Druid Lyrist'),
    (select id from sets where short_name = 'ody'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Petrified Field'),
    (select id from sets where short_name = 'ody'),
    '323',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystic Enforcer'),
    (select id from sets where short_name = 'ody'),
    '290',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashen Firebeast'),
    (select id from sets where short_name = 'ody'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fervent Denial'),
    (select id from sets where short_name = 'ody'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battle Strain'),
    (select id from sets where short_name = 'ody'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skeletal Scrying'),
    (select id from sets where short_name = 'ody'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Immobilizing Ink'),
    (select id from sets where short_name = 'ody'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kamahl, Pit Fighter'),
    (select id from sets where short_name = 'ody'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ody'),
    '331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ody'),
    '336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Decaying Soil'),
    (select id from sets where short_name = 'ody'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphere of Law'),
    (select id from sets where short_name = 'ody'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Testament of Faith'),
    (select id from sets where short_name = 'ody'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Iridescent Angel'),
    (select id from sets where short_name = 'ody'),
    '288',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Metamorphic Wurm'),
    (select id from sets where short_name = 'ody'),
    '250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nut Collector'),
    (select id from sets where short_name = 'ody'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Peek'),
    (select id from sets where short_name = 'ody'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyshooter'),
    (select id from sets where short_name = 'ody'),
    '270',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Primal Frenzy'),
    (select id from sets where short_name = 'ody'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Entomb'),
    (select id from sets where short_name = 'ody'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spiritualize'),
    (select id from sets where short_name = 'ody'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dusk Imp'),
    (select id from sets where short_name = 'ody'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divine Sacrament'),
    (select id from sets where short_name = 'ody'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diabolic Tutor'),
    (select id from sets where short_name = 'ody'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blazing Salvo'),
    (select id from sets where short_name = 'ody'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivify'),
    (select id from sets where short_name = 'ody'),
    '281',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Master Apothecary'),
    (select id from sets where short_name = 'ody'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rites of Refusal'),
    (select id from sets where short_name = 'ody'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Penitent'),
    (select id from sets where short_name = 'ody'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darkwater Egg'),
    (select id from sets where short_name = 'ody'),
    '299',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ivy Elemental'),
    (select id from sets where short_name = 'ody'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'New Frontiers'),
    (select id from sets where short_name = 'ody'),
    '257',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mortivore'),
    (select id from sets where short_name = 'ody'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rabid Elephant'),
    (select id from sets where short_name = 'ody'),
    '263',
    'common'
) 
 on conflict do nothing;
