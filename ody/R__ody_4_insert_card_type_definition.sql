insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Afflict'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Infected Vermin'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Infected Vermin'),
        (select types.id from types where name = 'Rat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Refresh'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Plains')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Mountain')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dematerialize'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Springing Tiger'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Springing Tiger'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shadowblood Egg'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Predict'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Millikin'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Millikin'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Millikin'),
        (select types.id from types where name = 'Construct')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Scorching Missile'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pianna, Nomad Captain'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pianna, Nomad Captain'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pianna, Nomad Captain'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pianna, Nomad Captain'),
        (select types.id from types where name = 'Nomad')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aven Cloudchaser'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aven Cloudchaser'),
        (select types.id from types where name = 'Bird')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aven Cloudchaser'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Auramancer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Auramancer'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Auramancer'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Reckless Charge'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rotting Giant'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rotting Giant'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rotting Giant'),
        (select types.id from types where name = 'Giant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Krosan Avenger'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Krosan Avenger'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Krosan Avenger'),
        (select types.id from types where name = 'Druid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Last Rites'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cabal Patriarch'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cabal Patriarch'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cabal Patriarch'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cabal Patriarch'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Frenetic Ogre'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Frenetic Ogre'),
        (select types.id from types where name = 'Ogre')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crystal Quarry'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Patron Wizard'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Patron Wizard'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Patron Wizard'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Childhood Horror'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Childhood Horror'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Still Life'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aboshan''s Desire'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aboshan''s Desire'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cephalid Shrine'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Deep Reconnaissance'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soulcatcher'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soulcatcher'),
        (select types.id from types where name = 'Bird')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soulcatcher'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Delaying Shield'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cease-Fire'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Engulfing Flames'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stalking Bloodsucker'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stalking Bloodsucker'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pardic Firecat'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pardic Firecat'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pardic Firecat'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Face of Fear'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Face of Fear'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Barbarian Ring'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Squirrel Mob'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Squirrel Mob'),
        (select types.id from types where name = 'Squirrel')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bog Wreckage'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Epicenter'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cephalid Looter'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cephalid Looter'),
        (select types.id from types where name = 'Cephalid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cephalid Looter'),
        (select types.id from types where name = 'Rogue')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystic Zealot'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystic Zealot'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystic Zealot'),
        (select types.id from types where name = 'Nomad')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystic Zealot'),
        (select types.id from types where name = 'Mystic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rites of Spring'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tireless Tribe'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tireless Tribe'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tireless Tribe'),
        (select types.id from types where name = 'Nomad')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wayward Angel'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wayward Angel'),
        (select types.id from types where name = 'Angel')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wayward Angel'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mossfire Egg'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Magnivore'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Magnivore'),
        (select types.id from types where name = 'Lhurgoyf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Holistic Wisdom'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Muscle Burst'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Deluge'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Squirrel Nest'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Squirrel Nest'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sandstone Deadfall'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ember Beast'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ember Beast'),
        (select types.id from types where name = 'Beast')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hallowed Healer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hallowed Healer'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hallowed Healer'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Darkwater Catacombs'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Swamp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystic Crusader'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystic Crusader'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystic Crusader'),
        (select types.id from types where name = 'Nomad')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystic Crusader'),
        (select types.id from types where name = 'Mystic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aven Flock'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aven Flock'),
        (select types.id from types where name = 'Bird')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aven Flock'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dreamwinder'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dreamwinder'),
        (select types.id from types where name = 'Serpent')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Repentant Vampire'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Repentant Vampire'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Demoralize'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tarnished Citadel'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Patriarch''s Desire'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Patriarch''s Desire'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Forest')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bearscape'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Execute'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Woodland Druid'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Woodland Druid'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Woodland Druid'),
        (select types.id from types where name = 'Druid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Malevolent Awakening'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Embolden'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Caustic Tar'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Caustic Tar'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Mountain')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mirari'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mirari'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aether Burst'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sacred Rites'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Whipkeeper'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Whipkeeper'),
        (select types.id from types where name = 'Dwarf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Standstill'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ancestral Tribute'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nantuko Disciple'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nantuko Disciple'),
        (select types.id from types where name = 'Insect')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nantuko Disciple'),
        (select types.id from types where name = 'Druid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Painbringer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Painbringer'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Painbringer'),
        (select types.id from types where name = 'Minion')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Moment''s Peace'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Call of the Herd'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Recoup'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gorilla Titan'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gorilla Titan'),
        (select types.id from types where name = 'Ape')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bamboozle'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pardic Swordsmith'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pardic Swordsmith'),
        (select types.id from types where name = 'Dwarf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Concentrate'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Morbid Hunger'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nantuko Mentor'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nantuko Mentor'),
        (select types.id from types where name = 'Insect')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nantuko Mentor'),
        (select types.id from types where name = 'Druid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystic Visionary'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystic Visionary'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystic Visionary'),
        (select types.id from types where name = 'Nomad')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystic Visionary'),
        (select types.id from types where name = 'Mystic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Abandoned Outpost'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mine Layer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mine Layer'),
        (select types.id from types where name = 'Dwarf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Atogatog'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Atogatog'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Atogatog'),
        (select types.id from types where name = 'Atog')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pardic Miner'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pardic Miner'),
        (select types.id from types where name = 'Dwarf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nantuko Shrine'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sphere of Duty'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Molten Influence'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Traumatize'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aven Archer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aven Archer'),
        (select types.id from types where name = 'Bird')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aven Archer'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aven Archer'),
        (select types.id from types where name = 'Archer')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Braids, Cabal Minion'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Braids, Cabal Minion'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Braids, Cabal Minion'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Braids, Cabal Minion'),
        (select types.id from types where name = 'Minion')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thought Eater'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thought Eater'),
        (select types.id from types where name = 'Beast')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vampiric Dragon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vampiric Dragon'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vampiric Dragon'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Volley of Boulders'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Steamclaw'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chamber of Manipulation'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chamber of Manipulation'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombify'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Plains')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Island')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Balshan Griffin'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Balshan Griffin'),
        (select types.id from types where name = 'Griffin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Savage Firecat'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Savage Firecat'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Savage Firecat'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shifty Doppelganger'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shifty Doppelganger'),
        (select types.id from types where name = 'Shapeshifter')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Coffin Purge'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Anarchist'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Anarchist'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Anarchist'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Minotaur Explorer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Minotaur Explorer'),
        (select types.id from types where name = 'Minotaur')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Minotaur Explorer'),
        (select types.id from types where name = 'Scout')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Blessed Orator'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Blessed Orator'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Blessed Orator'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sylvan Might'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chance Encounter'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Battle of Wits'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aven Smokeweaver'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aven Smokeweaver'),
        (select types.id from types where name = 'Bird')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aven Smokeweaver'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Swamp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aven Fisher'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aven Fisher'),
        (select types.id from types where name = 'Bird')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aven Fisher'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wild Mongrel'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wild Mongrel'),
        (select types.id from types where name = 'Dog')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nantuko Elder'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nantuko Elder'),
        (select types.id from types where name = 'Insect')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nantuko Elder'),
        (select types.id from types where name = 'Druid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Balshan Beguiler'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Balshan Beguiler'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Balshan Beguiler'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gravestorm'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Resilient Wanderer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Resilient Wanderer'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Resilient Wanderer'),
        (select types.id from types where name = 'Nomad')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Whispering Shade'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Whispering Shade'),
        (select types.id from types where name = 'Shade')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie Cannibal'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie Cannibal'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Treetop Sentinel'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Treetop Sentinel'),
        (select types.id from types where name = 'Bird')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Treetop Sentinel'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Scrivener'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Scrivener'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Scrivener'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pilgrim of Justice'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pilgrim of Justice'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pilgrim of Justice'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thermal Blast'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Filthy Cur'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Filthy Cur'),
        (select types.id from types where name = 'Dog')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Seton, Krosan Protector'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Seton, Krosan Protector'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Seton, Krosan Protector'),
        (select types.id from types where name = 'Centaur')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Seton, Krosan Protector'),
        (select types.id from types where name = 'Druid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Piper''s Melody'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nomad Stadium'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Flame Burst'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Forest')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Earnest Fellowship'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Screams of the Damned'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chainflinger'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chainflinger'),
        (select types.id from types where name = 'Beast')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pedantic Learning'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Psychatog'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Psychatog'),
        (select types.id from types where name = 'Atog')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Repel'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bash to Bits'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Phantatog'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Phantatog'),
        (select types.id from types where name = 'Atog')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shadowblood Ridge'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Magma Vein'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dwarven Recruiter'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dwarven Recruiter'),
        (select types.id from types where name = 'Dwarf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Beast Attack'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mossfire Valley'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Barbarian Lunatic'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Barbarian Lunatic'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Barbarian Lunatic'),
        (select types.id from types where name = 'Barbarian')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Angelic Wall'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Angelic Wall'),
        (select types.id from types where name = 'Wall')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cephalid Scout'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cephalid Scout'),
        (select types.id from types where name = 'Cephalid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cephalid Scout'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cephalid Scout'),
        (select types.id from types where name = 'Scout')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dwarven Grunt'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dwarven Grunt'),
        (select types.id from types where name = 'Dwarf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rites of Initiation'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Seafloor Debris'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Charmed Pendant'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Seton''s Desire'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Seton''s Desire'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sungrass Egg'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spellbane Centaur'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spellbane Centaur'),
        (select types.id from types where name = 'Centaur')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thought Nibbler'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thought Nibbler'),
        (select types.id from types where name = 'Beast')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Forest')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Otarian Juggernaut'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Otarian Juggernaut'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Otarian Juggernaut'),
        (select types.id from types where name = 'Juggernaut')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sarcatog'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sarcatog'),
        (select types.id from types where name = 'Atog')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sphere of Reason'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cephalid Retainer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cephalid Retainer'),
        (select types.id from types where name = 'Cephalid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Price of Glory'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Simplify'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Diligent Farmhand'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Diligent Farmhand'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Diligent Farmhand'),
        (select types.id from types where name = 'Druid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mad Dog'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mad Dog'),
        (select types.id from types where name = 'Dog')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sungrass Prairie'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sphere of Truth'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Balancing Act'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Buried Alive'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Persuasion'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Persuasion'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cognivore'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cognivore'),
        (select types.id from types where name = 'Lhurgoyf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tremble'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Plains')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kirtar''s Desire'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kirtar''s Desire'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Escape Artist'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Escape Artist'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Escape Artist'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Life Burst'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Puppeteer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Puppeteer'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Puppeteer'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cartographer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cartographer'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aboshan, Cephalid Emperor'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aboshan, Cephalid Emperor'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aboshan, Cephalid Emperor'),
        (select types.id from types where name = 'Cephalid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aboshan, Cephalid Emperor'),
        (select types.id from types where name = 'Noble')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Swamp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Need for Speed'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chatter of the Squirrel'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cabal Pit'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dwarven Shrine'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Psionic Gift'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Psionic Gift'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Obstinate Familiar'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Obstinate Familiar'),
        (select types.id from types where name = 'Lizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mindslicer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mindslicer'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tattoo Ward'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tattoo Ward'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Unifying Theory'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Liquid Fire'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Overeager Apprentice'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Overeager Apprentice'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Overeager Apprentice'),
        (select types.id from types where name = 'Minion')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Think Tank'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Devoted Caretaker'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Devoted Caretaker'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Devoted Caretaker'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Amugaba'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Amugaba'),
        (select types.id from types where name = 'Illusion')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cursed Monstrosity'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cursed Monstrosity'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bomb Squad'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bomb Squad'),
        (select types.id from types where name = 'Dwarf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Divert'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Swamp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Acceptable Losses'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Twigwalker'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Twigwalker'),
        (select types.id from types where name = 'Insect')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Decimate'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Demolish'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Decompose'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Phantom Whelp'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Phantom Whelp'),
        (select types.id from types where name = 'Illusion')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Phantom Whelp'),
        (select types.id from types where name = 'Dog')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shadowmage Infiltrator'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shadowmage Infiltrator'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shadowmage Infiltrator'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Morgue Theft'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stone-Tongue Basilisk'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stone-Tongue Basilisk'),
        (select types.id from types where name = 'Basilisk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Forest')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skull Fracture'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Graceful Antelope'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Graceful Antelope'),
        (select types.id from types where name = 'Antelope')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aegis of Honor'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Werebear'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Werebear'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Werebear'),
        (select types.id from types where name = 'Bear')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Werebear'),
        (select types.id from types where name = 'Druid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Overrun'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ground Seal'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dirty Wererat'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dirty Wererat'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dirty Wererat'),
        (select types.id from types where name = 'Rat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dirty Wererat'),
        (select types.id from types where name = 'Minion')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Volcanic Spray'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Extract'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Confessor'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Confessor'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Confessor'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aven Shrine'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sadistic Hypnotist'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sadistic Hypnotist'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sadistic Hypnotist'),
        (select types.id from types where name = 'Minion')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Seize the Day'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Howling Gale'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mudhole'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ray of Distortion'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nimble Mongoose'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nimble Mongoose'),
        (select types.id from types where name = 'Mongoose')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Burning Sands'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Mountain')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shelter'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Upheaval'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Seafloor Debris'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aven Windreader'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aven Windreader'),
        (select types.id from types where name = 'Bird')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aven Windreader'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aven Windreader'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nomad Decoy'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nomad Decoy'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nomad Decoy'),
        (select types.id from types where name = 'Nomad')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dwarven Strike Force'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dwarven Strike Force'),
        (select types.id from types where name = 'Dwarf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dwarven Strike Force'),
        (select types.id from types where name = 'Berserker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Touch of Invisibility'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Luminous Guardian'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Luminous Guardian'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Luminous Guardian'),
        (select types.id from types where name = 'Nomad')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thaumatog'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thaumatog'),
        (select types.id from types where name = 'Atog')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Catalyst Stone'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Haunting Echoes'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Leaf Dancer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Leaf Dancer'),
        (select types.id from types where name = 'Centaur')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Krosan Beast'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Krosan Beast'),
        (select types.id from types where name = 'Squirrel')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Krosan Beast'),
        (select types.id from types where name = 'Beast')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Second Thoughts'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cephalid Broker'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cephalid Broker'),
        (select types.id from types where name = 'Cephalid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shower of Coals'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elephant Ambush'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dogged Hunter'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dogged Hunter'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dogged Hunter'),
        (select types.id from types where name = 'Nomad')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Limestone Golem'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Limestone Golem'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Limestone Golem'),
        (select types.id from types where name = 'Golem')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cantivore'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cantivore'),
        (select types.id from types where name = 'Lhurgoyf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kirtar''s Wrath'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Halberdier'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Halberdier'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Halberdier'),
        (select types.id from types where name = 'Barbarian')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Syncopate'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Island')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thought Devourer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thought Devourer'),
        (select types.id from types where name = 'Beast')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tombfire'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ravaged Highlands'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Verdant Succession'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pilgrim of Virtue'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pilgrim of Virtue'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pilgrim of Virtue'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zoologist'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zoologist'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zoologist'),
        (select types.id from types where name = 'Druid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Island')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Words of Wisdom'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cabal Inquisitor'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cabal Inquisitor'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cabal Inquisitor'),
        (select types.id from types where name = 'Minion')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mind Burst'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Karmic Justice'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Terravore'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Terravore'),
        (select types.id from types where name = 'Lhurgoyf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie Assassin'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie Assassin'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie Assassin'),
        (select types.id from types where name = 'Assassin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chlorophant'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chlorophant'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tainted Pact'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lithatog'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lithatog'),
        (select types.id from types where name = 'Atog')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aura Graft'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skycloud Expanse'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gravedigger'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gravedigger'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Junk Golem'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Junk Golem'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Junk Golem'),
        (select types.id from types where name = 'Golem')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kamahl''s Desire'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kamahl''s Desire'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Centaur Garden'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crypt Creeper'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crypt Creeper'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cephalid Looter'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cephalid Looter'),
        (select types.id from types where name = 'Cephalid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cephalid Looter'),
        (select types.id from types where name = 'Rogue')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie Infestation'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lava Blister'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Patchwork Gnomes'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Patchwork Gnomes'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Patchwork Gnomes'),
        (select types.id from types where name = 'Gnome')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spark Mage'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spark Mage'),
        (select types.id from types where name = 'Dwarf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spark Mage'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fledgling Imp'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fledgling Imp'),
        (select types.id from types where name = 'Imp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dedicated Martyr'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dedicated Martyr'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dedicated Martyr'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Deserted Temple'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Innocent Blood'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pulsating Illusion'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pulsating Illusion'),
        (select types.id from types where name = 'Illusion')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Steam Vines'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Steam Vines'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Traveling Plague'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Traveling Plague'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Time Stretch'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cultural Exchange'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sphere of Grace'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cephalid Coliseum'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Firebolt'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hint of Insanity'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Frightcrawler'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Frightcrawler'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gallantry'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nefarious Lich'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Impulsive Maneuvers'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ghastly Demise'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Laquatus''s Creativity'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodcurdler'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodcurdler'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Timberland Ruins'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cabal Shrine'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Roar of the Wurm'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Careful Study'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Beloved Chaplain'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Beloved Chaplain'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Beloved Chaplain'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Krosan Archer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Krosan Archer'),
        (select types.id from types where name = 'Centaur')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Krosan Archer'),
        (select types.id from types where name = 'Archer')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Mountain')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crashing Centaur'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crashing Centaur'),
        (select types.id from types where name = 'Centaur')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Patrol Hound'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Patrol Hound'),
        (select types.id from types where name = 'Dog')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Druid''s Call'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Druid''s Call'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Earth Rift'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Animal Boneyard'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Animal Boneyard'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Famished Ghoul'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Famished Ghoul'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lieutenant Kirtar'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lieutenant Kirtar'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lieutenant Kirtar'),
        (select types.id from types where name = 'Bird')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lieutenant Kirtar'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skycloud Egg'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Druid Lyrist'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Druid Lyrist'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Druid Lyrist'),
        (select types.id from types where name = 'Druid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Petrified Field'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystic Enforcer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystic Enforcer'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystic Enforcer'),
        (select types.id from types where name = 'Nomad')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystic Enforcer'),
        (select types.id from types where name = 'Mystic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ashen Firebeast'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ashen Firebeast'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ashen Firebeast'),
        (select types.id from types where name = 'Beast')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fervent Denial'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Battle Strain'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skeletal Scrying'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Immobilizing Ink'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Immobilizing Ink'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kamahl, Pit Fighter'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kamahl, Pit Fighter'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kamahl, Pit Fighter'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kamahl, Pit Fighter'),
        (select types.id from types where name = 'Barbarian')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Plains')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Island')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Decaying Soil'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sphere of Law'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Testament of Faith'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Iridescent Angel'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Iridescent Angel'),
        (select types.id from types where name = 'Angel')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Metamorphic Wurm'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Metamorphic Wurm'),
        (select types.id from types where name = 'Elephant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Metamorphic Wurm'),
        (select types.id from types where name = 'Wurm')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nut Collector'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nut Collector'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nut Collector'),
        (select types.id from types where name = 'Druid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Peek'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skyshooter'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skyshooter'),
        (select types.id from types where name = 'Centaur')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skyshooter'),
        (select types.id from types where name = 'Archer')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Primal Frenzy'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Primal Frenzy'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Entomb'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spiritualize'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dusk Imp'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dusk Imp'),
        (select types.id from types where name = 'Imp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Divine Sacrament'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Diabolic Tutor'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Blazing Salvo'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vivify'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Master Apothecary'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Master Apothecary'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Master Apothecary'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rites of Refusal'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystic Penitent'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystic Penitent'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystic Penitent'),
        (select types.id from types where name = 'Nomad')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mystic Penitent'),
        (select types.id from types where name = 'Mystic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Darkwater Egg'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ivy Elemental'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ivy Elemental'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'New Frontiers'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mortivore'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mortivore'),
        (select types.id from types where name = 'Lhurgoyf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rabid Elephant'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rabid Elephant'),
        (select types.id from types where name = 'Elephant')
    ) 
 on conflict do nothing;
