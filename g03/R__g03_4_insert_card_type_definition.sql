insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Intuition'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Argothian Enchantress'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Argothian Enchantress'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Argothian Enchantress'),
        (select types.id from types where name = 'Druid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Living Death'),
        (select types.id from types where name = 'Sorcery')
    ) 
 on conflict do nothing;
