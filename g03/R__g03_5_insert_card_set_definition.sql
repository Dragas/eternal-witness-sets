insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Intuition'),
    (select id from sets where short_name = 'g03'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Argothian Enchantress'),
    (select id from sets where short_name = 'g03'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Living Death'),
    (select id from sets where short_name = 'g03'),
    '3',
    'rare'
) 
 on conflict do nothing;
