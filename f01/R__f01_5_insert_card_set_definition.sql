insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Impulse'),
    (select id from sets where short_name = 'f01'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fireblast'),
    (select id from sets where short_name = 'f01'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jackal Pup'),
    (select id from sets where short_name = 'f01'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quirion Ranger'),
    (select id from sets where short_name = 'f01'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ophidian'),
    (select id from sets where short_name = 'f01'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carnophage'),
    (select id from sets where short_name = 'f01'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'f01'),
    '6',
    'rare'
) 
 on conflict do nothing;
