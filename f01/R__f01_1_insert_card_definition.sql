    insert into mtgcard(name) values ('Impulse') on conflict do nothing;
    insert into mtgcard(name) values ('Fireblast') on conflict do nothing;
    insert into mtgcard(name) values ('Jackal Pup') on conflict do nothing;
    insert into mtgcard(name) values ('Quirion Ranger') on conflict do nothing;
    insert into mtgcard(name) values ('Ophidian') on conflict do nothing;
    insert into mtgcard(name) values ('Carnophage') on conflict do nothing;
    insert into mtgcard(name) values ('Swords to Plowshares') on conflict do nothing;
