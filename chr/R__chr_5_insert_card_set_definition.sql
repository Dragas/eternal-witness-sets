insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'The Wretched'),
    (select id from sets where short_name = 'chr'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abu Ja''far'),
    (select id from sets where short_name = 'chr'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Safe Haven'),
    (select id from sets where short_name = 'chr'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Indestructible Aura'),
    (select id from sets where short_name = 'chr'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas'),
    (select id from sets where short_name = 'chr'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voodoo Doll'),
    (select id from sets where short_name = 'chr'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Palladia-Mors'),
    (select id from sets where short_name = 'chr'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = 'chr'),
    '116d',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Shrine'),
    (select id from sets where short_name = 'chr'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Concordant Crossroads'),
    (select id from sets where short_name = 'chr'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Wonder'),
    (select id from sets where short_name = 'chr'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = 'chr'),
    '115b',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelic Voices'),
    (select id from sets where short_name = 'chr'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rabid Wombat'),
    (select id from sets where short_name = 'chr'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = 'chr'),
    '114c',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Xira Arien'),
    (select id from sets where short_name = 'chr'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = 'chr'),
    '115d',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Opposition'),
    (select id from sets where short_name = 'chr'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Fallen'),
    (select id from sets where short_name = 'chr'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = 'chr'),
    '114a',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Johan'),
    (select id from sets where short_name = 'chr'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Vapor'),
    (select id from sets where short_name = 'chr'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akron Legionnaire'),
    (select id from sets where short_name = 'chr'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feldon''s Cane'),
    (select id from sets where short_name = 'chr'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teleport'),
    (select id from sets where short_name = 'chr'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Triassic Egg'),
    (select id from sets where short_name = 'chr'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serpent Generator'),
    (select id from sets where short_name = 'chr'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Horn of Deafening'),
    (select id from sets where short_name = 'chr'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Land''s Edge'),
    (select id from sets where short_name = 'chr'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fallen Angel'),
    (select id from sets where short_name = 'chr'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dakkon Blackblade'),
    (select id from sets where short_name = 'chr'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Moon'),
    (select id from sets where short_name = 'chr'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Witch Hunter'),
    (select id from sets where short_name = 'chr'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stangg'),
    (select id from sets where short_name = 'chr'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = 'chr'),
    '116c',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arena of the Ancients'),
    (select id from sets where short_name = 'chr'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = 'chr'),
    '116a',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tormod''s Crypt'),
    (select id from sets where short_name = 'chr'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Active Volcano'),
    (select id from sets where short_name = 'chr'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sivitri Scarzam'),
    (select id from sets where short_name = 'chr'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boomerang'),
    (select id from sets where short_name = 'chr'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'chr'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gauntlets of Chaos'),
    (select id from sets where short_name = 'chr'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jalum Tome'),
    (select id from sets where short_name = 'chr'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'D''Avenant Archer'),
    (select id from sets where short_name = 'chr'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Drake'),
    (select id from sets where short_name = 'chr'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Shadows'),
    (select id from sets where short_name = 'chr'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cuombajj Witches'),
    (select id from sets where short_name = 'chr'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'War Elephant'),
    (select id from sets where short_name = 'chr'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Banshee'),
    (select id from sets where short_name = 'chr'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flash Flood'),
    (select id from sets where short_name = 'chr'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Undoing'),
    (select id from sets where short_name = 'chr'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nebuchadnezzar'),
    (select id from sets where short_name = 'chr'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Argothian Pixies'),
    (select id from sets where short_name = 'chr'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fountain of Youth'),
    (select id from sets where short_name = 'chr'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Book of Rass'),
    (select id from sets where short_name = 'chr'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood of the Martyr'),
    (select id from sets where short_name = 'chr'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Recall'),
    (select id from sets where short_name = 'chr'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghazbán Ogre'),
    (select id from sets where short_name = 'chr'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = 'chr'),
    '114b',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cocoon'),
    (select id from sets where short_name = 'chr'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scavenger Folk'),
    (select id from sets where short_name = 'chr'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ayesha Tanaka'),
    (select id from sets where short_name = 'chr'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barl''s Cage'),
    (select id from sets where short_name = 'chr'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emerald Dragonfly'),
    (select id from sets where short_name = 'chr'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divine Offering'),
    (select id from sets where short_name = 'chr'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Rats'),
    (select id from sets where short_name = 'chr'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Repentant Blacksmith'),
    (select id from sets where short_name = 'chr'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shimian Night Stalker'),
    (select id from sets where short_name = 'chr'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Runesword'),
    (select id from sets where short_name = 'chr'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keepers of the Faith'),
    (select id from sets where short_name = 'chr'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Takklemaggot'),
    (select id from sets where short_name = 'chr'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Axelrod Gunnarson'),
    (select id from sets where short_name = 'chr'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dance of Many'),
    (select id from sets where short_name = 'chr'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vaevictis Asmadi'),
    (select id from sets where short_name = 'chr'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marhault Elsdragon'),
    (select id from sets where short_name = 'chr'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Puppet Master'),
    (select id from sets where short_name = 'chr'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hasran Ogress'),
    (select id from sets where short_name = 'chr'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Artisans'),
    (select id from sets where short_name = 'chr'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ivory Guardians'),
    (select id from sets where short_name = 'chr'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shield Wall'),
    (select id from sets where short_name = 'chr'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Armor'),
    (select id from sets where short_name = 'chr'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Digging Team'),
    (select id from sets where short_name = 'chr'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dandân'),
    (select id from sets where short_name = 'chr'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azure Drake'),
    (select id from sets where short_name = 'chr'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sol''kanar the Swamp King'),
    (select id from sets where short_name = 'chr'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Transmogrant'),
    (select id from sets where short_name = 'chr'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rubinia Soulsinger'),
    (select id from sets where short_name = 'chr'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hell''s Caretaker'),
    (select id from sets where short_name = 'chr'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beasts of Bogardan'),
    (select id from sets where short_name = 'chr'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Craw Giant'),
    (select id from sets where short_name = 'chr'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fishliver Oil'),
    (select id from sets where short_name = 'chr'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcades Sabboth'),
    (select id from sets where short_name = 'chr'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kei Takahashi'),
    (select id from sets where short_name = 'chr'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Petra Sphinx'),
    (select id from sets where short_name = 'chr'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Erhnam Djinn'),
    (select id from sets where short_name = 'chr'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakalite'),
    (select id from sets where short_name = 'chr'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Revelation'),
    (select id from sets where short_name = 'chr'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Altar'),
    (select id from sets where short_name = 'chr'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Slug'),
    (select id from sets where short_name = 'chr'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jeweled Bird'),
    (select id from sets where short_name = 'chr'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblins of the Flarg'),
    (select id from sets where short_name = 'chr'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat Warriors'),
    (select id from sets where short_name = 'chr'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primordial Ooze'),
    (select id from sets where short_name = 'chr'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yawgmoth Demon'),
    (select id from sets where short_name = 'chr'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = 'chr'),
    '115a',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = 'chr'),
    '115c',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Remove Soul'),
    (select id from sets where short_name = 'chr'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = 'chr'),
    '116b',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tobias Andrion'),
    (select id from sets where short_name = 'chr'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Transmutation'),
    (select id from sets where short_name = 'chr'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = 'chr'),
    '114d',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bronze Horse'),
    (select id from sets where short_name = 'chr'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm Seeker'),
    (select id from sets where short_name = 'chr'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chromium'),
    (select id from sets where short_name = 'chr'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tor Wauki'),
    (select id from sets where short_name = 'chr'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aladdin'),
    (select id from sets where short_name = 'chr'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gabriel Angelfire'),
    (select id from sets where short_name = 'chr'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enchantment Alteration'),
    (select id from sets where short_name = 'chr'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Metamorphosis'),
    (select id from sets where short_name = 'chr'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain Yeti'),
    (select id from sets where short_name = 'chr'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Juxtapose'),
    (select id from sets where short_name = 'chr'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cyclone'),
    (select id from sets where short_name = 'chr'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Heat'),
    (select id from sets where short_name = 'chr'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sentinel'),
    (select id from sets where short_name = 'chr'),
    '107',
    'rare'
) 
 on conflict do nothing;
