insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Insect'),
    (select id from sets where short_name = 'tsoi'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'tsoi'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clue'),
    (select id from sets where short_name = 'tsoi'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clue'),
    (select id from sets where short_name = 'tsoi'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tsoi'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clue'),
    (select id from sets where short_name = 'tsoi'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Devil'),
    (select id from sets where short_name = 'tsoi'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shadows Over Innistrad Checklist 2'),
    (select id from sets where short_name = 'tsoi'),
    'CH2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shadows Over Innistrad Checklist 1'),
    (select id from sets where short_name = 'tsoi'),
    'CH1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clue'),
    (select id from sets where short_name = 'tsoi'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ooze'),
    (select id from sets where short_name = 'tsoi'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace, Unraveler of Secrets Emblem'),
    (select id from sets where short_name = 'tsoi'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Knight'),
    (select id from sets where short_name = 'tsoi'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Human Cleric'),
    (select id from sets where short_name = 'tsoi'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clue'),
    (select id from sets where short_name = 'tsoi'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arlinn Kord Emblem'),
    (select id from sets where short_name = 'tsoi'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'tsoi'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Human Soldier'),
    (select id from sets where short_name = 'tsoi'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tsoi'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clue'),
    (select id from sets where short_name = 'tsoi'),
    '11',
    'common'
) 
 on conflict do nothing;
