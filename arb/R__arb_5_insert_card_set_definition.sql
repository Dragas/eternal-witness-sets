insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Gorger Wurm'),
    (select id from sets where short_name = 'arb'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vedalken Ghoul'),
    (select id from sets where short_name = 'arb'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zealous Persecution'),
    (select id from sets where short_name = 'arb'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ethercaste Knight'),
    (select id from sets where short_name = 'arb'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blitz Hellion'),
    (select id from sets where short_name = 'arb'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grixis Sojourners'),
    (select id from sets where short_name = 'arb'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of New Alara'),
    (select id from sets where short_name = 'arb'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Ambush Beetle'),
    (select id from sets where short_name = 'arb'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nemesis of Reason'),
    (select id from sets where short_name = 'arb'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necromancer''s Covenant'),
    (select id from sets where short_name = 'arb'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uril, the Miststalker'),
    (select id from sets where short_name = 'arb'),
    '124',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pale Recluse'),
    (select id from sets where short_name = 'arb'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jhessian Zombies'),
    (select id from sets where short_name = 'arb'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormcaller''s Boon'),
    (select id from sets where short_name = 'arb'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bant Sureblade'),
    (select id from sets where short_name = 'arb'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Defiler of Souls'),
    (select id from sets where short_name = 'arb'),
    '37',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Intimidation Bolt'),
    (select id from sets where short_name = 'arb'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meddling Mage'),
    (select id from sets where short_name = 'arb'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Violent Outburst'),
    (select id from sets where short_name = 'arb'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vectis Dominator'),
    (select id from sets where short_name = 'arb'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Valley Rannet'),
    (select id from sets where short_name = 'arb'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Madrush Cyclops'),
    (select id from sets where short_name = 'arb'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rhox Brute'),
    (select id from sets where short_name = 'arb'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Reaver'),
    (select id from sets where short_name = 'arb'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Offering to Asha'),
    (select id from sets where short_name = 'arb'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talon Trooper'),
    (select id from sets where short_name = 'arb'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Retaliator Griffin'),
    (select id from sets where short_name = 'arb'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Colossal Might'),
    (select id from sets where short_name = 'arb'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grizzled Leotau'),
    (select id from sets where short_name = 'arb'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Double Negative'),
    (select id from sets where short_name = 'arb'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mage Slayer'),
    (select id from sets where short_name = 'arb'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sovereigns of Lost Alara'),
    (select id from sets where short_name = 'arb'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Etherium Abomination'),
    (select id from sets where short_name = 'arb'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Funeral'),
    (select id from sets where short_name = 'arb'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glassdust Hulk'),
    (select id from sets where short_name = 'arb'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Appeasement'),
    (select id from sets where short_name = 'arb'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thraximundar'),
    (select id from sets where short_name = 'arb'),
    '113',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sen Triplets'),
    (select id from sets where short_name = 'arb'),
    '109',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Nexus'),
    (select id from sets where short_name = 'arb'),
    '130',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thopter Foundry'),
    (select id from sets where short_name = 'arb'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aven Mimeomancer'),
    (select id from sets where short_name = 'arb'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soulquake'),
    (select id from sets where short_name = 'arb'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spellbound Dragon'),
    (select id from sets where short_name = 'arb'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fight to the Death'),
    (select id from sets where short_name = 'arb'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Etherwrought Page'),
    (select id from sets where short_name = 'arb'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reborn Hope'),
    (select id from sets where short_name = 'arb'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Predatory Advantage'),
    (select id from sets where short_name = 'arb'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vedalken Heretic'),
    (select id from sets where short_name = 'arb'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Naya Hushblade'),
    (select id from sets where short_name = 'arb'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trace of Abundance'),
    (select id from sets where short_name = 'arb'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jund Hackblade'),
    (select id from sets where short_name = 'arb'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enlisted Wurm'),
    (select id from sets where short_name = 'arb'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Morbid Bloom'),
    (select id from sets where short_name = 'arb'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unbender Tine'),
    (select id from sets where short_name = 'arb'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kathari Bomber'),
    (select id from sets where short_name = 'arb'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slave of Bolas'),
    (select id from sets where short_name = 'arb'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sanctum Plowbeast'),
    (select id from sets where short_name = 'arb'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vithian Renegades'),
    (select id from sets where short_name = 'arb'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lavalanche'),
    (select id from sets where short_name = 'arb'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Qasali Pridemage'),
    (select id from sets where short_name = 'arb'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Veinfire Borderpost'),
    (select id from sets where short_name = 'arb'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karrthus, Tyrant of Jund'),
    (select id from sets where short_name = 'arb'),
    '117',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mistvein Borderpost'),
    (select id from sets where short_name = 'arb'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Filigree Angel'),
    (select id from sets where short_name = 'arb'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Breath of Malfegor'),
    (select id from sets where short_name = 'arb'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Godtracker of Jund'),
    (select id from sets where short_name = 'arb'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naya Sojourners'),
    (select id from sets where short_name = 'arb'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodbraid Elf'),
    (select id from sets where short_name = 'arb'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enigma Sphinx'),
    (select id from sets where short_name = 'arb'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vengeful Rebirth'),
    (select id from sets where short_name = 'arb'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dauntless Escort'),
    (select id from sets where short_name = 'arb'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mycoid Shepherd'),
    (select id from sets where short_name = 'arb'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Igneous Pouncer'),
    (select id from sets where short_name = 'arb'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Behemoth Sledge'),
    (select id from sets where short_name = 'arb'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jund Sojourners'),
    (select id from sets where short_name = 'arb'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Esper Sojourners'),
    (select id from sets where short_name = 'arb'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Armorguard'),
    (select id from sets where short_name = 'arb'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mask of Riddles'),
    (select id from sets where short_name = 'arb'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gloryscale Viashino'),
    (select id from sets where short_name = 'arb'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wargate'),
    (select id from sets where short_name = 'arb'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wildfield Borderpost'),
    (select id from sets where short_name = 'arb'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drastic Revelation'),
    (select id from sets where short_name = 'arb'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brainbite'),
    (select id from sets where short_name = 'arb'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Identity Crisis'),
    (select id from sets where short_name = 'arb'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fieldmist Borderpost'),
    (select id from sets where short_name = 'arb'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ardent Plea'),
    (select id from sets where short_name = 'arb'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grixis Grimblade'),
    (select id from sets where short_name = 'arb'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shield of the Righteous'),
    (select id from sets where short_name = 'arb'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spellbreaker Behemoth'),
    (select id from sets where short_name = 'arb'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nulltread Gargantuan'),
    (select id from sets where short_name = 'arb'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glory of Warfare'),
    (select id from sets where short_name = 'arb'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knotvine Paladin'),
    (select id from sets where short_name = 'arb'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sigil of the Nayan Gods'),
    (select id from sets where short_name = 'arb'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Putrid Leech'),
    (select id from sets where short_name = 'arb'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadshot Minotaur'),
    (select id from sets where short_name = 'arb'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anathemancer'),
    (select id from sets where short_name = 'arb'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Singe-Mind Ogre'),
    (select id from sets where short_name = 'arb'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tainted Sigil'),
    (select id from sets where short_name = 'arb'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Monstrous Carabid'),
    (select id from sets where short_name = 'arb'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thought Hemorrhage'),
    (select id from sets where short_name = 'arb'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Manipulation'),
    (select id from sets where short_name = 'arb'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winged Coatl'),
    (select id from sets where short_name = 'arb'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magefire Wings'),
    (select id from sets where short_name = 'arb'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mayael''s Aria'),
    (select id from sets where short_name = 'arb'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sanity Gnawers'),
    (select id from sets where short_name = 'arb'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ethersworn Shieldmage'),
    (select id from sets where short_name = 'arb'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time Sieve'),
    (select id from sets where short_name = 'arb'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloven Casting'),
    (select id from sets where short_name = 'arb'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphinx of the Steel Wind'),
    (select id from sets where short_name = 'arb'),
    '110',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Marisi''s Twinclaws'),
    (select id from sets where short_name = 'arb'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unscythe, Killer of Kings'),
    (select id from sets where short_name = 'arb'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crystallization'),
    (select id from sets where short_name = 'arb'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marrow Chomper'),
    (select id from sets where short_name = 'arb'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Finest Hour'),
    (select id from sets where short_name = 'arb'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jenara, Asura of War'),
    (select id from sets where short_name = 'arb'),
    '128',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Flurry of Wings'),
    (select id from sets where short_name = 'arb'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sewn-Eye Drake'),
    (select id from sets where short_name = 'arb'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terminate'),
    (select id from sets where short_name = 'arb'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Denial'),
    (select id from sets where short_name = 'arb'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lich Lord of Unx'),
    (select id from sets where short_name = 'arb'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sigiled Behemoth'),
    (select id from sets where short_name = 'arb'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kathari Remnant'),
    (select id from sets where short_name = 'arb'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Esper Stormblade'),
    (select id from sets where short_name = 'arb'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lorescale Coatl'),
    (select id from sets where short_name = 'arb'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firewild Borderpost'),
    (select id from sets where short_name = 'arb'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Pulse'),
    (select id from sets where short_name = 'arb'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Architects of Will'),
    (select id from sets where short_name = 'arb'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bant Sojourners'),
    (select id from sets where short_name = 'arb'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Messenger Falcons'),
    (select id from sets where short_name = 'arb'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deathbringer Thoctar'),
    (select id from sets where short_name = 'arb'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deny Reality'),
    (select id from sets where short_name = 'arb'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stun Sniper'),
    (select id from sets where short_name = 'arb'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Illusory Demon'),
    (select id from sets where short_name = 'arb'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arsenal Thresher'),
    (select id from sets where short_name = 'arb'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cerodon Yearling'),
    (select id from sets where short_name = 'arb'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sigil Captain'),
    (select id from sets where short_name = 'arb'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Broodmother'),
    (select id from sets where short_name = 'arb'),
    '53',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sages of the Anima'),
    (select id from sets where short_name = 'arb'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sangrite Backlash'),
    (select id from sets where short_name = 'arb'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyclaw Thrash'),
    (select id from sets where short_name = 'arb'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demonic Dread'),
    (select id from sets where short_name = 'arb'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lord of Extinction'),
    (select id from sets where short_name = 'arb'),
    '91',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Captured Sunlight'),
    (select id from sets where short_name = 'arb'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bituminous Blast'),
    (select id from sets where short_name = 'arb'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demonspine Whip'),
    (select id from sets where short_name = 'arb'),
    '39',
    'uncommon'
) 
 on conflict do nothing;
