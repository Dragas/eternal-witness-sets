insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Rescuer Sphinx'),
    (select id from sets where short_name = 'war'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'war'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burning Prophet'),
    (select id from sets where short_name = 'war'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'New Horizons'),
    (select id from sets where short_name = 'war'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erratic Visionary'),
    (select id from sets where short_name = 'war'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace''s Ruse'),
    (select id from sets where short_name = 'war'),
    '273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'war'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Storm the Citadel'),
    (select id from sets where short_name = 'war'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dreadhorde Invasion'),
    (select id from sets where short_name = 'war'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'war'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Return to Nature'),
    (select id from sets where short_name = 'war'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Casualties of War'),
    (select id from sets where short_name = 'war'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Davriel''s Shadowfugue'),
    (select id from sets where short_name = 'war'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nahiri''s Stoneblades'),
    (select id from sets where short_name = 'war'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shriekdiver'),
    (select id from sets where short_name = 'war'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gleaming Overseer'),
    (select id from sets where short_name = 'war'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'war'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'God-Eternal Rhonas'),
    (select id from sets where short_name = 'war'),
    '163',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Naga Eternal'),
    (select id from sets where short_name = 'war'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jiang Yanggu, Wildcrafter'),
    (select id from sets where short_name = 'war'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Planewide Celebration'),
    (select id from sets where short_name = 'war'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bulwark Giant'),
    (select id from sets where short_name = 'war'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jaya, Venerated Firemage'),
    (select id from sets where short_name = 'war'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Interplanar Beacon'),
    (select id from sets where short_name = 'war'),
    '247',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis, the Hate-Twisted'),
    (select id from sets where short_name = 'war'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duskmantle Operative'),
    (select id from sets where short_name = 'war'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Geode'),
    (select id from sets where short_name = 'war'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mayhem Devil'),
    (select id from sets where short_name = 'war'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Samut, Tyrant Smasher'),
    (select id from sets where short_name = 'war'),
    '235★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana, Dreadhorde General'),
    (select id from sets where short_name = 'war'),
    '97★',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Domri, Anarch of Bolas'),
    (select id from sets where short_name = 'war'),
    '191★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Pyrohelix'),
    (select id from sets where short_name = 'war'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Davriel, Rogue Shadowmage'),
    (select id from sets where short_name = 'war'),
    '83★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blindblast'),
    (select id from sets where short_name = 'war'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'war'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kiora, Behemoth Beckoner'),
    (select id from sets where short_name = 'war'),
    '232★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spark Harvest'),
    (select id from sets where short_name = 'war'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Commence the Endgame'),
    (select id from sets where short_name = 'war'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'war'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mobilized District'),
    (select id from sets where short_name = 'war'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vizier of the Scorpion'),
    (select id from sets where short_name = 'war'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'war'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nissa, Who Shakes the World'),
    (select id from sets where short_name = 'war'),
    '169★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Parhelion II'),
    (select id from sets where short_name = 'war'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Wanderer'),
    (select id from sets where short_name = 'war'),
    '37★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Topple the Statue'),
    (select id from sets where short_name = 'war'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tibalt''s Rager'),
    (select id from sets where short_name = 'war'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grim Initiate'),
    (select id from sets where short_name = 'war'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sarkhan the Masterless'),
    (select id from sets where short_name = 'war'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merfolk Skydiver'),
    (select id from sets where short_name = 'war'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ahn-Crop Invader'),
    (select id from sets where short_name = 'war'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sarkhan''s Catharsis'),
    (select id from sets where short_name = 'war'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jiang Yanggu, Wildcrafter'),
    (select id from sets where short_name = 'war'),
    '164★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Challenger Troll'),
    (select id from sets where short_name = 'war'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Despark'),
    (select id from sets where short_name = 'war'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sorin, Vengeful Bloodlord'),
    (select id from sets where short_name = 'war'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Charmed Stray'),
    (select id from sets where short_name = 'war'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charity Extractor'),
    (select id from sets where short_name = 'war'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'God-Eternal Bontu'),
    (select id from sets where short_name = 'war'),
    '92',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Krenko, Tin Street Kingpin'),
    (select id from sets where short_name = 'war'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivien''s Grizzly'),
    (select id from sets where short_name = 'war'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bolt Bend'),
    (select id from sets where short_name = 'war'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gideon Blackblade'),
    (select id from sets where short_name = 'war'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chandra, Fire Artisan'),
    (select id from sets where short_name = 'war'),
    '119★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tithebearer Giant'),
    (select id from sets where short_name = 'war'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashiok, Dream Render'),
    (select id from sets where short_name = 'war'),
    '228★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angrath, Captain of Chaos'),
    (select id from sets where short_name = 'war'),
    '227★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karn, the Great Creator'),
    (select id from sets where short_name = 'war'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nahiri, Storm of Stone'),
    (select id from sets where short_name = 'war'),
    '233★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prismite'),
    (select id from sets where short_name = 'war'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'war'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relentless Advance'),
    (select id from sets where short_name = 'war'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arlinn, Voice of the Pack'),
    (select id from sets where short_name = 'war'),
    '150★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gideon, the Oathsworn'),
    (select id from sets where short_name = 'war'),
    '265',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vivien, Champion of the Wilds'),
    (select id from sets where short_name = 'war'),
    '180★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Toll of the Invasion'),
    (select id from sets where short_name = 'war'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tenth District Legionnaire'),
    (select id from sets where short_name = 'war'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dreadhorde Butcher'),
    (select id from sets where short_name = 'war'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kraul Stinger'),
    (select id from sets where short_name = 'war'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'war'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snarespinner'),
    (select id from sets where short_name = 'war'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace''s Triumph'),
    (select id from sets where short_name = 'war'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steady Aim'),
    (select id from sets where short_name = 'war'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pollenbright Druid'),
    (select id from sets where short_name = 'war'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Finale of Glory'),
    (select id from sets where short_name = 'war'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Band Together'),
    (select id from sets where short_name = 'war'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ignite the Beacon'),
    (select id from sets where short_name = 'war'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sorin''s Thirst'),
    (select id from sets where short_name = 'war'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fblthp, the Lost'),
    (select id from sets where short_name = 'war'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teyo, the Shieldmage'),
    (select id from sets where short_name = 'war'),
    '32★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gideon''s Battle Cry'),
    (select id from sets where short_name = 'war'),
    '267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arlinn, Voice of the Pack'),
    (select id from sets where short_name = 'war'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mizzium Tank'),
    (select id from sets where short_name = 'war'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Niv-Mizzet Reborn'),
    (select id from sets where short_name = 'war'),
    '208',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lazotep Reaver'),
    (select id from sets where short_name = 'war'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Role Reversal'),
    (select id from sets where short_name = 'war'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storrev, Devkarin Lich'),
    (select id from sets where short_name = 'war'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vraska, Swarm''s Eminence'),
    (select id from sets where short_name = 'war'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Saheeli, Sublime Artificer'),
    (select id from sets where short_name = 'war'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Davriel, Rogue Shadowmage'),
    (select id from sets where short_name = 'war'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis, the Hate-Twisted'),
    (select id from sets where short_name = 'war'),
    '100★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forced Landing'),
    (select id from sets where short_name = 'war'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teferi, Time Raveler'),
    (select id from sets where short_name = 'war'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dovin''s Veto'),
    (select id from sets where short_name = 'war'),
    '193',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'God-Eternal Kefnet'),
    (select id from sets where short_name = 'war'),
    '53',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aid the Fallen'),
    (select id from sets where short_name = 'war'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Neheb, Dreadhorde Champion'),
    (select id from sets where short_name = 'war'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dovin, Hand of Control'),
    (select id from sets where short_name = 'war'),
    '229★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Time Wipe'),
    (select id from sets where short_name = 'war'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Narset''s Reversal'),
    (select id from sets where short_name = 'war'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bond of Insight'),
    (select id from sets where short_name = 'war'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Finale of Revelation'),
    (select id from sets where short_name = 'war'),
    '51',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Callous Dismissal'),
    (select id from sets where short_name = 'war'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Invading Manticore'),
    (select id from sets where short_name = 'war'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ral, Storm Conduit'),
    (select id from sets where short_name = 'war'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Courage in Crisis'),
    (select id from sets where short_name = 'war'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deliver Unto Evil'),
    (select id from sets where short_name = 'war'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tibalt, Rakish Instigator'),
    (select id from sets where short_name = 'war'),
    '146★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desperate Lunge'),
    (select id from sets where short_name = 'war'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ral, Storm Conduit'),
    (select id from sets where short_name = 'war'),
    '211★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jace, Wielder of Mysteries'),
    (select id from sets where short_name = 'war'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ral''s Outburst'),
    (select id from sets where short_name = 'war'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ajani, the Greathearted'),
    (select id from sets where short_name = 'war'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathsprout'),
    (select id from sets where short_name = 'war'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Paradise Druid'),
    (select id from sets where short_name = 'war'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'war'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Triumph'),
    (select id from sets where short_name = 'war'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dreadmalkin'),
    (select id from sets where short_name = 'war'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Divine Arrow'),
    (select id from sets where short_name = 'war'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arlinn''s Wolf'),
    (select id from sets where short_name = 'war'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spellkeeper Weird'),
    (select id from sets where short_name = 'war'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'war'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Honor the God-Pharaoh'),
    (select id from sets where short_name = 'war'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tamiyo, Collector of Tales'),
    (select id from sets where short_name = 'war'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eternal Taskmaster'),
    (select id from sets where short_name = 'war'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nissa''s Triumph'),
    (select id from sets where short_name = 'war'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kiora, Behemoth Beckoner'),
    (select id from sets where short_name = 'war'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ugin, the Ineffable'),
    (select id from sets where short_name = 'war'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heartfire'),
    (select id from sets where short_name = 'war'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grateful Apparition'),
    (select id from sets where short_name = 'war'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'God-Pharaoh''s Statue'),
    (select id from sets where short_name = 'war'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashiok''s Skulker'),
    (select id from sets where short_name = 'war'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stealth Mission'),
    (select id from sets where short_name = 'war'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tibalt, Rakish Instigator'),
    (select id from sets where short_name = 'war'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Diviner'),
    (select id from sets where short_name = 'war'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Finale of Promise'),
    (select id from sets where short_name = 'war'),
    '127',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gateway Plaza'),
    (select id from sets where short_name = 'war'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreadhorde Twins'),
    (select id from sets where short_name = 'war'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Makeshift Battalion'),
    (select id from sets where short_name = 'war'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'war'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Triumph'),
    (select id from sets where short_name = 'war'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gideon''s Sacrifice'),
    (select id from sets where short_name = 'war'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Finale of Eternity'),
    (select id from sets where short_name = 'war'),
    '91',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wanderer''s Strike'),
    (select id from sets where short_name = 'war'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cruel Celebrant'),
    (select id from sets where short_name = 'war'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Assault Team'),
    (select id from sets where short_name = 'war'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nahiri, Storm of Stone'),
    (select id from sets where short_name = 'war'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Devouring Hellion'),
    (select id from sets where short_name = 'war'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trusted Pegasus'),
    (select id from sets where short_name = 'war'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battlefield Promotion'),
    (select id from sets where short_name = 'war'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Turret Ogre'),
    (select id from sets where short_name = 'war'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Narset, Parter of Veils'),
    (select id from sets where short_name = 'war'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Huatli, the Sun''s Heart'),
    (select id from sets where short_name = 'war'),
    '230★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Contentious Plan'),
    (select id from sets where short_name = 'war'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kronch Wrangler'),
    (select id from sets where short_name = 'war'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spellgorger Weird'),
    (select id from sets where short_name = 'war'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunblade Angel'),
    (select id from sets where short_name = 'war'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spark Reaper'),
    (select id from sets where short_name = 'war'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Neoform'),
    (select id from sets where short_name = 'war'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pouncing Lynx'),
    (select id from sets where short_name = 'war'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Samut''s Sprint'),
    (select id from sets where short_name = 'war'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crush Dissent'),
    (select id from sets where short_name = 'war'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis''s Cruelty'),
    (select id from sets where short_name = 'war'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Runes'),
    (select id from sets where short_name = 'war'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Widespread Brutality'),
    (select id from sets where short_name = 'war'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaya, Bane of the Dead'),
    (select id from sets where short_name = 'war'),
    '231★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Finale of Devastation'),
    (select id from sets where short_name = 'war'),
    '160',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thundering Ceratok'),
    (select id from sets where short_name = 'war'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sky Theater Strix'),
    (select id from sets where short_name = 'war'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simic Guildgate'),
    (select id from sets where short_name = 'war'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, Dragon-God'),
    (select id from sets where short_name = 'war'),
    '207',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kaya''s Ghostform'),
    (select id from sets where short_name = 'war'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ugin''s Conjurant'),
    (select id from sets where short_name = 'war'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sorin, Vengeful Bloodlord'),
    (select id from sets where short_name = 'war'),
    '217★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ugin, the Ineffable'),
    (select id from sets where short_name = 'war'),
    '2★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karn, the Great Creator'),
    (select id from sets where short_name = 'war'),
    '1★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rubblebelt Rioters'),
    (select id from sets where short_name = 'war'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unlikely Aid'),
    (select id from sets where short_name = 'war'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'war'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kasmina, Enigmatic Mentor'),
    (select id from sets where short_name = 'war'),
    '56★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tezzeret, Master of the Bridge'),
    (select id from sets where short_name = 'war'),
    '275',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gideon''s Company'),
    (select id from sets where short_name = 'war'),
    '268',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'war'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guildpact Informant'),
    (select id from sets where short_name = 'war'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Elderspell'),
    (select id from sets where short_name = 'war'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Law-Rune Enforcer'),
    (select id from sets where short_name = 'war'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Invade the City'),
    (select id from sets where short_name = 'war'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guild Globe'),
    (select id from sets where short_name = 'war'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Opportunist'),
    (select id from sets where short_name = 'war'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teyo, the Shieldmage'),
    (select id from sets where short_name = 'war'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kasmina''s Transmutation'),
    (select id from sets where short_name = 'war'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kaya, Bane of the Dead'),
    (select id from sets where short_name = 'war'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'war'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angrath, Captain of Chaos'),
    (select id from sets where short_name = 'war'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wardscale Crocodile'),
    (select id from sets where short_name = 'war'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emergence Zone'),
    (select id from sets where short_name = 'war'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Saheeli, Sublime Artificer'),
    (select id from sets where short_name = 'war'),
    '234★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bleeding Edge'),
    (select id from sets where short_name = 'war'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kiora''s Dambreaker'),
    (select id from sets where short_name = 'war'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Martyr for the Cause'),
    (select id from sets where short_name = 'war'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Totally Lost'),
    (select id from sets where short_name = 'war'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silent Submersible'),
    (select id from sets where short_name = 'war'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raging Kronch'),
    (select id from sets where short_name = 'war'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bond of Passion'),
    (select id from sets where short_name = 'war'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heartwarming Redemption'),
    (select id from sets where short_name = 'war'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ironclad Krovod'),
    (select id from sets where short_name = 'war'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demolish'),
    (select id from sets where short_name = 'war'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Domri''s Ambush'),
    (select id from sets where short_name = 'war'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Pridemate'),
    (select id from sets where short_name = 'war'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evolution Sage'),
    (select id from sets where short_name = 'war'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Awakening of Vitu-Ghazi'),
    (select id from sets where short_name = 'war'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lazotep Behemoth'),
    (select id from sets where short_name = 'war'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace, Arcane Strategist'),
    (select id from sets where short_name = 'war'),
    '270',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tyrant''s Scorn'),
    (select id from sets where short_name = 'war'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gideon Blackblade'),
    (select id from sets where short_name = 'war'),
    '13★',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Karn''s Bastion'),
    (select id from sets where short_name = 'war'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Banehound'),
    (select id from sets where short_name = 'war'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Huatli''s Raptor'),
    (select id from sets where short_name = 'war'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bond of Discipline'),
    (select id from sets where short_name = 'war'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spark Double'),
    (select id from sets where short_name = 'war'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'No Escape'),
    (select id from sets where short_name = 'war'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dovin, Hand of Control'),
    (select id from sets where short_name = 'war'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jaya, Venerated Firemage'),
    (select id from sets where short_name = 'war'),
    '135★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Massacre Girl'),
    (select id from sets where short_name = 'war'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani, the Greathearted'),
    (select id from sets where short_name = 'war'),
    '184★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Centaur Nurturer'),
    (select id from sets where short_name = 'war'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ilharg, the Raze-Boar'),
    (select id from sets where short_name = 'war'),
    '133',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vraska, Swarm''s Eminence'),
    (select id from sets where short_name = 'war'),
    '236★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Domri, Anarch of Bolas'),
    (select id from sets where short_name = 'war'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, Dragon-God'),
    (select id from sets where short_name = 'war'),
    '207★',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Arboreal Grazer'),
    (select id from sets where short_name = 'war'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace''s Projection'),
    (select id from sets where short_name = 'war'),
    '272',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leyline Prowler'),
    (select id from sets where short_name = 'war'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loxodon Sergeant'),
    (select id from sets where short_name = 'war'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flux Channeler'),
    (select id from sets where short_name = 'war'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bond of Revival'),
    (select id from sets where short_name = 'war'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kasmina, Enigmatic Mentor'),
    (select id from sets where short_name = 'war'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Time Twist'),
    (select id from sets where short_name = 'war'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Living Twister'),
    (select id from sets where short_name = 'war'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pledge of Unity'),
    (select id from sets where short_name = 'war'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orzhov Guildgate'),
    (select id from sets where short_name = 'war'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Single Combat'),
    (select id from sets where short_name = 'war'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tamiyo''s Epiphany'),
    (select id from sets where short_name = 'war'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Wanderer'),
    (select id from sets where short_name = 'war'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Augur of Bolas'),
    (select id from sets where short_name = 'war'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bolas''s Citadel'),
    (select id from sets where short_name = 'war'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra, Fire Artisan'),
    (select id from sets where short_name = 'war'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elite Guardmage'),
    (select id from sets where short_name = 'war'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chainwhip Cyclops'),
    (select id from sets where short_name = 'war'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'War Screecher'),
    (select id from sets where short_name = 'war'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prison Realm'),
    (select id from sets where short_name = 'war'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jace, Wielder of Mysteries'),
    (select id from sets where short_name = 'war'),
    '54★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gideon''s Triumph'),
    (select id from sets where short_name = 'war'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Huatli, the Sun''s Heart'),
    (select id from sets where short_name = 'war'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana, Dreadhorde General'),
    (select id from sets where short_name = 'war'),
    '97',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Herald of the Dreadhorde'),
    (select id from sets where short_name = 'war'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'war'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cyclops Electromancer'),
    (select id from sets where short_name = 'war'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Saheeli''s Silverwing'),
    (select id from sets where short_name = 'war'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Defiant Strike'),
    (select id from sets where short_name = 'war'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Roalesk, Apex Hybrid'),
    (select id from sets where short_name = 'war'),
    '213',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Command the Dreadhorde'),
    (select id from sets where short_name = 'war'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Iron Bully'),
    (select id from sets where short_name = 'war'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sarkhan the Masterless'),
    (select id from sets where short_name = 'war'),
    '143★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'God-Eternal Oketra'),
    (select id from sets where short_name = 'war'),
    '16',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bond of Flourishing'),
    (select id from sets where short_name = 'war'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teyo''s Lightshield'),
    (select id from sets where short_name = 'war'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Samut, Tyrant Smasher'),
    (select id from sets where short_name = 'war'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Narset, Parter of Veils'),
    (select id from sets where short_name = 'war'),
    '61★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ravnica at War'),
    (select id from sets where short_name = 'war'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Assailant'),
    (select id from sets where short_name = 'war'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feather, the Redeemed'),
    (select id from sets where short_name = 'war'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enforcer Griffin'),
    (select id from sets where short_name = 'war'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tamiyo, Collector of Tales'),
    (select id from sets where short_name = 'war'),
    '220★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blast Zone'),
    (select id from sets where short_name = 'war'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Solar Blaze'),
    (select id from sets where short_name = 'war'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firemind Vessel'),
    (select id from sets where short_name = 'war'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jaya''s Greeting'),
    (select id from sets where short_name = 'war'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rising Populace'),
    (select id from sets where short_name = 'war'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivien''s Arkbow'),
    (select id from sets where short_name = 'war'),
    '181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lazotep Plating'),
    (select id from sets where short_name = 'war'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rally of Wings'),
    (select id from sets where short_name = 'war'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashiok, Dream Render'),
    (select id from sets where short_name = 'war'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tomik, Distinguished Advokist'),
    (select id from sets where short_name = 'war'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mowu, Loyal Companion'),
    (select id from sets where short_name = 'war'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angrath''s Rampage'),
    (select id from sets where short_name = 'war'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eternal Skylord'),
    (select id from sets where short_name = 'war'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enter the God-Eternals'),
    (select id from sets where short_name = 'war'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi, Time Raveler'),
    (select id from sets where short_name = 'war'),
    '221★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tolsimir, Friend to Wolves'),
    (select id from sets where short_name = 'war'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa, Who Shakes the World'),
    (select id from sets where short_name = 'war'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thunder Drake'),
    (select id from sets where short_name = 'war'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vraska''s Finisher'),
    (select id from sets where short_name = 'war'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oath of Kaya'),
    (select id from sets where short_name = 'war'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bioessence Hydra'),
    (select id from sets where short_name = 'war'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primordial Wurm'),
    (select id from sets where short_name = 'war'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloom Hulk'),
    (select id from sets where short_name = 'war'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Price of Betrayal'),
    (select id from sets where short_name = 'war'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dreadhorde Arcanist'),
    (select id from sets where short_name = 'war'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aven Eternal'),
    (select id from sets where short_name = 'war'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivien, Champion of the Wilds'),
    (select id from sets where short_name = 'war'),
    '180',
    'rare'
) 
 on conflict do nothing;
