    insert into mtgcard(name) values ('Command Tower') on conflict do nothing;
    insert into mtgcard(name) values ('Bane of Progress') on conflict do nothing;
    insert into mtgcard(name) values ('Seedborn Muse') on conflict do nothing;
    insert into mtgcard(name) values ('Omnath, Locus of Mana') on conflict do nothing;
    insert into mtgcard(name) values ('Sol Ring') on conflict do nothing;
    insert into mtgcard(name) values ('Sylvan Library') on conflict do nothing;
    insert into mtgcard(name) values ('Freyalise, Llanowar''s Fury') on conflict do nothing;
    insert into mtgcard(name) values ('Worldly Tutor') on conflict do nothing;
