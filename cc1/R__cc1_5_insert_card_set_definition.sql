insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Command Tower'),
    (select id from sets where short_name = 'cc1'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bane of Progress'),
    (select id from sets where short_name = 'cc1'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seedborn Muse'),
    (select id from sets where short_name = 'cc1'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Omnath, Locus of Mana'),
    (select id from sets where short_name = 'cc1'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'cc1'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Library'),
    (select id from sets where short_name = 'cc1'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Freyalise, Llanowar''s Fury'),
    (select id from sets where short_name = 'cc1'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Worldly Tutor'),
    (select id from sets where short_name = 'cc1'),
    '6',
    'rare'
) 
 on conflict do nothing;
