insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Song of the Dryads'),
    (select id from sets where short_name = 'plist'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ith, High Arcanist'),
    (select id from sets where short_name = 'plist'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spike Weaver'),
    (select id from sets where short_name = 'plist'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wastes'),
    (select id from sets where short_name = 'plist'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lotus Bloom'),
    (select id from sets where short_name = 'plist'),
    '261',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spined Thopter'),
    (select id from sets where short_name = 'plist'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Entreat the Angels'),
    (select id from sets where short_name = 'plist'),
    '17',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Khalni Hydra'),
    (select id from sets where short_name = 'plist'),
    '181',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scarecrone'),
    (select id from sets where short_name = 'plist'),
    '267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imperious Perfect'),
    (select id from sets where short_name = 'plist'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gatekeeper of Malakir'),
    (select id from sets where short_name = 'plist'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stonybrook Banneret'),
    (select id from sets where short_name = 'plist'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Army Ants'),
    (select id from sets where short_name = 'plist'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marauding Raptor'),
    (select id from sets where short_name = 'plist'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Chain Veil'),
    (select id from sets where short_name = 'plist'),
    '247',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Skithiryx, the Blight Dragon'),
    (select id from sets where short_name = 'plist'),
    '113',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Obliterator'),
    (select id from sets where short_name = 'plist'),
    '110',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'plist'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Blueprints'),
    (select id from sets where short_name = 'plist'),
    '277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lazav, the Multifarious'),
    (select id from sets where short_name = 'plist'),
    '216',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Primal Vigor'),
    (select id from sets where short_name = 'plist'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brazen Borrower // Petty Theft'),
    (select id from sets where short_name = 'plist'),
    '48',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tuktuk the Explorer'),
    (select id from sets where short_name = 'plist'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slimefoot, the Stowaway'),
    (select id from sets where short_name = 'plist'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Flame of Keld'),
    (select id from sets where short_name = 'plist'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crown of Empires'),
    (select id from sets where short_name = 'plist'),
    '250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moon-Eating Dog'),
    (select id from sets where short_name = 'plist'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Omnath, Locus of Rage'),
    (select id from sets where short_name = 'plist'),
    '224',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Riptide Laboratory'),
    (select id from sets where short_name = 'plist'),
    '295',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fabricate'),
    (select id from sets where short_name = 'plist'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burst Lightning'),
    (select id from sets where short_name = 'plist'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nykthos, Shrine to Nyx'),
    (select id from sets where short_name = 'plist'),
    '293',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bonehoard'),
    (select id from sets where short_name = 'plist'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moonmist'),
    (select id from sets where short_name = 'plist'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Crafter'),
    (select id from sets where short_name = 'plist'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necromancy'),
    (select id from sets where short_name = 'plist'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gideon, Ally of Zendikar'),
    (select id from sets where short_name = 'plist'),
    '18',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fathom Mage'),
    (select id from sets where short_name = 'plist'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warp World'),
    (select id from sets where short_name = 'plist'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tortured Existence'),
    (select id from sets where short_name = 'plist'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Awakening'),
    (select id from sets where short_name = 'plist'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clearwater Goblet'),
    (select id from sets where short_name = 'plist'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiery Justice'),
    (select id from sets where short_name = 'plist'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akroma''s Memorial'),
    (select id from sets where short_name = 'plist'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noble Benefactor'),
    (select id from sets where short_name = 'plist'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fractured Powerstone'),
    (select id from sets where short_name = 'plist'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urborg Panther'),
    (select id from sets where short_name = 'plist'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eternal Witness'),
    (select id from sets where short_name = 'plist'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cathedral of War'),
    (select id from sets where short_name = 'plist'),
    '283',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tombstalker'),
    (select id from sets where short_name = 'plist'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brothers Yamazaki'),
    (select id from sets where short_name = 'plist'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hell''s Thunder'),
    (select id from sets where short_name = 'plist'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodghast'),
    (select id from sets where short_name = 'plist'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Everflowing Chalice'),
    (select id from sets where short_name = 'plist'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Contagion Engine'),
    (select id from sets where short_name = 'plist'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ezuri, Claw of Progress'),
    (select id from sets where short_name = 'plist'),
    '210',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Worn Powerstone'),
    (select id from sets where short_name = 'plist'),
    '281',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Springjack Shepherd'),
    (select id from sets where short_name = 'plist'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Lore'),
    (select id from sets where short_name = 'plist'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zendikar Resurgent'),
    (select id from sets where short_name = 'plist'),
    '199',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Helix Pinnacle'),
    (select id from sets where short_name = 'plist'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winds of Abandon'),
    (select id from sets where short_name = 'plist'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Chieftain'),
    (select id from sets where short_name = 'plist'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nylea, God of the Hunt'),
    (select id from sets where short_name = 'plist'),
    '186',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Emrakul, the Aeons Torn'),
    (select id from sets where short_name = 'plist'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gempalm Polluter'),
    (select id from sets where short_name = 'plist'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erratic Portal'),
    (select id from sets where short_name = 'plist'),
    '253',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trusty Machete'),
    (select id from sets where short_name = 'plist'),
    '276',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Broodstar'),
    (select id from sets where short_name = 'plist'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Splicer''s Skill'),
    (select id from sets where short_name = 'plist'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = 'plist'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hashep Oasis'),
    (select id from sets where short_name = 'plist'),
    '289',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jadelight Ranger'),
    (select id from sets where short_name = 'plist'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Decree of Annihilation'),
    (select id from sets where short_name = 'plist'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Puppet''s Verdict'),
    (select id from sets where short_name = 'plist'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Grenade'),
    (select id from sets where short_name = 'plist'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Monastery Mentor'),
    (select id from sets where short_name = 'plist'),
    '25',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fodder Cannon'),
    (select id from sets where short_name = 'plist'),
    '256',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Djinn Illuminatus'),
    (select id from sets where short_name = 'plist'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forced Fruition'),
    (select id from sets where short_name = 'plist'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jwari Shapeshifter'),
    (select id from sets where short_name = 'plist'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hunted Dragon'),
    (select id from sets where short_name = 'plist'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dryad Arbor'),
    (select id from sets where short_name = 'plist'),
    '285',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weatherlight'),
    (select id from sets where short_name = 'plist'),
    '279',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chromanticore'),
    (select id from sets where short_name = 'plist'),
    '202',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Regal Caracal'),
    (select id from sets where short_name = 'plist'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ulvenwald Mysteries'),
    (select id from sets where short_name = 'plist'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trokin High Guard'),
    (select id from sets where short_name = 'plist'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wayfarer''s Bauble'),
    (select id from sets where short_name = 'plist'),
    '278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Reborn'),
    (select id from sets where short_name = 'plist'),
    '292',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anointer Priest'),
    (select id from sets where short_name = 'plist'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Behemoth'),
    (select id from sets where short_name = 'plist'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eye of the Storm'),
    (select id from sets where short_name = 'plist'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Angel'),
    (select id from sets where short_name = 'plist'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nezumi Graverobber // Nighteyes the Desecrator'),
    (select id from sets where short_name = 'plist'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Murderous Redcap'),
    (select id from sets where short_name = 'plist'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terminate'),
    (select id from sets where short_name = 'plist'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sky Hussar'),
    (select id from sets where short_name = 'plist'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kher Keep'),
    (select id from sets where short_name = 'plist'),
    '291',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancestor''s Prophet'),
    (select id from sets where short_name = 'plist'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noggle Hedge-Mage'),
    (select id from sets where short_name = 'plist'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shambling Shell'),
    (select id from sets where short_name = 'plist'),
    '227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pact of Negation'),
    (select id from sets where short_name = 'plist'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curious Pair // Treats to Share'),
    (select id from sets where short_name = 'plist'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cruel Tutor'),
    (select id from sets where short_name = 'plist'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Challenger'),
    (select id from sets where short_name = 'plist'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Muscle Sliver'),
    (select id from sets where short_name = 'plist'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bogbrew Witch'),
    (select id from sets where short_name = 'plist'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Virtus the Veiled'),
    (select id from sets where short_name = 'plist'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Murderous Cut'),
    (select id from sets where short_name = 'plist'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cut // Ribbons'),
    (select id from sets where short_name = 'plist'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampire Nighthawk'),
    (select id from sets where short_name = 'plist'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyromancer Ascension'),
    (select id from sets where short_name = 'plist'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necravolver'),
    (select id from sets where short_name = 'plist'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel''s Grace'),
    (select id from sets where short_name = 'plist'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coveted Peacock'),
    (select id from sets where short_name = 'plist'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Relentless Rats'),
    (select id from sets where short_name = 'plist'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Craving'),
    (select id from sets where short_name = 'plist'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saber Ants'),
    (select id from sets where short_name = 'plist'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skullclamp'),
    (select id from sets where short_name = 'plist'),
    '270',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kazandu Blademaster'),
    (select id from sets where short_name = 'plist'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sensei Golden-Tail'),
    (select id from sets where short_name = 'plist'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loyal Unicorn'),
    (select id from sets where short_name = 'plist'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodlord of Vaasgoth'),
    (select id from sets where short_name = 'plist'),
    '86',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dromar, the Banisher'),
    (select id from sets where short_name = 'plist'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Light Up the Stage'),
    (select id from sets where short_name = 'plist'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plated Geopede'),
    (select id from sets where short_name = 'plist'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Extinction'),
    (select id from sets where short_name = 'plist'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abrade'),
    (select id from sets where short_name = 'plist'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beck // Call'),
    (select id from sets where short_name = 'plist'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Amoeboid Changeling'),
    (select id from sets where short_name = 'plist'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simian Spirit Guide'),
    (select id from sets where short_name = 'plist'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ixidor, Reality Sculptor'),
    (select id from sets where short_name = 'plist'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas'),
    (select id from sets where short_name = 'plist'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hammer Mage'),
    (select id from sets where short_name = 'plist'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Odds // Ends'),
    (select id from sets where short_name = 'plist'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Thug'),
    (select id from sets where short_name = 'plist'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Death'),
    (select id from sets where short_name = 'plist'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oloro, Ageless Ascetic'),
    (select id from sets where short_name = 'plist'),
    '223',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Unburial Rites'),
    (select id from sets where short_name = 'plist'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zendikar Incarnate'),
    (select id from sets where short_name = 'plist'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shuriken'),
    (select id from sets where short_name = 'plist'),
    '269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hua Tuo, Honored Physician'),
    (select id from sets where short_name = 'plist'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Genesis Hydra'),
    (select id from sets where short_name = 'plist'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Food Chain'),
    (select id from sets where short_name = 'plist'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wild Research'),
    (select id from sets where short_name = 'plist'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Council''s Judgment'),
    (select id from sets where short_name = 'plist'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grizzly Fate'),
    (select id from sets where short_name = 'plist'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snubhorn Sentry'),
    (select id from sets where short_name = 'plist'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Godsire'),
    (select id from sets where short_name = 'plist'),
    '213',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Korlash, Heir to Blackblade'),
    (select id from sets where short_name = 'plist'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Man-o''-War'),
    (select id from sets where short_name = 'plist'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scroll Rack'),
    (select id from sets where short_name = 'plist'),
    '268',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gemstone Mine'),
    (select id from sets where short_name = 'plist'),
    '288',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tree of Tales'),
    (select id from sets where short_name = 'plist'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'plist'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lash Out'),
    (select id from sets where short_name = 'plist'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diregraf Captain'),
    (select id from sets where short_name = 'plist'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hundred-Handed One'),
    (select id from sets where short_name = 'plist'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ishkanah, Grafwidow'),
    (select id from sets where short_name = 'plist'),
    '178',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tamiyo''s Journal'),
    (select id from sets where short_name = 'plist'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shelldock Isle'),
    (select id from sets where short_name = 'plist'),
    '296',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Octopus Umbra'),
    (select id from sets where short_name = 'plist'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reality Shift'),
    (select id from sets where short_name = 'plist'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swiftfoot Boots'),
    (select id from sets where short_name = 'plist'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cadaverous Knight'),
    (select id from sets where short_name = 'plist'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pouncing Shoreshark'),
    (select id from sets where short_name = 'plist'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wrenn and Six'),
    (select id from sets where short_name = 'plist'),
    '232',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vanguard of Brimaz'),
    (select id from sets where short_name = 'plist'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Emeria, the Sky Ruin'),
    (select id from sets where short_name = 'plist'),
    '287',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necrotic Sliver'),
    (select id from sets where short_name = 'plist'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Anthem'),
    (select id from sets where short_name = 'plist'),
    '168',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temporal Manipulation'),
    (select id from sets where short_name = 'plist'),
    '80',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chemister''s Insight'),
    (select id from sets where short_name = 'plist'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cloudgoat Ranger'),
    (select id from sets where short_name = 'plist'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crystalline Sliver'),
    (select id from sets where short_name = 'plist'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boggart Arsonists'),
    (select id from sets where short_name = 'plist'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Patron Wizard'),
    (select id from sets where short_name = 'plist'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcum''s Astrolabe'),
    (select id from sets where short_name = 'plist'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grim Haruspex'),
    (select id from sets where short_name = 'plist'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voracious Dragon'),
    (select id from sets where short_name = 'plist'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Parallax Wave'),
    (select id from sets where short_name = 'plist'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Latchkey Faerie'),
    (select id from sets where short_name = 'plist'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oubliette'),
    (select id from sets where short_name = 'plist'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Land Tax'),
    (select id from sets where short_name = 'plist'),
    '23',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nahiri, the Lithomancer'),
    (select id from sets where short_name = 'plist'),
    '26',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sedge Sliver'),
    (select id from sets where short_name = 'plist'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rootrunner'),
    (select id from sets where short_name = 'plist'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aven Riftwatcher'),
    (select id from sets where short_name = 'plist'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tek'),
    (select id from sets where short_name = 'plist'),
    '273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Progenitus'),
    (select id from sets where short_name = 'plist'),
    '226',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thought Vessel'),
    (select id from sets where short_name = 'plist'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Triplicate Spirits'),
    (select id from sets where short_name = 'plist'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emeria Angel'),
    (select id from sets where short_name = 'plist'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Panharmonicon'),
    (select id from sets where short_name = 'plist'),
    '264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gorm the Great'),
    (select id from sets where short_name = 'plist'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Archdruid'),
    (select id from sets where short_name = 'plist'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Acorn Catapult'),
    (select id from sets where short_name = 'plist'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prismatic Geoscope'),
    (select id from sets where short_name = 'plist'),
    '266',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vodalian Illusionist'),
    (select id from sets where short_name = 'plist'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alpha Kavu'),
    (select id from sets where short_name = 'plist'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wizened Cenn'),
    (select id from sets where short_name = 'plist'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Retreat to Coralhelm'),
    (select id from sets where short_name = 'plist'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inventors'' Fair'),
    (select id from sets where short_name = 'plist'),
    '290',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pegasus Stampede'),
    (select id from sets where short_name = 'plist'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evermind'),
    (select id from sets where short_name = 'plist'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Displacer'),
    (select id from sets where short_name = 'plist'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grim Harvest'),
    (select id from sets where short_name = 'plist'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jaya''s Immolating Inferno'),
    (select id from sets where short_name = 'plist'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Platinum Angel'),
    (select id from sets where short_name = 'plist'),
    '265',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Promise of Power'),
    (select id from sets where short_name = 'plist'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lashweed Lurker'),
    (select id from sets where short_name = 'plist'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Command Beacon'),
    (select id from sets where short_name = 'plist'),
    '284',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fall of the Titans'),
    (select id from sets where short_name = 'plist'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Tyranny'),
    (select id from sets where short_name = 'plist'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampire Nocturnus'),
    (select id from sets where short_name = 'plist'),
    '119',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'plist'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Memnite'),
    (select id from sets where short_name = 'plist'),
    '262',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'plist'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blight Sickle'),
    (select id from sets where short_name = 'plist'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Training Grounds'),
    (select id from sets where short_name = 'plist'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beseech the Queen'),
    (select id from sets where short_name = 'plist'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moggcatcher'),
    (select id from sets where short_name = 'plist'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Body Snatcher'),
    (select id from sets where short_name = 'plist'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vitu-Ghazi Guildmage'),
    (select id from sets where short_name = 'plist'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brain Freeze'),
    (select id from sets where short_name = 'plist'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Helm of Kaldra'),
    (select id from sets where short_name = 'plist'),
    '258',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hammer of Purphoros'),
    (select id from sets where short_name = 'plist'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skred'),
    (select id from sets where short_name = 'plist'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soltari Monk'),
    (select id from sets where short_name = 'plist'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lorthos, the Tidemaker'),
    (select id from sets where short_name = 'plist'),
    '64',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hidetsugu''s Second Rite'),
    (select id from sets where short_name = 'plist'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Isochron Scepter'),
    (select id from sets where short_name = 'plist'),
    '259',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Squirrel Mob'),
    (select id from sets where short_name = 'plist'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Belfry Spirit'),
    (select id from sets where short_name = 'plist'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Door to Nothingness'),
    (select id from sets where short_name = 'plist'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gigantosaurus'),
    (select id from sets where short_name = 'plist'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enlightened Tutor'),
    (select id from sets where short_name = 'plist'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merfolk Mistbinder'),
    (select id from sets where short_name = 'plist'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kalastria Highborn'),
    (select id from sets where short_name = 'plist'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Etherium-Horn Sorcerer'),
    (select id from sets where short_name = 'plist'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lord of the Undead'),
    (select id from sets where short_name = 'plist'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Victimize'),
    (select id from sets where short_name = 'plist'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcbound Slith'),
    (select id from sets where short_name = 'plist'),
    '243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harmonize'),
    (select id from sets where short_name = 'plist'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thrumming Stone'),
    (select id from sets where short_name = 'plist'),
    '275',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Workhorse'),
    (select id from sets where short_name = 'plist'),
    '280',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Muldrotha, the Gravetide'),
    (select id from sets where short_name = 'plist'),
    '220',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shivan Meteor'),
    (select id from sets where short_name = 'plist'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fomori Nomad'),
    (select id from sets where short_name = 'plist'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Calming Licid'),
    (select id from sets where short_name = 'plist'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krark''s Thumb'),
    (select id from sets where short_name = 'plist'),
    '260',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Battle of Wits'),
    (select id from sets where short_name = 'plist'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa Revane'),
    (select id from sets where short_name = 'plist'),
    '185',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Electrostatic Pummeler'),
    (select id from sets where short_name = 'plist'),
    '252',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Doom Blade'),
    (select id from sets where short_name = 'plist'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Parallel Evolution'),
    (select id from sets where short_name = 'plist'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'History of Benalia'),
    (select id from sets where short_name = 'plist'),
    '20',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goldenglow Moth'),
    (select id from sets where short_name = 'plist'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anax, Hardened in the Forge'),
    (select id from sets where short_name = 'plist'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angrath, Captain of Chaos'),
    (select id from sets where short_name = 'plist'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eternal Dominion'),
    (select id from sets where short_name = 'plist'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vesuva'),
    (select id from sets where short_name = 'plist'),
    '298',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Endling'),
    (select id from sets where short_name = 'plist'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cryptic Command'),
    (select id from sets where short_name = 'plist'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Willow Dryad'),
    (select id from sets where short_name = 'plist'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doran, the Siege Tower'),
    (select id from sets where short_name = 'plist'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Tower'),
    (select id from sets where short_name = 'plist'),
    '294',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Scion'),
    (select id from sets where short_name = 'plist'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul of Zendikar'),
    (select id from sets where short_name = 'plist'),
    '193',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scourge of the Throne'),
    (select id from sets where short_name = 'plist'),
    '151',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sunscape Battlemage'),
    (select id from sets where short_name = 'plist'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rest in Peace'),
    (select id from sets where short_name = 'plist'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thought-Knot Seer'),
    (select id from sets where short_name = 'plist'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'plist'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Temple'),
    (select id from sets where short_name = 'plist'),
    '286',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dune-Brood Nephilim'),
    (select id from sets where short_name = 'plist'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Patron of the Akki'),
    (select id from sets where short_name = 'plist'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightshade Peddler'),
    (select id from sets where short_name = 'plist'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cradle Guard'),
    (select id from sets where short_name = 'plist'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keeper of the Nine Gales'),
    (select id from sets where short_name = 'plist'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mycosynth Golem'),
    (select id from sets where short_name = 'plist'),
    '263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Groundbreaker'),
    (select id from sets where short_name = 'plist'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enclave Cryptologist'),
    (select id from sets where short_name = 'plist'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Nexus'),
    (select id from sets where short_name = 'plist'),
    '218',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bojuka Bog'),
    (select id from sets where short_name = 'plist'),
    '282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fellwar Stone'),
    (select id from sets where short_name = 'plist'),
    '255',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brink of Madness'),
    (select id from sets where short_name = 'plist'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mischievous Quanar'),
    (select id from sets where short_name = 'plist'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kalonian Hydra'),
    (select id from sets where short_name = 'plist'),
    '180',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Drift of Phantasms'),
    (select id from sets where short_name = 'plist'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chulane, Teller of Tales'),
    (select id from sets where short_name = 'plist'),
    '203',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pteramander'),
    (select id from sets where short_name = 'plist'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kingpin''s Pet'),
    (select id from sets where short_name = 'plist'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iterative Analysis'),
    (select id from sets where short_name = 'plist'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'plist'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Doomwake Giant'),
    (select id from sets where short_name = 'plist'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reveillark'),
    (select id from sets where short_name = 'plist'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruination Guide'),
    (select id from sets where short_name = 'plist'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kilnmouth Dragon'),
    (select id from sets where short_name = 'plist'),
    '140',
    'rare'
) 
 on conflict do nothing;
