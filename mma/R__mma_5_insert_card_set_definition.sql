insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Mad Auntie'),
    (select id from sets where short_name = 'mma'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cryptic Command'),
    (select id from sets where short_name = 'mma'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Logic Knot'),
    (select id from sets where short_name = 'mma'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Veteran Armorer'),
    (select id from sets where short_name = 'mma'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eternal Witness'),
    (select id from sets where short_name = 'mma'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reach of Branches'),
    (select id from sets where short_name = 'mma'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lotus Bloom'),
    (select id from sets where short_name = 'mma'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verdeloth the Ancient'),
    (select id from sets where short_name = 'mma'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sudden Shock'),
    (select id from sets where short_name = 'mma'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reveillark'),
    (select id from sets where short_name = 'mma'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kokusho, the Evening Star'),
    (select id from sets where short_name = 'mma'),
    '89',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tribal Flames'),
    (select id from sets where short_name = 'mma'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desperate Ritual'),
    (select id from sets where short_name = 'mma'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tidehollow Sculler'),
    (select id from sets where short_name = 'mma'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marsh Flitter'),
    (select id from sets where short_name = 'mma'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Echoing Courage'),
    (select id from sets where short_name = 'mma'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kira, Great Glass-Spinner'),
    (select id from sets where short_name = 'mma'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woodfall Primus'),
    (select id from sets where short_name = 'mma'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tombstalker'),
    (select id from sets where short_name = 'mma'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreamspoiler Witches'),
    (select id from sets where short_name = 'mma'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Petals of Insight'),
    (select id from sets where short_name = 'mma'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctum Gargoyle'),
    (select id from sets where short_name = 'mma'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glimmervoid'),
    (select id from sets where short_name = 'mma'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stinkdrinker Daredevil'),
    (select id from sets where short_name = 'mma'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gleam of Resistance'),
    (select id from sets where short_name = 'mma'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Denied'),
    (select id from sets where short_name = 'mma'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dampen Thought'),
    (select id from sets where short_name = 'mma'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sarkhan Vol'),
    (select id from sets where short_name = 'mma'),
    '183',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mothdust Changeling'),
    (select id from sets where short_name = 'mma'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Torrent of Stone'),
    (select id from sets where short_name = 'mma'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paradise Mantle'),
    (select id from sets where short_name = 'mma'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hammerheim Deadeye'),
    (select id from sets where short_name = 'mma'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Otherworldly Journey'),
    (select id from sets where short_name = 'mma'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chalice of the Void'),
    (select id from sets where short_name = 'mma'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avian Changeling'),
    (select id from sets where short_name = 'mma'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Figure of Destiny'),
    (select id from sets where short_name = 'mma'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivid Crag'),
    (select id from sets where short_name = 'mma'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bridge from Below'),
    (select id from sets where short_name = 'mma'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonstorm'),
    (select id from sets where short_name = 'mma'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thieving Sprite'),
    (select id from sets where short_name = 'mma'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reach Through Mists'),
    (select id from sets where short_name = 'mma'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Executioner''s Capsule'),
    (select id from sets where short_name = 'mma'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warren Pilferers'),
    (select id from sets where short_name = 'mma'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greater Gargadon'),
    (select id from sets where short_name = 'mma'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shrapnel Blast'),
    (select id from sets where short_name = 'mma'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Amrou Seekers'),
    (select id from sets where short_name = 'mma'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stir the Pride'),
    (select id from sets where short_name = 'mma'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Bounty'),
    (select id from sets where short_name = 'mma'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Mechanist'),
    (select id from sets where short_name = 'mma'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stingscourger'),
    (select id from sets where short_name = 'mma'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Moon'),
    (select id from sets where short_name = 'mma'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel''s Grace'),
    (select id from sets where short_name = 'mma'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grand Arbiter Augustin IV'),
    (select id from sets where short_name = 'mma'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Citanul Woodreaders'),
    (select id from sets where short_name = 'mma'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Macabre'),
    (select id from sets where short_name = 'mma'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yosei, the Morning Star'),
    (select id from sets where short_name = 'mma'),
    '35',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cenn''s Enlistment'),
    (select id from sets where short_name = 'mma'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spellstutter Sprite'),
    (select id from sets where short_name = 'mma'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keiga, the Tide Star'),
    (select id from sets where short_name = 'mma'),
    '48',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Narcomoeba'),
    (select id from sets where short_name = 'mma'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Murderous Redcap'),
    (select id from sets where short_name = 'mma'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tooth and Nail'),
    (select id from sets where short_name = 'mma'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dispeller''s Capsule'),
    (select id from sets where short_name = 'mma'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Traumatic Visions'),
    (select id from sets where short_name = 'mma'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'mma'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Squee, Goblin Nabob'),
    (select id from sets where short_name = 'mma'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oona, Queen of the Fae'),
    (select id from sets where short_name = 'mma'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aether Spellbomb'),
    (select id from sets where short_name = 'mma'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blinkmoth Nexus'),
    (select id from sets where short_name = 'mma'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drag Down'),
    (select id from sets where short_name = 'mma'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skeletal Vampire'),
    (select id from sets where short_name = 'mma'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Confidant'),
    (select id from sets where short_name = 'mma'),
    '75',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thallid Germinator'),
    (select id from sets where short_name = 'mma'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kataki, War''s Wage'),
    (select id from sets where short_name = 'mma'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nantuko Shaman'),
    (select id from sets where short_name = 'mma'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'War-Spike Changeling'),
    (select id from sets where short_name = 'mma'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warren Weirding'),
    (select id from sets where short_name = 'mma'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Countryside Crusher'),
    (select id from sets where short_name = 'mma'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Amrou Scout'),
    (select id from sets where short_name = 'mma'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Enforcer'),
    (select id from sets where short_name = 'mma'),
    '209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Peppersmoke'),
    (select id from sets where short_name = 'mma'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Auntie''s Snitch'),
    (select id from sets where short_name = 'mma'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Pulse'),
    (select id from sets where short_name = 'mma'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Masked Admirers'),
    (select id from sets where short_name = 'mma'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Helix'),
    (select id from sets where short_name = 'mma'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glacial Ray'),
    (select id from sets where short_name = 'mma'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hana Kami'),
    (select id from sets where short_name = 'mma'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Funeral'),
    (select id from sets where short_name = 'mma'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tarmogoyf'),
    (select id from sets where short_name = 'mma'),
    '166',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pact of Negation'),
    (select id from sets where short_name = 'mma'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frogmite'),
    (select id from sets where short_name = 'mma'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyreach Manta'),
    (select id from sets where short_name = 'mma'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jhoira of the Ghitu'),
    (select id from sets where short_name = 'mma'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saltfield Recluse'),
    (select id from sets where short_name = 'mma'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Vial'),
    (select id from sets where short_name = 'mma'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Test of Faith'),
    (select id from sets where short_name = 'mma'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thallid'),
    (select id from sets where short_name = 'mma'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sandsower'),
    (select id from sets where short_name = 'mma'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cloudgoat Ranger'),
    (select id from sets where short_name = 'mma'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Errant Ephemeron'),
    (select id from sets where short_name = 'mma'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cold-Eyed Selkie'),
    (select id from sets where short_name = 'mma'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raven''s Crime'),
    (select id from sets where short_name = 'mma'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elspeth, Knight-Errant'),
    (select id from sets where short_name = 'mma'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Empty the Warrens'),
    (select id from sets where short_name = 'mma'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crush Underfoot'),
    (select id from sets where short_name = 'mma'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of the Reliquary'),
    (select id from sets where short_name = 'mma'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Worm Harvest'),
    (select id from sets where short_name = 'mma'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kithkin Greatheart'),
    (select id from sets where short_name = 'mma'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ryusei, the Falling Star'),
    (select id from sets where short_name = 'mma'),
    '128',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Riftsweeper'),
    (select id from sets where short_name = 'mma'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spell Snare'),
    (select id from sets where short_name = 'mma'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gifts Ungiven'),
    (select id from sets where short_name = 'mma'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rude Awakening'),
    (select id from sets where short_name = 'mma'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demigod of Revenge'),
    (select id from sets where short_name = 'mma'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Durkwood Baloth'),
    (select id from sets where short_name = 'mma'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erratic Mutation'),
    (select id from sets where short_name = 'mma'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doubling Season'),
    (select id from sets where short_name = 'mma'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sporesower Thallid'),
    (select id from sets where short_name = 'mma'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Etherium Sculptor'),
    (select id from sets where short_name = 'mma'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Engineered Explosives'),
    (select id from sets where short_name = 'mma'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thirst for Knowledge'),
    (select id from sets where short_name = 'mma'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blind-Spot Giant'),
    (select id from sets where short_name = 'mma'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ivory Giant'),
    (select id from sets where short_name = 'mma'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Absorb Vis'),
    (select id from sets where short_name = 'mma'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blightspeaker'),
    (select id from sets where short_name = 'mma'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trygon Predator'),
    (select id from sets where short_name = 'mma'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Echoing Truth'),
    (select id from sets where short_name = 'mma'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivid Creek'),
    (select id from sets where short_name = 'mma'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grapeshot'),
    (select id from sets where short_name = 'mma'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grinning Ignus'),
    (select id from sets where short_name = 'mma'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kitchen Finks'),
    (select id from sets where short_name = 'mma'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Festering Goblin'),
    (select id from sets where short_name = 'mma'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ethersworn Canonist'),
    (select id from sets where short_name = 'mma'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Molten Disaster'),
    (select id from sets where short_name = 'mma'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivid Grove'),
    (select id from sets where short_name = 'mma'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Latchkey Faerie'),
    (select id from sets where short_name = 'mma'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Search for Tomorrow'),
    (select id from sets where short_name = 'mma'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pallid Mycoderm'),
    (select id from sets where short_name = 'mma'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meloku the Clouded Mirror'),
    (select id from sets where short_name = 'mma'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Facevaulter'),
    (select id from sets where short_name = 'mma'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Rattle'),
    (select id from sets where short_name = 'mma'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scion of Oona'),
    (select id from sets where short_name = 'mma'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyrite Spellbomb'),
    (select id from sets where short_name = 'mma'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runed Stalactite'),
    (select id from sets where short_name = 'mma'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sporoloth Ancient'),
    (select id from sets where short_name = 'mma'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rathi Trapper'),
    (select id from sets where short_name = 'mma'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Etched Oracle'),
    (select id from sets where short_name = 'mma'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Take Possession'),
    (select id from sets where short_name = 'mma'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Progenitus'),
    (select id from sets where short_name = 'mma'),
    '182',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Academy Ruins'),
    (select id from sets where short_name = 'mma'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terashi''s Grasp'),
    (select id from sets where short_name = 'mma'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plumeveil'),
    (select id from sets where short_name = 'mma'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Peer Through Depths'),
    (select id from sets where short_name = 'mma'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Walker of the Grove'),
    (select id from sets where short_name = 'mma'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Syphon Life'),
    (select id from sets where short_name = 'mma'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Cloud'),
    (select id from sets where short_name = 'mma'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bonesplitter'),
    (select id from sets where short_name = 'mma'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'mma'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Adarkar Valkyrie'),
    (select id from sets where short_name = 'mma'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyromancer''s Swath'),
    (select id from sets where short_name = 'mma'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Life from the Loam'),
    (select id from sets where short_name = 'mma'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pestermite'),
    (select id from sets where short_name = 'mma'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aethersnipe'),
    (select id from sets where short_name = 'mma'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sword of Light and Shadow'),
    (select id from sets where short_name = 'mma'),
    '217',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Imperiosaur'),
    (select id from sets where short_name = 'mma'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Retriever'),
    (select id from sets where short_name = 'mma'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vedalken Dismisser'),
    (select id from sets where short_name = 'mma'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divinity of Pride'),
    (select id from sets where short_name = 'mma'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Path to Exile'),
    (select id from sets where short_name = 'mma'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcbound Ravager'),
    (select id from sets where short_name = 'mma'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Earwig Squad'),
    (select id from sets where short_name = 'mma'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relic of Progenitus'),
    (select id from sets where short_name = 'mma'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vivid Marsh'),
    (select id from sets where short_name = 'mma'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tromp the Domains'),
    (select id from sets where short_name = 'mma'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Careful Consideration'),
    (select id from sets where short_name = 'mma'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bound in Silence'),
    (select id from sets where short_name = 'mma'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Incremental Growth'),
    (select id from sets where short_name = 'mma'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phthisis'),
    (select id from sets where short_name = 'mma'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tar Pitcher'),
    (select id from sets where short_name = 'mma'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blinding Beam'),
    (select id from sets where short_name = 'mma'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Esperzoa'),
    (select id from sets where short_name = 'mma'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vendilion Clique'),
    (select id from sets where short_name = 'mma'),
    '70',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dakmor Salvage'),
    (select id from sets where short_name = 'mma'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flickerwisp'),
    (select id from sets where short_name = 'mma'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glen Elendra Archmage'),
    (select id from sets where short_name = 'mma'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Electrolyze'),
    (select id from sets where short_name = 'mma'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meadowboon'),
    (select id from sets where short_name = 'mma'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thundering Giant'),
    (select id from sets where short_name = 'mma'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moldervine Cloak'),
    (select id from sets where short_name = 'mma'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deepcavern Imp'),
    (select id from sets where short_name = 'mma'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brute Force'),
    (select id from sets where short_name = 'mma'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rift Bolt'),
    (select id from sets where short_name = 'mma'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rift Elemental'),
    (select id from sets where short_name = 'mma'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thundercloud Shaman'),
    (select id from sets where short_name = 'mma'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jugan, the Rising Star'),
    (select id from sets where short_name = 'mma'),
    '150',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Arcbound Stinger'),
    (select id from sets where short_name = 'mma'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riftwing Cloudskate'),
    (select id from sets where short_name = 'mma'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mogg War Marshal'),
    (select id from sets where short_name = 'mma'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slaughter Pact'),
    (select id from sets where short_name = 'mma'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Grip'),
    (select id from sets where short_name = 'mma'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sword of Fire and Ice'),
    (select id from sets where short_name = 'mma'),
    '216',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Court Homunculus'),
    (select id from sets where short_name = 'mma'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vedalken Shackles'),
    (select id from sets where short_name = 'mma'),
    '218',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lava Spike'),
    (select id from sets where short_name = 'mma'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hillcomber Giant'),
    (select id from sets where short_name = 'mma'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Penumbra Spider'),
    (select id from sets where short_name = 'mma'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Auriok Salvagers'),
    (select id from sets where short_name = 'mma'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Summoner''s Pact'),
    (select id from sets where short_name = 'mma'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stonehewer Giant'),
    (select id from sets where short_name = 'mma'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Street Wraith'),
    (select id from sets where short_name = 'mma'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivid Meadow'),
    (select id from sets where short_name = 'mma'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Epochrasite'),
    (select id from sets where short_name = 'mma'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Manamorphose'),
    (select id from sets where short_name = 'mma'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Horobi''s Whisper'),
    (select id from sets where short_name = 'mma'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mulldrifter'),
    (select id from sets where short_name = 'mma'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feudkiller''s Verdict'),
    (select id from sets where short_name = 'mma'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Perilous Research'),
    (select id from sets where short_name = 'mma'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thallid Shell-Dweller'),
    (select id from sets where short_name = 'mma'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stinkweed Imp'),
    (select id from sets where short_name = 'mma'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kiki-Jiki, Mirror Breaker'),
    (select id from sets where short_name = 'mma'),
    '120',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Giant Dustwasp'),
    (select id from sets where short_name = 'mma'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greater Mossdog'),
    (select id from sets where short_name = 'mma'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Extirpate'),
    (select id from sets where short_name = 'mma'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiery Fall'),
    (select id from sets where short_name = 'mma'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kodama''s Reach'),
    (select id from sets where short_name = 'mma'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pardic Dragon'),
    (select id from sets where short_name = 'mma'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcbound Wanderer'),
    (select id from sets where short_name = 'mma'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcbound Worker'),
    (select id from sets where short_name = 'mma'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fury Charm'),
    (select id from sets where short_name = 'mma'),
    '114',
    'common'
) 
 on conflict do nothing;
