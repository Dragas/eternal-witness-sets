insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Surgical Extraction'),
    (select id from sets where short_name = 'pnph'),
    '*74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sheoldred, Whispering One'),
    (select id from sets where short_name = 'pnph'),
    '73',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Metamorph'),
    (select id from sets where short_name = 'pnph'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Priest of Urabrask'),
    (select id from sets where short_name = 'pnph'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pristine Talisman'),
    (select id from sets where short_name = 'pnph'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Superion'),
    (select id from sets where short_name = 'pnph'),
    '146',
    'rare'
) 
 on conflict do nothing;
