    insert into mtgcard(name) values ('Surgical Extraction') on conflict do nothing;
    insert into mtgcard(name) values ('Sheoldred, Whispering One') on conflict do nothing;
    insert into mtgcard(name) values ('Phyrexian Metamorph') on conflict do nothing;
    insert into mtgcard(name) values ('Priest of Urabrask') on conflict do nothing;
    insert into mtgcard(name) values ('Pristine Talisman') on conflict do nothing;
    insert into mtgcard(name) values ('Myr Superion') on conflict do nothing;
