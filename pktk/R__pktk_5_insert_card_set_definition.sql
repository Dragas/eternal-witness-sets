insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Deflecting Palm'),
    (select id from sets where short_name = 'pktk'),
    '173s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Villainous Wealth'),
    (select id from sets where short_name = 'pktk'),
    '211s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sultai Charm'),
    (select id from sets where short_name = 'pktk'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hardened Scales'),
    (select id from sets where short_name = 'pktk'),
    '133p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Narset, Enlightened Master'),
    (select id from sets where short_name = 'pktk'),
    '190s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Avalanche Tusker'),
    (select id from sets where short_name = 'pktk'),
    '166s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Butcher of the Horde'),
    (select id from sets where short_name = 'pktk'),
    '168s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rattleclaw Mystic'),
    (select id from sets where short_name = 'pktk'),
    '144s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crackling Doom'),
    (select id from sets where short_name = 'pktk'),
    '171s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Master of Pearls'),
    (select id from sets where short_name = 'pktk'),
    '18s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dig Through Time'),
    (select id from sets where short_name = 'pktk'),
    '36s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clever Impersonator'),
    (select id from sets where short_name = 'pktk'),
    '34p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ankle Shanker'),
    (select id from sets where short_name = 'pktk'),
    '164s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temur Ascendancy'),
    (select id from sets where short_name = 'pktk'),
    '207s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ankle Shanker'),
    (select id from sets where short_name = 'pktk'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Throne of Tarkir'),
    (select id from sets where short_name = 'pktk'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon-Style Twins'),
    (select id from sets where short_name = 'pktk'),
    '108s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Herald of Anafenza'),
    (select id from sets where short_name = 'pktk'),
    '12s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trail of Mystery'),
    (select id from sets where short_name = 'pktk'),
    '154s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abzan Ascendancy'),
    (select id from sets where short_name = 'pktk'),
    '160s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ivorytusk Fortress'),
    (select id from sets where short_name = 'pktk'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakshasa Vizier'),
    (select id from sets where short_name = 'pktk'),
    '193s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necropolis Fiend'),
    (select id from sets where short_name = 'pktk'),
    '82s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thousand Winds'),
    (select id from sets where short_name = 'pktk'),
    '58s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grim Haruspex'),
    (select id from sets where short_name = 'pktk'),
    '73s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temur Ascendancy'),
    (select id from sets where short_name = 'pktk'),
    '207p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kheru Lich Lord'),
    (select id from sets where short_name = 'pktk'),
    '182s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flying Crane Technique'),
    (select id from sets where short_name = 'pktk'),
    '176s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sultai Ascendancy'),
    (select id from sets where short_name = 'pktk'),
    '203s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duneblast'),
    (select id from sets where short_name = 'pktk'),
    '174s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sidisi, Brood Tyrant'),
    (select id from sets where short_name = 'pktk'),
    '199s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Surrak Dragonclaw'),
    (select id from sets where short_name = 'pktk'),
    '206s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jeskai Ascendancy'),
    (select id from sets where short_name = 'pktk'),
    '180s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Icy Blast'),
    (select id from sets where short_name = 'pktk'),
    '42s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anafenza, the Foremost'),
    (select id from sets where short_name = 'pktk'),
    '163s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'High Sentinels of Arashin'),
    (select id from sets where short_name = 'pktk'),
    '13s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Utter End'),
    (select id from sets where short_name = 'pktk'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dig Through Time'),
    (select id from sets where short_name = 'pktk'),
    '36p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mardu Ascendancy'),
    (select id from sets where short_name = 'pktk'),
    '185s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avalanche Tusker'),
    (select id from sets where short_name = 'pktk'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Utter End'),
    (select id from sets where short_name = 'pktk'),
    '210s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trap Essence'),
    (select id from sets where short_name = 'pktk'),
    '209s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Utter End'),
    (select id from sets where short_name = 'pktk'),
    '210p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ivorytusk Fortress'),
    (select id from sets where short_name = 'pktk'),
    '179s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rattleclaw Mystic'),
    (select id from sets where short_name = 'pktk'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakshasa Vizier'),
    (select id from sets where short_name = 'pktk'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zurgo Helmsmasher'),
    (select id from sets where short_name = 'pktk'),
    '214s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Villainous Wealth'),
    (select id from sets where short_name = 'pktk'),
    '211p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hardened Scales'),
    (select id from sets where short_name = 'pktk'),
    '133s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heir of the Wilds'),
    (select id from sets where short_name = 'pktk'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sage of the Inward Eye'),
    (select id from sets where short_name = 'pktk'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodsoaked Champion'),
    (select id from sets where short_name = 'pktk'),
    '66s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crater''s Claws'),
    (select id from sets where short_name = 'pktk'),
    '106s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sage of the Inward Eye'),
    (select id from sets where short_name = 'pktk'),
    '195s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jeering Instigator'),
    (select id from sets where short_name = 'pktk'),
    '113s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Siege Rhino'),
    (select id from sets where short_name = 'pktk'),
    '200s',
    'rare'
) 
 on conflict do nothing;
