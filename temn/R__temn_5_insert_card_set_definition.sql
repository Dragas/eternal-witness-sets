insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'temn'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Horror'),
    (select id from sets where short_name = 'temn'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana, the Last Hope Emblem'),
    (select id from sets where short_name = 'temn'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'temn'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'temn'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tamiyo, Field Researcher Emblem'),
    (select id from sets where short_name = 'temn'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldritch Moon Checklist'),
    (select id from sets where short_name = 'temn'),
    'CH1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spider'),
    (select id from sets where short_name = 'temn'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Human'),
    (select id from sets where short_name = 'temn'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Human Wizard'),
    (select id from sets where short_name = 'temn'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'temn'),
    '6',
    'common'
) 
 on conflict do nothing;
