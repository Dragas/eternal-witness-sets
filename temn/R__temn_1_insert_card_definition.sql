    insert into mtgcard(name) values ('Zombie') on conflict do nothing;
    insert into mtgcard(name) values ('Eldrazi Horror') on conflict do nothing;
    insert into mtgcard(name) values ('Liliana, the Last Hope Emblem') on conflict do nothing;
    insert into mtgcard(name) values ('Tamiyo, Field Researcher Emblem') on conflict do nothing;
    insert into mtgcard(name) values ('Eldritch Moon Checklist') on conflict do nothing;
    insert into mtgcard(name) values ('Spider') on conflict do nothing;
    insert into mtgcard(name) values ('Human') on conflict do nothing;
    insert into mtgcard(name) values ('Human Wizard') on conflict do nothing;
