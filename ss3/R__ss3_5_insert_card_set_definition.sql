insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Rite of Flame'),
    (select id from sets where short_name = 'ss3'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Young Pyromancer'),
    (select id from sets where short_name = 'ss3'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiery Confluence'),
    (select id from sets where short_name = 'ss3'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cathartic Reunion'),
    (select id from sets where short_name = 'ss3'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyroblast'),
    (select id from sets where short_name = 'ss3'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyromancer Ascension'),
    (select id from sets where short_name = 'ss3'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra, Torch of Defiance'),
    (select id from sets where short_name = 'ss3'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Past in Flames'),
    (select id from sets where short_name = 'ss3'),
    '4',
    'mythic'
) 
 on conflict do nothing;
