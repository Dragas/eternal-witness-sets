    insert into mtgcard(name) values ('Rite of Flame') on conflict do nothing;
    insert into mtgcard(name) values ('Young Pyromancer') on conflict do nothing;
    insert into mtgcard(name) values ('Fiery Confluence') on conflict do nothing;
    insert into mtgcard(name) values ('Cathartic Reunion') on conflict do nothing;
    insert into mtgcard(name) values ('Pyroblast') on conflict do nothing;
    insert into mtgcard(name) values ('Pyromancer Ascension') on conflict do nothing;
    insert into mtgcard(name) values ('Chandra, Torch of Defiance') on conflict do nothing;
    insert into mtgcard(name) values ('Past in Flames') on conflict do nothing;
