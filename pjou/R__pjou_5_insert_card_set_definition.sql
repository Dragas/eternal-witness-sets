insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Scourge of Fleets'),
    (select id from sets where short_name = 'pjou'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spawn of Thraxes'),
    (select id from sets where short_name = 'pjou'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Doomwake Giant'),
    (select id from sets where short_name = 'pjou'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dawnbringer Charioteers'),
    (select id from sets where short_name = 'pjou'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Squelching Leeches'),
    (select id from sets where short_name = 'pjou'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eidolon of Blossoms'),
    (select id from sets where short_name = 'pjou'),
    '*122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dictate of the Twin Gods'),
    (select id from sets where short_name = 'pjou'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dictate of Kruphix'),
    (select id from sets where short_name = 'pjou'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heroes'' Bane'),
    (select id from sets where short_name = 'pjou'),
    '126',
    'rare'
) 
 on conflict do nothing;
