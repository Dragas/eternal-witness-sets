    insert into mtgcard(name) values ('Scourge of Fleets') on conflict do nothing;
    insert into mtgcard(name) values ('Spawn of Thraxes') on conflict do nothing;
    insert into mtgcard(name) values ('Doomwake Giant') on conflict do nothing;
    insert into mtgcard(name) values ('Dawnbringer Charioteers') on conflict do nothing;
    insert into mtgcard(name) values ('Squelching Leeches') on conflict do nothing;
    insert into mtgcard(name) values ('Eidolon of Blossoms') on conflict do nothing;
    insert into mtgcard(name) values ('Dictate of the Twin Gods') on conflict do nothing;
    insert into mtgcard(name) values ('Dictate of Kruphix') on conflict do nothing;
    insert into mtgcard(name) values ('Heroes'' Bane') on conflict do nothing;
