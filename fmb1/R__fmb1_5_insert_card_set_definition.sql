insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Triskelion'),
    (select id from sets where short_name = 'fmb1'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blighted Agent'),
    (select id from sets where short_name = 'fmb1'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sarkhan the Mad'),
    (select id from sets where short_name = 'fmb1'),
    '90',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Karrthus, Tyrant of Jund'),
    (select id from sets where short_name = 'fmb1'),
    '87',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Greater Mossdog'),
    (select id from sets where short_name = 'fmb1'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Shards'),
    (select id from sets where short_name = 'fmb1'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Paradox Haze'),
    (select id from sets where short_name = 'fmb1'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Bushwhacker'),
    (select id from sets where short_name = 'fmb1'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manaweft Sliver'),
    (select id from sets where short_name = 'fmb1'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Council Guardian'),
    (select id from sets where short_name = 'fmb1'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lapse of Certainty'),
    (select id from sets where short_name = 'fmb1'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Puzzle Box'),
    (select id from sets where short_name = 'fmb1'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Isamaru, Hound of Konda'),
    (select id from sets where short_name = 'fmb1'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magewright''s Stone'),
    (select id from sets where short_name = 'fmb1'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sheltering Ancient'),
    (select id from sets where short_name = 'fmb1'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rune-Tail, Kitsune Ascendant // Rune-Tail''s Essence'),
    (select id from sets where short_name = 'fmb1'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirrodin''s Core'),
    (select id from sets where short_name = 'fmb1'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ravenous Trap'),
    (select id from sets where short_name = 'fmb1'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Echoing Decay'),
    (select id from sets where short_name = 'fmb1'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viscera Seer'),
    (select id from sets where short_name = 'fmb1'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Celestial Kirin'),
    (select id from sets where short_name = 'fmb1'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Allosaurus Rider'),
    (select id from sets where short_name = 'fmb1'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yore-Tiller Nephilim'),
    (select id from sets where short_name = 'fmb1'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm Crow'),
    (select id from sets where short_name = 'fmb1'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lantern of Insight'),
    (select id from sets where short_name = 'fmb1'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bramblewood Paragon'),
    (select id from sets where short_name = 'fmb1'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Misthollow Griffin'),
    (select id from sets where short_name = 'fmb1'),
    '26',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chimney Imp'),
    (select id from sets where short_name = 'fmb1'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maro'),
    (select id from sets where short_name = 'fmb1'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fatespinner'),
    (select id from sets where short_name = 'fmb1'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lich''s Mirror'),
    (select id from sets where short_name = 'fmb1'),
    '106',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'White Knight'),
    (select id from sets where short_name = 'fmb1'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fungusaur'),
    (select id from sets where short_name = 'fmb1'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marrow-Gnawer'),
    (select id from sets where short_name = 'fmb1'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Scout'),
    (select id from sets where short_name = 'fmb1'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glittering Wish'),
    (select id from sets where short_name = 'fmb1'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Norin the Wary'),
    (select id from sets where short_name = 'fmb1'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scourge of the Throne'),
    (select id from sets where short_name = 'fmb1'),
    '58',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Leveler'),
    (select id from sets where short_name = 'fmb1'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bringer of the Black Dawn'),
    (select id from sets where short_name = 'fmb1'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balduvian Rage'),
    (select id from sets where short_name = 'fmb1'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burning Inquiry'),
    (select id from sets where short_name = 'fmb1'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stigma Lasher'),
    (select id from sets where short_name = 'fmb1'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pull from Eternity'),
    (select id from sets where short_name = 'fmb1'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reki, the History of Kamigawa'),
    (select id from sets where short_name = 'fmb1'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alchemist''s Refuge'),
    (select id from sets where short_name = 'fmb1'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sen Triplets'),
    (select id from sets where short_name = 'fmb1'),
    '91',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sinew Sliver'),
    (select id from sets where short_name = 'fmb1'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fiery Gambit'),
    (select id from sets where short_name = 'fmb1'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Norn''s Annex'),
    (select id from sets where short_name = 'fmb1'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eidolon of Rhetoric'),
    (select id from sets where short_name = 'fmb1'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Storm'),
    (select id from sets where short_name = 'fmb1'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Codex Shredder'),
    (select id from sets where short_name = 'fmb1'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gleeful Sabotage'),
    (select id from sets where short_name = 'fmb1'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Memnite'),
    (select id from sets where short_name = 'fmb1'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Iron Myr'),
    (select id from sets where short_name = 'fmb1'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spike Feeder'),
    (select id from sets where short_name = 'fmb1'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul''s Attendant'),
    (select id from sets where short_name = 'fmb1'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balefire Liege'),
    (select id from sets where short_name = 'fmb1'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treasonous Ogre'),
    (select id from sets where short_name = 'fmb1'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flamekin Harbinger'),
    (select id from sets where short_name = 'fmb1'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nezumi Shortfang // Stabwhisker the Odious'),
    (select id from sets where short_name = 'fmb1'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Funeral Charm'),
    (select id from sets where short_name = 'fmb1'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Geth''s Grimoire'),
    (select id from sets where short_name = 'fmb1'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Minamo, School at Water''s Edge'),
    (select id from sets where short_name = 'fmb1'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wear // Tear'),
    (select id from sets where short_name = 'fmb1'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Nexus'),
    (select id from sets where short_name = 'fmb1'),
    '88',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Braid of Fire'),
    (select id from sets where short_name = 'fmb1'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Proclamation of Rebirth'),
    (select id from sets where short_name = 'fmb1'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scryb Ranger'),
    (select id from sets where short_name = 'fmb1'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reaper King'),
    (select id from sets where short_name = 'fmb1'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boundless Realms'),
    (select id from sets where short_name = 'fmb1'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Intruder Alarm'),
    (select id from sets where short_name = 'fmb1'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hornet Sting'),
    (select id from sets where short_name = 'fmb1'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frozen Aether'),
    (select id from sets where short_name = 'fmb1'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grand Architect'),
    (select id from sets where short_name = 'fmb1'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lumithread Field'),
    (select id from sets where short_name = 'fmb1'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyretic Ritual'),
    (select id from sets where short_name = 'fmb1'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zur''s Weirding'),
    (select id from sets where short_name = 'fmb1'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rescue from the Underworld'),
    (select id from sets where short_name = 'fmb1'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Form of the Dragon'),
    (select id from sets where short_name = 'fmb1'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rhox'),
    (select id from sets where short_name = 'fmb1'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pili-Pala'),
    (select id from sets where short_name = 'fmb1'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myojin of Life''s Web'),
    (select id from sets where short_name = 'fmb1'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gilder Bairn'),
    (select id from sets where short_name = 'fmb1'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Funeral'),
    (select id from sets where short_name = 'fmb1'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Springjack Shepherd'),
    (select id from sets where short_name = 'fmb1'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Celestial Dawn'),
    (select id from sets where short_name = 'fmb1'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Patron of the Moon'),
    (select id from sets where short_name = 'fmb1'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harmonic Sliver'),
    (select id from sets where short_name = 'fmb1'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sosuke, Son of Seshiro'),
    (select id from sets where short_name = 'fmb1'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mindslaver'),
    (select id from sets where short_name = 'fmb1'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Panglacial Wurm'),
    (select id from sets where short_name = 'fmb1'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stalking Stones'),
    (select id from sets where short_name = 'fmb1'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aurelia''s Fury'),
    (select id from sets where short_name = 'fmb1'),
    '83',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Guerrilla Tactics'),
    (select id from sets where short_name = 'fmb1'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Conspiracy'),
    (select id from sets where short_name = 'fmb1'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Helix Pinnacle'),
    (select id from sets where short_name = 'fmb1'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Witchbane Orb'),
    (select id from sets where short_name = 'fmb1'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Amulet of Vigor'),
    (select id from sets where short_name = 'fmb1'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Changeling Hero'),
    (select id from sets where short_name = 'fmb1'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'One with Nothing'),
    (select id from sets where short_name = 'fmb1'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sundial of the Infinite'),
    (select id from sets where short_name = 'fmb1'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drogskol Captain'),
    (select id from sets where short_name = 'fmb1'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Delay'),
    (select id from sets where short_name = 'fmb1'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shizo, Death''s Storehouse'),
    (select id from sets where short_name = 'fmb1'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spellweaver Volute'),
    (select id from sets where short_name = 'fmb1'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knowledge Pool'),
    (select id from sets where short_name = 'fmb1'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Herald of Leshrac'),
    (select id from sets where short_name = 'fmb1'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kulrath Knight'),
    (select id from sets where short_name = 'fmb1'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Puca''s Mischief'),
    (select id from sets where short_name = 'fmb1'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spelltithe Enforcer'),
    (select id from sets where short_name = 'fmb1'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trailblazer''s Boots'),
    (select id from sets where short_name = 'fmb1'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blasting Station'),
    (select id from sets where short_name = 'fmb1'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Game-Trail Changeling'),
    (select id from sets where short_name = 'fmb1'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ogre Gatecrasher'),
    (select id from sets where short_name = 'fmb1'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boreal Druid'),
    (select id from sets where short_name = 'fmb1'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undead Warchief'),
    (select id from sets where short_name = 'fmb1'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Not of This World'),
    (select id from sets where short_name = 'fmb1'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Noggle Bandit'),
    (select id from sets where short_name = 'fmb1'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archetype of Endurance'),
    (select id from sets where short_name = 'fmb1'),
    '62',
    'uncommon'
) 
 on conflict do nothing;
