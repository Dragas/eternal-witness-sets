insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'River''s Favor'),
    (select id from sets where short_name = 'ana'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ana'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ana'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Reward'),
    (select id from sets where short_name = 'ana'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zephyr Gull'),
    (select id from sets where short_name = 'ana'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ana'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raging Goblin'),
    (select id from sets where short_name = 'ana'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ana'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight''s Pledge'),
    (select id from sets where short_name = 'ana'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ana'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ana'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inspiring Commander'),
    (select id from sets where short_name = 'ana'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spiritual Guardian'),
    (select id from sets where short_name = 'ana'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shrine Keeper'),
    (select id from sets where short_name = 'ana'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ana'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ana'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treetop Warden'),
    (select id from sets where short_name = 'ana'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ana'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ana'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ana'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feral Roar'),
    (select id from sets where short_name = 'ana'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctuary Cat'),
    (select id from sets where short_name = 'ana'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tactical Advantage'),
    (select id from sets where short_name = 'ana'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ana'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shorecomber Crab'),
    (select id from sets where short_name = 'ana'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'ana'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rise from the Grave'),
    (select id from sets where short_name = 'ana'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ana'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ana'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ana'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Confront the Assault'),
    (select id from sets where short_name = 'ana'),
    '3',
    'uncommon'
) 
 on conflict do nothing;
