insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Stormscale Anarch'),
    (select id from sets where short_name = 'dis'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestial Ancient'),
    (select id from sets where short_name = 'dis'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Novijen Sages'),
    (select id from sets where short_name = 'dis'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silkwing Scout'),
    (select id from sets where short_name = 'dis'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stoic Ephemera'),
    (select id from sets where short_name = 'dis'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avatar of Discord'),
    (select id from sets where short_name = 'dis'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gnat Alley Creeper'),
    (select id from sets where short_name = 'dis'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Twinstrike'),
    (select id from sets where short_name = 'dis'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Guildmage'),
    (select id from sets where short_name = 'dis'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Utvara Scalper'),
    (select id from sets where short_name = 'dis'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bronze Bombshell'),
    (select id from sets where short_name = 'dis'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Muse Vessel'),
    (select id from sets where short_name = 'dis'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrecking Ball'),
    (select id from sets where short_name = 'dis'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Omnibian'),
    (select id from sets where short_name = 'dis'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Assault Zeppelid'),
    (select id from sets where short_name = 'dis'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakdos Signet'),
    (select id from sets where short_name = 'dis'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cytospawn Shambler'),
    (select id from sets where short_name = 'dis'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carom'),
    (select id from sets where short_name = 'dis'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sporeback Troll'),
    (select id from sets where short_name = 'dis'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lyzolda, the Blood Witch'),
    (select id from sets where short_name = 'dis'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azorius Herald'),
    (select id from sets where short_name = 'dis'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Street Savvy'),
    (select id from sets where short_name = 'dis'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistral Charger'),
    (select id from sets where short_name = 'dis'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ignorant Bliss'),
    (select id from sets where short_name = 'dis'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Squealing Devil'),
    (select id from sets where short_name = 'dis'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Freewind Equenaut'),
    (select id from sets where short_name = 'dis'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skullmead Cauldron'),
    (select id from sets where short_name = 'dis'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wakestone Gargoyle'),
    (select id from sets where short_name = 'dis'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Taste for Mayhem'),
    (select id from sets where short_name = 'dis'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overrule'),
    (select id from sets where short_name = 'dis'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Govern the Guildless'),
    (select id from sets where short_name = 'dis'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pillar of the Paruns'),
    (select id from sets where short_name = 'dis'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Momir Vig, Simic Visionary'),
    (select id from sets where short_name = 'dis'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prahv, Spires of Order'),
    (select id from sets where short_name = 'dis'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ragamuffyn'),
    (select id from sets where short_name = 'dis'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crime // Punishment'),
    (select id from sets where short_name = 'dis'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Biomantic Mastery'),
    (select id from sets where short_name = 'dis'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Carnarium'),
    (select id from sets where short_name = 'dis'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Valor Made Real'),
    (select id from sets where short_name = 'dis'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spell Snare'),
    (select id from sets where short_name = 'dis'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Delirium Skeins'),
    (select id from sets where short_name = 'dis'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Protean Hulk'),
    (select id from sets where short_name = 'dis'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enemy of the Guildpact'),
    (select id from sets where short_name = 'dis'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simic Growth Chamber'),
    (select id from sets where short_name = 'dis'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tidespout Tyrant'),
    (select id from sets where short_name = 'dis'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Transguild Courier'),
    (select id from sets where short_name = 'dis'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drekavac'),
    (select id from sets where short_name = 'dis'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Court Hussar'),
    (select id from sets where short_name = 'dis'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sandstorm Eidolon'),
    (select id from sets where short_name = 'dis'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kindle the Carnage'),
    (select id from sets where short_name = 'dis'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Odds // Ends'),
    (select id from sets where short_name = 'dis'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evolution Vat'),
    (select id from sets where short_name = 'dis'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Psychotic Fury'),
    (select id from sets where short_name = 'dis'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steeling Stance'),
    (select id from sets where short_name = 'dis'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Utopia Sprawl'),
    (select id from sets where short_name = 'dis'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aurora Eidolon'),
    (select id from sets where short_name = 'dis'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trial // Error'),
    (select id from sets where short_name = 'dis'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coiling Oracle'),
    (select id from sets where short_name = 'dis'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightcreep'),
    (select id from sets where short_name = 'dis'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Condemn'),
    (select id from sets where short_name = 'dis'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Entropic Eidolon'),
    (select id from sets where short_name = 'dis'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aquastrand Spider'),
    (select id from sets where short_name = 'dis'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simic Basilisk'),
    (select id from sets where short_name = 'dis'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos the Defiler'),
    (select id from sets where short_name = 'dis'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slithering Shade'),
    (select id from sets where short_name = 'dis'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Research // Development'),
    (select id from sets where short_name = 'dis'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aethermage''s Touch'),
    (select id from sets where short_name = 'dis'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vigean Hydropon'),
    (select id from sets where short_name = 'dis'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riot Spikes'),
    (select id from sets where short_name = 'dis'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plaxcaster Frogling'),
    (select id from sets where short_name = 'dis'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Simic Initiate'),
    (select id from sets where short_name = 'dis'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soulsworn Jury'),
    (select id from sets where short_name = 'dis'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vesper Ghoul'),
    (select id from sets where short_name = 'dis'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blessing of the Nephilim'),
    (select id from sets where short_name = 'dis'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wit''s End'),
    (select id from sets where short_name = 'dis'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unliving Psychopath'),
    (select id from sets where short_name = 'dis'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nettling Curse'),
    (select id from sets where short_name = 'dis'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sprouting Phytohydra'),
    (select id from sets where short_name = 'dis'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infernal Tutor'),
    (select id from sets where short_name = 'dis'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azorius Aethermage'),
    (select id from sets where short_name = 'dis'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ocular Halo'),
    (select id from sets where short_name = 'dis'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hellhole Rats'),
    (select id from sets where short_name = 'dis'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cytoshape'),
    (select id from sets where short_name = 'dis'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azorius Ploy'),
    (select id from sets where short_name = 'dis'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Helium Squirter'),
    (select id from sets where short_name = 'dis'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Patagia Viper'),
    (select id from sets where short_name = 'dis'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cytoplast Root-Kin'),
    (select id from sets where short_name = 'dis'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vigean Graftmage'),
    (select id from sets where short_name = 'dis'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Riteknife'),
    (select id from sets where short_name = 'dis'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verdant Eidolon'),
    (select id from sets where short_name = 'dis'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enigma Eidolon'),
    (select id from sets where short_name = 'dis'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azorius Guildmage'),
    (select id from sets where short_name = 'dis'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seal of Fire'),
    (select id from sets where short_name = 'dis'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bond of Agony'),
    (select id from sets where short_name = 'dis'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pain Magnification'),
    (select id from sets where short_name = 'dis'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Haazda Exonerator'),
    (select id from sets where short_name = 'dis'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghost Quarter'),
    (select id from sets where short_name = 'dis'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Haazda Shield Mate'),
    (select id from sets where short_name = 'dis'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plaxmanta'),
    (select id from sets where short_name = 'dis'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thrive'),
    (select id from sets where short_name = 'dis'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brace for Impact'),
    (select id from sets where short_name = 'dis'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vision Skeins'),
    (select id from sets where short_name = 'dis'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyscribing'),
    (select id from sets where short_name = 'dis'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Simic Signet'),
    (select id from sets where short_name = 'dis'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon''s Jester'),
    (select id from sets where short_name = 'dis'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windreaver'),
    (select id from sets where short_name = 'dis'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Minister of Impediments'),
    (select id from sets where short_name = 'dis'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voidslime'),
    (select id from sets where short_name = 'dis'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elemental Resonance'),
    (select id from sets where short_name = 'dis'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Paladin of Prahv'),
    (select id from sets where short_name = 'dis'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ratcatcher'),
    (select id from sets where short_name = 'dis'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sky Hussar'),
    (select id from sets where short_name = 'dis'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bound // Determined'),
    (select id from sets where short_name = 'dis'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rain of Gore'),
    (select id from sets where short_name = 'dis'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swift Silence'),
    (select id from sets where short_name = 'dis'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grand Arbiter Augustin IV'),
    (select id from sets where short_name = 'dis'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cackling Flames'),
    (select id from sets where short_name = 'dis'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rise // Fall'),
    (select id from sets where short_name = 'dis'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seal of Doom'),
    (select id from sets where short_name = 'dis'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shielding Plax'),
    (select id from sets where short_name = 'dis'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leafdrake Roost'),
    (select id from sets where short_name = 'dis'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Writ of Passage'),
    (select id from sets where short_name = 'dis'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hide // Seek'),
    (select id from sets where short_name = 'dis'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gobhobbler Rats'),
    (select id from sets where short_name = 'dis'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Proper Burial'),
    (select id from sets where short_name = 'dis'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonfire'),
    (select id from sets where short_name = 'dis'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Isperia the Inscrutable'),
    (select id from sets where short_name = 'dis'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stalking Vengeance'),
    (select id from sets where short_name = 'dis'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kill-Suit Cultist'),
    (select id from sets where short_name = 'dis'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pure // Simple'),
    (select id from sets where short_name = 'dis'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hit // Run'),
    (select id from sets where short_name = 'dis'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Augermage'),
    (select id from sets where short_name = 'dis'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azorius First-Wing'),
    (select id from sets where short_name = 'dis'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dovescape'),
    (select id from sets where short_name = 'dis'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slaughterhouse Bouncer'),
    (select id from sets where short_name = 'dis'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flash Foliage'),
    (select id from sets where short_name = 'dis'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Breeding Pool'),
    (select id from sets where short_name = 'dis'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beacon Hawk'),
    (select id from sets where short_name = 'dis'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vigean Intuition'),
    (select id from sets where short_name = 'dis'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stomp and Howl'),
    (select id from sets where short_name = 'dis'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Ickspitter'),
    (select id from sets where short_name = 'dis'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rix Maadi, Dungeon Palace'),
    (select id from sets where short_name = 'dis'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hallowed Fountain'),
    (select id from sets where short_name = 'dis'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Walking Archive'),
    (select id from sets where short_name = 'dis'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flame-Kin War Scout'),
    (select id from sets where short_name = 'dis'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guardian of the Guildpact'),
    (select id from sets where short_name = 'dis'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Might of the Nephilim'),
    (select id from sets where short_name = 'dis'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brain Pry'),
    (select id from sets where short_name = 'dis'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Experiment Kraj'),
    (select id from sets where short_name = 'dis'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Proclamation of Rebirth'),
    (select id from sets where short_name = 'dis'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whiptail Moloch'),
    (select id from sets where short_name = 'dis'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Palliation Accord'),
    (select id from sets where short_name = 'dis'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cytoplast Manipulator'),
    (select id from sets where short_name = 'dis'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magewright''s Stone'),
    (select id from sets where short_name = 'dis'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Supply // Demand'),
    (select id from sets where short_name = 'dis'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weight of Spires'),
    (select id from sets where short_name = 'dis'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Simic Guildmage'),
    (select id from sets where short_name = 'dis'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azorius Signet'),
    (select id from sets where short_name = 'dis'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flaring Flame-Kin'),
    (select id from sets where short_name = 'dis'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Macabre Waltz'),
    (select id from sets where short_name = 'dis'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'War''s Toll'),
    (select id from sets where short_name = 'dis'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loaming Shaman'),
    (select id from sets where short_name = 'dis'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dread Slag'),
    (select id from sets where short_name = 'dis'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Pit Dragon'),
    (select id from sets where short_name = 'dis'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azorius Chancery'),
    (select id from sets where short_name = 'dis'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fertile Imagination'),
    (select id from sets where short_name = 'dis'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jagged Poppet'),
    (select id from sets where short_name = 'dis'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Psychic Possession'),
    (select id from sets where short_name = 'dis'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anthem of Rakdos'),
    (select id from sets where short_name = 'dis'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pride of the Clouds'),
    (select id from sets where short_name = 'dis'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crypt Champion'),
    (select id from sets where short_name = 'dis'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Novijen, Heart of Progress'),
    (select id from sets where short_name = 'dis'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plumes of Peace'),
    (select id from sets where short_name = 'dis'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Crypt'),
    (select id from sets where short_name = 'dis'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trygon Predator'),
    (select id from sets where short_name = 'dis'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nihilistic Glee'),
    (select id from sets where short_name = 'dis'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Sky Swallower'),
    (select id from sets where short_name = 'dis'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Ragworm'),
    (select id from sets where short_name = 'dis'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Indrik Stomphowler'),
    (select id from sets where short_name = 'dis'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ogre Gatecrasher'),
    (select id from sets where short_name = 'dis'),
    '67',
    'common'
) 
 on conflict do nothing;
