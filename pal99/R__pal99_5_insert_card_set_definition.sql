insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Karn, Silver Golem'),
    (select id from sets where short_name = 'pal99'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pal99'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skittering Skirge'),
    (select id from sets where short_name = 'pal99'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pal99'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pal99'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pal99'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pal99'),
    '3†',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rewind'),
    (select id from sets where short_name = 'pal99'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pal99'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pouncing Jaguar'),
    (select id from sets where short_name = 'pal99'),
    '2',
    'rare'
) 
 on conflict do nothing;
