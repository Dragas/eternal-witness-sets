insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Nerf War'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dragon'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dragon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dragon'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sword of Dungeons & Dragons'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sword of Dungeons & Dragons'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grimlock, Dinobot Leader // Grimlock, Ferocious King'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grimlock, Dinobot Leader // Grimlock, Ferocious King'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grimlock, Dinobot Leader // Grimlock, Ferocious King'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grimlock, Dinobot Leader // Grimlock, Ferocious King'),
        (select types.id from types where name = 'Autobot')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grimlock, Dinobot Leader // Grimlock, Ferocious King'),
        (select types.id from types where name = '//')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grimlock, Dinobot Leader // Grimlock, Ferocious King'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grimlock, Dinobot Leader // Grimlock, Ferocious King'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grimlock, Dinobot Leader // Grimlock, Ferocious King'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grimlock, Dinobot Leader // Grimlock, Ferocious King'),
        (select types.id from types where name = 'Dinosaur')
    ) 
 on conflict do nothing;
