insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Nerf War'),
    (select id from sets where short_name = 'h17'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'h17'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sword of Dungeons & Dragons'),
    (select id from sets where short_name = 'h17'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Grimlock, Dinobot Leader // Grimlock, Ferocious King'),
    (select id from sets where short_name = 'h17'),
    '1',
    'mythic'
) 
 on conflict do nothing;
