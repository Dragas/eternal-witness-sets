insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Centaur Courser'),
    (select id from sets where short_name = 'm10'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Xathrid Demon'),
    (select id from sets where short_name = 'm10'),
    '122',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wall of Faith'),
    (select id from sets where short_name = 'm10'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm10'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirror of Fate'),
    (select id from sets where short_name = 'm10'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lava Axe'),
    (select id from sets where short_name = 'm10'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jackal Familiar'),
    (select id from sets where short_name = 'm10'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emerald Oryx'),
    (select id from sets where short_name = 'm10'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Excommunicate'),
    (select id from sets where short_name = 'm10'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sleep'),
    (select id from sets where short_name = 'm10'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Assassinate'),
    (select id from sets where short_name = 'm10'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Levitation'),
    (select id from sets where short_name = 'm10'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silence'),
    (select id from sets where short_name = 'm10'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armored Ascension'),
    (select id from sets where short_name = 'm10'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel''s Feather'),
    (select id from sets where short_name = 'm10'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm10'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disorient'),
    (select id from sets where short_name = 'm10'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Frost'),
    (select id from sets where short_name = 'm10'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm10'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sign in Blood'),
    (select id from sets where short_name = 'm10'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'm10'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Giant'),
    (select id from sets where short_name = 'm10'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = 'm10'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'm10'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ornithopter'),
    (select id from sets where short_name = 'm10'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viashino Spearhunter'),
    (select id from sets where short_name = 'm10'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm10'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Warrior'),
    (select id from sets where short_name = 'm10'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cancel'),
    (select id from sets where short_name = 'm10'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runeclaw Bear'),
    (select id from sets where short_name = 'm10'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spellbook'),
    (select id from sets where short_name = 'm10'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Siege Mastodon'),
    (select id from sets where short_name = 'm10'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warpath Ghoul'),
    (select id from sets where short_name = 'm10'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Master of the Wild Hunt'),
    (select id from sets where short_name = 'm10'),
    '191',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dragonskull Summit'),
    (select id from sets where short_name = 'm10'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyroclasm'),
    (select id from sets where short_name = 'm10'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jump'),
    (select id from sets where short_name = 'm10'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Planar Cleansing'),
    (select id from sets where short_name = 'm10'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Divination'),
    (select id from sets where short_name = 'm10'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm10'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bogardan Hellkite'),
    (select id from sets where short_name = 'm10'),
    '127',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm10'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ball Lightning'),
    (select id from sets where short_name = 'm10'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm10'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undead Slayer'),
    (select id from sets where short_name = 'm10'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flashfreeze'),
    (select id from sets where short_name = 'm10'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Honor of the Pure'),
    (select id from sets where short_name = 'm10'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Control'),
    (select id from sets where short_name = 'm10'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Craw Wurm'),
    (select id from sets where short_name = 'm10'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'm10'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Megrim'),
    (select id from sets where short_name = 'm10'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ponder'),
    (select id from sets where short_name = 'm10'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'm10'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Capricious Efreet'),
    (select id from sets where short_name = 'm10'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = 'm10'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'm10'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Djinn of Wishes'),
    (select id from sets where short_name = 'm10'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mist Leopard'),
    (select id from sets where short_name = 'm10'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'White Knight'),
    (select id from sets where short_name = 'm10'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blinding Mage'),
    (select id from sets where short_name = 'm10'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drudge Skeletons'),
    (select id from sets where short_name = 'm10'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oakenform'),
    (select id from sets where short_name = 'm10'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Veteran Armorsmith'),
    (select id from sets where short_name = 'm10'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'm10'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Might of Oaks'),
    (select id from sets where short_name = 'm10'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sanguine Bond'),
    (select id from sets where short_name = 'm10'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Great Sable Stag'),
    (select id from sets where short_name = 'm10'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coat of Arms'),
    (select id from sets where short_name = 'm10'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani Goldmane'),
    (select id from sets where short_name = 'm10'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Divine Verdict'),
    (select id from sets where short_name = 'm10'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Berserkers of Blood Ridge'),
    (select id from sets where short_name = 'm10'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Acolyte of Xathrid'),
    (select id from sets where short_name = 'm10'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ice Cage'),
    (select id from sets where short_name = 'm10'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Safe Passage'),
    (select id from sets where short_name = 'm10'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm10'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = 'm10'),
    '168',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dread Warlock'),
    (select id from sets where short_name = 'm10'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unholy Strength'),
    (select id from sets where short_name = 'm10'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Visionary'),
    (select id from sets where short_name = 'm10'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inferno Elemental'),
    (select id from sets where short_name = 'm10'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enormous Baloth'),
    (select id from sets where short_name = 'm10'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kelinore Bat'),
    (select id from sets where short_name = 'm10'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rootbound Crag'),
    (select id from sets where short_name = 'm10'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Archdruid'),
    (select id from sets where short_name = 'm10'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Bleed'),
    (select id from sets where short_name = 'm10'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Piper'),
    (select id from sets where short_name = 'm10'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = 'm10'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'm10'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nature''s Spiral'),
    (select id from sets where short_name = 'm10'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darksteel Colossus'),
    (select id from sets where short_name = 'm10'),
    '208',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dragon Whelp'),
    (select id from sets where short_name = 'm10'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Holy Strength'),
    (select id from sets where short_name = 'm10'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Wraith'),
    (select id from sets where short_name = 'm10'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demon''s Horn'),
    (select id from sets where short_name = 'm10'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Consume Spirit'),
    (select id from sets where short_name = 'm10'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Underworld Dreams'),
    (select id from sets where short_name = 'm10'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathmark'),
    (select id from sets where short_name = 'm10'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'm10'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm10'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magma Phoenix'),
    (select id from sets where short_name = 'm10'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cemetery Reaper'),
    (select id from sets where short_name = 'm10'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm10'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seismic Strike'),
    (select id from sets where short_name = 'm10'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Artillery'),
    (select id from sets where short_name = 'm10'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Baneslayer Angel'),
    (select id from sets where short_name = 'm10'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Griffin Sentinel'),
    (select id from sets where short_name = 'm10'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Solemn Offering'),
    (select id from sets where short_name = 'm10'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadly Recluse'),
    (select id from sets where short_name = 'm10'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = 'm10'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Warden'),
    (select id from sets where short_name = 'm10'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elite Vanguard'),
    (select id from sets where short_name = 'm10'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fiery Hellhound'),
    (select id from sets where short_name = 'm10'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doom Blade'),
    (select id from sets where short_name = 'm10'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ant Queen'),
    (select id from sets where short_name = 'm10'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fabricate'),
    (select id from sets where short_name = 'm10'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Relentless Rats'),
    (select id from sets where short_name = 'm10'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rod of Ruin'),
    (select id from sets where short_name = 'm10'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Haunting Echoes'),
    (select id from sets where short_name = 'm10'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Spring'),
    (select id from sets where short_name = 'm10'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bramble Creeper'),
    (select id from sets where short_name = 'm10'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manabarbs'),
    (select id from sets where short_name = 'm10'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trumpet Blast'),
    (select id from sets where short_name = 'm10'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kalonian Behemoth'),
    (select id from sets where short_name = 'm10'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = 'm10'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Righteousness'),
    (select id from sets where short_name = 'm10'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Garruk Wildspeaker'),
    (select id from sets where short_name = 'm10'),
    '183',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm10'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Fire'),
    (select id from sets where short_name = 'm10'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Platinum Angel'),
    (select id from sets where short_name = 'm10'),
    '218',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Coral Merfolk'),
    (select id from sets where short_name = 'm10'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm10'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = 'm10'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Awakener Druid'),
    (select id from sets where short_name = 'm10'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'm10'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Elemental'),
    (select id from sets where short_name = 'm10'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sparkmage Apprentice'),
    (select id from sets where short_name = 'm10'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Polymorph'),
    (select id from sets where short_name = 'm10'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Looming Shade'),
    (select id from sets where short_name = 'm10'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm10'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clone'),
    (select id from sets where short_name = 'm10'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm10'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hive Mind'),
    (select id from sets where short_name = 'm10'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Protean Hydra'),
    (select id from sets where short_name = 'm10'),
    '200',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Act of Treason'),
    (select id from sets where short_name = 'm10'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merfolk Looter'),
    (select id from sets where short_name = 'm10'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glorious Charge'),
    (select id from sets where short_name = 'm10'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightwielder Paladin'),
    (select id from sets where short_name = 'm10'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = 'm10'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magebane Armor'),
    (select id from sets where short_name = 'm10'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Specter'),
    (select id from sets where short_name = 'm10'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kindled Fury'),
    (select id from sets where short_name = 'm10'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm10'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disentomb'),
    (select id from sets where short_name = 'm10'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon''s Claw'),
    (select id from sets where short_name = 'm10'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Regenerate'),
    (select id from sets where short_name = 'm10'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel''s Mercy'),
    (select id from sets where short_name = 'm10'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Aristocrat'),
    (select id from sets where short_name = 'm10'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Razorfoot Griffin'),
    (select id from sets where short_name = 'm10'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Nocturnus'),
    (select id from sets where short_name = 'm10'),
    '118',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sunpetal Grove'),
    (select id from sets where short_name = 'm10'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Siege-Gang Commander'),
    (select id from sets where short_name = 'm10'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mold Adder'),
    (select id from sets where short_name = 'm10'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Howl of the Night Pack'),
    (select id from sets where short_name = 'm10'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chandra Nalaar'),
    (select id from sets where short_name = 'm10'),
    '132',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Burning Inquiry'),
    (select id from sets where short_name = 'm10'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wind Drake'),
    (select id from sets where short_name = 'm10'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace Beleren'),
    (select id from sets where short_name = 'm10'),
    '58',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Guardian Seraph'),
    (select id from sets where short_name = 'm10'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raging Goblin'),
    (select id from sets where short_name = 'm10'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warp World'),
    (select id from sets where short_name = 'm10'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gargoyle Castle'),
    (select id from sets where short_name = 'm10'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = 'm10'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx Ambassador'),
    (select id from sets where short_name = 'm10'),
    '73',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Illusionary Servant'),
    (select id from sets where short_name = 'm10'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zephyr Sprite'),
    (select id from sets where short_name = 'm10'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhox Pikemaster'),
    (select id from sets where short_name = 'm10'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firebreathing'),
    (select id from sets where short_name = 'm10'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Canyon Minotaur'),
    (select id from sets where short_name = 'm10'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howling Mine'),
    (select id from sets where short_name = 'm10'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merfolk Sovereign'),
    (select id from sets where short_name = 'm10'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drowned Catacomb'),
    (select id from sets where short_name = 'm10'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Borderland Ranger'),
    (select id from sets where short_name = 'm10'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mesa Enchantress'),
    (select id from sets where short_name = 'm10'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Essence Scatter'),
    (select id from sets where short_name = 'm10'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pithing Needle'),
    (select id from sets where short_name = 'm10'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ignite Disorder'),
    (select id from sets where short_name = 'm10'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Veteran Swordsmith'),
    (select id from sets where short_name = 'm10'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = 'm10'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Royal Assassin'),
    (select id from sets where short_name = 'm10'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Piker'),
    (select id from sets where short_name = 'm10'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rise from the Grave'),
    (select id from sets where short_name = 'm10'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whispersilk Cloak'),
    (select id from sets where short_name = 'm10'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm10'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Goliath'),
    (select id from sets where short_name = 'm10'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Palace Guard'),
    (select id from sets where short_name = 'm10'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prized Unicorn'),
    (select id from sets where short_name = 'm10'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Telepathy'),
    (select id from sets where short_name = 'm10'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weakness'),
    (select id from sets where short_name = 'm10'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm10'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snapping Drake'),
    (select id from sets where short_name = 'm10'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bountiful Harvest'),
    (select id from sets where short_name = 'm10'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormfront Pegasus'),
    (select id from sets where short_name = 'm10'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fog'),
    (select id from sets where short_name = 'm10'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overrun'),
    (select id from sets where short_name = 'm10'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sage Owl'),
    (select id from sets where short_name = 'm10'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana Vess'),
    (select id from sets where short_name = 'm10'),
    '102',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'm10'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cudgel Troll'),
    (select id from sets where short_name = 'm10'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harm''s Way'),
    (select id from sets where short_name = 'm10'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tempest of Light'),
    (select id from sets where short_name = 'm10'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burst of Speed'),
    (select id from sets where short_name = 'm10'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alluring Siren'),
    (select id from sets where short_name = 'm10'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Shatter'),
    (select id from sets where short_name = 'm10'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Child of Night'),
    (select id from sets where short_name = 'm10'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm10'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Captain of the Watch'),
    (select id from sets where short_name = 'm10'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Black Knight'),
    (select id from sets where short_name = 'm10'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Howling Banshee'),
    (select id from sets where short_name = 'm10'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lurking Predators'),
    (select id from sets where short_name = 'm10'),
    '190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diabolic Tutor'),
    (select id from sets where short_name = 'm10'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tendrils of Corruption'),
    (select id from sets where short_name = 'm10'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Panic Attack'),
    (select id from sets where short_name = 'm10'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kraken''s Eye'),
    (select id from sets where short_name = 'm10'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silvercoat Lion'),
    (select id from sets where short_name = 'm10'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prodigal Pyromancer'),
    (select id from sets where short_name = 'm10'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm10'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windstorm'),
    (select id from sets where short_name = 'm10'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glacial Fortress'),
    (select id from sets where short_name = 'm10'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gorgon Flail'),
    (select id from sets where short_name = 'm10'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm10'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Indestructibility'),
    (select id from sets where short_name = 'm10'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lifelink'),
    (select id from sets where short_name = 'm10'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Acidic Slime'),
    (select id from sets where short_name = 'm10'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Bone'),
    (select id from sets where short_name = 'm10'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Time Warp'),
    (select id from sets where short_name = 'm10'),
    '75',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Twincast'),
    (select id from sets where short_name = 'm10'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wurm''s Tooth'),
    (select id from sets where short_name = 'm10'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serpent of the Endless Sea'),
    (select id from sets where short_name = 'm10'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stampeding Rhino'),
    (select id from sets where short_name = 'm10'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Chieftain'),
    (select id from sets where short_name = 'm10'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestial Purge'),
    (select id from sets where short_name = 'm10'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Entangling Vines'),
    (select id from sets where short_name = 'm10'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Traumatize'),
    (select id from sets where short_name = 'm10'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = 'm10'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yawning Fissure'),
    (select id from sets where short_name = 'm10'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horned Turtle'),
    (select id from sets where short_name = 'm10'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Convincing Mirage'),
    (select id from sets where short_name = 'm10'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tome Scour'),
    (select id from sets where short_name = 'm10'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Open the Vaults'),
    (select id from sets where short_name = 'm10'),
    '21',
    'rare'
) 
 on conflict do nothing;
