    insert into mtgcard(name) values ('Shatterskull Charger') on conflict do nothing;
    insert into mtgcard(name) values ('Linvala, Shield of Sea Gate') on conflict do nothing;
    insert into mtgcard(name) values ('Nullpriest of Oblivion') on conflict do nothing;
    insert into mtgcard(name) values ('Squad Commander') on conflict do nothing;
    insert into mtgcard(name) values ('Taborax, Hope''s Demise') on conflict do nothing;
    insert into mtgcard(name) values ('Charix, the Raging Isle') on conflict do nothing;
    insert into mtgcard(name) values ('Thieving Skydiver') on conflict do nothing;
    insert into mtgcard(name) values ('Nimble Trapfinder') on conflict do nothing;
    insert into mtgcard(name) values ('Emeria''s Call // Emeria, Shattered Skyclave') on conflict do nothing;
    insert into mtgcard(name) values ('Swarm Shambler') on conflict do nothing;
    insert into mtgcard(name) values ('Omnath, Locus of Creation') on conflict do nothing;
    insert into mtgcard(name) values ('Kargan Intimidator') on conflict do nothing;
    insert into mtgcard(name) values ('Zareth San, the Trickster') on conflict do nothing;
    insert into mtgcard(name) values ('Orah, Skyclave Hierophant') on conflict do nothing;
    insert into mtgcard(name) values ('Riverglide Pathway // Lavaglide Pathway') on conflict do nothing;
    insert into mtgcard(name) values ('Lithoform Engine') on conflict do nothing;
    insert into mtgcard(name) values ('Brightclimb Pathway // Grimclimb Pathway') on conflict do nothing;
    insert into mtgcard(name) values ('Roiling Vortex') on conflict do nothing;
    insert into mtgcard(name) values ('Shadows'' Verdict') on conflict do nothing;
    insert into mtgcard(name) values ('Maul of the Skyclaves') on conflict do nothing;
    insert into mtgcard(name) values ('Grakmaw, Skyclave Ravager') on conflict do nothing;
    insert into mtgcard(name) values ('Tazri, Beacon of Unity') on conflict do nothing;
    insert into mtgcard(name) values ('Clearwater Pathway // Murkwater Pathway') on conflict do nothing;
    insert into mtgcard(name) values ('Valakut Awakening // Valakut Stoneforge') on conflict do nothing;
    insert into mtgcard(name) values ('Akiri, Fearless Voyager') on conflict do nothing;
    insert into mtgcard(name) values ('Oran-Rief Ooze') on conflict do nothing;
    insert into mtgcard(name) values ('Scute Swarm') on conflict do nothing;
    insert into mtgcard(name) values ('Kaza, Roil Chaser') on conflict do nothing;
    insert into mtgcard(name) values ('Skyclave Shade') on conflict do nothing;
    insert into mtgcard(name) values ('Hagra Mauling // Hagra Broodpit') on conflict do nothing;
    insert into mtgcard(name) values ('Relic Robber') on conflict do nothing;
    insert into mtgcard(name) values ('Nahiri, Heir of the Ancients') on conflict do nothing;
    insert into mtgcard(name) values ('Ancient Greenwarden') on conflict do nothing;
    insert into mtgcard(name) values ('Moraug, Fury of Akoum') on conflict do nothing;
    insert into mtgcard(name) values ('Skyclave Apparition') on conflict do nothing;
    insert into mtgcard(name) values ('Kazandu Mammoth // Kazandu Valley') on conflict do nothing;
    insert into mtgcard(name) values ('Verazol, the Split Current') on conflict do nothing;
    insert into mtgcard(name) values ('Cragplate Baloth') on conflict do nothing;
    insert into mtgcard(name) values ('Wayward Guide-Beast') on conflict do nothing;
    insert into mtgcard(name) values ('Myriad Construct') on conflict do nothing;
    insert into mtgcard(name) values ('Luminarch Aspirant') on conflict do nothing;
    insert into mtgcard(name) values ('Angel of Destiny') on conflict do nothing;
    insert into mtgcard(name) values ('Skyclave Relic') on conflict do nothing;
    insert into mtgcard(name) values ('Glasspool Mimic // Glasspool Shore') on conflict do nothing;
    insert into mtgcard(name) values ('Sea Gate Stormcaller') on conflict do nothing;
    insert into mtgcard(name) values ('Lotus Cobra') on conflict do nothing;
    insert into mtgcard(name) values ('Tajuru Paragon') on conflict do nothing;
    insert into mtgcard(name) values ('Coralhelm Chronicler') on conflict do nothing;
    insert into mtgcard(name) values ('Branchloft Pathway // Boulderloft Pathway') on conflict do nothing;
    insert into mtgcard(name) values ('Drana, the Last Bloodchief') on conflict do nothing;
    insert into mtgcard(name) values ('Inscription of Ruin') on conflict do nothing;
    insert into mtgcard(name) values ('Jace, Mirror Mage') on conflict do nothing;
    insert into mtgcard(name) values ('Archpriest of Iona') on conflict do nothing;
    insert into mtgcard(name) values ('Scourge of the Skyclaves') on conflict do nothing;
    insert into mtgcard(name) values ('Magmatic Channeler') on conflict do nothing;
    insert into mtgcard(name) values ('Legion Angel') on conflict do nothing;
    insert into mtgcard(name) values ('Inscription of Insight') on conflict do nothing;
    insert into mtgcard(name) values ('Agadeem''s Awakening // Agadeem, the Undercrypt') on conflict do nothing;
    insert into mtgcard(name) values ('Zagras, Thief of Heartbeats') on conflict do nothing;
    insert into mtgcard(name) values ('Crawling Barrens') on conflict do nothing;
    insert into mtgcard(name) values ('Throne of Makindi') on conflict do nothing;
    insert into mtgcard(name) values ('Valakut Exploration') on conflict do nothing;
    insert into mtgcard(name) values ('Maddening Cacophony') on conflict do nothing;
    insert into mtgcard(name) values ('Yasharn, Implacable Earth') on conflict do nothing;
    insert into mtgcard(name) values ('Nissa of Shadowed Boughs') on conflict do nothing;
    insert into mtgcard(name) values ('Coveted Prize') on conflict do nothing;
    insert into mtgcard(name) values ('Phylath, World Sculptor') on conflict do nothing;
    insert into mtgcard(name) values ('Needleverge Pathway // Pillarverge Pathway') on conflict do nothing;
    insert into mtgcard(name) values ('Nighthawk Scavenger') on conflict do nothing;
    insert into mtgcard(name) values ('Nahiri''s Lithoforming') on conflict do nothing;
    insert into mtgcard(name) values ('Ondu Inversion // Ondu Skyruins') on conflict do nothing;
    insert into mtgcard(name) values ('Leyline Tyrant') on conflict do nothing;
    insert into mtgcard(name) values ('Sea Gate Restoration // Sea Gate, Reborn') on conflict do nothing;
    insert into mtgcard(name) values ('Master of Winds') on conflict do nothing;
    insert into mtgcard(name) values ('Soul Shatter') on conflict do nothing;
    insert into mtgcard(name) values ('Inscription of Abundance') on conflict do nothing;
    insert into mtgcard(name) values ('Forsaken Monument') on conflict do nothing;
    insert into mtgcard(name) values ('Shatterskull Smashing // Shatterskull, the Hammer Pass') on conflict do nothing;
    insert into mtgcard(name) values ('Turntimber Symbiosis // Turntimber, Serpentine Wood') on conflict do nothing;
    insert into mtgcard(name) values ('Confounding Conundrum') on conflict do nothing;
    insert into mtgcard(name) values ('Ashaya, Soul of the Wild') on conflict do nothing;
    insert into mtgcard(name) values ('Felidar Retreat') on conflict do nothing;
    insert into mtgcard(name) values ('Archon of Emeria') on conflict do nothing;
    insert into mtgcard(name) values ('Cragcrown Pathway // Timbercrown Pathway') on conflict do nothing;
