insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Shatterskull Charger'),
    (select id from sets where short_name = 'pznr'),
    '159s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Linvala, Shield of Sea Gate'),
    (select id from sets where short_name = 'pznr'),
    '226p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nullpriest of Oblivion'),
    (select id from sets where short_name = 'pznr'),
    '118p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Squad Commander'),
    (select id from sets where short_name = 'pznr'),
    '41p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Squad Commander'),
    (select id from sets where short_name = 'pznr'),
    '41s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Taborax, Hope''s Demise'),
    (select id from sets where short_name = 'pznr'),
    '129s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Charix, the Raging Isle'),
    (select id from sets where short_name = 'pznr'),
    '49p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thieving Skydiver'),
    (select id from sets where short_name = 'pznr'),
    '85s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nimble Trapfinder'),
    (select id from sets where short_name = 'pznr'),
    '72p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emeria''s Call // Emeria, Shattered Skyclave'),
    (select id from sets where short_name = 'pznr'),
    '12s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swarm Shambler'),
    (select id from sets where short_name = 'pznr'),
    '207p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Omnath, Locus of Creation'),
    (select id from sets where short_name = 'pznr'),
    '232s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kargan Intimidator'),
    (select id from sets where short_name = 'pznr'),
    '145p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zareth San, the Trickster'),
    (select id from sets where short_name = 'pznr'),
    '242s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orah, Skyclave Hierophant'),
    (select id from sets where short_name = 'pznr'),
    '233p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Riverglide Pathway // Lavaglide Pathway'),
    (select id from sets where short_name = 'pznr'),
    '264s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lithoform Engine'),
    (select id from sets where short_name = 'pznr'),
    '245p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Brightclimb Pathway // Grimclimb Pathway'),
    (select id from sets where short_name = 'pznr'),
    '259s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roiling Vortex'),
    (select id from sets where short_name = 'pznr'),
    '156s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nimble Trapfinder'),
    (select id from sets where short_name = 'pznr'),
    '72s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shadows'' Verdict'),
    (select id from sets where short_name = 'pznr'),
    '124s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maul of the Skyclaves'),
    (select id from sets where short_name = 'pznr'),
    '27s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grakmaw, Skyclave Ravager'),
    (select id from sets where short_name = 'pznr'),
    '223s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tazri, Beacon of Unity'),
    (select id from sets where short_name = 'pznr'),
    '44s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Clearwater Pathway // Murkwater Pathway'),
    (select id from sets where short_name = 'pznr'),
    '260s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Valakut Awakening // Valakut Stoneforge'),
    (select id from sets where short_name = 'pznr'),
    '174s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akiri, Fearless Voyager'),
    (select id from sets where short_name = 'pznr'),
    '220p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief Ooze'),
    (select id from sets where short_name = 'pznr'),
    '198p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scute Swarm'),
    (select id from sets where short_name = 'pznr'),
    '203p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaza, Roil Chaser'),
    (select id from sets where short_name = 'pznr'),
    '225p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyclave Shade'),
    (select id from sets where short_name = 'pznr'),
    '125s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hagra Mauling // Hagra Broodpit'),
    (select id from sets where short_name = 'pznr'),
    '106s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lithoform Engine'),
    (select id from sets where short_name = 'pznr'),
    '245s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Relic Robber'),
    (select id from sets where short_name = 'pznr'),
    '153s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nahiri, Heir of the Ancients'),
    (select id from sets where short_name = 'pznr'),
    '230s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ancient Greenwarden'),
    (select id from sets where short_name = 'pznr'),
    '178p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Moraug, Fury of Akoum'),
    (select id from sets where short_name = 'pznr'),
    '150p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Skyclave Apparition'),
    (select id from sets where short_name = 'pznr'),
    '39p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kazandu Mammoth // Kazandu Valley'),
    (select id from sets where short_name = 'pznr'),
    '189s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verazol, the Split Current'),
    (select id from sets where short_name = 'pznr'),
    '239s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cragplate Baloth'),
    (select id from sets where short_name = 'pznr'),
    '183s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wayward Guide-Beast'),
    (select id from sets where short_name = 'pznr'),
    '176s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shadows'' Verdict'),
    (select id from sets where short_name = 'pznr'),
    '124p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaza, Roil Chaser'),
    (select id from sets where short_name = 'pznr'),
    '225s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myriad Construct'),
    (select id from sets where short_name = 'pznr'),
    '246s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Luminarch Aspirant'),
    (select id from sets where short_name = 'pznr'),
    '24s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myriad Construct'),
    (select id from sets where short_name = 'pznr'),
    '246p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel of Destiny'),
    (select id from sets where short_name = 'pznr'),
    '2s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Skyclave Relic'),
    (select id from sets where short_name = 'pznr'),
    '252s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glasspool Mimic // Glasspool Shore'),
    (select id from sets where short_name = 'pznr'),
    '60s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Stormcaller'),
    (select id from sets where short_name = 'pznr'),
    '77p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Skyclave Relic'),
    (select id from sets where short_name = 'pznr'),
    '252p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orah, Skyclave Hierophant'),
    (select id from sets where short_name = 'pznr'),
    '233s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lotus Cobra'),
    (select id from sets where short_name = 'pznr'),
    '193p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tajuru Paragon'),
    (select id from sets where short_name = 'pznr'),
    '209s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coralhelm Chronicler'),
    (select id from sets where short_name = 'pznr'),
    '54s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relic Robber'),
    (select id from sets where short_name = 'pznr'),
    '153p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief Ooze'),
    (select id from sets where short_name = 'pznr'),
    '198s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Branchloft Pathway // Boulderloft Pathway'),
    (select id from sets where short_name = 'pznr'),
    '258s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akiri, Fearless Voyager'),
    (select id from sets where short_name = 'pznr'),
    '220s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drana, the Last Bloodchief'),
    (select id from sets where short_name = 'pznr'),
    '98s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Inscription of Ruin'),
    (select id from sets where short_name = 'pznr'),
    '108s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jace, Mirror Mage'),
    (select id from sets where short_name = 'pznr'),
    '63p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Archpriest of Iona'),
    (select id from sets where short_name = 'pznr'),
    '5s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scourge of the Skyclaves'),
    (select id from sets where short_name = 'pznr'),
    '122p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swarm Shambler'),
    (select id from sets where short_name = 'pznr'),
    '207s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jace, Mirror Mage'),
    (select id from sets where short_name = 'pznr'),
    '63s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Inscription of Ruin'),
    (select id from sets where short_name = 'pznr'),
    '108p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lotus Cobra'),
    (select id from sets where short_name = 'pznr'),
    '193s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magmatic Channeler'),
    (select id from sets where short_name = 'pznr'),
    '148s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verazol, the Split Current'),
    (select id from sets where short_name = 'pznr'),
    '239p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Legion Angel'),
    (select id from sets where short_name = 'pznr'),
    '23p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inscription of Insight'),
    (select id from sets where short_name = 'pznr'),
    '61p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zareth San, the Trickster'),
    (select id from sets where short_name = 'pznr'),
    '242p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Agadeem''s Awakening // Agadeem, the Undercrypt'),
    (select id from sets where short_name = 'pznr'),
    '90s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Zagras, Thief of Heartbeats'),
    (select id from sets where short_name = 'pznr'),
    '241p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roiling Vortex'),
    (select id from sets where short_name = 'pznr'),
    '156p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crawling Barrens'),
    (select id from sets where short_name = 'pznr'),
    '262s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Linvala, Shield of Sea Gate'),
    (select id from sets where short_name = 'pznr'),
    '226s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Throne of Makindi'),
    (select id from sets where short_name = 'pznr'),
    '265p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scourge of the Skyclaves'),
    (select id from sets where short_name = 'pznr'),
    '122s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scute Swarm'),
    (select id from sets where short_name = 'pznr'),
    '203s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Valakut Exploration'),
    (select id from sets where short_name = 'pznr'),
    '175p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maddening Cacophony'),
    (select id from sets where short_name = 'pznr'),
    '67s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nullpriest of Oblivion'),
    (select id from sets where short_name = 'pznr'),
    '118s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Taborax, Hope''s Demise'),
    (select id from sets where short_name = 'pznr'),
    '129p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tazri, Beacon of Unity'),
    (select id from sets where short_name = 'pznr'),
    '44p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Yasharn, Implacable Earth'),
    (select id from sets where short_name = 'pznr'),
    '240p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cragplate Baloth'),
    (select id from sets where short_name = 'pznr'),
    '183p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa of Shadowed Boughs'),
    (select id from sets where short_name = 'pznr'),
    '231p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thieving Skydiver'),
    (select id from sets where short_name = 'pznr'),
    '85p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coveted Prize'),
    (select id from sets where short_name = 'pznr'),
    '95s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inscription of Insight'),
    (select id from sets where short_name = 'pznr'),
    '61s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phylath, World Sculptor'),
    (select id from sets where short_name = 'pznr'),
    '234s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Needleverge Pathway // Pillarverge Pathway'),
    (select id from sets where short_name = 'pznr'),
    '263s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nighthawk Scavenger'),
    (select id from sets where short_name = 'pznr'),
    '115p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coralhelm Chronicler'),
    (select id from sets where short_name = 'pznr'),
    '54p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Stormcaller'),
    (select id from sets where short_name = 'pznr'),
    '77s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nahiri''s Lithoforming'),
    (select id from sets where short_name = 'pznr'),
    '151s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crawling Barrens'),
    (select id from sets where short_name = 'pznr'),
    '262p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ondu Inversion // Ondu Skyruins'),
    (select id from sets where short_name = 'pznr'),
    '30s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Charix, the Raging Isle'),
    (select id from sets where short_name = 'pznr'),
    '49s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drana, the Last Bloodchief'),
    (select id from sets where short_name = 'pznr'),
    '98p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Leyline Tyrant'),
    (select id from sets where short_name = 'pznr'),
    '147s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nahiri, Heir of the Ancients'),
    (select id from sets where short_name = 'pznr'),
    '230p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Yasharn, Implacable Earth'),
    (select id from sets where short_name = 'pznr'),
    '240s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Valakut Exploration'),
    (select id from sets where short_name = 'pznr'),
    '175s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Restoration // Sea Gate, Reborn'),
    (select id from sets where short_name = 'pznr'),
    '76s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Master of Winds'),
    (select id from sets where short_name = 'pznr'),
    '68p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nighthawk Scavenger'),
    (select id from sets where short_name = 'pznr'),
    '115s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyclave Apparition'),
    (select id from sets where short_name = 'pznr'),
    '39s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Luminarch Aspirant'),
    (select id from sets where short_name = 'pznr'),
    '24p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Shatter'),
    (select id from sets where short_name = 'pznr'),
    '127s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Throne of Makindi'),
    (select id from sets where short_name = 'pznr'),
    '265s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Master of Winds'),
    (select id from sets where short_name = 'pznr'),
    '68s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inscription of Abundance'),
    (select id from sets where short_name = 'pznr'),
    '186p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Greenwarden'),
    (select id from sets where short_name = 'pznr'),
    '178s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Omnath, Locus of Creation'),
    (select id from sets where short_name = 'pznr'),
    '232p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Skyclave Shade'),
    (select id from sets where short_name = 'pznr'),
    '125p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archpriest of Iona'),
    (select id from sets where short_name = 'pznr'),
    '5p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tajuru Paragon'),
    (select id from sets where short_name = 'pznr'),
    '209p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forsaken Monument'),
    (select id from sets where short_name = 'pznr'),
    '244p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shatterskull Smashing // Shatterskull, the Hammer Pass'),
    (select id from sets where short_name = 'pznr'),
    '161s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wayward Guide-Beast'),
    (select id from sets where short_name = 'pznr'),
    '176p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Turntimber Symbiosis // Turntimber, Serpentine Wood'),
    (select id from sets where short_name = 'pznr'),
    '215s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Confounding Conundrum'),
    (select id from sets where short_name = 'pznr'),
    '53p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashaya, Soul of the Wild'),
    (select id from sets where short_name = 'pznr'),
    '179s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Grakmaw, Skyclave Ravager'),
    (select id from sets where short_name = 'pznr'),
    '223p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatterskull Charger'),
    (select id from sets where short_name = 'pznr'),
    '159p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kargan Intimidator'),
    (select id from sets where short_name = 'pznr'),
    '145s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leyline Tyrant'),
    (select id from sets where short_name = 'pznr'),
    '147p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Maddening Cacophony'),
    (select id from sets where short_name = 'pznr'),
    '67p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa of Shadowed Boughs'),
    (select id from sets where short_name = 'pznr'),
    '231s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Zagras, Thief of Heartbeats'),
    (select id from sets where short_name = 'pznr'),
    '241s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coveted Prize'),
    (select id from sets where short_name = 'pznr'),
    '95p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inscription of Abundance'),
    (select id from sets where short_name = 'pznr'),
    '186s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Confounding Conundrum'),
    (select id from sets where short_name = 'pznr'),
    '53s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Felidar Retreat'),
    (select id from sets where short_name = 'pznr'),
    '16p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Legion Angel'),
    (select id from sets where short_name = 'pznr'),
    '23s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Shatter'),
    (select id from sets where short_name = 'pznr'),
    '127p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forsaken Monument'),
    (select id from sets where short_name = 'pznr'),
    '244s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Archon of Emeria'),
    (select id from sets where short_name = 'pznr'),
    '4p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Felidar Retreat'),
    (select id from sets where short_name = 'pznr'),
    '16s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashaya, Soul of the Wild'),
    (select id from sets where short_name = 'pznr'),
    '179p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cragcrown Pathway // Timbercrown Pathway'),
    (select id from sets where short_name = 'pznr'),
    '261s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archon of Emeria'),
    (select id from sets where short_name = 'pznr'),
    '4s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nahiri''s Lithoforming'),
    (select id from sets where short_name = 'pznr'),
    '151p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magmatic Channeler'),
    (select id from sets where short_name = 'pznr'),
    '148p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phylath, World Sculptor'),
    (select id from sets where short_name = 'pznr'),
    '234p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel of Destiny'),
    (select id from sets where short_name = 'pznr'),
    '2p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Moraug, Fury of Akoum'),
    (select id from sets where short_name = 'pznr'),
    '150s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Maul of the Skyclaves'),
    (select id from sets where short_name = 'pznr'),
    '27p',
    'rare'
) 
 on conflict do nothing;
