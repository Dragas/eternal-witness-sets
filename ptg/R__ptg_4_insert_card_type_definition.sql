insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Nightmare Moon // Princess Luna'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nightmare Moon // Princess Luna'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nightmare Moon // Princess Luna'),
        (select types.id from types where name = 'Alicorn')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nightmare Moon // Princess Luna'),
        (select types.id from types where name = '//')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nightmare Moon // Princess Luna'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nightmare Moon // Princess Luna'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nightmare Moon // Princess Luna'),
        (select types.id from types where name = 'Alicorn')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Princess Twilight Sparkle'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Princess Twilight Sparkle'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Princess Twilight Sparkle'),
        (select types.id from types where name = 'Alicorn')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rarity'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rarity'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rarity'),
        (select types.id from types where name = 'Unicorn')
    ) 
 on conflict do nothing;
