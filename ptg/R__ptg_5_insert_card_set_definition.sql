insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Nightmare Moon // Princess Luna'),
    (select id from sets where short_name = 'ptg'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Princess Twilight Sparkle'),
    (select id from sets where short_name = 'ptg'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rarity'),
    (select id from sets where short_name = 'ptg'),
    '3',
    'mythic'
) 
 on conflict do nothing;
