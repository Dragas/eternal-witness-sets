insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Alhammarret, High Arbiter'),
    (select id from sets where short_name = 'pori'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Embermaw Hellion'),
    (select id from sets where short_name = 'pori'),
    '141s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vryn Wingmare'),
    (select id from sets where short_name = 'pori'),
    '40s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Managorger Hydra'),
    (select id from sets where short_name = 'pori'),
    '186p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mizzium Meddler'),
    (select id from sets where short_name = 'pori'),
    '64s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kytheon''s Irregulars'),
    (select id from sets where short_name = 'pori'),
    '24s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hixus, Prison Warden'),
    (select id from sets where short_name = 'pori'),
    '19s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abbot of Keral Keep'),
    (select id from sets where short_name = 'pori'),
    '127s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Revenge'),
    (select id from sets where short_name = 'pori'),
    '177s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Priest of the Blood Rite'),
    (select id from sets where short_name = 'pori'),
    '112s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pia and Kiran Nalaar'),
    (select id from sets where short_name = 'pori'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kothophed, Soul Hoarder'),
    (select id from sets where short_name = 'pori'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana, Heretical Healer // Liliana, Defiant Necromancer'),
    (select id from sets where short_name = 'pori'),
    '106s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chandra, Fire of Kaladesh // Chandra, Roaring Flame'),
    (select id from sets where short_name = 'pori'),
    '135s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Graveblade Marauder'),
    (select id from sets where short_name = 'pori'),
    '101s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kytheon, Hero of Akros // Gideon, Battle-Forged'),
    (select id from sets where short_name = 'pori'),
    '23s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Honored Hierarch'),
    (select id from sets where short_name = 'pori'),
    '182s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harbinger of the Tides'),
    (select id from sets where short_name = 'pori'),
    '58s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Ignition'),
    (select id from sets where short_name = 'pori'),
    '137s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Languish'),
    (select id from sets where short_name = 'pori'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Outland Colossus'),
    (select id from sets where short_name = 'pori'),
    '193s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Petition'),
    (select id from sets where short_name = 'pori'),
    '90s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gideon''s Phalanx'),
    (select id from sets where short_name = 'pori'),
    '14s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Managorger Hydra'),
    (select id from sets where short_name = 'pori'),
    '186s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knight of the White Orchid'),
    (select id from sets where short_name = 'pori'),
    '21s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Languish'),
    (select id from sets where short_name = 'pori'),
    '105s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Willbreaker'),
    (select id from sets where short_name = 'pori'),
    '84s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pia and Kiran Nalaar'),
    (select id from sets where short_name = 'pori'),
    '157s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conclave Naturalists'),
    (select id from sets where short_name = 'pori'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nissa''s Revelation'),
    (select id from sets where short_name = 'pori'),
    '191s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jace''s Sanctum'),
    (select id from sets where short_name = 'pori'),
    '61p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa, Vastwood Seer // Nissa, Sage Animist'),
    (select id from sets where short_name = 'pori'),
    '189s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Alhammarret, High Arbiter'),
    (select id from sets where short_name = 'pori'),
    '43s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thopter Spy Network'),
    (select id from sets where short_name = 'pori'),
    '79s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kothophed, Soul Hoarder'),
    (select id from sets where short_name = 'pori'),
    '104s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hixus, Prison Warden'),
    (select id from sets where short_name = 'pori'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jace, Vryn''s Prodigy // Jace, Telepath Unbound'),
    (select id from sets where short_name = 'pori'),
    '60s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Exquisite Firecraft'),
    (select id from sets where short_name = 'pori'),
    '143s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evolutionary Leap'),
    (select id from sets where short_name = 'pori'),
    '176s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dwynen, Gilt-Leaf Daen'),
    (select id from sets where short_name = 'pori'),
    '172s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scab-Clan Berserker'),
    (select id from sets where short_name = 'pori'),
    '160s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soulblade Djinn'),
    (select id from sets where short_name = 'pori'),
    '75s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relic Seeker'),
    (select id from sets where short_name = 'pori'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relic Seeker'),
    (select id from sets where short_name = 'pori'),
    '29s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tragic Arrogance'),
    (select id from sets where short_name = 'pori'),
    '38s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gilt-Leaf Winnower'),
    (select id from sets where short_name = 'pori'),
    '99s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mizzium Meddler'),
    (select id from sets where short_name = 'pori'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Talent of the Telepath'),
    (select id from sets where short_name = 'pori'),
    '78s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dwynen, Gilt-Leaf Daen'),
    (select id from sets where short_name = 'pori'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword of the Animist'),
    (select id from sets where short_name = 'pori'),
    '240p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Despoiler of Souls'),
    (select id from sets where short_name = 'pori'),
    '93s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Piledriver'),
    (select id from sets where short_name = 'pori'),
    '151s',
    'rare'
) 
 on conflict do nothing;
