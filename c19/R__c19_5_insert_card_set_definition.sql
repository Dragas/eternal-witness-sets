insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Blossoming Sands'),
    (select id from sets where short_name = 'c19'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naya Charm'),
    (select id from sets where short_name = 'c19'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sanitarium Skeleton'),
    (select id from sets where short_name = 'c19'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darkwater Catacombs'),
    (select id from sets where short_name = 'c19'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boneyard Parley'),
    (select id from sets where short_name = 'c19'),
    '107',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Overseer of the Damned'),
    (select id from sets where short_name = 'c19'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm Herd'),
    (select id from sets where short_name = 'c19'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Farseek'),
    (select id from sets where short_name = 'c19'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Secrets of the Dead'),
    (select id from sets where short_name = 'c19'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thieving Amalgam'),
    (select id from sets where short_name = 'c19'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thornwood Falls'),
    (select id from sets where short_name = 'c19'),
    '283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cinder Barrens'),
    (select id from sets where short_name = 'c19'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Graypelt Refuge'),
    (select id from sets where short_name = 'c19'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Memorial to Folly'),
    (select id from sets where short_name = 'c19'),
    '259',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wildfire Devils'),
    (select id from sets where short_name = 'c19'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystic Retrieval'),
    (select id from sets where short_name = 'c19'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grimoire of the Dead'),
    (select id from sets where short_name = 'c19'),
    '213',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c19'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sultai Charm'),
    (select id from sets where short_name = 'c19'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul of Zendikar'),
    (select id from sets where short_name = 'c19'),
    '182',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Magmaquake'),
    (select id from sets where short_name = 'c19'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Elder'),
    (select id from sets where short_name = 'c19'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c19'),
    '290',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reliquary Tower'),
    (select id from sets where short_name = 'c19'),
    '268',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Murderous Compulsion'),
    (select id from sets where short_name = 'c19'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tahngarth, First Mate'),
    (select id from sets where short_name = 'c19'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wingmate Roc'),
    (select id from sets where short_name = 'c19'),
    '78',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c19'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tectonic Hellion'),
    (select id from sets where short_name = 'c19'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tezzeret''s Gambit'),
    (select id from sets where short_name = 'c19'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deathmist Raptor'),
    (select id from sets where short_name = 'c19'),
    '160',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Archfiend of Spite'),
    (select id from sets where short_name = 'c19'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vesuvan Shapeshifter'),
    (select id from sets where short_name = 'c19'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruul Turf'),
    (select id from sets where short_name = 'c19'),
    '250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warstorm Surge'),
    (select id from sets where short_name = 'c19'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selesnya Sanctuary'),
    (select id from sets where short_name = 'c19'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Biomass Mutation'),
    (select id from sets where short_name = 'c19'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anje''s Ravager'),
    (select id from sets where short_name = 'c19'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Second Harvest'),
    (select id from sets where short_name = 'c19'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of the False God'),
    (select id from sets where short_name = 'c19'),
    '280',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wind-Scarred Crag'),
    (select id from sets where short_name = 'c19'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sagu Mauler'),
    (select id from sets where short_name = 'c19'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exotic Orchard'),
    (select id from sets where short_name = 'c19'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c19'),
    '288',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mortuary Mire'),
    (select id from sets where short_name = 'c19'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stromkirk Occultist'),
    (select id from sets where short_name = 'c19'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jungle Shrine'),
    (select id from sets where short_name = 'c19'),
    '255',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vitu-Ghazi Guildmage'),
    (select id from sets where short_name = 'c19'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Guildgate'),
    (select id from sets where short_name = 'c19'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Solemn Simulacrum'),
    (select id from sets where short_name = 'c19'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rolling Temblor'),
    (select id from sets where short_name = 'c19'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Increasing Vengeance'),
    (select id from sets where short_name = 'c19'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swiftwater Cliffs'),
    (select id from sets where short_name = 'c19'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opulent Palace'),
    (select id from sets where short_name = 'c19'),
    '264',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Backdraft Hellkite'),
    (select id from sets where short_name = 'c19'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Geth, Lord of the Vault'),
    (select id from sets where short_name = 'c19'),
    '114',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Squee, Goblin Nabob'),
    (select id from sets where short_name = 'c19'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sevinne, the Chronoclasm'),
    (select id from sets where short_name = 'c19'),
    '49',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Empowered Autogenerator'),
    (select id from sets where short_name = 'c19'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Key to the City'),
    (select id from sets where short_name = 'c19'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Coast'),
    (select id from sets where short_name = 'c19'),
    '287',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pristine Angel'),
    (select id from sets where short_name = 'c19'),
    '70',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Trostani, Selesnya''s Voice'),
    (select id from sets where short_name = 'c19'),
    '204',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Grim Haruspex'),
    (select id from sets where short_name = 'c19'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'K''rrik, Son of Yawgmoth'),
    (select id from sets where short_name = 'c19'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystic Monastery'),
    (select id from sets where short_name = 'c19'),
    '262',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Stolen Identity'),
    (select id from sets where short_name = 'c19'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tranquil Cove'),
    (select id from sets where short_name = 'c19'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mire in Misery'),
    (select id from sets where short_name = 'c19'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trail of Mystery'),
    (select id from sets where short_name = 'c19'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myriad Landscape'),
    (select id from sets where short_name = 'c19'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elsha of the Infinite'),
    (select id from sets where short_name = 'c19'),
    '40',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Golgari Rot Farm'),
    (select id from sets where short_name = 'c19'),
    '248',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stone Quarry'),
    (select id from sets where short_name = 'c19'),
    '276',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sun Titan'),
    (select id from sets where short_name = 'c19'),
    '76',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Selesnya Eulogist'),
    (select id from sets where short_name = 'c19'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul of Innistrad'),
    (select id from sets where short_name = 'c19'),
    '130',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bojuka Bog'),
    (select id from sets where short_name = 'c19'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shrine of the Forsaken Gods'),
    (select id from sets where short_name = 'c19'),
    '273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Violent Eruption'),
    (select id from sets where short_name = 'c19'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodthirsty Blade'),
    (select id from sets where short_name = 'c19'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Simic Growth Chamber'),
    (select id from sets where short_name = 'c19'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Commander''s Insignia'),
    (select id from sets where short_name = 'c19'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thalia''s Geistcaller'),
    (select id from sets where short_name = 'c19'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Highland Lake'),
    (select id from sets where short_name = 'c19'),
    '251',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Big Game Hunter'),
    (select id from sets where short_name = 'c19'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avacyn''s Judgment'),
    (select id from sets where short_name = 'c19'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feldon of the Third Path'),
    (select id from sets where short_name = 'c19'),
    '141',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Reality Shift'),
    (select id from sets where short_name = 'c19'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leadership Vacuum'),
    (select id from sets where short_name = 'c19'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Adephage'),
    (select id from sets where short_name = 'c19'),
    '169',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Putrefy'),
    (select id from sets where short_name = 'c19'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armillary Sphere'),
    (select id from sets where short_name = 'c19'),
    '209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overwhelming Stampede'),
    (select id from sets where short_name = 'c19'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pristine Skywise'),
    (select id from sets where short_name = 'c19'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elemental Bond'),
    (select id from sets where short_name = 'c19'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Akoum Refuge'),
    (select id from sets where short_name = 'c19'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pendant of Prosperity'),
    (select id from sets where short_name = 'c19'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightshade Assassin'),
    (select id from sets where short_name = 'c19'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azorius Chancery'),
    (select id from sets where short_name = 'c19'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thragtusk'),
    (select id from sets where short_name = 'c19'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Echoing Truth'),
    (select id from sets where short_name = 'c19'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ral Zarek'),
    (select id from sets where short_name = 'c19'),
    '198',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rakdos Carnarium'),
    (select id from sets where short_name = 'c19'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Colossal Majesty'),
    (select id from sets where short_name = 'c19'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dockside Extortionist'),
    (select id from sets where short_name = 'c19'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rogue''s Passage'),
    (select id from sets where short_name = 'c19'),
    '270',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ignite the Future'),
    (select id from sets where short_name = 'c19'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Izzet Boilerworks'),
    (select id from sets where short_name = 'c19'),
    '252',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trostani''s Judgment'),
    (select id from sets where short_name = 'c19'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grave Scrabbler'),
    (select id from sets where short_name = 'c19'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakdos Guildgate'),
    (select id from sets where short_name = 'c19'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Apex Altisaur'),
    (select id from sets where short_name = 'c19'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faith of the Devoted'),
    (select id from sets where short_name = 'c19'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyfire Phoenix'),
    (select id from sets where short_name = 'c19'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flayer of the Hatebound'),
    (select id from sets where short_name = 'c19'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Great Oak Guardian'),
    (select id from sets where short_name = 'c19'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forgotten Cave'),
    (select id from sets where short_name = 'c19'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kadena, Slinking Sorcerer'),
    (select id from sets where short_name = 'c19'),
    '45',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rayami, First of the Fallen'),
    (select id from sets where short_name = 'c19'),
    '48',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Commander''s Sphere'),
    (select id from sets where short_name = 'c19'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Foundry'),
    (select id from sets where short_name = 'c19'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kazandu Refuge'),
    (select id from sets where short_name = 'c19'),
    '256',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heart-Piercer Manticore'),
    (select id from sets where short_name = 'c19'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azorius Locket'),
    (select id from sets where short_name = 'c19'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plaguecrafter'),
    (select id from sets where short_name = 'c19'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'In Garruk''s Wake'),
    (select id from sets where short_name = 'c19'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Willbender'),
    (select id from sets where short_name = 'c19'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Growing Ranks'),
    (select id from sets where short_name = 'c19'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thought Sponge'),
    (select id from sets where short_name = 'c19'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cinder Glade'),
    (select id from sets where short_name = 'c19'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chaos Warp'),
    (select id from sets where short_name = 'c19'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mass Diminish'),
    (select id from sets where short_name = 'c19'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scaretiller'),
    (select id from sets where short_name = 'c19'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wayfaring Temple'),
    (select id from sets where short_name = 'c19'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fresh Meat'),
    (select id from sets where short_name = 'c19'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tempt with Discovery'),
    (select id from sets where short_name = 'c19'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Eldest Reborn'),
    (select id from sets where short_name = 'c19'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deep Analysis'),
    (select id from sets where short_name = 'c19'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fervent Denial'),
    (select id from sets where short_name = 'c19'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alchemist''s Greeting'),
    (select id from sets where short_name = 'c19'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foul Orchard'),
    (select id from sets where short_name = 'c19'),
    '244',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Think Twice'),
    (select id from sets where short_name = 'c19'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emmara Tandris'),
    (select id from sets where short_name = 'c19'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sevinne''s Reclamation'),
    (select id from sets where short_name = 'c19'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guttersnipe'),
    (select id from sets where short_name = 'c19'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jungle Hollow'),
    (select id from sets where short_name = 'c19'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampaging Baloths'),
    (select id from sets where short_name = 'c19'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jace''s Sanctum'),
    (select id from sets where short_name = 'c19'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silumgar Assassin'),
    (select id from sets where short_name = 'c19'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'River Kelpie'),
    (select id from sets where short_name = 'c19'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stratus Dancer'),
    (select id from sets where short_name = 'c19'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghastly Conscription'),
    (select id from sets where short_name = 'c19'),
    '115',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pramikon, Sky Rampart'),
    (select id from sets where short_name = 'c19'),
    '47',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Beacon of Unrest'),
    (select id from sets where short_name = 'c19'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strionic Resonator'),
    (select id from sets where short_name = 'c19'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hedron Archive'),
    (select id from sets where short_name = 'c19'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Runic Repetition'),
    (select id from sets where short_name = 'c19'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c19'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grismold, the Dreadsower'),
    (select id from sets where short_name = 'c19'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mimic Vat'),
    (select id from sets where short_name = 'c19'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thran Dynamo'),
    (select id from sets where short_name = 'c19'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anje Falkenrath'),
    (select id from sets where short_name = 'c19'),
    '37',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bounty of the Luxa'),
    (select id from sets where short_name = 'c19'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rugged Highlands'),
    (select id from sets where short_name = 'c19'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bane of the Living'),
    (select id from sets where short_name = 'c19'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thespian''s Stage'),
    (select id from sets where short_name = 'c19'),
    '282',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightmare Unmaking'),
    (select id from sets where short_name = 'c19'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sundering Growth'),
    (select id from sets where short_name = 'c19'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gargoyle Castle'),
    (select id from sets where short_name = 'c19'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Increasing Devotion'),
    (select id from sets where short_name = 'c19'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'c19'),
    '281',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naya Panorama'),
    (select id from sets where short_name = 'c19'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curse of Fool''s Wisdom'),
    (select id from sets where short_name = 'c19'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Guildgate'),
    (select id from sets where short_name = 'c19'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magus of the Wheel'),
    (select id from sets where short_name = 'c19'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Greaves'),
    (select id from sets where short_name = 'c19'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Rebirth'),
    (select id from sets where short_name = 'c19'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hate Mirage'),
    (select id from sets where short_name = 'c19'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ohran Frostfang'),
    (select id from sets where short_name = 'c19'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sudden Substitution'),
    (select id from sets where short_name = 'c19'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c19'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volrath, the Shapestealer'),
    (select id from sets where short_name = 'c19'),
    '51',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scroll of Fate'),
    (select id from sets where short_name = 'c19'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Izzet Locket'),
    (select id from sets where short_name = 'c19'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boros Garrison'),
    (select id from sets where short_name = 'c19'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c19'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk, Primal Hunter'),
    (select id from sets where short_name = 'c19'),
    '167',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sanctum of Eternity'),
    (select id from sets where short_name = 'c19'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vraska the Unseen'),
    (select id from sets where short_name = 'c19'),
    '207',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bone Miser'),
    (select id from sets where short_name = 'c19'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roc Egg'),
    (select id from sets where short_name = 'c19'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doomed Necromancer'),
    (select id from sets where short_name = 'c19'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Talrand, Sky Summoner'),
    (select id from sets where short_name = 'c19'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shamanic Revelation'),
    (select id from sets where short_name = 'c19'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gift of Doom'),
    (select id from sets where short_name = 'c19'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Locket'),
    (select id from sets where short_name = 'c19'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hooded Hydra'),
    (select id from sets where short_name = 'c19'),
    '172',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c19'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c19'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Intangible Virtue'),
    (select id from sets where short_name = 'c19'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodfell Caves'),
    (select id from sets where short_name = 'c19'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Secret Plans'),
    (select id from sets where short_name = 'c19'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Withering'),
    (select id from sets where short_name = 'c19'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desperate Ravings'),
    (select id from sets where short_name = 'c19'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Malevolent Whispers'),
    (select id from sets where short_name = 'c19'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'c19'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghired''s Belligerence'),
    (select id from sets where short_name = 'c19'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ray of Distortion'),
    (select id from sets where short_name = 'c19'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harmonize'),
    (select id from sets where short_name = 'c19'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Song of the Worldsoul'),
    (select id from sets where short_name = 'c19'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gorgon Recluse'),
    (select id from sets where short_name = 'c19'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greven, Predator Captain'),
    (select id from sets where short_name = 'c19'),
    '43',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Zombie Infestation'),
    (select id from sets where short_name = 'c19'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prairie Stream'),
    (select id from sets where short_name = 'c19'),
    '265',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clever Impersonator'),
    (select id from sets where short_name = 'c19'),
    '82',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Flamerush Rider'),
    (select id from sets where short_name = 'c19'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonmaster Outcast'),
    (select id from sets where short_name = 'c19'),
    '139',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Drownyard Temple'),
    (select id from sets where short_name = 'c19'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ixidron'),
    (select id from sets where short_name = 'c19'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Command Tower'),
    (select id from sets where short_name = 'c19'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marisi, Breaker of the Coil'),
    (select id from sets where short_name = 'c19'),
    '46',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Izzet Guildgate'),
    (select id from sets where short_name = 'c19'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barren Moor'),
    (select id from sets where short_name = 'c19'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Aqueduct'),
    (select id from sets where short_name = 'c19'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gerrard, Weatherlight Hero'),
    (select id from sets where short_name = 'c19'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis Reignited'),
    (select id from sets where short_name = 'c19'),
    '124',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Crackling Drake'),
    (select id from sets where short_name = 'c19'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chromeshell Crab'),
    (select id from sets where short_name = 'c19'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cliffside Rescuer'),
    (select id from sets where short_name = 'c19'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Momentous Fall'),
    (select id from sets where short_name = 'c19'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Refuse // Cooperate'),
    (select id from sets where short_name = 'c19'),
    '199',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Call to the Netherworld'),
    (select id from sets where short_name = 'c19'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chemister''s Insight'),
    (select id from sets where short_name = 'c19'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Divine Reckoning'),
    (select id from sets where short_name = 'c19'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burnished Hart'),
    (select id from sets where short_name = 'c19'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urban Evolution'),
    (select id from sets where short_name = 'c19'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desolation Twin'),
    (select id from sets where short_name = 'c19'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'From Under the Floorboards'),
    (select id from sets where short_name = 'c19'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c19'),
    '302',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kadena''s Silencer'),
    (select id from sets where short_name = 'c19'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghired, Conclave Exile'),
    (select id from sets where short_name = 'c19'),
    '42',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hour of Reckoning'),
    (select id from sets where short_name = 'c19'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c19'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mandate of Peace'),
    (select id from sets where short_name = 'c19'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Devil''s Play'),
    (select id from sets where short_name = 'c19'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Guildgate'),
    (select id from sets where short_name = 'c19'),
    '275',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c19'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slice in Twain'),
    (select id from sets where short_name = 'c19'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ash Barrens'),
    (select id from sets where short_name = 'c19'),
    '227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fact or Fiction'),
    (select id from sets where short_name = 'c19'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chainer, Nightmare Adept'),
    (select id from sets where short_name = 'c19'),
    '39',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Geier Reach Sanitarium'),
    (select id from sets where short_name = 'c19'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woodland Stream'),
    (select id from sets where short_name = 'c19'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Explore'),
    (select id from sets where short_name = 'c19'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghostly Prison'),
    (select id from sets where short_name = 'c19'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Idol of Oblivion'),
    (select id from sets where short_name = 'c19'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Garruk''s Packleader'),
    (select id from sets where short_name = 'c19'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ainok Survivalist'),
    (select id from sets where short_name = 'c19'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c19'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burning Vengeance'),
    (select id from sets where short_name = 'c19'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Faithless Looting'),
    (select id from sets where short_name = 'c19'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Purify the Grave'),
    (select id from sets where short_name = 'c19'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Icefeather Aven'),
    (select id from sets where short_name = 'c19'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kheru Spellsnatcher'),
    (select id from sets where short_name = 'c19'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunken Hollow'),
    (select id from sets where short_name = 'c19'),
    '278',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodhall Priest'),
    (select id from sets where short_name = 'c19'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c19'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doomed Artisan'),
    (select id from sets where short_name = 'c19'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skinthinner'),
    (select id from sets where short_name = 'c19'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nantuko Vigilante'),
    (select id from sets where short_name = 'c19'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oona''s Grace'),
    (select id from sets where short_name = 'c19'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seedborn Muse'),
    (select id from sets where short_name = 'c19'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sungrass Prairie'),
    (select id from sets where short_name = 'c19'),
    '277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thelonite Hermit'),
    (select id from sets where short_name = 'c19'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'c19'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Road of Return'),
    (select id from sets where short_name = 'c19'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dusk // Dawn'),
    (select id from sets where short_name = 'c19'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Full Flowering'),
    (select id from sets where short_name = 'c19'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aeon Engine'),
    (select id from sets where short_name = 'c19'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hex'),
    (select id from sets where short_name = 'c19'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Verge'),
    (select id from sets where short_name = 'c19'),
    '257',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Druid''s Deliverance'),
    (select id from sets where short_name = 'c19'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Wastes'),
    (select id from sets where short_name = 'c19'),
    '258',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rootborn Defenses'),
    (select id from sets where short_name = 'c19'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast Within'),
    (select id from sets where short_name = 'c19'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prismatic Strands'),
    (select id from sets where short_name = 'c19'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Champion of Stray Souls'),
    (select id from sets where short_name = 'c19'),
    '109',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hedonist''s Trove'),
    (select id from sets where short_name = 'c19'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel of Sanctions'),
    (select id from sets where short_name = 'c19'),
    '61',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Zetalpa, Primal Dawn'),
    (select id from sets where short_name = 'c19'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c19'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meteor Golem'),
    (select id from sets where short_name = 'c19'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Atla Palani, Nest Tender'),
    (select id from sets where short_name = 'c19'),
    '38',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Den Protector'),
    (select id from sets where short_name = 'c19'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thousand Winds'),
    (select id from sets where short_name = 'c19'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Asylum Visitor'),
    (select id from sets where short_name = 'c19'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voice of Many'),
    (select id from sets where short_name = 'c19'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rix Maadi, Dungeon Palace'),
    (select id from sets where short_name = 'c19'),
    '269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cultivate'),
    (select id from sets where short_name = 'c19'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fiery Temper'),
    (select id from sets where short_name = 'c19'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Farm // Market'),
    (select id from sets where short_name = 'c19'),
    '192',
    'uncommon'
) 
 on conflict do nothing;
