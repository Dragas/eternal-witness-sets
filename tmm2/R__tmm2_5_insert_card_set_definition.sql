insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tmm2'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Worm'),
    (select id from sets where short_name = 'tmm2'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snake'),
    (select id from sets where short_name = 'tmm2'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elephant'),
    (select id from sets where short_name = 'tmm2'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tmm2'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Germ'),
    (select id from sets where short_name = 'tmm2'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insect'),
    (select id from sets where short_name = 'tmm2'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrull'),
    (select id from sets where short_name = 'tmm2'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'tmm2'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Rogue'),
    (select id from sets where short_name = 'tmm2'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golem'),
    (select id from sets where short_name = 'tmm2'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Spawn'),
    (select id from sets where short_name = 'tmm2'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr'),
    (select id from sets where short_name = 'tmm2'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Spawn'),
    (select id from sets where short_name = 'tmm2'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tmm2'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Spawn'),
    (select id from sets where short_name = 'tmm2'),
    '3',
    'common'
) 
 on conflict do nothing;
