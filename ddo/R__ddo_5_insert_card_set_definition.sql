insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Kiora, the Crashing Wave'),
    (select id from sets where short_name = 'ddo'),
    '34',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Soul Parry'),
    (select id from sets where short_name = 'ddo'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dauntless Onslaught'),
    (select id from sets where short_name = 'ddo'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'ddo'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Javelineers'),
    (select id from sets where short_name = 'ddo'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Banisher Priest'),
    (select id from sets where short_name = 'ddo'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nimbus Swimmer'),
    (select id from sets where short_name = 'ddo'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kinsbaile Skirmisher'),
    (select id from sets where short_name = 'ddo'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddo'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddo'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Man-o''-War'),
    (select id from sets where short_name = 'ddo'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time to Feed'),
    (select id from sets where short_name = 'ddo'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gempalm Avenger'),
    (select id from sets where short_name = 'ddo'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddo'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plasm Capture'),
    (select id from sets where short_name = 'ddo'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddo'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coiling Oracle'),
    (select id from sets where short_name = 'ddo'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'ddo'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gustcloak Harrier'),
    (select id from sets where short_name = 'ddo'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scourge of Fleets'),
    (select id from sets where short_name = 'ddo'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lorescale Coatl'),
    (select id from sets where short_name = 'ddo'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddo'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gustcloak Savior'),
    (select id from sets where short_name = 'ddo'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Decree of Justice'),
    (select id from sets where short_name = 'ddo'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Veteran Armorsmith'),
    (select id from sets where short_name = 'ddo'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Standing Troops'),
    (select id from sets where short_name = 'ddo'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urban Evolution'),
    (select id from sets where short_name = 'ddo'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddo'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Accumulated Knowledge'),
    (select id from sets where short_name = 'ddo'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunlance'),
    (select id from sets where short_name = 'ddo'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Captain of the Watch'),
    (select id from sets where short_name = 'ddo'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddo'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mortal''s Ardor'),
    (select id from sets where short_name = 'ddo'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nessian Asp'),
    (select id from sets where short_name = 'ddo'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aetherize'),
    (select id from sets where short_name = 'ddo'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Explosive Vegetation'),
    (select id from sets where short_name = 'ddo'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raise the Alarm'),
    (select id from sets where short_name = 'ddo'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddo'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elspeth, Sun''s Champion'),
    (select id from sets where short_name = 'ddo'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Temple of the False God'),
    (select id from sets where short_name = 'ddo'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Noble Templar'),
    (select id from sets where short_name = 'ddo'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mighty Leap'),
    (select id from sets where short_name = 'ddo'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inkwell Leviathan'),
    (select id from sets where short_name = 'ddo'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loxodon Partisan'),
    (select id from sets where short_name = 'ddo'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whelming Wave'),
    (select id from sets where short_name = 'ddo'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Omenspeaker'),
    (select id from sets where short_name = 'ddo'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Skyfisher'),
    (select id from sets where short_name = 'ddo'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dictate of Heliod'),
    (select id from sets where short_name = 'ddo'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grazing Gladehart'),
    (select id from sets where short_name = 'ddo'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simic Sky Swallower'),
    (select id from sets where short_name = 'ddo'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Court Street Denizen'),
    (select id from sets where short_name = 'ddo'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Explore'),
    (select id from sets where short_name = 'ddo'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Surrakar Banisher'),
    (select id from sets where short_name = 'ddo'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gustcloak Skirmisher'),
    (select id from sets where short_name = 'ddo'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mother of Runes'),
    (select id from sets where short_name = 'ddo'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sealock Monster'),
    (select id from sets where short_name = 'ddo'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kraken'),
    (select id from sets where short_name = 'ddo'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kiora''s Follower'),
    (select id from sets where short_name = 'ddo'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Secluded Steppe'),
    (select id from sets where short_name = 'ddo'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Precinct Captain'),
    (select id from sets where short_name = 'ddo'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Veteran Swordsmith'),
    (select id from sets where short_name = 'ddo'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddo'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Celestial Flare'),
    (select id from sets where short_name = 'ddo'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Netcaster Spider'),
    (select id from sets where short_name = 'ddo'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gustcloak Sentinel'),
    (select id from sets where short_name = 'ddo'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddo'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Peel from Reality'),
    (select id from sets where short_name = 'ddo'),
    '40',
    'common'
) 
 on conflict do nothing;
