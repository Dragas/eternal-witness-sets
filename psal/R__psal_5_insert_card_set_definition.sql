insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Magma Sliver'),
    (select id from sets where short_name = 'psal'),
    'D1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Caller of the Claw'),
    (select id from sets where short_name = 'psal'),
    'B37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lure'),
    (select id from sets where short_name = 'psal'),
    'K16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'G9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guardian''s Magemark'),
    (select id from sets where short_name = 'psal'),
    'G5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'C60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clutch of the Undercity'),
    (select id from sets where short_name = 'psal'),
    'E14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'G24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nantuko Husk'),
    (select id from sets where short_name = 'psal'),
    'F3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'E57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Warden'),
    (select id from sets where short_name = 'psal'),
    'J27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'I11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Chariot'),
    (select id from sets where short_name = 'psal'),
    'I17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nikko-Onna'),
    (select id from sets where short_name = 'psal'),
    'C2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kirtar''s Wrath'),
    (select id from sets where short_name = 'psal'),
    'C37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'K60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Festering Goblin'),
    (select id from sets where short_name = 'psal'),
    'F6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wizard Replica'),
    (select id from sets where short_name = 'psal'),
    'H19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'K59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Evangel'),
    (select id from sets where short_name = 'psal'),
    'J55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horror of Horrors'),
    (select id from sets where short_name = 'psal'),
    'F26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'psal'),
    'I54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Patrol'),
    (select id from sets where short_name = 'psal'),
    'L54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'J23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'J57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Banishing'),
    (select id from sets where short_name = 'psal'),
    'F53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Indrik Stomphowler'),
    (select id from sets where short_name = 'psal'),
    'K51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Iron-Barb Hellion'),
    (select id from sets where short_name = 'psal'),
    'K4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thran Golem'),
    (select id from sets where short_name = 'psal'),
    'G13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghor-Clan Savage'),
    (select id from sets where short_name = 'psal'),
    'I29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Engulfing Flames'),
    (select id from sets where short_name = 'psal'),
    'D5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slagwurm Armor'),
    (select id from sets where short_name = 'psal'),
    'L43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'K58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iron-Barb Hellion'),
    (select id from sets where short_name = 'psal'),
    'K3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Call for Blood'),
    (select id from sets where short_name = 'psal'),
    'F52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saltblast'),
    (select id from sets where short_name = 'psal'),
    'L28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'D24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'E21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thallid Germinator'),
    (select id from sets where short_name = 'psal'),
    'J43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crown of Vigor'),
    (select id from sets where short_name = 'psal'),
    'B54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Denied'),
    (select id from sets where short_name = 'psal'),
    'F42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Throat Slitter'),
    (select id from sets where short_name = 'psal'),
    'E3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Throat Slitter'),
    (select id from sets where short_name = 'psal'),
    'E2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'D60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crypt Sliver'),
    (select id from sets where short_name = 'psal'),
    'D29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Skirmisher'),
    (select id from sets where short_name = 'psal'),
    'L3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Waxmane Baku'),
    (select id from sets where short_name = 'psal'),
    'C32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Specter''s Shroud'),
    (select id from sets where short_name = 'psal'),
    'L15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vedalken Entrancer'),
    (select id from sets where short_name = 'psal'),
    'H18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Nomad'),
    (select id from sets where short_name = 'psal'),
    'C17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Booby Trap'),
    (select id from sets where short_name = 'psal'),
    'H49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vulshok Gauntlets'),
    (select id from sets where short_name = 'psal'),
    'L31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Den-Guard'),
    (select id from sets where short_name = 'psal'),
    'L29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'L59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Timberwatch Elf'),
    (select id from sets where short_name = 'psal'),
    'B18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Confiscate'),
    (select id from sets where short_name = 'psal'),
    'G39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raging Goblin'),
    (select id from sets where short_name = 'psal'),
    'I6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soratami Rainshaper'),
    (select id from sets where short_name = 'psal'),
    'H43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Cub'),
    (select id from sets where short_name = 'psal'),
    'L42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kamahl, Pit Fighter'),
    (select id from sets where short_name = 'psal'),
    'I1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primal Forcemage'),
    (select id from sets where short_name = 'psal'),
    'I52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'I44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Threaten'),
    (select id from sets where short_name = 'psal'),
    'I26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'L22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scatter the Seeds'),
    (select id from sets where short_name = 'psal'),
    'J18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'J48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Guildmage'),
    (select id from sets where short_name = 'psal'),
    'B51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'psal'),
    'L8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keening Banshee'),
    (select id from sets where short_name = 'psal'),
    'E15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crown of Vigor'),
    (select id from sets where short_name = 'psal'),
    'D6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coastal Piracy'),
    (select id from sets where short_name = 'psal'),
    'E49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'I46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elite'),
    (select id from sets where short_name = 'psal'),
    'B17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'L57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Chariot'),
    (select id from sets where short_name = 'psal'),
    'I4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'C46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'D48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'K21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tears of Rage'),
    (select id from sets where short_name = 'psal'),
    'I3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wirewood Pride'),
    (select id from sets where short_name = 'psal'),
    'B55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'K45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frostling'),
    (select id from sets where short_name = 'psal'),
    'C6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'K34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Belltower Sphinx'),
    (select id from sets where short_name = 'psal'),
    'H14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'I32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darigaaz''s Charm'),
    (select id from sets where short_name = 'psal'),
    'D39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'D12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wirewood Hivemaster'),
    (select id from sets where short_name = 'psal'),
    'B4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merfolk of the Pearl Trident'),
    (select id from sets where short_name = 'psal'),
    'E43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'E56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ardent Militia'),
    (select id from sets where short_name = 'psal'),
    'G26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'L58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carrion Rats'),
    (select id from sets where short_name = 'psal'),
    'A53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'psal'),
    'F55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'E10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'E9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'J45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = 'psal'),
    'D55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saltblast'),
    (select id from sets where short_name = 'psal'),
    'L53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azami, Lady of Scrolls'),
    (select id from sets where short_name = 'psal'),
    'H1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'D22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Flock'),
    (select id from sets where short_name = 'psal'),
    'C3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Indrik Stomphowler'),
    (select id from sets where short_name = 'psal'),
    'K38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treetop Scout'),
    (select id from sets where short_name = 'psal'),
    'B5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tower of Murmurs'),
    (select id from sets where short_name = 'psal'),
    'H25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primal Forcemage'),
    (select id from sets where short_name = 'psal'),
    'I39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blind Creeper'),
    (select id from sets where short_name = 'psal'),
    'F29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Banishing'),
    (select id from sets where short_name = 'psal'),
    'F41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vedalken Entrancer'),
    (select id from sets where short_name = 'psal'),
    'H44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Pit Offering'),
    (select id from sets where short_name = 'psal'),
    'F25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'J47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Juniper Order Ranger'),
    (select id from sets where short_name = 'psal'),
    'J15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keldon Mantle'),
    (select id from sets where short_name = 'psal'),
    'D54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Auratouched Mage'),
    (select id from sets where short_name = 'psal'),
    'G27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feral Lightning'),
    (select id from sets where short_name = 'psal'),
    'I50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ursapine'),
    (select id from sets where short_name = 'psal'),
    'K49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aven Windreader'),
    (select id from sets where short_name = 'psal'),
    'H52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Root Sliver'),
    (select id from sets where short_name = 'psal'),
    'D15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'C9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Walker of Secret Ways'),
    (select id from sets where short_name = 'psal'),
    'E52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Warden'),
    (select id from sets where short_name = 'psal'),
    'J14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tolsimir Wolfblood'),
    (select id from sets where short_name = 'psal'),
    'J1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Minamo Scrollkeeper'),
    (select id from sets where short_name = 'psal'),
    'H6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'D33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'J44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clutch of the Undercity'),
    (select id from sets where short_name = 'psal'),
    'E38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necromantic Thirst'),
    (select id from sets where short_name = 'psal'),
    'F44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'I20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blade Sliver'),
    (select id from sets where short_name = 'psal'),
    'D3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marrow-Gnawer'),
    (select id from sets where short_name = 'psal'),
    'A1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nezumi Graverobber // Nighteyes the Desecrator'),
    (select id from sets where short_name = 'psal'),
    'A38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'E12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skarrg, the Rage Pits'),
    (select id from sets where short_name = 'psal'),
    'I28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'I36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Martyr of Frost'),
    (select id from sets where short_name = 'psal'),
    'H5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flowstone Shambler'),
    (select id from sets where short_name = 'psal'),
    'K53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swarm of Rats'),
    (select id from sets where short_name = 'psal'),
    'A14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gruul Scrapper'),
    (select id from sets where short_name = 'psal'),
    'I43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wirewood Channeler'),
    (select id from sets where short_name = 'psal'),
    'B27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stir the Pride'),
    (select id from sets where short_name = 'psal'),
    'C52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'K12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Supply // Demand'),
    (select id from sets where short_name = 'psal'),
    'J16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'psal'),
    'L34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Festering Goblin'),
    (select id from sets where short_name = 'psal'),
    'F17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nullmage Shepherd'),
    (select id from sets where short_name = 'psal'),
    'B2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'E46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Freewind Equenaut'),
    (select id from sets where short_name = 'psal'),
    'G41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Signet'),
    (select id from sets where short_name = 'psal'),
    'L55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soratami Mindsweeper'),
    (select id from sets where short_name = 'psal'),
    'H50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skullsnatcher'),
    (select id from sets where short_name = 'psal'),
    'A43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pain Kami'),
    (select id from sets where short_name = 'psal'),
    'C28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nezumi Cutthroat'),
    (select id from sets where short_name = 'psal'),
    'A29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fertile Imagination'),
    (select id from sets where short_name = 'psal'),
    'J2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vedalken Entrancer'),
    (select id from sets where short_name = 'psal'),
    'H7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raging Goblin'),
    (select id from sets where short_name = 'psal'),
    'I7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shinen of Fury''s Fire'),
    (select id from sets where short_name = 'psal'),
    'C42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ninja of the Deep Hours'),
    (select id from sets where short_name = 'psal'),
    'E30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'C56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Birchlore Rangers'),
    (select id from sets where short_name = 'psal'),
    'B52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blademane Baku'),
    (select id from sets where short_name = 'psal'),
    'C4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blade Sliver'),
    (select id from sets where short_name = 'psal'),
    'D2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Groffskithur'),
    (select id from sets where short_name = 'psal'),
    'K30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Timberwatch Elf'),
    (select id from sets where short_name = 'psal'),
    'B19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bonesplitter'),
    (select id from sets where short_name = 'psal'),
    'L40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'G22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Skirmisher'),
    (select id from sets where short_name = 'psal'),
    'L39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molder Slug'),
    (select id from sets where short_name = 'psal'),
    'K37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vedalken Entrancer'),
    (select id from sets where short_name = 'psal'),
    'H31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sage Owl'),
    (select id from sets where short_name = 'psal'),
    'E31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'J21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'J60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit Link'),
    (select id from sets where short_name = 'psal'),
    'C40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantom Nomad'),
    (select id from sets where short_name = 'psal'),
    'C7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Indrik Stomphowler'),
    (select id from sets where short_name = 'psal'),
    'I38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit Link'),
    (select id from sets where short_name = 'psal'),
    'G3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spectral Sliver'),
    (select id from sets where short_name = 'psal'),
    'D27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serpent Skin'),
    (select id from sets where short_name = 'psal'),
    'K44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Page'),
    (select id from sets where short_name = 'psal'),
    'C14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dance of Shadows'),
    (select id from sets where short_name = 'psal'),
    'F50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seht''s Tiger'),
    (select id from sets where short_name = 'psal'),
    'L13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'L47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flowstone Shambler'),
    (select id from sets where short_name = 'psal'),
    'K7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Elder'),
    (select id from sets where short_name = 'psal'),
    'L51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spineless Thug'),
    (select id from sets where short_name = 'psal'),
    'F18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elite'),
    (select id from sets where short_name = 'psal'),
    'B16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nezumi Bone-Reader'),
    (select id from sets where short_name = 'psal'),
    'A50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Infiltrator'),
    (select id from sets where short_name = 'psal'),
    'E4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thelonite Hermit'),
    (select id from sets where short_name = 'psal'),
    'J49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'psal'),
    'F7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'G48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Walker of Secret Ways'),
    (select id from sets where short_name = 'psal'),
    'E51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'J10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kitsune Mystic // Autumn-Tail, Kitsune Sage'),
    (select id from sets where short_name = 'psal'),
    'G37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'D46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'G35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horizon Seed'),
    (select id from sets where short_name = 'psal'),
    'C38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Banishing'),
    (select id from sets where short_name = 'psal'),
    'D32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Patrol'),
    (select id from sets where short_name = 'psal'),
    'L19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'I22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul of Magma'),
    (select id from sets where short_name = 'psal'),
    'C18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'psal'),
    'F43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Vanguard'),
    (select id from sets where short_name = 'psal'),
    'B13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'E35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'I23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vedalken Dismisser'),
    (select id from sets where short_name = 'psal'),
    'H54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'K24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'L23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Betrayal of Flesh'),
    (select id from sets where short_name = 'psal'),
    'A52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seed Spark'),
    (select id from sets where short_name = 'psal'),
    'J40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'G60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Freewind Equenaut'),
    (select id from sets where short_name = 'psal'),
    'G17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'K10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Toxin Sliver'),
    (select id from sets where short_name = 'psal'),
    'D25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plagued Rusalka'),
    (select id from sets where short_name = 'psal'),
    'F28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Mercy'),
    (select id from sets where short_name = 'psal'),
    'G14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savage Thallid'),
    (select id from sets where short_name = 'psal'),
    'J5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ninja of the Deep Hours'),
    (select id from sets where short_name = 'psal'),
    'E6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Okiba-Gang Shinobi'),
    (select id from sets where short_name = 'psal'),
    'E53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Signet'),
    (select id from sets where short_name = 'psal'),
    'L41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fangren Hunter'),
    (select id from sets where short_name = 'psal'),
    'K29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vedalken Mastermind'),
    (select id from sets where short_name = 'psal'),
    'H51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Carrion Rats'),
    (select id from sets where short_name = 'psal'),
    'A41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vedalken Dismisser'),
    (select id from sets where short_name = 'psal'),
    'H55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Root Sliver'),
    (select id from sets where short_name = 'psal'),
    'D14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crown of Ascension'),
    (select id from sets where short_name = 'psal'),
    'E28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Threaten'),
    (select id from sets where short_name = 'psal'),
    'I27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pain Kami'),
    (select id from sets where short_name = 'psal'),
    'C29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rhox'),
    (select id from sets where short_name = 'psal'),
    'K1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyrider Trainee'),
    (select id from sets where short_name = 'psal'),
    'G54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Detainment Spell'),
    (select id from sets where short_name = 'psal'),
    'J32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'G8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'I59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'J24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Freewind Equenaut'),
    (select id from sets where short_name = 'psal'),
    'G4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rats'' Feast'),
    (select id from sets where short_name = 'psal'),
    'A55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seeker of Skybreak'),
    (select id from sets where short_name = 'psal'),
    'B30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blind Creeper'),
    (select id from sets where short_name = 'psal'),
    'F4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'K35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyroclasm'),
    (select id from sets where short_name = 'psal'),
    'K27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Suppress'),
    (select id from sets where short_name = 'psal'),
    'A26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leonin Bola'),
    (select id from sets where short_name = 'psal'),
    'L16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'D47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'I35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soulsworn Jury'),
    (select id from sets where short_name = 'psal'),
    'G43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanguine Praetor'),
    (select id from sets where short_name = 'psal'),
    'F49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'G36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hinder'),
    (select id from sets where short_name = 'psal'),
    'H38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'G46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'G21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scab-Clan Mauler'),
    (select id from sets where short_name = 'psal'),
    'I56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Puppeteer'),
    (select id from sets where short_name = 'psal'),
    'H2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'One Dozen Eyes'),
    (select id from sets where short_name = 'psal'),
    'K40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'psal'),
    'G42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Fire Fiend'),
    (select id from sets where short_name = 'psal'),
    'I5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savage Offensive'),
    (select id from sets where short_name = 'psal'),
    'D18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quick Sliver'),
    (select id from sets where short_name = 'psal'),
    'D42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shinen of Fury''s Fire'),
    (select id from sets where short_name = 'psal'),
    'C55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'J9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Warrior'),
    (select id from sets where short_name = 'psal'),
    'B41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nacatl War-Pride'),
    (select id from sets where short_name = 'psal'),
    'L38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skullsnatcher'),
    (select id from sets where short_name = 'psal'),
    'E19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'D58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fertile Ground'),
    (select id from sets where short_name = 'psal'),
    'K6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'K8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blademane Baku'),
    (select id from sets where short_name = 'psal'),
    'C30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'K22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spectral Sliver'),
    (select id from sets where short_name = 'psal'),
    'D28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ardent Militia'),
    (select id from sets where short_name = 'psal'),
    'G15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ambush Commander'),
    (select id from sets where short_name = 'psal'),
    'B25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wayfarer''s Bauble'),
    (select id from sets where short_name = 'psal'),
    'D44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'D23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deflection'),
    (select id from sets where short_name = 'psal'),
    'E37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sinstriker''s Will'),
    (select id from sets where short_name = 'psal'),
    'G16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Infiltrator''s Magemark'),
    (select id from sets where short_name = 'psal'),
    'G19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yuki-Onna'),
    (select id from sets where short_name = 'psal'),
    'C41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Striker'),
    (select id from sets where short_name = 'psal'),
    'I19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soratami Mindsweeper'),
    (select id from sets where short_name = 'psal'),
    'H3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Taj-Nar Swordsmith'),
    (select id from sets where short_name = 'psal'),
    'L4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'K32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'C8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aphetto Dredging'),
    (select id from sets where short_name = 'psal'),
    'D40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'K20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk of the Pearl Trident'),
    (select id from sets where short_name = 'psal'),
    'E42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boomerang'),
    (select id from sets where short_name = 'psal'),
    'H16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Greaves'),
    (select id from sets where short_name = 'psal'),
    'L2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'D35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wojek Apothecary'),
    (select id from sets where short_name = 'psal'),
    'J50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'C21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urborg Emissary'),
    (select id from sets where short_name = 'psal'),
    'E50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'K56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mesmeric Orb'),
    (select id from sets where short_name = 'psal'),
    'H13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestial Kirin'),
    (select id from sets where short_name = 'psal'),
    'C25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sandsower'),
    (select id from sets where short_name = 'psal'),
    'J26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Voice of the Woods'),
    (select id from sets where short_name = 'psal'),
    'B1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'J58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treetop Scout'),
    (select id from sets where short_name = 'psal'),
    'B6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Patron of the Nezumi'),
    (select id from sets where short_name = 'psal'),
    'A25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'J8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crippling Fatigue'),
    (select id from sets where short_name = 'psal'),
    'A30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Wings'),
    (select id from sets where short_name = 'psal'),
    'E55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sandsower'),
    (select id from sets where short_name = 'psal'),
    'C51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nocturnal Raid'),
    (select id from sets where short_name = 'psal'),
    'A3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Cub'),
    (select id from sets where short_name = 'psal'),
    'L18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'D11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'L24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grifter''s Blade'),
    (select id from sets where short_name = 'psal'),
    'L27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'I60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loxodon Stalwart'),
    (select id from sets where short_name = 'psal'),
    'G2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'K46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'J33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Den-Guard'),
    (select id from sets where short_name = 'psal'),
    'L30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Messenger'),
    (select id from sets where short_name = 'psal'),
    'B15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doomsday Specter'),
    (select id from sets where short_name = 'psal'),
    'E13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightscape Battlemage'),
    (select id from sets where short_name = 'psal'),
    'E39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Signet'),
    (select id from sets where short_name = 'psal'),
    'L56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'C20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Martyr of Frost'),
    (select id from sets where short_name = 'psal'),
    'H17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'L21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'C44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dimir Guildmage'),
    (select id from sets where short_name = 'psal'),
    'H15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'J35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = 'psal'),
    'K28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plagued Rusalka'),
    (select id from sets where short_name = 'psal'),
    'F40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'G33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guardian''s Magemark'),
    (select id from sets where short_name = 'psal'),
    'G52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'C35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Chariot'),
    (select id from sets where short_name = 'psal'),
    'I18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crypt Sliver'),
    (select id from sets where short_name = 'psal'),
    'D30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Predator''s Strike'),
    (select id from sets where short_name = 'psal'),
    'K52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shinen of Fury''s Fire'),
    (select id from sets where short_name = 'psal'),
    'C54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spineless Thug'),
    (select id from sets where short_name = 'psal'),
    'F19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'I10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'J12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'psal'),
    'I53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terrarion'),
    (select id from sets where short_name = 'psal'),
    'D19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nezumi Graverobber // Nighteyes the Desecrator'),
    (select id from sets where short_name = 'psal'),
    'A39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karplusan Wolverine'),
    (select id from sets where short_name = 'psal'),
    'K18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = 'psal'),
    'K19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'G20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'E36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunter Sliver'),
    (select id from sets where short_name = 'psal'),
    'D53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terrarion'),
    (select id from sets where short_name = 'psal'),
    'D20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arctic Nishoba'),
    (select id from sets where short_name = 'psal'),
    'L26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wayfarer''s Bauble'),
    (select id from sets where short_name = 'psal'),
    'D43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strength in Numbers'),
    (select id from sets where short_name = 'psal'),
    'J52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'D9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diabolic Tutor'),
    (select id from sets where short_name = 'psal'),
    'A51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'K9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'K11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leyline of the Meek'),
    (select id from sets where short_name = 'psal'),
    'J13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soratami Mindsweeper'),
    (select id from sets where short_name = 'psal'),
    'H28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'C59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Evangel'),
    (select id from sets where short_name = 'psal'),
    'J54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wirewood Hivemaster'),
    (select id from sets where short_name = 'psal'),
    'B3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'I33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Festering Goblin'),
    (select id from sets where short_name = 'psal'),
    'F5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thallid Germinator'),
    (select id from sets where short_name = 'psal'),
    'J19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Slash'),
    (select id from sets where short_name = 'psal'),
    'F14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gluttonous Zombie'),
    (select id from sets where short_name = 'psal'),
    'F2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'C36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wirewood Herald'),
    (select id from sets where short_name = 'psal'),
    'B43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quick Sliver'),
    (select id from sets where short_name = 'psal'),
    'D16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodscent'),
    (select id from sets where short_name = 'psal'),
    'D4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whitemane Lion'),
    (select id from sets where short_name = 'psal'),
    'L32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skullsnatcher'),
    (select id from sets where short_name = 'psal'),
    'E17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heedless One'),
    (select id from sets where short_name = 'psal'),
    'B39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Genju of the Falls'),
    (select id from sets where short_name = 'psal'),
    'G40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vacuumelt'),
    (select id from sets where short_name = 'psal'),
    'H4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'E34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'D10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alpha Status'),
    (select id from sets where short_name = 'psal'),
    'B38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'E22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'G45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'J11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'C11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyrider Trainee'),
    (select id from sets where short_name = 'psal'),
    'G6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'I47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'G58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Hammer'),
    (select id from sets where short_name = 'psal'),
    'K31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'I48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grifter''s Blade'),
    (select id from sets where short_name = 'psal'),
    'L50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'J22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raksha Golden Cub'),
    (select id from sets where short_name = 'psal'),
    'L49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'L12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strength in Numbers'),
    (select id from sets where short_name = 'psal'),
    'J42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swarm of Rats'),
    (select id from sets where short_name = 'psal'),
    'A2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightguard Patrol'),
    (select id from sets where short_name = 'psal'),
    'G29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'J46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghor-Clan Savage'),
    (select id from sets where short_name = 'psal'),
    'I55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul Nodorog'),
    (select id from sets where short_name = 'psal'),
    'K54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nantuko Husk'),
    (select id from sets where short_name = 'psal'),
    'F15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'C47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dirty Wererat'),
    (select id from sets where short_name = 'psal'),
    'A31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'psal'),
    'L45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'C24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'K47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kami of the Palace Fields'),
    (select id from sets where short_name = 'psal'),
    'C50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hunter Sliver'),
    (select id from sets where short_name = 'psal'),
    'D8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blade Sliver'),
    (select id from sets where short_name = 'psal'),
    'D52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burning-Tree Bloodscale'),
    (select id from sets where short_name = 'psal'),
    'I30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunted Wumpus'),
    (select id from sets where short_name = 'psal'),
    'K26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul of Magma'),
    (select id from sets where short_name = 'psal'),
    'C19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Sanctuary'),
    (select id from sets where short_name = 'psal'),
    'L44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spineless Thug'),
    (select id from sets where short_name = 'psal'),
    'F31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darigaaz''s Charm'),
    (select id from sets where short_name = 'psal'),
    'D51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volcanic Hammer'),
    (select id from sets where short_name = 'psal'),
    'C43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brood Sliver'),
    (select id from sets where short_name = 'psal'),
    'D13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jedit Ojanen of Efrava'),
    (select id from sets where short_name = 'psal'),
    'L1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'G32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savage Thallid'),
    (select id from sets where short_name = 'psal'),
    'J4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'C45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wellwisher'),
    (select id from sets where short_name = 'psal'),
    'B7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fangren Pathcutter'),
    (select id from sets where short_name = 'psal'),
    'K14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'I57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fertile Imagination'),
    (select id from sets where short_name = 'psal'),
    'J38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lovisa Coldeyes'),
    (select id from sets where short_name = 'psal'),
    'I13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aether Shockwave'),
    (select id from sets where short_name = 'psal'),
    'C26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'psal'),
    'G53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'E8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wirewood Pride'),
    (select id from sets where short_name = 'psal'),
    'B32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chittering Rats'),
    (select id from sets where short_name = 'psal'),
    'A54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'D21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'D34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primal Forcemage'),
    (select id from sets where short_name = 'psal'),
    'I40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Strength in Numbers'),
    (select id from sets where short_name = 'psal'),
    'J53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Hammer'),
    (select id from sets where short_name = 'psal'),
    'K17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'G59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'J36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fertile Ground'),
    (select id from sets where short_name = 'psal'),
    'K5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravestorm'),
    (select id from sets where short_name = 'psal'),
    'A37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swarm of Rats'),
    (select id from sets where short_name = 'psal'),
    'A27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feral Lightning'),
    (select id from sets where short_name = 'psal'),
    'I2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'G11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kodama''s Reach'),
    (select id from sets where short_name = 'psal'),
    'K43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sever Soul'),
    (select id from sets where short_name = 'psal'),
    'A15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darigaaz''s Caldera'),
    (select id from sets where short_name = 'psal'),
    'D26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sever Soul'),
    (select id from sets where short_name = 'psal'),
    'A16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'I21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balduvian Rage'),
    (select id from sets where short_name = 'psal'),
    'I14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'C34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Banishing'),
    (select id from sets where short_name = 'psal'),
    'D31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'C10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jetting Glasskite'),
    (select id from sets where short_name = 'psal'),
    'H27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'E59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chittering Rats'),
    (select id from sets where short_name = 'psal'),
    'A5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golgari Guildmage'),
    (select id from sets where short_name = 'psal'),
    'F39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bramble Elemental'),
    (select id from sets where short_name = 'psal'),
    'J29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dimir Infiltrator'),
    (select id from sets where short_name = 'psal'),
    'E5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'I12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kami of the Palace Fields'),
    (select id from sets where short_name = 'psal'),
    'C27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nantuko Husk'),
    (select id from sets where short_name = 'psal'),
    'F27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skullsnatcher'),
    (select id from sets where short_name = 'psal'),
    'A42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Groffskithur'),
    (select id from sets where short_name = 'psal'),
    'K55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'L11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dirge of Dread'),
    (select id from sets where short_name = 'psal'),
    'A18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'E60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vesper Ghoul'),
    (select id from sets where short_name = 'psal'),
    'E44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Groffskithur'),
    (select id from sets where short_name = 'psal'),
    'K42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Higure, the Still Wind'),
    (select id from sets where short_name = 'psal'),
    'E1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'E33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crypt Sliver'),
    (select id from sets where short_name = 'psal'),
    'D50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Twilight Drover'),
    (select id from sets where short_name = 'psal'),
    'J37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sage Owl'),
    (select id from sets where short_name = 'psal'),
    'E7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plumes of Peace'),
    (select id from sets where short_name = 'psal'),
    'G31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undying Rage'),
    (select id from sets where short_name = 'psal'),
    'I51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'C12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Okiba-Gang Shinobi'),
    (select id from sets where short_name = 'psal'),
    'E54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirri, Cat Warrior'),
    (select id from sets where short_name = 'psal'),
    'L25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cephalid Constable'),
    (select id from sets where short_name = 'psal'),
    'E25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beast of Burden'),
    (select id from sets where short_name = 'psal'),
    'J25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Genju of the Fields'),
    (select id from sets where short_name = 'psal'),
    'G28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'J20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'J56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'K23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Guildmage'),
    (select id from sets where short_name = 'psal'),
    'J51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blademane Baku'),
    (select id from sets where short_name = 'psal'),
    'C53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'E20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Groffskithur'),
    (select id from sets where short_name = 'psal'),
    'K41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vulshok Sorcerer'),
    (select id from sets where short_name = 'psal'),
    'I8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aphetto Dredging'),
    (select id from sets where short_name = 'psal'),
    'D41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soratami Rainshaper'),
    (select id from sets where short_name = 'psal'),
    'H30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Supply // Demand'),
    (select id from sets where short_name = 'psal'),
    'J28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Waxmane Baku'),
    (select id from sets where short_name = 'psal'),
    'C31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'D36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enormous Baloth'),
    (select id from sets where short_name = 'psal'),
    'K2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thundermare'),
    (select id from sets where short_name = 'psal'),
    'I49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selesnya Sanctuary'),
    (select id from sets where short_name = 'psal'),
    'L33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crushing Pain'),
    (select id from sets where short_name = 'psal'),
    'C15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Messenger'),
    (select id from sets where short_name = 'psal'),
    'B14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'E11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'C48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golgari Guildmage'),
    (select id from sets where short_name = 'psal'),
    'B50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'K57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marsh Crocodile'),
    (select id from sets where short_name = 'psal'),
    'E27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'D45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scab-Clan Mauler'),
    (select id from sets where short_name = 'psal'),
    'I31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'I58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunter Sliver'),
    (select id from sets where short_name = 'psal'),
    'D7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'L10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riptide Replicator'),
    (select id from sets where short_name = 'psal'),
    'D49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quick Sliver'),
    (select id from sets where short_name = 'psal'),
    'D17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'L9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Shikari'),
    (select id from sets where short_name = 'psal'),
    'L37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carrion Howler'),
    (select id from sets where short_name = 'psal'),
    'F38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'L60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arctic Nishoba'),
    (select id from sets where short_name = 'psal'),
    'L14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chittering Rats'),
    (select id from sets where short_name = 'psal'),
    'A19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Retribution'),
    (select id from sets where short_name = 'psal'),
    'G1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infernal Contract'),
    (select id from sets where short_name = 'psal'),
    'A13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thallid Shell-Dweller'),
    (select id from sets where short_name = 'psal'),
    'J6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistblade Shinobi'),
    (select id from sets where short_name = 'psal'),
    'E29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'E47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'I34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oyobi, Who Split the Heavens'),
    (select id from sets where short_name = 'psal'),
    'C1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'G12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stalking Vengeance'),
    (select id from sets where short_name = 'psal'),
    'I25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death Denied'),
    (select id from sets where short_name = 'psal'),
    'F54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormscape Battlemage'),
    (select id from sets where short_name = 'psal'),
    'E40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woebearer'),
    (select id from sets where short_name = 'psal'),
    'F51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'C22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dehydration'),
    (select id from sets where short_name = 'psal'),
    'H42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thallid Shell-Dweller'),
    (select id from sets where short_name = 'psal'),
    'J7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'I9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blinking Spirit'),
    (select id from sets where short_name = 'psal'),
    'C49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frostling'),
    (select id from sets where short_name = 'psal'),
    'C16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Struggle for Sanity'),
    (select id from sets where short_name = 'psal'),
    'E16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'G10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lure'),
    (select id from sets where short_name = 'psal'),
    'K39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul Scrapper'),
    (select id from sets where short_name = 'psal'),
    'I42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Luminous Angel'),
    (select id from sets where short_name = 'psal'),
    'G25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lord of the Undead'),
    (select id from sets where short_name = 'psal'),
    'F1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'C58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fangren Pathcutter'),
    (select id from sets where short_name = 'psal'),
    'K15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nezumi Cutthroat'),
    (select id from sets where short_name = 'psal'),
    'A17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Larceny'),
    (select id from sets where short_name = 'psal'),
    'A49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'I45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Cub'),
    (select id from sets where short_name = 'psal'),
    'L7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Windreader'),
    (select id from sets where short_name = 'psal'),
    'H53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Skyhunter'),
    (select id from sets where short_name = 'psal'),
    'L52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sprout'),
    (select id from sets where short_name = 'psal'),
    'J30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Three Dreams'),
    (select id from sets where short_name = 'psal'),
    'G49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viridian Scout'),
    (select id from sets where short_name = 'psal'),
    'B31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woebearer'),
    (select id from sets where short_name = 'psal'),
    'F16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bonesplitter'),
    (select id from sets where short_name = 'psal'),
    'L5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wirewood Lodge'),
    (select id from sets where short_name = 'psal'),
    'B28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Helldozer'),
    (select id from sets where short_name = 'psal'),
    'F13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sprout'),
    (select id from sets where short_name = 'psal'),
    'J41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'C57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'L35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'G23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Coils'),
    (select id from sets where short_name = 'psal'),
    'I37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'G56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Den-Guard'),
    (select id from sets where short_name = 'psal'),
    'L17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hair-Strung Koto'),
    (select id from sets where short_name = 'psal'),
    'H37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'E23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'E32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'J59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ninja of the Deep Hours'),
    (select id from sets where short_name = 'psal'),
    'E41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dehydration'),
    (select id from sets where short_name = 'psal'),
    'H41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Flock'),
    (select id from sets where short_name = 'psal'),
    'C39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'I24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enormous Baloth'),
    (select id from sets where short_name = 'psal'),
    'K50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crippling Fatigue'),
    (select id from sets where short_name = 'psal'),
    'A6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seeker of Skybreak'),
    (select id from sets where short_name = 'psal'),
    'B29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jedit''s Dragoons'),
    (select id from sets where short_name = 'psal'),
    'L6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lavaborn Muse'),
    (select id from sets where short_name = 'psal'),
    'C13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'D57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Puppeteer'),
    (select id from sets where short_name = 'psal'),
    'H39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'E24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nullmage Shepherd'),
    (select id from sets where short_name = 'psal'),
    'B40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plumes of Peace'),
    (select id from sets where short_name = 'psal'),
    'G55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'L46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'E58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soulsworn Jury'),
    (select id from sets where short_name = 'psal'),
    'G44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'K33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dirty Wererat'),
    (select id from sets where short_name = 'psal'),
    'A7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Devouring Rage'),
    (select id from sets where short_name = 'psal'),
    'C5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chill Haunting'),
    (select id from sets where short_name = 'psal'),
    'E26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Warrior'),
    (select id from sets where short_name = 'psal'),
    'B42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cover of Darkness'),
    (select id from sets where short_name = 'psal'),
    'D37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fangren Firstborn'),
    (select id from sets where short_name = 'psal'),
    'K13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boomerang'),
    (select id from sets where short_name = 'psal'),
    'H29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Banishing'),
    (select id from sets where short_name = 'psal'),
    'F30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'E48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'L36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Champion'),
    (select id from sets where short_name = 'psal'),
    'B49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flight of Fancy'),
    (select id from sets where short_name = 'psal'),
    'G30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'G34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'B47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Belltower Sphinx'),
    (select id from sets where short_name = 'psal'),
    'H26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crypt Rats'),
    (select id from sets where short_name = 'psal'),
    'A28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel of Mercy'),
    (select id from sets where short_name = 'psal'),
    'G38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skullsnatcher'),
    (select id from sets where short_name = 'psal'),
    'A4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rats'' Feast'),
    (select id from sets where short_name = 'psal'),
    'A44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fallen Angel'),
    (select id from sets where short_name = 'psal'),
    'F37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'A21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raging Goblin'),
    (select id from sets where short_name = 'psal'),
    'I41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balduvian Rage'),
    (select id from sets where short_name = 'psal'),
    'I15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'J34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'D59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctum Guardian'),
    (select id from sets where short_name = 'psal'),
    'G51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wirewood Pride'),
    (select id from sets where short_name = 'psal'),
    'B44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carrion Rats'),
    (select id from sets where short_name = 'psal'),
    'A40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'D56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pulse of the Tangle'),
    (select id from sets where short_name = 'psal'),
    'K25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'psal'),
    'F32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'C33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frenzied Goblin'),
    (select id from sets where short_name = 'psal'),
    'I16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overwhelm'),
    (select id from sets where short_name = 'psal'),
    'J39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'psal'),
    'G57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skullsnatcher'),
    (select id from sets where short_name = 'psal'),
    'E18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Auratouched Mage'),
    (select id from sets where short_name = 'psal'),
    'G50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'psal'),
    'L48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodline Shaman'),
    (select id from sets where short_name = 'psal'),
    'B26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vacuumelt'),
    (select id from sets where short_name = 'psal'),
    'H40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'C23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'K36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infiltrator''s Magemark'),
    (select id from sets where short_name = 'psal'),
    'G7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sporesower Thallid'),
    (select id from sets where short_name = 'psal'),
    'J3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blade Sliver'),
    (select id from sets where short_name = 'psal'),
    'D38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'E45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'psal'),
    'K48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Herd Gnarr'),
    (select id from sets where short_name = 'psal'),
    'J17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'H36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thallid Germinator'),
    (select id from sets where short_name = 'psal'),
    'J31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'psal'),
    'G47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightguard Patrol'),
    (select id from sets where short_name = 'psal'),
    'G18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Birchlore Rangers'),
    (select id from sets where short_name = 'psal'),
    'B53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'psal'),
    'L20',
    'common'
) 
 on conflict do nothing;
