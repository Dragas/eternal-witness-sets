    insert into mtgcard(name) values ('Fervent Champion') on conflict do nothing;
    insert into mtgcard(name) values ('The Cauldron of Eternity') on conflict do nothing;
    insert into mtgcard(name) values ('Oko, Thief of Crowns') on conflict do nothing;
    insert into mtgcard(name) values ('Torbran, Thane of Red Fell') on conflict do nothing;
    insert into mtgcard(name) values ('Castle Locthwain') on conflict do nothing;
    insert into mtgcard(name) values ('Brazen Borrower // Petty Theft') on conflict do nothing;
    insert into mtgcard(name) values ('Dance of the Manse') on conflict do nothing;
    insert into mtgcard(name) values ('Worthy Knight') on conflict do nothing;
    insert into mtgcard(name) values ('The Great Henge') on conflict do nothing;
    insert into mtgcard(name) values ('Castle Ardenvale') on conflict do nothing;
    insert into mtgcard(name) values ('Irencrag Pyromancer') on conflict do nothing;
    insert into mtgcard(name) values ('Folio of Fancies') on conflict do nothing;
    insert into mtgcard(name) values ('Gadwick, the Wizened') on conflict do nothing;
    insert into mtgcard(name) values ('Stormfist Crusader') on conflict do nothing;
    insert into mtgcard(name) values ('Return of the Wildspeaker') on conflict do nothing;
    insert into mtgcard(name) values ('Faeburrow Elder') on conflict do nothing;
    insert into mtgcard(name) values ('Ayara, First of Locthwain') on conflict do nothing;
    insert into mtgcard(name) values ('Witch''s Vengeance') on conflict do nothing;
    insert into mtgcard(name) values ('Castle Vantress') on conflict do nothing;
    insert into mtgcard(name) values ('Embercleave') on conflict do nothing;
    insert into mtgcard(name) values ('Clackbridge Troll') on conflict do nothing;
    insert into mtgcard(name) values ('The Magic Mirror') on conflict do nothing;
    insert into mtgcard(name) values ('The Royal Scions') on conflict do nothing;
    insert into mtgcard(name) values ('Stolen by the Fae') on conflict do nothing;
    insert into mtgcard(name) values ('Lovestruck Beast // Heart''s Desire') on conflict do nothing;
    insert into mtgcard(name) values ('Castle Embereth') on conflict do nothing;
    insert into mtgcard(name) values ('Sorcerous Spyglass') on conflict do nothing;
    insert into mtgcard(name) values ('Fabled Passage') on conflict do nothing;
    insert into mtgcard(name) values ('Piper of the Swarm') on conflict do nothing;
    insert into mtgcard(name) values ('Linden, the Steadfast Queen') on conflict do nothing;
    insert into mtgcard(name) values ('Murderous Rider // Swift End') on conflict do nothing;
    insert into mtgcard(name) values ('Midnight Clock') on conflict do nothing;
    insert into mtgcard(name) values ('Lochmere Serpent') on conflict do nothing;
    insert into mtgcard(name) values ('Gilded Goose') on conflict do nothing;
    insert into mtgcard(name) values ('Charming Prince') on conflict do nothing;
    insert into mtgcard(name) values ('Garruk, Cursed Huntsman') on conflict do nothing;
    insert into mtgcard(name) values ('Fae of Wishes // Granted') on conflict do nothing;
    insert into mtgcard(name) values ('Wildborn Preserver') on conflict do nothing;
    insert into mtgcard(name) values ('Hushbringer') on conflict do nothing;
    insert into mtgcard(name) values ('Outlaws'' Merriment') on conflict do nothing;
    insert into mtgcard(name) values ('Feasting Troll King') on conflict do nothing;
    insert into mtgcard(name) values ('Acclaimed Contender') on conflict do nothing;
    insert into mtgcard(name) values ('Rankle, Master of Pranks') on conflict do nothing;
    insert into mtgcard(name) values ('Doom Foretold') on conflict do nothing;
    insert into mtgcard(name) values ('Irencrag Feat') on conflict do nothing;
    insert into mtgcard(name) values ('Questing Beast') on conflict do nothing;
    insert into mtgcard(name) values ('Happily Ever After') on conflict do nothing;
    insert into mtgcard(name) values ('Once Upon a Time') on conflict do nothing;
    insert into mtgcard(name) values ('The Circle of Loyalty') on conflict do nothing;
    insert into mtgcard(name) values ('Bonecrusher Giant // Stomp') on conflict do nothing;
    insert into mtgcard(name) values ('Emry, Lurker of the Loch') on conflict do nothing;
    insert into mtgcard(name) values ('Opportunistic Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Oathsworn Knight') on conflict do nothing;
    insert into mtgcard(name) values ('Yorvo, Lord of Garenbrig') on conflict do nothing;
    insert into mtgcard(name) values ('Harmonious Archon') on conflict do nothing;
    insert into mtgcard(name) values ('Stonecoil Serpent') on conflict do nothing;
    insert into mtgcard(name) values ('Blacklance Paragon') on conflict do nothing;
    insert into mtgcard(name) values ('Realm-Cloaked Giant // Cast Off') on conflict do nothing;
    insert into mtgcard(name) values ('Wicked Wolf') on conflict do nothing;
    insert into mtgcard(name) values ('Giant Killer // Chop Down') on conflict do nothing;
    insert into mtgcard(name) values ('Castle Garenbrig') on conflict do nothing;
    insert into mtgcard(name) values ('Robber of the Rich') on conflict do nothing;
    insert into mtgcard(name) values ('Escape to the Wilds') on conflict do nothing;
    insert into mtgcard(name) values ('Fires of Invention') on conflict do nothing;
    insert into mtgcard(name) values ('Sundering Stroke') on conflict do nothing;
    insert into mtgcard(name) values ('Vantress Gargoyle') on conflict do nothing;
    insert into mtgcard(name) values ('Mirrormade') on conflict do nothing;
    insert into mtgcard(name) values ('Wishclaw Talisman') on conflict do nothing;
