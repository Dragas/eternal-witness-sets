insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Fervent Champion'),
    (select id from sets where short_name = 'peld'),
    '124s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Cauldron of Eternity'),
    (select id from sets where short_name = 'peld'),
    '82p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Oko, Thief of Crowns'),
    (select id from sets where short_name = 'peld'),
    '197p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Torbran, Thane of Red Fell'),
    (select id from sets where short_name = 'peld'),
    '147s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Castle Locthwain'),
    (select id from sets where short_name = 'peld'),
    '241s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brazen Borrower // Petty Theft'),
    (select id from sets where short_name = 'peld'),
    '39s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dance of the Manse'),
    (select id from sets where short_name = 'peld'),
    '186p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Worthy Knight'),
    (select id from sets where short_name = 'peld'),
    '36p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Great Henge'),
    (select id from sets where short_name = 'peld'),
    '161s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Castle Ardenvale'),
    (select id from sets where short_name = 'peld'),
    '238p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Irencrag Pyromancer'),
    (select id from sets where short_name = 'peld'),
    '128p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Great Henge'),
    (select id from sets where short_name = 'peld'),
    '161p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Folio of Fancies'),
    (select id from sets where short_name = 'peld'),
    '46s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fervent Champion'),
    (select id from sets where short_name = 'peld'),
    '124p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gadwick, the Wizened'),
    (select id from sets where short_name = 'peld'),
    '48s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stormfist Crusader'),
    (select id from sets where short_name = 'peld'),
    '203s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Return of the Wildspeaker'),
    (select id from sets where short_name = 'peld'),
    '172s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faeburrow Elder'),
    (select id from sets where short_name = 'peld'),
    '190p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ayara, First of Locthwain'),
    (select id from sets where short_name = 'peld'),
    '75s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Witch''s Vengeance'),
    (select id from sets where short_name = 'peld'),
    '111p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Castle Vantress'),
    (select id from sets where short_name = 'peld'),
    '242s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Embercleave'),
    (select id from sets where short_name = 'peld'),
    '120p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Clackbridge Troll'),
    (select id from sets where short_name = 'peld'),
    '84s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Magic Mirror'),
    (select id from sets where short_name = 'peld'),
    '51p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Faeburrow Elder'),
    (select id from sets where short_name = 'peld'),
    '190s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Royal Scions'),
    (select id from sets where short_name = 'peld'),
    '199s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stolen by the Fae'),
    (select id from sets where short_name = 'peld'),
    '66s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Return of the Wildspeaker'),
    (select id from sets where short_name = 'peld'),
    '172p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Embercleave'),
    (select id from sets where short_name = 'peld'),
    '120s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lovestruck Beast // Heart''s Desire'),
    (select id from sets where short_name = 'peld'),
    '165s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Castle Embereth'),
    (select id from sets where short_name = 'peld'),
    '239s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sorcerous Spyglass'),
    (select id from sets where short_name = 'peld'),
    '233p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brazen Borrower // Petty Theft'),
    (select id from sets where short_name = 'peld'),
    '39p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fabled Passage'),
    (select id from sets where short_name = 'peld'),
    '244s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Piper of the Swarm'),
    (select id from sets where short_name = 'peld'),
    '100p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Linden, the Steadfast Queen'),
    (select id from sets where short_name = 'peld'),
    '20p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Murderous Rider // Swift End'),
    (select id from sets where short_name = 'peld'),
    '97s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Midnight Clock'),
    (select id from sets where short_name = 'peld'),
    '54p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lochmere Serpent'),
    (select id from sets where short_name = 'peld'),
    '195p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gilded Goose'),
    (select id from sets where short_name = 'peld'),
    '160s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Charming Prince'),
    (select id from sets where short_name = 'peld'),
    '8p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Garruk, Cursed Huntsman'),
    (select id from sets where short_name = 'peld'),
    '191s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fae of Wishes // Granted'),
    (select id from sets where short_name = 'peld'),
    '44p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wildborn Preserver'),
    (select id from sets where short_name = 'peld'),
    '182p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hushbringer'),
    (select id from sets where short_name = 'peld'),
    '18p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fabled Passage'),
    (select id from sets where short_name = 'peld'),
    '244p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Outlaws'' Merriment'),
    (select id from sets where short_name = 'peld'),
    '198p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Feasting Troll King'),
    (select id from sets where short_name = 'peld'),
    '152s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Piper of the Swarm'),
    (select id from sets where short_name = 'peld'),
    '100s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Acclaimed Contender'),
    (select id from sets where short_name = 'peld'),
    '1p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rankle, Master of Pranks'),
    (select id from sets where short_name = 'peld'),
    '101p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Doom Foretold'),
    (select id from sets where short_name = 'peld'),
    '187p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lochmere Serpent'),
    (select id from sets where short_name = 'peld'),
    '195s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Charming Prince'),
    (select id from sets where short_name = 'peld'),
    '8s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Outlaws'' Merriment'),
    (select id from sets where short_name = 'peld'),
    '198s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Irencrag Feat'),
    (select id from sets where short_name = 'peld'),
    '127p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Questing Beast'),
    (select id from sets where short_name = 'peld'),
    '171s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Happily Ever After'),
    (select id from sets where short_name = 'peld'),
    '16s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Once Upon a Time'),
    (select id from sets where short_name = 'peld'),
    '169s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Linden, the Steadfast Queen'),
    (select id from sets where short_name = 'peld'),
    '20s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Circle of Loyalty'),
    (select id from sets where short_name = 'peld'),
    '9s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bonecrusher Giant // Stomp'),
    (select id from sets where short_name = 'peld'),
    '115s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sorcerous Spyglass'),
    (select id from sets where short_name = 'peld'),
    '233s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Royal Scions'),
    (select id from sets where short_name = 'peld'),
    '199p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Emry, Lurker of the Loch'),
    (select id from sets where short_name = 'peld'),
    '43s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Opportunistic Dragon'),
    (select id from sets where short_name = 'peld'),
    '133s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Castle Vantress'),
    (select id from sets where short_name = 'peld'),
    '242p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Once Upon a Time'),
    (select id from sets where short_name = 'peld'),
    '169p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oathsworn Knight'),
    (select id from sets where short_name = 'peld'),
    '98p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yorvo, Lord of Garenbrig'),
    (select id from sets where short_name = 'peld'),
    '185p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harmonious Archon'),
    (select id from sets where short_name = 'peld'),
    '17s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Acclaimed Contender'),
    (select id from sets where short_name = 'peld'),
    '1s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Irencrag Feat'),
    (select id from sets where short_name = 'peld'),
    '127s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stonecoil Serpent'),
    (select id from sets where short_name = 'peld'),
    '235p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Witch''s Vengeance'),
    (select id from sets where short_name = 'peld'),
    '111s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blacklance Paragon'),
    (select id from sets where short_name = 'peld'),
    '79s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feasting Troll King'),
    (select id from sets where short_name = 'peld'),
    '152p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rankle, Master of Pranks'),
    (select id from sets where short_name = 'peld'),
    '101s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Realm-Cloaked Giant // Cast Off'),
    (select id from sets where short_name = 'peld'),
    '26p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wicked Wolf'),
    (select id from sets where short_name = 'peld'),
    '181p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bonecrusher Giant // Stomp'),
    (select id from sets where short_name = 'peld'),
    '115p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Killer // Chop Down'),
    (select id from sets where short_name = 'peld'),
    '14p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Irencrag Pyromancer'),
    (select id from sets where short_name = 'peld'),
    '128s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Castle Locthwain'),
    (select id from sets where short_name = 'peld'),
    '241p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Castle Garenbrig'),
    (select id from sets where short_name = 'peld'),
    '240s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Castle Embereth'),
    (select id from sets where short_name = 'peld'),
    '239p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wildborn Preserver'),
    (select id from sets where short_name = 'peld'),
    '182s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oko, Thief of Crowns'),
    (select id from sets where short_name = 'peld'),
    '197s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Doom Foretold'),
    (select id from sets where short_name = 'peld'),
    '187s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Happily Ever After'),
    (select id from sets where short_name = 'peld'),
    '16p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gadwick, the Wizened'),
    (select id from sets where short_name = 'peld'),
    '48p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Robber of the Rich'),
    (select id from sets where short_name = 'peld'),
    '138s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Escape to the Wilds'),
    (select id from sets where short_name = 'peld'),
    '189s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Castle Ardenvale'),
    (select id from sets where short_name = 'peld'),
    '238s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Midnight Clock'),
    (select id from sets where short_name = 'peld'),
    '54s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fires of Invention'),
    (select id from sets where short_name = 'peld'),
    '125s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gilded Goose'),
    (select id from sets where short_name = 'peld'),
    '160p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sundering Stroke'),
    (select id from sets where short_name = 'peld'),
    '144p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Escape to the Wilds'),
    (select id from sets where short_name = 'peld'),
    '189p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yorvo, Lord of Garenbrig'),
    (select id from sets where short_name = 'peld'),
    '185s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dance of the Manse'),
    (select id from sets where short_name = 'peld'),
    '186s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Circle of Loyalty'),
    (select id from sets where short_name = 'peld'),
    '9p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vantress Gargoyle'),
    (select id from sets where short_name = 'peld'),
    '71s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clackbridge Troll'),
    (select id from sets where short_name = 'peld'),
    '84p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirrormade'),
    (select id from sets where short_name = 'peld'),
    '55s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Realm-Cloaked Giant // Cast Off'),
    (select id from sets where short_name = 'peld'),
    '26s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Questing Beast'),
    (select id from sets where short_name = 'peld'),
    '171p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ayara, First of Locthwain'),
    (select id from sets where short_name = 'peld'),
    '75p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Opportunistic Dragon'),
    (select id from sets where short_name = 'peld'),
    '133p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emry, Lurker of the Loch'),
    (select id from sets where short_name = 'peld'),
    '43p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Murderous Rider // Swift End'),
    (select id from sets where short_name = 'peld'),
    '97p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vantress Gargoyle'),
    (select id from sets where short_name = 'peld'),
    '71p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blacklance Paragon'),
    (select id from sets where short_name = 'peld'),
    '79p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oathsworn Knight'),
    (select id from sets where short_name = 'peld'),
    '98s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Cauldron of Eternity'),
    (select id from sets where short_name = 'peld'),
    '82s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sundering Stroke'),
    (select id from sets where short_name = 'peld'),
    '144s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lovestruck Beast // Heart''s Desire'),
    (select id from sets where short_name = 'peld'),
    '165p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stolen by the Fae'),
    (select id from sets where short_name = 'peld'),
    '66p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hushbringer'),
    (select id from sets where short_name = 'peld'),
    '18s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wicked Wolf'),
    (select id from sets where short_name = 'peld'),
    '181s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wishclaw Talisman'),
    (select id from sets where short_name = 'peld'),
    '110s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Robber of the Rich'),
    (select id from sets where short_name = 'peld'),
    '138p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fae of Wishes // Granted'),
    (select id from sets where short_name = 'peld'),
    '44s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Garruk, Cursed Huntsman'),
    (select id from sets where short_name = 'peld'),
    '191p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stonecoil Serpent'),
    (select id from sets where short_name = 'peld'),
    '235s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Folio of Fancies'),
    (select id from sets where short_name = 'peld'),
    '46p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirrormade'),
    (select id from sets where short_name = 'peld'),
    '55p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Magic Mirror'),
    (select id from sets where short_name = 'peld'),
    '51s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stormfist Crusader'),
    (select id from sets where short_name = 'peld'),
    '203p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Torbran, Thane of Red Fell'),
    (select id from sets where short_name = 'peld'),
    '147p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fires of Invention'),
    (select id from sets where short_name = 'peld'),
    '125p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harmonious Archon'),
    (select id from sets where short_name = 'peld'),
    '17p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wishclaw Talisman'),
    (select id from sets where short_name = 'peld'),
    '110p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Killer // Chop Down'),
    (select id from sets where short_name = 'peld'),
    '14s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Castle Garenbrig'),
    (select id from sets where short_name = 'peld'),
    '240p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Worthy Knight'),
    (select id from sets where short_name = 'peld'),
    '36s',
    'rare'
) 
 on conflict do nothing;
