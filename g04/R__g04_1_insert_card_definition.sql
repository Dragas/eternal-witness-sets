    insert into mtgcard(name) values ('Armageddon') on conflict do nothing;
    insert into mtgcard(name) values ('Balance') on conflict do nothing;
    insert into mtgcard(name) values ('Deranged Hermit') on conflict do nothing;
    insert into mtgcard(name) values ('Hermit Druid') on conflict do nothing;
    insert into mtgcard(name) values ('Time Warp') on conflict do nothing;
    insert into mtgcard(name) values ('Phyrexian Negator') on conflict do nothing;
