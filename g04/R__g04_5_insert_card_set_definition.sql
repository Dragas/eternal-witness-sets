insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = 'g04'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balance'),
    (select id from sets where short_name = 'g04'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deranged Hermit'),
    (select id from sets where short_name = 'g04'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hermit Druid'),
    (select id from sets where short_name = 'g04'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Time Warp'),
    (select id from sets where short_name = 'g04'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Negator'),
    (select id from sets where short_name = 'g04'),
    '4',
    'rare'
) 
 on conflict do nothing;
