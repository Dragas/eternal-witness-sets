insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'l12'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight'),
    (select id from sets where short_name = 'l12'),
    '2',
    'common'
) 
 on conflict do nothing;
