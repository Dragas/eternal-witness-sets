insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Orcish Healer'),
    (select id from sets where short_name = 'cst'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cst'),
    '377',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bounty of the Hunt'),
    (select id from sets where short_name = 'cst'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashen Ghoul'),
    (select id from sets where short_name = 'cst'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Portent'),
    (select id from sets where short_name = 'cst'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cst'),
    '382',
    'common'
) ,
(
    (select id from mtgcard where name = 'Legions of Lim-Dûl'),
    (select id from sets where short_name = 'cst'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'cst'),
    '378',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barbed Sextant'),
    (select id from sets where short_name = 'cst'),
    '312',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aurochs'),
    (select id from sets where short_name = 'cst'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Browse'),
    (select id from sets where short_name = 'cst'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Insidious Bookworms'),
    (select id from sets where short_name = 'cst'),
    '51a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Storm Elemental'),
    (select id from sets where short_name = 'cst'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balduvian Dead'),
    (select id from sets where short_name = 'cst'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gangrenous Zombies'),
    (select id from sets where short_name = 'cst'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'cst'),
    '380',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Pride'),
    (select id from sets where short_name = 'cst'),
    '9b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Trap Door Spider'),
    (select id from sets where short_name = 'cst'),
    '293',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Home Guard'),
    (select id from sets where short_name = 'cst'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woolly Mammoths'),
    (select id from sets where short_name = 'cst'),
    '278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wings of Aesthir'),
    (select id from sets where short_name = 'cst'),
    '305',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Essence Flare'),
    (select id from sets where short_name = 'cst'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Lumberjack'),
    (select id from sets where short_name = 'cst'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zuran Spellcaster'),
    (select id from sets where short_name = 'cst'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'cst'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scars of the Veteran'),
    (select id from sets where short_name = 'cst'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'cst'),
    '379',
    'common'
) ,
(
    (select id from mtgcard where name = 'Casting of Bones'),
    (select id from sets where short_name = 'cst'),
    '44b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Banishing'),
    (select id from sets where short_name = 'cst'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skull Catapult'),
    (select id from sets where short_name = 'cst'),
    '336',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcum''s Weathervane'),
    (select id from sets where short_name = 'cst'),
    '310',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Elite Guard'),
    (select id from sets where short_name = 'cst'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viscerid Drone'),
    (select id from sets where short_name = 'cst'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whalebone Glider'),
    (select id from sets where short_name = 'cst'),
    '349',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'cst'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'cst'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'cst'),
    '374',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cst'),
    '369',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cst'),
    '375',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cst'),
    '376',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iceberg'),
    (select id from sets where short_name = 'cst'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = 'cst'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'cst'),
    '372',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistfolk'),
    (select id from sets where short_name = 'cst'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gorilla Shaman'),
    (select id from sets where short_name = 'cst'),
    '72a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cst'),
    '370',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lat-Nam''s Legacy'),
    (select id from sets where short_name = 'cst'),
    '30b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drift of the Dead'),
    (select id from sets where short_name = 'cst'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snow Devil'),
    (select id from sets where short_name = 'cst'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cst'),
    '371',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Burn'),
    (select id from sets where short_name = 'cst'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cst'),
    '383',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reinforcements'),
    (select id from sets where short_name = 'cst'),
    '12b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Fiend'),
    (select id from sets where short_name = 'cst'),
    '57a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tinder Wall'),
    (select id from sets where short_name = 'cst'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Binding Grasp'),
    (select id from sets where short_name = 'cst'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'cst'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadly Insect'),
    (select id from sets where short_name = 'cst'),
    '86a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Dead'),
    (select id from sets where short_name = 'cst'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Spark'),
    (select id from sets where short_name = 'cst'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'cst'),
    '373',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cst'),
    '381',
    'common'
) 
 on conflict do nothing;
