insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Orazca Frillback'),
    (select id from sets where short_name = 'rix'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghalta, Primal Hunger'),
    (select id from sets where short_name = 'rix'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mutiny'),
    (select id from sets where short_name = 'rix'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravenous Chupacabra'),
    (select id from sets where short_name = 'rix'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flood of Recollection'),
    (select id from sets where short_name = 'rix'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swab Goblin'),
    (select id from sets where short_name = 'rix'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Martyr of Dusk'),
    (select id from sets where short_name = 'rix'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunt the Weak'),
    (select id from sets where short_name = 'rix'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moment of Triumph'),
    (select id from sets where short_name = 'rix'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Majestic Heliopterus'),
    (select id from sets where short_name = 'rix'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cherished Hatchling'),
    (select id from sets where short_name = 'rix'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bombard'),
    (select id from sets where short_name = 'rix'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Siegehorn Ceratops'),
    (select id from sets where short_name = 'rix'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = 'rix'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrashing Brontodon'),
    (select id from sets where short_name = 'rix'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reckless Rage'),
    (select id from sets where short_name = 'rix'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forerunner of the Legion'),
    (select id from sets where short_name = 'rix'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sanguine Glorifier'),
    (select id from sets where short_name = 'rix'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sadistic Skymarcher'),
    (select id from sets where short_name = 'rix'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brass''s Bounty'),
    (select id from sets where short_name = 'rix'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arterial Flow'),
    (select id from sets where short_name = 'rix'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dire Fleet Daredevil'),
    (select id from sets where short_name = 'rix'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Timestream Navigator'),
    (select id from sets where short_name = 'rix'),
    '59',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goblin Trailblazer'),
    (select id from sets where short_name = 'rix'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swift Warden'),
    (select id from sets where short_name = 'rix'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Strength of the Pack'),
    (select id from sets where short_name = 'rix'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mist-Cloaked Herald'),
    (select id from sets where short_name = 'rix'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Etali, Primal Storm'),
    (select id from sets where short_name = 'rix'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Daring Buccaneer'),
    (select id from sets where short_name = 'rix'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Profane Procession // Tomb of the Dusk Rose'),
    (select id from sets where short_name = 'rix'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Siren Reaver'),
    (select id from sets where short_name = 'rix'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Baffling End'),
    (select id from sets where short_name = 'rix'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swaggering Corsair'),
    (select id from sets where short_name = 'rix'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slippery Scoundrel'),
    (select id from sets where short_name = 'rix'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merfolk Mistbinder'),
    (select id from sets where short_name = 'rix'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul of the Rapids'),
    (select id from sets where short_name = 'rix'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tilonalli''s Summoner'),
    (select id from sets where short_name = 'rix'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jadecraft Artisan'),
    (select id from sets where short_name = 'rix'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Revenant'),
    (select id from sets where short_name = 'rix'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Luminous Bonds'),
    (select id from sets where short_name = 'rix'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relentless Raptor'),
    (select id from sets where short_name = 'rix'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mastermind''s Acquisition'),
    (select id from sets where short_name = 'rix'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azor''s Gateway // Sanctum of the Sun'),
    (select id from sets where short_name = 'rix'),
    '176',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forerunner of the Heralds'),
    (select id from sets where short_name = 'rix'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azor, the Lawbringer'),
    (select id from sets where short_name = 'rix'),
    '154',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'rix'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Quarry'),
    (select id from sets where short_name = 'rix'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dusk Legion Zealot'),
    (select id from sets where short_name = 'rix'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forsaken Sanctuary'),
    (select id from sets where short_name = 'rix'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Legion Lieutenant'),
    (select id from sets where short_name = 'rix'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Highland Lake'),
    (select id from sets where short_name = 'rix'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cacophodon'),
    (select id from sets where short_name = 'rix'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wayward Swordtooth'),
    (select id from sets where short_name = 'rix'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Traveler''s Amulet'),
    (select id from sets where short_name = 'rix'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golden Guardian // Gold-Forge Garrison'),
    (select id from sets where short_name = 'rix'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Huatli, Radiant Champion'),
    (select id from sets where short_name = 'rix'),
    '159',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kumena''s Awakening'),
    (select id from sets where short_name = 'rix'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zetalpa, Primal Dawn'),
    (select id from sets where short_name = 'rix'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stampeding Horncrest'),
    (select id from sets where short_name = 'rix'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Creeper'),
    (select id from sets where short_name = 'rix'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gruesome Fate'),
    (select id from sets where short_name = 'rix'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riverwise Augur'),
    (select id from sets where short_name = 'rix'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snubhorn Sentry'),
    (select id from sets where short_name = 'rix'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oathsworn Vampire'),
    (select id from sets where short_name = 'rix'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Storm Fleet Sprinter'),
    (select id from sets where short_name = 'rix'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Form of the Dinosaur'),
    (select id from sets where short_name = 'rix'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Protean Raider'),
    (select id from sets where short_name = 'rix'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slaughter the Strong'),
    (select id from sets where short_name = 'rix'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Inquiry'),
    (select id from sets where short_name = 'rix'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seafloor Oracle'),
    (select id from sets where short_name = 'rix'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silvergill Adept'),
    (select id from sets where short_name = 'rix'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'See Red'),
    (select id from sets where short_name = 'rix'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giltgrove Stalker'),
    (select id from sets where short_name = 'rix'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angrath''s Ambusher'),
    (select id from sets where short_name = 'rix'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nezahal, Primal Tide'),
    (select id from sets where short_name = 'rix'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Release to the Wind'),
    (select id from sets where short_name = 'rix'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kitesail Corsair'),
    (select id from sets where short_name = 'rix'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vraska''s Conquistador'),
    (select id from sets where short_name = 'rix'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sphinx''s Decree'),
    (select id from sets where short_name = 'rix'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Champion of Dusk'),
    (select id from sets where short_name = 'rix'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dire Fleet Neckbreaker'),
    (select id from sets where short_name = 'rix'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plummet'),
    (select id from sets where short_name = 'rix'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mausoleum Harpy'),
    (select id from sets where short_name = 'rix'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vraska''s Scorn'),
    (select id from sets where short_name = 'rix'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aquatic Incursion'),
    (select id from sets where short_name = 'rix'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zacama, Primal Calamity'),
    (select id from sets where short_name = 'rix'),
    '174',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fanatical Firebrand'),
    (select id from sets where short_name = 'rix'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strider Harness'),
    (select id from sets where short_name = 'rix'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silverclad Ferocidons'),
    (select id from sets where short_name = 'rix'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'rix'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gleaming Barrier'),
    (select id from sets where short_name = 'rix'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'rix'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hardy Veteran'),
    (select id from sets where short_name = 'rix'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sun-Collared Raptor'),
    (select id from sets where short_name = 'rix'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sworn Guardian'),
    (select id from sets where short_name = 'rix'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Expel from Orazca'),
    (select id from sets where short_name = 'rix'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Path of Mettle // Metzali, Tower of Triumph'),
    (select id from sets where short_name = 'rix'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'rix'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tomb Robber'),
    (select id from sets where short_name = 'rix'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sun Sentinel'),
    (select id from sets where short_name = 'rix'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jade Bearer'),
    (select id from sets where short_name = 'rix'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orazca Relic'),
    (select id from sets where short_name = 'rix'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angrath''s Fury'),
    (select id from sets where short_name = 'rix'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm Fleet Swashbuckler'),
    (select id from sets where short_name = 'rix'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Canal Monitor'),
    (select id from sets where short_name = 'rix'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Captain''s Hook'),
    (select id from sets where short_name = 'rix'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cleansing Ray'),
    (select id from sets where short_name = 'rix'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curious Obsession'),
    (select id from sets where short_name = 'rix'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warkite Marauder'),
    (select id from sets where short_name = 'rix'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crested Herdcaller'),
    (select id from sets where short_name = 'rix'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Admiral''s Order'),
    (select id from sets where short_name = 'rix'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Everdawn Champion'),
    (select id from sets where short_name = 'rix'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blood Sun'),
    (select id from sets where short_name = 'rix'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dire Fleet Poisoner'),
    (select id from sets where short_name = 'rix'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golden Demise'),
    (select id from sets where short_name = 'rix'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Secrets of the Golden City'),
    (select id from sets where short_name = 'rix'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Waterknot'),
    (select id from sets where short_name = 'rix'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charging Tuskodon'),
    (select id from sets where short_name = 'rix'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hadana''s Climb // Winged Temple of Orazca'),
    (select id from sets where short_name = 'rix'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grasping Scoundrel'),
    (select id from sets where short_name = 'rix'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Resplendent Griffin'),
    (select id from sets where short_name = 'rix'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vraska, Scheming Gorgon'),
    (select id from sets where short_name = 'rix'),
    '197',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Squire''s Devotion'),
    (select id from sets where short_name = 'rix'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'rix'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = 'rix'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'rix'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cinder Barrens'),
    (select id from sets where short_name = 'rix'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dinosaur Hunter'),
    (select id from sets where short_name = 'rix'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skymarcher Aspirant'),
    (select id from sets where short_name = 'rix'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forerunner of the Empire'),
    (select id from sets where short_name = 'rix'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enter the Unknown'),
    (select id from sets where short_name = 'rix'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Radiant Destiny'),
    (select id from sets where short_name = 'rix'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twilight Prophet'),
    (select id from sets where short_name = 'rix'),
    '88',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jungleborn Pioneer'),
    (select id from sets where short_name = 'rix'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jadelight Ranger'),
    (select id from sets where short_name = 'rix'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tendershoot Dryad'),
    (select id from sets where short_name = 'rix'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kumena, Tyrant of Orazca'),
    (select id from sets where short_name = 'rix'),
    '162',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Angrath, the Flame-Chained'),
    (select id from sets where short_name = 'rix'),
    '152',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Induced Amnesia'),
    (select id from sets where short_name = 'rix'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imperial Ceratops'),
    (select id from sets where short_name = 'rix'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elenda, the Dusk Rose'),
    (select id from sets where short_name = 'rix'),
    '157',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dead Man''s Chest'),
    (select id from sets where short_name = 'rix'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bishop of Binding'),
    (select id from sets where short_name = 'rix'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vona''s Hunger'),
    (select id from sets where short_name = 'rix'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crafty Cutpurse'),
    (select id from sets where short_name = 'rix'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spire Winder'),
    (select id from sets where short_name = 'rix'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadeye Brawler'),
    (select id from sets where short_name = 'rix'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raging Regisaur'),
    (select id from sets where short_name = 'rix'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Path of Discovery'),
    (select id from sets where short_name = 'rix'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Immortal Sun'),
    (select id from sets where short_name = 'rix'),
    '180',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sea Legs'),
    (select id from sets where short_name = 'rix'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brazen Freebooter'),
    (select id from sets where short_name = 'rix'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hornswoggle'),
    (select id from sets where short_name = 'rix'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'rix'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trapjaw Tyrant'),
    (select id from sets where short_name = 'rix'),
    '29',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forerunner of the Coalition'),
    (select id from sets where short_name = 'rix'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shake the Foundations'),
    (select id from sets where short_name = 'rix'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blazing Hope'),
    (select id from sets where short_name = 'rix'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orazca Raptor'),
    (select id from sets where short_name = 'rix'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Legion Conquistador'),
    (select id from sets where short_name = 'rix'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moment of Craving'),
    (select id from sets where short_name = 'rix'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Storm the Vault // Vault of Catlacan'),
    (select id from sets where short_name = 'rix'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sun-Crested Pterodon'),
    (select id from sets where short_name = 'rix'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dusk Charger'),
    (select id from sets where short_name = 'rix'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunderherd Migration'),
    (select id from sets where short_name = 'rix'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tetzimoc, Primal Death'),
    (select id from sets where short_name = 'rix'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arch of Orazca'),
    (select id from sets where short_name = 'rix'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pitiless Plunderer'),
    (select id from sets where short_name = 'rix'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deeproot Elite'),
    (select id from sets where short_name = 'rix'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pirate''s Pillage'),
    (select id from sets where short_name = 'rix'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Paladin of Atonement'),
    (select id from sets where short_name = 'rix'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woodland Stream'),
    (select id from sets where short_name = 'rix'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Awakened Amalgam'),
    (select id from sets where short_name = 'rix'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Colossal Dreadmaw'),
    (select id from sets where short_name = 'rix'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Champion'),
    (select id from sets where short_name = 'rix'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'World Shaper'),
    (select id from sets where short_name = 'rix'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voracious Vampire'),
    (select id from sets where short_name = 'rix'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'River Darter'),
    (select id from sets where short_name = 'rix'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Famished Paladin'),
    (select id from sets where short_name = 'rix'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Journey to Eternity // Atzal, Cave of Eternity'),
    (select id from sets where short_name = 'rix'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Buccaneer''s Bravado'),
    (select id from sets where short_name = 'rix'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rekindling Phoenix'),
    (select id from sets where short_name = 'rix'),
    '111',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Deadeye Rig-Hauler'),
    (select id from sets where short_name = 'rix'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Impale'),
    (select id from sets where short_name = 'rix'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raptor Companion'),
    (select id from sets where short_name = 'rix'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crashing Tide'),
    (select id from sets where short_name = 'rix'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aggressive Urge'),
    (select id from sets where short_name = 'rix'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frilled Deathspitter'),
    (select id from sets where short_name = 'rix'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of the Stampede'),
    (select id from sets where short_name = 'rix'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sailor of Means'),
    (select id from sets where short_name = 'rix'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Needletooth Raptor'),
    (select id from sets where short_name = 'rix'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Divine Verdict'),
    (select id from sets where short_name = 'rix'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tilonalli''s Crown'),
    (select id from sets where short_name = 'rix'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angrath, Minotaur Pirate'),
    (select id from sets where short_name = 'rix'),
    '201',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Recover'),
    (select id from sets where short_name = 'rix'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fathom Fleet Boarder'),
    (select id from sets where short_name = 'rix'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foul Orchard'),
    (select id from sets where short_name = 'rix'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Polyraptor'),
    (select id from sets where short_name = 'rix'),
    '144',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Temple Altisaur'),
    (select id from sets where short_name = 'rix'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overgrown Armasaur'),
    (select id from sets where short_name = 'rix'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reaver Ambush'),
    (select id from sets where short_name = 'rix'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Atzocan Seer'),
    (select id from sets where short_name = 'rix'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pride of Conquerors'),
    (select id from sets where short_name = 'rix'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silent Gravestone'),
    (select id from sets where short_name = 'rix'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exultant Skymarcher'),
    (select id from sets where short_name = 'rix'),
    '7',
    'common'
) 
 on conflict do nothing;
