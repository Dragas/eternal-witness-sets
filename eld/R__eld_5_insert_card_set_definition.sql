insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Venerable Knight'),
    (select id from sets where short_name = 'eld'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'eld'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enchanted Carriage'),
    (select id from sets where short_name = 'eld'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Locthwain Gargoyle'),
    (select id from sets where short_name = 'eld'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rowan, Fearless Sparkmage'),
    (select id from sets where short_name = 'eld'),
    '304',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Merchant of the Vale // Haggle'),
    (select id from sets where short_name = 'eld'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tome of Legends'),
    (select id from sets where short_name = 'eld'),
    '332',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Improbable Alliance'),
    (select id from sets where short_name = 'eld'),
    '193',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Witch''s Oven'),
    (select id from sets where short_name = 'eld'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Syr Elenora, the Discerning'),
    (select id from sets where short_name = 'eld'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant''s Skewer'),
    (select id from sets where short_name = 'eld'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Linden, the Steadfast Queen'),
    (select id from sets where short_name = 'eld'),
    '340',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flaxen Intruder // Welcome Home'),
    (select id from sets where short_name = 'eld'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wintermoor Commander'),
    (select id from sets where short_name = 'eld'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spinning Wheel'),
    (select id from sets where short_name = 'eld'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sorcerer''s Broom'),
    (select id from sets where short_name = 'eld'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lochmere Serpent'),
    (select id from sets where short_name = 'eld'),
    '381',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wishclaw Talisman'),
    (select id from sets where short_name = 'eld'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Syr Carah, the Bold'),
    (select id from sets where short_name = 'eld'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Magic Mirror'),
    (select id from sets where short_name = 'eld'),
    '51',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wicked Wolf'),
    (select id from sets where short_name = 'eld'),
    '181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tall as a Beanstalk'),
    (select id from sets where short_name = 'eld'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bonecrusher Giant // Stomp'),
    (select id from sets where short_name = 'eld'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Piper of the Swarm'),
    (select id from sets where short_name = 'eld'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Castle Vantress'),
    (select id from sets where short_name = 'eld'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merchant of the Vale // Haggle'),
    (select id from sets where short_name = 'eld'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inspiring Veteran'),
    (select id from sets where short_name = 'eld'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mistford River Turtle'),
    (select id from sets where short_name = 'eld'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Naughty'),
    (select id from sets where short_name = 'eld'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clackbridge Troll'),
    (select id from sets where short_name = 'eld'),
    '353',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knight of the Keep'),
    (select id from sets where short_name = 'eld'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Acclaimed Contender'),
    (select id from sets where short_name = 'eld'),
    '334',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ogre Errant'),
    (select id from sets where short_name = 'eld'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Robber of the Rich'),
    (select id from sets where short_name = 'eld'),
    '138',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Midnight Clock'),
    (select id from sets where short_name = 'eld'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mysterious Pathlighter'),
    (select id from sets where short_name = 'eld'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shambling Suit'),
    (select id from sets where short_name = 'eld'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Castle Locthwain'),
    (select id from sets where short_name = 'eld'),
    '389',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Embereth Shieldbreaker // Battle Display'),
    (select id from sets where short_name = 'eld'),
    '292',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Faerie Formation'),
    (select id from sets where short_name = 'eld'),
    '316',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emry, Lurker of the Loch'),
    (select id from sets where short_name = 'eld'),
    '342',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savvy Hunter'),
    (select id from sets where short_name = 'eld'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Embereth Skyblazer'),
    (select id from sets where short_name = 'eld'),
    '321',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystic Sanctuary'),
    (select id from sets where short_name = 'eld'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burning-Yard Trainer'),
    (select id from sets where short_name = 'eld'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Garenbrig Carver // Shield''s Might'),
    (select id from sets where short_name = 'eld'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Castle Locthwain'),
    (select id from sets where short_name = 'eld'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maraleaf Rider'),
    (select id from sets where short_name = 'eld'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Escape to the Wilds'),
    (select id from sets where short_name = 'eld'),
    '379',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Once and Future'),
    (select id from sets where short_name = 'eld'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Faeburrow Elder'),
    (select id from sets where short_name = 'eld'),
    '190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bartered Cow'),
    (select id from sets where short_name = 'eld'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Piper of the Swarm'),
    (select id from sets where short_name = 'eld'),
    '392',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ayara, First of Locthwain'),
    (select id from sets where short_name = 'eld'),
    '350',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Insatiable Appetite'),
    (select id from sets where short_name = 'eld'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirrormade'),
    (select id from sets where short_name = 'eld'),
    '347',
    'rare'
) ,
(
    (select id from mtgcard where name = 'True Love''s Kiss'),
    (select id from sets where short_name = 'eld'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Torbran, Thane of Red Fell'),
    (select id from sets where short_name = 'eld'),
    '367',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sundering Stroke'),
    (select id from sets where short_name = 'eld'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Irencrag Feat'),
    (select id from sets where short_name = 'eld'),
    '362',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trail of Crumbs'),
    (select id from sets where short_name = 'eld'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glass Casket'),
    (select id from sets where short_name = 'eld'),
    '393',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stormfist Crusader'),
    (select id from sets where short_name = 'eld'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sage of the Falls'),
    (select id from sets where short_name = 'eld'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wildborn Preserver'),
    (select id from sets where short_name = 'eld'),
    '375',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rosethorn Acolyte // Seasonal Ritual'),
    (select id from sets where short_name = 'eld'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Banish into Fable'),
    (select id from sets where short_name = 'eld'),
    '325',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eye Collector'),
    (select id from sets where short_name = 'eld'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Belle of the Brawl'),
    (select id from sets where short_name = 'eld'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lost Legion'),
    (select id from sets where short_name = 'eld'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heraldic Banner'),
    (select id from sets where short_name = 'eld'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'eld'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feasting Troll King'),
    (select id from sets where short_name = 'eld'),
    '368',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rampart Smasher'),
    (select id from sets where short_name = 'eld'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gilded Goose'),
    (select id from sets where short_name = 'eld'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wicked Wolf'),
    (select id from sets where short_name = 'eld'),
    '374',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stolen by the Fae'),
    (select id from sets where short_name = 'eld'),
    '348',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animating Faerie // Bring to Life'),
    (select id from sets where short_name = 'eld'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Worthy Knight'),
    (select id from sets where short_name = 'eld'),
    '341',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lochmere Serpent'),
    (select id from sets where short_name = 'eld'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'eld'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Order of Midnight // Alter Fate'),
    (select id from sets where short_name = 'eld'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weaselback Redcap'),
    (select id from sets where short_name = 'eld'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fell the Pheasant'),
    (select id from sets where short_name = 'eld'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lovestruck Beast // Heart''s Desire'),
    (select id from sets where short_name = 'eld'),
    '299',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oko, Thief of Crowns'),
    (select id from sets where short_name = 'eld'),
    '197',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Revenge of Ravens'),
    (select id from sets where short_name = 'eld'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wildwood Tracker'),
    (select id from sets where short_name = 'eld'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Circle of Loyalty'),
    (select id from sets where short_name = 'eld'),
    '336',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Roving Keep'),
    (select id from sets where short_name = 'eld'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Memory Theft'),
    (select id from sets where short_name = 'eld'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reaper of Night // Harvest Fear'),
    (select id from sets where short_name = 'eld'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathless Knight'),
    (select id from sets where short_name = 'eld'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scorching Dragonfire'),
    (select id from sets where short_name = 'eld'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Youthful Knight'),
    (select id from sets where short_name = 'eld'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodhaze Wolverine'),
    (select id from sets where short_name = 'eld'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Escape to the Wilds'),
    (select id from sets where short_name = 'eld'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Opportunistic Dragon'),
    (select id from sets where short_name = 'eld'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Once Upon a Time'),
    (select id from sets where short_name = 'eld'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Opportunity'),
    (select id from sets where short_name = 'eld'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Outmuscle'),
    (select id from sets where short_name = 'eld'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Irencrag Pyromancer'),
    (select id from sets where short_name = 'eld'),
    '363',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Folio of Fancies'),
    (select id from sets where short_name = 'eld'),
    '343',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Castle Vantress'),
    (select id from sets where short_name = 'eld'),
    '390',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'eld'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'eld'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'So Tiny'),
    (select id from sets where short_name = 'eld'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bake into a Pie'),
    (select id from sets where short_name = 'eld'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silverwing Squadron'),
    (select id from sets where short_name = 'eld'),
    '315',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barrow Witches'),
    (select id from sets where short_name = 'eld'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seven Dwarves'),
    (select id from sets where short_name = 'eld'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rosethorn Acolyte // Seasonal Ritual'),
    (select id from sets where short_name = 'eld'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flaxen Intruder // Welcome Home'),
    (select id from sets where short_name = 'eld'),
    '297',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beanstalk Giant // Fertile Footsteps'),
    (select id from sets where short_name = 'eld'),
    '295',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Murderous Rider // Swift End'),
    (select id from sets where short_name = 'eld'),
    '287',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'eld'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Worthy Knight'),
    (select id from sets where short_name = 'eld'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lucky Clover'),
    (select id from sets where short_name = 'eld'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beloved Princess'),
    (select id from sets where short_name = 'eld'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lovestruck Beast // Heart''s Desire'),
    (select id from sets where short_name = 'eld'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Syr Alin, the Lion''s Claw'),
    (select id from sets where short_name = 'eld'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reaper of Night // Harvest Fear'),
    (select id from sets where short_name = 'eld'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doom Foretold'),
    (select id from sets where short_name = 'eld'),
    '378',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'eld'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Outlaws'' Merriment'),
    (select id from sets where short_name = 'eld'),
    '382',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fires of Invention'),
    (select id from sets where short_name = 'eld'),
    '361',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stormfist Crusader'),
    (select id from sets where short_name = 'eld'),
    '383',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golden Egg'),
    (select id from sets where short_name = 'eld'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vantress Paladin'),
    (select id from sets where short_name = 'eld'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Questing Beast'),
    (select id from sets where short_name = 'eld'),
    '372',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Workshop Elders'),
    (select id from sets where short_name = 'eld'),
    '318',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rally for the Throne'),
    (select id from sets where short_name = 'eld'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wildborn Preserver'),
    (select id from sets where short_name = 'eld'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glass Casket'),
    (select id from sets where short_name = 'eld'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kenrith, the Returned King'),
    (select id from sets where short_name = 'eld'),
    '303',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fabled Passage'),
    (select id from sets where short_name = 'eld'),
    '391',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prophet of the Peak'),
    (select id from sets where short_name = 'eld'),
    '227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'eld'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shimmer Dragon'),
    (select id from sets where short_name = 'eld'),
    '317',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dwarven Mine'),
    (select id from sets where short_name = 'eld'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf''s Quarry'),
    (select id from sets where short_name = 'eld'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Outlaws'' Merriment'),
    (select id from sets where short_name = 'eld'),
    '198',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cauldron''s Gift'),
    (select id from sets where short_name = 'eld'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dance of the Manse'),
    (select id from sets where short_name = 'eld'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fervent Champion'),
    (select id from sets where short_name = 'eld'),
    '360',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Joust'),
    (select id from sets where short_name = 'eld'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harmonious Archon'),
    (select id from sets where short_name = 'eld'),
    '338',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Deafening Silence'),
    (select id from sets where short_name = 'eld'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Witching Well'),
    (select id from sets where short_name = 'eld'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gadwick, the Wizened'),
    (select id from sets where short_name = 'eld'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Epic Downfall'),
    (select id from sets where short_name = 'eld'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hushbringer'),
    (select id from sets where short_name = 'eld'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Outflank'),
    (select id from sets where short_name = 'eld'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireborn Knight'),
    (select id from sets where short_name = 'eld'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keeper of Fables'),
    (select id from sets where short_name = 'eld'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Archon of Absolution'),
    (select id from sets where short_name = 'eld'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Redcap Raiders'),
    (select id from sets where short_name = 'eld'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Robber of the Rich'),
    (select id from sets where short_name = 'eld'),
    '365',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Foulmire Knight // Profane Insight'),
    (select id from sets where short_name = 'eld'),
    '286',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moonlit Scavengers'),
    (select id from sets where short_name = 'eld'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Royal Scions'),
    (select id from sets where short_name = 'eld'),
    '199',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Folio of Fancies'),
    (select id from sets where short_name = 'eld'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knights'' Charge'),
    (select id from sets where short_name = 'eld'),
    '328',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Malevolent Noble'),
    (select id from sets where short_name = 'eld'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Embereth Paladin'),
    (select id from sets where short_name = 'eld'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Redcap Melee'),
    (select id from sets where short_name = 'eld'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mace of the Valiant'),
    (select id from sets where short_name = 'eld'),
    '314',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Embereth Shieldbreaker // Battle Display'),
    (select id from sets where short_name = 'eld'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steelgaze Griffin'),
    (select id from sets where short_name = 'eld'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Order of Midnight // Alter Fate'),
    (select id from sets where short_name = 'eld'),
    '288',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kenrith''s Transformation'),
    (select id from sets where short_name = 'eld'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Castle Ardenvale'),
    (select id from sets where short_name = 'eld'),
    '386',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scalding Cauldron'),
    (select id from sets where short_name = 'eld'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oko''s Accomplices'),
    (select id from sets where short_name = 'eld'),
    '310',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wicked Guardian'),
    (select id from sets where short_name = 'eld'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alela, Artful Provocateur'),
    (select id from sets where short_name = 'eld'),
    '324',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Garenbrig Squire'),
    (select id from sets where short_name = 'eld'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Killer // Chop Down'),
    (select id from sets where short_name = 'eld'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wind-Scarred Crag'),
    (select id from sets where short_name = 'eld'),
    '308',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flutterfox'),
    (select id from sets where short_name = 'eld'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'eld'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Didn''t Say Please'),
    (select id from sets where short_name = 'eld'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shining Armor'),
    (select id from sets where short_name = 'eld'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brimstone Trebuchet'),
    (select id from sets where short_name = 'eld'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faeburrow Elder'),
    (select id from sets where short_name = 'eld'),
    '380',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forever Young'),
    (select id from sets where short_name = 'eld'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Vandal'),
    (select id from sets where short_name = 'eld'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Circle of Loyalty'),
    (select id from sets where short_name = 'eld'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Torbran, Thane of Red Fell'),
    (select id from sets where short_name = 'eld'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Sprite // Mesmeric Glare'),
    (select id from sets where short_name = 'eld'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tuinvale Treefolk // Oaken Boon'),
    (select id from sets where short_name = 'eld'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sporecap Spider'),
    (select id from sets where short_name = 'eld'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silverflame Squire // On Alert'),
    (select id from sets where short_name = 'eld'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Edgewall Innkeeper'),
    (select id from sets where short_name = 'eld'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Realm-Cloaked Giant // Cast Off'),
    (select id from sets where short_name = 'eld'),
    '277',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'eld'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curious Pair // Treats to Share'),
    (select id from sets where short_name = 'eld'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Castle Ardenvale'),
    (select id from sets where short_name = 'eld'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thorn Mammoth'),
    (select id from sets where short_name = 'eld'),
    '323',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inquisitive Puppet'),
    (select id from sets where short_name = 'eld'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merfolk Secretkeeper // Venture Deeper'),
    (select id from sets where short_name = 'eld'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Return of the Wildspeaker'),
    (select id from sets where short_name = 'eld'),
    '373',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Queen of Ice // Rage of Winter'),
    (select id from sets where short_name = 'eld'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oko''s Hospitality'),
    (select id from sets where short_name = 'eld'),
    '312',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slaying Fire'),
    (select id from sets where short_name = 'eld'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oko, the Trickster'),
    (select id from sets where short_name = 'eld'),
    '309',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chulane, Teller of Tales'),
    (select id from sets where short_name = 'eld'),
    '326',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Arcane Signet'),
    (select id from sets where short_name = 'eld'),
    '331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garenbrig Paladin'),
    (select id from sets where short_name = 'eld'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Taste of Death'),
    (select id from sets where short_name = 'eld'),
    '320',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yorvo, Lord of Garenbrig'),
    (select id from sets where short_name = 'eld'),
    '376',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Resolute Rider'),
    (select id from sets where short_name = 'eld'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clackbridge Troll'),
    (select id from sets where short_name = 'eld'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overwhelmed Apprentice'),
    (select id from sets where short_name = 'eld'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gluttonous Troll'),
    (select id from sets where short_name = 'eld'),
    '327',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prized Griffin'),
    (select id from sets where short_name = 'eld'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kenrith''s Transformation'),
    (select id from sets where short_name = 'eld'),
    '395',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weapon Rack'),
    (select id from sets where short_name = 'eld'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curious Pair // Treats to Share'),
    (select id from sets where short_name = 'eld'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oakhame Ranger // Bring Back'),
    (select id from sets where short_name = 'eld'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tempting Witch'),
    (select id from sets where short_name = 'eld'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Castle Embereth'),
    (select id from sets where short_name = 'eld'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tuinvale Treefolk // Oaken Boon'),
    (select id from sets where short_name = 'eld'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drown in the Loch'),
    (select id from sets where short_name = 'eld'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chittering Witch'),
    (select id from sets where short_name = 'eld'),
    '319',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ferocity of the Wilds'),
    (select id from sets where short_name = 'eld'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Festive Funeral'),
    (select id from sets where short_name = 'eld'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oathsworn Knight'),
    (select id from sets where short_name = 'eld'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steelbane Hydra'),
    (select id from sets where short_name = 'eld'),
    '322',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Korvold, Fae-Cursed King'),
    (select id from sets where short_name = 'eld'),
    '329',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Corridor Monitor'),
    (select id from sets where short_name = 'eld'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Embercleave'),
    (select id from sets where short_name = 'eld'),
    '359',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rankle, Master of Pranks'),
    (select id from sets where short_name = 'eld'),
    '101',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Garrison Griffin'),
    (select id from sets where short_name = 'eld'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunderous Snapper'),
    (select id from sets where short_name = 'eld'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oko, Thief of Crowns'),
    (select id from sets where short_name = 'eld'),
    '271',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Trapped in the Tower'),
    (select id from sets where short_name = 'eld'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fortifying Provisions'),
    (select id from sets where short_name = 'eld'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lonesome Unicorn // Rider in Need'),
    (select id from sets where short_name = 'eld'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Henge Walker'),
    (select id from sets where short_name = 'eld'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Castle Embereth'),
    (select id from sets where short_name = 'eld'),
    '387',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mad Ratter'),
    (select id from sets where short_name = 'eld'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cauldron Familiar'),
    (select id from sets where short_name = 'eld'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feasting Troll King'),
    (select id from sets where short_name = 'eld'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wandermare'),
    (select id from sets where short_name = 'eld'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Irencrag Pyromancer'),
    (select id from sets where short_name = 'eld'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Syr Faren, the Hengehammer'),
    (select id from sets where short_name = 'eld'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loch Dragon'),
    (select id from sets where short_name = 'eld'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'eld'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barge In'),
    (select id from sets where short_name = 'eld'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rowan''s Battleguard'),
    (select id from sets where short_name = 'eld'),
    '306',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slaying Fire'),
    (select id from sets where short_name = 'eld'),
    '394',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Realm-Cloaked Giant // Cast Off'),
    (select id from sets where short_name = 'eld'),
    '26',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wishclaw Talisman'),
    (select id from sets where short_name = 'eld'),
    '357',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Midnight Clock'),
    (select id from sets where short_name = 'eld'),
    '346',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crystal Slipper'),
    (select id from sets where short_name = 'eld'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oakhame Adversary'),
    (select id from sets where short_name = 'eld'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mirrormade'),
    (select id from sets where short_name = 'eld'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fires of Invention'),
    (select id from sets where short_name = 'eld'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shepherd of the Flock // Usher to Safety'),
    (select id from sets where short_name = 'eld'),
    '278',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Signpost Scarecrow'),
    (select id from sets where short_name = 'eld'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ardenvale Paladin'),
    (select id from sets where short_name = 'eld'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Syr Gwyn, Hero of Ashvale'),
    (select id from sets where short_name = 'eld'),
    '330',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rimrock Knight // Boulder Rush'),
    (select id from sets where short_name = 'eld'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sundering Stroke'),
    (select id from sets where short_name = 'eld'),
    '366',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stonecoil Serpent'),
    (select id from sets where short_name = 'eld'),
    '385',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beanstalk Giant // Fertile Footsteps'),
    (select id from sets where short_name = 'eld'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Syr Konrad, the Grim'),
    (select id from sets where short_name = 'eld'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Animating Faerie // Bring to Life'),
    (select id from sets where short_name = 'eld'),
    '280',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stolen by the Fae'),
    (select id from sets where short_name = 'eld'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elite Headhunter'),
    (select id from sets where short_name = 'eld'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Embercleave'),
    (select id from sets where short_name = 'eld'),
    '120',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'The Cauldron of Eternity'),
    (select id from sets where short_name = 'eld'),
    '352',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Garruk, Cursed Huntsman'),
    (select id from sets where short_name = 'eld'),
    '191',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Castle Garenbrig'),
    (select id from sets where short_name = 'eld'),
    '388',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'eld'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oathsworn Knight'),
    (select id from sets where short_name = 'eld'),
    '354',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yorvo, Lord of Garenbrig'),
    (select id from sets where short_name = 'eld'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Run Away Together'),
    (select id from sets where short_name = 'eld'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Great Henge'),
    (select id from sets where short_name = 'eld'),
    '161',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blacklance Paragon'),
    (select id from sets where short_name = 'eld'),
    '351',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Opportunistic Dragon'),
    (select id from sets where short_name = 'eld'),
    '364',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Royal Scions'),
    (select id from sets where short_name = 'eld'),
    '272',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gingerbread Cabin'),
    (select id from sets where short_name = 'eld'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rimrock Knight // Boulder Rush'),
    (select id from sets where short_name = 'eld'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stonecoil Serpent'),
    (select id from sets where short_name = 'eld'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcanist''s Owl'),
    (select id from sets where short_name = 'eld'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rowan''s Stalwarts'),
    (select id from sets where short_name = 'eld'),
    '307',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Irencrag Feat'),
    (select id from sets where short_name = 'eld'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frogify'),
    (select id from sets where short_name = 'eld'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lonesome Unicorn // Rider in Need'),
    (select id from sets where short_name = 'eld'),
    '276',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crashing Drawbridge'),
    (select id from sets where short_name = 'eld'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fabled Passage'),
    (select id from sets where short_name = 'eld'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Witch''s Cottage'),
    (select id from sets where short_name = 'eld'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Return of the Wildspeaker'),
    (select id from sets where short_name = 'eld'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grumgully, the Generous'),
    (select id from sets where short_name = 'eld'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Garenbrig Carver // Shield''s Might'),
    (select id from sets where short_name = 'eld'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ardenvale Tactician // Dizzying Swoop'),
    (select id from sets where short_name = 'eld'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emry, Lurker of the Loch'),
    (select id from sets where short_name = 'eld'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blow Your House Down'),
    (select id from sets where short_name = 'eld'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mantle of Tides'),
    (select id from sets where short_name = 'eld'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shepherd of the Flock // Usher to Safety'),
    (select id from sets where short_name = 'eld'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sorcerous Spyglass'),
    (select id from sets where short_name = 'eld'),
    '384',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unexplained Vision'),
    (select id from sets where short_name = 'eld'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brazen Borrower // Petty Theft'),
    (select id from sets where short_name = 'eld'),
    '39',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Opt'),
    (select id from sets where short_name = 'eld'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oakhame Ranger // Bring Back'),
    (select id from sets where short_name = 'eld'),
    '302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Happily Ever After'),
    (select id from sets where short_name = 'eld'),
    '337',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clockwork Servant'),
    (select id from sets where short_name = 'eld'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Return to Nature'),
    (select id from sets where short_name = 'eld'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vantress Gargoyle'),
    (select id from sets where short_name = 'eld'),
    '349',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fervent Champion'),
    (select id from sets where short_name = 'eld'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foulmire Knight // Profane Insight'),
    (select id from sets where short_name = 'eld'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Foreboding Fruit'),
    (select id from sets where short_name = 'eld'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harmonious Archon'),
    (select id from sets where short_name = 'eld'),
    '17',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ardenvale Tactician // Dizzying Swoop'),
    (select id from sets where short_name = 'eld'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Into the Story'),
    (select id from sets where short_name = 'eld'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thrill of Possibility'),
    (select id from sets where short_name = 'eld'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Idyllic Grange'),
    (select id from sets where short_name = 'eld'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silverflame Ritual'),
    (select id from sets where short_name = 'eld'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steelclaw Lance'),
    (select id from sets where short_name = 'eld'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fling'),
    (select id from sets where short_name = 'eld'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tournament Grounds'),
    (select id from sets where short_name = 'eld'),
    '248',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Sprite // Mesmeric Glare'),
    (select id from sets where short_name = 'eld'),
    '283',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reave Soul'),
    (select id from sets where short_name = 'eld'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk Secretkeeper // Venture Deeper'),
    (select id from sets where short_name = 'eld'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silverflame Squire // On Alert'),
    (select id from sets where short_name = 'eld'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Killer // Chop Down'),
    (select id from sets where short_name = 'eld'),
    '275',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'eld'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gilded Goose'),
    (select id from sets where short_name = 'eld'),
    '369',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dance of the Manse'),
    (select id from sets where short_name = 'eld'),
    '377',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Righteousness'),
    (select id from sets where short_name = 'eld'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Searing Barrage'),
    (select id from sets where short_name = 'eld'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Happily Ever After'),
    (select id from sets where short_name = 'eld'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brazen Borrower // Petty Theft'),
    (select id from sets where short_name = 'eld'),
    '281',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rosethorn Halberd'),
    (select id from sets where short_name = 'eld'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fierce Witchstalker'),
    (select id from sets where short_name = 'eld'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raging Redcap'),
    (select id from sets where short_name = 'eld'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'All That Glitters'),
    (select id from sets where short_name = 'eld'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Locthwain Paladin'),
    (select id from sets where short_name = 'eld'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Witch''s Vengeance'),
    (select id from sets where short_name = 'eld'),
    '358',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vantress Gargoyle'),
    (select id from sets where short_name = 'eld'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'eld'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'eld'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Witch''s Vengeance'),
    (select id from sets where short_name = 'eld'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Charming Prince'),
    (select id from sets where short_name = 'eld'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Charming Prince'),
    (select id from sets where short_name = 'eld'),
    '335',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hushbringer'),
    (select id from sets where short_name = 'eld'),
    '339',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Queen of Ice // Rage of Winter'),
    (select id from sets where short_name = 'eld'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skullknocker Ogre'),
    (select id from sets where short_name = 'eld'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tome Raider'),
    (select id from sets where short_name = 'eld'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fae of Wishes // Granted'),
    (select id from sets where short_name = 'eld'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Castle Garenbrig'),
    (select id from sets where short_name = 'eld'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Questing Beast'),
    (select id from sets where short_name = 'eld'),
    '171',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sorcerous Spyglass'),
    (select id from sets where short_name = 'eld'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'eld'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'eld'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maraleaf Pixie'),
    (select id from sets where short_name = 'eld'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gadwick, the Wizened'),
    (select id from sets where short_name = 'eld'),
    '344',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Murderous Rider // Swift End'),
    (select id from sets where short_name = 'eld'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Doom Foretold'),
    (select id from sets where short_name = 'eld'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'eld'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Guidemother // Gift of the Fae'),
    (select id from sets where short_name = 'eld'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smitten Swordmaster // Curry Favor'),
    (select id from sets where short_name = 'eld'),
    '290',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lash of Thorns'),
    (select id from sets where short_name = 'eld'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk, Cursed Huntsman'),
    (select id from sets where short_name = 'eld'),
    '270',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'The Cauldron of Eternity'),
    (select id from sets where short_name = 'eld'),
    '82',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Inspiring Veteran'),
    (select id from sets where short_name = 'eld'),
    '397',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Magic Mirror'),
    (select id from sets where short_name = 'eld'),
    '345',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Command Tower'),
    (select id from sets where short_name = 'eld'),
    '333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Improbable Alliance'),
    (select id from sets where short_name = 'eld'),
    '396',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Faerie Guidemother // Gift of the Fae'),
    (select id from sets where short_name = 'eld'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ayara, First of Locthwain'),
    (select id from sets where short_name = 'eld'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Specter''s Shriek'),
    (select id from sets where short_name = 'eld'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Piper of the Swarm'),
    (select id from sets where short_name = 'eld'),
    '355',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rankle, Master of Pranks'),
    (select id from sets where short_name = 'eld'),
    '356',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Charmed Sleep'),
    (select id from sets where short_name = 'eld'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Turn into a Pumpkin'),
    (select id from sets where short_name = 'eld'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Once Upon a Time'),
    (select id from sets where short_name = 'eld'),
    '371',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Covetous Urge'),
    (select id from sets where short_name = 'eld'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'eld'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thornwood Falls'),
    (select id from sets where short_name = 'eld'),
    '313',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gingerbrute'),
    (select id from sets where short_name = 'eld'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jousting Dummy'),
    (select id from sets where short_name = 'eld'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shinechaser'),
    (select id from sets where short_name = 'eld'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smitten Swordmaster // Curry Favor'),
    (select id from sets where short_name = 'eld'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'eld'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wishful Merfolk'),
    (select id from sets where short_name = 'eld'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Linden, the Steadfast Queen'),
    (select id from sets where short_name = 'eld'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Acclaimed Contender'),
    (select id from sets where short_name = 'eld'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystical Dispute'),
    (select id from sets where short_name = 'eld'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fae of Wishes // Granted'),
    (select id from sets where short_name = 'eld'),
    '282',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bramblefort Fink'),
    (select id from sets where short_name = 'eld'),
    '311',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blacklance Paragon'),
    (select id from sets where short_name = 'eld'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Claim the Firstborn'),
    (select id from sets where short_name = 'eld'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Great Henge'),
    (select id from sets where short_name = 'eld'),
    '370',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bonecrusher Giant // Stomp'),
    (select id from sets where short_name = 'eld'),
    '291',
    'rare'
) 
 on conflict do nothing;
