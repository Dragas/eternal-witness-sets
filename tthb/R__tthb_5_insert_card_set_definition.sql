insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Human Soldier'),
    (select id from sets where short_name = 'tthb'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gold'),
    (select id from sets where short_name = 'tthb'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goat'),
    (select id from sets where short_name = 'tthb'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'tthb'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spider'),
    (select id from sets where short_name = 'tthb'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tthb'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tthb'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tentacle'),
    (select id from sets where short_name = 'tthb'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Satyr'),
    (select id from sets where short_name = 'tthb'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reflection'),
    (select id from sets where short_name = 'tthb'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kraken'),
    (select id from sets where short_name = 'tthb'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = 'tthb'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall'),
    (select id from sets where short_name = 'tthb'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pegasus'),
    (select id from sets where short_name = 'tthb'),
    '3',
    'common'
) 
 on conflict do nothing;
