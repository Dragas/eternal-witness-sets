insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Eron the Relentless'),
    (select id from sets where short_name = 'ptc'),
    'mj73sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hymn to Tourach'),
    (select id from sets where short_name = 'ptc'),
    'll38d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Control Magic'),
    (select id from sets where short_name = 'ptc'),
    'ml64sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'ptc'),
    'et208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Banishing'),
    (select id from sets where short_name = 'ptc'),
    'll119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'ptc'),
    'ml52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orgg'),
    (select id from sets where short_name = 'ptc'),
    'et63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jester''s Cap'),
    (select id from sets where short_name = 'ptc'),
    'ml324sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = 'ptc'),
    'et338sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'ptc'),
    'll112sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashes to Ashes'),
    (select id from sets where short_name = 'ptc'),
    'll33sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hymn to Tourach'),
    (select id from sets where short_name = 'ptc'),
    'll38a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = 'ptc'),
    'ml15sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blank Card'),
    (select id from sets where short_name = 'ptc'),
    '0',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karplusan Forest'),
    (select id from sets where short_name = 'ptc'),
    'et356',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Order of the Ebon Hand'),
    (select id from sets where short_name = 'ptc'),
    'gb42c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'ptc'),
    'et22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Order of the Ebon Hand'),
    (select id from sets where short_name = 'ptc'),
    'll42c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balance'),
    (select id from sets where short_name = 'ptc'),
    'ml6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'An-Zerrin Ruins'),
    (select id from sets where short_name = 'ptc'),
    'et48sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = 'ptc'),
    'mj194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = 'ptc'),
    'et194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strip Mine'),
    (select id from sets where short_name = 'ptc'),
    'll363',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ptc'),
    'gb370',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necropotence'),
    (select id from sets where short_name = 'ptc'),
    'll154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ebon Stronghold'),
    (select id from sets where short_name = 'ptc'),
    'll95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Icy Manipulator'),
    (select id from sets where short_name = 'ptc'),
    'shr322',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Autumn Willow'),
    (select id from sets where short_name = 'ptc'),
    'pp83sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'ptc'),
    'bl58sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'ptc'),
    'ml65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = 'ptc'),
    'ml361',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fyndhorn Elves'),
    (select id from sets where short_name = 'ptc'),
    'bl244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'ptc'),
    'gb129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = 'ptc'),
    'gb160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'ptc'),
    'et112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strip Mine'),
    (select id from sets where short_name = 'ptc'),
    'ml363',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ivory Tower'),
    (select id from sets where short_name = 'ptc'),
    'pp328sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Icy Manipulator'),
    (select id from sets where short_name = 'ptc'),
    'bl322',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Erhnam Djinn'),
    (select id from sets where short_name = 'ptc'),
    'gb64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Banishing'),
    (select id from sets where short_name = 'ptc'),
    'gb119sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jester''s Cap'),
    (select id from sets where short_name = 'ptc'),
    'mj324',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lava Tubes'),
    (select id from sets where short_name = 'ptc'),
    'gb358',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zuran Orb'),
    (select id from sets where short_name = 'ptc'),
    'gb350',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ptc'),
    'gb371',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'ptc'),
    'gb192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Control Magic'),
    (select id from sets where short_name = 'ptc'),
    'ml64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spectral Bears'),
    (select id from sets where short_name = 'ptc'),
    'bl98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ptc'),
    'll370',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sulfurous Springs'),
    (select id from sets where short_name = 'ptc'),
    'gb360',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zuran Orb'),
    (select id from sets where short_name = 'ptc'),
    'et350',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ptc'),
    'et365',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arenson''s Aura'),
    (select id from sets where short_name = 'ptc'),
    'shr3sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Library'),
    (select id from sets where short_name = 'ptc'),
    'pp273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ivory Tower'),
    (select id from sets where short_name = 'ptc'),
    'ml328',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dwarven Catapult'),
    (select id from sets where short_name = 'ptc'),
    'et51sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ptc'),
    'shr364',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icy Manipulator'),
    (select id from sets where short_name = 'ptc'),
    'et322',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ptc'),
    'et375',
    'common'
) ,
(
    (select id from mtgcard where name = 'Svyelunite Temple'),
    (select id from sets where short_name = 'ptc'),
    'ml102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Land Tax'),
    (select id from sets where short_name = 'ptc'),
    'bl34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feldon''s Cane'),
    (select id from sets where short_name = 'ptc'),
    'll50sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Memory Lapse'),
    (select id from sets where short_name = 'ptc'),
    'shr32asb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Burn'),
    (select id from sets where short_name = 'ptc'),
    'll161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karplusan Forest'),
    (select id from sets where short_name = 'ptc'),
    'mj356',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ptc'),
    'bl378',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = 'ptc'),
    'mj219sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Detonate'),
    (select id from sets where short_name = 'ptc'),
    'mj184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = 'ptc'),
    'shr15sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howling Mine'),
    (select id from sets where short_name = 'ptc'),
    'mj325',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Autumn Willow'),
    (select id from sets where short_name = 'ptc'),
    'et83sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hallowed Ground'),
    (select id from sets where short_name = 'ptc'),
    'ml29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balance'),
    (select id from sets where short_name = 'ptc'),
    'bl6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea Sprite'),
    (select id from sets where short_name = 'ptc'),
    'ml38sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'ptc'),
    'mj112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zuran Orb'),
    (select id from sets where short_name = 'ptc'),
    'pp350',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'ptc'),
    'bl50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = 'ptc'),
    'pp338',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ptc'),
    'bl364',
    'common'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'ptc'),
    'shr112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Autumn Willow'),
    (select id from sets where short_name = 'ptc'),
    'pp83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dwarven Ruins'),
    (select id from sets where short_name = 'ptc'),
    'mj94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Vise'),
    (select id from sets where short_name = 'ptc'),
    'mj299',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zuran Orb'),
    (select id from sets where short_name = 'ptc'),
    'bl350',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elkin Bottle'),
    (select id from sets where short_name = 'ptc'),
    'mj317',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Specter'),
    (select id from sets where short_name = 'ptc'),
    'gb142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ptc'),
    'pp364',
    'common'
) ,
(
    (select id from mtgcard where name = 'Millstone'),
    (select id from sets where short_name = 'ptc'),
    'ml336',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Order of the Ebon Hand'),
    (select id from sets where short_name = 'ptc'),
    'gb42b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ptc'),
    'll372',
    'common'
) ,
(
    (select id from mtgcard where name = 'Autumn Willow'),
    (select id from sets where short_name = 'ptc'),
    'bl83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Safe Haven'),
    (select id from sets where short_name = 'ptc'),
    'll118sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hurricane'),
    (select id from sets where short_name = 'ptc'),
    'pp251',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karma'),
    (select id from sets where short_name = 'ptc'),
    'bl32sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ptc'),
    'mj365',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = 'ptc'),
    'gb338sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = 'ptc'),
    'gb219sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ptc'),
    'ml369',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'ptc'),
    'ml22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ivory Tower'),
    (select id from sets where short_name = 'ptc'),
    'll328',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = 'ptc'),
    'et189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Havenwood Battleground'),
    (select id from sets where short_name = 'ptc'),
    'pp96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jalum Tome'),
    (select id from sets where short_name = 'ptc'),
    'll54sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dance of the Dead'),
    (select id from sets where short_name = 'ptc'),
    'll118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Red Elemental Blast'),
    (select id from sets where short_name = 'ptc'),
    'gb218sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fellwar Stone'),
    (select id from sets where short_name = 'ptc'),
    'et319',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Vise'),
    (select id from sets where short_name = 'ptc'),
    'bl299sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = 'ptc'),
    'll338',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'ptc'),
    'ml58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zuran Orb'),
    (select id from sets where short_name = 'ptc'),
    'mj350',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ptc'),
    'et374',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'ptc'),
    'et52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'ptc'),
    'pp22sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormbind'),
    (select id from sets where short_name = 'ptc'),
    'et304',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'ptc'),
    'mj52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Library'),
    (select id from sets where short_name = 'ptc'),
    'bl273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ihsan''s Shade'),
    (select id from sets where short_name = 'ptc'),
    'gb53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Land Tax'),
    (select id from sets where short_name = 'ptc'),
    'pp34sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jayemdae Tome'),
    (select id from sets where short_name = 'ptc'),
    'ml331',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ptc'),
    'll371',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divine Offering'),
    (select id from sets where short_name = 'ptc'),
    'pp6sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ptc'),
    'bl365',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tranquility'),
    (select id from sets where short_name = 'ptc'),
    'gb277sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feldon''s Cane'),
    (select id from sets where short_name = 'ptc'),
    'ml50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'ptc'),
    'shr65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ptc'),
    'pp377',
    'common'
) ,
(
    (select id from mtgcard where name = 'Land Tax'),
    (select id from sets where short_name = 'ptc'),
    'ml34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Divine Offering'),
    (select id from sets where short_name = 'ptc'),
    'bl6sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stromgald Cabal'),
    (select id from sets where short_name = 'ptc'),
    'll166sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ptc'),
    'et366',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hymn to Tourach'),
    (select id from sets where short_name = 'ptc'),
    'll38c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = 'ptc'),
    'll338sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winter Orb'),
    (select id from sets where short_name = 'ptc'),
    'mj358',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ptc'),
    'shr367',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serrated Arrows'),
    (select id from sets where short_name = 'ptc'),
    'pp110sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disrupting Scepter'),
    (select id from sets where short_name = 'ptc'),
    'et316sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strip Mine'),
    (select id from sets where short_name = 'ptc'),
    'pp363',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ptc'),
    'shr368',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anarchy'),
    (select id from sets where short_name = 'ptc'),
    'mj49sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ruins of Trokair'),
    (select id from sets where short_name = 'ptc'),
    'shr100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spectral Bears'),
    (select id from sets where short_name = 'ptc'),
    'pp98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zuran Orb'),
    (select id from sets where short_name = 'ptc'),
    'shr350',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ptc'),
    'et364',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = 'ptc'),
    'bl5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ihsan''s Shade'),
    (select id from sets where short_name = 'ptc'),
    'gb53sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Divine Offering'),
    (select id from sets where short_name = 'ptc'),
    'et6sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'ptc'),
    'et50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Torture'),
    (select id from sets where short_name = 'ptc'),
    'll59sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erhnam Djinn'),
    (select id from sets where short_name = 'ptc'),
    'pp64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Archers'),
    (select id from sets where short_name = 'ptc'),
    'pp243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balance'),
    (select id from sets where short_name = 'ptc'),
    'et6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'ptc'),
    'shr52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zuran Orb'),
    (select id from sets where short_name = 'ptc'),
    'll350',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ptc'),
    'pp365',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ptc'),
    'pp366',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serrated Arrows'),
    (select id from sets where short_name = 'ptc'),
    'et110sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fellwar Stone'),
    (select id from sets where short_name = 'ptc'),
    'bl319',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = 'ptc'),
    'shr361',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = 'ptc'),
    'et17sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = 'ptc'),
    'bl15sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serrated Arrows'),
    (select id from sets where short_name = 'ptc'),
    'gb110sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Svyelunite Temple'),
    (select id from sets where short_name = 'ptc'),
    'shr102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Order of the Ebon Hand'),
    (select id from sets where short_name = 'ptc'),
    'll42a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruins of Trokair'),
    (select id from sets where short_name = 'ptc'),
    'bl100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyroblast'),
    (select id from sets where short_name = 'ptc'),
    'mj213sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Essence Filter'),
    (select id from sets where short_name = 'ptc'),
    'pp233sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'ptc'),
    'gb112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = 'ptc'),
    'pp5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruins of Trokair'),
    (select id from sets where short_name = 'ptc'),
    'pp100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aeolipile'),
    (select id from sets where short_name = 'ptc'),
    'pp81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'ptc'),
    'et58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'ptc'),
    'bl261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Green'),
    (select id from sets where short_name = 'ptc'),
    'bl14sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = 'ptc'),
    'ml17sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ptc'),
    'mj373',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deflection'),
    (select id from sets where short_name = 'ptc'),
    'ml81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Truce'),
    (select id from sets where short_name = 'ptc'),
    'shr20sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'ptc'),
    'mj208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divine Offering'),
    (select id from sets where short_name = 'ptc'),
    'shr6sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icy Manipulator'),
    (select id from sets where short_name = 'ptc'),
    'pp322',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ptc'),
    'gb372',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zuran Orb'),
    (select id from sets where short_name = 'ptc'),
    'ml350',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hymn to Tourach'),
    (select id from sets where short_name = 'ptc'),
    'gb38c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Order of Leitbur'),
    (select id from sets where short_name = 'ptc'),
    'bl16b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ring of Renewal'),
    (select id from sets where short_name = 'ptc'),
    'et89sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Icy Manipulator'),
    (select id from sets where short_name = 'ptc'),
    'ml322',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serrated Arrows'),
    (select id from sets where short_name = 'ptc'),
    'mj110sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Sink'),
    (select id from sets where short_name = 'ptc'),
    'shr93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'ptc'),
    'shr58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Green'),
    (select id from sets where short_name = 'ptc'),
    'bl16sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = 'ptc'),
    'pp361',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reverse Damage'),
    (select id from sets where short_name = 'ptc'),
    'et45sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drain Life'),
    (select id from sets where short_name = 'ptc'),
    'll132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whirling Dervish'),
    (select id from sets where short_name = 'ptc'),
    'bl288sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hymn to Tourach'),
    (select id from sets where short_name = 'ptc'),
    'gb38a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ptc'),
    'mj374',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ptc'),
    'ml365',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brushland'),
    (select id from sets where short_name = 'ptc'),
    'bl352',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knight of Stromgald'),
    (select id from sets where short_name = 'ptc'),
    'gb138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karplusan Forest'),
    (select id from sets where short_name = 'ptc'),
    'gb356',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'ptc'),
    'shr22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fellwar Stone'),
    (select id from sets where short_name = 'ptc'),
    'pp319',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Black'),
    (select id from sets where short_name = 'ptc'),
    'shr14sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = 'ptc'),
    'gb361',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Autumn Willow'),
    (select id from sets where short_name = 'ptc'),
    'et83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = 'ptc'),
    'pp5sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ivory Tower'),
    (select id from sets where short_name = 'ptc'),
    'bl328',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Apocalypse Chime'),
    (select id from sets where short_name = 'ptc'),
    'et101sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abbey Gargoyles'),
    (select id from sets where short_name = 'ptc'),
    'bl1sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wizards'' School'),
    (select id from sets where short_name = 'ptc'),
    'ml115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fellwar Stone'),
    (select id from sets where short_name = 'ptc'),
    'mj319',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brushland'),
    (select id from sets where short_name = 'ptc'),
    'et352',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Havenwood Battleground'),
    (select id from sets where short_name = 'ptc'),
    'mj96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hydroblast'),
    (select id from sets where short_name = 'ptc'),
    'ml72sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erhnam Djinn'),
    (select id from sets where short_name = 'ptc'),
    'bl64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jalum Tome'),
    (select id from sets where short_name = 'ptc'),
    'll54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Detonate'),
    (select id from sets where short_name = 'ptc'),
    'mj184sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Specter'),
    (select id from sets where short_name = 'ptc'),
    'll142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stormbind'),
    (select id from sets where short_name = 'ptc'),
    'mj304',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ptc'),
    'shr366',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aeolipile'),
    (select id from sets where short_name = 'ptc'),
    'ml81sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serrated Arrows'),
    (select id from sets where short_name = 'ptc'),
    'll110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Memory Lapse'),
    (select id from sets where short_name = 'ptc'),
    'shr32bsb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blinking Spirit'),
    (select id from sets where short_name = 'ptc'),
    'et8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fountain of Youth'),
    (select id from sets where short_name = 'ptc'),
    'ml98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Apocalypse Chime'),
    (select id from sets where short_name = 'ptc'),
    'll101sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea Sprite'),
    (select id from sets where short_name = 'ptc'),
    'shr38sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'ptc'),
    'bl22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = 'ptc'),
    'et234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abbey Gargoyles'),
    (select id from sets where short_name = 'ptc'),
    'shr1sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Energy Storm'),
    (select id from sets where short_name = 'ptc'),
    'et24sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ptc'),
    'bl376',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karma'),
    (select id from sets where short_name = 'ptc'),
    'pp32sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'ptc'),
    'et52sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serrated Arrows'),
    (select id from sets where short_name = 'ptc'),
    'll110sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Land Tax'),
    (select id from sets where short_name = 'ptc'),
    'pp34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hymn to Tourach'),
    (select id from sets where short_name = 'ptc'),
    'gb38d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ptc'),
    'mj366',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fellwar Stone'),
    (select id from sets where short_name = 'ptc'),
    'pp319sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steal Artifact'),
    (select id from sets where short_name = 'ptc'),
    'ml105sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ptc'),
    'shr369',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ivory Tower'),
    (select id from sets where short_name = 'ptc'),
    'shr328',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = 'ptc'),
    'et361',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ptc'),
    'ml367',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jayemdae Tome'),
    (select id from sets where short_name = 'ptc'),
    'et331',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brushland'),
    (select id from sets where short_name = 'ptc'),
    'pp352',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Land Tax'),
    (select id from sets where short_name = 'ptc'),
    'et34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'ptc'),
    'mj192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balance'),
    (select id from sets where short_name = 'ptc'),
    'mj6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Erhnam Djinn'),
    (select id from sets where short_name = 'ptc'),
    'et64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fountain of Youth'),
    (select id from sets where short_name = 'ptc'),
    'mj98sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serrated Arrows'),
    (select id from sets where short_name = 'ptc'),
    'ml110sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hymn to Tourach'),
    (select id from sets where short_name = 'ptc'),
    'gb38b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Order of the Ebon Hand'),
    (select id from sets where short_name = 'ptc'),
    'll42b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'ptc'),
    'pp58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Adarkar Wastes'),
    (select id from sets where short_name = 'ptc'),
    'shr351',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hymn to Tourach'),
    (select id from sets where short_name = 'ptc'),
    'll38b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ptc'),
    'ml364',
    'common'
) ,
(
    (select id from mtgcard where name = 'Adarkar Wastes'),
    (select id from sets where short_name = 'ptc'),
    'ml351',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strip Mine'),
    (select id from sets where short_name = 'ptc'),
    'bl363',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blinking Spirit'),
    (select id from sets where short_name = 'ptc'),
    'ml8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ptc'),
    'bl366',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ptc'),
    'pp378',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'ptc'),
    'pp52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = 'ptc'),
    'mj361',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'ptc'),
    'et192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icy Manipulator'),
    (select id from sets where short_name = 'ptc'),
    'mj322',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Divine Offering'),
    (select id from sets where short_name = 'ptc'),
    'ml6sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'ptc'),
    'pp22',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Rack'),
    (select id from sets where short_name = 'ptc'),
    'll352sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = 'ptc'),
    'mj219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Town'),
    (select id from sets where short_name = 'ptc'),
    'et15sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Order of Leitbur'),
    (select id from sets where short_name = 'ptc'),
    'bl16csb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spell Blast'),
    (select id from sets where short_name = 'ptc'),
    'shr103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Land Tax'),
    (select id from sets where short_name = 'ptc'),
    'shr34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruins of Trokair'),
    (select id from sets where short_name = 'ptc'),
    'ml100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ptc'),
    'shr365',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of Stromgald'),
    (select id from sets where short_name = 'ptc'),
    'll138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'ptc'),
    'bl52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = 'ptc'),
    'mj189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'ptc'),
    'bl58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Order of the Ebon Hand'),
    (select id from sets where short_name = 'ptc'),
    'gb42a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Recall'),
    (select id from sets where short_name = 'ptc'),
    'shr24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ptc'),
    'mj375',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ptc'),
    'pp376',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'ptc'),
    'gb208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Recall'),
    (select id from sets where short_name = 'ptc'),
    'ml24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Order of Leitbur'),
    (select id from sets where short_name = 'ptc'),
    'bl16a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ptc'),
    'et373',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barbed Sextant'),
    (select id from sets where short_name = 'ptc'),
    'gb312',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feldon''s Cane'),
    (select id from sets where short_name = 'ptc'),
    'shr50sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steal Artifact'),
    (select id from sets where short_name = 'ptc'),
    'shr105sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reverse Damage'),
    (select id from sets where short_name = 'ptc'),
    'pp45sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ptc'),
    'mj364',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zur''s Weirding'),
    (select id from sets where short_name = 'ptc'),
    'et110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balance'),
    (select id from sets where short_name = 'ptc'),
    'pp6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Havenwood Battleground'),
    (select id from sets where short_name = 'ptc'),
    'bl96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balance'),
    (select id from sets where short_name = 'ptc'),
    'shr6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'ptc'),
    'll129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fountain of Youth'),
    (select id from sets where short_name = 'ptc'),
    'shr98sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ptc'),
    'ml368',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Town'),
    (select id from sets where short_name = 'ptc'),
    'et15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = 'ptc'),
    'bl17sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ptc'),
    'ml366',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ptc'),
    'bl377',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ivory Tower'),
    (select id from sets where short_name = 'ptc'),
    'mj328',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meekstone'),
    (select id from sets where short_name = 'ptc'),
    'll335sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feldon''s Cane'),
    (select id from sets where short_name = 'ptc'),
    'mj50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Millstone'),
    (select id from sets where short_name = 'ptc'),
    'shr336',
    'rare'
) 
 on conflict do nothing;
