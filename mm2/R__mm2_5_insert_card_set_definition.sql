insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Spread the Sickness'),
    (select id from sets where short_name = 'mm2'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vendilion Clique'),
    (select id from sets where short_name = 'mm2'),
    '67',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Incandescent Soulstoke'),
    (select id from sets where short_name = 'mm2'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Guildmage'),
    (select id from sets where short_name = 'mm2'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cryptic Command'),
    (select id from sets where short_name = 'mm2'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conclave Phalanx'),
    (select id from sets where short_name = 'mm2'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moonlit Strider'),
    (select id from sets where short_name = 'mm2'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Confidant'),
    (select id from sets where short_name = 'mm2'),
    '75',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hearthfire Hobgoblin'),
    (select id from sets where short_name = 'mm2'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = 'mm2'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glint Hawk Idol'),
    (select id from sets where short_name = 'mm2'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wilt-Leaf Liege'),
    (select id from sets where short_name = 'mm2'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Water Servant'),
    (select id from sets where short_name = 'mm2'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Matca Rioters'),
    (select id from sets where short_name = 'mm2'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nobilis of War'),
    (select id from sets where short_name = 'mm2'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inner-Flame Igniter'),
    (select id from sets where short_name = 'mm2'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Guildmage'),
    (select id from sets where short_name = 'mm2'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ethercaste Knight'),
    (select id from sets where short_name = 'mm2'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wayfarer''s Bauble'),
    (select id from sets where short_name = 'mm2'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necroskitter'),
    (select id from sets where short_name = 'mm2'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sign in Blood'),
    (select id from sets where short_name = 'mm2'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul Turf'),
    (select id from sets where short_name = 'mm2'),
    '244',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Somber Hoverguard'),
    (select id from sets where short_name = 'mm2'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spikeshot Elder'),
    (select id from sets where short_name = 'mm2'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'All Suns'' Dawn'),
    (select id from sets where short_name = 'mm2'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death Denied'),
    (select id from sets where short_name = 'mm2'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plaxcaster Frogling'),
    (select id from sets where short_name = 'mm2'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Worldheart Phoenix'),
    (select id from sets where short_name = 'mm2'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Temple'),
    (select id from sets where short_name = 'mm2'),
    '240',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Comet Storm'),
    (select id from sets where short_name = 'mm2'),
    '111',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Battlegrace Angel'),
    (select id from sets where short_name = 'mm2'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tarmogoyf'),
    (select id from sets where short_name = 'mm2'),
    '165',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bloodthrone Vampire'),
    (select id from sets where short_name = 'mm2'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daybreak Coronet'),
    (select id from sets where short_name = 'mm2'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiery Fall'),
    (select id from sets where short_name = 'mm2'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cytoplast Root-Kin'),
    (select id from sets where short_name = 'mm2'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dispatch'),
    (select id from sets where short_name = 'mm2'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thunderblust'),
    (select id from sets where short_name = 'mm2'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Copper Carapace'),
    (select id from sets where short_name = 'mm2'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vengeful Rebirth'),
    (select id from sets where short_name = 'mm2'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pillory of the Sleepless'),
    (select id from sets where short_name = 'mm2'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mutagenic Growth'),
    (select id from sets where short_name = 'mm2'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pelakka Wurm'),
    (select id from sets where short_name = 'mm2'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Etched Champion'),
    (select id from sets where short_name = 'mm2'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emrakul, the Aeons Torn'),
    (select id from sets where short_name = 'mm2'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Elesh Norn, Grand Cenobite'),
    (select id from sets where short_name = 'mm2'),
    '16',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Agony Warp'),
    (select id from sets where short_name = 'mm2'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Electrolyze'),
    (select id from sets where short_name = 'mm2'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampire Outcasts'),
    (select id from sets where short_name = 'mm2'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plummet'),
    (select id from sets where short_name = 'mm2'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bitterblossom'),
    (select id from sets where short_name = 'mm2'),
    '71',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Qumulox'),
    (select id from sets where short_name = 'mm2'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Helium Squirter'),
    (select id from sets where short_name = 'mm2'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloud Elemental'),
    (select id from sets where short_name = 'mm2'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darksteel Citadel'),
    (select id from sets where short_name = 'mm2'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Snake'),
    (select id from sets where short_name = 'mm2'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shadowmage Infiltrator'),
    (select id from sets where short_name = 'mm2'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Waking Nightmare'),
    (select id from sets where short_name = 'mm2'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dismember'),
    (select id from sets where short_name = 'mm2'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shrewd Hatchling'),
    (select id from sets where short_name = 'mm2'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyreach Manta'),
    (select id from sets where short_name = 'mm2'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragonsoul Knight'),
    (select id from sets where short_name = 'mm2'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chimeric Mass'),
    (select id from sets where short_name = 'mm2'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Novijen Sages'),
    (select id from sets where short_name = 'mm2'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overwhelming Stampede'),
    (select id from sets where short_name = 'mm2'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inexorable Tide'),
    (select id from sets where short_name = 'mm2'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunlance'),
    (select id from sets where short_name = 'mm2'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Narcolepsy'),
    (select id from sets where short_name = 'mm2'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Lacerator'),
    (select id from sets where short_name = 'mm2'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Puppeteer Clique'),
    (select id from sets where short_name = 'mm2'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spellskite'),
    (select id from sets where short_name = 'mm2'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Everflowing Chalice'),
    (select id from sets where short_name = 'mm2'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terashi''s Grasp'),
    (select id from sets where short_name = 'mm2'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gut Shot'),
    (select id from sets where short_name = 'mm2'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azorius Chancery'),
    (select id from sets where short_name = 'mm2'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thief of Hope'),
    (select id from sets where short_name = 'mm2'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ant Queen'),
    (select id from sets where short_name = 'mm2'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Devouring Greed'),
    (select id from sets where short_name = 'mm2'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dread Drone'),
    (select id from sets where short_name = 'mm2'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flayer Husk'),
    (select id from sets where short_name = 'mm2'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vigean Graftmage'),
    (select id from sets where short_name = 'mm2'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kitesail'),
    (select id from sets where short_name = 'mm2'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scavenger Drake'),
    (select id from sets where short_name = 'mm2'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'mm2'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reassembling Skeleton'),
    (select id from sets where short_name = 'mm2'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin War Paint'),
    (select id from sets where short_name = 'mm2'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nest Invader'),
    (select id from sets where short_name = 'mm2'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nameless Inversion'),
    (select id from sets where short_name = 'mm2'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scute Mob'),
    (select id from sets where short_name = 'mm2'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Rot Farm'),
    (select id from sets where short_name = 'mm2'),
    '243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Splinter Twin'),
    (select id from sets where short_name = 'mm2'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Root-Kin Ally'),
    (select id from sets where short_name = 'mm2'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vapor Snag'),
    (select id from sets where short_name = 'mm2'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Court Homunculus'),
    (select id from sets where short_name = 'mm2'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lorescale Coatl'),
    (select id from sets where short_name = 'mm2'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghost Council of Orzhova'),
    (select id from sets where short_name = 'mm2'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Creakwood Liege'),
    (select id from sets where short_name = 'mm2'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cathodion'),
    (select id from sets where short_name = 'mm2'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrive'),
    (select id from sets where short_name = 'mm2'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Otherworldly Journey'),
    (select id from sets where short_name = 'mm2'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karplusan Strider'),
    (select id from sets where short_name = 'mm2'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darksteel Axe'),
    (select id from sets where short_name = 'mm2'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Swiftblade'),
    (select id from sets where short_name = 'mm2'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lodestone Myr'),
    (select id from sets where short_name = 'mm2'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Iona, Shield of Emeria'),
    (select id from sets where short_name = 'mm2'),
    '20',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Izzet Boilerworks'),
    (select id from sets where short_name = 'mm2'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vines of Vastwood'),
    (select id from sets where short_name = 'mm2'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savage Twister'),
    (select id from sets where short_name = 'mm2'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Faerie Mechanist'),
    (select id from sets where short_name = 'mm2'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duskhunter Bat'),
    (select id from sets where short_name = 'mm2'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'mm2'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Long-Forgotten Gohei'),
    (select id from sets where short_name = 'mm2'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lodestone Golem'),
    (select id from sets where short_name = 'mm2'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Initiate'),
    (select id from sets where short_name = 'mm2'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scatter the Seeds'),
    (select id from sets where short_name = 'mm2'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mox Opal'),
    (select id from sets where short_name = 'mm2'),
    '223',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Banefire'),
    (select id from sets where short_name = 'mm2'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soulbright Flamekin'),
    (select id from sets where short_name = 'mm2'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunspear Shikari'),
    (select id from sets where short_name = 'mm2'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kozilek, Butcher of Truth'),
    (select id from sets where short_name = 'mm2'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Surgical Extraction'),
    (select id from sets where short_name = 'mm2'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodshot Trainee'),
    (select id from sets where short_name = 'mm2'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Aqueduct'),
    (select id from sets where short_name = 'mm2'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Restless Apparition'),
    (select id from sets where short_name = 'mm2'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steady Progress'),
    (select id from sets where short_name = 'mm2'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sigil Blessing'),
    (select id from sets where short_name = 'mm2'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Taj-Nar Swordsmith'),
    (select id from sets where short_name = 'mm2'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bestial Menace'),
    (select id from sets where short_name = 'mm2'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hurkyl''s Recall'),
    (select id from sets where short_name = 'mm2'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Garrison'),
    (select id from sets where short_name = 'mm2'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kami of Ancient Law'),
    (select id from sets where short_name = 'mm2'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rusted Relic'),
    (select id from sets where short_name = 'mm2'),
    '227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blinding Souleater'),
    (select id from sets where short_name = 'mm2'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghostly Changeling'),
    (select id from sets where short_name = 'mm2'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necrogenesis'),
    (select id from sets where short_name = 'mm2'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scuttling Death'),
    (select id from sets where short_name = 'mm2'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrap in Flames'),
    (select id from sets where short_name = 'mm2'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sickle Ripper'),
    (select id from sets where short_name = 'mm2'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blades of Velis Vel'),
    (select id from sets where short_name = 'mm2'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plagued Rusalka'),
    (select id from sets where short_name = 'mm2'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Etched Oracle'),
    (select id from sets where short_name = 'mm2'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thoughtcast'),
    (select id from sets where short_name = 'mm2'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Air Servant'),
    (select id from sets where short_name = 'mm2'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashenmoor Gouger'),
    (select id from sets where short_name = 'mm2'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Carnarium'),
    (select id from sets where short_name = 'mm2'),
    '247',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daggerclaw Imp'),
    (select id from sets where short_name = 'mm2'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fulminator Mage'),
    (select id from sets where short_name = 'mm2'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Apocalypse Hydra'),
    (select id from sets where short_name = 'mm2'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overwhelm'),
    (select id from sets where short_name = 'mm2'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Niv-Mizzet, the Firemind'),
    (select id from sets where short_name = 'mm2'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Indomitable Archangel'),
    (select id from sets where short_name = 'mm2'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphere of the Suns'),
    (select id from sets where short_name = 'mm2'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spectral Procession'),
    (select id from sets where short_name = 'mm2'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Argent Sphinx'),
    (select id from sets where short_name = 'mm2'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scion of the Wild'),
    (select id from sets where short_name = 'mm2'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirror Entity'),
    (select id from sets where short_name = 'mm2'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arrest'),
    (select id from sets where short_name = 'mm2'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ulamog, the Infinite Gyre'),
    (select id from sets where short_name = 'mm2'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Combust'),
    (select id from sets where short_name = 'mm2'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Surrakar Spellblade'),
    (select id from sets where short_name = 'mm2'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viashino Slaughtermaster'),
    (select id from sets where short_name = 'mm2'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sundering Vitae'),
    (select id from sets where short_name = 'mm2'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horde of Notions'),
    (select id from sets where short_name = 'mm2'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Commune with Nature'),
    (select id from sets where short_name = 'mm2'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Profane Command'),
    (select id from sets where short_name = 'mm2'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Instill Infection'),
    (select id from sets where short_name = 'mm2'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aethersnipe'),
    (select id from sets where short_name = 'mm2'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swans of Bryn Argoll'),
    (select id from sets where short_name = 'mm2'),
    '199',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blinkmoth Nexus'),
    (select id from sets where short_name = 'mm2'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cranial Plating'),
    (select id from sets where short_name = 'mm2'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blood Ogre'),
    (select id from sets where short_name = 'mm2'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Endrek Sahr, Master Breeder'),
    (select id from sets where short_name = 'mm2'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stormblood Berserker'),
    (select id from sets where short_name = 'mm2'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deathmark'),
    (select id from sets where short_name = 'mm2'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Simic Growth Chamber'),
    (select id from sets where short_name = 'mm2'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Skirmisher'),
    (select id from sets where short_name = 'mm2'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Telling Time'),
    (select id from sets where short_name = 'mm2'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skarrgan Firebird'),
    (select id from sets where short_name = 'mm2'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Noble Hierarch'),
    (select id from sets where short_name = 'mm2'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aquastrand Spider'),
    (select id from sets where short_name = 'mm2'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drooling Groodion'),
    (select id from sets where short_name = 'mm2'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kozilek''s Predator'),
    (select id from sets where short_name = 'mm2'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raise the Alarm'),
    (select id from sets where short_name = 'mm2'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kavu Primarch'),
    (select id from sets where short_name = 'mm2'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hellkite Charger'),
    (select id from sets where short_name = 'mm2'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frogmite'),
    (select id from sets where short_name = 'mm2'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leyline of Sanctity'),
    (select id from sets where short_name = 'mm2'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mighty Leap'),
    (select id from sets where short_name = 'mm2'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Algae Gharial'),
    (select id from sets where short_name = 'mm2'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fortify'),
    (select id from sets where short_name = 'mm2'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tezzeret''s Gambit'),
    (select id from sets where short_name = 'mm2'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guile'),
    (select id from sets where short_name = 'mm2'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Bounty'),
    (select id from sets where short_name = 'mm2'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hikari, Twilight Guardian'),
    (select id from sets where short_name = 'mm2'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smokebraider'),
    (select id from sets where short_name = 'mm2'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orzhov Basilica'),
    (select id from sets where short_name = 'mm2'),
    '246',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oblivion Ring'),
    (select id from sets where short_name = 'mm2'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'All Is Dust'),
    (select id from sets where short_name = 'mm2'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tumble Magnet'),
    (select id from sets where short_name = 'mm2'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gnarlid Pack'),
    (select id from sets where short_name = 'mm2'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wildfire'),
    (select id from sets where short_name = 'mm2'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Culling Dais'),
    (select id from sets where short_name = 'mm2'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tukatongue Thallid'),
    (select id from sets where short_name = 'mm2'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Expedition Map'),
    (select id from sets where short_name = 'mm2'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Artisan of Kozilek'),
    (select id from sets where short_name = 'mm2'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Midnight Banshee'),
    (select id from sets where short_name = 'mm2'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wings of Velis Vel'),
    (select id from sets where short_name = 'mm2'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karn Liberated'),
    (select id from sets where short_name = 'mm2'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Celestial Purge'),
    (select id from sets where short_name = 'mm2'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gorehorn Minotaurs'),
    (select id from sets where short_name = 'mm2'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tezzeret the Seeker'),
    (select id from sets where short_name = 'mm2'),
    '62',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Glassdust Hulk'),
    (select id from sets where short_name = 'mm2'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tribal Flames'),
    (select id from sets where short_name = 'mm2'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eye of Ugin'),
    (select id from sets where short_name = 'mm2'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kor Duelist'),
    (select id from sets where short_name = 'mm2'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mulldrifter'),
    (select id from sets where short_name = 'mm2'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grim Affliction'),
    (select id from sets where short_name = 'mm2'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrecking Ball'),
    (select id from sets where short_name = 'mm2'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bone Splinters'),
    (select id from sets where short_name = 'mm2'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shrivel'),
    (select id from sets where short_name = 'mm2'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Sanctuary'),
    (select id from sets where short_name = 'mm2'),
    '248',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Remand'),
    (select id from sets where short_name = 'mm2'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Precursor Golem'),
    (select id from sets where short_name = 'mm2'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gust-Skimmer'),
    (select id from sets where short_name = 'mm2'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrummingbird'),
    (select id from sets where short_name = 'mm2'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunforger'),
    (select id from sets where short_name = 'mm2'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myr Enforcer'),
    (select id from sets where short_name = 'mm2'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Repeal'),
    (select id from sets where short_name = 'mm2'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smash to Smithereens'),
    (select id from sets where short_name = 'mm2'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Fireslinger'),
    (select id from sets where short_name = 'mm2'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stoic Rebuttal'),
    (select id from sets where short_name = 'mm2'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Leak'),
    (select id from sets where short_name = 'mm2'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sickleslicer'),
    (select id from sets where short_name = 'mm2'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primeval Titan'),
    (select id from sets where short_name = 'mm2'),
    '156',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Spitebellows'),
    (select id from sets where short_name = 'mm2'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brute Force'),
    (select id from sets where short_name = 'mm2'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runed Servitor'),
    (select id from sets where short_name = 'mm2'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mortarpod'),
    (select id from sets where short_name = 'mm2'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burst Lightning'),
    (select id from sets where short_name = 'mm2'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alloy Myr'),
    (select id from sets where short_name = 'mm2'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ulamog''s Crusher'),
    (select id from sets where short_name = 'mm2'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Etched Monstrosity'),
    (select id from sets where short_name = 'mm2'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirran Crusader'),
    (select id from sets where short_name = 'mm2'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flashfreeze'),
    (select id from sets where short_name = 'mm2'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kiki-Jiki, Mirror Breaker'),
    (select id from sets where short_name = 'mm2'),
    '121',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wolfbriar Elemental'),
    (select id from sets where short_name = 'mm2'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Waxmane Baku'),
    (select id from sets where short_name = 'mm2'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Apostle''s Blessing'),
    (select id from sets where short_name = 'mm2'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myrsmith'),
    (select id from sets where short_name = 'mm2'),
    '28',
    'uncommon'
) 
 on conflict do nothing;
