insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Call to Heel'),
    (select id from sets where short_name = 'ala'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight-Captain of Eos'),
    (select id from sets where short_name = 'ala'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Godtoucher'),
    (select id from sets where short_name = 'ala'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Esper Panorama'),
    (select id from sets where short_name = 'ala'),
    '223',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savage Hunger'),
    (select id from sets where short_name = 'ala'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mosstodon'),
    (select id from sets where short_name = 'ala'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelsong'),
    (select id from sets where short_name = 'ala'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Filigree Sages'),
    (select id from sets where short_name = 'ala'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ad Nauseam'),
    (select id from sets where short_name = 'ala'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Immortal Coil'),
    (select id from sets where short_name = 'ala'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Bant'),
    (select id from sets where short_name = 'ala'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Etherium Sculptor'),
    (select id from sets where short_name = 'ala'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cruel Ultimatum'),
    (select id from sets where short_name = 'ala'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Submersion'),
    (select id from sets where short_name = 'ala'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tower Gargoyle'),
    (select id from sets where short_name = 'ala'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swerve'),
    (select id from sets where short_name = 'ala'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ala'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spearbreaker Behemoth'),
    (select id from sets where short_name = 'ala'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kathari Screecher'),
    (select id from sets where short_name = 'ala'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mycoloth'),
    (select id from sets where short_name = 'ala'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shore Snapper'),
    (select id from sets where short_name = 'ala'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sangrite Surge'),
    (select id from sets where short_name = 'ala'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sharding Sphinx'),
    (select id from sets where short_name = 'ala'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jhessian Infiltrator'),
    (select id from sets where short_name = 'ala'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gift of the Gargantuan'),
    (select id from sets where short_name = 'ala'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Welkin Guide'),
    (select id from sets where short_name = 'ala'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Esper'),
    (select id from sets where short_name = 'ala'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shadowfeed'),
    (select id from sets where short_name = 'ala'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhox War Monk'),
    (select id from sets where short_name = 'ala'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ranger of Eos'),
    (select id from sets where short_name = 'ala'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tidehollow Sculler'),
    (select id from sets where short_name = 'ala'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Mountaineer'),
    (select id from sets where short_name = 'ala'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of the Skyward Eye'),
    (select id from sets where short_name = 'ala'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctum Gargoyle'),
    (select id from sets where short_name = 'ala'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necrogenesis'),
    (select id from sets where short_name = 'ala'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ala'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Resounding Roar'),
    (select id from sets where short_name = 'ala'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rafiq of the Many'),
    (select id from sets where short_name = 'ala'),
    '185',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Memory Erosion'),
    (select id from sets where short_name = 'ala'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Protomatter Powder'),
    (select id from sets where short_name = 'ala'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ala'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ethersworn Canonist'),
    (select id from sets where short_name = 'ala'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Salvage Titan'),
    (select id from sets where short_name = 'ala'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ala'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bant Battlemage'),
    (select id from sets where short_name = 'ala'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Esper Battlemage'),
    (select id from sets where short_name = 'ala'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon''s Herald'),
    (select id from sets where short_name = 'ala'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coma Veil'),
    (select id from sets where short_name = 'ala'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glaze Fiend'),
    (select id from sets where short_name = 'ala'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ala'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ala'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scavenger Drake'),
    (select id from sets where short_name = 'ala'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mayael the Anima'),
    (select id from sets where short_name = 'ala'),
    '179',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Master of Etherium'),
    (select id from sets where short_name = 'ala'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cavern Thoctar'),
    (select id from sets where short_name = 'ala'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Predator Dragon'),
    (select id from sets where short_name = 'ala'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel''s Herald'),
    (select id from sets where short_name = 'ala'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Onyx Goblet'),
    (select id from sets where short_name = 'ala'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ala'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crucible of Fire'),
    (select id from sets where short_name = 'ala'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Courier''s Capsule'),
    (select id from sets where short_name = 'ala'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clarion Ultimatum'),
    (select id from sets where short_name = 'ala'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tezzeret the Seeker'),
    (select id from sets where short_name = 'ala'),
    '60',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blightning'),
    (select id from sets where short_name = 'ala'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul''s Grace'),
    (select id from sets where short_name = 'ala'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ridge Rannet'),
    (select id from sets where short_name = 'ala'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Empyrial Archangel'),
    (select id from sets where short_name = 'ala'),
    '166',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Elspeth, Knight-Errant'),
    (select id from sets where short_name = 'ala'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ala'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feral Hydra'),
    (select id from sets where short_name = 'ala'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Naya Charm'),
    (select id from sets where short_name = 'ala'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cancel'),
    (select id from sets where short_name = 'ala'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Executioner''s Capsule'),
    (select id from sets where short_name = 'ala'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tidehollow Strix'),
    (select id from sets where short_name = 'ala'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rockcaster Platoon'),
    (select id from sets where short_name = 'ala'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Metallurgeon'),
    (select id from sets where short_name = 'ala'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sigiled Paladin'),
    (select id from sets where short_name = 'ala'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dregscape Zombie'),
    (select id from sets where short_name = 'ala'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hissing Iguanar'),
    (select id from sets where short_name = 'ala'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Baron'),
    (select id from sets where short_name = 'ala'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grixis Battlemage'),
    (select id from sets where short_name = 'ala'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guardians of Akrasa'),
    (select id from sets where short_name = 'ala'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naya Battlemage'),
    (select id from sets where short_name = 'ala'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Grixis'),
    (select id from sets where short_name = 'ala'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gustrider Exuberant'),
    (select id from sets where short_name = 'ala'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flameblast Dragon'),
    (select id from sets where short_name = 'ala'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thunder-Thrash Elder'),
    (select id from sets where short_name = 'ala'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mindlock Orb'),
    (select id from sets where short_name = 'ala'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prince of Thralls'),
    (select id from sets where short_name = 'ala'),
    '182',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Brilliant Ultimatum'),
    (select id from sets where short_name = 'ala'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akrasan Squire'),
    (select id from sets where short_name = 'ala'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tortoise Formation'),
    (select id from sets where short_name = 'ala'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Incurable Ogre'),
    (select id from sets where short_name = 'ala'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Resounding Silence'),
    (select id from sets where short_name = 'ala'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minion Reflector'),
    (select id from sets where short_name = 'ala'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wild Nacatl'),
    (select id from sets where short_name = 'ala'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Weaver'),
    (select id from sets where short_name = 'ala'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ooze Garden'),
    (select id from sets where short_name = 'ala'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stoic Angel'),
    (select id from sets where short_name = 'ala'),
    '199',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knight of the White Orchid'),
    (select id from sets where short_name = 'ala'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Resounding Scream'),
    (select id from sets where short_name = 'ala'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sharuum the Hegemon'),
    (select id from sets where short_name = 'ala'),
    '194',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bant Charm'),
    (select id from sets where short_name = 'ala'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Resounding Thunder'),
    (select id from sets where short_name = 'ala'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Topan Ascetic'),
    (select id from sets where short_name = 'ala'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viscera Dragger'),
    (select id from sets where short_name = 'ala'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcane Sanctum'),
    (select id from sets where short_name = 'ala'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Covenant of Minds'),
    (select id from sets where short_name = 'ala'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Excommunicate'),
    (select id from sets where short_name = 'ala'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kederekt Creeper'),
    (select id from sets where short_name = 'ala'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sedris, the Traitor King'),
    (select id from sets where short_name = 'ala'),
    '193',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Magma Spray'),
    (select id from sets where short_name = 'ala'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire-Field Ogre'),
    (select id from sets where short_name = 'ala'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Invincible Hymn'),
    (select id from sets where short_name = 'ala'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steelclad Serpent'),
    (select id from sets where short_name = 'ala'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = 'ala'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Deathraiders'),
    (select id from sets where short_name = 'ala'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul''s Might'),
    (select id from sets where short_name = 'ala'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lich''s Mirror'),
    (select id from sets where short_name = 'ala'),
    '210',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Where Ancients Tread'),
    (select id from sets where short_name = 'ala'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jund Charm'),
    (select id from sets where short_name = 'ala'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Undead Leotau'),
    (select id from sets where short_name = 'ala'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ala'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woolly Thoctar'),
    (select id from sets where short_name = 'ala'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fatestitcher'),
    (select id from sets where short_name = 'ala'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ala'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Violent Ultimatum'),
    (select id from sets where short_name = 'ala'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blister Beetle'),
    (select id from sets where short_name = 'ala'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudheath Drake'),
    (select id from sets where short_name = 'ala'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hell''s Thunder'),
    (select id from sets where short_name = 'ala'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dawnray Archer'),
    (select id from sets where short_name = 'ala'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodthorn Taunter'),
    (select id from sets where short_name = 'ala'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Caldera Hellion'),
    (select id from sets where short_name = 'ala'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hellkite Overlord'),
    (select id from sets where short_name = 'ala'),
    '172',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sprouting Thrinax'),
    (select id from sets where short_name = 'ala'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oblivion Ring'),
    (select id from sets where short_name = 'ala'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodpyre Elemental'),
    (select id from sets where short_name = 'ala'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhox Charger'),
    (select id from sets where short_name = 'ala'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sigil Blessing'),
    (select id from sets where short_name = 'ala'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thoughtcutter Agent'),
    (select id from sets where short_name = 'ala'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Titanic Ultimatum'),
    (select id from sets where short_name = 'ala'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rip-Clan Crasher'),
    (select id from sets where short_name = 'ala'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cathartic Adept'),
    (select id from sets where short_name = 'ala'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mighty Emergence'),
    (select id from sets where short_name = 'ala'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ajani Vengeant'),
    (select id from sets where short_name = 'ala'),
    '154',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lightning Talons'),
    (select id from sets where short_name = 'ala'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relic of Progenitus'),
    (select id from sets where short_name = 'ala'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steward of Valeron'),
    (select id from sets where short_name = 'ala'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Algae Gharial'),
    (select id from sets where short_name = 'ala'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crumbling Necropolis'),
    (select id from sets where short_name = 'ala'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ala'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skeletonize'),
    (select id from sets where short_name = 'ala'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cunning Lethemancer'),
    (select id from sets where short_name = 'ala'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scourge Devil'),
    (select id from sets where short_name = 'ala'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kederekt Leviathan'),
    (select id from sets where short_name = 'ala'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hindering Light'),
    (select id from sets where short_name = 'ala'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kiss of the Amesha'),
    (select id from sets where short_name = 'ala'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Windwright Mage'),
    (select id from sets where short_name = 'ala'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Punish Ignorance'),
    (select id from sets where short_name = 'ala'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunseed Nurturer'),
    (select id from sets where short_name = 'ala'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drumhunter'),
    (select id from sets where short_name = 'ala'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Fodder'),
    (select id from sets where short_name = 'ala'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jund Panorama'),
    (select id from sets where short_name = 'ala'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Court Archers'),
    (select id from sets where short_name = 'ala'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Esper Charm'),
    (select id from sets where short_name = 'ala'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skeletal Kathari'),
    (select id from sets where short_name = 'ala'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sigil of Distinction'),
    (select id from sets where short_name = 'ala'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grixis Charm'),
    (select id from sets where short_name = 'ala'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skill Borrower'),
    (select id from sets where short_name = 'ala'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Broodmate Dragon'),
    (select id from sets where short_name = 'ala'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ala'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ala'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bant Panorama'),
    (select id from sets where short_name = 'ala'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archdemon of Unx'),
    (select id from sets where short_name = 'ala'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cylian Elf'),
    (select id from sets where short_name = 'ala'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skullmulcher'),
    (select id from sets where short_name = 'ala'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Behemoth''s Herald'),
    (select id from sets where short_name = 'ala'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vein Drinker'),
    (select id from sets where short_name = 'ala'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ala'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marble Chalice'),
    (select id from sets where short_name = 'ala'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scourglass'),
    (select id from sets where short_name = 'ala'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viashino Skeleton'),
    (select id from sets where short_name = 'ala'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battlegrace Angel'),
    (select id from sets where short_name = 'ala'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gather Specimens'),
    (select id from sets where short_name = 'ala'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bull Cerodon'),
    (select id from sets where short_name = 'ala'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ala'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keeper of Progenitus'),
    (select id from sets where short_name = 'ala'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ala'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx''s Herald'),
    (select id from sets where short_name = 'ala'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Qasali Ambusher'),
    (select id from sets where short_name = 'ala'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vithian Stinger'),
    (select id from sets where short_name = 'ala'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vectis Silencers'),
    (select id from sets where short_name = 'ala'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seaside Citadel'),
    (select id from sets where short_name = 'ala'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deft Duelist'),
    (select id from sets where short_name = 'ala'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manaplasm'),
    (select id from sets where short_name = 'ala'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exuberant Firestoker'),
    (select id from sets where short_name = 'ala'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cradle of Vitality'),
    (select id from sets where short_name = 'ala'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sedraxis Specter'),
    (select id from sets where short_name = 'ala'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grixis Panorama'),
    (select id from sets where short_name = 'ala'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sacellum Godspeaker'),
    (select id from sets where short_name = 'ala'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Puppet Conjurer'),
    (select id from sets where short_name = 'ala'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sarkhan Vol'),
    (select id from sets where short_name = 'ala'),
    '191',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goblin Assault'),
    (select id from sets where short_name = 'ala'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vicious Shadows'),
    (select id from sets where short_name = 'ala'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sighted-Caste Sorcerer'),
    (select id from sets where short_name = 'ala'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx Sovereign'),
    (select id from sets where short_name = 'ala'),
    '196',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rakeclaw Gargantuan'),
    (select id from sets where short_name = 'ala'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Outrider of Jhess'),
    (select id from sets where short_name = 'ala'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dispeller''s Capsule'),
    (select id from sets where short_name = 'ala'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kresh the Bloodbraided'),
    (select id from sets where short_name = 'ala'),
    '178',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bone Splinters'),
    (select id from sets where short_name = 'ala'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rockslide Elemental'),
    (select id from sets where short_name = 'ala'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Etherium Astrolabe'),
    (select id from sets where short_name = 'ala'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lush Growth'),
    (select id from sets where short_name = 'ala'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spell Snip'),
    (select id from sets where short_name = 'ala'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Realm Razer'),
    (select id from sets where short_name = 'ala'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angelic Benediction'),
    (select id from sets where short_name = 'ala'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yoked Plowbeast'),
    (select id from sets where short_name = 'ala'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Druid of the Anima'),
    (select id from sets where short_name = 'ala'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathgreeter'),
    (select id from sets where short_name = 'ala'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Naya'),
    (select id from sets where short_name = 'ala'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Agony Warp'),
    (select id from sets where short_name = 'ala'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Godsire'),
    (select id from sets where short_name = 'ala'),
    '170',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ala'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Shrine'),
    (select id from sets where short_name = 'ala'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul''s Fire'),
    (select id from sets where short_name = 'ala'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Branching Bolt'),
    (select id from sets where short_name = 'ala'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Waveskimmer Aven'),
    (select id from sets where short_name = 'ala'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreg Reaver'),
    (select id from sets where short_name = 'ala'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thorn-Thrash Viashino'),
    (select id from sets where short_name = 'ala'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ala'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quietus Spike'),
    (select id from sets where short_name = 'ala'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ala'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jund Battlemage'),
    (select id from sets where short_name = 'ala'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Carrion Thrash'),
    (select id from sets where short_name = 'ala'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Resounding Wave'),
    (select id from sets where short_name = 'ala'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jhessian Lookout'),
    (select id from sets where short_name = 'ala'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon''s Herald'),
    (select id from sets where short_name = 'ala'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savage Lands'),
    (select id from sets where short_name = 'ala'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fleshbag Marauder'),
    (select id from sets where short_name = 'ala'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tar Fiend'),
    (select id from sets where short_name = 'ala'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Visionary'),
    (select id from sets where short_name = 'ala'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Cultist'),
    (select id from sets where short_name = 'ala'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Infest'),
    (select id from sets where short_name = 'ala'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Naya Panorama'),
    (select id from sets where short_name = 'ala'),
    '227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ala'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corpse Connoisseur'),
    (select id from sets where short_name = 'ala'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Banewasp Affliction'),
    (select id from sets where short_name = 'ala'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Jund'),
    (select id from sets where short_name = 'ala'),
    '215',
    'common'
) 
 on conflict do nothing;
