    insert into mtgcard(name) values ('Call to Heel') on conflict do nothing;
    insert into mtgcard(name) values ('Knight-Captain of Eos') on conflict do nothing;
    insert into mtgcard(name) values ('Godtoucher') on conflict do nothing;
    insert into mtgcard(name) values ('Esper Panorama') on conflict do nothing;
    insert into mtgcard(name) values ('Savage Hunger') on conflict do nothing;
    insert into mtgcard(name) values ('Mosstodon') on conflict do nothing;
    insert into mtgcard(name) values ('Angelsong') on conflict do nothing;
    insert into mtgcard(name) values ('Filigree Sages') on conflict do nothing;
    insert into mtgcard(name) values ('Ad Nauseam') on conflict do nothing;
    insert into mtgcard(name) values ('Immortal Coil') on conflict do nothing;
    insert into mtgcard(name) values ('Obelisk of Bant') on conflict do nothing;
    insert into mtgcard(name) values ('Etherium Sculptor') on conflict do nothing;
    insert into mtgcard(name) values ('Cruel Ultimatum') on conflict do nothing;
    insert into mtgcard(name) values ('Volcanic Submersion') on conflict do nothing;
    insert into mtgcard(name) values ('Tower Gargoyle') on conflict do nothing;
    insert into mtgcard(name) values ('Swerve') on conflict do nothing;
    insert into mtgcard(name) values ('Forest') on conflict do nothing;
    insert into mtgcard(name) values ('Spearbreaker Behemoth') on conflict do nothing;
    insert into mtgcard(name) values ('Kathari Screecher') on conflict do nothing;
    insert into mtgcard(name) values ('Mycoloth') on conflict do nothing;
    insert into mtgcard(name) values ('Shore Snapper') on conflict do nothing;
    insert into mtgcard(name) values ('Sangrite Surge') on conflict do nothing;
    insert into mtgcard(name) values ('Sharding Sphinx') on conflict do nothing;
    insert into mtgcard(name) values ('Jhessian Infiltrator') on conflict do nothing;
    insert into mtgcard(name) values ('Gift of the Gargantuan') on conflict do nothing;
    insert into mtgcard(name) values ('Welkin Guide') on conflict do nothing;
    insert into mtgcard(name) values ('Obelisk of Esper') on conflict do nothing;
    insert into mtgcard(name) values ('Shadowfeed') on conflict do nothing;
    insert into mtgcard(name) values ('Rhox War Monk') on conflict do nothing;
    insert into mtgcard(name) values ('Ranger of Eos') on conflict do nothing;
    insert into mtgcard(name) values ('Tidehollow Sculler') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Mountaineer') on conflict do nothing;
    insert into mtgcard(name) values ('Knight of the Skyward Eye') on conflict do nothing;
    insert into mtgcard(name) values ('Sanctum Gargoyle') on conflict do nothing;
    insert into mtgcard(name) values ('Necrogenesis') on conflict do nothing;
    insert into mtgcard(name) values ('Plains') on conflict do nothing;
    insert into mtgcard(name) values ('Resounding Roar') on conflict do nothing;
    insert into mtgcard(name) values ('Rafiq of the Many') on conflict do nothing;
    insert into mtgcard(name) values ('Memory Erosion') on conflict do nothing;
    insert into mtgcard(name) values ('Protomatter Powder') on conflict do nothing;
    insert into mtgcard(name) values ('Ethersworn Canonist') on conflict do nothing;
    insert into mtgcard(name) values ('Salvage Titan') on conflict do nothing;
    insert into mtgcard(name) values ('Island') on conflict do nothing;
    insert into mtgcard(name) values ('Bant Battlemage') on conflict do nothing;
    insert into mtgcard(name) values ('Esper Battlemage') on conflict do nothing;
    insert into mtgcard(name) values ('Dragon''s Herald') on conflict do nothing;
    insert into mtgcard(name) values ('Coma Veil') on conflict do nothing;
    insert into mtgcard(name) values ('Glaze Fiend') on conflict do nothing;
    insert into mtgcard(name) values ('Scavenger Drake') on conflict do nothing;
    insert into mtgcard(name) values ('Mayael the Anima') on conflict do nothing;
    insert into mtgcard(name) values ('Master of Etherium') on conflict do nothing;
    insert into mtgcard(name) values ('Cavern Thoctar') on conflict do nothing;
    insert into mtgcard(name) values ('Predator Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Angel''s Herald') on conflict do nothing;
    insert into mtgcard(name) values ('Onyx Goblet') on conflict do nothing;
    insert into mtgcard(name) values ('Crucible of Fire') on conflict do nothing;
    insert into mtgcard(name) values ('Courier''s Capsule') on conflict do nothing;
    insert into mtgcard(name) values ('Clarion Ultimatum') on conflict do nothing;
    insert into mtgcard(name) values ('Tezzeret the Seeker') on conflict do nothing;
    insert into mtgcard(name) values ('Blightning') on conflict do nothing;
    insert into mtgcard(name) values ('Soul''s Grace') on conflict do nothing;
    insert into mtgcard(name) values ('Ridge Rannet') on conflict do nothing;
    insert into mtgcard(name) values ('Empyrial Archangel') on conflict do nothing;
    insert into mtgcard(name) values ('Elspeth, Knight-Errant') on conflict do nothing;
    insert into mtgcard(name) values ('Swamp') on conflict do nothing;
    insert into mtgcard(name) values ('Feral Hydra') on conflict do nothing;
    insert into mtgcard(name) values ('Naya Charm') on conflict do nothing;
    insert into mtgcard(name) values ('Cancel') on conflict do nothing;
    insert into mtgcard(name) values ('Executioner''s Capsule') on conflict do nothing;
    insert into mtgcard(name) values ('Tidehollow Strix') on conflict do nothing;
    insert into mtgcard(name) values ('Rockcaster Platoon') on conflict do nothing;
    insert into mtgcard(name) values ('Metallurgeon') on conflict do nothing;
    insert into mtgcard(name) values ('Sigiled Paladin') on conflict do nothing;
    insert into mtgcard(name) values ('Dregscape Zombie') on conflict do nothing;
    insert into mtgcard(name) values ('Hissing Iguanar') on conflict do nothing;
    insert into mtgcard(name) values ('Death Baron') on conflict do nothing;
    insert into mtgcard(name) values ('Grixis Battlemage') on conflict do nothing;
    insert into mtgcard(name) values ('Guardians of Akrasa') on conflict do nothing;
    insert into mtgcard(name) values ('Naya Battlemage') on conflict do nothing;
    insert into mtgcard(name) values ('Obelisk of Grixis') on conflict do nothing;
    insert into mtgcard(name) values ('Gustrider Exuberant') on conflict do nothing;
    insert into mtgcard(name) values ('Flameblast Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Thunder-Thrash Elder') on conflict do nothing;
    insert into mtgcard(name) values ('Mindlock Orb') on conflict do nothing;
    insert into mtgcard(name) values ('Prince of Thralls') on conflict do nothing;
    insert into mtgcard(name) values ('Brilliant Ultimatum') on conflict do nothing;
    insert into mtgcard(name) values ('Akrasan Squire') on conflict do nothing;
    insert into mtgcard(name) values ('Tortoise Formation') on conflict do nothing;
    insert into mtgcard(name) values ('Incurable Ogre') on conflict do nothing;
    insert into mtgcard(name) values ('Resounding Silence') on conflict do nothing;
    insert into mtgcard(name) values ('Minion Reflector') on conflict do nothing;
    insert into mtgcard(name) values ('Wild Nacatl') on conflict do nothing;
    insert into mtgcard(name) values ('Jungle Weaver') on conflict do nothing;
    insert into mtgcard(name) values ('Ooze Garden') on conflict do nothing;
    insert into mtgcard(name) values ('Stoic Angel') on conflict do nothing;
    insert into mtgcard(name) values ('Knight of the White Orchid') on conflict do nothing;
    insert into mtgcard(name) values ('Resounding Scream') on conflict do nothing;
    insert into mtgcard(name) values ('Sharuum the Hegemon') on conflict do nothing;
    insert into mtgcard(name) values ('Bant Charm') on conflict do nothing;
    insert into mtgcard(name) values ('Resounding Thunder') on conflict do nothing;
    insert into mtgcard(name) values ('Topan Ascetic') on conflict do nothing;
    insert into mtgcard(name) values ('Viscera Dragger') on conflict do nothing;
    insert into mtgcard(name) values ('Arcane Sanctum') on conflict do nothing;
    insert into mtgcard(name) values ('Covenant of Minds') on conflict do nothing;
    insert into mtgcard(name) values ('Excommunicate') on conflict do nothing;
    insert into mtgcard(name) values ('Kederekt Creeper') on conflict do nothing;
    insert into mtgcard(name) values ('Sedris, the Traitor King') on conflict do nothing;
    insert into mtgcard(name) values ('Magma Spray') on conflict do nothing;
    insert into mtgcard(name) values ('Fire-Field Ogre') on conflict do nothing;
    insert into mtgcard(name) values ('Invincible Hymn') on conflict do nothing;
    insert into mtgcard(name) values ('Steelclad Serpent') on conflict do nothing;
    insert into mtgcard(name) values ('Naturalize') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Deathraiders') on conflict do nothing;
    insert into mtgcard(name) values ('Soul''s Might') on conflict do nothing;
    insert into mtgcard(name) values ('Lich''s Mirror') on conflict do nothing;
    insert into mtgcard(name) values ('Where Ancients Tread') on conflict do nothing;
    insert into mtgcard(name) values ('Jund Charm') on conflict do nothing;
    insert into mtgcard(name) values ('Undead Leotau') on conflict do nothing;
    insert into mtgcard(name) values ('Mountain') on conflict do nothing;
    insert into mtgcard(name) values ('Woolly Thoctar') on conflict do nothing;
    insert into mtgcard(name) values ('Fatestitcher') on conflict do nothing;
    insert into mtgcard(name) values ('Violent Ultimatum') on conflict do nothing;
    insert into mtgcard(name) values ('Blister Beetle') on conflict do nothing;
    insert into mtgcard(name) values ('Cloudheath Drake') on conflict do nothing;
    insert into mtgcard(name) values ('Hell''s Thunder') on conflict do nothing;
    insert into mtgcard(name) values ('Dawnray Archer') on conflict do nothing;
    insert into mtgcard(name) values ('Bloodthorn Taunter') on conflict do nothing;
    insert into mtgcard(name) values ('Caldera Hellion') on conflict do nothing;
    insert into mtgcard(name) values ('Hellkite Overlord') on conflict do nothing;
    insert into mtgcard(name) values ('Sprouting Thrinax') on conflict do nothing;
    insert into mtgcard(name) values ('Oblivion Ring') on conflict do nothing;
    insert into mtgcard(name) values ('Bloodpyre Elemental') on conflict do nothing;
    insert into mtgcard(name) values ('Rhox Charger') on conflict do nothing;
    insert into mtgcard(name) values ('Sigil Blessing') on conflict do nothing;
    insert into mtgcard(name) values ('Thoughtcutter Agent') on conflict do nothing;
    insert into mtgcard(name) values ('Titanic Ultimatum') on conflict do nothing;
    insert into mtgcard(name) values ('Rip-Clan Crasher') on conflict do nothing;
    insert into mtgcard(name) values ('Cathartic Adept') on conflict do nothing;
    insert into mtgcard(name) values ('Mighty Emergence') on conflict do nothing;
    insert into mtgcard(name) values ('Ajani Vengeant') on conflict do nothing;
    insert into mtgcard(name) values ('Lightning Talons') on conflict do nothing;
    insert into mtgcard(name) values ('Relic of Progenitus') on conflict do nothing;
    insert into mtgcard(name) values ('Steward of Valeron') on conflict do nothing;
    insert into mtgcard(name) values ('Algae Gharial') on conflict do nothing;
    insert into mtgcard(name) values ('Crumbling Necropolis') on conflict do nothing;
    insert into mtgcard(name) values ('Skeletonize') on conflict do nothing;
    insert into mtgcard(name) values ('Cunning Lethemancer') on conflict do nothing;
    insert into mtgcard(name) values ('Scourge Devil') on conflict do nothing;
    insert into mtgcard(name) values ('Kederekt Leviathan') on conflict do nothing;
    insert into mtgcard(name) values ('Hindering Light') on conflict do nothing;
    insert into mtgcard(name) values ('Kiss of the Amesha') on conflict do nothing;
    insert into mtgcard(name) values ('Windwright Mage') on conflict do nothing;
    insert into mtgcard(name) values ('Punish Ignorance') on conflict do nothing;
    insert into mtgcard(name) values ('Sunseed Nurturer') on conflict do nothing;
    insert into mtgcard(name) values ('Drumhunter') on conflict do nothing;
    insert into mtgcard(name) values ('Dragon Fodder') on conflict do nothing;
    insert into mtgcard(name) values ('Jund Panorama') on conflict do nothing;
    insert into mtgcard(name) values ('Court Archers') on conflict do nothing;
    insert into mtgcard(name) values ('Esper Charm') on conflict do nothing;
    insert into mtgcard(name) values ('Skeletal Kathari') on conflict do nothing;
    insert into mtgcard(name) values ('Sigil of Distinction') on conflict do nothing;
    insert into mtgcard(name) values ('Grixis Charm') on conflict do nothing;
    insert into mtgcard(name) values ('Skill Borrower') on conflict do nothing;
    insert into mtgcard(name) values ('Broodmate Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Bant Panorama') on conflict do nothing;
    insert into mtgcard(name) values ('Archdemon of Unx') on conflict do nothing;
    insert into mtgcard(name) values ('Cylian Elf') on conflict do nothing;
    insert into mtgcard(name) values ('Skullmulcher') on conflict do nothing;
    insert into mtgcard(name) values ('Behemoth''s Herald') on conflict do nothing;
    insert into mtgcard(name) values ('Vein Drinker') on conflict do nothing;
    insert into mtgcard(name) values ('Marble Chalice') on conflict do nothing;
    insert into mtgcard(name) values ('Scourglass') on conflict do nothing;
    insert into mtgcard(name) values ('Viashino Skeleton') on conflict do nothing;
    insert into mtgcard(name) values ('Battlegrace Angel') on conflict do nothing;
    insert into mtgcard(name) values ('Gather Specimens') on conflict do nothing;
    insert into mtgcard(name) values ('Bull Cerodon') on conflict do nothing;
    insert into mtgcard(name) values ('Keeper of Progenitus') on conflict do nothing;
    insert into mtgcard(name) values ('Sphinx''s Herald') on conflict do nothing;
    insert into mtgcard(name) values ('Qasali Ambusher') on conflict do nothing;
    insert into mtgcard(name) values ('Vithian Stinger') on conflict do nothing;
    insert into mtgcard(name) values ('Vectis Silencers') on conflict do nothing;
    insert into mtgcard(name) values ('Seaside Citadel') on conflict do nothing;
    insert into mtgcard(name) values ('Deft Duelist') on conflict do nothing;
    insert into mtgcard(name) values ('Manaplasm') on conflict do nothing;
    insert into mtgcard(name) values ('Exuberant Firestoker') on conflict do nothing;
    insert into mtgcard(name) values ('Cradle of Vitality') on conflict do nothing;
    insert into mtgcard(name) values ('Sedraxis Specter') on conflict do nothing;
    insert into mtgcard(name) values ('Grixis Panorama') on conflict do nothing;
    insert into mtgcard(name) values ('Sacellum Godspeaker') on conflict do nothing;
    insert into mtgcard(name) values ('Puppet Conjurer') on conflict do nothing;
    insert into mtgcard(name) values ('Sarkhan Vol') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Assault') on conflict do nothing;
    insert into mtgcard(name) values ('Vicious Shadows') on conflict do nothing;
    insert into mtgcard(name) values ('Sighted-Caste Sorcerer') on conflict do nothing;
    insert into mtgcard(name) values ('Sphinx Sovereign') on conflict do nothing;
    insert into mtgcard(name) values ('Rakeclaw Gargantuan') on conflict do nothing;
    insert into mtgcard(name) values ('Outrider of Jhess') on conflict do nothing;
    insert into mtgcard(name) values ('Dispeller''s Capsule') on conflict do nothing;
    insert into mtgcard(name) values ('Kresh the Bloodbraided') on conflict do nothing;
    insert into mtgcard(name) values ('Bone Splinters') on conflict do nothing;
    insert into mtgcard(name) values ('Rockslide Elemental') on conflict do nothing;
    insert into mtgcard(name) values ('Etherium Astrolabe') on conflict do nothing;
    insert into mtgcard(name) values ('Lush Growth') on conflict do nothing;
    insert into mtgcard(name) values ('Spell Snip') on conflict do nothing;
    insert into mtgcard(name) values ('Realm Razer') on conflict do nothing;
    insert into mtgcard(name) values ('Angelic Benediction') on conflict do nothing;
    insert into mtgcard(name) values ('Yoked Plowbeast') on conflict do nothing;
    insert into mtgcard(name) values ('Druid of the Anima') on conflict do nothing;
    insert into mtgcard(name) values ('Deathgreeter') on conflict do nothing;
    insert into mtgcard(name) values ('Obelisk of Naya') on conflict do nothing;
    insert into mtgcard(name) values ('Agony Warp') on conflict do nothing;
    insert into mtgcard(name) values ('Godsire') on conflict do nothing;
    insert into mtgcard(name) values ('Jungle Shrine') on conflict do nothing;
    insert into mtgcard(name) values ('Soul''s Fire') on conflict do nothing;
    insert into mtgcard(name) values ('Branching Bolt') on conflict do nothing;
    insert into mtgcard(name) values ('Waveskimmer Aven') on conflict do nothing;
    insert into mtgcard(name) values ('Dreg Reaver') on conflict do nothing;
    insert into mtgcard(name) values ('Thorn-Thrash Viashino') on conflict do nothing;
    insert into mtgcard(name) values ('Quietus Spike') on conflict do nothing;
    insert into mtgcard(name) values ('Jund Battlemage') on conflict do nothing;
    insert into mtgcard(name) values ('Carrion Thrash') on conflict do nothing;
    insert into mtgcard(name) values ('Resounding Wave') on conflict do nothing;
    insert into mtgcard(name) values ('Jhessian Lookout') on conflict do nothing;
    insert into mtgcard(name) values ('Demon''s Herald') on conflict do nothing;
    insert into mtgcard(name) values ('Savage Lands') on conflict do nothing;
    insert into mtgcard(name) values ('Fleshbag Marauder') on conflict do nothing;
    insert into mtgcard(name) values ('Tar Fiend') on conflict do nothing;
    insert into mtgcard(name) values ('Elvish Visionary') on conflict do nothing;
    insert into mtgcard(name) values ('Blood Cultist') on conflict do nothing;
    insert into mtgcard(name) values ('Infest') on conflict do nothing;
    insert into mtgcard(name) values ('Naya Panorama') on conflict do nothing;
    insert into mtgcard(name) values ('Corpse Connoisseur') on conflict do nothing;
    insert into mtgcard(name) values ('Banewasp Affliction') on conflict do nothing;
    insert into mtgcard(name) values ('Obelisk of Jund') on conflict do nothing;
