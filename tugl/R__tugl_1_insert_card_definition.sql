    insert into mtgcard(name) values ('Sheep') on conflict do nothing;
    insert into mtgcard(name) values ('Zombie') on conflict do nothing;
    insert into mtgcard(name) values ('Squirrel') on conflict do nothing;
    insert into mtgcard(name) values ('Soldier') on conflict do nothing;
    insert into mtgcard(name) values ('Pegasus') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin') on conflict do nothing;
