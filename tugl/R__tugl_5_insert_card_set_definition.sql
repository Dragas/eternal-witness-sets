insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Sheep'),
    (select id from sets where short_name = 'tugl'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tugl'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squirrel'),
    (select id from sets where short_name = 'tugl'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tugl'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pegasus'),
    (select id from sets where short_name = 'tugl'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tugl'),
    '4',
    'common'
) 
 on conflict do nothing;
