insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'trix'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Huatli, Radiant Champion Emblem'),
    (select id from sets where short_name = 'trix'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rivals of Ixalan Checklist'),
    (select id from sets where short_name = 'trix'),
    'CH1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'trix'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golem'),
    (select id from sets where short_name = 'trix'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'City''s Blessing'),
    (select id from sets where short_name = 'trix'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'trix'),
    '1',
    'common'
) 
 on conflict do nothing;
