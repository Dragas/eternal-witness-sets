    insert into mtgcard(name) values ('Elemental') on conflict do nothing;
    insert into mtgcard(name) values ('Huatli, Radiant Champion Emblem') on conflict do nothing;
    insert into mtgcard(name) values ('Rivals of Ixalan Checklist') on conflict do nothing;
    insert into mtgcard(name) values ('Saproling') on conflict do nothing;
    insert into mtgcard(name) values ('Golem') on conflict do nothing;
    insert into mtgcard(name) values ('City''s Blessing') on conflict do nothing;
