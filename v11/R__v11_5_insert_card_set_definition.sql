insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ulamog, the Infinite Gyre'),
    (select id from sets where short_name = 'v11'),
    '14',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Visara the Dreadful'),
    (select id from sets where short_name = 'v11'),
    '15',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Omnath, Locus of Mana'),
    (select id from sets where short_name = 'v11'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Captain Sisay'),
    (select id from sets where short_name = 'v11'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mikaeus, the Lunarch'),
    (select id from sets where short_name = 'v11'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Progenitus'),
    (select id from sets where short_name = 'v11'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Doran, the Siege Tower'),
    (select id from sets where short_name = 'v11'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sun Quan, Lord of Wu'),
    (select id from sets where short_name = 'v11'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rafiq of the Many'),
    (select id from sets where short_name = 'v11'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kresh the Bloodbraided'),
    (select id from sets where short_name = 'v11'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Oona, Queen of the Fae'),
    (select id from sets where short_name = 'v11'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kiki-Jiki, Mirror Breaker'),
    (select id from sets where short_name = 'v11'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cao Cao, Lord of Wei'),
    (select id from sets where short_name = 'v11'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Teferi, Mage of Zhalfir'),
    (select id from sets where short_name = 'v11'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sharuum the Hegemon'),
    (select id from sets where short_name = 'v11'),
    '11',
    'mythic'
) 
 on conflict do nothing;
