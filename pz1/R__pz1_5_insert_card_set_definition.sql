insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Desertion'),
    (select id from sets where short_name = 'pz1'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anya, Merciless Angel'),
    (select id from sets where short_name = 'pz1'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of the False God'),
    (select id from sets where short_name = 'pz1'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Command Tower'),
    (select id from sets where short_name = 'pz1'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flusterstorm'),
    (select id from sets where short_name = 'pz1'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Mimeoplasm'),
    (select id from sets where short_name = 'pz1'),
    '114',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rhystic Study'),
    (select id from sets where short_name = 'pz1'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Decree of Pain'),
    (select id from sets where short_name = 'pz1'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Titania, Protector of Argoth'),
    (select id from sets where short_name = 'pz1'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verdant Confluence'),
    (select id from sets where short_name = 'pz1'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Invigorate'),
    (select id from sets where short_name = 'pz1'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bastion Protector'),
    (select id from sets where short_name = 'pz1'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vow of Lightning'),
    (select id from sets where short_name = 'pz1'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Electrolyze'),
    (select id from sets where short_name = 'pz1'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daretti, Scrap Savant'),
    (select id from sets where short_name = 'pz1'),
    '56',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Synthetic Destiny'),
    (select id from sets where short_name = 'pz1'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Propaganda'),
    (select id from sets where short_name = 'pz1'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind''s Eye'),
    (select id from sets where short_name = 'pz1'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wonder'),
    (select id from sets where short_name = 'pz1'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anger'),
    (select id from sets where short_name = 'pz1'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dread Summons'),
    (select id from sets where short_name = 'pz1'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Great Oak Guardian'),
    (select id from sets where short_name = 'pz1'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Behemoth Sledge'),
    (select id from sets where short_name = 'pz1'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scourge of Nel Toth'),
    (select id from sets where short_name = 'pz1'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meteor Blast'),
    (select id from sets where short_name = 'pz1'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fact or Fiction'),
    (select id from sets where short_name = 'pz1'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Saltcrusted Steppe'),
    (select id from sets where short_name = 'pz1'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dack Fayden'),
    (select id from sets where short_name = 'pz1'),
    '97',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Toxic Deluge'),
    (select id from sets where short_name = 'pz1'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scroll Rack'),
    (select id from sets where short_name = 'pz1'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vela the Night-Clad'),
    (select id from sets where short_name = 'pz1'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animar, Soul of Elements'),
    (select id from sets where short_name = 'pz1'),
    '93',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Worn Powerstone'),
    (select id from sets where short_name = 'pz1'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brawn'),
    (select id from sets where short_name = 'pz1'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Wanderer'),
    (select id from sets where short_name = 'pz1'),
    '110',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Seaside Citadel'),
    (select id from sets where short_name = 'pz1'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warchief Giant'),
    (select id from sets where short_name = 'pz1'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krosan Grip'),
    (select id from sets where short_name = 'pz1'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thran Dynamo'),
    (select id from sets where short_name = 'pz1'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swiftfoot Boots'),
    (select id from sets where short_name = 'pz1'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Molten Slagheap'),
    (select id from sets where short_name = 'pz1'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diaochan, Artful Beauty'),
    (select id from sets where short_name = 'pz1'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Caller of the Pack'),
    (select id from sets where short_name = 'pz1'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thief of Blood'),
    (select id from sets where short_name = 'pz1'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spirit Mantle'),
    (select id from sets where short_name = 'pz1'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghave, Guru of Spores'),
    (select id from sets where short_name = 'pz1'),
    '104',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ezuri, Claw of Progress'),
    (select id from sets where short_name = 'pz1'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Blossoms'),
    (select id from sets where short_name = 'pz1'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mizzix''s Mastery'),
    (select id from sets where short_name = 'pz1'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shardless Agent'),
    (select id from sets where short_name = 'pz1'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Compulsive Research'),
    (select id from sets where short_name = 'pz1'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vow of Duty'),
    (select id from sets where short_name = 'pz1'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Faithless Looting'),
    (select id from sets where short_name = 'pz1'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Elder'),
    (select id from sets where short_name = 'pz1'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meren of Clan Nel Toth'),
    (select id from sets where short_name = 'pz1'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pathbreaker Ibex'),
    (select id from sets where short_name = 'pz1'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grasp of Fate'),
    (select id from sets where short_name = 'pz1'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Righteous Confluence'),
    (select id from sets where short_name = 'pz1'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ezuri''s Predation'),
    (select id from sets where short_name = 'pz1'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oreskos Explorer'),
    (select id from sets where short_name = 'pz1'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Illusory Ambusher'),
    (select id from sets where short_name = 'pz1'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Daxos''s Torment'),
    (select id from sets where short_name = 'pz1'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'pz1'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rite of the Raging Storm'),
    (select id from sets where short_name = 'pz1'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jungle Shrine'),
    (select id from sets where short_name = 'pz1'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crumbling Necropolis'),
    (select id from sets where short_name = 'pz1'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karlov of the Ghost Council'),
    (select id from sets where short_name = 'pz1'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nest Invader'),
    (select id from sets where short_name = 'pz1'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Khalni Garden'),
    (select id from sets where short_name = 'pz1'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daxos the Returned'),
    (select id from sets where short_name = 'pz1'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Broodbirth Viper'),
    (select id from sets where short_name = 'pz1'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodspore Thrinax'),
    (select id from sets where short_name = 'pz1'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Herald of the Host'),
    (select id from sets where short_name = 'pz1'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Centaur Vinecrasher'),
    (select id from sets where short_name = 'pz1'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Freyalise, Llanowar''s Fury'),
    (select id from sets where short_name = 'pz1'),
    '78',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vow of Malice'),
    (select id from sets where short_name = 'pz1'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harmonize'),
    (select id from sets where short_name = 'pz1'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shielded by Faith'),
    (select id from sets where short_name = 'pz1'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Punishing Fire'),
    (select id from sets where short_name = 'pz1'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mirror Match'),
    (select id from sets where short_name = 'pz1'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corpse Augur'),
    (select id from sets where short_name = 'pz1'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaalia of the Vast'),
    (select id from sets where short_name = 'pz1'),
    '106',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu'),
    (select id from sets where short_name = 'pz1'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Edric, Spymaster of Trest'),
    (select id from sets where short_name = 'pz1'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Basalt Monolith'),
    (select id from sets where short_name = 'pz1'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Command Beacon'),
    (select id from sets where short_name = 'pz1'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abyssal Persecutor'),
    (select id from sets where short_name = 'pz1'),
    '38',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Arcane Sanctum'),
    (select id from sets where short_name = 'pz1'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fiery Confluence'),
    (select id from sets where short_name = 'pz1'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deadly Tempest'),
    (select id from sets where short_name = 'pz1'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gigantoplasm'),
    (select id from sets where short_name = 'pz1'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Omens'),
    (select id from sets where short_name = 'pz1'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Sharpshooter'),
    (select id from sets where short_name = 'pz1'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marchesa, the Black Rose'),
    (select id from sets where short_name = 'pz1'),
    '111',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mazirek, Kraul Death Priest'),
    (select id from sets where short_name = 'pz1'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savage Lands'),
    (select id from sets where short_name = 'pz1'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'True-Name Nemesis'),
    (select id from sets where short_name = 'pz1'),
    '35',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kaseto, Orochi Archmage'),
    (select id from sets where short_name = 'pz1'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sandstone Oracle'),
    (select id from sets where short_name = 'pz1'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'pz1'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thought Vessel'),
    (select id from sets where short_name = 'pz1'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Library'),
    (select id from sets where short_name = 'pz1'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viscera Seer'),
    (select id from sets where short_name = 'pz1'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arjun, the Shifting Flame'),
    (select id from sets where short_name = 'pz1'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rancor'),
    (select id from sets where short_name = 'pz1'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seal of the Guildpact'),
    (select id from sets where short_name = 'pz1'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis of the Black Oath'),
    (select id from sets where short_name = 'pz1'),
    '45',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Magus of the Wheel'),
    (select id from sets where short_name = 'pz1'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Awaken the Sky Tyrant'),
    (select id from sets where short_name = 'pz1'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreadship Reef'),
    (select id from sets where short_name = 'pz1'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Homeward Path'),
    (select id from sets where short_name = 'pz1'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blade of Selves'),
    (select id from sets where short_name = 'pz1'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tragic Slip'),
    (select id from sets where short_name = 'pz1'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vow of Wildness'),
    (select id from sets where short_name = 'pz1'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mystic Confluence'),
    (select id from sets where short_name = 'pz1'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonlair Spider'),
    (select id from sets where short_name = 'pz1'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vow of Flight'),
    (select id from sets where short_name = 'pz1'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deceiver Exarch'),
    (select id from sets where short_name = 'pz1'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eternal Witness'),
    (select id from sets where short_name = 'pz1'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skullwinder'),
    (select id from sets where short_name = 'pz1'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krosan Verge'),
    (select id from sets where short_name = 'pz1'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dream Pillager'),
    (select id from sets where short_name = 'pz1'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arachnogenesis'),
    (select id from sets where short_name = 'pz1'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nahiri, the Lithomancer'),
    (select id from sets where short_name = 'pz1'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kalemne, Disciple of Iroas'),
    (select id from sets where short_name = 'pz1'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hull Breach'),
    (select id from sets where short_name = 'pz1'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chaos Warp'),
    (select id from sets where short_name = 'pz1'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirari''s Wake'),
    (select id from sets where short_name = 'pz1'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duplicant'),
    (select id from sets where short_name = 'pz1'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghostly Prison'),
    (select id from sets where short_name = 'pz1'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loyal Retainers'),
    (select id from sets where short_name = 'pz1'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi, Temporal Archmage'),
    (select id from sets where short_name = 'pz1'),
    '34',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scytheclaw'),
    (select id from sets where short_name = 'pz1'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Containment Priest'),
    (select id from sets where short_name = 'pz1'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aethersnatch'),
    (select id from sets where short_name = 'pz1'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Banshee of the Dread Choir'),
    (select id from sets where short_name = 'pz1'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bojuka Bog'),
    (select id from sets where short_name = 'pz1'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dawnbreak Reclaimer'),
    (select id from sets where short_name = 'pz1'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deep Analysis'),
    (select id from sets where short_name = 'pz1'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kalemne''s Captain'),
    (select id from sets where short_name = 'pz1'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'pz1'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mizzix of the Izmagnus'),
    (select id from sets where short_name = 'pz1'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wretched Confluence'),
    (select id from sets where short_name = 'pz1'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fungal Reaches'),
    (select id from sets where short_name = 'pz1'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firespout'),
    (select id from sets where short_name = 'pz1'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cultivate'),
    (select id from sets where short_name = 'pz1'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Greaves'),
    (select id from sets where short_name = 'pz1'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reya Dawnbringer'),
    (select id from sets where short_name = 'pz1'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Zedruu the Greathearted'),
    (select id from sets where short_name = 'pz1'),
    '119',
    'mythic'
) 
 on conflict do nothing;
