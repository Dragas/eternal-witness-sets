insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Faerie Spy'),
    (select id from sets where short_name = 'tust'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Construct'),
    (select id from sets where short_name = 'tust'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter // Thopter'),
    (select id from sets where short_name = 'tust'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit // Spirit'),
    (select id from sets where short_name = 'tust'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tust'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rogue'),
    (select id from sets where short_name = 'tust'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squirrel'),
    (select id from sets where short_name = 'tust'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie // Zombie'),
    (select id from sets where short_name = 'tust'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire // Vampire'),
    (select id from sets where short_name = 'tust'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast // Beast'),
    (select id from sets where short_name = 'tust'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tust'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental // Elemental'),
    (select id from sets where short_name = 'tust'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental // Elemental'),
    (select id from sets where short_name = 'tust'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Storm Crow'),
    (select id from sets where short_name = 'tust'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling // Saproling'),
    (select id from sets where short_name = 'tust'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clue // Clue'),
    (select id from sets where short_name = 'tust'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brainiac'),
    (select id from sets where short_name = 'tust'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goat'),
    (select id from sets where short_name = 'tust'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gnome'),
    (select id from sets where short_name = 'tust'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel // Angel'),
    (select id from sets where short_name = 'tust'),
    '1',
    'common'
) 
 on conflict do nothing;
