insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Consecrate // Consume'),
    (select id from sets where short_name = 'rna'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warrant // Warden'),
    (select id from sets where short_name = 'rna'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Charging War Boar'),
    (select id from sets where short_name = 'rna'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Lost Thoughts'),
    (select id from sets where short_name = 'rna'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Junktroller'),
    (select id from sets where short_name = 'rna'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cult Guildmage'),
    (select id from sets where short_name = 'rna'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Captive Audience'),
    (select id from sets where short_name = 'rna'),
    '160',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Elite Arrester'),
    (select id from sets where short_name = 'rna'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Incubation Druid'),
    (select id from sets where short_name = 'rna'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plaza of Harmony'),
    (select id from sets where short_name = 'rna'),
    '254',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ethereal Absolution'),
    (select id from sets where short_name = 'rna'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Expose to Daylight'),
    (select id from sets where short_name = 'rna'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Consign to the Pit'),
    (select id from sets where short_name = 'rna'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blade Juggler'),
    (select id from sets where short_name = 'rna'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Judith, the Scourge Diva'),
    (select id from sets where short_name = 'rna'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dovin''s Automaton'),
    (select id from sets where short_name = 'rna'),
    '268',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clear the Stage'),
    (select id from sets where short_name = 'rna'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dovin''s Acuity'),
    (select id from sets where short_name = 'rna'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nikya of the Old Ways'),
    (select id from sets where short_name = 'rna'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Biogenic Upgrade'),
    (select id from sets where short_name = 'rna'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Growth-Chamber Guardian'),
    (select id from sets where short_name = 'rna'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Syndicate Messenger'),
    (select id from sets where short_name = 'rna'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prying Eyes'),
    (select id from sets where short_name = 'rna'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windstorm Drake'),
    (select id from sets where short_name = 'rna'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rampaging Rendhorn'),
    (select id from sets where short_name = 'rna'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zhur-Taa Goblin'),
    (select id from sets where short_name = 'rna'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azorius Locket'),
    (select id from sets where short_name = 'rna'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Gathering'),
    (select id from sets where short_name = 'rna'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chillbringer'),
    (select id from sets where short_name = 'rna'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flames of the Raze-Boar'),
    (select id from sets where short_name = 'rna'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Regenesis'),
    (select id from sets where short_name = 'rna'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tower Defense'),
    (select id from sets where short_name = 'rna'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'rna'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Domri''s Nodorog'),
    (select id from sets where short_name = 'rna'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Undercity Scavenger'),
    (select id from sets where short_name = 'rna'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Justiciar''s Portal'),
    (select id from sets where short_name = 'rna'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Absorb'),
    (select id from sets where short_name = 'rna'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruul Locket'),
    (select id from sets where short_name = 'rna'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Act of Treason'),
    (select id from sets where short_name = 'rna'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Domri, Chaos Bringer'),
    (select id from sets where short_name = 'rna'),
    '166',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Eyes Everywhere'),
    (select id from sets where short_name = 'rna'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Humongulus'),
    (select id from sets where short_name = 'rna'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireblade Artist'),
    (select id from sets where short_name = 'rna'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lumbering Battlement'),
    (select id from sets where short_name = 'rna'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Code of Constraint'),
    (select id from sets where short_name = 'rna'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gyre Engineer'),
    (select id from sets where short_name = 'rna'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Guildgate'),
    (select id from sets where short_name = 'rna'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pteramander'),
    (select id from sets where short_name = 'rna'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Trumpeter'),
    (select id from sets where short_name = 'rna'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampage of the Clans'),
    (select id from sets where short_name = 'rna'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Racketeers'),
    (select id from sets where short_name = 'rna'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Simic Locket'),
    (select id from sets where short_name = 'rna'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scorchmark'),
    (select id from sets where short_name = 'rna'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul Beastmaster'),
    (select id from sets where short_name = 'rna'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gates Ablaze'),
    (select id from sets where short_name = 'rna'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Impassioned Orator'),
    (select id from sets where short_name = 'rna'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrash // Threat'),
    (select id from sets where short_name = 'rna'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Axebane Beast'),
    (select id from sets where short_name = 'rna'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sauroform Hybrid'),
    (select id from sets where short_name = 'rna'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rubblebelt Recluse'),
    (select id from sets where short_name = 'rna'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'rna'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul Spellbreaker'),
    (select id from sets where short_name = 'rna'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruul Guildgate'),
    (select id from sets where short_name = 'rna'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gateway Plaza'),
    (select id from sets where short_name = 'rna'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clan Guildmage'),
    (select id from sets where short_name = 'rna'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sky Tether'),
    (select id from sets where short_name = 'rna'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Combine Guildmage'),
    (select id from sets where short_name = 'rna'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feral Maaka'),
    (select id from sets where short_name = 'rna'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx of the Guildpact'),
    (select id from sets where short_name = 'rna'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ill-Gotten Inheritance'),
    (select id from sets where short_name = 'rna'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Verity Circle'),
    (select id from sets where short_name = 'rna'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deface'),
    (select id from sets where short_name = 'rna'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Catacomb Crocodile'),
    (select id from sets where short_name = 'rna'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Immolation Shaman'),
    (select id from sets where short_name = 'rna'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Senate Griffin'),
    (select id from sets where short_name = 'rna'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unbreakable Formation'),
    (select id from sets where short_name = 'rna'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'rna'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dagger Caster'),
    (select id from sets where short_name = 'rna'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gravel-Hide Goblin'),
    (select id from sets where short_name = 'rna'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Final Payment'),
    (select id from sets where short_name = 'rna'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savage Smash'),
    (select id from sets where short_name = 'rna'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghor-Clan Wrecker'),
    (select id from sets where short_name = 'rna'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakdos, the Showstopper'),
    (select id from sets where short_name = 'rna'),
    '199',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Smelt-Ward Ignus'),
    (select id from sets where short_name = 'rna'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Locket'),
    (select id from sets where short_name = 'rna'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frilled Mystic'),
    (select id from sets where short_name = 'rna'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bolrac-Clan Crusher'),
    (select id from sets where short_name = 'rna'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mirror March'),
    (select id from sets where short_name = 'rna'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Revival // Revenge'),
    (select id from sets where short_name = 'rna'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'rna'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wilderness Reclamation'),
    (select id from sets where short_name = 'rna'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Resolute Watchdog'),
    (select id from sets where short_name = 'rna'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grasping Thrull'),
    (select id from sets where short_name = 'rna'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spikewheel Acrobat'),
    (select id from sets where short_name = 'rna'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Haazda Officer'),
    (select id from sets where short_name = 'rna'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mesmerizing Benthid'),
    (select id from sets where short_name = 'rna'),
    '43',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Electrodominance'),
    (select id from sets where short_name = 'rna'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Territorial Boar'),
    (select id from sets where short_name = 'rna'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kaya''s Wrath'),
    (select id from sets where short_name = 'rna'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Roustabout'),
    (select id from sets where short_name = 'rna'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gatebreaker Ram'),
    (select id from sets where short_name = 'rna'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Root Snare'),
    (select id from sets where short_name = 'rna'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pestilent Spirit'),
    (select id from sets where short_name = 'rna'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Crypt'),
    (select id from sets where short_name = 'rna'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twilight Panther'),
    (select id from sets where short_name = 'rna'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azorius Knight-Arbiter'),
    (select id from sets where short_name = 'rna'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vizkopa Vampire'),
    (select id from sets where short_name = 'rna'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Watchful Giant'),
    (select id from sets where short_name = 'rna'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Haunt of Hightower'),
    (select id from sets where short_name = 'rna'),
    '273',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Simic Guildgate'),
    (select id from sets where short_name = 'rna'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dovin''s Dismissal'),
    (select id from sets where short_name = 'rna'),
    '267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arrester''s Admonition'),
    (select id from sets where short_name = 'rna'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bring to Trial'),
    (select id from sets where short_name = 'rna'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emergency Powers'),
    (select id from sets where short_name = 'rna'),
    '169',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aeromunculus'),
    (select id from sets where short_name = 'rna'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scrabbling Claws'),
    (select id from sets where short_name = 'rna'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gutterbones'),
    (select id from sets where short_name = 'rna'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Enforcer'),
    (select id from sets where short_name = 'rna'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight of Sorrows'),
    (select id from sets where short_name = 'rna'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravager Wurm'),
    (select id from sets where short_name = 'rna'),
    '200',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Simic Guildgate'),
    (select id from sets where short_name = 'rna'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Biogenic Ooze'),
    (select id from sets where short_name = 'rna'),
    '122',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cry of the Carnarium'),
    (select id from sets where short_name = 'rna'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rumbling Ruin'),
    (select id from sets where short_name = 'rna'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Precognitive Perception'),
    (select id from sets where short_name = 'rna'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drill Bit'),
    (select id from sets where short_name = 'rna'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wrecking Beast'),
    (select id from sets where short_name = 'rna'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orzhov Locket'),
    (select id from sets where short_name = 'rna'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shimmer of Possibility'),
    (select id from sets where short_name = 'rna'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of the Last Breath'),
    (select id from sets where short_name = 'rna'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mass Manipulation'),
    (select id from sets where short_name = 'rna'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rubblebelt Runner'),
    (select id from sets where short_name = 'rna'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Amplifire'),
    (select id from sets where short_name = 'rna'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smothering Tithe'),
    (select id from sets where short_name = 'rna'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benthic Biomancer'),
    (select id from sets where short_name = 'rna'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vindictive Vampire'),
    (select id from sets where short_name = 'rna'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Growth Spiral'),
    (select id from sets where short_name = 'rna'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Debtors'' Transport'),
    (select id from sets where short_name = 'rna'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deputy of Detention'),
    (select id from sets where short_name = 'rna'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel of Grace'),
    (select id from sets where short_name = 'rna'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hydroid Krasis'),
    (select id from sets where short_name = 'rna'),
    '183',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bedevil'),
    (select id from sets where short_name = 'rna'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Get the Point'),
    (select id from sets where short_name = 'rna'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhythm of the Wild'),
    (select id from sets where short_name = 'rna'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prime Speaker Vannifar'),
    (select id from sets where short_name = 'rna'),
    '195',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hero of Precinct One'),
    (select id from sets where short_name = 'rna'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coral Commando'),
    (select id from sets where short_name = 'rna'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Concordia Pegasus'),
    (select id from sets where short_name = 'rna'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cavalcade of Calamity'),
    (select id from sets where short_name = 'rna'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rubble Reading'),
    (select id from sets where short_name = 'rna'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stony Strength'),
    (select id from sets where short_name = 'rna'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Brushstrider'),
    (select id from sets where short_name = 'rna'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spawn of Mayhem'),
    (select id from sets where short_name = 'rna'),
    '85',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Footlight Fiend'),
    (select id from sets where short_name = 'rna'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swirling Torrent'),
    (select id from sets where short_name = 'rna'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Storm Strike'),
    (select id from sets where short_name = 'rna'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trollbred Guardian'),
    (select id from sets where short_name = 'rna'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Domri, City Smasher'),
    (select id from sets where short_name = 'rna'),
    '269',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gruul Guildgate'),
    (select id from sets where short_name = 'rna'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodmist Infiltrator'),
    (select id from sets where short_name = 'rna'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Senate Guildmage'),
    (select id from sets where short_name = 'rna'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Light Up the Stage'),
    (select id from sets where short_name = 'rna'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azorius Guildgate'),
    (select id from sets where short_name = 'rna'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'rna'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thought Collapse'),
    (select id from sets where short_name = 'rna'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carrion Imp'),
    (select id from sets where short_name = 'rna'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tome of the Guildpact'),
    (select id from sets where short_name = 'rna'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plague Wight'),
    (select id from sets where short_name = 'rna'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skewer the Critics'),
    (select id from sets where short_name = 'rna'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pitiless Pontiff'),
    (select id from sets where short_name = 'rna'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gate Colossus'),
    (select id from sets where short_name = 'rna'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mammoth Spider'),
    (select id from sets where short_name = 'rna'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunder Shaman'),
    (select id from sets where short_name = 'rna'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Collision // Colossus'),
    (select id from sets where short_name = 'rna'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Macabre Mockery'),
    (select id from sets where short_name = 'rna'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cindervines'),
    (select id from sets where short_name = 'rna'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spire Mangler'),
    (select id from sets where short_name = 'rna'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hackrobat'),
    (select id from sets where short_name = 'rna'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'End-Raze Forerunners'),
    (select id from sets where short_name = 'rna'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clear the Mind'),
    (select id from sets where short_name = 'rna'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Duelist'),
    (select id from sets where short_name = 'rna'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lawmage''s Binding'),
    (select id from sets where short_name = 'rna'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thirsting Shade'),
    (select id from sets where short_name = 'rna'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Civic Stalwart'),
    (select id from sets where short_name = 'rna'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Depose // Deploy'),
    (select id from sets where short_name = 'rna'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Theater of Horrors'),
    (select id from sets where short_name = 'rna'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arrester''s Zeal'),
    (select id from sets where short_name = 'rna'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dovin, Architect of Law'),
    (select id from sets where short_name = 'rna'),
    '265',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Archway Angel'),
    (select id from sets where short_name = 'rna'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tin Street Dodger'),
    (select id from sets where short_name = 'rna'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clamor Shaman'),
    (select id from sets where short_name = 'rna'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bedeck // Bedazzle'),
    (select id from sets where short_name = 'rna'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slimebind'),
    (select id from sets where short_name = 'rna'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grotesque Demise'),
    (select id from sets where short_name = 'rna'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Noxious Groodion'),
    (select id from sets where short_name = 'rna'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burn Bright'),
    (select id from sets where short_name = 'rna'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gift of Strength'),
    (select id from sets where short_name = 'rna'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Breeding Pool'),
    (select id from sets where short_name = 'rna'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frenzied Arynx'),
    (select id from sets where short_name = 'rna'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Titanic Brawl'),
    (select id from sets where short_name = 'rna'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teysa Karlov'),
    (select id from sets where short_name = 'rna'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tithe Taker'),
    (select id from sets where short_name = 'rna'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skarrgan Hellkite'),
    (select id from sets where short_name = 'rna'),
    '114',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ministrant of Obligation'),
    (select id from sets where short_name = 'rna'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rafter Demon'),
    (select id from sets where short_name = 'rna'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undercity''s Embrace'),
    (select id from sets where short_name = 'rna'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Summary Judgment'),
    (select id from sets where short_name = 'rna'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forbidding Spirit'),
    (select id from sets where short_name = 'rna'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azorius Skyguard'),
    (select id from sets where short_name = 'rna'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rix Maadi Reveler'),
    (select id from sets where short_name = 'rna'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enraged Ceratok'),
    (select id from sets where short_name = 'rna'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lavinia, Azorius Renegade'),
    (select id from sets where short_name = 'rna'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prowling Caracal'),
    (select id from sets where short_name = 'rna'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burning-Tree Vandal'),
    (select id from sets where short_name = 'rna'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ragefire'),
    (select id from sets where short_name = 'rna'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orzhov Guildgate'),
    (select id from sets where short_name = 'rna'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Galloping Lizrog'),
    (select id from sets where short_name = 'rna'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guardian Project'),
    (select id from sets where short_name = 'rna'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'High Alert'),
    (select id from sets where short_name = 'rna'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Essence Capture'),
    (select id from sets where short_name = 'rna'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Godless Shrine'),
    (select id from sets where short_name = 'rna'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphinx of Foresight'),
    (select id from sets where short_name = 'rna'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rally to Battle'),
    (select id from sets where short_name = 'rna'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glass of the Guildpact'),
    (select id from sets where short_name = 'rna'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carnival // Carnage'),
    (select id from sets where short_name = 'rna'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sagittars'' Volley'),
    (select id from sets where short_name = 'rna'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Biomancer''s Familiar'),
    (select id from sets where short_name = 'rna'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sage''s Row Savant'),
    (select id from sets where short_name = 'rna'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seraph of the Scales'),
    (select id from sets where short_name = 'rna'),
    '205',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sphinx''s Insight'),
    (select id from sets where short_name = 'rna'),
    '209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Font of Agonies'),
    (select id from sets where short_name = 'rna'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Firewheeler'),
    (select id from sets where short_name = 'rna'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scuttlegator'),
    (select id from sets where short_name = 'rna'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakdos Guildgate'),
    (select id from sets where short_name = 'rna'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx of New Prahv'),
    (select id from sets where short_name = 'rna'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Senate Courier'),
    (select id from sets where short_name = 'rna'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skitter Eel'),
    (select id from sets where short_name = 'rna'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit of the Spires'),
    (select id from sets where short_name = 'rna'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spear Spewer'),
    (select id from sets where short_name = 'rna'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stomping Ground'),
    (select id from sets where short_name = 'rna'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zegana, Utopian Speaker'),
    (select id from sets where short_name = 'rna'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Incubation // Incongruity'),
    (select id from sets where short_name = 'rna'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sharktocrab'),
    (select id from sets where short_name = 'rna'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Priest of Forgotten Gods'),
    (select id from sets where short_name = 'rna'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azorius Guildgate'),
    (select id from sets where short_name = 'rna'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bankrupt in Blood'),
    (select id from sets where short_name = 'rna'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dovin, Grand Arbiter'),
    (select id from sets where short_name = 'rna'),
    '167',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sentinel''s Mark'),
    (select id from sets where short_name = 'rna'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Basilica Bell-Haunt'),
    (select id from sets where short_name = 'rna'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Syndicate Guildmage'),
    (select id from sets where short_name = 'rna'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Persistent Petitioners'),
    (select id from sets where short_name = 'rna'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Awaken the Erstwhile'),
    (select id from sets where short_name = 'rna'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silhana Wayfinder'),
    (select id from sets where short_name = 'rna'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Screaming Shield'),
    (select id from sets where short_name = 'rna'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mortify'),
    (select id from sets where short_name = 'rna'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gateway Sneak'),
    (select id from sets where short_name = 'rna'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quench'),
    (select id from sets where short_name = 'rna'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Exaltation'),
    (select id from sets where short_name = 'rna'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Saruli Caretaker'),
    (select id from sets where short_name = 'rna'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skatewing Spy'),
    (select id from sets where short_name = 'rna'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tenth District Veteran'),
    (select id from sets where short_name = 'rna'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rubble Slinger'),
    (select id from sets where short_name = 'rna'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steeple Creeper'),
    (select id from sets where short_name = 'rna'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dead Revels'),
    (select id from sets where short_name = 'rna'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Repudiate // Replicate'),
    (select id from sets where short_name = 'rna'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Guildgate'),
    (select id from sets where short_name = 'rna'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bladebrand'),
    (select id from sets where short_name = 'rna'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Applied Biomancy'),
    (select id from sets where short_name = 'rna'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hallowed Fountain'),
    (select id from sets where short_name = 'rna'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imperious Oligarch'),
    (select id from sets where short_name = 'rna'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kaya, Orzhov Usurper'),
    (select id from sets where short_name = 'rna'),
    '186',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Open the Gates'),
    (select id from sets where short_name = 'rna'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simic Ascendancy'),
    (select id from sets where short_name = 'rna'),
    '207',
    'rare'
) 
 on conflict do nothing;
