insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Contagion'),
    (select id from sets where short_name = 'all'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Royal Herbalist'),
    (select id from sets where short_name = 'all'),
    '15a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadly Insect'),
    (select id from sets where short_name = 'all'),
    '86b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sheltered Valley'),
    (select id from sets where short_name = 'all'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soldevi Sage'),
    (select id from sets where short_name = 'all'),
    '34a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Martyrdom'),
    (select id from sets where short_name = 'all'),
    '10b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Awesome Presence'),
    (select id from sets where short_name = 'all'),
    '23b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Splintering Wind'),
    (select id from sets where short_name = 'all'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stromgald Spy'),
    (select id from sets where short_name = 'all'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Escort'),
    (select id from sets where short_name = 'all'),
    '7a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exile'),
    (select id from sets where short_name = 'all'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balduvian War-Makers'),
    (select id from sets where short_name = 'all'),
    '66a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fevered Strength'),
    (select id from sets where short_name = 'all'),
    '50a',
    'common'
) ,
(
    (select id from mtgcard where name = 'School of the Unseen'),
    (select id from sets where short_name = 'all'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Benthic Explorers'),
    (select id from sets where short_name = 'all'),
    '24a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shield Sphere'),
    (select id from sets where short_name = 'all'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reprisal'),
    (select id from sets where short_name = 'all'),
    '13a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Storm Cauldron'),
    (select id from sets where short_name = 'all'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp Mosquito'),
    (select id from sets where short_name = 'all'),
    '63a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bounty of the Hunt'),
    (select id from sets where short_name = 'all'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fatal Lore'),
    (select id from sets where short_name = 'all'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm Elemental'),
    (select id from sets where short_name = 'all'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Engine'),
    (select id from sets where short_name = 'all'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Fiend'),
    (select id from sets where short_name = 'all'),
    '57b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viscerid Drone'),
    (select id from sets where short_name = 'all'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Storm Crow'),
    (select id from sets where short_name = 'all'),
    '36b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Misinformation'),
    (select id from sets where short_name = 'all'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Devourer'),
    (select id from sets where short_name = 'all'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whip Vine'),
    (select id from sets where short_name = 'all'),
    '103b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sworn Defender'),
    (select id from sets where short_name = 'all'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gorilla Berserkers'),
    (select id from sets where short_name = 'all'),
    '93a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aesthir Glider'),
    (select id from sets where short_name = 'all'),
    '116a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ivory Gargoyle'),
    (select id from sets where short_name = 'all'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soldier of Fortune'),
    (select id from sets where short_name = 'all'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pillage'),
    (select id from sets where short_name = 'all'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diseased Vermin'),
    (select id from sets where short_name = 'all'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Benthic Explorers'),
    (select id from sets where short_name = 'all'),
    '24b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Storm Shaman'),
    (select id from sets where short_name = 'all'),
    '81b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guerrilla Tactics'),
    (select id from sets where short_name = 'all'),
    '74b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heart of Yavimaya'),
    (select id from sets where short_name = 'all'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcane Denial'),
    (select id from sets where short_name = 'all'),
    '22b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aesthir Glider'),
    (select id from sets where short_name = 'all'),
    '116b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lim-Dûl''s Paladin'),
    (select id from sets where short_name = 'all'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nature''s Wrath'),
    (select id from sets where short_name = 'all'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noble Steeds'),
    (select id from sets where short_name = 'all'),
    '11a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gorilla Chieftain'),
    (select id from sets where short_name = 'all'),
    '94a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gargantuan Gorilla'),
    (select id from sets where short_name = 'all'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Agent of Stromgald'),
    (select id from sets where short_name = 'all'),
    '64a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whip Vine'),
    (select id from sets where short_name = 'all'),
    '103a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insidious Bookworms'),
    (select id from sets where short_name = 'all'),
    '51a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guerrilla Tactics'),
    (select id from sets where short_name = 'all'),
    '74a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ritual of the Machine'),
    (select id from sets where short_name = 'all'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soldevi Sage'),
    (select id from sets where short_name = 'all'),
    '34b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lim-Dûl''s High Guard'),
    (select id from sets where short_name = 'all'),
    '55b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carrier Pigeons'),
    (select id from sets where short_name = 'all'),
    '1a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Browse'),
    (select id from sets where short_name = 'all'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gorilla Shaman'),
    (select id from sets where short_name = 'all'),
    '72b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldevi Digger'),
    (select id from sets where short_name = 'all'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burnout'),
    (select id from sets where short_name = 'all'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Spirit Guide'),
    (select id from sets where short_name = 'all'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nature''s Blessing'),
    (select id from sets where short_name = 'all'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Ants'),
    (select id from sets where short_name = 'all'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Library of Lat-Nam'),
    (select id from sets where short_name = 'all'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dystopia'),
    (select id from sets where short_name = 'all'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lord of Tresserhorn'),
    (select id from sets where short_name = 'all'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gorilla War Cry'),
    (select id from sets where short_name = 'all'),
    '73a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whirling Catapult'),
    (select id from sets where short_name = 'all'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Bard'),
    (select id from sets where short_name = 'all'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bestial Fury'),
    (select id from sets where short_name = 'all'),
    '67a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sol Grail'),
    (select id from sets where short_name = 'all'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scars of the Veteran'),
    (select id from sets where short_name = 'all'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thawing Glaciers'),
    (select id from sets where short_name = 'all'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Royal Herbalist'),
    (select id from sets where short_name = 'all'),
    '15b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Noble Steeds'),
    (select id from sets where short_name = 'all'),
    '11b',
    'common'
) ,
(
    (select id from mtgcard where name = 'False Demise'),
    (select id from sets where short_name = 'all'),
    '27a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gift of the Woods'),
    (select id from sets where short_name = 'all'),
    '92b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Martyrdom'),
    (select id from sets where short_name = 'all'),
    '10a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gorilla Chieftain'),
    (select id from sets where short_name = 'all'),
    '94b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Errand of Duty'),
    (select id from sets where short_name = 'all'),
    '2a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Veteran''s Voice'),
    (select id from sets where short_name = 'all'),
    '84b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Boon'),
    (select id from sets where short_name = 'all'),
    '58b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Escort'),
    (select id from sets where short_name = 'all'),
    '7b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bestial Fury'),
    (select id from sets where short_name = 'all'),
    '67b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Groundbreaker'),
    (select id from sets where short_name = 'all'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Portal'),
    (select id from sets where short_name = 'all'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fyndhorn Druid'),
    (select id from sets where short_name = 'all'),
    '90a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Home Guard'),
    (select id from sets where short_name = 'all'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gustha''s Scepter'),
    (select id from sets where short_name = 'all'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Insidious Bookworms'),
    (select id from sets where short_name = 'all'),
    '51b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undergrowth'),
    (select id from sets where short_name = 'all'),
    '102b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Ranger'),
    (select id from sets where short_name = 'all'),
    '88b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nature''s Chosen'),
    (select id from sets where short_name = 'all'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feast or Famine'),
    (select id from sets where short_name = 'all'),
    '49a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winter''s Night'),
    (select id from sets where short_name = 'all'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Awesome Presence'),
    (select id from sets where short_name = 'all'),
    '23a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldevi Adnate'),
    (select id from sets where short_name = 'all'),
    '60b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reprisal'),
    (select id from sets where short_name = 'all'),
    '13b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lodestone Bauble'),
    (select id from sets where short_name = 'all'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soldevi Sentry'),
    (select id from sets where short_name = 'all'),
    '132a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Casting of Bones'),
    (select id from sets where short_name = 'all'),
    '44b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Errand of Duty'),
    (select id from sets where short_name = 'all'),
    '2b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldevi Adnate'),
    (select id from sets where short_name = 'all'),
    '60a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wandering Mage'),
    (select id from sets where short_name = 'all'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soldevi Excavations'),
    (select id from sets where short_name = 'all'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balduvian Horde'),
    (select id from sets where short_name = 'all'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Astrolabe'),
    (select id from sets where short_name = 'all'),
    '118b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Ancients'),
    (select id from sets where short_name = 'all'),
    '104b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Juniper Order Advocate'),
    (select id from sets where short_name = 'all'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Storm Shaman'),
    (select id from sets where short_name = 'all'),
    '81a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Veteran''s Voice'),
    (select id from sets where short_name = 'all'),
    '84a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Ancients'),
    (select id from sets where short_name = 'all'),
    '104a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldevi Heretic'),
    (select id from sets where short_name = 'all'),
    '33b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Surge of Strength'),
    (select id from sets where short_name = 'all'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rogue Skycaptain'),
    (select id from sets where short_name = 'all'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Fiend'),
    (select id from sets where short_name = 'all'),
    '57a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Force of Will'),
    (select id from sets where short_name = 'all'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Undergrowth'),
    (select id from sets where short_name = 'all'),
    '102a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Omen of Fire'),
    (select id from sets where short_name = 'all'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carrier Pigeons'),
    (select id from sets where short_name = 'all'),
    '1b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldevi Steam Beast'),
    (select id from sets where short_name = 'all'),
    '133b',
    'common'
) ,
(
    (select id from mtgcard where name = 'False Demise'),
    (select id from sets where short_name = 'all'),
    '27b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inheritance'),
    (select id from sets where short_name = 'all'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Foresight'),
    (select id from sets where short_name = 'all'),
    '29a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Storm Crow'),
    (select id from sets where short_name = 'all'),
    '36a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tornado'),
    (select id from sets where short_name = 'all'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balduvian Trading Post'),
    (select id from sets where short_name = 'all'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diminishing Returns'),
    (select id from sets where short_name = 'all'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foresight'),
    (select id from sets where short_name = 'all'),
    '29b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Taste of Paradise'),
    (select id from sets where short_name = 'all'),
    '100b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hail Storm'),
    (select id from sets where short_name = 'all'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Sphere'),
    (select id from sets where short_name = 'all'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Pride'),
    (select id from sets where short_name = 'all'),
    '9b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enslaved Scout'),
    (select id from sets where short_name = 'all'),
    '71b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keeper of Tresserhorn'),
    (select id from sets where short_name = 'all'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp Mosquito'),
    (select id from sets where short_name = 'all'),
    '63b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Ranger'),
    (select id from sets where short_name = 'all'),
    '88a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadly Insect'),
    (select id from sets where short_name = 'all'),
    '86a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lake of the Dead'),
    (select id from sets where short_name = 'all'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcane Denial'),
    (select id from sets where short_name = 'all'),
    '22a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Misfortune'),
    (select id from sets where short_name = 'all'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Helm of Obedience'),
    (select id from sets where short_name = 'all'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krovikan Plague'),
    (select id from sets where short_name = 'all'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stench of Decay'),
    (select id from sets where short_name = 'all'),
    '61b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Varchild''s Crusader'),
    (select id from sets where short_name = 'all'),
    '82a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lim-Dûl''s High Guard'),
    (select id from sets where short_name = 'all'),
    '55a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viscerid Armor'),
    (select id from sets where short_name = 'all'),
    '41b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Taste of Paradise'),
    (select id from sets where short_name = 'all'),
    '100a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Boon'),
    (select id from sets where short_name = 'all'),
    '58a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Astrolabe'),
    (select id from sets where short_name = 'all'),
    '118a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Spark'),
    (select id from sets where short_name = 'all'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soldevi Sentry'),
    (select id from sets where short_name = 'all'),
    '132b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gorilla Shaman'),
    (select id from sets where short_name = 'all'),
    '72a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reinforcements'),
    (select id from sets where short_name = 'all'),
    '12b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unlikely Alliance'),
    (select id from sets where short_name = 'all'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spiny Starfish'),
    (select id from sets where short_name = 'all'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sustaining Spirit'),
    (select id from sets where short_name = 'all'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fevered Strength'),
    (select id from sets where short_name = 'all'),
    '50b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tidal Control'),
    (select id from sets where short_name = 'all'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lat-Nam''s Legacy'),
    (select id from sets where short_name = 'all'),
    '30b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kaysa'),
    (select id from sets where short_name = 'all'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Pride'),
    (select id from sets where short_name = 'all'),
    '9a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Suffocation'),
    (select id from sets where short_name = 'all'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Royal Decree'),
    (select id from sets where short_name = 'all'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Floodwater Dam'),
    (select id from sets where short_name = 'all'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soldevi Heretic'),
    (select id from sets where short_name = 'all'),
    '33a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thought Lash'),
    (select id from sets where short_name = 'all'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scarab of the Unseen'),
    (select id from sets where short_name = 'all'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Primitive Justice'),
    (select id from sets where short_name = 'all'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Varchild''s Crusader'),
    (select id from sets where short_name = 'all'),
    '82b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reinforcements'),
    (select id from sets where short_name = 'all'),
    '12a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Cylix'),
    (select id from sets where short_name = 'all'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Agent of Stromgald'),
    (select id from sets where short_name = 'all'),
    '64b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phelddagrif'),
    (select id from sets where short_name = 'all'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viscerid Armor'),
    (select id from sets where short_name = 'all'),
    '41a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gift of the Woods'),
    (select id from sets where short_name = 'all'),
    '92a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balduvian War-Makers'),
    (select id from sets where short_name = 'all'),
    '66b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyrokinesis'),
    (select id from sets where short_name = 'all'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Aesthir'),
    (select id from sets where short_name = 'all'),
    '21a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Compass'),
    (select id from sets where short_name = 'all'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Outpost'),
    (select id from sets where short_name = 'all'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gorilla War Cry'),
    (select id from sets where short_name = 'all'),
    '73b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krovikan Horror'),
    (select id from sets where short_name = 'all'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lat-Nam''s Legacy'),
    (select id from sets where short_name = 'all'),
    '30a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gorilla Berserkers'),
    (select id from sets where short_name = 'all'),
    '93b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian War Beast'),
    (select id from sets where short_name = 'all'),
    '127a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian War Beast'),
    (select id from sets where short_name = 'all'),
    '127b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Casting of Bones'),
    (select id from sets where short_name = 'all'),
    '44a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enslaved Scout'),
    (select id from sets where short_name = 'all'),
    '71a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldevi Steam Beast'),
    (select id from sets where short_name = 'all'),
    '133a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chaos Harlequin'),
    (select id from sets where short_name = 'all'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Varchild''s War-Riders'),
    (select id from sets where short_name = 'all'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feast or Famine'),
    (select id from sets where short_name = 'all'),
    '49b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fyndhorn Druid'),
    (select id from sets where short_name = 'all'),
    '90b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Energy Arc'),
    (select id from sets where short_name = 'all'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seasoned Tactician'),
    (select id from sets where short_name = 'all'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stench of Decay'),
    (select id from sets where short_name = 'all'),
    '61a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lim-Dûl''s Vault'),
    (select id from sets where short_name = 'all'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balduvian Dead'),
    (select id from sets where short_name = 'all'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Aesthir'),
    (select id from sets where short_name = 'all'),
    '21b',
    'common'
) 
 on conflict do nothing;
