insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Liliana of the Dark Realms'),
    (select id from sets where short_name = 'm14'),
    '102',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Angelic Accord'),
    (select id from sets where short_name = 'm14'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm14'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quicken'),
    (select id from sets where short_name = 'm14'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Banisher Priest'),
    (select id from sets where short_name = 'm14'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rise of the Dark Realms'),
    (select id from sets where short_name = 'm14'),
    '111',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm14'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trollhide'),
    (select id from sets where short_name = 'm14'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brave the Elements'),
    (select id from sets where short_name = 'm14'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coral Merfolk'),
    (select id from sets where short_name = 'm14'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Auramancer'),
    (select id from sets where short_name = 'm14'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Air Servant'),
    (select id from sets where short_name = 'm14'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Molten Birth'),
    (select id from sets where short_name = 'm14'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Imposing Sovereign'),
    (select id from sets where short_name = 'm14'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm14'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trading Post'),
    (select id from sets where short_name = 'm14'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kalonian Tusker'),
    (select id from sets where short_name = 'm14'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Messenger Drake'),
    (select id from sets where short_name = 'm14'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grim Return'),
    (select id from sets where short_name = 'm14'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm14'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Encroaching Wastes'),
    (select id from sets where short_name = 'm14'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silence'),
    (select id from sets where short_name = 'm14'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lava Axe'),
    (select id from sets where short_name = 'm14'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burning Earth'),
    (select id from sets where short_name = 'm14'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fortify'),
    (select id from sets where short_name = 'm14'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm14'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shadowborn Apostle'),
    (select id from sets where short_name = 'm14'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howl of the Night Pack'),
    (select id from sets where short_name = 'm14'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ring of Three Wishes'),
    (select id from sets where short_name = 'm14'),
    '216',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Warden of Evos Isle'),
    (select id from sets where short_name = 'm14'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Millstone'),
    (select id from sets where short_name = 'm14'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm14'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ranger''s Guile'),
    (select id from sets where short_name = 'm14'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = 'm14'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Xathrid Necromancer'),
    (select id from sets where short_name = 'm14'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scourge of Valkas'),
    (select id from sets where short_name = 'm14'),
    '151',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jace, Memory Adept'),
    (select id from sets where short_name = 'm14'),
    '60',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Suntail Hawk'),
    (select id from sets where short_name = 'm14'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darksteel Ingot'),
    (select id from sets where short_name = 'm14'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rootwalla'),
    (select id from sets where short_name = 'm14'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = 'm14'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Master of Diversion'),
    (select id from sets where short_name = 'm14'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darksteel Forge'),
    (select id from sets where short_name = 'm14'),
    '206',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm14'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sporemound'),
    (select id from sets where short_name = 'm14'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bramblecrush'),
    (select id from sets where short_name = 'm14'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'm14'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk Spy'),
    (select id from sets where short_name = 'm14'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sliver Construct'),
    (select id from sets where short_name = 'm14'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archaeomancer'),
    (select id from sets where short_name = 'm14'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm14'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ajani, Caller of the Pride'),
    (select id from sets where short_name = 'm14'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Canyon Minotaur'),
    (select id from sets where short_name = 'm14'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liturgy of Blood'),
    (select id from sets where short_name = 'm14'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = 'm14'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thorncaster Sliver'),
    (select id from sets where short_name = 'm14'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soulmender'),
    (select id from sets where short_name = 'm14'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = 'm14'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shadowborn Demon'),
    (select id from sets where short_name = 'm14'),
    '115',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nephalia Seakite'),
    (select id from sets where short_name = 'm14'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Artificer''s Hex'),
    (select id from sets where short_name = 'm14'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armored Cancrix'),
    (select id from sets where short_name = 'm14'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lay of the Land'),
    (select id from sets where short_name = 'm14'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pay No Heed'),
    (select id from sets where short_name = 'm14'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windstorm'),
    (select id from sets where short_name = 'm14'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm14'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elixir of Immortality'),
    (select id from sets where short_name = 'm14'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volcanic Geyser'),
    (select id from sets where short_name = 'm14'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ratchet Bomb'),
    (select id from sets where short_name = 'm14'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vile Rebirth'),
    (select id from sets where short_name = 'm14'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Siege Mastodon'),
    (select id from sets where short_name = 'm14'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Galerider Sliver'),
    (select id from sets where short_name = 'm14'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blessing'),
    (select id from sets where short_name = 'm14'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spell Blast'),
    (select id from sets where short_name = 'm14'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kalonian Hydra'),
    (select id from sets where short_name = 'm14'),
    '181',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm14'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Diplomats'),
    (select id from sets where short_name = 'm14'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'm14'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shrivel'),
    (select id from sets where short_name = 'm14'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Essence Scatter'),
    (select id from sets where short_name = 'm14'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Regathan Firecat'),
    (select id from sets where short_name = 'm14'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Striking Sliver'),
    (select id from sets where short_name = 'm14'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Path of Bravery'),
    (select id from sets where short_name = 'm14'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archangel of Thune'),
    (select id from sets where short_name = 'm14'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Seacoast Drake'),
    (select id from sets where short_name = 'm14'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gnawing Zombie'),
    (select id from sets where short_name = 'm14'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Chosen'),
    (select id from sets where short_name = 'm14'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Briarpack Alpha'),
    (select id from sets where short_name = 'm14'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Witchstalker'),
    (select id from sets where short_name = 'm14'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm14'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tenacious Dead'),
    (select id from sets where short_name = 'm14'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Favor'),
    (select id from sets where short_name = 'm14'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fiendslayer Paladin'),
    (select id from sets where short_name = 'm14'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Undead Minotaur'),
    (select id from sets where short_name = 'm14'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clone'),
    (select id from sets where short_name = 'm14'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Mystic'),
    (select id from sets where short_name = 'm14'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blightcaster'),
    (select id from sets where short_name = 'm14'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thunder Strike'),
    (select id from sets where short_name = 'm14'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Staff of the Death Magus'),
    (select id from sets where short_name = 'm14'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'm14'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blur Sliver'),
    (select id from sets where short_name = 'm14'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Outrage'),
    (select id from sets where short_name = 'm14'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Academy Raider'),
    (select id from sets where short_name = 'm14'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm14'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battle Sliver'),
    (select id from sets where short_name = 'm14'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Door of Destinies'),
    (select id from sets where short_name = 'm14'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestial Flare'),
    (select id from sets where short_name = 'm14'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Staff of the Flame Magus'),
    (select id from sets where short_name = 'm14'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trained Condor'),
    (select id from sets where short_name = 'm14'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm14'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sensory Deprivation'),
    (select id from sets where short_name = 'm14'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Groundshaker Sliver'),
    (select id from sets where short_name = 'm14'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mark of the Vampire'),
    (select id from sets where short_name = 'm14'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sentinel Sliver'),
    (select id from sets where short_name = 'm14'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bogbrew Witch'),
    (select id from sets where short_name = 'm14'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Doom Blade'),
    (select id from sets where short_name = 'm14'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Megantic Sliver'),
    (select id from sets where short_name = 'm14'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vial of Poison'),
    (select id from sets where short_name = 'm14'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demolish'),
    (select id from sets where short_name = 'm14'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Capashen Knight'),
    (select id from sets where short_name = 'm14'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm14'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Domestication'),
    (select id from sets where short_name = 'm14'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Swords'),
    (select id from sets where short_name = 'm14'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plummet'),
    (select id from sets where short_name = 'm14'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Warlord'),
    (select id from sets where short_name = 'm14'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shimmering Grotto'),
    (select id from sets where short_name = 'm14'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelic Wall'),
    (select id from sets where short_name = 'm14'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Syphon Sliver'),
    (select id from sets where short_name = 'm14'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frost Breath'),
    (select id from sets where short_name = 'm14'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Talons'),
    (select id from sets where short_name = 'm14'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'm14'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charging Griffin'),
    (select id from sets where short_name = 'm14'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk''s Horde'),
    (select id from sets where short_name = 'm14'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Devout Invocation'),
    (select id from sets where short_name = 'm14'),
    '16',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shiv''s Embrace'),
    (select id from sets where short_name = 'm14'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = 'm14'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fog'),
    (select id from sets where short_name = 'm14'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woodborn Behemoth'),
    (select id from sets where short_name = 'm14'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hunt the Weak'),
    (select id from sets where short_name = 'm14'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smelt'),
    (select id from sets where short_name = 'm14'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm14'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Staff of the Sun Magus'),
    (select id from sets where short_name = 'm14'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mutavault'),
    (select id from sets where short_name = 'm14'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dawnstrike Paladin'),
    (select id from sets where short_name = 'm14'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strionic Resonator'),
    (select id from sets where short_name = 'm14'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm14'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cancel'),
    (select id from sets where short_name = 'm14'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Ricochet'),
    (select id from sets where short_name = 'm14'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lifebane Zombie'),
    (select id from sets where short_name = 'm14'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Hatchling'),
    (select id from sets where short_name = 'm14'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Planar Cleansing'),
    (select id from sets where short_name = 'm14'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elite Arcanist'),
    (select id from sets where short_name = 'm14'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rod of Ruin'),
    (select id from sets where short_name = 'm14'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bubbling Cauldron'),
    (select id from sets where short_name = 'm14'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Traumatize'),
    (select id from sets where short_name = 'm14'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oath of the Ancient Wood'),
    (select id from sets where short_name = 'm14'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyromancer''s Gauntlet'),
    (select id from sets where short_name = 'm14'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Solemn Offering'),
    (select id from sets where short_name = 'm14'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minotaur Abomination'),
    (select id from sets where short_name = 'm14'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vastwood Hydra'),
    (select id from sets where short_name = 'm14'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathgaze Cockatrice'),
    (select id from sets where short_name = 'm14'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireshrieker'),
    (select id from sets where short_name = 'm14'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bonescythe Sliver'),
    (select id from sets where short_name = 'm14'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Water Servant'),
    (select id from sets where short_name = 'm14'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Predatory Sliver'),
    (select id from sets where short_name = 'm14'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brindle Boar'),
    (select id from sets where short_name = 'm14'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightwing Shade'),
    (select id from sets where short_name = 'm14'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Bairn'),
    (select id from sets where short_name = 'm14'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Reaver'),
    (select id from sets where short_name = 'm14'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quag Sickness'),
    (select id from sets where short_name = 'm14'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mindsparker'),
    (select id from sets where short_name = 'm14'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Garruk, Caller of Beasts'),
    (select id from sets where short_name = 'm14'),
    '172',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goblin Shortcutter'),
    (select id from sets where short_name = 'm14'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flames of the Firebrand'),
    (select id from sets where short_name = 'm14'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tome Scour'),
    (select id from sets where short_name = 'm14'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dismiss into Dream'),
    (select id from sets where short_name = 'm14'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Advocate of the Beast'),
    (select id from sets where short_name = 'm14'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Griffin Sentinel'),
    (select id from sets where short_name = 'm14'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enlarge'),
    (select id from sets where short_name = 'm14'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savage Summoning'),
    (select id from sets where short_name = 'm14'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm14'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Congregate'),
    (select id from sets where short_name = 'm14'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = 'm14'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marauding Maulhorn'),
    (select id from sets where short_name = 'm14'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barrage of Expendables'),
    (select id from sets where short_name = 'm14'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Act of Treason'),
    (select id from sets where short_name = 'm14'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Claustrophobia'),
    (select id from sets where short_name = 'm14'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra, Pyromaster'),
    (select id from sets where short_name = 'm14'),
    '132',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Time Ebb'),
    (select id from sets where short_name = 'm14'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divination'),
    (select id from sets where short_name = 'm14'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ogre Battledriver'),
    (select id from sets where short_name = 'm14'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Show of Valor'),
    (select id from sets where short_name = 'm14'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steelform Sliver'),
    (select id from sets where short_name = 'm14'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Child of Night'),
    (select id from sets where short_name = 'm14'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Accorder''s Shield'),
    (select id from sets where short_name = 'm14'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guardian of the Ages'),
    (select id from sets where short_name = 'm14'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deadly Recluse'),
    (select id from sets where short_name = 'm14'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Phoenix'),
    (select id from sets where short_name = 'm14'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corpse Hauler'),
    (select id from sets where short_name = 'm14'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'm14'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stonehorn Chanter'),
    (select id from sets where short_name = 'm14'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Accursed Spirit'),
    (select id from sets where short_name = 'm14'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illusionary Armor'),
    (select id from sets where short_name = 'm14'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Staff of the Wild Magus'),
    (select id from sets where short_name = 'm14'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm14'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Frost'),
    (select id from sets where short_name = 'm14'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Corrupt'),
    (select id from sets where short_name = 'm14'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wring Flesh'),
    (select id from sets where short_name = 'm14'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diabolic Tutor'),
    (select id from sets where short_name = 'm14'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm14'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rumbling Baloth'),
    (select id from sets where short_name = 'm14'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seraph of the Sword'),
    (select id from sets where short_name = 'm14'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voracious Wurm'),
    (select id from sets where short_name = 'm14'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Egg'),
    (select id from sets where short_name = 'm14'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm14'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tidebinder Mage'),
    (select id from sets where short_name = 'm14'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Indestructibility'),
    (select id from sets where short_name = 'm14'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gladecover Scout'),
    (select id from sets where short_name = 'm14'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Warrior'),
    (select id from sets where short_name = 'm14'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Opportunity'),
    (select id from sets where short_name = 'm14'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Haunted Plate Mail'),
    (select id from sets where short_name = 'm14'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Young Pyromancer'),
    (select id from sets where short_name = 'm14'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Divine Favor'),
    (select id from sets where short_name = 'm14'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Awaken the Ancient'),
    (select id from sets where short_name = 'm14'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hive Stirrings'),
    (select id from sets where short_name = 'm14'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanguine Bond'),
    (select id from sets where short_name = 'm14'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'm14'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disperse'),
    (select id from sets where short_name = 'm14'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primeval Bounty'),
    (select id from sets where short_name = 'm14'),
    '190',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wild Guess'),
    (select id from sets where short_name = 'm14'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Festering Newt'),
    (select id from sets where short_name = 'm14'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scavenging Ooze'),
    (select id from sets where short_name = 'm14'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Prophecy'),
    (select id from sets where short_name = 'm14'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Into the Wilds'),
    (select id from sets where short_name = 'm14'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cyclops Tyrant'),
    (select id from sets where short_name = 'm14'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fleshpulper Giant'),
    (select id from sets where short_name = 'm14'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Altar''s Reap'),
    (select id from sets where short_name = 'm14'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glimpse the Future'),
    (select id from sets where short_name = 'm14'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seismic Stomp'),
    (select id from sets where short_name = 'm14'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Verdant Haven'),
    (select id from sets where short_name = 'm14'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windreader Sphinx'),
    (select id from sets where short_name = 'm14'),
    '81',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jace''s Mindseeker'),
    (select id from sets where short_name = 'm14'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pillarfield Ox'),
    (select id from sets where short_name = 'm14'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Colossal Whale'),
    (select id from sets where short_name = 'm14'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Staff of the Mind Magus'),
    (select id from sets where short_name = 'm14'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pitchburn Devils'),
    (select id from sets where short_name = 'm14'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zephyr Charge'),
    (select id from sets where short_name = 'm14'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scroll Thief'),
    (select id from sets where short_name = 'm14'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manaweft Sliver'),
    (select id from sets where short_name = 'm14'),
    '184',
    'uncommon'
) 
 on conflict do nothing;
