insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Zealous Persecution'),
    (select id from sets where short_name = 'e02'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rush of Adrenaline'),
    (select id from sets where short_name = 'e02'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shielded by Faith'),
    (select id from sets where short_name = 'e02'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jungle Shrine'),
    (select id from sets where short_name = 'e02'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merfolk Sovereign'),
    (select id from sets where short_name = 'e02'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Threads of Disloyalty'),
    (select id from sets where short_name = 'e02'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Borderland Ranger'),
    (select id from sets where short_name = 'e02'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Gale'),
    (select id from sets where short_name = 'e02'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampire Noble'),
    (select id from sets where short_name = 'e02'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Veteran''s Reflexes'),
    (select id from sets where short_name = 'e02'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beacon of Immortality'),
    (select id from sets where short_name = 'e02'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampire Nighthawk'),
    (select id from sets where short_name = 'e02'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hunter''s Prowess'),
    (select id from sets where short_name = 'e02'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Path to Exile'),
    (select id from sets where short_name = 'e02'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rancor'),
    (select id from sets where short_name = 'e02'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mass Mutiny'),
    (select id from sets where short_name = 'e02'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crumbling Necropolis'),
    (select id from sets where short_name = 'e02'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul of the Harvest'),
    (select id from sets where short_name = 'e02'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vow of Lightning'),
    (select id from sets where short_name = 'e02'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Concentrate'),
    (select id from sets where short_name = 'e02'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodbond Vampire'),
    (select id from sets where short_name = 'e02'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necropolis Regent'),
    (select id from sets where short_name = 'e02'),
    '20',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lightning Helix'),
    (select id from sets where short_name = 'e02'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mortify'),
    (select id from sets where short_name = 'e02'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doom Blade'),
    (select id from sets where short_name = 'e02'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Time Warp'),
    (select id from sets where short_name = 'e02'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jungle Barrier'),
    (select id from sets where short_name = 'e02'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aggravated Assault'),
    (select id from sets where short_name = 'e02'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vow of Flight'),
    (select id from sets where short_name = 'e02'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Adaptive Automaton'),
    (select id from sets where short_name = 'e02'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disaster Radius'),
    (select id from sets where short_name = 'e02'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quicksilver Amulet'),
    (select id from sets where short_name = 'e02'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vow of Duty'),
    (select id from sets where short_name = 'e02'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blatant Thievery'),
    (select id from sets where short_name = 'e02'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shared Animosity'),
    (select id from sets where short_name = 'e02'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tainted Field'),
    (select id from sets where short_name = 'e02'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coat with Venom'),
    (select id from sets where short_name = 'e02'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Child of Night'),
    (select id from sets where short_name = 'e02'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Day of Judgment'),
    (select id from sets where short_name = 'e02'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vow of Wildness'),
    (select id from sets where short_name = 'e02'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampire Interloper'),
    (select id from sets where short_name = 'e02'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prey Upon'),
    (select id from sets where short_name = 'e02'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'e02'),
    'T1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prismatic Lens'),
    (select id from sets where short_name = 'e02'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'e02'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Innocent Blood'),
    (select id from sets where short_name = 'e02'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = 'e02'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urge to Feed'),
    (select id from sets where short_name = 'e02'),
    '21',
    'uncommon'
) 
 on conflict do nothing;
