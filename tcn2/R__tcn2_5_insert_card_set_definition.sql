insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tcn2'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insect'),
    (select id from sets where short_name = 'tcn2'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tcn2'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Monarch'),
    (select id from sets where short_name = 'tcn2'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tcn2'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lizard'),
    (select id from sets where short_name = 'tcn2'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Construct'),
    (select id from sets where short_name = 'tcn2'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tcn2'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tcn2'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tcn2'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Assassin'),
    (select id from sets where short_name = 'tcn2'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tcn2'),
    '4',
    'common'
) 
 on conflict do nothing;
