    insert into mtgcard(name) values ('Garruk''s Horde') on conflict do nothing;
    insert into mtgcard(name) values ('Dungrove Elder') on conflict do nothing;
    insert into mtgcard(name) values ('Stormblood Berserker') on conflict do nothing;
    insert into mtgcard(name) values ('Bloodlord of Vaasgoth') on conflict do nothing;
    insert into mtgcard(name) values ('Chandra''s Phoenix') on conflict do nothing;
