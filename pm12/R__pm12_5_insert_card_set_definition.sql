insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Garruk''s Horde'),
    (select id from sets where short_name = 'pm12'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dungrove Elder'),
    (select id from sets where short_name = 'pm12'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stormblood Berserker'),
    (select id from sets where short_name = 'pm12'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodlord of Vaasgoth'),
    (select id from sets where short_name = 'pm12'),
    '82',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Phoenix'),
    (select id from sets where short_name = 'pm12'),
    '*126',
    'rare'
) 
 on conflict do nothing;
