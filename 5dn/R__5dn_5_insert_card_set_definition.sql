insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Fleshgrafter'),
    (select id from sets where short_name = '5dn'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rude Awakening'),
    (select id from sets where short_name = '5dn'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plasma Elemental'),
    (select id from sets where short_name = '5dn'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Myr Servitor'),
    (select id from sets where short_name = '5dn'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Suntouched Myr'),
    (select id from sets where short_name = '5dn'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Auriok Salvagers'),
    (select id from sets where short_name = '5dn'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Iron-Barb Hellion'),
    (select id from sets where short_name = '5dn'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loxodon Stalwart'),
    (select id from sets where short_name = '5dn'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clock of Omens'),
    (select id from sets where short_name = '5dn'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nim Grotesque'),
    (select id from sets where short_name = '5dn'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Paradise Mantle'),
    (select id from sets where short_name = '5dn'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Relic Barrier'),
    (select id from sets where short_name = '5dn'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dawn''s Reflection'),
    (select id from sets where short_name = '5dn'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rain of Rust'),
    (select id from sets where short_name = '5dn'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bringer of the Red Dawn'),
    (select id from sets where short_name = '5dn'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reversal of Fortune'),
    (select id from sets where short_name = '5dn'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gemstone Array'),
    (select id from sets where short_name = '5dn'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spinal Parasite'),
    (select id from sets where short_name = '5dn'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Relentless Rats'),
    (select id from sets where short_name = '5dn'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shattered Dreams'),
    (select id from sets where short_name = '5dn'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krark-Clan Engineers'),
    (select id from sets where short_name = '5dn'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Razorgrass Screen'),
    (select id from sets where short_name = '5dn'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doubling Cube'),
    (select id from sets where short_name = '5dn'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ion Storm'),
    (select id from sets where short_name = '5dn'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beacon of Unrest'),
    (select id from sets where short_name = '5dn'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ebon Drake'),
    (select id from sets where short_name = '5dn'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Endless Whispers'),
    (select id from sets where short_name = '5dn'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vanquish'),
    (select id from sets where short_name = '5dn'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Conjurer''s Bauble'),
    (select id from sets where short_name = '5dn'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crucible of Worlds'),
    (select id from sets where short_name = '5dn'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viridian Scout'),
    (select id from sets where short_name = '5dn'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Summoner''s Egg'),
    (select id from sets where short_name = '5dn'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ferocious Charge'),
    (select id from sets where short_name = '5dn'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Geyser'),
    (select id from sets where short_name = '5dn'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beacon of Tomorrows'),
    (select id from sets where short_name = '5dn'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Opaline Bracers'),
    (select id from sets where short_name = '5dn'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sawtooth Thresher'),
    (select id from sets where short_name = '5dn'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wayfarer''s Bauble'),
    (select id from sets where short_name = '5dn'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Composite Golem'),
    (select id from sets where short_name = '5dn'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvok Explorer'),
    (select id from sets where short_name = '5dn'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lunar Avenger'),
    (select id from sets where short_name = '5dn'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Auriok Windwalker'),
    (select id from sets where short_name = '5dn'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lose Hope'),
    (select id from sets where short_name = '5dn'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raksha Golden Cub'),
    (select id from sets where short_name = '5dn'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bringer of the Blue Dawn'),
    (select id from sets where short_name = '5dn'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moriok Rigger'),
    (select id from sets where short_name = '5dn'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vulshok Sorcerer'),
    (select id from sets where short_name = '5dn'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serum Visions'),
    (select id from sets where short_name = '5dn'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trinket Mage'),
    (select id from sets where short_name = '5dn'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heliophial'),
    (select id from sets where short_name = '5dn'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Energy Chamber'),
    (select id from sets where short_name = '5dn'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magma Jet'),
    (select id from sets where short_name = '5dn'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grafted Wargear'),
    (select id from sets where short_name = '5dn'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avarice Totem'),
    (select id from sets where short_name = '5dn'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Qumulox'),
    (select id from sets where short_name = '5dn'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clearwater Goblet'),
    (select id from sets where short_name = '5dn'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bringer of the Black Dawn'),
    (select id from sets where short_name = '5dn'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myr Quadropod'),
    (select id from sets where short_name = '5dn'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hoverguard Sweepers'),
    (select id from sets where short_name = '5dn'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magma Giant'),
    (select id from sets where short_name = '5dn'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Retaliate'),
    (select id from sets where short_name = '5dn'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fangren Pathcutter'),
    (select id from sets where short_name = '5dn'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Early Frost'),
    (select id from sets where short_name = '5dn'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Squire'),
    (select id from sets where short_name = '5dn'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stasis Cocoon'),
    (select id from sets where short_name = '5dn'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beacon of Creation'),
    (select id from sets where short_name = '5dn'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tyrranax'),
    (select id from sets where short_name = '5dn'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tangle Asp'),
    (select id from sets where short_name = '5dn'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sparring Collar'),
    (select id from sets where short_name = '5dn'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beacon of Immortality'),
    (select id from sets where short_name = '5dn'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fold into Aether'),
    (select id from sets where short_name = '5dn'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Possessed Portal'),
    (select id from sets where short_name = '5dn'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Night''s Whisper'),
    (select id from sets where short_name = '5dn'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cackling Imp'),
    (select id from sets where short_name = '5dn'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krark-Clan Ogre'),
    (select id from sets where short_name = '5dn'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silent Arbiter'),
    (select id from sets where short_name = '5dn'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Suncrusher'),
    (select id from sets where short_name = '5dn'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pentad Prism'),
    (select id from sets where short_name = '5dn'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skullcage'),
    (select id from sets where short_name = '5dn'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armed Response'),
    (select id from sets where short_name = '5dn'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blasting Station'),
    (select id from sets where short_name = '5dn'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Advanced Hoverguard'),
    (select id from sets where short_name = '5dn'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desecration Elemental'),
    (select id from sets where short_name = '5dn'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Horned Helm'),
    (select id from sets where short_name = '5dn'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mephidross Vampire'),
    (select id from sets where short_name = '5dn'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vicious Betrayal'),
    (select id from sets where short_name = '5dn'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Staff of Domination'),
    (select id from sets where short_name = '5dn'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Joiner Adept'),
    (select id from sets where short_name = '5dn'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Channel the Suns'),
    (select id from sets where short_name = '5dn'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Prowler'),
    (select id from sets where short_name = '5dn'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Screaming Fury'),
    (select id from sets where short_name = '5dn'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arachnoid'),
    (select id from sets where short_name = '5dn'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thought Courier'),
    (select id from sets where short_name = '5dn'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Engineered Explosives'),
    (select id from sets where short_name = '5dn'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viridian Lorebearers'),
    (select id from sets where short_name = '5dn'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dross Crocodile'),
    (select id from sets where short_name = '5dn'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disruption Aura'),
    (select id from sets where short_name = '5dn'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blind Creeper'),
    (select id from sets where short_name = '5dn'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ouphe Vandals'),
    (select id from sets where short_name = '5dn'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Solarion'),
    (select id from sets where short_name = '5dn'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cranial Plating'),
    (select id from sets where short_name = '5dn'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stand Firm'),
    (select id from sets where short_name = '5dn'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vedalken Orrery'),
    (select id from sets where short_name = '5dn'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Healer''s Headdress'),
    (select id from sets where short_name = '5dn'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Skirmisher'),
    (select id from sets where short_name = '5dn'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Devour in Shadow'),
    (select id from sets where short_name = '5dn'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Acquire'),
    (select id from sets where short_name = '5dn'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fist of Suns'),
    (select id from sets where short_name = '5dn'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tel-Jilad Justice'),
    (select id from sets where short_name = '5dn'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chimeric Coils'),
    (select id from sets where short_name = '5dn'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ensouled Scimitar'),
    (select id from sets where short_name = '5dn'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bringer of the White Dawn'),
    (select id from sets where short_name = '5dn'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tornado Elemental'),
    (select id from sets where short_name = '5dn'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Synod Centurion'),
    (select id from sets where short_name = '5dn'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Furnace Whelp'),
    (select id from sets where short_name = '5dn'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feedback Bolt'),
    (select id from sets where short_name = '5dn'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loxodon Anchorite'),
    (select id from sets where short_name = '5dn'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Neurok Stealthsuit'),
    (select id from sets where short_name = '5dn'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guardian Idol'),
    (select id from sets where short_name = '5dn'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Artificer''s Intuition'),
    (select id from sets where short_name = '5dn'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Cannon'),
    (select id from sets where short_name = '5dn'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Artifacts'),
    (select id from sets where short_name = '5dn'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spectral Shift'),
    (select id from sets where short_name = '5dn'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Etched Oracle'),
    (select id from sets where short_name = '5dn'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Razormane Masticore'),
    (select id from sets where short_name = '5dn'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spark Elemental'),
    (select id from sets where short_name = '5dn'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abuna''s Chant'),
    (select id from sets where short_name = '5dn'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Auriok Champion'),
    (select id from sets where short_name = '5dn'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bringer of the Green Dawn'),
    (select id from sets where short_name = '5dn'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rite of Passage'),
    (select id from sets where short_name = '5dn'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beacon of Destruction'),
    (select id from sets where short_name = '5dn'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mycosynth Golem'),
    (select id from sets where short_name = '5dn'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vedalken Mastermind'),
    (select id from sets where short_name = '5dn'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krark-Clan Ironworks'),
    (select id from sets where short_name = '5dn'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eternal Witness'),
    (select id from sets where short_name = '5dn'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Summoning Station'),
    (select id from sets where short_name = '5dn'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Door to Nothingness'),
    (select id from sets where short_name = '5dn'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anodet Lurker'),
    (select id from sets where short_name = '5dn'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lantern of Insight'),
    (select id from sets where short_name = '5dn'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blinkmoth Infusion'),
    (select id from sets where short_name = '5dn'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ferropede'),
    (select id from sets where short_name = '5dn'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vedalken Shackles'),
    (select id from sets where short_name = '5dn'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'All Suns'' Dawn'),
    (select id from sets where short_name = '5dn'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Into Thin Air'),
    (select id from sets where short_name = '5dn'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plunge into Darkness'),
    (select id from sets where short_name = '5dn'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Baton of Courage'),
    (select id from sets where short_name = '5dn'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcbound Wanderer'),
    (select id from sets where short_name = '5dn'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Roar of Reclamation'),
    (select id from sets where short_name = '5dn'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eon Hub'),
    (select id from sets where short_name = '5dn'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steelshaper''s Gift'),
    (select id from sets where short_name = '5dn'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magnetic Theft'),
    (select id from sets where short_name = '5dn'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cosmic Larva'),
    (select id from sets where short_name = '5dn'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grinding Station'),
    (select id from sets where short_name = '5dn'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Granulate'),
    (select id from sets where short_name = '5dn'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fill with Fright'),
    (select id from sets where short_name = '5dn'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Condescend'),
    (select id from sets where short_name = '5dn'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thermal Navigator'),
    (select id from sets where short_name = '5dn'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tel-Jilad Lifebreather'),
    (select id from sets where short_name = '5dn'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Salvaging Station'),
    (select id from sets where short_name = '5dn'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infused Arrows'),
    (select id from sets where short_name = '5dn'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Brawler'),
    (select id from sets where short_name = '5dn'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battered Golem'),
    (select id from sets where short_name = '5dn'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyreach Manta'),
    (select id from sets where short_name = '5dn'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Helm of Kaldra'),
    (select id from sets where short_name = '5dn'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eyes of the Watcher'),
    (select id from sets where short_name = '5dn'),
    '30',
    'uncommon'
) 
 on conflict do nothing;
