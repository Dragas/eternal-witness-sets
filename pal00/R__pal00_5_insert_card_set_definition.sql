insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Pillage'),
    (select id from sets where short_name = 'pal00'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pal00'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uktabi Orangutan'),
    (select id from sets where short_name = 'pal00'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pal00'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enlightened Tutor'),
    (select id from sets where short_name = 'pal00'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stupor'),
    (select id from sets where short_name = 'pal00'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'pal00'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pal00'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pal00'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pal00'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chill'),
    (select id from sets where short_name = 'pal00'),
    '4',
    'rare'
) 
 on conflict do nothing;
