insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Mirari''s Wake'),
    (select id from sets where short_name = 'ha3'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gempalm Incinerator'),
    (select id from sets where short_name = 'ha3'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krosan Tusker'),
    (select id from sets where short_name = 'ha3'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancient Ziggurat'),
    (select id from sets where short_name = 'ha3'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maze''s End'),
    (select id from sets where short_name = 'ha3'),
    '27',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Akroma''s Memorial'),
    (select id from sets where short_name = 'ha3'),
    '24',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Honden of Cleansing Fire'),
    (select id from sets where short_name = 'ha3'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tectonic Reformation'),
    (select id from sets where short_name = 'ha3'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tempered Steel'),
    (select id from sets where short_name = 'ha3'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roar of the Wurm'),
    (select id from sets where short_name = 'ha3'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Timely Reinforcements'),
    (select id from sets where short_name = 'ha3'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ratchet Bomb'),
    (select id from sets where short_name = 'ha3'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Honden of Life''s Web'),
    (select id from sets where short_name = 'ha3'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ulamog, the Ceaseless Hunger'),
    (select id from sets where short_name = 'ha3'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swan Song'),
    (select id from sets where short_name = 'ha3'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silent Departure'),
    (select id from sets where short_name = 'ha3'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Honden of Infinite Rage'),
    (select id from sets where short_name = 'ha3'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gempalm Polluter'),
    (select id from sets where short_name = 'ha3'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enchantress''s Presence'),
    (select id from sets where short_name = 'ha3'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Momentary Blink'),
    (select id from sets where short_name = 'ha3'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Obliterator'),
    (select id from sets where short_name = 'ha3'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Honden of Seeing Winds'),
    (select id from sets where short_name = 'ha3'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Honden of Night''s Reach'),
    (select id from sets where short_name = 'ha3'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Body Double'),
    (select id from sets where short_name = 'ha3'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Devil''s Play'),
    (select id from sets where short_name = 'ha3'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unburial Rites'),
    (select id from sets where short_name = 'ha3'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chainer''s Edict'),
    (select id from sets where short_name = 'ha3'),
    '10',
    'uncommon'
) 
 on conflict do nothing;
