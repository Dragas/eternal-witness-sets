insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'God-Eternal Bontu'),
    (select id from sets where short_name = 'ps19'),
    '92',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, Dragon-God'),
    (select id from sets where short_name = 'ps19'),
    '207',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'God-Eternal Rhonas'),
    (select id from sets where short_name = 'ps19'),
    '163',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'God-Eternal Kefnet'),
    (select id from sets where short_name = 'ps19'),
    '53',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'God-Eternal Oketra'),
    (select id from sets where short_name = 'ps19'),
    '16',
    'mythic'
) 
 on conflict do nothing;
