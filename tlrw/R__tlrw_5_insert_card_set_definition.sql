insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'tlrw'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shapeshifter'),
    (select id from sets where short_name = 'tlrw'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tlrw'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elf Warrior'),
    (select id from sets where short_name = 'tlrw'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk Wizard'),
    (select id from sets where short_name = 'tlrw'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tlrw'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental Shaman'),
    (select id from sets where short_name = 'tlrw'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kithkin Soldier'),
    (select id from sets where short_name = 'tlrw'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tlrw'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Rogue'),
    (select id from sets where short_name = 'tlrw'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avatar'),
    (select id from sets where short_name = 'tlrw'),
    '1',
    'common'
) 
 on conflict do nothing;
