    insert into mtgcard(name) values ('Mind''s Desire') on conflict do nothing;
    insert into mtgcard(name) values ('Living Wish') on conflict do nothing;
    insert into mtgcard(name) values ('Orim''s Chant') on conflict do nothing;
    insert into mtgcard(name) values ('Demonic Tutor') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Piledriver') on conflict do nothing;
