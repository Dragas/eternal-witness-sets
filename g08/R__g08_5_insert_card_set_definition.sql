insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Mind''s Desire'),
    (select id from sets where short_name = 'g08'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Living Wish'),
    (select id from sets where short_name = 'g08'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orim''s Chant'),
    (select id from sets where short_name = 'g08'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonic Tutor'),
    (select id from sets where short_name = 'g08'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Piledriver'),
    (select id from sets where short_name = 'g08'),
    '4',
    'rare'
) 
 on conflict do nothing;
