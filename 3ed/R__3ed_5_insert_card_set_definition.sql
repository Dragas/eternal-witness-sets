insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Web'),
    (select id from sets where short_name = '3ed'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = '3ed'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Erg Raiders'),
    (select id from sets where short_name = '3ed'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Granite Gargoyle'),
    (select id from sets where short_name = '3ed'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uthden Troll'),
    (select id from sets where short_name = '3ed'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reverse Damage'),
    (select id from sets where short_name = '3ed'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = '3ed'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thicket Basilisk'),
    (select id from sets where short_name = '3ed'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Library of Leng'),
    (select id from sets where short_name = '3ed'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pirate Ship'),
    (select id from sets where short_name = '3ed'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blue Elemental Blast'),
    (select id from sets where short_name = '3ed'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Hive'),
    (select id from sets where short_name = '3ed'),
    '277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = '3ed'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brass Man'),
    (select id from sets where short_name = '3ed'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Copy Artifact'),
    (select id from sets where short_name = '3ed'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Onulet'),
    (select id from sets where short_name = '3ed'),
    '269',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = '3ed'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Contract from Below'),
    (select id from sets where short_name = '3ed'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Short'),
    (select id from sets where short_name = '3ed'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Power Sink'),
    (select id from sets where short_name = '3ed'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Wraith'),
    (select id from sets where short_name = '3ed'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island Fish Jasconius'),
    (select id from sets where short_name = '3ed'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Creature Bond'),
    (select id from sets where short_name = '3ed'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flight'),
    (select id from sets where short_name = '3ed'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Water'),
    (select id from sets where short_name = '3ed'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wooden Sphere'),
    (select id from sets where short_name = '3ed'),
    '281',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firebreathing'),
    (select id from sets where short_name = '3ed'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Helm of Chatzuk'),
    (select id from sets where short_name = '3ed'),
    '250',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mishra''s War Machine'),
    (select id from sets where short_name = '3ed'),
    '266',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '3ed'),
    '303',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scryb Sprites'),
    (select id from sets where short_name = '3ed'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Vise'),
    (select id from sets where short_name = '3ed'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = '3ed'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Underground Sea'),
    (select id from sets where short_name = '3ed'),
    '290',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prodigal Sorcerer'),
    (select id from sets where short_name = '3ed'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spell Blast'),
    (select id from sets where short_name = '3ed'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Millstone'),
    (select id from sets where short_name = '3ed'),
    '265',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = '3ed'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '3ed'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eye for an Eye'),
    (select id from sets where short_name = '3ed'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Archers'),
    (select id from sets where short_name = '3ed'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Green'),
    (select id from sets where short_name = '3ed'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '3ed'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Rack'),
    (select id from sets where short_name = '3ed'),
    '278',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jandor''s Ring'),
    (select id from sets where short_name = '3ed'),
    '256',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestial Prism'),
    (select id from sets where short_name = '3ed'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ivory Tower'),
    (select id from sets where short_name = '3ed'),
    '254',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aladdin''s Lamp'),
    (select id from sets where short_name = '3ed'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Specter'),
    (select id from sets where short_name = '3ed'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Red Elemental Blast'),
    (select id from sets where short_name = '3ed'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conservator'),
    (select id from sets where short_name = '3ed'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merfolk of the Pearl Trident'),
    (select id from sets where short_name = '3ed'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '3ed'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crumble'),
    (select id from sets where short_name = '3ed'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Twist'),
    (select id from sets where short_name = '3ed'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Healing Salve'),
    (select id from sets where short_name = '3ed'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scavenging Ghoul'),
    (select id from sets where short_name = '3ed'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serendib Efreet'),
    (select id from sets where short_name = '3ed'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verduran Enchantress'),
    (select id from sets where short_name = '3ed'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lure'),
    (select id from sets where short_name = '3ed'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = '3ed'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Holy Strength'),
    (select id from sets where short_name = '3ed'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chaoslace'),
    (select id from sets where short_name = '3ed'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ebony Horse'),
    (select id from sets where short_name = '3ed'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Regrowth'),
    (select id from sets where short_name = '3ed'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Net'),
    (select id from sets where short_name = '3ed'),
    '275',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unholy Strength'),
    (select id from sets where short_name = '3ed'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Timber Wolves'),
    (select id from sets where short_name = '3ed'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cockatrice'),
    (select id from sets where short_name = '3ed'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wanderlust'),
    (select id from sets where short_name = '3ed'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aladdin''s Ring'),
    (select id from sets where short_name = '3ed'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = '3ed'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Engine'),
    (select id from sets where short_name = '3ed'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Castle'),
    (select id from sets where short_name = '3ed'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Swords'),
    (select id from sets where short_name = '3ed'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lord of Atlantis'),
    (select id from sets where short_name = '3ed'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '3ed'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scrubland'),
    (select id from sets where short_name = '3ed'),
    '286',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Psychic Venom'),
    (select id from sets where short_name = '3ed'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mahamoti Djinn'),
    (select id from sets where short_name = '3ed'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plague Rats'),
    (select id from sets where short_name = '3ed'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sedge Troll'),
    (select id from sets where short_name = '3ed'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = '3ed'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Juggernaut'),
    (select id from sets where short_name = '3ed'),
    '259',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Royal Assassin'),
    (select id from sets where short_name = '3ed'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea Serpent'),
    (select id from sets where short_name = '3ed'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drain Power'),
    (select id from sets where short_name = '3ed'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunglasses of Urza'),
    (select id from sets where short_name = '3ed'),
    '276',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '3ed'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basalt Monolith'),
    (select id from sets where short_name = '3ed'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Bone'),
    (select id from sets where short_name = '3ed'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jade Monolith'),
    (select id from sets where short_name = '3ed'),
    '255',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '3ed'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Living Wall'),
    (select id from sets where short_name = '3ed'),
    '262',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shatterstorm'),
    (select id from sets where short_name = '3ed'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Blue'),
    (select id from sets where short_name = '3ed'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = '3ed'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Taiga'),
    (select id from sets where short_name = '3ed'),
    '287',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Will-o''-the-Wisp'),
    (select id from sets where short_name = '3ed'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Water Elemental'),
    (select id from sets where short_name = '3ed'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death Ward'),
    (select id from sets where short_name = '3ed'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Badlands'),
    (select id from sets where short_name = '3ed'),
    '282',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sleight of Mind'),
    (select id from sets where short_name = '3ed'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bayou'),
    (select id from sets where short_name = '3ed'),
    '283',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crusade'),
    (select id from sets where short_name = '3ed'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meekstone'),
    (select id from sets where short_name = '3ed'),
    '264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Force of Nature'),
    (select id from sets where short_name = '3ed'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tranquility'),
    (select id from sets where short_name = '3ed'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Artillery'),
    (select id from sets where short_name = '3ed'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drudge Skeletons'),
    (select id from sets where short_name = '3ed'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '3ed'),
    '302',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savannah'),
    (select id from sets where short_name = '3ed'),
    '285',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lord of the Pit'),
    (select id from sets where short_name = '3ed'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'War Mammoth'),
    (select id from sets where short_name = '3ed'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Vault'),
    (select id from sets where short_name = '3ed'),
    '263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Northern Paladin'),
    (select id from sets where short_name = '3ed'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lifelace'),
    (select id from sets where short_name = '3ed'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Iron Star'),
    (select id from sets where short_name = '3ed'),
    '252',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = '3ed'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = '3ed'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ankh of Mishra'),
    (select id from sets where short_name = '3ed'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magical Hack'),
    (select id from sets where short_name = '3ed'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fire Elemental'),
    (select id from sets where short_name = '3ed'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fog'),
    (select id from sets where short_name = '3ed'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Benalish Hero'),
    (select id from sets where short_name = '3ed'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Whelp'),
    (select id from sets where short_name = '3ed'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = '3ed'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Eruption'),
    (select id from sets where short_name = '3ed'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Stone'),
    (select id from sets where short_name = '3ed'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weakness'),
    (select id from sets where short_name = '3ed'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = '3ed'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tsunami'),
    (select id from sets where short_name = '3ed'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bottle of Suleiman'),
    (select id from sets where short_name = '3ed'),
    '237',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Liege'),
    (select id from sets where short_name = '3ed'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fear'),
    (select id from sets where short_name = '3ed'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Warriors'),
    (select id from sets where short_name = '3ed'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frozen Shade'),
    (select id from sets where short_name = '3ed'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scathe Zombies'),
    (select id from sets where short_name = '3ed'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Regeneration'),
    (select id from sets where short_name = '3ed'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vesuvan Doppelganger'),
    (select id from sets where short_name = '3ed'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = '3ed'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ironroot Treefolk'),
    (select id from sets where short_name = '3ed'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mons''s Goblin Raiders'),
    (select id from sets where short_name = '3ed'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demonic Attorney'),
    (select id from sets where short_name = '3ed'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = '3ed'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fungusaur'),
    (select id from sets where short_name = '3ed'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conversion'),
    (select id from sets where short_name = '3ed'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kormus Bell'),
    (select id from sets where short_name = '3ed'),
    '260',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Oriflamme'),
    (select id from sets where short_name = '3ed'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aspect of Wolf'),
    (select id from sets where short_name = '3ed'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guardian Angel'),
    (select id from sets where short_name = '3ed'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Samite Healer'),
    (select id from sets where short_name = '3ed'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Farmstead'),
    (select id from sets where short_name = '3ed'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantom Monster'),
    (select id from sets where short_name = '3ed'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sacrifice'),
    (select id from sets where short_name = '3ed'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Forces'),
    (select id from sets where short_name = '3ed'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smoke'),
    (select id from sets where short_name = '3ed'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flashfires'),
    (select id from sets where short_name = '3ed'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tunnel'),
    (select id from sets where short_name = '3ed'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mijae Djinn'),
    (select id from sets where short_name = '3ed'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flying Carpet'),
    (select id from sets where short_name = '3ed'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Earthbind'),
    (select id from sets where short_name = '3ed'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: White'),
    (select id from sets where short_name = '3ed'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Growth'),
    (select id from sets where short_name = '3ed'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = '3ed'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clockwork Beast'),
    (select id from sets where short_name = '3ed'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jump'),
    (select id from sets where short_name = '3ed'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thoughtlace'),
    (select id from sets where short_name = '3ed'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonic Tutor'),
    (select id from sets where short_name = '3ed'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grizzly Bears'),
    (select id from sets where short_name = '3ed'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desert Twister'),
    (select id from sets where short_name = '3ed'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Throne of Bone'),
    (select id from sets where short_name = '3ed'),
    '279',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Manabarbs'),
    (select id from sets where short_name = '3ed'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rocket Launcher'),
    (select id from sets where short_name = '3ed'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = '3ed'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warp Artifact'),
    (select id from sets where short_name = '3ed'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nettling Imp'),
    (select id from sets where short_name = '3ed'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '3ed'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cursed Land'),
    (select id from sets where short_name = '3ed'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rod of Ruin'),
    (select id from sets where short_name = '3ed'),
    '273',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kird Ape'),
    (select id from sets where short_name = '3ed'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Animate Artifact'),
    (select id from sets where short_name = '3ed'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Atog'),
    (select id from sets where short_name = '3ed'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = '3ed'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island Sanctuary'),
    (select id from sets where short_name = '3ed'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blue Ward'),
    (select id from sets where short_name = '3ed'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Energy Flux'),
    (select id from sets where short_name = '3ed'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tundra'),
    (select id from sets where short_name = '3ed'),
    '289',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bad Moon'),
    (select id from sets where short_name = '3ed'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Wood'),
    (select id from sets where short_name = '3ed'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Surge'),
    (select id from sets where short_name = '3ed'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '3ed'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tropical Island'),
    (select id from sets where short_name = '3ed'),
    '288',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balance'),
    (select id from sets where short_name = '3ed'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fork'),
    (select id from sets where short_name = '3ed'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone Giant'),
    (select id from sets where short_name = '3ed'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pearled Unicorn'),
    (select id from sets where short_name = '3ed'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '3ed'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Craw Wurm'),
    (select id from sets where short_name = '3ed'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glasses of Urza'),
    (select id from sets where short_name = '3ed'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Lands'),
    (select id from sets where short_name = '3ed'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roc of Kher Ridges'),
    (select id from sets where short_name = '3ed'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keldon Warlord'),
    (select id from sets where short_name = '3ed'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savannah Lions'),
    (select id from sets where short_name = '3ed'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disrupting Scepter'),
    (select id from sets where short_name = '3ed'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jandor''s Saddlebags'),
    (select id from sets where short_name = '3ed'),
    '257',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Ice'),
    (select id from sets where short_name = '3ed'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volcanic Island'),
    (select id from sets where short_name = '3ed'),
    '291',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kudzu'),
    (select id from sets where short_name = '3ed'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ivory Cup'),
    (select id from sets where short_name = '3ed'),
    '253',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Brambles'),
    (select id from sets where short_name = '3ed'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karma'),
    (select id from sets where short_name = '3ed'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ornithopter'),
    (select id from sets where short_name = '3ed'),
    '270',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '3ed'),
    '306',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reconstruction'),
    (select id from sets where short_name = '3ed'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demonic Hordes'),
    (select id from sets where short_name = '3ed'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Flare'),
    (select id from sets where short_name = '3ed'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clone'),
    (select id from sets where short_name = '3ed'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pestilence'),
    (select id from sets where short_name = '3ed'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = '3ed'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '3ed'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sorceress Queen'),
    (select id from sets where short_name = '3ed'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hurkyl''s Recall'),
    (select id from sets where short_name = '3ed'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Terrain'),
    (select id from sets where short_name = '3ed'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Purelace'),
    (select id from sets where short_name = '3ed'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = '3ed'),
    '267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stream of Life'),
    (select id from sets where short_name = '3ed'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'White Ward'),
    (select id from sets where short_name = '3ed'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Channel'),
    (select id from sets where short_name = '3ed'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Simulacrum'),
    (select id from sets where short_name = '3ed'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Air'),
    (select id from sets where short_name = '3ed'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '3ed'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shanodin Dryads'),
    (select id from sets where short_name = '3ed'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hurricane'),
    (select id from sets where short_name = '3ed'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Titania''s Song'),
    (select id from sets where short_name = '3ed'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Green Ward'),
    (select id from sets where short_name = '3ed'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wheel of Fortune'),
    (select id from sets where short_name = '3ed'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Resurrection'),
    (select id from sets where short_name = '3ed'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rock Hydra'),
    (select id from sets where short_name = '3ed'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burrowing'),
    (select id from sets where short_name = '3ed'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Balloon Brigade'),
    (select id from sets where short_name = '3ed'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'El-Hajjâj'),
    (select id from sets where short_name = '3ed'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Black'),
    (select id from sets where short_name = '3ed'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Ward'),
    (select id from sets where short_name = '3ed'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plateau'),
    (select id from sets where short_name = '3ed'),
    '284',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fastbond'),
    (select id from sets where short_name = '3ed'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dwarven Weaponsmith'),
    (select id from sets where short_name = '3ed'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hill Giant'),
    (select id from sets where short_name = '3ed'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lifeforce'),
    (select id from sets where short_name = '3ed'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = '3ed'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '3ed'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nether Shadow'),
    (select id from sets where short_name = '3ed'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dingus Egg'),
    (select id from sets where short_name = '3ed'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'White Knight'),
    (select id from sets where short_name = '3ed'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disintegrate'),
    (select id from sets where short_name = '3ed'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mesa Pegasus'),
    (select id from sets where short_name = '3ed'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lance'),
    (select id from sets where short_name = '3ed'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Primal Clay'),
    (select id from sets where short_name = '3ed'),
    '271',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = '3ed'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Veteran Bodyguard'),
    (select id from sets where short_name = '3ed'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evil Presence'),
    (select id from sets where short_name = '3ed'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Righteousness'),
    (select id from sets where short_name = '3ed'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drain Life'),
    (select id from sets where short_name = '3ed'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Control Magic'),
    (select id from sets where short_name = '3ed'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Instill Energy'),
    (select id from sets where short_name = '3ed'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Howl from Beyond'),
    (select id from sets where short_name = '3ed'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = '3ed'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paralyze'),
    (select id from sets where short_name = '3ed'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magnetic Mountain'),
    (select id from sets where short_name = '3ed'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feedback'),
    (select id from sets where short_name = '3ed'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steal Artifact'),
    (select id from sets where short_name = '3ed'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Animate Wall'),
    (select id from sets where short_name = '3ed'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Earth Elemental'),
    (select id from sets where short_name = '3ed'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gray Ogre'),
    (select id from sets where short_name = '3ed'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin King'),
    (select id from sets where short_name = '3ed'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dancing Scimitar'),
    (select id from sets where short_name = '3ed'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathlace'),
    (select id from sets where short_name = '3ed'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathgrip'),
    (select id from sets where short_name = '3ed'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Holy Armor'),
    (select id from sets where short_name = '3ed'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Living Artifact'),
    (select id from sets where short_name = '3ed'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Red Ward'),
    (select id from sets where short_name = '3ed'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reverse Polarity'),
    (select id from sets where short_name = '3ed'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = '3ed'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Personal Incarnation'),
    (select id from sets where short_name = '3ed'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Siren''s Call'),
    (select id from sets where short_name = '3ed'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jayemdae Tome'),
    (select id from sets where short_name = '3ed'),
    '258',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zombie Master'),
    (select id from sets where short_name = '3ed'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ley Druid'),
    (select id from sets where short_name = '3ed'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Knight'),
    (select id from sets where short_name = '3ed'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armageddon Clock'),
    (select id from sets where short_name = '3ed'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animate Dead'),
    (select id from sets where short_name = '3ed'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Obsianus Golem'),
    (select id from sets where short_name = '3ed'),
    '268',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lifetap'),
    (select id from sets where short_name = '3ed'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darkpact'),
    (select id from sets where short_name = '3ed'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winter Orb'),
    (select id from sets where short_name = '3ed'),
    '280',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blessing'),
    (select id from sets where short_name = '3ed'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Power Leak'),
    (select id from sets where short_name = '3ed'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hurloon Minotaur'),
    (select id from sets where short_name = '3ed'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Braingeyser'),
    (select id from sets where short_name = '3ed'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Fire'),
    (select id from sets where short_name = '3ed'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crystal Rod'),
    (select id from sets where short_name = '3ed'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = '3ed'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gloom'),
    (select id from sets where short_name = '3ed'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unstable Mutation'),
    (select id from sets where short_name = '3ed'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howling Mine'),
    (select id from sets where short_name = '3ed'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stasis'),
    (select id from sets where short_name = '3ed'),
    '83',
    'rare'
) 
 on conflict do nothing;
