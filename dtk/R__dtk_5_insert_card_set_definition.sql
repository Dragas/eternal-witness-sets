insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Gate Smasher'),
    (select id from sets where short_name = 'dtk'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightwalker'),
    (select id from sets where short_name = 'dtk'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shape the Sands'),
    (select id from sets where short_name = 'dtk'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glade Watcher'),
    (select id from sets where short_name = 'dtk'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foe-Razer Regent'),
    (select id from sets where short_name = 'dtk'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Commune with Lava'),
    (select id from sets where short_name = 'dtk'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Qarsi Sadist'),
    (select id from sets where short_name = 'dtk'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silumgar''s Scorn'),
    (select id from sets where short_name = 'dtk'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pristine Skywise'),
    (select id from sets where short_name = 'dtk'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Press the Advantage'),
    (select id from sets where short_name = 'dtk'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakshasa Gravecaller'),
    (select id from sets where short_name = 'dtk'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Obscuring Aether'),
    (select id from sets where short_name = 'dtk'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Battle Mastery'),
    (select id from sets where short_name = 'dtk'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Territorial Roc'),
    (select id from sets where short_name = 'dtk'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dtk'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'dtk'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strongarm Monk'),
    (select id from sets where short_name = 'dtk'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragonlord''s Servant'),
    (select id from sets where short_name = 'dtk'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Surge of Righteousness'),
    (select id from sets where short_name = 'dtk'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tapestry of the Ages'),
    (select id from sets where short_name = 'dtk'),
    '246',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dromoka Warrior'),
    (select id from sets where short_name = 'dtk'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ire Shaman'),
    (select id from sets where short_name = 'dtk'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sarkhan Unbroken'),
    (select id from sets where short_name = 'dtk'),
    '230',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Monastery Loremaster'),
    (select id from sets where short_name = 'dtk'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Roast'),
    (select id from sets where short_name = 'dtk'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragonlord Kolaghan'),
    (select id from sets where short_name = 'dtk'),
    '218',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Arashin Foremost'),
    (select id from sets where short_name = 'dtk'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Carp'),
    (select id from sets where short_name = 'dtk'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silumgar Spell-Eater'),
    (select id from sets where short_name = 'dtk'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deadly Wanderings'),
    (select id from sets where short_name = 'dtk'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'dtk'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'dtk'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necromaster Dragon'),
    (select id from sets where short_name = 'dtk'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corpseweft'),
    (select id from sets where short_name = 'dtk'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = 'dtk'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kolaghan Forerunners'),
    (select id from sets where short_name = 'dtk'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Icefall Regent'),
    (select id from sets where short_name = 'dtk'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dromoka Captain'),
    (select id from sets where short_name = 'dtk'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragonlord Silumgar'),
    (select id from sets where short_name = 'dtk'),
    '220',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Taigam''s Strike'),
    (select id from sets where short_name = 'dtk'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scion of Ugin'),
    (select id from sets where short_name = 'dtk'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silumgar Monument'),
    (select id from sets where short_name = 'dtk'),
    '243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Berserker'),
    (select id from sets where short_name = 'dtk'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Learn from the Past'),
    (select id from sets where short_name = 'dtk'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glint'),
    (select id from sets where short_name = 'dtk'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Revealing Wind'),
    (select id from sets where short_name = 'dtk'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myth Realized'),
    (select id from sets where short_name = 'dtk'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sibsig Icebreakers'),
    (select id from sets where short_name = 'dtk'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormrider Rig'),
    (select id from sets where short_name = 'dtk'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ainok Artillerist'),
    (select id from sets where short_name = 'dtk'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Virulent Plague'),
    (select id from sets where short_name = 'dtk'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Minister of Pain'),
    (select id from sets where short_name = 'dtk'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Student of Ojutai'),
    (select id from sets where short_name = 'dtk'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Void Squall'),
    (select id from sets where short_name = 'dtk'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Explosive Vegetation'),
    (select id from sets where short_name = 'dtk'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blessed Reincarnation'),
    (select id from sets where short_name = 'dtk'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Salt Road Quartermasters'),
    (select id from sets where short_name = 'dtk'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silumgar Butcher'),
    (select id from sets where short_name = 'dtk'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Youthful Scholar'),
    (select id from sets where short_name = 'dtk'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clone Legion'),
    (select id from sets where short_name = 'dtk'),
    '48',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pinion Feast'),
    (select id from sets where short_name = 'dtk'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sarkhan''s Rage'),
    (select id from sets where short_name = 'dtk'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Narset Transcendent'),
    (select id from sets where short_name = 'dtk'),
    '225',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vandalize'),
    (select id from sets where short_name = 'dtk'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Sunstriker'),
    (select id from sets where short_name = 'dtk'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = 'dtk'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood-Chin Fanatic'),
    (select id from sets where short_name = 'dtk'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dutiful Attendant'),
    (select id from sets where short_name = 'dtk'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gurmag Drowner'),
    (select id from sets where short_name = 'dtk'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Fodder'),
    (select id from sets where short_name = 'dtk'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Echoes of the Kin Tree'),
    (select id from sets where short_name = 'dtk'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragonlord''s Prerogative'),
    (select id from sets where short_name = 'dtk'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dtk'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dirgur Nemesis'),
    (select id from sets where short_name = 'dtk'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enduring Victory'),
    (select id from sets where short_name = 'dtk'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anafenza, Kin-Tree Spirit'),
    (select id from sets where short_name = 'dtk'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Champion of Arashin'),
    (select id from sets where short_name = 'dtk'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Salt Road Ambushers'),
    (select id from sets where short_name = 'dtk'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Impact Tremors'),
    (select id from sets where short_name = 'dtk'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ojutai Interceptor'),
    (select id from sets where short_name = 'dtk'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Descent of the Dragons'),
    (select id from sets where short_name = 'dtk'),
    '133',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ambuscade Shaman'),
    (select id from sets where short_name = 'dtk'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Twin Bolt'),
    (select id from sets where short_name = 'dtk'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dtk'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Whisperer'),
    (select id from sets where short_name = 'dtk'),
    '137',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Great Teacher''s Decree'),
    (select id from sets where short_name = 'dtk'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enduring Scalelord'),
    (select id from sets where short_name = 'dtk'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Herdchaser Dragon'),
    (select id from sets where short_name = 'dtk'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'dtk'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Defeat'),
    (select id from sets where short_name = 'dtk'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kolaghan Monument'),
    (select id from sets where short_name = 'dtk'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Atarka Monument'),
    (select id from sets where short_name = 'dtk'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Qarsi Deceiver'),
    (select id from sets where short_name = 'dtk'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sabertooth Outrider'),
    (select id from sets where short_name = 'dtk'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Display of Dominance'),
    (select id from sets where short_name = 'dtk'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ojutai''s Summons'),
    (select id from sets where short_name = 'dtk'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'dtk'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon-Scarred Bear'),
    (select id from sets where short_name = 'dtk'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silkwrap'),
    (select id from sets where short_name = 'dtk'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savage Ventmaw'),
    (select id from sets where short_name = 'dtk'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silumgar Sorcerer'),
    (select id from sets where short_name = 'dtk'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Screamreach Brawler'),
    (select id from sets where short_name = 'dtk'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Draconic Roar'),
    (select id from sets where short_name = 'dtk'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dtk'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormwing Dragon'),
    (select id from sets where short_name = 'dtk'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sidisi, Undead Vizier'),
    (select id from sets where short_name = 'dtk'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scaleguard Sentinels'),
    (select id from sets where short_name = 'dtk'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anticipate'),
    (select id from sets where short_name = 'dtk'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ukud Cobra'),
    (select id from sets where short_name = 'dtk'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Conifer Strider'),
    (select id from sets where short_name = 'dtk'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lose Calm'),
    (select id from sets where short_name = 'dtk'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illusory Gains'),
    (select id from sets where short_name = 'dtk'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reckless Imp'),
    (select id from sets where short_name = 'dtk'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathmist Raptor'),
    (select id from sets where short_name = 'dtk'),
    '180',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dragon''s Eye Sentry'),
    (select id from sets where short_name = 'dtk'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathbringer Regent'),
    (select id from sets where short_name = 'dtk'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'dtk'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Rush'),
    (select id from sets where short_name = 'dtk'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avatar of the Resolute'),
    (select id from sets where short_name = 'dtk'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kolaghan''s Command'),
    (select id from sets where short_name = 'dtk'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stampeding Elk Herd'),
    (select id from sets where short_name = 'dtk'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lurking Arynx'),
    (select id from sets where short_name = 'dtk'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Assault Formation'),
    (select id from sets where short_name = 'dtk'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Artful Maneuver'),
    (select id from sets where short_name = 'dtk'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foul-Tongue Shriek'),
    (select id from sets where short_name = 'dtk'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kindled Fury'),
    (select id from sets where short_name = 'dtk'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rending Volley'),
    (select id from sets where short_name = 'dtk'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sandsteppe Scavenger'),
    (select id from sets where short_name = 'dtk'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pitiless Horde'),
    (select id from sets where short_name = 'dtk'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keeper of the Lens'),
    (select id from sets where short_name = 'dtk'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Profaner of the Dead'),
    (select id from sets where short_name = 'dtk'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skywise Teachings'),
    (select id from sets where short_name = 'dtk'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hidden Dragonslayer'),
    (select id from sets where short_name = 'dtk'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death Wind'),
    (select id from sets where short_name = 'dtk'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tread Upon'),
    (select id from sets where short_name = 'dtk'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hand of Silumgar'),
    (select id from sets where short_name = 'dtk'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Herald of Dromoka'),
    (select id from sets where short_name = 'dtk'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sandcrafter Mage'),
    (select id from sets where short_name = 'dtk'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kolaghan Stormsinger'),
    (select id from sets where short_name = 'dtk'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reduce in Stature'),
    (select id from sets where short_name = 'dtk'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coat with Venom'),
    (select id from sets where short_name = 'dtk'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ojutai Monument'),
    (select id from sets where short_name = 'dtk'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Lore'),
    (select id from sets where short_name = 'dtk'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Atarka''s Command'),
    (select id from sets where short_name = 'dtk'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonlord Dromoka'),
    (select id from sets where short_name = 'dtk'),
    '217',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'dtk'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Secure the Wastes'),
    (select id from sets where short_name = 'dtk'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tail Slash'),
    (select id from sets where short_name = 'dtk'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruthless Deathfang'),
    (select id from sets where short_name = 'dtk'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shambling Goblin'),
    (select id from sets where short_name = 'dtk'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marang River Skeleton'),
    (select id from sets where short_name = 'dtk'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spidersilk Net'),
    (select id from sets where short_name = 'dtk'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Risen Executioner'),
    (select id from sets where short_name = 'dtk'),
    '116',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Updraft Elemental'),
    (select id from sets where short_name = 'dtk'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'dtk'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gleam of Authority'),
    (select id from sets where short_name = 'dtk'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sandstorm Charger'),
    (select id from sets where short_name = 'dtk'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Vision'),
    (select id from sets where short_name = 'dtk'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Belltoll Dragon'),
    (select id from sets where short_name = 'dtk'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Haven of the Spirit Dragon'),
    (select id from sets where short_name = 'dtk'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fate Forgotten'),
    (select id from sets where short_name = 'dtk'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seismic Rupture'),
    (select id from sets where short_name = 'dtk'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Graceblade Artisan'),
    (select id from sets where short_name = 'dtk'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Contradict'),
    (select id from sets where short_name = 'dtk'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sprinting Warbrute'),
    (select id from sets where short_name = 'dtk'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flatten'),
    (select id from sets where short_name = 'dtk'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunderbreak Regent'),
    (select id from sets where short_name = 'dtk'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dromoka''s Gift'),
    (select id from sets where short_name = 'dtk'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shaman of Forgotten Ways'),
    (select id from sets where short_name = 'dtk'),
    '204',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dragonlord Ojutai'),
    (select id from sets where short_name = 'dtk'),
    '219',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Glaring Aegis'),
    (select id from sets where short_name = 'dtk'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aerie Bowmasters'),
    (select id from sets where short_name = 'dtk'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scale Blessing'),
    (select id from sets where short_name = 'dtk'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stormcrag Elemental'),
    (select id from sets where short_name = 'dtk'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Elders'),
    (select id from sets where short_name = 'dtk'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kolaghan Aspirant'),
    (select id from sets where short_name = 'dtk'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Acid-Spewer Dragon'),
    (select id from sets where short_name = 'dtk'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boltwing Marauder'),
    (select id from sets where short_name = 'dtk'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ainok Survivalist'),
    (select id from sets where short_name = 'dtk'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Foul-Tongue Invocation'),
    (select id from sets where short_name = 'dtk'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunbringer''s Touch'),
    (select id from sets where short_name = 'dtk'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Hunter'),
    (select id from sets where short_name = 'dtk'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elusive Spellfist'),
    (select id from sets where short_name = 'dtk'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ojutai''s Breath'),
    (select id from sets where short_name = 'dtk'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sheltered Aerie'),
    (select id from sets where short_name = 'dtk'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Atarka Efreet'),
    (select id from sets where short_name = 'dtk'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hedonist''s Trove'),
    (select id from sets where short_name = 'dtk'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vulturous Aven'),
    (select id from sets where short_name = 'dtk'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Surrak, the Hunt Caller'),
    (select id from sets where short_name = 'dtk'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ultimate Price'),
    (select id from sets where short_name = 'dtk'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Butcher''s Glee'),
    (select id from sets where short_name = 'dtk'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sight of the Scalelords'),
    (select id from sets where short_name = 'dtk'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gravepurge'),
    (select id from sets where short_name = 'dtk'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Tactician'),
    (select id from sets where short_name = 'dtk'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silumgar''s Command'),
    (select id from sets where short_name = 'dtk'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zurgo Bellstriker'),
    (select id from sets where short_name = 'dtk'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Encase in Ice'),
    (select id from sets where short_name = 'dtk'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tormenting Voice'),
    (select id from sets where short_name = 'dtk'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Den Protector'),
    (select id from sets where short_name = 'dtk'),
    '181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cunning Breezedancer'),
    (select id from sets where short_name = 'dtk'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Center Soul'),
    (select id from sets where short_name = 'dtk'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Radiant Purge'),
    (select id from sets where short_name = 'dtk'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunscorch Regent'),
    (select id from sets where short_name = 'dtk'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crater Elemental'),
    (select id from sets where short_name = 'dtk'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dance of the Skywise'),
    (select id from sets where short_name = 'dtk'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inspiring Call'),
    (select id from sets where short_name = 'dtk'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancestral Statue'),
    (select id from sets where short_name = 'dtk'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragonloft Idol'),
    (select id from sets where short_name = 'dtk'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ojutai''s Command'),
    (select id from sets where short_name = 'dtk'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dtk'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gudul Lurker'),
    (select id from sets where short_name = 'dtk'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Atarka Beastbreaker'),
    (select id from sets where short_name = 'dtk'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Servant of the Scale'),
    (select id from sets where short_name = 'dtk'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harbinger of the Hunt'),
    (select id from sets where short_name = 'dtk'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Misthoof Kirin'),
    (select id from sets where short_name = 'dtk'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'dtk'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragonlord Atarka'),
    (select id from sets where short_name = 'dtk'),
    '216',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kolaghan Skirmisher'),
    (select id from sets where short_name = 'dtk'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Tempest'),
    (select id from sets where short_name = 'dtk'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Collected Company'),
    (select id from sets where short_name = 'dtk'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sidisi''s Faithful'),
    (select id from sets where short_name = 'dtk'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hardened Berserker'),
    (select id from sets where short_name = 'dtk'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ojutai Exemplars'),
    (select id from sets where short_name = 'dtk'),
    '27',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mystic Meditation'),
    (select id from sets where short_name = 'dtk'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zephyr Scribe'),
    (select id from sets where short_name = 'dtk'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dromoka Monument'),
    (select id from sets where short_name = 'dtk'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Atarka Pummeler'),
    (select id from sets where short_name = 'dtk'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dtk'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arashin Sovereign'),
    (select id from sets where short_name = 'dtk'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guardian Shield-Bearer'),
    (select id from sets where short_name = 'dtk'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Segmented Krotiq'),
    (select id from sets where short_name = 'dtk'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sight Beyond Sight'),
    (select id from sets where short_name = 'dtk'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dromoka''s Command'),
    (select id from sets where short_name = 'dtk'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foul Renewal'),
    (select id from sets where short_name = 'dtk'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stratus Dancer'),
    (select id from sets where short_name = 'dtk'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vial of Dragonfire'),
    (select id from sets where short_name = 'dtk'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magmatic Chasm'),
    (select id from sets where short_name = 'dtk'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wandering Tombshell'),
    (select id from sets where short_name = 'dtk'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shorecrasher Elemental'),
    (select id from sets where short_name = 'dtk'),
    '73',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blood-Chin Rager'),
    (select id from sets where short_name = 'dtk'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'dtk'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Qal Sisma Behemoth'),
    (select id from sets where short_name = 'dtk'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Custodian of the Trove'),
    (select id from sets where short_name = 'dtk'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'dtk'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silumgar Assassin'),
    (select id from sets where short_name = 'dtk'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Resupply'),
    (select id from sets where short_name = 'dtk'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warbringer'),
    (select id from sets where short_name = 'dtk'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Berserkers'' Onslaught'),
    (select id from sets where short_name = 'dtk'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Summit Prowler'),
    (select id from sets where short_name = 'dtk'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Colossodon Yearling'),
    (select id from sets where short_name = 'dtk'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Epic Confrontation'),
    (select id from sets where short_name = 'dtk'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swift Warkite'),
    (select id from sets where short_name = 'dtk'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Damnable Pact'),
    (select id from sets where short_name = 'dtk'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Self-Inflicted Wound'),
    (select id from sets where short_name = 'dtk'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'dtk'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirror Mockery'),
    (select id from sets where short_name = 'dtk'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'dtk'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sarkhan''s Triumph'),
    (select id from sets where short_name = 'dtk'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Palace Familiar'),
    (select id from sets where short_name = 'dtk'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orator of Ojutai'),
    (select id from sets where short_name = 'dtk'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dromoka Dunecaster'),
    (select id from sets where short_name = 'dtk'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shieldhide Dragon'),
    (select id from sets where short_name = 'dtk'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Profound Journey'),
    (select id from sets where short_name = 'dtk'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marsh Hulk'),
    (select id from sets where short_name = 'dtk'),
    '109',
    'common'
) 
 on conflict do nothing;
