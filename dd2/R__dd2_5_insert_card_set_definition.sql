insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Quicksilver Dragon'),
    (select id from sets where short_name = 'dd2'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra Nalaar'),
    (select id from sets where short_name = 'dd2'),
    '34',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bottle Gnomes'),
    (select id from sets where short_name = 'dd2'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyre Charger'),
    (select id from sets where short_name = 'dd2'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chandra Nalaar'),
    (select id from sets where short_name = 'dd2'),
    '34★',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Brine Elemental'),
    (select id from sets where short_name = 'dd2'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cone of Flame'),
    (select id from sets where short_name = 'dd2'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Furnace Whelp'),
    (select id from sets where short_name = 'dd2'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jace Beleren'),
    (select id from sets where short_name = 'dd2'),
    '1★',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fireslinger'),
    (select id from sets where short_name = 'dd2'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Firebolt'),
    (select id from sets where short_name = 'dd2'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slith Firewalker'),
    (select id from sets where short_name = 'dd2'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'dd2'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Errant Ephemeron'),
    (select id from sets where short_name = 'dd2'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'dd2'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ophidian'),
    (select id from sets where short_name = 'dd2'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dd2'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Condescend'),
    (select id from sets where short_name = 'dd2'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = 'dd2'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oxidda Golem'),
    (select id from sets where short_name = 'dd2'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakdos Pit Dragon'),
    (select id from sets where short_name = 'dd2'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Repulse'),
    (select id from sets where short_name = 'dd2'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guile'),
    (select id from sets where short_name = 'dd2'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flame Javelin'),
    (select id from sets where short_name = 'dd2'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Man-o''-War'),
    (select id from sets where short_name = 'dd2'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fledgling Mawcor'),
    (select id from sets where short_name = 'dd2'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keldon Megaliths'),
    (select id from sets where short_name = 'dd2'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mulldrifter'),
    (select id from sets where short_name = 'dd2'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'dd2'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flamekin Brawler'),
    (select id from sets where short_name = 'dd2'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spire Golem'),
    (select id from sets where short_name = 'dd2'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aethersnipe'),
    (select id from sets where short_name = 'dd2'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Willbender'),
    (select id from sets where short_name = 'dd2'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chartooth Cougar'),
    (select id from sets where short_name = 'dd2'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soulbright Flamekin'),
    (select id from sets where short_name = 'dd2'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'dd2'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Martyr of Frost'),
    (select id from sets where short_name = 'dd2'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = 'dd2'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'dd2'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terrain Generator'),
    (select id from sets where short_name = 'dd2'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dd2'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riftwing Cloudskate'),
    (select id from sets where short_name = 'dd2'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inner-Flame Acolyte'),
    (select id from sets where short_name = 'dd2'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ingot Chewer'),
    (select id from sets where short_name = 'dd2'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fathom Seer'),
    (select id from sets where short_name = 'dd2'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fact or Fiction'),
    (select id from sets where short_name = 'dd2'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magma Jet'),
    (select id from sets where short_name = 'dd2'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hostility'),
    (select id from sets where short_name = 'dd2'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fireblast'),
    (select id from sets where short_name = 'dd2'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seal of Fire'),
    (select id from sets where short_name = 'dd2'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu'),
    (select id from sets where short_name = 'dd2'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daze'),
    (select id from sets where short_name = 'dd2'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace Beleren'),
    (select id from sets where short_name = 'dd2'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'dd2'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voidmage Apprentice'),
    (select id from sets where short_name = 'dd2'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Deceit'),
    (select id from sets where short_name = 'dd2'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dd2'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Waterspout Djinn'),
    (select id from sets where short_name = 'dd2'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancestral Vision'),
    (select id from sets where short_name = 'dd2'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flamewave Invoker'),
    (select id from sets where short_name = 'dd2'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dd2'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gush'),
    (select id from sets where short_name = 'dd2'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Stone'),
    (select id from sets where short_name = 'dd2'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demonfire'),
    (select id from sets where short_name = 'dd2'),
    '57',
    'rare'
) 
 on conflict do nothing;
