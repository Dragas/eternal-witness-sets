insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Dark Depths'),
    (select id from sets where short_name = '2xm'),
    '314',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Graven Cairns'),
    (select id from sets where short_name = '2xm'),
    '320',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skirsdag High Priest'),
    (select id from sets where short_name = '2xm'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woodland Champion'),
    (select id from sets where short_name = '2xm'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Executioner''s Capsule'),
    (select id from sets where short_name = '2xm'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greater Good'),
    (select id from sets where short_name = '2xm'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silumgar Scavenger'),
    (select id from sets where short_name = '2xm'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beacon of Unrest'),
    (select id from sets where short_name = '2xm'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glaze Fiend'),
    (select id from sets where short_name = '2xm'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conclave Naturalists'),
    (select id from sets where short_name = '2xm'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyrite Spellbomb'),
    (select id from sets where short_name = '2xm'),
    '283',
    'common'
) ,
(
    (select id from mtgcard where name = 'O-Naginata'),
    (select id from sets where short_name = '2xm'),
    '278',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = '2xm'),
    '329',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sword of Feast and Famine'),
    (select id from sets where short_name = '2xm'),
    '364',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Salivating Gremlins'),
    (select id from sets where short_name = '2xm'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blightsteel Colossus'),
    (select id from sets where short_name = '2xm'),
    '357',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Galvanic Blast'),
    (select id from sets where short_name = '2xm'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boon Reflection'),
    (select id from sets where short_name = '2xm'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chrome Mox'),
    (select id from sets where short_name = '2xm'),
    '358',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Alabaster Mage'),
    (select id from sets where short_name = '2xm'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stonehewer Giant'),
    (select id from sets where short_name = '2xm'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enlarge'),
    (select id from sets where short_name = '2xm'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chromatic Star'),
    (select id from sets where short_name = '2xm'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Godo, Bandit Warlord'),
    (select id from sets where short_name = '2xm'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sen Triplets'),
    (select id from sets where short_name = '2xm'),
    '218',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cathodion'),
    (select id from sets where short_name = '2xm'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Abunas'),
    (select id from sets where short_name = '2xm'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '2xm'),
    '381',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grand Architect'),
    (select id from sets where short_name = '2xm'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archangel of Thune'),
    (select id from sets where short_name = '2xm'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ulvenwald Mysteries'),
    (select id from sets where short_name = '2xm'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hanna, Ship''s Navigator'),
    (select id from sets where short_name = '2xm'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crop Rotation'),
    (select id from sets where short_name = '2xm'),
    '349',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blightsteel Colossus'),
    (select id from sets where short_name = '2xm'),
    '235',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fire-Lit Thicket'),
    (select id from sets where short_name = '2xm'),
    '317',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Weapon Surge'),
    (select id from sets where short_name = '2xm'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Veteran Explorer'),
    (select id from sets where short_name = '2xm'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yavimaya''s Embrace'),
    (select id from sets where short_name = '2xm'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glint-Sleeve Artisan'),
    (select id from sets where short_name = '2xm'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exploration'),
    (select id from sets where short_name = '2xm'),
    '351',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword of Body and Mind'),
    (select id from sets where short_name = '2xm'),
    '295',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Flayer Husk'),
    (select id from sets where short_name = '2xm'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doubling Season'),
    (select id from sets where short_name = '2xm'),
    '350',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Breya, Etherium Shaper'),
    (select id from sets where short_name = '2xm'),
    '192',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Glassdust Hulk'),
    (select id from sets where short_name = '2xm'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rugged Prairie'),
    (select id from sets where short_name = '2xm'),
    '325',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kuldotha Flamefiend'),
    (select id from sets where short_name = '2xm'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sword of Fire and Ice'),
    (select id from sets where short_name = '2xm'),
    '365',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Magus of the Will'),
    (select id from sets where short_name = '2xm'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abrade'),
    (select id from sets where short_name = '2xm'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battle-Rattle Shaman'),
    (select id from sets where short_name = '2xm'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brudiclad, Telchor Engineer'),
    (select id from sets where short_name = '2xm'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword of Fire and Ice'),
    (select id from sets where short_name = '2xm'),
    '297',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thespian''s Stage'),
    (select id from sets where short_name = '2xm'),
    '327',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Welding Jar'),
    (select id from sets where short_name = '2xm'),
    '307',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blade Splicer'),
    (select id from sets where short_name = '2xm'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Remember the Fallen'),
    (select id from sets where short_name = '2xm'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravenous Intruder'),
    (select id from sets where short_name = '2xm'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pentad Prism'),
    (select id from sets where short_name = '2xm'),
    '281',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Endless Atlas'),
    (select id from sets where short_name = '2xm'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Isochron Scepter'),
    (select id from sets where short_name = '2xm'),
    '264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Batterskull'),
    (select id from sets where short_name = '2xm'),
    '356',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cloudreader Sphinx'),
    (select id from sets where short_name = '2xm'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wound Reflection'),
    (select id from sets where short_name = '2xm'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duplicant'),
    (select id from sets where short_name = '2xm'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thraben Inspector'),
    (select id from sets where short_name = '2xm'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karrthus, Tyrant of Jund'),
    (select id from sets where short_name = '2xm'),
    '205',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Master of Etherium'),
    (select id from sets where short_name = '2xm'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whisperer of the Wilds'),
    (select id from sets where short_name = '2xm'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Defiant Salvager'),
    (select id from sets where short_name = '2xm'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyrewild Shaman'),
    (select id from sets where short_name = '2xm'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trinisphere'),
    (select id from sets where short_name = '2xm'),
    '303',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Myr Battlesphere'),
    (select id from sets where short_name = '2xm'),
    '276',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Council''s Judgment'),
    (select id from sets where short_name = '2xm'),
    '336',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conjurer''s Closet'),
    (select id from sets where short_name = '2xm'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Metallic Rebuke'),
    (select id from sets where short_name = '2xm'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wooded Bastion'),
    (select id from sets where short_name = '2xm'),
    '332',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunforger'),
    (select id from sets where short_name = '2xm'),
    '293',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword of Light and Shadow'),
    (select id from sets where short_name = '2xm'),
    '366',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bloodbriar'),
    (select id from sets where short_name = '2xm'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blinkmoth Nexus'),
    (select id from sets where short_name = '2xm'),
    '311',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Guide'),
    (select id from sets where short_name = '2xm'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skinwing'),
    (select id from sets where short_name = '2xm'),
    '288',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meddling Mage'),
    (select id from sets where short_name = '2xm'),
    '355',
    'rare'
) ,
(
    (select id from mtgcard where name = 'High Market'),
    (select id from sets where short_name = '2xm'),
    '321',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thopter Foundry'),
    (select id from sets where short_name = '2xm'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sneak Attack'),
    (select id from sets where short_name = '2xm'),
    '145',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ion Storm'),
    (select id from sets where short_name = '2xm'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Argivian Restoration'),
    (select id from sets where short_name = '2xm'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Gaveleer'),
    (select id from sets where short_name = '2xm'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crushing Vines'),
    (select id from sets where short_name = '2xm'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Gate'),
    (select id from sets where short_name = '2xm'),
    '324',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noble Hierarch'),
    (select id from sets where short_name = '2xm'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unlicensed Disintegration'),
    (select id from sets where short_name = '2xm'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pongify'),
    (select id from sets where short_name = '2xm'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gleaming Barrier'),
    (select id from sets where short_name = '2xm'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Engineered Explosives'),
    (select id from sets where short_name = '2xm'),
    '252',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fulminator Mage'),
    (select id from sets where short_name = '2xm'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darksteel Forge'),
    (select id from sets where short_name = '2xm'),
    '248',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Crop Rotation'),
    (select id from sets where short_name = '2xm'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terastodon'),
    (select id from sets where short_name = '2xm'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Divest'),
    (select id from sets where short_name = '2xm'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basilisk Collar'),
    (select id from sets where short_name = '2xm'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Bauble'),
    (select id from sets where short_name = '2xm'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thirst for Knowledge'),
    (select id from sets where short_name = '2xm'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sword of War and Peace'),
    (select id from sets where short_name = '2xm'),
    '300',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Master Transmuter'),
    (select id from sets where short_name = '2xm'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cast Down'),
    (select id from sets where short_name = '2xm'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fortify'),
    (select id from sets where short_name = '2xm'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glimmervoid'),
    (select id from sets where short_name = '2xm'),
    '319',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deepglow Skate'),
    (select id from sets where short_name = '2xm'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghor-Clan Rampager'),
    (select id from sets where short_name = '2xm'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karn Liberated'),
    (select id from sets where short_name = '2xm'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cragganwick Cremator'),
    (select id from sets where short_name = '2xm'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Peace Strider'),
    (select id from sets where short_name = '2xm'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disciple of the Vault'),
    (select id from sets where short_name = '2xm'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stoneforge Mystic'),
    (select id from sets where short_name = '2xm'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darksteel Axe'),
    (select id from sets where short_name = '2xm'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Charm'),
    (select id from sets where short_name = '2xm'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Crypt'),
    (select id from sets where short_name = '2xm'),
    '270',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gelatinous Genesis'),
    (select id from sets where short_name = '2xm'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jhoira, Weatherlight Captain'),
    (select id from sets where short_name = '2xm'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sift'),
    (select id from sets where short_name = '2xm'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Sphinx'),
    (select id from sets where short_name = '2xm'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Walking Ballista'),
    (select id from sets where short_name = '2xm'),
    '306',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cascade Bluffs'),
    (select id from sets where short_name = '2xm'),
    '313',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twilight Mire'),
    (select id from sets where short_name = '2xm'),
    '328',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = '2xm'),
    '330',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iron Bully'),
    (select id from sets where short_name = '2xm'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = '2xm'),
    '370',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mox Opal'),
    (select id from sets where short_name = '2xm'),
    '362',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Culling Dais'),
    (select id from sets where short_name = '2xm'),
    '246',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Expedition Map'),
    (select id from sets where short_name = '2xm'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death''s Shadow'),
    (select id from sets where short_name = '2xm'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mox Opal'),
    (select id from sets where short_name = '2xm'),
    '275',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vengevine'),
    (select id from sets where short_name = '2xm'),
    '185',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Reclamation Sage'),
    (select id from sets where short_name = '2xm'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coretapper'),
    (select id from sets where short_name = '2xm'),
    '244',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Falkenrath Aristocrat'),
    (select id from sets where short_name = '2xm'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Braids, Conjurer Adept'),
    (select id from sets where short_name = '2xm'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shamanic Revelation'),
    (select id from sets where short_name = '2xm'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Costly Plunder'),
    (select id from sets where short_name = '2xm'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = '2xm'),
    '323',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sword of the Meek'),
    (select id from sets where short_name = '2xm'),
    '299',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skullmulcher'),
    (select id from sets where short_name = '2xm'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eager Construct'),
    (select id from sets where short_name = '2xm'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heartless Pillage'),
    (select id from sets where short_name = '2xm'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sneak Attack'),
    (select id from sets where short_name = '2xm'),
    '348',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sword of Body and Mind'),
    (select id from sets where short_name = '2xm'),
    '363',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Angel of the Dawn'),
    (select id from sets where short_name = '2xm'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurmcoil Engine'),
    (select id from sets where short_name = '2xm'),
    '308',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chord of Calling'),
    (select id from sets where short_name = '2xm'),
    '384',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flickerwisp'),
    (select id from sets where short_name = '2xm'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chatter of the Squirrel'),
    (select id from sets where short_name = '2xm'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iron League Steed'),
    (select id from sets where short_name = '2xm'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Guide'),
    (select id from sets where short_name = '2xm'),
    '347',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sickleslicer'),
    (select id from sets where short_name = '2xm'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '2xm'),
    '379',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = '2xm'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Pulse'),
    (select id from sets where short_name = '2xm'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strength of Arms'),
    (select id from sets where short_name = '2xm'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancestral Blade'),
    (select id from sets where short_name = '2xm'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Revoker'),
    (select id from sets where short_name = '2xm'),
    '282',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rolling Earthquake'),
    (select id from sets where short_name = '2xm'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Academy Ruins'),
    (select id from sets where short_name = '2xm'),
    '369',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Metalspinner''s Puzzleknot'),
    (select id from sets where short_name = '2xm'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crib Swap'),
    (select id from sets where short_name = '2xm'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clone Shell'),
    (select id from sets where short_name = '2xm'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Confidant'),
    (select id from sets where short_name = '2xm'),
    '342',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rapacious Dragon'),
    (select id from sets where short_name = '2xm'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ichor Wellspring'),
    (select id from sets where short_name = '2xm'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Crypt'),
    (select id from sets where short_name = '2xm'),
    '361',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fatal Push'),
    (select id from sets where short_name = '2xm'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hinder'),
    (select id from sets where short_name = '2xm'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Parasitic Strix'),
    (select id from sets where short_name = '2xm'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riku of Two Reflections'),
    (select id from sets where short_name = '2xm'),
    '214',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lightning Greaves'),
    (select id from sets where short_name = '2xm'),
    '360',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fetid Heath'),
    (select id from sets where short_name = '2xm'),
    '316',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Salvage Titan'),
    (select id from sets where short_name = '2xm'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selesnya Guildmage'),
    (select id from sets where short_name = '2xm'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Stirrings'),
    (select id from sets where short_name = '2xm'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Esperzoa'),
    (select id from sets where short_name = '2xm'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vulshok Gauntlets'),
    (select id from sets where short_name = '2xm'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Toxic Deluge'),
    (select id from sets where short_name = '2xm'),
    '345',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kazuul''s Toll Collector'),
    (select id from sets where short_name = '2xm'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Surge Node'),
    (select id from sets where short_name = '2xm'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tumble Magnet'),
    (select id from sets where short_name = '2xm'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trash for Treasure'),
    (select id from sets where short_name = '2xm'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodshot Trainee'),
    (select id from sets where short_name = '2xm'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maze of Ith'),
    (select id from sets where short_name = '2xm'),
    '322',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myrsmith'),
    (select id from sets where short_name = '2xm'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Painsmith'),
    (select id from sets where short_name = '2xm'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magnifying Glass'),
    (select id from sets where short_name = '2xm'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jhoira''s Familiar'),
    (select id from sets where short_name = '2xm'),
    '265',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Revoke Existence'),
    (select id from sets where short_name = '2xm'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vexing Shusher'),
    (select id from sets where short_name = '2xm'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Valor in Akros'),
    (select id from sets where short_name = '2xm'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Myr Retriever'),
    (select id from sets where short_name = '2xm'),
    '277',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathreap Ritual'),
    (select id from sets where short_name = '2xm'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Noble Hierarch'),
    (select id from sets where short_name = '2xm'),
    '352',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balduvian Rage'),
    (select id from sets where short_name = '2xm'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Reflection'),
    (select id from sets where short_name = '2xm'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '2xm'),
    '380',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctum Spirit'),
    (select id from sets where short_name = '2xm'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doubling Season'),
    (select id from sets where short_name = '2xm'),
    '164',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dismantle'),
    (select id from sets where short_name = '2xm'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heat Shimmer'),
    (select id from sets where short_name = '2xm'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaalia of the Vast'),
    (select id from sets where short_name = '2xm'),
    '354',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Arixmethes, Slumbering Isle'),
    (select id from sets where short_name = '2xm'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temur Battle Rage'),
    (select id from sets where short_name = '2xm'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Puresteel Paladin'),
    (select id from sets where short_name = '2xm'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disciple of Bolas'),
    (select id from sets where short_name = '2xm'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Expedition Map'),
    (select id from sets where short_name = '2xm'),
    '359',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savageborn Hydra'),
    (select id from sets where short_name = '2xm'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Valorous Stance'),
    (select id from sets where short_name = '2xm'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heartbeat of Spring'),
    (select id from sets where short_name = '2xm'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '2xm'),
    '377',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reshape'),
    (select id from sets where short_name = '2xm'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bosh, Iron Golem'),
    (select id from sets where short_name = '2xm'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Axe'),
    (select id from sets where short_name = '2xm'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '2xm'),
    '375',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunken Ruins'),
    (select id from sets where short_name = '2xm'),
    '326',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Geth, Lord of the Vault'),
    (select id from sets where short_name = '2xm'),
    '94',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = '2xm'),
    '371',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword of War and Peace'),
    (select id from sets where short_name = '2xm'),
    '367',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '2xm'),
    '378',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brimstone Volley'),
    (select id from sets where short_name = '2xm'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skinbrand Goblin'),
    (select id from sets where short_name = '2xm'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tempered Steel'),
    (select id from sets where short_name = '2xm'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thoughtseize'),
    (select id from sets where short_name = '2xm'),
    '344',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Land Tax'),
    (select id from sets where short_name = '2xm'),
    '20',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dread Return'),
    (select id from sets where short_name = '2xm'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karn Liberated'),
    (select id from sets where short_name = '2xm'),
    '333',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vampire Hexmage'),
    (select id from sets where short_name = '2xm'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Might of the Masses'),
    (select id from sets where short_name = '2xm'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magus of the Abyss'),
    (select id from sets where short_name = '2xm'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voice of Resurgence'),
    (select id from sets where short_name = '2xm'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Everflowing Chalice'),
    (select id from sets where short_name = '2xm'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = '2xm'),
    '331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '2xm'),
    '374',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ash Barrens'),
    (select id from sets where short_name = '2xm'),
    '310',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inkwell Leviathan'),
    (select id from sets where short_name = '2xm'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avacyn, Angel of Hope'),
    (select id from sets where short_name = '2xm'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kaalia of the Vast'),
    (select id from sets where short_name = '2xm'),
    '204',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lux Cannon'),
    (select id from sets where short_name = '2xm'),
    '268',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sentinel of the Pearl Trident'),
    (select id from sets where short_name = '2xm'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doomed Necromancer'),
    (select id from sets where short_name = '2xm'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Baleful Strix'),
    (select id from sets where short_name = '2xm'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sundering Titan'),
    (select id from sets where short_name = '2xm'),
    '292',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Might'),
    (select id from sets where short_name = '2xm'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vedalken Infuser'),
    (select id from sets where short_name = '2xm'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Throne of Geth'),
    (select id from sets where short_name = '2xm'),
    '301',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Vandal'),
    (select id from sets where short_name = '2xm'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clear Shot'),
    (select id from sets where short_name = '2xm'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hidden Stockpile'),
    (select id from sets where short_name = '2xm'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = '2xm'),
    '383',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Academy Ruins'),
    (select id from sets where short_name = '2xm'),
    '309',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ad Nauseam'),
    (select id from sets where short_name = '2xm'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Accomplished Automaton'),
    (select id from sets where short_name = '2xm'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spellskite'),
    (select id from sets where short_name = '2xm'),
    '289',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Council''s Judgment'),
    (select id from sets where short_name = '2xm'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Topple the Statue'),
    (select id from sets where short_name = '2xm'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ethersworn Canonist'),
    (select id from sets where short_name = '2xm'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Progenitor Mimic'),
    (select id from sets where short_name = '2xm'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphinx Summoner'),
    (select id from sets where short_name = '2xm'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Masterwork of Ingenuity'),
    (select id from sets where short_name = '2xm'),
    '271',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sharuum the Hegemon'),
    (select id from sets where short_name = '2xm'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Confidant'),
    (select id from sets where short_name = '2xm'),
    '81',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tuktuk the Explorer'),
    (select id from sets where short_name = '2xm'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thoughtseize'),
    (select id from sets where short_name = '2xm'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cathartic Reunion'),
    (select id from sets where short_name = '2xm'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sword of Light and Shadow'),
    (select id from sets where short_name = '2xm'),
    '298',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rage Reflection'),
    (select id from sets where short_name = '2xm'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Batterskull'),
    (select id from sets where short_name = '2xm'),
    '234',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Crusader of Odric'),
    (select id from sets where short_name = '2xm'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter Engineer'),
    (select id from sets where short_name = '2xm'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rhys the Redeemed'),
    (select id from sets where short_name = '2xm'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fatal Push'),
    (select id from sets where short_name = '2xm'),
    '343',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Adaptive Automaton'),
    (select id from sets where short_name = '2xm'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vish Kal, Blood Arbiter'),
    (select id from sets where short_name = '2xm'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = '2xm'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctum Gargoyle'),
    (select id from sets where short_name = '2xm'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weapons Trainer'),
    (select id from sets where short_name = '2xm'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Echoes'),
    (select id from sets where short_name = '2xm'),
    '136',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Awakening Zone'),
    (select id from sets where short_name = '2xm'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Auriok Salvagers'),
    (select id from sets where short_name = '2xm'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chief of the Foundry'),
    (select id from sets where short_name = '2xm'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treasure Mage'),
    (select id from sets where short_name = '2xm'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '2xm'),
    '382',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steel Sabotage'),
    (select id from sets where short_name = '2xm'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '2xm'),
    '376',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swiftblade Vindicator'),
    (select id from sets where short_name = '2xm'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Time Sieve'),
    (select id from sets where short_name = '2xm'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flooded Grove'),
    (select id from sets where short_name = '2xm'),
    '318',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Geist of Saint Traft'),
    (select id from sets where short_name = '2xm'),
    '197',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = '2xm'),
    '372',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kuldotha Forgemaster'),
    (select id from sets where short_name = '2xm'),
    '266',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword of Feast and Famine'),
    (select id from sets where short_name = '2xm'),
    '296',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kozilek''s Predator'),
    (select id from sets where short_name = '2xm'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace, the Mind Sculptor'),
    (select id from sets where short_name = '2xm'),
    '56',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Skithiryx, the Blight Dragon'),
    (select id from sets where short_name = '2xm'),
    '107',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Metamorph'),
    (select id from sets where short_name = '2xm'),
    '341',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '2xm'),
    '373',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rush of Knowledge'),
    (select id from sets where short_name = '2xm'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ravenous Trap'),
    (select id from sets where short_name = '2xm'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exploration'),
    (select id from sets where short_name = '2xm'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ensnaring Bridge'),
    (select id from sets where short_name = '2xm'),
    '253',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Oblivion Stone'),
    (select id from sets where short_name = '2xm'),
    '279',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Toxic Deluge'),
    (select id from sets where short_name = '2xm'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Moon'),
    (select id from sets where short_name = '2xm'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Atraxa, Praetors'' Voice'),
    (select id from sets where short_name = '2xm'),
    '190',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blood Moon'),
    (select id from sets where short_name = '2xm'),
    '346',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Austere Command'),
    (select id from sets where short_name = '2xm'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oubliette'),
    (select id from sets where short_name = '2xm'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sculpting Steel'),
    (select id from sets where short_name = '2xm'),
    '286',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ratchet Bomb'),
    (select id from sets where short_name = '2xm'),
    '284',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fencing Ace'),
    (select id from sets where short_name = '2xm'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Metamorph'),
    (select id from sets where short_name = '2xm'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dire Fleet Hoarder'),
    (select id from sets where short_name = '2xm'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Scarab God'),
    (select id from sets where short_name = '2xm'),
    '216',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Frogify'),
    (select id from sets where short_name = '2xm'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Imperial Recruiter'),
    (select id from sets where short_name = '2xm'),
    '131',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thragtusk'),
    (select id from sets where short_name = '2xm'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Buried Ruin'),
    (select id from sets where short_name = '2xm'),
    '312',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Atraxa, Praetors'' Voice'),
    (select id from sets where short_name = '2xm'),
    '353',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chord of Calling'),
    (select id from sets where short_name = '2xm'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avacyn, Angel of Hope'),
    (select id from sets where short_name = '2xm'),
    '335',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Treasure Keeper'),
    (select id from sets where short_name = '2xm'),
    '302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Champion of Lambholt'),
    (select id from sets where short_name = '2xm'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darksteel Citadel'),
    (select id from sets where short_name = '2xm'),
    '315',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = '2xm'),
    '338',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Force of Will'),
    (select id from sets where short_name = '2xm'),
    '51',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Morkrut Banshee'),
    (select id from sets where short_name = '2xm'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meddling Mage'),
    (select id from sets where short_name = '2xm'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death-Hood Cobra'),
    (select id from sets where short_name = '2xm'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corridor Monitor'),
    (select id from sets where short_name = '2xm'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Invigorate'),
    (select id from sets where short_name = '2xm'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Apprentice Wizard'),
    (select id from sets where short_name = '2xm'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cranial Plating'),
    (select id from sets where short_name = '2xm'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thought Reflection'),
    (select id from sets where short_name = '2xm'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mesmeric Orb'),
    (select id from sets where short_name = '2xm'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faerie Mechanist'),
    (select id from sets where short_name = '2xm'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cogwork Assembler'),
    (select id from sets where short_name = '2xm'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Nexus'),
    (select id from sets where short_name = '2xm'),
    '206',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Open the Vaults'),
    (select id from sets where short_name = '2xm'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avenger of Zendikar'),
    (select id from sets where short_name = '2xm'),
    '152',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Golem Artisan'),
    (select id from sets where short_name = '2xm'),
    '258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Greaves'),
    (select id from sets where short_name = '2xm'),
    '267',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Well of Ideas'),
    (select id from sets where short_name = '2xm'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mazirek, Kraul Death Priest'),
    (select id from sets where short_name = '2xm'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kemba, Kha Regent'),
    (select id from sets where short_name = '2xm'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Path to Exile'),
    (select id from sets where short_name = '2xm'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dualcaster Mage'),
    (select id from sets where short_name = '2xm'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stoneforge Mystic'),
    (select id from sets where short_name = '2xm'),
    '337',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Force of Will'),
    (select id from sets where short_name = '2xm'),
    '340',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jace, the Mind Sculptor'),
    (select id from sets where short_name = '2xm'),
    '334',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ovalchase Daredevil'),
    (select id from sets where short_name = '2xm'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golem-Skin Gauntlets'),
    (select id from sets where short_name = '2xm'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grim Lavamancer'),
    (select id from sets where short_name = '2xm'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sandstone Oracle'),
    (select id from sets where short_name = '2xm'),
    '285',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Driver of the Dead'),
    (select id from sets where short_name = '2xm'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurmcoil Engine'),
    (select id from sets where short_name = '2xm'),
    '368',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Supernatural Stamina'),
    (select id from sets where short_name = '2xm'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liege of the Tangle'),
    (select id from sets where short_name = '2xm'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fierce Empath'),
    (select id from sets where short_name = '2xm'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merciless Eviction'),
    (select id from sets where short_name = '2xm'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphinx of the Guildpact'),
    (select id from sets where short_name = '2xm'),
    '290',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Master Splicer'),
    (select id from sets where short_name = '2xm'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chrome Mox'),
    (select id from sets where short_name = '2xm'),
    '240',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Springleaf Drum'),
    (select id from sets where short_name = '2xm'),
    '291',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcum Dagsson'),
    (select id from sets where short_name = '2xm'),
    '41',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Basalt Monolith'),
    (select id from sets where short_name = '2xm'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bone Picker'),
    (select id from sets where short_name = '2xm'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Aberration'),
    (select id from sets where short_name = '2xm'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodspore Thrinax'),
    (select id from sets where short_name = '2xm'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cyclonic Rift'),
    (select id from sets where short_name = '2xm'),
    '339',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Manamorphose'),
    (select id from sets where short_name = '2xm'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Riddlesmith'),
    (select id from sets where short_name = '2xm'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hammer of Nazahn'),
    (select id from sets where short_name = '2xm'),
    '260',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relic Runner'),
    (select id from sets where short_name = '2xm'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drown in Sorrow'),
    (select id from sets where short_name = '2xm'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cyclonic Rift'),
    (select id from sets where short_name = '2xm'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twisted Abomination'),
    (select id from sets where short_name = '2xm'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blasphemous Act'),
    (select id from sets where short_name = '2xm'),
    '117',
    'rare'
) 
 on conflict do nothing;
