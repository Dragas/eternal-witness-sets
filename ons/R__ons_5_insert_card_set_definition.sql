insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ons'),
    '339',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snarling Undorak'),
    (select id from sets where short_name = 'ons'),
    '283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Charge'),
    (select id from sets where short_name = 'ons'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ons'),
    '343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Venomspout Brackus'),
    (select id from sets where short_name = 'ons'),
    '295',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Solace'),
    (select id from sets where short_name = 'ons'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gigapede'),
    (select id from sets where short_name = 'ons'),
    '264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Airborne Aid'),
    (select id from sets where short_name = 'ons'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Risky Move'),
    (select id from sets where short_name = 'ons'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Syphon Mind'),
    (select id from sets where short_name = 'ons'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krosan Tusker'),
    (select id from sets where short_name = 'ons'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Graxiplon'),
    (select id from sets where short_name = 'ons'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shared Triumph'),
    (select id from sets where short_name = 'ons'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lavamancer''s Skill'),
    (select id from sets where short_name = 'ons'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunfire Balm'),
    (select id from sets where short_name = 'ons'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cover of Darkness'),
    (select id from sets where short_name = 'ons'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Doubtless One'),
    (select id from sets where short_name = 'ons'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daru Healer'),
    (select id from sets where short_name = 'ons'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gustcloak Savior'),
    (select id from sets where short_name = 'ons'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Words of Worship'),
    (select id from sets where short_name = 'ons'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kamahl''s Summons'),
    (select id from sets where short_name = 'ons'),
    '269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prowling Pangolin'),
    (select id from sets where short_name = 'ons'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Polluted Delta'),
    (select id from sets where short_name = 'ons'),
    '321',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crowd Favorites'),
    (select id from sets where short_name = 'ons'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aphetto Vulture'),
    (select id from sets where short_name = 'ons'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mistform Shrieker'),
    (select id from sets where short_name = 'ons'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wirewood Elf'),
    (select id from sets where short_name = 'ons'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Everglove Courier'),
    (select id from sets where short_name = 'ons'),
    '262',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death Match'),
    (select id from sets where short_name = 'ons'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kamahl, Fist of Krosa'),
    (select id from sets where short_name = 'ons'),
    '268',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Endemic Plague'),
    (select id from sets where short_name = 'ons'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sparksmith'),
    (select id from sets where short_name = 'ons'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ebonblade Reaper'),
    (select id from sets where short_name = 'ons'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ons'),
    '338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Echoes'),
    (select id from sets where short_name = 'ons'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ixidor''s Will'),
    (select id from sets where short_name = 'ons'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Piety Charm'),
    (select id from sets where short_name = 'ons'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Centaur Glade'),
    (select id from sets where short_name = 'ons'),
    '251',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hystrodon'),
    (select id from sets where short_name = 'ons'),
    '266',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disciple of Grace'),
    (select id from sets where short_name = 'ons'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chain of Acid'),
    (select id from sets where short_name = 'ons'),
    '252',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Screeching Buzzard'),
    (select id from sets where short_name = 'ons'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clone'),
    (select id from sets where short_name = 'ons'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ons'),
    '337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kaboom!'),
    (select id from sets where short_name = 'ons'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Renewed Faith'),
    (select id from sets where short_name = 'ons'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voice of the Woods'),
    (select id from sets where short_name = 'ons'),
    '297',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nantuko Husk'),
    (select id from sets where short_name = 'ons'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lay Waste'),
    (select id from sets where short_name = 'ons'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cruel Revival'),
    (select id from sets where short_name = 'ons'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fleeting Aven'),
    (select id from sets where short_name = 'ons'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Words of War'),
    (select id from sets where short_name = 'ons'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dwarven Blastminer'),
    (select id from sets where short_name = 'ons'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unified Strike'),
    (select id from sets where short_name = 'ons'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Biorhythm'),
    (select id from sets where short_name = 'ons'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Towering Baloth'),
    (select id from sets where short_name = 'ons'),
    '292',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Misery Charm'),
    (select id from sets where short_name = 'ons'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Warrior'),
    (select id from sets where short_name = 'ons'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wave of Indifference'),
    (select id from sets where short_name = 'ons'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riptide Entrancer'),
    (select id from sets where short_name = 'ons'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overwhelming Instinct'),
    (select id from sets where short_name = 'ons'),
    '276',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Patriarch''s Bidding'),
    (select id from sets where short_name = 'ons'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crown of Ascension'),
    (select id from sets where short_name = 'ons'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mage''s Guile'),
    (select id from sets where short_name = 'ons'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chain of Vapor'),
    (select id from sets where short_name = 'ons'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ixidor, Reality Sculptor'),
    (select id from sets where short_name = 'ons'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trickery Charm'),
    (select id from sets where short_name = 'ons'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Symbiotic Elf'),
    (select id from sets where short_name = 'ons'),
    '288',
    'common'
) ,
(
    (select id from mtgcard where name = 'Complicate'),
    (select id from sets where short_name = 'ons'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wirewood Herald'),
    (select id from sets where short_name = 'ons'),
    '302',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ons'),
    '335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riptide Chronologist'),
    (select id from sets where short_name = 'ons'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reckless One'),
    (select id from sets where short_name = 'ons'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Broodhatch Nantuko'),
    (select id from sets where short_name = 'ons'),
    '250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daunting Defender'),
    (select id from sets where short_name = 'ons'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aphetto Grifter'),
    (select id from sets where short_name = 'ons'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Sledder'),
    (select id from sets where short_name = 'ons'),
    '209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oversold Cemetery'),
    (select id from sets where short_name = 'ons'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Syphon Soul'),
    (select id from sets where short_name = 'ons'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whipcorder'),
    (select id from sets where short_name = 'ons'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doomed Necromancer'),
    (select id from sets where short_name = 'ons'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Headhunter'),
    (select id from sets where short_name = 'ons'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weird Harvest'),
    (select id from sets where short_name = 'ons'),
    '299',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wirewood Lodge'),
    (select id from sets where short_name = 'ons'),
    '329',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silvos, Rogue Elemental'),
    (select id from sets where short_name = 'ons'),
    '282',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serpentine Basilisk'),
    (select id from sets where short_name = 'ons'),
    '280',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Foothill Guide'),
    (select id from sets where short_name = 'ons'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tephraderm'),
    (select id from sets where short_name = 'ons'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Riptide Replicator'),
    (select id from sets where short_name = 'ons'),
    '309',
    'rare'
) ,
(
    (select id from mtgcard where name = 'True Believer'),
    (select id from sets where short_name = 'ons'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Vanguard'),
    (select id from sets where short_name = 'ons'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barren Moor'),
    (select id from sets where short_name = 'ons'),
    '312',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spurred Wolverine'),
    (select id from sets where short_name = 'ons'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Backslide'),
    (select id from sets where short_name = 'ons'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Convalescent Care'),
    (select id from sets where short_name = 'ons'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frightshroud Courier'),
    (select id from sets where short_name = 'ons'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doom Cannon'),
    (select id from sets where short_name = 'ons'),
    '307',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gratuitous Violence'),
    (select id from sets where short_name = 'ons'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cabal Archon'),
    (select id from sets where short_name = 'ons'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sage Aven'),
    (select id from sets where short_name = 'ons'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skittish Valesk'),
    (select id from sets where short_name = 'ons'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spined Basher'),
    (select id from sets where short_name = 'ons'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riptide Biologist'),
    (select id from sets where short_name = 'ons'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ons'),
    '348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Soulgazer'),
    (select id from sets where short_name = 'ons'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death Pulse'),
    (select id from sets where short_name = 'ons'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unholy Grotto'),
    (select id from sets where short_name = 'ons'),
    '327',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Menacing Ogre'),
    (select id from sets where short_name = 'ons'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Words of Wind'),
    (select id from sets where short_name = 'ons'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aura Extraction'),
    (select id from sets where short_name = 'ons'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inspirit'),
    (select id from sets where short_name = 'ons'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leery Fogbeast'),
    (select id from sets where short_name = 'ons'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fade from Memory'),
    (select id from sets where short_name = 'ons'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Strongarm Tactics'),
    (select id from sets where short_name = 'ons'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oblation'),
    (select id from sets where short_name = 'ons'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Machinist'),
    (select id from sets where short_name = 'ons'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Defensive Maneuvers'),
    (select id from sets where short_name = 'ons'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slice and Dice'),
    (select id from sets where short_name = 'ons'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brightstone Ritual'),
    (select id from sets where short_name = 'ons'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disruptive Pitmage'),
    (select id from sets where short_name = 'ons'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akroma''s Vengeance'),
    (select id from sets where short_name = 'ons'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodline Shaman'),
    (select id from sets where short_name = 'ons'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wheel and Deal'),
    (select id from sets where short_name = 'ons'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dive Bomber'),
    (select id from sets where short_name = 'ons'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mobilization'),
    (select id from sets where short_name = 'ons'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jareth, Leonine Titan'),
    (select id from sets where short_name = 'ons'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meddle'),
    (select id from sets where short_name = 'ons'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Future Sight'),
    (select id from sets where short_name = 'ons'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodstained Mire'),
    (select id from sets where short_name = 'ons'),
    '313',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gangrenous Goliath'),
    (select id from sets where short_name = 'ons'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dispersing Orb'),
    (select id from sets where short_name = 'ons'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Taunting Elf'),
    (select id from sets where short_name = 'ons'),
    '290',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chain of Smog'),
    (select id from sets where short_name = 'ons'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crown of Fury'),
    (select id from sets where short_name = 'ons'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tribal Golem'),
    (select id from sets where short_name = 'ons'),
    '311',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ons'),
    '336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tribal Unity'),
    (select id from sets where short_name = 'ons'),
    '294',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nosy Goblin'),
    (select id from sets where short_name = 'ons'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stag Beetle'),
    (select id from sets where short_name = 'ons'),
    '285',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grinning Demon'),
    (select id from sets where short_name = 'ons'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Sky Raider'),
    (select id from sets where short_name = 'ons'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voidmage Prodigy'),
    (select id from sets where short_name = 'ons'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spitting Gourna'),
    (select id from sets where short_name = 'ons'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disciple of Malice'),
    (select id from sets where short_name = 'ons'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Catapult Squad'),
    (select id from sets where short_name = 'ons'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enchantress''s Presence'),
    (select id from sets where short_name = 'ons'),
    '261',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fallen Cleric'),
    (select id from sets where short_name = 'ons'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nameless One'),
    (select id from sets where short_name = 'ons'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aven Fateshaper'),
    (select id from sets where short_name = 'ons'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Windswept Heath'),
    (select id from sets where short_name = 'ons'),
    '328',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ons'),
    '341',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ons'),
    '349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ons'),
    '331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glarecaster'),
    (select id from sets where short_name = 'ons'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Sharpshooter'),
    (select id from sets where short_name = 'ons'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Pioneer'),
    (select id from sets where short_name = 'ons'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Butcher Orgg'),
    (select id from sets where short_name = 'ons'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rorix Bladewing'),
    (select id from sets where short_name = 'ons'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cryptic Gateway'),
    (select id from sets where short_name = 'ons'),
    '306',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mistform Mask'),
    (select id from sets where short_name = 'ons'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sandskin'),
    (select id from sets where short_name = 'ons'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krosan Groundshaker'),
    (select id from sets where short_name = 'ons'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blatant Thievery'),
    (select id from sets where short_name = 'ons'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mythic Proportions'),
    (select id from sets where short_name = 'ons'),
    '274',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fever Charm'),
    (select id from sets where short_name = 'ons'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ons'),
    '350',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'ons'),
    '227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dirge of Dread'),
    (select id from sets where short_name = 'ons'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ons'),
    '333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tempting Wurm'),
    (select id from sets where short_name = 'ons'),
    '291',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Riptide Shapeshifter'),
    (select id from sets where short_name = 'ons'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Supreme Inquisitor'),
    (select id from sets where short_name = 'ons'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Guidance'),
    (select id from sets where short_name = 'ons'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Head Games'),
    (select id from sets where short_name = 'ons'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gravel Slinger'),
    (select id from sets where short_name = 'ons'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rotlung Reanimator'),
    (select id from sets where short_name = 'ons'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Accursed Centaur'),
    (select id from sets where short_name = 'ons'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crown of Suspicion'),
    (select id from sets where short_name = 'ons'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spy Network'),
    (select id from sets where short_name = 'ons'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Mulch'),
    (select id from sets where short_name = 'ons'),
    '298',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skirk Commando'),
    (select id from sets where short_name = 'ons'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Birchlore Rangers'),
    (select id from sets where short_name = 'ons'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Contested Cliffs'),
    (select id from sets where short_name = 'ons'),
    '314',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Improvised Armor'),
    (select id from sets where short_name = 'ons'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ons'),
    '342',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glory Seeker'),
    (select id from sets where short_name = 'ons'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcanis the Omnipotent'),
    (select id from sets where short_name = 'ons'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imagecrafter'),
    (select id from sets where short_name = 'ons'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Run Wild'),
    (select id from sets where short_name = 'ons'),
    '279',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aurification'),
    (select id from sets where short_name = 'ons'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shepherd of Rot'),
    (select id from sets where short_name = 'ons'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Roost'),
    (select id from sets where short_name = 'ons'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Insurrection'),
    (select id from sets where short_name = 'ons'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wirewood Savage'),
    (select id from sets where short_name = 'ons'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grand Melee'),
    (select id from sets where short_name = 'ons'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chain of Silence'),
    (select id from sets where short_name = 'ons'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smother'),
    (select id from sets where short_name = 'ons'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blackmail'),
    (select id from sets where short_name = 'ons'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battlefield Medic'),
    (select id from sets where short_name = 'ons'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seaside Haven'),
    (select id from sets where short_name = 'ons'),
    '323',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Withering Hex'),
    (select id from sets where short_name = 'ons'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anurid Murkdiver'),
    (select id from sets where short_name = 'ons'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunder of Hooves'),
    (select id from sets where short_name = 'ons'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mistform Stalker'),
    (select id from sets where short_name = 'ons'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battering Craghorn'),
    (select id from sets where short_name = 'ons'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erratic Explosion'),
    (select id from sets where short_name = 'ons'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Haunted Cadaver'),
    (select id from sets where short_name = 'ons'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pearlspear Courier'),
    (select id from sets where short_name = 'ons'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mistform Mutant'),
    (select id from sets where short_name = 'ons'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shade''s Breath'),
    (select id from sets where short_name = 'ons'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Information Dealer'),
    (select id from sets where short_name = 'ons'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Burrows'),
    (select id from sets where short_name = 'ons'),
    '318',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wirewood Pride'),
    (select id from sets where short_name = 'ons'),
    '303',
    'common'
) ,
(
    (select id from mtgcard where name = 'Break Open'),
    (select id from sets where short_name = 'ons'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Symbiotic Wurm'),
    (select id from sets where short_name = 'ons'),
    '289',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ons'),
    '340',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feeding Frenzy'),
    (select id from sets where short_name = 'ons'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Catapult Master'),
    (select id from sets where short_name = 'ons'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Colossus'),
    (select id from sets where short_name = 'ons'),
    '270',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wretched Anurid'),
    (select id from sets where short_name = 'ons'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Starlit Sanctum'),
    (select id from sets where short_name = 'ons'),
    '325',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rummaging Wizard'),
    (select id from sets where short_name = 'ons'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slate of Ancestry'),
    (select id from sets where short_name = 'ons'),
    '310',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avarax'),
    (select id from sets where short_name = 'ons'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Scrapper'),
    (select id from sets where short_name = 'ons'),
    '258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crown of Awe'),
    (select id from sets where short_name = 'ons'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crude Rampart'),
    (select id from sets where short_name = 'ons'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquil Thicket'),
    (select id from sets where short_name = 'ons'),
    '326',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nova Cleric'),
    (select id from sets where short_name = 'ons'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ons'),
    '347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Solar Blast'),
    (select id from sets where short_name = 'ons'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Callous Oppressor'),
    (select id from sets where short_name = 'ons'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flooded Strand'),
    (select id from sets where short_name = 'ons'),
    '316',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Starstorm'),
    (select id from sets where short_name = 'ons'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reminisce'),
    (select id from sets where short_name = 'ons'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gustcloak Harrier'),
    (select id from sets where short_name = 'ons'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harsh Mercy'),
    (select id from sets where short_name = 'ons'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ons'),
    '344',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skirk Fire Marshal'),
    (select id from sets where short_name = 'ons'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Custody Battle'),
    (select id from sets where short_name = 'ons'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Infest'),
    (select id from sets where short_name = 'ons'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Righteous Cause'),
    (select id from sets where short_name = 'ons'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steely Resolve'),
    (select id from sets where short_name = 'ons'),
    '286',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dawning Purist'),
    (select id from sets where short_name = 'ons'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gustcloak Sentinel'),
    (select id from sets where short_name = 'ons'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barkhide Mauler'),
    (select id from sets where short_name = 'ons'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cabal Slaver'),
    (select id from sets where short_name = 'ons'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Commando Raid'),
    (select id from sets where short_name = 'ons'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Pyromancer'),
    (select id from sets where short_name = 'ons'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Symbiotic Beast'),
    (select id from sets where short_name = 'ons'),
    '287',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Undead Gladiator'),
    (select id from sets where short_name = 'ons'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Read the Runes'),
    (select id from sets where short_name = 'ons'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Profane Prayers'),
    (select id from sets where short_name = 'ons'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ascending Aven'),
    (select id from sets where short_name = 'ons'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Peer Pressure'),
    (select id from sets where short_name = 'ons'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wellwisher'),
    (select id from sets where short_name = 'ons'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silent Specter'),
    (select id from sets where short_name = 'ons'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = 'ons'),
    '275',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grassland Crusader'),
    (select id from sets where short_name = 'ons'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exalted Angel'),
    (select id from sets where short_name = 'ons'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grand Coliseum'),
    (select id from sets where short_name = 'ons'),
    '319',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animal Magnetism'),
    (select id from sets where short_name = 'ons'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akroma''s Blessing'),
    (select id from sets where short_name = 'ons'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Walking Desecration'),
    (select id from sets where short_name = 'ons'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Invigorating Boon'),
    (select id from sets where short_name = 'ons'),
    '267',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aphetto Dredging'),
    (select id from sets where short_name = 'ons'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forgotten Cave'),
    (select id from sets where short_name = 'ons'),
    '317',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daru Encampment'),
    (select id from sets where short_name = 'ons'),
    '315',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crafty Pathmage'),
    (select id from sets where short_name = 'ons'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ironfist Crusher'),
    (select id from sets where short_name = 'ons'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Pathcutter'),
    (select id from sets where short_name = 'ons'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soulless One'),
    (select id from sets where short_name = 'ons'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snapping Thragg'),
    (select id from sets where short_name = 'ons'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Annex'),
    (select id from sets where short_name = 'ons'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ravenous Baloth'),
    (select id from sets where short_name = 'ons'),
    '278',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aggravated Assault'),
    (select id from sets where short_name = 'ons'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boneknitter'),
    (select id from sets where short_name = 'ons'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sigil of the New Dawn'),
    (select id from sets where short_name = 'ons'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gustcloak Skirmisher'),
    (select id from sets where short_name = 'ons'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Screaming Seahawk'),
    (select id from sets where short_name = 'ons'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghosthelm Courier'),
    (select id from sets where short_name = 'ons'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cabal Executioner'),
    (select id from sets where short_name = 'ons'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daru Lancer'),
    (select id from sets where short_name = 'ons'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wooded Foothills'),
    (select id from sets where short_name = 'ons'),
    '330',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Entrails Feaster'),
    (select id from sets where short_name = 'ons'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancestor''s Prophet'),
    (select id from sets where short_name = 'ons'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Visara the Dreadful'),
    (select id from sets where short_name = 'ons'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swat'),
    (select id from sets where short_name = 'ons'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Discombobulate'),
    (select id from sets where short_name = 'ons'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demystify'),
    (select id from sets where short_name = 'ons'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Astral Slide'),
    (select id from sets where short_name = 'ons'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Charging Slateback'),
    (select id from sets where short_name = 'ons'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Words of Waste'),
    (select id from sets where short_name = 'ons'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Choking Tethers'),
    (select id from sets where short_name = 'ons'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pinpoint Avalanche'),
    (select id from sets where short_name = 'ons'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychic Trance'),
    (select id from sets where short_name = 'ons'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Rift'),
    (select id from sets where short_name = 'ons'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Searing Flesh'),
    (select id from sets where short_name = 'ons'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lonely Sandbar'),
    (select id from sets where short_name = 'ons'),
    '320',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riptide Laboratory'),
    (select id from sets where short_name = 'ons'),
    '322',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aven Brigadier'),
    (select id from sets where short_name = 'ons'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Explosive Vegetation'),
    (select id from sets where short_name = 'ons'),
    '263',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thrashing Mudspawn'),
    (select id from sets where short_name = 'ons'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gluttonous Zombie'),
    (select id from sets where short_name = 'ons'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Threaten'),
    (select id from sets where short_name = 'ons'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Essence Fracture'),
    (select id from sets where short_name = 'ons'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aphetto Alchemist'),
    (select id from sets where short_name = 'ons'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blistering Firecat'),
    (select id from sets where short_name = 'ons'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gravespawn Sovereign'),
    (select id from sets where short_name = 'ons'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vitality Charm'),
    (select id from sets where short_name = 'ons'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistform Skyreaver'),
    (select id from sets where short_name = 'ons'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silklash Spider'),
    (select id from sets where short_name = 'ons'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ons'),
    '332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Festering Goblin'),
    (select id from sets where short_name = 'ons'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crown of Vigor'),
    (select id from sets where short_name = 'ons'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flamestick Courier'),
    (select id from sets where short_name = 'ons'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thoughtbound Primoc'),
    (select id from sets where short_name = 'ons'),
    '240',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dream Chisel'),
    (select id from sets where short_name = 'ons'),
    '308',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ons'),
    '345',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trade Secrets'),
    (select id from sets where short_name = 'ons'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quicksilver Dragon'),
    (select id from sets where short_name = 'ons'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slipstream Eel'),
    (select id from sets where short_name = 'ons'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heedless One'),
    (select id from sets where short_name = 'ons'),
    '265',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Secluded Steppe'),
    (select id from sets where short_name = 'ons'),
    '324',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gustcloak Runner'),
    (select id from sets where short_name = 'ons'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daru Cavalier'),
    (select id from sets where short_name = 'ons'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skirk Prospector'),
    (select id from sets where short_name = 'ons'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistform Wall'),
    (select id from sets where short_name = 'ons'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Airdrop Condor'),
    (select id from sets where short_name = 'ons'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'ons'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'False Cure'),
    (select id from sets where short_name = 'ons'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shieldmage Elder'),
    (select id from sets where short_name = 'ons'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Severed Legion'),
    (select id from sets where short_name = 'ons'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spitfire Handler'),
    (select id from sets where short_name = 'ons'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Primal Boost'),
    (select id from sets where short_name = 'ons'),
    '277',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Standardize'),
    (select id from sets where short_name = 'ons'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Artificial Evolution'),
    (select id from sets where short_name = 'ons'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Embermage Goblin'),
    (select id from sets where short_name = 'ons'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treespring Lorian'),
    (select id from sets where short_name = 'ons'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weathered Wayfarer'),
    (select id from sets where short_name = 'ons'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Piledriver'),
    (select id from sets where short_name = 'ons'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elven Riders'),
    (select id from sets where short_name = 'ons'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chain of Plasma'),
    (select id from sets where short_name = 'ons'),
    '193',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sea''s Claim'),
    (select id from sets where short_name = 'ons'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shaleskin Bruiser'),
    (select id from sets where short_name = 'ons'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ons'),
    '346',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ons'),
    '334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Words of Wilding'),
    (select id from sets where short_name = 'ons'),
    '305',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Taskmaster'),
    (select id from sets where short_name = 'ons'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistform Dreamer'),
    (select id from sets where short_name = 'ons'),
    '93',
    'common'
) 
 on conflict do nothing;
