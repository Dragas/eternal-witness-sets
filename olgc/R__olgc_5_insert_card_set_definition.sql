insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Plateau'),
    (select id from sets where short_name = 'olgc'),
    '2018A',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Volcanic Island'),
    (select id from sets where short_name = 'olgc'),
    '2018NA',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tropical Island'),
    (select id from sets where short_name = 'olgc'),
    '2019',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Cradle'),
    (select id from sets where short_name = 'olgc'),
    '2014',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Taiga'),
    (select id from sets where short_name = 'olgc'),
    '2017EU',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'City of Traitors'),
    (select id from sets where short_name = 'olgc'),
    '2019NA',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Badlands'),
    (select id from sets where short_name = 'olgc'),
    '2016NA',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bayou'),
    (select id from sets where short_name = 'olgc'),
    '2019A',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'olgc'),
    '2012',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tundra'),
    (select id from sets where short_name = 'olgc'),
    '2015',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Savannah'),
    (select id from sets where short_name = 'olgc'),
    '2017NA',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Underground Sea'),
    (select id from sets where short_name = 'olgc'),
    '2016EU',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Force of Will'),
    (select id from sets where short_name = 'olgc'),
    '2011',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scrubland'),
    (select id from sets where short_name = 'olgc'),
    '2018',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wasteland'),
    (select id from sets where short_name = 'olgc'),
    '2013',
    'uncommon'
) 
 on conflict do nothing;
