    insert into mtgcard(name) values ('Zur the Enchanter') on conflict do nothing;
    insert into mtgcard(name) values ('Imperial Seal') on conflict do nothing;
    insert into mtgcard(name) values ('Mana Drain') on conflict do nothing;
    insert into mtgcard(name) values ('Defense of the Heart') on conflict do nothing;
    insert into mtgcard(name) values ('Command Beacon') on conflict do nothing;
    insert into mtgcard(name) values ('Azusa, Lost but Seeking') on conflict do nothing;
    insert into mtgcard(name) values ('Mystic Confluence') on conflict do nothing;
