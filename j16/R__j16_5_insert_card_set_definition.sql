insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Zur the Enchanter'),
    (select id from sets where short_name = 'j16'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imperial Seal'),
    (select id from sets where short_name = 'j16'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Drain'),
    (select id from sets where short_name = 'j16'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Defense of the Heart'),
    (select id from sets where short_name = 'j16'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Command Beacon'),
    (select id from sets where short_name = 'j16'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azusa, Lost but Seeking'),
    (select id from sets where short_name = 'j16'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystic Confluence'),
    (select id from sets where short_name = 'j16'),
    '5',
    'rare'
) 
 on conflict do nothing;
