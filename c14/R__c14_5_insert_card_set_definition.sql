insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Skullclamp'),
    (select id from sets where short_name = 'c14'),
    '268',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beastmaster Ascension'),
    (select id from sets where short_name = 'c14'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nomads'' Assembly'),
    (select id from sets where short_name = 'c14'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of the False God'),
    (select id from sets where short_name = 'c14'),
    '314',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tendrils of Corruption'),
    (select id from sets where short_name = 'c14'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Masked Admirers'),
    (select id from sets where short_name = 'c14'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scrap Mastery'),
    (select id from sets where short_name = 'c14'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dulcet Sirens'),
    (select id from sets where short_name = 'c14'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cathodion'),
    (select id from sets where short_name = 'c14'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bitter Feud'),
    (select id from sets where short_name = 'c14'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reaper from the Abyss'),
    (select id from sets where short_name = 'c14'),
    '159',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Caged Sun'),
    (select id from sets where short_name = 'c14'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Masterwork of Ingenuity'),
    (select id from sets where short_name = 'c14'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bojuka Bog'),
    (select id from sets where short_name = 'c14'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loreseeker''s Stone'),
    (select id from sets where short_name = 'c14'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Skysweeper'),
    (select id from sets where short_name = 'c14'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dualcaster Mage'),
    (select id from sets where short_name = 'c14'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brine Elemental'),
    (select id from sets where short_name = 'c14'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disciple of Bolas'),
    (select id from sets where short_name = 'c14'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c14'),
    '322',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Ranger'),
    (select id from sets where short_name = 'c14'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hoverguard Sweepers'),
    (select id from sets where short_name = 'c14'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Xathrid Demon'),
    (select id from sets where short_name = 'c14'),
    '170',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Epochrasite'),
    (select id from sets where short_name = 'c14'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul of the Harvest'),
    (select id from sets where short_name = 'c14'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nekrataal'),
    (select id from sets where short_name = 'c14'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deploy to the Front'),
    (select id from sets where short_name = 'c14'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Polluted Mire'),
    (select id from sets where short_name = 'c14'),
    '307',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thornweald Archer'),
    (select id from sets where short_name = 'c14'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx of Jwar Isle'),
    (select id from sets where short_name = 'c14'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moss Diamond'),
    (select id from sets where short_name = 'c14'),
    '252',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Celestial Crusader'),
    (select id from sets where short_name = 'c14'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fresh Meat'),
    (select id from sets where short_name = 'c14'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tyrant''s Familiar'),
    (select id from sets where short_name = 'c14'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spitebellows'),
    (select id from sets where short_name = 'c14'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stitcher Geralf'),
    (select id from sets where short_name = 'c14'),
    '17',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rampaging Baloths'),
    (select id from sets where short_name = 'c14'),
    '212',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Marshal''s Anthem'),
    (select id from sets where short_name = 'c14'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dread Return'),
    (select id from sets where short_name = 'c14'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tuktuk the Explorer'),
    (select id from sets where short_name = 'c14'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mycosynth Wellspring'),
    (select id from sets where short_name = 'c14'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trading Post'),
    (select id from sets where short_name = 'c14'),
    '279',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphinx of Magosi'),
    (select id from sets where short_name = 'c14'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hunting Triad'),
    (select id from sets where short_name = 'c14'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lys Alana Huntmaster'),
    (select id from sets where short_name = 'c14'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cyclonic Rift'),
    (select id from sets where short_name = 'c14'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crown of Doom'),
    (select id from sets where short_name = 'c14'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Titania, Protector of Argoth'),
    (select id from sets where short_name = 'c14'),
    '50',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Victimize'),
    (select id from sets where short_name = 'c14'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Afterlife'),
    (select id from sets where short_name = 'c14'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beetleback Chief'),
    (select id from sets where short_name = 'c14'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wren''s Run Packmaster'),
    (select id from sets where short_name = 'c14'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benevolent Offering'),
    (select id from sets where short_name = 'c14'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bottle Gnomes'),
    (select id from sets where short_name = 'c14'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Havenwood Battleground'),
    (select id from sets where short_name = 'c14'),
    '301',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Everflowing Chalice'),
    (select id from sets where short_name = 'c14'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pristine Talisman'),
    (select id from sets where short_name = 'c14'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Junk Diver'),
    (select id from sets where short_name = 'c14'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abyssal Persecutor'),
    (select id from sets where short_name = 'c14'),
    '132',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Decree of Justice'),
    (select id from sets where short_name = 'c14'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Welder'),
    (select id from sets where short_name = 'c14'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Remote Isle'),
    (select id from sets where short_name = 'c14'),
    '309',
    'common'
) ,
(
    (select id from mtgcard where name = 'Butcher of Malakir'),
    (select id from sets where short_name = 'c14'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marble Diamond'),
    (select id from sets where short_name = 'c14'),
    '248',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sacred Mesa'),
    (select id from sets where short_name = 'c14'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c14'),
    '324',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mutilate'),
    (select id from sets where short_name = 'c14'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dismiss'),
    (select id from sets where short_name = 'c14'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whipflare'),
    (select id from sets where short_name = 'c14'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wave of Vitriol'),
    (select id from sets where short_name = 'c14'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dregs of Sorrow'),
    (select id from sets where short_name = 'c14'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Turn to Frog'),
    (select id from sets where short_name = 'c14'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fool''s Demise'),
    (select id from sets where short_name = 'c14'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c14'),
    '321',
    'common'
) ,
(
    (select id from mtgcard where name = 'Willbender'),
    (select id from sets where short_name = 'c14'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c14'),
    '320',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Skirmisher'),
    (select id from sets where short_name = 'c14'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Impact Resonance'),
    (select id from sets where short_name = 'c14'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gray Merchant of Asphodel'),
    (select id from sets where short_name = 'c14'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Starstorm'),
    (select id from sets where short_name = 'c14'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wolfbriar Elemental'),
    (select id from sets where short_name = 'c14'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Midnight Haunting'),
    (select id from sets where short_name = 'c14'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terastodon'),
    (select id from sets where short_name = 'c14'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampire Hexmage'),
    (select id from sets where short_name = 'c14'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bogardan Hellkite'),
    (select id from sets where short_name = 'c14'),
    '173',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swiftfoot Boots'),
    (select id from sets where short_name = 'c14'),
    '275',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feldon of the Third Path'),
    (select id from sets where short_name = 'c14'),
    '35',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Serra Avatar'),
    (select id from sets where short_name = 'c14'),
    '87',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Brave the Elements'),
    (select id from sets where short_name = 'c14'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Strata Scythe'),
    (select id from sets where short_name = 'c14'),
    '274',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c14'),
    '325',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexia''s Core'),
    (select id from sets where short_name = 'c14'),
    '306',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelic Field Marshal'),
    (select id from sets where short_name = 'c14'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mulldrifter'),
    (select id from sets where short_name = 'c14'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Assault Suit'),
    (select id from sets where short_name = 'c14'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Farhaven Elf'),
    (select id from sets where short_name = 'c14'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Predator, Flagship'),
    (select id from sets where short_name = 'c14'),
    '263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruby Medallion'),
    (select id from sets where short_name = 'c14'),
    '265',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c14'),
    '319',
    'common'
) ,
(
    (select id from mtgcard where name = 'Well of Ideas'),
    (select id from sets where short_name = 'c14'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pilgrim''s Eye'),
    (select id from sets where short_name = 'c14'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lifeblood Hydra'),
    (select id from sets where short_name = 'c14'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mask of Memory'),
    (select id from sets where short_name = 'c14'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gift of Estates'),
    (select id from sets where short_name = 'c14'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Offering'),
    (select id from sets where short_name = 'c14'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grave Sifter'),
    (select id from sets where short_name = 'c14'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cackling Counterpart'),
    (select id from sets where short_name = 'c14'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raving Dead'),
    (select id from sets where short_name = 'c14'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Everglades'),
    (select id from sets where short_name = 'c14'),
    '294',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shaper Parasite'),
    (select id from sets where short_name = 'c14'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Word of Seizing'),
    (select id from sets where short_name = 'c14'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sky Diamond'),
    (select id from sets where short_name = 'c14'),
    '269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Commander''s Sphere'),
    (select id from sets where short_name = 'c14'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smoldering Crater'),
    (select id from sets where short_name = 'c14'),
    '312',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crypt Ghast'),
    (select id from sets where short_name = 'c14'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Incite Rebellion'),
    (select id from sets where short_name = 'c14'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wayfarer''s Bauble'),
    (select id from sets where short_name = 'c14'),
    '281',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ezuri, Renegade Leader'),
    (select id from sets where short_name = 'c14'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pestilence Demon'),
    (select id from sets where short_name = 'c14'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Intellectual Offering'),
    (select id from sets where short_name = 'c14'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twilight Shepherd'),
    (select id from sets where short_name = 'c14'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desert Twister'),
    (select id from sets where short_name = 'c14'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thran Dynamo'),
    (select id from sets where short_name = 'c14'),
    '277',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'c14'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c14'),
    '332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grave Titan'),
    (select id from sets where short_name = 'c14'),
    '145',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bloodgift Demon'),
    (select id from sets where short_name = 'c14'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skirsdag High Priest'),
    (select id from sets where short_name = 'c14'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Freyalise, Llanowar''s Fury'),
    (select id from sets where short_name = 'c14'),
    '43',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Profane Command'),
    (select id from sets where short_name = 'c14'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c14'),
    '327',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pongify'),
    (select id from sets where short_name = 'c14'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Adarkar Valkyrie'),
    (select id from sets where short_name = 'c14'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bonehoard'),
    (select id from sets where short_name = 'c14'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fog Bank'),
    (select id from sets where short_name = 'c14'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Read the Bones'),
    (select id from sets where short_name = 'c14'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx of Uthuun'),
    (select id from sets where short_name = 'c14'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Distorting Wake'),
    (select id from sets where short_name = 'c14'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tranquil Thicket'),
    (select id from sets where short_name = 'c14'),
    '316',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sign in Blood'),
    (select id from sets where short_name = 'c14'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silverblade Paladin'),
    (select id from sets where short_name = 'c14'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ur-Golem''s Eye'),
    (select id from sets where short_name = 'c14'),
    '280',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magmaquake'),
    (select id from sets where short_name = 'c14'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Solemn Simulacrum'),
    (select id from sets where short_name = 'c14'),
    '271',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wing Shards'),
    (select id from sets where short_name = 'c14'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jet Medallion'),
    (select id from sets where short_name = 'c14'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nantuko Shade'),
    (select id from sets where short_name = 'c14'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi, Temporal Archmage'),
    (select id from sets where short_name = 'c14'),
    '19',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Slippery Karst'),
    (select id from sets where short_name = 'c14'),
    '311',
    'common'
) ,
(
    (select id from mtgcard where name = 'Concentrate'),
    (select id from sets where short_name = 'c14'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Geist-Honored Monk'),
    (select id from sets where short_name = 'c14'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Oracle'),
    (select id from sets where short_name = 'c14'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of the Dire Hour'),
    (select id from sets where short_name = 'c14'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wake the Dead'),
    (select id from sets where short_name = 'c14'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Collective Unconscious'),
    (select id from sets where short_name = 'c14'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ingot Chewer'),
    (select id from sets where short_name = 'c14'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c14'),
    '326',
    'common'
) ,
(
    (select id from mtgcard where name = 'Into the Roil'),
    (select id from sets where short_name = 'c14'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rite of Replication'),
    (select id from sets where short_name = 'c14'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infernal Offering'),
    (select id from sets where short_name = 'c14'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreamstone Hedron'),
    (select id from sets where short_name = 'c14'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barren Moor'),
    (select id from sets where short_name = 'c14'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flamekin Village'),
    (select id from sets where short_name = 'c14'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drove of Elves'),
    (select id from sets where short_name = 'c14'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hoard-Smelter Dragon'),
    (select id from sets where short_name = 'c14'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Artisan of Kozilek'),
    (select id from sets where short_name = 'c14'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Myriad Landscape'),
    (select id from sets where short_name = 'c14'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Emerald Medallion'),
    (select id from sets where short_name = 'c14'),
    '237',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c14'),
    '318',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mentor of the Meek'),
    (select id from sets where short_name = 'c14'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c14'),
    '337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Sun''s Zenith'),
    (select id from sets where short_name = 'c14'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reliquary Tower'),
    (select id from sets where short_name = 'c14'),
    '308',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Palladium Myr'),
    (select id from sets where short_name = 'c14'),
    '258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Siege Behemoth'),
    (select id from sets where short_name = 'c14'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liquimetal Coating'),
    (select id from sets where short_name = 'c14'),
    '246',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Call to Mind'),
    (select id from sets where short_name = 'c14'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Comeuppance'),
    (select id from sets where short_name = 'c14'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tormod''s Crypt'),
    (select id from sets where short_name = 'c14'),
    '278',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necromantic Selection'),
    (select id from sets where short_name = 'c14'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Song of the Dryads'),
    (select id from sets where short_name = 'c14'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tectonic Edge'),
    (select id from sets where short_name = 'c14'),
    '313',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hallowed Spiritkeeper'),
    (select id from sets where short_name = 'c14'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magus of the Coffers'),
    (select id from sets where short_name = 'c14'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evernight Shade'),
    (select id from sets where short_name = 'c14'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Exclude'),
    (select id from sets where short_name = 'c14'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunderfoot Baloth'),
    (select id from sets where short_name = 'c14'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcane Lighthouse'),
    (select id from sets where short_name = 'c14'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Condemn'),
    (select id from sets where short_name = 'c14'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pearl Medallion'),
    (select id from sets where short_name = 'c14'),
    '260',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c14'),
    '333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nahiri, the Lithomancer'),
    (select id from sets where short_name = 'c14'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Elvish Archdruid'),
    (select id from sets where short_name = 'c14'),
    '190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loxodon Warhammer'),
    (select id from sets where short_name = 'c14'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cathars'' Crusade'),
    (select id from sets where short_name = 'c14'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'True Conviction'),
    (select id from sets where short_name = 'c14'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'c14'),
    '315',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karoo'),
    (select id from sets where short_name = 'c14'),
    '303',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Priest of Titania'),
    (select id from sets where short_name = 'c14'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drana, Kalastria Bloodchief'),
    (select id from sets where short_name = 'c14'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jungle Basin'),
    (select id from sets where short_name = 'c14'),
    '302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Promise of Power'),
    (select id from sets where short_name = 'c14'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rush of Knowledge'),
    (select id from sets where short_name = 'c14'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Titania''s Chosen'),
    (select id from sets where short_name = 'c14'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fathom Seer'),
    (select id from sets where short_name = 'c14'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grand Abolisher'),
    (select id from sets where short_name = 'c14'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Secluded Steppe'),
    (select id from sets where short_name = 'c14'),
    '310',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon of Wailing Agonies'),
    (select id from sets where short_name = 'c14'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sudden Spoiling'),
    (select id from sets where short_name = 'c14'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Requiem Angel'),
    (select id from sets where short_name = 'c14'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ixidron'),
    (select id from sets where short_name = 'c14'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pontiff of Blight'),
    (select id from sets where short_name = 'c14'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sapphire Medallion'),
    (select id from sets where short_name = 'c14'),
    '266',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wolfcaller''s Howl'),
    (select id from sets where short_name = 'c14'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief, the Vastwood'),
    (select id from sets where short_name = 'c14'),
    '305',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c14'),
    '329',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primordial Sage'),
    (select id from sets where short_name = 'c14'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armistice'),
    (select id from sets where short_name = 'c14'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kor Sanctifiers'),
    (select id from sets where short_name = 'c14'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unstable Obelisk'),
    (select id from sets where short_name = 'c14'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reef Worm'),
    (select id from sets where short_name = 'c14'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c14'),
    '334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warmonger Hellkite'),
    (select id from sets where short_name = 'c14'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Stone'),
    (select id from sets where short_name = 'c14'),
    '250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'c14'),
    '270',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coral Atoll'),
    (select id from sets where short_name = 'c14'),
    '287',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crypt of Agadeem'),
    (select id from sets where short_name = 'c14'),
    '288',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steel Hellkite'),
    (select id from sets where short_name = 'c14'),
    '273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infinite Reflection'),
    (select id from sets where short_name = 'c14'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emeria, the Sky Ruin'),
    (select id from sets where short_name = 'c14'),
    '293',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frost Titan'),
    (select id from sets where short_name = 'c14'),
    '112',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ghoulcaller Gisa'),
    (select id from sets where short_name = 'c14'),
    '23',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Panic Spellbomb'),
    (select id from sets where short_name = 'c14'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blasphemous Act'),
    (select id from sets where short_name = 'c14'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pentavus'),
    (select id from sets where short_name = 'c14'),
    '261',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sun Titan'),
    (select id from sets where short_name = 'c14'),
    '91',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis of the Black Oath'),
    (select id from sets where short_name = 'c14'),
    '27',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Great Furnace'),
    (select id from sets where short_name = 'c14'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wood Elves'),
    (select id from sets where short_name = 'c14'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Safekeeper'),
    (select id from sets where short_name = 'c14'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Containment Priest'),
    (select id from sets where short_name = 'c14'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jazal Goldmane'),
    (select id from sets where short_name = 'c14'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Argentum Armor'),
    (select id from sets where short_name = 'c14'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reclamation Sage'),
    (select id from sets where short_name = 'c14'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spine of Ish Sah'),
    (select id from sets where short_name = 'c14'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Ingester'),
    (select id from sets where short_name = 'c14'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Joraga Warcaller'),
    (select id from sets where short_name = 'c14'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Creeperhulk'),
    (select id from sets where short_name = 'c14'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aether Snap'),
    (select id from sets where short_name = 'c14'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c14'),
    '335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kemba, Kha Regent'),
    (select id from sets where short_name = 'c14'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whitemane Lion'),
    (select id from sets where short_name = 'c14'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stroke of Genius'),
    (select id from sets where short_name = 'c14'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Praetor''s Counsel'),
    (select id from sets where short_name = 'c14'),
    '209',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ghost Quarter'),
    (select id from sets where short_name = 'c14'),
    '298',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daretti, Scrap Savant'),
    (select id from sets where short_name = 'c14'),
    '33',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fire Diamond'),
    (select id from sets where short_name = 'c14'),
    '240',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Return to Dust'),
    (select id from sets where short_name = 'c14'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Riptide Survivor'),
    (select id from sets where short_name = 'c14'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chaos Warp'),
    (select id from sets where short_name = 'c14'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flesh Carver'),
    (select id from sets where short_name = 'c14'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grim Flowering'),
    (select id from sets where short_name = 'c14'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silklash Spider'),
    (select id from sets where short_name = 'c14'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c14'),
    '328',
    'common'
) ,
(
    (select id from mtgcard where name = 'Syphon Mind'),
    (select id from sets where short_name = 'c14'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overseer of the Damned'),
    (select id from sets where short_name = 'c14'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aether Gale'),
    (select id from sets where short_name = 'c14'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bad Moon'),
    (select id from sets where short_name = 'c14'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flickerwisp'),
    (select id from sets where short_name = 'c14'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Haunted Fengraf'),
    (select id from sets where short_name = 'c14'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Reaver'),
    (select id from sets where short_name = 'c14'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fell the Mighty'),
    (select id from sets where short_name = 'c14'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wurmcoil Engine'),
    (select id from sets where short_name = 'c14'),
    '283',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c14'),
    '331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Malicious Affliction'),
    (select id from sets where short_name = 'c14'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword of Vengeance'),
    (select id from sets where short_name = 'c14'),
    '276',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burnished Hart'),
    (select id from sets where short_name = 'c14'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Morkrut Banshee'),
    (select id from sets where short_name = 'c14'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Compulsive Research'),
    (select id from sets where short_name = 'c14'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spoils of Blood'),
    (select id from sets where short_name = 'c14'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Domineering Will'),
    (select id from sets where short_name = 'c14'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tornado Elemental'),
    (select id from sets where short_name = 'c14'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c14'),
    '323',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jalum Tome'),
    (select id from sets where short_name = 'c14'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myr Sire'),
    (select id from sets where short_name = 'c14'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Visionary'),
    (select id from sets where short_name = 'c14'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Essence Warden'),
    (select id from sets where short_name = 'c14'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lashwrithe'),
    (select id from sets where short_name = 'c14'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Mystic'),
    (select id from sets where short_name = 'c14'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faithless Looting'),
    (select id from sets where short_name = 'c14'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seer''s Sundial'),
    (select id from sets where short_name = 'c14'),
    '267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Breaching Leviathan'),
    (select id from sets where short_name = 'c14'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c14'),
    '330',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Retriever'),
    (select id from sets where short_name = 'c14'),
    '255',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Timberwatch Elf'),
    (select id from sets where short_name = 'c14'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moonsilver Spear'),
    (select id from sets where short_name = 'c14'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forgotten Cave'),
    (select id from sets where short_name = 'c14'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tragic Slip'),
    (select id from sets where short_name = 'c14'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overwhelming Stampede'),
    (select id from sets where short_name = 'c14'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dormant Volcano'),
    (select id from sets where short_name = 'c14'),
    '291',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c14'),
    '336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charcoal Diamond'),
    (select id from sets where short_name = 'c14'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ichor Wellspring'),
    (select id from sets where short_name = 'c14'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shriekmaw'),
    (select id from sets where short_name = 'c14'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drifting Meadow'),
    (select id from sets where short_name = 'c14'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crystal Vein'),
    (select id from sets where short_name = 'c14'),
    '289',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Annihilate'),
    (select id from sets where short_name = 'c14'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skeletal Scrying'),
    (select id from sets where short_name = 'c14'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Immaculate Magistrate'),
    (select id from sets where short_name = 'c14'),
    '201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stormsurge Kraken'),
    (select id from sets where short_name = 'c14'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whirlwind'),
    (select id from sets where short_name = 'c14'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'c14'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Battlesphere'),
    (select id from sets where short_name = 'c14'),
    '254',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oblation'),
    (select id from sets where short_name = 'c14'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu'),
    (select id from sets where short_name = 'c14'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zoetic Cavern'),
    (select id from sets where short_name = 'c14'),
    '317',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darksteel Citadel'),
    (select id from sets where short_name = 'c14'),
    '290',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deep-Sea Kraken'),
    (select id from sets where short_name = 'c14'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Gargantua'),
    (select id from sets where short_name = 'c14'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bosh, Iron Golem'),
    (select id from sets where short_name = 'c14'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wellwisher'),
    (select id from sets where short_name = 'c14'),
    '223',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harrow'),
    (select id from sets where short_name = 'c14'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Worn Powerstone'),
    (select id from sets where short_name = 'c14'),
    '282',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lorthos, the Tidemaker'),
    (select id from sets where short_name = 'c14'),
    '117',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lonely Sandbar'),
    (select id from sets where short_name = 'c14'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Martial Coup'),
    (select id from sets where short_name = 'c14'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = 'c14'),
    '257',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunblast Angel'),
    (select id from sets where short_name = 'c14'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gargoyle Castle'),
    (select id from sets where short_name = 'c14'),
    '297',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Offering'),
    (select id from sets where short_name = 'c14'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imperious Perfect'),
    (select id from sets where short_name = 'c14'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Buried Ruin'),
    (select id from sets where short_name = 'c14'),
    '286',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spectral Procession'),
    (select id from sets where short_name = 'c14'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azure Mage'),
    (select id from sets where short_name = 'c14'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mobilization'),
    (select id from sets where short_name = 'c14'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'White Sun''s Zenith'),
    (select id from sets where short_name = 'c14'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overrun'),
    (select id from sets where short_name = 'c14'),
    '207',
    'uncommon'
) 
 on conflict do nothing;
