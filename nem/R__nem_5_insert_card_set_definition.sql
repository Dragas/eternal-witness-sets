insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Belbe''s Armor'),
    (select id from sets where short_name = 'nem'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ensnare'),
    (select id from sets where short_name = 'nem'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rathi Fiend'),
    (select id from sets where short_name = 'nem'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volrath the Fallen'),
    (select id from sets where short_name = 'nem'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chieftain en-Dal'),
    (select id from sets where short_name = 'nem'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shrieking Mogg'),
    (select id from sets where short_name = 'nem'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stronghold Gambit'),
    (select id from sets where short_name = 'nem'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flint Golem'),
    (select id from sets where short_name = 'nem'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Poacher'),
    (select id from sets where short_name = 'nem'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rackling'),
    (select id from sets where short_name = 'nem'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flowstone Armor'),
    (select id from sets where short_name = 'nem'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sivvi''s Ruse'),
    (select id from sets where short_name = 'nem'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fog Patch'),
    (select id from sets where short_name = 'nem'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Parallax Dementia'),
    (select id from sets where short_name = 'nem'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightbringer'),
    (select id from sets where short_name = 'nem'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Behemoth'),
    (select id from sets where short_name = 'nem'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pack Hunt'),
    (select id from sets where short_name = 'nem'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animate Land'),
    (select id from sets where short_name = 'nem'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Netter en-Dal'),
    (select id from sets where short_name = 'nem'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overlaid Terrain'),
    (select id from sets where short_name = 'nem'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wandering Eye'),
    (select id from sets where short_name = 'nem'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Predator, Flagship'),
    (select id from sets where short_name = 'nem'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mossdog'),
    (select id from sets where short_name = 'nem'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lawbringer'),
    (select id from sets where short_name = 'nem'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Downhill Charge'),
    (select id from sets where short_name = 'nem'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coiling Woodworm'),
    (select id from sets where short_name = 'nem'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silkenfist Fighter'),
    (select id from sets where short_name = 'nem'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Barrier'),
    (select id from sets where short_name = 'nem'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seal of Fire'),
    (select id from sets where short_name = 'nem'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rootwater Thief'),
    (select id from sets where short_name = 'nem'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seal of Doom'),
    (select id from sets where short_name = 'nem'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Haven'),
    (select id from sets where short_name = 'nem'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Swords'),
    (select id from sets where short_name = 'nem'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rathi Assassin'),
    (select id from sets where short_name = 'nem'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flowstone Overseer'),
    (select id from sets where short_name = 'nem'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voice of Truth'),
    (select id from sets where short_name = 'nem'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mogg Salvage'),
    (select id from sets where short_name = 'nem'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Ridgeback'),
    (select id from sets where short_name = 'nem'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rath''s Edge'),
    (select id from sets where short_name = 'nem'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stronghold Discipline'),
    (select id from sets where short_name = 'nem'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seal of Removal'),
    (select id from sets where short_name = 'nem'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blinding Angel'),
    (select id from sets where short_name = 'nem'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Defiant Vanguard'),
    (select id from sets where short_name = 'nem'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Defiant Falcon'),
    (select id from sets where short_name = 'nem'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Mammoth'),
    (select id from sets where short_name = 'nem'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Laccolith Rig'),
    (select id from sets where short_name = 'nem'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woodripper'),
    (select id from sets where short_name = 'nem'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spiteful Bully'),
    (select id from sets where short_name = 'nem'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seal of Strength'),
    (select id from sets where short_name = 'nem'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhox'),
    (select id from sets where short_name = 'nem'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Cache'),
    (select id from sets where short_name = 'nem'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flowstone Thopter'),
    (select id from sets where short_name = 'nem'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nesting Wurm'),
    (select id from sets where short_name = 'nem'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Hydra'),
    (select id from sets where short_name = 'nem'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Saproling Cluster'),
    (select id from sets where short_name = 'nem'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noble Stand'),
    (select id from sets where short_name = 'nem'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Complex Automaton'),
    (select id from sets where short_name = 'nem'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carrion Wall'),
    (select id from sets where short_name = 'nem'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stronghold Biologist'),
    (select id from sets where short_name = 'nem'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death Pit Offering'),
    (select id from sets where short_name = 'nem'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arc Mage'),
    (select id from sets where short_name = 'nem'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dominate'),
    (select id from sets where short_name = 'nem'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oraxid'),
    (select id from sets where short_name = 'nem'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rathi Intimidator'),
    (select id from sets where short_name = 'nem'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flame Rift'),
    (select id from sets where short_name = 'nem'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Triumph'),
    (select id from sets where short_name = 'nem'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Parallax Tide'),
    (select id from sets where short_name = 'nem'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Parallax Nexus'),
    (select id from sets where short_name = 'nem'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Laccolith Whelp'),
    (select id from sets where short_name = 'nem'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Favor'),
    (select id from sets where short_name = 'nem'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flowstone Wall'),
    (select id from sets where short_name = 'nem'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jolting Merfolk'),
    (select id from sets where short_name = 'nem'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Slash'),
    (select id from sets where short_name = 'nem'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rootwater Commando'),
    (select id from sets where short_name = 'nem'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silkenfist Order'),
    (select id from sets where short_name = 'nem'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harvest Mage'),
    (select id from sets where short_name = 'nem'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moggcatcher'),
    (select id from sets where short_name = 'nem'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eye of Yawgmoth'),
    (select id from sets where short_name = 'nem'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sivvi''s Valor'),
    (select id from sets where short_name = 'nem'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blastoderm'),
    (select id from sets where short_name = 'nem'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flowstone Strike'),
    (select id from sets where short_name = 'nem'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Laccolith Warrior'),
    (select id from sets where short_name = 'nem'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stampede Driver'),
    (select id from sets where short_name = 'nem'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Sentinel'),
    (select id from sets where short_name = 'nem'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viseling'),
    (select id from sets where short_name = 'nem'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Cutter'),
    (select id from sets where short_name = 'nem'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Air Bladder'),
    (select id from sets where short_name = 'nem'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Topple'),
    (select id from sets where short_name = 'nem'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kill Switch'),
    (select id from sets where short_name = 'nem'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Accumulated Knowledge'),
    (select id from sets where short_name = 'nem'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tangle Wire'),
    (select id from sets where short_name = 'nem'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Off Balance'),
    (select id from sets where short_name = 'nem'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pale Moon'),
    (select id from sets where short_name = 'nem'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seal of Cleansing'),
    (select id from sets where short_name = 'nem'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trickster Mage'),
    (select id from sets where short_name = 'nem'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reverent Silence'),
    (select id from sets where short_name = 'nem'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rejuvenation Chamber'),
    (select id from sets where short_name = 'nem'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flowstone Surge'),
    (select id from sets where short_name = 'nem'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fanatical Devotion'),
    (select id from sets where short_name = 'nem'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divining Witch'),
    (select id from sets where short_name = 'nem'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rusting Golem'),
    (select id from sets where short_name = 'nem'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seahunter'),
    (select id from sets where short_name = 'nem'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infiltrate'),
    (select id from sets where short_name = 'nem'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Refreshing Rain'),
    (select id from sets where short_name = 'nem'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flowstone Crusher'),
    (select id from sets where short_name = 'nem'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plague Witch'),
    (select id from sets where short_name = 'nem'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vicious Hunger'),
    (select id from sets where short_name = 'nem'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Parallax Wave'),
    (select id from sets where short_name = 'nem'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Claim'),
    (select id from sets where short_name = 'nem'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daze'),
    (select id from sets where short_name = 'nem'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Submerge'),
    (select id from sets where short_name = 'nem'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stronghold Machinist'),
    (select id from sets where short_name = 'nem'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stronghold Zeppelin'),
    (select id from sets where short_name = 'nem'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treetop Bracers'),
    (select id from sets where short_name = 'nem'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Belbe''s Percher'),
    (select id from sets where short_name = 'nem'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rupture'),
    (select id from sets where short_name = 'nem'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bola Warrior'),
    (select id from sets where short_name = 'nem'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling Burst'),
    (select id from sets where short_name = 'nem'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sneaky Homunculus'),
    (select id from sets where short_name = 'nem'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Prowler'),
    (select id from sets where short_name = 'nem'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oracle''s Attendants'),
    (select id from sets where short_name = 'nem'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Laccolith Titan'),
    (select id from sets where short_name = 'nem'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spineless Thug'),
    (select id from sets where short_name = 'nem'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lin Sivvi, Defiant Hero'),
    (select id from sets where short_name = 'nem'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ascendant Evincar'),
    (select id from sets where short_name = 'nem'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spiritual Asylum'),
    (select id from sets where short_name = 'nem'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terrain Generator'),
    (select id from sets where short_name = 'nem'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Defender en-Vec'),
    (select id from sets where short_name = 'nem'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudskate'),
    (select id from sets where short_name = 'nem'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flowstone Slide'),
    (select id from sets where short_name = 'nem'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rising Waters'),
    (select id from sets where short_name = 'nem'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mogg Toady'),
    (select id from sets where short_name = 'nem'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Driver'),
    (select id from sets where short_name = 'nem'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Massacre'),
    (select id from sets where short_name = 'nem'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Murderous Betrayal'),
    (select id from sets where short_name = 'nem'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Battlefield Percher'),
    (select id from sets where short_name = 'nem'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mogg Alarm'),
    (select id from sets where short_name = 'nem'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Laccolith Grunt'),
    (select id from sets where short_name = 'nem'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Parallax Inhibitor'),
    (select id from sets where short_name = 'nem'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sliptide Serpent'),
    (select id from sets where short_name = 'nem'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Belbe''s Portal'),
    (select id from sets where short_name = 'nem'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avenger en-Dal'),
    (select id from sets where short_name = 'nem'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lashknife'),
    (select id from sets where short_name = 'nem'),
    '9',
    'common'
) 
 on conflict do nothing;
