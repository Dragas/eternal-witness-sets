insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'The Legend of Arena'),
    (select id from sets where short_name = 'htr18'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sol, Advocate Eternal'),
    (select id from sets where short_name = 'htr18'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kharis & The Beholder'),
    (select id from sets where short_name = 'htr18'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Optimus Prime, Inspiring Leader'),
    (select id from sets where short_name = 'htr18'),
    '2',
    'mythic'
) 
 on conflict do nothing;
