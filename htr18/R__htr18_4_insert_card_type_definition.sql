insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'The Legend of Arena'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Legend of Arena'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Legend of Arena'),
        (select types.id from types where name = 'Saga')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sol, Advocate Eternal'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sol, Advocate Eternal'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sol, Advocate Eternal'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sol, Advocate Eternal'),
        (select types.id from types where name = 'Angel')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kharis & The Beholder'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kharis & The Beholder'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kharis & The Beholder'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kharis & The Beholder'),
        (select types.id from types where name = 'Eye')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kharis & The Beholder'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Optimus Prime, Inspiring Leader'),
        (select types.id from types where name = 'Autobot')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Optimus Prime, Inspiring Leader'),
        (select types.id from types where name = 'Character')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Optimus Prime, Inspiring Leader'),
        (select types.id from types where name = 'Bot')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Optimus Prime, Inspiring Leader'),
        (select types.id from types where name = 'Mode')
    ) 
 on conflict do nothing;
