insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Sultai Ascendancy'),
    (select id from sets where short_name = 'cp2'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hero''s Downfall'),
    (select id from sets where short_name = 'cp2'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whip of Erebos'),
    (select id from sets where short_name = 'cp2'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reaper of the Wilds'),
    (select id from sets where short_name = 'cp2'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Courser of Kruphix'),
    (select id from sets where short_name = 'cp2'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necropolis Fiend'),
    (select id from sets where short_name = 'cp2'),
    '1',
    'rare'
) 
 on conflict do nothing;
