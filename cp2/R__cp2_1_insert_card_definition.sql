    insert into mtgcard(name) values ('Sultai Ascendancy') on conflict do nothing;
    insert into mtgcard(name) values ('Hero''s Downfall') on conflict do nothing;
    insert into mtgcard(name) values ('Whip of Erebos') on conflict do nothing;
    insert into mtgcard(name) values ('Reaper of the Wilds') on conflict do nothing;
    insert into mtgcard(name) values ('Courser of Kruphix') on conflict do nothing;
    insert into mtgcard(name) values ('Necropolis Fiend') on conflict do nothing;
