insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Search for Azcanta // Azcanta, the Sunken Ruin'),
    (select id from sets where short_name = 'pxtc'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treasure Map // Treasure Cove'),
    (select id from sets where short_name = 'pxtc'),
    '250',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arguel''s Blood Fast // Temple of Aclazotz'),
    (select id from sets where short_name = 'pxtc'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Growing Rites of Itlimoc // Itlimoc, Cradle of the Sun'),
    (select id from sets where short_name = 'pxtc'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dowsing Dagger // Lost Vale'),
    (select id from sets where short_name = 'pxtc'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conqueror''s Galleon // Conqueror''s Foothold'),
    (select id from sets where short_name = 'pxtc'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primal Amulet // Primal Wellspring'),
    (select id from sets where short_name = 'pxtc'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Legion''s Landing // Adanto, the First Fort'),
    (select id from sets where short_name = 'pxtc'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vance''s Blasting Cannons // Spitfire Bastion'),
    (select id from sets where short_name = 'pxtc'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thaumatic Compass // Spires of Orazca'),
    (select id from sets where short_name = 'pxtc'),
    '249',
    'rare'
) 
 on conflict do nothing;
