    insert into mtgcard(name) values ('Search for Azcanta // Azcanta, the Sunken Ruin') on conflict do nothing;
    insert into mtgcard(name) values ('Treasure Map // Treasure Cove') on conflict do nothing;
    insert into mtgcard(name) values ('Arguel''s Blood Fast // Temple of Aclazotz') on conflict do nothing;
    insert into mtgcard(name) values ('Growing Rites of Itlimoc // Itlimoc, Cradle of the Sun') on conflict do nothing;
    insert into mtgcard(name) values ('Dowsing Dagger // Lost Vale') on conflict do nothing;
    insert into mtgcard(name) values ('Conqueror''s Galleon // Conqueror''s Foothold') on conflict do nothing;
    insert into mtgcard(name) values ('Primal Amulet // Primal Wellspring') on conflict do nothing;
    insert into mtgcard(name) values ('Legion''s Landing // Adanto, the First Fort') on conflict do nothing;
    insert into mtgcard(name) values ('Vance''s Blasting Cannons // Spitfire Bastion') on conflict do nothing;
    insert into mtgcard(name) values ('Thaumatic Compass // Spires of Orazca') on conflict do nothing;
