insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Condemn'),
    (select id from sets where short_name = 'td2'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Triumph of the Hordes'),
    (select id from sets where short_name = 'td2'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rot Wolf'),
    (select id from sets where short_name = 'td2'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Ghoul'),
    (select id from sets where short_name = 'td2'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necroskitter'),
    (select id from sets where short_name = 'td2'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'td2'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silent Arbiter'),
    (select id from sets where short_name = 'td2'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Plaguelord'),
    (select id from sets where short_name = 'td2'),
    '48',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Drooling Groodion'),
    (select id from sets where short_name = 'td2'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Altar'),
    (select id from sets where short_name = 'td2'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barter in Blood'),
    (select id from sets where short_name = 'td2'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grand Architect'),
    (select id from sets where short_name = 'td2'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'td2'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ardent Recruit'),
    (select id from sets where short_name = 'td2'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lumengrid Gargoyle'),
    (select id from sets where short_name = 'td2'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fume Spitter'),
    (select id from sets where short_name = 'td2'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steel Wall'),
    (select id from sets where short_name = 'td2'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glimmerpoint Stag'),
    (select id from sets where short_name = 'td2'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Contagion Clasp'),
    (select id from sets where short_name = 'td2'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Myr Sire'),
    (select id from sets where short_name = 'td2'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancient Den'),
    (select id from sets where short_name = 'td2'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coastal Tower'),
    (select id from sets where short_name = 'td2'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Myr Retriever'),
    (select id from sets where short_name = 'td2'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Talisman of Progress'),
    (select id from sets where short_name = 'td2'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'td2'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spread the Sickness'),
    (select id from sets where short_name = 'td2'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mitotic Slime'),
    (select id from sets where short_name = 'td2'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plague Myr'),
    (select id from sets where short_name = 'td2'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seat of the Synod'),
    (select id from sets where short_name = 'td2'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bonesplitter'),
    (select id from sets where short_name = 'td2'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dispatch'),
    (select id from sets where short_name = 'td2'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reprocess'),
    (select id from sets where short_name = 'td2'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spire Serpent'),
    (select id from sets where short_name = 'td2'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blinkmoth Nexus'),
    (select id from sets where short_name = 'td2'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viridian Claw'),
    (select id from sets where short_name = 'td2'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Purge'),
    (select id from sets where short_name = 'td2'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'td2'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desecration Elemental'),
    (select id from sets where short_name = 'td2'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slagwurm Armor'),
    (select id from sets where short_name = 'td2'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Symbiotic Elf'),
    (select id from sets where short_name = 'td2'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'td2'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Juggernaut'),
    (select id from sets where short_name = 'td2'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'White Sun''s Zenith'),
    (select id from sets where short_name = 'td2'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Memnite'),
    (select id from sets where short_name = 'td2'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thunderstaff'),
    (select id from sets where short_name = 'td2'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'td2'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Test of Faith'),
    (select id from sets where short_name = 'td2'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'One Dozen Eyes'),
    (select id from sets where short_name = 'td2'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plaguemaw Beast'),
    (select id from sets where short_name = 'td2'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Innocent Blood'),
    (select id from sets where short_name = 'td2'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Morbid Plunder'),
    (select id from sets where short_name = 'td2'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forbidding Watchtower'),
    (select id from sets where short_name = 'td2'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'td2'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thirst for Knowledge'),
    (select id from sets where short_name = 'td2'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Sun''s Zenith'),
    (select id from sets where short_name = 'td2'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'td2'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Protean Hulk'),
    (select id from sets where short_name = 'td2'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Platinum Angel'),
    (select id from sets where short_name = 'td2'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Turn the Tide'),
    (select id from sets where short_name = 'td2'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Putrefy'),
    (select id from sets where short_name = 'td2'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'td2'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flesh-Eater Imp'),
    (select id from sets where short_name = 'td2'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steelshaper''s Gift'),
    (select id from sets where short_name = 'td2'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darksteel Gargoyle'),
    (select id from sets where short_name = 'td2'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'td2'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'td2'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silver Myr'),
    (select id from sets where short_name = 'td2'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viridian Corrupter'),
    (select id from sets where short_name = 'td2'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diabolic Servitude'),
    (select id from sets where short_name = 'td2'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Neurok Stealthsuit'),
    (select id from sets where short_name = 'td2'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stoic Rebuttal'),
    (select id from sets where short_name = 'td2'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duplicant'),
    (select id from sets where short_name = 'td2'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'td2'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mortarpod'),
    (select id from sets where short_name = 'td2'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'td2'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viridian Emissary'),
    (select id from sets where short_name = 'td2'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spawning Pit'),
    (select id from sets where short_name = 'td2'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trinket Mage'),
    (select id from sets where short_name = 'td2'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Argent Sphinx'),
    (select id from sets where short_name = 'td2'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kuldotha Forgemaster'),
    (select id from sets where short_name = 'td2'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Snuffers'),
    (select id from sets where short_name = 'td2'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tainted Wood'),
    (select id from sets where short_name = 'td2'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grafted Exoskeleton'),
    (select id from sets where short_name = 'td2'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darksteel Sentinel'),
    (select id from sets where short_name = 'td2'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gold Myr'),
    (select id from sets where short_name = 'td2'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exhume'),
    (select id from sets where short_name = 'td2'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arrest'),
    (select id from sets where short_name = 'td2'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vedalken Certarch'),
    (select id from sets where short_name = 'td2'),
    '5',
    'common'
) 
 on conflict do nothing;
