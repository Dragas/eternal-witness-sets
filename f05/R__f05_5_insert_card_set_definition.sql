insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Icy Manipulator'),
    (select id from sets where short_name = 'f05'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fact or Fiction'),
    (select id from sets where short_name = 'f05'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blastoderm'),
    (select id from sets where short_name = 'f05'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'f05'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Juggernaut'),
    (select id from sets where short_name = 'f05'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rancor'),
    (select id from sets where short_name = 'f05'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = 'f05'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seal of Cleansing'),
    (select id from sets where short_name = 'f05'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kird Ape'),
    (select id from sets where short_name = 'f05'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'f05'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu'),
    (select id from sets where short_name = 'f05'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cabal Therapy'),
    (select id from sets where short_name = 'f05'),
    '5',
    'rare'
) 
 on conflict do nothing;
