insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Murderous Betrayal'),
    (select id from sets where short_name = '8ed'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Worship'),
    (select id from sets where short_name = '8ed'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Elemental'),
    (select id from sets where short_name = '8ed'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Standing Troops'),
    (select id from sets where short_name = '8ed'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Wraith'),
    (select id from sets where short_name = '8ed'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = '8ed'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silverback Ape'),
    (select id from sets where short_name = '8ed'),
    'S7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tundra Wolves'),
    (select id from sets where short_name = '8ed'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = '8ed'),
    '322',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aven Cloudchaser'),
    (select id from sets where short_name = '8ed'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scathe Zombies'),
    (select id from sets where short_name = '8ed'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Staunch Defenders'),
    (select id from sets where short_name = '8ed'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stream of Life'),
    (select id from sets where short_name = '8ed'),
    '282',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plague Wind'),
    (select id from sets where short_name = '8ed'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Llanowar Behemoth'),
    (select id from sets where short_name = '8ed'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = '8ed'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '8ed'),
    '339',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temporal Adept'),
    (select id from sets where short_name = '8ed'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tidal Kraken'),
    (select id from sets where short_name = '8ed'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archivist'),
    (select id from sets where short_name = '8ed'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Raider'),
    (select id from sets where short_name = '8ed'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curiosity'),
    (select id from sets where short_name = '8ed'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vexing Arcanix'),
    (select id from sets where short_name = '8ed'),
    '319',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Catalog'),
    (select id from sets where short_name = '8ed'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = '8ed'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Banishing'),
    (select id from sets where short_name = '8ed'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crossbow Infantry'),
    (select id from sets where short_name = '8ed'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Revive'),
    (select id from sets where short_name = '8ed'),
    '276',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oracle''s Attendants'),
    (select id from sets where short_name = '8ed'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rolling Stones'),
    (select id from sets where short_name = '8ed'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ravenous Rats'),
    (select id from sets where short_name = '8ed'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Badger'),
    (select id from sets where short_name = '8ed'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disrupting Scepter'),
    (select id from sets where short_name = '8ed'),
    '298',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = '8ed'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '8ed'),
    '346',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thorn Elemental'),
    (select id from sets where short_name = '8ed'),
    '283',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm Crow'),
    (select id from sets where short_name = '8ed'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moss Monster'),
    (select id from sets where short_name = '8ed'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boil'),
    (select id from sets where short_name = '8ed'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vengeance'),
    (select id from sets where short_name = '8ed'),
    'S2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raging Goblin'),
    (select id from sets where short_name = '8ed'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vizzerdrix'),
    (select id from sets where short_name = '8ed'),
    'S5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ogre Taskmaster'),
    (select id from sets where short_name = '8ed'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Creeping Mold'),
    (select id from sets where short_name = '8ed'),
    '240',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Twiddle'),
    (select id from sets where short_name = '8ed'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Larceny'),
    (select id from sets where short_name = '8ed'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rod of Ruin'),
    (select id from sets where short_name = '8ed'),
    '312',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coral Eel'),
    (select id from sets where short_name = '8ed'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deflection'),
    (select id from sets where short_name = '8ed'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coastal Piracy'),
    (select id from sets where short_name = '8ed'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blinding Angel'),
    (select id from sets where short_name = '8ed'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '8ed'),
    '340',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boomerang'),
    (select id from sets where short_name = '8ed'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fertile Ground'),
    (select id from sets where short_name = '8ed'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drudge Skeletons'),
    (select id from sets where short_name = '8ed'),
    '129s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = '8ed'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swarm of Rats'),
    (select id from sets where short_name = '8ed'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plow Under'),
    (select id from sets where short_name = '8ed'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathgazer'),
    (select id from sets where short_name = '8ed'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lord of the Undead'),
    (select id from sets where short_name = '8ed'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Plaguelord'),
    (select id from sets where short_name = '8ed'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sage of Lat-Nam'),
    (select id from sets where short_name = '8ed'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Regeneration'),
    (select id from sets where short_name = '8ed'),
    '275',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enrage'),
    (select id from sets where short_name = '8ed'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fallen Angel'),
    (select id from sets where short_name = '8ed'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = '8ed'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Moon'),
    (select id from sets where short_name = '8ed'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: White'),
    (select id from sets where short_name = '8ed'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Master Decoy'),
    (select id from sets where short_name = '8ed'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lone Wolf'),
    (select id from sets where short_name = '8ed'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dingus Egg'),
    (select id from sets where short_name = '8ed'),
    '297',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '8ed'),
    '342',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nantuko Disciple'),
    (select id from sets where short_name = '8ed'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Call of the Wild'),
    (select id from sets where short_name = '8ed'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rhox'),
    (select id from sets where short_name = '8ed'),
    '277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Norwood Ranger'),
    (select id from sets where short_name = '8ed'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Black'),
    (select id from sets where short_name = '8ed'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyroclasm'),
    (select id from sets where short_name = '8ed'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nausea'),
    (select id from sets where short_name = '8ed'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blanchwood Armor'),
    (select id from sets where short_name = '8ed'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spreading Algae'),
    (select id from sets where short_name = '8ed'),
    '281',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Salt Marsh'),
    (select id from sets where short_name = '8ed'),
    '325',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = '8ed'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Millstone'),
    (select id from sets where short_name = '8ed'),
    '307',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rewind'),
    (select id from sets where short_name = '8ed'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '8ed'),
    '335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Healing Salve'),
    (select id from sets where short_name = '8ed'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Okk'),
    (select id from sets where short_name = '8ed'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ridgeline Rager'),
    (select id from sets where short_name = '8ed'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = '8ed'),
    '329',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flight'),
    (select id from sets where short_name = '8ed'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glorious Anthem'),
    (select id from sets where short_name = '8ed'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balduvian Barbarians'),
    (select id from sets where short_name = '8ed'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = '8ed'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gluttonous Zombie'),
    (select id from sets where short_name = '8ed'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Scrapper'),
    (select id from sets where short_name = '8ed'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Confiscate'),
    (select id from sets where short_name = '8ed'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demystify'),
    (select id from sets where short_name = '8ed'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '8ed'),
    '341',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shock Troops'),
    (select id from sets where short_name = '8ed'),
    '223',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Lyrist'),
    (select id from sets where short_name = '8ed'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Furnace of Rath'),
    (select id from sets where short_name = '8ed'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '8ed'),
    '348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rushwood Dryad'),
    (select id from sets where short_name = '8ed'),
    '278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lava Axe'),
    (select id from sets where short_name = '8ed'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombify'),
    (select id from sets where short_name = '8ed'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Royal Assassin'),
    (select id from sets where short_name = '8ed'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '8ed'),
    '336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Western Paladin'),
    (select id from sets where short_name = '8ed'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coercion'),
    (select id from sets where short_name = '8ed'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glory Seeker'),
    (select id from sets where short_name = '8ed'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zur''s Weirding'),
    (select id from sets where short_name = '8ed'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Air'),
    (select id from sets where short_name = '8ed'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ivory Mask'),
    (select id from sets where short_name = '8ed'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flying Carpet'),
    (select id from sets where short_name = '8ed'),
    '301',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wooden Sphere'),
    (select id from sets where short_name = '8ed'),
    '321',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Looming Shade'),
    (select id from sets where short_name = '8ed'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Mercy'),
    (select id from sets where short_name = '8ed'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Bend'),
    (select id from sets where short_name = '8ed'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Leak'),
    (select id from sets where short_name = '8ed'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Intrepid Hero'),
    (select id from sets where short_name = '8ed'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Hammer'),
    (select id from sets where short_name = '8ed'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Might of Oaks'),
    (select id from sets where short_name = '8ed'),
    '265',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emperor Crocodile'),
    (select id from sets where short_name = '8ed'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Blue'),
    (select id from sets where short_name = '8ed'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Carrion Wall'),
    (select id from sets where short_name = '8ed'),
    '121s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Foratog'),
    (select id from sets where short_name = '8ed'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blaze'),
    (select id from sets where short_name = '8ed'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = '8ed'),
    '328',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Razorfoot Griffin'),
    (select id from sets where short_name = '8ed'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viashino Sandstalker'),
    (select id from sets where short_name = '8ed'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Defense Grid'),
    (select id from sets where short_name = '8ed'),
    '296',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Searing Wind'),
    (select id from sets where short_name = '8ed'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = '8ed'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guerrilla Tactics'),
    (select id from sets where short_name = '8ed'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Howling Mine'),
    (select id from sets where short_name = '8ed'),
    '303',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elfhame Palace'),
    (select id from sets where short_name = '8ed'),
    '324',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Throne of Bone'),
    (select id from sets where short_name = '8ed'),
    '317',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abyssal Specter'),
    (select id from sets where short_name = '8ed'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Cockroach'),
    (select id from sets where short_name = '8ed'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Suntail Hawk'),
    (select id from sets where short_name = '8ed'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Samite Healer'),
    (select id from sets where short_name = '8ed'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Panic Attack'),
    (select id from sets where short_name = '8ed'),
    '209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Distorting Lens'),
    (select id from sets where short_name = '8ed'),
    '299',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Star Compass'),
    (select id from sets where short_name = '8ed'),
    '315',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampiric Spirit'),
    (select id from sets where short_name = '8ed'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urborg Volcano'),
    (select id from sets where short_name = '8ed'),
    '327',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aladdin''s Ring'),
    (select id from sets where short_name = '8ed'),
    '291',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Piper'),
    (select id from sets where short_name = '8ed'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ambition''s Cost'),
    (select id from sets where short_name = '8ed'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Primeval Force'),
    (select id from sets where short_name = '8ed'),
    '273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sacred Ground'),
    (select id from sets where short_name = '8ed'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Severed Legion'),
    (select id from sets where short_name = '8ed'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spineless Thug'),
    (select id from sets where short_name = '8ed'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = '8ed'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Stone'),
    (select id from sets where short_name = '8ed'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunweb'),
    (select id from sets where short_name = '8ed'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maro'),
    (select id from sets where short_name = '8ed'),
    '264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Persecute'),
    (select id from sets where short_name = '8ed'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grizzly Bears'),
    (select id from sets where short_name = '8ed'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sacred Nectar'),
    (select id from sets where short_name = '8ed'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctimony'),
    (select id from sets where short_name = '8ed'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sudden Impact'),
    (select id from sets where short_name = '8ed'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Slash'),
    (select id from sets where short_name = '8ed'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Carrion Wall'),
    (select id from sets where short_name = '8ed'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Solidarity'),
    (select id from sets where short_name = '8ed'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fungusaur'),
    (select id from sets where short_name = '8ed'),
    '250',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Puzzle Box'),
    (select id from sets where short_name = '8ed'),
    '316',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Arena'),
    (select id from sets where short_name = '8ed'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rain of Blades'),
    (select id from sets where short_name = '8ed'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Canyon Wildcat'),
    (select id from sets where short_name = '8ed'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diabolic Tutor'),
    (select id from sets where short_name = '8ed'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '8ed'),
    '332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tremor'),
    (select id from sets where short_name = '8ed'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brass Herald'),
    (select id from sets where short_name = '8ed'),
    '293',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unholy Strength'),
    (select id from sets where short_name = '8ed'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = '8ed'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Monstrous Growth'),
    (select id from sets where short_name = '8ed'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea Eagle'),
    (select id from sets where short_name = '8ed'),
    'S4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '8ed'),
    '349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Spy'),
    (select id from sets where short_name = '8ed'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Flock'),
    (select id from sets where short_name = '8ed'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Two-Headed Dragon'),
    (select id from sets where short_name = '8ed'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blessed Reversal'),
    (select id from sets where short_name = '8ed'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chastise'),
    (select id from sets where short_name = '8ed'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seasoned Marshal'),
    (select id from sets where short_name = '8ed'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steal Artifact'),
    (select id from sets where short_name = '8ed'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = '8ed'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eager Cadet'),
    (select id from sets where short_name = '8ed'),
    'S1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fyndhorn Elder'),
    (select id from sets where short_name = '8ed'),
    '251',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flashfires'),
    (select id from sets where short_name = '8ed'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Megrim'),
    (select id from sets where short_name = '8ed'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Glider'),
    (select id from sets where short_name = '8ed'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '8ed'),
    '333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balance of Power'),
    (select id from sets where short_name = '8ed'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = '8ed'),
    '330',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Planar Portal'),
    (select id from sets where short_name = '8ed'),
    '311',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Pioneer'),
    (select id from sets where short_name = '8ed'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Champion'),
    (select id from sets where short_name = '8ed'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Enchantress'),
    (select id from sets where short_name = '8ed'),
    '290',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merchant of Secrets'),
    (select id from sets where short_name = '8ed'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reflexes'),
    (select id from sets where short_name = '8ed'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Swords'),
    (select id from sets where short_name = '8ed'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ardent Militia'),
    (select id from sets where short_name = '8ed'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Intruder Alarm'),
    (select id from sets where short_name = '8ed'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spirit Link'),
    (select id from sets where short_name = '8ed'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seismic Assault'),
    (select id from sets where short_name = '8ed'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beast of Burden'),
    (select id from sets where short_name = '8ed'),
    '292',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Living Terrain'),
    (select id from sets where short_name = '8ed'),
    '260',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bog Imp'),
    (select id from sets where short_name = '8ed'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Demolition Team'),
    (select id from sets where short_name = '8ed'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coastal Tower'),
    (select id from sets where short_name = '8ed'),
    '323',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scathe Zombies'),
    (select id from sets where short_name = '8ed'),
    '160s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fighting Drake'),
    (select id from sets where short_name = '8ed'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sea Monster'),
    (select id from sets where short_name = '8ed'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coastal Hornclaw'),
    (select id from sets where short_name = '8ed'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = '8ed'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Craw Wurm'),
    (select id from sets where short_name = '8ed'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrath of Marit Lage'),
    (select id from sets where short_name = '8ed'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plague Beetle'),
    (select id from sets where short_name = '8ed'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Blast'),
    (select id from sets where short_name = '8ed'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hibernation'),
    (select id from sets where short_name = '8ed'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '8ed'),
    '331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coat of Arms'),
    (select id from sets where short_name = '8ed'),
    '294',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lure'),
    (select id from sets where short_name = '8ed'),
    '263',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '8ed'),
    '337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '8ed'),
    '338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Feast'),
    (select id from sets where short_name = '8ed'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = '8ed'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sneaky Homunculus'),
    (select id from sets where short_name = '8ed'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enormous Baloth'),
    (select id from sets where short_name = '8ed'),
    'S6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lhurgoyf'),
    (select id from sets where short_name = '8ed'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '8ed'),
    '334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Herald'),
    (select id from sets where short_name = '8ed'),
    '252',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hammer of Bogardan'),
    (select id from sets where short_name = '8ed'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '8ed'),
    '347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Fisher'),
    (select id from sets where short_name = '8ed'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Puppeteer'),
    (select id from sets where short_name = '8ed'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '8ed'),
    '350',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horned Turtle'),
    (select id from sets where short_name = '8ed'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avatar of Hope'),
    (select id from sets where short_name = '8ed'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Holy Strength'),
    (select id from sets where short_name = '8ed'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodshot Cyclops'),
    (select id from sets where short_name = '8ed'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = '8ed'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Remove Soul'),
    (select id from sets where short_name = '8ed'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fear'),
    (select id from sets where short_name = '8ed'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elite Archers'),
    (select id from sets where short_name = '8ed'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lesser Gargadon'),
    (select id from sets where short_name = '8ed'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Story Circle'),
    (select id from sets where short_name = '8ed'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Invisibility'),
    (select id from sets where short_name = '8ed'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Spears'),
    (select id from sets where short_name = '8ed'),
    '320',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treasure Trove'),
    (select id from sets where short_name = '8ed'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Chariot'),
    (select id from sets where short_name = '8ed'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fleeting Image'),
    (select id from sets where short_name = '8ed'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relentless Assault'),
    (select id from sets where short_name = '8ed'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Redeem'),
    (select id from sets where short_name = '8ed'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Natural Affinity'),
    (select id from sets where short_name = '8ed'),
    '269',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elite Javelineer'),
    (select id from sets where short_name = '8ed'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vine Trellis'),
    (select id from sets where short_name = '8ed'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spined Wurm'),
    (select id from sets where short_name = '8ed'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Hulk'),
    (select id from sets where short_name = '8ed'),
    '310',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vernal Bloom'),
    (select id from sets where short_name = '8ed'),
    '286',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merchant Scroll'),
    (select id from sets where short_name = '8ed'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Venerable Monk'),
    (select id from sets where short_name = '8ed'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crystal Rod'),
    (select id from sets where short_name = '8ed'),
    '295',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Noble Purpose'),
    (select id from sets where short_name = '8ed'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fodder Cannon'),
    (select id from sets where short_name = '8ed'),
    '302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inspiration'),
    (select id from sets where short_name = '8ed'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Warrior'),
    (select id from sets where short_name = '8ed'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inferno'),
    (select id from sets where short_name = '8ed'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Telepathy'),
    (select id from sets where short_name = '8ed'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diving Griffin'),
    (select id from sets where short_name = '8ed'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spitting Spider'),
    (select id from sets where short_name = '8ed'),
    '280',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fugitive Wizard'),
    (select id from sets where short_name = '8ed'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sage Owl'),
    (select id from sets where short_name = '8ed'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mahamoti Djinn'),
    (select id from sets where short_name = '8ed'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mogg Sentry'),
    (select id from sets where short_name = '8ed'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin King'),
    (select id from sets where short_name = '8ed'),
    '190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savannah Lions'),
    (select id from sets where short_name = '8ed'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fecundity'),
    (select id from sets where short_name = '8ed'),
    '247',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nekrataal'),
    (select id from sets where short_name = '8ed'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Colossus'),
    (select id from sets where short_name = '8ed'),
    '309',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Patagia Golem'),
    (select id from sets where short_name = '8ed'),
    '308',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelic Page'),
    (select id from sets where short_name = '8ed'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cowardice'),
    (select id from sets where short_name = '8ed'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jayemdae Tome'),
    (select id from sets where short_name = '8ed'),
    '306',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sabretooth Tiger'),
    (select id from sets where short_name = '8ed'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obliterate'),
    (select id from sets where short_name = '8ed'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grave Pact'),
    (select id from sets where short_name = '8ed'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thieves'' Auction'),
    (select id from sets where short_name = '8ed'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drudge Skeletons'),
    (select id from sets where short_name = '8ed'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Holy Day'),
    (select id from sets where short_name = '8ed'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slay'),
    (select id from sets where short_name = '8ed'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = '8ed'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spiketail Hatchling'),
    (select id from sets where short_name = '8ed'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skull of Orm'),
    (select id from sets where short_name = '8ed'),
    '313',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Collective Unconscious'),
    (select id from sets where short_name = '8ed'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maggot Carrier'),
    (select id from sets where short_name = '8ed'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deepwood Ghoul'),
    (select id from sets where short_name = '8ed'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dehydration'),
    (select id from sets where short_name = '8ed'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lava Hounds'),
    (select id from sets where short_name = '8ed'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karma'),
    (select id from sets where short_name = '8ed'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Verduran Enchantress'),
    (select id from sets where short_name = '8ed'),
    '285',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Peach Garden Oath'),
    (select id from sets where short_name = '8ed'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demolish'),
    (select id from sets where short_name = '8ed'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Clash'),
    (select id from sets where short_name = '8ed'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Artillery'),
    (select id from sets where short_name = '8ed'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Index'),
    (select id from sets where short_name = '8ed'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eastern Paladin'),
    (select id from sets where short_name = '8ed'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rukh Egg'),
    (select id from sets where short_name = '8ed'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wood Elves'),
    (select id from sets where short_name = '8ed'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyrotechnics'),
    (select id from sets where short_name = '8ed'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = '8ed'),
    '222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flash Counter'),
    (select id from sets where short_name = '8ed'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hulking Cyclops'),
    (select id from sets where short_name = '8ed'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = '8ed'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Canopy Spider'),
    (select id from sets where short_name = '8ed'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sever Soul'),
    (select id from sets where short_name = '8ed'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ensnaring Bridge'),
    (select id from sets where short_name = '8ed'),
    '300',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serpent Warrior'),
    (select id from sets where short_name = '8ed'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sizzle'),
    (select id from sets where short_name = '8ed'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Oasis'),
    (select id from sets where short_name = '8ed'),
    '326',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bribery'),
    (select id from sets where short_name = '8ed'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Choke'),
    (select id from sets where short_name = '8ed'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Armor'),
    (select id from sets where short_name = '8ed'),
    '318',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primeval Shambler'),
    (select id from sets where short_name = '8ed'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '8ed'),
    '344',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Octopus'),
    (select id from sets where short_name = '8ed'),
    'S3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '8ed'),
    '345',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Sludge'),
    (select id from sets where short_name = '8ed'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cinder Wall'),
    (select id from sets where short_name = '8ed'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = '8ed'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wing Snare'),
    (select id from sets where short_name = '8ed'),
    '288',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sword Dancer'),
    (select id from sets where short_name = '8ed'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Iron Star'),
    (select id from sets where short_name = '8ed'),
    '304',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thieving Magpie'),
    (select id from sets where short_name = '8ed'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Concentrate'),
    (select id from sets where short_name = '8ed'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Master Healer'),
    (select id from sets where short_name = '8ed'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Underworld Dreams'),
    (select id from sets where short_name = '8ed'),
    '168',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hunted Wumpus'),
    (select id from sets where short_name = '8ed'),
    '258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vicious Hunger'),
    (select id from sets where short_name = '8ed'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Execute'),
    (select id from sets where short_name = '8ed'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evacuation'),
    (select id from sets where short_name = '8ed'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Green'),
    (select id from sets where short_name = '8ed'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death Pits of Rath'),
    (select id from sets where short_name = '8ed'),
    '125s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '8ed'),
    '343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hill Giant'),
    (select id from sets where short_name = '8ed'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anaba Shaman'),
    (select id from sets where short_name = '8ed'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trade Routes'),
    (select id from sets where short_name = '8ed'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wind Drake'),
    (select id from sets where short_name = '8ed'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Pit Offering'),
    (select id from sets where short_name = '8ed'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Honor Guard'),
    (select id from sets where short_name = '8ed'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Pits of Rath'),
    (select id from sets where short_name = '8ed'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dusk Imp'),
    (select id from sets where short_name = '8ed'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daring Apprentice'),
    (select id from sets where short_name = '8ed'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ivory Cup'),
    (select id from sets where short_name = '8ed'),
    '305',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spellbook'),
    (select id from sets where short_name = '8ed'),
    '314',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trained Armodon'),
    (select id from sets where short_name = '8ed'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shifting Sky'),
    (select id from sets where short_name = '8ed'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warped Devotion'),
    (select id from sets where short_name = '8ed'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Horned Troll'),
    (select id from sets where short_name = '8ed'),
    '257',
    'common'
) 
 on conflict do nothing;
