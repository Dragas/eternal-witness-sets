insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Thallid'),
    (select id from sets where short_name = 'fem'),
    '74d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Initiates of the Ebon Hand'),
    (select id from sets where short_name = 'fem'),
    '39b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Farrel''s Zealot'),
    (select id from sets where short_name = 'fem'),
    '3a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tourach''s Chant'),
    (select id from sets where short_name = 'fem'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heroism'),
    (select id from sets where short_name = 'fem'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elven Fortress'),
    (select id from sets where short_name = 'fem'),
    '65b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basal Thrull'),
    (select id from sets where short_name = 'fem'),
    '34a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Ruins'),
    (select id from sets where short_name = 'fem'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Order of Leitbur'),
    (select id from sets where short_name = 'fem'),
    '16c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Delif''s Cube'),
    (select id from sets where short_name = 'fem'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thorn Thallid'),
    (select id from sets where short_name = 'fem'),
    '80c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Skirmishers'),
    (select id from sets where short_name = 'fem'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spore Cloud'),
    (select id from sets where short_name = 'fem'),
    '72a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thorn Thallid'),
    (select id from sets where short_name = 'fem'),
    '80a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spore Cloud'),
    (select id from sets where short_name = 'fem'),
    '72c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armor Thrull'),
    (select id from sets where short_name = 'fem'),
    '33c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Homarid'),
    (select id from sets where short_name = 'fem'),
    '19d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Grenade'),
    (select id from sets where short_name = 'fem'),
    '56c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Armorer'),
    (select id from sets where short_name = 'fem'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Veteran'),
    (select id from sets where short_name = 'fem'),
    '62a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thorn Thallid'),
    (select id from sets where short_name = 'fem'),
    '80d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Scout'),
    (select id from sets where short_name = 'fem'),
    '68b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Hunter'),
    (select id from sets where short_name = 'fem'),
    '67c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit Shield'),
    (select id from sets where short_name = 'fem'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin War Drums'),
    (select id from sets where short_name = 'fem'),
    '58c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Order of the Ebon Hand'),
    (select id from sets where short_name = 'fem'),
    '42c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Delif''s Cone'),
    (select id from sets where short_name = 'fem'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'River Merfolk'),
    (select id from sets where short_name = 'fem'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Spy'),
    (select id from sets where short_name = 'fem'),
    '61c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin War Drums'),
    (select id from sets where short_name = 'fem'),
    '58d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin War Drums'),
    (select id from sets where short_name = 'fem'),
    '58a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Combat Medic'),
    (select id from sets where short_name = 'fem'),
    '1b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vodalian Mage'),
    (select id from sets where short_name = 'fem'),
    '30c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Chirurgeon'),
    (select id from sets where short_name = 'fem'),
    '54a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Javelineers'),
    (select id from sets where short_name = 'fem'),
    '8c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spore Cloud'),
    (select id from sets where short_name = 'fem'),
    '72b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Homarid Spawning Bed'),
    (select id from sets where short_name = 'fem'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thallid'),
    (select id from sets where short_name = 'fem'),
    '74c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merseine'),
    (select id from sets where short_name = 'fem'),
    '23c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tidal Flats'),
    (select id from sets where short_name = 'fem'),
    '27a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necrite'),
    (select id from sets where short_name = 'fem'),
    '41a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Svyelunite Priest'),
    (select id from sets where short_name = 'fem'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Veteran'),
    (select id from sets where short_name = 'fem'),
    '62c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Homarid'),
    (select id from sets where short_name = 'fem'),
    '19c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Lieutenant'),
    (select id from sets where short_name = 'fem'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armor Thrull'),
    (select id from sets where short_name = 'fem'),
    '33d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sand Silos'),
    (select id from sets where short_name = 'fem'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ebon Stronghold'),
    (select id from sets where short_name = 'fem'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ebon Praetor'),
    (select id from sets where short_name = 'fem'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Farmer'),
    (select id from sets where short_name = 'fem'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zelyon Sword'),
    (select id from sets where short_name = 'fem'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tidal Flats'),
    (select id from sets where short_name = 'fem'),
    '27c',
    'common'
) ,
(
    (select id from mtgcard where name = 'High Tide'),
    (select id from sets where short_name = 'fem'),
    '18a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Veteran'),
    (select id from sets where short_name = 'fem'),
    '62d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mindstab Thrull'),
    (select id from sets where short_name = 'fem'),
    '40a',
    'common'
) ,
(
    (select id from mtgcard where name = 'High Tide'),
    (select id from sets where short_name = 'fem'),
    '18c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thallid'),
    (select id from sets where short_name = 'fem'),
    '74a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Night Soil'),
    (select id from sets where short_name = 'fem'),
    '71a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruins of Trokair'),
    (select id from sets where short_name = 'fem'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thrull Champion'),
    (select id from sets where short_name = 'fem'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Night Soil'),
    (select id from sets where short_name = 'fem'),
    '71b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tidal Flats'),
    (select id from sets where short_name = 'fem'),
    '27b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brassclaw Orcs'),
    (select id from sets where short_name = 'fem'),
    '49d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Hunter'),
    (select id from sets where short_name = 'fem'),
    '67b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thelonite Monk'),
    (select id from sets where short_name = 'fem'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Farrel''s Zealot'),
    (select id from sets where short_name = 'fem'),
    '3c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hymn to Tourach'),
    (select id from sets where short_name = 'fem'),
    '38c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin War Drums'),
    (select id from sets where short_name = 'fem'),
    '58b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orgg'),
    (select id from sets where short_name = 'fem'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hymn to Tourach'),
    (select id from sets where short_name = 'fem'),
    '38d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Initiates of the Ebon Hand'),
    (select id from sets where short_name = 'fem'),
    '39a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merseine'),
    (select id from sets where short_name = 'fem'),
    '23b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Order of the Ebon Hand'),
    (select id from sets where short_name = 'fem'),
    '42b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Homarid Warrior'),
    (select id from sets where short_name = 'fem'),
    '22a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Initiates of the Ebon Hand'),
    (select id from sets where short_name = 'fem'),
    '39c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bottomless Vault'),
    (select id from sets where short_name = 'fem'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necrite'),
    (select id from sets where short_name = 'fem'),
    '41c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Scout'),
    (select id from sets where short_name = 'fem'),
    '68a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vodalian Knights'),
    (select id from sets where short_name = 'fem'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deep Spawn'),
    (select id from sets where short_name = 'fem'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Icatian Scout'),
    (select id from sets where short_name = 'fem'),
    '13c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Soldier'),
    (select id from sets where short_name = 'fem'),
    '53a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mindstab Thrull'),
    (select id from sets where short_name = 'fem'),
    '40b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hand of Justice'),
    (select id from sets where short_name = 'fem'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Icatian Phalanx'),
    (select id from sets where short_name = 'fem'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vodalian Soldiers'),
    (select id from sets where short_name = 'fem'),
    '31a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balm of Restoration'),
    (select id from sets where short_name = 'fem'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thallid'),
    (select id from sets where short_name = 'fem'),
    '74b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thorn Thallid'),
    (select id from sets where short_name = 'fem'),
    '80b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hymn to Tourach'),
    (select id from sets where short_name = 'fem'),
    '38b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conch Horn'),
    (select id from sets where short_name = 'fem'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Icatian Scout'),
    (select id from sets where short_name = 'fem'),
    '13a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Flotilla'),
    (select id from sets where short_name = 'fem'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dwarven Soldier'),
    (select id from sets where short_name = 'fem'),
    '53c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Grenade'),
    (select id from sets where short_name = 'fem'),
    '56a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Svyelunite Temple'),
    (select id from sets where short_name = 'fem'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Spy'),
    (select id from sets where short_name = 'fem'),
    '61b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Catapult'),
    (select id from sets where short_name = 'fem'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Combat Medic'),
    (select id from sets where short_name = 'fem'),
    '1d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Havenwood Battleground'),
    (select id from sets where short_name = 'fem'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raiding Party'),
    (select id from sets where short_name = 'fem'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hollow Trees'),
    (select id from sets where short_name = 'fem'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mindstab Thrull'),
    (select id from sets where short_name = 'fem'),
    '40c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elven Fortress'),
    (select id from sets where short_name = 'fem'),
    '65a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elven Fortress'),
    (select id from sets where short_name = 'fem'),
    '65c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Infantry'),
    (select id from sets where short_name = 'fem'),
    '7d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Chirurgeon'),
    (select id from sets where short_name = 'fem'),
    '54b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vodalian Soldiers'),
    (select id from sets where short_name = 'fem'),
    '31d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thelon''s Curse'),
    (select id from sets where short_name = 'fem'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armor Thrull'),
    (select id from sets where short_name = 'fem'),
    '33b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Combat Medic'),
    (select id from sets where short_name = 'fem'),
    '1a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thelon''s Chant'),
    (select id from sets where short_name = 'fem'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Farrel''s Zealot'),
    (select id from sets where short_name = 'fem'),
    '3b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Order of the Ebon Hand'),
    (select id from sets where short_name = 'fem'),
    '42a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Derelor'),
    (select id from sets where short_name = 'fem'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Exchange'),
    (select id from sets where short_name = 'fem'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aeolipile'),
    (select id from sets where short_name = 'fem'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Kites'),
    (select id from sets where short_name = 'fem'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Breeding Pit'),
    (select id from sets where short_name = 'fem'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brassclaw Orcs'),
    (select id from sets where short_name = 'fem'),
    '49c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Hold'),
    (select id from sets where short_name = 'fem'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Draconian Cylix'),
    (select id from sets where short_name = 'fem'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vodalian Mage'),
    (select id from sets where short_name = 'fem'),
    '30b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ring of Renewal'),
    (select id from sets where short_name = 'fem'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Basal Thrull'),
    (select id from sets where short_name = 'fem'),
    '34c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armor Thrull'),
    (select id from sets where short_name = 'fem'),
    '33a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thallid Devourer'),
    (select id from sets where short_name = 'fem'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Implements of Sacrifice'),
    (select id from sets where short_name = 'fem'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brassclaw Orcs'),
    (select id from sets where short_name = 'fem'),
    '49b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merseine'),
    (select id from sets where short_name = 'fem'),
    '23d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Farrel''s Mantle'),
    (select id from sets where short_name = 'fem'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merseine'),
    (select id from sets where short_name = 'fem'),
    '23a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tidal Influence'),
    (select id from sets where short_name = 'fem'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Icatian Moneychanger'),
    (select id from sets where short_name = 'fem'),
    '10a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basal Thrull'),
    (select id from sets where short_name = 'fem'),
    '34d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Warrens'),
    (select id from sets where short_name = 'fem'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Veteran'),
    (select id from sets where short_name = 'fem'),
    '62b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vodalian Soldiers'),
    (select id from sets where short_name = 'fem'),
    '31b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vodalian Mage'),
    (select id from sets where short_name = 'fem'),
    '30a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Homarid Shaman'),
    (select id from sets where short_name = 'fem'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rainbow Vale'),
    (select id from sets where short_name = 'fem'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'High Tide'),
    (select id from sets where short_name = 'fem'),
    '18b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elven Lyre'),
    (select id from sets where short_name = 'fem'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thrull Wizard'),
    (select id from sets where short_name = 'fem'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seasinger'),
    (select id from sets where short_name = 'fem'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elven Fortress'),
    (select id from sets where short_name = 'fem'),
    '65d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Javelineers'),
    (select id from sets where short_name = 'fem'),
    '8b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Chirurgeon'),
    (select id from sets where short_name = 'fem'),
    '54c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Homarid Warrior'),
    (select id from sets where short_name = 'fem'),
    '22b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Homarid'),
    (select id from sets where short_name = 'fem'),
    '19b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Town'),
    (select id from sets where short_name = 'fem'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Icatian Moneychanger'),
    (select id from sets where short_name = 'fem'),
    '10b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Spy'),
    (select id from sets where short_name = 'fem'),
    '61a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thelonite Druid'),
    (select id from sets where short_name = 'fem'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vodalian War Machine'),
    (select id from sets where short_name = 'fem'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fungal Bloom'),
    (select id from sets where short_name = 'fem'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Icatian Moneychanger'),
    (select id from sets where short_name = 'fem'),
    '10c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Scout'),
    (select id from sets where short_name = 'fem'),
    '68c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Homarid'),
    (select id from sets where short_name = 'fem'),
    '19a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Priest'),
    (select id from sets where short_name = 'fem'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tourach''s Gate'),
    (select id from sets where short_name = 'fem'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Icatian Store'),
    (select id from sets where short_name = 'fem'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thrull Retainer'),
    (select id from sets where short_name = 'fem'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vodalian Soldiers'),
    (select id from sets where short_name = 'fem'),
    '31c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Scout'),
    (select id from sets where short_name = 'fem'),
    '13d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Grenade'),
    (select id from sets where short_name = 'fem'),
    '56b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Hunter'),
    (select id from sets where short_name = 'fem'),
    '67a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Infantry'),
    (select id from sets where short_name = 'fem'),
    '7b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Farrelite Priest'),
    (select id from sets where short_name = 'fem'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necrite'),
    (select id from sets where short_name = 'fem'),
    '41b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Captain'),
    (select id from sets where short_name = 'fem'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feral Thallid'),
    (select id from sets where short_name = 'fem'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Icatian Scout'),
    (select id from sets where short_name = 'fem'),
    '13b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Soldier'),
    (select id from sets where short_name = 'fem'),
    '53b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Lieutenant'),
    (select id from sets where short_name = 'fem'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hymn to Tourach'),
    (select id from sets where short_name = 'fem'),
    '38a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Order of Leitbur'),
    (select id from sets where short_name = 'fem'),
    '16a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Night Soil'),
    (select id from sets where short_name = 'fem'),
    '71c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Combat Medic'),
    (select id from sets where short_name = 'fem'),
    '1c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Infantry'),
    (select id from sets where short_name = 'fem'),
    '7c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Javelineers'),
    (select id from sets where short_name = 'fem'),
    '8a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Infantry'),
    (select id from sets where short_name = 'fem'),
    '7a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spore Flower'),
    (select id from sets where short_name = 'fem'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Homarid Warrior'),
    (select id from sets where short_name = 'fem'),
    '22c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Order of Leitbur'),
    (select id from sets where short_name = 'fem'),
    '16b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brassclaw Orcs'),
    (select id from sets where short_name = 'fem'),
    '49a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basal Thrull'),
    (select id from sets where short_name = 'fem'),
    '34b',
    'common'
) 
 on conflict do nothing;
