insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tdgm'),
    '1',
    'common'
) 
 on conflict do nothing;
