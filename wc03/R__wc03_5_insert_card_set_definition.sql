insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Goblin Warchief'),
    (select id from sets where short_name = 'wc03'),
    'we97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zombify'),
    (select id from sets where short_name = 'wc03'),
    'pk174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'wc03'),
    'we322',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc03'),
    'dh350',
    'common'
) ,
(
    (select id from mtgcard where name = 'Decree of Justice'),
    (select id from sets where short_name = 'wc03'),
    'dz8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Patriarch''s Bidding'),
    (select id from sets where short_name = 'wc03'),
    'we161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantom Nishoba'),
    (select id from sets where short_name = 'wc03'),
    'pk190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roar of the Wurm'),
    (select id from sets where short_name = 'wc03'),
    'dh266',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mirari''s Wake'),
    (select id from sets where short_name = 'wc03'),
    'dz139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Verge'),
    (select id from sets where short_name = 'wc03'),
    'dz141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stitch Together'),
    (select id from sets where short_name = 'wc03'),
    'pk72sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cabal Therapy'),
    (select id from sets where short_name = 'wc03'),
    'pk62sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quiet Speculation'),
    (select id from sets where short_name = 'wc03'),
    'dh49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Undead Gladiator'),
    (select id from sets where short_name = 'wc03'),
    'pk178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Centaur Garden'),
    (select id from sets where short_name = 'wc03'),
    'dh316',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc03'),
    'dz348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smother'),
    (select id from sets where short_name = 'wc03'),
    'we170sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc03'),
    'dz349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc03'),
    'dz336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = 'wc03'),
    'dh112sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circular Logic'),
    (select id from sets where short_name = 'wc03'),
    'dz33sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'wc03'),
    'dz332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Feast'),
    (select id from sets where short_name = 'wc03'),
    'pk165sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Haunting Echoes'),
    (select id from sets where short_name = 'wc03'),
    'pk142sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dave Humpherys Bio'),
    (select id from sets where short_name = 'wc03'),
    'dh0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circular Logic'),
    (select id from sets where short_name = 'wc03'),
    'dz33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc03'),
    'we346',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Leak'),
    (select id from sets where short_name = 'wc03'),
    'dh89sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smother'),
    (select id from sets where short_name = 'wc03'),
    'pk170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Recoup'),
    (select id from sets where short_name = 'wc03'),
    'pk216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cunning Wish'),
    (select id from sets where short_name = 'wc03'),
    'dz37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hunting Pack'),
    (select id from sets where short_name = 'wc03'),
    'dz121sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ray of Revelation'),
    (select id from sets where short_name = 'wc03'),
    'dh20sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolfgang Eder Decklist'),
    (select id from sets where short_name = 'wc03'),
    'we0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'wc03'),
    'dh322',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stitch Together'),
    (select id from sets where short_name = 'wc03'),
    'pk72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'wc03'),
    'dz333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barren Moor'),
    (select id from sets where short_name = 'wc03'),
    'pk312',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodstained Mire'),
    (select id from sets where short_name = 'wc03'),
    'we313',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sickening Dreams'),
    (select id from sets where short_name = 'wc03'),
    'pk83sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc03'),
    'dh337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dave Humpherys Decklist'),
    (select id from sets where short_name = 'wc03'),
    'dh0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc03'),
    'pk339',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolfgang Eder Bio'),
    (select id from sets where short_name = 'wc03'),
    'we0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Recoup'),
    (select id from sets where short_name = 'wc03'),
    'pk216sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc03'),
    'we342',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc03'),
    'dh348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Symbiotic Wurm'),
    (select id from sets where short_name = 'wc03'),
    'pk289',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc03'),
    'dh336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stupefying Touch'),
    (select id from sets where short_name = 'wc03'),
    'dh48sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vengeful Dreams'),
    (select id from sets where short_name = 'wc03'),
    'dz21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gempalm Incinerator'),
    (select id from sets where short_name = 'wc03'),
    'we94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circular Logic'),
    (select id from sets where short_name = 'wc03'),
    'dh33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arrogant Wurm'),
    (select id from sets where short_name = 'wc03'),
    'dh120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Decompose'),
    (select id from sets where short_name = 'wc03'),
    'pk128sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Peer Kröger Bio'),
    (select id from sets where short_name = 'wc03'),
    'pk0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc03'),
    'we339',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deep Analysis'),
    (select id from sets where short_name = 'wc03'),
    'dh36sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moment''s Peace'),
    (select id from sets where short_name = 'wc03'),
    'dz251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Innocent Blood'),
    (select id from sets where short_name = 'wc03'),
    'pk145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Visara the Dreadful'),
    (select id from sets where short_name = 'wc03'),
    'pk179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wonder'),
    (select id from sets where short_name = 'wc03'),
    'dh54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aquamoeba'),
    (select id from sets where short_name = 'wc03'),
    'dh24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc03'),
    'dz337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyroclasm'),
    (select id from sets where short_name = 'wc03'),
    'pk210sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = 'wc03'),
    'dh112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krosan Reclamation'),
    (select id from sets where short_name = 'wc03'),
    'dz122sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flooded Strand'),
    (select id from sets where short_name = 'wc03'),
    'dz316',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Piledriver'),
    (select id from sets where short_name = 'wc03'),
    'we205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'wc03'),
    'dz331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Grappler'),
    (select id from sets where short_name = 'wc03'),
    'we100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ray of Distortion'),
    (select id from sets where short_name = 'wc03'),
    'dz42sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Entomb'),
    (select id from sets where short_name = 'wc03'),
    'pk132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nantuko Vigilante'),
    (select id from sets where short_name = 'wc03'),
    'dh132sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daniel Zink Decklist'),
    (select id from sets where short_name = 'wc03'),
    'dz0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skycloud Expanse'),
    (select id from sets where short_name = 'wc03'),
    'dz327',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anger'),
    (select id from sets where short_name = 'wc03'),
    'pk77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc03'),
    'we343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc03'),
    'pk344',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Banishing'),
    (select id from sets where short_name = 'wc03'),
    'we123sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demolish'),
    (select id from sets where short_name = 'wc03'),
    'pk183sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc03'),
    'dz335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc03'),
    'pk345',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chainer''s Edict'),
    (select id from sets where short_name = 'wc03'),
    'pk57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cabal Therapy'),
    (select id from sets where short_name = 'wc03'),
    'we62sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daniel Zink Bio'),
    (select id from sets where short_name = 'wc03'),
    'dz0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc03'),
    'we344',
    'common'
) ,
(
    (select id from mtgcard where name = '2003 World Championships Ad'),
    (select id from sets where short_name = 'wc03'),
    '0',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deep Analysis'),
    (select id from sets where short_name = 'wc03'),
    'dh36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Taskmaster'),
    (select id from sets where short_name = 'wc03'),
    'we210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krosan Reclamation'),
    (select id from sets where short_name = 'wc03'),
    'dh122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Siege-Gang Commander'),
    (select id from sets where short_name = 'wc03'),
    'we103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Basking Rootwalla'),
    (select id from sets where short_name = 'wc03'),
    'dh121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ray of Revelation'),
    (select id from sets where short_name = 'wc03'),
    'dz20sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Patriarch''s Bidding'),
    (select id from sets where short_name = 'wc03'),
    'pk161sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Reclamation'),
    (select id from sets where short_name = 'wc03'),
    'dh122sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodstained Mire'),
    (select id from sets where short_name = 'wc03'),
    'pk313',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'wc03'),
    'dz58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guiltfeeder'),
    (select id from sets where short_name = 'wc03'),
    'pk68sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vengeful Dreams'),
    (select id from sets where short_name = 'wc03'),
    'dz21sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sulfuric Vortex'),
    (select id from sets where short_name = 'wc03'),
    'we106sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc03'),
    'pk341',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc03'),
    'dh338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wonder'),
    (select id from sets where short_name = 'wc03'),
    'dh54sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skirk Prospector'),
    (select id from sets where short_name = 'wc03'),
    'we230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Renewed Faith'),
    (select id from sets where short_name = 'wc03'),
    'dz50sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Buried Alive'),
    (select id from sets where short_name = 'wc03'),
    'pk118sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc03'),
    'pk340',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Sledder'),
    (select id from sets where short_name = 'wc03'),
    'we209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Renewed Faith'),
    (select id from sets where short_name = 'wc03'),
    'dz50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Mongrel'),
    (select id from sets where short_name = 'wc03'),
    'dh283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc03'),
    'dh349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Sharpshooter'),
    (select id from sets where short_name = 'wc03'),
    'we207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deep Analysis'),
    (select id from sets where short_name = 'wc03'),
    'dz36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcanis the Omnipotent'),
    (select id from sets where short_name = 'wc03'),
    'pk66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shadowblood Ridge'),
    (select id from sets where short_name = 'wc03'),
    'we326',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smother'),
    (select id from sets where short_name = 'wc03'),
    'we170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc03'),
    'pk346',
    'common'
) ,
(
    (select id from mtgcard where name = 'Starstorm'),
    (select id from sets where short_name = 'wc03'),
    'we328sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Peer Kröger Decklist'),
    (select id from sets where short_name = 'wc03'),
    'pk0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc03'),
    'we340',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ray of Revelation'),
    (select id from sets where short_name = 'wc03'),
    'dh20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Centaur'),
    (select id from sets where short_name = 'wc03'),
    'dh127sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc03'),
    'dz347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anurid Brushhopper'),
    (select id from sets where short_name = 'wc03'),
    'dz137sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Last Rites'),
    (select id from sets where short_name = 'wc03'),
    'pk146sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shadowblood Ridge'),
    (select id from sets where short_name = 'wc03'),
    'pk326',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Careful Study'),
    (select id from sets where short_name = 'wc03'),
    'dh70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wing Shards'),
    (select id from sets where short_name = 'wc03'),
    'dz25sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Compulsion'),
    (select id from sets where short_name = 'wc03'),
    'dz34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flaring Pain'),
    (select id from sets where short_name = 'wc03'),
    'we89sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exalted Angel'),
    (select id from sets where short_name = 'wc03'),
    'dz28sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burning Wish'),
    (select id from sets where short_name = 'wc03'),
    'pk83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Doomed Necromancer'),
    (select id from sets where short_name = 'wc03'),
    'pk140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sparksmith'),
    (select id from sets where short_name = 'wc03'),
    'we235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blank Card'),
    (select id from sets where short_name = 'wc03'),
    '00',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Leak'),
    (select id from sets where short_name = 'wc03'),
    'dz89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirari'),
    (select id from sets where short_name = 'wc03'),
    'dz303',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elfhame Palace'),
    (select id from sets where short_name = 'wc03'),
    'dz324',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Envelop'),
    (select id from sets where short_name = 'wc03'),
    'dh39sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cabal Therapy'),
    (select id from sets where short_name = 'wc03'),
    'pk62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Buried Alive'),
    (select id from sets where short_name = 'wc03'),
    'pk118',
    'uncommon'
) 
 on conflict do nothing;
