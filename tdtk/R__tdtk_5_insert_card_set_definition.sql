insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tdtk'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Narset Transcendent Emblem'),
    (select id from sets where short_name = 'tdtk'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Horror'),
    (select id from sets where short_name = 'tdtk'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Morph'),
    (select id from sets where short_name = 'tdtk'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warrior'),
    (select id from sets where short_name = 'tdtk'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tdtk'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tdtk'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Djinn Monk'),
    (select id from sets where short_name = 'tdtk'),
    '2',
    'common'
) 
 on conflict do nothing;
