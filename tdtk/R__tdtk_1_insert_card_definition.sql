    insert into mtgcard(name) values ('Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Narset Transcendent Emblem') on conflict do nothing;
    insert into mtgcard(name) values ('Zombie Horror') on conflict do nothing;
    insert into mtgcard(name) values ('Morph') on conflict do nothing;
    insert into mtgcard(name) values ('Warrior') on conflict do nothing;
    insert into mtgcard(name) values ('Zombie') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin') on conflict do nothing;
    insert into mtgcard(name) values ('Djinn Monk') on conflict do nothing;
