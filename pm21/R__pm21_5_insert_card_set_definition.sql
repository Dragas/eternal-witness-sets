insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Elder Gargaroth'),
    (select id from sets where short_name = 'pm21'),
    '179p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Incinerator'),
    (select id from sets where short_name = 'pm21'),
    '136p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ugin, the Spirit Dragon'),
    (select id from sets where short_name = 'pm21'),
    '1s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Volcanic Salvo'),
    (select id from sets where short_name = 'pm21'),
    '172s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shacklegeist'),
    (select id from sets where short_name = 'pm21'),
    '70s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chromatic Orrery'),
    (select id from sets where short_name = 'pm21'),
    '228p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Temple of Silence'),
    (select id from sets where short_name = 'pm21'),
    '255p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Radha, Heart of Keld'),
    (select id from sets where short_name = 'pm21'),
    '224s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terror of the Peaks'),
    (select id from sets where short_name = 'pm21'),
    '164p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chromatic Orrery'),
    (select id from sets where short_name = 'pm21'),
    '228s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Temple of Mystery'),
    (select id from sets where short_name = 'pm21'),
    '254s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Ageless Insight'),
    (select id from sets where short_name = 'pm21'),
    '76p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi, Master of Time'),
    (select id from sets where short_name = 'pm21'),
    '275p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Primal Might'),
    (select id from sets where short_name = 'pm21'),
    '197p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sporeweb Weaver'),
    (select id from sets where short_name = 'pm21'),
    '208p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thieves'' Guild Enforcer'),
    (select id from sets where short_name = 'pm21'),
    '125p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scavenging Ooze'),
    (select id from sets where short_name = 'pm21'),
    '204s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brash Taunter'),
    (select id from sets where short_name = 'pm21'),
    '133s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necromentia'),
    (select id from sets where short_name = 'pm21'),
    '116p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necromentia'),
    (select id from sets where short_name = 'pm21'),
    '116s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiery Emancipation'),
    (select id from sets where short_name = 'pm21'),
    '143s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Glorious Anthem'),
    (select id from sets where short_name = 'pm21'),
    '21s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Idol of Endurance'),
    (select id from sets where short_name = 'pm21'),
    '23p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'See the Truth'),
    (select id from sets where short_name = 'pm21'),
    '69s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Niambi, Esteemed Speaker'),
    (select id from sets where short_name = 'pm21'),
    '222p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Massacre Wurm'),
    (select id from sets where short_name = 'pm21'),
    '114s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Temple of Epiphany'),
    (select id from sets where short_name = 'pm21'),
    '252p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vito, Thorn of the Dusk Rose'),
    (select id from sets where short_name = 'pm21'),
    '127p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaervek, the Spiteful'),
    (select id from sets where short_name = 'pm21'),
    '106p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi, Master of Time'),
    (select id from sets where short_name = 'pm21'),
    '75p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sublime Epiphany'),
    (select id from sets where short_name = 'pm21'),
    '74s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Salvo'),
    (select id from sets where short_name = 'pm21'),
    '172p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Basri Ket'),
    (select id from sets where short_name = 'pm21'),
    '7p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sporeweb Weaver'),
    (select id from sets where short_name = 'pm21'),
    '208s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animal Sanctuary'),
    (select id from sets where short_name = 'pm21'),
    '242p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Epiphany'),
    (select id from sets where short_name = 'pm21'),
    '252s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Garruk, Unleashed'),
    (select id from sets where short_name = 'pm21'),
    '183p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Subira, Tulzidi Caravanner'),
    (select id from sets where short_name = 'pm21'),
    '162p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Transmogrify'),
    (select id from sets where short_name = 'pm21'),
    '167p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Garruk, Unleashed'),
    (select id from sets where short_name = 'pm21'),
    '183s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kaervek, the Spiteful'),
    (select id from sets where short_name = 'pm21'),
    '106s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Discontinuity'),
    (select id from sets where short_name = 'pm21'),
    '48s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Standard Bearer'),
    (select id from sets where short_name = 'pm21'),
    '110s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra, Heart of Fire'),
    (select id from sets where short_name = 'pm21'),
    '135s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Temple of Malady'),
    (select id from sets where short_name = 'pm21'),
    '253p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Speaker of the Heavens'),
    (select id from sets where short_name = 'pm21'),
    '38p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glorious Anthem'),
    (select id from sets where short_name = 'pm21'),
    '21p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Incinerator'),
    (select id from sets where short_name = 'pm21'),
    '136s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conspicuous Snoop'),
    (select id from sets where short_name = 'pm21'),
    '139s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hooded Blightfang'),
    (select id from sets where short_name = 'pm21'),
    '104s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grim Tutor'),
    (select id from sets where short_name = 'pm21'),
    '103p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Containment Priest'),
    (select id from sets where short_name = 'pm21'),
    '13s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mangara, the Diplomat'),
    (select id from sets where short_name = 'pm21'),
    '27s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Solemn Simulacrum'),
    (select id from sets where short_name = 'pm21'),
    '239s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Massacre Wurm'),
    (select id from sets where short_name = 'pm21'),
    '114p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vito, Thorn of the Dusk Rose'),
    (select id from sets where short_name = 'pm21'),
    '127s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Runed Halo'),
    (select id from sets where short_name = 'pm21'),
    '32p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nine Lives'),
    (select id from sets where short_name = 'pm21'),
    '28s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mazemind Tome'),
    (select id from sets where short_name = 'pm21'),
    '232p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Silence'),
    (select id from sets where short_name = 'pm21'),
    '255s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pursued Whale'),
    (select id from sets where short_name = 'pm21'),
    '60p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fabled Passage'),
    (select id from sets where short_name = 'pm21'),
    '246s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonic Embrace'),
    (select id from sets where short_name = 'pm21'),
    '95p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sublime Epiphany'),
    (select id from sets where short_name = 'pm21'),
    '74p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azusa, Lost but Seeking'),
    (select id from sets where short_name = 'pm21'),
    '173s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Basri''s Lieutenant'),
    (select id from sets where short_name = 'pm21'),
    '9p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Ageless Insight'),
    (select id from sets where short_name = 'pm21'),
    '76s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana, Waker of the Dead'),
    (select id from sets where short_name = 'pm21'),
    '108p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sparkhunter Masticore'),
    (select id from sets where short_name = 'pm21'),
    '240p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conspicuous Snoop'),
    (select id from sets where short_name = 'pm21'),
    '139p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Triumph'),
    (select id from sets where short_name = 'pm21'),
    '256s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ugin, the Spirit Dragon'),
    (select id from sets where short_name = 'pm21'),
    '1p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jolrael, Mwonvuli Recluse'),
    (select id from sets where short_name = 'pm21'),
    '191p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barrin, Tolarian Archmage'),
    (select id from sets where short_name = 'pm21'),
    '45p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra, Heart of Fire'),
    (select id from sets where short_name = 'pm21'),
    '135p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fabled Passage'),
    (select id from sets where short_name = 'pm21'),
    '246p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Baneslayer Angel'),
    (select id from sets where short_name = 'pm21'),
    '6s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pursued Whale'),
    (select id from sets where short_name = 'pm21'),
    '60s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiery Emancipation'),
    (select id from sets where short_name = 'pm21'),
    '143p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Runed Halo'),
    (select id from sets where short_name = 'pm21'),
    '32s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shacklegeist'),
    (select id from sets where short_name = 'pm21'),
    '70p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana, Waker of the Dead'),
    (select id from sets where short_name = 'pm21'),
    '108s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stormwing Entity'),
    (select id from sets where short_name = 'pm21'),
    '73s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi, Master of Time'),
    (select id from sets where short_name = 'pm21'),
    '276p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mazemind Tome'),
    (select id from sets where short_name = 'pm21'),
    '232s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Speaker of the Heavens'),
    (select id from sets where short_name = 'pm21'),
    '38s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Standard Bearer'),
    (select id from sets where short_name = 'pm21'),
    '110p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Solemn Simulacrum'),
    (select id from sets where short_name = 'pm21'),
    '239p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elder Gargaroth'),
    (select id from sets where short_name = 'pm21'),
    '179s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thieves'' Guild Enforcer'),
    (select id from sets where short_name = 'pm21'),
    '125s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gadrak, the Crown-Scourge'),
    (select id from sets where short_name = 'pm21'),
    '146p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jolrael, Mwonvuli Recluse'),
    (select id from sets where short_name = 'pm21'),
    '191s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi, Master of Time'),
    (select id from sets where short_name = 'pm21'),
    '276s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Heroic Intervention'),
    (select id from sets where short_name = 'pm21'),
    '188p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Idol of Endurance'),
    (select id from sets where short_name = 'pm21'),
    '23s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scavenging Ooze'),
    (select id from sets where short_name = 'pm21'),
    '204p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Radha, Heart of Keld'),
    (select id from sets where short_name = 'pm21'),
    '224p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghostly Pilferer'),
    (select id from sets where short_name = 'pm21'),
    '52p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Malady'),
    (select id from sets where short_name = 'pm21'),
    '253s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Triumph'),
    (select id from sets where short_name = 'pm21'),
    '256p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi, Master of Time'),
    (select id from sets where short_name = 'pm21'),
    '75s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mangara, the Diplomat'),
    (select id from sets where short_name = 'pm21'),
    '27p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Double Vision'),
    (select id from sets where short_name = 'pm21'),
    '142p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pack Leader'),
    (select id from sets where short_name = 'pm21'),
    '29s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azusa, Lost but Seeking'),
    (select id from sets where short_name = 'pm21'),
    '173p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Niambi, Esteemed Speaker'),
    (select id from sets where short_name = 'pm21'),
    '222s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animal Sanctuary'),
    (select id from sets where short_name = 'pm21'),
    '242s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Discontinuity'),
    (select id from sets where short_name = 'pm21'),
    '48p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Garruk''s Harbinger'),
    (select id from sets where short_name = 'pm21'),
    '185s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi, Master of Time'),
    (select id from sets where short_name = 'pm21'),
    '275s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Teferi, Master of Time'),
    (select id from sets where short_name = 'pm21'),
    '277s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Subira, Tulzidi Caravanner'),
    (select id from sets where short_name = 'pm21'),
    '162s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primal Might'),
    (select id from sets where short_name = 'pm21'),
    '197s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'See the Truth'),
    (select id from sets where short_name = 'pm21'),
    '69p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sparkhunter Masticore'),
    (select id from sets where short_name = 'pm21'),
    '240s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grim Tutor'),
    (select id from sets where short_name = 'pm21'),
    '103s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Heroic Intervention'),
    (select id from sets where short_name = 'pm21'),
    '188s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sanctum of All'),
    (select id from sets where short_name = 'pm21'),
    '225p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nine Lives'),
    (select id from sets where short_name = 'pm21'),
    '28p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hooded Blightfang'),
    (select id from sets where short_name = 'pm21'),
    '104p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Peer into the Abyss'),
    (select id from sets where short_name = 'pm21'),
    '117s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi, Master of Time'),
    (select id from sets where short_name = 'pm21'),
    '277p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Temple of Mystery'),
    (select id from sets where short_name = 'pm21'),
    '254p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feline Sovereign'),
    (select id from sets where short_name = 'pm21'),
    '180p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Transmogrify'),
    (select id from sets where short_name = 'pm21'),
    '167s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Baneslayer Angel'),
    (select id from sets where short_name = 'pm21'),
    '6p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Double Vision'),
    (select id from sets where short_name = 'pm21'),
    '142s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terror of the Peaks'),
    (select id from sets where short_name = 'pm21'),
    '164s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Basri Ket'),
    (select id from sets where short_name = 'pm21'),
    '7s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ghostly Pilferer'),
    (select id from sets where short_name = 'pm21'),
    '52s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brash Taunter'),
    (select id from sets where short_name = 'pm21'),
    '133p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonic Embrace'),
    (select id from sets where short_name = 'pm21'),
    '95s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Peer into the Abyss'),
    (select id from sets where short_name = 'pm21'),
    '117p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barrin, Tolarian Archmage'),
    (select id from sets where short_name = 'pm21'),
    '45s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feline Sovereign'),
    (select id from sets where short_name = 'pm21'),
    '180s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stormwing Entity'),
    (select id from sets where short_name = 'pm21'),
    '73p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Basri''s Lieutenant'),
    (select id from sets where short_name = 'pm21'),
    '9s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Garruk''s Harbinger'),
    (select id from sets where short_name = 'pm21'),
    '185p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gadrak, the Crown-Scourge'),
    (select id from sets where short_name = 'pm21'),
    '146s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sanctum of All'),
    (select id from sets where short_name = 'pm21'),
    '225s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pack Leader'),
    (select id from sets where short_name = 'pm21'),
    '29p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Containment Priest'),
    (select id from sets where short_name = 'pm21'),
    '13p',
    'rare'
) 
 on conflict do nothing;
