    insert into mtgcard(name) values ('Dark Confidant') on conflict do nothing;
    insert into mtgcard(name) values ('Sword of Fire and Ice') on conflict do nothing;
    insert into mtgcard(name) values ('Entomb') on conflict do nothing;
    insert into mtgcard(name) values ('Vendilion Clique') on conflict do nothing;
    insert into mtgcard(name) values ('Mana Crypt') on conflict do nothing;
    insert into mtgcard(name) values ('Wolf') on conflict do nothing;
    insert into mtgcard(name) values ('Doubling Season') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Welder') on conflict do nothing;
    insert into mtgcard(name) values ('Bitterblossom') on conflict do nothing;
