insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Dark Confidant'),
    (select id from sets where short_name = 'g11'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword of Fire and Ice'),
    (select id from sets where short_name = 'g11'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Entomb'),
    (select id from sets where short_name = 'g11'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vendilion Clique'),
    (select id from sets where short_name = 'g11'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Crypt'),
    (select id from sets where short_name = 'g11'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'g11'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doubling Season'),
    (select id from sets where short_name = 'g11'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Welder'),
    (select id from sets where short_name = 'g11'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bitterblossom'),
    (select id from sets where short_name = 'g11'),
    '1',
    'rare'
) 
 on conflict do nothing;
