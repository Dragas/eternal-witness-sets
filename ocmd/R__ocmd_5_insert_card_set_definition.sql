insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Numot, the Devastator'),
    (select id from sets where short_name = 'ocmd'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karador, Ghost Chieftain'),
    (select id from sets where short_name = 'ocmd'),
    '207',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kaalia of the Vast'),
    (select id from sets where short_name = 'ocmd'),
    '206',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'The Mimeoplasm'),
    (select id from sets where short_name = 'ocmd'),
    '210',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Damia, Sage of Stone'),
    (select id from sets where short_name = 'ocmd'),
    '191',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Teneb, the Harvester'),
    (select id from sets where short_name = 'ocmd'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zedruu the Greathearted'),
    (select id from sets where short_name = 'ocmd'),
    '240',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Oros, the Avenger'),
    (select id from sets where short_name = 'ocmd'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animar, Soul of Elements'),
    (select id from sets where short_name = 'ocmd'),
    '181',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vorosh, the Hunter'),
    (select id from sets where short_name = 'ocmd'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tariel, Reckoner of Souls'),
    (select id from sets where short_name = 'ocmd'),
    '229',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Riku of Two Reflections'),
    (select id from sets where short_name = 'ocmd'),
    '220',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ruhan of the Fomori'),
    (select id from sets where short_name = 'ocmd'),
    '221',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Intet, the Dreamer'),
    (select id from sets where short_name = 'ocmd'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghave, Guru of Spores'),
    (select id from sets where short_name = 'ocmd'),
    '200',
    'mythic'
) 
 on conflict do nothing;
