    insert into mtgcard(name) values ('Elephant') on conflict do nothing;
    insert into mtgcard(name) values ('Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Soldier') on conflict do nothing;
    insert into mtgcard(name) values ('Zombie') on conflict do nothing;
    insert into mtgcard(name) values ('Wurm') on conflict do nothing;
    insert into mtgcard(name) values ('Squirrel') on conflict do nothing;
