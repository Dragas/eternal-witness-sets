insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Golem''s Heart'),
    (select id from sets where short_name = 'pwp10'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fling'),
    (select id from sets where short_name = 'pwp10'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plague Stinger'),
    (select id from sets where short_name = 'pwp10'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skinrender'),
    (select id from sets where short_name = 'pwp10'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kor Firewalker'),
    (select id from sets where short_name = 'pwp10'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curse of Wizardry'),
    (select id from sets where short_name = 'pwp10'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Syphon Mind'),
    (select id from sets where short_name = 'pwp10'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leatherback Baloth'),
    (select id from sets where short_name = 'pwp10'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Ranger'),
    (select id from sets where short_name = 'pwp10'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pathrazer of Ulamog'),
    (select id from sets where short_name = 'pwp10'),
    '46',
    'rare'
) 
 on conflict do nothing;
