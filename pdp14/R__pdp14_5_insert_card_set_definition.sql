insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Scavenging Ooze'),
    (select id from sets where short_name = 'pdp14'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bonescythe Sliver'),
    (select id from sets where short_name = 'pdp14'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ogre Battledriver'),
    (select id from sets where short_name = 'pdp14'),
    '2',
    'rare'
) 
 on conflict do nothing;
