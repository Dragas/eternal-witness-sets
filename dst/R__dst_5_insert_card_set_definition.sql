insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ur-Golem''s Eye'),
    (select id from sets where short_name = 'dst'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death-Mask Duplicant'),
    (select id from sets where short_name = 'dst'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Echoing Ruin'),
    (select id from sets where short_name = 'dst'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scavenging Scarab'),
    (select id from sets where short_name = 'dst'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karstoderm'),
    (select id from sets where short_name = 'dst'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leonin Battlemage'),
    (select id from sets where short_name = 'dst'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Roaring Slagwurm'),
    (select id from sets where short_name = 'dst'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magnetic Flux'),
    (select id from sets where short_name = 'dst'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nourish'),
    (select id from sets where short_name = 'dst'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reshape'),
    (select id from sets where short_name = 'dst'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Echoing Calm'),
    (select id from sets where short_name = 'dst'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darksteel Brute'),
    (select id from sets where short_name = 'dst'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tears of Rage'),
    (select id from sets where short_name = 'dst'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Geth''s Grimoire'),
    (select id from sets where short_name = 'dst'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stand Together'),
    (select id from sets where short_name = 'dst'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Myr Landshaper'),
    (select id from sets where short_name = 'dst'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nim Abomination'),
    (select id from sets where short_name = 'dst'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wand of the Elements'),
    (select id from sets where short_name = 'dst'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grimclaw Bats'),
    (select id from sets where short_name = 'dst'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oxidize'),
    (select id from sets where short_name = 'dst'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Last Word'),
    (select id from sets where short_name = 'dst'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crazed Goblin'),
    (select id from sets where short_name = 'dst'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spire Golem'),
    (select id from sets where short_name = 'dst'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scrounge'),
    (select id from sets where short_name = 'dst'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pteron Ghost'),
    (select id from sets where short_name = 'dst'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darksteel Reactor'),
    (select id from sets where short_name = 'dst'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tel-Jilad Outrider'),
    (select id from sets where short_name = 'dst'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dross Golem'),
    (select id from sets where short_name = 'dst'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drooling Ogre'),
    (select id from sets where short_name = 'dst'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tanglewalker'),
    (select id from sets where short_name = 'dst'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Second Sight'),
    (select id from sets where short_name = 'dst'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barbed Lightning'),
    (select id from sets where short_name = 'dst'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shriveling Rot'),
    (select id from sets where short_name = 'dst'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gemini Engine'),
    (select id from sets where short_name = 'dst'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spawning Pit'),
    (select id from sets where short_name = 'dst'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drill-Skimmer'),
    (select id from sets where short_name = 'dst'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tangle Spider'),
    (select id from sets where short_name = 'dst'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sundering Titan'),
    (select id from sets where short_name = 'dst'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcbound Reclaimer'),
    (select id from sets where short_name = 'dst'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcbound Fiend'),
    (select id from sets where short_name = 'dst'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demon''s Horn'),
    (select id from sets where short_name = 'dst'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shunt'),
    (select id from sets where short_name = 'dst'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oxidda Golem'),
    (select id from sets where short_name = 'dst'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurm''s Tooth'),
    (select id from sets where short_name = 'dst'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Machinate'),
    (select id from sets where short_name = 'dst'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel''s Feather'),
    (select id from sets where short_name = 'dst'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Myr Matrix'),
    (select id from sets where short_name = 'dst'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Echoing Truth'),
    (select id from sets where short_name = 'dst'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spellbinder'),
    (select id from sets where short_name = 'dst'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darksteel Gargoyle'),
    (select id from sets where short_name = 'dst'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Retract'),
    (select id from sets where short_name = 'dst'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Panoptic Mirror'),
    (select id from sets where short_name = 'dst'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mephitic Ooze'),
    (select id from sets where short_name = 'dst'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skullclamp'),
    (select id from sets where short_name = 'dst'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leonin Shikari'),
    (select id from sets where short_name = 'dst'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Memnarch'),
    (select id from sets where short_name = 'dst'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rebuking Ceremony'),
    (select id from sets where short_name = 'dst'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loxodon Mystic'),
    (select id from sets where short_name = 'dst'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sword of Light and Shadow'),
    (select id from sets where short_name = 'dst'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Furnace Dragon'),
    (select id from sets where short_name = 'dst'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viridian Acolyte'),
    (select id from sets where short_name = 'dst'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quicksilver Behemoth'),
    (select id from sets where short_name = 'dst'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Razor Golem'),
    (select id from sets where short_name = 'dst'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soulscour'),
    (select id from sets where short_name = 'dst'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Well of Lost Dreams'),
    (select id from sets where short_name = 'dst'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Echoing Courage'),
    (select id from sets where short_name = 'dst'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcbound Ravager'),
    (select id from sets where short_name = 'dst'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ritual of Restoration'),
    (select id from sets where short_name = 'dst'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Vial'),
    (select id from sets where short_name = 'dst'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcbound Crusher'),
    (select id from sets where short_name = 'dst'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcbound Worker'),
    (select id from sets where short_name = 'dst'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stir the Pride'),
    (select id from sets where short_name = 'dst'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon''s Claw'),
    (select id from sets where short_name = 'dst'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wirefly Hive'),
    (select id from sets where short_name = 'dst'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcbound Slith'),
    (select id from sets where short_name = 'dst'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Auriok Siege Sled'),
    (select id from sets where short_name = 'dst'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcbound Lancer'),
    (select id from sets where short_name = 'dst'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hallow'),
    (select id from sets where short_name = 'dst'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serum Powder'),
    (select id from sets where short_name = 'dst'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thunderstaff'),
    (select id from sets where short_name = 'dst'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Screams from Within'),
    (select id from sets where short_name = 'dst'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flamebreak'),
    (select id from sets where short_name = 'dst'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pulse of the Tangle'),
    (select id from sets where short_name = 'dst'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcbound Overseer'),
    (select id from sets where short_name = 'dst'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword of Fire and Ice'),
    (select id from sets where short_name = 'dst'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Murderous Spoils'),
    (select id from sets where short_name = 'dst'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'dst'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death Cloud'),
    (select id from sets where short_name = 'dst'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Auriok Glaivemaster'),
    (select id from sets where short_name = 'dst'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fangren Firstborn'),
    (select id from sets where short_name = 'dst'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chittering Rats'),
    (select id from sets where short_name = 'dst'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darksteel Forge'),
    (select id from sets where short_name = 'dst'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Essence Drain'),
    (select id from sets where short_name = 'dst'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carry Away'),
    (select id from sets where short_name = 'dst'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Specter''s Shroud'),
    (select id from sets where short_name = 'dst'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darksteel Pendant'),
    (select id from sets where short_name = 'dst'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Juggernaut'),
    (select id from sets where short_name = 'dst'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dismantle'),
    (select id from sets where short_name = 'dst'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Metal Fatigue'),
    (select id from sets where short_name = 'dst'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emissary of Despair'),
    (select id from sets where short_name = 'dst'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pristine Angel'),
    (select id from sets where short_name = 'dst'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voltaic Construct'),
    (select id from sets where short_name = 'dst'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcbound Hybrid'),
    (select id from sets where short_name = 'dst'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Archaeologist'),
    (select id from sets where short_name = 'dst'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcbound Bruiser'),
    (select id from sets where short_name = 'dst'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steelshaper Apprentice'),
    (select id from sets where short_name = 'dst'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heartseeker'),
    (select id from sets where short_name = 'dst'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leonin Bola'),
    (select id from sets where short_name = 'dst'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krark-Clan Stoker'),
    (select id from sets where short_name = 'dst'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tel-Jilad Wolf'),
    (select id from sets where short_name = 'dst'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whispersilk Cloak'),
    (select id from sets where short_name = 'dst'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vulshok Morningstar'),
    (select id from sets where short_name = 'dst'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Moonvessel'),
    (select id from sets where short_name = 'dst'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darksteel Ingot'),
    (select id from sets where short_name = 'dst'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spincrusher'),
    (select id from sets where short_name = 'dst'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Psychic Overload'),
    (select id from sets where short_name = 'dst'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pulse of the Grid'),
    (select id from sets where short_name = 'dst'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vulshok War Boar'),
    (select id from sets where short_name = 'dst'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pulse of the Dross'),
    (select id from sets where short_name = 'dst'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hunger of the Nim'),
    (select id from sets where short_name = 'dst'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coretapper'),
    (select id from sets where short_name = 'dst'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Purge'),
    (select id from sets where short_name = 'dst'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pulse of the Fields'),
    (select id from sets where short_name = 'dst'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blinkmoth Nexus'),
    (select id from sets where short_name = 'dst'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Greater Harvester'),
    (select id from sets where short_name = 'dst'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savage Beating'),
    (select id from sets where short_name = 'dst'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcbound Stinger'),
    (select id from sets where short_name = 'dst'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darksteel Citadel'),
    (select id from sets where short_name = 'dst'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hoverguard Observer'),
    (select id from sets where short_name = 'dst'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viridian Zealot'),
    (select id from sets where short_name = 'dst'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lich''s Tomb'),
    (select id from sets where short_name = 'dst'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Talon of Pain'),
    (select id from sets where short_name = 'dst'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darksteel Colossus'),
    (select id from sets where short_name = 'dst'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kraken''s Eye'),
    (select id from sets where short_name = 'dst'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mirrodin''s Core'),
    (select id from sets where short_name = 'dst'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Surestrike Trident'),
    (select id from sets where short_name = 'dst'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vedalken Engineer'),
    (select id from sets where short_name = 'dst'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcane Spyglass'),
    (select id from sets where short_name = 'dst'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chromescale Drake'),
    (select id from sets where short_name = 'dst'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unforge'),
    (select id from sets where short_name = 'dst'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burden of Greed'),
    (select id from sets where short_name = 'dst'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shield of Kaldra'),
    (select id from sets where short_name = 'dst'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trinisphere'),
    (select id from sets where short_name = 'dst'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chimeric Egg'),
    (select id from sets where short_name = 'dst'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nemesis Mask'),
    (select id from sets where short_name = 'dst'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aether Snap'),
    (select id from sets where short_name = 'dst'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Synod Artificer'),
    (select id from sets where short_name = 'dst'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slobad, Goblin Tinkerer'),
    (select id from sets where short_name = 'dst'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inflame'),
    (select id from sets where short_name = 'dst'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vex'),
    (select id from sets where short_name = 'dst'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Test of Faith'),
    (select id from sets where short_name = 'dst'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Neurok Transmuter'),
    (select id from sets where short_name = 'dst'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mycosynth Lattice'),
    (select id from sets where short_name = 'dst'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tangle Golem'),
    (select id from sets where short_name = 'dst'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pulse of the Forge'),
    (select id from sets where short_name = 'dst'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thought Dissector'),
    (select id from sets where short_name = 'dst'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eater of Days'),
    (select id from sets where short_name = 'dst'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Genesis Chamber'),
    (select id from sets where short_name = 'dst'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ageless Entity'),
    (select id from sets where short_name = 'dst'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emissary of Hope'),
    (select id from sets where short_name = 'dst'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Turn the Tables'),
    (select id from sets where short_name = 'dst'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Echoing Decay'),
    (select id from sets where short_name = 'dst'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infested Roothold'),
    (select id from sets where short_name = 'dst'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Neurok Prodigy'),
    (select id from sets where short_name = 'dst'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reap and Sow'),
    (select id from sets where short_name = 'dst'),
    '81',
    'common'
) 
 on conflict do nothing;
