insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Sengir Vampire'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sengir Vampire'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Demon of Loathing'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Demon of Loathing'),
        (select types.id from types where name = 'Demon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Loxodon Line Breaker'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Loxodon Line Breaker'),
        (select types.id from types where name = 'Elephant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Loxodon Line Breaker'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Malakir Cullblade'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Malakir Cullblade'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Malakir Cullblade'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Glint'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Burn Bright'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Frilled Sea Serpent'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Frilled Sea Serpent'),
        (select types.id from types where name = 'Serpent')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Affectionate Indrik'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Affectionate Indrik'),
        (select types.id from types where name = 'Beast')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tactical Advantage'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Murder'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soulhunter Rakshasa'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soulhunter Rakshasa'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soulhunter Rakshasa'),
        (select types.id from types where name = 'Demon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sentinel Spider'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sentinel Spider'),
        (select types.id from types where name = 'Spider')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Krovikan Scoundrel'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Krovikan Scoundrel'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Krovikan Scoundrel'),
        (select types.id from types where name = 'Rogue')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Impassioned Orator'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Impassioned Orator'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Impassioned Orator'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Treetop Warden'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Treetop Warden'),
        (select types.id from types where name = 'Elf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Treetop Warden'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Titanic Growth'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Swamp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Angel of Vitality'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Angel of Vitality'),
        (select types.id from types where name = 'Angel')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tin Street Cadet'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tin Street Cadet'),
        (select types.id from types where name = 'Goblin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hallowed Priest'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hallowed Priest'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hallowed Priest'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Riddlemaster Sphinx'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Riddlemaster Sphinx'),
        (select types.id from types where name = 'Sphinx')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Moorland Inquisitor'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Moorland Inquisitor'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Moorland Inquisitor'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Mountain')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cruel Cut'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Winged Words'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Epic Proportions'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Epic Proportions'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soulblade Djinn'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soulblade Djinn'),
        (select types.id from types where name = 'Djinn')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'World Shaper'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'World Shaper'),
        (select types.id from types where name = 'Merfolk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'World Shaper'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Angelic Guardian'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Angelic Guardian'),
        (select types.id from types where name = 'Angel')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goblin Trashmaster'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goblin Trashmaster'),
        (select types.id from types where name = 'Goblin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goblin Trashmaster'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rabid Bite'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Colossal Majesty'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Armored Whirl Turtle'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Armored Whirl Turtle'),
        (select types.id from types where name = 'Turtle')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goring Ceratops'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goring Ceratops'),
        (select types.id from types where name = 'Dinosaur')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Coral Merfolk'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Coral Merfolk'),
        (select types.id from types where name = 'Merfolk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nest Robber'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nest Robber'),
        (select types.id from types where name = 'Dinosaur')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Woodland Mystic'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Woodland Mystic'),
        (select types.id from types where name = 'Elf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Woodland Mystic'),
        (select types.id from types where name = 'Druid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ilysian Caryatid'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ilysian Caryatid'),
        (select types.id from types where name = 'Plant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stony Strength'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Unlikely Aid'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Waterknot'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Waterknot'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wall of Runes'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wall of Runes'),
        (select types.id from types where name = 'Wall')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Molten Ravager'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Molten Ravager'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Greenwood Sentinel'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Greenwood Sentinel'),
        (select types.id from types where name = 'Elf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Greenwood Sentinel'),
        (select types.id from types where name = 'Scout')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jungle Delver'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jungle Delver'),
        (select types.id from types where name = 'Merfolk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jungle Delver'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sanitarium Skeleton'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sanitarium Skeleton'),
        (select types.id from types where name = 'Skeleton')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goblin Gathering'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gigantosaurus'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gigantosaurus'),
        (select types.id from types where name = 'Dinosaur')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sanctuary Cat'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sanctuary Cat'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Baloth Packhunter'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Baloth Packhunter'),
        (select types.id from types where name = 'Beast')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zephyr Gull'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zephyr Gull'),
        (select types.id from types where name = 'Bird')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Charmed Stray'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Charmed Stray'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Charging Badger'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Charging Badger'),
        (select types.id from types where name = 'Badger')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Siege Dragon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Siege Dragon'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Plains')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shrine Keeper'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shrine Keeper'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shrine Keeper'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nimble Pilferer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nimble Pilferer'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nimble Pilferer'),
        (select types.id from types where name = 'Rogue')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Generous Stray'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Generous Stray'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Prized Unicorn'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Prized Unicorn'),
        (select types.id from types where name = 'Unicorn')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Immortal Phoenix'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Immortal Phoenix'),
        (select types.id from types where name = 'Phoenix')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Unsummon'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rampaging Brontodon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rampaging Brontodon'),
        (select types.id from types where name = 'Dinosaur')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fencing Ace'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fencing Ace'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fencing Ace'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Island')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spiritual Guardian'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spiritual Guardian'),
        (select types.id from types where name = 'Spirit')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Forest')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Overflowing Insight'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wildwood Patrol'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wildwood Patrol'),
        (select types.id from types where name = 'Centaur')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wildwood Patrol'),
        (select types.id from types where name = 'Scout')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Confront the Assault'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Windreader Sphinx'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Windreader Sphinx'),
        (select types.id from types where name = 'Sphinx')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Volcanic Dragon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Volcanic Dragon'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Savage Gorger'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Savage Gorger'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Feral Roar'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skeleton Archer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skeleton Archer'),
        (select types.id from types where name = 'Skeleton')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skeleton Archer'),
        (select types.id from types where name = 'Archer')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Leonin Warleader'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Leonin Warleader'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Leonin Warleader'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bond of Discipline'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sleep'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vampire Opportunist'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vampire Opportunist'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hurloon Minotaur'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hurloon Minotaur'),
        (select types.id from types where name = 'Minotaur')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pacifism'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pacifism'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Angelic Reward'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Angelic Reward'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sworn Guardian'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sworn Guardian'),
        (select types.id from types where name = 'Merfolk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sworn Guardian'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eternal Thirst'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eternal Thirst'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Inescapable Blaze'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Raise Dead'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ogre Battledriver'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ogre Battledriver'),
        (select types.id from types where name = 'Ogre')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ogre Battledriver'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bombard'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Windstorm Drake'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Windstorm Drake'),
        (select types.id from types where name = 'Drake')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Maniacal Rage'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Maniacal Rage'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Octoprophet'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Octoprophet'),
        (select types.id from types where name = 'Octopus')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goblin Gang Leader'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goblin Gang Leader'),
        (select types.id from types where name = 'Goblin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goblin Gang Leader'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Warden of Evos Isle'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Warden of Evos Isle'),
        (select types.id from types where name = 'Bird')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Warden of Evos Isle'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rumbling Baloth'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rumbling Baloth'),
        (select types.id from types where name = 'Beast')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Raging Goblin'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Raging Goblin'),
        (select types.id from types where name = 'Goblin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Raging Goblin'),
        (select types.id from types where name = 'Berserker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bad Deal'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Reduce to Ashes'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Raid Bombardment'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Typhoid Rats'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Typhoid Rats'),
        (select types.id from types where name = 'Rat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Air Elemental'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Air Elemental'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shock'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Serra Angel'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Serra Angel'),
        (select types.id from types where name = 'Angel')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Witch''s Familiar'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Witch''s Familiar'),
        (select types.id from types where name = 'Frog')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goblin Tunneler'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goblin Tunneler'),
        (select types.id from types where name = 'Goblin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goblin Tunneler'),
        (select types.id from types where name = 'Rogue')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Knight''s Pledge'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Knight''s Pledge'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Scathe Zombies'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Scathe Zombies'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'River''s Favor'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'River''s Favor'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nightmare'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nightmare'),
        (select types.id from types where name = 'Nightmare')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nightmare'),
        (select types.id from types where name = 'Horse')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fearless Halberdier'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fearless Halberdier'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fearless Halberdier'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Inspiring Commander'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Inspiring Commander'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Inspiring Commander'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soulmender'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soulmender'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soulmender'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Evolving Wilds'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Compound Fracture'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cloudkin Seer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cloudkin Seer'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cloudkin Seer'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mardu Outrider'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mardu Outrider'),
        (select types.id from types where name = 'Orc')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mardu Outrider'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Waterkin Shaman'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Waterkin Shaman'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Waterkin Shaman'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Storm Strike'),
        (select types.id from types where name = 'Instant')
    ) 
 on conflict do nothing;
