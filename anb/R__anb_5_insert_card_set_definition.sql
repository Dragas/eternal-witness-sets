insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = 'anb'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demon of Loathing'),
    (select id from sets where short_name = 'anb'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loxodon Line Breaker'),
    (select id from sets where short_name = 'anb'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Malakir Cullblade'),
    (select id from sets where short_name = 'anb'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glint'),
    (select id from sets where short_name = 'anb'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burn Bright'),
    (select id from sets where short_name = 'anb'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frilled Sea Serpent'),
    (select id from sets where short_name = 'anb'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Affectionate Indrik'),
    (select id from sets where short_name = 'anb'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tactical Advantage'),
    (select id from sets where short_name = 'anb'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murder'),
    (select id from sets where short_name = 'anb'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soulhunter Rakshasa'),
    (select id from sets where short_name = 'anb'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sentinel Spider'),
    (select id from sets where short_name = 'anb'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krovikan Scoundrel'),
    (select id from sets where short_name = 'anb'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Impassioned Orator'),
    (select id from sets where short_name = 'anb'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treetop Warden'),
    (select id from sets where short_name = 'anb'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Titanic Growth'),
    (select id from sets where short_name = 'anb'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'anb'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Vitality'),
    (select id from sets where short_name = 'anb'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tin Street Cadet'),
    (select id from sets where short_name = 'anb'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hallowed Priest'),
    (select id from sets where short_name = 'anb'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Riddlemaster Sphinx'),
    (select id from sets where short_name = 'anb'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moorland Inquisitor'),
    (select id from sets where short_name = 'anb'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'anb'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cruel Cut'),
    (select id from sets where short_name = 'anb'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winged Words'),
    (select id from sets where short_name = 'anb'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Epic Proportions'),
    (select id from sets where short_name = 'anb'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soulblade Djinn'),
    (select id from sets where short_name = 'anb'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'World Shaper'),
    (select id from sets where short_name = 'anb'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angelic Guardian'),
    (select id from sets where short_name = 'anb'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Trashmaster'),
    (select id from sets where short_name = 'anb'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rabid Bite'),
    (select id from sets where short_name = 'anb'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Colossal Majesty'),
    (select id from sets where short_name = 'anb'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armored Whirl Turtle'),
    (select id from sets where short_name = 'anb'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goring Ceratops'),
    (select id from sets where short_name = 'anb'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coral Merfolk'),
    (select id from sets where short_name = 'anb'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nest Robber'),
    (select id from sets where short_name = 'anb'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woodland Mystic'),
    (select id from sets where short_name = 'anb'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ilysian Caryatid'),
    (select id from sets where short_name = 'anb'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stony Strength'),
    (select id from sets where short_name = 'anb'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unlikely Aid'),
    (select id from sets where short_name = 'anb'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Waterknot'),
    (select id from sets where short_name = 'anb'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Runes'),
    (select id from sets where short_name = 'anb'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molten Ravager'),
    (select id from sets where short_name = 'anb'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greenwood Sentinel'),
    (select id from sets where short_name = 'anb'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Delver'),
    (select id from sets where short_name = 'anb'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanitarium Skeleton'),
    (select id from sets where short_name = 'anb'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Gathering'),
    (select id from sets where short_name = 'anb'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gigantosaurus'),
    (select id from sets where short_name = 'anb'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sanctuary Cat'),
    (select id from sets where short_name = 'anb'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baloth Packhunter'),
    (select id from sets where short_name = 'anb'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zephyr Gull'),
    (select id from sets where short_name = 'anb'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charmed Stray'),
    (select id from sets where short_name = 'anb'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charging Badger'),
    (select id from sets where short_name = 'anb'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Siege Dragon'),
    (select id from sets where short_name = 'anb'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'anb'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shrine Keeper'),
    (select id from sets where short_name = 'anb'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nimble Pilferer'),
    (select id from sets where short_name = 'anb'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Generous Stray'),
    (select id from sets where short_name = 'anb'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prized Unicorn'),
    (select id from sets where short_name = 'anb'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Immortal Phoenix'),
    (select id from sets where short_name = 'anb'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = 'anb'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampaging Brontodon'),
    (select id from sets where short_name = 'anb'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fencing Ace'),
    (select id from sets where short_name = 'anb'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'anb'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spiritual Guardian'),
    (select id from sets where short_name = 'anb'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'anb'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overflowing Insight'),
    (select id from sets where short_name = 'anb'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wildwood Patrol'),
    (select id from sets where short_name = 'anb'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Confront the Assault'),
    (select id from sets where short_name = 'anb'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Windreader Sphinx'),
    (select id from sets where short_name = 'anb'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Dragon'),
    (select id from sets where short_name = 'anb'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savage Gorger'),
    (select id from sets where short_name = 'anb'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feral Roar'),
    (select id from sets where short_name = 'anb'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skeleton Archer'),
    (select id from sets where short_name = 'anb'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Warleader'),
    (select id from sets where short_name = 'anb'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bond of Discipline'),
    (select id from sets where short_name = 'anb'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sleep'),
    (select id from sets where short_name = 'anb'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampire Opportunist'),
    (select id from sets where short_name = 'anb'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hurloon Minotaur'),
    (select id from sets where short_name = 'anb'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'anb'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Reward'),
    (select id from sets where short_name = 'anb'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sworn Guardian'),
    (select id from sets where short_name = 'anb'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eternal Thirst'),
    (select id from sets where short_name = 'anb'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inescapable Blaze'),
    (select id from sets where short_name = 'anb'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = 'anb'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ogre Battledriver'),
    (select id from sets where short_name = 'anb'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bombard'),
    (select id from sets where short_name = 'anb'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windstorm Drake'),
    (select id from sets where short_name = 'anb'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maniacal Rage'),
    (select id from sets where short_name = 'anb'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Octoprophet'),
    (select id from sets where short_name = 'anb'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Gang Leader'),
    (select id from sets where short_name = 'anb'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warden of Evos Isle'),
    (select id from sets where short_name = 'anb'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rumbling Baloth'),
    (select id from sets where short_name = 'anb'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raging Goblin'),
    (select id from sets where short_name = 'anb'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bad Deal'),
    (select id from sets where short_name = 'anb'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reduce to Ashes'),
    (select id from sets where short_name = 'anb'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raid Bombardment'),
    (select id from sets where short_name = 'anb'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Typhoid Rats'),
    (select id from sets where short_name = 'anb'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = 'anb'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'anb'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'anb'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Witch''s Familiar'),
    (select id from sets where short_name = 'anb'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Tunneler'),
    (select id from sets where short_name = 'anb'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight''s Pledge'),
    (select id from sets where short_name = 'anb'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scathe Zombies'),
    (select id from sets where short_name = 'anb'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'River''s Favor'),
    (select id from sets where short_name = 'anb'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = 'anb'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fearless Halberdier'),
    (select id from sets where short_name = 'anb'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inspiring Commander'),
    (select id from sets where short_name = 'anb'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soulmender'),
    (select id from sets where short_name = 'anb'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'anb'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Compound Fracture'),
    (select id from sets where short_name = 'anb'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudkin Seer'),
    (select id from sets where short_name = 'anb'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mardu Outrider'),
    (select id from sets where short_name = 'anb'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Waterkin Shaman'),
    (select id from sets where short_name = 'anb'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Storm Strike'),
    (select id from sets where short_name = 'anb'),
    '86',
    'common'
) 
 on conflict do nothing;
