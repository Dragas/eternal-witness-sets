insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Glorious Anthem'),
    (select id from sets where short_name = 'pjse'),
    '1E08',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shard Phoenix'),
    (select id from sets where short_name = 'pjse'),
    '2E06',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Royal Assassin'),
    (select id from sets where short_name = 'pjse'),
    '2E05',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soltari Priest'),
    (select id from sets where short_name = 'pjse'),
    '1E07',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Elder'),
    (select id from sets where short_name = 'pjse'),
    '1E06',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Champion'),
    (select id from sets where short_name = 'pjse'),
    '2E08',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whirling Dervish'),
    (select id from sets where short_name = 'pjse'),
    '2E07',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slith Firewalker'),
    (select id from sets where short_name = 'pjse'),
    '1E05',
    'rare'
) 
 on conflict do nothing;
