insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Vampire // Treasure'),
    (select id from sets where short_name = 'f17'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dinosaur // Treasure'),
    (select id from sets where short_name = 'f17'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Servo Exhibition'),
    (select id from sets where short_name = 'f17'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noose Constrictor'),
    (select id from sets where short_name = 'f17'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pirate // Treasure'),
    (select id from sets where short_name = 'f17'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unlicensed Disintegration'),
    (select id from sets where short_name = 'f17'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fatal Push'),
    (select id from sets where short_name = 'f17'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Renegade Rallier'),
    (select id from sets where short_name = 'f17'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reverse Engineer'),
    (select id from sets where short_name = 'f17'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aether Hub'),
    (select id from sets where short_name = 'f17'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Incendiary Flow'),
    (select id from sets where short_name = 'f17'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fortune''s Favor'),
    (select id from sets where short_name = 'f17'),
    '2',
    'rare'
) 
 on conflict do nothing;
