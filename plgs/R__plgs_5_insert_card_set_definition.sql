insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Hangarback Walker'),
    (select id from sets where short_name = 'plgs'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reliquary Tower'),
    (select id from sets where short_name = 'plgs'),
    '1',
    'rare'
) 
 on conflict do nothing;
