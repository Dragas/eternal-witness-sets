    insert into types (name) values('Artifact') on conflict do nothing;
    insert into types (name) values('Creature') on conflict do nothing;
    insert into types (name) values('Construct') on conflict do nothing;
    insert into types (name) values('Land') on conflict do nothing;
