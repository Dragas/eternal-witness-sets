insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Melek, Izzet Paragon'),
    (select id from sets where short_name = 'pdgm'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trostani''s Summoner'),
    (select id from sets where short_name = 'pdgm'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Render Silent'),
    (select id from sets where short_name = 'pdgm'),
    '*96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Breaking // Entering'),
    (select id from sets where short_name = 'pdgm'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maze''s End'),
    (select id from sets where short_name = 'pdgm'),
    '152',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pdgm'),
    '157',
    'common'
) 
 on conflict do nothing;
