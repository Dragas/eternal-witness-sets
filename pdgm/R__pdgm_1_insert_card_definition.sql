    insert into mtgcard(name) values ('Melek, Izzet Paragon') on conflict do nothing;
    insert into mtgcard(name) values ('Trostani''s Summoner') on conflict do nothing;
    insert into mtgcard(name) values ('Render Silent') on conflict do nothing;
    insert into mtgcard(name) values ('Breaking // Entering') on conflict do nothing;
    insert into mtgcard(name) values ('Maze''s End') on conflict do nothing;
    insert into mtgcard(name) values ('Plains') on conflict do nothing;
