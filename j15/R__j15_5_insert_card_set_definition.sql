insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Wasteland'),
    (select id from sets where short_name = 'j15'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dualcaster Mage'),
    (select id from sets where short_name = 'j15'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shardless Agent'),
    (select id from sets where short_name = 'j15'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rishadan Port'),
    (select id from sets where short_name = 'j15'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temporal Manipulation'),
    (select id from sets where short_name = 'j15'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Damnation'),
    (select id from sets where short_name = 'j15'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feldon of the Third Path'),
    (select id from sets where short_name = 'j15'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ravages of War'),
    (select id from sets where short_name = 'j15'),
    '4',
    'rare'
) 
 on conflict do nothing;
