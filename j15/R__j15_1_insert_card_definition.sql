    insert into mtgcard(name) values ('Wasteland') on conflict do nothing;
    insert into mtgcard(name) values ('Dualcaster Mage') on conflict do nothing;
    insert into mtgcard(name) values ('Shardless Agent') on conflict do nothing;
    insert into mtgcard(name) values ('Rishadan Port') on conflict do nothing;
    insert into mtgcard(name) values ('Temporal Manipulation') on conflict do nothing;
    insert into mtgcard(name) values ('Damnation') on conflict do nothing;
    insert into mtgcard(name) values ('Feldon of the Third Path') on conflict do nothing;
    insert into mtgcard(name) values ('Ravages of War') on conflict do nothing;
