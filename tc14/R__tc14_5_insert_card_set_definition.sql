insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Elf Druid'),
    (select id from sets where short_name = 'tc14'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tc14'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pentavite'),
    (select id from sets where short_name = 'tc14'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tc14'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pegasus'),
    (select id from sets where short_name = 'tc14'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurm'),
    (select id from sets where short_name = 'tc14'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fish'),
    (select id from sets where short_name = 'tc14'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tuktuk the Returned'),
    (select id from sets where short_name = 'tc14'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treefolk'),
    (select id from sets where short_name = 'tc14'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daretti, Scrap Savant Emblem'),
    (select id from sets where short_name = 'tc14'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tc14'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis of the Black Oath Emblem'),
    (select id from sets where short_name = 'tc14'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat'),
    (select id from sets where short_name = 'tc14'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stoneforged Blade'),
    (select id from sets where short_name = 'tc14'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goat'),
    (select id from sets where short_name = 'tc14'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tc14'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tc14'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ape'),
    (select id from sets where short_name = 'tc14'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon'),
    (select id from sets where short_name = 'tc14'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'tc14'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elephant'),
    (select id from sets where short_name = 'tc14'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elf Warrior'),
    (select id from sets where short_name = 'tc14'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tc14'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kraken'),
    (select id from sets where short_name = 'tc14'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurm'),
    (select id from sets where short_name = 'tc14'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tc14'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon'),
    (select id from sets where short_name = 'tc14'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whale'),
    (select id from sets where short_name = 'tc14'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teferi, Temporal Archmage Emblem'),
    (select id from sets where short_name = 'tc14'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gargoyle'),
    (select id from sets where short_name = 'tc14'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tc14'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Soldier'),
    (select id from sets where short_name = 'tc14'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr'),
    (select id from sets where short_name = 'tc14'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'tc14'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Germ'),
    (select id from sets where short_name = 'tc14'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horror'),
    (select id from sets where short_name = 'tc14'),
    '15',
    'common'
) 
 on conflict do nothing;
