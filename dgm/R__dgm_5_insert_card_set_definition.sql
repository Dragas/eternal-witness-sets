insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Golgari Guildgate'),
    (select id from sets where short_name = 'dgm'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flesh // Blood'),
    (select id from sets where short_name = 'dgm'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voice of Resurgence'),
    (select id from sets where short_name = 'dgm'),
    '114',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Azorius Cluestone'),
    (select id from sets where short_name = 'dgm'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunspire Gatekeepers'),
    (select id from sets where short_name = 'dgm'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maze Behemoth'),
    (select id from sets where short_name = 'dgm'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Putrefy'),
    (select id from sets where short_name = 'dgm'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Cluestone'),
    (select id from sets where short_name = 'dgm'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Profit // Loss'),
    (select id from sets where short_name = 'dgm'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plasm Capture'),
    (select id from sets where short_name = 'dgm'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Restore the Peace'),
    (select id from sets where short_name = 'dgm'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feral Animist'),
    (select id from sets where short_name = 'dgm'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woodlot Crawler'),
    (select id from sets where short_name = 'dgm'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warped Physique'),
    (select id from sets where short_name = 'dgm'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Guildgate'),
    (select id from sets where short_name = 'dgm'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Toil // Trouble'),
    (select id from sets where short_name = 'dgm'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bane Alley Blackguard'),
    (select id from sets where short_name = 'dgm'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Guildgate'),
    (select id from sets where short_name = 'dgm'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trostani''s Summoner'),
    (select id from sets where short_name = 'dgm'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viashino Firstblade'),
    (select id from sets where short_name = 'dgm'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beetleform Mage'),
    (select id from sets where short_name = 'dgm'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zhur-Taa Ancient'),
    (select id from sets where short_name = 'dgm'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carnage Gladiator'),
    (select id from sets where short_name = 'dgm'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hidden Strings'),
    (select id from sets where short_name = 'dgm'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ready // Willing'),
    (select id from sets where short_name = 'dgm'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drown in Filth'),
    (select id from sets where short_name = 'dgm'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Renegade Krasis'),
    (select id from sets where short_name = 'dgm'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruul Guildgate'),
    (select id from sets where short_name = 'dgm'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadbridge Chant'),
    (select id from sets where short_name = 'dgm'),
    '63',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Boros Mastiff'),
    (select id from sets where short_name = 'dgm'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bronzebeak Moa'),
    (select id from sets where short_name = 'dgm'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Catch // Release'),
    (select id from sets where short_name = 'dgm'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savageborn Hydra'),
    (select id from sets where short_name = 'dgm'),
    '100',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Showstopper'),
    (select id from sets where short_name = 'dgm'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sin Collector'),
    (select id from sets where short_name = 'dgm'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pilfered Plans'),
    (select id from sets where short_name = 'dgm'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Cluestone'),
    (select id from sets where short_name = 'dgm'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unflinching Courage'),
    (select id from sets where short_name = 'dgm'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sire of Insanity'),
    (select id from sets where short_name = 'dgm'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Progenitor Mimic'),
    (select id from sets where short_name = 'dgm'),
    '92',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scion of Vitu-Ghazi'),
    (select id from sets where short_name = 'dgm'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wake the Reflections'),
    (select id from sets where short_name = 'dgm'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Melek, Izzet Paragon'),
    (select id from sets where short_name = 'dgm'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maze''s End'),
    (select id from sets where short_name = 'dgm'),
    '152',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Punish the Enemy'),
    (select id from sets where short_name = 'dgm'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bred for the Hunt'),
    (select id from sets where short_name = 'dgm'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Haunter of Nightveil'),
    (select id from sets where short_name = 'dgm'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Runner''s Bane'),
    (select id from sets where short_name = 'dgm'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saruli Gatekeepers'),
    (select id from sets where short_name = 'dgm'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orzhov Cluestone'),
    (select id from sets where short_name = 'dgm'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deputy of Acquittals'),
    (select id from sets where short_name = 'dgm'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Varolz, the Scar-Striped'),
    (select id from sets where short_name = 'dgm'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Riot Piker'),
    (select id from sets where short_name = 'dgm'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skylasher'),
    (select id from sets where short_name = 'dgm'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jelenn Sphinx'),
    (select id from sets where short_name = 'dgm'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mindstatic'),
    (select id from sets where short_name = 'dgm'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nivix Cyclops'),
    (select id from sets where short_name = 'dgm'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battering Krasis'),
    (select id from sets where short_name = 'dgm'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blaze Commando'),
    (select id from sets where short_name = 'dgm'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steeple Roc'),
    (select id from sets where short_name = 'dgm'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Debt to the Deathless'),
    (select id from sets where short_name = 'dgm'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hired Torturer'),
    (select id from sets where short_name = 'dgm'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azorius Guildgate'),
    (select id from sets where short_name = 'dgm'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Breaking // Entering'),
    (select id from sets where short_name = 'dgm'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Legion''s Initiative'),
    (select id from sets where short_name = 'dgm'),
    '81',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dragonshift'),
    (select id from sets where short_name = 'dgm'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Battleshaper'),
    (select id from sets where short_name = 'dgm'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exava, Rakdos Blood Witch'),
    (select id from sets where short_name = 'dgm'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Korozda Gorgon'),
    (select id from sets where short_name = 'dgm'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Notion Thief'),
    (select id from sets where short_name = 'dgm'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pontiff of Blight'),
    (select id from sets where short_name = 'dgm'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phytoburst'),
    (select id from sets where short_name = 'dgm'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lyev Decree'),
    (select id from sets where short_name = 'dgm'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maze Sentinel'),
    (select id from sets where short_name = 'dgm'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Morgue Burst'),
    (select id from sets where short_name = 'dgm'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Master of Cruelties'),
    (select id from sets where short_name = 'dgm'),
    '82',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fluxcharger'),
    (select id from sets where short_name = 'dgm'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sinister Possession'),
    (select id from sets where short_name = 'dgm'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lavinia of the Tenth'),
    (select id from sets where short_name = 'dgm'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warleader''s Helix'),
    (select id from sets where short_name = 'dgm'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smelt-Ward Gatekeepers'),
    (select id from sets where short_name = 'dgm'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruric Thar, the Unbowed'),
    (select id from sets where short_name = 'dgm'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Possibility Storm'),
    (select id from sets where short_name = 'dgm'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Haazda Snare Squad'),
    (select id from sets where short_name = 'dgm'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maze Rusher'),
    (select id from sets where short_name = 'dgm'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wind Drake'),
    (select id from sets where short_name = 'dgm'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boros Cluestone'),
    (select id from sets where short_name = 'dgm'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clear a Path'),
    (select id from sets where short_name = 'dgm'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyrewild Shaman'),
    (select id from sets where short_name = 'dgm'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Izzet Cluestone'),
    (select id from sets where short_name = 'dgm'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Turn // Burn'),
    (select id from sets where short_name = 'dgm'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krasis Incubation'),
    (select id from sets where short_name = 'dgm'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orzhov Guildgate'),
    (select id from sets where short_name = 'dgm'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Guildgate'),
    (select id from sets where short_name = 'dgm'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dimir Guildgate'),
    (select id from sets where short_name = 'dgm'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fatal Fumes'),
    (select id from sets where short_name = 'dgm'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gaze of Granite'),
    (select id from sets where short_name = 'dgm'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Murmuring Phantasm'),
    (select id from sets where short_name = 'dgm'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Give // Take'),
    (select id from sets where short_name = 'dgm'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Protect // Serve'),
    (select id from sets where short_name = 'dgm'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aetherling'),
    (select id from sets where short_name = 'dgm'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uncovered Clues'),
    (select id from sets where short_name = 'dgm'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Scrivener'),
    (select id from sets where short_name = 'dgm'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Cluestone'),
    (select id from sets where short_name = 'dgm'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beck // Call'),
    (select id from sets where short_name = 'dgm'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Test Pilot'),
    (select id from sets where short_name = 'dgm'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scab-Clan Giant'),
    (select id from sets where short_name = 'dgm'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ascended Lawmage'),
    (select id from sets where short_name = 'dgm'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blast of Genius'),
    (select id from sets where short_name = 'dgm'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Obzedat''s Aid'),
    (select id from sets where short_name = 'dgm'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Drake'),
    (select id from sets where short_name = 'dgm'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul Cluestone'),
    (select id from sets where short_name = 'dgm'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rubblebelt Maaka'),
    (select id from sets where short_name = 'dgm'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Renounce the Guilds'),
    (select id from sets where short_name = 'dgm'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tajic, Blade of the Legion'),
    (select id from sets where short_name = 'dgm'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mending Touch'),
    (select id from sets where short_name = 'dgm'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Down // Dirty'),
    (select id from sets where short_name = 'dgm'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crypt Incursion'),
    (select id from sets where short_name = 'dgm'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reap Intellect'),
    (select id from sets where short_name = 'dgm'),
    '95',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Emmara Tandris'),
    (select id from sets where short_name = 'dgm'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teysa, Envoy of Ghosts'),
    (select id from sets where short_name = 'dgm'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maw of the Obzedat'),
    (select id from sets where short_name = 'dgm'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spike Jester'),
    (select id from sets where short_name = 'dgm'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Far // Away'),
    (select id from sets where short_name = 'dgm'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wear // Tear'),
    (select id from sets where short_name = 'dgm'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maze Glider'),
    (select id from sets where short_name = 'dgm'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirko Vosk, Mind Drinker'),
    (select id from sets where short_name = 'dgm'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Riot Control'),
    (select id from sets where short_name = 'dgm'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mutant''s Prey'),
    (select id from sets where short_name = 'dgm'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Council of the Absolute'),
    (select id from sets where short_name = 'dgm'),
    '62',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vorel of the Hull Clade'),
    (select id from sets where short_name = 'dgm'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alive // Well'),
    (select id from sets where short_name = 'dgm'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maze Abomination'),
    (select id from sets where short_name = 'dgm'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul War Chant'),
    (select id from sets where short_name = 'dgm'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Species Gorger'),
    (select id from sets where short_name = 'dgm'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tithe Drinker'),
    (select id from sets where short_name = 'dgm'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trait Doctoring'),
    (select id from sets where short_name = 'dgm'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Cluestone'),
    (select id from sets where short_name = 'dgm'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Render Silent'),
    (select id from sets where short_name = 'dgm'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armored Wolf-Rider'),
    (select id from sets where short_name = 'dgm'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Baron of Vizkopa'),
    (select id from sets where short_name = 'dgm'),
    '57',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gleam of Battle'),
    (select id from sets where short_name = 'dgm'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Guildgate'),
    (select id from sets where short_name = 'dgm'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Awe for the Guilds'),
    (select id from sets where short_name = 'dgm'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rot Farm Skeleton'),
    (select id from sets where short_name = 'dgm'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weapon Surge'),
    (select id from sets where short_name = 'dgm'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Advent of the Wurm'),
    (select id from sets where short_name = 'dgm'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Opal Lake Gatekeepers'),
    (select id from sets where short_name = 'dgm'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ubul Sar Gatekeepers'),
    (select id from sets where short_name = 'dgm'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kraul Warrior'),
    (select id from sets where short_name = 'dgm'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ral Zarek'),
    (select id from sets where short_name = 'dgm'),
    '94',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Zhur-Taa Druid'),
    (select id from sets where short_name = 'dgm'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simic Guildgate'),
    (select id from sets where short_name = 'dgm'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golgari Cluestone'),
    (select id from sets where short_name = 'dgm'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armed // Dangerous'),
    (select id from sets where short_name = 'dgm'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thrashing Mossdog'),
    (select id from sets where short_name = 'dgm'),
    '50',
    'common'
) 
 on conflict do nothing;
