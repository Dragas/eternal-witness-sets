insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Remand'),
    (select id from sets where short_name = 'ddm'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Corpse Traders'),
    (select id from sets where short_name = 'ddm'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reaper of the Wilds'),
    (select id from sets where short_name = 'ddm'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stonefare Crocodile'),
    (select id from sets where short_name = 'ddm'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddm'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thought Scour'),
    (select id from sets where short_name = 'ddm'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stab Wound'),
    (select id from sets where short_name = 'ddm'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marsh Casualties'),
    (select id from sets where short_name = 'ddm'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddm'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spelltwine'),
    (select id from sets where short_name = 'ddm'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddm'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tainted Wood'),
    (select id from sets where short_name = 'ddm'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aether Figment'),
    (select id from sets where short_name = 'ddm'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aether Adept'),
    (select id from sets where short_name = 'ddm'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Control Magic'),
    (select id from sets where short_name = 'ddm'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Night''s Whisper'),
    (select id from sets where short_name = 'ddm'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vraska the Unseen'),
    (select id from sets where short_name = 'ddm'),
    '42',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aeon Chronicler'),
    (select id from sets where short_name = 'ddm'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Oracle'),
    (select id from sets where short_name = 'ddm'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Festerhide Boar'),
    (select id from sets where short_name = 'ddm'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nekrataal'),
    (select id from sets where short_name = 'ddm'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddm'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Highway Robber'),
    (select id from sets where short_name = 'ddm'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddm'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leyline Phantom'),
    (select id from sets where short_name = 'ddm'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddm'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddm'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dream Stalker'),
    (select id from sets where short_name = 'ddm'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk Wayfinder'),
    (select id from sets where short_name = 'ddm'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Claustrophobia'),
    (select id from sets where short_name = 'ddm'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grisly Spectacle'),
    (select id from sets where short_name = 'ddm'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace''s Ingenuity'),
    (select id from sets where short_name = 'ddm'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Consume Strength'),
    (select id from sets where short_name = 'ddm'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Dragon'),
    (select id from sets where short_name = 'ddm'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief Recluse'),
    (select id from sets where short_name = 'ddm'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddm'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace, Architect of Thought'),
    (select id from sets where short_name = 'ddm'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddm'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddm'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Summoner''s Bane'),
    (select id from sets where short_name = 'ddm'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Last Kiss'),
    (select id from sets where short_name = 'ddm'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spawnwrithe'),
    (select id from sets where short_name = 'ddm'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prohibit'),
    (select id from sets where short_name = 'ddm'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krovikan Mist'),
    (select id from sets where short_name = 'ddm'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Cloud'),
    (select id from sets where short_name = 'ddm'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pulse Tracker'),
    (select id from sets where short_name = 'ddm'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Body Double'),
    (select id from sets where short_name = 'ddm'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Guildgate'),
    (select id from sets where short_name = 'ddm'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddm'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death-Hood Cobra'),
    (select id from sets where short_name = 'ddm'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ohran Viper'),
    (select id from sets where short_name = 'ddm'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Griptide'),
    (select id from sets where short_name = 'ddm'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wight of Precinct Six'),
    (select id from sets where short_name = 'ddm'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Riftwing Cloudskate'),
    (select id from sets where short_name = 'ddm'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chronomaton'),
    (select id from sets where short_name = 'ddm'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ray of Command'),
    (select id from sets where short_name = 'ddm'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stealer of Secrets'),
    (select id from sets where short_name = 'ddm'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Memory Lapse'),
    (select id from sets where short_name = 'ddm'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tragic Slip'),
    (select id from sets where short_name = 'ddm'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasured Find'),
    (select id from sets where short_name = 'ddm'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Archaeomancer'),
    (select id from sets where short_name = 'ddm'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Acidic Slime'),
    (select id from sets where short_name = 'ddm'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mold Shambler'),
    (select id from sets where short_name = 'ddm'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slate Street Ruffian'),
    (select id from sets where short_name = 'ddm'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Into the Roil'),
    (select id from sets where short_name = 'ddm'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddm'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace''s Mindseeker'),
    (select id from sets where short_name = 'ddm'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddm'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Agoraphobia'),
    (select id from sets where short_name = 'ddm'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddm'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vinelasher Kudzu'),
    (select id from sets where short_name = 'ddm'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Underworld Connections'),
    (select id from sets where short_name = 'ddm'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Future Sight'),
    (select id from sets where short_name = 'ddm'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Putrid Leech'),
    (select id from sets where short_name = 'ddm'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rogue''s Passage'),
    (select id from sets where short_name = 'ddm'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tavern Swindler'),
    (select id from sets where short_name = 'ddm'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Halimar Depths'),
    (select id from sets where short_name = 'ddm'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crosstown Courier'),
    (select id from sets where short_name = 'ddm'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drooling Groodion'),
    (select id from sets where short_name = 'ddm'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sadistic Augermage'),
    (select id from sets where short_name = 'ddm'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gatecreeper Vine'),
    (select id from sets where short_name = 'ddm'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shadow Alley Denizen'),
    (select id from sets where short_name = 'ddm'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'River Boa'),
    (select id from sets where short_name = 'ddm'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddm'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace''s Phantasm'),
    (select id from sets where short_name = 'ddm'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dread Statuary'),
    (select id from sets where short_name = 'ddm'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Bear'),
    (select id from sets where short_name = 'ddm'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Errant Ephemeron'),
    (select id from sets where short_name = 'ddm'),
    '20',
    'common'
) 
 on conflict do nothing;
