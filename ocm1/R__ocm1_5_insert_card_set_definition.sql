insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Brion Stoutarm'),
    (select id from sets where short_name = 'ocm1'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Godo, Bandit Warlord'),
    (select id from sets where short_name = 'ocm1'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mayael the Anima'),
    (select id from sets where short_name = 'ocm1'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Glissa, the Traitor'),
    (select id from sets where short_name = 'ocm1'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Karn, Silver Golem'),
    (select id from sets where short_name = 'ocm1'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zur the Enchanter'),
    (select id from sets where short_name = 'ocm1'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sliver Queen'),
    (select id from sets where short_name = 'ocm1'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azusa, Lost but Seeking'),
    (select id from sets where short_name = 'ocm1'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karrthus, Tyrant of Jund'),
    (select id from sets where short_name = 'ocm1'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Grimgrin, Corpse-Born'),
    (select id from sets where short_name = 'ocm1'),
    '5',
    'mythic'
) 
 on conflict do nothing;
