    insert into types (name) values('Instant') on conflict do nothing;
    insert into types (name) values('Legendary') on conflict do nothing;
    insert into types (name) values('Creature') on conflict do nothing;
    insert into types (name) values('Spirit') on conflict do nothing;
    insert into types (name) values('Cleric') on conflict do nothing;
    insert into types (name) values('Creature') on conflict do nothing;
    insert into types (name) values('Elemental') on conflict do nothing;
    insert into types (name) values('Land') on conflict do nothing;
    insert into types (name) values('Legendary') on conflict do nothing;
    insert into types (name) values('Creature') on conflict do nothing;
    insert into types (name) values('Human') on conflict do nothing;
    insert into types (name) values('Soldier') on conflict do nothing;
