    insert into mtgcard(name) values ('Abrupt Decay') on conflict do nothing;
    insert into mtgcard(name) values ('Geist of Saint Traft') on conflict do nothing;
    insert into mtgcard(name) values ('Vengevine') on conflict do nothing;
    insert into mtgcard(name) values ('Inkmoth Nexus') on conflict do nothing;
    insert into mtgcard(name) values ('Thalia, Guardian of Thraben') on conflict do nothing;
