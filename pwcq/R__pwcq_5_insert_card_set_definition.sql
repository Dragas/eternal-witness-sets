insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Abrupt Decay'),
    (select id from sets where short_name = 'pwcq'),
    '2016',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Geist of Saint Traft'),
    (select id from sets where short_name = 'pwcq'),
    '2014',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vengevine'),
    (select id from sets where short_name = 'pwcq'),
    '2013',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Inkmoth Nexus'),
    (select id from sets where short_name = 'pwcq'),
    '2017',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thalia, Guardian of Thraben'),
    (select id from sets where short_name = 'pwcq'),
    '2015',
    'rare'
) 
 on conflict do nothing;
