insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Elvish Archers'),
    (select id from sets where short_name = 'rqs'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'War Mammoth'),
    (select id from sets where short_name = 'rqs'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = 'rqs'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'rqs'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glasses of Urza'),
    (select id from sets where short_name = 'rqs'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'rqs'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jayemdae Tome'),
    (select id from sets where short_name = 'rqs'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'rqs'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battering Ram'),
    (select id from sets where short_name = 'rqs'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'rqs'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'rqs'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mesa Pegasus'),
    (select id from sets where short_name = 'rqs'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'rqs'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warp Artifact'),
    (select id from sets where short_name = 'rqs'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ironclaw Orcs'),
    (select id from sets where short_name = 'rqs'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'rqs'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whirling Dervish'),
    (select id from sets where short_name = 'rqs'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disintegrate'),
    (select id from sets where short_name = 'rqs'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'rqs'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Imp'),
    (select id from sets where short_name = 'rqs'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'rqs'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = 'rqs'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sorceress Queen'),
    (select id from sets where short_name = 'rqs'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Oriflamme'),
    (select id from sets where short_name = 'rqs'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zephyr Falcon'),
    (select id from sets where short_name = 'rqs'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Bats'),
    (select id from sets where short_name = 'rqs'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mons''s Goblin Raiders'),
    (select id from sets where short_name = 'rqs'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Detonate'),
    (select id from sets where short_name = 'rqs'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clockwork Beast'),
    (select id from sets where short_name = 'rqs'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'rqs'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = 'rqs'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grizzly Bears'),
    (select id from sets where short_name = 'rqs'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murk Dwellers'),
    (select id from sets where short_name = 'rqs'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'rqs'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rod of Ruin'),
    (select id from sets where short_name = 'rqs'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Twiddle'),
    (select id from sets where short_name = 'rqs'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Wraith'),
    (select id from sets where short_name = 'rqs'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Power Sink'),
    (select id from sets where short_name = 'rqs'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Artillery'),
    (select id from sets where short_name = 'rqs'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyrotechnics'),
    (select id from sets where short_name = 'rqs'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scathe Zombies'),
    (select id from sets where short_name = 'rqs'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'rqs'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'rqs'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pearled Unicorn'),
    (select id from sets where short_name = 'rqs'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alabaster Potion'),
    (select id from sets where short_name = 'rqs'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Untamed Wilds'),
    (select id from sets where short_name = 'rqs'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weakness'),
    (select id from sets where short_name = 'rqs'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk of the Pearl Trident'),
    (select id from sets where short_name = 'rqs'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Energy Flux'),
    (select id from sets where short_name = 'rqs'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'rqs'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lost Soul'),
    (select id from sets where short_name = 'rqs'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Healing Salve'),
    (select id from sets where short_name = 'rqs'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = 'rqs'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reverse Damage'),
    (select id from sets where short_name = 'rqs'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scryb Sprites'),
    (select id from sets where short_name = 'rqs'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hill Giant'),
    (select id from sets where short_name = 'rqs'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Black'),
    (select id from sets where short_name = 'rqs'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Bone'),
    (select id from sets where short_name = 'rqs'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elven Riders'),
    (select id from sets where short_name = 'rqs'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Durkwood Boars'),
    (select id from sets where short_name = 'rqs'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cursed Land'),
    (select id from sets where short_name = 'rqs'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winter Blast'),
    (select id from sets where short_name = 'rqs'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'rqs'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feedback'),
    (select id from sets where short_name = 'rqs'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'rqs'),
    '65',
    'common'
) 
 on conflict do nothing;
