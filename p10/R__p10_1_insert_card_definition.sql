    insert into mtgcard(name) values ('Celestial Purge') on conflict do nothing;
    insert into mtgcard(name) values ('Lightning Bolt') on conflict do nothing;
    insert into mtgcard(name) values ('Volcanic Fallout') on conflict do nothing;
    insert into mtgcard(name) values ('Infest') on conflict do nothing;
    insert into mtgcard(name) values ('Sign in Blood') on conflict do nothing;
    insert into mtgcard(name) values ('Harrow') on conflict do nothing;
    insert into mtgcard(name) values ('Bituminous Blast') on conflict do nothing;
    insert into mtgcard(name) values ('Cancel') on conflict do nothing;
    insert into mtgcard(name) values ('Burst Lightning') on conflict do nothing;
