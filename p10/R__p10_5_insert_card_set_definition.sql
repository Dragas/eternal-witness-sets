insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Celestial Purge'),
    (select id from sets where short_name = 'p10'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'p10'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Fallout'),
    (select id from sets where short_name = 'p10'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infest'),
    (select id from sets where short_name = 'p10'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sign in Blood'),
    (select id from sets where short_name = 'p10'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harrow'),
    (select id from sets where short_name = 'p10'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bituminous Blast'),
    (select id from sets where short_name = 'p10'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cancel'),
    (select id from sets where short_name = 'p10'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burst Lightning'),
    (select id from sets where short_name = 'p10'),
    '8',
    'rare'
) 
 on conflict do nothing;
