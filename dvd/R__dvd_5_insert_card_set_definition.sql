insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Lord of the Pit'),
    (select id from sets where short_name = 'dvd'),
    '30',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Healing Salve'),
    (select id from sets where short_name = 'dvd'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abyssal Specter'),
    (select id from sets where short_name = 'dvd'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Foul Imp'),
    (select id from sets where short_name = 'dvd'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reya Dawnbringer'),
    (select id from sets where short_name = 'dvd'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'dvd'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akroma, Angel of Wrath'),
    (select id from sets where short_name = 'dvd'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stinkweed Imp'),
    (select id from sets where short_name = 'dvd'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra''s Embrace'),
    (select id from sets where short_name = 'dvd'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Otherworldly Journey'),
    (select id from sets where short_name = 'dvd'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cackling Imp'),
    (select id from sets where short_name = 'dvd'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charging Paladin'),
    (select id from sets where short_name = 'dvd'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Breeding Pit'),
    (select id from sets where short_name = 'dvd'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Corrupt'),
    (select id from sets where short_name = 'dvd'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'dvd'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Souldrinker'),
    (select id from sets where short_name = 'dvd'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dvd'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cruel Edict'),
    (select id from sets where short_name = 'dvd'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marble Diamond'),
    (select id from sets where short_name = 'dvd'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oni Possession'),
    (select id from sets where short_name = 'dvd'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demonic Tutor'),
    (select id from sets where short_name = 'dvd'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel''s Feather'),
    (select id from sets where short_name = 'dvd'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abyssal Gatekeeper'),
    (select id from sets where short_name = 'dvd'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'dvd'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daggerclaw Imp'),
    (select id from sets where short_name = 'dvd'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelic Page'),
    (select id from sets where short_name = 'dvd'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Secluded Steppe'),
    (select id from sets where short_name = 'dvd'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Twilight Shepherd'),
    (select id from sets where short_name = 'dvd'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dvd'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soot Imp'),
    (select id from sets where short_name = 'dvd'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Venerable Monk'),
    (select id from sets where short_name = 'dvd'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overeager Apprentice'),
    (select id from sets where short_name = 'dvd'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sustainer of the Realm'),
    (select id from sets where short_name = 'dvd'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barter in Blood'),
    (select id from sets where short_name = 'dvd'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'dvd'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'dvd'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'dvd'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Benediction'),
    (select id from sets where short_name = 'dvd'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dusk Imp'),
    (select id from sets where short_name = 'dvd'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dvd'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faith''s Fetters'),
    (select id from sets where short_name = 'dvd'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Promise of Power'),
    (select id from sets where short_name = 'dvd'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fallen Angel'),
    (select id from sets where short_name = 'dvd'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelic Protector'),
    (select id from sets where short_name = 'dvd'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Luminous Angel'),
    (select id from sets where short_name = 'dvd'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dvd'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reiver Demon'),
    (select id from sets where short_name = 'dvd'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Consume Spirit'),
    (select id from sets where short_name = 'dvd'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demon''s Horn'),
    (select id from sets where short_name = 'dvd'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barren Moor'),
    (select id from sets where short_name = 'dvd'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Righteous Cause'),
    (select id from sets where short_name = 'dvd'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'dvd'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'dvd'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra Advocate'),
    (select id from sets where short_name = 'dvd'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Banishing'),
    (select id from sets where short_name = 'dvd'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelsong'),
    (select id from sets where short_name = 'dvd'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Mercy'),
    (select id from sets where short_name = 'dvd'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kuro, Pitlord'),
    (select id from sets where short_name = 'dvd'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra''s Boon'),
    (select id from sets where short_name = 'dvd'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demon''s Jester'),
    (select id from sets where short_name = 'dvd'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unholy Strength'),
    (select id from sets where short_name = 'dvd'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Priest'),
    (select id from sets where short_name = 'dvd'),
    '2',
    'uncommon'
) 
 on conflict do nothing;
