insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Marsh Flitter'),
    (select id from sets where short_name = 'lrw'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant''s Ire'),
    (select id from sets where short_name = 'lrw'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stonybrook Angler'),
    (select id from sets where short_name = 'lrw'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fertile Ground'),
    (select id from sets where short_name = 'lrw'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Amoeboid Changeling'),
    (select id from sets where short_name = 'lrw'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inner-Flame Igniter'),
    (select id from sets where short_name = 'lrw'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nova Chaser'),
    (select id from sets where short_name = 'lrw'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'lrw'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Auntie''s Hovel'),
    (select id from sets where short_name = 'lrw'),
    '267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prowess of the Fair'),
    (select id from sets where short_name = 'lrw'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oakgnarl Warrior'),
    (select id from sets where short_name = 'lrw'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kithkin Mourncaller'),
    (select id from sets where short_name = 'lrw'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gilt-Leaf Seer'),
    (select id from sets where short_name = 'lrw'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nath of the Gilt-Leaf'),
    (select id from sets where short_name = 'lrw'),
    '250',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Entangling Trap'),
    (select id from sets where short_name = 'lrw'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rootgrapple'),
    (select id from sets where short_name = 'lrw'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Poplar Shaman'),
    (select id from sets where short_name = 'lrw'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wellgabber Apothecary'),
    (select id from sets where short_name = 'lrw'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Summon the School'),
    (select id from sets where short_name = 'lrw'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thorntooth Witch'),
    (select id from sets where short_name = 'lrw'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drowner of Secrets'),
    (select id from sets where short_name = 'lrw'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Timber Protector'),
    (select id from sets where short_name = 'lrw'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Purity'),
    (select id from sets where short_name = 'lrw'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana Vess'),
    (select id from sets where short_name = 'lrw'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thousand-Year Elixir'),
    (select id from sets where short_name = 'lrw'),
    '263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whirlpool Whelm'),
    (select id from sets where short_name = 'lrw'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boggart Shenanigans'),
    (select id from sets where short_name = 'lrw'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flamekin Bladewhirl'),
    (select id from sets where short_name = 'lrw'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boggart Birth Rite'),
    (select id from sets where short_name = 'lrw'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oblivion Ring'),
    (select id from sets where short_name = 'lrw'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Surgespanner'),
    (select id from sets where short_name = 'lrw'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'lrw'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of Meadowgrain'),
    (select id from sets where short_name = 'lrw'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Harbinger'),
    (select id from sets where short_name = 'lrw'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Facevaulter'),
    (select id from sets where short_name = 'lrw'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Favor of the Mighty'),
    (select id from sets where short_name = 'lrw'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Changeling Hero'),
    (select id from sets where short_name = 'lrw'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'lrw'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lignify'),
    (select id from sets where short_name = 'lrw'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ajani Goldmane'),
    (select id from sets where short_name = 'lrw'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glen Elendra Pranksters'),
    (select id from sets where short_name = 'lrw'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Changeling Titan'),
    (select id from sets where short_name = 'lrw'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vivid Creek'),
    (select id from sets where short_name = 'lrw'),
    '276',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guile'),
    (select id from sets where short_name = 'lrw'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sower of Temptation'),
    (select id from sets where short_name = 'lrw'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Amphitheater'),
    (select id from sets where short_name = 'lrw'),
    '266',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flamekin Brawler'),
    (select id from sets where short_name = 'lrw'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Judge of Currents'),
    (select id from sets where short_name = 'lrw'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog-Strider Ash'),
    (select id from sets where short_name = 'lrw'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guardian of Cloverdell'),
    (select id from sets where short_name = 'lrw'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chandra Nalaar'),
    (select id from sets where short_name = 'lrw'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivid Marsh'),
    (select id from sets where short_name = 'lrw'),
    '278',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gaddock Teeg'),
    (select id from sets where short_name = 'lrw'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lace with Moonglove'),
    (select id from sets where short_name = 'lrw'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Tauntings'),
    (select id from sets where short_name = 'lrw'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Incremental Growth'),
    (select id from sets where short_name = 'lrw'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mosswort Bridge'),
    (select id from sets where short_name = 'lrw'),
    '270',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'lrw'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mad Auntie'),
    (select id from sets where short_name = 'lrw'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mournwhelk'),
    (select id from sets where short_name = 'lrw'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leaf Gilder'),
    (select id from sets where short_name = 'lrw'),
    '227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merrow Commerce'),
    (select id from sets where short_name = 'lrw'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Immaculate Magistrate'),
    (select id from sets where short_name = 'lrw'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Epic Proportions'),
    (select id from sets where short_name = 'lrw'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thoughtseize'),
    (select id from sets where short_name = 'lrw'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wren''s Run Packmaster'),
    (select id from sets where short_name = 'lrw'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'lrw'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primal Command'),
    (select id from sets where short_name = 'lrw'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Colfenor''s Urn'),
    (select id from sets where short_name = 'lrw'),
    '254',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shelldock Isle'),
    (select id from sets where short_name = 'lrw'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stinkdrinker Daredevil'),
    (select id from sets where short_name = 'lrw'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deeptread Merrow'),
    (select id from sets where short_name = 'lrw'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kithkin Healer'),
    (select id from sets where short_name = 'lrw'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Triclopean Sight'),
    (select id from sets where short_name = 'lrw'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hornet Harasser'),
    (select id from sets where short_name = 'lrw'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gilt-Leaf Ambush'),
    (select id from sets where short_name = 'lrw'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spellstutter Sprite'),
    (select id from sets where short_name = 'lrw'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'lrw'),
    '290',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mudbutton Torchrunner'),
    (select id from sets where short_name = 'lrw'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Incendiary Command'),
    (select id from sets where short_name = 'lrw'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Branchbender'),
    (select id from sets where short_name = 'lrw'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goldmeadow Dodger'),
    (select id from sets where short_name = 'lrw'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivid Crag'),
    (select id from sets where short_name = 'lrw'),
    '275',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Militia''s Pride'),
    (select id from sets where short_name = 'lrw'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Familiar''s Ruse'),
    (select id from sets where short_name = 'lrw'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wispmare'),
    (select id from sets where short_name = 'lrw'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Eulogist'),
    (select id from sets where short_name = 'lrw'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Peppersmoke'),
    (select id from sets where short_name = 'lrw'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Consuming Bonfire'),
    (select id from sets where short_name = 'lrw'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wizened Cenn'),
    (select id from sets where short_name = 'lrw'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thundercloud Shaman'),
    (select id from sets where short_name = 'lrw'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'lrw'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glarewielder'),
    (select id from sets where short_name = 'lrw'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blades of Velis Vel'),
    (select id from sets where short_name = 'lrw'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ethereal Whiskergill'),
    (select id from sets where short_name = 'lrw'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'lrw'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howltooth Hollow'),
    (select id from sets where short_name = 'lrw'),
    '269',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vigor'),
    (select id from sets where short_name = 'lrw'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hostility'),
    (select id from sets where short_name = 'lrw'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'lrw'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exiled Boggart'),
    (select id from sets where short_name = 'lrw'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kinsbaile Skirmisher'),
    (select id from sets where short_name = 'lrw'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashling the Pilgrim'),
    (select id from sets where short_name = 'lrw'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boggart Sprite-Chaser'),
    (select id from sets where short_name = 'lrw'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Captivating Glance'),
    (select id from sets where short_name = 'lrw'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kithkin Harbinger'),
    (select id from sets where short_name = 'lrw'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vivid Grove'),
    (select id from sets where short_name = 'lrw'),
    '277',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lammastide Weave'),
    (select id from sets where short_name = 'lrw'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nath''s Buffoon'),
    (select id from sets where short_name = 'lrw'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goldmeadow Stalwart'),
    (select id from sets where short_name = 'lrw'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arbiter of Knollridge'),
    (select id from sets where short_name = 'lrw'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wanderwine Prophets'),
    (select id from sets where short_name = 'lrw'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'lrw'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'lrw'),
    '283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'lrw'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skeletal Changeling'),
    (select id from sets where short_name = 'lrw'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Imperious Perfect'),
    (select id from sets where short_name = 'lrw'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'lrw'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ringskipper'),
    (select id from sets where short_name = 'lrw'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Profane Command'),
    (select id from sets where short_name = 'lrw'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blind-Spot Giant'),
    (select id from sets where short_name = 'lrw'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Springjack Knight'),
    (select id from sets where short_name = 'lrw'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jagged-Scar Archers'),
    (select id from sets where short_name = 'lrw'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lys Alana Scarblade'),
    (select id from sets where short_name = 'lrw'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goatnapper'),
    (select id from sets where short_name = 'lrw'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Makeshift Mannequin'),
    (select id from sets where short_name = 'lrw'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battle Mastery'),
    (select id from sets where short_name = 'lrw'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hurly-Burly'),
    (select id from sets where short_name = 'lrw'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Protective Bubble'),
    (select id from sets where short_name = 'lrw'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warren-Scourge Elf'),
    (select id from sets where short_name = 'lrw'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreamspoiler Witches'),
    (select id from sets where short_name = 'lrw'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knucklebone Witch'),
    (select id from sets where short_name = 'lrw'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soaring Hope'),
    (select id from sets where short_name = 'lrw'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Broken Ambitions'),
    (select id from sets where short_name = 'lrw'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horde of Notions'),
    (select id from sets where short_name = 'lrw'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Galepowder Mage'),
    (select id from sets where short_name = 'lrw'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zephyr Net'),
    (select id from sets where short_name = 'lrw'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cryptic Command'),
    (select id from sets where short_name = 'lrw'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'lrw'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Axegrinder Giant'),
    (select id from sets where short_name = 'lrw'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silvergill Douser'),
    (select id from sets where short_name = 'lrw'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spiderwig Boggart'),
    (select id from sets where short_name = 'lrw'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battlewand Oak'),
    (select id from sets where short_name = 'lrw'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lys Alana Huntmaster'),
    (select id from sets where short_name = 'lrw'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wanderer''s Twig'),
    (select id from sets where short_name = 'lrw'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boggart Harbinger'),
    (select id from sets where short_name = 'lrw'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Austere Command'),
    (select id from sets where short_name = 'lrw'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'lrw'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kithkin Daggerdare'),
    (select id from sets where short_name = 'lrw'),
    '223',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woodland Guidance'),
    (select id from sets where short_name = 'lrw'),
    '243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nameless Inversion'),
    (select id from sets where short_name = 'lrw'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hoarder''s Greed'),
    (select id from sets where short_name = 'lrw'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oona''s Prowler'),
    (select id from sets where short_name = 'lrw'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hamletback Goliath'),
    (select id from sets where short_name = 'lrw'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moonglove Extract'),
    (select id from sets where short_name = 'lrw'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Handservant'),
    (select id from sets where short_name = 'lrw'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Final Revels'),
    (select id from sets where short_name = 'lrw'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flamekin Spitfire'),
    (select id from sets where short_name = 'lrw'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thieving Sprite'),
    (select id from sets where short_name = 'lrw'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dread'),
    (select id from sets where short_name = 'lrw'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Runed Stalactite'),
    (select id from sets where short_name = 'lrw'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ingot Chewer'),
    (select id from sets where short_name = 'lrw'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windbrisk Heights'),
    (select id from sets where short_name = 'lrw'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silvergill Adept'),
    (select id from sets where short_name = 'lrw'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cloudgoat Ranger'),
    (select id from sets where short_name = 'lrw'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thoughtweft Trio'),
    (select id from sets where short_name = 'lrw'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Secluded Glen'),
    (select id from sets where short_name = 'lrw'),
    '271',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boggart Loggers'),
    (select id from sets where short_name = 'lrw'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treefolk Harbinger'),
    (select id from sets where short_name = 'lrw'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Changeling Berserker'),
    (select id from sets where short_name = 'lrw'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wort, Boggart Auntie'),
    (select id from sets where short_name = 'lrw'),
    '252',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'lrw'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heat Shimmer'),
    (select id from sets where short_name = 'lrw'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Paperfin Rascal'),
    (select id from sets where short_name = 'lrw'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivid Meadow'),
    (select id from sets where short_name = 'lrw'),
    '279',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scarred Vinebreeder'),
    (select id from sets where short_name = 'lrw'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boggart Mob'),
    (select id from sets where short_name = 'lrw'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Turtleshell Changeling'),
    (select id from sets where short_name = 'lrw'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cenn''s Heir'),
    (select id from sets where short_name = 'lrw'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ceaseless Searblades'),
    (select id from sets where short_name = 'lrw'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crib Swap'),
    (select id from sets where short_name = 'lrw'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mulldrifter'),
    (select id from sets where short_name = 'lrw'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brion Stoutarm'),
    (select id from sets where short_name = 'lrw'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightshade Stinger'),
    (select id from sets where short_name = 'lrw'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woodland Changeling'),
    (select id from sets where short_name = 'lrw'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sentry Oak'),
    (select id from sets where short_name = 'lrw'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rebellion of the Flamekin'),
    (select id from sets where short_name = 'lrw'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dauntless Dourbark'),
    (select id from sets where short_name = 'lrw'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirror Entity'),
    (select id from sets where short_name = 'lrw'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Streambed Aquitects'),
    (select id from sets where short_name = 'lrw'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doran, the Siege Tower'),
    (select id from sets where short_name = 'lrw'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'lrw'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Herbal Poultice'),
    (select id from sets where short_name = 'lrw'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merrow Harbinger'),
    (select id from sets where short_name = 'lrw'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merrow Reejerey'),
    (select id from sets where short_name = 'lrw'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shimmering Grotto'),
    (select id from sets where short_name = 'lrw'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kinsbaile Balloonist'),
    (select id from sets where short_name = 'lrw'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunrise Sovereign'),
    (select id from sets where short_name = 'lrw'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goldmeadow Harrier'),
    (select id from sets where short_name = 'lrw'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squeaking Pie Sneak'),
    (select id from sets where short_name = 'lrw'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Briarhorn'),
    (select id from sets where short_name = 'lrw'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heal the Scars'),
    (select id from sets where short_name = 'lrw'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cairn Wanderer'),
    (select id from sets where short_name = 'lrw'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'lrw'),
    '282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ponder'),
    (select id from sets where short_name = 'lrw'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunter of Eyeblights'),
    (select id from sets where short_name = 'lrw'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warren Pilferers'),
    (select id from sets where short_name = 'lrw'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathrender'),
    (select id from sets where short_name = 'lrw'),
    '255',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faultgrinder'),
    (select id from sets where short_name = 'lrw'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burrenton Forge-Tender'),
    (select id from sets where short_name = 'lrw'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hillcomber Giant'),
    (select id from sets where short_name = 'lrw'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harpoon Sniper'),
    (select id from sets where short_name = 'lrw'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weed Strangle'),
    (select id from sets where short_name = 'lrw'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Veteran of the Depths'),
    (select id from sets where short_name = 'lrw'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quill-Slinger Boggart'),
    (select id from sets where short_name = 'lrw'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aethersnipe'),
    (select id from sets where short_name = 'lrw'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scattering Stroke'),
    (select id from sets where short_name = 'lrw'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lairwatch Giant'),
    (select id from sets where short_name = 'lrw'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eyes of the Wisent'),
    (select id from sets where short_name = 'lrw'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloudthresher'),
    (select id from sets where short_name = 'lrw'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Incandescent Soulstoke'),
    (select id from sets where short_name = 'lrw'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Masked Admirers'),
    (select id from sets where short_name = 'lrw'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Garruk Wildspeaker'),
    (select id from sets where short_name = 'lrw'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hunt Down'),
    (select id from sets where short_name = 'lrw'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gilt-Leaf Palace'),
    (select id from sets where short_name = 'lrw'),
    '268',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inkfathom Divers'),
    (select id from sets where short_name = 'lrw'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wanderwine Hub'),
    (select id from sets where short_name = 'lrw'),
    '280',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fathom Trawl'),
    (select id from sets where short_name = 'lrw'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lowland Oaf'),
    (select id from sets where short_name = 'lrw'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace Beleren'),
    (select id from sets where short_name = 'lrw'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mistbind Clique'),
    (select id from sets where short_name = 'lrw'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fodder Launch'),
    (select id from sets where short_name = 'lrw'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hoofprints of the Stag'),
    (select id from sets where short_name = 'lrw'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Adder-Staff Boggart'),
    (select id from sets where short_name = 'lrw'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tarfire'),
    (select id from sets where short_name = 'lrw'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Needle Drop'),
    (select id from sets where short_name = 'lrw'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fallowsage'),
    (select id from sets where short_name = 'lrw'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fire-Belly Changeling'),
    (select id from sets where short_name = 'lrw'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'lrw'),
    '288',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghostly Changeling'),
    (select id from sets where short_name = 'lrw'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Ricochet'),
    (select id from sets where short_name = 'lrw'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashling''s Prerogative'),
    (select id from sets where short_name = 'lrw'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nectar Faerie'),
    (select id from sets where short_name = 'lrw'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brigid, Hero of Kinsbaile'),
    (select id from sets where short_name = 'lrw'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spring Cleaning'),
    (select id from sets where short_name = 'lrw'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nettlevine Blight'),
    (select id from sets where short_name = 'lrw'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'lrw'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lash Out'),
    (select id from sets where short_name = 'lrw'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Harbinger'),
    (select id from sets where short_name = 'lrw'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pollen Lullaby'),
    (select id from sets where short_name = 'lrw'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spinerock Knoll'),
    (select id from sets where short_name = 'lrw'),
    '274',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faerie Trickery'),
    (select id from sets where short_name = 'lrw'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Twinning Glass'),
    (select id from sets where short_name = 'lrw'),
    '264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crush Underfoot'),
    (select id from sets where short_name = 'lrw'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glimmerdust Nap'),
    (select id from sets where short_name = 'lrw'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Surge of Thoughtweft'),
    (select id from sets where short_name = 'lrw'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eyeblight''s Ending'),
    (select id from sets where short_name = 'lrw'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shapesharer'),
    (select id from sets where short_name = 'lrw'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smokebraider'),
    (select id from sets where short_name = 'lrw'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thorn of Amethyst'),
    (select id from sets where short_name = 'lrw'),
    '262',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tideshaper Mystic'),
    (select id from sets where short_name = 'lrw'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudcrown Oak'),
    (select id from sets where short_name = 'lrw'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kithkin Greatheart'),
    (select id from sets where short_name = 'lrw'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oaken Brawler'),
    (select id from sets where short_name = 'lrw'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shriekmaw'),
    (select id from sets where short_name = 'lrw'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plover Knights'),
    (select id from sets where short_name = 'lrw'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soulbright Flamekin'),
    (select id from sets where short_name = 'lrw'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Promenade'),
    (select id from sets where short_name = 'lrw'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seedguide Ash'),
    (select id from sets where short_name = 'lrw'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Caterwauling Boggart'),
    (select id from sets where short_name = 'lrw'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moonglove Winnower'),
    (select id from sets where short_name = 'lrw'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avian Changeling'),
    (select id from sets where short_name = 'lrw'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Benthicore'),
    (select id from sets where short_name = 'lrw'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bog Hoodlums'),
    (select id from sets where short_name = 'lrw'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pestermite'),
    (select id from sets where short_name = 'lrw'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nath''s Elite'),
    (select id from sets where short_name = 'lrw'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flamekin Harbinger'),
    (select id from sets where short_name = 'lrw'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sentinels of Glen Elendra'),
    (select id from sets where short_name = 'lrw'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shields of Velis Vel'),
    (select id from sets where short_name = 'lrw'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ego Erasure'),
    (select id from sets where short_name = 'lrw'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Echoes'),
    (select id from sets where short_name = 'lrw'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scion of Oona'),
    (select id from sets where short_name = 'lrw'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forced Fruition'),
    (select id from sets where short_name = 'lrw'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fistful of Force'),
    (select id from sets where short_name = 'lrw'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dawnfluke'),
    (select id from sets where short_name = 'lrw'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Colfenor''s Plans'),
    (select id from sets where short_name = 'lrw'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inner-Flame Acolyte'),
    (select id from sets where short_name = 'lrw'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Harbinger'),
    (select id from sets where short_name = 'lrw'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wren''s Run Vanquisher'),
    (select id from sets where short_name = 'lrw'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Springleaf Drum'),
    (select id from sets where short_name = 'lrw'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hearthcage Giant'),
    (select id from sets where short_name = 'lrw'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sygg, River Guide'),
    (select id from sets where short_name = 'lrw'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wings of Velis Vel'),
    (select id from sets where short_name = 'lrw'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boggart Forager'),
    (select id from sets where short_name = 'lrw'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rings of Brighthearth'),
    (select id from sets where short_name = 'lrw'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Footbottom Feast'),
    (select id from sets where short_name = 'lrw'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Neck Snap'),
    (select id from sets where short_name = 'lrw'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aquitect''s Will'),
    (select id from sets where short_name = 'lrw'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dolmen Gate'),
    (select id from sets where short_name = 'lrw'),
    '256',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wydwen, the Biting Gale'),
    (select id from sets where short_name = 'lrw'),
    '253',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tar Pitcher'),
    (select id from sets where short_name = 'lrw'),
    '193',
    'uncommon'
) 
 on conflict do nothing;
