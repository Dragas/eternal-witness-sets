    insert into mtgcard(name) values ('Beast') on conflict do nothing;
    insert into mtgcard(name) values ('Soldier') on conflict do nothing;
    insert into mtgcard(name) values ('Wurm') on conflict do nothing;
    insert into mtgcard(name) values ('Zombie') on conflict do nothing;
    insert into mtgcard(name) values ('Pentavite') on conflict do nothing;
    insert into mtgcard(name) values ('Bird') on conflict do nothing;
    insert into mtgcard(name) values ('Saproling') on conflict do nothing;
