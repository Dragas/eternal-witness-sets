insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tm12'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tm12'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurm'),
    (select id from sets where short_name = 'tm12'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tm12'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pentavite'),
    (select id from sets where short_name = 'tm12'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird'),
    (select id from sets where short_name = 'tm12'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tm12'),
    '5',
    'common'
) 
 on conflict do nothing;
