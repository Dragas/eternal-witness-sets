insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Angel of Glory''s Rise'),
    (select id from sets where short_name = 'pres'),
    'A9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bristling Hydra'),
    (select id from sets where short_name = 'pres'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hamletback Goliath'),
    (select id from sets where short_name = 'pres'),
    'A10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Atarka, World Render'),
    (select id from sets where short_name = 'pres'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Chieftain'),
    (select id from sets where short_name = 'pres'),
    '141*',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Genesis Hydra'),
    (select id from sets where short_name = 'pres'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angelic Skirmisher'),
    (select id from sets where short_name = 'pres'),
    'A11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beast Whisperer'),
    (select id from sets where short_name = 'pres'),
    '123*',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Etali, Primal Storm'),
    (select id from sets where short_name = 'pres'),
    '100*',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Retaliator Griffin'),
    (select id from sets where short_name = 'pres'),
    'A4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Felidar Sovereign'),
    (select id from sets where short_name = 'pres'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brion Stoutarm'),
    (select id from sets where short_name = 'pres'),
    'A2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Outland Colossus'),
    (select id from sets where short_name = 'pres'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jaya Ballard, Task Mage'),
    (select id from sets where short_name = 'pres'),
    'A1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curator of Mysteries'),
    (select id from sets where short_name = 'pres'),
    '49*',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terastodon'),
    (select id from sets where short_name = 'pres'),
    'A6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loam Lion'),
    (select id from sets where short_name = 'pres'),
    '13*',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Xathrid Necromancer'),
    (select id from sets where short_name = 'pres'),
    'A12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lathliss, Dragon Queen'),
    (select id from sets where short_name = 'pres'),
    '149*',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knight Exemplar'),
    (select id from sets where short_name = 'pres'),
    'A7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terra Stomper'),
    (select id from sets where short_name = 'pres'),
    'A5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief, the Vastwood'),
    (select id from sets where short_name = 'pres'),
    '221*',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunblast Angel'),
    (select id from sets where short_name = 'pres'),
    'A8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thalia''s Lancers'),
    (select id from sets where short_name = 'pres'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Broodmate Dragon'),
    (select id from sets where short_name = 'pres'),
    'A3',
    'rare'
) 
 on conflict do nothing;
