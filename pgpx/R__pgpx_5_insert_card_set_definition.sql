insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Griselbrand'),
    (select id from sets where short_name = 'pgpx'),
    '2015',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pgpx'),
    '2018d',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pgpx'),
    '2018c',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stoneforge Mystic'),
    (select id from sets where short_name = 'pgpx'),
    '2016',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Batterskull'),
    (select id from sets where short_name = 'pgpx'),
    '2014',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chrome Mox'),
    (select id from sets where short_name = 'pgpx'),
    '2009',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pgpx'),
    '2018f',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Guide'),
    (select id from sets where short_name = 'pgpx'),
    '2012a',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pgpx'),
    '2018b',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Umezawa''s Jitte'),
    (select id from sets where short_name = 'pgpx'),
    '2010',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pgpx'),
    '2018a',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mutavault'),
    (select id from sets where short_name = 'pgpx'),
    '2018',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spiritmonger'),
    (select id from sets where short_name = 'pgpx'),
    '2007',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Pulse'),
    (select id from sets where short_name = 'pgpx'),
    '2011',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Call of the Herd'),
    (select id from sets where short_name = 'pgpx'),
    '2008',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lotus Cobra'),
    (select id from sets where short_name = 'pgpx'),
    '2012b',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword of Feast and Famine'),
    (select id from sets where short_name = 'pgpx'),
    '2016b',
    'rare'
) ,
(
    (select id from mtgcard where name = 'All Is Dust'),
    (select id from sets where short_name = 'pgpx'),
    '2013b',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primeval Titan'),
    (select id from sets where short_name = 'pgpx'),
    '2013a',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Progenitus'),
    (select id from sets where short_name = 'pgpx'),
    '2017',
    'mythic'
) 
 on conflict do nothing;
