insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Barrin'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ashnod'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mishra'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hanna'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Squee'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mirri'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Greven il-Vec'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lyna'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sidar Kondo'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Titania'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rofellos'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Maraxus'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ertai'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Selenia'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tawnos'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Takara'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Karn'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gix'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sliver Queen, Brood Mother'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Starke'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crovax'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gerrard'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tahngarth'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eladamri'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Urza'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Oracle'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Multani'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sisay'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Orim'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Volrath'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Serra'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Xantcha'),
        (select types.id from types where name = 'Vanguard')
    ) 
 on conflict do nothing;
