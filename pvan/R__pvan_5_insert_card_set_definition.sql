insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Barrin'),
    (select id from sets where short_name = 'pvan'),
    '201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashnod'),
    (select id from sets where short_name = 'pvan'),
    '401',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mishra'),
    (select id from sets where short_name = 'pvan'),
    '403',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hanna'),
    (select id from sets where short_name = 'pvan'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Squee'),
    (select id from sets where short_name = 'pvan'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirri'),
    (select id from sets where short_name = 'pvan'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Greven il-Vec'),
    (select id from sets where short_name = 'pvan'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lyna'),
    (select id from sets where short_name = 'pvan'),
    '302',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sidar Kondo'),
    (select id from sets where short_name = 'pvan'),
    '306',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Titania'),
    (select id from sets where short_name = 'pvan'),
    '406',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rofellos'),
    (select id from sets where short_name = 'pvan'),
    '305',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maraxus'),
    (select id from sets where short_name = 'pvan'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ertai'),
    (select id from sets where short_name = 'pvan'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selenia'),
    (select id from sets where short_name = 'pvan'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tawnos'),
    (select id from sets where short_name = 'pvan'),
    '405',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Takara'),
    (select id from sets where short_name = 'pvan'),
    '308',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karn'),
    (select id from sets where short_name = 'pvan'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gix'),
    (select id from sets where short_name = 'pvan'),
    '402',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sliver Queen, Brood Mother'),
    (select id from sets where short_name = 'pvan'),
    '307',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Starke'),
    (select id from sets where short_name = 'pvan'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crovax'),
    (select id from sets where short_name = 'pvan'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gerrard'),
    (select id from sets where short_name = 'pvan'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tahngarth'),
    (select id from sets where short_name = 'pvan'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eladamri'),
    (select id from sets where short_name = 'pvan'),
    '301',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza'),
    (select id from sets where short_name = 'pvan'),
    '407',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oracle'),
    (select id from sets where short_name = 'pvan'),
    '304',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Multani'),
    (select id from sets where short_name = 'pvan'),
    '303',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sisay'),
    (select id from sets where short_name = 'pvan'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orim'),
    (select id from sets where short_name = 'pvan'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volrath'),
    (select id from sets where short_name = 'pvan'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra'),
    (select id from sets where short_name = 'pvan'),
    '404',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Xantcha'),
    (select id from sets where short_name = 'pvan'),
    '408',
    'rare'
) 
 on conflict do nothing;
