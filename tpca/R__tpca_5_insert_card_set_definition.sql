insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Hellion'),
    (select id from sets where short_name = 'tpca'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tpca'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ooze'),
    (select id from sets where short_name = 'tpca'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tpca'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plant'),
    (select id from sets where short_name = 'tpca'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tpca'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goat'),
    (select id from sets where short_name = 'tpca'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Germ'),
    (select id from sets where short_name = 'tpca'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tpca'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tpca'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boar'),
    (select id from sets where short_name = 'tpca'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Spawn'),
    (select id from sets where short_name = 'tpca'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'tpca'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spider'),
    (select id from sets where short_name = 'tpca'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ooze'),
    (select id from sets where short_name = 'tpca'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Spawn'),
    (select id from sets where short_name = 'tpca'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insect'),
    (select id from sets where short_name = 'tpca'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Spawn'),
    (select id from sets where short_name = 'tpca'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi'),
    (select id from sets where short_name = 'tpca'),
    '1',
    'common'
) 
 on conflict do nothing;
