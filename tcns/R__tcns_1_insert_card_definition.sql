    insert into mtgcard(name) values ('Zombie') on conflict do nothing;
    insert into mtgcard(name) values ('Squirrel') on conflict do nothing;
    insert into mtgcard(name) values ('Elephant') on conflict do nothing;
    insert into mtgcard(name) values ('Spirit') on conflict do nothing;
    insert into mtgcard(name) values ('Construct') on conflict do nothing;
    insert into mtgcard(name) values ('Demon') on conflict do nothing;
    insert into mtgcard(name) values ('Wolf') on conflict do nothing;
    insert into mtgcard(name) values ('Ogre') on conflict do nothing;
    insert into mtgcard(name) values ('Dack Fayden Emblem') on conflict do nothing;
