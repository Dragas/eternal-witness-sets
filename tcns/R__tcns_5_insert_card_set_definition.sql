insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tcns'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squirrel'),
    (select id from sets where short_name = 'tcns'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elephant'),
    (select id from sets where short_name = 'tcns'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tcns'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Construct'),
    (select id from sets where short_name = 'tcns'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon'),
    (select id from sets where short_name = 'tcns'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'tcns'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ogre'),
    (select id from sets where short_name = 'tcns'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dack Fayden Emblem'),
    (select id from sets where short_name = 'tcns'),
    '9',
    'common'
) 
 on conflict do nothing;
