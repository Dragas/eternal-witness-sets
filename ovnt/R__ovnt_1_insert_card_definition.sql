    insert into mtgcard(name) values ('Mox Emerald') on conflict do nothing;
    insert into mtgcard(name) values ('Black Lotus') on conflict do nothing;
    insert into mtgcard(name) values ('Mox Jet') on conflict do nothing;
    insert into mtgcard(name) values ('Time Walk') on conflict do nothing;
    insert into mtgcard(name) values ('Timetwister') on conflict do nothing;
    insert into mtgcard(name) values ('Mox Sapphire') on conflict do nothing;
    insert into mtgcard(name) values ('Mox Pearl') on conflict do nothing;
    insert into mtgcard(name) values ('Ancestral Recall') on conflict do nothing;
    insert into mtgcard(name) values ('Mox Ruby') on conflict do nothing;
