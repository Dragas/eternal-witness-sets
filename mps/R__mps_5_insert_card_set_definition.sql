insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ornithopter'),
    (select id from sets where short_name = 'mps'),
    '42',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sword of War and Peace'),
    (select id from sets where short_name = 'mps'),
    '51',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vedalken Shackles'),
    (select id from sets where short_name = 'mps'),
    '53',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Engineered Explosives'),
    (select id from sets where short_name = 'mps'),
    '36',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Duplicant'),
    (select id from sets where short_name = 'mps'),
    '35',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Platinum Angel'),
    (select id from sets where short_name = 'mps'),
    '46',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Black Vise'),
    (select id from sets where short_name = 'mps'),
    '32',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Planar Bridge'),
    (select id from sets where short_name = 'mps'),
    '45',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Verdurous Gearhulk'),
    (select id from sets where short_name = 'mps'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Steel Overseer'),
    (select id from sets where short_name = 'mps'),
    '27',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rings of Brighthearth'),
    (select id from sets where short_name = 'mps'),
    '21',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sword of Light and Shadow'),
    (select id from sets where short_name = 'mps'),
    '30',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mana Crypt'),
    (select id from sets where short_name = 'mps'),
    '16',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mana Vault'),
    (select id from sets where short_name = 'mps'),
    '17',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chalice of the Void'),
    (select id from sets where short_name = 'mps'),
    '33',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mox Opal'),
    (select id from sets where short_name = 'mps'),
    '19',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sword of Body and Mind'),
    (select id from sets where short_name = 'mps'),
    '50',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Crucible of Worlds'),
    (select id from sets where short_name = 'mps'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Painter''s Servant'),
    (select id from sets where short_name = 'mps'),
    '20',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gauntlet of Power'),
    (select id from sets where short_name = 'mps'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Static Orb'),
    (select id from sets where short_name = 'mps'),
    '26',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wurmcoil Engine'),
    (select id from sets where short_name = 'mps'),
    '54',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Staff of Domination'),
    (select id from sets where short_name = 'mps'),
    '48',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Trinisphere'),
    (select id from sets where short_name = 'mps'),
    '52',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Extraplanar Lens'),
    (select id from sets where short_name = 'mps'),
    '38',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Torrential Gearhulk'),
    (select id from sets where short_name = 'mps'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Champion''s Helm'),
    (select id from sets where short_name = 'mps'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'mps'),
    '24',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scroll Rack'),
    (select id from sets where short_name = 'mps'),
    '22',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chrome Mox'),
    (select id from sets where short_name = 'mps'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Oblivion Stone'),
    (select id from sets where short_name = 'mps'),
    '41',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Defense Grid'),
    (select id from sets where short_name = 'mps'),
    '34',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pithing Needle'),
    (select id from sets where short_name = 'mps'),
    '44',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Paradox Engine'),
    (select id from sets where short_name = 'mps'),
    '43',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Solemn Simulacrum'),
    (select id from sets where short_name = 'mps'),
    '25',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Combustible Gearhulk'),
    (select id from sets where short_name = 'mps'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Grindstone'),
    (select id from sets where short_name = 'mps'),
    '39',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ensnaring Bridge'),
    (select id from sets where short_name = 'mps'),
    '37',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lotus Petal'),
    (select id from sets where short_name = 'mps'),
    '15',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aether Vial'),
    (select id from sets where short_name = 'mps'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hangarback Walker'),
    (select id from sets where short_name = 'mps'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chromatic Lantern'),
    (select id from sets where short_name = 'mps'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sphere of Resistance'),
    (select id from sets where short_name = 'mps'),
    '47',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lightning Greaves'),
    (select id from sets where short_name = 'mps'),
    '14',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sword of Fire and Ice'),
    (select id from sets where short_name = 'mps'),
    '29',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Noxious Gearhulk'),
    (select id from sets where short_name = 'mps'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cloudstone Curio'),
    (select id from sets where short_name = 'mps'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sundering Titan'),
    (select id from sets where short_name = 'mps'),
    '49',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sculpting Steel'),
    (select id from sets where short_name = 'mps'),
    '23',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mind''s Eye'),
    (select id from sets where short_name = 'mps'),
    '18',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Arcbound Ravager'),
    (select id from sets where short_name = 'mps'),
    '31',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cataclysmic Gearhulk'),
    (select id from sets where short_name = 'mps'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Meekstone'),
    (select id from sets where short_name = 'mps'),
    '40',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sword of Feast and Famine'),
    (select id from sets where short_name = 'mps'),
    '28',
    'mythic'
) 
 on conflict do nothing;
