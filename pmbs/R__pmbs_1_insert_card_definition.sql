    insert into mtgcard(name) values ('Hero of Bladehold') on conflict do nothing;
    insert into mtgcard(name) values ('Glissa, the Traitor') on conflict do nothing;
    insert into mtgcard(name) values ('Treasure Mage') on conflict do nothing;
    insert into mtgcard(name) values ('Mirran Crusader') on conflict do nothing;
    insert into mtgcard(name) values ('Black Sun''s Zenith') on conflict do nothing;
    insert into mtgcard(name) values ('Thopter Assembly') on conflict do nothing;
