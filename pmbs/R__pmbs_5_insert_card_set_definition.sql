insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Hero of Bladehold'),
    (select id from sets where short_name = 'pmbs'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Glissa, the Traitor'),
    (select id from sets where short_name = 'pmbs'),
    '96',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Treasure Mage'),
    (select id from sets where short_name = 'pmbs'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mirran Crusader'),
    (select id from sets where short_name = 'pmbs'),
    '*14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Black Sun''s Zenith'),
    (select id from sets where short_name = 'pmbs'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thopter Assembly'),
    (select id from sets where short_name = 'pmbs'),
    '140',
    'rare'
) 
 on conflict do nothing;
