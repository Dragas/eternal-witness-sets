insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Qilin''s Blessing'),
    (select id from sets where short_name = 'gs1'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feiyi Snake'),
    (select id from sets where short_name = 'gs1'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'gs1'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brilliant Plan'),
    (select id from sets where short_name = 'gs1'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sacred White Deer'),
    (select id from sets where short_name = 'gs1'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drown in Shapelessness'),
    (select id from sets where short_name = 'gs1'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Journey for the Elixir'),
    (select id from sets where short_name = 'gs1'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'gs1'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aggressive Instinct'),
    (select id from sets where short_name = 'gs1'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earthshaking Si'),
    (select id from sets where short_name = 'gs1'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Breath of Fire'),
    (select id from sets where short_name = 'gs1'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Pangolin'),
    (select id from sets where short_name = 'gs1'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = 'gs1'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Screeching Phoenix'),
    (select id from sets where short_name = 'gs1'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon''s Presence'),
    (select id from sets where short_name = 'gs1'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hardened-Scale Armor'),
    (select id from sets where short_name = 'gs1'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cleansing Screech'),
    (select id from sets where short_name = 'gs1'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ferocious Zheng'),
    (select id from sets where short_name = 'gs1'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'gs1'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Colorful Feiyi Sparrow'),
    (select id from sets where short_name = 'gs1'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meandering River'),
    (select id from sets where short_name = 'gs1'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leopard-Spotted Jiao'),
    (select id from sets where short_name = 'gs1'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Confidence from Strength'),
    (select id from sets where short_name = 'gs1'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancestor Dragon'),
    (select id from sets where short_name = 'gs1'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nine-Tail White Fox'),
    (select id from sets where short_name = 'gs1'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire-Omen Crane'),
    (select id from sets where short_name = 'gs1'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mu Yanling'),
    (select id from sets where short_name = 'gs1'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Moon-Eating Dog'),
    (select id from sets where short_name = 'gs1'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Welkin Tern'),
    (select id from sets where short_name = 'gs1'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earth-Origin Yak'),
    (select id from sets where short_name = 'gs1'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mowu // Mowu'),
    (select id from sets where short_name = 'gs1'),
    'T1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloak of Mists'),
    (select id from sets where short_name = 'gs1'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormcloud Spirit'),
    (select id from sets where short_name = 'gs1'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rhythmic Water Vortex'),
    (select id from sets where short_name = 'gs1'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivid Flying Fish'),
    (select id from sets where short_name = 'gs1'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Timber Gorge'),
    (select id from sets where short_name = 'gs1'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'gs1'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Purple-Crystal Crab'),
    (select id from sets where short_name = 'gs1'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jiang Yanggu'),
    (select id from sets where short_name = 'gs1'),
    '22',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Heavenly Qilin'),
    (select id from sets where short_name = 'gs1'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armored Whirl Turtle'),
    (select id from sets where short_name = 'gs1'),
    '7',
    'common'
) 
 on conflict do nothing;
