insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Marit Lage'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Marit Lage'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Marit Lage'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Marit Lage'),
        (select types.id from types where name = 'Avatar')
    ) 
 on conflict do nothing;
