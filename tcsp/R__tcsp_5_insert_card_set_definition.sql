insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Marit Lage'),
    (select id from sets where short_name = 'tcsp'),
    '1',
    'rare'
) 
 on conflict do nothing;
