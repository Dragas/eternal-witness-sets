insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Copy'),
    (select id from sets where short_name = 'tznr'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Warrior'),
    (select id from sets where short_name = 'tznr'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drake'),
    (select id from sets where short_name = 'tznr'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illusion'),
    (select id from sets where short_name = 'tznr'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Construct'),
    (select id from sets where short_name = 'tznr'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat'),
    (select id from sets where short_name = 'tznr'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insect'),
    (select id from sets where short_name = 'tznr'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel Warrior'),
    (select id from sets where short_name = 'tznr'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Construct'),
    (select id from sets where short_name = 'tznr'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plant'),
    (select id from sets where short_name = 'tznr'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat Beast'),
    (select id from sets where short_name = 'tznr'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hydra'),
    (select id from sets where short_name = 'tznr'),
    '9',
    'common'
) 
 on conflict do nothing;
