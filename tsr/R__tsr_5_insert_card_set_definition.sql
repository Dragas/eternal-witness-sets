insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Chalice of the Void'),
    (select id from sets where short_name = 'tsr'),
    '390',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Relentless Rats'),
    (select id from sets where short_name = 'tsr'),
    '329',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Path to Exile'),
    (select id from sets where short_name = 'tsr'),
    '299',
    'mythic'
) 
 on conflict do nothing;
