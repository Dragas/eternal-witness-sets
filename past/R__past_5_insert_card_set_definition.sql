insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Call from the Grave'),
    (select id from sets where short_name = 'past'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Catapult'),
    (select id from sets where short_name = 'past'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Dragon'),
    (select id from sets where short_name = 'past'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Struggle'),
    (select id from sets where short_name = 'past'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aswan Jaguar'),
    (select id from sets where short_name = 'past'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rainbow Knights'),
    (select id from sets where short_name = 'past'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Polka Band'),
    (select id from sets where short_name = 'past'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gem Bazaar'),
    (select id from sets where short_name = 'past'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pandora''s Box'),
    (select id from sets where short_name = 'past'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prismatic Dragon'),
    (select id from sets where short_name = 'past'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necropolis of Azar'),
    (select id from sets where short_name = 'past'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whimsy'),
    (select id from sets where short_name = 'past'),
    '10',
    'common'
) 
 on conflict do nothing;
