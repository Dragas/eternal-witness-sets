insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Orzhov Guildgate'),
    (select id from sets where short_name = 'gtc'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skarrg Guildmage'),
    (select id from sets where short_name = 'gtc'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drakewing Krasis'),
    (select id from sets where short_name = 'gtc'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frenzied Tilling'),
    (select id from sets where short_name = 'gtc'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Primordial'),
    (select id from sets where short_name = 'gtc'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shadow Slice'),
    (select id from sets where short_name = 'gtc'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mugging'),
    (select id from sets where short_name = 'gtc'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sage''s Row Denizen'),
    (select id from sets where short_name = 'gtc'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mental Vapors'),
    (select id from sets where short_name = 'gtc'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shambleshark'),
    (select id from sets where short_name = 'gtc'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blind Obedience'),
    (select id from sets where short_name = 'gtc'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruul Ragebeast'),
    (select id from sets where short_name = 'gtc'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spark Trooper'),
    (select id from sets where short_name = 'gtc'),
    '199',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sacred Foundry'),
    (select id from sets where short_name = 'gtc'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diluvian Primordial'),
    (select id from sets where short_name = 'gtc'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hellkite Tyrant'),
    (select id from sets where short_name = 'gtc'),
    '94',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Foundry Street Denizen'),
    (select id from sets where short_name = 'gtc'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of Obligation'),
    (select id from sets where short_name = 'gtc'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Adaptive Snapjaw'),
    (select id from sets where short_name = 'gtc'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychic Strike'),
    (select id from sets where short_name = 'gtc'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serene Remembrance'),
    (select id from sets where short_name = 'gtc'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Miming Slime'),
    (select id from sets where short_name = 'gtc'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scorchwalker'),
    (select id from sets where short_name = 'gtc'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hellraiser Goblin'),
    (select id from sets where short_name = 'gtc'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aurelia''s Fury'),
    (select id from sets where short_name = 'gtc'),
    '144',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Master Biomancer'),
    (select id from sets where short_name = 'gtc'),
    '176',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tin Street Market'),
    (select id from sets where short_name = 'gtc'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simic Guildgate'),
    (select id from sets where short_name = 'gtc'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murder Investigation'),
    (select id from sets where short_name = 'gtc'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Biovisionary'),
    (select id from sets where short_name = 'gtc'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ordruun Veteran'),
    (select id from sets where short_name = 'gtc'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Executioner''s Swing'),
    (select id from sets where short_name = 'gtc'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Domri Rade'),
    (select id from sets where short_name = 'gtc'),
    '156',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Razortip Whip'),
    (select id from sets where short_name = 'gtc'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cartel Aristocrat'),
    (select id from sets where short_name = 'gtc'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Incursion Specialist'),
    (select id from sets where short_name = 'gtc'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ogre Slumlord'),
    (select id from sets where short_name = 'gtc'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skarrg Goliath'),
    (select id from sets where short_name = 'gtc'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Syndic of Tithes'),
    (select id from sets where short_name = 'gtc'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basilica Guards'),
    (select id from sets where short_name = 'gtc'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hydroform'),
    (select id from sets where short_name = 'gtc'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Experiment One'),
    (select id from sets where short_name = 'gtc'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Primal Visitation'),
    (select id from sets where short_name = 'gtc'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rubblebelt Raiders'),
    (select id from sets where short_name = 'gtc'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scatter Arc'),
    (select id from sets where short_name = 'gtc'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crackling Perimeter'),
    (select id from sets where short_name = 'gtc'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aetherize'),
    (select id from sets where short_name = 'gtc'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Signal the Clans'),
    (select id from sets where short_name = 'gtc'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kingpin''s Pet'),
    (select id from sets where short_name = 'gtc'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pit Fight'),
    (select id from sets where short_name = 'gtc'),
    '223',
    'common'
) ,
(
    (select id from mtgcard where name = 'Martial Glory'),
    (select id from sets where short_name = 'gtc'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crypt Ghast'),
    (select id from sets where short_name = 'gtc'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghor-Clan Rampager'),
    (select id from sets where short_name = 'gtc'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Truefire Paladin'),
    (select id from sets where short_name = 'gtc'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dinrova Horror'),
    (select id from sets where short_name = 'gtc'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fortress Cyclops'),
    (select id from sets where short_name = 'gtc'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ripscale Predator'),
    (select id from sets where short_name = 'gtc'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disciple of the Old Ways'),
    (select id from sets where short_name = 'gtc'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gift of Orzhova'),
    (select id from sets where short_name = 'gtc'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vizkopa Guildmage'),
    (select id from sets where short_name = 'gtc'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wojek Halberdiers'),
    (select id from sets where short_name = 'gtc'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spire Tracer'),
    (select id from sets where short_name = 'gtc'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunhome Guildmage'),
    (select id from sets where short_name = 'gtc'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Assemble the Legion'),
    (select id from sets where short_name = 'gtc'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mark for Death'),
    (select id from sets where short_name = 'gtc'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Predator''s Rapport'),
    (select id from sets where short_name = 'gtc'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Watery Grave'),
    (select id from sets where short_name = 'gtc'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hold the Gates'),
    (select id from sets where short_name = 'gtc'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Guildgate'),
    (select id from sets where short_name = 'gtc'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = 'gtc'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obzedat, Ghost Council'),
    (select id from sets where short_name = 'gtc'),
    '182',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dimir Keyrune'),
    (select id from sets where short_name = 'gtc'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Midnight Recovery'),
    (select id from sets where short_name = 'gtc'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stolen Identity'),
    (select id from sets where short_name = 'gtc'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clinging Anemones'),
    (select id from sets where short_name = 'gtc'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aurelia, the Warleader'),
    (select id from sets where short_name = 'gtc'),
    '143',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Furious Resistance'),
    (select id from sets where short_name = 'gtc'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fathom Mage'),
    (select id from sets where short_name = 'gtc'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Fluxmage'),
    (select id from sets where short_name = 'gtc'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slate Street Ruffian'),
    (select id from sets where short_name = 'gtc'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Metropolis Sprite'),
    (select id from sets where short_name = 'gtc'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voidwalk'),
    (select id from sets where short_name = 'gtc'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enter the Infinite'),
    (select id from sets where short_name = 'gtc'),
    '34',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gyre Sage'),
    (select id from sets where short_name = 'gtc'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Consuming Aberration'),
    (select id from sets where short_name = 'gtc'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duskmantle Seer'),
    (select id from sets where short_name = 'gtc'),
    '159',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Whispering Madness'),
    (select id from sets where short_name = 'gtc'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shielded Passage'),
    (select id from sets where short_name = 'gtc'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unexpected Results'),
    (select id from sets where short_name = 'gtc'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smog Elemental'),
    (select id from sets where short_name = 'gtc'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Grind'),
    (select id from sets where short_name = 'gtc'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Madcap Skills'),
    (select id from sets where short_name = 'gtc'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lazav, Dimir Mastermind'),
    (select id from sets where short_name = 'gtc'),
    '174',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Daring Skyjek'),
    (select id from sets where short_name = 'gtc'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bioshift'),
    (select id from sets where short_name = 'gtc'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Godless Shrine'),
    (select id from sets where short_name = 'gtc'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Assault Griffin'),
    (select id from sets where short_name = 'gtc'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frontline Medic'),
    (select id from sets where short_name = 'gtc'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wildwood Rebirth'),
    (select id from sets where short_name = 'gtc'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spell Rupture'),
    (select id from sets where short_name = 'gtc'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lord of the Void'),
    (select id from sets where short_name = 'gtc'),
    '71',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bane Alley Broker'),
    (select id from sets where short_name = 'gtc'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skygames'),
    (select id from sets where short_name = 'gtc'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undercity Plague'),
    (select id from sets where short_name = 'gtc'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zarichi Tiger'),
    (select id from sets where short_name = 'gtc'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul Keyrune'),
    (select id from sets where short_name = 'gtc'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skullcrack'),
    (select id from sets where short_name = 'gtc'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deathpact Angel'),
    (select id from sets where short_name = 'gtc'),
    '153',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Undercity Informer'),
    (select id from sets where short_name = 'gtc'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Reckoner'),
    (select id from sets where short_name = 'gtc'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gutter Skulk'),
    (select id from sets where short_name = 'gtc'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rapid Hybridization'),
    (select id from sets where short_name = 'gtc'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Corpse Blockade'),
    (select id from sets where short_name = 'gtc'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'High Priest of Penance'),
    (select id from sets where short_name = 'gtc'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foundry Champion'),
    (select id from sets where short_name = 'gtc'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tower Defense'),
    (select id from sets where short_name = 'gtc'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Borborygmos Enraged'),
    (select id from sets where short_name = 'gtc'),
    '147',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Guardian of the Gateless'),
    (select id from sets where short_name = 'gtc'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wrecking Ogre'),
    (select id from sets where short_name = 'gtc'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Immortal Servitude'),
    (select id from sets where short_name = 'gtc'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Devour Flesh'),
    (select id from sets where short_name = 'gtc'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burning-Tree Emissary'),
    (select id from sets where short_name = 'gtc'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guildscorn Ward'),
    (select id from sets where short_name = 'gtc'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ember Beast'),
    (select id from sets where short_name = 'gtc'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Massive Raid'),
    (select id from sets where short_name = 'gtc'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beckon Apparition'),
    (select id from sets where short_name = 'gtc'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Adephage'),
    (select id from sets where short_name = 'gtc'),
    '121',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cinder Elemental'),
    (select id from sets where short_name = 'gtc'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rubblehulk'),
    (select id from sets where short_name = 'gtc'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Charm'),
    (select id from sets where short_name = 'gtc'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dutiful Thrull'),
    (select id from sets where short_name = 'gtc'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul Charm'),
    (select id from sets where short_name = 'gtc'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Purge the Profane'),
    (select id from sets where short_name = 'gtc'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gideon, Champion of Justice'),
    (select id from sets where short_name = 'gtc'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mindeye Drake'),
    (select id from sets where short_name = 'gtc'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Holy Mantle'),
    (select id from sets where short_name = 'gtc'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scab-Clan Charger'),
    (select id from sets where short_name = 'gtc'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keymaster Rogue'),
    (select id from sets where short_name = 'gtc'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simic Charm'),
    (select id from sets where short_name = 'gtc'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shadow Alley Denizen'),
    (select id from sets where short_name = 'gtc'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illness in the Ranks'),
    (select id from sets where short_name = 'gtc'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Realmwright'),
    (select id from sets where short_name = 'gtc'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Millennial Gargoyle'),
    (select id from sets where short_name = 'gtc'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul Guildgate'),
    (select id from sets where short_name = 'gtc'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duskmantle Guildmage'),
    (select id from sets where short_name = 'gtc'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bomber Corps'),
    (select id from sets where short_name = 'gtc'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nav Squad Commandos'),
    (select id from sets where short_name = 'gtc'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urban Evolution'),
    (select id from sets where short_name = 'gtc'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Elite'),
    (select id from sets where short_name = 'gtc'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Act of Treason'),
    (select id from sets where short_name = 'gtc'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boros Guildgate'),
    (select id from sets where short_name = 'gtc'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ground Assault'),
    (select id from sets where short_name = 'gtc'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deathcult Rogue'),
    (select id from sets where short_name = 'gtc'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glaring Spotlight'),
    (select id from sets where short_name = 'gtc'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forced Adaptation'),
    (select id from sets where short_name = 'gtc'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shattering Blow'),
    (select id from sets where short_name = 'gtc'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crowned Ceratok'),
    (select id from sets where short_name = 'gtc'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Molten Primordial'),
    (select id from sets where short_name = 'gtc'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gateway Shade'),
    (select id from sets where short_name = 'gtc'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urbis Protector'),
    (select id from sets where short_name = 'gtc'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mystic Genesis'),
    (select id from sets where short_name = 'gtc'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyknight Legionnaire'),
    (select id from sets where short_name = 'gtc'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paranoid Delusions'),
    (select id from sets where short_name = 'gtc'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hands of Binding'),
    (select id from sets where short_name = 'gtc'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wight of Precinct Six'),
    (select id from sets where short_name = 'gtc'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Luminate Primordial'),
    (select id from sets where short_name = 'gtc'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death''s Approach'),
    (select id from sets where short_name = 'gtc'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prophetic Prism'),
    (select id from sets where short_name = 'gtc'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Way of the Thief'),
    (select id from sets where short_name = 'gtc'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crocanura'),
    (select id from sets where short_name = 'gtc'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dying Wish'),
    (select id from sets where short_name = 'gtc'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Legion Loyalist'),
    (select id from sets where short_name = 'gtc'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gridlock'),
    (select id from sets where short_name = 'gtc'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thespian''s Stage'),
    (select id from sets where short_name = 'gtc'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ivy Lane Denizen'),
    (select id from sets where short_name = 'gtc'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk of the Depths'),
    (select id from sets where short_name = 'gtc'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelic Skirmisher'),
    (select id from sets where short_name = 'gtc'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Contaminated Ground'),
    (select id from sets where short_name = 'gtc'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grisly Spectacle'),
    (select id from sets where short_name = 'gtc'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrull Parasite'),
    (select id from sets where short_name = 'gtc'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Keyrune'),
    (select id from sets where short_name = 'gtc'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rust Scarab'),
    (select id from sets where short_name = 'gtc'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Verdant Haven'),
    (select id from sets where short_name = 'gtc'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Last Thoughts'),
    (select id from sets where short_name = 'gtc'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aerial Maneuver'),
    (select id from sets where short_name = 'gtc'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arrows of Justice'),
    (select id from sets where short_name = 'gtc'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Court Street Denizen'),
    (select id from sets where short_name = 'gtc'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vizkopa Confessor'),
    (select id from sets where short_name = 'gtc'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Illusionist''s Bracers'),
    (select id from sets where short_name = 'gtc'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firefist Striker'),
    (select id from sets where short_name = 'gtc'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight Watch'),
    (select id from sets where short_name = 'gtc'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Syndicate Enforcer'),
    (select id from sets where short_name = 'gtc'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruination Wurm'),
    (select id from sets where short_name = 'gtc'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alms Beast'),
    (select id from sets where short_name = 'gtc'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zhur-Taa Swine'),
    (select id from sets where short_name = 'gtc'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simic Keyrune'),
    (select id from sets where short_name = 'gtc'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sapphire Drake'),
    (select id from sets where short_name = 'gtc'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Structural Collapse'),
    (select id from sets where short_name = 'gtc'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyblinder Staff'),
    (select id from sets where short_name = 'gtc'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burst of Strength'),
    (select id from sets where short_name = 'gtc'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basilica Screecher'),
    (select id from sets where short_name = 'gtc'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riot Gear'),
    (select id from sets where short_name = 'gtc'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boros Charm'),
    (select id from sets where short_name = 'gtc'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mortus Strider'),
    (select id from sets where short_name = 'gtc'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Homing Lightning'),
    (select id from sets where short_name = 'gtc'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Towering Thunderfist'),
    (select id from sets where short_name = 'gtc'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nimbus Swimmer'),
    (select id from sets where short_name = 'gtc'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coerced Confession'),
    (select id from sets where short_name = 'gtc'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warmind Infantry'),
    (select id from sets where short_name = 'gtc'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merciless Eviction'),
    (select id from sets where short_name = 'gtc'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frilled Oculus'),
    (select id from sets where short_name = 'gtc'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elusive Krasis'),
    (select id from sets where short_name = 'gtc'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balustrade Spy'),
    (select id from sets where short_name = 'gtc'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leyline Phantom'),
    (select id from sets where short_name = 'gtc'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Agoraphobia'),
    (select id from sets where short_name = 'gtc'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firemane Avenger'),
    (select id from sets where short_name = 'gtc'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angelic Edict'),
    (select id from sets where short_name = 'gtc'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudfin Raptor'),
    (select id from sets where short_name = 'gtc'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightveil Specter'),
    (select id from sets where short_name = 'gtc'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Manipulator'),
    (select id from sets where short_name = 'gtc'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wasteland Viper'),
    (select id from sets where short_name = 'gtc'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Greenside Watcher'),
    (select id from sets where short_name = 'gtc'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ooze Flux'),
    (select id from sets where short_name = 'gtc'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sepulchral Primordial'),
    (select id from sets where short_name = 'gtc'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Totally Lost'),
    (select id from sets where short_name = 'gtc'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Breeding Pool'),
    (select id from sets where short_name = 'gtc'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'One Thousand Lashes'),
    (select id from sets where short_name = 'gtc'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Call of the Nightwing'),
    (select id from sets where short_name = 'gtc'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clan Defiance'),
    (select id from sets where short_name = 'gtc'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treasury Thrull'),
    (select id from sets where short_name = 'gtc'),
    '201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stomping Ground'),
    (select id from sets where short_name = 'gtc'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prime Speaker Zegana'),
    (select id from sets where short_name = 'gtc'),
    '188',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dimir Charm'),
    (select id from sets where short_name = 'gtc'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Righteous Charge'),
    (select id from sets where short_name = 'gtc'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Horror of the Dim'),
    (select id from sets where short_name = 'gtc'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viashino Shanktail'),
    (select id from sets where short_name = 'gtc'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hindervines'),
    (select id from sets where short_name = 'gtc'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Killing Glare'),
    (select id from sets where short_name = 'gtc'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armored Transport'),
    (select id from sets where short_name = 'gtc'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Debtor''s Pulpit'),
    (select id from sets where short_name = 'gtc'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slaughterhorn'),
    (select id from sets where short_name = 'gtc'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Five-Alarm Fire'),
    (select id from sets where short_name = 'gtc'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alpha Authority'),
    (select id from sets where short_name = 'gtc'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Biomass Mutation'),
    (select id from sets where short_name = 'gtc'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Keyrune'),
    (select id from sets where short_name = 'gtc'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Ransom'),
    (select id from sets where short_name = 'gtc'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skinbrand Goblin'),
    (select id from sets where short_name = 'gtc'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zameck Guildmage'),
    (select id from sets where short_name = 'gtc'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smite'),
    (select id from sets where short_name = 'gtc'),
    '25',
    'common'
) 
 on conflict do nothing;
