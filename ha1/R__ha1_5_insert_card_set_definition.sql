insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Burning-Tree Emissary'),
    (select id from sets where short_name = 'ha1'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tendrils of Corruption'),
    (select id from sets where short_name = 'ha1'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cryptbreaker'),
    (select id from sets where short_name = 'ha1'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Visionary'),
    (select id from sets where short_name = 'ha1'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kinsbaile Cavalier'),
    (select id from sets where short_name = 'ha1'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hidetsugu''s Second Rite'),
    (select id from sets where short_name = 'ha1'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Specter'),
    (select id from sets where short_name = 'ha1'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imperious Perfect'),
    (select id from sets where short_name = 'ha1'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Warden'),
    (select id from sets where short_name = 'ha1'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Matron'),
    (select id from sets where short_name = 'ha1'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kiln Fiend'),
    (select id from sets where short_name = 'ha1'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Stone'),
    (select id from sets where short_name = 'ha1'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure Hunt'),
    (select id from sets where short_name = 'ha1'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Ascendant'),
    (select id from sets where short_name = 'ha1'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fauna Shaman'),
    (select id from sets where short_name = 'ha1'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Captain Sisay'),
    (select id from sets where short_name = 'ha1'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ornithopter'),
    (select id from sets where short_name = 'ha1'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darksteel Reactor'),
    (select id from sets where short_name = 'ha1'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Arena'),
    (select id from sets where short_name = 'ha1'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Distant Melody'),
    (select id from sets where short_name = 'ha1'),
    '5',
    'common'
) 
 on conflict do nothing;
