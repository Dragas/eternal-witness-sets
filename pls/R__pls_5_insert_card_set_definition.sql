insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Planeswalker''s Mirth'),
    (select id from sets where short_name = 'pls'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meteor Crater'),
    (select id from sets where short_name = 'pls'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orim''s Chant'),
    (select id from sets where short_name = 'pls'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aura Blast'),
    (select id from sets where short_name = 'pls'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lord of the Undead'),
    (select id from sets where short_name = 'pls'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heroic Defiance'),
    (select id from sets where short_name = 'pls'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shifting Sky'),
    (select id from sets where short_name = 'pls'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Planar Overlay'),
    (select id from sets where short_name = 'pls'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thornscape Battlemage'),
    (select id from sets where short_name = 'pls'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quirion Explorer'),
    (select id from sets where short_name = 'pls'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Radiant Kavu'),
    (select id from sets where short_name = 'pls'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meddling Mage'),
    (select id from sets where short_name = 'pls'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terminate'),
    (select id from sets where short_name = 'pls'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primal Growth'),
    (select id from sets where short_name = 'pls'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Draco'),
    (select id from sets where short_name = 'pls'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lashknife Barrier'),
    (select id from sets where short_name = 'pls'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightscape Familiar'),
    (select id from sets where short_name = 'pls'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunderscape Familiar'),
    (select id from sets where short_name = 'pls'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steel Leaf Paladin'),
    (select id from sets where short_name = 'pls'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Root Greevil'),
    (select id from sets where short_name = 'pls'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sawtooth Loon'),
    (select id from sets where short_name = 'pls'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Singe'),
    (select id from sets where short_name = 'pls'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stratadon'),
    (select id from sets where short_name = 'pls'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terminal Moraine'),
    (select id from sets where short_name = 'pls'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keldon Mantle'),
    (select id from sets where short_name = 'pls'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Kavu'),
    (select id from sets where short_name = 'pls'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Amphibious Kavu'),
    (select id from sets where short_name = 'pls'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daring Leap'),
    (select id from sets where short_name = 'pls'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warped Devotion'),
    (select id from sets where short_name = 'pls'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Waterspout Elemental'),
    (select id from sets where short_name = 'pls'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shivan Wurm'),
    (select id from sets where short_name = 'pls'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stormscape Familiar'),
    (select id from sets where short_name = 'pls'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exotic Disease'),
    (select id from sets where short_name = 'pls'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Confound'),
    (select id from sets where short_name = 'pls'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Allied Strategies'),
    (select id from sets where short_name = 'pls'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mogg Jailer'),
    (select id from sets where short_name = 'pls'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rushing River'),
    (select id from sets where short_name = 'pls'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ertai''s Trickery'),
    (select id from sets where short_name = 'pls'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alpha Kavu'),
    (select id from sets where short_name = 'pls'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shriek of Dread'),
    (select id from sets where short_name = 'pls'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hobble'),
    (select id from sets where short_name = 'pls'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunting Drake'),
    (select id from sets where short_name = 'pls'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunken Hope'),
    (select id from sets where short_name = 'pls'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyship Weatherlight'),
    (select id from sets where short_name = 'pls'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ertai, the Corrupted'),
    (select id from sets where short_name = 'pls'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forsaken City'),
    (select id from sets where short_name = 'pls'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Might'),
    (select id from sets where short_name = 'pls'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hull Breach'),
    (select id from sets where short_name = 'pls'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dralnu''s Crusade'),
    (select id from sets where short_name = 'pls'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tahngarth, Talruum Hero'),
    (select id from sets where short_name = 'pls'),
    '74★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Falling Timber'),
    (select id from sets where short_name = 'pls'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kavu Recluse'),
    (select id from sets where short_name = 'pls'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sleeping Potion'),
    (select id from sets where short_name = 'pls'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Planeswalker''s Fury'),
    (select id from sets where short_name = 'pls'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treva''s Charm'),
    (select id from sets where short_name = 'pls'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rith''s Grove'),
    (select id from sets where short_name = 'pls'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darigaaz''s Caldera'),
    (select id from sets where short_name = 'pls'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Spider'),
    (select id from sets where short_name = 'pls'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voice of All'),
    (select id from sets where short_name = 'pls'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thornscape Familiar'),
    (select id from sets where short_name = 'pls'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diabolic Intent'),
    (select id from sets where short_name = 'pls'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Morgue Toad'),
    (select id from sets where short_name = 'pls'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Bloodstock'),
    (select id from sets where short_name = 'pls'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Planeswalker''s Mischief'),
    (select id from sets where short_name = 'pls'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Malicious Advice'),
    (select id from sets where short_name = 'pls'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crosis''s Catacombs'),
    (select id from sets where short_name = 'pls'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stormscape Battlemage'),
    (select id from sets where short_name = 'pls'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Destructive Flow'),
    (select id from sets where short_name = 'pls'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marsh Crocodile'),
    (select id from sets where short_name = 'pls'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slingshot Goblin'),
    (select id from sets where short_name = 'pls'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doomsday Specter'),
    (select id from sets where short_name = 'pls'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mogg Sentry'),
    (select id from sets where short_name = 'pls'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dromar''s Cavern'),
    (select id from sets where short_name = 'pls'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arctic Merfolk'),
    (select id from sets where short_name = 'pls'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Down'),
    (select id from sets where short_name = 'pls'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treva''s Ruins'),
    (select id from sets where short_name = 'pls'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nemata, Grove Guardian'),
    (select id from sets where short_name = 'pls'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Planeswalker''s Scorn'),
    (select id from sets where short_name = 'pls'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magnigoth Treefolk'),
    (select id from sets where short_name = 'pls'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloud Cover'),
    (select id from sets where short_name = 'pls'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunscape Familiar'),
    (select id from sets where short_name = 'pls'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dominaria''s Judgment'),
    (select id from sets where short_name = 'pls'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pollen Remedy'),
    (select id from sets where short_name = 'pls'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyship Weatherlight'),
    (select id from sets where short_name = 'pls'),
    '133★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Surprise Deployment'),
    (select id from sets where short_name = 'pls'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gainsay'),
    (select id from sets where short_name = 'pls'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Implode'),
    (select id from sets where short_name = 'pls'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volcano Imp'),
    (select id from sets where short_name = 'pls'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunscape Battlemage'),
    (select id from sets where short_name = 'pls'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Herald'),
    (select id from sets where short_name = 'pls'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maggot Carrier'),
    (select id from sets where short_name = 'pls'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silver Drake'),
    (select id from sets where short_name = 'pls'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cavern Harpy'),
    (select id from sets where short_name = 'pls'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sinister Strength'),
    (select id from sets where short_name = 'pls'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Star Compass'),
    (select id from sets where short_name = 'pls'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pygmy Kavu'),
    (select id from sets where short_name = 'pls'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Planeswalker''s Favor'),
    (select id from sets where short_name = 'pls'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Samite Elder'),
    (select id from sets where short_name = 'pls'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crosis''s Charm'),
    (select id from sets where short_name = 'pls'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fleetfoot Panther'),
    (select id from sets where short_name = 'pls'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guard Dogs'),
    (select id from sets where short_name = 'pls'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mirrorwood Treefolk'),
    (select id from sets where short_name = 'pls'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sisay''s Ingenuity'),
    (select id from sets where short_name = 'pls'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Honorable Scout'),
    (select id from sets where short_name = 'pls'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadapult'),
    (select id from sets where short_name = 'pls'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aurora Griffin'),
    (select id from sets where short_name = 'pls'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Blessing'),
    (select id from sets where short_name = 'pls'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Samite Pilgrim'),
    (select id from sets where short_name = 'pls'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tahngarth, Talruum Hero'),
    (select id from sets where short_name = 'pls'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Natural Emergence'),
    (select id from sets where short_name = 'pls'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Cylix'),
    (select id from sets where short_name = 'pls'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Multani''s Harmony'),
    (select id from sets where short_name = 'pls'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dromar''s Charm'),
    (select id from sets where short_name = 'pls'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sea Snidd'),
    (select id from sets where short_name = 'pls'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Game'),
    (select id from sets where short_name = 'pls'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mire Kavu'),
    (select id from sets where short_name = 'pls'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dralnu''s Pet'),
    (select id from sets where short_name = 'pls'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Questing Phelddagrif'),
    (select id from sets where short_name = 'pls'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darigaaz''s Charm'),
    (select id from sets where short_name = 'pls'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gerrard''s Command'),
    (select id from sets where short_name = 'pls'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Razing Snidd'),
    (select id from sets where short_name = 'pls'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Guilt'),
    (select id from sets where short_name = 'pls'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Suspicions'),
    (select id from sets where short_name = 'pls'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightscape Battlemage'),
    (select id from sets where short_name = 'pls'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thunderscape Battlemage'),
    (select id from sets where short_name = 'pls'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Insolence'),
    (select id from sets where short_name = 'pls'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magma Burst'),
    (select id from sets where short_name = 'pls'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sparkcaster'),
    (select id from sets where short_name = 'pls'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Escape Routes'),
    (select id from sets where short_name = 'pls'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eladamri''s Call'),
    (select id from sets where short_name = 'pls'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rith''s Charm'),
    (select id from sets where short_name = 'pls'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keldon Twilight'),
    (select id from sets where short_name = 'pls'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disciple of Kangee'),
    (select id from sets where short_name = 'pls'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Noxious Vapors'),
    (select id from sets where short_name = 'pls'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu'),
    (select id from sets where short_name = 'pls'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Tyranny'),
    (select id from sets where short_name = 'pls'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Scuta'),
    (select id from sets where short_name = 'pls'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strafe'),
    (select id from sets where short_name = 'pls'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slay'),
    (select id from sets where short_name = 'pls'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Horned Kavu'),
    (select id from sets where short_name = 'pls'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'March of Souls'),
    (select id from sets where short_name = 'pls'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quirion Dryad'),
    (select id from sets where short_name = 'pls'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death Bomb'),
    (select id from sets where short_name = 'pls'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ertai, the Corrupted'),
    (select id from sets where short_name = 'pls'),
    '107★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Caldera Kavu'),
    (select id from sets where short_name = 'pls'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lava Zombie'),
    (select id from sets where short_name = 'pls'),
    '113',
    'common'
) 
 on conflict do nothing;
