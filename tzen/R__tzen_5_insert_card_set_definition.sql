insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Zombie Giant'),
    (select id from sets where short_name = 'tzen'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'tzen'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illusion'),
    (select id from sets where short_name = 'tzen'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tzen'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snake'),
    (select id from sets where short_name = 'tzen'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire'),
    (select id from sets where short_name = 'tzen'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird'),
    (select id from sets where short_name = 'tzen'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Soldier'),
    (select id from sets where short_name = 'tzen'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'tzen'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tzen'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk'),
    (select id from sets where short_name = 'tzen'),
    '5',
    'common'
) 
 on conflict do nothing;
