insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Shambling Remains'),
    (select id from sets where short_name = 'ddk'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scorched Rusalka'),
    (select id from sets where short_name = 'ddk'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Omens'),
    (select id from sets where short_name = 'ddk'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddk'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flame Slash'),
    (select id from sets where short_name = 'ddk'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strangling Soot'),
    (select id from sets where short_name = 'ddk'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mesmeric Fiend'),
    (select id from sets where short_name = 'ddk'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddk'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Absorb Vis'),
    (select id from sets where short_name = 'ddk'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terminate'),
    (select id from sets where short_name = 'ddk'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vithian Stinger'),
    (select id from sets where short_name = 'ddk'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddk'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sorin''s Thirst'),
    (select id from sets where short_name = 'ddk'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom General'),
    (select id from sets where short_name = 'ddk'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bump in the Night'),
    (select id from sets where short_name = 'ddk'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sorin, Lord of Innistrad'),
    (select id from sets where short_name = 'ddk'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Devil''s Play'),
    (select id from sets where short_name = 'ddk'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corpse Connoisseur'),
    (select id from sets where short_name = 'ddk'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duskhunter Bat'),
    (select id from sets where short_name = 'ddk'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Outcasts'),
    (select id from sets where short_name = 'ddk'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reassembling Skeleton'),
    (select id from sets where short_name = 'ddk'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mortify'),
    (select id from sets where short_name = 'ddk'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashmouth Hound'),
    (select id from sets where short_name = 'ddk'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakdos Carnarium'),
    (select id from sets where short_name = 'ddk'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Lacerator'),
    (select id from sets where short_name = 'ddk'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddk'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faithless Looting'),
    (select id from sets where short_name = 'ddk'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Torrent of Souls'),
    (select id from sets where short_name = 'ddk'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddk'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Revenant Patriarch'),
    (select id from sets where short_name = 'ddk'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Field of Souls'),
    (select id from sets where short_name = 'ddk'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampire''s Bite'),
    (select id from sets where short_name = 'ddk'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sulfuric Vortex'),
    (select id from sets where short_name = 'ddk'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddk'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hellrider'),
    (select id from sets where short_name = 'ddk'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skirsdag Cultist'),
    (select id from sets where short_name = 'ddk'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blightning'),
    (select id from sets where short_name = 'ddk'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodrage Vampire'),
    (select id from sets where short_name = 'ddk'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddk'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unmake'),
    (select id from sets where short_name = 'ddk'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Butcher of Malakir'),
    (select id from sets where short_name = 'ddk'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Breaking Point'),
    (select id from sets where short_name = 'ddk'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spectral Procession'),
    (select id from sets where short_name = 'ddk'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddk'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Recoup'),
    (select id from sets where short_name = 'ddk'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Browbeat'),
    (select id from sets where short_name = 'ddk'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Child of Night'),
    (select id from sets where short_name = 'ddk'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mad Prophet'),
    (select id from sets where short_name = 'ddk'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zealous Persecution'),
    (select id from sets where short_name = 'ddk'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tainted Field'),
    (select id from sets where short_name = 'ddk'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mark of the Vampire'),
    (select id from sets where short_name = 'ddk'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blazing Salvo'),
    (select id from sets where short_name = 'ddk'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = 'ddk'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddk'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mausoleum Guard'),
    (select id from sets where short_name = 'ddk'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddk'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyroclasm'),
    (select id from sets where short_name = 'ddk'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddk'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gatekeeper of Malakir'),
    (select id from sets where short_name = 'ddk'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doomed Traveler'),
    (select id from sets where short_name = 'ddk'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancient Craving'),
    (select id from sets where short_name = 'ddk'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twilight Drover'),
    (select id from sets where short_name = 'ddk'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death Grasp'),
    (select id from sets where short_name = 'ddk'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Geistflame'),
    (select id from sets where short_name = 'ddk'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddk'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Nighthawk'),
    (select id from sets where short_name = 'ddk'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Arsonist'),
    (select id from sets where short_name = 'ddk'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gang of Devils'),
    (select id from sets where short_name = 'ddk'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fiend Hunter'),
    (select id from sets where short_name = 'ddk'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flame Javelin'),
    (select id from sets where short_name = 'ddk'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Akoum Refuge'),
    (select id from sets where short_name = 'ddk'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scourge Devil'),
    (select id from sets where short_name = 'ddk'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'ddk'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hellspark Elemental'),
    (select id from sets where short_name = 'ddk'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coal Stoker'),
    (select id from sets where short_name = 'ddk'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urge to Feed'),
    (select id from sets where short_name = 'ddk'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lingering Souls'),
    (select id from sets where short_name = 'ddk'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Decompose'),
    (select id from sets where short_name = 'ddk'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lavaborn Muse'),
    (select id from sets where short_name = 'ddk'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tibalt, the Fiend-Blooded'),
    (select id from sets where short_name = 'ddk'),
    '41',
    'mythic'
) 
 on conflict do nothing;
