insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'jmp'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Buried Ruin'),
    (select id from sets where short_name = 'jmp'),
    '491',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crushing Canopy'),
    (select id from sets where short_name = 'jmp'),
    '386',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Shrieker'),
    (select id from sets where short_name = 'jmp'),
    '345',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'jmp'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cathar''s Companion'),
    (select id from sets where short_name = 'jmp'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silhana Wayfinder'),
    (select id from sets where short_name = 'jmp'),
    '430',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Archon of Justice'),
    (select id from sets where short_name = 'jmp'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leave in the Dust'),
    (select id from sets where short_name = 'jmp'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curious Obsession'),
    (select id from sets where short_name = 'jmp'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fanatical Firebrand'),
    (select id from sets where short_name = 'jmp'),
    '315',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Tower'),
    (select id from sets where short_name = 'jmp'),
    '493',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Last Gasp'),
    (select id from sets where short_name = 'jmp'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Auger Spree'),
    (select id from sets where short_name = 'jmp'),
    '449',
    'common'
) ,
(
    (select id from mtgcard where name = 'Affectionate Indrik'),
    (select id from sets where short_name = 'jmp'),
    '373',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'jmp'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ulvenwald Hydra'),
    (select id from sets where short_name = 'jmp'),
    '439',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hungry Flames'),
    (select id from sets where short_name = 'jmp'),
    '336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grave Bramble'),
    (select id from sets where short_name = 'jmp'),
    '401',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thought Collapse'),
    (select id from sets where short_name = 'jmp'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghoulcaller Gisa'),
    (select id from sets where short_name = 'jmp'),
    '236',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Raging Regisaur'),
    (select id from sets where short_name = 'jmp'),
    '455',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Neyith of the Dire Hunt'),
    (select id from sets where short_name = 'jmp'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'jmp'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Allosaurus Shepherd'),
    (select id from sets where short_name = 'jmp'),
    '28',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Coastal Piracy'),
    (select id from sets where short_name = 'jmp'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brightmare'),
    (select id from sets where short_name = 'jmp'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bathe in Dragonfire'),
    (select id from sets where short_name = 'jmp'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sailor of Means'),
    (select id from sets where short_name = 'jmp'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corpse Hauler'),
    (select id from sets where short_name = 'jmp'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodrock Cyclops'),
    (select id from sets where short_name = 'jmp'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Patron of the Valiant'),
    (select id from sets where short_name = 'jmp'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Celestial Mantle'),
    (select id from sets where short_name = 'jmp'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thought Scour'),
    (select id from sets where short_name = 'jmp'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exclude'),
    (select id from sets where short_name = 'jmp'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cloudreader Sphinx'),
    (select id from sets where short_name = 'jmp'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyroclastic Elemental'),
    (select id from sets where short_name = 'jmp'),
    '356',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thriving Bluff'),
    (select id from sets where short_name = 'jmp'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Brushstrider'),
    (select id from sets where short_name = 'jmp'),
    '434',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zendikar''s Roil'),
    (select id from sets where short_name = 'jmp'),
    '448',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fertilid'),
    (select id from sets where short_name = 'jmp'),
    '398',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thriving Moor'),
    (select id from sets where short_name = 'jmp'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampaging Brontodon'),
    (select id from sets where short_name = 'jmp'),
    '423',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feral Invocation'),
    (select id from sets where short_name = 'jmp'),
    '396',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chart a Course'),
    (select id from sets where short_name = 'jmp'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mikaeus, the Lunarch'),
    (select id from sets where short_name = 'jmp'),
    '123',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'jmp'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chained Brute'),
    (select id from sets where short_name = 'jmp'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Market'),
    (select id from sets where short_name = 'jmp'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'High Sentinels of Arashin'),
    (select id from sets where short_name = 'jmp'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orazca Frillback'),
    (select id from sets where short_name = 'jmp'),
    '416',
    'common'
) ,
(
    (select id from mtgcard where name = 'Valorous Stance'),
    (select id from sets where short_name = 'jmp'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Face of Divinity'),
    (select id from sets where short_name = 'jmp'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ornery Goblin'),
    (select id from sets where short_name = 'jmp'),
    '353',
    'common'
) ,
(
    (select id from mtgcard where name = 'Young Pyromancer'),
    (select id from sets where short_name = 'jmp'),
    '372',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu'),
    (select id from sets where short_name = 'jmp'),
    '319',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Archaeomender'),
    (select id from sets where short_name = 'jmp'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghirapur Guide'),
    (select id from sets where short_name = 'jmp'),
    '400',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barter in Blood'),
    (select id from sets where short_name = 'jmp'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Malakir Familiar'),
    (select id from sets where short_name = 'jmp'),
    '253',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'jmp'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Essence Flux'),
    (select id from sets where short_name = 'jmp'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sethron, Hurloon General'),
    (select id from sets where short_name = 'jmp'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'jmp'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Innocent Blood'),
    (select id from sets where short_name = 'jmp'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tibalt''s Rager'),
    (select id from sets where short_name = 'jmp'),
    '366',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magma Jet'),
    (select id from sets where short_name = 'jmp'),
    '346',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scroll of Avacyn'),
    (select id from sets where short_name = 'jmp'),
    '483',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'jmp'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Gargantua'),
    (select id from sets where short_name = 'jmp'),
    '265',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Read the Runes'),
    (select id from sets where short_name = 'jmp'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bake into a Pie'),
    (select id from sets where short_name = 'jmp'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Muxus, Goblin Grandee'),
    (select id from sets where short_name = 'jmp'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Furnace Whelp'),
    (select id from sets where short_name = 'jmp'),
    '323',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel of Mercy'),
    (select id from sets where short_name = 'jmp'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riddle of Lightning'),
    (select id from sets where short_name = 'jmp'),
    '359',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragonloft Idol'),
    (select id from sets where short_name = 'jmp'),
    '463',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gonti, Lord of Luxury'),
    (select id from sets where short_name = 'jmp'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blessed Spirits'),
    (select id from sets where short_name = 'jmp'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ravenous Baloth'),
    (select id from sets where short_name = 'jmp'),
    '424',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knight of the Tusk'),
    (select id from sets where short_name = 'jmp'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serendib Efreet'),
    (select id from sets where short_name = 'jmp'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Weaver of Lightning'),
    (select id from sets where short_name = 'jmp'),
    '371',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'jmp'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voice of the Provinces'),
    (select id from sets where short_name = 'jmp'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brindle Shoat'),
    (select id from sets where short_name = 'jmp'),
    '380',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drana, Liberator of Malakir'),
    (select id from sets where short_name = 'jmp'),
    '225',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wight of Precinct Six'),
    (select id from sets where short_name = 'jmp'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cauldron Familiar'),
    (select id from sets where short_name = 'jmp'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dualcaster Mage'),
    (select id from sets where short_name = 'jmp'),
    '313',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inspiring Unicorn'),
    (select id from sets where short_name = 'jmp'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Rally'),
    (select id from sets where short_name = 'jmp'),
    '329',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Divine Arrow'),
    (select id from sets where short_name = 'jmp'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Agonizing Syphon'),
    (select id from sets where short_name = 'jmp'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kitesail Corsair'),
    (select id from sets where short_name = 'jmp'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hamletback Goliath'),
    (select id from sets where short_name = 'jmp'),
    '332',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'jmp'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Initiate''s Companion'),
    (select id from sets where short_name = 'jmp'),
    '403',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'jmp'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lawless Broker'),
    (select id from sets where short_name = 'jmp'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Child of Night'),
    (select id from sets where short_name = 'jmp'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scholar of the Lost Trove'),
    (select id from sets where short_name = 'jmp'),
    '496',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guardian Idol'),
    (select id from sets where short_name = 'jmp'),
    '467',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Oracle'),
    (select id from sets where short_name = 'jmp'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spectral Sailor'),
    (select id from sets where short_name = 'jmp'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keeper of Fables'),
    (select id from sets where short_name = 'jmp'),
    '407',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jousting Dummy'),
    (select id from sets where short_name = 'jmp'),
    '470',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mugging'),
    (select id from sets where short_name = 'jmp'),
    '352',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boggart Brute'),
    (select id from sets where short_name = 'jmp'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talrand, Sky Summoner'),
    (select id from sets where short_name = 'jmp'),
    '181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zurzoth, Chaos Rider'),
    (select id from sets where short_name = 'jmp'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scrounging Bandar'),
    (select id from sets where short_name = 'jmp'),
    '428',
    'common'
) ,
(
    (select id from mtgcard where name = 'Miasmic Mummy'),
    (select id from sets where short_name = 'jmp'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tinybones, Trinket Thief'),
    (select id from sets where short_name = 'jmp'),
    '17',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ravenous Chupacabra'),
    (select id from sets where short_name = 'jmp'),
    '269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Erratic Visionary'),
    (select id from sets where short_name = 'jmp'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcane Encyclopedia'),
    (select id from sets where short_name = 'jmp'),
    '459',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Parasitic Implant'),
    (select id from sets where short_name = 'jmp'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kira, Great Glass-Spinner'),
    (select id from sets where short_name = 'jmp'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Craterhoof Behemoth'),
    (select id from sets where short_name = 'jmp'),
    '385',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Take Heart'),
    (select id from sets where short_name = 'jmp'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feral Hydra'),
    (select id from sets where short_name = 'jmp'),
    '395',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mentor of the Meek'),
    (select id from sets where short_name = 'jmp'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Artist'),
    (select id from sets where short_name = 'jmp'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Charmbreaker Devils'),
    (select id from sets where short_name = 'jmp'),
    '303',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sarkhan''s Rage'),
    (select id from sets where short_name = 'jmp'),
    '360',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nocturnal Feeder'),
    (select id from sets where short_name = 'jmp'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Departed Deckhand'),
    (select id from sets where short_name = 'jmp'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terrarion'),
    (select id from sets where short_name = 'jmp'),
    '488',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rapacious Dragon'),
    (select id from sets where short_name = 'jmp'),
    '358',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Penumbra Bobcat'),
    (select id from sets where short_name = 'jmp'),
    '418',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marauder''s Axe'),
    (select id from sets where short_name = 'jmp'),
    '473',
    'common'
) ,
(
    (select id from mtgcard where name = 'Branching Evolution'),
    (select id from sets where short_name = 'jmp'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primordial Sage'),
    (select id from sets where short_name = 'jmp'),
    '422',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Talrand''s Invocation'),
    (select id from sets where short_name = 'jmp'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Storm Sculptor'),
    (select id from sets where short_name = 'jmp'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragonspeaker Shaman'),
    (select id from sets where short_name = 'jmp'),
    '312',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cradle of Vitality'),
    (select id from sets where short_name = 'jmp'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corpse Traders'),
    (select id from sets where short_name = 'jmp'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sharding Sphinx'),
    (select id from sets where short_name = 'jmp'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wailing Ghoul'),
    (select id from sets where short_name = 'jmp'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'jmp'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stab Wound'),
    (select id from sets where short_name = 'jmp'),
    '281',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cadaver Imp'),
    (select id from sets where short_name = 'jmp'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Long Road Home'),
    (select id from sets where short_name = 'jmp'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rattlechains'),
    (select id from sets where short_name = 'jmp'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vedalken Archmage'),
    (select id from sets where short_name = 'jmp'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overgrown Battlement'),
    (select id from sets where short_name = 'jmp'),
    '417',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Verdant Embrace'),
    (select id from sets where short_name = 'jmp'),
    '441',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bulwark Giant'),
    (select id from sets where short_name = 'jmp'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Divination'),
    (select id from sets where short_name = 'jmp'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dawntreader Elk'),
    (select id from sets where short_name = 'jmp'),
    '387',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battleground Geist'),
    (select id from sets where short_name = 'jmp'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thirst for Knowledge'),
    (select id from sets where short_name = 'jmp'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Carven Caryatid'),
    (select id from sets where short_name = 'jmp'),
    '382',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fusion Elemental'),
    (select id from sets where short_name = 'jmp'),
    '451',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vedalken Entrancer'),
    (select id from sets where short_name = 'jmp'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doublecast'),
    (select id from sets where short_name = 'jmp'),
    '307',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rageblood Shaman'),
    (select id from sets where short_name = 'jmp'),
    '357',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elemental Uprising'),
    (select id from sets where short_name = 'jmp'),
    '390',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreamstone Hedron'),
    (select id from sets where short_name = 'jmp'),
    '464',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flames of the Firebrand'),
    (select id from sets where short_name = 'jmp'),
    '317',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leaf Gilder'),
    (select id from sets where short_name = 'jmp'),
    '408',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aerial Assault'),
    (select id from sets where short_name = 'jmp'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Spiritdancer'),
    (select id from sets where short_name = 'jmp'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Host'),
    (select id from sets where short_name = 'jmp'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Path to Exile'),
    (select id from sets where short_name = 'jmp'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'jmp'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flames of the Raze-Boar'),
    (select id from sets where short_name = 'jmp'),
    '318',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bruvac the Grandiloquent'),
    (select id from sets where short_name = 'jmp'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goblin Goon'),
    (select id from sets where short_name = 'jmp'),
    '326',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eternal Thirst'),
    (select id from sets where short_name = 'jmp'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'jmp'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Haven Pilgrim'),
    (select id from sets where short_name = 'jmp'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oracle of Mul Daya'),
    (select id from sets where short_name = 'jmp'),
    '415',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exhume'),
    (select id from sets where short_name = 'jmp'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Elemental'),
    (select id from sets where short_name = 'jmp'),
    '344',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scuttlemutt'),
    (select id from sets where short_name = 'jmp'),
    '484',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Torch Fiend'),
    (select id from sets where short_name = 'jmp'),
    '367',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oneirophage'),
    (select id from sets where short_name = 'jmp'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rhox Faithmender'),
    (select id from sets where short_name = 'jmp'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'jmp'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghoulcaller''s Accomplice'),
    (select id from sets where short_name = 'jmp'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightwalker'),
    (select id from sets where short_name = 'jmp'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reanimate'),
    (select id from sets where short_name = 'jmp'),
    '270',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Lore'),
    (select id from sets where short_name = 'jmp'),
    '328',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brushstrider'),
    (select id from sets where short_name = 'jmp'),
    '381',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Debaser'),
    (select id from sets where short_name = 'jmp'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drover of the Mighty'),
    (select id from sets where short_name = 'jmp'),
    '388',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'jmp'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cryptic Serpent'),
    (select id from sets where short_name = 'jmp'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Isamaru, Hound of Konda'),
    (select id from sets where short_name = 'jmp'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wildheart Invoker'),
    (select id from sets where short_name = 'jmp'),
    '444',
    'common'
) ,
(
    (select id from mtgcard where name = 'Assault Formation'),
    (select id from sets where short_name = 'jmp'),
    '378',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blighted Bat'),
    (select id from sets where short_name = 'jmp'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inferno Hellion'),
    (select id from sets where short_name = 'jmp'),
    '337',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kiln Fiend'),
    (select id from sets where short_name = 'jmp'),
    '338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bogbrew Witch'),
    (select id from sets where short_name = 'jmp'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Path of Bravery'),
    (select id from sets where short_name = 'jmp'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Somberwald Stag'),
    (select id from sets where short_name = 'jmp'),
    '431',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aegis Turtle'),
    (select id from sets where short_name = 'jmp'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fa''adiyah Seer'),
    (select id from sets where short_name = 'jmp'),
    '394',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ormos, Archive Keeper'),
    (select id from sets where short_name = 'jmp'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arbor Armament'),
    (select id from sets where short_name = 'jmp'),
    '376',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plagued Rusalka'),
    (select id from sets where short_name = 'jmp'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'jmp'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pouncing Cheetah'),
    (select id from sets where short_name = 'jmp'),
    '419',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'jmp'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'jmp'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drainpipe Vermin'),
    (select id from sets where short_name = 'jmp'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nebelgast Herald'),
    (select id from sets where short_name = 'jmp'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'jmp'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gird for Battle'),
    (select id from sets where short_name = 'jmp'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nature''s Way'),
    (select id from sets where short_name = 'jmp'),
    '412',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cinder Elemental'),
    (select id from sets where short_name = 'jmp'),
    '304',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Act of Treason'),
    (select id from sets where short_name = 'jmp'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rogue''s Gloves'),
    (select id from sets where short_name = 'jmp'),
    '479',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thriving Grove'),
    (select id from sets where short_name = 'jmp'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sigiled Starfish'),
    (select id from sets where short_name = 'jmp'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tandem Tactics'),
    (select id from sets where short_name = 'jmp'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Suspicious Bookcase'),
    (select id from sets where short_name = 'jmp'),
    '487',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hellrider'),
    (select id from sets where short_name = 'jmp'),
    '334',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rise of the Dark Realms'),
    (select id from sets where short_name = 'jmp'),
    '271',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ironshell Beetle'),
    (select id from sets where short_name = 'jmp'),
    '405',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dinrova Horror'),
    (select id from sets where short_name = 'jmp'),
    '450',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Awakener Druid'),
    (select id from sets where short_name = 'jmp'),
    '379',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Release the Dogs'),
    (select id from sets where short_name = 'jmp'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Lightning'),
    (select id from sets where short_name = 'jmp'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thriving Isle'),
    (select id from sets where short_name = 'jmp'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bone Picker'),
    (select id from sets where short_name = 'jmp'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Collateral Damage'),
    (select id from sets where short_name = 'jmp'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Towering-Wave Mystic'),
    (select id from sets where short_name = 'jmp'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windstorm Drake'),
    (select id from sets where short_name = 'jmp'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flame Lash'),
    (select id from sets where short_name = 'jmp'),
    '316',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghoulraiser'),
    (select id from sets where short_name = 'jmp'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oona''s Blackguard'),
    (select id from sets where short_name = 'jmp'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'jmp'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Arbiter'),
    (select id from sets where short_name = 'jmp'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gifted Aetherborn'),
    (select id from sets where short_name = 'jmp'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Makeshift Munitions'),
    (select id from sets where short_name = 'jmp'),
    '348',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Time to Feed'),
    (select id from sets where short_name = 'jmp'),
    '438',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Rager'),
    (select id from sets where short_name = 'jmp'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harvester of Souls'),
    (select id from sets where short_name = 'jmp'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Murmuring Phantasm'),
    (select id from sets where short_name = 'jmp'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cemetery Recruitment'),
    (select id from sets where short_name = 'jmp'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'jmp'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Chieftain'),
    (select id from sets where short_name = 'jmp'),
    '324',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rupture Spire'),
    (select id from sets where short_name = 'jmp'),
    '495',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tempting Witch'),
    (select id from sets where short_name = 'jmp'),
    '283',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sheoldred, Whispering One'),
    (select id from sets where short_name = 'jmp'),
    '278',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scourge of Nel Toth'),
    (select id from sets where short_name = 'jmp'),
    '274',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Borderland Minotaur'),
    (select id from sets where short_name = 'jmp'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barrage of Expendables'),
    (select id from sets where short_name = 'jmp'),
    '292',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enlarge'),
    (select id from sets where short_name = 'jmp'),
    '392',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cloudshift'),
    (select id from sets where short_name = 'jmp'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghalta, Primal Hunger'),
    (select id from sets where short_name = 'jmp'),
    '399',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warden of Evos Isle'),
    (select id from sets where short_name = 'jmp'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Riptide Laboratory'),
    (select id from sets where short_name = 'jmp'),
    '494',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancestral Statue'),
    (select id from sets where short_name = 'jmp'),
    '458',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Chosen'),
    (select id from sets where short_name = 'jmp'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Explore'),
    (select id from sets where short_name = 'jmp'),
    '393',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fling'),
    (select id from sets where short_name = 'jmp'),
    '320',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'jmp'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fell Specter'),
    (select id from sets where short_name = 'jmp'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dance with Devils'),
    (select id from sets where short_name = 'jmp'),
    '306',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Momentous Fall'),
    (select id from sets where short_name = 'jmp'),
    '411',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Diadem'),
    (select id from sets where short_name = 'jmp'),
    '343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sage''s Row Savant'),
    (select id from sets where short_name = 'jmp'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ogre Slumlord'),
    (select id from sets where short_name = 'jmp'),
    '260',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rumbling Baloth'),
    (select id from sets where short_name = 'jmp'),
    '426',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lathliss, Dragon Queen'),
    (select id from sets where short_name = 'jmp'),
    '340',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selhoff Occultist'),
    (select id from sets where short_name = 'jmp'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eternal Taskmaster'),
    (select id from sets where short_name = 'jmp'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Befuddle'),
    (select id from sets where short_name = 'jmp'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bubbling Cauldron'),
    (select id from sets where short_name = 'jmp'),
    '460',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Narcolepsy'),
    (select id from sets where short_name = 'jmp'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wizard''s Retort'),
    (select id from sets where short_name = 'jmp'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kalastria Nightwatch'),
    (select id from sets where short_name = 'jmp'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lurking Predators'),
    (select id from sets where short_name = 'jmp'),
    '410',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'jmp'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodhunter Bat'),
    (select id from sets where short_name = 'jmp'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'jmp'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Etali, Primal Storm'),
    (select id from sets where short_name = 'jmp'),
    '314',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'jmp'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Assassin''s Strike'),
    (select id from sets where short_name = 'jmp'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Emancipation Angel'),
    (select id from sets where short_name = 'jmp'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Languish'),
    (select id from sets where short_name = 'jmp'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'jmp'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sangromancer'),
    (select id from sets where short_name = 'jmp'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wildsize'),
    (select id from sets where short_name = 'jmp'),
    '445',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seismic Elemental'),
    (select id from sets where short_name = 'jmp'),
    '362',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Roving Keep'),
    (select id from sets where short_name = 'jmp'),
    '480',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Vines'),
    (select id from sets where short_name = 'jmp'),
    '443',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blindblast'),
    (select id from sets where short_name = 'jmp'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Outnumber'),
    (select id from sets where short_name = 'jmp'),
    '354',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashmouth Hound'),
    (select id from sets where short_name = 'jmp'),
    '290',
    'common'
) ,
(
    (select id from mtgcard where name = 'Towering Titan'),
    (select id from sets where short_name = 'jmp'),
    '31',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thundering Spineback'),
    (select id from sets where short_name = 'jmp'),
    '437',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woodborn Behemoth'),
    (select id from sets where short_name = 'jmp'),
    '446',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'jmp'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grim Lavamancer'),
    (select id from sets where short_name = 'jmp'),
    '331',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myr Sire'),
    (select id from sets where short_name = 'jmp'),
    '475',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winged Words'),
    (select id from sets where short_name = 'jmp'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Launch Party'),
    (select id from sets where short_name = 'jmp'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beetleback Chief'),
    (select id from sets where short_name = 'jmp'),
    '294',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aether Spellbomb'),
    (select id from sets where short_name = 'jmp'),
    '456',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molten Ravager'),
    (select id from sets where short_name = 'jmp'),
    '351',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feral Prowler'),
    (select id from sets where short_name = 'jmp'),
    '397',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Lost Thoughts'),
    (select id from sets where short_name = 'jmp'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Falkenrath Noble'),
    (select id from sets where short_name = 'jmp'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wishful Merfolk'),
    (select id from sets where short_name = 'jmp'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'jmp'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prophetic Prism'),
    (select id from sets where short_name = 'jmp'),
    '478',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scarecrone'),
    (select id from sets where short_name = 'jmp'),
    '482',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'jmp'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aegis of the Heavens'),
    (select id from sets where short_name = 'jmp'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nyxathid'),
    (select id from sets where short_name = 'jmp'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meteor Golem'),
    (select id from sets where short_name = 'jmp'),
    '474',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Corsair Captain'),
    (select id from sets where short_name = 'jmp'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archon of Redemption'),
    (select id from sets where short_name = 'jmp'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scholar of the Lost Trove'),
    (select id from sets where short_name = 'jmp'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forced Worship'),
    (select id from sets where short_name = 'jmp'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dauntless Onslaught'),
    (select id from sets where short_name = 'jmp'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trusty Retriever'),
    (select id from sets where short_name = 'jmp'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Elite'),
    (select id from sets where short_name = 'jmp'),
    '250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savage Stomp'),
    (select id from sets where short_name = 'jmp'),
    '427',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crookclaw Transmuter'),
    (select id from sets where short_name = 'jmp'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Waterknot'),
    (select id from sets where short_name = 'jmp'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heartfire'),
    (select id from sets where short_name = 'jmp'),
    '333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Visionary'),
    (select id from sets where short_name = 'jmp'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exquisite Blood'),
    (select id from sets where short_name = 'jmp'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moment of Heroism'),
    (select id from sets where short_name = 'jmp'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning-Core Excavator'),
    (select id from sets where short_name = 'jmp'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodbond Vampire'),
    (select id from sets where short_name = 'jmp'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pirate''s Cutlass'),
    (select id from sets where short_name = 'jmp'),
    '477',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inniaz, the Gale Force'),
    (select id from sets where short_name = 'jmp'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chamber Sentry'),
    (select id from sets where short_name = 'jmp'),
    '461',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thermo-Alchemist'),
    (select id from sets where short_name = 'jmp'),
    '365',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rishkar, Peema Renegade'),
    (select id from sets where short_name = 'jmp'),
    '425',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rishadan Airship'),
    (select id from sets where short_name = 'jmp'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volley Veteran'),
    (select id from sets where short_name = 'jmp'),
    '369',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Phoenix'),
    (select id from sets where short_name = 'jmp'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'jmp'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'jmp'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanitarium Skeleton'),
    (select id from sets where short_name = 'jmp'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minotaur Skullcleaver'),
    (select id from sets where short_name = 'jmp'),
    '349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alabaster Mage'),
    (select id from sets where short_name = 'jmp'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kels, Fight Fixer'),
    (select id from sets where short_name = 'jmp'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ironroot Warlord'),
    (select id from sets where short_name = 'jmp'),
    '452',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel of the Dire Hour'),
    (select id from sets where short_name = 'jmp'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chain Lightning'),
    (select id from sets where short_name = 'jmp'),
    '302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unstable Obelisk'),
    (select id from sets where short_name = 'jmp'),
    '489',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ambassador Oak'),
    (select id from sets where short_name = 'jmp'),
    '375',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skittering Surveyor'),
    (select id from sets where short_name = 'jmp'),
    '486',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armorcraft Judge'),
    (select id from sets where short_name = 'jmp'),
    '377',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fortify'),
    (select id from sets where short_name = 'jmp'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Scholar'),
    (select id from sets where short_name = 'jmp'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sarkhan''s Unsealing'),
    (select id from sets where short_name = 'jmp'),
    '361',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Presence of Gond'),
    (select id from sets where short_name = 'jmp'),
    '420',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whelming Wave'),
    (select id from sets where short_name = 'jmp'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Immolating Gyre'),
    (select id from sets where short_name = 'jmp'),
    '20',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forge Devil'),
    (select id from sets where short_name = 'jmp'),
    '322',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flurry of Horns'),
    (select id from sets where short_name = 'jmp'),
    '321',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crow of Dark Tidings'),
    (select id from sets where short_name = 'jmp'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'jmp'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirrodin''s Core'),
    (select id from sets where short_name = 'jmp'),
    '492',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gingerbrute'),
    (select id from sets where short_name = 'jmp'),
    '466',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunter''s Insight'),
    (select id from sets where short_name = 'jmp'),
    '402',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'jmp'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodrage Brawler'),
    (select id from sets where short_name = 'jmp'),
    '296',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'jmp'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hedron Archive'),
    (select id from sets where short_name = 'jmp'),
    '468',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'jmp'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voyage''s End'),
    (select id from sets where short_name = 'jmp'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Macabre Waltz'),
    (select id from sets where short_name = 'jmp'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curiosity'),
    (select id from sets where short_name = 'jmp'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Hatchling'),
    (select id from sets where short_name = 'jmp'),
    '310',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exclusion Mage'),
    (select id from sets where short_name = 'jmp'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lawmage''s Binding'),
    (select id from sets where short_name = 'jmp'),
    '453',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knightly Valor'),
    (select id from sets where short_name = 'jmp'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Commando'),
    (select id from sets where short_name = 'jmp'),
    '325',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampire Neonate'),
    (select id from sets where short_name = 'jmp'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battlefield Promotion'),
    (select id from sets where short_name = 'jmp'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primeval Bounty'),
    (select id from sets where short_name = 'jmp'),
    '421',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nightshade Stinger'),
    (select id from sets where short_name = 'jmp'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'jmp'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'jmp'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Cat'),
    (select id from sets where short_name = 'jmp'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Infestation'),
    (select id from sets where short_name = 'jmp'),
    '288',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Festering Newt'),
    (select id from sets where short_name = 'jmp'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sweep Away'),
    (select id from sets where short_name = 'jmp'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burglar Rat'),
    (select id from sets where short_name = 'jmp'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'jmp'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ronom Unicorn'),
    (select id from sets where short_name = 'jmp'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minotaur Sureshot'),
    (select id from sets where short_name = 'jmp'),
    '350',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krenko, Mob Boss'),
    (select id from sets where short_name = 'jmp'),
    '339',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Fodder'),
    (select id from sets where short_name = 'jmp'),
    '309',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'jmp'),
    '342',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vastwood Zendikon'),
    (select id from sets where short_name = 'jmp'),
    '440',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windreader Sphinx'),
    (select id from sets where short_name = 'jmp'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thragtusk'),
    (select id from sets where short_name = 'jmp'),
    '436',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Perilous Myr'),
    (select id from sets where short_name = 'jmp'),
    '476',
    'common'
) ,
(
    (select id from mtgcard where name = 'Homing Lightning'),
    (select id from sets where short_name = 'jmp'),
    '335',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warfire Javelineer'),
    (select id from sets where short_name = 'jmp'),
    '370',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shambling Goblin'),
    (select id from sets where short_name = 'jmp'),
    '277',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravewaker'),
    (select id from sets where short_name = 'jmp'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spiteful Prankster'),
    (select id from sets where short_name = 'jmp'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slate Street Ruffian'),
    (select id from sets where short_name = 'jmp'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhystic Study'),
    (select id from sets where short_name = 'jmp'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul of the Harvest'),
    (select id from sets where short_name = 'jmp'),
    '432',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Draconic Roar'),
    (select id from sets where short_name = 'jmp'),
    '308',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Champion of Lambholt'),
    (select id from sets where short_name = 'jmp'),
    '383',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonlord''s Servant'),
    (select id from sets where short_name = 'jmp'),
    '311',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mark of the Vampire'),
    (select id from sets where short_name = 'jmp'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwynen''s Elite'),
    (select id from sets where short_name = 'jmp'),
    '389',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ball Lightning'),
    (select id from sets where short_name = 'jmp'),
    '291',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mire Triton'),
    (select id from sets where short_name = 'jmp'),
    '257',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swarm of Bloodflies'),
    (select id from sets where short_name = 'jmp'),
    '282',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lena, Selfless Champion'),
    (select id from sets where short_name = 'jmp'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Salvage'),
    (select id from sets where short_name = 'jmp'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sin Prodder'),
    (select id from sets where short_name = 'jmp'),
    '363',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Juggernaut'),
    (select id from sets where short_name = 'jmp'),
    '471',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodshot Trainee'),
    (select id from sets where short_name = 'jmp'),
    '298',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Healer''s Hawk'),
    (select id from sets where short_name = 'jmp'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steel-Plume Marshal'),
    (select id from sets where short_name = 'jmp'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Linvala, Keeper of Silence'),
    (select id from sets where short_name = 'jmp'),
    '119',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Angelic Page'),
    (select id from sets where short_name = 'jmp'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Archaeologist'),
    (select id from sets where short_name = 'jmp'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spitting Earth'),
    (select id from sets where short_name = 'jmp'),
    '364',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = 'jmp'),
    '275',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nessian Hornbeetle'),
    (select id from sets where short_name = 'jmp'),
    '413',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Borderland Marauder'),
    (select id from sets where short_name = 'jmp'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mesa Unicorn'),
    (select id from sets where short_name = 'jmp'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Douse in Gloom'),
    (select id from sets where short_name = 'jmp'),
    '223',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Ranger'),
    (select id from sets where short_name = 'jmp'),
    '435',
    'common'
) ,
(
    (select id from mtgcard where name = 'Entomber Exarch'),
    (select id from sets where short_name = 'jmp'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tithebearer Giant'),
    (select id from sets where short_name = 'jmp'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Herald''s Horn'),
    (select id from sets where short_name = 'jmp'),
    '469',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'jmp'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Signpost Scarecrow'),
    (select id from sets where short_name = 'jmp'),
    '485',
    'common'
) ,
(
    (select id from mtgcard where name = 'Peel from Reality'),
    (select id from sets where short_name = 'jmp'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alloy Myr'),
    (select id from sets where short_name = 'jmp'),
    '457',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inspired Charge'),
    (select id from sets where short_name = 'jmp'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'jmp'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inspiring Captain'),
    (select id from sets where short_name = 'jmp'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Irresistible Prey'),
    (select id from sets where short_name = 'jmp'),
    '406',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inspiring Call'),
    (select id from sets where short_name = 'jmp'),
    '404',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sky Tether'),
    (select id from sets where short_name = 'jmp'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Funeral Rites'),
    (select id from sets where short_name = 'jmp'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Fallout'),
    (select id from sets where short_name = 'jmp'),
    '368',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Archangel'),
    (select id from sets where short_name = 'jmp'),
    '454',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'jmp'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chromatic Sphere'),
    (select id from sets where short_name = 'jmp'),
    '462',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Broodlings'),
    (select id from sets where short_name = 'jmp'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Settle the Score'),
    (select id from sets where short_name = 'jmp'),
    '276',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Belltower Sphinx'),
    (select id from sets where short_name = 'jmp'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gargoyle Sentinel'),
    (select id from sets where short_name = 'jmp'),
    '465',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'jmp'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runed Servitor'),
    (select id from sets where short_name = 'jmp'),
    '481',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lifecrafter''s Gift'),
    (select id from sets where short_name = 'jmp'),
    '409',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelic Edict'),
    (select id from sets where short_name = 'jmp'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gristle Grinner'),
    (select id from sets where short_name = 'jmp'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sporemound'),
    (select id from sets where short_name = 'jmp'),
    '433',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Instigator'),
    (select id from sets where short_name = 'jmp'),
    '327',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dutiful Attendant'),
    (select id from sets where short_name = 'jmp'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Witch of the Moors'),
    (select id from sets where short_name = 'jmp'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prescient Chimera'),
    (select id from sets where short_name = 'jmp'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raise the Alarm'),
    (select id from sets where short_name = 'jmp'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Indomitable Will'),
    (select id from sets where short_name = 'jmp'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wren''s Run Vanquisher'),
    (select id from sets where short_name = 'jmp'),
    '447',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Axe'),
    (select id from sets where short_name = 'jmp'),
    '341',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Commune with Dinosaurs'),
    (select id from sets where short_name = 'jmp'),
    '384',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prosperous Pirates'),
    (select id from sets where short_name = 'jmp'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Shortcutter'),
    (select id from sets where short_name = 'jmp'),
    '330',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warmonger''s Chariot'),
    (select id from sets where short_name = 'jmp'),
    '490',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Emiel the Blessed'),
    (select id from sets where short_name = 'jmp'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Affa Guard Hound'),
    (select id from sets where short_name = 'jmp'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mausoleum Turnkey'),
    (select id from sets where short_name = 'jmp'),
    '255',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'New Horizons'),
    (select id from sets where short_name = 'jmp'),
    '414',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Blossoms'),
    (select id from sets where short_name = 'jmp'),
    '442',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Supply Runners'),
    (select id from sets where short_name = 'jmp'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Geode'),
    (select id from sets where short_name = 'jmp'),
    '472',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death''s Approach'),
    (select id from sets where short_name = 'jmp'),
    '222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Reaver'),
    (select id from sets where short_name = 'jmp'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Reclamation'),
    (select id from sets where short_name = 'jmp'),
    '267',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selvala, Heart of the Wilds'),
    (select id from sets where short_name = 'jmp'),
    '429',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cathars'' Crusade'),
    (select id from sets where short_name = 'jmp'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Octoprophet'),
    (select id from sets where short_name = 'jmp'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bone Splinters'),
    (select id from sets where short_name = 'jmp'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thriving Heath'),
    (select id from sets where short_name = 'jmp'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magmaquake'),
    (select id from sets where short_name = 'jmp'),
    '347',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duelist''s Heritage'),
    (select id from sets where short_name = 'jmp'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aggressive Urge'),
    (select id from sets where short_name = 'jmp'),
    '374',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blessed Sanctuary'),
    (select id from sets where short_name = 'jmp'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Archdruid'),
    (select id from sets where short_name = 'jmp'),
    '391',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pillar of Flame'),
    (select id from sets where short_name = 'jmp'),
    '355',
    'common'
) 
 on conflict do nothing;
