insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Smother'),
    (select id from sets where short_name = 'f03'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whipcorder'),
    (select id from sets where short_name = 'f03'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'f03'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Priest of Titania'),
    (select id from sets where short_name = 'f03'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Withered Wretch'),
    (select id from sets where short_name = 'f03'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bottle Gnomes'),
    (select id from sets where short_name = 'f03'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Bombardment'),
    (select id from sets where short_name = 'f03'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Capsize'),
    (select id from sets where short_name = 'f03'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scragnoth'),
    (select id from sets where short_name = 'f03'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crystalline Sliver'),
    (select id from sets where short_name = 'f03'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Tusker'),
    (select id from sets where short_name = 'f03'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sparksmith'),
    (select id from sets where short_name = 'f03'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Muscle Sliver'),
    (select id from sets where short_name = 'f03'),
    '2',
    'rare'
) 
 on conflict do nothing;
