insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Jasmine Boreal'),
    (select id from sets where short_name = 'tsb'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jolrael, Empress of Beasts'),
    (select id from sets where short_name = 'tsb'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avoid Fate'),
    (select id from sets where short_name = 'tsb'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flying Men'),
    (select id from sets where short_name = 'tsb'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shadowmage Infiltrator'),
    (select id from sets where short_name = 'tsb'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Undead Warchief'),
    (select id from sets where short_name = 'tsb'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tribal Flames'),
    (select id from sets where short_name = 'tsb'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uthden Troll'),
    (select id from sets where short_name = 'tsb'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grinning Totem'),
    (select id from sets where short_name = 'tsb'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Browbeat'),
    (select id from sets where short_name = 'tsb'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Snowman'),
    (select id from sets where short_name = 'tsb'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestial Dawn'),
    (select id from sets where short_name = 'tsb'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Whelp'),
    (select id from sets where short_name = 'tsb'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Willbender'),
    (select id from sets where short_name = 'tsb'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gemstone Mine'),
    (select id from sets where short_name = 'tsb'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Withered Wretch'),
    (select id from sets where short_name = 'tsb'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unstable Mutation'),
    (select id from sets where short_name = 'tsb'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Oyster'),
    (select id from sets where short_name = 'tsb'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Blessing'),
    (select id from sets where short_name = 'tsb'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Librarian'),
    (select id from sets where short_name = 'tsb'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Angel'),
    (select id from sets where short_name = 'tsb'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pendelhaven'),
    (select id from sets where short_name = 'tsb'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scragnoth'),
    (select id from sets where short_name = 'tsb'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Void'),
    (select id from sets where short_name = 'tsb'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Moat'),
    (select id from sets where short_name = 'tsb'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Liege'),
    (select id from sets where short_name = 'tsb'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'War Barge'),
    (select id from sets where short_name = 'tsb'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coalition Victory'),
    (select id from sets where short_name = 'tsb'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp Mosquito'),
    (select id from sets where short_name = 'tsb'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stormbind'),
    (select id from sets where short_name = 'tsb'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiery Justice'),
    (select id from sets where short_name = 'tsb'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dauthi Slayer'),
    (select id from sets where short_name = 'tsb'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Assault // Battery'),
    (select id from sets where short_name = 'tsb'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sol''kanar the Swamp King'),
    (select id from sets where short_name = 'tsb'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evil Eye of Orms-by-Gore'),
    (select id from sets where short_name = 'tsb'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cockatrice'),
    (select id from sets where short_name = 'tsb'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pirate Ship'),
    (select id from sets where short_name = 'tsb'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soltari Priest'),
    (select id from sets where short_name = 'tsb'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fire Whip'),
    (select id from sets where short_name = 'tsb'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Essence Sliver'),
    (select id from sets where short_name = 'tsb'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ovinomancer'),
    (select id from sets where short_name = 'tsb'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enduring Renewal'),
    (select id from sets where short_name = 'tsb'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darkness'),
    (select id from sets where short_name = 'tsb'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bad Moon'),
    (select id from sets where short_name = 'tsb'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Collector'),
    (select id from sets where short_name = 'tsb'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zhalfirin Commander'),
    (select id from sets where short_name = 'tsb'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thornscape Battlemage'),
    (select id from sets where short_name = 'tsb'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystic Snake'),
    (select id from sets where short_name = 'tsb'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avatar of Woe'),
    (select id from sets where short_name = 'tsb'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merieke Ri Berit'),
    (select id from sets where short_name = 'tsb'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sengir Autocrat'),
    (select id from sets where short_name = 'tsb'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twisted Abomination'),
    (select id from sets where short_name = 'tsb'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Rack'),
    (select id from sets where short_name = 'tsb'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sindbad'),
    (select id from sets where short_name = 'tsb'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merfolk Assassin'),
    (select id from sets where short_name = 'tsb'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thallid'),
    (select id from sets where short_name = 'tsb'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Funeral Charm'),
    (select id from sets where short_name = 'tsb'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feldon''s Cane'),
    (select id from sets where short_name = 'tsb'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spike Feeder'),
    (select id from sets where short_name = 'tsb'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas'),
    (select id from sets where short_name = 'tsb'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hunting Moa'),
    (select id from sets where short_name = 'tsb'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Craw Giant'),
    (select id from sets where short_name = 'tsb'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hail Storm'),
    (select id from sets where short_name = 'tsb'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prodigal Sorcerer'),
    (select id from sets where short_name = 'tsb'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stormscape Familiar'),
    (select id from sets where short_name = 'tsb'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Auratog'),
    (select id from sets where short_name = 'tsb'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conspiracy'),
    (select id from sets where short_name = 'tsb'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonstorm'),
    (select id from sets where short_name = 'tsb'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tormod''s Crypt'),
    (select id from sets where short_name = 'tsb'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mindless Automaton'),
    (select id from sets where short_name = 'tsb'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiery Temper'),
    (select id from sets where short_name = 'tsb'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shadow Guildmage'),
    (select id from sets where short_name = 'tsb'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akroma, Angel of Wrath'),
    (select id from sets where short_name = 'tsb'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Squire'),
    (select id from sets where short_name = 'tsb'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Witch Hunter'),
    (select id from sets where short_name = 'tsb'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wildfire Emissary'),
    (select id from sets where short_name = 'tsb'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uncle Istvan'),
    (select id from sets where short_name = 'tsb'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Cloudscraper'),
    (select id from sets where short_name = 'tsb'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whirling Dervish'),
    (select id from sets where short_name = 'tsb'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serrated Arrows'),
    (select id from sets where short_name = 'tsb'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Defiant Vanguard'),
    (select id from sets where short_name = 'tsb'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Icatian Javelineers'),
    (select id from sets where short_name = 'tsb'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Suq''Ata Lancer'),
    (select id from sets where short_name = 'tsb'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disintegrate'),
    (select id from sets where short_name = 'tsb'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lord of Atlantis'),
    (select id from sets where short_name = 'tsb'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avalanche Riders'),
    (select id from sets where short_name = 'tsb'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dandân'),
    (select id from sets where short_name = 'tsb'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirari'),
    (select id from sets where short_name = 'tsb'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Honorable Passage'),
    (select id from sets where short_name = 'tsb'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voidmage Prodigy'),
    (select id from sets where short_name = 'tsb'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mistform Ultimus'),
    (select id from sets where short_name = 'tsb'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'tsb'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leviathan'),
    (select id from sets where short_name = 'tsb'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eron the Relentless'),
    (select id from sets where short_name = 'tsb'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghost Ship'),
    (select id from sets where short_name = 'tsb'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Psionic Blast'),
    (select id from sets where short_name = 'tsb'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pandemonium'),
    (select id from sets where short_name = 'tsb'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dodecapod'),
    (select id from sets where short_name = 'tsb'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moorish Cavalry'),
    (select id from sets where short_name = 'tsb'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sacred Mesa'),
    (select id from sets where short_name = 'tsb'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Claws of Gix'),
    (select id from sets where short_name = 'tsb'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Safe Haven'),
    (select id from sets where short_name = 'tsb'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vhati il-Dal'),
    (select id from sets where short_name = 'tsb'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spitting Slug'),
    (select id from sets where short_name = 'tsb'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Resurrection'),
    (select id from sets where short_name = 'tsb'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Roots'),
    (select id from sets where short_name = 'tsb'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Consecrate Land'),
    (select id from sets where short_name = 'tsb'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whispers of the Muse'),
    (select id from sets where short_name = 'tsb'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faceless Butcher'),
    (select id from sets where short_name = 'tsb'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desolation Giant'),
    (select id from sets where short_name = 'tsb'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arena'),
    (select id from sets where short_name = 'tsb'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orgg'),
    (select id from sets where short_name = 'tsb'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Undertaker'),
    (select id from sets where short_name = 'tsb'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Valor'),
    (select id from sets where short_name = 'tsb'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Call of the Herd'),
    (select id from sets where short_name = 'tsb'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystic Enforcer'),
    (select id from sets where short_name = 'tsb'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kobold Taskmaster'),
    (select id from sets where short_name = 'tsb'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verdeloth the Ancient'),
    (select id from sets where short_name = 'tsb'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desert'),
    (select id from sets where short_name = 'tsb'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stupor'),
    (select id from sets where short_name = 'tsb'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spined Sliver'),
    (select id from sets where short_name = 'tsb'),
    '101',
    'rare'
) 
 on conflict do nothing;
