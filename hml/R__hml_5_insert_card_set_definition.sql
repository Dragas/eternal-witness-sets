insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Faerie Noble'),
    (select id from sets where short_name = 'hml'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feroz''s Ban'),
    (select id from sets where short_name = 'hml'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Koskun Falls'),
    (select id from sets where short_name = 'hml'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mesa Falcon'),
    (select id from sets where short_name = 'hml'),
    '10b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Samite Alchemist'),
    (select id from sets where short_name = 'hml'),
    '13b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Funeral March'),
    (select id from sets where short_name = 'hml'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Labyrinth Minotaur'),
    (select id from sets where short_name = 'hml'),
    '30b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carapace'),
    (select id from sets where short_name = 'hml'),
    '84a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hungry Mist'),
    (select id from sets where short_name = 'hml'),
    '88a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feast of the Unicorn'),
    (select id from sets where short_name = 'hml'),
    '47a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cemetery Gate'),
    (select id from sets where short_name = 'hml'),
    '44a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sengir Autocrat'),
    (select id from sets where short_name = 'hml'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Joven'),
    (select id from sets where short_name = 'hml'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Willow Faerie'),
    (select id from sets where short_name = 'hml'),
    '99b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abbey Matron'),
    (select id from sets where short_name = 'hml'),
    '2a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Castle Sengir'),
    (select id from sets where short_name = 'hml'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soraya the Falconer'),
    (select id from sets where short_name = 'hml'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Joven''s Ferrets'),
    (select id from sets where short_name = 'hml'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Narwhal'),
    (select id from sets where short_name = 'hml'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primal Order'),
    (select id from sets where short_name = 'hml'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roterothopter'),
    (select id from sets where short_name = 'hml'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aysen Abbey'),
    (select id from sets where short_name = 'hml'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Torture'),
    (select id from sets where short_name = 'hml'),
    '59a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sengir Bats'),
    (select id from sets where short_name = 'hml'),
    '57b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Headstone'),
    (select id from sets where short_name = 'hml'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shrink'),
    (select id from sets where short_name = 'hml'),
    '97a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Truce'),
    (select id from sets where short_name = 'hml'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Koskun Keep'),
    (select id from sets where short_name = 'hml'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Mine'),
    (select id from sets where short_name = 'hml'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sengir Bats'),
    (select id from sets where short_name = 'hml'),
    '57a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Memory Lapse'),
    (select id from sets where short_name = 'hml'),
    '32a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clockwork Gnomes'),
    (select id from sets where short_name = 'hml'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Root Spider'),
    (select id from sets where short_name = 'hml'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Folk of An-Havva'),
    (select id from sets where short_name = 'hml'),
    '87b',
    'common'
) ,
(
    (select id from mtgcard where name = 'An-Havva Constable'),
    (select id from sets where short_name = 'hml'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coral Reef'),
    (select id from sets where short_name = 'hml'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cemetery Gate'),
    (select id from sets where short_name = 'hml'),
    '44b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leaping Lizard'),
    (select id from sets where short_name = 'hml'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anaba Shaman'),
    (select id from sets where short_name = 'hml'),
    '67b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Torture'),
    (select id from sets where short_name = 'hml'),
    '59b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Sea Clan'),
    (select id from sets where short_name = 'hml'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dwarven Trader'),
    (select id from sets where short_name = 'hml'),
    '72a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandler'),
    (select id from sets where short_name = 'hml'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'An-Zerrin Ruins'),
    (select id from sets where short_name = 'hml'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Irini Sengir'),
    (select id from sets where short_name = 'hml'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Baron Sengir'),
    (select id from sets where short_name = 'hml'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drudge Spell'),
    (select id from sets where short_name = 'hml'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Kelp'),
    (select id from sets where short_name = 'hml'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dwarven Pony'),
    (select id from sets where short_name = 'hml'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dry Spell'),
    (select id from sets where short_name = 'hml'),
    '46a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anaba Bodyguard'),
    (select id from sets where short_name = 'hml'),
    '66b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reef Pirates'),
    (select id from sets where short_name = 'hml'),
    '36a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mammoth Harness'),
    (select id from sets where short_name = 'hml'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Samite Alchemist'),
    (select id from sets where short_name = 'hml'),
    '13a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Aviary'),
    (select id from sets where short_name = 'hml'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Memory Lapse'),
    (select id from sets where short_name = 'hml'),
    '32b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carapace'),
    (select id from sets where short_name = 'hml'),
    '84b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abbey Gargoyles'),
    (select id from sets where short_name = 'hml'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aysen Crusader'),
    (select id from sets where short_name = 'hml'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Paladin'),
    (select id from sets where short_name = 'hml'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mesa Falcon'),
    (select id from sets where short_name = 'hml'),
    '10a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aysen Bureaucrats'),
    (select id from sets where short_name = 'hml'),
    '3b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spectral Bears'),
    (select id from sets where short_name = 'hml'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aliban''s Tower'),
    (select id from sets where short_name = 'hml'),
    '61a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ebony Rhino'),
    (select id from sets where short_name = 'hml'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ihsan''s Shade'),
    (select id from sets where short_name = 'hml'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Didgeridoo'),
    (select id from sets where short_name = 'hml'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Willow Faerie'),
    (select id from sets where short_name = 'hml'),
    '99a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serrated Arrows'),
    (select id from sets where short_name = 'hml'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Carriage'),
    (select id from sets where short_name = 'hml'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ambush Party'),
    (select id from sets where short_name = 'hml'),
    '63a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anaba Bodyguard'),
    (select id from sets where short_name = 'hml'),
    '66a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea Sprite'),
    (select id from sets where short_name = 'hml'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mystic Decree'),
    (select id from sets where short_name = 'hml'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Greater Werewolf'),
    (select id from sets where short_name = 'hml'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Trader'),
    (select id from sets where short_name = 'hml'),
    '72b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Timmerian Fiends'),
    (select id from sets where short_name = 'hml'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leeches'),
    (select id from sets where short_name = 'hml'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aysen Bureaucrats'),
    (select id from sets where short_name = 'hml'),
    '3a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daughter of Autumn'),
    (select id from sets where short_name = 'hml'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dry Spell'),
    (select id from sets where short_name = 'hml'),
    '46b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast Walkers'),
    (select id from sets where short_name = 'hml'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'An-Havva Township'),
    (select id from sets where short_name = 'hml'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clockwork Steed'),
    (select id from sets where short_name = 'hml'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Broken Visage'),
    (select id from sets where short_name = 'hml'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Folk of An-Havva'),
    (select id from sets where short_name = 'hml'),
    '87a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evaporate'),
    (select id from sets where short_name = 'hml'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hungry Mist'),
    (select id from sets where short_name = 'hml'),
    '88b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reveka, Wizard Savant'),
    (select id from sets where short_name = 'hml'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Bestiary'),
    (select id from sets where short_name = 'hml'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rysorian Badger'),
    (select id from sets where short_name = 'hml'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Renewal'),
    (select id from sets where short_name = 'hml'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aysen Highway'),
    (select id from sets where short_name = 'hml'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hazduhr the Abbot'),
    (select id from sets where short_name = 'hml'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winter Sky'),
    (select id from sets where short_name = 'hml'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Retribution'),
    (select id from sets where short_name = 'hml'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eron the Relentless'),
    (select id from sets where short_name = 'hml'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marjhan'),
    (select id from sets where short_name = 'hml'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reef Pirates'),
    (select id from sets where short_name = 'hml'),
    '36b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea Troll'),
    (select id from sets where short_name = 'hml'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ambush'),
    (select id from sets where short_name = 'hml'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Albatross'),
    (select id from sets where short_name = 'hml'),
    '27a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Maze'),
    (select id from sets where short_name = 'hml'),
    '25a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anaba Ancestor'),
    (select id from sets where short_name = 'hml'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anaba Shaman'),
    (select id from sets where short_name = 'hml'),
    '67a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Willow Priestess'),
    (select id from sets where short_name = 'hml'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wizards'' School'),
    (select id from sets where short_name = 'hml'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aether Storm'),
    (select id from sets where short_name = 'hml'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Autumn Willow'),
    (select id from sets where short_name = 'hml'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Apocalypse Chime'),
    (select id from sets where short_name = 'hml'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghost Hounds'),
    (select id from sets where short_name = 'hml'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Joven''s Tools'),
    (select id from sets where short_name = 'hml'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Maze'),
    (select id from sets where short_name = 'hml'),
    '25b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merchant Scroll'),
    (select id from sets where short_name = 'hml'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aliban''s Tower'),
    (select id from sets where short_name = 'hml'),
    '61b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shrink'),
    (select id from sets where short_name = 'hml'),
    '97b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rashka the Slayer'),
    (select id from sets where short_name = 'hml'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forget'),
    (select id from sets where short_name = 'hml'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clockwork Swarm'),
    (select id from sets where short_name = 'hml'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heart Wolf'),
    (select id from sets where short_name = 'hml'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death Speakers'),
    (select id from sets where short_name = 'hml'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Albatross'),
    (select id from sets where short_name = 'hml'),
    '27b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baki''s Curse'),
    (select id from sets where short_name = 'hml'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trade Caravan'),
    (select id from sets where short_name = 'hml'),
    '19b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ironclaw Curse'),
    (select id from sets where short_name = 'hml'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anaba Spirit Crafter'),
    (select id from sets where short_name = 'hml'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ambush Party'),
    (select id from sets where short_name = 'hml'),
    '63b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Labyrinth Minotaur'),
    (select id from sets where short_name = 'hml'),
    '30a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grandmother Sengir'),
    (select id from sets where short_name = 'hml'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roots'),
    (select id from sets where short_name = 'hml'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'An-Havva Inn'),
    (select id from sets where short_name = 'hml'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feast of the Unicorn'),
    (select id from sets where short_name = 'hml'),
    '47b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chain Stasis'),
    (select id from sets where short_name = 'hml'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Inquisitors'),
    (select id from sets where short_name = 'hml'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prophecy'),
    (select id from sets where short_name = 'hml'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Oyster'),
    (select id from sets where short_name = 'hml'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jinx'),
    (select id from sets where short_name = 'hml'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abbey Matron'),
    (select id from sets where short_name = 'hml'),
    '2b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trade Caravan'),
    (select id from sets where short_name = 'hml'),
    '19a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Veldrane of Sengir'),
    (select id from sets where short_name = 'hml'),
    '60',
    'rare'
) 
 on conflict do nothing;
