insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Poison Counter'),
    (select id from sets where short_name = 'tnph'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr'),
    (select id from sets where short_name = 'tnph'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tnph'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golem'),
    (select id from sets where short_name = 'tnph'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tnph'),
    '2',
    'common'
) 
 on conflict do nothing;
