insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Soul of Zendikar'),
    (select id from sets where short_name = 'pdp15'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Soul of Ravnica'),
    (select id from sets where short_name = 'pdp15'),
    '1',
    'mythic'
) 
 on conflict do nothing;
