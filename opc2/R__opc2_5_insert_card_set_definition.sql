insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Truga Jungle'),
    (select id from sets where short_name = 'opc2'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nephalia'),
    (select id from sets where short_name = 'opc2'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Planewide Disaster'),
    (select id from sets where short_name = 'opc2'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Zephyr Maze'),
    (select id from sets where short_name = 'opc2'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spatial Merging'),
    (select id from sets where short_name = 'opc2'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aretopolis'),
    (select id from sets where short_name = 'opc2'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orochi Colony'),
    (select id from sets where short_name = 'opc2'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quicksilver Sea'),
    (select id from sets where short_name = 'opc2'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Loft Gardens'),
    (select id from sets where short_name = 'opc2'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reality Shaping'),
    (select id from sets where short_name = 'opc2'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kharasha Foothills'),
    (select id from sets where short_name = 'opc2'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windriddle Palaces'),
    (select id from sets where short_name = 'opc2'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chaotic Aether'),
    (select id from sets where short_name = 'opc2'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glen Elendra'),
    (select id from sets where short_name = 'opc2'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Onakke Catacomb'),
    (select id from sets where short_name = 'opc2'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodhill Bastion'),
    (select id from sets where short_name = 'opc2'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time Distortion'),
    (select id from sets where short_name = 'opc2'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mount Keralia'),
    (select id from sets where short_name = 'opc2'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Furnace Layer'),
    (select id from sets where short_name = 'opc2'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jund'),
    (select id from sets where short_name = 'opc2'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Interplanar Tunnel'),
    (select id from sets where short_name = 'opc2'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mutual Epiphany'),
    (select id from sets where short_name = 'opc2'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Astral Arena'),
    (select id from sets where short_name = 'opc2'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grand Ossuary'),
    (select id from sets where short_name = 'opc2'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kilnspire District'),
    (select id from sets where short_name = 'opc2'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Morphic Tide'),
    (select id from sets where short_name = 'opc2'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prahv'),
    (select id from sets where short_name = 'opc2'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orzhova'),
    (select id from sets where short_name = 'opc2'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grove of the Dreampods'),
    (select id from sets where short_name = 'opc2'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hedron Fields of Agadeem'),
    (select id from sets where short_name = 'opc2'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Norn''s Dominion'),
    (select id from sets where short_name = 'opc2'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talon Gates'),
    (select id from sets where short_name = 'opc2'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gavony'),
    (select id from sets where short_name = 'opc2'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lair of the Ashen Idol'),
    (select id from sets where short_name = 'opc2'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trail of the Mage-Rings'),
    (select id from sets where short_name = 'opc2'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Takenuma'),
    (select id from sets where short_name = 'opc2'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akoum'),
    (select id from sets where short_name = 'opc2'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stensia'),
    (select id from sets where short_name = 'opc2'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kessig'),
    (select id from sets where short_name = 'opc2'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Edge of Malacol'),
    (select id from sets where short_name = 'opc2'),
    '13',
    'common'
) 
 on conflict do nothing;
