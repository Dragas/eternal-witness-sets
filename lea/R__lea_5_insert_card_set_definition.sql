insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Dwarven Demolition Team'),
    (select id from sets where short_name = 'lea'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sinkhole'),
    (select id from sets where short_name = 'lea'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evil Presence'),
    (select id from sets where short_name = 'lea'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crusade'),
    (select id from sets where short_name = 'lea'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Forces'),
    (select id from sets where short_name = 'lea'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fear'),
    (select id from sets where short_name = 'lea'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hill Giant'),
    (select id from sets where short_name = 'lea'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'lea'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feedback'),
    (select id from sets where short_name = 'lea'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guardian Angel'),
    (select id from sets where short_name = 'lea'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Benalish Hero'),
    (select id from sets where short_name = 'lea'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lifetap'),
    (select id from sets where short_name = 'lea'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sacrifice'),
    (select id from sets where short_name = 'lea'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = 'lea'),
    '266',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conversion'),
    (select id from sets where short_name = 'lea'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meekstone'),
    (select id from sets where short_name = 'lea'),
    '260',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Stone'),
    (select id from sets where short_name = 'lea'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Ward'),
    (select id from sets where short_name = 'lea'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Invisibility'),
    (select id from sets where short_name = 'lea'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Vault'),
    (select id from sets where short_name = 'lea'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'lea'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Green'),
    (select id from sets where short_name = 'lea'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Sink'),
    (select id from sets where short_name = 'lea'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Terrain'),
    (select id from sets where short_name = 'lea'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Archers'),
    (select id from sets where short_name = 'lea'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Green Ward'),
    (select id from sets where short_name = 'lea'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blue Elemental Blast'),
    (select id from sets where short_name = 'lea'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = 'lea'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lord of Atlantis'),
    (select id from sets where short_name = 'lea'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Force of Nature'),
    (select id from sets where short_name = 'lea'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wanderlust'),
    (select id from sets where short_name = 'lea'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Library of Leng'),
    (select id from sets where short_name = 'lea'),
    '257',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drudge Skeletons'),
    (select id from sets where short_name = 'lea'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathgrip'),
    (select id from sets where short_name = 'lea'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thoughtlace'),
    (select id from sets where short_name = 'lea'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'lea'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clockwork Beast'),
    (select id from sets where short_name = 'lea'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lord of the Pit'),
    (select id from sets where short_name = 'lea'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Icy Manipulator'),
    (select id from sets where short_name = 'lea'),
    '248',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lure'),
    (select id from sets where short_name = 'lea'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Net'),
    (select id from sets where short_name = 'lea'),
    '270',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merfolk of the Pearl Trident'),
    (select id from sets where short_name = 'lea'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magical Hack'),
    (select id from sets where short_name = 'lea'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dwarven Warriors'),
    (select id from sets where short_name = 'lea'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Purelace'),
    (select id from sets where short_name = 'lea'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uthden Troll'),
    (select id from sets where short_name = 'lea'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Copper Tablet'),
    (select id from sets where short_name = 'lea'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Two-Headed Giant of Foriys'),
    (select id from sets where short_name = 'lea'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Farmstead'),
    (select id from sets where short_name = 'lea'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simulacrum'),
    (select id from sets where short_name = 'lea'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mahamoti Djinn'),
    (select id from sets where short_name = 'lea'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'lea'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weakness'),
    (select id from sets where short_name = 'lea'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Helm of Chatzuk'),
    (select id from sets where short_name = 'lea'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Web'),
    (select id from sets where short_name = 'lea'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Camouflage'),
    (select id from sets where short_name = 'lea'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'lea'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lifelace'),
    (select id from sets where short_name = 'lea'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zombie Master'),
    (select id from sets where short_name = 'lea'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firebreathing'),
    (select id from sets where short_name = 'lea'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forcefield'),
    (select id from sets where short_name = 'lea'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kormus Bell'),
    (select id from sets where short_name = 'lea'),
    '256',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rock Hydra'),
    (select id from sets where short_name = 'lea'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bayou'),
    (select id from sets where short_name = 'lea'),
    '278',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Water'),
    (select id from sets where short_name = 'lea'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lich'),
    (select id from sets where short_name = 'lea'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scavenging Ghoul'),
    (select id from sets where short_name = 'lea'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bad Moon'),
    (select id from sets where short_name = 'lea'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'White Ward'),
    (select id from sets where short_name = 'lea'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jade Monolith'),
    (select id from sets where short_name = 'lea'),
    '252',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Living Wall'),
    (select id from sets where short_name = 'lea'),
    '258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Obsianus Golem'),
    (select id from sets where short_name = 'lea'),
    '267',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Resurrection'),
    (select id from sets where short_name = 'lea'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'White Knight'),
    (select id from sets where short_name = 'lea'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = 'lea'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = 'lea'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Balloon Brigade'),
    (select id from sets where short_name = 'lea'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'lea'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howling Mine'),
    (select id from sets where short_name = 'lea'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hurricane'),
    (select id from sets where short_name = 'lea'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Hive'),
    (select id from sets where short_name = 'lea'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = 'lea'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twiddle'),
    (select id from sets where short_name = 'lea'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iron Star'),
    (select id from sets where short_name = 'lea'),
    '250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = 'lea'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin King'),
    (select id from sets where short_name = 'lea'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Royal Assassin'),
    (select id from sets where short_name = 'lea'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fungusaur'),
    (select id from sets where short_name = 'lea'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = 'lea'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Instill Energy'),
    (select id from sets where short_name = 'lea'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drain Life'),
    (select id from sets where short_name = 'lea'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Taiga'),
    (select id from sets where short_name = 'lea'),
    '282',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Manabarbs'),
    (select id from sets where short_name = 'lea'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'lea'),
    '290',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raging River'),
    (select id from sets where short_name = 'lea'),
    '168',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Power Surge'),
    (select id from sets where short_name = 'lea'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Braingeyser'),
    (select id from sets where short_name = 'lea'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Illusionary Mask'),
    (select id from sets where short_name = 'lea'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Northern Paladin'),
    (select id from sets where short_name = 'lea'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dingus Egg'),
    (select id from sets where short_name = 'lea'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animate Artifact'),
    (select id from sets where short_name = 'lea'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Basalt Monolith'),
    (select id from sets where short_name = 'lea'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bog Wraith'),
    (select id from sets where short_name = 'lea'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wheel of Fortune'),
    (select id from sets where short_name = 'lea'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flight'),
    (select id from sets where short_name = 'lea'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howl from Beyond'),
    (select id from sets where short_name = 'lea'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = 'lea'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Whelp'),
    (select id from sets where short_name = 'lea'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demonic Hordes'),
    (select id from sets where short_name = 'lea'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scryb Sprites'),
    (select id from sets where short_name = 'lea'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pearled Unicorn'),
    (select id from sets where short_name = 'lea'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plateau'),
    (select id from sets where short_name = 'lea'),
    '279',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'lea'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karma'),
    (select id from sets where short_name = 'lea'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balance'),
    (select id from sets where short_name = 'lea'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathlace'),
    (select id from sets where short_name = 'lea'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancestral Recall'),
    (select id from sets where short_name = 'lea'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonic Tutor'),
    (select id from sets where short_name = 'lea'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Badlands'),
    (select id from sets where short_name = 'lea'),
    '277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chaoslace'),
    (select id from sets where short_name = 'lea'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roc of Kher Ridges'),
    (select id from sets where short_name = 'lea'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gray Ogre'),
    (select id from sets where short_name = 'lea'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Short'),
    (select id from sets where short_name = 'lea'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crystal Rod'),
    (select id from sets where short_name = 'lea'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vesuvan Doppelganger'),
    (select id from sets where short_name = 'lea'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Black Vise'),
    (select id from sets where short_name = 'lea'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquility'),
    (select id from sets where short_name = 'lea'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = 'lea'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Red Elemental Blast'),
    (select id from sets where short_name = 'lea'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hurloon Minotaur'),
    (select id from sets where short_name = 'lea'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Control Magic'),
    (select id from sets where short_name = 'lea'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smoke'),
    (select id from sets where short_name = 'lea'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'False Orders'),
    (select id from sets where short_name = 'lea'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Flare'),
    (select id from sets where short_name = 'lea'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone Giant'),
    (select id from sets where short_name = 'lea'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Lands'),
    (select id from sets where short_name = 'lea'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nettling Imp'),
    (select id from sets where short_name = 'lea'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shanodin Dryads'),
    (select id from sets where short_name = 'lea'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mox Sapphire'),
    (select id from sets where short_name = 'lea'),
    '265',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steal Artifact'),
    (select id from sets where short_name = 'lea'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spell Blast'),
    (select id from sets where short_name = 'lea'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Blue'),
    (select id from sets where short_name = 'lea'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = 'lea'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disintegrate'),
    (select id from sets where short_name = 'lea'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mox Ruby'),
    (select id from sets where short_name = 'lea'),
    '264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cyclopean Tomb'),
    (select id from sets where short_name = 'lea'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jade Statue'),
    (select id from sets where short_name = 'lea'),
    '253',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Water Elemental'),
    (select id from sets where short_name = 'lea'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Wood'),
    (select id from sets where short_name = 'lea'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mox Pearl'),
    (select id from sets where short_name = 'lea'),
    '263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animate Dead'),
    (select id from sets where short_name = 'lea'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keldon Warlord'),
    (select id from sets where short_name = 'lea'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Time Vault'),
    (select id from sets where short_name = 'lea'),
    '274',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unholy Strength'),
    (select id from sets where short_name = 'lea'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'lea'),
    '288',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Oriflamme'),
    (select id from sets where short_name = 'lea'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chaos Orb'),
    (select id from sets where short_name = 'lea'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mox Jet'),
    (select id from sets where short_name = 'lea'),
    '262',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: White'),
    (select id from sets where short_name = 'lea'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winter Orb'),
    (select id from sets where short_name = 'lea'),
    '275',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blue Ward'),
    (select id from sets where short_name = 'lea'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reverse Damage'),
    (select id from sets where short_name = 'lea'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savannah'),
    (select id from sets where short_name = 'lea'),
    '280',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Word of Command'),
    (select id from sets where short_name = 'lea'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Contract from Below'),
    (select id from sets where short_name = 'lea'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blaze of Glory'),
    (select id from sets where short_name = 'lea'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ice Storm'),
    (select id from sets where short_name = 'lea'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ivory Cup'),
    (select id from sets where short_name = 'lea'),
    '251',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Swords'),
    (select id from sets where short_name = 'lea'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Timetwister'),
    (select id from sets where short_name = 'lea'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cockatrice'),
    (select id from sets where short_name = 'lea'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warp Artifact'),
    (select id from sets where short_name = 'lea'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tsunami'),
    (select id from sets where short_name = 'lea'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Verduran Enchantress'),
    (select id from sets where short_name = 'lea'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tundra'),
    (select id from sets where short_name = 'lea'),
    '284',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burrowing'),
    (select id from sets where short_name = 'lea'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Will-o''-the-Wisp'),
    (select id from sets where short_name = 'lea'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'lea'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Throne of Bone'),
    (select id from sets where short_name = 'lea'),
    '273',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Celestial Prism'),
    (select id from sets where short_name = 'lea'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fastbond'),
    (select id from sets where short_name = 'lea'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Psionic Blast'),
    (select id from sets where short_name = 'lea'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Earthbind'),
    (select id from sets where short_name = 'lea'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Eruption'),
    (select id from sets where short_name = 'lea'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Natural Selection'),
    (select id from sets where short_name = 'lea'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gloom'),
    (select id from sets where short_name = 'lea'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Artillery'),
    (select id from sets where short_name = 'lea'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tropical Island'),
    (select id from sets where short_name = 'lea'),
    '283',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stream of Life'),
    (select id from sets where short_name = 'lea'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Bone'),
    (select id from sets where short_name = 'lea'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Brambles'),
    (select id from sets where short_name = 'lea'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rod of Ruin'),
    (select id from sets where short_name = 'lea'),
    '268',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Holy Armor'),
    (select id from sets where short_name = 'lea'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Castle'),
    (select id from sets where short_name = 'lea'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mox Emerald'),
    (select id from sets where short_name = 'lea'),
    '261',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Black Lotus'),
    (select id from sets where short_name = 'lea'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sedge Troll'),
    (select id from sets where short_name = 'lea'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'lea'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tunnel'),
    (select id from sets where short_name = 'lea'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Earth Elemental'),
    (select id from sets where short_name = 'lea'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kudzu'),
    (select id from sets where short_name = 'lea'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plague Rats'),
    (select id from sets where short_name = 'lea'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = 'lea'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Specter'),
    (select id from sets where short_name = 'lea'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mons''s Goblin Raiders'),
    (select id from sets where short_name = 'lea'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stasis'),
    (select id from sets where short_name = 'lea'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'lea'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Regeneration'),
    (select id from sets where short_name = 'lea'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = 'lea'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ironroot Treefolk'),
    (select id from sets where short_name = 'lea'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Regrowth'),
    (select id from sets where short_name = 'lea'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Timber Wolves'),
    (select id from sets where short_name = 'lea'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'lea'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wooden Sphere'),
    (select id from sets where short_name = 'lea'),
    '276',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Paralyze'),
    (select id from sets where short_name = 'lea'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scrubland'),
    (select id from sets where short_name = 'lea'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Craw Wurm'),
    (select id from sets where short_name = 'lea'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunglasses of Urza'),
    (select id from sets where short_name = 'lea'),
    '271',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island Sanctuary'),
    (select id from sets where short_name = 'lea'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Black Knight'),
    (select id from sets where short_name = 'lea'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Channel'),
    (select id from sets where short_name = 'lea'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'lea'),
    '269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Conservator'),
    (select id from sets where short_name = 'lea'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'War Mammoth'),
    (select id from sets where short_name = 'lea'),
    '227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Living Artifact'),
    (select id from sets where short_name = 'lea'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disrupting Scepter'),
    (select id from sets where short_name = 'lea'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jayemdae Tome'),
    (select id from sets where short_name = 'lea'),
    '254',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Personal Incarnation'),
    (select id from sets where short_name = 'lea'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glasses of Urza'),
    (select id from sets where short_name = 'lea'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jump'),
    (select id from sets where short_name = 'lea'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Veteran Bodyguard'),
    (select id from sets where short_name = 'lea'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Ice'),
    (select id from sets where short_name = 'lea'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Power Leak'),
    (select id from sets where short_name = 'lea'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = 'lea'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grizzly Bears'),
    (select id from sets where short_name = 'lea'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cursed Land'),
    (select id from sets where short_name = 'lea'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fog'),
    (select id from sets where short_name = 'lea'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savannah Lions'),
    (select id from sets where short_name = 'lea'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pirate Ship'),
    (select id from sets where short_name = 'lea'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea Serpent'),
    (select id from sets where short_name = 'lea'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Righteousness'),
    (select id from sets where short_name = 'lea'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frozen Shade'),
    (select id from sets where short_name = 'lea'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Consecrate Land'),
    (select id from sets where short_name = 'lea'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sleight of Mind'),
    (select id from sets where short_name = 'lea'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pestilence'),
    (select id from sets where short_name = 'lea'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'lea'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ironclaw Orcs'),
    (select id from sets where short_name = 'lea'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'lea'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Animate Wall'),
    (select id from sets where short_name = 'lea'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Siren''s Call'),
    (select id from sets where short_name = 'lea'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fire Elemental'),
    (select id from sets where short_name = 'lea'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gauntlet of Might'),
    (select id from sets where short_name = 'lea'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Air'),
    (select id from sets where short_name = 'lea'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Juggernaut'),
    (select id from sets where short_name = 'lea'),
    '255',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lance'),
    (select id from sets where short_name = 'lea'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Time Walk'),
    (select id from sets where short_name = 'lea'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Red Ward'),
    (select id from sets where short_name = 'lea'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Berserk'),
    (select id from sets where short_name = 'lea'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Healing Salve'),
    (select id from sets where short_name = 'lea'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lifeforce'),
    (select id from sets where short_name = 'lea'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Liege'),
    (select id from sets where short_name = 'lea'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantom Monster'),
    (select id from sets where short_name = 'lea'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prodigal Sorcerer'),
    (select id from sets where short_name = 'lea'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = 'lea'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fork'),
    (select id from sets where short_name = 'lea'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darkpact'),
    (select id from sets where short_name = 'lea'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thicket Basilisk'),
    (select id from sets where short_name = 'lea'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Holy Strength'),
    (select id from sets where short_name = 'lea'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scathe Zombies'),
    (select id from sets where short_name = 'lea'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drain Power'),
    (select id from sets where short_name = 'lea'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mesa Pegasus'),
    (select id from sets where short_name = 'lea'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'lea'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'lea'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'lea'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Creature Bond'),
    (select id from sets where short_name = 'lea'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flashfires'),
    (select id from sets where short_name = 'lea'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Twist'),
    (select id from sets where short_name = 'lea'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Samite Healer'),
    (select id from sets where short_name = 'lea'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Fire'),
    (select id from sets where short_name = 'lea'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clone'),
    (select id from sets where short_name = 'lea'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blessing'),
    (select id from sets where short_name = 'lea'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nether Shadow'),
    (select id from sets where short_name = 'lea'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Granite Gargoyle'),
    (select id from sets where short_name = 'lea'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'lea'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychic Venom'),
    (select id from sets where short_name = 'lea'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ankh of Mishra'),
    (select id from sets where short_name = 'lea'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'lea'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ley Druid'),
    (select id from sets where short_name = 'lea'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death Ward'),
    (select id from sets where short_name = 'lea'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Copy Artifact'),
    (select id from sets where short_name = 'lea'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonic Attorney'),
    (select id from sets where short_name = 'lea'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wild Growth'),
    (select id from sets where short_name = 'lea'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aspect of Wolf'),
    (select id from sets where short_name = 'lea'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = 'lea'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Underground Sea'),
    (select id from sets where short_name = 'lea'),
    '285',
    'rare'
) 
 on conflict do nothing;
