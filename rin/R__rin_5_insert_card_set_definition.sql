insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = 'rin'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Avenger'),
    (select id from sets where short_name = 'rin'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'War Elephant'),
    (select id from sets where short_name = 'rin'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strip Mine'),
    (select id from sets where short_name = 'rin'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tawnos''s Wand'),
    (select id from sets where short_name = 'rin'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = 'rin'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sandstorm'),
    (select id from sets where short_name = 'rin'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Repentant Blacksmith'),
    (select id from sets where short_name = 'rin'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grapeshot Catapult'),
    (select id from sets where short_name = 'rin'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Battle Gear'),
    (select id from sets where short_name = 'rin'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sindbad'),
    (select id from sets where short_name = 'rin'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = 'rin'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clay Statue'),
    (select id from sets where short_name = 'rin'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Triskelion'),
    (select id from sets where short_name = 'rin'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feldon''s Cane'),
    (select id from sets where short_name = 'rin'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cyclone'),
    (select id from sets where short_name = 'rin'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cuombajj Witches'),
    (select id from sets where short_name = 'rin'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = 'rin'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nafs Asp'),
    (select id from sets where short_name = 'rin'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Argothian Pixies'),
    (select id from sets where short_name = 'rin'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Metamorphosis'),
    (select id from sets where short_name = 'rin'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bronze Tablet'),
    (select id from sets where short_name = 'rin'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakalite'),
    (select id from sets where short_name = 'rin'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = 'rin'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ali Baba'),
    (select id from sets where short_name = 'rin'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tawnos''s Weaponry'),
    (select id from sets where short_name = 'rin'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abu Ja''far'),
    (select id from sets where short_name = 'rin'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jeweled Bird'),
    (select id from sets where short_name = 'rin'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Xenic Poltergeist'),
    (select id from sets where short_name = 'rin'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oasis'),
    (select id from sets where short_name = 'rin'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = 'rin'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dandân'),
    (select id from sets where short_name = 'rin'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Tortoise'),
    (select id from sets where short_name = 'rin'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Amulet of Kroog'),
    (select id from sets where short_name = 'rin'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coral Helm'),
    (select id from sets where short_name = 'rin'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Erhnam Djinn'),
    (select id from sets where short_name = 'rin'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ironclaw Orcs'),
    (select id from sets where short_name = 'rin'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Artifacts'),
    (select id from sets where short_name = 'rin'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shapeshifter'),
    (select id from sets where short_name = 'rin'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Spears'),
    (select id from sets where short_name = 'rin'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Altar'),
    (select id from sets where short_name = 'rin'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Junún Efreet'),
    (select id from sets where short_name = 'rin'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = 'rin'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aladdin'),
    (select id from sets where short_name = 'rin'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fishliver Oil'),
    (select id from sets where short_name = 'rin'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Undoing'),
    (select id from sets where short_name = 'rin'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bird Maiden'),
    (select id from sets where short_name = 'rin'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Detonate'),
    (select id from sets where short_name = 'rin'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tetravus'),
    (select id from sets where short_name = 'rin'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Twiddle'),
    (select id from sets where short_name = 'rin'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Colossus of Sardia'),
    (select id from sets where short_name = 'rin'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cursed Rack'),
    (select id from sets where short_name = 'rin'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yotian Soldier'),
    (select id from sets where short_name = 'rin'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = 'rin'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghazbán Ogre'),
    (select id from sets where short_name = 'rin'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = 'rin'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hurr Jackal'),
    (select id from sets where short_name = 'rin'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = 'rin'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = 'rin'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clockwork Avian'),
    (select id from sets where short_name = 'rin'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jalum Tome'),
    (select id from sets where short_name = 'rin'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'rin'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yawgmoth Demon'),
    (select id from sets where short_name = 'rin'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = 'rin'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battering Ram'),
    (select id from sets where short_name = 'rin'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Artisans'),
    (select id from sets where short_name = 'rin'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = 'rin'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Transmogrant'),
    (select id from sets where short_name = 'rin'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hasran Ogress'),
    (select id from sets where short_name = 'rin'),
    '53',
    'common'
) 
 on conflict do nothing;
