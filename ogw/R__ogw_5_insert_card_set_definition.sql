insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'World Breaker'),
    (select id from sets where short_name = 'ogw'),
    '126',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'ogw'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prophet of Distortion'),
    (select id from sets where short_name = 'ogw'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reckless Bushwhacker'),
    (select id from sets where short_name = 'ogw'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tar Snare'),
    (select id from sets where short_name = 'ogw'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jwar Isle Avenger'),
    (select id from sets where short_name = 'ogw'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harvester Troll'),
    (select id from sets where short_name = 'ogw'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Witness the End'),
    (select id from sets where short_name = 'ogw'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Umara Entangler'),
    (select id from sets where short_name = 'ogw'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Munda''s Vanguard'),
    (select id from sets where short_name = 'ogw'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Affa Protector'),
    (select id from sets where short_name = 'ogw'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dimensional Infiltrator'),
    (select id from sets where short_name = 'ogw'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stalking Drone'),
    (select id from sets where short_name = 'ogw'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chitinous Cloak'),
    (select id from sets where short_name = 'ogw'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reaver Drone'),
    (select id from sets where short_name = 'ogw'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kalitas, Traitor of Ghet'),
    (select id from sets where short_name = 'ogw'),
    '86',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Drana''s Chosen'),
    (select id from sets where short_name = 'ogw'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Isolation Zone'),
    (select id from sets where short_name = 'ogw'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Remorseless Punishment'),
    (select id from sets where short_name = 'ogw'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abstruse Interference'),
    (select id from sets where short_name = 'ogw'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oblivion Strike'),
    (select id from sets where short_name = 'ogw'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slaughter Drone'),
    (select id from sets where short_name = 'ogw'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gladehart Cavalry'),
    (select id from sets where short_name = 'ogw'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lead by Example'),
    (select id from sets where short_name = 'ogw'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slip Through Space'),
    (select id from sets where short_name = 'ogw'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relief Captain'),
    (select id from sets where short_name = 'ogw'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Immolating Glare'),
    (select id from sets where short_name = 'ogw'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Make a Stand'),
    (select id from sets where short_name = 'ogw'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Mimic'),
    (select id from sets where short_name = 'ogw'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saddleback Lagac'),
    (select id from sets where short_name = 'ogw'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overwhelming Denial'),
    (select id from sets where short_name = 'ogw'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'General Tazri'),
    (select id from sets where short_name = 'ogw'),
    '19',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Walker of the Wastes'),
    (select id from sets where short_name = 'ogw'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gift of Tusks'),
    (select id from sets where short_name = 'ogw'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Matter Reshaper'),
    (select id from sets where short_name = 'ogw'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flayer Drone'),
    (select id from sets where short_name = 'ogw'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Captain''s Claws'),
    (select id from sets where short_name = 'ogw'),
    '162†',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampire Envoy'),
    (select id from sets where short_name = 'ogw'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Needle Spires'),
    (select id from sets where short_name = 'ogw'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roiling Waters'),
    (select id from sets where short_name = 'ogw'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Baloth Pup'),
    (select id from sets where short_name = 'ogw'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquil Expanse'),
    (select id from sets where short_name = 'ogw'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Devour in Flames'),
    (select id from sets where short_name = 'ogw'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Netcaster Spider'),
    (select id from sets where short_name = 'ogw'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thought Harvester'),
    (select id from sets where short_name = 'ogw'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Call the Gatewatch'),
    (select id from sets where short_name = 'ogw'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strider Harness'),
    (select id from sets where short_name = 'ogw'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maw of Kozilek'),
    (select id from sets where short_name = 'ogw'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deepfathom Skulker'),
    (select id from sets where short_name = 'ogw'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shoulder to Shoulder'),
    (select id from sets where short_name = 'ogw'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Containment Membrane'),
    (select id from sets where short_name = 'ogw'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra, Flamecaller'),
    (select id from sets where short_name = 'ogw'),
    '104',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mindmelter'),
    (select id from sets where short_name = 'ogw'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Advocate'),
    (select id from sets where short_name = 'ogw'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Null Caller'),
    (select id from sets where short_name = 'ogw'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Void Shatter'),
    (select id from sets where short_name = 'ogw'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mighty Leap'),
    (select id from sets where short_name = 'ogw'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boulder Salvo'),
    (select id from sets where short_name = 'ogw'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spatial Contortion'),
    (select id from sets where short_name = 'ogw'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blinding Drone'),
    (select id from sets where short_name = 'ogw'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bearer of Silence'),
    (select id from sets where short_name = 'ogw'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Freerunner'),
    (select id from sets where short_name = 'ogw'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Captain''s Claws'),
    (select id from sets where short_name = 'ogw'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reality Smasher'),
    (select id from sets where short_name = 'ogw'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steppe Glider'),
    (select id from sets where short_name = 'ogw'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kor Sky Climber'),
    (select id from sets where short_name = 'ogw'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cliffhaven Vampire'),
    (select id from sets where short_name = 'ogw'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Timber Gorge'),
    (select id from sets where short_name = 'ogw'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spawnbinder Mage'),
    (select id from sets where short_name = 'ogw'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Untamed Hunger'),
    (select id from sets where short_name = 'ogw'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Expedite'),
    (select id from sets where short_name = 'ogw'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cinder Hellion'),
    (select id from sets where short_name = 'ogw'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruin in Their Wake'),
    (select id from sets where short_name = 'ogw'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kor Scythemaster'),
    (select id from sets where short_name = 'ogw'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wastes'),
    (select id from sets where short_name = 'ogw'),
    '184a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oath of Nissa'),
    (select id from sets where short_name = 'ogw'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Aggressor'),
    (select id from sets where short_name = 'ogw'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Submerged Boneyard'),
    (select id from sets where short_name = 'ogw'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Joraga Auxiliary'),
    (select id from sets where short_name = 'ogw'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seer''s Lantern'),
    (select id from sets where short_name = 'ogw'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zendikar Resurgent'),
    (select id from sets where short_name = 'ogw'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kazuul''s Toll Collector'),
    (select id from sets where short_name = 'ogw'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wastes'),
    (select id from sets where short_name = 'ogw'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Press into Service'),
    (select id from sets where short_name = 'ogw'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meandering River'),
    (select id from sets where short_name = 'ogw'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Corpse Churn'),
    (select id from sets where short_name = 'ogw'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Endbringer'),
    (select id from sets where short_name = 'ogw'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Void Grafter'),
    (select id from sets where short_name = 'ogw'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grip of the Roil'),
    (select id from sets where short_name = 'ogw'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hissing Quagmire'),
    (select id from sets where short_name = 'ogw'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akoum Flameseeker'),
    (select id from sets where short_name = 'ogw'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unity of Purpose'),
    (select id from sets where short_name = 'ogw'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wastes'),
    (select id from sets where short_name = 'ogw'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corrupted Crossroads'),
    (select id from sets where short_name = 'ogw'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kozilek''s Return'),
    (select id from sets where short_name = 'ogw'),
    '98',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Malakir Soothsayer'),
    (select id from sets where short_name = 'ogw'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sparkmage''s Gambit'),
    (select id from sets where short_name = 'ogw'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sweep Away'),
    (select id from sets where short_name = 'ogw'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Searing Light'),
    (select id from sets where short_name = 'ogw'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flaying Tendrils'),
    (select id from sets where short_name = 'ogw'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Crab'),
    (select id from sets where short_name = 'ogw'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hedron Alignment'),
    (select id from sets where short_name = 'ogw'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stormchaser Mage'),
    (select id from sets where short_name = 'ogw'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weapons Trainer'),
    (select id from sets where short_name = 'ogw'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grasp of Darkness'),
    (select id from sets where short_name = 'ogw'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Expedition Raptor'),
    (select id from sets where short_name = 'ogw'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scion Summoner'),
    (select id from sets where short_name = 'ogw'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Consuming Sinkhole'),
    (select id from sets where short_name = 'ogw'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravity Negator'),
    (select id from sets where short_name = 'ogw'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx of the Final Word'),
    (select id from sets where short_name = 'ogw'),
    '63',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Unknown Shores'),
    (select id from sets where short_name = 'ogw'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baloth Null'),
    (select id from sets where short_name = 'ogw'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inverter of Truth'),
    (select id from sets where short_name = 'ogw'),
    '72',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fall of the Titans'),
    (select id from sets where short_name = 'ogw'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jori En, Ruin Diver'),
    (select id from sets where short_name = 'ogw'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relentless Hunter'),
    (select id from sets where short_name = 'ogw'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zulaport Chainmage'),
    (select id from sets where short_name = 'ogw'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Embodiment of Fury'),
    (select id from sets where short_name = 'ogw'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reflector Mage'),
    (select id from sets where short_name = 'ogw'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cinder Barrens'),
    (select id from sets where short_name = 'ogw'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Immobilizer Eldrazi'),
    (select id from sets where short_name = 'ogw'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nissa''s Judgment'),
    (select id from sets where short_name = 'ogw'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oath of Gideon'),
    (select id from sets where short_name = 'ogw'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Essence Depleter'),
    (select id from sets where short_name = 'ogw'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ondu War Cleric'),
    (select id from sets where short_name = 'ogw'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wastes'),
    (select id from sets where short_name = 'ogw'),
    '183a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hedron Crawler'),
    (select id from sets where short_name = 'ogw'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruins of Oran-Rief'),
    (select id from sets where short_name = 'ogw'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Birthing Hulk'),
    (select id from sets where short_name = 'ogw'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Iona''s Blessing'),
    (select id from sets where short_name = 'ogw'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Makindi Aeronaut'),
    (select id from sets where short_name = 'ogw'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kozilek''s Shrieker'),
    (select id from sets where short_name = 'ogw'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deceiver of Form'),
    (select id from sets where short_name = 'ogw'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stoneforge Acolyte'),
    (select id from sets where short_name = 'ogw'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nissa, Voice of Zendikar'),
    (select id from sets where short_name = 'ogw'),
    '138',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Unnatural Endurance'),
    (select id from sets where short_name = 'ogw'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stoneforge Masterwork'),
    (select id from sets where short_name = 'ogw'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loam Larva'),
    (select id from sets where short_name = 'ogw'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Embodiment of Insight'),
    (select id from sets where short_name = 'ogw'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tajuru Pathwarden'),
    (select id from sets where short_name = 'ogw'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyromancer''s Assault'),
    (select id from sets where short_name = 'ogw'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kozilek''s Pathfinder'),
    (select id from sets where short_name = 'ogw'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seed Guardian'),
    (select id from sets where short_name = 'ogw'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cultivator Drone'),
    (select id from sets where short_name = 'ogw'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crush of Tentacles'),
    (select id from sets where short_name = 'ogw'),
    '53',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sifter of Skulls'),
    (select id from sets where short_name = 'ogw'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cyclone Sire'),
    (select id from sets where short_name = 'ogw'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thought-Knot Seer'),
    (select id from sets where short_name = 'ogw'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reality Hemorrhage'),
    (select id from sets where short_name = 'ogw'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pulse of Murasa'),
    (select id from sets where short_name = 'ogw'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warden of Geometries'),
    (select id from sets where short_name = 'ogw'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Resurgence'),
    (select id from sets where short_name = 'ogw'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Canopy Gorger'),
    (select id from sets where short_name = 'ogw'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Wreckage'),
    (select id from sets where short_name = 'ogw'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Holdout Settlement'),
    (select id from sets where short_name = 'ogw'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mina and Denn, Wildborn'),
    (select id from sets where short_name = 'ogw'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crumbling Vestige'),
    (select id from sets where short_name = 'ogw'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kozilek''s Translator'),
    (select id from sets where short_name = 'ogw'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tears of Valakut'),
    (select id from sets where short_name = 'ogw'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bone Saw'),
    (select id from sets where short_name = 'ogw'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wandering Fumarole'),
    (select id from sets where short_name = 'ogw'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Comparative Analysis'),
    (select id from sets where short_name = 'ogw'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Dark-Dwellers'),
    (select id from sets where short_name = 'ogw'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Obligator'),
    (select id from sets where short_name = 'ogw'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vines of the Recluse'),
    (select id from sets where short_name = 'ogw'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental Uprising'),
    (select id from sets where short_name = 'ogw'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Haven Outfitter'),
    (select id from sets where short_name = 'ogw'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dread Defiler'),
    (select id from sets where short_name = 'ogw'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ayli, Eternal Pilgrim'),
    (select id from sets where short_name = 'ogw'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oath of Jace'),
    (select id from sets where short_name = 'ogw'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sky Scourer'),
    (select id from sets where short_name = 'ogw'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brute Strength'),
    (select id from sets where short_name = 'ogw'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Visions of Brutality'),
    (select id from sets where short_name = 'ogw'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mirrorpool'),
    (select id from sets where short_name = 'ogw'),
    '174',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Linvala, the Preserver'),
    (select id from sets where short_name = 'ogw'),
    '25',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kozilek, the Great Distortion'),
    (select id from sets where short_name = 'ogw'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Displacer'),
    (select id from sets where short_name = 'ogw'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warping Wail'),
    (select id from sets where short_name = 'ogw'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tyrant of Valakut'),
    (select id from sets where short_name = 'ogw'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vile Redeemer'),
    (select id from sets where short_name = 'ogw'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zada''s Commando'),
    (select id from sets where short_name = 'ogw'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oath of Chandra'),
    (select id from sets where short_name = 'ogw'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dazzling Reflection'),
    (select id from sets where short_name = 'ogw'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Havoc Sower'),
    (select id from sets where short_name = 'ogw'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Allied Reinforcements'),
    (select id from sets where short_name = 'ogw'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Natural State'),
    (select id from sets where short_name = 'ogw'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bonds of Mortality'),
    (select id from sets where short_name = 'ogw'),
    '128',
    'uncommon'
) 
 on conflict do nothing;
