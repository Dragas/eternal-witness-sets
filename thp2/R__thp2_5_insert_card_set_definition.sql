insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'The Vanquisher'),
    (select id from sets where short_name = 'thp2'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Explorer'),
    (select id from sets where short_name = 'thp2'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Savant'),
    (select id from sets where short_name = 'thp2'),
    '4b',
    'common'
) ,
(
    (select id from mtgcard where name = 'The General'),
    (select id from sets where short_name = 'thp2'),
    '4a',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Tyrant'),
    (select id from sets where short_name = 'thp2'),
    '4c',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Provider'),
    (select id from sets where short_name = 'thp2'),
    '4e',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Warmonger'),
    (select id from sets where short_name = 'thp2'),
    '4d',
    'common'
) 
 on conflict do nothing;
