insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'The Vanquisher'),
        (select types.id from types where name = 'Hero')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Explorer'),
        (select types.id from types where name = 'Hero')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Savant'),
        (select types.id from types where name = 'Hero')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The General'),
        (select types.id from types where name = 'Hero')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Tyrant'),
        (select types.id from types where name = 'Hero')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Provider'),
        (select types.id from types where name = 'Hero')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Warmonger'),
        (select types.id from types where name = 'Hero')
    ) 
 on conflict do nothing;
