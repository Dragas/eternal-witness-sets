    insert into mtgcard(name) values ('The Vanquisher') on conflict do nothing;
    insert into mtgcard(name) values ('The Explorer') on conflict do nothing;
    insert into mtgcard(name) values ('The Savant') on conflict do nothing;
    insert into mtgcard(name) values ('The General') on conflict do nothing;
    insert into mtgcard(name) values ('The Tyrant') on conflict do nothing;
    insert into mtgcard(name) values ('The Provider') on conflict do nothing;
    insert into mtgcard(name) values ('The Warmonger') on conflict do nothing;
