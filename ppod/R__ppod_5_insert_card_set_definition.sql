insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Bull Hippo'),
    (select id from sets where short_name = 'ppod'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feral Shadow'),
    (select id from sets where short_name = 'ppod'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armored Pegasus'),
    (select id from sets where short_name = 'ppod'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloud Pirates'),
    (select id from sets where short_name = 'ppod'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snapping Drake'),
    (select id from sets where short_name = 'ppod'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm Crow'),
    (select id from sets where short_name = 'ppod'),
    '6',
    'rare'
) 
 on conflict do nothing;
