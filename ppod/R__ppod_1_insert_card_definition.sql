    insert into mtgcard(name) values ('Bull Hippo') on conflict do nothing;
    insert into mtgcard(name) values ('Feral Shadow') on conflict do nothing;
    insert into mtgcard(name) values ('Armored Pegasus') on conflict do nothing;
    insert into mtgcard(name) values ('Cloud Pirates') on conflict do nothing;
    insert into mtgcard(name) values ('Snapping Drake') on conflict do nothing;
    insert into mtgcard(name) values ('Storm Crow') on conflict do nothing;
