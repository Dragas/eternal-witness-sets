insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Myr'),
    (select id from sets where short_name = 'p04'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Powder Keg'),
    (select id from sets where short_name = 'p04'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'p04'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pentavite'),
    (select id from sets where short_name = 'p04'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'p04'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'p04'),
    '5',
    'common'
) 
 on conflict do nothing;
