insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tddt'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall'),
    (select id from sets where short_name = 'tddt'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tddt'),
    '1',
    'common'
) 
 on conflict do nothing;
