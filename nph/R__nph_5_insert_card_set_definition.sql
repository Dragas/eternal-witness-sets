insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Fresh Meat'),
    (select id from sets where short_name = 'nph'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loxodon Convert'),
    (select id from sets where short_name = 'nph'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shattered Angel'),
    (select id from sets where short_name = 'nph'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Artillerize'),
    (select id from sets where short_name = 'nph'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corrosive Gale'),
    (select id from sets where short_name = 'nph'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dismember'),
    (select id from sets where short_name = 'nph'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cathedral Membrane'),
    (select id from sets where short_name = 'nph'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vorinclex, Voice of Hunger'),
    (select id from sets where short_name = 'nph'),
    '127',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mycosynth Wellspring'),
    (select id from sets where short_name = 'nph'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vulshok Refugee'),
    (select id from sets where short_name = 'nph'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ichor Explosion'),
    (select id from sets where short_name = 'nph'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darksteel Relic'),
    (select id from sets where short_name = 'nph'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Omen Machine'),
    (select id from sets where short_name = 'nph'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Geosurge'),
    (select id from sets where short_name = 'nph'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Surge Node'),
    (select id from sets where short_name = 'nph'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moltensteel Dragon'),
    (select id from sets where short_name = 'nph'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conversion Chamber'),
    (select id from sets where short_name = 'nph'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Hulk'),
    (select id from sets where short_name = 'nph'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insatiable Souleater'),
    (select id from sets where short_name = 'nph'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enslave'),
    (select id from sets where short_name = 'nph'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spire Monitor'),
    (select id from sets where short_name = 'nph'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spellskite'),
    (select id from sets where short_name = 'nph'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noxious Revival'),
    (select id from sets where short_name = 'nph'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leeching Bite'),
    (select id from sets where short_name = 'nph'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychic Barrier'),
    (select id from sets where short_name = 'nph'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deceiver Exarch'),
    (select id from sets where short_name = 'nph'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blinding Souleater'),
    (select id from sets where short_name = 'nph'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Porcelain Legionnaire'),
    (select id from sets where short_name = 'nph'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'nph'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vital Splicer'),
    (select id from sets where short_name = 'nph'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Corrupted Resolve'),
    (select id from sets where short_name = 'nph'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pith Driller'),
    (select id from sets where short_name = 'nph'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Superion'),
    (select id from sets where short_name = 'nph'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maul Splicer'),
    (select id from sets where short_name = 'nph'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slash Panther'),
    (select id from sets where short_name = 'nph'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Razor Swine'),
    (select id from sets where short_name = 'nph'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Suture Priest'),
    (select id from sets where short_name = 'nph'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greenhilt Trainee'),
    (select id from sets where short_name = 'nph'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Ingester'),
    (select id from sets where short_name = 'nph'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'nph'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scrapyard Salvo'),
    (select id from sets where short_name = 'nph'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chained Throatseeker'),
    (select id from sets where short_name = 'nph'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'nph'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sheoldred, Whispering One'),
    (select id from sets where short_name = 'nph'),
    '73',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mortis Dogs'),
    (select id from sets where short_name = 'nph'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hex Parasite'),
    (select id from sets where short_name = 'nph'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Obliterator'),
    (select id from sets where short_name = 'nph'),
    '68',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Glistening Oil'),
    (select id from sets where short_name = 'nph'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tormentor Exarch'),
    (select id from sets where short_name = 'nph'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unwinding Clock'),
    (select id from sets where short_name = 'nph'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dispatch'),
    (select id from sets where short_name = 'nph'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inquisitor Exarch'),
    (select id from sets where short_name = 'nph'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evil Presence'),
    (select id from sets where short_name = 'nph'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'nph'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necropouncer'),
    (select id from sets where short_name = 'nph'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Caged Sun'),
    (select id from sets where short_name = 'nph'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Argent Mutation'),
    (select id from sets where short_name = 'nph'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Psychic Surgery'),
    (select id from sets where short_name = 'nph'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death-Hood Cobra'),
    (select id from sets where short_name = 'nph'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marrow Shards'),
    (select id from sets where short_name = 'nph'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Toxic Nim'),
    (select id from sets where short_name = 'nph'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whipflare'),
    (select id from sets where short_name = 'nph'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Caress of Phyrexia'),
    (select id from sets where short_name = 'nph'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shrine of Limitless Power'),
    (select id from sets where short_name = 'nph'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'nph'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Act of Aggression'),
    (select id from sets where short_name = 'nph'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mental Misstep'),
    (select id from sets where short_name = 'nph'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ogre Menial'),
    (select id from sets where short_name = 'nph'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viridian Harvest'),
    (select id from sets where short_name = 'nph'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chancellor of the Tangle'),
    (select id from sets where short_name = 'nph'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'War Report'),
    (select id from sets where short_name = 'nph'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Remember the Fallen'),
    (select id from sets where short_name = 'nph'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mindculling'),
    (select id from sets where short_name = 'nph'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vapor Snag'),
    (select id from sets where short_name = 'nph'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dementia Bat'),
    (select id from sets where short_name = 'nph'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shriek Raptor'),
    (select id from sets where short_name = 'nph'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sensor Splicer'),
    (select id from sets where short_name = 'nph'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Geth''s Verdict'),
    (select id from sets where short_name = 'nph'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fallen Ferromancer'),
    (select id from sets where short_name = 'nph'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Victorious Destruction'),
    (select id from sets where short_name = 'nph'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rotted Hystrix'),
    (select id from sets where short_name = 'nph'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Entomber Exarch'),
    (select id from sets where short_name = 'nph'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lost Leonin'),
    (select id from sets where short_name = 'nph'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Master Splicer'),
    (select id from sets where short_name = 'nph'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Invader Parasite'),
    (select id from sets where short_name = 'nph'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Swarmlord'),
    (select id from sets where short_name = 'nph'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glistener Elf'),
    (select id from sets where short_name = 'nph'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lashwrithe'),
    (select id from sets where short_name = 'nph'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Numbing Dose'),
    (select id from sets where short_name = 'nph'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'nph'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kiln Walker'),
    (select id from sets where short_name = 'nph'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Impaler Shrike'),
    (select id from sets where short_name = 'nph'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Torpor Orb'),
    (select id from sets where short_name = 'nph'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Praetor''s Grasp'),
    (select id from sets where short_name = 'nph'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hovermyr'),
    (select id from sets where short_name = 'nph'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flameborn Viron'),
    (select id from sets where short_name = 'nph'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Furnace Scamp'),
    (select id from sets where short_name = 'nph'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gitaxian Probe'),
    (select id from sets where short_name = 'nph'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'nph'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shrine of Piercing Vision'),
    (select id from sets where short_name = 'nph'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blind Zealot'),
    (select id from sets where short_name = 'nph'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grim Affliction'),
    (select id from sets where short_name = 'nph'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brutalizer Exarch'),
    (select id from sets where short_name = 'nph'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Exclusion Ritual'),
    (select id from sets where short_name = 'nph'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Apostle''s Blessing'),
    (select id from sets where short_name = 'nph'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pestilent Souleater'),
    (select id from sets where short_name = 'nph'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arm with Aether'),
    (select id from sets where short_name = 'nph'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reaper of Sheoldred'),
    (select id from sets where short_name = 'nph'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bludgeon Brawl'),
    (select id from sets where short_name = 'nph'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gut Shot'),
    (select id from sets where short_name = 'nph'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Norn''s Annex'),
    (select id from sets where short_name = 'nph'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Due Respect'),
    (select id from sets where short_name = 'nph'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Conduit'),
    (select id from sets where short_name = 'nph'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volt Charge'),
    (select id from sets where short_name = 'nph'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Immolating Souleater'),
    (select id from sets where short_name = 'nph'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alloy Myr'),
    (select id from sets where short_name = 'nph'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mutagenic Growth'),
    (select id from sets where short_name = 'nph'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urabrask the Hidden'),
    (select id from sets where short_name = 'nph'),
    '98',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chancellor of the Spires'),
    (select id from sets where short_name = 'nph'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shrine of Boundless Growth'),
    (select id from sets where short_name = 'nph'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pristine Talisman'),
    (select id from sets where short_name = 'nph'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Unlife'),
    (select id from sets where short_name = 'nph'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trespassing Souleater'),
    (select id from sets where short_name = 'nph'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elesh Norn, Grand Cenobite'),
    (select id from sets where short_name = 'nph'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Birthing Pod'),
    (select id from sets where short_name = 'nph'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blade Splicer'),
    (select id from sets where short_name = 'nph'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruthless Invasion'),
    (select id from sets where short_name = 'nph'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whispering Specter'),
    (select id from sets where short_name = 'nph'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spined Thopter'),
    (select id from sets where short_name = 'nph'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jin-Gitaxias, Core Augur'),
    (select id from sets where short_name = 'nph'),
    '37',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mycosynth Fiend'),
    (select id from sets where short_name = 'nph'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chancellor of the Annex'),
    (select id from sets where short_name = 'nph'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jor Kadeen, the Prevailer'),
    (select id from sets where short_name = 'nph'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slag Fiend'),
    (select id from sets where short_name = 'nph'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Triumph of the Hordes'),
    (select id from sets where short_name = 'nph'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'nph'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Isolation Cell'),
    (select id from sets where short_name = 'nph'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Puresteel Paladin'),
    (select id from sets where short_name = 'nph'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viridian Betrayers'),
    (select id from sets where short_name = 'nph'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gremlin Mine'),
    (select id from sets where short_name = 'nph'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Batterskull'),
    (select id from sets where short_name = 'nph'),
    '130',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blighted Agent'),
    (select id from sets where short_name = 'nph'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast Within'),
    (select id from sets where short_name = 'nph'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spinebiter'),
    (select id from sets where short_name = 'nph'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Defensive Stance'),
    (select id from sets where short_name = 'nph'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Priest of Urabrask'),
    (select id from sets where short_name = 'nph'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shrine of Loyal Legions'),
    (select id from sets where short_name = 'nph'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mindcrank'),
    (select id from sets where short_name = 'nph'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shrine of Burning Rage'),
    (select id from sets where short_name = 'nph'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Metamorph'),
    (select id from sets where short_name = 'nph'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sickleslicer'),
    (select id from sets where short_name = 'nph'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'nph'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Postmortem Lunge'),
    (select id from sets where short_name = 'nph'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viral Drake'),
    (select id from sets where short_name = 'nph'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rage Extractor'),
    (select id from sets where short_name = 'nph'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'nph'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexia''s Core'),
    (select id from sets where short_name = 'nph'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Surgical Extraction'),
    (select id from sets where short_name = 'nph'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chancellor of the Forge'),
    (select id from sets where short_name = 'nph'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Auriok Survivors'),
    (select id from sets where short_name = 'nph'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forced Worship'),
    (select id from sets where short_name = 'nph'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wing Splicer'),
    (select id from sets where short_name = 'nph'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thundering Tanadon'),
    (select id from sets where short_name = 'nph'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Parasitic Implant'),
    (select id from sets where short_name = 'nph'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Melira, Sylvok Outcast'),
    (select id from sets where short_name = 'nph'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Despise'),
    (select id from sets where short_name = 'nph'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chancellor of the Dross'),
    (select id from sets where short_name = 'nph'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glissa''s Scorn'),
    (select id from sets where short_name = 'nph'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vault Skirge'),
    (select id from sets where short_name = 'nph'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Xenograft'),
    (select id from sets where short_name = 'nph'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karn Liberated'),
    (select id from sets where short_name = 'nph'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sword of War and Peace'),
    (select id from sets where short_name = 'nph'),
    '161',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Etched Monstrosity'),
    (select id from sets where short_name = 'nph'),
    '135',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Life''s Finale'),
    (select id from sets where short_name = 'nph'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tezzeret''s Gambit'),
    (select id from sets where short_name = 'nph'),
    '47',
    'uncommon'
) 
 on conflict do nothing;
