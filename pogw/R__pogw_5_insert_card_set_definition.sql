insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Immolating Glare'),
    (select id from sets where short_name = 'pogw'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Remorseless Punishment'),
    (select id from sets where short_name = 'pogw'),
    '89s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overwhelming Denial'),
    (select id from sets where short_name = 'pogw'),
    '61s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Endbringer'),
    (select id from sets where short_name = 'pogw'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruins of Oran-Rief'),
    (select id from sets where short_name = 'pogw'),
    '176s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gladehart Cavalry'),
    (select id from sets where short_name = 'pogw'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone Haven Outfitter'),
    (select id from sets where short_name = 'pogw'),
    '37s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Endbringer'),
    (select id from sets where short_name = 'pogw'),
    '3s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Captain''s Claws'),
    (select id from sets where short_name = 'pogw'),
    '162s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sifter of Skulls'),
    (select id from sets where short_name = 'pogw'),
    '77s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dread Defiler'),
    (select id from sets where short_name = 'pogw'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oath of Chandra'),
    (select id from sets where short_name = 'pogw'),
    '113s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Displacer'),
    (select id from sets where short_name = 'pogw'),
    '13s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hedron Alignment'),
    (select id from sets where short_name = 'pogw'),
    '57s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Wreckage'),
    (select id from sets where short_name = 'pogw'),
    '177s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mina and Denn, Wildborn'),
    (select id from sets where short_name = 'pogw'),
    '156s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jori En, Ruin Diver'),
    (select id from sets where short_name = 'pogw'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deceiver of Form'),
    (select id from sets where short_name = 'pogw'),
    '1s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inverter of Truth'),
    (select id from sets where short_name = 'pogw'),
    '72s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Matter Reshaper'),
    (select id from sets where short_name = 'pogw'),
    '6s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dread Defiler'),
    (select id from sets where short_name = 'pogw'),
    '68s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crush of Tentacles'),
    (select id from sets where short_name = 'pogw'),
    '53s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Linvala, the Preserver'),
    (select id from sets where short_name = 'pogw'),
    '25s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Drana''s Chosen'),
    (select id from sets where short_name = 'pogw'),
    '84s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deepfathom Skulker'),
    (select id from sets where short_name = 'pogw'),
    '43s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kozilek''s Return'),
    (select id from sets where short_name = 'pogw'),
    '98s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dimensional Infiltrator'),
    (select id from sets where short_name = 'pogw'),
    '44s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jori En, Ruin Diver'),
    (select id from sets where short_name = 'pogw'),
    '155s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kozilek, the Great Distortion'),
    (select id from sets where short_name = 'pogw'),
    '4s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tyrant of Valakut'),
    (select id from sets where short_name = 'pogw'),
    '119s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bearer of Silence'),
    (select id from sets where short_name = 'pogw'),
    '67s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fall of the Titans'),
    (select id from sets where short_name = 'pogw'),
    '109s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa, Voice of Zendikar'),
    (select id from sets where short_name = 'pogw'),
    '138p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Call the Gatewatch'),
    (select id from sets where short_name = 'pogw'),
    '16s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ayli, Eternal Pilgrim'),
    (select id from sets where short_name = 'pogw'),
    '151s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stoneforge Masterwork'),
    (select id from sets where short_name = 'pogw'),
    '166s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Needle Spires'),
    (select id from sets where short_name = 'pogw'),
    '175s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zendikar Resurgent'),
    (select id from sets where short_name = 'pogw'),
    '147s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zendikar Resurgent'),
    (select id from sets where short_name = 'pogw'),
    '147p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vile Redeemer'),
    (select id from sets where short_name = 'pogw'),
    '125s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deepfathom Skulker'),
    (select id from sets where short_name = 'pogw'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'General Tazri'),
    (select id from sets where short_name = 'pogw'),
    '19s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chandra, Flamecaller'),
    (select id from sets where short_name = 'pogw'),
    '104s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hissing Quagmire'),
    (select id from sets where short_name = 'pogw'),
    '171s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oath of Jace'),
    (select id from sets where short_name = 'pogw'),
    '60s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'World Breaker'),
    (select id from sets where short_name = 'pogw'),
    '126s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goblin Dark-Dwellers'),
    (select id from sets where short_name = 'pogw'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tyrant of Valakut'),
    (select id from sets where short_name = 'pogw'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reality Smasher'),
    (select id from sets where short_name = 'pogw'),
    '7s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gladehart Cavalry'),
    (select id from sets where short_name = 'pogw'),
    '132s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wandering Fumarole'),
    (select id from sets where short_name = 'pogw'),
    '182s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Mimic'),
    (select id from sets where short_name = 'pogw'),
    '2s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thought-Knot Seer'),
    (select id from sets where short_name = 'pogw'),
    '9s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Obligator'),
    (select id from sets where short_name = 'pogw'),
    '96s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa, Voice of Zendikar'),
    (select id from sets where short_name = 'pogw'),
    '138s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Oath of Gideon'),
    (select id from sets where short_name = 'pogw'),
    '30s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Munda''s Vanguard'),
    (select id from sets where short_name = 'pogw'),
    '29s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Dark-Dwellers'),
    (select id from sets where short_name = 'pogw'),
    '110s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirrorpool'),
    (select id from sets where short_name = 'pogw'),
    '174s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Corrupted Crossroads'),
    (select id from sets where short_name = 'pogw'),
    '169s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kalitas, Traitor of Ghet'),
    (select id from sets where short_name = 'pogw'),
    '86s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Munda''s Vanguard'),
    (select id from sets where short_name = 'pogw'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Advocate'),
    (select id from sets where short_name = 'pogw'),
    '144s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphinx of the Final Word'),
    (select id from sets where short_name = 'pogw'),
    '63s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Oath of Nissa'),
    (select id from sets where short_name = 'pogw'),
    '140s',
    'rare'
) 
 on conflict do nothing;
