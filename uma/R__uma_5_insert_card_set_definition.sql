insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Martyr of Sands'),
    (select id from sets where short_name = 'uma'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dig Through Time'),
    (select id from sets where short_name = 'uma'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urborg, Tomb of Yawgmoth'),
    (select id from sets where short_name = 'uma'),
    '254',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Artisan of Kozilek'),
    (select id from sets where short_name = 'uma'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reanimate'),
    (select id from sets where short_name = 'uma'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Back to Basics'),
    (select id from sets where short_name = 'uma'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Undying Rage'),
    (select id from sets where short_name = 'uma'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shirei, Shizo''s Caretaker'),
    (select id from sets where short_name = 'uma'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Repel the Darkness'),
    (select id from sets where short_name = 'uma'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ronom Unicorn'),
    (select id from sets where short_name = 'uma'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Verdant Eidolon'),
    (select id from sets where short_name = 'uma'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Through the Breach'),
    (select id from sets where short_name = 'uma'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magmaw'),
    (select id from sets where short_name = 'uma'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firewing Phoenix'),
    (select id from sets where short_name = 'uma'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deranged Assistant'),
    (select id from sets where short_name = 'uma'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scuzzback Marauders'),
    (select id from sets where short_name = 'uma'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emrakul, the Aeons Torn'),
    (select id from sets where short_name = 'uma'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Prey Upon'),
    (select id from sets where short_name = 'uma'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seize the Day'),
    (select id from sets where short_name = 'uma'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kodama''s Reach'),
    (select id from sets where short_name = 'uma'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thespian''s Stage'),
    (select id from sets where short_name = 'uma'),
    '253',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul''s Fire'),
    (select id from sets where short_name = 'uma'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Creeping Tar Pit'),
    (select id from sets where short_name = 'uma'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Olivia''s Dragoon'),
    (select id from sets where short_name = 'uma'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mark of the Vampire'),
    (select id from sets where short_name = 'uma'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Reverence'),
    (select id from sets where short_name = 'uma'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rolling Temblor'),
    (select id from sets where short_name = 'uma'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Turn to Mist'),
    (select id from sets where short_name = 'uma'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sleight of Hand'),
    (select id from sets where short_name = 'uma'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Emancipation Angel'),
    (select id from sets where short_name = 'uma'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Apprentice Necromancer'),
    (select id from sets where short_name = 'uma'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodflow Connoisseur'),
    (select id from sets where short_name = 'uma'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Patchwork Gnomes'),
    (select id from sets where short_name = 'uma'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faith''s Fetters'),
    (select id from sets where short_name = 'uma'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fulminator Mage'),
    (select id from sets where short_name = 'uma'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stirring Wildwood'),
    (select id from sets where short_name = 'uma'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shielding Plax'),
    (select id from sets where short_name = 'uma'),
    '222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cavern of Souls'),
    (select id from sets where short_name = 'uma'),
    '237',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fiery Temper'),
    (select id from sets where short_name = 'uma'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basking Rootwalla'),
    (select id from sets where short_name = 'uma'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Last Gasp'),
    (select id from sets where short_name = 'uma'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mage-Ring Network'),
    (select id from sets where short_name = 'uma'),
    '246',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moan of the Unhallowed'),
    (select id from sets where short_name = 'uma'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Miraculous Recovery'),
    (select id from sets where short_name = 'uma'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desperate Ritual'),
    (select id from sets where short_name = 'uma'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beckon Apparition'),
    (select id from sets where short_name = 'uma'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakdos Shred-Freak'),
    (select id from sets where short_name = 'uma'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rally the Peasants'),
    (select id from sets where short_name = 'uma'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demonic Tutor'),
    (select id from sets where short_name = 'uma'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Altar'),
    (select id from sets where short_name = 'uma'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eel Umbra'),
    (select id from sets where short_name = 'uma'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanitarium Skeleton'),
    (select id from sets where short_name = 'uma'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire // Ice'),
    (select id from sets where short_name = 'uma'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bridge from Below'),
    (select id from sets where short_name = 'uma'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treasure Cruise'),
    (select id from sets where short_name = 'uma'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lavaclaw Reaches'),
    (select id from sets where short_name = 'uma'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rune Snag'),
    (select id from sets where short_name = 'uma'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tarmogoyf'),
    (select id from sets where short_name = 'uma'),
    '187',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Reya Dawnbringer'),
    (select id from sets where short_name = 'uma'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shed Weakness'),
    (select id from sets where short_name = 'uma'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balefire Dragon'),
    (select id from sets where short_name = 'uma'),
    '124',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Circular Logic'),
    (select id from sets where short_name = 'uma'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slum Reaper'),
    (select id from sets where short_name = 'uma'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boar Umbra'),
    (select id from sets where short_name = 'uma'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vengevine'),
    (select id from sets where short_name = 'uma'),
    '189',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Flight of Fancy'),
    (select id from sets where short_name = 'uma'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reveillark'),
    (select id from sets where short_name = 'uma'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestial Colonnade'),
    (select id from sets where short_name = 'uma'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wild Mongrel'),
    (select id from sets where short_name = 'uma'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faithless Looting'),
    (select id from sets where short_name = 'uma'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghoulcaller''s Accomplice'),
    (select id from sets where short_name = 'uma'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Retrieval'),
    (select id from sets where short_name = 'uma'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stream of Consciousness'),
    (select id from sets where short_name = 'uma'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tethmos High Priest'),
    (select id from sets where short_name = 'uma'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bitterblossom'),
    (select id from sets where short_name = 'uma'),
    '85',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Become Immense'),
    (select id from sets where short_name = 'uma'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dakmor Salvage'),
    (select id from sets where short_name = 'uma'),
    '240',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Appetite for Brains'),
    (select id from sets where short_name = 'uma'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Just the Wind'),
    (select id from sets where short_name = 'uma'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Devoted Druid'),
    (select id from sets where short_name = 'uma'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Iridescent Drake'),
    (select id from sets where short_name = 'uma'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woodfall Primus'),
    (select id from sets where short_name = 'uma'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Generator Servant'),
    (select id from sets where short_name = 'uma'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boneyard Wurm'),
    (select id from sets where short_name = 'uma'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Laboratory Maniac'),
    (select id from sets where short_name = 'uma'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goryo''s Vengeance'),
    (select id from sets where short_name = 'uma'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temporal Manipulation'),
    (select id from sets where short_name = 'uma'),
    '77',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sigil of the New Dawn'),
    (select id from sets where short_name = 'uma'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Guildmage'),
    (select id from sets where short_name = 'uma'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karn Liberated'),
    (select id from sets where short_name = 'uma'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Seismic Assault'),
    (select id from sets where short_name = 'uma'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghoulsteed'),
    (select id from sets where short_name = 'uma'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kozilek, Butcher of Truth'),
    (select id from sets where short_name = 'uma'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hooting Mandrills'),
    (select id from sets where short_name = 'uma'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spoils of the Vault'),
    (select id from sets where short_name = 'uma'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skywing Aven'),
    (select id from sets where short_name = 'uma'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stitcher''s Apprentice'),
    (select id from sets where short_name = 'uma'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Platinum Emperion'),
    (select id from sets where short_name = 'uma'),
    '233',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mistveil Plains'),
    (select id from sets where short_name = 'uma'),
    '247',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stitched Drake'),
    (select id from sets where short_name = 'uma'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Offalsnout'),
    (select id from sets where short_name = 'uma'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Denied'),
    (select id from sets where short_name = 'uma'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akroan Crusader'),
    (select id from sets where short_name = 'uma'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Think Twice'),
    (select id from sets where short_name = 'uma'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heliod''s Pilgrim'),
    (select id from sets where short_name = 'uma'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prismatic Lens'),
    (select id from sets where short_name = 'uma'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spider Umbra'),
    (select id from sets where short_name = 'uma'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lava Spike'),
    (select id from sets where short_name = 'uma'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raging Ravine'),
    (select id from sets where short_name = 'uma'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blast of Genius'),
    (select id from sets where short_name = 'uma'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Containment Priest'),
    (select id from sets where short_name = 'uma'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snapcaster Mage'),
    (select id from sets where short_name = 'uma'),
    '71',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'uma'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Miming Slime'),
    (select id from sets where short_name = 'uma'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daybreak Coronet'),
    (select id from sets where short_name = 'uma'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swift Reckoning'),
    (select id from sets where short_name = 'uma'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Double Cleave'),
    (select id from sets where short_name = 'uma'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wickerbough Elder'),
    (select id from sets where short_name = 'uma'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talrand, Sky Summoner'),
    (select id from sets where short_name = 'uma'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crushing Canopy'),
    (select id from sets where short_name = 'uma'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mahamoti Djinn'),
    (select id from sets where short_name = 'uma'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hyena Umbra'),
    (select id from sets where short_name = 'uma'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pulse of Murasa'),
    (select id from sets where short_name = 'uma'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lord of Extinction'),
    (select id from sets where short_name = 'uma'),
    '203',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mikaeus, the Unhallowed'),
    (select id from sets where short_name = 'uma'),
    '106',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mad Prophet'),
    (select id from sets where short_name = 'uma'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golgari Grave-Troll'),
    (select id from sets where short_name = 'uma'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reckless Wurm'),
    (select id from sets where short_name = 'uma'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shriekmaw'),
    (select id from sets where short_name = 'uma'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'All Is Dust'),
    (select id from sets where short_name = 'uma'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heap Doll'),
    (select id from sets where short_name = 'uma'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gods Willing'),
    (select id from sets where short_name = 'uma'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golgari Thug'),
    (select id from sets where short_name = 'uma'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Songs of the Damned'),
    (select id from sets where short_name = 'uma'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sparkspitter'),
    (select id from sets where short_name = 'uma'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vessel of Endless Rest'),
    (select id from sets where short_name = 'uma'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hero of Leina Tower'),
    (select id from sets where short_name = 'uma'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Conviction'),
    (select id from sets where short_name = 'uma'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nourishing Shoal'),
    (select id from sets where short_name = 'uma'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Satyr Wayfinder'),
    (select id from sets where short_name = 'uma'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stingerfling Spider'),
    (select id from sets where short_name = 'uma'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sublime Archangel'),
    (select id from sets where short_name = 'uma'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Groundskeeper'),
    (select id from sets where short_name = 'uma'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phalanx Leader'),
    (select id from sets where short_name = 'uma'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warleader''s Helix'),
    (select id from sets where short_name = 'uma'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wingsteed Rider'),
    (select id from sets where short_name = 'uma'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Hunger'),
    (select id from sets where short_name = 'uma'),
    '193',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Young Pyromancer'),
    (select id from sets where short_name = 'uma'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Charm'),
    (select id from sets where short_name = 'uma'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sovereigns of Lost Alara'),
    (select id from sets where short_name = 'uma'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desolate Lighthouse'),
    (select id from sets where short_name = 'uma'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whirlwind Adept'),
    (select id from sets where short_name = 'uma'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Renewal'),
    (select id from sets where short_name = 'uma'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Despair'),
    (select id from sets where short_name = 'uma'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Resurrection'),
    (select id from sets where short_name = 'uma'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cathodion'),
    (select id from sets where short_name = 'uma'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chainer''s Edict'),
    (select id from sets where short_name = 'uma'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marang River Prowler'),
    (select id from sets where short_name = 'uma'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vexing Devil'),
    (select id from sets where short_name = 'uma'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kitchen Finks'),
    (select id from sets where short_name = 'uma'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plumeveil'),
    (select id from sets where short_name = 'uma'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brawn'),
    (select id from sets where short_name = 'uma'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Walker of the Grove'),
    (select id from sets where short_name = 'uma'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murderous Redcap'),
    (select id from sets where short_name = 'uma'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dawn Charm'),
    (select id from sets where short_name = 'uma'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vengeful Rebirth'),
    (select id from sets where short_name = 'uma'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disrupting Shoal'),
    (select id from sets where short_name = 'uma'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grave Strength'),
    (select id from sets where short_name = 'uma'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Conflagrate'),
    (select id from sets where short_name = 'uma'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reviving Vapors'),
    (select id from sets where short_name = 'uma'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frantic Search'),
    (select id from sets where short_name = 'uma'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancestor''s Chosen'),
    (select id from sets where short_name = 'uma'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fecundity'),
    (select id from sets where short_name = 'uma'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arena Athlete'),
    (select id from sets where short_name = 'uma'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fume Spitter'),
    (select id from sets where short_name = 'uma'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancient Tomb'),
    (select id from sets where short_name = 'uma'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glen Elendra Archmage'),
    (select id from sets where short_name = 'uma'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fauna Shaman'),
    (select id from sets where short_name = 'uma'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gamble'),
    (select id from sets where short_name = 'uma'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pattern of Rebirth'),
    (select id from sets where short_name = 'uma'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Tower'),
    (select id from sets where short_name = 'uma'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lotus-Eye Mystics'),
    (select id from sets where short_name = 'uma'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Defy Gravity'),
    (select id from sets where short_name = 'uma'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Conscription'),
    (select id from sets where short_name = 'uma'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Runed Halo'),
    (select id from sets where short_name = 'uma'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyspear Cavalry'),
    (select id from sets where short_name = 'uma'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slippery Bogle'),
    (select id from sets where short_name = 'uma'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Furnace Celebration'),
    (select id from sets where short_name = 'uma'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Malevolent Whispers'),
    (select id from sets where short_name = 'uma'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Icatian Crier'),
    (select id from sets where short_name = 'uma'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urban Evolution'),
    (select id from sets where short_name = 'uma'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Buried Alive'),
    (select id from sets where short_name = 'uma'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unholy Hunger'),
    (select id from sets where short_name = 'uma'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Staunch-Hearted Warrior'),
    (select id from sets where short_name = 'uma'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rogue''s Passage'),
    (select id from sets where short_name = 'uma'),
    '250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thermo-Alchemist'),
    (select id from sets where short_name = 'uma'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raid Bombardment'),
    (select id from sets where short_name = 'uma'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Travel Preparations'),
    (select id from sets where short_name = 'uma'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Archaeomancer'),
    (select id from sets where short_name = 'uma'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garna, the Bloodflame'),
    (select id from sets where short_name = 'uma'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rise from the Tides'),
    (select id from sets where short_name = 'uma'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gurmag Angler'),
    (select id from sets where short_name = 'uma'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leovold, Emissary of Trest'),
    (select id from sets where short_name = 'uma'),
    '202',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mana Vault'),
    (select id from sets where short_name = 'uma'),
    '229',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Safehold Elite'),
    (select id from sets where short_name = 'uma'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magus of the Bazaar'),
    (select id from sets where short_name = 'uma'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Pulse'),
    (select id from sets where short_name = 'uma'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eternal Witness'),
    (select id from sets where short_name = 'uma'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Myr Servitor'),
    (select id from sets where short_name = 'uma'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Entomb'),
    (select id from sets where short_name = 'uma'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Life from the Loam'),
    (select id from sets where short_name = 'uma'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tasigur, the Golden Fang'),
    (select id from sets where short_name = 'uma'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reckless Charge'),
    (select id from sets where short_name = 'uma'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mammoth Umbra'),
    (select id from sets where short_name = 'uma'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unstable Mutation'),
    (select id from sets where short_name = 'uma'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Penumbra Wurm'),
    (select id from sets where short_name = 'uma'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Depths'),
    (select id from sets where short_name = 'uma'),
    '241',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Snake Umbra'),
    (select id from sets where short_name = 'uma'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grave Scrabbler'),
    (select id from sets where short_name = 'uma'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golgari Brownscale'),
    (select id from sets where short_name = 'uma'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karakas'),
    (select id from sets where short_name = 'uma'),
    '244',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hissing Iguanar'),
    (select id from sets where short_name = 'uma'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana of the Veil'),
    (select id from sets where short_name = 'uma'),
    '104',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forbidden Alchemy'),
    (select id from sets where short_name = 'uma'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Foil'),
    (select id from sets where short_name = 'uma'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flagstones of Trokair'),
    (select id from sets where short_name = 'uma'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crow of Dark Tidings'),
    (select id from sets where short_name = 'uma'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ulamog''s Crusher'),
    (select id from sets where short_name = 'uma'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spider Spawning'),
    (select id from sets where short_name = 'uma'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wandering Champion'),
    (select id from sets where short_name = 'uma'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fiend Hunter'),
    (select id from sets where short_name = 'uma'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Visions of Beyond'),
    (select id from sets where short_name = 'uma'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Engineered Explosives'),
    (select id from sets where short_name = 'uma'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Dabbling'),
    (select id from sets where short_name = 'uma'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gaddock Teeg'),
    (select id from sets where short_name = 'uma'),
    '199',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreamscape Artist'),
    (select id from sets where short_name = 'uma'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aethersnipe'),
    (select id from sets where short_name = 'uma'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Twins of Maurer Estate'),
    (select id from sets where short_name = 'uma'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Canker Abomination'),
    (select id from sets where short_name = 'uma'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brazen Scourge'),
    (select id from sets where short_name = 'uma'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Squee, Goblin Nabob'),
    (select id from sets where short_name = 'uma'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sultai Skullkeeper'),
    (select id from sets where short_name = 'uma'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ingot Chewer'),
    (select id from sets where short_name = 'uma'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightbird''s Clutches'),
    (select id from sets where short_name = 'uma'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Countersquall'),
    (select id from sets where short_name = 'uma'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Lore'),
    (select id from sets where short_name = 'uma'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unburial Rites'),
    (select id from sets where short_name = 'uma'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spirit Cairn'),
    (select id from sets where short_name = 'uma'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sigarda, Host of Herons'),
    (select id from sets where short_name = 'uma'),
    '206',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hero of Iroas'),
    (select id from sets where short_name = 'uma'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anger'),
    (select id from sets where short_name = 'uma'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Molten Birth'),
    (select id from sets where short_name = 'uma'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Noble Hierarch'),
    (select id from sets where short_name = 'uma'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ulamog, the Infinite Gyre'),
    (select id from sets where short_name = 'uma'),
    '7',
    'mythic'
) 
 on conflict do nothing;
