insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Narset of the Ancient Way Emblem'),
    (select id from sets where short_name = 'tiko'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tiko'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shark'),
    (select id from sets where short_name = 'tiko'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Companion'),
    (select id from sets where short_name = 'tiko'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Human Soldier'),
    (select id from sets where short_name = 'tiko'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat Bird'),
    (select id from sets where short_name = 'tiko'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Human Soldier'),
    (select id from sets where short_name = 'tiko'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dinosaur Beast'),
    (select id from sets where short_name = 'tiko'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feather'),
    (select id from sets where short_name = 'tiko'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ability Punchcard'),
    (select id from sets where short_name = 'tiko'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kraken'),
    (select id from sets where short_name = 'tiko'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat'),
    (select id from sets where short_name = 'tiko'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ability Punchcard'),
    (select id from sets where short_name = 'tiko'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Human Soldier'),
    (select id from sets where short_name = 'tiko'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dinosaur'),
    (select id from sets where short_name = 'tiko'),
    '8',
    'common'
) 
 on conflict do nothing;
