insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Bident of Thassa'),
    (select id from sets where short_name = 'pths'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ember Swallower'),
    (select id from sets where short_name = 'pths'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karametra''s Acolyte'),
    (select id from sets where short_name = 'pths'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nighthowler'),
    (select id from sets where short_name = 'pths'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anthousa, Setessan Hero'),
    (select id from sets where short_name = 'pths'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shipbreaker Kraken'),
    (select id from sets where short_name = 'pths'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phalanx Leader'),
    (select id from sets where short_name = 'pths'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Caryatid'),
    (select id from sets where short_name = 'pths'),
    '*180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestial Archon'),
    (select id from sets where short_name = 'pths'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abhorrent Overlord'),
    (select id from sets where short_name = 'pths'),
    '75',
    'rare'
) 
 on conflict do nothing;
