insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Thopter'),
    (select id from sets where short_name = 'tddu'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter'),
    (select id from sets where short_name = 'tddu'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elf Warrior'),
    (select id from sets where short_name = 'tddu'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr'),
    (select id from sets where short_name = 'tddu'),
    '2',
    'common'
) 
 on conflict do nothing;
