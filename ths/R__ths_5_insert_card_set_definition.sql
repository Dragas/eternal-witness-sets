insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Coastline Chimera'),
    (select id from sets where short_name = 'ths'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Priest of Iroas'),
    (select id from sets where short_name = 'ths'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sentry of the Underworld'),
    (select id from sets where short_name = 'ths'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viper''s Kiss'),
    (select id from sets where short_name = 'ths'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lost in a Labyrinth'),
    (select id from sets where short_name = 'ths'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vaporkin'),
    (select id from sets where short_name = 'ths'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spellheart Chimera'),
    (select id from sets where short_name = 'ths'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phalanx Leader'),
    (select id from sets where short_name = 'ths'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Temple of Silence'),
    (select id from sets where short_name = 'ths'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Returned Centaur'),
    (select id from sets where short_name = 'ths'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Triton Fortune Hunter'),
    (select id from sets where short_name = 'ths'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yoked Ox'),
    (select id from sets where short_name = 'ths'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shipwreck Singer'),
    (select id from sets where short_name = 'ths'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Insatiable Harpy'),
    (select id from sets where short_name = 'ths'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Voyaging Satyr'),
    (select id from sets where short_name = 'ths'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ths'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Last Breath'),
    (select id from sets where short_name = 'ths'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Master of Waves'),
    (select id from sets where short_name = 'ths'),
    '53',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nimbus Naiad'),
    (select id from sets where short_name = 'ths'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rage of Purphoros'),
    (select id from sets where short_name = 'ths'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pheres-Band Centaurs'),
    (select id from sets where short_name = 'ths'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sip of Hemlock'),
    (select id from sets where short_name = 'ths'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fleetfeather Sandals'),
    (select id from sets where short_name = 'ths'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spearpoint Oread'),
    (select id from sets where short_name = 'ths'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gift of Immortality'),
    (select id from sets where short_name = 'ths'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Traveler''s Amulet'),
    (select id from sets where short_name = 'ths'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Favored Hoplite'),
    (select id from sets where short_name = 'ths'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nighthowler'),
    (select id from sets where short_name = 'ths'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ths'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Benthic Giant'),
    (select id from sets where short_name = 'ths'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Centaur Battlemaster'),
    (select id from sets where short_name = 'ths'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Psychic Intrusion'),
    (select id from sets where short_name = 'ths'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nessian Asp'),
    (select id from sets where short_name = 'ths'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rescue from the Underworld'),
    (select id from sets where short_name = 'ths'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nylea''s Emissary'),
    (select id from sets where short_name = 'ths'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Temple of Mystery'),
    (select id from sets where short_name = 'ths'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ths'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shredding Winds'),
    (select id from sets where short_name = 'ths'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Staunch-Hearted Warrior'),
    (select id from sets where short_name = 'ths'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reaper of the Wilds'),
    (select id from sets where short_name = 'ths'),
    '201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spear of Heliod'),
    (select id from sets where short_name = 'ths'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Setessan Griffin'),
    (select id from sets where short_name = 'ths'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nylea''s Disciple'),
    (select id from sets where short_name = 'ths'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karametra''s Acolyte'),
    (select id from sets where short_name = 'ths'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fleshmad Steed'),
    (select id from sets where short_name = 'ths'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voyage''s End'),
    (select id from sets where short_name = 'ths'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Underworld Cerberus'),
    (select id from sets where short_name = 'ths'),
    '208',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ths'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thassa''s Bounty'),
    (select id from sets where short_name = 'ths'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Xenagos, the Reveler'),
    (select id from sets where short_name = 'ths'),
    '209',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mistcutter Hydra'),
    (select id from sets where short_name = 'ths'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stoneshock Giant'),
    (select id from sets where short_name = 'ths'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Titan''s Strength'),
    (select id from sets where short_name = 'ths'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Colossus of Akros'),
    (select id from sets where short_name = 'ths'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hammer of Purphoros'),
    (select id from sets where short_name = 'ths'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Labyrinth Champion'),
    (select id from sets where short_name = 'ths'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sealock Monster'),
    (select id from sets where short_name = 'ths'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mnemonic Wall'),
    (select id from sets where short_name = 'ths'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boon of Erebos'),
    (select id from sets where short_name = 'ths'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abhorrent Overlord'),
    (select id from sets where short_name = 'ths'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shipbreaker Kraken'),
    (select id from sets where short_name = 'ths'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prophet of Kruphix'),
    (select id from sets where short_name = 'ths'),
    '199',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nessian Courser'),
    (select id from sets where short_name = 'ths'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple of Abandon'),
    (select id from sets where short_name = 'ths'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stormbreath Dragon'),
    (select id from sets where short_name = 'ths'),
    '143',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Heliod''s Emissary'),
    (select id from sets where short_name = 'ths'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Polukranos, World Eater'),
    (select id from sets where short_name = 'ths'),
    '172',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Demolish'),
    (select id from sets where short_name = 'ths'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loathsome Catoblepas'),
    (select id from sets where short_name = 'ths'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tymaret, the Murder King'),
    (select id from sets where short_name = 'ths'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hopeful Eidolon'),
    (select id from sets where short_name = 'ths'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battlewise Hoplite'),
    (select id from sets where short_name = 'ths'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Purphoros''s Emissary'),
    (select id from sets where short_name = 'ths'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Temple of Triumph'),
    (select id from sets where short_name = 'ths'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ths'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cavalry Pegasus'),
    (select id from sets where short_name = 'ths'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Betrayal'),
    (select id from sets where short_name = 'ths'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stymied Hopes'),
    (select id from sets where short_name = 'ths'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fanatic of Mogis'),
    (select id from sets where short_name = 'ths'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Triad of Fates'),
    (select id from sets where short_name = 'ths'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savage Surge'),
    (select id from sets where short_name = 'ths'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hero''s Downfall'),
    (select id from sets where short_name = 'ths'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashiok, Nightmare Weaver'),
    (select id from sets where short_name = 'ths'),
    '188',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Erebos''s Emissary'),
    (select id from sets where short_name = 'ths'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sea God''s Revenge'),
    (select id from sets where short_name = 'ths'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feral Invocation'),
    (select id from sets where short_name = 'ths'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flamecast Wheel'),
    (select id from sets where short_name = 'ths'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soldier of the Pantheon'),
    (select id from sets where short_name = 'ths'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Annul'),
    (select id from sets where short_name = 'ths'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashen Rider'),
    (select id from sets where short_name = 'ths'),
    '187',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hunt the Hunter'),
    (select id from sets where short_name = 'ths'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fleecemane Lion'),
    (select id from sets where short_name = 'ths'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cutthroat Maneuver'),
    (select id from sets where short_name = 'ths'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anax and Cymede'),
    (select id from sets where short_name = 'ths'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warriors'' Lesson'),
    (select id from sets where short_name = 'ths'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fabled Hero'),
    (select id from sets where short_name = 'ths'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thoughtseize'),
    (select id from sets where short_name = 'ths'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boulderfall'),
    (select id from sets where short_name = 'ths'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sedge Scorpion'),
    (select id from sets where short_name = 'ths'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Asphodel Wanderer'),
    (select id from sets where short_name = 'ths'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Firedrinker Satyr'),
    (select id from sets where short_name = 'ths'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Minotaur Skullcleaver'),
    (select id from sets where short_name = 'ths'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple of Deceit'),
    (select id from sets where short_name = 'ths'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lagonna-Band Elder'),
    (select id from sets where short_name = 'ths'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akroan Hoplite'),
    (select id from sets where short_name = 'ths'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daxos of Meletis'),
    (select id from sets where short_name = 'ths'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coordinated Assault'),
    (select id from sets where short_name = 'ths'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Witches'' Eye'),
    (select id from sets where short_name = 'ths'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steam Augury'),
    (select id from sets where short_name = 'ths'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spark Jolt'),
    (select id from sets where short_name = 'ths'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ephara''s Warden'),
    (select id from sets where short_name = 'ths'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wavecrash Triton'),
    (select id from sets where short_name = 'ths'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horizon Chimera'),
    (select id from sets where short_name = 'ths'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anvilwrought Raptor'),
    (select id from sets where short_name = 'ths'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burnished Hart'),
    (select id from sets where short_name = 'ths'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ordeal of Heliod'),
    (select id from sets where short_name = 'ths'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kragma Warcaller'),
    (select id from sets where short_name = 'ths'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Curse of the Swine'),
    (select id from sets where short_name = 'ths'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Destructive Revelry'),
    (select id from sets where short_name = 'ths'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Time to Feed'),
    (select id from sets where short_name = 'ths'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divine Verdict'),
    (select id from sets where short_name = 'ths'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ths'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Decorated Griffin'),
    (select id from sets where short_name = 'ths'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Purphoros, God of the Forge'),
    (select id from sets where short_name = 'ths'),
    '135',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Battlewise Valor'),
    (select id from sets where short_name = 'ths'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Polis Crusher'),
    (select id from sets where short_name = 'ths'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ths'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hundred-Handed One'),
    (select id from sets where short_name = 'ths'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disciple of Phenax'),
    (select id from sets where short_name = 'ths'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leafcrown Dryad'),
    (select id from sets where short_name = 'ths'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nykthos, Shrine to Nyx'),
    (select id from sets where short_name = 'ths'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guardians of Meletis'),
    (select id from sets where short_name = 'ths'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baleful Eidolon'),
    (select id from sets where short_name = 'ths'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bident of Thassa'),
    (select id from sets where short_name = 'ths'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Breaching Hippocamp'),
    (select id from sets where short_name = 'ths'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Satyr Hedonist'),
    (select id from sets where short_name = 'ths'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bow of Nylea'),
    (select id from sets where short_name = 'ths'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magma Jet'),
    (select id from sets where short_name = 'ths'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ths'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crackling Triton'),
    (select id from sets where short_name = 'ths'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glare of Heresy'),
    (select id from sets where short_name = 'ths'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vanquish the Foul'),
    (select id from sets where short_name = 'ths'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anger of the Gods'),
    (select id from sets where short_name = 'ths'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fate Foretold'),
    (select id from sets where short_name = 'ths'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Returned Phalanx'),
    (select id from sets where short_name = 'ths'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Messenger''s Speed'),
    (select id from sets where short_name = 'ths'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scholar of Athreos'),
    (select id from sets where short_name = 'ths'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reverent Hunter'),
    (select id from sets where short_name = 'ths'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'March of the Returned'),
    (select id from sets where short_name = 'ths'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dissolve'),
    (select id from sets where short_name = 'ths'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Borderland Minotaur'),
    (select id from sets where short_name = 'ths'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Setessan Battle Priest'),
    (select id from sets where short_name = 'ths'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mogis''s Marauder'),
    (select id from sets where short_name = 'ths'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ordeal of Erebos'),
    (select id from sets where short_name = 'ths'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whip of Erebos'),
    (select id from sets where short_name = 'ths'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prognostic Sphinx'),
    (select id from sets where short_name = 'ths'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestial Archon'),
    (select id from sets where short_name = 'ths'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Portent of Betrayal'),
    (select id from sets where short_name = 'ths'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Griptide'),
    (select id from sets where short_name = 'ths'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akroan Crusader'),
    (select id from sets where short_name = 'ths'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chained to the Rocks'),
    (select id from sets where short_name = 'ths'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Artisan of Forms'),
    (select id from sets where short_name = 'ths'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hythonia the Cruel'),
    (select id from sets where short_name = 'ths'),
    '91',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gods Willing'),
    (select id from sets where short_name = 'ths'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horizon Scholar'),
    (select id from sets where short_name = 'ths'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ths'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keepsake Gorgon'),
    (select id from sets where short_name = 'ths'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leonin Snarecaster'),
    (select id from sets where short_name = 'ths'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gray Merchant of Asphodel'),
    (select id from sets where short_name = 'ths'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathbellow Raider'),
    (select id from sets where short_name = 'ths'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ths'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Felhide Minotaur'),
    (select id from sets where short_name = 'ths'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Artisan''s Sorrow'),
    (select id from sets where short_name = 'ths'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ray of Dissolution'),
    (select id from sets where short_name = 'ths'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Medomai the Ageless'),
    (select id from sets where short_name = 'ths'),
    '196',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Flamespeaker Adept'),
    (select id from sets where short_name = 'ths'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aqueous Form'),
    (select id from sets where short_name = 'ths'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lash of the Whip'),
    (select id from sets where short_name = 'ths'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bronze Sable'),
    (select id from sets where short_name = 'ths'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evangel of Heliod'),
    (select id from sets where short_name = 'ths'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Strike'),
    (select id from sets where short_name = 'ths'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meletis Charlatan'),
    (select id from sets where short_name = 'ths'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ths'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pharika''s Cure'),
    (select id from sets where short_name = 'ths'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prowler''s Helm'),
    (select id from sets where short_name = 'ths'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tormented Hero'),
    (select id from sets where short_name = 'ths'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Defend the Hearth'),
    (select id from sets where short_name = 'ths'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nemesis of Mortals'),
    (select id from sets where short_name = 'ths'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anthousa, Setessan Hero'),
    (select id from sets where short_name = 'ths'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Agent of Horizons'),
    (select id from sets where short_name = 'ths'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vulpine Goliath'),
    (select id from sets where short_name = 'ths'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opaline Unicorn'),
    (select id from sets where short_name = 'ths'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erebos, God of the Dead'),
    (select id from sets where short_name = 'ths'),
    '85',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ill-Tempered Cyclops'),
    (select id from sets where short_name = 'ths'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood-Toll Harpy'),
    (select id from sets where short_name = 'ths'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Caryatid'),
    (select id from sets where short_name = 'ths'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thassa, God of the Sea'),
    (select id from sets where short_name = 'ths'),
    '66',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ths'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chosen by Heliod'),
    (select id from sets where short_name = 'ths'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Triton Shorethief'),
    (select id from sets where short_name = 'ths'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Mantle'),
    (select id from sets where short_name = 'ths'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Read the Bones'),
    (select id from sets where short_name = 'ths'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyxis of Pandemonium'),
    (select id from sets where short_name = 'ths'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silent Artisan'),
    (select id from sets where short_name = 'ths'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ths'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chronicler of Heroes'),
    (select id from sets where short_name = 'ths'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arbor Colossus'),
    (select id from sets where short_name = 'ths'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wingsteed Rider'),
    (select id from sets where short_name = 'ths'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Commune with the Gods'),
    (select id from sets where short_name = 'ths'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Observant Alseid'),
    (select id from sets where short_name = 'ths'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arena Athlete'),
    (select id from sets where short_name = 'ths'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Satyr Piper'),
    (select id from sets where short_name = 'ths'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ember Swallower'),
    (select id from sets where short_name = 'ths'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cavern Lampad'),
    (select id from sets where short_name = 'ths'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fade into Antiquity'),
    (select id from sets where short_name = 'ths'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ths'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ordeal of Nylea'),
    (select id from sets where short_name = 'ths'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dauntless Onslaught'),
    (select id from sets where short_name = 'ths'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gainsay'),
    (select id from sets where short_name = 'ths'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nylea''s Presence'),
    (select id from sets where short_name = 'ths'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ths'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Celebrants'),
    (select id from sets where short_name = 'ths'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ths'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heliod, God of the Sun'),
    (select id from sets where short_name = 'ths'),
    '17',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ths'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akroan Horse'),
    (select id from sets where short_name = 'ths'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Agent of the Fates'),
    (select id from sets where short_name = 'ths'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Peak Eruption'),
    (select id from sets where short_name = 'ths'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scourgemark'),
    (select id from sets where short_name = 'ths'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Traveling Philosopher'),
    (select id from sets where short_name = 'ths'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Titan of Eternal Fire'),
    (select id from sets where short_name = 'ths'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Triton Tactics'),
    (select id from sets where short_name = 'ths'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ordeal of Purphoros'),
    (select id from sets where short_name = 'ths'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swan Song'),
    (select id from sets where short_name = 'ths'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ths'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nylea, God of the Hunt'),
    (select id from sets where short_name = 'ths'),
    '166',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Omenspeaker'),
    (select id from sets where short_name = 'ths'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ths'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pharika''s Mender'),
    (select id from sets where short_name = 'ths'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boon Satyr'),
    (select id from sets where short_name = 'ths'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thassa''s Emissary'),
    (select id from sets where short_name = 'ths'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Two-Headed Cerberus'),
    (select id from sets where short_name = 'ths'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prescient Chimera'),
    (select id from sets where short_name = 'ths'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ordeal of Thassa'),
    (select id from sets where short_name = 'ths'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Satyr Rambler'),
    (select id from sets where short_name = 'ths'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ths'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elspeth, Sun''s Champion'),
    (select id from sets where short_name = 'ths'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rageblood Shaman'),
    (select id from sets where short_name = 'ths'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unknown Shores'),
    (select id from sets where short_name = 'ths'),
    '229',
    'common'
) 
 on conflict do nothing;
