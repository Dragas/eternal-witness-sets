insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Fated Intervention'),
    (select id from sets where short_name = 'cp1'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Mystery'),
    (select id from sets where short_name = 'cp1'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Font of Fertility'),
    (select id from sets where short_name = 'cp1'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prophet of Kruphix'),
    (select id from sets where short_name = 'cp1'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hydra Broodmaster'),
    (select id from sets where short_name = 'cp1'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prognostic Sphinx'),
    (select id from sets where short_name = 'cp1'),
    '1',
    'rare'
) 
 on conflict do nothing;
