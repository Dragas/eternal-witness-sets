    insert into mtgcard(name) values ('Fated Intervention') on conflict do nothing;
    insert into mtgcard(name) values ('Temple of Mystery') on conflict do nothing;
    insert into mtgcard(name) values ('Font of Fertility') on conflict do nothing;
    insert into mtgcard(name) values ('Prophet of Kruphix') on conflict do nothing;
    insert into mtgcard(name) values ('Hydra Broodmaster') on conflict do nothing;
    insert into mtgcard(name) values ('Prognostic Sphinx') on conflict do nothing;
