insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Oracle''s Vault'),
    (select id from sets where short_name = 'pakh'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Irrigated Farmland'),
    (select id from sets where short_name = 'pakh'),
    '245s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bounty of the Luxa'),
    (select id from sets where short_name = 'pakh'),
    '196s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dispossess'),
    (select id from sets where short_name = 'pakh'),
    '86s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oracle''s Vault'),
    (select id from sets where short_name = 'pakh'),
    '234s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bontu the Glorified'),
    (select id from sets where short_name = 'pakh'),
    '82s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Commit // Memory'),
    (select id from sets where short_name = 'pakh'),
    '211p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sandwurm Convergence'),
    (select id from sets where short_name = 'pakh'),
    '183s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyramid of the Pantheon'),
    (select id from sets where short_name = 'pakh'),
    '235s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scattered Groves'),
    (select id from sets where short_name = 'pakh'),
    '247s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa, Steward of Elements'),
    (select id from sets where short_name = 'pakh'),
    '204s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Canyon Slough'),
    (select id from sets where short_name = 'pakh'),
    '239s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sweltering Suns'),
    (select id from sets where short_name = 'pakh'),
    '149s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cruel Reality'),
    (select id from sets where short_name = 'pakh'),
    '84s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Prepare // Fight'),
    (select id from sets where short_name = 'pakh'),
    '220s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana, Death''s Majesty'),
    (select id from sets where short_name = 'pakh'),
    '97s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pull from Tomorrow'),
    (select id from sets where short_name = 'pakh'),
    '65s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Honored Hydra'),
    (select id from sets where short_name = 'pakh'),
    '172s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harvest Season'),
    (select id from sets where short_name = 'pakh'),
    '170p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archfiend of Ifnir'),
    (select id from sets where short_name = 'pakh'),
    '78s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel of Sanctions'),
    (select id from sets where short_name = 'pakh'),
    '1s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vizier of Many Faces'),
    (select id from sets where short_name = 'pakh'),
    '74s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harvest Season'),
    (select id from sets where short_name = 'pakh'),
    '170s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drake Haven'),
    (select id from sets where short_name = 'pakh'),
    '51s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Combat Celebrant'),
    (select id from sets where short_name = 'pakh'),
    '125s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vizier of the Menagerie'),
    (select id from sets where short_name = 'pakh'),
    '192s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Champion of Rhonas'),
    (select id from sets where short_name = 'pakh'),
    '159s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Insult // Injury'),
    (select id from sets where short_name = 'pakh'),
    '213s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heaven // Earth'),
    (select id from sets where short_name = 'pakh'),
    '224s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hazoret the Fervent'),
    (select id from sets where short_name = 'pakh'),
    '136s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Glorybringer'),
    (select id from sets where short_name = 'pakh'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cascading Cataracts'),
    (select id from sets where short_name = 'pakh'),
    '240s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Throne of the God-Pharaoh'),
    (select id from sets where short_name = 'pakh'),
    '237s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'New Perspectives'),
    (select id from sets where short_name = 'pakh'),
    '63s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gideon of the Trials'),
    (select id from sets where short_name = 'pakh'),
    '14s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hazoret''s Favor'),
    (select id from sets where short_name = 'pakh'),
    '137s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'As Foretold'),
    (select id from sets where short_name = 'pakh'),
    '42s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Glorious End'),
    (select id from sets where short_name = 'pakh'),
    '133s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gideon''s Intervention'),
    (select id from sets where short_name = 'pakh'),
    '15s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temmet, Vizier of Naktamun'),
    (select id from sets where short_name = 'pakh'),
    '207s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Neheb, the Worthy'),
    (select id from sets where short_name = 'pakh'),
    '203s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aven Mindcensor'),
    (select id from sets where short_name = 'pakh'),
    '5p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anointed Procession'),
    (select id from sets where short_name = 'pakh'),
    '2p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harsh Mentor'),
    (select id from sets where short_name = 'pakh'),
    '135s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mouth // Feed'),
    (select id from sets where short_name = 'pakh'),
    '214s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rhonas the Indomitable'),
    (select id from sets where short_name = 'pakh'),
    '182s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Glorybringer'),
    (select id from sets where short_name = 'pakh'),
    '134s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trueheart Duelist'),
    (select id from sets where short_name = 'pakh'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aven Mindcensor'),
    (select id from sets where short_name = 'pakh'),
    '5s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anointed Procession'),
    (select id from sets where short_name = 'pakh'),
    '2s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shadow of the Grave'),
    (select id from sets where short_name = 'pakh'),
    '107s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heart-Piercer Manticore'),
    (select id from sets where short_name = 'pakh'),
    '138s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glyph Keeper'),
    (select id from sets where short_name = 'pakh'),
    '55s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kefnet the Mindful'),
    (select id from sets where short_name = 'pakh'),
    '59s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fetid Pools'),
    (select id from sets where short_name = 'pakh'),
    '243s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sheltered Thicket'),
    (select id from sets where short_name = 'pakh'),
    '248s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Commit // Memory'),
    (select id from sets where short_name = 'pakh'),
    '211s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oketra the True'),
    (select id from sets where short_name = 'pakh'),
    '21s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cut // Ribbons'),
    (select id from sets where short_name = 'pakh'),
    '223s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hapatra, Vizier of Poisons'),
    (select id from sets where short_name = 'pakh'),
    '199s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Approach of the Second Sun'),
    (select id from sets where short_name = 'pakh'),
    '4s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rags // Riches'),
    (select id from sets where short_name = 'pakh'),
    '222s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curator of Mysteries'),
    (select id from sets where short_name = 'pakh'),
    '49s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dusk // Dawn'),
    (select id from sets where short_name = 'pakh'),
    '210s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul-Scar Mage'),
    (select id from sets where short_name = 'pakh'),
    '148s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Regal Caracal'),
    (select id from sets where short_name = 'pakh'),
    '24s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plague Belcher'),
    (select id from sets where short_name = 'pakh'),
    '104s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Samut, Voice of Dissent'),
    (select id from sets where short_name = 'pakh'),
    '205s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Mastery'),
    (select id from sets where short_name = 'pakh'),
    '98s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Channeler Initiate'),
    (select id from sets where short_name = 'pakh'),
    '160s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archfiend of Ifnir'),
    (select id from sets where short_name = 'pakh'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pull from Tomorrow'),
    (select id from sets where short_name = 'pakh'),
    '65p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glory-Bound Initiate'),
    (select id from sets where short_name = 'pakh'),
    '16s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Failure // Comply'),
    (select id from sets where short_name = 'pakh'),
    '221s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prowling Serpopard'),
    (select id from sets where short_name = 'pakh'),
    '180s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Never // Return'),
    (select id from sets where short_name = 'pakh'),
    '212s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dread Wanderer'),
    (select id from sets where short_name = 'pakh'),
    '88s',
    'rare'
) 
 on conflict do nothing;
