insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Wall of Vipers'),
    (select id from sets where short_name = 'pcy'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Samite Sanctuary'),
    (select id from sets where short_name = 'pcy'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Despoil'),
    (select id from sets where short_name = 'pcy'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Glider'),
    (select id from sets where short_name = 'pcy'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhystic Deluge'),
    (select id from sets where short_name = 'pcy'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pygmy Razorback'),
    (select id from sets where short_name = 'pcy'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Vapors'),
    (select id from sets where short_name = 'pcy'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avatar of Woe'),
    (select id from sets where short_name = 'pcy'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vitalizing Wind'),
    (select id from sets where short_name = 'pcy'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noxious Field'),
    (select id from sets where short_name = 'pcy'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keldon Arsonist'),
    (select id from sets where short_name = 'pcy'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plague Fiend'),
    (select id from sets where short_name = 'pcy'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirror Strike'),
    (select id from sets where short_name = 'pcy'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Denying Wind'),
    (select id from sets where short_name = 'pcy'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hollow Warrior'),
    (select id from sets where short_name = 'pcy'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Charmer'),
    (select id from sets where short_name = 'pcy'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brutal Suppression'),
    (select id from sets where short_name = 'pcy'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Excavation'),
    (select id from sets where short_name = 'pcy'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Infernal Genesis'),
    (select id from sets where short_name = 'pcy'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barbed Field'),
    (select id from sets where short_name = 'pcy'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fault Riders'),
    (select id from sets where short_name = 'pcy'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shield Dancer'),
    (select id from sets where short_name = 'pcy'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stormwatch Eagle'),
    (select id from sets where short_name = 'pcy'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhystic Lightning'),
    (select id from sets where short_name = 'pcy'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elephant Resurgence'),
    (select id from sets where short_name = 'pcy'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mageta''s Boon'),
    (select id from sets where short_name = 'pcy'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Troublesome Spirit'),
    (select id from sets where short_name = 'pcy'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Task Mage Assembly'),
    (select id from sets where short_name = 'pcy'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heightened Awareness'),
    (select id from sets where short_name = 'pcy'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whip Sergeant'),
    (select id from sets where short_name = 'pcy'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Search for Survivors'),
    (select id from sets where short_name = 'pcy'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spore Frog'),
    (select id from sets where short_name = 'pcy'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhystic Study'),
    (select id from sets where short_name = 'pcy'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abolish'),
    (select id from sets where short_name = 'pcy'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blessed Wind'),
    (select id from sets where short_name = 'pcy'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flameshot'),
    (select id from sets where short_name = 'pcy'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alexi''s Cloak'),
    (select id from sets where short_name = 'pcy'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhystic Shield'),
    (select id from sets where short_name = 'pcy'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhystic Circle'),
    (select id from sets where short_name = 'pcy'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhystic Cave'),
    (select id from sets where short_name = 'pcy'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spiketail Drake'),
    (select id from sets where short_name = 'pcy'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spur Grappler'),
    (select id from sets where short_name = 'pcy'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avatar of Fury'),
    (select id from sets where short_name = 'pcy'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Troubled Healer'),
    (select id from sets where short_name = 'pcy'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steal Strength'),
    (select id from sets where short_name = 'pcy'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zerapa Minotaur'),
    (select id from sets where short_name = 'pcy'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thresher Beast'),
    (select id from sets where short_name = 'pcy'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mageta the Lion'),
    (select id from sets where short_name = 'pcy'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wing Storm'),
    (select id from sets where short_name = 'pcy'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Greel''s Caress'),
    (select id from sets where short_name = 'pcy'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keldon Battlewagon'),
    (select id from sets where short_name = 'pcy'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ridgeline Rager'),
    (select id from sets where short_name = 'pcy'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lesser Gargadon'),
    (select id from sets where short_name = 'pcy'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Terrain'),
    (select id from sets where short_name = 'pcy'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Citadel of Pain'),
    (select id from sets where short_name = 'pcy'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dual Nature'),
    (select id from sets where short_name = 'pcy'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mungha Wurm'),
    (select id from sets where short_name = 'pcy'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alexi, Zephyr Mage'),
    (select id from sets where short_name = 'pcy'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Squirrel Wrangler'),
    (select id from sets where short_name = 'pcy'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scoria Cat'),
    (select id from sets where short_name = 'pcy'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snag'),
    (select id from sets where short_name = 'pcy'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rhystic Syphon'),
    (select id from sets where short_name = 'pcy'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bog Elemental'),
    (select id from sets where short_name = 'pcy'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wild Might'),
    (select id from sets where short_name = 'pcy'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Searing Wind'),
    (select id from sets where short_name = 'pcy'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Copper-Leaf Angel'),
    (select id from sets where short_name = 'pcy'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overburden'),
    (select id from sets where short_name = 'pcy'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avatar of Hope'),
    (select id from sets where short_name = 'pcy'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rhystic Scrying'),
    (select id from sets where short_name = 'pcy'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Well of Discovery'),
    (select id from sets where short_name = 'pcy'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jolrael''s Favor'),
    (select id from sets where short_name = 'pcy'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foil'),
    (select id from sets where short_name = 'pcy'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marsh Boa'),
    (select id from sets where short_name = 'pcy'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vintara Snapper'),
    (select id from sets where short_name = 'pcy'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Panic Attack'),
    (select id from sets where short_name = 'pcy'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Excise'),
    (select id from sets where short_name = 'pcy'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fen Stalker'),
    (select id from sets where short_name = 'pcy'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reveille Squad'),
    (select id from sets where short_name = 'pcy'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Branded Brawlers'),
    (select id from sets where short_name = 'pcy'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rethink'),
    (select id from sets where short_name = 'pcy'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunken Field'),
    (select id from sets where short_name = 'pcy'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mercenary Informer'),
    (select id from sets where short_name = 'pcy'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spiketail Hatchling'),
    (select id from sets where short_name = 'pcy'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trenching Steed'),
    (select id from sets where short_name = 'pcy'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrive'),
    (select id from sets where short_name = 'pcy'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whipstitched Zombie'),
    (select id from sets where short_name = 'pcy'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forgotten Harvest'),
    (select id from sets where short_name = 'pcy'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword Dancer'),
    (select id from sets where short_name = 'pcy'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sheltering Prayers'),
    (select id from sets where short_name = 'pcy'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glittering Lynx'),
    (select id from sets where short_name = 'pcy'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Latulla''s Orders'),
    (select id from sets where short_name = 'pcy'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coastal Hornclaw'),
    (select id from sets where short_name = 'pcy'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quicksilver Wall'),
    (select id from sets where short_name = 'pcy'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flay'),
    (select id from sets where short_name = 'pcy'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mine Bearer'),
    (select id from sets where short_name = 'pcy'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glittering Lion'),
    (select id from sets where short_name = 'pcy'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jolrael, Empress of Beasts'),
    (select id from sets where short_name = 'pcy'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coffin Puppets'),
    (select id from sets where short_name = 'pcy'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jeweled Spirit'),
    (select id from sets where short_name = 'pcy'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plague Wind'),
    (select id from sets where short_name = 'pcy'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Endbringer''s Revel'),
    (select id from sets where short_name = 'pcy'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Windscouter'),
    (select id from sets where short_name = 'pcy'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Well of Life'),
    (select id from sets where short_name = 'pcy'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gulf Squid'),
    (select id from sets where short_name = 'pcy'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avatar of Will'),
    (select id from sets where short_name = 'pcy'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Devastate'),
    (select id from sets where short_name = 'pcy'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chilling Apparition'),
    (select id from sets where short_name = 'pcy'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flowering Field'),
    (select id from sets where short_name = 'pcy'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coffin Puppets'),
    (select id from sets where short_name = 'pcy'),
    '60s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avatar of Might'),
    (select id from sets where short_name = 'pcy'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rebel Informer'),
    (select id from sets where short_name = 'pcy'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fickle Efreet'),
    (select id from sets where short_name = 'pcy'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inflame'),
    (select id from sets where short_name = 'pcy'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Verdant Field'),
    (select id from sets where short_name = 'pcy'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shrouded Serpent'),
    (select id from sets where short_name = 'pcy'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keldon Firebombers'),
    (select id from sets where short_name = 'pcy'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rib Cage Spider'),
    (select id from sets where short_name = 'pcy'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darba'),
    (select id from sets where short_name = 'pcy'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Agent of Shauku'),
    (select id from sets where short_name = 'pcy'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keldon Berserker'),
    (select id from sets where short_name = 'pcy'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Root Cage'),
    (select id from sets where short_name = 'pcy'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aura Fracture'),
    (select id from sets where short_name = 'pcy'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhystic Tutor'),
    (select id from sets where short_name = 'pcy'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Strings'),
    (select id from sets where short_name = 'pcy'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pit Raptor'),
    (select id from sets where short_name = 'pcy'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death Charmer'),
    (select id from sets where short_name = 'pcy'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Celestial Convergence'),
    (select id from sets where short_name = 'pcy'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Greel, Mind Raker'),
    (select id from sets where short_name = 'pcy'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ribbon Snake'),
    (select id from sets where short_name = 'pcy'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Calming Verse'),
    (select id from sets where short_name = 'pcy'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diving Griffin'),
    (select id from sets where short_name = 'pcy'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Entangler'),
    (select id from sets where short_name = 'pcy'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Veteran Brawlers'),
    (select id from sets where short_name = 'pcy'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nakaya Shade'),
    (select id from sets where short_name = 'pcy'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wintermoon Mesa'),
    (select id from sets where short_name = 'pcy'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Withdraw'),
    (select id from sets where short_name = 'pcy'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spitting Spider'),
    (select id from sets where short_name = 'pcy'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silt Crawler'),
    (select id from sets where short_name = 'pcy'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Outbreak'),
    (select id from sets where short_name = 'pcy'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Psychic Theft'),
    (select id from sets where short_name = 'pcy'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hazy Homunculus'),
    (select id from sets where short_name = 'pcy'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vintara Elephant'),
    (select id from sets where short_name = 'pcy'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chimeric Idol'),
    (select id from sets where short_name = 'pcy'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Latulla, Keldon Overseer'),
    (select id from sets where short_name = 'pcy'),
    '95',
    'rare'
) 
 on conflict do nothing;
