insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Lightning Shrieker'),
    (select id from sets where short_name = 'frf'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fierce Invocation'),
    (select id from sets where short_name = 'frf'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hungering Yeti'),
    (select id from sets where short_name = 'frf'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rageform'),
    (select id from sets where short_name = 'frf'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Map the Wastes'),
    (select id from sets where short_name = 'frf'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hooded Assassin'),
    (select id from sets where short_name = 'frf'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Skirmisher'),
    (select id from sets where short_name = 'frf'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wildcall'),
    (select id from sets where short_name = 'frf'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Honor''s Reward'),
    (select id from sets where short_name = 'frf'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alesha, Who Smiles at Death'),
    (select id from sets where short_name = 'frf'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Defiant Ogre'),
    (select id from sets where short_name = 'frf'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Hollow'),
    (select id from sets where short_name = 'frf'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mardu Woe-Reaper'),
    (select id from sets where short_name = 'frf'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winds of Qal Sisma'),
    (select id from sets where short_name = 'frf'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mardu Scout'),
    (select id from sets where short_name = 'frf'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sage''s Reverie'),
    (select id from sets where short_name = 'frf'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Archers of Qarsi'),
    (select id from sets where short_name = 'frf'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dromoka, the Eternal'),
    (select id from sets where short_name = 'frf'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arashin Cleric'),
    (select id from sets where short_name = 'frf'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghastly Conscription'),
    (select id from sets where short_name = 'frf'),
    '70',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Great-Horn Krushok'),
    (select id from sets where short_name = 'frf'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sultai Emissary'),
    (select id from sets where short_name = 'frf'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vaultbreaker'),
    (select id from sets where short_name = 'frf'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'frf'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Boom Keg'),
    (select id from sets where short_name = 'frf'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tasigur''s Cruelty'),
    (select id from sets where short_name = 'frf'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lotus Path Djinn'),
    (select id from sets where short_name = 'frf'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swiftwater Cliffs'),
    (select id from sets where short_name = 'frf'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marang River Prowler'),
    (select id from sets where short_name = 'frf'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mob Rule'),
    (select id from sets where short_name = 'frf'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rite of Undoing'),
    (select id from sets where short_name = 'frf'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'frf'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abzan Beastmaster'),
    (select id from sets where short_name = 'frf'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sultai Skullkeeper'),
    (select id from sets where short_name = 'frf'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enhanced Awareness'),
    (select id from sets where short_name = 'frf'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archfiend of Depravity'),
    (select id from sets where short_name = 'frf'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Citadel Siege'),
    (select id from sets where short_name = 'frf'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'War Flare'),
    (select id from sets where short_name = 'frf'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blossoming Sands'),
    (select id from sets where short_name = 'frf'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grim Contest'),
    (select id from sets where short_name = 'frf'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hewed Stone Retainers'),
    (select id from sets where short_name = 'frf'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sibsig Host'),
    (select id from sets where short_name = 'frf'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orc Sureshot'),
    (select id from sets where short_name = 'frf'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sudden Reclamation'),
    (select id from sets where short_name = 'frf'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jeskai Runemark'),
    (select id from sets where short_name = 'frf'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daghatar the Adamant'),
    (select id from sets where short_name = 'frf'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fruit of the First Tree'),
    (select id from sets where short_name = 'frf'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thornwood Falls'),
    (select id from sets where short_name = 'frf'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warden of the First Tree'),
    (select id from sets where short_name = 'frf'),
    '143',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Flamerush Rider'),
    (select id from sets where short_name = 'frf'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gore Swine'),
    (select id from sets where short_name = 'frf'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mardu Runemark'),
    (select id from sets where short_name = 'frf'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Refocus'),
    (select id from sets where short_name = 'frf'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fascination'),
    (select id from sets where short_name = 'frf'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jeskai Barricade'),
    (select id from sets where short_name = 'frf'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Supplant Form'),
    (select id from sets where short_name = 'frf'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abzan Advantage'),
    (select id from sets where short_name = 'frf'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cached Defenses'),
    (select id from sets where short_name = 'frf'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'frf'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pilgrim of the Fires'),
    (select id from sets where short_name = 'frf'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frontier Mastodon'),
    (select id from sets where short_name = 'frf'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yasova Dragonclaw'),
    (select id from sets where short_name = 'frf'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mastery of the Unseen'),
    (select id from sets where short_name = 'frf'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Humble Defector'),
    (select id from sets where short_name = 'frf'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silumgar, the Drifting Death'),
    (select id from sets where short_name = 'frf'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Renowned Weaponsmith'),
    (select id from sets where short_name = 'frf'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feral Krushok'),
    (select id from sets where short_name = 'frf'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soulflayer'),
    (select id from sets where short_name = 'frf'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyrotechnics'),
    (select id from sets where short_name = 'frf'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'frf'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elite Scaleguard'),
    (select id from sets where short_name = 'frf'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Qarsi High Priest'),
    (select id from sets where short_name = 'frf'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ugin, the Spirit Dragon'),
    (select id from sets where short_name = 'frf'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wandering Champion'),
    (select id from sets where short_name = 'frf'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fearsome Awakening'),
    (select id from sets where short_name = 'frf'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Heelcutter'),
    (select id from sets where short_name = 'frf'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whisk Away'),
    (select id from sets where short_name = 'frf'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whisperer of the Wilds'),
    (select id from sets where short_name = 'frf'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shamanic Revelation'),
    (select id from sets where short_name = 'frf'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temur Sabertooth'),
    (select id from sets where short_name = 'frf'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Valorous Stance'),
    (select id from sets where short_name = 'frf'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'frf'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arashin War Beast'),
    (select id from sets where short_name = 'frf'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whisperwood Elemental'),
    (select id from sets where short_name = 'frf'),
    '145',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ojutai, Soul of Winter'),
    (select id from sets where short_name = 'frf'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tranquil Cove'),
    (select id from sets where short_name = 'frf'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dismal Backwater'),
    (select id from sets where short_name = 'frf'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Slash'),
    (select id from sets where short_name = 'frf'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alesha''s Vanguard'),
    (select id from sets where short_name = 'frf'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Deal'),
    (select id from sets where short_name = 'frf'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diplomacy of the Wastes'),
    (select id from sets where short_name = 'frf'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crucible of the Spirit Dragon'),
    (select id from sets where short_name = 'frf'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rally the Ancestors'),
    (select id from sets where short_name = 'frf'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jeskai Sage'),
    (select id from sets where short_name = 'frf'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grave Strength'),
    (select id from sets where short_name = 'frf'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sandsteppe Mastodon'),
    (select id from sets where short_name = 'frf'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harsh Sustenance'),
    (select id from sets where short_name = 'frf'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragonrage'),
    (select id from sets where short_name = 'frf'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'frf'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Atarka, World Render'),
    (select id from sets where short_name = 'frf'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merciless Executioner'),
    (select id from sets where short_name = 'frf'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battle Brawler'),
    (select id from sets where short_name = 'frf'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Bell Monk'),
    (select id from sets where short_name = 'frf'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ugin''s Construct'),
    (select id from sets where short_name = 'frf'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sage-Eye Avengers'),
    (select id from sets where short_name = 'frf'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Break Through the Line'),
    (select id from sets where short_name = 'frf'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tasigur, the Golden Fang'),
    (select id from sets where short_name = 'frf'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ethereal Ambush'),
    (select id from sets where short_name = 'frf'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scoured Barrens'),
    (select id from sets where short_name = 'frf'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Channel Harm'),
    (select id from sets where short_name = 'frf'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Outpost Siege'),
    (select id from sets where short_name = 'frf'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'frf'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temur Battle Rage'),
    (select id from sets where short_name = 'frf'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'frf'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Return to the Earth'),
    (select id from sets where short_name = 'frf'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kolaghan, the Storm''s Fury'),
    (select id from sets where short_name = 'frf'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sultai Runemark'),
    (select id from sets where short_name = 'frf'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'frf'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightform'),
    (select id from sets where short_name = 'frf'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Douse in Gloom'),
    (select id from sets where short_name = 'frf'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcbond'),
    (select id from sets where short_name = 'frf'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Will of the Naga'),
    (select id from sets where short_name = 'frf'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temporal Trespass'),
    (select id from sets where short_name = 'frf'),
    '55',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Frontier Siege'),
    (select id from sets where short_name = 'frf'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloudform'),
    (select id from sets where short_name = 'frf'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'frf'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Write into Being'),
    (select id from sets where short_name = 'frf'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temur War Shaman'),
    (select id from sets where short_name = 'frf'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Destructor Dragon'),
    (select id from sets where short_name = 'frf'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wind-Scarred Crag'),
    (select id from sets where short_name = 'frf'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brutal Hordechief'),
    (select id from sets where short_name = 'frf'),
    '64',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Monastery Mentor'),
    (select id from sets where short_name = 'frf'),
    '20',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Friendly Fire'),
    (select id from sets where short_name = 'frf'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mistfire Adept'),
    (select id from sets where short_name = 'frf'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smoldering Efreet'),
    (select id from sets where short_name = 'frf'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sandblast'),
    (select id from sets where short_name = 'frf'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bathe in Dragonfire'),
    (select id from sets where short_name = 'frf'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunt the Weak'),
    (select id from sets where short_name = 'frf'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reach of Shadows'),
    (select id from sets where short_name = 'frf'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shu Yun, the Silent Tempest'),
    (select id from sets where short_name = 'frf'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakshasa''s Disdain'),
    (select id from sets where short_name = 'frf'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shaman of the Great Hunt'),
    (select id from sets where short_name = 'frf'),
    '113',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mardu Shadowspear'),
    (select id from sets where short_name = 'frf'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ainok Guide'),
    (select id from sets where short_name = 'frf'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abzan Skycaptain'),
    (select id from sets where short_name = 'frf'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mindscour Dragon'),
    (select id from sets where short_name = 'frf'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gurmag Angler'),
    (select id from sets where short_name = 'frf'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mardu Strike Leader'),
    (select id from sets where short_name = 'frf'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodfell Caves'),
    (select id from sets where short_name = 'frf'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abzan Kin-Guard'),
    (select id from sets where short_name = 'frf'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Noxious Dragon'),
    (select id from sets where short_name = 'frf'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pressure Point'),
    (select id from sets where short_name = 'frf'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Typhoid Rats'),
    (select id from sets where short_name = 'frf'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruthless Instincts'),
    (select id from sets where short_name = 'frf'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wardscale Dragon'),
    (select id from sets where short_name = 'frf'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scroll of the Masters'),
    (select id from sets where short_name = 'frf'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Palace Siege'),
    (select id from sets where short_name = 'frf'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aven Surveyor'),
    (select id from sets where short_name = 'frf'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Torrent Elemental'),
    (select id from sets where short_name = 'frf'),
    '56',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Temur Runemark'),
    (select id from sets where short_name = 'frf'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragonscale General'),
    (select id from sets where short_name = 'frf'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reality Shift'),
    (select id from sets where short_name = 'frf'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancestral Vengeance'),
    (select id from sets where short_name = 'frf'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crux of Fate'),
    (select id from sets where short_name = 'frf'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Battlefront Krushok'),
    (select id from sets where short_name = 'frf'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shockmaw Dragon'),
    (select id from sets where short_name = 'frf'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cunning Strike'),
    (select id from sets where short_name = 'frf'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Neutralizing Blast'),
    (select id from sets where short_name = 'frf'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hero''s Blade'),
    (select id from sets where short_name = 'frf'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodfire Enforcers'),
    (select id from sets where short_name = 'frf'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lotus-Eye Mystics'),
    (select id from sets where short_name = 'frf'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jeskai Infiltrator'),
    (select id from sets where short_name = 'frf'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frost Walker'),
    (select id from sets where short_name = 'frf'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shifting Loyalties'),
    (select id from sets where short_name = 'frf'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Monastery Siege'),
    (select id from sets where short_name = 'frf'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sandsteppe Outcast'),
    (select id from sets where short_name = 'frf'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Formless Nurturing'),
    (select id from sets where short_name = 'frf'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sibsig Muckdraggers'),
    (select id from sets where short_name = 'frf'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rugged Highlands'),
    (select id from sets where short_name = 'frf'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Collateral Damage'),
    (select id from sets where short_name = 'frf'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Summons'),
    (select id from sets where short_name = 'frf'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soulfire Grand Master'),
    (select id from sets where short_name = 'frf'),
    '27',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ambush Krotiq'),
    (select id from sets where short_name = 'frf'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abzan Runemark'),
    (select id from sets where short_name = 'frf'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flamewake Phoenix'),
    (select id from sets where short_name = 'frf'),
    '100',
    'rare'
) 
 on conflict do nothing;
