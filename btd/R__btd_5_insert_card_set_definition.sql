insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Killer Whale'),
    (select id from sets where short_name = 'btd'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smoldering Crater'),
    (select id from sets where short_name = 'btd'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'btd'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloud Elemental'),
    (select id from sets where short_name = 'btd'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blizzard Elemental'),
    (select id from sets where short_name = 'btd'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Polluted Mire'),
    (select id from sets where short_name = 'btd'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tar Pit Warrior'),
    (select id from sets where short_name = 'btd'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Growth'),
    (select id from sets where short_name = 'btd'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Impulse'),
    (select id from sets where short_name = 'btd'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Wurm'),
    (select id from sets where short_name = 'btd'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'btd'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wayward Soul'),
    (select id from sets where short_name = 'btd'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talruum Minotaur'),
    (select id from sets where short_name = 'btd'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slippery Karst'),
    (select id from sets where short_name = 'btd'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crashing Boars'),
    (select id from sets where short_name = 'btd'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = 'btd'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = 'btd'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'btd'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'btd'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woolly Spider'),
    (select id from sets where short_name = 'btd'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sonic Burst'),
    (select id from sets where short_name = 'btd'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = 'btd'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thundering Giant'),
    (select id from sets where short_name = 'btd'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = 'btd'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'btd'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Segmented Wurm'),
    (select id from sets where short_name = 'btd'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scaled Wurm'),
    (select id from sets where short_name = 'btd'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'btd'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'btd'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'btd'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'btd'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'btd'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ball Lightning'),
    (select id from sets where short_name = 'btd'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fog'),
    (select id from sets where short_name = 'btd'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gaseous Form'),
    (select id from sets where short_name = 'btd'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drain Life'),
    (select id from sets where short_name = 'btd'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skittering Horror'),
    (select id from sets where short_name = 'btd'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'btd'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'btd'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leviathan'),
    (select id from sets where short_name = 'btd'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skittering Skirge'),
    (select id from sets where short_name = 'btd'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plated Spider'),
    (select id from sets where short_name = 'btd'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mahamoti Djinn'),
    (select id from sets where short_name = 'btd'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diabolic Edict'),
    (select id from sets where short_name = 'btd'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fog Elemental'),
    (select id from sets where short_name = 'btd'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lava Axe'),
    (select id from sets where short_name = 'btd'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'btd'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feral Shadow'),
    (select id from sets where short_name = 'btd'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'btd'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunderbolt'),
    (select id from sets where short_name = 'btd'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kird Ape'),
    (select id from sets where short_name = 'btd'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fallen Angel'),
    (select id from sets where short_name = 'btd'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snapping Drake'),
    (select id from sets where short_name = 'btd'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Sink'),
    (select id from sets where short_name = 'btd'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raging Goblin'),
    (select id from sets where short_name = 'btd'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'btd'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coercion'),
    (select id from sets where short_name = 'btd'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quirion Elves'),
    (select id from sets where short_name = 'btd'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shambling Strider'),
    (select id from sets where short_name = 'btd'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = 'btd'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Force of Nature'),
    (select id from sets where short_name = 'btd'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'btd'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lowland Giant'),
    (select id from sets where short_name = 'btd'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Svyelunite Temple'),
    (select id from sets where short_name = 'btd'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Erhnam Djinn'),
    (select id from sets where short_name = 'btd'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hulking Cyclops'),
    (select id from sets where short_name = 'btd'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ebon Stronghold'),
    (select id from sets where short_name = 'btd'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hollow Dogs'),
    (select id from sets where short_name = 'btd'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'btd'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viashino Warrior'),
    (select id from sets where short_name = 'btd'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Havenwood Battleground'),
    (select id from sets where short_name = 'btd'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tolarian Winds'),
    (select id from sets where short_name = 'btd'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clockwork Beast'),
    (select id from sets where short_name = 'btd'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balduvian Horde'),
    (select id from sets where short_name = 'btd'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Remote Isle'),
    (select id from sets where short_name = 'btd'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'btd'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Ruins'),
    (select id from sets where short_name = 'btd'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crash of Rhinos'),
    (select id from sets where short_name = 'btd'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diabolic Vision'),
    (select id from sets where short_name = 'btd'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cloud Djinn'),
    (select id from sets where short_name = 'btd'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bone Harvest'),
    (select id from sets where short_name = 'btd'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Crab'),
    (select id from sets where short_name = 'btd'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clockwork Avian'),
    (select id from sets where short_name = 'btd'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodrock Cyclops'),
    (select id from sets where short_name = 'btd'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Stroke'),
    (select id from sets where short_name = 'btd'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'btd'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'btd'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vigilant Drake'),
    (select id from sets where short_name = 'btd'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'btd'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadly Insect'),
    (select id from sets where short_name = 'btd'),
    '53',
    'common'
) 
 on conflict do nothing;
