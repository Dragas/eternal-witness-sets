insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Bone Dragon'),
    (select id from sets where short_name = 'pm19'),
    '88s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'One with the Machine'),
    (select id from sets where short_name = 'pm19'),
    '66s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mistcaller'),
    (select id from sets where short_name = 'pm19'),
    '62s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Djinn of Wishes'),
    (select id from sets where short_name = 'pm19'),
    '52s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Isolate'),
    (select id from sets where short_name = 'pm19'),
    '17s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alpine Moon'),
    (select id from sets where short_name = 'pm19'),
    '128s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Clancaller'),
    (select id from sets where short_name = 'pm19'),
    '179p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prodigious Growth'),
    (select id from sets where short_name = 'pm19'),
    '194s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chaos Wand'),
    (select id from sets where short_name = 'pm19'),
    '228s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chromium, the Mutable'),
    (select id from sets where short_name = 'pm19'),
    '214s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Death Baron'),
    (select id from sets where short_name = 'pm19'),
    '90p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dismissive Pyromancer'),
    (select id from sets where short_name = 'pm19'),
    '136s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana, Untouched by Death'),
    (select id from sets where short_name = 'pm19'),
    '106s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sarkhan, Fireblood'),
    (select id from sets where short_name = 'pm19'),
    '154s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Crucible of Worlds'),
    (select id from sets where short_name = 'pm19'),
    '229s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hungering Hydra'),
    (select id from sets where short_name = 'pm19'),
    '189s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Rejuvenator'),
    (select id from sets where short_name = 'pm19'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fraying Omnipotence'),
    (select id from sets where short_name = 'pm19'),
    '97s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark-Dweller Oracle'),
    (select id from sets where short_name = 'pm19'),
    '134s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Open the Graves'),
    (select id from sets where short_name = 'pm19'),
    '112s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guttersnipe'),
    (select id from sets where short_name = 'pm19'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demanding Dragon'),
    (select id from sets where short_name = 'pm19'),
    '135s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goreclaw, Terror of Qal Sisma'),
    (select id from sets where short_name = 'pm19'),
    '186s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tezzeret, Artifice Master'),
    (select id from sets where short_name = 'pm19'),
    '79s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Remorseful Cleric'),
    (select id from sets where short_name = 'pm19'),
    '33p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leonin Warleader'),
    (select id from sets where short_name = 'pm19'),
    '23p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lathliss, Dragon Queen'),
    (select id from sets where short_name = 'pm19'),
    '149s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mentor of the Meek'),
    (select id from sets where short_name = 'pm19'),
    '27s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sai, Master Thopterist'),
    (select id from sets where short_name = 'pm19'),
    '69p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Trashmaster'),
    (select id from sets where short_name = 'pm19'),
    '144s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desecrated Tomb'),
    (select id from sets where short_name = 'pm19'),
    '230s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cleansing Nova'),
    (select id from sets where short_name = 'pm19'),
    '9p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desecrated Tomb'),
    (select id from sets where short_name = 'pm19'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phylactery Lich'),
    (select id from sets where short_name = 'pm19'),
    '113s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goreclaw, Terror of Qal Sisma'),
    (select id from sets where short_name = 'pm19'),
    '186p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infernal Reckoning'),
    (select id from sets where short_name = 'pm19'),
    '102s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Supreme Phantom'),
    (select id from sets where short_name = 'pm19'),
    '76p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivien''s Invocation'),
    (select id from sets where short_name = 'pm19'),
    '209s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demon of Catastrophes'),
    (select id from sets where short_name = 'pm19'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gigantosaurus'),
    (select id from sets where short_name = 'pm19'),
    '185p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Resplendent Angel'),
    (select id from sets where short_name = 'pm19'),
    '34s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thorn Lieutenant'),
    (select id from sets where short_name = 'pm19'),
    '203p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leonin Warleader'),
    (select id from sets where short_name = 'pm19'),
    '23s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Transmogrifying Wand'),
    (select id from sets where short_name = 'pm19'),
    '247s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lena, Selfless Champion'),
    (select id from sets where short_name = 'pm19'),
    '21s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reliquary Tower'),
    (select id from sets where short_name = 'pm19'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scapeshift'),
    (select id from sets where short_name = 'pm19'),
    '201s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Death Baron'),
    (select id from sets where short_name = 'pm19'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, the Ravager // Nicol Bolas, the Arisen'),
    (select id from sets where short_name = 'pm19'),
    '218s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vivien Reid'),
    (select id from sets where short_name = 'pm19'),
    '208p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Runic Armasaur'),
    (select id from sets where short_name = 'pm19'),
    '200s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pelakka Wurm'),
    (select id from sets where short_name = 'pm19'),
    '192s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Remorseful Cleric'),
    (select id from sets where short_name = 'pm19'),
    '33s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gigantosaurus'),
    (select id from sets where short_name = 'pm19'),
    '185s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon''s Hoard'),
    (select id from sets where short_name = 'pm19'),
    '232s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Supreme Phantom'),
    (select id from sets where short_name = 'pm19'),
    '76s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Murder'),
    (select id from sets where short_name = 'pm19'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Graveyard Marshal'),
    (select id from sets where short_name = 'pm19'),
    '99s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Trashmaster'),
    (select id from sets where short_name = 'pm19'),
    '144p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Resplendent Angel'),
    (select id from sets where short_name = 'pm19'),
    '34p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Demon of Catastrophes'),
    (select id from sets where short_name = 'pm19'),
    '91s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Valiant Knight'),
    (select id from sets where short_name = 'pm19'),
    '42s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Detection Tower'),
    (select id from sets where short_name = 'pm19'),
    '249s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thorn Lieutenant'),
    (select id from sets where short_name = 'pm19'),
    '203s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demanding Dragon'),
    (select id from sets where short_name = 'pm19'),
    '135p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sigiled Sword of Valeron'),
    (select id from sets where short_name = 'pm19'),
    '244s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death Baron'),
    (select id from sets where short_name = 'pm19'),
    '90s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Suncleanser'),
    (select id from sets where short_name = 'pm19'),
    '39s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Runic Armasaur'),
    (select id from sets where short_name = 'pm19'),
    '200p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystic Archaeologist'),
    (select id from sets where short_name = 'pm19'),
    '63s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Palladia-Mors, the Ruiner'),
    (select id from sets where short_name = 'pm19'),
    '219s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Spit Flame'),
    (select id from sets where short_name = 'pm19'),
    '160s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Patient Rebuilding'),
    (select id from sets where short_name = 'pm19'),
    '67s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cleansing Nova'),
    (select id from sets where short_name = 'pm19'),
    '9s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sarkhan''s Unsealing'),
    (select id from sets where short_name = 'pm19'),
    '155s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Clancaller'),
    (select id from sets where short_name = 'pm19'),
    '179s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Open the Graves'),
    (select id from sets where short_name = 'pm19'),
    '112p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Banefire'),
    (select id from sets where short_name = 'pm19'),
    '130p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Last Stand'),
    (select id from sets where short_name = 'pm19'),
    '4s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani, Adversary of Tyrants'),
    (select id from sets where short_name = 'pm19'),
    '3s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Metamorphic Alteration'),
    (select id from sets where short_name = 'pm19'),
    '60s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Amulet of Safekeeping'),
    (select id from sets where short_name = 'pm19'),
    '226s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vaevictis Asmadi, the Dire'),
    (select id from sets where short_name = 'pm19'),
    '225s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Apex of Power'),
    (select id from sets where short_name = 'pm19'),
    '129s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Contract'),
    (select id from sets where short_name = 'pm19'),
    '107s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcades, the Strategist'),
    (select id from sets where short_name = 'pm19'),
    '212s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Banefire'),
    (select id from sets where short_name = 'pm19'),
    '130s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sai, Master Thopterist'),
    (select id from sets where short_name = 'pm19'),
    '69s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magistrate''s Scepter'),
    (select id from sets where short_name = 'pm19'),
    '238s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Isareth the Awakener'),
    (select id from sets where short_name = 'pm19'),
    '104s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Militia Bugler'),
    (select id from sets where short_name = 'pm19'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vivien Reid'),
    (select id from sets where short_name = 'pm19'),
    '208s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Omniscience'),
    (select id from sets where short_name = 'pm19'),
    '65s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Windreader Sphinx'),
    (select id from sets where short_name = 'pm19'),
    '84s',
    'rare'
) 
 on conflict do nothing;
