insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Prismatic Geoscope'),
    (select id from sets where short_name = 'j17'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Homeward Path'),
    (select id from sets where short_name = 'j17'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rules Lawyer'),
    (select id from sets where short_name = 'j17'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pendelhaven'),
    (select id from sets where short_name = 'j17'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaddock Teeg'),
    (select id from sets where short_name = 'j17'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avacyn, Angel of Hope'),
    (select id from sets where short_name = 'j17'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Doran, the Siege Tower'),
    (select id from sets where short_name = 'j17'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Spellskite'),
    (select id from sets where short_name = 'j17'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Capture of Jingzhou'),
    (select id from sets where short_name = 'j17'),
    '2',
    'rare'
) 
 on conflict do nothing;
