    insert into mtgcard(name) values ('Prismatic Geoscope') on conflict do nothing;
    insert into mtgcard(name) values ('Homeward Path') on conflict do nothing;
    insert into mtgcard(name) values ('Rules Lawyer') on conflict do nothing;
    insert into mtgcard(name) values ('Pendelhaven') on conflict do nothing;
    insert into mtgcard(name) values ('Gaddock Teeg') on conflict do nothing;
    insert into mtgcard(name) values ('Avacyn, Angel of Hope') on conflict do nothing;
    insert into mtgcard(name) values ('Doran, the Siege Tower') on conflict do nothing;
    insert into mtgcard(name) values ('Spellskite') on conflict do nothing;
    insert into mtgcard(name) values ('Capture of Jingzhou') on conflict do nothing;
