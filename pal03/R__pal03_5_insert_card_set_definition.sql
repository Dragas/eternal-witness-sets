insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Elvish Aberration'),
    (select id from sets where short_name = 'pal03'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pal03'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skirk Marauder'),
    (select id from sets where short_name = 'pal03'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pal03'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pal03'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pal03'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pal03'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bonesplitter'),
    (select id from sets where short_name = 'pal03'),
    '8',
    'rare'
) 
 on conflict do nothing;
