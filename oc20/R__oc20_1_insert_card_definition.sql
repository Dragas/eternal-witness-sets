    insert into mtgcard(name) values ('Gavi, Nest Warden') on conflict do nothing;
    insert into mtgcard(name) values ('Kathril, Aspect Warper') on conflict do nothing;
    insert into mtgcard(name) values ('Kalamax, the Stormsire') on conflict do nothing;
    insert into mtgcard(name) values ('Otrimi, the Ever-Playful') on conflict do nothing;
    insert into mtgcard(name) values ('Jirina Kudro') on conflict do nothing;
