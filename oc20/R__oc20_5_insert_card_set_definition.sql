insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Gavi, Nest Warden'),
    (select id from sets where short_name = 'oc20'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kathril, Aspect Warper'),
    (select id from sets where short_name = 'oc20'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kalamax, the Stormsire'),
    (select id from sets where short_name = 'oc20'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Otrimi, the Ever-Playful'),
    (select id from sets where short_name = 'oc20'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jirina Kudro'),
    (select id from sets where short_name = 'oc20'),
    '8',
    'mythic'
) 
 on conflict do nothing;
