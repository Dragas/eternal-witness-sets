insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Enigma Drake'),
    (select id from sets where short_name = 'akr'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bone Picker'),
    (select id from sets where short_name = 'akr'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodlust Inciter'),
    (select id from sets where short_name = 'akr'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hour of Revelation'),
    (select id from sets where short_name = 'akr'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wasteland Scorpion'),
    (select id from sets where short_name = 'akr'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beneath the Sands'),
    (select id from sets where short_name = 'akr'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mighty Leap'),
    (select id from sets where short_name = 'akr'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Solemnity'),
    (select id from sets where short_name = 'akr'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prowling Serpopard'),
    (select id from sets where short_name = 'akr'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Festering Mummy'),
    (select id from sets where short_name = 'akr'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blur of Blades'),
    (select id from sets where short_name = 'akr'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dauntless Aven'),
    (select id from sets where short_name = 'akr'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fan Bearer'),
    (select id from sets where short_name = 'akr'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Defiant Greatmaw'),
    (select id from sets where short_name = 'akr'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunset Pyramid'),
    (select id from sets where short_name = 'akr'),
    '280',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rags // Riches'),
    (select id from sets where short_name = 'akr'),
    '252',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pursue Glory'),
    (select id from sets where short_name = 'akr'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Resilient Khenra'),
    (select id from sets where short_name = 'akr'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Countervailing Winds'),
    (select id from sets where short_name = 'akr'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unburden'),
    (select id from sets where short_name = 'akr'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trespasser''s Curse'),
    (select id from sets where short_name = 'akr'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Haze of Pollen'),
    (select id from sets where short_name = 'akr'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crocodile of the Crossing'),
    (select id from sets where short_name = 'akr'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pact of Negation'),
    (select id from sets where short_name = 'akr'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temmet, Vizier of Naktamun'),
    (select id from sets where short_name = 'akr'),
    '266',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spellweaver Eternal'),
    (select id from sets where short_name = 'akr'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Start // Finish'),
    (select id from sets where short_name = 'akr'),
    '264',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forsake the Worldly'),
    (select id from sets where short_name = 'akr'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pouncing Cheetah'),
    (select id from sets where short_name = 'akr'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'akr'),
    '336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'akr'),
    '313',
    'common'
) ,
(
    (select id from mtgcard where name = 'Samut, Voice of Dissent'),
    (select id from sets where short_name = 'akr'),
    '258',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hashep Oasis'),
    (select id from sets where short_name = 'akr'),
    '301',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Khenra Eternal'),
    (select id from sets where short_name = 'akr'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bontu''s Monument'),
    (select id from sets where short_name = 'akr'),
    '269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana, Death''s Majesty'),
    (select id from sets where short_name = 'akr'),
    '111',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Baleful Ammit'),
    (select id from sets where short_name = 'akr'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'akr'),
    '314',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'akr'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx''s Revelation'),
    (select id from sets where short_name = 'akr'),
    '262',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wander in Death'),
    (select id from sets where short_name = 'akr'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, God-Pharaoh'),
    (select id from sets where short_name = 'akr'),
    '247',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Honored Crop-Captain'),
    (select id from sets where short_name = 'akr'),
    '240',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Impeccable Timing'),
    (select id from sets where short_name = 'akr'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Razaketh, the Foulblooded'),
    (select id from sets where short_name = 'akr'),
    '120',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rhonas''s Stalwart'),
    (select id from sets where short_name = 'akr'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Edifice of Authority'),
    (select id from sets where short_name = 'akr'),
    '270',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oashra Cultivator'),
    (select id from sets where short_name = 'akr'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cartouche of Knowledge'),
    (select id from sets where short_name = 'akr'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Khenra Scrapper'),
    (select id from sets where short_name = 'akr'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grind // Dust'),
    (select id from sets where short_name = 'akr'),
    '237',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'akr'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curator of Mysteries'),
    (select id from sets where short_name = 'akr'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Neheb, the Eternal'),
    (select id from sets where short_name = 'akr'),
    '167',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Onward // Victory'),
    (select id from sets where short_name = 'akr'),
    '250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Irrigated Farmland'),
    (select id from sets where short_name = 'akr'),
    '304',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Perilous Vault'),
    (select id from sets where short_name = 'akr'),
    '278',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Scorpion God'),
    (select id from sets where short_name = 'akr'),
    '260',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sunscourge Champion'),
    (select id from sets where short_name = 'akr'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Approach of the Second Sun'),
    (select id from sets where short_name = 'akr'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oketra''s Attendant'),
    (select id from sets where short_name = 'akr'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shefet Dunes'),
    (select id from sets where short_name = 'akr'),
    '329',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Open Fire'),
    (select id from sets where short_name = 'akr'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merciless Javelineer'),
    (select id from sets where short_name = 'akr'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Defeat'),
    (select id from sets where short_name = 'akr'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'akr'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vizier of Remedies'),
    (select id from sets where short_name = 'akr'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sand Strangler'),
    (select id from sets where short_name = 'akr'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doomed Dissenter'),
    (select id from sets where short_name = 'akr'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Forgotten Pharaohs'),
    (select id from sets where short_name = 'akr'),
    '282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hour of Devastation'),
    (select id from sets where short_name = 'akr'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Renewed Faith'),
    (select id from sets where short_name = 'akr'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bontu the Glorified'),
    (select id from sets where short_name = 'akr'),
    '95',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hazoret''s Monument'),
    (select id from sets where short_name = 'akr'),
    '273',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Compulsory Rest'),
    (select id from sets where short_name = 'akr'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Labyrinth Guardian'),
    (select id from sets where short_name = 'akr'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heaven // Earth'),
    (select id from sets where short_name = 'akr'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunscorched Desert'),
    (select id from sets where short_name = 'akr'),
    '331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'akr'),
    '319',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'akr'),
    '312',
    'common'
) ,
(
    (select id from mtgcard where name = 'Djeru''s Resolve'),
    (select id from sets where short_name = 'akr'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'akr'),
    '306',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sandwurm Convergence'),
    (select id from sets where short_name = 'akr'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scarab Feast'),
    (select id from sets where short_name = 'akr'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Essence Scatter'),
    (select id from sets where short_name = 'akr'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trial of Ambition'),
    (select id from sets where short_name = 'akr'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unquenchable Thirst'),
    (select id from sets where short_name = 'akr'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Imminent Doom'),
    (select id from sets where short_name = 'akr'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'akr'),
    '324',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wayward Servant'),
    (select id from sets where short_name = 'akr'),
    '267',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lethal Sting'),
    (select id from sets where short_name = 'akr'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hornet Queen'),
    (select id from sets where short_name = 'akr'),
    '196',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Supernatural Stamina'),
    (select id from sets where short_name = 'akr'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Sanctions'),
    (select id from sets where short_name = 'akr'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pull from Tomorrow'),
    (select id from sets where short_name = 'akr'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Puncturing Blow'),
    (select id from sets where short_name = 'akr'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Floodwaters'),
    (select id from sets where short_name = 'akr'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Destined // Lead'),
    (select id from sets where short_name = 'akr'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stinging Shot'),
    (select id from sets where short_name = 'akr'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Striped Riverwinder'),
    (select id from sets where short_name = 'akr'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'God-Pharaoh''s Gift'),
    (select id from sets where short_name = 'akr'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pathmaker Initiate'),
    (select id from sets where short_name = 'akr'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nimble-Blade Khenra'),
    (select id from sets where short_name = 'akr'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thorned Moloch'),
    (select id from sets where short_name = 'akr'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riddleform'),
    (select id from sets where short_name = 'akr'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reason // Believe'),
    (select id from sets where short_name = 'akr'),
    '253',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Doomfall'),
    (select id from sets where short_name = 'akr'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winds of Rebuke'),
    (select id from sets where short_name = 'akr'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Those Who Serve'),
    (select id from sets where short_name = 'akr'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horror of the Broken Lands'),
    (select id from sets where short_name = 'akr'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Final Reward'),
    (select id from sets where short_name = 'akr'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pride Sovereign'),
    (select id from sets where short_name = 'akr'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rest in Peace'),
    (select id from sets where short_name = 'akr'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deem Worthy'),
    (select id from sets where short_name = 'akr'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aven Mindcensor'),
    (select id from sets where short_name = 'akr'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sacred Cat'),
    (select id from sets where short_name = 'akr'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drake Haven'),
    (select id from sets where short_name = 'akr'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'akr'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Never // Return'),
    (select id from sets where short_name = 'akr'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slither Blade'),
    (select id from sets where short_name = 'akr'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Claim // Fame'),
    (select id from sets where short_name = 'akr'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wasp of the Bitter End'),
    (select id from sets where short_name = 'akr'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shadowstorm Vizier'),
    (select id from sets where short_name = 'akr'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shefet Monitor'),
    (select id from sets where short_name = 'akr'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unesh, Criosphinx Sovereign'),
    (select id from sets where short_name = 'akr'),
    '85',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Harsh Mentor'),
    (select id from sets where short_name = 'akr'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Splendid Agony'),
    (select id from sets where short_name = 'akr'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naga Vitalist'),
    (select id from sets where short_name = 'akr'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shed Weakness'),
    (select id from sets where short_name = 'akr'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'akr'),
    '334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bitterbow Sharpshooters'),
    (select id from sets where short_name = 'akr'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dune Beetle'),
    (select id from sets where short_name = 'akr'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Majestic Myriarch'),
    (select id from sets where short_name = 'akr'),
    '200',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Canyon Slough'),
    (select id from sets where short_name = 'akr'),
    '284',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crash Through'),
    (select id from sets where short_name = 'akr'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brute Strength'),
    (select id from sets where short_name = 'akr'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhonas the Indomitable'),
    (select id from sets where short_name = 'akr'),
    '213',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nimble Obstructionist'),
    (select id from sets where short_name = 'akr'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Initiate''s Companion'),
    (select id from sets where short_name = 'akr'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trial of Zeal'),
    (select id from sets where short_name = 'akr'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desert of the Fervent'),
    (select id from sets where short_name = 'akr'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sidewinder Naga'),
    (select id from sets where short_name = 'akr'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unconventional Tactics'),
    (select id from sets where short_name = 'akr'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overwhelming Splendor'),
    (select id from sets where short_name = 'akr'),
    '30',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Greater Sandwurm'),
    (select id from sets where short_name = 'akr'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kefnet the Mindful'),
    (select id from sets where short_name = 'akr'),
    '66',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ruthless Sniper'),
    (select id from sets where short_name = 'akr'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reduce // Rubble'),
    (select id from sets where short_name = 'akr'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Strategic Planning'),
    (select id from sets where short_name = 'akr'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trial of Knowledge'),
    (select id from sets where short_name = 'akr'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'akr'),
    '333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vile Manifestation'),
    (select id from sets where short_name = 'akr'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ahn-Crop Champion'),
    (select id from sets where short_name = 'akr'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Mastery'),
    (select id from sets where short_name = 'akr'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abrade'),
    (select id from sets where short_name = 'akr'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'akr'),
    '337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oasis Ritualist'),
    (select id from sets where short_name = 'akr'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thoughtseize'),
    (select id from sets where short_name = 'akr'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desert''s Hold'),
    (select id from sets where short_name = 'akr'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Scarab God'),
    (select id from sets where short_name = 'akr'),
    '259',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Glorybringer'),
    (select id from sets where short_name = 'akr'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lay Claim'),
    (select id from sets where short_name = 'akr'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Supply Caravan'),
    (select id from sets where short_name = 'akr'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magma Spray'),
    (select id from sets where short_name = 'akr'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crested Sunmare'),
    (select id from sets where short_name = 'akr'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'By Force'),
    (select id from sets where short_name = 'akr'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cut // Ribbons'),
    (select id from sets where short_name = 'akr'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glory-Bound Initiate'),
    (select id from sets where short_name = 'akr'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vizier of the Anointed'),
    (select id from sets where short_name = 'akr'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aerial Guide'),
    (select id from sets where short_name = 'akr'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scattered Groves'),
    (select id from sets where short_name = 'akr'),
    '327',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ornery Kudu'),
    (select id from sets where short_name = 'akr'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abandoned Sarcophagus'),
    (select id from sets where short_name = 'akr'),
    '268',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jace, Unraveler of Secrets'),
    (select id from sets where short_name = 'akr'),
    '65',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'akr'),
    '335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'akr'),
    '307',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dusk // Dawn'),
    (select id from sets where short_name = 'akr'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tah-Crop Elite'),
    (select id from sets where short_name = 'akr'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shadow of the Grave'),
    (select id from sets where short_name = 'akr'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Struggle // Survive'),
    (select id from sets where short_name = 'akr'),
    '265',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sweltering Suns'),
    (select id from sets where short_name = 'akr'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dispossess'),
    (select id from sets where short_name = 'akr'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Watchful Naga'),
    (select id from sets where short_name = 'akr'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Naga Oracle'),
    (select id from sets where short_name = 'akr'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quarry Hauler'),
    (select id from sets where short_name = 'akr'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cartouche of Zeal'),
    (select id from sets where short_name = 'akr'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soulstinger'),
    (select id from sets where short_name = 'akr'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Wind Guide'),
    (select id from sets where short_name = 'akr'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nissa, Steward of Elements'),
    (select id from sets where short_name = 'akr'),
    '248',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Anger of the Gods'),
    (select id from sets where short_name = 'akr'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prepare // Fight'),
    (select id from sets where short_name = 'akr'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Farm // Market'),
    (select id from sets where short_name = 'akr'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hooded Brawler'),
    (select id from sets where short_name = 'akr'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thresher Lizard'),
    (select id from sets where short_name = 'akr'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feral Prowler'),
    (select id from sets where short_name = 'akr'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trial of Solidarity'),
    (select id from sets where short_name = 'akr'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Throne of the God-Pharaoh'),
    (select id from sets where short_name = 'akr'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hope Tender'),
    (select id from sets where short_name = 'akr'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'New Perspectives'),
    (select id from sets where short_name = 'akr'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Watchers of the Dead'),
    (select id from sets where short_name = 'akr'),
    '283',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Locust God'),
    (select id from sets where short_name = 'akr'),
    '243',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Synchronized Strike'),
    (select id from sets where short_name = 'akr'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anointed Procession'),
    (select id from sets where short_name = 'akr'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marauding Boneslasher'),
    (select id from sets where short_name = 'akr'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leave // Chance'),
    (select id from sets where short_name = 'akr'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'akr'),
    '321',
    'common'
) ,
(
    (select id from mtgcard where name = 'Compelling Argument'),
    (select id from sets where short_name = 'akr'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cryptic Serpent'),
    (select id from sets where short_name = 'akr'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oketra''s Avenger'),
    (select id from sets where short_name = 'akr'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steward of Solidarity'),
    (select id from sets where short_name = 'akr'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hazoret the Fervent'),
    (select id from sets where short_name = 'akr'),
    '159',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lord of the Accursed'),
    (select id from sets where short_name = 'akr'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seer of the Last Tomorrow'),
    (select id from sets where short_name = 'akr'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'akr'),
    '317',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manglehorn'),
    (select id from sets where short_name = 'akr'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'akr'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kefnet''s Monument'),
    (select id from sets where short_name = 'akr'),
    '275',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Supreme Will'),
    (select id from sets where short_name = 'akr'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hieroglyphic Illumination'),
    (select id from sets where short_name = 'akr'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'akr'),
    '316',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burning-Fist Minotaur'),
    (select id from sets where short_name = 'akr'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'akr'),
    '310',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eternal of Harsh Truths'),
    (select id from sets where short_name = 'akr'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'In Oketra''s Name'),
    (select id from sets where short_name = 'akr'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ipnu Rivulet'),
    (select id from sets where short_name = 'akr'),
    '303',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glorious End'),
    (select id from sets where short_name = 'akr'),
    '156',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hour of Promise'),
    (select id from sets where short_name = 'akr'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hekma Sentinels'),
    (select id from sets where short_name = 'akr'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Khenra Charioteer'),
    (select id from sets where short_name = 'akr'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firebrand Archer'),
    (select id from sets where short_name = 'akr'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vizier of Deferment'),
    (select id from sets where short_name = 'akr'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'akr'),
    '309',
    'common'
) ,
(
    (select id from mtgcard where name = 'Combat Celebrant'),
    (select id from sets where short_name = 'akr'),
    '148',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aven of Enduring Hope'),
    (select id from sets where short_name = 'akr'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gideon of the Trials'),
    (select id from sets where short_name = 'akr'),
    '19',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sheltered Thicket'),
    (select id from sets where short_name = 'akr'),
    '330',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Collected Company'),
    (select id from sets where short_name = 'akr'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exemplar of Strength'),
    (select id from sets where short_name = 'akr'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'akr'),
    '320',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'akr'),
    '308',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fervent Paincaster'),
    (select id from sets where short_name = 'akr'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dissenter''s Deliverance'),
    (select id from sets where short_name = 'akr'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'akr'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirage Mirror'),
    (select id from sets where short_name = 'akr'),
    '276',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gate to the Afterlife'),
    (select id from sets where short_name = 'akr'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bontu''s Last Reckoning'),
    (select id from sets where short_name = 'akr'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonic Pact'),
    (select id from sets where short_name = 'akr'),
    '99',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cartouche of Strength'),
    (select id from sets where short_name = 'akr'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desert of the Mindful'),
    (select id from sets where short_name = 'akr'),
    '290',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'akr'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hapatra, Vizier of Poisons'),
    (select id from sets where short_name = 'akr'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disposal Mummy'),
    (select id from sets where short_name = 'akr'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Torment of Hailfire'),
    (select id from sets where short_name = 'akr'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Champion of Wits'),
    (select id from sets where short_name = 'akr'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'As Foretold'),
    (select id from sets where short_name = 'akr'),
    '49',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tormenting Voice'),
    (select id from sets where short_name = 'akr'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'akr'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gilded Cerodon'),
    (select id from sets where short_name = 'akr'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obelisk Spider'),
    (select id from sets where short_name = 'akr'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Neheb, the Worthy'),
    (select id from sets where short_name = 'akr'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Miasmic Mummy'),
    (select id from sets where short_name = 'akr'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Champion of Rhonas'),
    (select id from sets where short_name = 'akr'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'akr'),
    '322',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insult // Injury'),
    (select id from sets where short_name = 'akr'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cartouche of Solidarity'),
    (select id from sets where short_name = 'akr'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sixth Sense'),
    (select id from sets where short_name = 'akr'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spring // Mind'),
    (select id from sets where short_name = 'akr'),
    '263',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anointer Priest'),
    (select id from sets where short_name = 'akr'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cartouche of Ambition'),
    (select id from sets where short_name = 'akr'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sifter Wurm'),
    (select id from sets where short_name = 'akr'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Regal Caracal'),
    (select id from sets where short_name = 'akr'),
    '339',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ifnir Deadlands'),
    (select id from sets where short_name = 'akr'),
    '302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battlefield Scavenger'),
    (select id from sets where short_name = 'akr'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pitiless Vizier'),
    (select id from sets where short_name = 'akr'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Samut, the Tested'),
    (select id from sets where short_name = 'akr'),
    '257',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nef-Crop Entangler'),
    (select id from sets where short_name = 'akr'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'akr'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Earthshaker Khenra'),
    (select id from sets where short_name = 'akr'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul-Scar Mage'),
    (select id from sets where short_name = 'akr'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'akr'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Protection of the Hekma'),
    (select id from sets where short_name = 'akr'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Archfiend of Ifnir'),
    (select id from sets where short_name = 'akr'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crypt of the Eternals'),
    (select id from sets where short_name = 'akr'),
    '286',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seeker of Insight'),
    (select id from sets where short_name = 'akr'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shimmerscale Drake'),
    (select id from sets where short_name = 'akr'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'akr'),
    '332',
    'common'
) ,
(
    (select id from mtgcard where name = 'River Hoopoe'),
    (select id from sets where short_name = 'akr'),
    '256',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magmaroth'),
    (select id from sets where short_name = 'akr'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nest of Scarabs'),
    (select id from sets where short_name = 'akr'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vizier of Tumbling Sands'),
    (select id from sets where short_name = 'akr'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Commit // Memory'),
    (select id from sets where short_name = 'akr'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zealot of the God-Pharaoh'),
    (select id from sets where short_name = 'akr'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancient Crab'),
    (select id from sets where short_name = 'akr'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blighted Bat'),
    (select id from sets where short_name = 'akr'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ominous Sphinx'),
    (select id from sets where short_name = 'akr'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Consign // Oblivion'),
    (select id from sets where short_name = 'akr'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rhonas''s Monument'),
    (select id from sets where short_name = 'akr'),
    '279',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scavenger Grounds'),
    (select id from sets where short_name = 'akr'),
    '328',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'akr'),
    '318',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cascading Cataracts'),
    (select id from sets where short_name = 'akr'),
    '285',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vizier of the Menagerie'),
    (select id from sets where short_name = 'akr'),
    '224',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vizier of Many Faces'),
    (select id from sets where short_name = 'akr'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desert of the Glorified'),
    (select id from sets where short_name = 'akr'),
    '288',
    'common'
) ,
(
    (select id from mtgcard where name = 'Failure // Comply'),
    (select id from sets where short_name = 'akr'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'akr'),
    '338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dread Wanderer'),
    (select id from sets where short_name = 'akr'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desert of the True'),
    (select id from sets where short_name = 'akr'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oketra the True'),
    (select id from sets where short_name = 'akr'),
    '27',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Appeal // Authority'),
    (select id from sets where short_name = 'akr'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Life Goes On'),
    (select id from sets where short_name = 'akr'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Refuse // Cooperate'),
    (select id from sets where short_name = 'akr'),
    '255',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aven Initiate'),
    (select id from sets where short_name = 'akr'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cast Out'),
    (select id from sets where short_name = 'akr'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lord of Extinction'),
    (select id from sets where short_name = 'akr'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cruel Reality'),
    (select id from sets where short_name = 'akr'),
    '98',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ramunap Ruins'),
    (select id from sets where short_name = 'akr'),
    '326',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shatterstorm'),
    (select id from sets where short_name = 'akr'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra, Pyromaster'),
    (select id from sets where short_name = 'akr'),
    '146',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Desert Cerodon'),
    (select id from sets where short_name = 'akr'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'akr'),
    '325',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gideon''s Intervention'),
    (select id from sets where short_name = 'akr'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fetid Pools'),
    (select id from sets where short_name = 'akr'),
    '293',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'akr'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Defeat'),
    (select id from sets where short_name = 'akr'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Censor'),
    (select id from sets where short_name = 'akr'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desert of the Indomitable'),
    (select id from sets where short_name = 'akr'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mouth // Feed'),
    (select id from sets where short_name = 'akr'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'akr'),
    '315',
    'common'
) ,
(
    (select id from mtgcard where name = 'Driven // Despair'),
    (select id from sets where short_name = 'akr'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oketra''s Monument'),
    (select id from sets where short_name = 'akr'),
    '277',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Solitary Camel'),
    (select id from sets where short_name = 'akr'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ahn-Crop Crasher'),
    (select id from sets where short_name = 'akr'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'akr'),
    '311',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trial of Strength'),
    (select id from sets where short_name = 'akr'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Binding Mummy'),
    (select id from sets where short_name = 'akr'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gust Walker'),
    (select id from sets where short_name = 'akr'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ramunap Excavator'),
    (select id from sets where short_name = 'akr'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'akr'),
    '323',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hollow One'),
    (select id from sets where short_name = 'akr'),
    '274',
    'rare'
) 
 on conflict do nothing;
