insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Aswan Jaguar'),
        (select types.id from types where name = 'Summon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aswan Jaguar'),
        (select types.id from types where name = 'Jaguar')
    ) 
 on conflict do nothing;
