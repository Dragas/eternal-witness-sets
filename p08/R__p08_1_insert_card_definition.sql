    insert into mtgcard(name) values ('Ponder') on conflict do nothing;
    insert into mtgcard(name) values ('Corrupt') on conflict do nothing;
    insert into mtgcard(name) values ('Harmonize') on conflict do nothing;
    insert into mtgcard(name) values ('Mana Tithe') on conflict do nothing;
    insert into mtgcard(name) values ('Tidings') on conflict do nothing;
    insert into mtgcard(name) values ('Damnation') on conflict do nothing;
    insert into mtgcard(name) values ('Incinerate') on conflict do nothing;
