insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ponder'),
    (select id from sets where short_name = 'p08'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corrupt'),
    (select id from sets where short_name = 'p08'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harmonize'),
    (select id from sets where short_name = 'p08'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Tithe'),
    (select id from sets where short_name = 'p08'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tidings'),
    (select id from sets where short_name = 'p08'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Damnation'),
    (select id from sets where short_name = 'p08'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = 'p08'),
    '3',
    'rare'
) 
 on conflict do nothing;
