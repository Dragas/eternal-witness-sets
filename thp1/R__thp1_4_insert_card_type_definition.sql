insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'The Hunter'),
        (select types.id from types where name = 'Hero')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Slayer'),
        (select types.id from types where name = 'Hero')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Harvester'),
        (select types.id from types where name = 'Hero')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Protector'),
        (select types.id from types where name = 'Hero')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Avenger'),
        (select types.id from types where name = 'Hero')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Warrior'),
        (select types.id from types where name = 'Hero')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Philosopher'),
        (select types.id from types where name = 'Hero')
    ) 
 on conflict do nothing;
