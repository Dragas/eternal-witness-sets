    insert into mtgcard(name) values ('The Hunter') on conflict do nothing;
    insert into mtgcard(name) values ('The Slayer') on conflict do nothing;
    insert into mtgcard(name) values ('The Harvester') on conflict do nothing;
    insert into mtgcard(name) values ('The Protector') on conflict do nothing;
    insert into mtgcard(name) values ('The Avenger') on conflict do nothing;
    insert into mtgcard(name) values ('The Warrior') on conflict do nothing;
    insert into mtgcard(name) values ('The Philosopher') on conflict do nothing;
