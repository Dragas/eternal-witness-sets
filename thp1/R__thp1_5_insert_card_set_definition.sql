insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'The Hunter'),
    (select id from sets where short_name = 'thp1'),
    '1e',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Slayer'),
    (select id from sets where short_name = 'thp1'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Harvester'),
    (select id from sets where short_name = 'thp1'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Protector'),
    (select id from sets where short_name = 'thp1'),
    '1a',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Avenger'),
    (select id from sets where short_name = 'thp1'),
    '1c',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Warrior'),
    (select id from sets where short_name = 'thp1'),
    '1d',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Philosopher'),
    (select id from sets where short_name = 'thp1'),
    '1b',
    'common'
) 
 on conflict do nothing;
