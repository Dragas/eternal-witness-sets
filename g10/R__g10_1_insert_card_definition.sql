    insert into mtgcard(name) values ('Thawing Glaciers') on conflict do nothing;
    insert into mtgcard(name) values ('Phyrexian Dreadnought') on conflict do nothing;
    insert into mtgcard(name) values ('Wasteland') on conflict do nothing;
    insert into mtgcard(name) values ('Natural Order') on conflict do nothing;
    insert into mtgcard(name) values ('Wheel of Fortune') on conflict do nothing;
    insert into mtgcard(name) values ('Land Tax') on conflict do nothing;
    insert into mtgcard(name) values ('Morphling') on conflict do nothing;
    insert into mtgcard(name) values ('Sinkhole') on conflict do nothing;
