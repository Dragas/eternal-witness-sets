insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Thawing Glaciers'),
    (select id from sets where short_name = 'g10'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Dreadnought'),
    (select id from sets where short_name = 'g10'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wasteland'),
    (select id from sets where short_name = 'g10'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Natural Order'),
    (select id from sets where short_name = 'g10'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wheel of Fortune'),
    (select id from sets where short_name = 'g10'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Land Tax'),
    (select id from sets where short_name = 'g10'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Morphling'),
    (select id from sets where short_name = 'g10'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sinkhole'),
    (select id from sets where short_name = 'g10'),
    '1',
    'rare'
) 
 on conflict do nothing;
