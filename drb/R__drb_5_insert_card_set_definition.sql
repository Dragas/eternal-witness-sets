insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ebon Dragon'),
    (select id from sets where short_name = 'drb'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonstorm'),
    (select id from sets where short_name = 'drb'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Form of the Dragon'),
    (select id from sets where short_name = 'drb'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = 'drb'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hellkite Overlord'),
    (select id from sets where short_name = 'drb'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rith, the Awakener'),
    (select id from sets where short_name = 'drb'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Draco'),
    (select id from sets where short_name = 'drb'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas'),
    (select id from sets where short_name = 'drb'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Whelp'),
    (select id from sets where short_name = 'drb'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Two-Headed Dragon'),
    (select id from sets where short_name = 'drb'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thunder Dragon'),
    (select id from sets where short_name = 'drb'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bogardan Hellkite'),
    (select id from sets where short_name = 'drb'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Niv-Mizzet, the Firemind'),
    (select id from sets where short_name = 'drb'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kokusho, the Evening Star'),
    (select id from sets where short_name = 'drb'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bladewing the Risen'),
    (select id from sets where short_name = 'drb'),
    '1',
    'rare'
) 
 on conflict do nothing;
