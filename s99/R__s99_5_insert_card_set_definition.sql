insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Wind Drake'),
    (select id from sets where short_name = 's99'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nature''s Lore'),
    (select id from sets where short_name = 's99'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Griffin'),
    (select id from sets where short_name = 's99'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Remove Soul'),
    (select id from sets where short_name = 's99'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 's99'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charging Paladin'),
    (select id from sets where short_name = 's99'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Yeti'),
    (select id from sets where short_name = 's99'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scathe Zombies'),
    (select id from sets where short_name = 's99'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 's99'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abyssal Horror'),
    (select id from sets where short_name = 's99'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Royal Falcon'),
    (select id from sets where short_name = 's99'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Tempest'),
    (select id from sets where short_name = 's99'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Undo'),
    (select id from sets where short_name = 's99'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel of Mercy'),
    (select id from sets where short_name = 's99'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Foot Soldiers'),
    (select id from sets where short_name = 's99'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Feast'),
    (select id from sets where short_name = 's99'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hand of Death'),
    (select id from sets where short_name = 's99'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'False Peace'),
    (select id from sets where short_name = 's99'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sea Eagle'),
    (select id from sets where short_name = 's99'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lynx'),
    (select id from sets where short_name = 's99'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Royal Trooper'),
    (select id from sets where short_name = 's99'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bog Wraith'),
    (select id from sets where short_name = 's99'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = 's99'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 's99'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vizzerdrix'),
    (select id from sets where short_name = 's99'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Champion Lancer'),
    (select id from sets where short_name = 's99'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archangel'),
    (select id from sets where short_name = 's99'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Offering'),
    (select id from sets where short_name = 's99'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alluring Scent'),
    (select id from sets where short_name = 's99'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coercion'),
    (select id from sets where short_name = 's99'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 's99'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk of the Pearl Trident'),
    (select id from sets where short_name = 's99'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Piracy'),
    (select id from sets where short_name = 's99'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ogre Warrior'),
    (select id from sets where short_name = 's99'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Last Chance'),
    (select id from sets where short_name = 's99'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vengeance'),
    (select id from sets where short_name = 's99'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Durkwood Boars'),
    (select id from sets where short_name = 's99'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wind Sail'),
    (select id from sets where short_name = 's99'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scorching Spear'),
    (select id from sets where short_name = 's99'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feral Shadow'),
    (select id from sets where short_name = 's99'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trained Orgg'),
    (select id from sets where short_name = 's99'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tremor'),
    (select id from sets where short_name = 's99'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Summer Bloom'),
    (select id from sets where short_name = 's99'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Border Guard'),
    (select id from sets where short_name = 's99'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bargain'),
    (select id from sets where short_name = 's99'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 's99'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thorn Elemental'),
    (select id from sets where short_name = 's99'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blinding Light'),
    (select id from sets where short_name = 's99'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Earth Elemental'),
    (select id from sets where short_name = 's99'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 's99'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Storm Crow'),
    (select id from sets where short_name = 's99'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Elemental'),
    (select id from sets where short_name = 's99'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Norwood Archers'),
    (select id from sets where short_name = 's99'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Monstrous Growth'),
    (select id from sets where short_name = 's99'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jagged Lightning'),
    (select id from sets where short_name = 's99'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Venerable Monk'),
    (select id from sets where short_name = 's99'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Octopus'),
    (select id from sets where short_name = 's99'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exhaustion'),
    (select id from sets where short_name = 's99'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Glider'),
    (select id from sets where short_name = 's99'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sacred Nectar'),
    (select id from sets where short_name = 's99'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Breath of Life'),
    (select id from sets where short_name = 's99'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dakmor Ghoul'),
    (select id from sets where short_name = 's99'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moon Sprite'),
    (select id from sets where short_name = 's99'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lava Axe'),
    (select id from sets where short_name = 's99'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 's99'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Imp'),
    (select id from sets where short_name = 's99'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Untamed Wilds'),
    (select id from sets where short_name = 's99'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 's99'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squall'),
    (select id from sets where short_name = 's99'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Basilisk'),
    (select id from sets where short_name = 's99'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Mountaineer'),
    (select id from sets where short_name = 's99'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whirlwind'),
    (select id from sets where short_name = 's99'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Veteran Cavalier'),
    (select id from sets where short_name = 's99'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Renewing Touch'),
    (select id from sets where short_name = 's99'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dakmor Scorpion'),
    (select id from sets where short_name = 's99'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravenous Rats'),
    (select id from sets where short_name = 's99'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bull Hippo'),
    (select id from sets where short_name = 's99'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eye Spy'),
    (select id from sets where short_name = 's99'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel of Light'),
    (select id from sets where short_name = 's99'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Settler'),
    (select id from sets where short_name = 's99'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Southern Elephant'),
    (select id from sets where short_name = 's99'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hulking Ogre'),
    (select id from sets where short_name = 's99'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Natural Spring'),
    (select id from sets where short_name = 's99'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thunder Dragon'),
    (select id from sets where short_name = 's99'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tidings'),
    (select id from sets where short_name = 's99'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantom Warrior'),
    (select id from sets where short_name = 's99'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grizzly Bears'),
    (select id from sets where short_name = 's99'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Extinguish'),
    (select id from sets where short_name = 's99'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shrieking Specter'),
    (select id from sets where short_name = 's99'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 's99'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serpent Warrior'),
    (select id from sets where short_name = 's99'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Owl Familiar'),
    (select id from sets where short_name = 's99'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nature''s Cloak'),
    (select id from sets where short_name = 's99'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 's99'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relentless Assault'),
    (select id from sets where short_name = 's99'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Dragon'),
    (select id from sets where short_name = 's99'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Righteous Fury'),
    (select id from sets where short_name = 's99'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Water Elemental'),
    (select id from sets where short_name = 's99'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Denizen of the Deep'),
    (select id from sets where short_name = 's99'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mons''s Goblin Raiders'),
    (select id from sets where short_name = 's99'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ransack'),
    (select id from sets where short_name = 's99'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ingenious Thief'),
    (select id from sets where short_name = 's99'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 's99'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 's99'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Chariot'),
    (select id from sets where short_name = 's99'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dakmor Lancer'),
    (select id from sets where short_name = 's99'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cinder Storm'),
    (select id from sets where short_name = 's99'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 's99'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 's99'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancient Craving'),
    (select id from sets where short_name = 's99'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raging Goblin'),
    (select id from sets where short_name = 's99'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spitting Earth'),
    (select id from sets where short_name = 's99'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 's99'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Man-o''-War'),
    (select id from sets where short_name = 's99'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight Errant'),
    (select id from sets where short_name = 's99'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychic Transfer'),
    (select id from sets where short_name = 's99'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Devastation'),
    (select id from sets where short_name = 's99'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whiptail Wurm'),
    (select id from sets where short_name = 's99'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin General'),
    (select id from sets where short_name = 's99'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 's99'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ardent Militia'),
    (select id from sets where short_name = 's99'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gerrard''s Wisdom'),
    (select id from sets where short_name = 's99'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snapping Drake'),
    (select id from sets where short_name = 's99'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = 's99'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hulking Goblin'),
    (select id from sets where short_name = 's99'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Lore'),
    (select id from sets where short_name = 's99'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Howling Fury'),
    (select id from sets where short_name = 's99'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Willow Elf'),
    (select id from sets where short_name = 's99'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 's99'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dakmor Plague'),
    (select id from sets where short_name = 's99'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lone Wolf'),
    (select id from sets where short_name = 's99'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sleight of Hand'),
    (select id from sets where short_name = 's99'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Touch of Brilliance'),
    (select id from sets where short_name = 's99'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = 's99'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coral Eel'),
    (select id from sets where short_name = 's99'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 's99'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Devoted Hero'),
    (select id from sets where short_name = 's99'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steadfastness'),
    (select id from sets where short_name = 's99'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Hero'),
    (select id from sets where short_name = 's99'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Cavaliers'),
    (select id from sets where short_name = 's99'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relearn'),
    (select id from sets where short_name = 's99'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 's99'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Blessing'),
    (select id from sets where short_name = 's99'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time Warp'),
    (select id from sets where short_name = 's99'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Devout Monk'),
    (select id from sets where short_name = 's99'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Muck Rats'),
    (select id from sets where short_name = 's99'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Hammer'),
    (select id from sets where short_name = 's99'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dread Reaper'),
    (select id from sets where short_name = 's99'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eager Cadet'),
    (select id from sets where short_name = 's99'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 's99'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gorilla Warrior'),
    (select id from sets where short_name = 's99'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Raiders'),
    (select id from sets where short_name = 's99'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Path of Peace'),
    (select id from sets where short_name = 's99'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silverback Ape'),
    (select id from sets where short_name = 's99'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Commando'),
    (select id from sets where short_name = 's99'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = 's99'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stream of Acid'),
    (select id from sets where short_name = 's99'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 's99'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loyal Sentry'),
    (select id from sets where short_name = 's99'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barbtooth Wurm'),
    (select id from sets where short_name = 's99'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dakmor Sorceress'),
    (select id from sets where short_name = 's99'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hollow Dogs'),
    (select id from sets where short_name = 's99'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chorus of Woe'),
    (select id from sets where short_name = 's99'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wood Elves'),
    (select id from sets where short_name = 's99'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = 's99'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Righteous Charge'),
    (select id from sets where short_name = 's99'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Ox'),
    (select id from sets where short_name = 's99'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pride of Lions'),
    (select id from sets where short_name = 's99'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 's99'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time Ebb'),
    (select id from sets where short_name = 's99'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wicked Pact'),
    (select id from sets where short_name = 's99'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grim Tutor'),
    (select id from sets where short_name = 's99'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Norwood Ranger'),
    (select id from sets where short_name = 's99'),
    '138',
    'common'
) 
 on conflict do nothing;
