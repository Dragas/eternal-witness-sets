insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Rhino'),
    (select id from sets where short_name = 'tmh1'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spider'),
    (select id from sets where short_name = 'tmh1'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elephant'),
    (select id from sets where short_name = 'tmh1'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr'),
    (select id from sets where short_name = 'tmh1'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird'),
    (select id from sets where short_name = 'tmh1'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrenn and Six Emblem'),
    (select id from sets where short_name = 'tmh1'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marit Lage'),
    (select id from sets where short_name = 'tmh1'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squirrel'),
    (select id from sets where short_name = 'tmh1'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Construct'),
    (select id from sets where short_name = 'tmh1'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra the Benevolent Emblem'),
    (select id from sets where short_name = 'tmh1'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'tmh1'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tmh1'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shapeshifter'),
    (select id from sets where short_name = 'tmh1'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tmh1'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tmh1'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tmh1'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bear'),
    (select id from sets where short_name = 'tmh1'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tmh1'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golem'),
    (select id from sets where short_name = 'tmh1'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tmh1'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illusion'),
    (select id from sets where short_name = 'tmh1'),
    '5',
    'common'
) 
 on conflict do nothing;
