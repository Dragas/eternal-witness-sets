insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Vulshok Morningstar'),
    (select id from sets where short_name = 'ddi'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodfire Colossus'),
    (select id from sets where short_name = 'ddi'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Augury Owl'),
    (select id from sets where short_name = 'ddi'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steel of the Godhead'),
    (select id from sets where short_name = 'ddi'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scroll Thief'),
    (select id from sets where short_name = 'ddi'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sigil of Sleep'),
    (select id from sets where short_name = 'ddi'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seismic Strike'),
    (select id from sets where short_name = 'ddi'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Revoke Existence'),
    (select id from sets where short_name = 'ddi'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunblast Angel'),
    (select id from sets where short_name = 'ddi'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddi'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vulshok Berserker'),
    (select id from sets where short_name = 'ddi'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddi'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Galepowder Mage'),
    (select id from sets where short_name = 'ddi'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sky Spirit'),
    (select id from sets where short_name = 'ddi'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vulshok Battlegear'),
    (select id from sets where short_name = 'ddi'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Preordain'),
    (select id from sets where short_name = 'ddi'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Path to Exile'),
    (select id from sets where short_name = 'ddi'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Safe Passage'),
    (select id from sets where short_name = 'ddi'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flood Plain'),
    (select id from sets where short_name = 'ddi'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jedit''s Dragoons'),
    (select id from sets where short_name = 'ddi'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddi'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primal Plasma'),
    (select id from sets where short_name = 'ddi'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodfire Kavu'),
    (select id from sets where short_name = 'ddi'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Greater Stone Spirit'),
    (select id from sets where short_name = 'ddi'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coral Fighters'),
    (select id from sets where short_name = 'ddi'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Journeyer''s Kite'),
    (select id from sets where short_name = 'ddi'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Neurok Invisimancer'),
    (select id from sets where short_name = 'ddi'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jaws of Stone'),
    (select id from sets where short_name = 'ddi'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddi'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddi'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spire Barrage'),
    (select id from sets where short_name = 'ddi'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pilgrim''s Eye'),
    (select id from sets where short_name = 'ddi'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pygmy Pyrosaur'),
    (select id from sets where short_name = 'ddi'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistmeadow Witch'),
    (select id from sets where short_name = 'ddi'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plated Geopede'),
    (select id from sets where short_name = 'ddi'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clone'),
    (select id from sets where short_name = 'ddi'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oblivion Ring'),
    (select id from sets where short_name = 'ddi'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Earth Servant'),
    (select id from sets where short_name = 'ddi'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddi'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Membrane'),
    (select id from sets where short_name = 'ddi'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Downhill Charge'),
    (select id from sets where short_name = 'ddi'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Venser, the Sojourner'),
    (select id from sets where short_name = 'ddi'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddi'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sejiri Refuge'),
    (select id from sets where short_name = 'ddi'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddi'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minamo Sightbender'),
    (select id from sets where short_name = 'ddi'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vanish into Memory'),
    (select id from sets where short_name = 'ddi'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volley of Boulders'),
    (select id from sets where short_name = 'ddi'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azorius Chancery'),
    (select id from sets where short_name = 'ddi'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chartooth Cougar'),
    (select id from sets where short_name = 'ddi'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Shield'),
    (select id from sets where short_name = 'ddi'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vulshok Sorcerer'),
    (select id from sets where short_name = 'ddi'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Geyser Glider'),
    (select id from sets where short_name = 'ddi'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cache Raiders'),
    (select id from sets where short_name = 'ddi'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'New Benalia'),
    (select id from sets where short_name = 'ddi'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lithophage'),
    (select id from sets where short_name = 'ddi'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphinx of Uthuun'),
    (select id from sets where short_name = 'ddi'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anger'),
    (select id from sets where short_name = 'ddi'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armillary Sphere'),
    (select id from sets where short_name = 'ddi'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cosi''s Ravager'),
    (select id from sets where short_name = 'ddi'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windreaver'),
    (select id from sets where short_name = 'ddi'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slith Strider'),
    (select id from sets where short_name = 'ddi'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overrule'),
    (select id from sets where short_name = 'ddi'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wayfarer''s Bauble'),
    (select id from sets where short_name = 'ddi'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cryptic Annelid'),
    (select id from sets where short_name = 'ddi'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fiery Hellhound'),
    (select id from sets where short_name = 'ddi'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddi'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddi'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Torchling'),
    (select id from sets where short_name = 'ddi'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Koth of the Hammer'),
    (select id from sets where short_name = 'ddi'),
    '44',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wall of Denial'),
    (select id from sets where short_name = 'ddi'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whitemane Lion'),
    (select id from sets where short_name = 'ddi'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Cartographer'),
    (select id from sets where short_name = 'ddi'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sawtooth Loon'),
    (select id from sets where short_name = 'ddi'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soaring Seacliff'),
    (select id from sets where short_name = 'ddi'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Searing Blaze'),
    (select id from sets where short_name = 'ddi'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Giant'),
    (select id from sets where short_name = 'ddi'),
    '55',
    'uncommon'
) 
 on conflict do nothing;
