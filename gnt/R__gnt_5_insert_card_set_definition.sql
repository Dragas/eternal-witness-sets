insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'gnt'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lord of the Accursed'),
    (select id from sets where short_name = 'gnt'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whirler Rogue'),
    (select id from sets where short_name = 'gnt'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rolling Thunder'),
    (select id from sets where short_name = 'gnt'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overcome'),
    (select id from sets where short_name = 'gnt'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bright Reprisal'),
    (select id from sets where short_name = 'gnt'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Favorable Winds'),
    (select id from sets where short_name = 'gnt'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bombard'),
    (select id from sets where short_name = 'gnt'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Subjugator Angel'),
    (select id from sets where short_name = 'gnt'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Howling Golem'),
    (select id from sets where short_name = 'gnt'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tattered Mummy'),
    (select id from sets where short_name = 'gnt'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inspired Sphinx'),
    (select id from sets where short_name = 'gnt'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pilgrim''s Eye'),
    (select id from sets where short_name = 'gnt'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zahid, Djinn of the Lamp'),
    (select id from sets where short_name = 'gnt'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Everdawn Champion'),
    (select id from sets where short_name = 'gnt'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'gnt'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seek the Wilds'),
    (select id from sets where short_name = 'gnt'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fleshbag Marauder'),
    (select id from sets where short_name = 'gnt'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fan Bearer'),
    (select id from sets where short_name = 'gnt'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Mastery'),
    (select id from sets where short_name = 'gnt'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snare Thopter'),
    (select id from sets where short_name = 'gnt'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Claustrophobia'),
    (select id from sets where short_name = 'gnt'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Scholar'),
    (select id from sets where short_name = 'gnt'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bone Splinters'),
    (select id from sets where short_name = 'gnt'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'gnt'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Goliath'),
    (select id from sets where short_name = 'gnt'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tormenting Voice'),
    (select id from sets where short_name = 'gnt'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hydrolash'),
    (select id from sets where short_name = 'gnt'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cruel Revival'),
    (select id from sets where short_name = 'gnt'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Siege-Gang Commander'),
    (select id from sets where short_name = 'gnt'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Call the Cavalry'),
    (select id from sets where short_name = 'gnt'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruesome Fate'),
    (select id from sets where short_name = 'gnt'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'gnt'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'gnt'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Militant Angel'),
    (select id from sets where short_name = 'gnt'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Soulblade Djinn'),
    (select id from sets where short_name = 'gnt'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rhonas''s Monument'),
    (select id from sets where short_name = 'gnt'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'gnt'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'gnt'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'gnt'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inspired Charge'),
    (select id from sets where short_name = 'gnt'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Welder Automaton'),
    (select id from sets where short_name = 'gnt'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'gnt'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seismic Elemental'),
    (select id from sets where short_name = 'gnt'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nissa''s Revelation'),
    (select id from sets where short_name = 'gnt'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eager Construct'),
    (select id from sets where short_name = 'gnt'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'gnt'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Captivating Crew'),
    (select id from sets where short_name = 'gnt'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jungle Delver'),
    (select id from sets where short_name = 'gnt'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghalta, Primal Hunger'),
    (select id from sets where short_name = 'gnt'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aerial Responder'),
    (select id from sets where short_name = 'gnt'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Languish'),
    (select id from sets where short_name = 'gnt'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rot Hulk'),
    (select id from sets where short_name = 'gnt'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Always Watching'),
    (select id from sets where short_name = 'gnt'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inspiring Captain'),
    (select id from sets where short_name = 'gnt'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrashing Brontodon'),
    (select id from sets where short_name = 'gnt'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Filigree Familiar'),
    (select id from sets where short_name = 'gnt'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = 'gnt'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'gnt'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manalith'),
    (select id from sets where short_name = 'gnt'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avatar of Growth'),
    (select id from sets where short_name = 'gnt'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dragon Fodder'),
    (select id from sets where short_name = 'gnt'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Advanced Stitchwing'),
    (select id from sets where short_name = 'gnt'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zulaport Cutthroat'),
    (select id from sets where short_name = 'gnt'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Benalish Marshal'),
    (select id from sets where short_name = 'gnt'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'gnt'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mesa Unicorn'),
    (select id from sets where short_name = 'gnt'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thallid Soothsayer'),
    (select id from sets where short_name = 'gnt'),
    '35',
    'uncommon'
) 
 on conflict do nothing;
