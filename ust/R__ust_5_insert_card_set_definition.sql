insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Steel Squirrel'),
    (select id from sets where short_name = 'ust'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight of the Widget'),
    (select id from sets where short_name = 'ust'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Split Screen'),
    (select id from sets where short_name = 'ust'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Novellamental'),
    (select id from sets where short_name = 'ust'),
    '41a',
    'common'
) ,
(
    (select id from mtgcard where name = 'GO TO JAIL'),
    (select id from sets where short_name = 'ust'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insufferable Syphon'),
    (select id from sets where short_name = 'ust'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Humming-'),
    (select id from sets where short_name = 'ust'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Aerie'),
    (select id from sets where short_name = 'ust'),
    '181',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Contraption Cannon'),
    (select id from sets where short_name = 'ust'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Genetic Recombinator'),
    (select id from sets where short_name = 'ust'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ordinary Pony'),
    (select id from sets where short_name = 'ust'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Do-It-Yourself Seraph'),
    (select id from sets where short_name = 'ust'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Spy Eye'),
    (select id from sets where short_name = 'ust'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ust'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Widget Contraption'),
    (select id from sets where short_name = 'ust'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Very Cryptic Command'),
    (select id from sets where short_name = 'ust'),
    '49d',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steady-Handed Mook'),
    (select id from sets where short_name = 'ust'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Swirly Disc'),
    (select id from sets where short_name = 'ust'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clocknapper'),
    (select id from sets where short_name = 'ust'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clever Combo'),
    (select id from sets where short_name = 'ust'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'First Pick'),
    (select id from sets where short_name = 'ust'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Staff of the Letter Magus'),
    (select id from sets where short_name = 'ust'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Secret Base'),
    (select id from sets where short_name = 'ust'),
    '165b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baron Von Count'),
    (select id from sets where short_name = 'ust'),
    '127',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blurry Beeble'),
    (select id from sets where short_name = 'ust'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Novellamental'),
    (select id from sets where short_name = 'ust'),
    '41d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crafty Octopus'),
    (select id from sets where short_name = 'ust'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'S.N.E.A.K. Dispatcher'),
    (select id from sets where short_name = 'ust'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dogsnail Engine'),
    (select id from sets where short_name = 'ust'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enraged Killbot'),
    (select id from sets where short_name = 'ust'),
    '145d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Target Minotaur'),
    (select id from sets where short_name = 'ust'),
    '98d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sundering Fork'),
    (select id from sets where short_name = 'ust'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skull Saucer'),
    (select id from sets where short_name = 'ust'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steamflogger Service Rep'),
    (select id from sets where short_name = 'ust'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Adorable Kitten'),
    (select id from sets where short_name = 'ust'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gnomeball Machine'),
    (select id from sets where short_name = 'ust'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Auto-Key'),
    (select id from sets where short_name = 'ust'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Very Cryptic Command'),
    (select id from sets where short_name = 'ust'),
    '49b',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mary O''Kill'),
    (select id from sets where short_name = 'ust'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gnome-Made Engine'),
    (select id from sets where short_name = 'ust'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snickering Squirrel'),
    (select id from sets where short_name = 'ust'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garbage Elemental'),
    (select id from sets where short_name = 'ust'),
    '82a',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Very Cryptic Command'),
    (select id from sets where short_name = 'ust'),
    '49c',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Big Idea'),
    (select id from sets where short_name = 'ust'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hoisted Hireling'),
    (select id from sets where short_name = 'ust'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hard Hat Area'),
    (select id from sets where short_name = 'ust'),
    '187',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mer Man'),
    (select id from sets where short_name = 'ust'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Haberdasher'),
    (select id from sets where short_name = 'ust'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Time Out'),
    (select id from sets where short_name = 'ust'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Head Banger'),
    (select id from sets where short_name = 'ust'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Crocodile'),
    (select id from sets where short_name = 'ust'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rapid Prototyper'),
    (select id from sets where short_name = 'ust'),
    '200',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Super-Duper Death Ray'),
    (select id from sets where short_name = 'ust'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beast in Show'),
    (select id from sets where short_name = 'ust'),
    '103a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Handy Dandy Clone Machine'),
    (select id from sets where short_name = 'ust'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hangman'),
    (select id from sets where short_name = 'ust'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knight of the Kitchen Sink'),
    (select id from sets where short_name = 'ust'),
    '12a',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Riveting Rigger'),
    (select id from sets where short_name = 'ust'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Grand Calcutron'),
    (select id from sets where short_name = 'ust'),
    '131',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Knight of the Kitchen Sink'),
    (select id from sets where short_name = 'ust'),
    '12f',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jackknight'),
    (select id from sets where short_name = 'ust'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gift Horse'),
    (select id from sets where short_name = 'ust'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steamflogger Temp'),
    (select id from sets where short_name = 'ust'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Defective Detective'),
    (select id from sets where short_name = 'ust'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hazmat Suit (Used)'),
    (select id from sets where short_name = 'ust'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ust'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Border Guardian'),
    (select id from sets where short_name = 'ust'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shaggy Camel'),
    (select id from sets where short_name = 'ust'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oaken Power Suit'),
    (select id from sets where short_name = 'ust'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knight of the Kitchen Sink'),
    (select id from sets where short_name = 'ust'),
    '12c',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bumbling Pangolin'),
    (select id from sets where short_name = 'ust'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Extremely Slow Zombie'),
    (select id from sets where short_name = 'ust'),
    '54a',
    'common'
) ,
(
    (select id from mtgcard where name = 'capital offense'),
    (select id from sets where short_name = 'ust'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Countdown Is at One'),
    (select id from sets where short_name = 'ust'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrench-Rigger'),
    (select id from sets where short_name = 'ust'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Amateur Auteur'),
    (select id from sets where short_name = 'ust'),
    '3d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Buzzing Whack-a-Doodle'),
    (select id from sets where short_name = 'ust'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feisty Stegosaurus'),
    (select id from sets where short_name = 'ust'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ol'' Buzzbark'),
    (select id from sets where short_name = 'ust'),
    '134',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Curious Killbot'),
    (select id from sets where short_name = 'ust'),
    '145a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Targeting Rocket'),
    (select id from sets where short_name = 'ust'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dr. Julius Jumblemorph'),
    (select id from sets where short_name = 'ust'),
    '130',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Monkey-'),
    (select id from sets where short_name = 'ust'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gimme Five'),
    (select id from sets where short_name = 'ust'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sly Spy'),
    (select id from sets where short_name = 'ust'),
    '67d',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Party Crasher'),
    (select id from sets where short_name = 'ust'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Socketed Sprocketer'),
    (select id from sets where short_name = 'ust'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Record Store'),
    (select id from sets where short_name = 'ust'),
    '201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sly Spy'),
    (select id from sets where short_name = 'ust'),
    '67a',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Finders, Keepers'),
    (select id from sets where short_name = 'ust'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lackey Recycler'),
    (select id from sets where short_name = 'ust'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Common Iguana'),
    (select id from sets where short_name = 'ust'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombified'),
    (select id from sets where short_name = 'ust'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ineffable Blessing'),
    (select id from sets where short_name = 'ust'),
    '113f',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Box of Free-Range Goblins'),
    (select id from sets where short_name = 'ust'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magic Word'),
    (select id from sets where short_name = 'ust'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Joyride Rigger'),
    (select id from sets where short_name = 'ust'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Secret Base'),
    (select id from sets where short_name = 'ust'),
    '165e',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rules Lawyer'),
    (select id from sets where short_name = 'ust'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tread Mill'),
    (select id from sets where short_name = 'ust'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Labro Bot'),
    (select id from sets where short_name = 'ust'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Suspicious Nanny'),
    (select id from sets where short_name = 'ust'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Novellamental'),
    (select id from sets where short_name = 'ust'),
    '41c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Slingshot'),
    (select id from sets where short_name = 'ust'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Amateur Auteur'),
    (select id from sets where short_name = 'ust'),
    '3c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Numbing Jellyfish'),
    (select id from sets where short_name = 'ust'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hydradoodle'),
    (select id from sets where short_name = 'ust'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beast in Show'),
    (select id from sets where short_name = 'ust'),
    '103b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Target Minotaur'),
    (select id from sets where short_name = 'ust'),
    '98c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dictation Quillograph'),
    (select id from sets where short_name = 'ust'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hot Fix'),
    (select id from sets where short_name = 'ust'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ust'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ineffable Blessing'),
    (select id from sets where short_name = 'ust'),
    '113b',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Target Minotaur'),
    (select id from sets where short_name = 'ust'),
    '98b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kindly Cognician'),
    (select id from sets where short_name = 'ust'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arms Depot'),
    (select id from sets where short_name = 'ust'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mad Science Fair Project'),
    (select id from sets where short_name = 'ust'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grusilda, Monster Masher'),
    (select id from sets where short_name = 'ust'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knight of the Kitchen Sink'),
    (select id from sets where short_name = 'ust'),
    '12d',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oddly Uneven'),
    (select id from sets where short_name = 'ust'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chittering Doom'),
    (select id from sets where short_name = 'ust'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hammer Helper'),
    (select id from sets where short_name = 'ust'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhino-'),
    (select id from sets where short_name = 'ust'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selfie Preservation'),
    (select id from sets where short_name = 'ust'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squirrel Dealer'),
    (select id from sets where short_name = 'ust'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Delighted Killbot'),
    (select id from sets where short_name = 'ust'),
    '145b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chipper Chopper'),
    (select id from sets where short_name = 'ust'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phoebe, Head of S.N.E.A.K.'),
    (select id from sets where short_name = 'ust'),
    '135',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Applied Aeronautics'),
    (select id from sets where short_name = 'ust'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quick-Stick Lick Trick'),
    (select id from sets where short_name = 'ust'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voracious Vacuum'),
    (select id from sets where short_name = 'ust'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stinging Scorpion'),
    (select id from sets where short_name = 'ust'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Division Table'),
    (select id from sets where short_name = 'ust'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steamflogger Boss'),
    (select id from sets where short_name = 'ust'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ineffable Blessing'),
    (select id from sets where short_name = 'ust'),
    '113c',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sly Spy'),
    (select id from sets where short_name = 'ust'),
    '67c',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Garbage Elemental'),
    (select id from sets where short_name = 'ust'),
    '82b',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teacher''s Pet'),
    (select id from sets where short_name = 'ust'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chivalrous Chevalier'),
    (select id from sets where short_name = 'ust'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Willing Test Subject'),
    (select id from sets where short_name = 'ust'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ineffable Blessing'),
    (select id from sets where short_name = 'ust'),
    '113e',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Secret Base'),
    (select id from sets where short_name = 'ust'),
    '165a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Very Cryptic Command'),
    (select id from sets where short_name = 'ust'),
    '49a',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kindslaver'),
    (select id from sets where short_name = 'ust'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lobe Lobber'),
    (select id from sets where short_name = 'ust'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Amateur Auteur'),
    (select id from sets where short_name = 'ust'),
    '3b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steamflogger of the Month'),
    (select id from sets where short_name = 'ust'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Secret Base'),
    (select id from sets where short_name = 'ust'),
    '165c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast in Show'),
    (select id from sets where short_name = 'ust'),
    '103d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Old Guard'),
    (select id from sets where short_name = 'ust'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Modular Monstrosity'),
    (select id from sets where short_name = 'ust'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Proper Laboratory Attire'),
    (select id from sets where short_name = 'ust'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Everythingamajig'),
    (select id from sets where short_name = 'ust'),
    '147f',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Garbage Elemental'),
    (select id from sets where short_name = 'ust'),
    '82d',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Old-Fashioned Vampire'),
    (select id from sets where short_name = 'ust'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Garbage Elemental'),
    (select id from sets where short_name = 'ust'),
    '82c',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Side Quest'),
    (select id from sets where short_name = 'ust'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boomflinger'),
    (select id from sets where short_name = 'ust'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Refibrillator'),
    (select id from sets where short_name = 'ust'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mother Kangaroo'),
    (select id from sets where short_name = 'ust'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Target Minotaur'),
    (select id from sets where short_name = 'ust'),
    '98a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cramped Bunker'),
    (select id from sets where short_name = 'ust'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Five-Finger Discount'),
    (select id from sets where short_name = 'ust'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Better Than One'),
    (select id from sets where short_name = 'ust'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Multi-Headed'),
    (select id from sets where short_name = 'ust'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Everythingamajig'),
    (select id from sets where short_name = 'ust'),
    '147c',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animate Library'),
    (select id from sets where short_name = 'ust'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cogmentor'),
    (select id from sets where short_name = 'ust'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eager Beaver'),
    (select id from sets where short_name = 'ust'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spike, Tournament Grinder'),
    (select id from sets where short_name = 'ust'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'More or Less'),
    (select id from sets where short_name = 'ust'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Extremely Slow Zombie'),
    (select id from sets where short_name = 'ust'),
    '54d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Extremely Slow Zombie'),
    (select id from sets where short_name = 'ust'),
    '54b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Everythingamajig'),
    (select id from sets where short_name = 'ust'),
    '147b',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Robo-'),
    (select id from sets where short_name = 'ust'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duplication Device'),
    (select id from sets where short_name = 'ust'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aerial Toastmaster'),
    (select id from sets where short_name = 'ust'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Half-Squirrel, Half-'),
    (select id from sets where short_name = 'ust'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Graveyard Busybody'),
    (select id from sets where short_name = 'ust'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Novellamental'),
    (select id from sets where short_name = 'ust'),
    '41b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dirty Rat'),
    (select id from sets where short_name = 'ust'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Incite Insight'),
    (select id from sets where short_name = 'ust'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword of Dungeons & Dragons'),
    (select id from sets where short_name = 'ust'),
    '163',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Overt Operative'),
    (select id from sets where short_name = 'ust'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Midlife Upgrade'),
    (select id from sets where short_name = 'ust'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ineffable Blessing'),
    (select id from sets where short_name = 'ust'),
    '113d',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angelic Rocket'),
    (select id from sets where short_name = 'ust'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sly Spy'),
    (select id from sets where short_name = 'ust'),
    '67f',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steam-Powered'),
    (select id from sets where short_name = 'ust'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ineffable Blessing'),
    (select id from sets where short_name = 'ust'),
    '113a',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Turbo-Thwacking Auto-Hammer'),
    (select id from sets where short_name = 'ust'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Half-Kitten, Half-'),
    (select id from sets where short_name = 'ust'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slaying Mantis'),
    (select id from sets where short_name = 'ust'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Watermarket'),
    (select id from sets where short_name = 'ust'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deadly Poison Sampler'),
    (select id from sets where short_name = 'ust'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Work a Double'),
    (select id from sets where short_name = 'ust'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'It That Gets Left Hanging'),
    (select id from sets where short_name = 'ust'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infinity Elemental'),
    (select id from sets where short_name = 'ust'),
    '88',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Subcontract'),
    (select id from sets where short_name = 'ust'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ground Pounder'),
    (select id from sets where short_name = 'ust'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earl of Squirrel'),
    (select id from sets where short_name = 'ust'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thud-for-Duds'),
    (select id from sets where short_name = 'ust'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bee-Bee Gun'),
    (select id from sets where short_name = 'ust'),
    '171',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Optical Optimizer'),
    (select id from sets where short_name = 'ust'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Over My Dead Bodies'),
    (select id from sets where short_name = 'ust'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inflation Station'),
    (select id from sets where short_name = 'ust'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Everythingamajig'),
    (select id from sets where short_name = 'ust'),
    '147d',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steamfloggery'),
    (select id from sets where short_name = 'ust'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Half-Orc, Half-'),
    (select id from sets where short_name = 'ust'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beast in Show'),
    (select id from sets where short_name = 'ust'),
    '103c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guest List'),
    (select id from sets where short_name = 'ust'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twiddlestick Charger'),
    (select id from sets where short_name = 'ust'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Everythingamajig'),
    (select id from sets where short_name = 'ust'),
    '147e',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Druid of the Sacred Beaker'),
    (select id from sets where short_name = 'ust'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = '"Rumors of My Death . . ."'),
    (select id from sets where short_name = 'ust'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Summon the Pack'),
    (select id from sets where short_name = 'ust'),
    '74',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Success!'),
    (select id from sets where short_name = 'ust'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Painiac'),
    (select id from sets where short_name = 'ust'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sacrifice Play'),
    (select id from sets where short_name = 'ust'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pet Project'),
    (select id from sets where short_name = 'ust'),
    '198',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'As Luck Would Have It'),
    (select id from sets where short_name = 'ust'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Neural Network'),
    (select id from sets where short_name = 'ust'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Top-Secret Tunnel'),
    (select id from sets where short_name = 'ust'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Very Cryptic Command'),
    (select id from sets where short_name = 'ust'),
    '49f',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clock of DOOOOOOOOOOOOM!'),
    (select id from sets where short_name = 'ust'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dual Doomsuits'),
    (select id from sets where short_name = 'ust'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Very Cryptic Command'),
    (select id from sets where short_name = 'ust'),
    '49e',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shellephant'),
    (select id from sets where short_name = 'ust'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Garbage Elemental'),
    (select id from sets where short_name = 'ust'),
    '82f',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight of the Kitchen Sink'),
    (select id from sets where short_name = 'ust'),
    '12e',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Everythingamajig'),
    (select id from sets where short_name = 'ust'),
    '147a',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Big Boa Constrictor'),
    (select id from sets where short_name = 'ust'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Three-Headed Goblin'),
    (select id from sets where short_name = 'ust'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza, Academy Headmaster'),
    (select id from sets where short_name = 'ust'),
    '136',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sly Spy'),
    (select id from sets where short_name = 'ust'),
    '67b',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Despondent Killbot'),
    (select id from sets where short_name = 'ust'),
    '145c',
    'common'
) ,
(
    (select id from mtgcard where name = 'By Gnome Means'),
    (select id from sets where short_name = 'ust'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mandatory Friendship Shackles'),
    (select id from sets where short_name = 'ust'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hammer Jammer'),
    (select id from sets where short_name = 'ust'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stamp of Approval'),
    (select id from sets where short_name = 'ust'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Accessories to Murder'),
    (select id from sets where short_name = 'ust'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inhumaniac'),
    (select id from sets where short_name = 'ust'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Really Epic Punch'),
    (select id from sets where short_name = 'ust'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sly Spy'),
    (select id from sets where short_name = 'ust'),
    '67e',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dispatch Dispensary'),
    (select id from sets where short_name = 'ust'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Entirely Normal Armchair'),
    (select id from sets where short_name = 'ust'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Garbage Elemental'),
    (select id from sets where short_name = 'ust'),
    '82e',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ust'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serpentine'),
    (select id from sets where short_name = 'ust'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Amateur Auteur'),
    (select id from sets where short_name = 'ust'),
    '3a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Half-Shark, Half-'),
    (select id from sets where short_name = 'ust'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hammerfest Boomtacular'),
    (select id from sets where short_name = 'ust'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crow Storm'),
    (select id from sets where short_name = 'ust'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spell Suck'),
    (select id from sets where short_name = 'ust'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squirrel-Powered Scheme'),
    (select id from sets where short_name = 'ust'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Fortune'),
    (select id from sets where short_name = 'ust'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'X'),
    (select id from sets where short_name = 'ust'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Just Desserts'),
    (select id from sets where short_name = 'ust'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Buzz Buggy'),
    (select id from sets where short_name = 'ust'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Masterful Ninja'),
    (select id from sets where short_name = 'ust'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ust'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of the Kitchen Sink'),
    (select id from sets where short_name = 'ust'),
    '12b',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Secret Base'),
    (select id from sets where short_name = 'ust'),
    '165d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Extremely Slow Zombie'),
    (select id from sets where short_name = 'ust'),
    '54c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sap Sucker'),
    (select id from sets where short_name = 'ust'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krark''s Other Thumb'),
    (select id from sets where short_name = 'ust'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jamming Device'),
    (select id from sets where short_name = 'ust'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ninja'),
    (select id from sets where short_name = 'ust'),
    '61',
    'uncommon'
) 
 on conflict do nothing;
