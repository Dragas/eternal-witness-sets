insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Path to Exile'),
    (select id from sets where short_name = 'md1'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Windbrisk Heights'),
    (select id from sets where short_name = 'md1'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'md1'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elspeth, Knight-Errant'),
    (select id from sets where short_name = 'md1'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Raise the Alarm'),
    (select id from sets where short_name = 'md1'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'md1'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'md1'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Intangible Virtue'),
    (select id from sets where short_name = 'md1'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Isolated Chapel'),
    (select id from sets where short_name = 'md1'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inquisition of Kozilek'),
    (select id from sets where short_name = 'md1'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Caves of Koilos'),
    (select id from sets where short_name = 'md1'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vault of the Archangel'),
    (select id from sets where short_name = 'md1'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lingering Souls'),
    (select id from sets where short_name = 'md1'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kataki, War''s Wage'),
    (select id from sets where short_name = 'md1'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spectral Procession'),
    (select id from sets where short_name = 'md1'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sword of Feast and Famine'),
    (select id from sets where short_name = 'md1'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ghost Quarter'),
    (select id from sets where short_name = 'md1'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burrenton Forge-Tender'),
    (select id from sets where short_name = 'md1'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tidehollow Sculler'),
    (select id from sets where short_name = 'md1'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shrine of Loyal Legions'),
    (select id from sets where short_name = 'md1'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Honor of the Pure'),
    (select id from sets where short_name = 'md1'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Warden'),
    (select id from sets where short_name = 'md1'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zealous Persecution'),
    (select id from sets where short_name = 'md1'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'md1'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dismember'),
    (select id from sets where short_name = 'md1'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Relic of Progenitus'),
    (select id from sets where short_name = 'md1'),
    '21',
    'uncommon'
) 
 on conflict do nothing;
