insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Thunderbreak Regent'),
    (select id from sets where short_name = 'pdtk'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harbinger of the Hunt'),
    (select id from sets where short_name = 'pdtk'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonlord Kolaghan'),
    (select id from sets where short_name = 'pdtk'),
    '218s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thunderbreak Regent'),
    (select id from sets where short_name = 'pdtk'),
    '162s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Living Lore'),
    (select id from sets where short_name = 'pdtk'),
    '61s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ojutai''s Command'),
    (select id from sets where short_name = 'pdtk'),
    '227s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silumgar''s Command'),
    (select id from sets where short_name = 'pdtk'),
    '232s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anafenza, Kin-Tree Spirit'),
    (select id from sets where short_name = 'pdtk'),
    '2s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necromaster Dragon'),
    (select id from sets where short_name = 'pdtk'),
    '226s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonlord Ojutai'),
    (select id from sets where short_name = 'pdtk'),
    '219s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dragonlord Dromoka'),
    (select id from sets where short_name = 'pdtk'),
    '217s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Necromaster Dragon'),
    (select id from sets where short_name = 'pdtk'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Surrak, the Hunt Caller'),
    (select id from sets where short_name = 'pdtk'),
    '210s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foe-Razer Regent'),
    (select id from sets where short_name = 'pdtk'),
    '187s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sidisi, Undead Vizier'),
    (select id from sets where short_name = 'pdtk'),
    '120s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hidden Dragonslayer'),
    (select id from sets where short_name = 'pdtk'),
    '23s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dromoka''s Command'),
    (select id from sets where short_name = 'pdtk'),
    '221s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boltwing Marauder'),
    (select id from sets where short_name = 'pdtk'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Profaner of the Dead'),
    (select id from sets where short_name = 'pdtk'),
    '70s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harbinger of the Hunt'),
    (select id from sets where short_name = 'pdtk'),
    '223s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunscorch Regent'),
    (select id from sets where short_name = 'pdtk'),
    '41s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blessed Reincarnation'),
    (select id from sets where short_name = 'pdtk'),
    '47s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arashin Sovereign'),
    (select id from sets where short_name = 'pdtk'),
    '212s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boltwing Marauder'),
    (select id from sets where short_name = 'pdtk'),
    '214s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kolaghan''s Command'),
    (select id from sets where short_name = 'pdtk'),
    '224s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pristine Skywise'),
    (select id from sets where short_name = 'pdtk'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathbringer Regent'),
    (select id from sets where short_name = 'pdtk'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pristine Skywise'),
    (select id from sets where short_name = 'pdtk'),
    '228s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avatar of the Resolute'),
    (select id from sets where short_name = 'pdtk'),
    '175s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pitiless Horde'),
    (select id from sets where short_name = 'pdtk'),
    '112s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ojutai''s Command'),
    (select id from sets where short_name = 'pdtk'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Atarka''s Command'),
    (select id from sets where short_name = 'pdtk'),
    '213s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Damnable Pact'),
    (select id from sets where short_name = 'pdtk'),
    '93s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood-Chin Fanatic'),
    (select id from sets where short_name = 'pdtk'),
    '88s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Vision'),
    (select id from sets where short_name = 'pdtk'),
    '167s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ire Shaman'),
    (select id from sets where short_name = 'pdtk'),
    '141s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crater Elemental'),
    (select id from sets where short_name = 'pdtk'),
    '132s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scaleguard Sentinels'),
    (select id from sets where short_name = 'pdtk'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arashin Foremost'),
    (select id from sets where short_name = 'pdtk'),
    '3s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Den Protector'),
    (select id from sets where short_name = 'pdtk'),
    '181s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonlord Silumgar'),
    (select id from sets where short_name = 'pdtk'),
    '220s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Icefall Regent'),
    (select id from sets where short_name = 'pdtk'),
    '58s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myth Realized'),
    (select id from sets where short_name = 'pdtk'),
    '26s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sidisi, Undead Vizier'),
    (select id from sets where short_name = 'pdtk'),
    '120p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonlord Atarka'),
    (select id from sets where short_name = 'pdtk'),
    '216s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Deathbringer Regent'),
    (select id from sets where short_name = 'pdtk'),
    '96s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silumgar Assassin'),
    (select id from sets where short_name = 'pdtk'),
    '121s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stratus Dancer'),
    (select id from sets where short_name = 'pdtk'),
    '80s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zurgo Bellstriker'),
    (select id from sets where short_name = 'pdtk'),
    '169s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arashin Sovereign'),
    (select id from sets where short_name = 'pdtk'),
    '212',
    'rare'
) 
 on conflict do nothing;
