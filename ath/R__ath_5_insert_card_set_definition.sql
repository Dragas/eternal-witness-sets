insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Goblin Offensive'),
    (select id from sets where short_name = 'ath'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin King'),
    (select id from sets where short_name = 'ath'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Mutant'),
    (select id from sets where short_name = 'ath'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hurricane'),
    (select id from sets where short_name = 'ath'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mogg Flunkies'),
    (select id from sets where short_name = 'ath'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Uthden Troll'),
    (select id from sets where short_name = 'ath'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ath'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Digging Team'),
    (select id from sets where short_name = 'ath'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Dragon'),
    (select id from sets where short_name = 'ath'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirri, Cat Warrior'),
    (select id from sets where short_name = 'ath'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Icatian Javelineers'),
    (select id from sets where short_name = 'ath'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strip Mine'),
    (select id from sets where short_name = 'ath'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carnivorous Plant'),
    (select id from sets where short_name = 'ath'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ath'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raging Goblin'),
    (select id from sets where short_name = 'ath'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'ath'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erhnam Djinn'),
    (select id from sets where short_name = 'ath'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'ath'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'ath'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slippery Karst'),
    (select id from sets where short_name = 'ath'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feast of the Unicorn'),
    (select id from sets where short_name = 'ath'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serrated Arrows'),
    (select id from sets where short_name = 'ath'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Specter'),
    (select id from sets where short_name = 'ath'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'ath'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ath'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pegasus Stampede'),
    (select id from sets where short_name = 'ath'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overrun'),
    (select id from sets where short_name = 'ath'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Warrens'),
    (select id from sets where short_name = 'ath'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ath'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ath'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smoldering Crater'),
    (select id from sets where short_name = 'ath'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ath'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'ath'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Tinkerer'),
    (select id from sets where short_name = 'ath'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Combat Medic'),
    (select id from sets where short_name = 'ath'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hymn to Tourach'),
    (select id from sets where short_name = 'ath'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scavenger Folk'),
    (select id from sets where short_name = 'ath'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'ath'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Polluted Mire'),
    (select id from sets where short_name = 'ath'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Recruiter'),
    (select id from sets where short_name = 'ath'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyrokinesis'),
    (select id from sets where short_name = 'ath'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mogg Raider'),
    (select id from sets where short_name = 'ath'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Balloon Brigade'),
    (select id from sets where short_name = 'ath'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aesthir Glider'),
    (select id from sets where short_name = 'ath'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cuombajj Witches'),
    (select id from sets where short_name = 'ath'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of Stromgald'),
    (select id from sets where short_name = 'ath'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sacred Mesa'),
    (select id from sets where short_name = 'ath'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pendelhaven'),
    (select id from sets where short_name = 'ath'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Matron'),
    (select id from sets where short_name = 'ath'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warrior''s Honor'),
    (select id from sets where short_name = 'ath'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ihsan''s Shade'),
    (select id from sets where short_name = 'ath'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Grenade'),
    (select id from sets where short_name = 'ath'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mogg Fanatic'),
    (select id from sets where short_name = 'ath'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lady Orca'),
    (select id from sets where short_name = 'ath'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jalum Tome'),
    (select id from sets where short_name = 'ath'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'White Knight'),
    (select id from sets where short_name = 'ath'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Knight'),
    (select id from sets where short_name = 'ath'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyrotechnics'),
    (select id from sets where short_name = 'ath'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Samite Healer'),
    (select id from sets where short_name = 'ath'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'ath'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Benalish Knight'),
    (select id from sets where short_name = 'ath'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Order of the White Shield'),
    (select id from sets where short_name = 'ath'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Snowman'),
    (select id from sets where short_name = 'ath'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Hero'),
    (select id from sets where short_name = 'ath'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Vandal'),
    (select id from sets where short_name = 'ath'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brushland'),
    (select id from sets where short_name = 'ath'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Canopy Spider'),
    (select id from sets where short_name = 'ath'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'ath'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = 'ath'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Youthful Knight'),
    (select id from sets where short_name = 'ath'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drifting Meadow'),
    (select id from sets where short_name = 'ath'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ath'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = 'ath'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gorilla Chieftain'),
    (select id from sets where short_name = 'ath'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = 'ath'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pegasus Charger'),
    (select id from sets where short_name = 'ath'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spectral Bears'),
    (select id from sets where short_name = 'ath'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woolly Spider'),
    (select id from sets where short_name = 'ath'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ath'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Freewind Falcon'),
    (select id from sets where short_name = 'ath'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infantry Veteran'),
    (select id from sets where short_name = 'ath'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ranger en-Vec'),
    (select id from sets where short_name = 'ath'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armored Pegasus'),
    (select id from sets where short_name = 'ath'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unholy Strength'),
    (select id from sets where short_name = 'ath'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = 'ath'),
    '56',
    'common'
) 
 on conflict do nothing;
