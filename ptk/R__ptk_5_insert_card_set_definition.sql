insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Lady Zhurong, Warrior Queen'),
    (select id from sets where short_name = 'ptk'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ptk'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trained Jackal'),
    (select id from sets where short_name = 'ptk'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Taoist Mystic'),
    (select id from sets where short_name = 'ptk'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shu Cavalry'),
    (select id from sets where short_name = 'ptk'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rockslide Ambush'),
    (select id from sets where short_name = 'ptk'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rally the Troops'),
    (select id from sets where short_name = 'ptk'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barbarian General'),
    (select id from sets where short_name = 'ptk'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yellow Scarves General'),
    (select id from sets where short_name = 'ptk'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghostly Visit'),
    (select id from sets where short_name = 'ptk'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lu Bu, Master-at-Arms'),
    (select id from sets where short_name = 'ptk'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lu Xun, Scholar General'),
    (select id from sets where short_name = 'ptk'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volunteer Militia'),
    (select id from sets where short_name = 'ptk'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Council of Advisors'),
    (select id from sets where short_name = 'ptk'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ambition''s Cost'),
    (select id from sets where short_name = 'ptk'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lady Sun'),
    (select id from sets where short_name = 'ptk'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Young Wei Recruits'),
    (select id from sets where short_name = 'ptk'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shu Elite Companions'),
    (select id from sets where short_name = 'ptk'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ravages of War'),
    (select id from sets where short_name = 'ptk'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystic Denial'),
    (select id from sets where short_name = 'ptk'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sage''s Knowledge'),
    (select id from sets where short_name = 'ptk'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guan Yu, Sainted Warrior'),
    (select id from sets where short_name = 'ptk'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sun Ce, Young Conquerer'),
    (select id from sets where short_name = 'ptk'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ptk'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Return to Battle'),
    (select id from sets where short_name = 'ptk'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Bowman'),
    (select id from sets where short_name = 'ptk'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zodiac Ox'),
    (select id from sets where short_name = 'ptk'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Imperial Recruiter'),
    (select id from sets where short_name = 'ptk'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Extinguish'),
    (select id from sets where short_name = 'ptk'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wu Longbowman'),
    (select id from sets where short_name = 'ptk'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wolf Pack'),
    (select id from sets where short_name = 'ptk'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zhang Fei, Fierce Warrior'),
    (select id from sets where short_name = 'ptk'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wielding the Green Dragon'),
    (select id from sets where short_name = 'ptk'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zhao Zilong, Tiger General'),
    (select id from sets where short_name = 'ptk'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wu Warship'),
    (select id from sets where short_name = 'ptk'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ptk'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Three Visits'),
    (select id from sets where short_name = 'ptk'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stalking Tiger'),
    (select id from sets where short_name = 'ptk'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burning of Xinye'),
    (select id from sets where short_name = 'ptk'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain Bandit'),
    (select id from sets where short_name = 'ptk'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shu Elite Infantry'),
    (select id from sets where short_name = 'ptk'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ma Chao, Western Warrior'),
    (select id from sets where short_name = 'ptk'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rolling Earthquake'),
    (select id from sets where short_name = 'ptk'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yellow Scarves Troops'),
    (select id from sets where short_name = 'ptk'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Control of the Court'),
    (select id from sets where short_name = 'ptk'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Peach Garden Oath'),
    (select id from sets where short_name = 'ptk'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Champion''s Victory'),
    (select id from sets where short_name = 'ptk'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Taunting Challenge'),
    (select id from sets where short_name = 'ptk'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zodiac Dragon'),
    (select id from sets where short_name = 'ptk'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ptk'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wu Admiral'),
    (select id from sets where short_name = 'ptk'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heavy Fog'),
    (select id from sets where short_name = 'ptk'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coercion'),
    (select id from sets where short_name = 'ptk'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Empty City Ruse'),
    (select id from sets where short_name = 'ptk'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fire Ambush'),
    (select id from sets where short_name = 'ptk'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trip Wire'),
    (select id from sets where short_name = 'ptk'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cao Ren, Wei Commander'),
    (select id from sets where short_name = 'ptk'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slashing Tiger'),
    (select id from sets where short_name = 'ptk'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dong Zhou, the Tyrant'),
    (select id from sets where short_name = 'ptk'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zodiac Goat'),
    (select id from sets where short_name = 'ptk'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zodiac Pig'),
    (select id from sets where short_name = 'ptk'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warrior''s Stand'),
    (select id from sets where short_name = 'ptk'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spoils of Victory'),
    (select id from sets where short_name = 'ptk'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hunting Cheetah'),
    (select id from sets where short_name = 'ptk'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marshaling the Troops'),
    (select id from sets where short_name = 'ptk'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relentless Assault'),
    (select id from sets where short_name = 'ptk'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desert Sandstorm'),
    (select id from sets where short_name = 'ptk'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'False Defeat'),
    (select id from sets where short_name = 'ptk'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corrupt Eunuchs'),
    (select id from sets where short_name = 'ptk'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cunning Advisor'),
    (select id from sets where short_name = 'ptk'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eunuchs'' Intrigues'),
    (select id from sets where short_name = 'ptk'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zodiac Monkey'),
    (select id from sets where short_name = 'ptk'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yuan Shao, the Indecisive'),
    (select id from sets where short_name = 'ptk'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone Catapult'),
    (select id from sets where short_name = 'ptk'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zhuge Jin, Wu Strategist'),
    (select id from sets where short_name = 'ptk'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wu Light Cavalry'),
    (select id from sets where short_name = 'ptk'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diaochan, Artful Beauty'),
    (select id from sets where short_name = 'ptk'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'False Mourning'),
    (select id from sets where short_name = 'ptk'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ravaging Horde'),
    (select id from sets where short_name = 'ptk'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sima Yi, Wei Field Marshal'),
    (select id from sets where short_name = 'ptk'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ptk'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kongming''s Contraptions'),
    (select id from sets where short_name = 'ptk'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Riding the Dilu Horse'),
    (select id from sets where short_name = 'ptk'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = 'ptk'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brilliant Plan'),
    (select id from sets where short_name = 'ptk'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Poison Arrow'),
    (select id from sets where short_name = 'ptk'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lone Wolf'),
    (select id from sets where short_name = 'ptk'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zodiac Snake'),
    (select id from sets where short_name = 'ptk'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sun Quan, Lord of Wu'),
    (select id from sets where short_name = 'ptk'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zhou Yu, Chief Commander'),
    (select id from sets where short_name = 'ptk'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wei Strike Force'),
    (select id from sets where short_name = 'ptk'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Virtuous Charge'),
    (select id from sets where short_name = 'ptk'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wei Infantry'),
    (select id from sets where short_name = 'ptk'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zhang He, Wei General'),
    (select id from sets where short_name = 'ptk'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ptk'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strategic Planning'),
    (select id from sets where short_name = 'ptk'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lu Meng, Wu General'),
    (select id from sets where short_name = 'ptk'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ptk'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shu Grain Caravan'),
    (select id from sets where short_name = 'ptk'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wu Elite Cavalry'),
    (select id from sets where short_name = 'ptk'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ptk'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Misfortune''s Gain'),
    (select id from sets where short_name = 'ptk'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zodiac Rooster'),
    (select id from sets where short_name = 'ptk'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Imperial Seal'),
    (select id from sets where short_name = 'ptk'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wei Elite Companions'),
    (select id from sets where short_name = 'ptk'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yellow Scarves Cavalry'),
    (select id from sets where short_name = 'ptk'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ptk'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Southern Elephant'),
    (select id from sets where short_name = 'ptk'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ptk'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Straw Soldiers'),
    (select id from sets where short_name = 'ptk'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wu Scout'),
    (select id from sets where short_name = 'ptk'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Famine'),
    (select id from sets where short_name = 'ptk'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Xun Yu, Wei Advisor'),
    (select id from sets where short_name = 'ptk'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zhang Liao, Hero of Hefei'),
    (select id from sets where short_name = 'ptk'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guan Yu''s 1,000-Li March'),
    (select id from sets where short_name = 'ptk'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Xiahou Dun, the One-Eyed'),
    (select id from sets where short_name = 'ptk'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ptk'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exhaustion'),
    (select id from sets where short_name = 'ptk'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Borrowing 100,000 Arrows'),
    (select id from sets where short_name = 'ptk'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Borrowing the East Wind'),
    (select id from sets where short_name = 'ptk'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forced Retreat'),
    (select id from sets where short_name = 'ptk'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wei Ambush Force'),
    (select id from sets where short_name = 'ptk'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wei Assassins'),
    (select id from sets where short_name = 'ptk'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lu Su, Wu Advisor'),
    (select id from sets where short_name = 'ptk'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corrupt Court Official'),
    (select id from sets where short_name = 'ptk'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ptk'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Broken Dam'),
    (select id from sets where short_name = 'ptk'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wei Scout'),
    (select id from sets where short_name = 'ptk'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flanking Troops'),
    (select id from sets where short_name = 'ptk'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yuan Shao''s Infantry'),
    (select id from sets where short_name = 'ptk'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loyal Retainers'),
    (select id from sets where short_name = 'ptk'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desperate Charge'),
    (select id from sets where short_name = 'ptk'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Renegade Troops'),
    (select id from sets where short_name = 'ptk'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vengeance'),
    (select id from sets where short_name = 'ptk'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blaze'),
    (select id from sets where short_name = 'ptk'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zodiac Tiger'),
    (select id from sets where short_name = 'ptk'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trained Cheetah'),
    (select id from sets where short_name = 'ptk'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zodiac Rabbit'),
    (select id from sets where short_name = 'ptk'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balance of Power'),
    (select id from sets where short_name = 'ptk'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liu Bei, Lord of Shu'),
    (select id from sets where short_name = 'ptk'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deception'),
    (select id from sets where short_name = 'ptk'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hua Tuo, Honored Physician'),
    (select id from sets where short_name = 'ptk'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Riding Red Hare'),
    (select id from sets where short_name = 'ptk'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Huang Zhong, Shu General'),
    (select id from sets where short_name = 'ptk'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Preemptive Strike'),
    (select id from sets where short_name = 'ptk'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overwhelming Forces'),
    (select id from sets where short_name = 'ptk'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imperial Edict'),
    (select id from sets where short_name = 'ptk'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ptk'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Red Cliffs Armada'),
    (select id from sets where short_name = 'ptk'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alert Shu Infantry'),
    (select id from sets where short_name = 'ptk'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shu General'),
    (select id from sets where short_name = 'ptk'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zodiac Dog'),
    (select id from sets where short_name = 'ptk'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eightfold Maze'),
    (select id from sets where short_name = 'ptk'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shu Foot Soldiers'),
    (select id from sets where short_name = 'ptk'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barbarian Horde'),
    (select id from sets where short_name = 'ptk'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Capture of Jingzhou'),
    (select id from sets where short_name = 'ptk'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warrior''s Oath'),
    (select id from sets where short_name = 'ptk'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cao Cao, Lord of Wei'),
    (select id from sets where short_name = 'ptk'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Taoist Hermit'),
    (select id from sets where short_name = 'ptk'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burning Fields'),
    (select id from sets where short_name = 'ptk'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meng Huo''s Horde'),
    (select id from sets where short_name = 'ptk'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stolen Grain'),
    (select id from sets where short_name = 'ptk'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meng Huo, Barbarian King'),
    (select id from sets where short_name = 'ptk'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zuo Ci, the Mocking Sage'),
    (select id from sets where short_name = 'ptk'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ptk'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Counterintelligence'),
    (select id from sets where short_name = 'ptk'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wu Infantry'),
    (select id from sets where short_name = 'ptk'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ptk'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shu Soldier-Farmers'),
    (select id from sets where short_name = 'ptk'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shu Defender'),
    (select id from sets where short_name = 'ptk'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pang Tong, "Young Phoenix"'),
    (select id from sets where short_name = 'ptk'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kongming, "Sleeping Dragon"'),
    (select id from sets where short_name = 'ptk'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wei Night Raiders'),
    (select id from sets where short_name = 'ptk'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spring of Eternal Peace'),
    (select id from sets where short_name = 'ptk'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wu Spy'),
    (select id from sets where short_name = 'ptk'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest Bear'),
    (select id from sets where short_name = 'ptk'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zodiac Horse'),
    (select id from sets where short_name = 'ptk'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zodiac Rat'),
    (select id from sets where short_name = 'ptk'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shu Farmer'),
    (select id from sets where short_name = 'ptk'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Independent Troops'),
    (select id from sets where short_name = 'ptk'),
    '114',
    'common'
) 
 on conflict do nothing;
