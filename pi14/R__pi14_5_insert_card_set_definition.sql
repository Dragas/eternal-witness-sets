insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Wash Out'),
    (select id from sets where short_name = 'pi14'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'pi14'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Acquire'),
    (select id from sets where short_name = 'pi14'),
    '16',
    'rare'
) 
 on conflict do nothing;
