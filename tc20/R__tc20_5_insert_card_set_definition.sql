insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Drake'),
    (select id from sets where short_name = 'tc20'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ability Punchcard'),
    (select id from sets where short_name = 'tc20'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Human'),
    (select id from sets where short_name = 'tc20'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snake'),
    (select id from sets where short_name = 'tc20'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird Illusion'),
    (select id from sets where short_name = 'tc20'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tc20'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insect'),
    (select id from sets where short_name = 'tc20'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tc20'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ability Punchcard'),
    (select id from sets where short_name = 'tc20'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tc20'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure'),
    (select id from sets where short_name = 'tc20'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'tc20'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tc20'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insect'),
    (select id from sets where short_name = 'tc20'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird'),
    (select id from sets where short_name = 'tc20'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tc20'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tc20'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dinosaur Cat'),
    (select id from sets where short_name = 'tc20'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Warrior'),
    (select id from sets where short_name = 'tc20'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tc20'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hydra'),
    (select id from sets where short_name = 'tc20'),
    '12',
    'common'
) 
 on conflict do nothing;
