insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Hold the Perimeter'),
    (select id from sets where short_name = 'cn2'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruul War Chant'),
    (select id from sets where short_name = 'cn2'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hurly-Burly'),
    (select id from sets where short_name = 'cn2'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Queen Marchesa'),
    (select id from sets where short_name = 'cn2'),
    '78',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Akroan Hoplite'),
    (select id from sets where short_name = 'cn2'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Capital Punishment'),
    (select id from sets where short_name = 'cn2'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grenzo, Havoc Raiser'),
    (select id from sets where short_name = 'cn2'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Regicide'),
    (select id from sets where short_name = 'cn2'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gratuitous Violence'),
    (select id from sets where short_name = 'cn2'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burgeoning'),
    (select id from sets where short_name = 'cn2'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noble Banneret'),
    (select id from sets where short_name = 'cn2'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crown-Hunter Hireling'),
    (select id from sets where short_name = 'cn2'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Covenant of Minds'),
    (select id from sets where short_name = 'cn2'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcane Savant'),
    (select id from sets where short_name = 'cn2'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyretic Hunter'),
    (select id from sets where short_name = 'cn2'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Runed Servitor'),
    (select id from sets where short_name = 'cn2'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Horn of Greed'),
    (select id from sets where short_name = 'cn2'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flame Slash'),
    (select id from sets where short_name = 'cn2'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opaline Unicorn'),
    (select id from sets where short_name = 'cn2'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Echoing Boon'),
    (select id from sets where short_name = 'cn2'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spectral Grasp'),
    (select id from sets where short_name = 'cn2'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Voyaging Satyr'),
    (select id from sets where short_name = 'cn2'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kill Shot'),
    (select id from sets where short_name = 'cn2'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = 'cn2'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Subterranean Tremors'),
    (select id from sets where short_name = 'cn2'),
    '58',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Murder'),
    (select id from sets where short_name = 'cn2'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctum Prelate'),
    (select id from sets where short_name = 'cn2'),
    '23',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Doomed Traveler'),
    (select id from sets where short_name = 'cn2'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Messenger Jays'),
    (select id from sets where short_name = 'cn2'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Throne of the High City'),
    (select id from sets where short_name = 'cn2'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Domesticated Hydra'),
    (select id from sets where short_name = 'cn2'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jeering Homunculus'),
    (select id from sets where short_name = 'cn2'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Protector of the Crown'),
    (select id from sets where short_name = 'cn2'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mnemonic Wall'),
    (select id from sets where short_name = 'cn2'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Public Execution'),
    (select id from sets where short_name = 'cn2'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stoneshock Giant'),
    (select id from sets where short_name = 'cn2'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Psychosis Crawler'),
    (select id from sets where short_name = 'cn2'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ember Beast'),
    (select id from sets where short_name = 'cn2'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fang of the Pack'),
    (select id from sets where short_name = 'cn2'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Paliano Vanguard'),
    (select id from sets where short_name = 'cn2'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spire Phantasm'),
    (select id from sets where short_name = 'cn2'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vaporkin'),
    (select id from sets where short_name = 'cn2'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thorn of the Black Rose'),
    (select id from sets where short_name = 'cn2'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk Looter'),
    (select id from sets where short_name = 'cn2'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Tunneler'),
    (select id from sets where short_name = 'cn2'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Farbog Boneflinger'),
    (select id from sets where short_name = 'cn2'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stunt Double'),
    (select id from sets where short_name = 'cn2'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kiln Fiend'),
    (select id from sets where short_name = 'cn2'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burning Wish'),
    (select id from sets where short_name = 'cn2'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shambling Goblin'),
    (select id from sets where short_name = 'cn2'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'cn2'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Entourage of Trest'),
    (select id from sets where short_name = 'cn2'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Palace Sentinels'),
    (select id from sets where short_name = 'cn2'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hexplate Golem'),
    (select id from sets where short_name = 'cn2'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wings of the Guard'),
    (select id from sets where short_name = 'cn2'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marchesa''s Decree'),
    (select id from sets where short_name = 'cn2'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Caller of Gales'),
    (select id from sets where short_name = 'cn2'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lay of the Land'),
    (select id from sets where short_name = 'cn2'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fleeting Distraction'),
    (select id from sets where short_name = 'cn2'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manaplasm'),
    (select id from sets where short_name = 'cn2'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Omenspeaker'),
    (select id from sets where short_name = 'cn2'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gang of Devils'),
    (select id from sets where short_name = 'cn2'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Balloon Brigade'),
    (select id from sets where short_name = 'cn2'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coiling Oracle'),
    (select id from sets where short_name = 'cn2'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hundred-Handed One'),
    (select id from sets where short_name = 'cn2'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duskmantle Seer'),
    (select id from sets where short_name = 'cn2'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leovold, Emissary of Trest'),
    (select id from sets where short_name = 'cn2'),
    '77',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Unnerve'),
    (select id from sets where short_name = 'cn2'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garbage Fire'),
    (select id from sets where short_name = 'cn2'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gleam of Resistance'),
    (select id from sets where short_name = 'cn2'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Repulse'),
    (select id from sets where short_name = 'cn2'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deceiver Exarch'),
    (select id from sets where short_name = 'cn2'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pariah'),
    (select id from sets where short_name = 'cn2'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overrun'),
    (select id from sets where short_name = 'cn2'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Besmirch'),
    (select id from sets where short_name = 'cn2'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guttersnipe'),
    (select id from sets where short_name = 'cn2'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hedron Matrix'),
    (select id from sets where short_name = 'cn2'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Into the Void'),
    (select id from sets where short_name = 'cn2'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inquisition of Kozilek'),
    (select id from sets where short_name = 'cn2'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ballot Broker'),
    (select id from sets where short_name = 'cn2'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Festergloom'),
    (select id from sets where short_name = 'cn2'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pharika''s Mender'),
    (select id from sets where short_name = 'cn2'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Natural Unity'),
    (select id from sets where short_name = 'cn2'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forgotten Ancient'),
    (select id from sets where short_name = 'cn2'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloaked Siren'),
    (select id from sets where short_name = 'cn2'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Twin Bolt'),
    (select id from sets where short_name = 'cn2'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Berserk'),
    (select id from sets where short_name = 'cn2'),
    '175',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Archdemon of Paliano'),
    (select id from sets where short_name = 'cn2'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Regal Behemoth'),
    (select id from sets where short_name = 'cn2'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Netcaster Spider'),
    (select id from sets where short_name = 'cn2'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plummet'),
    (select id from sets where short_name = 'cn2'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windborne Charge'),
    (select id from sets where short_name = 'cn2'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shipwreck Singer'),
    (select id from sets where short_name = 'cn2'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dread Statuary'),
    (select id from sets where short_name = 'cn2'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spirit of the Hearth'),
    (select id from sets where short_name = 'cn2'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghostly Prison'),
    (select id from sets where short_name = 'cn2'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Infest'),
    (select id from sets where short_name = 'cn2'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harvester of Souls'),
    (select id from sets where short_name = 'cn2'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Assemble the Rank and Vile'),
    (select id from sets where short_name = 'cn2'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rogue''s Passage'),
    (select id from sets where short_name = 'cn2'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lieutenants of the Guard'),
    (select id from sets where short_name = 'cn2'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trumpet Blast'),
    (select id from sets where short_name = 'cn2'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fleshbag Marauder'),
    (select id from sets where short_name = 'cn2'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lace with Moonglove'),
    (select id from sets where short_name = 'cn2'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ill-Tempered Cyclops'),
    (select id from sets where short_name = 'cn2'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illusionary Informant'),
    (select id from sets where short_name = 'cn2'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Palace Jailer'),
    (select id from sets where short_name = 'cn2'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hail of Arrows'),
    (select id from sets where short_name = 'cn2'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Bounty'),
    (select id from sets where short_name = 'cn2'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skittering Crustacean'),
    (select id from sets where short_name = 'cn2'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hamletback Goliath'),
    (select id from sets where short_name = 'cn2'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diabolic Tutor'),
    (select id from sets where short_name = 'cn2'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Garrulous Sycophant'),
    (select id from sets where short_name = 'cn2'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vertigo Spawn'),
    (select id from sets where short_name = 'cn2'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Irresistible Prey'),
    (select id from sets where short_name = 'cn2'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brushstrider'),
    (select id from sets where short_name = 'cn2'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tormenting Voice'),
    (select id from sets where short_name = 'cn2'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selvala''s Stampede'),
    (select id from sets where short_name = 'cn2'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyline Despot'),
    (select id from sets where short_name = 'cn2'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deadly Designs'),
    (select id from sets where short_name = 'cn2'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Havengul Vampire'),
    (select id from sets where short_name = 'cn2'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shimmering Grotto'),
    (select id from sets where short_name = 'cn2'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Affa Guard Hound'),
    (select id from sets where short_name = 'cn2'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deputized Protester'),
    (select id from sets where short_name = 'cn2'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grenzo''s Ruffians'),
    (select id from sets where short_name = 'cn2'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Adriana''s Valor'),
    (select id from sets where short_name = 'cn2'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Custodi Lich'),
    (select id from sets where short_name = 'cn2'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Child of Night'),
    (select id from sets where short_name = 'cn2'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gods Willing'),
    (select id from sets where short_name = 'cn2'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raise the Alarm'),
    (select id from sets where short_name = 'cn2'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'cn2'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strength in Numbers'),
    (select id from sets where short_name = 'cn2'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk Skyscout'),
    (select id from sets where short_name = 'cn2'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hallowed Burial'),
    (select id from sets where short_name = 'cn2'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphinx of Magosi'),
    (select id from sets where short_name = 'cn2'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serum Visions'),
    (select id from sets where short_name = 'cn2'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death Wind'),
    (select id from sets where short_name = 'cn2'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kami of the Crescent Moon'),
    (select id from sets where short_name = 'cn2'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dismiss'),
    (select id from sets where short_name = 'cn2'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Juniper Order Ranger'),
    (select id from sets where short_name = 'cn2'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keeper of Keys'),
    (select id from sets where short_name = 'cn2'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stormchaser Chimera'),
    (select id from sets where short_name = 'cn2'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selvala, Heart of the Wilds'),
    (select id from sets where short_name = 'cn2'),
    '70',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Charmbreaker Devils'),
    (select id from sets where short_name = 'cn2'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reviving Dose'),
    (select id from sets where short_name = 'cn2'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Expropriate'),
    (select id from sets where short_name = 'cn2'),
    '30',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Coordinated Assault'),
    (select id from sets where short_name = 'cn2'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'cn2'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast Within'),
    (select id from sets where short_name = 'cn2'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Canal Courier'),
    (select id from sets where short_name = 'cn2'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hollowhenge Spirit'),
    (select id from sets where short_name = 'cn2'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bronze Sable'),
    (select id from sets where short_name = 'cn2'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orchard Elemental'),
    (select id from sets where short_name = 'cn2'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keepsake Gorgon'),
    (select id from sets where short_name = 'cn2'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Illusion of Choice'),
    (select id from sets where short_name = 'cn2'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sovereign''s Realm'),
    (select id from sets where short_name = 'cn2'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Altar''s Reap'),
    (select id from sets where short_name = 'cn2'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daretti, Ingenious Iconoclast'),
    (select id from sets where short_name = 'cn2'),
    '74',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Custodi Peacekeeper'),
    (select id from sets where short_name = 'cn2'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divination'),
    (select id from sets where short_name = 'cn2'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sulfurous Blast'),
    (select id from sets where short_name = 'cn2'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burn Away'),
    (select id from sets where short_name = 'cn2'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smuggler Captain'),
    (select id from sets where short_name = 'cn2'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desertion'),
    (select id from sets where short_name = 'cn2'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avatar of Woe'),
    (select id from sets where short_name = 'cn2'),
    '128',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ogre Sentry'),
    (select id from sets where short_name = 'cn2'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Recruiter of the Guard'),
    (select id from sets where short_name = 'cn2'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiery Fall'),
    (select id from sets where short_name = 'cn2'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kaya, Ghost Assassin'),
    (select id from sets where short_name = 'cn2'),
    '222',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nessian Asp'),
    (select id from sets where short_name = 'cn2'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knights of the Black Rose'),
    (select id from sets where short_name = 'cn2'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blood-Toll Harpy'),
    (select id from sets where short_name = 'cn2'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spy Kit'),
    (select id from sets where short_name = 'cn2'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghostly Possession'),
    (select id from sets where short_name = 'cn2'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Summoner''s Bond'),
    (select id from sets where short_name = 'cn2'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stromkirk Patrol'),
    (select id from sets where short_name = 'cn2'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragonlair Spider'),
    (select id from sets where short_name = 'cn2'),
    '201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Caller of the Untamed'),
    (select id from sets where short_name = 'cn2'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carnage Gladiator'),
    (select id from sets where short_name = 'cn2'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Arena'),
    (select id from sets where short_name = 'cn2'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Splitting Slime'),
    (select id from sets where short_name = 'cn2'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coveted Peacock'),
    (select id from sets where short_name = 'cn2'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Platinum Angel'),
    (select id from sets where short_name = 'cn2'),
    '214',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Driver of the Dead'),
    (select id from sets where short_name = 'cn2'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Throne Warden'),
    (select id from sets where short_name = 'cn2'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emissary''s Ploy'),
    (select id from sets where short_name = 'cn2'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Incendiary Dissent'),
    (select id from sets where short_name = 'cn2'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mausoleum Turnkey'),
    (select id from sets where short_name = 'cn2'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hired Heist'),
    (select id from sets where short_name = 'cn2'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leovold''s Operative'),
    (select id from sets where short_name = 'cn2'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prey Upon'),
    (select id from sets where short_name = 'cn2'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fade into Antiquity'),
    (select id from sets where short_name = 'cn2'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kitesail'),
    (select id from sets where short_name = 'cn2'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravenous Leucrocota'),
    (select id from sets where short_name = 'cn2'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Borderland Explorer'),
    (select id from sets where short_name = 'cn2'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guardian of the Gateless'),
    (select id from sets where short_name = 'cn2'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Griffin'),
    (select id from sets where short_name = 'cn2'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Traumatic Visions'),
    (select id from sets where short_name = 'cn2'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Custodi Soulcaller'),
    (select id from sets where short_name = 'cn2'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Absorb Vis'),
    (select id from sets where short_name = 'cn2'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sinuous Vermin'),
    (select id from sets where short_name = 'cn2'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Animus of Predation'),
    (select id from sets where short_name = 'cn2'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hymn of the Wilds'),
    (select id from sets where short_name = 'cn2'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Menagerie Liberator'),
    (select id from sets where short_name = 'cn2'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Adriana, Captain of the Guard'),
    (select id from sets where short_name = 'cn2'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ascended Lawmage'),
    (select id from sets where short_name = 'cn2'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kaya, Ghost Assassin'),
    (select id from sets where short_name = 'cn2'),
    '75',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sangromancer'),
    (select id from sets where short_name = 'cn2'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Racketeer'),
    (select id from sets where short_name = 'cn2'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guul Draz Specter'),
    (select id from sets where short_name = 'cn2'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bonds of Quicksilver'),
    (select id from sets where short_name = 'cn2'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zealous Strike'),
    (select id from sets where short_name = 'cn2'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Pair'),
    (select id from sets where short_name = 'cn2'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Explosive Vegetation'),
    (select id from sets where short_name = 'cn2'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volatile Chimera'),
    (select id from sets where short_name = 'cn2'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exotic Orchard'),
    (select id from sets where short_name = 'cn2'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faith''s Reward'),
    (select id from sets where short_name = 'cn2'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Show and Tell'),
    (select id from sets where short_name = 'cn2'),
    '121',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Copperhorn Scout'),
    (select id from sets where short_name = 'cn2'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Followed Footsteps'),
    (select id from sets where short_name = 'cn2'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Weight Advantage'),
    (select id from sets where short_name = 'cn2'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = 'cn2'),
    '176',
    'rare'
) 
 on conflict do nothing;
