insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Knight Ally'),
    (select id from sets where short_name = 'tbfz'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plant'),
    (select id from sets where short_name = 'tbfz'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tbfz'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis Reignited Emblem'),
    (select id from sets where short_name = 'tbfz'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Octopus'),
    (select id from sets where short_name = 'tbfz'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi'),
    (select id from sets where short_name = 'tbfz'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kiora, Master of the Depths Emblem'),
    (select id from sets where short_name = 'tbfz'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Scion'),
    (select id from sets where short_name = 'tbfz'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Scion'),
    (select id from sets where short_name = 'tbfz'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Scion'),
    (select id from sets where short_name = 'tbfz'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Ally'),
    (select id from sets where short_name = 'tbfz'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gideon, Ally of Zendikar Emblem'),
    (select id from sets where short_name = 'tbfz'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tbfz'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tbfz'),
    '11',
    'common'
) 
 on conflict do nothing;
