insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Weird // Goblin'),
    (select id from sets where short_name = 'tgk1'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voja // Saproling'),
    (select id from sets where short_name = 'tgk1'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin // Soldier'),
    (select id from sets where short_name = 'tgk1'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier // Soldier'),
    (select id from sets where short_name = 'tgk1'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Copy // Horror'),
    (select id from sets where short_name = 'tgk1'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling // Insect'),
    (select id from sets where short_name = 'tgk1'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling // Elf Knight'),
    (select id from sets where short_name = 'tgk1'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurm // Saproling'),
    (select id from sets where short_name = 'tgk1'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental // Centaur'),
    (select id from sets where short_name = 'tgk1'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier // Goblin'),
    (select id from sets where short_name = 'tgk1'),
    '6',
    'common'
) 
 on conflict do nothing;
