    insert into mtgcard(name) values ('Horde of Boggarts') on conflict do nothing;
    insert into mtgcard(name) values ('Dawnglow Infusion') on conflict do nothing;
    insert into mtgcard(name) values ('Mountain') on conflict do nothing;
    insert into mtgcard(name) values ('Order of Whiteclay') on conflict do nothing;
    insert into mtgcard(name) values ('Scuttlemutt') on conflict do nothing;
    insert into mtgcard(name) values ('Chainbreaker') on conflict do nothing;
    insert into mtgcard(name) values ('Loamdragger Giant') on conflict do nothing;
    insert into mtgcard(name) values ('Runes of the Deus') on conflict do nothing;
    insert into mtgcard(name) values ('Fire-Lit Thicket') on conflict do nothing;
    insert into mtgcard(name) values ('Swans of Bryn Argoll') on conflict do nothing;
    insert into mtgcard(name) values ('Cerulean Wisps') on conflict do nothing;
    insert into mtgcard(name) values ('Crowd of Cinders') on conflict do nothing;
    insert into mtgcard(name) values ('Giantbaiting') on conflict do nothing;
    insert into mtgcard(name) values ('Revelsong Horn') on conflict do nothing;
    insert into mtgcard(name) values ('Reknit') on conflict do nothing;
    insert into mtgcard(name) values ('Umbral Mantle') on conflict do nothing;
    insert into mtgcard(name) values ('Greater Auramancy') on conflict do nothing;
    insert into mtgcard(name) values ('Twilight Shepherd') on conflict do nothing;
    insert into mtgcard(name) values ('Goldenglow Moth') on conflict do nothing;
    insert into mtgcard(name) values ('River''s Grasp') on conflict do nothing;
    insert into mtgcard(name) values ('Silkbind Faerie') on conflict do nothing;
    insert into mtgcard(name) values ('Firespout') on conflict do nothing;
    insert into mtgcard(name) values ('Kinscaer Harpoonist') on conflict do nothing;
    insert into mtgcard(name) values ('Corrosive Mentor') on conflict do nothing;
    insert into mtgcard(name) values ('Safehold Elite') on conflict do nothing;
    insert into mtgcard(name) values ('Splitting Headache') on conflict do nothing;
    insert into mtgcard(name) values ('Smolder Initiate') on conflict do nothing;
    insert into mtgcard(name) values ('Spiteful Visions') on conflict do nothing;
    insert into mtgcard(name) values ('Island') on conflict do nothing;
    insert into mtgcard(name) values ('Plains') on conflict do nothing;
    insert into mtgcard(name) values ('Boon Reflection') on conflict do nothing;
    insert into mtgcard(name) values ('Sinking Feeling') on conflict do nothing;
    insert into mtgcard(name) values ('Trip Noose') on conflict do nothing;
    insert into mtgcard(name) values ('Swamp') on conflict do nothing;
    insert into mtgcard(name) values ('Mana Reflection') on conflict do nothing;
    insert into mtgcard(name) values ('Thistledown Liege') on conflict do nothing;
    insert into mtgcard(name) values ('Cursecatcher') on conflict do nothing;
    insert into mtgcard(name) values ('Knacksaw Clique') on conflict do nothing;
    insert into mtgcard(name) values ('Last Breath') on conflict do nothing;
    insert into mtgcard(name) values ('Cinderbones') on conflict do nothing;
    insert into mtgcard(name) values ('Plague of Vermin') on conflict do nothing;
    insert into mtgcard(name) values ('Nurturer Initiate') on conflict do nothing;
    insert into mtgcard(name) values ('Kulrath Knight') on conflict do nothing;
    insert into mtgcard(name) values ('Oona, Queen of the Fae') on conflict do nothing;
    insert into mtgcard(name) values ('Reflecting Pool') on conflict do nothing;
    insert into mtgcard(name) values ('Rage Reflection') on conflict do nothing;
    insert into mtgcard(name) values ('Inkfathom Infiltrator') on conflict do nothing;
    insert into mtgcard(name) values ('Drowner Initiate') on conflict do nothing;
    insert into mtgcard(name) values ('Howl of the Night Pack') on conflict do nothing;
    insert into mtgcard(name) values ('Loch Korrigan') on conflict do nothing;
    insert into mtgcard(name) values ('Inkfathom Witch') on conflict do nothing;
    insert into mtgcard(name) values ('Everlasting Torment') on conflict do nothing;
    insert into mtgcard(name) values ('Fossil Find') on conflict do nothing;
    insert into mtgcard(name) values ('Boartusk Liege') on conflict do nothing;
    insert into mtgcard(name) values ('Flow of Ideas') on conflict do nothing;
    insert into mtgcard(name) values ('Wild Swing') on conflict do nothing;
    insert into mtgcard(name) values ('Safehold Duo') on conflict do nothing;
    insert into mtgcard(name) values ('Wildslayer Elves') on conflict do nothing;
    insert into mtgcard(name) values ('Rosheen Meanderer') on conflict do nothing;
    insert into mtgcard(name) values ('Thornwatch Scarecrow') on conflict do nothing;
    insert into mtgcard(name) values ('Barrenton Cragtreads') on conflict do nothing;
    insert into mtgcard(name) values ('Repel Intruders') on conflict do nothing;
    insert into mtgcard(name) values ('Sygg, River Cutthroat') on conflict do nothing;
    insert into mtgcard(name) values ('Grim Poppet') on conflict do nothing;
    insert into mtgcard(name) values ('Scrapbasket') on conflict do nothing;
    insert into mtgcard(name) values ('Mass Calcify') on conflict do nothing;
    insert into mtgcard(name) values ('Mystic Gate') on conflict do nothing;
    insert into mtgcard(name) values ('Fate Transfer') on conflict do nothing;
    insert into mtgcard(name) values ('Spiteflame Witch') on conflict do nothing;
    insert into mtgcard(name) values ('Woodfall Primus') on conflict do nothing;
    insert into mtgcard(name) values ('Blazethorn Scarecrow') on conflict do nothing;
    insert into mtgcard(name) values ('Dramatic Entrance') on conflict do nothing;
    insert into mtgcard(name) values ('Foxfire Oak') on conflict do nothing;
    insert into mtgcard(name) values ('Wicker Warcrawler') on conflict do nothing;
    insert into mtgcard(name) values ('Pili-Pala') on conflict do nothing;
    insert into mtgcard(name) values ('Parapet Watchers') on conflict do nothing;
    insert into mtgcard(name) values ('Steel of the Godhead') on conflict do nothing;
    insert into mtgcard(name) values ('Sootstoke Kindler') on conflict do nothing;
    insert into mtgcard(name) values ('Rattleblaze Scarecrow') on conflict do nothing;
    insert into mtgcard(name) values ('Augury Adept') on conflict do nothing;
    insert into mtgcard(name) values ('Torrent of Souls') on conflict do nothing;
    insert into mtgcard(name) values ('Prismwake Merrow') on conflict do nothing;
    insert into mtgcard(name) values ('Forest') on conflict do nothing;
    insert into mtgcard(name) values ('Reaper King') on conflict do nothing;
    insert into mtgcard(name) values ('Manamorphose') on conflict do nothing;
    insert into mtgcard(name) values ('Enchanted Evening') on conflict do nothing;
    insert into mtgcard(name) values ('Valleymaker') on conflict do nothing;
    insert into mtgcard(name) values ('Tower Above') on conflict do nothing;
    insert into mtgcard(name) values ('Strip Bare') on conflict do nothing;
    insert into mtgcard(name) values ('Madblind Mountain') on conflict do nothing;
    insert into mtgcard(name) values ('Witherscale Wurm') on conflict do nothing;
    insert into mtgcard(name) values ('Biting Tether') on conflict do nothing;
    insert into mtgcard(name) values ('Aphotic Wisps') on conflict do nothing;
    insert into mtgcard(name) values ('Wort, the Raidmother') on conflict do nothing;
    insert into mtgcard(name) values ('Mossbridge Troll') on conflict do nothing;
    insert into mtgcard(name) values ('Shield of the Oversoul') on conflict do nothing;
    insert into mtgcard(name) values ('Hollowborn Barghest') on conflict do nothing;
    insert into mtgcard(name) values ('Presence of Gond') on conflict do nothing;
    insert into mtgcard(name) values ('Wheel of Sun and Moon') on conflict do nothing;
    insert into mtgcard(name) values ('Blistering Dieflyn') on conflict do nothing;
    insert into mtgcard(name) values ('Morselhoarder') on conflict do nothing;
    insert into mtgcard(name) values ('Rhys the Redeemed') on conflict do nothing;
    insert into mtgcard(name) values ('Gloomwidow''s Feast') on conflict do nothing;
    insert into mtgcard(name) values ('Furystoke Giant') on conflict do nothing;
    insert into mtgcard(name) values ('Old Ghastbark') on conflict do nothing;
    insert into mtgcard(name) values ('Heartmender') on conflict do nothing;
    insert into mtgcard(name) values ('Watchwing Scarecrow') on conflict do nothing;
    insert into mtgcard(name) values ('Glamer Spinners') on conflict do nothing;
    insert into mtgcard(name) values ('Curse of Chains') on conflict do nothing;
    insert into mtgcard(name) values ('Windbrisk Raptor') on conflict do nothing;
    insert into mtgcard(name) values ('Spectral Procession') on conflict do nothing;
    insert into mtgcard(name) values ('Consign to Dream') on conflict do nothing;
    insert into mtgcard(name) values ('Gravelgill Axeshark') on conflict do nothing;
    insert into mtgcard(name) values ('Murderous Redcap') on conflict do nothing;
    insert into mtgcard(name) values ('Memory Sluice') on conflict do nothing;
    insert into mtgcard(name) values ('Wingrattle Scarecrow') on conflict do nothing;
    insert into mtgcard(name) values ('Safewright Quest') on conflict do nothing;
    insert into mtgcard(name) values ('Flourishing Defenses') on conflict do nothing;
    insert into mtgcard(name) values ('Bloodmark Mentor') on conflict do nothing;
    insert into mtgcard(name) values ('Boggart Ram-Gang') on conflict do nothing;
    insert into mtgcard(name) values ('Cinderhaze Wretch') on conflict do nothing;
    insert into mtgcard(name) values ('Moonring Island') on conflict do nothing;
    insert into mtgcard(name) values ('Beseech the Queen') on conflict do nothing;
    insert into mtgcard(name) values ('Helm of the Ghastlord') on conflict do nothing;
    insert into mtgcard(name) values ('Scuzzback Marauders') on conflict do nothing;
    insert into mtgcard(name) values ('Tyrannize') on conflict do nothing;
    insert into mtgcard(name) values ('Power of Fire') on conflict do nothing;
    insert into mtgcard(name) values ('Viridescent Wisps') on conflict do nothing;
    insert into mtgcard(name) values ('Mercy Killing') on conflict do nothing;
    insert into mtgcard(name) values ('Puppeteer Clique') on conflict do nothing;
    insert into mtgcard(name) values ('Scarscale Ritual') on conflict do nothing;
    insert into mtgcard(name) values ('Aethertow') on conflict do nothing;
    insert into mtgcard(name) values ('Bloodshed Fever') on conflict do nothing;
    insert into mtgcard(name) values ('Illuminated Folio') on conflict do nothing;
    insert into mtgcard(name) values ('Jaws of Stone') on conflict do nothing;
    insert into mtgcard(name) values ('Traitor''s Roar') on conflict do nothing;
    insert into mtgcard(name) values ('Turn to Mist') on conflict do nothing;
    insert into mtgcard(name) values ('Puresight Merrow') on conflict do nothing;
    insert into mtgcard(name) values ('Gloomlance') on conflict do nothing;
    insert into mtgcard(name) values ('Kitchen Finks') on conflict do nothing;
    insert into mtgcard(name) values ('Wooded Bastion') on conflict do nothing;
    insert into mtgcard(name) values ('Godhead of Awe') on conflict do nothing;
    insert into mtgcard(name) values ('Kithkin Shielddare') on conflict do nothing;
    insert into mtgcard(name) values ('Mistmeadow Witch') on conflict do nothing;
    insert into mtgcard(name) values ('Smash to Smithereens') on conflict do nothing;
    insert into mtgcard(name) values ('Burn Trail') on conflict do nothing;
    insert into mtgcard(name) values ('Kithkin Rabble') on conflict do nothing;
    insert into mtgcard(name) values ('Ashenmoor Gouger') on conflict do nothing;
    insert into mtgcard(name) values ('Manaforge Cinder') on conflict do nothing;
    insert into mtgcard(name) values ('Knollspine Invocation') on conflict do nothing;
    insert into mtgcard(name) values ('Devoted Druid') on conflict do nothing;
    insert into mtgcard(name) values ('Puca''s Mischief') on conflict do nothing;
    insert into mtgcard(name) values ('Heap Doll') on conflict do nothing;
    insert into mtgcard(name) values ('Torpor Dust') on conflict do nothing;
    insert into mtgcard(name) values ('Barrenton Medic') on conflict do nothing;
    insert into mtgcard(name) values ('Oversoul of Dusk') on conflict do nothing;
    insert into mtgcard(name) values ('Tattermunge Maniac') on conflict do nothing;
    insert into mtgcard(name) values ('Lurebound Scarecrow') on conflict do nothing;
    insert into mtgcard(name) values ('Farhaven Elf') on conflict do nothing;
    insert into mtgcard(name) values ('Ghastlord of Fugue') on conflict do nothing;
    insert into mtgcard(name) values ('Hungry Spriggan') on conflict do nothing;
    insert into mtgcard(name) values ('Crabapple Cohort') on conflict do nothing;
    insert into mtgcard(name) values ('Dire Undercurrents') on conflict do nothing;
    insert into mtgcard(name) values ('Scuzzback Scrapper') on conflict do nothing;
    insert into mtgcard(name) values ('Plumeveil') on conflict do nothing;
    insert into mtgcard(name) values ('Mudbrawler Cohort') on conflict do nothing;
    insert into mtgcard(name) values ('Faerie Swarm') on conflict do nothing;
    insert into mtgcard(name) values ('Elemental Mastery') on conflict do nothing;
    insert into mtgcard(name) values ('Elsewhere Flask') on conflict do nothing;
    insert into mtgcard(name) values ('Runed Halo') on conflict do nothing;
    insert into mtgcard(name) values ('Deus of Calamity') on conflict do nothing;
    insert into mtgcard(name) values ('Savor the Moment') on conflict do nothing;
    insert into mtgcard(name) values ('Glen Elendra Liege') on conflict do nothing;
    insert into mtgcard(name) values ('River Kelpie') on conflict do nothing;
    insert into mtgcard(name) values ('Grief Tyrant') on conflict do nothing;
    insert into mtgcard(name) values ('Oona''s Gatewarden') on conflict do nothing;
    insert into mtgcard(name) values ('Medicine Runner') on conflict do nothing;
    insert into mtgcard(name) values ('Merrow Wavebreakers') on conflict do nothing;
    insert into mtgcard(name) values ('Zealous Guardian') on conflict do nothing;
    insert into mtgcard(name) values ('Gloomwidow') on conflict do nothing;
    insert into mtgcard(name) values ('Ashenmoor Liege') on conflict do nothing;
    insert into mtgcard(name) values ('Fists of the Demigod') on conflict do nothing;
    insert into mtgcard(name) values ('Apothecary Initiate') on conflict do nothing;
    insert into mtgcard(name) values ('Din of the Fireherd') on conflict do nothing;
    insert into mtgcard(name) values ('Memory Plunder') on conflict do nothing;
    insert into mtgcard(name) values ('Emberstrike Duo') on conflict do nothing;
    insert into mtgcard(name) values ('Sunken Ruins') on conflict do nothing;
    insert into mtgcard(name) values ('Rune-Cervin Rider') on conflict do nothing;
    insert into mtgcard(name) values ('Ghastly Discovery') on conflict do nothing;
    insert into mtgcard(name) values ('Oracle of Nectars') on conflict do nothing;
    insert into mtgcard(name) values ('Seedcradle Witch') on conflict do nothing;
    insert into mtgcard(name) values ('Flame Javelin') on conflict do nothing;
    insert into mtgcard(name) values ('Crimson Wisps') on conflict do nothing;
    insert into mtgcard(name) values ('Leech Bonder') on conflict do nothing;
    insert into mtgcard(name) values ('Inescapable Brute') on conflict do nothing;
    insert into mtgcard(name) values ('Wanderbrine Rootcutters') on conflict do nothing;
    insert into mtgcard(name) values ('Roughshod Mentor') on conflict do nothing;
    insert into mtgcard(name) values ('Wasp Lancer') on conflict do nothing;
    insert into mtgcard(name) values ('Tattermunge Duo') on conflict do nothing;
    insert into mtgcard(name) values ('Ember Gale') on conflict do nothing;
    insert into mtgcard(name) values ('Dusk Urchins') on conflict do nothing;
    insert into mtgcard(name) values ('Blowfly Infestation') on conflict do nothing;
    insert into mtgcard(name) values ('Vexing Shusher') on conflict do nothing;
    insert into mtgcard(name) values ('Mistveil Plains') on conflict do nothing;
    insert into mtgcard(name) values ('Leechridden Swamp') on conflict do nothing;
    insert into mtgcard(name) values ('Whimwader') on conflict do nothing;
    insert into mtgcard(name) values ('Mistmeadow Skulk') on conflict do nothing;
    insert into mtgcard(name) values ('Slinking Giant') on conflict do nothing;
    insert into mtgcard(name) values ('Scar') on conflict do nothing;
    insert into mtgcard(name) values ('Drove of Elves') on conflict do nothing;
    insert into mtgcard(name) values ('Hollowsage') on conflict do nothing;
    insert into mtgcard(name) values ('Disturbing Plot') on conflict do nothing;
    insert into mtgcard(name) values ('Thoughtweft Gambit') on conflict do nothing;
    insert into mtgcard(name) values ('Merrow Grimeblotter') on conflict do nothing;
    insert into mtgcard(name) values ('Raven''s Run Dragoon') on conflict do nothing;
    insert into mtgcard(name) values ('Spell Syphon') on conflict do nothing;
    insert into mtgcard(name) values ('Wound Reflection') on conflict do nothing;
    insert into mtgcard(name) values ('Wilt-Leaf Cavaliers') on conflict do nothing;
    insert into mtgcard(name) values ('Fulminator Mage') on conflict do nothing;
    insert into mtgcard(name) values ('Painter''s Servant') on conflict do nothing;
    insert into mtgcard(name) values ('Resplendent Mentor') on conflict do nothing;
    insert into mtgcard(name) values ('Niveous Wisps') on conflict do nothing;
    insert into mtgcard(name) values ('Knollspine Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Blight Sickle') on conflict do nothing;
    insert into mtgcard(name) values ('Put Away') on conflict do nothing;
    insert into mtgcard(name) values ('Intimidator Initiate') on conflict do nothing;
    insert into mtgcard(name) values ('Elvish Hexhunter') on conflict do nothing;
    insert into mtgcard(name) values ('Thought Reflection') on conflict do nothing;
    insert into mtgcard(name) values ('Graven Cairns') on conflict do nothing;
    insert into mtgcard(name) values ('Tattermunge Witch') on conflict do nothing;
    insert into mtgcard(name) values ('Ballynock Cohort') on conflict do nothing;
    insert into mtgcard(name) values ('Prison Term') on conflict do nothing;
    insert into mtgcard(name) values ('Inquisitor''s Snare') on conflict do nothing;
    insert into mtgcard(name) values ('Polluted Bonds') on conflict do nothing;
    insert into mtgcard(name) values ('Cauldron of Souls') on conflict do nothing;
    insert into mtgcard(name) values ('Cragganwick Cremator') on conflict do nothing;
    insert into mtgcard(name) values ('Safehold Sentry') on conflict do nothing;
    insert into mtgcard(name) values ('Dream Salvage') on conflict do nothing;
    insert into mtgcard(name) values ('Briarberry Cohort') on conflict do nothing;
    insert into mtgcard(name) values ('Puncture Bolt') on conflict do nothing;
    insert into mtgcard(name) values ('Poison the Well') on conflict do nothing;
    insert into mtgcard(name) values ('Deep-Slumber Titan') on conflict do nothing;
    insert into mtgcard(name) values ('Mudbrawler Raiders') on conflict do nothing;
    insert into mtgcard(name) values ('Barkshell Blessing') on conflict do nothing;
    insert into mtgcard(name) values ('Demigod of Revenge') on conflict do nothing;
    insert into mtgcard(name) values ('Woeleecher') on conflict do nothing;
    insert into mtgcard(name) values ('Gleeful Sabotage') on conflict do nothing;
    insert into mtgcard(name) values ('Sapseep Forest') on conflict do nothing;
    insert into mtgcard(name) values ('Torture') on conflict do nothing;
    insert into mtgcard(name) values ('Toil to Renown') on conflict do nothing;
    insert into mtgcard(name) values ('Thistledown Duo') on conflict do nothing;
    insert into mtgcard(name) values ('Boggart Arsonists') on conflict do nothing;
    insert into mtgcard(name) values ('Worldpurge') on conflict do nothing;
    insert into mtgcard(name) values ('Juvenile Gloomwidow') on conflict do nothing;
    insert into mtgcard(name) values ('Advice from the Fae') on conflict do nothing;
    insert into mtgcard(name) values ('Rustrazor Butcher') on conflict do nothing;
    insert into mtgcard(name) values ('Spawnwrithe') on conflict do nothing;
    insert into mtgcard(name) values ('Pyre Charger') on conflict do nothing;
    insert into mtgcard(name) values ('Mirrorweave') on conflict do nothing;
    insert into mtgcard(name) values ('Midnight Banshee') on conflict do nothing;
    insert into mtgcard(name) values ('Raking Canopy') on conflict do nothing;
    insert into mtgcard(name) values ('Tatterkite') on conflict do nothing;
    insert into mtgcard(name) values ('Guttural Response') on conflict do nothing;
    insert into mtgcard(name) values ('Sickle Ripper') on conflict do nothing;
    insert into mtgcard(name) values ('Wilt-Leaf Liege') on conflict do nothing;
    insert into mtgcard(name) values ('Gnarled Effigy') on conflict do nothing;
    insert into mtgcard(name) values ('Prismatic Omen') on conflict do nothing;
    insert into mtgcard(name) values ('Isleback Spawn') on conflict do nothing;
    insert into mtgcard(name) values ('Deepchannel Mentor') on conflict do nothing;
    insert into mtgcard(name) values ('Faerie Macabre') on conflict do nothing;
    insert into mtgcard(name) values ('Somnomancer') on conflict do nothing;
    insert into mtgcard(name) values ('Ashenmoor Cohort') on conflict do nothing;
    insert into mtgcard(name) values ('Cemetery Puca') on conflict do nothing;
    insert into mtgcard(name) values ('Mine Excavation') on conflict do nothing;
    insert into mtgcard(name) values ('Impromptu Raid') on conflict do nothing;
    insert into mtgcard(name) values ('Incremental Blight') on conflict do nothing;
    insert into mtgcard(name) values ('Gravelgill Duo') on conflict do nothing;
    insert into mtgcard(name) values ('Corrupt') on conflict do nothing;
    insert into mtgcard(name) values ('Sootwalkers') on conflict do nothing;
    insert into mtgcard(name) values ('Counterbore') on conflict do nothing;
    insert into mtgcard(name) values ('Rite of Consumption') on conflict do nothing;
    insert into mtgcard(name) values ('Armored Ascension') on conflict do nothing;
    insert into mtgcard(name) values ('Cultbrand Cinder') on conflict do nothing;
    insert into mtgcard(name) values ('Pale Wayfarer') on conflict do nothing;
    insert into mtgcard(name) values ('Lockjaw Snapper') on conflict do nothing;
    insert into mtgcard(name) values ('Fracturing Gust') on conflict do nothing;
