insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Horde of Boggarts'),
    (select id from sets where short_name = 'shm'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dawnglow Infusion'),
    (select id from sets where short_name = 'shm'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'shm'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Order of Whiteclay'),
    (select id from sets where short_name = 'shm'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scuttlemutt'),
    (select id from sets where short_name = 'shm'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chainbreaker'),
    (select id from sets where short_name = 'shm'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loamdragger Giant'),
    (select id from sets where short_name = 'shm'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runes of the Deus'),
    (select id from sets where short_name = 'shm'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire-Lit Thicket'),
    (select id from sets where short_name = 'shm'),
    '271',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swans of Bryn Argoll'),
    (select id from sets where short_name = 'shm'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cerulean Wisps'),
    (select id from sets where short_name = 'shm'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crowd of Cinders'),
    (select id from sets where short_name = 'shm'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giantbaiting'),
    (select id from sets where short_name = 'shm'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Revelsong Horn'),
    (select id from sets where short_name = 'shm'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reknit'),
    (select id from sets where short_name = 'shm'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Umbral Mantle'),
    (select id from sets where short_name = 'shm'),
    '267',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Greater Auramancy'),
    (select id from sets where short_name = 'shm'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twilight Shepherd'),
    (select id from sets where short_name = 'shm'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goldenglow Moth'),
    (select id from sets where short_name = 'shm'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'River''s Grasp'),
    (select id from sets where short_name = 'shm'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silkbind Faerie'),
    (select id from sets where short_name = 'shm'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Firespout'),
    (select id from sets where short_name = 'shm'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kinscaer Harpoonist'),
    (select id from sets where short_name = 'shm'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corrosive Mentor'),
    (select id from sets where short_name = 'shm'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Safehold Elite'),
    (select id from sets where short_name = 'shm'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Splitting Headache'),
    (select id from sets where short_name = 'shm'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smolder Initiate'),
    (select id from sets where short_name = 'shm'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spiteful Visions'),
    (select id from sets where short_name = 'shm'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'shm'),
    '288',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'shm'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boon Reflection'),
    (select id from sets where short_name = 'shm'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sinking Feeling'),
    (select id from sets where short_name = 'shm'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'shm'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trip Noose'),
    (select id from sets where short_name = 'shm'),
    '266',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'shm'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Reflection'),
    (select id from sets where short_name = 'shm'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thistledown Liege'),
    (select id from sets where short_name = 'shm'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cursecatcher'),
    (select id from sets where short_name = 'shm'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knacksaw Clique'),
    (select id from sets where short_name = 'shm'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Last Breath'),
    (select id from sets where short_name = 'shm'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cinderbones'),
    (select id from sets where short_name = 'shm'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plague of Vermin'),
    (select id from sets where short_name = 'shm'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nurturer Initiate'),
    (select id from sets where short_name = 'shm'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kulrath Knight'),
    (select id from sets where short_name = 'shm'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oona, Queen of the Fae'),
    (select id from sets where short_name = 'shm'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reflecting Pool'),
    (select id from sets where short_name = 'shm'),
    '278★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rage Reflection'),
    (select id from sets where short_name = 'shm'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inkfathom Infiltrator'),
    (select id from sets where short_name = 'shm'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drowner Initiate'),
    (select id from sets where short_name = 'shm'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'shm'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howl of the Night Pack'),
    (select id from sets where short_name = 'shm'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loch Korrigan'),
    (select id from sets where short_name = 'shm'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inkfathom Witch'),
    (select id from sets where short_name = 'shm'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Everlasting Torment'),
    (select id from sets where short_name = 'shm'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fossil Find'),
    (select id from sets where short_name = 'shm'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boartusk Liege'),
    (select id from sets where short_name = 'shm'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flow of Ideas'),
    (select id from sets where short_name = 'shm'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Swing'),
    (select id from sets where short_name = 'shm'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Safehold Duo'),
    (select id from sets where short_name = 'shm'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wildslayer Elves'),
    (select id from sets where short_name = 'shm'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rosheen Meanderer'),
    (select id from sets where short_name = 'shm'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thornwatch Scarecrow'),
    (select id from sets where short_name = 'shm'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barrenton Cragtreads'),
    (select id from sets where short_name = 'shm'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Repel Intruders'),
    (select id from sets where short_name = 'shm'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sygg, River Cutthroat'),
    (select id from sets where short_name = 'shm'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'shm'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grim Poppet'),
    (select id from sets where short_name = 'shm'),
    '252',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scrapbasket'),
    (select id from sets where short_name = 'shm'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reflecting Pool'),
    (select id from sets where short_name = 'shm'),
    '278',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mass Calcify'),
    (select id from sets where short_name = 'shm'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystic Gate'),
    (select id from sets where short_name = 'shm'),
    '277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fate Transfer'),
    (select id from sets where short_name = 'shm'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spiteflame Witch'),
    (select id from sets where short_name = 'shm'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'shm'),
    '290',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woodfall Primus'),
    (select id from sets where short_name = 'shm'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blazethorn Scarecrow'),
    (select id from sets where short_name = 'shm'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dramatic Entrance'),
    (select id from sets where short_name = 'shm'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foxfire Oak'),
    (select id from sets where short_name = 'shm'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wicker Warcrawler'),
    (select id from sets where short_name = 'shm'),
    '269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pili-Pala'),
    (select id from sets where short_name = 'shm'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Parapet Watchers'),
    (select id from sets where short_name = 'shm'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steel of the Godhead'),
    (select id from sets where short_name = 'shm'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sootstoke Kindler'),
    (select id from sets where short_name = 'shm'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rattleblaze Scarecrow'),
    (select id from sets where short_name = 'shm'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Augury Adept'),
    (select id from sets where short_name = 'shm'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Torrent of Souls'),
    (select id from sets where short_name = 'shm'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prismwake Merrow'),
    (select id from sets where short_name = 'shm'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'shm'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reaper King'),
    (select id from sets where short_name = 'shm'),
    '260',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Manamorphose'),
    (select id from sets where short_name = 'shm'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enchanted Evening'),
    (select id from sets where short_name = 'shm'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Valleymaker'),
    (select id from sets where short_name = 'shm'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tower Above'),
    (select id from sets where short_name = 'shm'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Strip Bare'),
    (select id from sets where short_name = 'shm'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Madblind Mountain'),
    (select id from sets where short_name = 'shm'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Witherscale Wurm'),
    (select id from sets where short_name = 'shm'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Biting Tether'),
    (select id from sets where short_name = 'shm'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aphotic Wisps'),
    (select id from sets where short_name = 'shm'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wort, the Raidmother'),
    (select id from sets where short_name = 'shm'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mossbridge Troll'),
    (select id from sets where short_name = 'shm'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shield of the Oversoul'),
    (select id from sets where short_name = 'shm'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hollowborn Barghest'),
    (select id from sets where short_name = 'shm'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Presence of Gond'),
    (select id from sets where short_name = 'shm'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wheel of Sun and Moon'),
    (select id from sets where short_name = 'shm'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blistering Dieflyn'),
    (select id from sets where short_name = 'shm'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Morselhoarder'),
    (select id from sets where short_name = 'shm'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhys the Redeemed'),
    (select id from sets where short_name = 'shm'),
    '237',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gloomwidow''s Feast'),
    (select id from sets where short_name = 'shm'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Furystoke Giant'),
    (select id from sets where short_name = 'shm'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Old Ghastbark'),
    (select id from sets where short_name = 'shm'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heartmender'),
    (select id from sets where short_name = 'shm'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Watchwing Scarecrow'),
    (select id from sets where short_name = 'shm'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glamer Spinners'),
    (select id from sets where short_name = 'shm'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Curse of Chains'),
    (select id from sets where short_name = 'shm'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windbrisk Raptor'),
    (select id from sets where short_name = 'shm'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spectral Procession'),
    (select id from sets where short_name = 'shm'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Consign to Dream'),
    (select id from sets where short_name = 'shm'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravelgill Axeshark'),
    (select id from sets where short_name = 'shm'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murderous Redcap'),
    (select id from sets where short_name = 'shm'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Memory Sluice'),
    (select id from sets where short_name = 'shm'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wingrattle Scarecrow'),
    (select id from sets where short_name = 'shm'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Safewright Quest'),
    (select id from sets where short_name = 'shm'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'shm'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flourishing Defenses'),
    (select id from sets where short_name = 'shm'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodmark Mentor'),
    (select id from sets where short_name = 'shm'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boggart Ram-Gang'),
    (select id from sets where short_name = 'shm'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cinderhaze Wretch'),
    (select id from sets where short_name = 'shm'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moonring Island'),
    (select id from sets where short_name = 'shm'),
    '276',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beseech the Queen'),
    (select id from sets where short_name = 'shm'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Helm of the Ghastlord'),
    (select id from sets where short_name = 'shm'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scuzzback Marauders'),
    (select id from sets where short_name = 'shm'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tyrannize'),
    (select id from sets where short_name = 'shm'),
    '201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Power of Fire'),
    (select id from sets where short_name = 'shm'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viridescent Wisps'),
    (select id from sets where short_name = 'shm'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mercy Killing'),
    (select id from sets where short_name = 'shm'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Puppeteer Clique'),
    (select id from sets where short_name = 'shm'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scarscale Ritual'),
    (select id from sets where short_name = 'shm'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aethertow'),
    (select id from sets where short_name = 'shm'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'shm'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodshed Fever'),
    (select id from sets where short_name = 'shm'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illuminated Folio'),
    (select id from sets where short_name = 'shm'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jaws of Stone'),
    (select id from sets where short_name = 'shm'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Traitor''s Roar'),
    (select id from sets where short_name = 'shm'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Turn to Mist'),
    (select id from sets where short_name = 'shm'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Puresight Merrow'),
    (select id from sets where short_name = 'shm'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gloomlance'),
    (select id from sets where short_name = 'shm'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kitchen Finks'),
    (select id from sets where short_name = 'shm'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'shm'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wooded Bastion'),
    (select id from sets where short_name = 'shm'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Godhead of Awe'),
    (select id from sets where short_name = 'shm'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kithkin Shielddare'),
    (select id from sets where short_name = 'shm'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistmeadow Witch'),
    (select id from sets where short_name = 'shm'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smash to Smithereens'),
    (select id from sets where short_name = 'shm'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burn Trail'),
    (select id from sets where short_name = 'shm'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kithkin Rabble'),
    (select id from sets where short_name = 'shm'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashenmoor Gouger'),
    (select id from sets where short_name = 'shm'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Manaforge Cinder'),
    (select id from sets where short_name = 'shm'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knollspine Invocation'),
    (select id from sets where short_name = 'shm'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Devoted Druid'),
    (select id from sets where short_name = 'shm'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Puca''s Mischief'),
    (select id from sets where short_name = 'shm'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heap Doll'),
    (select id from sets where short_name = 'shm'),
    '253',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Torpor Dust'),
    (select id from sets where short_name = 'shm'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barrenton Medic'),
    (select id from sets where short_name = 'shm'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oversoul of Dusk'),
    (select id from sets where short_name = 'shm'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tattermunge Maniac'),
    (select id from sets where short_name = 'shm'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lurebound Scarecrow'),
    (select id from sets where short_name = 'shm'),
    '256',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Farhaven Elf'),
    (select id from sets where short_name = 'shm'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghastlord of Fugue'),
    (select id from sets where short_name = 'shm'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hungry Spriggan'),
    (select id from sets where short_name = 'shm'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crabapple Cohort'),
    (select id from sets where short_name = 'shm'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dire Undercurrents'),
    (select id from sets where short_name = 'shm'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scuzzback Scrapper'),
    (select id from sets where short_name = 'shm'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plumeveil'),
    (select id from sets where short_name = 'shm'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mudbrawler Cohort'),
    (select id from sets where short_name = 'shm'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Swarm'),
    (select id from sets where short_name = 'shm'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elemental Mastery'),
    (select id from sets where short_name = 'shm'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elsewhere Flask'),
    (select id from sets where short_name = 'shm'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runed Halo'),
    (select id from sets where short_name = 'shm'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deus of Calamity'),
    (select id from sets where short_name = 'shm'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savor the Moment'),
    (select id from sets where short_name = 'shm'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glen Elendra Liege'),
    (select id from sets where short_name = 'shm'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'River Kelpie'),
    (select id from sets where short_name = 'shm'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grief Tyrant'),
    (select id from sets where short_name = 'shm'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oona''s Gatewarden'),
    (select id from sets where short_name = 'shm'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Medicine Runner'),
    (select id from sets where short_name = 'shm'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'shm'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merrow Wavebreakers'),
    (select id from sets where short_name = 'shm'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zealous Guardian'),
    (select id from sets where short_name = 'shm'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gloomwidow'),
    (select id from sets where short_name = 'shm'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashenmoor Liege'),
    (select id from sets where short_name = 'shm'),
    '181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'shm'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fists of the Demigod'),
    (select id from sets where short_name = 'shm'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Apothecary Initiate'),
    (select id from sets where short_name = 'shm'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Din of the Fireherd'),
    (select id from sets where short_name = 'shm'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Memory Plunder'),
    (select id from sets where short_name = 'shm'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emberstrike Duo'),
    (select id from sets where short_name = 'shm'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunken Ruins'),
    (select id from sets where short_name = 'shm'),
    '280',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rune-Cervin Rider'),
    (select id from sets where short_name = 'shm'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghastly Discovery'),
    (select id from sets where short_name = 'shm'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oracle of Nectars'),
    (select id from sets where short_name = 'shm'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seedcradle Witch'),
    (select id from sets where short_name = 'shm'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'shm'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flame Javelin'),
    (select id from sets where short_name = 'shm'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crimson Wisps'),
    (select id from sets where short_name = 'shm'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leech Bonder'),
    (select id from sets where short_name = 'shm'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inescapable Brute'),
    (select id from sets where short_name = 'shm'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wanderbrine Rootcutters'),
    (select id from sets where short_name = 'shm'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Roughshod Mentor'),
    (select id from sets where short_name = 'shm'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wasp Lancer'),
    (select id from sets where short_name = 'shm'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tattermunge Duo'),
    (select id from sets where short_name = 'shm'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ember Gale'),
    (select id from sets where short_name = 'shm'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dusk Urchins'),
    (select id from sets where short_name = 'shm'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'shm'),
    '282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blowfly Infestation'),
    (select id from sets where short_name = 'shm'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vexing Shusher'),
    (select id from sets where short_name = 'shm'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mistveil Plains'),
    (select id from sets where short_name = 'shm'),
    '275',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leechridden Swamp'),
    (select id from sets where short_name = 'shm'),
    '273',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whimwader'),
    (select id from sets where short_name = 'shm'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistmeadow Skulk'),
    (select id from sets where short_name = 'shm'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slinking Giant'),
    (select id from sets where short_name = 'shm'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'shm'),
    '283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scar'),
    (select id from sets where short_name = 'shm'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'shm'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drove of Elves'),
    (select id from sets where short_name = 'shm'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hollowsage'),
    (select id from sets where short_name = 'shm'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'shm'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disturbing Plot'),
    (select id from sets where short_name = 'shm'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thoughtweft Gambit'),
    (select id from sets where short_name = 'shm'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merrow Grimeblotter'),
    (select id from sets where short_name = 'shm'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raven''s Run Dragoon'),
    (select id from sets where short_name = 'shm'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spell Syphon'),
    (select id from sets where short_name = 'shm'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wound Reflection'),
    (select id from sets where short_name = 'shm'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wilt-Leaf Cavaliers'),
    (select id from sets where short_name = 'shm'),
    '244',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fulminator Mage'),
    (select id from sets where short_name = 'shm'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Painter''s Servant'),
    (select id from sets where short_name = 'shm'),
    '257',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Resplendent Mentor'),
    (select id from sets where short_name = 'shm'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Niveous Wisps'),
    (select id from sets where short_name = 'shm'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knollspine Dragon'),
    (select id from sets where short_name = 'shm'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blight Sickle'),
    (select id from sets where short_name = 'shm'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Put Away'),
    (select id from sets where short_name = 'shm'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Intimidator Initiate'),
    (select id from sets where short_name = 'shm'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Hexhunter'),
    (select id from sets where short_name = 'shm'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thought Reflection'),
    (select id from sets where short_name = 'shm'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Graven Cairns'),
    (select id from sets where short_name = 'shm'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tattermunge Witch'),
    (select id from sets where short_name = 'shm'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ballynock Cohort'),
    (select id from sets where short_name = 'shm'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prison Term'),
    (select id from sets where short_name = 'shm'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inquisitor''s Snare'),
    (select id from sets where short_name = 'shm'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Polluted Bonds'),
    (select id from sets where short_name = 'shm'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cauldron of Souls'),
    (select id from sets where short_name = 'shm'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cragganwick Cremator'),
    (select id from sets where short_name = 'shm'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Safehold Sentry'),
    (select id from sets where short_name = 'shm'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dream Salvage'),
    (select id from sets where short_name = 'shm'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Briarberry Cohort'),
    (select id from sets where short_name = 'shm'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Puncture Bolt'),
    (select id from sets where short_name = 'shm'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Poison the Well'),
    (select id from sets where short_name = 'shm'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deep-Slumber Titan'),
    (select id from sets where short_name = 'shm'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mudbrawler Raiders'),
    (select id from sets where short_name = 'shm'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barkshell Blessing'),
    (select id from sets where short_name = 'shm'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demigod of Revenge'),
    (select id from sets where short_name = 'shm'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woeleecher'),
    (select id from sets where short_name = 'shm'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gleeful Sabotage'),
    (select id from sets where short_name = 'shm'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sapseep Forest'),
    (select id from sets where short_name = 'shm'),
    '279',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Torture'),
    (select id from sets where short_name = 'shm'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Toil to Renown'),
    (select id from sets where short_name = 'shm'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thistledown Duo'),
    (select id from sets where short_name = 'shm'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boggart Arsonists'),
    (select id from sets where short_name = 'shm'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Worldpurge'),
    (select id from sets where short_name = 'shm'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Juvenile Gloomwidow'),
    (select id from sets where short_name = 'shm'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Advice from the Fae'),
    (select id from sets where short_name = 'shm'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'shm'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rustrazor Butcher'),
    (select id from sets where short_name = 'shm'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spawnwrithe'),
    (select id from sets where short_name = 'shm'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyre Charger'),
    (select id from sets where short_name = 'shm'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mirrorweave'),
    (select id from sets where short_name = 'shm'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Midnight Banshee'),
    (select id from sets where short_name = 'shm'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raking Canopy'),
    (select id from sets where short_name = 'shm'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tatterkite'),
    (select id from sets where short_name = 'shm'),
    '264',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guttural Response'),
    (select id from sets where short_name = 'shm'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sickle Ripper'),
    (select id from sets where short_name = 'shm'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wilt-Leaf Liege'),
    (select id from sets where short_name = 'shm'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gnarled Effigy'),
    (select id from sets where short_name = 'shm'),
    '251',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prismatic Omen'),
    (select id from sets where short_name = 'shm'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Isleback Spawn'),
    (select id from sets where short_name = 'shm'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deepchannel Mentor'),
    (select id from sets where short_name = 'shm'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Faerie Macabre'),
    (select id from sets where short_name = 'shm'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Somnomancer'),
    (select id from sets where short_name = 'shm'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashenmoor Cohort'),
    (select id from sets where short_name = 'shm'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cemetery Puca'),
    (select id from sets where short_name = 'shm'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mine Excavation'),
    (select id from sets where short_name = 'shm'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Impromptu Raid'),
    (select id from sets where short_name = 'shm'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Incremental Blight'),
    (select id from sets where short_name = 'shm'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gravelgill Duo'),
    (select id from sets where short_name = 'shm'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corrupt'),
    (select id from sets where short_name = 'shm'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sootwalkers'),
    (select id from sets where short_name = 'shm'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Counterbore'),
    (select id from sets where short_name = 'shm'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rite of Consumption'),
    (select id from sets where short_name = 'shm'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armored Ascension'),
    (select id from sets where short_name = 'shm'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cultbrand Cinder'),
    (select id from sets where short_name = 'shm'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pale Wayfarer'),
    (select id from sets where short_name = 'shm'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lockjaw Snapper'),
    (select id from sets where short_name = 'shm'),
    '255',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fracturing Gust'),
    (select id from sets where short_name = 'shm'),
    '227',
    'rare'
) 
 on conflict do nothing;
