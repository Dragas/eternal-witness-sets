    insert into mtgcard(name) values ('Skarrg Goliath') on conflict do nothing;
    insert into mtgcard(name) values ('Firemane Avenger') on conflict do nothing;
    insert into mtgcard(name) values ('Rubblehulk') on conflict do nothing;
    insert into mtgcard(name) values ('Foundry Champion') on conflict do nothing;
    insert into mtgcard(name) values ('Nightveil Specter') on conflict do nothing;
    insert into mtgcard(name) values ('Consuming Aberration') on conflict do nothing;
    insert into mtgcard(name) values ('Treasury Thrull') on conflict do nothing;
    insert into mtgcard(name) values ('Zameck Guildmage') on conflict do nothing;
    insert into mtgcard(name) values ('Fathom Mage') on conflict do nothing;
