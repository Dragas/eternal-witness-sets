insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Skarrg Goliath'),
    (select id from sets where short_name = 'pgtc'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firemane Avenger'),
    (select id from sets where short_name = 'pgtc'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rubblehulk'),
    (select id from sets where short_name = 'pgtc'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foundry Champion'),
    (select id from sets where short_name = 'pgtc'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightveil Specter'),
    (select id from sets where short_name = 'pgtc'),
    '*222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Consuming Aberration'),
    (select id from sets where short_name = 'pgtc'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treasury Thrull'),
    (select id from sets where short_name = 'pgtc'),
    '201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zameck Guildmage'),
    (select id from sets where short_name = 'pgtc'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fathom Mage'),
    (select id from sets where short_name = 'pgtc'),
    '162',
    'rare'
) 
 on conflict do nothing;
