insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Sphinx Summoner'),
    (select id from sets where short_name = 'con'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fleshformer'),
    (select id from sets where short_name = 'con'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Ziggurat'),
    (select id from sets where short_name = 'con'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drag Down'),
    (select id from sets where short_name = 'con'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Salvage Slasher'),
    (select id from sets where short_name = 'con'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beacon Behemoth'),
    (select id from sets where short_name = 'con'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'View from Above'),
    (select id from sets where short_name = 'con'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ember Weaver'),
    (select id from sets where short_name = 'con'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nacatl Outlander'),
    (select id from sets where short_name = 'con'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragonsoul Knight'),
    (select id from sets where short_name = 'con'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sacellum Archers'),
    (select id from sets where short_name = 'con'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thornling'),
    (select id from sets where short_name = 'con'),
    '95',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shambling Remains'),
    (select id from sets where short_name = 'con'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Constricting Tendrils'),
    (select id from sets where short_name = 'con'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gwafa Hazid, Profiteer'),
    (select id from sets where short_name = 'con'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vectis Agents'),
    (select id from sets where short_name = 'con'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darklit Gargoyle'),
    (select id from sets where short_name = 'con'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhox Bodyguard'),
    (select id from sets where short_name = 'con'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wandering Goblins'),
    (select id from sets where short_name = 'con'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scepter of Fugue'),
    (select id from sets where short_name = 'con'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Master Transmuter'),
    (select id from sets where short_name = 'con'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hellspark Elemental'),
    (select id from sets where short_name = 'con'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gluttonous Slime'),
    (select id from sets where short_name = 'con'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shard Convergence'),
    (select id from sets where short_name = 'con'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Asha''s Favor'),
    (select id from sets where short_name = 'con'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Toxic Iguanar'),
    (select id from sets where short_name = 'con'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Path to Exile'),
    (select id from sets where short_name = 'con'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Esper Cormorants'),
    (select id from sets where short_name = 'con'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Reverence'),
    (select id from sets where short_name = 'con'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestial Purge'),
    (select id from sets where short_name = 'con'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rotting Rats'),
    (select id from sets where short_name = 'con'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Telemin Performance'),
    (select id from sets where short_name = 'con'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wretched Banquet'),
    (select id from sets where short_name = 'con'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Martial Coup'),
    (select id from sets where short_name = 'con'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frontline Sage'),
    (select id from sets where short_name = 'con'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grixis Illusionist'),
    (select id from sets where short_name = 'con'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, Planeswalker'),
    (select id from sets where short_name = 'con'),
    '120',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nacatl Hunt-Pride'),
    (select id from sets where short_name = 'con'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elder Mastery'),
    (select id from sets where short_name = 'con'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Apocalypse Hydra'),
    (select id from sets where short_name = 'con'),
    '98',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kranioceros'),
    (select id from sets where short_name = 'con'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vagrant Plowbeasts'),
    (select id from sets where short_name = 'con'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Absorb Vis'),
    (select id from sets where short_name = 'con'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rupture Spire'),
    (select id from sets where short_name = 'con'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molten Frame'),
    (select id from sets where short_name = 'con'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aerie Mystics'),
    (select id from sets where short_name = 'con'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gleam of Resistance'),
    (select id from sets where short_name = 'con'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Alara'),
    (select id from sets where short_name = 'con'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakka Mar'),
    (select id from sets where short_name = 'con'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Outlander'),
    (select id from sets where short_name = 'con'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jhessian Balmgiver'),
    (select id from sets where short_name = 'con'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giltspire Avenger'),
    (select id from sets where short_name = 'con'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aven Squire'),
    (select id from sets where short_name = 'con'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Filigree Fracture'),
    (select id from sets where short_name = 'con'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volcanic Fallout'),
    (select id from sets where short_name = 'con'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = 'con'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fiery Fall'),
    (select id from sets where short_name = 'con'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Cylix'),
    (select id from sets where short_name = 'con'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exotic Orchard'),
    (select id from sets where short_name = 'con'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noble Hierarch'),
    (select id from sets where short_name = 'con'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Worldheart Phoenix'),
    (select id from sets where short_name = 'con'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul''s Majesty'),
    (select id from sets where short_name = 'con'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scornful Aether-Lich'),
    (select id from sets where short_name = 'con'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cliffrunner Behemoth'),
    (select id from sets where short_name = 'con'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cumber Stone'),
    (select id from sets where short_name = 'con'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kaleidostone'),
    (select id from sets where short_name = 'con'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voracious Dragon'),
    (select id from sets where short_name = 'con'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sludge Strider'),
    (select id from sets where short_name = 'con'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Valeron Outlander'),
    (select id from sets where short_name = 'con'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Malfegor'),
    (select id from sets where short_name = 'con'),
    '117',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sedraxis Alchemist'),
    (select id from sets where short_name = 'con'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nyxathid'),
    (select id from sets where short_name = 'con'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Valiant Guard'),
    (select id from sets where short_name = 'con'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grixis Slavedriver'),
    (select id from sets where short_name = 'con'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Child of Alara'),
    (select id from sets where short_name = 'con'),
    '101',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kederekt Parasite'),
    (select id from sets where short_name = 'con'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scepter of Dominance'),
    (select id from sets where short_name = 'con'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scepter of Insight'),
    (select id from sets where short_name = 'con'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cylian Sunsinger'),
    (select id from sets where short_name = 'con'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spore Burst'),
    (select id from sets where short_name = 'con'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Might of Alara'),
    (select id from sets where short_name = 'con'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Canyon Minotaur'),
    (select id from sets where short_name = 'con'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sigil of the Empty Throne'),
    (select id from sets where short_name = 'con'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Tyrant'),
    (select id from sets where short_name = 'con'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ethersworn Adjudicator'),
    (select id from sets where short_name = 'con'),
    '26',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Parasitic Strix'),
    (select id from sets where short_name = 'con'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bone Saw'),
    (select id from sets where short_name = 'con'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Leotau'),
    (select id from sets where short_name = 'con'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tukatongue Thallid'),
    (select id from sets where short_name = 'con'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Archangel'),
    (select id from sets where short_name = 'con'),
    '115',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Yoke of the Damned'),
    (select id from sets where short_name = 'con'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manaforce Mace'),
    (select id from sets where short_name = 'con'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Charnelhoard Wurm'),
    (select id from sets where short_name = 'con'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maniacal Rage'),
    (select id from sets where short_name = 'con'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nacatl Savage'),
    (select id from sets where short_name = 'con'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of the Reliquary'),
    (select id from sets where short_name = 'con'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreadwing'),
    (select id from sets where short_name = 'con'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Controlled Instincts'),
    (select id from sets where short_name = 'con'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armillary Sphere'),
    (select id from sets where short_name = 'con'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Banefire'),
    (select id from sets where short_name = 'con'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Matca Rioters'),
    (select id from sets where short_name = 'con'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Worldly Counsel'),
    (select id from sets where short_name = 'con'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meglonoth'),
    (select id from sets where short_name = 'con'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rhox Meditant'),
    (select id from sets where short_name = 'con'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paleoloth'),
    (select id from sets where short_name = 'con'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infectious Horror'),
    (select id from sets where short_name = 'con'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Progenitus'),
    (select id from sets where short_name = 'con'),
    '121',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Font of Mythos'),
    (select id from sets where short_name = 'con'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reliquary Tower'),
    (select id from sets where short_name = 'con'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fusion Elemental'),
    (select id from sets where short_name = 'con'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyward Eye Prophets'),
    (select id from sets where short_name = 'con'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Corrupted Roots'),
    (select id from sets where short_name = 'con'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Suicidal Charge'),
    (select id from sets where short_name = 'con'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Trailblazer'),
    (select id from sets where short_name = 'con'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inkwell Leviathan'),
    (select id from sets where short_name = 'con'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magister Sphinx'),
    (select id from sets where short_name = 'con'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hellkite Hatchling'),
    (select id from sets where short_name = 'con'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Paragon of the Amesha'),
    (select id from sets where short_name = 'con'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Faerie Mechanist'),
    (select id from sets where short_name = 'con'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scattershot Archer'),
    (select id from sets where short_name = 'con'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vedalken Outlander'),
    (select id from sets where short_name = 'con'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unstable Frontier'),
    (select id from sets where short_name = 'con'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mark of Asylum'),
    (select id from sets where short_name = 'con'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Razerunners'),
    (select id from sets where short_name = 'con'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conflux'),
    (select id from sets where short_name = 'con'),
    '102',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pestilent Kathari'),
    (select id from sets where short_name = 'con'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Extractor Demon'),
    (select id from sets where short_name = 'con'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Esperzoa'),
    (select id from sets where short_name = 'con'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scarland Thrinax'),
    (select id from sets where short_name = 'con'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Temper'),
    (select id from sets where short_name = 'con'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brackwater Elemental'),
    (select id from sets where short_name = 'con'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ignite Disorder'),
    (select id from sets where short_name = 'con'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viashino Slaughtermaster'),
    (select id from sets where short_name = 'con'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodhall Ooze'),
    (select id from sets where short_name = 'con'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirror-Sigil Sergeant'),
    (select id from sets where short_name = 'con'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Countersquall'),
    (select id from sets where short_name = 'con'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lapse of Certainty'),
    (select id from sets where short_name = 'con'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quenchable Fire'),
    (select id from sets where short_name = 'con'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Traumatic Visions'),
    (select id from sets where short_name = 'con'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exploding Borders'),
    (select id from sets where short_name = 'con'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Outlander'),
    (select id from sets where short_name = 'con'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knotvine Mystic'),
    (select id from sets where short_name = 'con'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Bounty'),
    (select id from sets where short_name = 'con'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voices from the Void'),
    (select id from sets where short_name = 'con'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Court Homunculus'),
    (select id from sets where short_name = 'con'),
    '6',
    'common'
) 
 on conflict do nothing;
