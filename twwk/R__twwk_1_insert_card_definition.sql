    insert into mtgcard(name) values ('Construct') on conflict do nothing;
    insert into mtgcard(name) values ('Soldier Ally') on conflict do nothing;
    insert into mtgcard(name) values ('Elephant') on conflict do nothing;
    insert into mtgcard(name) values ('Ogre') on conflict do nothing;
    insert into mtgcard(name) values ('Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Plant') on conflict do nothing;
