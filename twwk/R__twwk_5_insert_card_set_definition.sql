insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Construct'),
    (select id from sets where short_name = 'twwk'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier Ally'),
    (select id from sets where short_name = 'twwk'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elephant'),
    (select id from sets where short_name = 'twwk'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ogre'),
    (select id from sets where short_name = 'twwk'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'twwk'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plant'),
    (select id from sets where short_name = 'twwk'),
    '5',
    'common'
) 
 on conflict do nothing;
