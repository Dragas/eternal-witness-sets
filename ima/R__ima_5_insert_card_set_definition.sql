insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Malfegor'),
    (select id from sets where short_name = 'ima'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blizzard Specter'),
    (select id from sets where short_name = 'ima'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Consecrated Sphinx'),
    (select id from sets where short_name = 'ima'),
    '47',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Keiga, the Tide Star'),
    (select id from sets where short_name = 'ima'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Festering Newt'),
    (select id from sets where short_name = 'ima'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunting Pack'),
    (select id from sets where short_name = 'ima'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inspiring Call'),
    (select id from sets where short_name = 'ima'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darksteel Axe'),
    (select id from sets where short_name = 'ima'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bewilder'),
    (select id from sets where short_name = 'ima'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Ascendant'),
    (select id from sets where short_name = 'ima'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Illusory Angel'),
    (select id from sets where short_name = 'ima'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Foul-Tongue Invocation'),
    (select id from sets where short_name = 'ima'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragonloft Idol'),
    (select id from sets where short_name = 'ima'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ainok Bond-Kin'),
    (select id from sets where short_name = 'ima'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scourge of Valkas'),
    (select id from sets where short_name = 'ima'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reave Soul'),
    (select id from sets where short_name = 'ima'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azorius Chancery'),
    (select id from sets where short_name = 'ima'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Draconic Roar'),
    (select id from sets where short_name = 'ima'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jhessian Thief'),
    (select id from sets where short_name = 'ima'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Furnace Whelp'),
    (select id from sets where short_name = 'ima'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jin-Gitaxias, Core Augur'),
    (select id from sets where short_name = 'ima'),
    '62',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Eternal Thirst'),
    (select id from sets where short_name = 'ima'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Firemane Angel'),
    (select id from sets where short_name = 'ima'),
    '199',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blinding Mage'),
    (select id from sets where short_name = 'ima'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thought Scour'),
    (select id from sets where short_name = 'ima'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teferi, Mage of Zhalfir'),
    (select id from sets where short_name = 'ima'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spiritmonger'),
    (select id from sets where short_name = 'ima'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel of Mercy'),
    (select id from sets where short_name = 'ima'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urabrask the Hidden'),
    (select id from sets where short_name = 'ima'),
    '152',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Doomed Traveler'),
    (select id from sets where short_name = 'ima'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'ima'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nantuko Shaman'),
    (select id from sets where short_name = 'ima'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magus of the Moon'),
    (select id from sets where short_name = 'ima'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knight of the Reliquary'),
    (select id from sets where short_name = 'ima'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chronicler of Heroes'),
    (select id from sets where short_name = 'ima'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thrill-Kill Assassin'),
    (select id from sets where short_name = 'ima'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Leak'),
    (select id from sets where short_name = 'ima'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mindcrank'),
    (select id from sets where short_name = 'ima'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bladewing the Risen'),
    (select id from sets where short_name = 'ima'),
    '193',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scion of Ugin'),
    (select id from sets where short_name = 'ima'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Pridemate'),
    (select id from sets where short_name = 'ima'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Haunting Hymn'),
    (select id from sets where short_name = 'ima'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Survival Cache'),
    (select id from sets where short_name = 'ima'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'ima'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moonglove Extract'),
    (select id from sets where short_name = 'ima'),
    '222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riverwheel Aerialists'),
    (select id from sets where short_name = 'ima'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fog Bank'),
    (select id from sets where short_name = 'ima'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Undercity Troll'),
    (select id from sets where short_name = 'ima'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cryptic Command'),
    (select id from sets where short_name = 'ima'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thoughtseize'),
    (select id from sets where short_name = 'ima'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Staggershock'),
    (select id from sets where short_name = 'ima'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelic Accord'),
    (select id from sets where short_name = 'ima'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Windfall'),
    (select id from sets where short_name = 'ima'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wrench Mind'),
    (select id from sets where short_name = 'ima'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Noxious Dragon'),
    (select id from sets where short_name = 'ima'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aerial Predation'),
    (select id from sets where short_name = 'ima'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Restoration Angel'),
    (select id from sets where short_name = 'ima'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grisly Spectacle'),
    (select id from sets where short_name = 'ima'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Topan Freeblade'),
    (select id from sets where short_name = 'ima'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keldon Halberdier'),
    (select id from sets where short_name = 'ima'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kolaghan Monument'),
    (select id from sets where short_name = 'ima'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abzan Battle Priest'),
    (select id from sets where short_name = 'ima'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pristine Talisman'),
    (select id from sets where short_name = 'ima'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'ima'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burrenton Forge-Tender'),
    (select id from sets where short_name = 'ima'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Charmbreaker Devils'),
    (select id from sets where short_name = 'ima'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nimbus Maze'),
    (select id from sets where short_name = 'ima'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Drain'),
    (select id from sets where short_name = 'ima'),
    '65',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stalwart Aven'),
    (select id from sets where short_name = 'ima'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shriekgeist'),
    (select id from sets where short_name = 'ima'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ojutai''s Breath'),
    (select id from sets where short_name = 'ima'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rosheen Meanderer'),
    (select id from sets where short_name = 'ima'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yosei, the Morning Star'),
    (select id from sets where short_name = 'ima'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Genesis Hydra'),
    (select id from sets where short_name = 'ima'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Garrison'),
    (select id from sets where short_name = 'ima'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Genesis Wave'),
    (select id from sets where short_name = 'ima'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Rot Farm'),
    (select id from sets where short_name = 'ima'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thran Dynamo'),
    (select id from sets where short_name = 'ima'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Timberland Guide'),
    (select id from sets where short_name = 'ima'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simic Growth Chamber'),
    (select id from sets where short_name = 'ima'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Electrolyze'),
    (select id from sets where short_name = 'ima'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seeker of the Way'),
    (select id from sets where short_name = 'ima'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunt the Weak'),
    (select id from sets where short_name = 'ima'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Baron of Vizkopa'),
    (select id from sets where short_name = 'ima'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carven Caryatid'),
    (select id from sets where short_name = 'ima'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Distortion Strike'),
    (select id from sets where short_name = 'ima'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guard Duty'),
    (select id from sets where short_name = 'ima'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Monastery Swiftspear'),
    (select id from sets where short_name = 'ima'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aetherize'),
    (select id from sets where short_name = 'ima'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lord of the Pit'),
    (select id from sets where short_name = 'ima'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Condescend'),
    (select id from sets where short_name = 'ima'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hoarding Dragon'),
    (select id from sets where short_name = 'ima'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Child of Night'),
    (select id from sets where short_name = 'ima'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illusory Ambusher'),
    (select id from sets where short_name = 'ima'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wing Shards'),
    (select id from sets where short_name = 'ima'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sphinx of Uthuun'),
    (select id from sets where short_name = 'ima'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jace''s Phantasm'),
    (select id from sets where short_name = 'ima'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prey''s Vengeance'),
    (select id from sets where short_name = 'ima'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Surreal Memoir'),
    (select id from sets where short_name = 'ima'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glimpse the Unthinkable'),
    (select id from sets where short_name = 'ima'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trepanation Blade'),
    (select id from sets where short_name = 'ima'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vent Sentinel'),
    (select id from sets where short_name = 'ima'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primeval Titan'),
    (select id from sets where short_name = 'ima'),
    '183',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Heroes'' Bane'),
    (select id from sets where short_name = 'ima'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ivy Elemental'),
    (select id from sets where short_name = 'ima'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Helix'),
    (select id from sets where short_name = 'ima'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mahamoti Djinn'),
    (select id from sets where short_name = 'ima'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sandstone Oracle'),
    (select id from sets where short_name = 'ima'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'ima'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lure'),
    (select id from sets where short_name = 'ima'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantom Tiger'),
    (select id from sets where short_name = 'ima'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodghast'),
    (select id from sets where short_name = 'ima'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Auriok Champion'),
    (select id from sets where short_name = 'ima'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Drake'),
    (select id from sets where short_name = 'ima'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul Turf'),
    (select id from sets where short_name = 'ima'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Path of Bravery'),
    (select id from sets where short_name = 'ima'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bogbrew Witch'),
    (select id from sets where short_name = 'ima'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enlarge'),
    (select id from sets where short_name = 'ima'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elesh Norn, Grand Cenobite'),
    (select id from sets where short_name = 'ima'),
    '18',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rune-Scarred Demon'),
    (select id from sets where short_name = 'ima'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Battle-Rattle Shaman'),
    (select id from sets where short_name = 'ima'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crowned Ceratok'),
    (select id from sets where short_name = 'ima'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Boilerworks'),
    (select id from sets where short_name = 'ima'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Great Teacher''s Decree'),
    (select id from sets where short_name = 'ima'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Splatter Thug'),
    (select id from sets where short_name = 'ima'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Sanctuary'),
    (select id from sets where short_name = 'ima'),
    '247',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Runed Servitor'),
    (select id from sets where short_name = 'ima'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greater Basilisk'),
    (select id from sets where short_name = 'ima'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jaddi Offshoot'),
    (select id from sets where short_name = 'ima'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trumpet Blast'),
    (select id from sets where short_name = 'ima'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kiln Fiend'),
    (select id from sets where short_name = 'ima'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Egg'),
    (select id from sets where short_name = 'ima'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Bauble'),
    (select id from sets where short_name = 'ima'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doom Blade'),
    (select id from sets where short_name = 'ima'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Day of the Dragons'),
    (select id from sets where short_name = 'ima'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Butcher''s Glee'),
    (select id from sets where short_name = 'ima'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horizon Canopy'),
    (select id from sets where short_name = 'ima'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guided Strike'),
    (select id from sets where short_name = 'ima'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azorius Charm'),
    (select id from sets where short_name = 'ima'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Obstinate Baloth'),
    (select id from sets where short_name = 'ima'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hypersonic Dragon'),
    (select id from sets where short_name = 'ima'),
    '201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wildsize'),
    (select id from sets where short_name = 'ima'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Star Compass'),
    (select id from sets where short_name = 'ima'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abzan Falconer'),
    (select id from sets where short_name = 'ima'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Repeal'),
    (select id from sets where short_name = 'ima'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diminish'),
    (select id from sets where short_name = 'ima'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bladewing''s Thrall'),
    (select id from sets where short_name = 'ima'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragonlord''s Servant'),
    (select id from sets where short_name = 'ima'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abyssal Persecutor'),
    (select id from sets where short_name = 'ima'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Manakin'),
    (select id from sets where short_name = 'ima'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vorinclex, Voice of Hunger'),
    (select id from sets where short_name = 'ima'),
    '189',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dimir Aqueduct'),
    (select id from sets where short_name = 'ima'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coordinated Assault'),
    (select id from sets where short_name = 'ima'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Search for Tomorrow'),
    (select id from sets where short_name = 'ima'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kiki-Jiki, Mirror Breaker'),
    (select id from sets where short_name = 'ima'),
    '136',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mnemonic Wall'),
    (select id from sets where short_name = 'ima'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampaging Baloths'),
    (select id from sets where short_name = 'ima'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cephalid Broker'),
    (select id from sets where short_name = 'ima'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancestral Vision'),
    (select id from sets where short_name = 'ima'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Supreme Verdict'),
    (select id from sets where short_name = 'ima'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thundermaw Hellkite'),
    (select id from sets where short_name = 'ima'),
    '149',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jungle Barrier'),
    (select id from sets where short_name = 'ima'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mark of Mutiny'),
    (select id from sets where short_name = 'ima'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'River of Tears'),
    (select id from sets where short_name = 'ima'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bubbling Cauldron'),
    (select id from sets where short_name = 'ima'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elusive Spellfist'),
    (select id from sets where short_name = 'ima'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kokusho, the Evening Star'),
    (select id from sets where short_name = 'ima'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mer-Ek Nightblade'),
    (select id from sets where short_name = 'ima'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Indulgent Tormentor'),
    (select id from sets where short_name = 'ima'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Claustrophobia'),
    (select id from sets where short_name = 'ima'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archangel of Thune'),
    (select id from sets where short_name = 'ima'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Simic Sky Swallower'),
    (select id from sets where short_name = 'ima'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dissolve'),
    (select id from sets where short_name = 'ima'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oblivion Stone'),
    (select id from sets where short_name = 'ima'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantom Monster'),
    (select id from sets where short_name = 'ima'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bala Ged Scorpion'),
    (select id from sets where short_name = 'ima'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Roots'),
    (select id from sets where short_name = 'ima'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earth Elemental'),
    (select id from sets where short_name = 'ima'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ryusei, the Falling Star'),
    (select id from sets where short_name = 'ima'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Rager'),
    (select id from sets where short_name = 'ima'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sheoldred, Whispering One'),
    (select id from sets where short_name = 'ima'),
    '108',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Orzhov Basilica'),
    (select id from sets where short_name = 'ima'),
    '243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Austere Command'),
    (select id from sets where short_name = 'ima'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aether Vial'),
    (select id from sets where short_name = 'ima'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overgrown Battlement'),
    (select id from sets where short_name = 'ima'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pentarch Ward'),
    (select id from sets where short_name = 'ima'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emeria Angel'),
    (select id from sets where short_name = 'ima'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Stone'),
    (select id from sets where short_name = 'ima'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dead Reveler'),
    (select id from sets where short_name = 'ima'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corpsejack Menace'),
    (select id from sets where short_name = 'ima'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savageborn Hydra'),
    (select id from sets where short_name = 'ima'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serum Powder'),
    (select id from sets where short_name = 'ima'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necropotence'),
    (select id from sets where short_name = 'ima'),
    '98',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Doorkeeper'),
    (select id from sets where short_name = 'ima'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Assault Formation'),
    (select id from sets where short_name = 'ima'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guttersnipe'),
    (select id from sets where short_name = 'ima'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Benevolent Ancestor'),
    (select id from sets where short_name = 'ima'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Graven Cairns'),
    (select id from sets where short_name = 'ima'),
    '237',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Channel'),
    (select id from sets where short_name = 'ima'),
    '157',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rotfeaster Maggot'),
    (select id from sets where short_name = 'ima'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emerge Unscathed'),
    (select id from sets where short_name = 'ima'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Bell Monk'),
    (select id from sets where short_name = 'ima'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Virulent Swipe'),
    (select id from sets where short_name = 'ima'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nature''s Claim'),
    (select id from sets where short_name = 'ima'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balustrade Spy'),
    (select id from sets where short_name = 'ima'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Netcaster Spider'),
    (select id from sets where short_name = 'ima'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iona''s Judgment'),
    (select id from sets where short_name = 'ima'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tormenting Voice'),
    (select id from sets where short_name = 'ima'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fury Charm'),
    (select id from sets where short_name = 'ima'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heat Ray'),
    (select id from sets where short_name = 'ima'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crucible of Fire'),
    (select id from sets where short_name = 'ima'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wight of Precinct Six'),
    (select id from sets where short_name = 'ima'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jugan, the Rising Star'),
    (select id from sets where short_name = 'ima'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lotus Cobra'),
    (select id from sets where short_name = 'ima'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curse of Predation'),
    (select id from sets where short_name = 'ima'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guardian Idol'),
    (select id from sets where short_name = 'ima'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanguine Bond'),
    (select id from sets where short_name = 'ima'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avacyn, Angel of Hope'),
    (select id from sets where short_name = 'ima'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rakdos Carnarium'),
    (select id from sets where short_name = 'ima'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grove of the Burnwillows'),
    (select id from sets where short_name = 'ima'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Durkwood Baloth'),
    (select id from sets where short_name = 'ima'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Student of Ojutai'),
    (select id from sets where short_name = 'ima'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vizkopa Guildmage'),
    (select id from sets where short_name = 'ima'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shimmering Grotto'),
    (select id from sets where short_name = 'ima'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Palladium Myr'),
    (select id from sets where short_name = 'ima'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pillar of Flame'),
    (select id from sets where short_name = 'ima'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anger of the Gods'),
    (select id from sets where short_name = 'ima'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Night of Souls'' Betrayal'),
    (select id from sets where short_name = 'ima'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sultai Flayer'),
    (select id from sets where short_name = 'ima'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skywise Teachings'),
    (select id from sets where short_name = 'ima'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis, the Fallen'),
    (select id from sets where short_name = 'ima'),
    '101',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Borderland Marauder'),
    (select id from sets where short_name = 'ima'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bogardan Hellkite'),
    (select id from sets where short_name = 'ima'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sustainer of the Realm'),
    (select id from sets where short_name = 'ima'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lead the Stampede'),
    (select id from sets where short_name = 'ima'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infantry Veteran'),
    (select id from sets where short_name = 'ima'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'ima'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Tempest'),
    (select id from sets where short_name = 'ima'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Amass the Components'),
    (select id from sets where short_name = 'ima'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'ima'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duskdale Wurm'),
    (select id from sets where short_name = 'ima'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ulcerate'),
    (select id from sets where short_name = 'ima'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tavern Swindler'),
    (select id from sets where short_name = 'ima'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flusterstorm'),
    (select id from sets where short_name = 'ima'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hammerhand'),
    (select id from sets where short_name = 'ima'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prodigal Pyromancer'),
    (select id from sets where short_name = 'ima'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rift Bolt'),
    (select id from sets where short_name = 'ima'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frost Lynx'),
    (select id from sets where short_name = 'ima'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Radiant Fountain'),
    (select id from sets where short_name = 'ima'),
    '244',
    'common'
) 
 on conflict do nothing;
