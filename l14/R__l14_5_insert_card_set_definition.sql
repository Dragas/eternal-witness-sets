insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'l14'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warrior'),
    (select id from sets where short_name = 'l14'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minotaur'),
    (select id from sets where short_name = 'l14'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squid'),
    (select id from sets where short_name = 'l14'),
    '3',
    'common'
) 
 on conflict do nothing;
