insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Blood Operative'),
    (select id from sets where short_name = 'pgrn'),
    '63s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Assure // Assemble'),
    (select id from sets where short_name = 'pgrn'),
    '221s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Narcomoeba'),
    (select id from sets where short_name = 'pgrn'),
    '47s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swiftblade Vindicator'),
    (select id from sets where short_name = 'pgrn'),
    '203s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ionize'),
    (select id from sets where short_name = 'pgrn'),
    '179p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beast Whisperer'),
    (select id from sets where short_name = 'pgrn'),
    '123p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ritual of Soot'),
    (select id from sets where short_name = 'pgrn'),
    '84s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steam Vents'),
    (select id from sets where short_name = 'pgrn'),
    '257p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tajic, Legion''s Edge'),
    (select id from sets where short_name = 'pgrn'),
    '204p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deafening Clarion'),
    (select id from sets where short_name = 'pgrn'),
    '165s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Midnight Reaper'),
    (select id from sets where short_name = 'pgrn'),
    '77p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple Garden'),
    (select id from sets where short_name = 'pgrn'),
    '258p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chromatic Lantern'),
    (select id from sets where short_name = 'pgrn'),
    '233p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hatchery Spider'),
    (select id from sets where short_name = 'pgrn'),
    '132s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Venerated Loxodon'),
    (select id from sets where short_name = 'pgrn'),
    '30p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Legion Warboss'),
    (select id from sets where short_name = 'pgrn'),
    '109p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deafening Clarion'),
    (select id from sets where short_name = 'pgrn'),
    '165p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Find // Finality'),
    (select id from sets where short_name = 'pgrn'),
    '225s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sinister Sabotage'),
    (select id from sets where short_name = 'pgrn'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Underrealm Lich'),
    (select id from sets where short_name = 'pgrn'),
    '211p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vraska, Golgari Queen'),
    (select id from sets where short_name = 'pgrn'),
    '213p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thief of Sanity'),
    (select id from sets where short_name = 'pgrn'),
    '205s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Assassin''s Trophy'),
    (select id from sets where short_name = 'pgrn'),
    '152s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knight of Autumn'),
    (select id from sets where short_name = 'pgrn'),
    '183p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Watery Grave'),
    (select id from sets where short_name = 'pgrn'),
    '259p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drowned Secrets'),
    (select id from sets where short_name = 'pgrn'),
    '39p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thousand-Year Storm'),
    (select id from sets where short_name = 'pgrn'),
    '207s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Trostani Discordant'),
    (select id from sets where short_name = 'pgrn'),
    '208s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mission Briefing'),
    (select id from sets where short_name = 'pgrn'),
    '44p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lazav, the Multifarious'),
    (select id from sets where short_name = 'pgrn'),
    '184s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Response // Resurgence'),
    (select id from sets where short_name = 'pgrn'),
    '229s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruesome Menagerie'),
    (select id from sets where short_name = 'pgrn'),
    '71s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emmara, Soul of the Accord'),
    (select id from sets where short_name = 'pgrn'),
    '168',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arclight Phoenix'),
    (select id from sets where short_name = 'pgrn'),
    '91p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Beast Whisperer'),
    (select id from sets where short_name = 'pgrn'),
    '123s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pelt Collector'),
    (select id from sets where short_name = 'pgrn'),
    '141p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tajic, Legion''s Edge'),
    (select id from sets where short_name = 'pgrn'),
    '204s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Risk Factor'),
    (select id from sets where short_name = 'pgrn'),
    '113p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unmoored Ego'),
    (select id from sets where short_name = 'pgrn'),
    '212p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emmara, Soul of the Accord'),
    (select id from sets where short_name = 'pgrn'),
    '168s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sacred Foundry'),
    (select id from sets where short_name = 'pgrn'),
    '254p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quasiduplicate'),
    (select id from sets where short_name = 'pgrn'),
    '51s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Expansion // Explosion'),
    (select id from sets where short_name = 'pgrn'),
    '224s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Venerated Loxodon'),
    (select id from sets where short_name = 'pgrn'),
    '30s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Light of the Legion'),
    (select id from sets where short_name = 'pgrn'),
    '19s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Citywide Bust'),
    (select id from sets where short_name = 'pgrn'),
    '4s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Runaway Steam-Kin'),
    (select id from sets where short_name = 'pgrn'),
    '115s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Izoni, Thousand-Eyed'),
    (select id from sets where short_name = 'pgrn'),
    '180s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aurelia, Exemplar of Justice'),
    (select id from sets where short_name = 'pgrn'),
    '153p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ionize'),
    (select id from sets where short_name = 'pgrn'),
    '179s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mausoleum Secrets'),
    (select id from sets where short_name = 'pgrn'),
    '75s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necrotic Wound'),
    (select id from sets where short_name = 'pgrn'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Underrealm Lich'),
    (select id from sets where short_name = 'pgrn'),
    '211s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dawn of Hope'),
    (select id from sets where short_name = 'pgrn'),
    '8s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mission Briefing'),
    (select id from sets where short_name = 'pgrn'),
    '44s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firemind''s Research'),
    (select id from sets where short_name = 'pgrn'),
    '171s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Experimental Frenzy'),
    (select id from sets where short_name = 'pgrn'),
    '99p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conclave Tribunal'),
    (select id from sets where short_name = 'pgrn'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Assassin''s Trophy'),
    (select id from sets where short_name = 'pgrn'),
    '152p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivid Revival'),
    (select id from sets where short_name = 'pgrn'),
    '148s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Doom Whisperer'),
    (select id from sets where short_name = 'pgrn'),
    '69p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mausoleum Secrets'),
    (select id from sets where short_name = 'pgrn'),
    '75p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aurelia, Exemplar of Justice'),
    (select id from sets where short_name = 'pgrn'),
    '153s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thief of Sanity'),
    (select id from sets where short_name = 'pgrn'),
    '205p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dawn of Hope'),
    (select id from sets where short_name = 'pgrn'),
    '8p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Niv-Mizzet, Parun'),
    (select id from sets where short_name = 'pgrn'),
    '192p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firemind''s Research'),
    (select id from sets where short_name = 'pgrn'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Challenger'),
    (select id from sets where short_name = 'pgrn'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Niv-Mizzet, Parun'),
    (select id from sets where short_name = 'pgrn'),
    '192s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Connive // Concoct'),
    (select id from sets where short_name = 'pgrn'),
    '222s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Risk Factor'),
    (select id from sets where short_name = 'pgrn'),
    '113s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bounty of Might'),
    (select id from sets where short_name = 'pgrn'),
    '124s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Runaway Steam-Kin'),
    (select id from sets where short_name = 'pgrn'),
    '115p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ritual of Soot'),
    (select id from sets where short_name = 'pgrn'),
    '84p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Etrata, the Silencer'),
    (select id from sets where short_name = 'pgrn'),
    '170s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knight of Autumn'),
    (select id from sets where short_name = 'pgrn'),
    '183s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thought Erasure'),
    (select id from sets where short_name = 'pgrn'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quasiduplicate'),
    (select id from sets where short_name = 'pgrn'),
    '51p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Divine Visitation'),
    (select id from sets where short_name = 'pgrn'),
    '10p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Overgrown Tomb'),
    (select id from sets where short_name = 'pgrn'),
    '253p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Expansion // Explosion'),
    (select id from sets where short_name = 'pgrn'),
    '224p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Legion Warboss'),
    (select id from sets where short_name = 'pgrn'),
    '109s',
    'rare'
) 
 on conflict do nothing;
