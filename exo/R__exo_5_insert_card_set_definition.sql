insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Cataclysm'),
    (select id from sets where short_name = 'exo'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mage il-Vec'),
    (select id from sets where short_name = 'exo'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sonic Burst'),
    (select id from sets where short_name = 'exo'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure Hunter'),
    (select id from sets where short_name = 'exo'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keeper of the Light'),
    (select id from sets where short_name = 'exo'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Theft of Dreams'),
    (select id from sets where short_name = 'exo'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Furnace Brood'),
    (select id from sets where short_name = 'exo'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Onslaught'),
    (select id from sets where short_name = 'exo'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk Looter'),
    (select id from sets where short_name = 'exo'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Convalescence'),
    (select id from sets where short_name = 'exo'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oath of Ghouls'),
    (select id from sets where short_name = 'exo'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mogg Assassin'),
    (select id from sets where short_name = 'exo'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dauthi Cutthroat'),
    (select id from sets where short_name = 'exo'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Standing Troops'),
    (select id from sets where short_name = 'exo'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fugue'),
    (select id from sets where short_name = 'exo'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mirozel'),
    (select id from sets where short_name = 'exo'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thalakos Scout'),
    (select id from sets where short_name = 'exo'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphere of Resistance'),
    (select id from sets where short_name = 'exo'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyshroud War Beast'),
    (select id from sets where short_name = 'exo'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Transmogrifying Licid'),
    (select id from sets where short_name = 'exo'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soltari Visionary'),
    (select id from sets where short_name = 'exo'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'High Ground'),
    (select id from sets where short_name = 'exo'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raging Goblin'),
    (select id from sets where short_name = 'exo'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Penance'),
    (select id from sets where short_name = 'exo'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spike Hatcher'),
    (select id from sets where short_name = 'exo'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bequeathal'),
    (select id from sets where short_name = 'exo'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Limited Resources'),
    (select id from sets where short_name = 'exo'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Manabond'),
    (select id from sets where short_name = 'exo'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyshaper'),
    (select id from sets where short_name = 'exo'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hatred'),
    (select id from sets where short_name = 'exo'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wayward Soul'),
    (select id from sets where short_name = 'exo'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forbid'),
    (select id from sets where short_name = 'exo'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reclaim'),
    (select id from sets where short_name = 'exo'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Song of Serenity'),
    (select id from sets where short_name = 'exo'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Erratic Portal'),
    (select id from sets where short_name = 'exo'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spellbook'),
    (select id from sets where short_name = 'exo'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Robe of Mirrors'),
    (select id from sets where short_name = 'exo'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reaping the Rewards'),
    (select id from sets where short_name = 'exo'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Medicine Bag'),
    (select id from sets where short_name = 'exo'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rootwater Alligator'),
    (select id from sets where short_name = 'exo'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Breach'),
    (select id from sets where short_name = 'exo'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pegasus Stampede'),
    (select id from sets where short_name = 'exo'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dauthi Jackal'),
    (select id from sets where short_name = 'exo'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Equilibrium'),
    (select id from sets where short_name = 'exo'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thalakos Drifters'),
    (select id from sets where short_name = 'exo'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oath of Lieges'),
    (select id from sets where short_name = 'exo'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wood Elves'),
    (select id from sets where short_name = 'exo'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death''s Duet'),
    (select id from sets where short_name = 'exo'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grollub'),
    (select id from sets where short_name = 'exo'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plated Rootwalla'),
    (select id from sets where short_name = 'exo'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Culling the Weak'),
    (select id from sets where short_name = 'exo'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spellshock'),
    (select id from sets where short_name = 'exo'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cunning'),
    (select id from sets where short_name = 'exo'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paroxysm'),
    (select id from sets where short_name = 'exo'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scalding Salamander'),
    (select id from sets where short_name = 'exo'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pandemonium'),
    (select id from sets where short_name = 'exo'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spike Cannibal'),
    (select id from sets where short_name = 'exo'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pit Spawn'),
    (select id from sets where short_name = 'exo'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scare Tactics'),
    (select id from sets where short_name = 'exo'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zealots en-Dal'),
    (select id from sets where short_name = 'exo'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keeper of the Dead'),
    (select id from sets where short_name = 'exo'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ravenous Baboons'),
    (select id from sets where short_name = 'exo'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirri, Cat Warrior'),
    (select id from sets where short_name = 'exo'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mindless Automaton'),
    (select id from sets where short_name = 'exo'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Over Matter'),
    (select id from sets where short_name = 'exo'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whiptongue Frog'),
    (select id from sets where short_name = 'exo'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'School of Piranha'),
    (select id from sets where short_name = 'exo'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dizzying Gaze'),
    (select id from sets where short_name = 'exo'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cursed Flesh'),
    (select id from sets where short_name = 'exo'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Hounds'),
    (select id from sets where short_name = 'exo'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exalted Dragon'),
    (select id from sets where short_name = 'exo'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keeper of the Mind'),
    (select id from sets where short_name = 'exo'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pygmy Troll'),
    (select id from sets where short_name = 'exo'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cartographer'),
    (select id from sets where short_name = 'exo'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Welkin Hawk'),
    (select id from sets where short_name = 'exo'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sabertooth Wyvern'),
    (select id from sets where short_name = 'exo'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Charging Paladin'),
    (select id from sets where short_name = 'exo'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shattering Pulse'),
    (select id from sets where short_name = 'exo'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scrivener'),
    (select id from sets where short_name = 'exo'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flowstone Flood'),
    (select id from sets where short_name = 'exo'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necrologia'),
    (select id from sets where short_name = 'exo'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kor Chant'),
    (select id from sets where short_name = 'exo'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Price of Progress'),
    (select id from sets where short_name = 'exo'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slaughter'),
    (select id from sets where short_name = 'exo'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reckless Ogre'),
    (select id from sets where short_name = 'exo'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ertai, Wizard Adept'),
    (select id from sets where short_name = 'exo'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cat Burglar'),
    (select id from sets where short_name = 'exo'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rootwater Mystic'),
    (select id from sets where short_name = 'exo'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rabid Wolverines'),
    (select id from sets where short_name = 'exo'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Tide'),
    (select id from sets where short_name = 'exo'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keeper of the Flame'),
    (select id from sets where short_name = 'exo'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spike Weaver'),
    (select id from sets where short_name = 'exo'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coat of Arms'),
    (select id from sets where short_name = 'exo'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nausea'),
    (select id from sets where short_name = 'exo'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reconnaissance'),
    (select id from sets where short_name = 'exo'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crashing Boars'),
    (select id from sets where short_name = 'exo'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anarchist'),
    (select id from sets where short_name = 'exo'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volrath''s Dungeon'),
    (select id from sets where short_name = 'exo'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fade Away'),
    (select id from sets where short_name = 'exo'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'City of Traitors'),
    (select id from sets where short_name = 'exo'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plaguebearer'),
    (select id from sets where short_name = 'exo'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cinder Crawler'),
    (select id from sets where short_name = 'exo'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dauthi Warlord'),
    (select id from sets where short_name = 'exo'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shield Mate'),
    (select id from sets where short_name = 'exo'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elven Palisade'),
    (select id from sets where short_name = 'exo'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Entropic Specter'),
    (select id from sets where short_name = 'exo'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Paladin en-Vec'),
    (select id from sets where short_name = 'exo'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Survival of the Fittest'),
    (select id from sets where short_name = 'exo'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Nets'),
    (select id from sets where short_name = 'exo'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Workhorse'),
    (select id from sets where short_name = 'exo'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Peace of Mind'),
    (select id from sets where short_name = 'exo'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Maggots'),
    (select id from sets where short_name = 'exo'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shackles'),
    (select id from sets where short_name = 'exo'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Memory Crystal'),
    (select id from sets where short_name = 'exo'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Recurring Nightmare'),
    (select id from sets where short_name = 'exo'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fighting Chance'),
    (select id from sets where short_name = 'exo'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ogre Shaman'),
    (select id from sets where short_name = 'exo'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jackalope Herd'),
    (select id from sets where short_name = 'exo'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seismic Assault'),
    (select id from sets where short_name = 'exo'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keeper of the Beasts'),
    (select id from sets where short_name = 'exo'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oath of Druids'),
    (select id from sets where short_name = 'exo'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carnophage'),
    (select id from sets where short_name = 'exo'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter Squadron'),
    (select id from sets where short_name = 'exo'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Monstrous Hound'),
    (select id from sets where short_name = 'exo'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Null Brooch'),
    (select id from sets where short_name = 'exo'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Warden'),
    (select id from sets where short_name = 'exo'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oath of Scholars'),
    (select id from sets where short_name = 'exo'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thrull Surgeon'),
    (select id from sets where short_name = 'exo'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Killer Whale'),
    (select id from sets where short_name = 'exo'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Predatory Hunger'),
    (select id from sets where short_name = 'exo'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Berserker'),
    (select id from sets where short_name = 'exo'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dominating Licid'),
    (select id from sets where short_name = 'exo'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angelic Blessing'),
    (select id from sets where short_name = 'exo'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oath of Mages'),
    (select id from sets where short_name = 'exo'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spike Rogue'),
    (select id from sets where short_name = 'exo'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Allay'),
    (select id from sets where short_name = 'exo'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure Trove'),
    (select id from sets where short_name = 'exo'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ephemeron'),
    (select id from sets where short_name = 'exo'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maniacal Rage'),
    (select id from sets where short_name = 'exo'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Resuscitate'),
    (select id from sets where short_name = 'exo'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Elite'),
    (select id from sets where short_name = 'exo'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avenging Druid'),
    (select id from sets where short_name = 'exo'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curiosity'),
    (select id from sets where short_name = 'exo'),
    '29',
    'uncommon'
) 
 on conflict do nothing;
