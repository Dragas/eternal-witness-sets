insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Path to Exile'),
    (select id from sets where short_name = 'ss2'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shielded by Faith'),
    (select id from sets where short_name = 'ss2'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Martyr''s Bond'),
    (select id from sets where short_name = 'ss2'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gideon Jura'),
    (select id from sets where short_name = 'ss2'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Worship'),
    (select id from sets where short_name = 'ss2'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blackblade Reforged'),
    (select id from sets where short_name = 'ss2'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rest in Peace'),
    (select id from sets where short_name = 'ss2'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'True Conviction'),
    (select id from sets where short_name = 'ss2'),
    '6',
    'rare'
) 
 on conflict do nothing;
