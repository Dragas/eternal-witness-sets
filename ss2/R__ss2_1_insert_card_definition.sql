    insert into mtgcard(name) values ('Path to Exile') on conflict do nothing;
    insert into mtgcard(name) values ('Shielded by Faith') on conflict do nothing;
    insert into mtgcard(name) values ('Martyr''s Bond') on conflict do nothing;
    insert into mtgcard(name) values ('Gideon Jura') on conflict do nothing;
    insert into mtgcard(name) values ('Worship') on conflict do nothing;
    insert into mtgcard(name) values ('Blackblade Reforged') on conflict do nothing;
    insert into mtgcard(name) values ('Rest in Peace') on conflict do nothing;
    insert into mtgcard(name) values ('True Conviction') on conflict do nothing;
