    insert into mtgcard(name) values ('Goblin') on conflict do nothing;
    insert into mtgcard(name) values ('Wurm') on conflict do nothing;
    insert into mtgcard(name) values ('Drake') on conflict do nothing;
    insert into mtgcard(name) values ('Elephant') on conflict do nothing;
    insert into mtgcard(name) values ('Elemental') on conflict do nothing;
    insert into mtgcard(name) values ('Elf Warrior') on conflict do nothing;
    insert into mtgcard(name) values ('Beast') on conflict do nothing;
