insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tdds'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurm'),
    (select id from sets where short_name = 'tdds'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drake'),
    (select id from sets where short_name = 'tdds'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elephant'),
    (select id from sets where short_name = 'tdds'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tdds'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elf Warrior'),
    (select id from sets where short_name = 'tdds'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tdds'),
    '4',
    'common'
) 
 on conflict do nothing;
