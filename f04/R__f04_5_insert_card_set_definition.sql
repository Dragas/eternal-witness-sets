insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Mother of Runes'),
    (select id from sets where short_name = 'f04'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carrion Feeder'),
    (select id from sets where short_name = 'f04'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treetop Village'),
    (select id from sets where short_name = 'f04'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slice and Dice'),
    (select id from sets where short_name = 'f04'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'f04'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Warchief'),
    (select id from sets where short_name = 'f04'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Willbender'),
    (select id from sets where short_name = 'f04'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Rift'),
    (select id from sets where short_name = 'f04'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silver Knight'),
    (select id from sets where short_name = 'f04'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reanimate'),
    (select id from sets where short_name = 'f04'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Accumulated Knowledge'),
    (select id from sets where short_name = 'f04'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avalanche Riders'),
    (select id from sets where short_name = 'f04'),
    '9',
    'rare'
) 
 on conflict do nothing;
