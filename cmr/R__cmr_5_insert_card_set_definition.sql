insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Alena, Kessig Trapper'),
    (select id from sets where short_name = 'cmr'),
    '570',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vault of Champions'),
    (select id from sets where short_name = 'cmr'),
    '360',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Command Tower'),
    (select id from sets where short_name = 'cmr'),
    '705',
    'common'
) ,
(
    (select id from mtgcard where name = 'Training Center'),
    (select id from sets where short_name = 'cmr'),
    '713',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sengir, the Dark Baron'),
    (select id from sets where short_name = 'cmr'),
    '722',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spectator Seating'),
    (select id from sets where short_name = 'cmr'),
    '356',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Undergrowth Stadium'),
    (select id from sets where short_name = 'cmr'),
    '714',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keeper of the Accord'),
    (select id from sets where short_name = 'cmr'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Confluence'),
    (select id from sets where short_name = 'cmr'),
    '721',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sengir, the Dark Baron'),
    (select id from sets where short_name = 'cmr'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sengir, the Dark Baron'),
    (select id from sets where short_name = 'cmr'),
    '568',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rejuvenating Springs'),
    (select id from sets where short_name = 'cmr'),
    '354',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rejuvenating Springs'),
    (select id from sets where short_name = 'cmr'),
    '709',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keeper of the Accord'),
    (select id from sets where short_name = 'cmr'),
    '621',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Halana, Kessig Ranger'),
    (select id from sets where short_name = 'cmr'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vault of Champions'),
    (select id from sets where short_name = 'cmr'),
    '715',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Command Tower'),
    (select id from sets where short_name = 'cmr'),
    '350',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prossh, Skyraider of Kher'),
    (select id from sets where short_name = 'cmr'),
    '530',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Alena, Kessig Trapper'),
    (select id from sets where short_name = 'cmr'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Commander''s Sphere'),
    (select id from sets where short_name = 'cmr'),
    '306',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Prismatic Piper'),
    (select id from sets where short_name = 'cmr'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Training Center'),
    (select id from sets where short_name = 'cmr'),
    '358',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spectator Seating'),
    (select id from sets where short_name = 'cmr'),
    '711',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Prismatic Piper'),
    (select id from sets where short_name = 'cmr'),
    '546',
    'common'
) ,
(
    (select id from mtgcard where name = 'Commander''s Sphere'),
    (select id from sets where short_name = 'cmr'),
    '693',
    'common'
) ,
(
    (select id from mtgcard where name = 'Halana, Kessig Ranger'),
    (select id from sets where short_name = 'cmr'),
    '579',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Undergrowth Stadium'),
    (select id from sets where short_name = 'cmr'),
    '359',
    'rare'
) 
 on conflict do nothing;
