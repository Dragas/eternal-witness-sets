insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Immaculate Magistrate'),
    (select id from sets where short_name = 'ps11'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imperious Perfect'),
    (select id from sets where short_name = 'ps11'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Garza Zol, Plague Queen'),
    (select id from sets where short_name = 'ps11'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Furnace of Rath'),
    (select id from sets where short_name = 'ps11'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Suntail Hawk'),
    (select id from sets where short_name = 'ps11'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meglonoth'),
    (select id from sets where short_name = 'ps11'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = 'ps11'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Condemn'),
    (select id from sets where short_name = 'ps11'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hamletback Goliath'),
    (select id from sets where short_name = 'ps11'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cho-Manno, Revolutionary'),
    (select id from sets where short_name = 'ps11'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glaze Fiend'),
    (select id from sets where short_name = 'ps11'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Champion'),
    (select id from sets where short_name = 'ps11'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woolly Thoctar'),
    (select id from sets where short_name = 'ps11'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dispeller''s Capsule'),
    (select id from sets where short_name = 'ps11'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Troll Ascetic'),
    (select id from sets where short_name = 'ps11'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sprouting Thrinax'),
    (select id from sets where short_name = 'ps11'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ps11'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kraken''s Eye'),
    (select id from sets where short_name = 'ps11'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glorious Anthem'),
    (select id from sets where short_name = 'ps11'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = 'ps11'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Font of Mythos'),
    (select id from sets where short_name = 'ps11'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ps11'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deft Duelist'),
    (select id from sets where short_name = 'ps11'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Honor of the Pure'),
    (select id from sets where short_name = 'ps11'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overrun'),
    (select id from sets where short_name = 'ps11'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mortivore'),
    (select id from sets where short_name = 'ps11'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Roost'),
    (select id from sets where short_name = 'ps11'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'ps11'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loxodon Warhammer'),
    (select id from sets where short_name = 'ps11'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spined Wurm'),
    (select id from sets where short_name = 'ps11'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boomerang'),
    (select id from sets where short_name = 'ps11'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = 'ps11'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flamekin Brawler'),
    (select id from sets where short_name = 'ps11'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravenous Rats'),
    (select id from sets where short_name = 'ps11'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = 'ps11'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Roughshod Mentor'),
    (select id from sets where short_name = 'ps11'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ps11'),
    '223',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Rack'),
    (select id from sets where short_name = 'ps11'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goldenglow Moth'),
    (select id from sets where short_name = 'ps11'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ps11'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Into the Roil'),
    (select id from sets where short_name = 'ps11'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Goliath'),
    (select id from sets where short_name = 'ps11'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ps11'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bottle Gnomes'),
    (select id from sets where short_name = 'ps11'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spirit of the Hearth'),
    (select id from sets where short_name = 'ps11'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathbringer Thoctar'),
    (select id from sets where short_name = 'ps11'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = 'ps11'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Borderland Behemoth'),
    (select id from sets where short_name = 'ps11'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirri, Cat Warrior'),
    (select id from sets where short_name = 'ps11'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel of Mercy'),
    (select id from sets where short_name = 'ps11'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Act of Treason'),
    (select id from sets where short_name = 'ps11'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Howling Mine'),
    (select id from sets where short_name = 'ps11'),
    '190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kiss of the Amesha'),
    (select id from sets where short_name = 'ps11'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Onyx Goblet'),
    (select id from sets where short_name = 'ps11'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloud Sprite'),
    (select id from sets where short_name = 'ps11'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tidehollow Strix'),
    (select id from sets where short_name = 'ps11'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divination'),
    (select id from sets where short_name = 'ps11'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Visionary'),
    (select id from sets where short_name = 'ps11'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon''s Claw'),
    (select id from sets where short_name = 'ps11'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ps11'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tundra Wolves'),
    (select id from sets where short_name = 'ps11'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bull Cerodon'),
    (select id from sets where short_name = 'ps11'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Memory Erosion'),
    (select id from sets where short_name = 'ps11'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ps11'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel''s Feather'),
    (select id from sets where short_name = 'ps11'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cancel'),
    (select id from sets where short_name = 'ps11'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abyssal Specter'),
    (select id from sets where short_name = 'ps11'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ps11'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'ps11'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blanchwood Armor'),
    (select id from sets where short_name = 'ps11'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dream Fracture'),
    (select id from sets where short_name = 'ps11'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Verdant Force'),
    (select id from sets where short_name = 'ps11'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Eulogist'),
    (select id from sets where short_name = 'ps11'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terminate'),
    (select id from sets where short_name = 'ps11'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = 'ps11'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hidden Horror'),
    (select id from sets where short_name = 'ps11'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jagged-Scar Archers'),
    (select id from sets where short_name = 'ps11'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mudbutton Torchrunner'),
    (select id from sets where short_name = 'ps11'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Wood'),
    (select id from sets where short_name = 'ps11'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runeclaw Bear'),
    (select id from sets where short_name = 'ps11'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ps11'),
    '222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howl of the Night Pack'),
    (select id from sets where short_name = 'ps11'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Promenade'),
    (select id from sets where short_name = 'ps11'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ps11'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Warrior'),
    (select id from sets where short_name = 'ps11'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = 'ps11'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ascendant Evincar'),
    (select id from sets where short_name = 'ps11'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ps11'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Spears'),
    (select id from sets where short_name = 'ps11'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = 'ps11'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Swing'),
    (select id from sets where short_name = 'ps11'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evacuation'),
    (select id from sets where short_name = 'ps11'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crowd of Cinders'),
    (select id from sets where short_name = 'ps11'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Skirmisher'),
    (select id from sets where short_name = 'ps11'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Civic Wayfinder'),
    (select id from sets where short_name = 'ps11'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tome Scour'),
    (select id from sets where short_name = 'ps11'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hurricane'),
    (select id from sets where short_name = 'ps11'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = 'ps11'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ps11'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sky Ruin Drake'),
    (select id from sets where short_name = 'ps11'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ps11'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudcrown Oak'),
    (select id from sets where short_name = 'ps11'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Essence Drain'),
    (select id from sets where short_name = 'ps11'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deluge'),
    (select id from sets where short_name = 'ps11'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Impelled Giant'),
    (select id from sets where short_name = 'ps11'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Control'),
    (select id from sets where short_name = 'ps11'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Shatter'),
    (select id from sets where short_name = 'ps11'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oakgnarl Warrior'),
    (select id from sets where short_name = 'ps11'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon''s Horn'),
    (select id from sets where short_name = 'ps11'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sangrite Surge'),
    (select id from sets where short_name = 'ps11'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Severed Legion'),
    (select id from sets where short_name = 'ps11'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jagged Lightning'),
    (select id from sets where short_name = 'ps11'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Furnace Whelp'),
    (select id from sets where short_name = 'ps11'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trained Armodon'),
    (select id from sets where short_name = 'ps11'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dusk Imp'),
    (select id from sets where short_name = 'ps11'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mass Calcify'),
    (select id from sets where short_name = 'ps11'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Obsidian Battle-Axe'),
    (select id from sets where short_name = 'ps11'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jayemdae Tome'),
    (select id from sets where short_name = 'ps11'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Acolyte of Xathrid'),
    (select id from sets where short_name = 'ps11'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blightning'),
    (select id from sets where short_name = 'ps11'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blaze'),
    (select id from sets where short_name = 'ps11'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duskdale Wurm'),
    (select id from sets where short_name = 'ps11'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ps11'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Essence Scatter'),
    (select id from sets where short_name = 'ps11'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talara''s Battalion'),
    (select id from sets where short_name = 'ps11'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Consume Spirit'),
    (select id from sets where short_name = 'ps11'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pariah'),
    (select id from sets where short_name = 'ps11'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crucible of Fire'),
    (select id from sets where short_name = 'ps11'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ps11'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Venerable Monk'),
    (select id from sets where short_name = 'ps11'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cinder Pyromancer'),
    (select id from sets where short_name = 'ps11'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Holy Strength'),
    (select id from sets where short_name = 'ps11'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'ps11'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of the Skyward Eye'),
    (select id from sets where short_name = 'ps11'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brion Stoutarm'),
    (select id from sets where short_name = 'ps11'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elven Riders'),
    (select id from sets where short_name = 'ps11'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Elemental'),
    (select id from sets where short_name = 'ps11'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Etherium Sculptor'),
    (select id from sets where short_name = 'ps11'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Violent Ultimatum'),
    (select id from sets where short_name = 'ps11'),
    '181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'ps11'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lava Axe'),
    (select id from sets where short_name = 'ps11'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Traumatize'),
    (select id from sets where short_name = 'ps11'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Isleback Spawn'),
    (select id from sets where short_name = 'ps11'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hellkite Charger'),
    (select id from sets where short_name = 'ps11'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prodigal Pyromancer'),
    (select id from sets where short_name = 'ps11'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grixis Battlemage'),
    (select id from sets where short_name = 'ps11'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enrage'),
    (select id from sets where short_name = 'ps11'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drudge Skeletons'),
    (select id from sets where short_name = 'ps11'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Parasitic Strix'),
    (select id from sets where short_name = 'ps11'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Banefire'),
    (select id from sets where short_name = 'ps11'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vengeful Firebrand'),
    (select id from sets where short_name = 'ps11'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nature''s Spiral'),
    (select id from sets where short_name = 'ps11'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coat of Arms'),
    (select id from sets where short_name = 'ps11'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lys Alana Huntmaster'),
    (select id from sets where short_name = 'ps11'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flameblast Dragon'),
    (select id from sets where short_name = 'ps11'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ps11'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rip-Clan Crasher'),
    (select id from sets where short_name = 'ps11'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Leak'),
    (select id from sets where short_name = 'ps11'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sharding Sphinx'),
    (select id from sets where short_name = 'ps11'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hill Giant'),
    (select id from sets where short_name = 'ps11'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurm''s Tooth'),
    (select id from sets where short_name = 'ps11'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Youthful Knight'),
    (select id from sets where short_name = 'ps11'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Sky Raider'),
    (select id from sets where short_name = 'ps11'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ps11'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampiric Dragon'),
    (select id from sets where short_name = 'ps11'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snapping Drake'),
    (select id from sets where short_name = 'ps11'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ornithopter'),
    (select id from sets where short_name = 'ps11'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'River Boa'),
    (select id from sets where short_name = 'ps11'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ps11'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plague Beetle'),
    (select id from sets where short_name = 'ps11'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Farhaven Elf'),
    (select id from sets where short_name = 'ps11'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rage Reflection'),
    (select id from sets where short_name = 'ps11'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trusty Machete'),
    (select id from sets where short_name = 'ps11'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Banewasp Affliction'),
    (select id from sets where short_name = 'ps11'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Holy Day'),
    (select id from sets where short_name = 'ps11'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Griffin Sentinel'),
    (select id from sets where short_name = 'ps11'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'ps11'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Counterbore'),
    (select id from sets where short_name = 'ps11'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ps11'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Scimitar'),
    (select id from sets where short_name = 'ps11'),
    '193',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vigor'),
    (select id from sets where short_name = 'ps11'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Herald'),
    (select id from sets where short_name = 'ps11'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thieving Magpie'),
    (select id from sets where short_name = 'ps11'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'ps11'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alpha Myr'),
    (select id from sets where short_name = 'ps11'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Razormane Masticore'),
    (select id from sets where short_name = 'ps11'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reckless Scholar'),
    (select id from sets where short_name = 'ps11'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodmark Mentor'),
    (select id from sets where short_name = 'ps11'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Grixis'),
    (select id from sets where short_name = 'ps11'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Denizen of the Deep'),
    (select id from sets where short_name = 'ps11'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Luminarch Ascension'),
    (select id from sets where short_name = 'ps11'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Fodder'),
    (select id from sets where short_name = 'ps11'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drove of Elves'),
    (select id from sets where short_name = 'ps11'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Piker'),
    (select id from sets where short_name = 'ps11'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ps11'),
    '209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombify'),
    (select id from sets where short_name = 'ps11'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantom Warrior'),
    (select id from sets where short_name = 'ps11'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = 'ps11'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'ps11'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Axegrinder Giant'),
    (select id from sets where short_name = 'ps11'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Blessing'),
    (select id from sets where short_name = 'ps11'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wanderer''s Twig'),
    (select id from sets where short_name = 'ps11'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bramblewood Paragon'),
    (select id from sets where short_name = 'ps11'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Faerie Mechanist'),
    (select id from sets where short_name = 'ps11'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earth Elemental'),
    (select id from sets where short_name = 'ps11'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eyeblight''s Ending'),
    (select id from sets where short_name = 'ps11'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mahamoti Djinn'),
    (select id from sets where short_name = 'ps11'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Spring'),
    (select id from sets where short_name = 'ps11'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Megrim'),
    (select id from sets where short_name = 'ps11'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moonglove Winnower'),
    (select id from sets where short_name = 'ps11'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Salvage Slasher'),
    (select id from sets where short_name = 'ps11'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Natural Spring'),
    (select id from sets where short_name = 'ps11'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voice of All'),
    (select id from sets where short_name = 'ps11'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sift'),
    (select id from sets where short_name = 'ps11'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molimo, Maro-Sorcerer'),
    (select id from sets where short_name = 'ps11'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra''s Embrace'),
    (select id from sets where short_name = 'ps11'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stonebrow, Krosan Hero'),
    (select id from sets where short_name = 'ps11'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Underworld Dreams'),
    (select id from sets where short_name = 'ps11'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unholy Strength'),
    (select id from sets where short_name = 'ps11'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greenweaver Druid'),
    (select id from sets where short_name = 'ps11'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kamahl, Pit Fighter'),
    (select id from sets where short_name = 'ps11'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kalonian Behemoth'),
    (select id from sets where short_name = 'ps11'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Master of Etherium'),
    (select id from sets where short_name = 'ps11'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphinx of Jwar Isle'),
    (select id from sets where short_name = 'ps11'),
    '55',
    'rare'
) 
 on conflict do nothing;
