insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Morbid Curiosity'),
    (select id from sets where short_name = 'bbd'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coralhelm Guide'),
    (select id from sets where short_name = 'bbd'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fan Favorite'),
    (select id from sets where short_name = 'bbd'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Millennial Gargoyle'),
    (select id from sets where short_name = 'bbd'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea of Clouds'),
    (select id from sets where short_name = 'bbd'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Breath'),
    (select id from sets where short_name = 'bbd'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doomed Dissenter'),
    (select id from sets where short_name = 'bbd'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oreskos Explorer'),
    (select id from sets where short_name = 'bbd'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nimbus Champion'),
    (select id from sets where short_name = 'bbd'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Regna, the Redeemer'),
    (select id from sets where short_name = 'bbd'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Decorated Champion'),
    (select id from sets where short_name = 'bbd'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sower of Temptation'),
    (select id from sets where short_name = 'bbd'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eyeblight Assassin'),
    (select id from sets where short_name = 'bbd'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Return to the Earth'),
    (select id from sets where short_name = 'bbd'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gang Up'),
    (select id from sets where short_name = 'bbd'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Archfiend of Despair'),
    (select id from sets where short_name = 'bbd'),
    '44',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Spectral Searchlight'),
    (select id from sets where short_name = 'bbd'),
    '246',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zndrsplt''s Judgment'),
    (select id from sets where short_name = 'bbd'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pierce Strider'),
    (select id from sets where short_name = 'bbd'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mindblade Render'),
    (select id from sets where short_name = 'bbd'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benthic Giant'),
    (select id from sets where short_name = 'bbd'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Huddle Up'),
    (select id from sets where short_name = 'bbd'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Night Market Guard'),
    (select id from sets where short_name = 'bbd'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrilling Encore'),
    (select id from sets where short_name = 'bbd'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bountiful Promenade'),
    (select id from sets where short_name = 'bbd'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slum Reaper'),
    (select id from sets where short_name = 'bbd'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Last Gasp'),
    (select id from sets where short_name = 'bbd'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sentinel Tower'),
    (select id from sets where short_name = 'bbd'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Claustrophobia'),
    (select id from sets where short_name = 'bbd'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Gift'),
    (select id from sets where short_name = 'bbd'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'bbd'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doubling Season'),
    (select id from sets where short_name = 'bbd'),
    '195',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Feral Hydra'),
    (select id from sets where short_name = 'bbd'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hunted Wumpus'),
    (select id from sets where short_name = 'bbd'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sickle Dancer'),
    (select id from sets where short_name = 'bbd'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tyrant''s Machine'),
    (select id from sets where short_name = 'bbd'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ley Weaver'),
    (select id from sets where short_name = 'bbd'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urborg Drake'),
    (select id from sets where short_name = 'bbd'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Najeela, the Blade-Blossom'),
    (select id from sets where short_name = 'bbd'),
    '62',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stadium Vendors'),
    (select id from sets where short_name = 'bbd'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'True-Name Nemesis'),
    (select id from sets where short_name = 'bbd'),
    '136',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Soaring Show-Off'),
    (select id from sets where short_name = 'bbd'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Scholar'),
    (select id from sets where short_name = 'bbd'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Champion of Arashin'),
    (select id from sets where short_name = 'bbd'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nyxathid'),
    (select id from sets where short_name = 'bbd'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sparring Mummy'),
    (select id from sets where short_name = 'bbd'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chakram Slinger'),
    (select id from sets where short_name = 'bbd'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seer''s Lantern'),
    (select id from sets where short_name = 'bbd'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lead by Example'),
    (select id from sets where short_name = 'bbd'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aurora Champion'),
    (select id from sets where short_name = 'bbd'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doomed Traveler'),
    (select id from sets where short_name = 'bbd'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Auger Spree'),
    (select id from sets where short_name = 'bbd'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Assassinate'),
    (select id from sets where short_name = 'bbd'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhox Brute'),
    (select id from sets where short_name = 'bbd'),
    '227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Khorvath''s Fury'),
    (select id from sets where short_name = 'bbd'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Canopy Spider'),
    (select id from sets where short_name = 'bbd'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'War''s Toll'),
    (select id from sets where short_name = 'bbd'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Apocalypse Hydra'),
    (select id from sets where short_name = 'bbd'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soulblade Corrupter'),
    (select id from sets where short_name = 'bbd'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kraul Warrior'),
    (select id from sets where short_name = 'bbd'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Regna''s Sanction'),
    (select id from sets where short_name = 'bbd'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gorm the Great'),
    (select id from sets where short_name = 'bbd'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rotfeaster Maggot'),
    (select id from sets where short_name = 'bbd'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rowan Kenrith'),
    (select id from sets where short_name = 'bbd'),
    '256',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Trumpet Blast'),
    (select id from sets where short_name = 'bbd'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Expedite'),
    (select id from sets where short_name = 'bbd'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raptor Companion'),
    (select id from sets where short_name = 'bbd'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savage Ventmaw'),
    (select id from sets where short_name = 'bbd'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Watercourser'),
    (select id from sets where short_name = 'bbd'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opportunity'),
    (select id from sets where short_name = 'bbd'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Virtus the Veiled'),
    (select id from sets where short_name = 'bbd'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Claim'),
    (select id from sets where short_name = 'bbd'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battle Mastery'),
    (select id from sets where short_name = 'bbd'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blaring Captain'),
    (select id from sets where short_name = 'bbd'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oracle''s Insight'),
    (select id from sets where short_name = 'bbd'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enthralling Victor'),
    (select id from sets where short_name = 'bbd'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'bbd'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relentless Hunter'),
    (select id from sets where short_name = 'bbd'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Primal Huntbeast'),
    (select id from sets where short_name = 'bbd'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battle-Rattle Shaman'),
    (select id from sets where short_name = 'bbd'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seedborn Muse'),
    (select id from sets where short_name = 'bbd'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Switcheroo'),
    (select id from sets where short_name = 'bbd'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quest for the Gravelord'),
    (select id from sets where short_name = 'bbd'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Generous Patron'),
    (select id from sets where short_name = 'bbd'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kitesail Corsair'),
    (select id from sets where short_name = 'bbd'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind''s Eye'),
    (select id from sets where short_name = 'bbd'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pir, Imaginative Rascal'),
    (select id from sets where short_name = 'bbd'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Talons'),
    (select id from sets where short_name = 'bbd'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jelenn Sphinx'),
    (select id from sets where short_name = 'bbd'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Khorvath Brightflame'),
    (select id from sets where short_name = 'bbd'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arena Rector'),
    (select id from sets where short_name = 'bbd'),
    '23',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tandem Tactics'),
    (select id from sets where short_name = 'bbd'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saddleback Lagac'),
    (select id from sets where short_name = 'bbd'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Morphic Pool'),
    (select id from sets where short_name = 'bbd'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fumble'),
    (select id from sets where short_name = 'bbd'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spire Garden'),
    (select id from sets where short_name = 'bbd'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Earth Elemental'),
    (select id from sets where short_name = 'bbd'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daggerback Basilisk'),
    (select id from sets where short_name = 'bbd'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unflinching Courage'),
    (select id from sets where short_name = 'bbd'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stunning Reversal'),
    (select id from sets where short_name = 'bbd'),
    '51',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Krav, the Unredeemed'),
    (select id from sets where short_name = 'bbd'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Charging Binox'),
    (select id from sets where short_name = 'bbd'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enduring Scalelord'),
    (select id from sets where short_name = 'bbd'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcane Artisan'),
    (select id from sets where short_name = 'bbd'),
    '33',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Consulate Skygate'),
    (select id from sets where short_name = 'bbd'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daggerdrome Imp'),
    (select id from sets where short_name = 'bbd'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Long Road Home'),
    (select id from sets where short_name = 'bbd'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'bbd'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spell Snare'),
    (select id from sets where short_name = 'bbd'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'bbd'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Virtus''s Maneuver'),
    (select id from sets where short_name = 'bbd'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spellseeker'),
    (select id from sets where short_name = 'bbd'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightwalker'),
    (select id from sets where short_name = 'bbd'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zndrsplt, Eye of Wisdom'),
    (select id from sets where short_name = 'bbd'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karametra''s Favor'),
    (select id from sets where short_name = 'bbd'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frost Lynx'),
    (select id from sets where short_name = 'bbd'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rowan Kenrith'),
    (select id from sets where short_name = 'bbd'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Evil Twin'),
    (select id from sets where short_name = 'bbd'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Peace Strider'),
    (select id from sets where short_name = 'bbd'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrasher Brute'),
    (select id from sets where short_name = 'bbd'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tenacious Dead'),
    (select id from sets where short_name = 'bbd'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azra Oddsmaker'),
    (select id from sets where short_name = 'bbd'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dinrova Horror'),
    (select id from sets where short_name = 'bbd'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel of Retribution'),
    (select id from sets where short_name = 'bbd'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Visionary'),
    (select id from sets where short_name = 'bbd'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Land Tax'),
    (select id from sets where short_name = 'bbd'),
    '94',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Luxury Suite'),
    (select id from sets where short_name = 'bbd'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kiss of the Amesha'),
    (select id from sets where short_name = 'bbd'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grotesque Mutation'),
    (select id from sets where short_name = 'bbd'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Hatchling'),
    (select id from sets where short_name = 'bbd'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lava-Field Overlord'),
    (select id from sets where short_name = 'bbd'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Play of the Game'),
    (select id from sets where short_name = 'bbd'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bathe in Dragonfire'),
    (select id from sets where short_name = 'bbd'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodborn Scoundrels'),
    (select id from sets where short_name = 'bbd'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spellweaver Duo'),
    (select id from sets where short_name = 'bbd'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Midnight Guard'),
    (select id from sets where short_name = 'bbd'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Last One Standing'),
    (select id from sets where short_name = 'bbd'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soulblade Renewer'),
    (select id from sets where short_name = 'bbd'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cheering Fanatic'),
    (select id from sets where short_name = 'bbd'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Impulse'),
    (select id from sets where short_name = 'bbd'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tavern Swindler'),
    (select id from sets where short_name = 'bbd'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skystreamer'),
    (select id from sets where short_name = 'bbd'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Will Kenrith'),
    (select id from sets where short_name = 'bbd'),
    '255',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Out of Bounds'),
    (select id from sets where short_name = 'bbd'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Totally Lost'),
    (select id from sets where short_name = 'bbd'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fog Bank'),
    (select id from sets where short_name = 'bbd'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantom Warrior'),
    (select id from sets where short_name = 'bbd'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Genesis Chamber'),
    (select id from sets where short_name = 'bbd'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Crowd Goes Wild'),
    (select id from sets where short_name = 'bbd'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mycosynth Lattice'),
    (select id from sets where short_name = 'bbd'),
    '241',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blaring Recruiter'),
    (select id from sets where short_name = 'bbd'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shambling Ghoul'),
    (select id from sets where short_name = 'bbd'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rebuke'),
    (select id from sets where short_name = 'bbd'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wandering Wolf'),
    (select id from sets where short_name = 'bbd'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'bbd'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hexplate Golem'),
    (select id from sets where short_name = 'bbd'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Lightsmith'),
    (select id from sets where short_name = 'bbd'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'bbd'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tidespout Tyrant'),
    (select id from sets where short_name = 'bbd'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eager Construct'),
    (select id from sets where short_name = 'bbd'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yotian Soldier'),
    (select id from sets where short_name = 'bbd'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mangara of Corondor'),
    (select id from sets where short_name = 'bbd'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Feud'),
    (select id from sets where short_name = 'bbd'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Victory Chimes'),
    (select id from sets where short_name = 'bbd'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Expedition Raptor'),
    (select id from sets where short_name = 'bbd'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Omenspeaker'),
    (select id from sets where short_name = 'bbd'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Borderland Marauder'),
    (select id from sets where short_name = 'bbd'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pathmaker Initiate'),
    (select id from sets where short_name = 'bbd'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fertilid'),
    (select id from sets where short_name = 'bbd'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Proud Mentor'),
    (select id from sets where short_name = 'bbd'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stone Golem'),
    (select id from sets where short_name = 'bbd'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plated Crusher'),
    (select id from sets where short_name = 'bbd'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Screeching Buzzard'),
    (select id from sets where short_name = 'bbd'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvia Brightspear'),
    (select id from sets where short_name = 'bbd'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beast Within'),
    (select id from sets where short_name = 'bbd'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grothama, All-Devouring'),
    (select id from sets where short_name = 'bbd'),
    '71',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Boldwyr Intimidator'),
    (select id from sets where short_name = 'bbd'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thunder Strike'),
    (select id from sets where short_name = 'bbd'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Noxious Dragon'),
    (select id from sets where short_name = 'bbd'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Saltwater Stalwart'),
    (select id from sets where short_name = 'bbd'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brightling'),
    (select id from sets where short_name = 'bbd'),
    '25',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blaze'),
    (select id from sets where short_name = 'bbd'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kor Spiritdancer'),
    (select id from sets where short_name = 'bbd'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Culling Dais'),
    (select id from sets where short_name = 'bbd'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chakram Retriever'),
    (select id from sets where short_name = 'bbd'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diabolic Intent'),
    (select id from sets where short_name = 'bbd'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ember Beast'),
    (select id from sets where short_name = 'bbd'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pir''s Whim'),
    (select id from sets where short_name = 'bbd'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noosegraf Mob'),
    (select id from sets where short_name = 'bbd'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'bbd'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Wayfinder'),
    (select id from sets where short_name = 'bbd'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Together Forever'),
    (select id from sets where short_name = 'bbd'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stolen Strategy'),
    (select id from sets where short_name = 'bbd'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Game Plan'),
    (select id from sets where short_name = 'bbd'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Call to Heel'),
    (select id from sets where short_name = 'bbd'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Will Kenrith'),
    (select id from sets where short_name = 'bbd'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wrap in Flames'),
    (select id from sets where short_name = 'bbd'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fertile Ground'),
    (select id from sets where short_name = 'bbd'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swarm of Bloodflies'),
    (select id from sets where short_name = 'bbd'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Archon of Valor''s Reach'),
    (select id from sets where short_name = 'bbd'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prakhata Club Security'),
    (select id from sets where short_name = 'bbd'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pulse of Murasa'),
    (select id from sets where short_name = 'bbd'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silverchase Fox'),
    (select id from sets where short_name = 'bbd'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'bbd'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chain Lightning'),
    (select id from sets where short_name = 'bbd'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gold-Forged Sentinel'),
    (select id from sets where short_name = 'bbd'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kraken Hatchling'),
    (select id from sets where short_name = 'bbd'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bramble Sovereign'),
    (select id from sets where short_name = 'bbd'),
    '65',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jubilant Mascot'),
    (select id from sets where short_name = 'bbd'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steppe Glider'),
    (select id from sets where short_name = 'bbd'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Riptide Crab'),
    (select id from sets where short_name = 'bbd'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Reveler'),
    (select id from sets where short_name = 'bbd'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Assassin''s Strike'),
    (select id from sets where short_name = 'bbd'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flamewave Invoker'),
    (select id from sets where short_name = 'bbd'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampire Charmseeker'),
    (select id from sets where short_name = 'bbd'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Royal Trooper'),
    (select id from sets where short_name = 'bbd'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hand of Silumgar'),
    (select id from sets where short_name = 'bbd'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Okaun, Eye of Chaos'),
    (select id from sets where short_name = 'bbd'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Razerunners'),
    (select id from sets where short_name = 'bbd'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'bbd'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Combo Attack'),
    (select id from sets where short_name = 'bbd'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Impetuous Protege'),
    (select id from sets where short_name = 'bbd'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelic Chorus'),
    (select id from sets where short_name = 'bbd'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gwafa Hazid, Profiteer'),
    (select id from sets where short_name = 'bbd'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azra Bladeseeker'),
    (select id from sets where short_name = 'bbd'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battle Rampart'),
    (select id from sets where short_name = 'bbd'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inner Demon'),
    (select id from sets where short_name = 'bbd'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Veteran Explorer'),
    (select id from sets where short_name = 'bbd'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Painful Lesson'),
    (select id from sets where short_name = 'bbd'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loyal Pegasus'),
    (select id from sets where short_name = 'bbd'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bring Down'),
    (select id from sets where short_name = 'bbd'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bonus Round'),
    (select id from sets where short_name = 'bbd'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Solemn Offering'),
    (select id from sets where short_name = 'bbd'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fill with Fright'),
    (select id from sets where short_name = 'bbd'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nirkana Revenant'),
    (select id from sets where short_name = 'bbd'),
    '150',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Take Up Arms'),
    (select id from sets where short_name = 'bbd'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Greater Good'),
    (select id from sets where short_name = 'bbd'),
    '201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Centaur Healer'),
    (select id from sets where short_name = 'bbd'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magmatic Force'),
    (select id from sets where short_name = 'bbd'),
    '181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vigor'),
    (select id from sets where short_name = 'bbd'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Toothy, Imaginary Friend'),
    (select id from sets where short_name = 'bbd'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Juggernaut'),
    (select id from sets where short_name = 'bbd'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magma Hellion'),
    (select id from sets where short_name = 'bbd'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bull-Rush Bruiser'),
    (select id from sets where short_name = 'bbd'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charging Rhino'),
    (select id from sets where short_name = 'bbd'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shoulder to Shoulder'),
    (select id from sets where short_name = 'bbd'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lore Weaver'),
    (select id from sets where short_name = 'bbd'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cowl Prowler'),
    (select id from sets where short_name = 'bbd'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magus of the Candelabra'),
    (select id from sets where short_name = 'bbd'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystic Confluence'),
    (select id from sets where short_name = 'bbd'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rushblade Commander'),
    (select id from sets where short_name = 'bbd'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aim High'),
    (select id from sets where short_name = 'bbd'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nimbus of the Isles'),
    (select id from sets where short_name = 'bbd'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'bbd'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liturgy of Blood'),
    (select id from sets where short_name = 'bbd'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Peregrine Drake'),
    (select id from sets where short_name = 'bbd'),
    '128',
    'uncommon'
) 
 on conflict do nothing;
