insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tm13'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tm13'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana of the Dark Realms Emblem'),
    (select id from sets where short_name = 'tm13'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hellion'),
    (select id from sets where short_name = 'tm13'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tm13'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drake'),
    (select id from sets where short_name = 'tm13'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurm'),
    (select id from sets where short_name = 'tm13'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tm13'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tm13'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat'),
    (select id from sets where short_name = 'tm13'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goat'),
    (select id from sets where short_name = 'tm13'),
    '2',
    'common'
) 
 on conflict do nothing;
