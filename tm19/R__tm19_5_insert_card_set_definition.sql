insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tm19'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ajani, Adversary of Tyrants Emblem'),
    (select id from sets where short_name = 'tm19'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivien Reid Emblem'),
    (select id from sets where short_name = 'tm19'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tm19'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tm19'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avatar'),
    (select id from sets where short_name = 'tm19'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Core Set 2019 Checklist'),
    (select id from sets where short_name = 'tm19'),
    'CH1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elf Warrior'),
    (select id from sets where short_name = 'tm19'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tm19'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ox'),
    (select id from sets where short_name = 'tm19'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat'),
    (select id from sets where short_name = 'tm19'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter'),
    (select id from sets where short_name = 'tm19'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'tm19'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tm19'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bat'),
    (select id from sets where short_name = 'tm19'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tm19'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tezzeret, Artifice Master Emblem'),
    (select id from sets where short_name = 'tm19'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight'),
    (select id from sets where short_name = 'tm19'),
    '4',
    'common'
) 
 on conflict do nothing;
