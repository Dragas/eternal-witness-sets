insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Wilt-Leaf Cavaliers'),
    (select id from sets where short_name = 'pg08'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cenn''s Tactician'),
    (select id from sets where short_name = 'pg08'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oona''s Blackguard'),
    (select id from sets where short_name = 'pg08'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boggart Ram-Gang'),
    (select id from sets where short_name = 'pg08'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'pg08'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lava Axe'),
    (select id from sets where short_name = 'pg08'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duergar Hedge-Mage'),
    (select id from sets where short_name = 'pg08'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selkie Hedge-Mage'),
    (select id from sets where short_name = 'pg08'),
    '20',
    'rare'
) 
 on conflict do nothing;
