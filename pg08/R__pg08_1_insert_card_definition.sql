    insert into mtgcard(name) values ('Wilt-Leaf Cavaliers') on conflict do nothing;
    insert into mtgcard(name) values ('Cenn''s Tactician') on conflict do nothing;
    insert into mtgcard(name) values ('Oona''s Blackguard') on conflict do nothing;
    insert into mtgcard(name) values ('Boggart Ram-Gang') on conflict do nothing;
    insert into mtgcard(name) values ('Gravedigger') on conflict do nothing;
    insert into mtgcard(name) values ('Lava Axe') on conflict do nothing;
    insert into mtgcard(name) values ('Duergar Hedge-Mage') on conflict do nothing;
    insert into mtgcard(name) values ('Selkie Hedge-Mage') on conflict do nothing;
