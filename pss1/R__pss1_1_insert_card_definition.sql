    insert into mtgcard(name) values ('Smoldering Marsh') on conflict do nothing;
    insert into mtgcard(name) values ('Canopy Vista') on conflict do nothing;
    insert into mtgcard(name) values ('Cinder Glade') on conflict do nothing;
    insert into mtgcard(name) values ('Sunken Hollow') on conflict do nothing;
    insert into mtgcard(name) values ('Prairie Stream') on conflict do nothing;
