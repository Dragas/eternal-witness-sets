insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Smoldering Marsh'),
    (select id from sets where short_name = 'pss1'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Canopy Vista'),
    (select id from sets where short_name = 'pss1'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cinder Glade'),
    (select id from sets where short_name = 'pss1'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunken Hollow'),
    (select id from sets where short_name = 'pss1'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prairie Stream'),
    (select id from sets where short_name = 'pss1'),
    '241',
    'rare'
) 
 on conflict do nothing;
