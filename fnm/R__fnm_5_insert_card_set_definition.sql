insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = 'fnm'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = 'fnm'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'River Boa'),
    (select id from sets where short_name = 'fnm'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Warp'),
    (select id from sets where short_name = 'fnm'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Staunch Defenders'),
    (select id from sets where short_name = 'fnm'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'fnm'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'fnm'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Geyser'),
    (select id from sets where short_name = 'fnm'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'fnm'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Longbow Archer'),
    (select id from sets where short_name = 'fnm'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prodigal Sorcerer'),
    (select id from sets where short_name = 'fnm'),
    '9',
    'rare'
) 
 on conflict do nothing;
