insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dkm'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guerrilla Tactics'),
    (select id from sets where short_name = 'dkm'),
    '13a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foul Familiar'),
    (select id from sets where short_name = 'dkm'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Trap Door Spider'),
    (select id from sets where short_name = 'dkm'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dkm'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'dkm'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balduvian Horde'),
    (select id from sets where short_name = 'dkm'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = 'dkm'),
    '14★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Burn'),
    (select id from sets where short_name = 'dkm'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bounty of the Hunt'),
    (select id from sets where short_name = 'dkm'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karplusan Forest'),
    (select id from sets where short_name = 'dkm'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'dkm'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lhurgoyf'),
    (select id from sets where short_name = 'dkm'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Fiend'),
    (select id from sets where short_name = 'dkm'),
    '8b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Ants'),
    (select id from sets where short_name = 'dkm'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lava Burst'),
    (select id from sets where short_name = 'dkm'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pillage'),
    (select id from sets where short_name = 'dkm'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'dkm'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abyssal Specter'),
    (select id from sets where short_name = 'dkm'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balduvian Bears'),
    (select id from sets where short_name = 'dkm'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lim-Dûl''s High Guard'),
    (select id from sets where short_name = 'dkm'),
    '6b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necropotence'),
    (select id from sets where short_name = 'dkm'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian War Beast'),
    (select id from sets where short_name = 'dkm'),
    '37b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = 'dkm'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Storm Shaman'),
    (select id from sets where short_name = 'dkm'),
    '21b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Mutant'),
    (select id from sets where short_name = 'dkm'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Cannoneers'),
    (select id from sets where short_name = 'dkm'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyroclasm'),
    (select id from sets where short_name = 'dkm'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Contagion'),
    (select id from sets where short_name = 'dkm'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woolly Spider'),
    (select id from sets where short_name = 'dkm'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Ancients'),
    (select id from sets where short_name = 'dkm'),
    '31a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barbed Sextant'),
    (select id from sets where short_name = 'dkm'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Spark'),
    (select id from sets where short_name = 'dkm'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Fiend'),
    (select id from sets where short_name = 'dkm'),
    '8a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Banishing'),
    (select id from sets where short_name = 'dkm'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sulfurous Springs'),
    (select id from sets where short_name = 'dkm'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lim-Dûl''s High Guard'),
    (select id from sets where short_name = 'dkm'),
    '6a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'dkm'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dkm'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Folk of the Pines'),
    (select id from sets where short_name = 'dkm'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guerrilla Tactics'),
    (select id from sets where short_name = 'dkm'),
    '13b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dkm'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dkm'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icy Manipulator'),
    (select id from sets where short_name = 'dkm'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = 'dkm'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elkin Bottle'),
    (select id from sets where short_name = 'dkm'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hurricane'),
    (select id from sets where short_name = 'dkm'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'dkm'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Walking Wall'),
    (select id from sets where short_name = 'dkm'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Ancients'),
    (select id from sets where short_name = 'dkm'),
    '31b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Storm Shaman'),
    (select id from sets where short_name = 'dkm'),
    '21a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fyndhorn Elves'),
    (select id from sets where short_name = 'dkm'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jokulhaups'),
    (select id from sets where short_name = 'dkm'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dkm'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian War Beast'),
    (select id from sets where short_name = 'dkm'),
    '37a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icy Manipulator'),
    (select id from sets where short_name = 'dkm'),
    '36★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Underground River'),
    (select id from sets where short_name = 'dkm'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Bard'),
    (select id from sets where short_name = 'dkm'),
    '24',
    'uncommon'
) 
 on conflict do nothing;
