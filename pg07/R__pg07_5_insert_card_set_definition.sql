insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'pg07'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Calciderm'),
    (select id from sets where short_name = 'pg07'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boomerang'),
    (select id from sets where short_name = 'pg07'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Stone'),
    (select id from sets where short_name = 'pg07'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zoetic Cavern'),
    (select id from sets where short_name = 'pg07'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yixlid Jailer'),
    (select id from sets where short_name = 'pg07'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dauntless Dourbark'),
    (select id from sets where short_name = 'pg07'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reckless Wurm'),
    (select id from sets where short_name = 'pg07'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mogg Fanatic'),
    (select id from sets where short_name = 'pg07'),
    '10',
    'rare'
) 
 on conflict do nothing;
