    insert into mtgcard(name) values ('Llanowar Elves') on conflict do nothing;
    insert into mtgcard(name) values ('Calciderm') on conflict do nothing;
    insert into mtgcard(name) values ('Boomerang') on conflict do nothing;
    insert into mtgcard(name) values ('Mind Stone') on conflict do nothing;
    insert into mtgcard(name) values ('Zoetic Cavern') on conflict do nothing;
    insert into mtgcard(name) values ('Yixlid Jailer') on conflict do nothing;
    insert into mtgcard(name) values ('Dauntless Dourbark') on conflict do nothing;
    insert into mtgcard(name) values ('Reckless Wurm') on conflict do nothing;
    insert into mtgcard(name) values ('Mogg Fanatic') on conflict do nothing;
