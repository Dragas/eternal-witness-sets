insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Fleetfoot Panther'),
    (select id from sets where short_name = 'ddh'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fire-Field Ogre'),
    (select id from sets where short_name = 'ddh'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddh'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Profane Command'),
    (select id from sets where short_name = 'ddh'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'ddh'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Recoil'),
    (select id from sets where short_name = 'ddh'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hellfire Mongrel'),
    (select id from sets where short_name = 'ddh'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Countersquall'),
    (select id from sets where short_name = 'ddh'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddh'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Recumbent Bliss'),
    (select id from sets where short_name = 'ddh'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icy Manipulator'),
    (select id from sets where short_name = 'ddh'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rise // Fall'),
    (select id from sets where short_name = 'ddh'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vitu-Ghazi, the City-Tree'),
    (select id from sets where short_name = 'ddh'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddh'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Ranger'),
    (select id from sets where short_name = 'ddh'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, Planeswalker'),
    (select id from sets where short_name = 'ddh'),
    '42',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Qasali Pridemage'),
    (select id from sets where short_name = 'ddh'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elder Mastery'),
    (select id from sets where short_name = 'ddh'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Nacatl'),
    (select id from sets where short_name = 'ddh'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shriekmaw'),
    (select id from sets where short_name = 'ddh'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spite // Malice'),
    (select id from sets where short_name = 'ddh'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jungle Shrine'),
    (select id from sets where short_name = 'ddh'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Helix'),
    (select id from sets where short_name = 'ddh'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Briarhorn'),
    (select id from sets where short_name = 'ddh'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Searing Meditation'),
    (select id from sets where short_name = 'ddh'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddh'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kird Ape'),
    (select id from sets where short_name = 'ddh'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Bounty'),
    (select id from sets where short_name = 'ddh'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slave of Bolas'),
    (select id from sets where short_name = 'ddh'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'ddh'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loxodon Hierarch'),
    (select id from sets where short_name = 'ddh'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jhessian Zombies'),
    (select id from sets where short_name = 'ddh'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vapor Snag'),
    (select id from sets where short_name = 'ddh'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddh'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woolly Thoctar'),
    (select id from sets where short_name = 'ddh'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kazandu Refuge'),
    (select id from sets where short_name = 'ddh'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steamcore Weird'),
    (select id from sets where short_name = 'ddh'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spitemare'),
    (select id from sets where short_name = 'ddh'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cruel Ultimatum'),
    (select id from sets where short_name = 'ddh'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crumbling Necropolis'),
    (select id from sets where short_name = 'ddh'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Surveilling Sprite'),
    (select id from sets where short_name = 'ddh'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deep Analysis'),
    (select id from sets where short_name = 'ddh'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lead the Stampede'),
    (select id from sets where short_name = 'ddh'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moroii'),
    (select id from sets where short_name = 'ddh'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nacatl Hunt-Pride'),
    (select id from sets where short_name = 'ddh'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Griffin Guide'),
    (select id from sets where short_name = 'ddh'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Undermine'),
    (select id from sets where short_name = 'ddh'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ageless Entity'),
    (select id from sets where short_name = 'ddh'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Morgue Toad'),
    (select id from sets where short_name = 'ddh'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slavering Nulls'),
    (select id from sets where short_name = 'ddh'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brackwater Elemental'),
    (select id from sets where short_name = 'ddh'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Titanic Ultimatum'),
    (select id from sets where short_name = 'ddh'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Essence Warden'),
    (select id from sets where short_name = 'ddh'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blazing Specter'),
    (select id from sets where short_name = 'ddh'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Graypelt Refuge'),
    (select id from sets where short_name = 'ddh'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Cutpurse'),
    (select id from sets where short_name = 'ddh'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pride of Lions'),
    (select id from sets where short_name = 'ddh'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Igneous Pouncer'),
    (select id from sets where short_name = 'ddh'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Agonizing Demise'),
    (select id from sets where short_name = 'ddh'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naya Charm'),
    (select id from sets where short_name = 'ddh'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sapseep Forest'),
    (select id from sets where short_name = 'ddh'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddh'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Grixis'),
    (select id from sets where short_name = 'ddh'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddh'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightscape Familiar'),
    (select id from sets where short_name = 'ddh'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Behemoth Sledge'),
    (select id from sets where short_name = 'ddh'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Pridemate'),
    (select id from sets where short_name = 'ddh'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marisi''s Twinclaws'),
    (select id from sets where short_name = 'ddh'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pain // Suffering'),
    (select id from sets where short_name = 'ddh'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loam Lion'),
    (select id from sets where short_name = 'ddh'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jade Mage'),
    (select id from sets where short_name = 'ddh'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rupture Spire'),
    (select id from sets where short_name = 'ddh'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Mantra'),
    (select id from sets where short_name = 'ddh'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Firemane Angel'),
    (select id from sets where short_name = 'ddh'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Canyon Wildcat'),
    (select id from sets where short_name = 'ddh'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ogre Savant'),
    (select id from sets where short_name = 'ddh'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grazing Gladehart'),
    (select id from sets where short_name = 'ddh'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ajani Vengeant'),
    (select id from sets where short_name = 'ddh'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddh'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grixis Charm'),
    (select id from sets where short_name = 'ddh'),
    '63',
    'uncommon'
) 
 on conflict do nothing;
