insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'tmp'),
    '340',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armored Pegasus'),
    (select id from sets where short_name = 'tmp'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Canyon Wildcat'),
    (select id from sets where short_name = 'tmp'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scabland'),
    (select id from sets where short_name = 'tmp'),
    '325',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winged Sliver'),
    (select id from sets where short_name = 'tmp'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rootwalla'),
    (select id from sets where short_name = 'tmp'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Blue'),
    (select id from sets where short_name = 'tmp'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disturbed Burial'),
    (select id from sets where short_name = 'tmp'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Echo Chamber'),
    (select id from sets where short_name = 'tmp'),
    '282',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Frenzy'),
    (select id from sets where short_name = 'tmp'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Souldrinker'),
    (select id from sets where short_name = 'tmp'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tradewind Rider'),
    (select id from sets where short_name = 'tmp'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dauthi Ghoul'),
    (select id from sets where short_name = 'tmp'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overrun'),
    (select id from sets where short_name = 'tmp'),
    '243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eladamri, Lord of Leaves'),
    (select id from sets where short_name = 'tmp'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rolling Thunder'),
    (select id from sets where short_name = 'tmp'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nurturing Licid'),
    (select id from sets where short_name = 'tmp'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jinxed Idol'),
    (select id from sets where short_name = 'tmp'),
    '293',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verdigris'),
    (select id from sets where short_name = 'tmp'),
    '264',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chaotic Goo'),
    (select id from sets where short_name = 'tmp'),
    '168',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Screeching Harpy'),
    (select id from sets where short_name = 'tmp'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Crab'),
    (select id from sets where short_name = 'tmp'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Elemental'),
    (select id from sets where short_name = 'tmp'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thalakos Seer'),
    (select id from sets where short_name = 'tmp'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vec Townships'),
    (select id from sets where short_name = 'tmp'),
    '329',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquility'),
    (select id from sets where short_name = 'tmp'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Bombardment'),
    (select id from sets where short_name = 'tmp'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Segmented Wurm'),
    (select id from sets where short_name = 'tmp'),
    '269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barbed Sliver'),
    (select id from sets where short_name = 'tmp'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soltari Guerrillas'),
    (select id from sets where short_name = 'tmp'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gallantry'),
    (select id from sets where short_name = 'tmp'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sudden Impact'),
    (select id from sets where short_name = 'tmp'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Insight'),
    (select id from sets where short_name = 'tmp'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dismiss'),
    (select id from sets where short_name = 'tmp'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Worthy Cause'),
    (select id from sets where short_name = 'tmp'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Natural Spring'),
    (select id from sets where short_name = 'tmp'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reality Anchor'),
    (select id from sets where short_name = 'tmp'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Salt Flats'),
    (select id from sets where short_name = 'tmp'),
    '324',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'tmp'),
    '335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Respite'),
    (select id from sets where short_name = 'tmp'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reap'),
    (select id from sets where short_name = 'tmp'),
    '247',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Canyon Drake'),
    (select id from sets where short_name = 'tmp'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trained Armodon'),
    (select id from sets where short_name = 'tmp'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kezzerdrix'),
    (select id from sets where short_name = 'tmp'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'tmp'),
    '346',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pit Imp'),
    (select id from sets where short_name = 'tmp'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruby Medallion'),
    (select id from sets where short_name = 'tmp'),
    '305',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Apes of Rath'),
    (select id from sets where short_name = 'tmp'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aluren'),
    (select id from sets where short_name = 'tmp'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Elf'),
    (select id from sets where short_name = 'tmp'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bellowing Fiend'),
    (select id from sets where short_name = 'tmp'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heart Sliver'),
    (select id from sets where short_name = 'tmp'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leeching Licid'),
    (select id from sets where short_name = 'tmp'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avenging Angel'),
    (select id from sets where short_name = 'tmp'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elven Warhounds'),
    (select id from sets where short_name = 'tmp'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verdant Force'),
    (select id from sets where short_name = 'tmp'),
    '263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mnemonic Sliver'),
    (select id from sets where short_name = 'tmp'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Squee''s Toy'),
    (select id from sets where short_name = 'tmp'),
    '309',
    'common'
) ,
(
    (select id from mtgcard where name = 'Safeguard'),
    (select id from sets where short_name = 'tmp'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orim''s Prayer'),
    (select id from sets where short_name = 'tmp'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coercion'),
    (select id from sets where short_name = 'tmp'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Diffusion'),
    (select id from sets where short_name = 'tmp'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boil'),
    (select id from sets where short_name = 'tmp'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Grimoire'),
    (select id from sets where short_name = 'tmp'),
    '301',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Metallic Sliver'),
    (select id from sets where short_name = 'tmp'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Hulk'),
    (select id from sets where short_name = 'tmp'),
    '302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Tomb'),
    (select id from sets where short_name = 'tmp'),
    '315',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cursed Scroll'),
    (select id from sets where short_name = 'tmp'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'tmp'),
    '350',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soltari Trooper'),
    (select id from sets where short_name = 'tmp'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scalding Tongs'),
    (select id from sets where short_name = 'tmp'),
    '307',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Interdict'),
    (select id from sets where short_name = 'tmp'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Time Warp'),
    (select id from sets where short_name = 'tmp'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spontaneous Combustion'),
    (select id from sets where short_name = 'tmp'),
    '273',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dracoplasm'),
    (select id from sets where short_name = 'tmp'),
    '266',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Static Orb'),
    (select id from sets where short_name = 'tmp'),
    '310',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soltari Priest'),
    (select id from sets where short_name = 'tmp'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ertai''s Meddling'),
    (select id from sets where short_name = 'tmp'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tahngarth''s Rage'),
    (select id from sets where short_name = 'tmp'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shadowstorm'),
    (select id from sets where short_name = 'tmp'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'tmp'),
    '347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jackal Pup'),
    (select id from sets where short_name = 'tmp'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trumpeting Armodon'),
    (select id from sets where short_name = 'tmp'),
    '262',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frog Tongue'),
    (select id from sets where short_name = 'tmp'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spinal Graft'),
    (select id from sets where short_name = 'tmp'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudchaser Eagle'),
    (select id from sets where short_name = 'tmp'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fevered Convulsions'),
    (select id from sets where short_name = 'tmp'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sapphire Medallion'),
    (select id from sets where short_name = 'tmp'),
    '306',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'tmp'),
    '333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harrow'),
    (select id from sets where short_name = 'tmp'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Manakin'),
    (select id from sets where short_name = 'tmp'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Repentance'),
    (select id from sets where short_name = 'tmp'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'tmp'),
    '336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = 'tmp'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghost Town'),
    (select id from sets where short_name = 'tmp'),
    '318',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coiled Tinviper'),
    (select id from sets where short_name = 'tmp'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flailing Drake'),
    (select id from sets where short_name = 'tmp'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pearl Medallion'),
    (select id from sets where short_name = 'tmp'),
    '300',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angelic Protector'),
    (select id from sets where short_name = 'tmp'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flowstone Giant'),
    (select id from sets where short_name = 'tmp'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mogg Hollows'),
    (select id from sets where short_name = 'tmp'),
    '320',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magmasaur'),
    (select id from sets where short_name = 'tmp'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Shadow'),
    (select id from sets where short_name = 'tmp'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ranger en-Vec'),
    (select id from sets where short_name = 'tmp'),
    '268',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wood Sage'),
    (select id from sets where short_name = 'tmp'),
    '275',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mogg Squad'),
    (select id from sets where short_name = 'tmp'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heartwood Giant'),
    (select id from sets where short_name = 'tmp'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rootwater Depths'),
    (select id from sets where short_name = 'tmp'),
    '323',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darkling Stalker'),
    (select id from sets where short_name = 'tmp'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Altar of Dementia'),
    (select id from sets where short_name = 'tmp'),
    '276',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mounted Archers'),
    (select id from sets where short_name = 'tmp'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gerrard''s Battle Cry'),
    (select id from sets where short_name = 'tmp'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soltari Monk'),
    (select id from sets where short_name = 'tmp'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deadshot'),
    (select id from sets where short_name = 'tmp'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Torture Chamber'),
    (select id from sets where short_name = 'tmp'),
    '313',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mongrel Pack'),
    (select id from sets where short_name = 'tmp'),
    '237',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shadow Rift'),
    (select id from sets where short_name = 'tmp'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'tmp'),
    '339',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chill'),
    (select id from sets where short_name = 'tmp'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blood Pet'),
    (select id from sets where short_name = 'tmp'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spike Drone'),
    (select id from sets where short_name = 'tmp'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evincar''s Justice'),
    (select id from sets where short_name = 'tmp'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'tmp'),
    '338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mogg Cannon'),
    (select id from sets where short_name = 'tmp'),
    '298',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scroll Rack'),
    (select id from sets where short_name = 'tmp'),
    '308',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = 'tmp'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spell Blast'),
    (select id from sets where short_name = 'tmp'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Muscle Sliver'),
    (select id from sets where short_name = 'tmp'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Furnace of Rath'),
    (select id from sets where short_name = 'tmp'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pallimud'),
    (select id from sets where short_name = 'tmp'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'tmp'),
    '331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Blast'),
    (select id from sets where short_name = 'tmp'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bottle Gnomes'),
    (select id from sets where short_name = 'tmp'),
    '278',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'tmp'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Legacy''s Allure'),
    (select id from sets where short_name = 'tmp'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Charging Rhino'),
    (select id from sets where short_name = 'tmp'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dauthi Slayer'),
    (select id from sets where short_name = 'tmp'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Legerdemain'),
    (select id from sets where short_name = 'tmp'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'tmp'),
    '344',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emerald Medallion'),
    (select id from sets where short_name = 'tmp'),
    '283',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Servant of Volrath'),
    (select id from sets where short_name = 'tmp'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time Ebb'),
    (select id from sets where short_name = 'tmp'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'tmp'),
    '349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serene Offering'),
    (select id from sets where short_name = 'tmp'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Death'),
    (select id from sets where short_name = 'tmp'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lotus Petal'),
    (select id from sets where short_name = 'tmp'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soltari Crusader'),
    (select id from sets where short_name = 'tmp'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Excavator'),
    (select id from sets where short_name = 'tmp'),
    '287',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: White'),
    (select id from sets where short_name = 'tmp'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dauthi Mindripper'),
    (select id from sets where short_name = 'tmp'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orim, Samite Healer'),
    (select id from sets where short_name = 'tmp'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Light of Day'),
    (select id from sets where short_name = 'tmp'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thumbscrews'),
    (select id from sets where short_name = 'tmp'),
    '312',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tooth and Claw'),
    (select id from sets where short_name = 'tmp'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enfeeblement'),
    (select id from sets where short_name = 'tmp'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Pits of Rath'),
    (select id from sets where short_name = 'tmp'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'tmp'),
    '348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steal Enchantment'),
    (select id from sets where short_name = 'tmp'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mogg Conscripts'),
    (select id from sets where short_name = 'tmp'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lowland Giant'),
    (select id from sets where short_name = 'tmp'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thalakos Sentry'),
    (select id from sets where short_name = 'tmp'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirri''s Guile'),
    (select id from sets where short_name = 'tmp'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whispers of the Muse'),
    (select id from sets where short_name = 'tmp'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Precognition'),
    (select id from sets where short_name = 'tmp'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corpse Dance'),
    (select id from sets where short_name = 'tmp'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Telethopter'),
    (select id from sets where short_name = 'tmp'),
    '311',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Imps'' Taunt'),
    (select id from sets where short_name = 'tmp'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Helm of Possession'),
    (select id from sets where short_name = 'tmp'),
    '291',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winter''s Grasp'),
    (select id from sets where short_name = 'tmp'),
    '265',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reflecting Pool'),
    (select id from sets where short_name = 'tmp'),
    '322',
    'rare'
) ,
(
    (select id from mtgcard where name = 'No Quarter'),
    (select id from sets where short_name = 'tmp'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rats of Rath'),
    (select id from sets where short_name = 'tmp'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rathi Dragon'),
    (select id from sets where short_name = 'tmp'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Essence Bottle'),
    (select id from sets where short_name = 'tmp'),
    '286',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dauthi Embrace'),
    (select id from sets where short_name = 'tmp'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hanna''s Custody'),
    (select id from sets where short_name = 'tmp'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Caldera Lake'),
    (select id from sets where short_name = 'tmp'),
    '316',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sacred Guide'),
    (select id from sets where short_name = 'tmp'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flowstone Sculpture'),
    (select id from sets where short_name = 'tmp'),
    '288',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Apocalypse'),
    (select id from sets where short_name = 'tmp'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stinging Licid'),
    (select id from sets where short_name = 'tmp'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dregs of Sorrow'),
    (select id from sets where short_name = 'tmp'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Advance Scout'),
    (select id from sets where short_name = 'tmp'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fool''s Tome'),
    (select id from sets where short_name = 'tmp'),
    '289',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unstable Shapeshifter'),
    (select id from sets where short_name = 'tmp'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Severance'),
    (select id from sets where short_name = 'tmp'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Auratog'),
    (select id from sets where short_name = 'tmp'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'tmp'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dream Cache'),
    (select id from sets where short_name = 'tmp'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carrionette'),
    (select id from sets where short_name = 'tmp'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Green'),
    (select id from sets where short_name = 'tmp'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Staunch Defenders'),
    (select id from sets where short_name = 'tmp'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'tmp'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit Mirror'),
    (select id from sets where short_name = 'tmp'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knight of Dusk'),
    (select id from sets where short_name = 'tmp'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Watchdog'),
    (select id from sets where short_name = 'tmp'),
    '314',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sea Monster'),
    (select id from sets where short_name = 'tmp'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fylamarid'),
    (select id from sets where short_name = 'tmp'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shocker'),
    (select id from sets where short_name = 'tmp'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marsh Lurker'),
    (select id from sets where short_name = 'tmp'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Spite'),
    (select id from sets where short_name = 'tmp'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Energizer'),
    (select id from sets where short_name = 'tmp'),
    '285',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wind Drake'),
    (select id from sets where short_name = 'tmp'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Banishing'),
    (select id from sets where short_name = 'tmp'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Troll'),
    (select id from sets where short_name = 'tmp'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Puppet Strings'),
    (select id from sets where short_name = 'tmp'),
    '304',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kindle'),
    (select id from sets where short_name = 'tmp'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'tmp'),
    '342',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bayou Dragonfly'),
    (select id from sets where short_name = 'tmp'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abandon Hope'),
    (select id from sets where short_name = 'tmp'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mogg Raider'),
    (select id from sets where short_name = 'tmp'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'tmp'),
    '345',
    'common'
) ,
(
    (select id from mtgcard where name = 'Field of Souls'),
    (select id from sets where short_name = 'tmp'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bounty Hunter'),
    (select id from sets where short_name = 'tmp'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm Front'),
    (select id from sets where short_name = 'tmp'),
    '259',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Root Maze'),
    (select id from sets where short_name = 'tmp'),
    '250',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wasteland'),
    (select id from sets where short_name = 'tmp'),
    '330',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rootbreaker Wurm'),
    (select id from sets where short_name = 'tmp'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selenia, Dark Angel'),
    (select id from sets where short_name = 'tmp'),
    '270',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marble Titan'),
    (select id from sets where short_name = 'tmp'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'tmp'),
    '343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earthcraft'),
    (select id from sets where short_name = 'tmp'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thalakos Mistfolk'),
    (select id from sets where short_name = 'tmp'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magnetic Web'),
    (select id from sets where short_name = 'tmp'),
    '295',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mawcor'),
    (select id from sets where short_name = 'tmp'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cinder Marsh'),
    (select id from sets where short_name = 'tmp'),
    '317',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soltari Emissary'),
    (select id from sets where short_name = 'tmp'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Extinction'),
    (select id from sets where short_name = 'tmp'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pegasus Refuge'),
    (select id from sets where short_name = 'tmp'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diabolic Edict'),
    (select id from sets where short_name = 'tmp'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Humility'),
    (select id from sets where short_name = 'tmp'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firefly'),
    (select id from sets where short_name = 'tmp'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rootwater Diver'),
    (select id from sets where short_name = 'tmp'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Renegade Warlord'),
    (select id from sets where short_name = 'tmp'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shimmering Wings'),
    (select id from sets where short_name = 'tmp'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winds of Rath'),
    (select id from sets where short_name = 'tmp'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nature''s Revolt'),
    (select id from sets where short_name = 'tmp'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Condor'),
    (select id from sets where short_name = 'tmp'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Emmessi Tome'),
    (select id from sets where short_name = 'tmp'),
    '284',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krakilin'),
    (select id from sets where short_name = 'tmp'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dirtcowl Wurm'),
    (select id from sets where short_name = 'tmp'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Forest'),
    (select id from sets where short_name = 'tmp'),
    '326',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thalakos Lowlands'),
    (select id from sets where short_name = 'tmp'),
    '328',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Commander Greven il-Vec'),
    (select id from sets where short_name = 'tmp'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soltari Lancer'),
    (select id from sets where short_name = 'tmp'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Sink'),
    (select id from sets where short_name = 'tmp'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opportunist'),
    (select id from sets where short_name = 'tmp'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reanimate'),
    (select id from sets where short_name = 'tmp'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Recycle'),
    (select id from sets where short_name = 'tmp'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hand to Hand'),
    (select id from sets where short_name = 'tmp'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Canopy Spider'),
    (select id from sets where short_name = 'tmp'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fugitive Druid'),
    (select id from sets where short_name = 'tmp'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Splicer'),
    (select id from sets where short_name = 'tmp'),
    '303',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Horned Turtle'),
    (select id from sets where short_name = 'tmp'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cold Storage'),
    (select id from sets where short_name = 'tmp'),
    '280',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'tmp'),
    '337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seeker of Skybreak'),
    (select id from sets where short_name = 'tmp'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hero''s Resolve'),
    (select id from sets where short_name = 'tmp'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stalking Stones'),
    (select id from sets where short_name = 'tmp'),
    '327',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crazed Armodon'),
    (select id from sets where short_name = 'tmp'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maze of Shadows'),
    (select id from sets where short_name = 'tmp'),
    '319',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volrath''s Curse'),
    (select id from sets where short_name = 'tmp'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Patchwork Gnomes'),
    (select id from sets where short_name = 'tmp'),
    '299',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soltari Foot Soldier'),
    (select id from sets where short_name = 'tmp'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fighting Drake'),
    (select id from sets where short_name = 'tmp'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vhati il-Dal'),
    (select id from sets where short_name = 'tmp'),
    '274',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Needle Storm'),
    (select id from sets where short_name = 'tmp'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight of Dawn'),
    (select id from sets where short_name = 'tmp'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'tmp'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flowstone Salamander'),
    (select id from sets where short_name = 'tmp'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Strength'),
    (select id from sets where short_name = 'tmp'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Booby Trap'),
    (select id from sets where short_name = 'tmp'),
    '277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stun'),
    (select id from sets where short_name = 'tmp'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jet Medallion'),
    (select id from sets where short_name = 'tmp'),
    '292',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armor Sliver'),
    (select id from sets where short_name = 'tmp'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dauthi Mercenary'),
    (select id from sets where short_name = 'tmp'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dauthi Horror'),
    (select id from sets where short_name = 'tmp'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'tmp'),
    '334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aftershock'),
    (select id from sets where short_name = 'tmp'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'tmp'),
    '341',
    'common'
) ,
(
    (select id from mtgcard where name = 'Intuition'),
    (select id from sets where short_name = 'tmp'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mogg Fanatic'),
    (select id from sets where short_name = 'tmp'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anoint'),
    (select id from sets where short_name = 'tmp'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rootwater Shaman'),
    (select id from sets where short_name = 'tmp'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rain of Tears'),
    (select id from sets where short_name = 'tmp'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Twitch'),
    (select id from sets where short_name = 'tmp'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = 'tmp'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oracle en-Vec'),
    (select id from sets where short_name = 'tmp'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benthic Behemoth'),
    (select id from sets where short_name = 'tmp'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rootwater Hunter'),
    (select id from sets where short_name = 'tmp'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manta Riders'),
    (select id from sets where short_name = 'tmp'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gaseous Form'),
    (select id from sets where short_name = 'tmp'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horned Sliver'),
    (select id from sets where short_name = 'tmp'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dread of Night'),
    (select id from sets where short_name = 'tmp'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Broken Fall'),
    (select id from sets where short_name = 'tmp'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flickering Ward'),
    (select id from sets where short_name = 'tmp'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duplicity'),
    (select id from sets where short_name = 'tmp'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pine Barrens'),
    (select id from sets where short_name = 'tmp'),
    '321',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Invulnerability'),
    (select id from sets where short_name = 'tmp'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thalakos Dreamsower'),
    (select id from sets where short_name = 'tmp'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Havoc'),
    (select id from sets where short_name = 'tmp'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warmth'),
    (select id from sets where short_name = 'tmp'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scragnoth'),
    (select id from sets where short_name = 'tmp'),
    '253',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eladamri''s Vineyard'),
    (select id from sets where short_name = 'tmp'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sadistic Glee'),
    (select id from sets where short_name = 'tmp'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minion of the Wastes'),
    (select id from sets where short_name = 'tmp'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'tmp'),
    '332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Black'),
    (select id from sets where short_name = 'tmp'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'tmp'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pincher Beetles'),
    (select id from sets where short_name = 'tmp'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maddening Imp'),
    (select id from sets where short_name = 'tmp'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fireslinger'),
    (select id from sets where short_name = 'tmp'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heartwood Treefolk'),
    (select id from sets where short_name = 'tmp'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Starke of Rath'),
    (select id from sets where short_name = 'tmp'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Escaped Shapeshifter'),
    (select id from sets where short_name = 'tmp'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whim of Volrath'),
    (select id from sets where short_name = 'tmp'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heartwood Dryad'),
    (select id from sets where short_name = 'tmp'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Choke'),
    (select id from sets where short_name = 'tmp'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Runes'),
    (select id from sets where short_name = 'tmp'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Perish'),
    (select id from sets where short_name = 'tmp'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Capsize'),
    (select id from sets where short_name = 'tmp'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Wurm'),
    (select id from sets where short_name = 'tmp'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scorched Earth'),
    (select id from sets where short_name = 'tmp'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quickening Licid'),
    (select id from sets where short_name = 'tmp'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Searing Touch'),
    (select id from sets where short_name = 'tmp'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Endless Scream'),
    (select id from sets where short_name = 'tmp'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elite Javelineer'),
    (select id from sets where short_name = 'tmp'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wind Dancer'),
    (select id from sets where short_name = 'tmp'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sandstone Warrior'),
    (select id from sets where short_name = 'tmp'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sarcomancy'),
    (select id from sets where short_name = 'tmp'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sky Spirit'),
    (select id from sets where short_name = 'tmp'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rootwater Matriarch'),
    (select id from sets where short_name = 'tmp'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meditate'),
    (select id from sets where short_name = 'tmp'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coffin Queen'),
    (select id from sets where short_name = 'tmp'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flowstone Wyvern'),
    (select id from sets where short_name = 'tmp'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lobotomy'),
    (select id from sets where short_name = 'tmp'),
    '267',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dauthi Marauder'),
    (select id from sets where short_name = 'tmp'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Vampire'),
    (select id from sets where short_name = 'tmp'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Ranger'),
    (select id from sets where short_name = 'tmp'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talon Sliver'),
    (select id from sets where short_name = 'tmp'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crown of Flames'),
    (select id from sets where short_name = 'tmp'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Master Decoy'),
    (select id from sets where short_name = 'tmp'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grindstone'),
    (select id from sets where short_name = 'tmp'),
    '290',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Propaganda'),
    (select id from sets where short_name = 'tmp'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = 'tmp'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Fury'),
    (select id from sets where short_name = 'tmp'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mindwhip Sliver'),
    (select id from sets where short_name = 'tmp'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enraging Licid'),
    (select id from sets where short_name = 'tmp'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clergy en-Vec'),
    (select id from sets where short_name = 'tmp'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clot Sliver'),
    (select id from sets where short_name = 'tmp'),
    '112',
    'common'
) 
 on conflict do nothing;
