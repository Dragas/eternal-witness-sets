    insert into mtgcard(name) values ('Swamp') on conflict do nothing;
    insert into mtgcard(name) values ('Armored Pegasus') on conflict do nothing;
    insert into mtgcard(name) values ('Canyon Wildcat') on conflict do nothing;
    insert into mtgcard(name) values ('Scabland') on conflict do nothing;
    insert into mtgcard(name) values ('Winged Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Rootwalla') on conflict do nothing;
    insert into mtgcard(name) values ('Circle of Protection: Blue') on conflict do nothing;
    insert into mtgcard(name) values ('Disturbed Burial') on conflict do nothing;
    insert into mtgcard(name) values ('Echo Chamber') on conflict do nothing;
    insert into mtgcard(name) values ('Blood Frenzy') on conflict do nothing;
    insert into mtgcard(name) values ('Souldrinker') on conflict do nothing;
    insert into mtgcard(name) values ('Tradewind Rider') on conflict do nothing;
    insert into mtgcard(name) values ('Dauthi Ghoul') on conflict do nothing;
    insert into mtgcard(name) values ('Overrun') on conflict do nothing;
    insert into mtgcard(name) values ('Eladamri, Lord of Leaves') on conflict do nothing;
    insert into mtgcard(name) values ('Rolling Thunder') on conflict do nothing;
    insert into mtgcard(name) values ('Nurturing Licid') on conflict do nothing;
    insert into mtgcard(name) values ('Jinxed Idol') on conflict do nothing;
    insert into mtgcard(name) values ('Verdigris') on conflict do nothing;
    insert into mtgcard(name) values ('Chaotic Goo') on conflict do nothing;
    insert into mtgcard(name) values ('Screeching Harpy') on conflict do nothing;
    insert into mtgcard(name) values ('Giant Crab') on conflict do nothing;
    insert into mtgcard(name) values ('Lightning Elemental') on conflict do nothing;
    insert into mtgcard(name) values ('Thalakos Seer') on conflict do nothing;
    insert into mtgcard(name) values ('Vec Townships') on conflict do nothing;
    insert into mtgcard(name) values ('Tranquility') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Bombardment') on conflict do nothing;
    insert into mtgcard(name) values ('Segmented Wurm') on conflict do nothing;
    insert into mtgcard(name) values ('Barbed Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Soltari Guerrillas') on conflict do nothing;
    insert into mtgcard(name) values ('Gallantry') on conflict do nothing;
    insert into mtgcard(name) values ('Sudden Impact') on conflict do nothing;
    insert into mtgcard(name) values ('Insight') on conflict do nothing;
    insert into mtgcard(name) values ('Dismiss') on conflict do nothing;
    insert into mtgcard(name) values ('Worthy Cause') on conflict do nothing;
    insert into mtgcard(name) values ('Natural Spring') on conflict do nothing;
    insert into mtgcard(name) values ('Reality Anchor') on conflict do nothing;
    insert into mtgcard(name) values ('Salt Flats') on conflict do nothing;
    insert into mtgcard(name) values ('Island') on conflict do nothing;
    insert into mtgcard(name) values ('Respite') on conflict do nothing;
    insert into mtgcard(name) values ('Reap') on conflict do nothing;
    insert into mtgcard(name) values ('Canyon Drake') on conflict do nothing;
    insert into mtgcard(name) values ('Trained Armodon') on conflict do nothing;
    insert into mtgcard(name) values ('Kezzerdrix') on conflict do nothing;
    insert into mtgcard(name) values ('Mountain') on conflict do nothing;
    insert into mtgcard(name) values ('Pit Imp') on conflict do nothing;
    insert into mtgcard(name) values ('Ruby Medallion') on conflict do nothing;
    insert into mtgcard(name) values ('Apes of Rath') on conflict do nothing;
    insert into mtgcard(name) values ('Aluren') on conflict do nothing;
    insert into mtgcard(name) values ('Skyshroud Elf') on conflict do nothing;
    insert into mtgcard(name) values ('Bellowing Fiend') on conflict do nothing;
    insert into mtgcard(name) values ('Heart Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Leeching Licid') on conflict do nothing;
    insert into mtgcard(name) values ('Avenging Angel') on conflict do nothing;
    insert into mtgcard(name) values ('Elven Warhounds') on conflict do nothing;
    insert into mtgcard(name) values ('Verdant Force') on conflict do nothing;
    insert into mtgcard(name) values ('Mnemonic Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Squee''s Toy') on conflict do nothing;
    insert into mtgcard(name) values ('Safeguard') on conflict do nothing;
    insert into mtgcard(name) values ('Orim''s Prayer') on conflict do nothing;
    insert into mtgcard(name) values ('Coercion') on conflict do nothing;
    insert into mtgcard(name) values ('Wall of Diffusion') on conflict do nothing;
    insert into mtgcard(name) values ('Boil') on conflict do nothing;
    insert into mtgcard(name) values ('Phyrexian Grimoire') on conflict do nothing;
    insert into mtgcard(name) values ('Metallic Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Phyrexian Hulk') on conflict do nothing;
    insert into mtgcard(name) values ('Ancient Tomb') on conflict do nothing;
    insert into mtgcard(name) values ('Cursed Scroll') on conflict do nothing;
    insert into mtgcard(name) values ('Forest') on conflict do nothing;
    insert into mtgcard(name) values ('Soltari Trooper') on conflict do nothing;
    insert into mtgcard(name) values ('Scalding Tongs') on conflict do nothing;
    insert into mtgcard(name) values ('Interdict') on conflict do nothing;
    insert into mtgcard(name) values ('Time Warp') on conflict do nothing;
    insert into mtgcard(name) values ('Spontaneous Combustion') on conflict do nothing;
    insert into mtgcard(name) values ('Dracoplasm') on conflict do nothing;
    insert into mtgcard(name) values ('Static Orb') on conflict do nothing;
    insert into mtgcard(name) values ('Soltari Priest') on conflict do nothing;
    insert into mtgcard(name) values ('Ertai''s Meddling') on conflict do nothing;
    insert into mtgcard(name) values ('Tahngarth''s Rage') on conflict do nothing;
    insert into mtgcard(name) values ('Shadowstorm') on conflict do nothing;
    insert into mtgcard(name) values ('Jackal Pup') on conflict do nothing;
    insert into mtgcard(name) values ('Trumpeting Armodon') on conflict do nothing;
    insert into mtgcard(name) values ('Frog Tongue') on conflict do nothing;
    insert into mtgcard(name) values ('Spinal Graft') on conflict do nothing;
    insert into mtgcard(name) values ('Cloudchaser Eagle') on conflict do nothing;
    insert into mtgcard(name) values ('Fevered Convulsions') on conflict do nothing;
    insert into mtgcard(name) values ('Sapphire Medallion') on conflict do nothing;
    insert into mtgcard(name) values ('Plains') on conflict do nothing;
    insert into mtgcard(name) values ('Harrow') on conflict do nothing;
    insert into mtgcard(name) values ('Manakin') on conflict do nothing;
    insert into mtgcard(name) values ('Repentance') on conflict do nothing;
    insert into mtgcard(name) values ('Rampant Growth') on conflict do nothing;
    insert into mtgcard(name) values ('Ghost Town') on conflict do nothing;
    insert into mtgcard(name) values ('Coiled Tinviper') on conflict do nothing;
    insert into mtgcard(name) values ('Flailing Drake') on conflict do nothing;
    insert into mtgcard(name) values ('Pearl Medallion') on conflict do nothing;
    insert into mtgcard(name) values ('Angelic Protector') on conflict do nothing;
    insert into mtgcard(name) values ('Flowstone Giant') on conflict do nothing;
    insert into mtgcard(name) values ('Mogg Hollows') on conflict do nothing;
    insert into mtgcard(name) values ('Magmasaur') on conflict do nothing;
    insert into mtgcard(name) values ('Circle of Protection: Shadow') on conflict do nothing;
    insert into mtgcard(name) values ('Ranger en-Vec') on conflict do nothing;
    insert into mtgcard(name) values ('Wood Sage') on conflict do nothing;
    insert into mtgcard(name) values ('Mogg Squad') on conflict do nothing;
    insert into mtgcard(name) values ('Heartwood Giant') on conflict do nothing;
    insert into mtgcard(name) values ('Rootwater Depths') on conflict do nothing;
    insert into mtgcard(name) values ('Darkling Stalker') on conflict do nothing;
    insert into mtgcard(name) values ('Altar of Dementia') on conflict do nothing;
    insert into mtgcard(name) values ('Mounted Archers') on conflict do nothing;
    insert into mtgcard(name) values ('Gerrard''s Battle Cry') on conflict do nothing;
    insert into mtgcard(name) values ('Soltari Monk') on conflict do nothing;
    insert into mtgcard(name) values ('Deadshot') on conflict do nothing;
    insert into mtgcard(name) values ('Torture Chamber') on conflict do nothing;
    insert into mtgcard(name) values ('Mongrel Pack') on conflict do nothing;
    insert into mtgcard(name) values ('Shadow Rift') on conflict do nothing;
    insert into mtgcard(name) values ('Chill') on conflict do nothing;
    insert into mtgcard(name) values ('Blood Pet') on conflict do nothing;
    insert into mtgcard(name) values ('Spike Drone') on conflict do nothing;
    insert into mtgcard(name) values ('Evincar''s Justice') on conflict do nothing;
    insert into mtgcard(name) values ('Mogg Cannon') on conflict do nothing;
    insert into mtgcard(name) values ('Scroll Rack') on conflict do nothing;
    insert into mtgcard(name) values ('Shatter') on conflict do nothing;
    insert into mtgcard(name) values ('Spell Blast') on conflict do nothing;
    insert into mtgcard(name) values ('Muscle Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Furnace of Rath') on conflict do nothing;
    insert into mtgcard(name) values ('Pallimud') on conflict do nothing;
    insert into mtgcard(name) values ('Lightning Blast') on conflict do nothing;
    insert into mtgcard(name) values ('Bottle Gnomes') on conflict do nothing;
    insert into mtgcard(name) values ('Pacifism') on conflict do nothing;
    insert into mtgcard(name) values ('Legacy''s Allure') on conflict do nothing;
    insert into mtgcard(name) values ('Charging Rhino') on conflict do nothing;
    insert into mtgcard(name) values ('Dauthi Slayer') on conflict do nothing;
    insert into mtgcard(name) values ('Legerdemain') on conflict do nothing;
    insert into mtgcard(name) values ('Emerald Medallion') on conflict do nothing;
    insert into mtgcard(name) values ('Servant of Volrath') on conflict do nothing;
    insert into mtgcard(name) values ('Time Ebb') on conflict do nothing;
    insert into mtgcard(name) values ('Serene Offering') on conflict do nothing;
    insert into mtgcard(name) values ('Living Death') on conflict do nothing;
    insert into mtgcard(name) values ('Lotus Petal') on conflict do nothing;
    insert into mtgcard(name) values ('Soltari Crusader') on conflict do nothing;
    insert into mtgcard(name) values ('Excavator') on conflict do nothing;
    insert into mtgcard(name) values ('Circle of Protection: White') on conflict do nothing;
    insert into mtgcard(name) values ('Dauthi Mindripper') on conflict do nothing;
    insert into mtgcard(name) values ('Orim, Samite Healer') on conflict do nothing;
    insert into mtgcard(name) values ('Light of Day') on conflict do nothing;
    insert into mtgcard(name) values ('Thumbscrews') on conflict do nothing;
    insert into mtgcard(name) values ('Tooth and Claw') on conflict do nothing;
    insert into mtgcard(name) values ('Enfeeblement') on conflict do nothing;
    insert into mtgcard(name) values ('Death Pits of Rath') on conflict do nothing;
    insert into mtgcard(name) values ('Steal Enchantment') on conflict do nothing;
    insert into mtgcard(name) values ('Mogg Conscripts') on conflict do nothing;
    insert into mtgcard(name) values ('Lowland Giant') on conflict do nothing;
    insert into mtgcard(name) values ('Thalakos Sentry') on conflict do nothing;
    insert into mtgcard(name) values ('Mirri''s Guile') on conflict do nothing;
    insert into mtgcard(name) values ('Whispers of the Muse') on conflict do nothing;
    insert into mtgcard(name) values ('Precognition') on conflict do nothing;
    insert into mtgcard(name) values ('Corpse Dance') on conflict do nothing;
    insert into mtgcard(name) values ('Telethopter') on conflict do nothing;
    insert into mtgcard(name) values ('Imps'' Taunt') on conflict do nothing;
    insert into mtgcard(name) values ('Helm of Possession') on conflict do nothing;
    insert into mtgcard(name) values ('Winter''s Grasp') on conflict do nothing;
    insert into mtgcard(name) values ('Reflecting Pool') on conflict do nothing;
    insert into mtgcard(name) values ('No Quarter') on conflict do nothing;
    insert into mtgcard(name) values ('Rats of Rath') on conflict do nothing;
    insert into mtgcard(name) values ('Rathi Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Essence Bottle') on conflict do nothing;
    insert into mtgcard(name) values ('Dauthi Embrace') on conflict do nothing;
    insert into mtgcard(name) values ('Hanna''s Custody') on conflict do nothing;
    insert into mtgcard(name) values ('Caldera Lake') on conflict do nothing;
    insert into mtgcard(name) values ('Sacred Guide') on conflict do nothing;
    insert into mtgcard(name) values ('Flowstone Sculpture') on conflict do nothing;
    insert into mtgcard(name) values ('Apocalypse') on conflict do nothing;
    insert into mtgcard(name) values ('Stinging Licid') on conflict do nothing;
    insert into mtgcard(name) values ('Dregs of Sorrow') on conflict do nothing;
    insert into mtgcard(name) values ('Advance Scout') on conflict do nothing;
    insert into mtgcard(name) values ('Fool''s Tome') on conflict do nothing;
    insert into mtgcard(name) values ('Unstable Shapeshifter') on conflict do nothing;
    insert into mtgcard(name) values ('Mana Severance') on conflict do nothing;
    insert into mtgcard(name) values ('Auratog') on conflict do nothing;
    insert into mtgcard(name) values ('Gravedigger') on conflict do nothing;
    insert into mtgcard(name) values ('Dream Cache') on conflict do nothing;
    insert into mtgcard(name) values ('Carrionette') on conflict do nothing;
    insert into mtgcard(name) values ('Circle of Protection: Green') on conflict do nothing;
    insert into mtgcard(name) values ('Staunch Defenders') on conflict do nothing;
    insert into mtgcard(name) values ('Disenchant') on conflict do nothing;
    insert into mtgcard(name) values ('Spirit Mirror') on conflict do nothing;
    insert into mtgcard(name) values ('Knight of Dusk') on conflict do nothing;
    insert into mtgcard(name) values ('Watchdog') on conflict do nothing;
    insert into mtgcard(name) values ('Sea Monster') on conflict do nothing;
    insert into mtgcard(name) values ('Fylamarid') on conflict do nothing;
    insert into mtgcard(name) values ('Shocker') on conflict do nothing;
    insert into mtgcard(name) values ('Marsh Lurker') on conflict do nothing;
    insert into mtgcard(name) values ('Reckless Spite') on conflict do nothing;
    insert into mtgcard(name) values ('Energizer') on conflict do nothing;
    insert into mtgcard(name) values ('Wind Drake') on conflict do nothing;
    insert into mtgcard(name) values ('Dark Banishing') on conflict do nothing;
    insert into mtgcard(name) values ('Skyshroud Troll') on conflict do nothing;
    insert into mtgcard(name) values ('Puppet Strings') on conflict do nothing;
    insert into mtgcard(name) values ('Kindle') on conflict do nothing;
    insert into mtgcard(name) values ('Bayou Dragonfly') on conflict do nothing;
    insert into mtgcard(name) values ('Abandon Hope') on conflict do nothing;
    insert into mtgcard(name) values ('Mogg Raider') on conflict do nothing;
    insert into mtgcard(name) values ('Field of Souls') on conflict do nothing;
    insert into mtgcard(name) values ('Bounty Hunter') on conflict do nothing;
    insert into mtgcard(name) values ('Storm Front') on conflict do nothing;
    insert into mtgcard(name) values ('Root Maze') on conflict do nothing;
    insert into mtgcard(name) values ('Wasteland') on conflict do nothing;
    insert into mtgcard(name) values ('Rootbreaker Wurm') on conflict do nothing;
    insert into mtgcard(name) values ('Selenia, Dark Angel') on conflict do nothing;
    insert into mtgcard(name) values ('Marble Titan') on conflict do nothing;
    insert into mtgcard(name) values ('Earthcraft') on conflict do nothing;
    insert into mtgcard(name) values ('Thalakos Mistfolk') on conflict do nothing;
    insert into mtgcard(name) values ('Magnetic Web') on conflict do nothing;
    insert into mtgcard(name) values ('Mawcor') on conflict do nothing;
    insert into mtgcard(name) values ('Cinder Marsh') on conflict do nothing;
    insert into mtgcard(name) values ('Soltari Emissary') on conflict do nothing;
    insert into mtgcard(name) values ('Extinction') on conflict do nothing;
    insert into mtgcard(name) values ('Pegasus Refuge') on conflict do nothing;
    insert into mtgcard(name) values ('Diabolic Edict') on conflict do nothing;
    insert into mtgcard(name) values ('Humility') on conflict do nothing;
    insert into mtgcard(name) values ('Firefly') on conflict do nothing;
    insert into mtgcard(name) values ('Rootwater Diver') on conflict do nothing;
    insert into mtgcard(name) values ('Renegade Warlord') on conflict do nothing;
    insert into mtgcard(name) values ('Shimmering Wings') on conflict do nothing;
    insert into mtgcard(name) values ('Winds of Rath') on conflict do nothing;
    insert into mtgcard(name) values ('Nature''s Revolt') on conflict do nothing;
    insert into mtgcard(name) values ('Skyshroud Condor') on conflict do nothing;
    insert into mtgcard(name) values ('Emmessi Tome') on conflict do nothing;
    insert into mtgcard(name) values ('Krakilin') on conflict do nothing;
    insert into mtgcard(name) values ('Dirtcowl Wurm') on conflict do nothing;
    insert into mtgcard(name) values ('Skyshroud Forest') on conflict do nothing;
    insert into mtgcard(name) values ('Thalakos Lowlands') on conflict do nothing;
    insert into mtgcard(name) values ('Commander Greven il-Vec') on conflict do nothing;
    insert into mtgcard(name) values ('Soltari Lancer') on conflict do nothing;
    insert into mtgcard(name) values ('Power Sink') on conflict do nothing;
    insert into mtgcard(name) values ('Opportunist') on conflict do nothing;
    insert into mtgcard(name) values ('Reanimate') on conflict do nothing;
    insert into mtgcard(name) values ('Recycle') on conflict do nothing;
    insert into mtgcard(name) values ('Hand to Hand') on conflict do nothing;
    insert into mtgcard(name) values ('Canopy Spider') on conflict do nothing;
    insert into mtgcard(name) values ('Fugitive Druid') on conflict do nothing;
    insert into mtgcard(name) values ('Phyrexian Splicer') on conflict do nothing;
    insert into mtgcard(name) values ('Horned Turtle') on conflict do nothing;
    insert into mtgcard(name) values ('Cold Storage') on conflict do nothing;
    insert into mtgcard(name) values ('Seeker of Skybreak') on conflict do nothing;
    insert into mtgcard(name) values ('Hero''s Resolve') on conflict do nothing;
    insert into mtgcard(name) values ('Stalking Stones') on conflict do nothing;
    insert into mtgcard(name) values ('Crazed Armodon') on conflict do nothing;
    insert into mtgcard(name) values ('Maze of Shadows') on conflict do nothing;
    insert into mtgcard(name) values ('Volrath''s Curse') on conflict do nothing;
    insert into mtgcard(name) values ('Patchwork Gnomes') on conflict do nothing;
    insert into mtgcard(name) values ('Soltari Foot Soldier') on conflict do nothing;
    insert into mtgcard(name) values ('Fighting Drake') on conflict do nothing;
    insert into mtgcard(name) values ('Vhati il-Dal') on conflict do nothing;
    insert into mtgcard(name) values ('Needle Storm') on conflict do nothing;
    insert into mtgcard(name) values ('Knight of Dawn') on conflict do nothing;
    insert into mtgcard(name) values ('Dark Ritual') on conflict do nothing;
    insert into mtgcard(name) values ('Flowstone Salamander') on conflict do nothing;
    insert into mtgcard(name) values ('Giant Strength') on conflict do nothing;
    insert into mtgcard(name) values ('Booby Trap') on conflict do nothing;
    insert into mtgcard(name) values ('Stun') on conflict do nothing;
    insert into mtgcard(name) values ('Jet Medallion') on conflict do nothing;
    insert into mtgcard(name) values ('Armor Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Dauthi Mercenary') on conflict do nothing;
    insert into mtgcard(name) values ('Dauthi Horror') on conflict do nothing;
    insert into mtgcard(name) values ('Aftershock') on conflict do nothing;
    insert into mtgcard(name) values ('Intuition') on conflict do nothing;
    insert into mtgcard(name) values ('Mogg Fanatic') on conflict do nothing;
    insert into mtgcard(name) values ('Anoint') on conflict do nothing;
    insert into mtgcard(name) values ('Rootwater Shaman') on conflict do nothing;
    insert into mtgcard(name) values ('Rain of Tears') on conflict do nothing;
    insert into mtgcard(name) values ('Twitch') on conflict do nothing;
    insert into mtgcard(name) values ('Circle of Protection: Red') on conflict do nothing;
    insert into mtgcard(name) values ('Oracle en-Vec') on conflict do nothing;
    insert into mtgcard(name) values ('Benthic Behemoth') on conflict do nothing;
    insert into mtgcard(name) values ('Rootwater Hunter') on conflict do nothing;
    insert into mtgcard(name) values ('Manta Riders') on conflict do nothing;
    insert into mtgcard(name) values ('Gaseous Form') on conflict do nothing;
    insert into mtgcard(name) values ('Horned Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Dread of Night') on conflict do nothing;
    insert into mtgcard(name) values ('Broken Fall') on conflict do nothing;
    insert into mtgcard(name) values ('Flickering Ward') on conflict do nothing;
    insert into mtgcard(name) values ('Duplicity') on conflict do nothing;
    insert into mtgcard(name) values ('Pine Barrens') on conflict do nothing;
    insert into mtgcard(name) values ('Invulnerability') on conflict do nothing;
    insert into mtgcard(name) values ('Thalakos Dreamsower') on conflict do nothing;
    insert into mtgcard(name) values ('Havoc') on conflict do nothing;
    insert into mtgcard(name) values ('Warmth') on conflict do nothing;
    insert into mtgcard(name) values ('Scragnoth') on conflict do nothing;
    insert into mtgcard(name) values ('Eladamri''s Vineyard') on conflict do nothing;
    insert into mtgcard(name) values ('Sadistic Glee') on conflict do nothing;
    insert into mtgcard(name) values ('Minion of the Wastes') on conflict do nothing;
    insert into mtgcard(name) values ('Circle of Protection: Black') on conflict do nothing;
    insert into mtgcard(name) values ('Counterspell') on conflict do nothing;
    insert into mtgcard(name) values ('Pincher Beetles') on conflict do nothing;
    insert into mtgcard(name) values ('Maddening Imp') on conflict do nothing;
    insert into mtgcard(name) values ('Fireslinger') on conflict do nothing;
    insert into mtgcard(name) values ('Heartwood Treefolk') on conflict do nothing;
    insert into mtgcard(name) values ('Starke of Rath') on conflict do nothing;
    insert into mtgcard(name) values ('Escaped Shapeshifter') on conflict do nothing;
    insert into mtgcard(name) values ('Whim of Volrath') on conflict do nothing;
    insert into mtgcard(name) values ('Heartwood Dryad') on conflict do nothing;
    insert into mtgcard(name) values ('Choke') on conflict do nothing;
    insert into mtgcard(name) values ('Ancient Runes') on conflict do nothing;
    insert into mtgcard(name) values ('Perish') on conflict do nothing;
    insert into mtgcard(name) values ('Capsize') on conflict do nothing;
    insert into mtgcard(name) values ('Wild Wurm') on conflict do nothing;
    insert into mtgcard(name) values ('Scorched Earth') on conflict do nothing;
    insert into mtgcard(name) values ('Quickening Licid') on conflict do nothing;
    insert into mtgcard(name) values ('Searing Touch') on conflict do nothing;
    insert into mtgcard(name) values ('Endless Scream') on conflict do nothing;
    insert into mtgcard(name) values ('Elite Javelineer') on conflict do nothing;
    insert into mtgcard(name) values ('Wind Dancer') on conflict do nothing;
    insert into mtgcard(name) values ('Sandstone Warrior') on conflict do nothing;
    insert into mtgcard(name) values ('Sarcomancy') on conflict do nothing;
    insert into mtgcard(name) values ('Sky Spirit') on conflict do nothing;
    insert into mtgcard(name) values ('Rootwater Matriarch') on conflict do nothing;
    insert into mtgcard(name) values ('Meditate') on conflict do nothing;
    insert into mtgcard(name) values ('Coffin Queen') on conflict do nothing;
    insert into mtgcard(name) values ('Flowstone Wyvern') on conflict do nothing;
    insert into mtgcard(name) values ('Lobotomy') on conflict do nothing;
    insert into mtgcard(name) values ('Dauthi Marauder') on conflict do nothing;
    insert into mtgcard(name) values ('Skyshroud Vampire') on conflict do nothing;
    insert into mtgcard(name) values ('Skyshroud Ranger') on conflict do nothing;
    insert into mtgcard(name) values ('Talon Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Crown of Flames') on conflict do nothing;
    insert into mtgcard(name) values ('Master Decoy') on conflict do nothing;
    insert into mtgcard(name) values ('Grindstone') on conflict do nothing;
    insert into mtgcard(name) values ('Propaganda') on conflict do nothing;
    insert into mtgcard(name) values ('Stone Rain') on conflict do nothing;
    insert into mtgcard(name) values ('Elvish Fury') on conflict do nothing;
    insert into mtgcard(name) values ('Mindwhip Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Enraging Licid') on conflict do nothing;
    insert into mtgcard(name) values ('Clergy en-Vec') on conflict do nothing;
    insert into mtgcard(name) values ('Clot Sliver') on conflict do nothing;
