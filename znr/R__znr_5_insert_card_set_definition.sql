insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Deadly Alliance'),
    (select id from sets where short_name = 'znr'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyclave Cleric // Skyclave Basilica'),
    (select id from sets where short_name = 'znr'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fearless Fledgling'),
    (select id from sets where short_name = 'znr'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tuktuk Rubblefort'),
    (select id from sets where short_name = 'znr'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyclave Pick-Axe'),
    (select id from sets where short_name = 'znr'),
    '309',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scion of the Swarm'),
    (select id from sets where short_name = 'znr'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyclave Squid'),
    (select id from sets where short_name = 'znr'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sure-Footed Infiltrator'),
    (select id from sets where short_name = 'znr'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cragcrown Pathway // Timbercrown Pathway'),
    (select id from sets where short_name = 'znr'),
    '287',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Branchloft Pathway // Boulderloft Pathway'),
    (select id from sets where short_name = 'znr'),
    '258',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyclave Shade'),
    (select id from sets where short_name = 'znr'),
    '298',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grotag Night-Runner'),
    (select id from sets where short_name = 'znr'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Verazol, the Split Current'),
    (select id from sets where short_name = 'znr'),
    '370',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodchief''s Thirst'),
    (select id from sets where short_name = 'znr'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Drain'),
    (select id from sets where short_name = 'znr'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nahiri''s Lithoforming'),
    (select id from sets where short_name = 'znr'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief Ooze'),
    (select id from sets where short_name = 'znr'),
    '361',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nullpriest of Oblivion'),
    (select id from sets where short_name = 'znr'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kargan Intimidator'),
    (select id from sets where short_name = 'znr'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strength of Solidarity'),
    (select id from sets where short_name = 'znr'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Expedition Diviner'),
    (select id from sets where short_name = 'znr'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shadow Stinger'),
    (select id from sets where short_name = 'znr'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kazandu Mammoth // Kazandu Valley'),
    (select id from sets where short_name = 'znr'),
    '305',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relic Amulet'),
    (select id from sets where short_name = 'znr'),
    '247',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Roiling Vortex'),
    (select id from sets where short_name = 'znr'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zulaport Duelist'),
    (select id from sets where short_name = 'znr'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relic Golem'),
    (select id from sets where short_name = 'znr'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yasharn, Implacable Earth'),
    (select id from sets where short_name = 'znr'),
    '371',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forsaken Monument'),
    (select id from sets where short_name = 'znr'),
    '374',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Master of Winds'),
    (select id from sets where short_name = 'znr'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Beckoning'),
    (select id from sets where short_name = 'znr'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brushfire Elemental'),
    (select id from sets where short_name = 'znr'),
    '311',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tajuru Paragon'),
    (select id from sets where short_name = 'znr'),
    '363',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Subtle Strike'),
    (select id from sets where short_name = 'znr'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soaring Thought-Thief'),
    (select id from sets where short_name = 'znr'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orah, Skyclave Hierophant'),
    (select id from sets where short_name = 'znr'),
    '385',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Carver'),
    (select id from sets where short_name = 'znr'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maddening Cacophony'),
    (select id from sets where short_name = 'znr'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merfolk Windrobber'),
    (select id from sets where short_name = 'znr'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silundi Vision // Silundi Isle'),
    (select id from sets where short_name = 'znr'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Expedition Healer'),
    (select id from sets where short_name = 'znr'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yasharn, Implacable Earth'),
    (select id from sets where short_name = 'znr'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cascade Seer'),
    (select id from sets where short_name = 'znr'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lotus Cobra'),
    (select id from sets where short_name = 'znr'),
    '307',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Linvala, Shield of Sea Gate'),
    (select id from sets where short_name = 'znr'),
    '368',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kitesail Cleric'),
    (select id from sets where short_name = 'znr'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Stormcaller'),
    (select id from sets where short_name = 'znr'),
    '334',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kargan Intimidator'),
    (select id from sets where short_name = 'znr'),
    '347',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'znr'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Valakut Exploration'),
    (select id from sets where short_name = 'znr'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Restoration // Sea Gate, Reborn'),
    (select id from sets where short_name = 'znr'),
    '76',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'znr'),
    '382',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orah, Skyclave Hierophant'),
    (select id from sets where short_name = 'znr'),
    '369',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'znr'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sizzling Barrage'),
    (select id from sets where short_name = 'znr'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bubble Snare'),
    (select id from sets where short_name = 'znr'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Stormcaller'),
    (select id from sets where short_name = 'znr'),
    '77',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Soul Shatter'),
    (select id from sets where short_name = 'znr'),
    '345',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Utility Knife'),
    (select id from sets where short_name = 'znr'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nimble Trapfinder'),
    (select id from sets where short_name = 'znr'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shepherd of Heroes'),
    (select id from sets where short_name = 'znr'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyclave Relic'),
    (select id from sets where short_name = 'znr'),
    '252',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Iridescent Hornbeetle'),
    (select id from sets where short_name = 'znr'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cunning Geysermage'),
    (select id from sets where short_name = 'znr'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cragplate Baloth'),
    (select id from sets where short_name = 'znr'),
    '359',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archon of Emeria'),
    (select id from sets where short_name = 'znr'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Valakut Awakening // Valakut Stoneforge'),
    (select id from sets where short_name = 'znr'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orah, Skyclave Hierophant'),
    (select id from sets where short_name = 'znr'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Concerted Defense'),
    (select id from sets where short_name = 'znr'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tangled Florahedron // Tangled Vale'),
    (select id from sets where short_name = 'znr'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shatterskull Smashing // Shatterskull, the Hammer Pass'),
    (select id from sets where short_name = 'znr'),
    '354',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Highborn Vampire'),
    (select id from sets where short_name = 'znr'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grakmaw, Skyclave Ravager'),
    (select id from sets where short_name = 'znr'),
    '366',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'znr'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riverglide Pathway // Lavaglide Pathway'),
    (select id from sets where short_name = 'znr'),
    '264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Canyon Jerboa'),
    (select id from sets where short_name = 'znr'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nullpriest of Oblivion'),
    (select id from sets where short_name = 'znr'),
    '342',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Acquisitions Expert'),
    (select id from sets where short_name = 'znr'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inscription of Insight'),
    (select id from sets where short_name = 'znr'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Expedition Skulker'),
    (select id from sets where short_name = 'znr'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tazeem Raptor'),
    (select id from sets where short_name = 'znr'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'znr'),
    '384',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Shatter'),
    (select id from sets where short_name = 'znr'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kazandu Mammoth // Kazandu Valley'),
    (select id from sets where short_name = 'znr'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pressure Point'),
    (select id from sets where short_name = 'znr'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Expedition Champion'),
    (select id from sets where short_name = 'znr'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jwari Disruption // Jwari Ruins'),
    (select id from sets where short_name = 'znr'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scute Swarm'),
    (select id from sets where short_name = 'znr'),
    '308',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shadows'' Verdict'),
    (select id from sets where short_name = 'znr'),
    '344',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blackbloom Rogue // Blackbloom Bog'),
    (select id from sets where short_name = 'znr'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scavenged Blade'),
    (select id from sets where short_name = 'znr'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Taborax, Hope''s Demise'),
    (select id from sets where short_name = 'znr'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forsaken Monument'),
    (select id from sets where short_name = 'znr'),
    '244',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dreadwurm'),
    (select id from sets where short_name = 'znr'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Allied Assault'),
    (select id from sets where short_name = 'znr'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kabira Takedown // Kabira Plateau'),
    (select id from sets where short_name = 'znr'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glacial Grasp'),
    (select id from sets where short_name = 'znr'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akoum Hellhound'),
    (select id from sets where short_name = 'znr'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vastwood Fortification // Vastwood Thicket'),
    (select id from sets where short_name = 'znr'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'znr'),
    '383',
    'common'
) ,
(
    (select id from mtgcard where name = 'Malakir Blood-Priest'),
    (select id from sets where short_name = 'znr'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shell Shield'),
    (select id from sets where short_name = 'znr'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyclave Plunder'),
    (select id from sets where short_name = 'znr'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nahiri, Heir of the Ancients'),
    (select id from sets where short_name = 'znr'),
    '230',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'znr'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nahiri''s Binding'),
    (select id from sets where short_name = 'znr'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Might of Murasa'),
    (select id from sets where short_name = 'znr'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scourge of the Skyclaves'),
    (select id from sets where short_name = 'znr'),
    '343',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Felidar Retreat'),
    (select id from sets where short_name = 'znr'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relic Robber'),
    (select id from sets where short_name = 'znr'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Journey to Oblivion'),
    (select id from sets where short_name = 'znr'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spitfire Lagac'),
    (select id from sets where short_name = 'znr'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fearless Fledgling'),
    (select id from sets where short_name = 'znr'),
    '291',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyclave Geopede'),
    (select id from sets where short_name = 'znr'),
    '301',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cleansing Wildfire'),
    (select id from sets where short_name = 'znr'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Agadeem''s Awakening // Agadeem, the Undercrypt'),
    (select id from sets where short_name = 'znr'),
    '336',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Linvala, Shield of Sea Gate'),
    (select id from sets where short_name = 'znr'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relic Robber'),
    (select id from sets where short_name = 'znr'),
    '351',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relic Vial'),
    (select id from sets where short_name = 'znr'),
    '250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scourge of the Skyclaves'),
    (select id from sets where short_name = 'znr'),
    '122',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Omnath, Locus of Creation'),
    (select id from sets where short_name = 'znr'),
    '232',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Coralhelm Chronicler'),
    (select id from sets where short_name = 'znr'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyclave Shade'),
    (select id from sets where short_name = 'znr'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shadows'' Verdict'),
    (select id from sets where short_name = 'znr'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dauntless Survivor'),
    (select id from sets where short_name = 'znr'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archon of Emeria'),
    (select id from sets where short_name = 'znr'),
    '315',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kabira Outrider'),
    (select id from sets where short_name = 'znr'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cinderclasm'),
    (select id from sets where short_name = 'znr'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Roil Eruption'),
    (select id from sets where short_name = 'znr'),
    '389',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'znr'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sneaking Guide'),
    (select id from sets where short_name = 'znr'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyclave Squid'),
    (select id from sets where short_name = 'znr'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'znr'),
    '381',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drana''s Silencer'),
    (select id from sets where short_name = 'znr'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyroclastic Hellion'),
    (select id from sets where short_name = 'znr'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Farsight Adept'),
    (select id from sets where short_name = 'znr'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glasspool Mimic // Glasspool Shore'),
    (select id from sets where short_name = 'znr'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coveted Prize'),
    (select id from sets where short_name = 'znr'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grotag Bug-Catcher'),
    (select id from sets where short_name = 'znr'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lithoform Blight'),
    (select id from sets where short_name = 'znr'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'znr'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drana, the Last Bloodchief'),
    (select id from sets where short_name = 'znr'),
    '98',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'znr'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Territorial Scythecat'),
    (select id from sets where short_name = 'znr'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Destiny'),
    (select id from sets where short_name = 'znr'),
    '314',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Beyeen Veil // Beyeen Coast'),
    (select id from sets where short_name = 'znr'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Roiling Regrowth'),
    (select id from sets where short_name = 'znr'),
    '390',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Malakir Rebirth // Malakir Mire'),
    (select id from sets where short_name = 'znr'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chilling Trap'),
    (select id from sets where short_name = 'znr'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Turntimber Symbiosis // Turntimber, Serpentine Wood'),
    (select id from sets where short_name = 'znr'),
    '215',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scute Swarm'),
    (select id from sets where short_name = 'znr'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maddening Cacophony'),
    (select id from sets where short_name = 'znr'),
    '330',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leyline Tyrant'),
    (select id from sets where short_name = 'znr'),
    '348',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Master of Winds'),
    (select id from sets where short_name = 'znr'),
    '331',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swarm Shambler'),
    (select id from sets where short_name = 'znr'),
    '362',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emeria Captain'),
    (select id from sets where short_name = 'znr'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Needleverge Pathway // Pillarverge Pathway'),
    (select id from sets where short_name = 'znr'),
    '263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lithoform Engine'),
    (select id from sets where short_name = 'znr'),
    '245',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Paired Tactician'),
    (select id from sets where short_name = 'znr'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Agadeem''s Awakening // Agadeem, the Undercrypt'),
    (select id from sets where short_name = 'znr'),
    '90',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Valakut Exploration'),
    (select id from sets where short_name = 'znr'),
    '303',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vastwood Surge'),
    (select id from sets where short_name = 'znr'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Akiri, Fearless Voyager'),
    (select id from sets where short_name = 'znr'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Banneret'),
    (select id from sets where short_name = 'znr'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Canopy Baloth'),
    (select id from sets where short_name = 'znr'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kaza, Roil Chaser'),
    (select id from sets where short_name = 'znr'),
    '367',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatterskull Charger'),
    (select id from sets where short_name = 'znr'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Attended Healer'),
    (select id from sets where short_name = 'znr'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Taborax, Hope''s Demise'),
    (select id from sets where short_name = 'znr'),
    '346',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa of Shadowed Boughs'),
    (select id from sets where short_name = 'znr'),
    '231',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nahiri, Heir of the Ancients'),
    (select id from sets where short_name = 'znr'),
    '282',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tormenting Voice'),
    (select id from sets where short_name = 'znr'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charix, the Raging Isle'),
    (select id from sets where short_name = 'znr'),
    '386',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tazeem Roilmage'),
    (select id from sets where short_name = 'znr'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kargan Warleader'),
    (select id from sets where short_name = 'znr'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'znr'),
    '276',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magmatic Channeler'),
    (select id from sets where short_name = 'znr'),
    '349',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marauding Blight-Priest'),
    (select id from sets where short_name = 'znr'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gnarlid Colony'),
    (select id from sets where short_name = 'znr'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruin Crab'),
    (select id from sets where short_name = 'znr'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Branchloft Pathway // Boulderloft Pathway'),
    (select id from sets where short_name = 'znr'),
    '284',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashaya, Soul of the Wild'),
    (select id from sets where short_name = 'znr'),
    '179',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kazuul''s Fury // Kazuul''s Cliffs'),
    (select id from sets where short_name = 'znr'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Verazol, the Split Current'),
    (select id from sets where short_name = 'znr'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'znr'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Song-Mad Treachery // Song-Mad Ruins'),
    (select id from sets where short_name = 'znr'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Riverglide Pathway // Lavaglide Pathway'),
    (select id from sets where short_name = 'znr'),
    '289',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Resolute Strike'),
    (select id from sets where short_name = 'znr'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kazandu Nectarpot'),
    (select id from sets where short_name = 'znr'),
    '306',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vine Gecko'),
    (select id from sets where short_name = 'znr'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swarm Shambler'),
    (select id from sets where short_name = 'znr'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nahiri''s Lithoforming'),
    (select id from sets where short_name = 'znr'),
    '350',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ondu Inversion // Ondu Skyruins'),
    (select id from sets where short_name = 'znr'),
    '321',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crawling Barrens'),
    (select id from sets where short_name = 'znr'),
    '262',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hagra Mauling // Hagra Broodpit'),
    (select id from sets where short_name = 'znr'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Squad Commander'),
    (select id from sets where short_name = 'znr'),
    '323',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kazandu Nectarpot'),
    (select id from sets where short_name = 'znr'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cliffhaven Sell-Sword'),
    (select id from sets where short_name = 'znr'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tajuru Blightblade'),
    (select id from sets where short_name = 'znr'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reclaim the Wastes'),
    (select id from sets where short_name = 'znr'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon''s Disciple'),
    (select id from sets where short_name = 'znr'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leyline Tyrant'),
    (select id from sets where short_name = 'znr'),
    '147',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Roil Eruption'),
    (select id from sets where short_name = 'znr'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Celebrant'),
    (select id from sets where short_name = 'znr'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Umara Wizard // Umara Skyfalls'),
    (select id from sets where short_name = 'znr'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Into the Roil'),
    (select id from sets where short_name = 'znr'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashaya, Soul of the Wild'),
    (select id from sets where short_name = 'znr'),
    '358',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Springmantle Cleric'),
    (select id from sets where short_name = 'znr'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tazri, Beacon of Unity'),
    (select id from sets where short_name = 'znr'),
    '324',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Brushfire Elemental'),
    (select id from sets where short_name = 'znr'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Turntimber Symbiosis // Turntimber, Serpentine Wood'),
    (select id from sets where short_name = 'znr'),
    '364',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Practiced Tactics'),
    (select id from sets where short_name = 'znr'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drana, the Last Bloodchief'),
    (select id from sets where short_name = 'znr'),
    '338',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Inscription of Ruin'),
    (select id from sets where short_name = 'znr'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coralhelm Chronicler'),
    (select id from sets where short_name = 'znr'),
    '327',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cleric of Life''s Bond'),
    (select id from sets where short_name = 'znr'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'znr'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moss-Pit Skeleton'),
    (select id from sets where short_name = 'znr'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'znr'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coveted Prize'),
    (select id from sets where short_name = 'znr'),
    '337',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Confounding Conundrum'),
    (select id from sets where short_name = 'znr'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jace, Mirror Mage'),
    (select id from sets where short_name = 'znr'),
    '281',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'znr'),
    '275',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charix, the Raging Isle'),
    (select id from sets where short_name = 'znr'),
    '325',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zof Consumption // Zof Bloodbog'),
    (select id from sets where short_name = 'znr'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seafloor Stalker'),
    (select id from sets where short_name = 'znr'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akiri, Fearless Voyager'),
    (select id from sets where short_name = 'znr'),
    '365',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel of Destiny'),
    (select id from sets where short_name = 'znr'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Khalni Ambush // Khalni Territory'),
    (select id from sets where short_name = 'znr'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyclave Pick-Axe'),
    (select id from sets where short_name = 'znr'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Throne of Makindi'),
    (select id from sets where short_name = 'znr'),
    '379',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spoils of Adventure'),
    (select id from sets where short_name = 'znr'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Omnath, Locus of Creation'),
    (select id from sets where short_name = 'znr'),
    '312',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Smite the Monstrous'),
    (select id from sets where short_name = 'znr'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zareth San, the Trickster'),
    (select id from sets where short_name = 'znr'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatterskull Minotaur'),
    (select id from sets where short_name = 'znr'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scorch Rider'),
    (select id from sets where short_name = 'znr'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fissure Wizard'),
    (select id from sets where short_name = 'znr'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tajuru Paragon'),
    (select id from sets where short_name = 'znr'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Living Tempest'),
    (select id from sets where short_name = 'znr'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Valakut Awakening // Valakut Stoneforge'),
    (select id from sets where short_name = 'znr'),
    '355',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Joraga Visionary'),
    (select id from sets where short_name = 'znr'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archpriest of Iona'),
    (select id from sets where short_name = 'znr'),
    '316',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inscription of Ruin'),
    (select id from sets where short_name = 'znr'),
    '340',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stonework Packbeast'),
    (select id from sets where short_name = 'znr'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Into the Roil'),
    (select id from sets where short_name = 'znr'),
    '387',
    'common'
) ,
(
    (select id from mtgcard where name = 'Field Research'),
    (select id from sets where short_name = 'znr'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lotus Cobra'),
    (select id from sets where short_name = 'znr'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spare Supplies'),
    (select id from sets where short_name = 'znr'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spikefield Hazard // Spikefield Cave'),
    (select id from sets where short_name = 'znr'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Restoration // Sea Gate, Reborn'),
    (select id from sets where short_name = 'znr'),
    '333',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thundering Sparkmage'),
    (select id from sets where short_name = 'znr'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cragplate Baloth'),
    (select id from sets where short_name = 'znr'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zagras, Thief of Heartbeats'),
    (select id from sets where short_name = 'znr'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merfolk Falconer'),
    (select id from sets where short_name = 'znr'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teeterpeak Ambusher'),
    (select id from sets where short_name = 'znr'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Makindi Stampede // Makindi Mesas'),
    (select id from sets where short_name = 'znr'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Needleverge Pathway // Pillarverge Pathway'),
    (select id from sets where short_name = 'znr'),
    '288',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'znr'),
    '380',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moraug, Fury of Akoum'),
    (select id from sets where short_name = 'znr'),
    '300',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ravager''s Mace'),
    (select id from sets where short_name = 'znr'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kaza, Roil Chaser'),
    (select id from sets where short_name = 'znr'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kazandu Stomper'),
    (select id from sets where short_name = 'znr'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Colossus'),
    (select id from sets where short_name = 'znr'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clearwater Pathway // Murkwater Pathway'),
    (select id from sets where short_name = 'znr'),
    '286',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dauntless Unity'),
    (select id from sets where short_name = 'znr'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hagra Mauling // Hagra Broodpit'),
    (select id from sets where short_name = 'znr'),
    '339',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyclave Shadowcat'),
    (select id from sets where short_name = 'znr'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lullmage''s Familiar'),
    (select id from sets where short_name = 'znr'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Turntimber Ascetic'),
    (select id from sets where short_name = 'znr'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clearwater Pathway // Murkwater Pathway'),
    (select id from sets where short_name = 'znr'),
    '260',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cleric of Chill Depths'),
    (select id from sets where short_name = 'znr'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akoum Hellhound'),
    (select id from sets where short_name = 'znr'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spitfire Lagac'),
    (select id from sets where short_name = 'znr'),
    '302',
    'common'
) ,
(
    (select id from mtgcard where name = 'Roiling Vortex'),
    (select id from sets where short_name = 'znr'),
    '352',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ondu Inversion // Ondu Skyruins'),
    (select id from sets where short_name = 'znr'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Confounding Conundrum'),
    (select id from sets where short_name = 'znr'),
    '326',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyclave Apparition'),
    (select id from sets where short_name = 'znr'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zareth San, the Trickster'),
    (select id from sets where short_name = 'znr'),
    '373',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nighthawk Scavenger'),
    (select id from sets where short_name = 'znr'),
    '341',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guul Draz Mucklord'),
    (select id from sets where short_name = 'znr'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prowling Felidar'),
    (select id from sets where short_name = 'znr'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Roost of Drakes'),
    (select id from sets where short_name = 'znr'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Molten Blast'),
    (select id from sets where short_name = 'znr'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'znr'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyclave Apparition'),
    (select id from sets where short_name = 'znr'),
    '322',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatterskull Smashing // Shatterskull, the Hammer Pass'),
    (select id from sets where short_name = 'znr'),
    '161',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Legion Angel'),
    (select id from sets where short_name = 'znr'),
    '318',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thwart the Grave'),
    (select id from sets where short_name = 'znr'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grakmaw, Skyclave Ravager'),
    (select id from sets where short_name = 'znr'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Legion Angel'),
    (select id from sets where short_name = 'znr'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Makindi Ox'),
    (select id from sets where short_name = 'znr'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inscription of Insight'),
    (select id from sets where short_name = 'znr'),
    '329',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Canyon Jerboa'),
    (select id from sets where short_name = 'znr'),
    '290',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelheart Protector'),
    (select id from sets where short_name = 'znr'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Broken Wings'),
    (select id from sets where short_name = 'znr'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vanquish the Weak'),
    (select id from sets where short_name = 'znr'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squad Commander'),
    (select id from sets where short_name = 'znr'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phylath, World Sculptor'),
    (select id from sets where short_name = 'znr'),
    '313',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deliberate'),
    (select id from sets where short_name = 'znr'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emeria''s Call // Emeria, Shattered Skyclave'),
    (select id from sets where short_name = 'znr'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bala Ged Recovery // Bala Ged Sanctuary'),
    (select id from sets where short_name = 'znr'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Greenwarden'),
    (select id from sets where short_name = 'znr'),
    '357',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Windrider Wizard'),
    (select id from sets where short_name = 'znr'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'znr'),
    '277',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lullmage''s Domination'),
    (select id from sets where short_name = 'znr'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Emeria''s Call // Emeria, Shattered Skyclave'),
    (select id from sets where short_name = 'znr'),
    '317',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fireblade Charger'),
    (select id from sets where short_name = 'znr'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Taunting Arbormage'),
    (select id from sets where short_name = 'znr'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Relic Axe'),
    (select id from sets where short_name = 'znr'),
    '248',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kargan Warleader'),
    (select id from sets where short_name = 'znr'),
    '391',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moraug, Fury of Akoum'),
    (select id from sets where short_name = 'znr'),
    '150',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nimble Trapfinder'),
    (select id from sets where short_name = 'znr'),
    '332',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goma Fada Vanguard'),
    (select id from sets where short_name = 'znr'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Territorial Scythecat'),
    (select id from sets where short_name = 'znr'),
    '310',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wayward Guide-Beast'),
    (select id from sets where short_name = 'znr'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inordinate Rage'),
    (select id from sets where short_name = 'znr'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myriad Construct'),
    (select id from sets where short_name = 'znr'),
    '376',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brightclimb Pathway // Grimclimb Pathway'),
    (select id from sets where short_name = 'znr'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Felidar Retreat'),
    (select id from sets where short_name = 'znr'),
    '292',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tazri, Beacon of Unity'),
    (select id from sets where short_name = 'znr'),
    '44',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Canopy Baloth'),
    (select id from sets where short_name = 'znr'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Throne of Makindi'),
    (select id from sets where short_name = 'znr'),
    '265',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreadwurm'),
    (select id from sets where short_name = 'znr'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Synchronized Spellcraft'),
    (select id from sets where short_name = 'znr'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magmatic Channeler'),
    (select id from sets where short_name = 'znr'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akoum Warrior // Akoum Teeth'),
    (select id from sets where short_name = 'znr'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crawling Barrens'),
    (select id from sets where short_name = 'znr'),
    '378',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'znr'),
    '278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cragcrown Pathway // Timbercrown Pathway'),
    (select id from sets where short_name = 'znr'),
    '261',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa of Shadowed Boughs'),
    (select id from sets where short_name = 'znr'),
    '283',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Adventure Awaits'),
    (select id from sets where short_name = 'znr'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyclave Geopede'),
    (select id from sets where short_name = 'znr'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anticognition'),
    (select id from sets where short_name = 'znr'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myriad Construct'),
    (select id from sets where short_name = 'znr'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thundering Rebuke'),
    (select id from sets where short_name = 'znr'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Base Camp'),
    (select id from sets where short_name = 'znr'),
    '257',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blood Price'),
    (select id from sets where short_name = 'znr'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancient Greenwarden'),
    (select id from sets where short_name = 'znr'),
    '178',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rockslide Sorcerer'),
    (select id from sets where short_name = 'znr'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inscription of Abundance'),
    (select id from sets where short_name = 'znr'),
    '360',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tajuru Snarecaster'),
    (select id from sets where short_name = 'znr'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'znr'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lithoform Engine'),
    (select id from sets where short_name = 'znr'),
    '375',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Veteran Adventurer'),
    (select id from sets where short_name = 'znr'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pelakka Predation // Pelakka Caverns'),
    (select id from sets where short_name = 'znr'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Murasa Rootgrazer'),
    (select id from sets where short_name = 'znr'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nimana Skydancer'),
    (select id from sets where short_name = 'znr'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scale the Heights'),
    (select id from sets where short_name = 'znr'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phylath, World Sculptor'),
    (select id from sets where short_name = 'znr'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Charix, the Raging Isle'),
    (select id from sets where short_name = 'znr'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ardent Electromancer'),
    (select id from sets where short_name = 'znr'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prowling Felidar'),
    (select id from sets where short_name = 'znr'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'znr'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nimana Skitter-Sneak'),
    (select id from sets where short_name = 'znr'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nighthawk Scavenger'),
    (select id from sets where short_name = 'znr'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brightclimb Pathway // Grimclimb Pathway'),
    (select id from sets where short_name = 'znr'),
    '285',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Luminarch Aspirant'),
    (select id from sets where short_name = 'znr'),
    '319',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wayward Guide-Beast'),
    (select id from sets where short_name = 'znr'),
    '356',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Needleverge Pathway // Pillarverge Pathway'),
    (select id from sets where short_name = 'znr'),
    '288†',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cliffhaven Kitesail'),
    (select id from sets where short_name = 'znr'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruin Crab'),
    (select id from sets where short_name = 'znr'),
    '295',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Murasa Brute'),
    (select id from sets where short_name = 'znr'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rabid Bite'),
    (select id from sets where short_name = 'znr'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zagras, Thief of Heartbeats'),
    (select id from sets where short_name = 'znr'),
    '372',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maul of the Skyclaves'),
    (select id from sets where short_name = 'znr'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatterskull Charger'),
    (select id from sets where short_name = 'znr'),
    '353',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sejiri Shelter // Sejiri Glacier'),
    (select id from sets where short_name = 'znr'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghastly Gloomhunter'),
    (select id from sets where short_name = 'znr'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murasa Sproutling'),
    (select id from sets where short_name = 'znr'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Risen Riptide'),
    (select id from sets where short_name = 'znr'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mesa Lynx'),
    (select id from sets where short_name = 'znr'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feed the Swarm'),
    (select id from sets where short_name = 'znr'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodchief''s Thirst'),
    (select id from sets where short_name = 'znr'),
    '388',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Archpriest of Iona'),
    (select id from sets where short_name = 'znr'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thieving Skydiver'),
    (select id from sets where short_name = 'znr'),
    '335',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hagra Constrictor'),
    (select id from sets where short_name = 'znr'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Roiling Regrowth'),
    (select id from sets where short_name = 'znr'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyclave Relic'),
    (select id from sets where short_name = 'znr'),
    '377',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyclave Sentinel'),
    (select id from sets where short_name = 'znr'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oblivion''s Hunger'),
    (select id from sets where short_name = 'znr'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Umara Mystic'),
    (select id from sets where short_name = 'znr'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maul of the Skyclaves'),
    (select id from sets where short_name = 'znr'),
    '320',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kor Blademaster'),
    (select id from sets where short_name = 'znr'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inscription of Abundance'),
    (select id from sets where short_name = 'znr'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Makindi Ox'),
    (select id from sets where short_name = 'znr'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief Ooze'),
    (select id from sets where short_name = 'znr'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa''s Zendikon'),
    (select id from sets where short_name = 'znr'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace, Mirror Mage'),
    (select id from sets where short_name = 'znr'),
    '63',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Luminarch Aspirant'),
    (select id from sets where short_name = 'znr'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glasspool Mimic // Glasspool Shore'),
    (select id from sets where short_name = 'znr'),
    '328',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thieving Skydiver'),
    (select id from sets where short_name = 'znr'),
    '85',
    'rare'
) 
 on conflict do nothing;
