insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Lord Windgrace'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lord Windgrace'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lord Windgrace'),
        (select types.id from types where name = 'Windgrace')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Saheeli, the Gifted'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Saheeli, the Gifted'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Saheeli, the Gifted'),
        (select types.id from types where name = 'Saheeli')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aminatou, the Fateshifter'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aminatou, the Fateshifter'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aminatou, the Fateshifter'),
        (select types.id from types where name = 'Aminatou')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Estrid, the Masked'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Estrid, the Masked'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Estrid, the Masked'),
        (select types.id from types where name = 'Estrid')
    ) 
 on conflict do nothing;
