insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Lord Windgrace'),
    (select id from sets where short_name = 'oc18'),
    '43',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Saheeli, the Gifted'),
    (select id from sets where short_name = 'oc18'),
    '44',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aminatou, the Fateshifter'),
    (select id from sets where short_name = 'oc18'),
    '37',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Estrid, the Masked'),
    (select id from sets where short_name = 'oc18'),
    '40',
    'mythic'
) 
 on conflict do nothing;
