insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Sylvan Ranger'),
    (select id from sets where short_name = 'pwp11'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vault Skirge'),
    (select id from sets where short_name = 'pwp11'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodcrazed Neonate'),
    (select id from sets where short_name = 'pwp11'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boneyard Wurm'),
    (select id from sets where short_name = 'pwp11'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Signal Pest'),
    (select id from sets where short_name = 'pwp11'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maul Splicer'),
    (select id from sets where short_name = 'pwp11'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Master''s Call'),
    (select id from sets where short_name = 'pwp11'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Flame'),
    (select id from sets where short_name = 'pwp11'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curse of the Bloody Tome'),
    (select id from sets where short_name = 'pwp11'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Auramancer'),
    (select id from sets where short_name = 'pwp11'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fling'),
    (select id from sets where short_name = 'pwp11'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tormented Soul'),
    (select id from sets where short_name = 'pwp11'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shrine of Burning Rage'),
    (select id from sets where short_name = 'pwp11'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plague Myr'),
    (select id from sets where short_name = 'pwp11'),
    '65',
    'rare'
) 
 on conflict do nothing;
