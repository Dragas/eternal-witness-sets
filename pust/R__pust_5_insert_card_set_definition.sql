insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Earl of Squirrel'),
    (select id from sets where short_name = 'pust'),
    '108',
    'rare'
) 
 on conflict do nothing;
