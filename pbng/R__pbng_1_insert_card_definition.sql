    insert into mtgcard(name) values ('Arbiter of the Ideal') on conflict do nothing;
    insert into mtgcard(name) values ('Silent Sentinel') on conflict do nothing;
    insert into mtgcard(name) values ('Nessian Wilds Ravager') on conflict do nothing;
    insert into mtgcard(name) values ('Kiora''s Follower') on conflict do nothing;
    insert into mtgcard(name) values ('Fated Conflagration') on conflict do nothing;
    insert into mtgcard(name) values ('Pain Seer') on conflict do nothing;
    insert into mtgcard(name) values ('Tromokratis') on conflict do nothing;
    insert into mtgcard(name) values ('Forgestoker Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Eater of Hope') on conflict do nothing;
