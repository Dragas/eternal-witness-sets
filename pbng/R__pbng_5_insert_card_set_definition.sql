insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Arbiter of the Ideal'),
    (select id from sets where short_name = 'pbng'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silent Sentinel'),
    (select id from sets where short_name = 'pbng'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nessian Wilds Ravager'),
    (select id from sets where short_name = 'pbng'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kiora''s Follower'),
    (select id from sets where short_name = 'pbng'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fated Conflagration'),
    (select id from sets where short_name = 'pbng'),
    '*94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pain Seer'),
    (select id from sets where short_name = 'pbng'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tromokratis'),
    (select id from sets where short_name = 'pbng'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forgestoker Dragon'),
    (select id from sets where short_name = 'pbng'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eater of Hope'),
    (select id from sets where short_name = 'pbng'),
    '66',
    'rare'
) 
 on conflict do nothing;
