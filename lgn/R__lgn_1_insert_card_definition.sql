    insert into mtgcard(name) values ('Timberwatch Elf') on conflict do nothing;
    insert into mtgcard(name) values ('Aphetto Exterminator') on conflict do nothing;
    insert into mtgcard(name) values ('Celestial Gatekeeper') on conflict do nothing;
    insert into mtgcard(name) values ('Canopy Crawler') on conflict do nothing;
    insert into mtgcard(name) values ('Corpse Harvester') on conflict do nothing;
    insert into mtgcard(name) values ('Sunstrike Legionnaire') on conflict do nothing;
    insert into mtgcard(name) values ('Ridgetop Raptor') on conflict do nothing;
    insert into mtgcard(name) values ('Tribal Forcemage') on conflict do nothing;
    insert into mtgcard(name) values ('Weaver of Lies') on conflict do nothing;
    insert into mtgcard(name) values ('Essence Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Flamewave Invoker') on conflict do nothing;
    insert into mtgcard(name) values ('Wall of Deceit') on conflict do nothing;
    insert into mtgcard(name) values ('Glintwing Invoker') on conflict do nothing;
    insert into mtgcard(name) values ('Keeneye Aven') on conflict do nothing;
    insert into mtgcard(name) values ('Mistform Wakecaster') on conflict do nothing;
    insert into mtgcard(name) values ('Shifting Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Imperial Hellkite') on conflict do nothing;
    insert into mtgcard(name) values ('Sootfeather Flock') on conflict do nothing;
    insert into mtgcard(name) values ('Defender of the Order') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Firebug') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Lookout') on conflict do nothing;
    insert into mtgcard(name) values ('Gempalm Incinerator') on conflict do nothing;
    insert into mtgcard(name) values ('Riptide Director') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Turncoat') on conflict do nothing;
    insert into mtgcard(name) values ('Drinker of Sorrow') on conflict do nothing;
    insert into mtgcard(name) values ('Rockshard Elemental') on conflict do nothing;
    insert into mtgcard(name) values ('Hollow Specter') on conflict do nothing;
    insert into mtgcard(name) values ('Embalmed Brawler') on conflict do nothing;
    insert into mtgcard(name) values ('Quick Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Beacon of Destiny') on conflict do nothing;
    insert into mtgcard(name) values ('Brood Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Swooping Talon') on conflict do nothing;
    insert into mtgcard(name) values ('Skirk Drill Sergeant') on conflict do nothing;
    insert into mtgcard(name) values ('Seedborn Muse') on conflict do nothing;
    insert into mtgcard(name) values ('Wirewood Channeler') on conflict do nothing;
    insert into mtgcard(name) values ('Aven Warhawk') on conflict do nothing;
    insert into mtgcard(name) values ('Daru Sanctifier') on conflict do nothing;
    insert into mtgcard(name) values ('Defiant Elf') on conflict do nothing;
    insert into mtgcard(name) values ('Aven Envoy') on conflict do nothing;
    insert into mtgcard(name) values ('Skirk Outrider') on conflict do nothing;
    insert into mtgcard(name) values ('Shaleskin Plower') on conflict do nothing;
    insert into mtgcard(name) values ('Scion of Darkness') on conflict do nothing;
    insert into mtgcard(name) values ('Berserk Murlodont') on conflict do nothing;
    insert into mtgcard(name) values ('Feral Throwback') on conflict do nothing;
    insert into mtgcard(name) values ('Starlight Invoker') on conflict do nothing;
    insert into mtgcard(name) values ('Lavaborn Muse') on conflict do nothing;
    insert into mtgcard(name) values ('Crypt Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Krosan Cloudscraper') on conflict do nothing;
    insert into mtgcard(name) values ('Branchsnap Lorian') on conflict do nothing;
    insert into mtgcard(name) values ('Riptide Mangler') on conflict do nothing;
    insert into mtgcard(name) values ('Voidmage Apprentice') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Assassin') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Grappler') on conflict do nothing;
    insert into mtgcard(name) values ('Warped Researcher') on conflict do nothing;
    insert into mtgcard(name) values ('Echo Tracer') on conflict do nothing;
    insert into mtgcard(name) values ('Ghastly Remains') on conflict do nothing;
    insert into mtgcard(name) values ('Havoc Demon') on conflict do nothing;
    insert into mtgcard(name) values ('Cloudreach Cavalry') on conflict do nothing;
    insert into mtgcard(name) values ('Gempalm Sorcerer') on conflict do nothing;
    insert into mtgcard(name) values ('Stoic Champion') on conflict do nothing;
    insert into mtgcard(name) values ('Windborn Muse') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Goon') on conflict do nothing;
    insert into mtgcard(name) values ('Planar Guide') on conflict do nothing;
    insert into mtgcard(name) values ('Bloodstoke Howler') on conflict do nothing;
    insert into mtgcard(name) values ('Deftblade Elite') on conflict do nothing;
    insert into mtgcard(name) values ('Akroma''s Devoted') on conflict do nothing;
    insert into mtgcard(name) values ('Mistform Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Patron of the Wild') on conflict do nothing;
    insert into mtgcard(name) values ('Blood Celebrant') on conflict do nothing;
    insert into mtgcard(name) values ('Akroma, Angel of Wrath') on conflict do nothing;
    insert into mtgcard(name) values ('Plated Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Earthblighter') on conflict do nothing;
    insert into mtgcard(name) values ('Macetail Hystrodon') on conflict do nothing;
    insert into mtgcard(name) values ('Cephalid Pathmage') on conflict do nothing;
    insert into mtgcard(name) values ('Unstable Hulk') on conflict do nothing;
    insert into mtgcard(name) values ('Skinthinner') on conflict do nothing;
    insert into mtgcard(name) values ('Infernal Caretaker') on conflict do nothing;
    insert into mtgcard(name) values ('Aven Redeemer') on conflict do nothing;
    insert into mtgcard(name) values ('Synapse Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Crookclaw Elder') on conflict do nothing;
    insert into mtgcard(name) values ('Gempalm Polluter') on conflict do nothing;
    insert into mtgcard(name) values ('Frenetic Raptor') on conflict do nothing;
    insert into mtgcard(name) values ('Magma Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Dynamo') on conflict do nothing;
    insert into mtgcard(name) values ('Stonewood Invoker') on conflict do nothing;
    insert into mtgcard(name) values ('Glowering Rogon') on conflict do nothing;
    insert into mtgcard(name) values ('Glowrider') on conflict do nothing;
    insert into mtgcard(name) values ('Elvish Soultiller') on conflict do nothing;
    insert into mtgcard(name) values ('Dreamborn Muse') on conflict do nothing;
    insert into mtgcard(name) values ('Caller of the Claw') on conflict do nothing;
    insert into mtgcard(name) values ('Fugitive Wizard') on conflict do nothing;
    insert into mtgcard(name) values ('Brontotherium') on conflict do nothing;
    insert into mtgcard(name) values ('Phage the Untouchable') on conflict do nothing;
    insert into mtgcard(name) values ('Blade Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Graveborn Muse') on conflict do nothing;
    insert into mtgcard(name) values ('Crested Craghorn') on conflict do nothing;
    insert into mtgcard(name) values ('Vile Deacon') on conflict do nothing;
    insert into mtgcard(name) values ('Mistform Seaswift') on conflict do nothing;
    insert into mtgcard(name) values ('Zombie Brute') on conflict do nothing;
    insert into mtgcard(name) values ('Wall of Hope') on conflict do nothing;
    insert into mtgcard(name) values ('Deathmark Prelate') on conflict do nothing;
    insert into mtgcard(name) values ('Krosan Vorine') on conflict do nothing;
    insert into mtgcard(name) values ('Withered Wretch') on conflict do nothing;
    insert into mtgcard(name) values ('Skirk Marauder') on conflict do nothing;
    insert into mtgcard(name) values ('Spectral Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Clickslither') on conflict do nothing;
    insert into mtgcard(name) values ('Toxin Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Whipgrass Entangler') on conflict do nothing;
    insert into mtgcard(name) values ('Primal Whisperer') on conflict do nothing;
    insert into mtgcard(name) values ('Hunter Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('White Knight') on conflict do nothing;
    insert into mtgcard(name) values ('Wingbeat Warrior') on conflict do nothing;
    insert into mtgcard(name) values ('Dripping Dead') on conflict do nothing;
    insert into mtgcard(name) values ('Totem Speaker') on conflict do nothing;
    insert into mtgcard(name) values ('Enormous Baloth') on conflict do nothing;
    insert into mtgcard(name) values ('Dermoplasm') on conflict do nothing;
    insert into mtgcard(name) values ('Merchant of Secrets') on conflict do nothing;
    insert into mtgcard(name) values ('Vexing Beetle') on conflict do nothing;
    insert into mtgcard(name) values ('Master of the Veil') on conflict do nothing;
    insert into mtgcard(name) values ('Gempalm Avenger') on conflict do nothing;
    insert into mtgcard(name) values ('Covert Operative') on conflict do nothing;
    insert into mtgcard(name) values ('Daru Mender') on conflict do nothing;
    insert into mtgcard(name) values ('Chromeshell Crab') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Clearcutter') on conflict do nothing;
    insert into mtgcard(name) values ('Ward Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Mistform Ultimus') on conflict do nothing;
    insert into mtgcard(name) values ('Primoc Escapee') on conflict do nothing;
    insert into mtgcard(name) values ('Nantuko Vigilante') on conflict do nothing;
    insert into mtgcard(name) values ('Wirewood Hivemaster') on conflict do nothing;
    insert into mtgcard(name) values ('Liege of the Axe') on conflict do nothing;
    insert into mtgcard(name) values ('Dark Supplicant') on conflict do nothing;
    insert into mtgcard(name) values ('Kilnmouth Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Bane of the Living') on conflict do nothing;
    insert into mtgcard(name) values ('Hundroog') on conflict do nothing;
    insert into mtgcard(name) values ('Keeper of the Nine Gales') on conflict do nothing;
    insert into mtgcard(name) values ('Lowland Tracker') on conflict do nothing;
    insert into mtgcard(name) values ('Gempalm Strider') on conflict do nothing;
    insert into mtgcard(name) values ('Needleshot Gourna') on conflict do nothing;
    insert into mtgcard(name) values ('Noxious Ghoul') on conflict do nothing;
    insert into mtgcard(name) values ('Willbender') on conflict do nothing;
    insert into mtgcard(name) values ('Warbreak Trumpeter') on conflict do nothing;
    insert into mtgcard(name) values ('Skirk Alarmist') on conflict do nothing;
    insert into mtgcard(name) values ('Root Sliver') on conflict do nothing;
    insert into mtgcard(name) values ('Smokespew Invoker') on conflict do nothing;
    insert into mtgcard(name) values ('Daru Stinger') on conflict do nothing;
