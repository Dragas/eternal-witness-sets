insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Timberwatch Elf'),
    (select id from sets where short_name = 'lgn'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aphetto Exterminator'),
    (select id from sets where short_name = 'lgn'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Celestial Gatekeeper'),
    (select id from sets where short_name = 'lgn'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Canopy Crawler'),
    (select id from sets where short_name = 'lgn'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Corpse Harvester'),
    (select id from sets where short_name = 'lgn'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunstrike Legionnaire'),
    (select id from sets where short_name = 'lgn'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ridgetop Raptor'),
    (select id from sets where short_name = 'lgn'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tribal Forcemage'),
    (select id from sets where short_name = 'lgn'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Weaver of Lies'),
    (select id from sets where short_name = 'lgn'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Essence Sliver'),
    (select id from sets where short_name = 'lgn'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flamewave Invoker'),
    (select id from sets where short_name = 'lgn'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Deceit'),
    (select id from sets where short_name = 'lgn'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glintwing Invoker'),
    (select id from sets where short_name = 'lgn'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keeneye Aven'),
    (select id from sets where short_name = 'lgn'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistform Wakecaster'),
    (select id from sets where short_name = 'lgn'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shifting Sliver'),
    (select id from sets where short_name = 'lgn'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Imperial Hellkite'),
    (select id from sets where short_name = 'lgn'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sootfeather Flock'),
    (select id from sets where short_name = 'lgn'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Defender of the Order'),
    (select id from sets where short_name = 'lgn'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Firebug'),
    (select id from sets where short_name = 'lgn'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Lookout'),
    (select id from sets where short_name = 'lgn'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gempalm Incinerator'),
    (select id from sets where short_name = 'lgn'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Riptide Director'),
    (select id from sets where short_name = 'lgn'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Turncoat'),
    (select id from sets where short_name = 'lgn'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drinker of Sorrow'),
    (select id from sets where short_name = 'lgn'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rockshard Elemental'),
    (select id from sets where short_name = 'lgn'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hollow Specter'),
    (select id from sets where short_name = 'lgn'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Embalmed Brawler'),
    (select id from sets where short_name = 'lgn'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quick Sliver'),
    (select id from sets where short_name = 'lgn'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beacon of Destiny'),
    (select id from sets where short_name = 'lgn'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brood Sliver'),
    (select id from sets where short_name = 'lgn'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swooping Talon'),
    (select id from sets where short_name = 'lgn'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skirk Drill Sergeant'),
    (select id from sets where short_name = 'lgn'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seedborn Muse'),
    (select id from sets where short_name = 'lgn'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wirewood Channeler'),
    (select id from sets where short_name = 'lgn'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aven Warhawk'),
    (select id from sets where short_name = 'lgn'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daru Sanctifier'),
    (select id from sets where short_name = 'lgn'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Defiant Elf'),
    (select id from sets where short_name = 'lgn'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Envoy'),
    (select id from sets where short_name = 'lgn'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skirk Outrider'),
    (select id from sets where short_name = 'lgn'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shaleskin Plower'),
    (select id from sets where short_name = 'lgn'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scion of Darkness'),
    (select id from sets where short_name = 'lgn'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Berserk Murlodont'),
    (select id from sets where short_name = 'lgn'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feral Throwback'),
    (select id from sets where short_name = 'lgn'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Starlight Invoker'),
    (select id from sets where short_name = 'lgn'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lavaborn Muse'),
    (select id from sets where short_name = 'lgn'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crypt Sliver'),
    (select id from sets where short_name = 'lgn'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krosan Cloudscraper'),
    (select id from sets where short_name = 'lgn'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Branchsnap Lorian'),
    (select id from sets where short_name = 'lgn'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Riptide Mangler'),
    (select id from sets where short_name = 'lgn'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voidmage Apprentice'),
    (select id from sets where short_name = 'lgn'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Assassin'),
    (select id from sets where short_name = 'lgn'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Grappler'),
    (select id from sets where short_name = 'lgn'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warped Researcher'),
    (select id from sets where short_name = 'lgn'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Echo Tracer'),
    (select id from sets where short_name = 'lgn'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghastly Remains'),
    (select id from sets where short_name = 'lgn'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Havoc Demon'),
    (select id from sets where short_name = 'lgn'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloudreach Cavalry'),
    (select id from sets where short_name = 'lgn'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gempalm Sorcerer'),
    (select id from sets where short_name = 'lgn'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stoic Champion'),
    (select id from sets where short_name = 'lgn'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Windborn Muse'),
    (select id from sets where short_name = 'lgn'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Goon'),
    (select id from sets where short_name = 'lgn'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Planar Guide'),
    (select id from sets where short_name = 'lgn'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodstoke Howler'),
    (select id from sets where short_name = 'lgn'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deftblade Elite'),
    (select id from sets where short_name = 'lgn'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akroma''s Devoted'),
    (select id from sets where short_name = 'lgn'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mistform Sliver'),
    (select id from sets where short_name = 'lgn'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Patron of the Wild'),
    (select id from sets where short_name = 'lgn'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Celebrant'),
    (select id from sets where short_name = 'lgn'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akroma, Angel of Wrath'),
    (select id from sets where short_name = 'lgn'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plated Sliver'),
    (select id from sets where short_name = 'lgn'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earthblighter'),
    (select id from sets where short_name = 'lgn'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Macetail Hystrodon'),
    (select id from sets where short_name = 'lgn'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cephalid Pathmage'),
    (select id from sets where short_name = 'lgn'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unstable Hulk'),
    (select id from sets where short_name = 'lgn'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skinthinner'),
    (select id from sets where short_name = 'lgn'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infernal Caretaker'),
    (select id from sets where short_name = 'lgn'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Redeemer'),
    (select id from sets where short_name = 'lgn'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Synapse Sliver'),
    (select id from sets where short_name = 'lgn'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crookclaw Elder'),
    (select id from sets where short_name = 'lgn'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gempalm Polluter'),
    (select id from sets where short_name = 'lgn'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frenetic Raptor'),
    (select id from sets where short_name = 'lgn'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magma Sliver'),
    (select id from sets where short_name = 'lgn'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Dynamo'),
    (select id from sets where short_name = 'lgn'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stonewood Invoker'),
    (select id from sets where short_name = 'lgn'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glowering Rogon'),
    (select id from sets where short_name = 'lgn'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glowrider'),
    (select id from sets where short_name = 'lgn'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Soultiller'),
    (select id from sets where short_name = 'lgn'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreamborn Muse'),
    (select id from sets where short_name = 'lgn'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Caller of the Claw'),
    (select id from sets where short_name = 'lgn'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fugitive Wizard'),
    (select id from sets where short_name = 'lgn'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brontotherium'),
    (select id from sets where short_name = 'lgn'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phage the Untouchable'),
    (select id from sets where short_name = 'lgn'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blade Sliver'),
    (select id from sets where short_name = 'lgn'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Graveborn Muse'),
    (select id from sets where short_name = 'lgn'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crested Craghorn'),
    (select id from sets where short_name = 'lgn'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vile Deacon'),
    (select id from sets where short_name = 'lgn'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistform Seaswift'),
    (select id from sets where short_name = 'lgn'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Brute'),
    (select id from sets where short_name = 'lgn'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Hope'),
    (select id from sets where short_name = 'lgn'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathmark Prelate'),
    (select id from sets where short_name = 'lgn'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krosan Vorine'),
    (select id from sets where short_name = 'lgn'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Withered Wretch'),
    (select id from sets where short_name = 'lgn'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skirk Marauder'),
    (select id from sets where short_name = 'lgn'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spectral Sliver'),
    (select id from sets where short_name = 'lgn'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clickslither'),
    (select id from sets where short_name = 'lgn'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Toxin Sliver'),
    (select id from sets where short_name = 'lgn'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whipgrass Entangler'),
    (select id from sets where short_name = 'lgn'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primal Whisperer'),
    (select id from sets where short_name = 'lgn'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hunter Sliver'),
    (select id from sets where short_name = 'lgn'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'White Knight'),
    (select id from sets where short_name = 'lgn'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wingbeat Warrior'),
    (select id from sets where short_name = 'lgn'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dripping Dead'),
    (select id from sets where short_name = 'lgn'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Totem Speaker'),
    (select id from sets where short_name = 'lgn'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enormous Baloth'),
    (select id from sets where short_name = 'lgn'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dermoplasm'),
    (select id from sets where short_name = 'lgn'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merchant of Secrets'),
    (select id from sets where short_name = 'lgn'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vexing Beetle'),
    (select id from sets where short_name = 'lgn'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Master of the Veil'),
    (select id from sets where short_name = 'lgn'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gempalm Avenger'),
    (select id from sets where short_name = 'lgn'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Covert Operative'),
    (select id from sets where short_name = 'lgn'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daru Mender'),
    (select id from sets where short_name = 'lgn'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chromeshell Crab'),
    (select id from sets where short_name = 'lgn'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Clearcutter'),
    (select id from sets where short_name = 'lgn'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ward Sliver'),
    (select id from sets where short_name = 'lgn'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mistform Ultimus'),
    (select id from sets where short_name = 'lgn'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primoc Escapee'),
    (select id from sets where short_name = 'lgn'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nantuko Vigilante'),
    (select id from sets where short_name = 'lgn'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wirewood Hivemaster'),
    (select id from sets where short_name = 'lgn'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liege of the Axe'),
    (select id from sets where short_name = 'lgn'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Supplicant'),
    (select id from sets where short_name = 'lgn'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kilnmouth Dragon'),
    (select id from sets where short_name = 'lgn'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bane of the Living'),
    (select id from sets where short_name = 'lgn'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hundroog'),
    (select id from sets where short_name = 'lgn'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keeper of the Nine Gales'),
    (select id from sets where short_name = 'lgn'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lowland Tracker'),
    (select id from sets where short_name = 'lgn'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gempalm Strider'),
    (select id from sets where short_name = 'lgn'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Needleshot Gourna'),
    (select id from sets where short_name = 'lgn'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Noxious Ghoul'),
    (select id from sets where short_name = 'lgn'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Willbender'),
    (select id from sets where short_name = 'lgn'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warbreak Trumpeter'),
    (select id from sets where short_name = 'lgn'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skirk Alarmist'),
    (select id from sets where short_name = 'lgn'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Root Sliver'),
    (select id from sets where short_name = 'lgn'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smokespew Invoker'),
    (select id from sets where short_name = 'lgn'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daru Stinger'),
    (select id from sets where short_name = 'lgn'),
    '10',
    'common'
) 
 on conflict do nothing;
