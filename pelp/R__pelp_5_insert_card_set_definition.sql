insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pelp'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pelp'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pelp'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pelp'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pelp'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pelp'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pelp'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pelp'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pelp'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pelp'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pelp'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pelp'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pelp'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pelp'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pelp'),
    '11',
    'common'
) 
 on conflict do nothing;
