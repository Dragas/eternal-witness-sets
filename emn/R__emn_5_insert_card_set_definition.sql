insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Bedlam Reveler'),
    (select id from sets where short_name = 'emn'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grim Flayer'),
    (select id from sets where short_name = 'emn'),
    '184',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cryptolith Fragment // Aurora of Emrakul'),
    (select id from sets where short_name = 'emn'),
    '193',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drownyard Behemoth'),
    (select id from sets where short_name = 'emn'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fogwalker'),
    (select id from sets where short_name = 'emn'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hanweir Garrison'),
    (select id from sets where short_name = 'emn'),
    '130a',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smoldering Werewolf // Erupting Dreadwolf'),
    (select id from sets where short_name = 'emn'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Summary Dismissal'),
    (select id from sets where short_name = 'emn'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whispers of Emrakul'),
    (select id from sets where short_name = 'emn'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ulvenwald Captive // Ulvenwald Abomination'),
    (select id from sets where short_name = 'emn'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drag Under'),
    (select id from sets where short_name = 'emn'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murder'),
    (select id from sets where short_name = 'emn'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Emrakul''s Influence'),
    (select id from sets where short_name = 'emn'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Faith Unbroken'),
    (select id from sets where short_name = 'emn'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clear Shot'),
    (select id from sets where short_name = 'emn'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Geist of the Lonely Vigil'),
    (select id from sets where short_name = 'emn'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eternal Scourge'),
    (select id from sets where short_name = 'emn'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Haunted Dead'),
    (select id from sets where short_name = 'emn'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drogskol Shieldmate'),
    (select id from sets where short_name = 'emn'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thraben Standard Bearer'),
    (select id from sets where short_name = 'emn'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abandon Reason'),
    (select id from sets where short_name = 'emn'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ingenious Skaab'),
    (select id from sets where short_name = 'emn'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Impetuous Devils'),
    (select id from sets where short_name = 'emn'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vexing Scuttler'),
    (select id from sets where short_name = 'emn'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Give No Ground'),
    (select id from sets where short_name = 'emn'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hanweir Battlements'),
    (select id from sets where short_name = 'emn'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nahiri''s Wrath'),
    (select id from sets where short_name = 'emn'),
    '137',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Grizzled Angler // Grisly Anglerfish'),
    (select id from sets where short_name = 'emn'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prey Upon'),
    (select id from sets where short_name = 'emn'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stromkirk Occultist'),
    (select id from sets where short_name = 'emn'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oath of Liliana'),
    (select id from sets where short_name = 'emn'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gavony Unhallowed'),
    (select id from sets where short_name = 'emn'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curious Homunculus // Voracious Reader'),
    (select id from sets where short_name = 'emn'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boon of Emrakul'),
    (select id from sets where short_name = 'emn'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voldaren Pariah // Abolisher of Bloodlines'),
    (select id from sets where short_name = 'emn'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wharf Infiltrator'),
    (select id from sets where short_name = 'emn'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swift Spinner'),
    (select id from sets where short_name = 'emn'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bruna, the Fading Light'),
    (select id from sets where short_name = 'emn'),
    '15a',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Succumb to Temptation'),
    (select id from sets where short_name = 'emn'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ishkanah, Grafwidow'),
    (select id from sets where short_name = 'emn'),
    '162',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Springsage Ritual'),
    (select id from sets where short_name = 'emn'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gisa and Geralf'),
    (select id from sets where short_name = 'emn'),
    '183',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Providence'),
    (select id from sets where short_name = 'emn'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'It of the Horrid Swarm'),
    (select id from sets where short_name = 'emn'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tree of Perdition'),
    (select id from sets where short_name = 'emn'),
    '109',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Noose Constrictor'),
    (select id from sets where short_name = 'emn'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Docent of Perfection // Final Iteration'),
    (select id from sets where short_name = 'emn'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mockery of Nature'),
    (select id from sets where short_name = 'emn'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Laboratory Brute'),
    (select id from sets where short_name = 'emn'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Otherworldly Outburst'),
    (select id from sets where short_name = 'emn'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lunarch Mantle'),
    (select id from sets where short_name = 'emn'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Peace of Mind'),
    (select id from sets where short_name = 'emn'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cemetery Recruitment'),
    (select id from sets where short_name = 'emn'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Turn Aside'),
    (select id from sets where short_name = 'emn'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Field Creeper'),
    (select id from sets where short_name = 'emn'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elder Deep-Fiend'),
    (select id from sets where short_name = 'emn'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Graf Rats'),
    (select id from sets where short_name = 'emn'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ulrich of the Krallenhorde // Ulrich, Uncontested Alpha'),
    (select id from sets where short_name = 'emn'),
    '191',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nebelgast Herald'),
    (select id from sets where short_name = 'emn'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Courageous Outrider'),
    (select id from sets where short_name = 'emn'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desperate Sentry'),
    (select id from sets where short_name = 'emn'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foul Emissary'),
    (select id from sets where short_name = 'emn'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mausoleum Wanderer'),
    (select id from sets where short_name = 'emn'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dawn Gryff'),
    (select id from sets where short_name = 'emn'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hamlet Captain'),
    (select id from sets where short_name = 'emn'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Geist of the Archives'),
    (select id from sets where short_name = 'emn'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Choking Restraints'),
    (select id from sets where short_name = 'emn'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mournwillow'),
    (select id from sets where short_name = 'emn'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bold Impaler'),
    (select id from sets where short_name = 'emn'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vildin-Pack Outcast // Dronepack Kindred'),
    (select id from sets where short_name = 'emn'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Backwoods Survivalists'),
    (select id from sets where short_name = 'emn'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Salvation'),
    (select id from sets where short_name = 'emn'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirrorwing Dragon'),
    (select id from sets where short_name = 'emn'),
    '136',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scour the Laboratory'),
    (select id from sets where short_name = 'emn'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Emrakul''s Evangel'),
    (select id from sets where short_name = 'emn'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Furyblade Vampire'),
    (select id from sets where short_name = 'emn'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Decimator of the Provinces'),
    (select id from sets where short_name = 'emn'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Midnight Scavengers'),
    (select id from sets where short_name = 'emn'),
    '96a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brisela, Voice of Nightmares'),
    (select id from sets where short_name = 'emn'),
    '15b',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Spirit of the Hunt'),
    (select id from sets where short_name = 'emn'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steadfast Cathar'),
    (select id from sets where short_name = 'emn'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chilling Grasp'),
    (select id from sets where short_name = 'emn'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Waxing Moon'),
    (select id from sets where short_name = 'emn'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cathar''s Shield'),
    (select id from sets where short_name = 'emn'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cryptbreaker'),
    (select id from sets where short_name = 'emn'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heron''s Grace Champion'),
    (select id from sets where short_name = 'emn'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Somberwald Stag'),
    (select id from sets where short_name = 'emn'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hanweir, the Writhing Township'),
    (select id from sets where short_name = 'emn'),
    '130b',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fortune''s Favor'),
    (select id from sets where short_name = 'emn'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chittering Host'),
    (select id from sets where short_name = 'emn'),
    '96b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terrarion'),
    (select id from sets where short_name = 'emn'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coax from the Blind Eternities'),
    (select id from sets where short_name = 'emn'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wolfkin Bond'),
    (select id from sets where short_name = 'emn'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spectral Reserves'),
    (select id from sets where short_name = 'emn'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lunar Force'),
    (select id from sets where short_name = 'emn'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shreds of Sanity'),
    (select id from sets where short_name = 'emn'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Imprisoned in the Moon'),
    (select id from sets where short_name = 'emn'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woodland Patrol'),
    (select id from sets where short_name = 'emn'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ulvenwald Observer'),
    (select id from sets where short_name = 'emn'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wailing Ghoul'),
    (select id from sets where short_name = 'emn'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Splendid Reclamation'),
    (select id from sets where short_name = 'emn'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conduit of Storms // Conduit of Emrakul'),
    (select id from sets where short_name = 'emn'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sigarda''s Aid'),
    (select id from sets where short_name = 'emn'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stensia Banquet'),
    (select id from sets where short_name = 'emn'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Assembled Alphas'),
    (select id from sets where short_name = 'emn'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crop Sigil'),
    (select id from sets where short_name = 'emn'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Olivia''s Dragoon'),
    (select id from sets where short_name = 'emn'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Markov Crusader'),
    (select id from sets where short_name = 'emn'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Collective Defiance'),
    (select id from sets where short_name = 'emn'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Geist-Fueled Scarecrow'),
    (select id from sets where short_name = 'emn'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abundant Maw'),
    (select id from sets where short_name = 'emn'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mercurial Geists'),
    (select id from sets where short_name = 'emn'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Displace'),
    (select id from sets where short_name = 'emn'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Distended Mindbender'),
    (select id from sets where short_name = 'emn'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emrakul, the Promised End'),
    (select id from sets where short_name = 'emn'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sanctifier of Souls'),
    (select id from sets where short_name = 'emn'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strange Augmentation'),
    (select id from sets where short_name = 'emn'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cultist''s Staff'),
    (select id from sets where short_name = 'emn'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alchemist''s Greeting'),
    (select id from sets where short_name = 'emn'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ironwright''s Cleansing'),
    (select id from sets where short_name = 'emn'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nephalia Academy'),
    (select id from sets where short_name = 'emn'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampire Cutthroat'),
    (select id from sets where short_name = 'emn'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Geier Reach Sanitarium'),
    (select id from sets where short_name = 'emn'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Identity Thief'),
    (select id from sets where short_name = 'emn'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thalia''s Lancers'),
    (select id from sets where short_name = 'emn'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lone Rider // It That Rides as One'),
    (select id from sets where short_name = 'emn'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spell Queller'),
    (select id from sets where short_name = 'emn'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wretched Gryff'),
    (select id from sets where short_name = 'emn'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slayer''s Cleaver'),
    (select id from sets where short_name = 'emn'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Contingency Plan'),
    (select id from sets where short_name = 'emn'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tamiyo, Field Researcher'),
    (select id from sets where short_name = 'emn'),
    '190',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Spontaneous Mutation'),
    (select id from sets where short_name = 'emn'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lashweed Lurker'),
    (select id from sets where short_name = 'emn'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Make Mischief'),
    (select id from sets where short_name = 'emn'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Take Inventory'),
    (select id from sets where short_name = 'emn'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deploy the Gatewatch'),
    (select id from sets where short_name = 'emn'),
    '20',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Guardian of Pilgrims'),
    (select id from sets where short_name = 'emn'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selfless Spirit'),
    (select id from sets where short_name = 'emn'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shrill Howler // Howling Chorus'),
    (select id from sets where short_name = 'emn'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blood Mist'),
    (select id from sets where short_name = 'emn'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enlightened Maniac'),
    (select id from sets where short_name = 'emn'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Borrowed Malevolence'),
    (select id from sets where short_name = 'emn'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thalia, Heretic Cathar'),
    (select id from sets where short_name = 'emn'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brazen Wolves'),
    (select id from sets where short_name = 'emn'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prying Questions'),
    (select id from sets where short_name = 'emn'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Elite'),
    (select id from sets where short_name = 'emn'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Subjugator Angel'),
    (select id from sets where short_name = 'emn'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Faithbearer Paladin'),
    (select id from sets where short_name = 'emn'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crossroads Consecrator'),
    (select id from sets where short_name = 'emn'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exultant Cultist'),
    (select id from sets where short_name = 'emn'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind''s Dilation'),
    (select id from sets where short_name = 'emn'),
    '70',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Skirsdag Supplicant'),
    (select id from sets where short_name = 'emn'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savage Alliance'),
    (select id from sets where short_name = 'emn'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blessed Alliance'),
    (select id from sets where short_name = 'emn'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Extricator of Sin // Extricator of Flesh'),
    (select id from sets where short_name = 'emn'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Permeating Mass'),
    (select id from sets where short_name = 'emn'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Weaver of Lightning'),
    (select id from sets where short_name = 'emn'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unsubstantiate'),
    (select id from sets where short_name = 'emn'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Insatiable Gorgers'),
    (select id from sets where short_name = 'emn'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thraben Foulbloods'),
    (select id from sets where short_name = 'emn'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ride Down'),
    (select id from sets where short_name = 'emn'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Campaign of Vengeance'),
    (select id from sets where short_name = 'emn'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodhall Priest'),
    (select id from sets where short_name = 'emn'),
    '181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stromkirk Condemned'),
    (select id from sets where short_name = 'emn'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spreading Flames'),
    (select id from sets where short_name = 'emn'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Certain Death'),
    (select id from sets where short_name = 'emn'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thermo-Alchemist'),
    (select id from sets where short_name = 'emn'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gisela, the Broken Blade'),
    (select id from sets where short_name = 'emn'),
    '28',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Collective Brutality'),
    (select id from sets where short_name = 'emn'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woodcutter''s Grit'),
    (select id from sets where short_name = 'emn'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Falkenrath Reaver'),
    (select id from sets where short_name = 'emn'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Repel the Abominable'),
    (select id from sets where short_name = 'emn'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Incendiary Flow'),
    (select id from sets where short_name = 'emn'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fiend Binder'),
    (select id from sets where short_name = 'emn'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Distemper of the Blood'),
    (select id from sets where short_name = 'emn'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rise from the Grave'),
    (select id from sets where short_name = 'emn'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dusk Feaster'),
    (select id from sets where short_name = 'emn'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ironclad Slayer'),
    (select id from sets where short_name = 'emn'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grapple with the Past'),
    (select id from sets where short_name = 'emn'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Long Road Home'),
    (select id from sets where short_name = 'emn'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Collective Effort'),
    (select id from sets where short_name = 'emn'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodbriar'),
    (select id from sets where short_name = 'emn'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Niblis of Frost'),
    (select id from sets where short_name = 'emn'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Galvanic Bombardment'),
    (select id from sets where short_name = 'emn'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lupine Prototype'),
    (select id from sets where short_name = 'emn'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Borrowed Hostility'),
    (select id from sets where short_name = 'emn'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Noosegraf Mob'),
    (select id from sets where short_name = 'emn'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Convolute'),
    (select id from sets where short_name = 'emn'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primal Druid'),
    (select id from sets where short_name = 'emn'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stitcher''s Graft'),
    (select id from sets where short_name = 'emn'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Separator'),
    (select id from sets where short_name = 'emn'),
    '199',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stensia Innkeeper'),
    (select id from sets where short_name = 'emn'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tangleclaw Werewolf // Fibrous Entangler'),
    (select id from sets where short_name = 'emn'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eldritch Evolution'),
    (select id from sets where short_name = 'emn'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Borrowed Grace'),
    (select id from sets where short_name = 'emn'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thirsting Axe'),
    (select id from sets where short_name = 'emn'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kessig Prowler // Sinuous Predator'),
    (select id from sets where short_name = 'emn'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sigardian Priest'),
    (select id from sets where short_name = 'emn'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harmless Offering'),
    (select id from sets where short_name = 'emn'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prophetic Ravings'),
    (select id from sets where short_name = 'emn'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gnarlwood Dryad'),
    (select id from sets where short_name = 'emn'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana, the Last Hope'),
    (select id from sets where short_name = 'emn'),
    '93',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Weirded Vampire'),
    (select id from sets where short_name = 'emn'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Graf Harvest'),
    (select id from sets where short_name = 'emn'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Advanced Stitchwing'),
    (select id from sets where short_name = 'emn'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deranged Whelp'),
    (select id from sets where short_name = 'emn'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tattered Haunter'),
    (select id from sets where short_name = 'emn'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruthless Disposal'),
    (select id from sets where short_name = 'emn'),
    '103',
    'uncommon'
) 
 on conflict do nothing;
