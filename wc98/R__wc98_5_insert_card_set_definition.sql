insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Survival of the Fittest'),
    (select id from sets where short_name = 'wc98'),
    'bs129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'wc98'),
    'bh331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gemstone Mine'),
    (select id from sets where short_name = 'wc98'),
    'bs164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cursed Scroll'),
    (select id from sets where short_name = 'wc98'),
    'br281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ball Lightning'),
    (select id from sets where short_name = 'wc98'),
    'br210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ben Rubin Bio'),
    (select id from sets where short_name = 'wc98'),
    'br0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grindstone'),
    (select id from sets where short_name = 'wc98'),
    'rb290sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Impulse'),
    (select id from sets where short_name = 'wc98'),
    'rb34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brian Hacker Decklist'),
    (select id from sets where short_name = 'wc98'),
    'bh0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Man-o''-War'),
    (select id from sets where short_name = 'wc98'),
    'bs37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blank Card'),
    (select id from sets where short_name = 'wc98'),
    '00',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'wc98'),
    'bh16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Warden'),
    (select id from sets where short_name = 'wc98'),
    'bh21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shattering Pulse'),
    (select id from sets where short_name = 'wc98'),
    'br102sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undiscovered Paradise'),
    (select id from sets where short_name = 'wc98'),
    'bs167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aura of Silence'),
    (select id from sets where short_name = 'wc98'),
    'bh7b',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dissipate'),
    (select id from sets where short_name = 'wc98'),
    'rb61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Memory Lapse'),
    (select id from sets where short_name = 'wc98'),
    'rb32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soltari Monk'),
    (select id from sets where short_name = 'wc98'),
    'bh45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Randy Buehler Bio'),
    (select id from sets where short_name = 'wc98'),
    'rb0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'wc98'),
    'bh16sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Underground River'),
    (select id from sets where short_name = 'wc98'),
    'bs362',
    'rare'
) ,
(
    (select id from mtgcard where name = '1998 World Championships Ad'),
    (select id from sets where short_name = 'wc98'),
    '0',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc98'),
    'br343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Force Spike'),
    (select id from sets where short_name = 'wc98'),
    'rb58b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nekrataal'),
    (select id from sets where short_name = 'wc98'),
    'bs66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Leak'),
    (select id from sets where short_name = 'wc98'),
    'rb36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mogg Flunkies'),
    (select id from sets where short_name = 'wc98'),
    'br92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc98'),
    'bs349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rainbow Efreet'),
    (select id from sets where short_name = 'wc98'),
    'rb41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ironclaw Orcs'),
    (select id from sets where short_name = 'wc98'),
    'br245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stalking Stones'),
    (select id from sets where short_name = 'wc98'),
    'rb327',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soltari Priest'),
    (select id from sets where short_name = 'wc98'),
    'bh46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viashino Sandstalker'),
    (select id from sets where short_name = 'wc98'),
    'br100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dwarven Miner'),
    (select id from sets where short_name = 'wc98'),
    'br169sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sea Sprite'),
    (select id from sets where short_name = 'wc98'),
    'rb38sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyroblast'),
    (select id from sets where short_name = 'wc98'),
    'bs213sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'White Knight'),
    (select id from sets where short_name = 'wc98'),
    'bh68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fireblast'),
    (select id from sets where short_name = 'wc98'),
    'br79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jackal Pup'),
    (select id from sets where short_name = 'wc98'),
    'br183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Vandal'),
    (select id from sets where short_name = 'wc98'),
    'br105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dismiss'),
    (select id from sets where short_name = 'wc98'),
    'rb58a',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc98'),
    'br345',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wasteland'),
    (select id from sets where short_name = 'wc98'),
    'br330',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc98'),
    'rb337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc98'),
    'bs348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Furnace'),
    (select id from sets where short_name = 'wc98'),
    'bs155sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cursed Scroll'),
    (select id from sets where short_name = 'wc98'),
    'bh281sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'wc98'),
    'br98b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mogg Fanatic'),
    (select id from sets where short_name = 'wc98'),
    'br190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volrath''s Stronghold'),
    (select id from sets where short_name = 'wc98'),
    'bs143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uktabi Orangutan'),
    (select id from sets where short_name = 'wc98'),
    'bs123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hall of Gemstone'),
    (select id from sets where short_name = 'wc98'),
    'bs221sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karplusan Forest'),
    (select id from sets where short_name = 'wc98'),
    'bs356',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'wc98'),
    'bh334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = 'wc98'),
    'rb391',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Randy Buehler Decklist'),
    (select id from sets where short_name = 'wc98'),
    'rb0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Empyrial Armor'),
    (select id from sets where short_name = 'wc98'),
    'bh13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc98'),
    'rb338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cataclysm'),
    (select id from sets where short_name = 'wc98'),
    'bh3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tradewind Rider'),
    (select id from sets where short_name = 'wc98'),
    'bs98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firestorm'),
    (select id from sets where short_name = 'wc98'),
    'br101sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Settlers'),
    (select id from sets where short_name = 'wc98'),
    'bs112b',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abeyance'),
    (select id from sets where short_name = 'wc98'),
    'bh1sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Roots'),
    (select id from sets where short_name = 'wc98'),
    'bs253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc98'),
    'bs347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Blossoms'),
    (select id from sets where short_name = 'wc98'),
    'bs125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Capsize'),
    (select id from sets where short_name = 'wc98'),
    'rb55sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudchaser Eagle'),
    (select id from sets where short_name = 'wc98'),
    'bs15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lobotomy'),
    (select id from sets where short_name = 'wc98'),
    'bs267',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brian Hacker Bio'),
    (select id from sets where short_name = 'wc98'),
    'bh0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = 'wc98'),
    'bs280',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spirit Link'),
    (select id from sets where short_name = 'wc98'),
    'bh64sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc98'),
    'br346',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brian Selden Decklist'),
    (select id from sets where short_name = 'wc98'),
    'bs0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'wc98'),
    'rb57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whispers of the Muse'),
    (select id from sets where short_name = 'wc98'),
    'rb103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc98'),
    'rb335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boil'),
    (select id from sets where short_name = 'wc98'),
    'bs165sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hydroblast'),
    (select id from sets where short_name = 'wc98'),
    'rb72sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = 'wc98'),
    'br184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc98'),
    'bs340',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aura of Silence'),
    (select id from sets where short_name = 'wc98'),
    'bh7bsb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc98'),
    'rb336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = 'wc98'),
    'bh7asb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Final Fortune'),
    (select id from sets where short_name = 'wc98'),
    'br174sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firestorm'),
    (select id from sets where short_name = 'wc98'),
    'bs101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Recurring Nightmare'),
    (select id from sets where short_name = 'wc98'),
    'bs72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verdant Force'),
    (select id from sets where short_name = 'wc98'),
    'bs263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hammer of Bogardan'),
    (select id from sets where short_name = 'wc98'),
    'br181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nomads en-Kor'),
    (select id from sets where short_name = 'wc98'),
    'bh9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc98'),
    'bs350',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brian Selden Bio'),
    (select id from sets where short_name = 'wc98'),
    'bs0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spike Feeder'),
    (select id from sets where short_name = 'wc98'),
    'bs118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wasteland'),
    (select id from sets where short_name = 'wc98'),
    'rb330sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'wc98'),
    'bh332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scroll Rack'),
    (select id from sets where short_name = 'wc98'),
    'bs308',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Staunch Defenders'),
    (select id from sets where short_name = 'wc98'),
    'bs49sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc98'),
    'br344',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paladin en-Vec'),
    (select id from sets where short_name = 'wc98'),
    'bh12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'wc98'),
    'bh333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ben Rubin Decklist'),
    (select id from sets where short_name = 'wc98'),
    'br0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emerald Charm'),
    (select id from sets where short_name = 'wc98'),
    'bs106sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dread of Night'),
    (select id from sets where short_name = 'wc98'),
    'bs130sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forbid'),
    (select id from sets where short_name = 'wc98'),
    'rb35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyroblast'),
    (select id from sets where short_name = 'wc98'),
    'br213sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'wc98'),
    'bs112a',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thrull Surgeon'),
    (select id from sets where short_name = 'wc98'),
    'bs76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bottle Gnomes'),
    (select id from sets where short_name = 'wc98'),
    'br278sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soltari Visionary'),
    (select id from sets where short_name = 'wc98'),
    'bh20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Thaumaturgist'),
    (select id from sets where short_name = 'wc98'),
    'br98sba',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tithe'),
    (select id from sets where short_name = 'wc98'),
    'bh23a',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reflecting Pool'),
    (select id from sets where short_name = 'wc98'),
    'bs322',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spike Weaver'),
    (select id from sets where short_name = 'wc98'),
    'bs128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spirit of the Night'),
    (select id from sets where short_name = 'wc98'),
    'bs146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warrior en-Kor'),
    (select id from sets where short_name = 'wc98'),
    'bh23b',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quicksand'),
    (select id from sets where short_name = 'wc98'),
    'rb166',
    'uncommon'
) 
 on conflict do nothing;
