insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Petals of Insight'),
    (select id from sets where short_name = 'chk'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ember-Fist Zubera'),
    (select id from sets where short_name = 'chk'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pull Under'),
    (select id from sets where short_name = 'chk'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boseiju, Who Shelters All'),
    (select id from sets where short_name = 'chk'),
    '273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oathkeeper, Takeno''s Daisho'),
    (select id from sets where short_name = 'chk'),
    '265',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harsh Deceiver'),
    (select id from sets where short_name = 'chk'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Villainous Ogre'),
    (select id from sets where short_name = 'chk'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Candles'' Glow'),
    (select id from sets where short_name = 'chk'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sift Through Sands'),
    (select id from sets where short_name = 'chk'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kiku, Night''s Flower'),
    (select id from sets where short_name = 'chk'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hisoka''s Defiance'),
    (select id from sets where short_name = 'chk'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thief of Hope'),
    (select id from sets where short_name = 'chk'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yamabushi''s Storm'),
    (select id from sets where short_name = 'chk'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nezumi Ronin'),
    (select id from sets where short_name = 'chk'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rend Spirit'),
    (select id from sets where short_name = 'chk'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tranquil Garden'),
    (select id from sets where short_name = 'chk'),
    '284',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kitsune Healer'),
    (select id from sets where short_name = 'chk'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lure'),
    (select id from sets where short_name = 'chk'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kodama of the South Tree'),
    (select id from sets where short_name = 'chk'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'General''s Kabuto'),
    (select id from sets where short_name = 'chk'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myojin of Night''s Reach'),
    (select id from sets where short_name = 'chk'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Matsu-Tribe Decoy'),
    (select id from sets where short_name = 'chk'),
    '227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azami, Lady of Scrolls'),
    (select id from sets where short_name = 'chk'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kiki-Jiki, Mirror Breaker'),
    (select id from sets where short_name = 'chk'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nezumi Cutthroat'),
    (select id from sets where short_name = 'chk'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ryusei, the Falling Star'),
    (select id from sets where short_name = 'chk'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rag Dealer'),
    (select id from sets where short_name = 'chk'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yamabushi''s Flame'),
    (select id from sets where short_name = 'chk'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Painwracker Oni'),
    (select id from sets where short_name = 'chk'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yosei, the Morning Star'),
    (select id from sets where short_name = 'chk'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Waking Nightmare'),
    (select id from sets where short_name = 'chk'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Seism'),
    (select id from sets where short_name = 'chk'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meloku the Clouded Mirror'),
    (select id from sets where short_name = 'chk'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'He Who Hungers'),
    (select id from sets where short_name = 'chk'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glimpse of Nature'),
    (select id from sets where short_name = 'chk'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Joyous Respite'),
    (select id from sets where short_name = 'chk'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soratami Mirror-Mage'),
    (select id from sets where short_name = 'chk'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eiganjo Castle'),
    (select id from sets where short_name = 'chk'),
    '275',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'chk'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kami of the Waning Moon'),
    (select id from sets where short_name = 'chk'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rootrunner'),
    (select id from sets where short_name = 'chk'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'chk'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aura of Dominion'),
    (select id from sets where short_name = 'chk'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gale Force'),
    (select id from sets where short_name = 'chk'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silent-Chant Zubera'),
    (select id from sets where short_name = 'chk'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earthshaker'),
    (select id from sets where short_name = 'chk'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'chk'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squelch'),
    (select id from sets where short_name = 'chk'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sokenzan Bruiser'),
    (select id from sets where short_name = 'chk'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kodama of the North Tree'),
    (select id from sets where short_name = 'chk'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Iname, Death Aspect'),
    (select id from sets where short_name = 'chk'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shizo, Death''s Storehouse'),
    (select id from sets where short_name = 'chk'),
    '283',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Befoul'),
    (select id from sets where short_name = 'chk'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sire of the Storm'),
    (select id from sets where short_name = 'chk'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Isamaru, Hound of Konda'),
    (select id from sets where short_name = 'chk'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swirl the Mists'),
    (select id from sets where short_name = 'chk'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Field of Reality'),
    (select id from sets where short_name = 'chk'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Venerable Kumo'),
    (select id from sets where short_name = 'chk'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strange Inversion'),
    (select id from sets where short_name = 'chk'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gifts Ungiven'),
    (select id from sets where short_name = 'chk'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feast of Worms'),
    (select id from sets where short_name = 'chk'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jushi Apprentice // Tomoya the Revealer'),
    (select id from sets where short_name = 'chk'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mothrider Samurai'),
    (select id from sets where short_name = 'chk'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'chk'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akki Coalflinger'),
    (select id from sets where short_name = 'chk'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seshiro the Anointed'),
    (select id from sets where short_name = 'chk'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dance of Shadows'),
    (select id from sets where short_name = 'chk'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Journeyer''s Kite'),
    (select id from sets where short_name = 'chk'),
    '257',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eerie Procession'),
    (select id from sets where short_name = 'chk'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Untaidake, the Cloud Keeper'),
    (select id from sets where short_name = 'chk'),
    '285',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Honden of Life''s Web'),
    (select id from sets where short_name = 'chk'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kashi-Tribe Reaver'),
    (select id from sets where short_name = 'chk'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orbweaver Kumo'),
    (select id from sets where short_name = 'chk'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wear Away'),
    (select id from sets where short_name = 'chk'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soratami Mirror-Guard'),
    (select id from sets where short_name = 'chk'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Waterveil Cavern'),
    (select id from sets where short_name = 'chk'),
    '286',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul of Magma'),
    (select id from sets where short_name = 'chk'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashen-Skin Zubera'),
    (select id from sets where short_name = 'chk'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kami of Fire''s Roar'),
    (select id from sets where short_name = 'chk'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tide of War'),
    (select id from sets where short_name = 'chk'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soratami Savant'),
    (select id from sets where short_name = 'chk'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Counsel of the Soratami'),
    (select id from sets where short_name = 'chk'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nine-Ringed Bo'),
    (select id from sets where short_name = 'chk'),
    '263',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soratami Seer'),
    (select id from sets where short_name = 'chk'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Honden of Cleansing Fire'),
    (select id from sets where short_name = 'chk'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = 'chk'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lantern-Lit Graveyard'),
    (select id from sets where short_name = 'chk'),
    '278',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Consuming Vortex'),
    (select id from sets where short_name = 'chk'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Budoka Gardener // Dokai, Weaver of Life'),
    (select id from sets where short_name = 'chk'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sensei''s Divining Top'),
    (select id from sets where short_name = 'chk'),
    '268',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Devouring Greed'),
    (select id from sets where short_name = 'chk'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'chk'),
    '288',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kami of Lunacy'),
    (select id from sets where short_name = 'chk'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sachi, Daughter of Seshiro'),
    (select id from sets where short_name = 'chk'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hisoka''s Guard'),
    (select id from sets where short_name = 'chk'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shell of the Last Kappa'),
    (select id from sets where short_name = 'chk'),
    '269',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cut the Tethers'),
    (select id from sets where short_name = 'chk'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reweave'),
    (select id from sets where short_name = 'chk'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Time of Need'),
    (select id from sets where short_name = 'chk'),
    '247',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Unspeakable'),
    (select id from sets where short_name = 'chk'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Honden of Night''s Reach'),
    (select id from sets where short_name = 'chk'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glacial Ray'),
    (select id from sets where short_name = 'chk'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jukai Messenger'),
    (select id from sets where short_name = 'chk'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Night Dealings'),
    (select id from sets where short_name = 'chk'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mindblaze'),
    (select id from sets where short_name = 'chk'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hall of the Bandit Lord'),
    (select id from sets where short_name = 'chk'),
    '277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kami of the Hunt'),
    (select id from sets where short_name = 'chk'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eight-and-a-Half-Tails'),
    (select id from sets where short_name = 'chk'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Order of the Sacred Bell'),
    (select id from sets where short_name = 'chk'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jugan, the Rising Star'),
    (select id from sets where short_name = 'chk'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thousand-legged Kami'),
    (select id from sets where short_name = 'chk'),
    '246',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shisato, Whispering Hunter'),
    (select id from sets where short_name = 'chk'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'chk'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Midnight Covenant'),
    (select id from sets where short_name = 'chk'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soratami Cloudskater'),
    (select id from sets where short_name = 'chk'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Konda, Lord of Eiganjo'),
    (select id from sets where short_name = 'chk'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orochi Sustainer'),
    (select id from sets where short_name = 'chk'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Devouring Rage'),
    (select id from sets where short_name = 'chk'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lava Spike'),
    (select id from sets where short_name = 'chk'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ronin Houndmaster'),
    (select id from sets where short_name = 'chk'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Callous Deceiver'),
    (select id from sets where short_name = 'chk'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathcurse Ogre'),
    (select id from sets where short_name = 'chk'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kokusho, the Evening Star'),
    (select id from sets where short_name = 'chk'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Floating-Dream Zubera'),
    (select id from sets where short_name = 'chk'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Graceful Adept'),
    (select id from sets where short_name = 'chk'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kami of the Palace Fields'),
    (select id from sets where short_name = 'chk'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pain Kami'),
    (select id from sets where short_name = 'chk'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oni Possession'),
    (select id from sets where short_name = 'chk'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blood Rites'),
    (select id from sets where short_name = 'chk'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soulless Revival'),
    (select id from sets where short_name = 'chk'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sideswipe'),
    (select id from sets where short_name = 'chk'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heartbeat of Spring'),
    (select id from sets where short_name = 'chk'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feral Deceiver'),
    (select id from sets where short_name = 'chk'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'No-Dachi'),
    (select id from sets where short_name = 'chk'),
    '264',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Through the Breach'),
    (select id from sets where short_name = 'chk'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'chk'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akki Lavarunner // Tok-Tok, Volcano Born'),
    (select id from sets where short_name = 'chk'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hankyu'),
    (select id from sets where short_name = 'chk'),
    '253',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Long-Forgotten Gohei'),
    (select id from sets where short_name = 'chk'),
    '261',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kami of the Painted Road'),
    (select id from sets where short_name = 'chk'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terashi''s Cry'),
    (select id from sets where short_name = 'chk'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shimatsu the Bloodcloaked'),
    (select id from sets where short_name = 'chk'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marrow-Gnawer'),
    (select id from sets where short_name = 'chk'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Indomitable Will'),
    (select id from sets where short_name = 'chk'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minamo, School at Water''s Edge'),
    (select id from sets where short_name = 'chk'),
    '279',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Call to Glory'),
    (select id from sets where short_name = 'chk'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Konda''s Banner'),
    (select id from sets where short_name = 'chk'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nature''s Will'),
    (select id from sets where short_name = 'chk'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gutwrencher Oni'),
    (select id from sets where short_name = 'chk'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nezumi Graverobber // Nighteyes the Desecrator'),
    (select id from sets where short_name = 'chk'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pinecrest Ridge'),
    (select id from sets where short_name = 'chk'),
    '281',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thoughtbind'),
    (select id from sets where short_name = 'chk'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Junkyo Bell'),
    (select id from sets where short_name = 'chk'),
    '258',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cruel Deceiver'),
    (select id from sets where short_name = 'chk'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kashi-Tribe Warriors'),
    (select id from sets where short_name = 'chk'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brothers Yamazaki'),
    (select id from sets where short_name = 'chk'),
    '160b',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Horizon Seed'),
    (select id from sets where short_name = 'chk'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orochi Hatchery'),
    (select id from sets where short_name = 'chk'),
    '266',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ethereal Haze'),
    (select id from sets where short_name = 'chk'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Distress'),
    (select id from sets where short_name = 'chk'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kami of Old Stone'),
    (select id from sets where short_name = 'chk'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghostly Prison'),
    (select id from sets where short_name = 'chk'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Konda''s Hatamoto'),
    (select id from sets where short_name = 'chk'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Uncontrollable Anger'),
    (select id from sets where short_name = 'chk'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strength of Cedars'),
    (select id from sets where short_name = 'chk'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guardian of Solitude'),
    (select id from sets where short_name = 'chk'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kodama''s Reach'),
    (select id from sets where short_name = 'chk'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bushi Tenderfoot // Kenzo the Hardhearted'),
    (select id from sets where short_name = 'chk'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hanabi Blast'),
    (select id from sets where short_name = 'chk'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forbidden Orchard'),
    (select id from sets where short_name = 'chk'),
    '276',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'chk'),
    '306',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kami of Twisted Reflection'),
    (select id from sets where short_name = 'chk'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Samurai Enforcers'),
    (select id from sets where short_name = 'chk'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serpent Skin'),
    (select id from sets where short_name = 'chk'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moonring Mirror'),
    (select id from sets where short_name = 'chk'),
    '262',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vassal''s Duty'),
    (select id from sets where short_name = 'chk'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myojin of Seeing Winds'),
    (select id from sets where short_name = 'chk'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wandering Ones'),
    (select id from sets where short_name = 'chk'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reito Lantern'),
    (select id from sets where short_name = 'chk'),
    '267',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Imi Statue'),
    (select id from sets where short_name = 'chk'),
    '255',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Elder'),
    (select id from sets where short_name = 'chk'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rend Flesh'),
    (select id from sets where short_name = 'chk'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soulblast'),
    (select id from sets where short_name = 'chk'),
    '190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burr Grafter'),
    (select id from sets where short_name = 'chk'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hideous Laughter'),
    (select id from sets where short_name = 'chk'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mystic Restraints'),
    (select id from sets where short_name = 'chk'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cleanfall'),
    (select id from sets where short_name = 'chk'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cage of Hands'),
    (select id from sets where short_name = 'chk'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tatsumasa, the Dragon''s Fang'),
    (select id from sets where short_name = 'chk'),
    '270',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lantern Kami'),
    (select id from sets where short_name = 'chk'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gibbering Kami'),
    (select id from sets where short_name = 'chk'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudcrest Lake'),
    (select id from sets where short_name = 'chk'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Peer Through Depths'),
    (select id from sets where short_name = 'chk'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akki Rockspeaker'),
    (select id from sets where short_name = 'chk'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'chk'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reach Through Mists'),
    (select id from sets where short_name = 'chk'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Student of Elements // Tobita, Master of Winds'),
    (select id from sets where short_name = 'chk'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blessed Breath'),
    (select id from sets where short_name = 'chk'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychic Puppetry'),
    (select id from sets where short_name = 'chk'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azusa, Lost but Seeking'),
    (select id from sets where short_name = 'chk'),
    '201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myojin of Cleansing Fire'),
    (select id from sets where short_name = 'chk'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Otherworldly Journey'),
    (select id from sets where short_name = 'chk'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'chk'),
    '303',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moss Kami'),
    (select id from sets where short_name = 'chk'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nagao, Bound by Honor'),
    (select id from sets where short_name = 'chk'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dampen Thought'),
    (select id from sets where short_name = 'chk'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orochi Eggwatcher // Shidako, Broodmistress'),
    (select id from sets where short_name = 'chk'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blind with Anger'),
    (select id from sets where short_name = 'chk'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unnatural Speed'),
    (select id from sets where short_name = 'chk'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frostwielder'),
    (select id from sets where short_name = 'chk'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jade Idol'),
    (select id from sets where short_name = 'chk'),
    '256',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lifted by Clouds'),
    (select id from sets where short_name = 'chk'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battle-Mad Ronin'),
    (select id from sets where short_name = 'chk'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hundred-Talon Kami'),
    (select id from sets where short_name = 'chk'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hana Kami'),
    (select id from sets where short_name = 'chk'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hisoka, Minamo Sensei'),
    (select id from sets where short_name = 'chk'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cranial Extraction'),
    (select id from sets where short_name = 'chk'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reverse the Sands'),
    (select id from sets where short_name = 'chk'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swallowing Plague'),
    (select id from sets where short_name = 'chk'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brothers Yamazaki'),
    (select id from sets where short_name = 'chk'),
    '160a',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Honden of Seeing Winds'),
    (select id from sets where short_name = 'chk'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soilshaper'),
    (select id from sets where short_name = 'chk'),
    '243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Honden of Infinite Rage'),
    (select id from sets where short_name = 'chk'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eye of Nowhere'),
    (select id from sets where short_name = 'chk'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Initiate of Blood // Goka the Unjust'),
    (select id from sets where short_name = 'chk'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Horobi, Death''s Wail'),
    (select id from sets where short_name = 'chk'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kami of Ancient Law'),
    (select id from sets where short_name = 'chk'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kuro, Pitlord'),
    (select id from sets where short_name = 'chk'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Numai Outcast'),
    (select id from sets where short_name = 'chk'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cursed Ronin'),
    (select id from sets where short_name = 'chk'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sensei Golden-Tail'),
    (select id from sets where short_name = 'chk'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soratami Rainshaper'),
    (select id from sets where short_name = 'chk'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'chk'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Struggle for Sanity'),
    (select id from sets where short_name = 'chk'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Honor-Worn Shaku'),
    (select id from sets where short_name = 'chk'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pious Kitsune'),
    (select id from sets where short_name = 'chk'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iname, Life Aspect'),
    (select id from sets where short_name = 'chk'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akki Avalanchers'),
    (select id from sets where short_name = 'chk'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nezumi Bone-Reader'),
    (select id from sets where short_name = 'chk'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'chk'),
    '302',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'chk'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wicked Akuba'),
    (select id from sets where short_name = 'chk'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kitsune Diviner'),
    (select id from sets where short_name = 'chk'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hair-Strung Koto'),
    (select id from sets where short_name = 'chk'),
    '252',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodthirsty Ogre'),
    (select id from sets where short_name = 'chk'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crushing Pain'),
    (select id from sets where short_name = 'chk'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kumano''s Pupils'),
    (select id from sets where short_name = 'chk'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kusari-Gama'),
    (select id from sets where short_name = 'chk'),
    '260',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ragged Veins'),
    (select id from sets where short_name = 'chk'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nezumi Shortfang // Stabwhisker the Odious'),
    (select id from sets where short_name = 'chk'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desperate Ritual'),
    (select id from sets where short_name = 'chk'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teller of Tales'),
    (select id from sets where short_name = 'chk'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'chk'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Uyo, Silent Prophet'),
    (select id from sets where short_name = 'chk'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Commune with Nature'),
    (select id from sets where short_name = 'chk'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kabuto Moth'),
    (select id from sets where short_name = 'chk'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unearthly Blizzard'),
    (select id from sets where short_name = 'chk'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Takeno, Samurai General'),
    (select id from sets where short_name = 'chk'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'chk'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kodama''s Might'),
    (select id from sets where short_name = 'chk'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sosuke, Son of Seshiro'),
    (select id from sets where short_name = 'chk'),
    '244',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vigilance'),
    (select id from sets where short_name = 'chk'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shinka, the Bloodsoaked Keep'),
    (select id from sets where short_name = 'chk'),
    '282',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'chk'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Masako the Humorless'),
    (select id from sets where short_name = 'chk'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hearth Kami'),
    (select id from sets where short_name = 'chk'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ben-Ben, Akki Hermit'),
    (select id from sets where short_name = 'chk'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Part the Veil'),
    (select id from sets where short_name = 'chk'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scuttling Death'),
    (select id from sets where short_name = 'chk'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hinder'),
    (select id from sets where short_name = 'chk'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Night of Souls'' Betrayal'),
    (select id from sets where short_name = 'chk'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hold the Line'),
    (select id from sets where short_name = 'chk'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'chk'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Innocence Kami'),
    (select id from sets where short_name = 'chk'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Myojin of Infinite Rage'),
    (select id from sets where short_name = 'chk'),
    '181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tenza, Godo''s Maul'),
    (select id from sets where short_name = 'chk'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seizan, Perverter of Truth'),
    (select id from sets where short_name = 'chk'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hikari, Twilight Guardian'),
    (select id from sets where short_name = 'chk'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brutal Deceiver'),
    (select id from sets where short_name = 'chk'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'River Kaijin'),
    (select id from sets where short_name = 'chk'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'chk'),
    '290',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dripping-Tongue Zubera'),
    (select id from sets where short_name = 'chk'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orochi Ranger'),
    (select id from sets where short_name = 'chk'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keiga, the Tide Star'),
    (select id from sets where short_name = 'chk'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Samurai of the Pale Curtain'),
    (select id from sets where short_name = 'chk'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ore Gorger'),
    (select id from sets where short_name = 'chk'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quiet Purity'),
    (select id from sets where short_name = 'chk'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Okina, Temple to the Grandfathers'),
    (select id from sets where short_name = 'chk'),
    '280',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myojin of Life''s Web'),
    (select id from sets where short_name = 'chk'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akki Underminer'),
    (select id from sets where short_name = 'chk'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'chk'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Speaker'),
    (select id from sets where short_name = 'chk'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kitsune Mystic // Autumn-Tail, Kitsune Sage'),
    (select id from sets where short_name = 'chk'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Humble Budoka'),
    (select id from sets where short_name = 'chk'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kitsune Riftwalker'),
    (select id from sets where short_name = 'chk'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orochi Leafcaller'),
    (select id from sets where short_name = 'chk'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reciprocate'),
    (select id from sets where short_name = 'chk'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'chk'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kumano, Master Yamabushi'),
    (select id from sets where short_name = 'chk'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Time Stop'),
    (select id from sets where short_name = 'chk'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uba Mask'),
    (select id from sets where short_name = 'chk'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zo-Zu the Punisher'),
    (select id from sets where short_name = 'chk'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vine Kami'),
    (select id from sets where short_name = 'chk'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kitsune Blademaster'),
    (select id from sets where short_name = 'chk'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Devoted Retainer'),
    (select id from sets where short_name = 'chk'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Godo, Bandit Warlord'),
    (select id from sets where short_name = 'chk'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dosan the Falling Leaf'),
    (select id from sets where short_name = 'chk'),
    '205',
    'rare'
) 
 on conflict do nothing;
