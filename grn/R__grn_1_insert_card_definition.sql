    insert into mtgcard(name) values ('Blood Operative') on conflict do nothing;
    insert into mtgcard(name) values ('Disdainful Stroke') on conflict do nothing;
    insert into mtgcard(name) values ('Book Devourer') on conflict do nothing;
    insert into mtgcard(name) values ('Dimir Informant') on conflict do nothing;
    insert into mtgcard(name) values ('Mission Briefing') on conflict do nothing;
    insert into mtgcard(name) values ('Invert // Invent') on conflict do nothing;
    insert into mtgcard(name) values ('Gravitic Punch') on conflict do nothing;
    insert into mtgcard(name) values ('Underrealm Lich') on conflict do nothing;
    insert into mtgcard(name) values ('Glowspore Shaman') on conflict do nothing;
    insert into mtgcard(name) values ('Doom Whisperer') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Electromancer') on conflict do nothing;
    insert into mtgcard(name) values ('Hatchery Spider') on conflict do nothing;
    insert into mtgcard(name) values ('Chemister''s Insight') on conflict do nothing;
    insert into mtgcard(name) values ('Centaur Peacemaker') on conflict do nothing;
    insert into mtgcard(name) values ('Pack''s Favor') on conflict do nothing;
    insert into mtgcard(name) values ('Kraul Raider') on conflict do nothing;
    insert into mtgcard(name) values ('Crush Contraband') on conflict do nothing;
    insert into mtgcard(name) values ('Chamber Sentry') on conflict do nothing;
    insert into mtgcard(name) values ('Direct Current') on conflict do nothing;
    insert into mtgcard(name) values ('Prey Upon') on conflict do nothing;
    insert into mtgcard(name) values ('Nightveil Predator') on conflict do nothing;
    insert into mtgcard(name) values ('Rubblebelt Boar') on conflict do nothing;
    insert into mtgcard(name) values ('Golgari Locket') on conflict do nothing;
    insert into mtgcard(name) values ('Deafening Clarion') on conflict do nothing;
    insert into mtgcard(name) values ('Find // Finality') on conflict do nothing;
    insert into mtgcard(name) values ('Loxodon Restorer') on conflict do nothing;
    insert into mtgcard(name) values ('Izzet Guildgate') on conflict do nothing;
    insert into mtgcard(name) values ('Bartizan Bats') on conflict do nothing;
    insert into mtgcard(name) values ('Devious Cover-Up') on conflict do nothing;
    insert into mtgcard(name) values ('Lazav, the Multifarious') on conflict do nothing;
    insert into mtgcard(name) values ('Nullhide Ferox') on conflict do nothing;
    insert into mtgcard(name) values ('Undercity Necrolisk') on conflict do nothing;
    insert into mtgcard(name) values ('Ritual of Soot') on conflict do nothing;
    insert into mtgcard(name) values ('Mausoleum Secrets') on conflict do nothing;
    insert into mtgcard(name) values ('Thousand-Year Storm') on conflict do nothing;
    insert into mtgcard(name) values ('Justice Strike') on conflict do nothing;
    insert into mtgcard(name) values ('Conclave Guildmage') on conflict do nothing;
    insert into mtgcard(name) values ('Vraska''s Stoneglare') on conflict do nothing;
    insert into mtgcard(name) values ('Barrier of Bones') on conflict do nothing;
    insert into mtgcard(name) values ('Swiftblade Vindicator') on conflict do nothing;
    insert into mtgcard(name) values ('Never Happened') on conflict do nothing;
    insert into mtgcard(name) values ('Island') on conflict do nothing;
    insert into mtgcard(name) values ('Selective Snare') on conflict do nothing;
    insert into mtgcard(name) values ('Temple Garden') on conflict do nothing;
    insert into mtgcard(name) values ('Garrison Sergeant') on conflict do nothing;
    insert into mtgcard(name) values ('March of the Multitudes') on conflict do nothing;
    insert into mtgcard(name) values ('Sworn Companions') on conflict do nothing;
    insert into mtgcard(name) values ('Siege Wurm') on conflict do nothing;
    insert into mtgcard(name) values ('Leapfrog') on conflict do nothing;
    insert into mtgcard(name) values ('Piston-Fist Cyclops') on conflict do nothing;
    insert into mtgcard(name) values ('Knight of Autumn') on conflict do nothing;
    insert into mtgcard(name) values ('Thief of Sanity') on conflict do nothing;
    insert into mtgcard(name) values ('Fearless Halberdier') on conflict do nothing;
    insert into mtgcard(name) values ('Generous Stray') on conflict do nothing;
    insert into mtgcard(name) values ('Healer''s Hawk') on conflict do nothing;
    insert into mtgcard(name) values ('Izzet Locket') on conflict do nothing;
    insert into mtgcard(name) values ('Erstwhile Trooper') on conflict do nothing;
    insert into mtgcard(name) values ('Veiled Shade') on conflict do nothing;
    insert into mtgcard(name) values ('Thoughtbound Phantasm') on conflict do nothing;
    insert into mtgcard(name) values ('Dimir Guildgate') on conflict do nothing;
    insert into mtgcard(name) values ('Golgari Raiders') on conflict do nothing;
    insert into mtgcard(name) values ('Wee Dragonauts') on conflict do nothing;
    insert into mtgcard(name) values ('Beacon Bolt') on conflict do nothing;
    insert into mtgcard(name) values ('Silent Dart') on conflict do nothing;
    insert into mtgcard(name) values ('Maximize Altitude') on conflict do nothing;
    insert into mtgcard(name) values ('Skyknight Legionnaire') on conflict do nothing;
    insert into mtgcard(name) values ('Pause for Reflection') on conflict do nothing;
    insert into mtgcard(name) values ('Emmara, Soul of the Accord') on conflict do nothing;
    insert into mtgcard(name) values ('Vraska, Regal Gorgon') on conflict do nothing;
    insert into mtgcard(name) values ('Status // Statue') on conflict do nothing;
    insert into mtgcard(name) values ('Wild Ceratok') on conflict do nothing;
    insert into mtgcard(name) values ('Urban Utopia') on conflict do nothing;
    insert into mtgcard(name) values ('House Guildmage') on conflict do nothing;
    insert into mtgcard(name) values ('Deadly Visit') on conflict do nothing;
    insert into mtgcard(name) values ('Inescapable Blaze') on conflict do nothing;
    insert into mtgcard(name) values ('Devkarin Dissident') on conflict do nothing;
    insert into mtgcard(name) values ('Kraul Swarm') on conflict do nothing;
    insert into mtgcard(name) values ('Intrusive Packbeast') on conflict do nothing;
    insert into mtgcard(name) values ('Roc Charger') on conflict do nothing;
    insert into mtgcard(name) values ('Integrity // Intervention') on conflict do nothing;
    insert into mtgcard(name) values ('Experimental Frenzy') on conflict do nothing;
    insert into mtgcard(name) values ('Gatekeeper Gargoyle') on conflict do nothing;
    insert into mtgcard(name) values ('Firemind''s Research') on conflict do nothing;
    insert into mtgcard(name) values ('Artful Takedown') on conflict do nothing;
    insert into mtgcard(name) values ('Risk Factor') on conflict do nothing;
    insert into mtgcard(name) values ('Barging Sergeant') on conflict do nothing;
    insert into mtgcard(name) values ('Nightveil Sprite') on conflict do nothing;
    insert into mtgcard(name) values ('Ochran Assassin') on conflict do nothing;
    insert into mtgcard(name) values ('Boros Challenger') on conflict do nothing;
    insert into mtgcard(name) values ('Wary Okapi') on conflict do nothing;
    insert into mtgcard(name) values ('Ironshell Beetle') on conflict do nothing;
    insert into mtgcard(name) values ('Capture Sphere') on conflict do nothing;
    insert into mtgcard(name) values ('Circuitous Route') on conflict do nothing;
    insert into mtgcard(name) values ('Vedalken Mesmerist') on conflict do nothing;
    insert into mtgcard(name) values ('Portcullis Vine') on conflict do nothing;
    insert into mtgcard(name) values ('Haazda Marshal') on conflict do nothing;
    insert into mtgcard(name) values ('Luminous Bonds') on conflict do nothing;
    insert into mtgcard(name) values ('Midnight Reaper') on conflict do nothing;
    insert into mtgcard(name) values ('Tajic, Legion''s Edge') on conflict do nothing;
    insert into mtgcard(name) values ('Legion Warboss') on conflict do nothing;
    insert into mtgcard(name) values ('Boros Locket') on conflict do nothing;
    insert into mtgcard(name) values ('Murmuring Mystic') on conflict do nothing;
    insert into mtgcard(name) values ('Truefire Captain') on conflict do nothing;
    insert into mtgcard(name) values ('Price of Fame') on conflict do nothing;
    insert into mtgcard(name) values ('Wall of Mist') on conflict do nothing;
    insert into mtgcard(name) values ('Pilfering Imp') on conflict do nothing;
    insert into mtgcard(name) values ('Wishcoin Crab') on conflict do nothing;
    insert into mtgcard(name) values ('Hellkite Whelp') on conflict do nothing;
    insert into mtgcard(name) values ('Sprouting Renewal') on conflict do nothing;
    insert into mtgcard(name) values ('Mephitic Vapors') on conflict do nothing;
    insert into mtgcard(name) values ('Demotion') on conflict do nothing;
    insert into mtgcard(name) values ('Disinformation Campaign') on conflict do nothing;
    insert into mtgcard(name) values ('Cosmotronic Wave') on conflict do nothing;
    insert into mtgcard(name) values ('Sinister Sabotage') on conflict do nothing;
    insert into mtgcard(name) values ('Selesnya Guildgate') on conflict do nothing;
    insert into mtgcard(name) values ('Bounty Agent') on conflict do nothing;
    insert into mtgcard(name) values ('Niv-Mizzet, Parun') on conflict do nothing;
    insert into mtgcard(name) values ('Arboretum Elemental') on conflict do nothing;
    insert into mtgcard(name) values ('Swathcutter Giant') on conflict do nothing;
    insert into mtgcard(name) values ('Electrostatic Field') on conflict do nothing;
    insert into mtgcard(name) values ('Charnel Troll') on conflict do nothing;
    insert into mtgcard(name) values ('Gruesome Menagerie') on conflict do nothing;
    insert into mtgcard(name) values ('League Guildmage') on conflict do nothing;
    insert into mtgcard(name) values ('Spinal Centipede') on conflict do nothing;
    insert into mtgcard(name) values ('Izoni, Thousand-Eyed') on conflict do nothing;
    insert into mtgcard(name) values ('Swarm Guildmage') on conflict do nothing;
    insert into mtgcard(name) values ('Arclight Phoenix') on conflict do nothing;
    insert into mtgcard(name) values ('Dimir Spybug') on conflict do nothing;
    insert into mtgcard(name) values ('Douser of Lights') on conflict do nothing;
    insert into mtgcard(name) values ('Thought Erasure') on conflict do nothing;
    insert into mtgcard(name) values ('Gateway Plaza') on conflict do nothing;
    insert into mtgcard(name) values ('Watery Grave') on conflict do nothing;
    insert into mtgcard(name) values ('Undercity Uprising') on conflict do nothing;
    insert into mtgcard(name) values ('Attendant of Vraska') on conflict do nothing;
    insert into mtgcard(name) values ('Mountain') on conflict do nothing;
    insert into mtgcard(name) values ('Street Riot') on conflict do nothing;
    insert into mtgcard(name) values ('Moodmark Painter') on conflict do nothing;
    insert into mtgcard(name) values ('Tenth District Guard') on conflict do nothing;
    insert into mtgcard(name) values ('Chance for Glory') on conflict do nothing;
    insert into mtgcard(name) values ('Golgari Guildgate') on conflict do nothing;
    insert into mtgcard(name) values ('Plaguecrafter') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Cratermaker') on conflict do nothing;
    insert into mtgcard(name) values ('Wand of Vertebrae') on conflict do nothing;
    insert into mtgcard(name) values ('Maniacal Rage') on conflict do nothing;
    insert into mtgcard(name) values ('Camaraderie') on conflict do nothing;
    insert into mtgcard(name) values ('Connive // Concoct') on conflict do nothing;
    insert into mtgcard(name) values ('Hunted Witness') on conflict do nothing;
    insert into mtgcard(name) values ('Sumala Woodshaper') on conflict do nothing;
    insert into mtgcard(name) values ('Sure Strike') on conflict do nothing;
    insert into mtgcard(name) values ('Beamsplitter Mage') on conflict do nothing;
    insert into mtgcard(name) values ('Assassin''s Trophy') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Banneret') on conflict do nothing;
    insert into mtgcard(name) values ('Join Shields') on conflict do nothing;
    insert into mtgcard(name) values ('Response // Resurgence') on conflict do nothing;
    insert into mtgcard(name) values ('Venerated Loxodon') on conflict do nothing;
    insert into mtgcard(name) values ('Unmoored Ego') on conflict do nothing;
    insert into mtgcard(name) values ('Boros Guildgate') on conflict do nothing;
    insert into mtgcard(name) values ('Enhanced Surveillance') on conflict do nothing;
    insert into mtgcard(name) values ('Necrotic Wound') on conflict do nothing;
    insert into mtgcard(name) values ('Blade Instructor') on conflict do nothing;
    insert into mtgcard(name) values ('Citywatch Sphinx') on conflict do nothing;
    insert into mtgcard(name) values ('Unexplained Disappearance') on conflict do nothing;
    insert into mtgcard(name) values ('Conclave Tribunal') on conflict do nothing;
    insert into mtgcard(name) values ('Beast Whisperer') on conflict do nothing;
    insert into mtgcard(name) values ('Vivid Revival') on conflict do nothing;
    insert into mtgcard(name) values ('Hammer Dropper') on conflict do nothing;
    insert into mtgcard(name) values ('Dream Eater') on conflict do nothing;
    insert into mtgcard(name) values ('Dead Weight') on conflict do nothing;
    insert into mtgcard(name) values ('Ral''s Dispersal') on conflict do nothing;
    insert into mtgcard(name) values ('Light of the Legion') on conflict do nothing;
    insert into mtgcard(name) values ('Vraska, Golgari Queen') on conflict do nothing;
    insert into mtgcard(name) values ('Narcomoeba') on conflict do nothing;
    insert into mtgcard(name) values ('Sunhome Stalwart') on conflict do nothing;
    insert into mtgcard(name) values ('Precision Bolt') on conflict do nothing;
    insert into mtgcard(name) values ('Mnemonic Betrayal') on conflict do nothing;
    insert into mtgcard(name) values ('Fresh-Faced Recruit') on conflict do nothing;
    insert into mtgcard(name) values ('Aurelia, Exemplar of Justice') on conflict do nothing;
    insert into mtgcard(name) values ('Passwall Adept') on conflict do nothing;
    insert into mtgcard(name) values ('Citywide Bust') on conflict do nothing;
    insert into mtgcard(name) values ('Dazzling Lights') on conflict do nothing;
    insert into mtgcard(name) values ('Flight of Equenauts') on conflict do nothing;
    insert into mtgcard(name) values ('Discovery // Dispersal') on conflict do nothing;
    insert into mtgcard(name) values ('Assure // Assemble') on conflict do nothing;
    insert into mtgcard(name) values ('Ral''s Staticaster') on conflict do nothing;
    insert into mtgcard(name) values ('Child of Night') on conflict do nothing;
    insert into mtgcard(name) values ('Legion Guildmage') on conflict do nothing;
    insert into mtgcard(name) values ('Skyline Scout') on conflict do nothing;
    insert into mtgcard(name) values ('Vicious Rumors') on conflict do nothing;
    insert into mtgcard(name) values ('Lotleth Giant') on conflict do nothing;
    insert into mtgcard(name) values ('Take Heart') on conflict do nothing;
    insert into mtgcard(name) values ('Fire Urchin') on conflict do nothing;
    insert into mtgcard(name) values ('Righteous Blow') on conflict do nothing;
    insert into mtgcard(name) values ('Affectionate Indrik') on conflict do nothing;
    insert into mtgcard(name) values ('Worldsoul Colossus') on conflict do nothing;
    insert into mtgcard(name) values ('Kraul Foragers') on conflict do nothing;
    insert into mtgcard(name) values ('Rampaging Monument') on conflict do nothing;
    insert into mtgcard(name) values ('Vigorspore Wurm') on conflict do nothing;
    insert into mtgcard(name) values ('Conclave Cavalier') on conflict do nothing;
    insert into mtgcard(name) values ('Sacred Foundry') on conflict do nothing;
    insert into mtgcard(name) values ('Steam Vents') on conflict do nothing;
    insert into mtgcard(name) values ('Plains') on conflict do nothing;
    insert into mtgcard(name) values ('Molderhulk') on conflict do nothing;
    insert into mtgcard(name) values ('Ledev Champion') on conflict do nothing;
    insert into mtgcard(name) values ('Severed Strands') on conflict do nothing;
    insert into mtgcard(name) values ('Swamp') on conflict do nothing;
    insert into mtgcard(name) values ('Ornery Goblin') on conflict do nothing;
    insert into mtgcard(name) values ('Pelt Collector') on conflict do nothing;
    insert into mtgcard(name) values ('Hired Poisoner') on conflict do nothing;
    insert into mtgcard(name) values ('Drowned Secrets') on conflict do nothing;
    insert into mtgcard(name) values ('Crushing Canopy') on conflict do nothing;
    insert into mtgcard(name) values ('District Guide') on conflict do nothing;
    insert into mtgcard(name) values ('Pitiless Gorgon') on conflict do nothing;
    insert into mtgcard(name) values ('Erratic Cyclops') on conflict do nothing;
    insert into mtgcard(name) values ('Command the Storm') on conflict do nothing;
    insert into mtgcard(name) values ('Ral, Caller of Storms') on conflict do nothing;
    insert into mtgcard(name) values ('Ral, Izzet Viceroy') on conflict do nothing;
    insert into mtgcard(name) values ('Muse Drake') on conflict do nothing;
    insert into mtgcard(name) values ('Radical Idea') on conflict do nothing;
    insert into mtgcard(name) values ('Guildmages'' Forum') on conflict do nothing;
    insert into mtgcard(name) values ('Ledev Guardian') on conflict do nothing;
    insert into mtgcard(name) values ('Sonic Assault') on conflict do nothing;
    insert into mtgcard(name) values ('Collar the Culprit') on conflict do nothing;
    insert into mtgcard(name) values ('Inspiring Unicorn') on conflict do nothing;
    insert into mtgcard(name) values ('Rosemane Centaur') on conflict do nothing;
    insert into mtgcard(name) values ('Dawn of Hope') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Locksmith') on conflict do nothing;
    insert into mtgcard(name) values ('Might of the Masses') on conflict do nothing;
    insert into mtgcard(name) values ('Torch Courier') on conflict do nothing;
    insert into mtgcard(name) values ('Hypothesizzle') on conflict do nothing;
    insert into mtgcard(name) values ('Forest') on conflict do nothing;
    insert into mtgcard(name) values ('Runaway Steam-Kin') on conflict do nothing;
    insert into mtgcard(name) values ('Guild Summit') on conflict do nothing;
    insert into mtgcard(name) values ('Divine Visitation') on conflict do nothing;
    insert into mtgcard(name) values ('Rhizome Lurcher') on conflict do nothing;
    insert into mtgcard(name) values ('Dimir Locket') on conflict do nothing;
    insert into mtgcard(name) values ('Expansion // Explosion') on conflict do nothing;
    insert into mtgcard(name) values ('Golgari Findbroker') on conflict do nothing;
    insert into mtgcard(name) values ('Omnispell Adept') on conflict do nothing;
    insert into mtgcard(name) values ('Glaive of the Guildpact') on conflict do nothing;
    insert into mtgcard(name) values ('Candlelight Vigil') on conflict do nothing;
    insert into mtgcard(name) values ('Bounty of Might') on conflict do nothing;
    insert into mtgcard(name) values ('Burglar Rat') on conflict do nothing;
    insert into mtgcard(name) values ('Selesnya Locket') on conflict do nothing;
    insert into mtgcard(name) values ('Chromatic Lantern') on conflict do nothing;
    insert into mtgcard(name) values ('Maximize Velocity') on conflict do nothing;
    insert into mtgcard(name) values ('Wojek Bodyguard') on conflict do nothing;
    insert into mtgcard(name) values ('Parhelion Patrol') on conflict do nothing;
    insert into mtgcard(name) values ('Vernadi Shieldmate') on conflict do nothing;
    insert into mtgcard(name) values ('Overgrown Tomb') on conflict do nothing;
    insert into mtgcard(name) values ('Crackling Drake') on conflict do nothing;
    insert into mtgcard(name) values ('Quasiduplicate') on conflict do nothing;
    insert into mtgcard(name) values ('Darkblade Agent') on conflict do nothing;
    insert into mtgcard(name) values ('Ionize') on conflict do nothing;
    insert into mtgcard(name) values ('Notion Rain') on conflict do nothing;
    insert into mtgcard(name) values ('Creeping Chill') on conflict do nothing;
    insert into mtgcard(name) values ('Impervious Greatwurm') on conflict do nothing;
    insert into mtgcard(name) values ('Lava Coil') on conflict do nothing;
    insert into mtgcard(name) values ('Whisper Agent') on conflict do nothing;
    insert into mtgcard(name) values ('Kraul Harpooner') on conflict do nothing;
    insert into mtgcard(name) values ('Grappling Sundew') on conflict do nothing;
    insert into mtgcard(name) values ('Smelt-Ward Minotaur') on conflict do nothing;
    insert into mtgcard(name) values ('Gird for Battle') on conflict do nothing;
    insert into mtgcard(name) values ('Trostani Discordant') on conflict do nothing;
    insert into mtgcard(name) values ('Etrata, the Silencer') on conflict do nothing;
    insert into mtgcard(name) values ('Whispering Snitch') on conflict do nothing;
    insert into mtgcard(name) values ('Watcher in the Mist') on conflict do nothing;
    insert into mtgcard(name) values ('Hitchclaw Recluse') on conflict do nothing;
    insert into mtgcard(name) values ('Flower // Flourish') on conflict do nothing;
