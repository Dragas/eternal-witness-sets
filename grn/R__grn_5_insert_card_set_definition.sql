insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Blood Operative'),
    (select id from sets where short_name = 'grn'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disdainful Stroke'),
    (select id from sets where short_name = 'grn'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Book Devourer'),
    (select id from sets where short_name = 'grn'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Informant'),
    (select id from sets where short_name = 'grn'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mission Briefing'),
    (select id from sets where short_name = 'grn'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Invert // Invent'),
    (select id from sets where short_name = 'grn'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gravitic Punch'),
    (select id from sets where short_name = 'grn'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Underrealm Lich'),
    (select id from sets where short_name = 'grn'),
    '211',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Glowspore Shaman'),
    (select id from sets where short_name = 'grn'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doom Whisperer'),
    (select id from sets where short_name = 'grn'),
    '69',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goblin Electromancer'),
    (select id from sets where short_name = 'grn'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hatchery Spider'),
    (select id from sets where short_name = 'grn'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chemister''s Insight'),
    (select id from sets where short_name = 'grn'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Centaur Peacemaker'),
    (select id from sets where short_name = 'grn'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pack''s Favor'),
    (select id from sets where short_name = 'grn'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kraul Raider'),
    (select id from sets where short_name = 'grn'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crush Contraband'),
    (select id from sets where short_name = 'grn'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chamber Sentry'),
    (select id from sets where short_name = 'grn'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Direct Current'),
    (select id from sets where short_name = 'grn'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prey Upon'),
    (select id from sets where short_name = 'grn'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightveil Predator'),
    (select id from sets where short_name = 'grn'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rubblebelt Boar'),
    (select id from sets where short_name = 'grn'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golgari Locket'),
    (select id from sets where short_name = 'grn'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deafening Clarion'),
    (select id from sets where short_name = 'grn'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Find // Finality'),
    (select id from sets where short_name = 'grn'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loxodon Restorer'),
    (select id from sets where short_name = 'grn'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Guildgate'),
    (select id from sets where short_name = 'grn'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bartizan Bats'),
    (select id from sets where short_name = 'grn'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Devious Cover-Up'),
    (select id from sets where short_name = 'grn'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lazav, the Multifarious'),
    (select id from sets where short_name = 'grn'),
    '184',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nullhide Ferox'),
    (select id from sets where short_name = 'grn'),
    '138',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Undercity Necrolisk'),
    (select id from sets where short_name = 'grn'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ritual of Soot'),
    (select id from sets where short_name = 'grn'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mausoleum Secrets'),
    (select id from sets where short_name = 'grn'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thousand-Year Storm'),
    (select id from sets where short_name = 'grn'),
    '207',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Justice Strike'),
    (select id from sets where short_name = 'grn'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Conclave Guildmage'),
    (select id from sets where short_name = 'grn'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vraska''s Stoneglare'),
    (select id from sets where short_name = 'grn'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barrier of Bones'),
    (select id from sets where short_name = 'grn'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swiftblade Vindicator'),
    (select id from sets where short_name = 'grn'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Never Happened'),
    (select id from sets where short_name = 'grn'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'grn'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selective Snare'),
    (select id from sets where short_name = 'grn'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Temple Garden'),
    (select id from sets where short_name = 'grn'),
    '258',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Garrison Sergeant'),
    (select id from sets where short_name = 'grn'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'March of the Multitudes'),
    (select id from sets where short_name = 'grn'),
    '188',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sworn Companions'),
    (select id from sets where short_name = 'grn'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Siege Wurm'),
    (select id from sets where short_name = 'grn'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leapfrog'),
    (select id from sets where short_name = 'grn'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Piston-Fist Cyclops'),
    (select id from sets where short_name = 'grn'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of Autumn'),
    (select id from sets where short_name = 'grn'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thief of Sanity'),
    (select id from sets where short_name = 'grn'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fearless Halberdier'),
    (select id from sets where short_name = 'grn'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Generous Stray'),
    (select id from sets where short_name = 'grn'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Healer''s Hawk'),
    (select id from sets where short_name = 'grn'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Locket'),
    (select id from sets where short_name = 'grn'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erstwhile Trooper'),
    (select id from sets where short_name = 'grn'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Veiled Shade'),
    (select id from sets where short_name = 'grn'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thoughtbound Phantasm'),
    (select id from sets where short_name = 'grn'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Guildgate'),
    (select id from sets where short_name = 'grn'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golgari Raiders'),
    (select id from sets where short_name = 'grn'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wee Dragonauts'),
    (select id from sets where short_name = 'grn'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beacon Bolt'),
    (select id from sets where short_name = 'grn'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silent Dart'),
    (select id from sets where short_name = 'grn'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maximize Altitude'),
    (select id from sets where short_name = 'grn'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyknight Legionnaire'),
    (select id from sets where short_name = 'grn'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pause for Reflection'),
    (select id from sets where short_name = 'grn'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emmara, Soul of the Accord'),
    (select id from sets where short_name = 'grn'),
    '168',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vraska, Regal Gorgon'),
    (select id from sets where short_name = 'grn'),
    '269',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Status // Statue'),
    (select id from sets where short_name = 'grn'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Ceratok'),
    (select id from sets where short_name = 'grn'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urban Utopia'),
    (select id from sets where short_name = 'grn'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'House Guildmage'),
    (select id from sets where short_name = 'grn'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deadly Visit'),
    (select id from sets where short_name = 'grn'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inescapable Blaze'),
    (select id from sets where short_name = 'grn'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Devkarin Dissident'),
    (select id from sets where short_name = 'grn'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kraul Swarm'),
    (select id from sets where short_name = 'grn'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Intrusive Packbeast'),
    (select id from sets where short_name = 'grn'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Roc Charger'),
    (select id from sets where short_name = 'grn'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Integrity // Intervention'),
    (select id from sets where short_name = 'grn'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Experimental Frenzy'),
    (select id from sets where short_name = 'grn'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gatekeeper Gargoyle'),
    (select id from sets where short_name = 'grn'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firemind''s Research'),
    (select id from sets where short_name = 'grn'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Artful Takedown'),
    (select id from sets where short_name = 'grn'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Risk Factor'),
    (select id from sets where short_name = 'grn'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barging Sergeant'),
    (select id from sets where short_name = 'grn'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightveil Sprite'),
    (select id from sets where short_name = 'grn'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ochran Assassin'),
    (select id from sets where short_name = 'grn'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Challenger'),
    (select id from sets where short_name = 'grn'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wary Okapi'),
    (select id from sets where short_name = 'grn'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ironshell Beetle'),
    (select id from sets where short_name = 'grn'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Capture Sphere'),
    (select id from sets where short_name = 'grn'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circuitous Route'),
    (select id from sets where short_name = 'grn'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vedalken Mesmerist'),
    (select id from sets where short_name = 'grn'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Portcullis Vine'),
    (select id from sets where short_name = 'grn'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Haazda Marshal'),
    (select id from sets where short_name = 'grn'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Luminous Bonds'),
    (select id from sets where short_name = 'grn'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Midnight Reaper'),
    (select id from sets where short_name = 'grn'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tajic, Legion''s Edge'),
    (select id from sets where short_name = 'grn'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Legion Warboss'),
    (select id from sets where short_name = 'grn'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Locket'),
    (select id from sets where short_name = 'grn'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murmuring Mystic'),
    (select id from sets where short_name = 'grn'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Truefire Captain'),
    (select id from sets where short_name = 'grn'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Price of Fame'),
    (select id from sets where short_name = 'grn'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Mist'),
    (select id from sets where short_name = 'grn'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pilfering Imp'),
    (select id from sets where short_name = 'grn'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wishcoin Crab'),
    (select id from sets where short_name = 'grn'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hellkite Whelp'),
    (select id from sets where short_name = 'grn'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sprouting Renewal'),
    (select id from sets where short_name = 'grn'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mephitic Vapors'),
    (select id from sets where short_name = 'grn'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demotion'),
    (select id from sets where short_name = 'grn'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disinformation Campaign'),
    (select id from sets where short_name = 'grn'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cosmotronic Wave'),
    (select id from sets where short_name = 'grn'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sinister Sabotage'),
    (select id from sets where short_name = 'grn'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selesnya Guildgate'),
    (select id from sets where short_name = 'grn'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bounty Agent'),
    (select id from sets where short_name = 'grn'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Niv-Mizzet, Parun'),
    (select id from sets where short_name = 'grn'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arboretum Elemental'),
    (select id from sets where short_name = 'grn'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swathcutter Giant'),
    (select id from sets where short_name = 'grn'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Electrostatic Field'),
    (select id from sets where short_name = 'grn'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Charnel Troll'),
    (select id from sets where short_name = 'grn'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruesome Menagerie'),
    (select id from sets where short_name = 'grn'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'League Guildmage'),
    (select id from sets where short_name = 'grn'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spinal Centipede'),
    (select id from sets where short_name = 'grn'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izoni, Thousand-Eyed'),
    (select id from sets where short_name = 'grn'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swarm Guildmage'),
    (select id from sets where short_name = 'grn'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arclight Phoenix'),
    (select id from sets where short_name = 'grn'),
    '91',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dimir Spybug'),
    (select id from sets where short_name = 'grn'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Douser of Lights'),
    (select id from sets where short_name = 'grn'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thought Erasure'),
    (select id from sets where short_name = 'grn'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gateway Plaza'),
    (select id from sets where short_name = 'grn'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Watery Grave'),
    (select id from sets where short_name = 'grn'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Undercity Uprising'),
    (select id from sets where short_name = 'grn'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Attendant of Vraska'),
    (select id from sets where short_name = 'grn'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'grn'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Street Riot'),
    (select id from sets where short_name = 'grn'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moodmark Painter'),
    (select id from sets where short_name = 'grn'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tenth District Guard'),
    (select id from sets where short_name = 'grn'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chance for Glory'),
    (select id from sets where short_name = 'grn'),
    '159',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Golgari Guildgate'),
    (select id from sets where short_name = 'grn'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plaguecrafter'),
    (select id from sets where short_name = 'grn'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Cratermaker'),
    (select id from sets where short_name = 'grn'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wand of Vertebrae'),
    (select id from sets where short_name = 'grn'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maniacal Rage'),
    (select id from sets where short_name = 'grn'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Camaraderie'),
    (select id from sets where short_name = 'grn'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Connive // Concoct'),
    (select id from sets where short_name = 'grn'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hunted Witness'),
    (select id from sets where short_name = 'grn'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sumala Woodshaper'),
    (select id from sets where short_name = 'grn'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sure Strike'),
    (select id from sets where short_name = 'grn'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beamsplitter Mage'),
    (select id from sets where short_name = 'grn'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Assassin''s Trophy'),
    (select id from sets where short_name = 'grn'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Banneret'),
    (select id from sets where short_name = 'grn'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Join Shields'),
    (select id from sets where short_name = 'grn'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Response // Resurgence'),
    (select id from sets where short_name = 'grn'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Venerated Loxodon'),
    (select id from sets where short_name = 'grn'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unmoored Ego'),
    (select id from sets where short_name = 'grn'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Guildgate'),
    (select id from sets where short_name = 'grn'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enhanced Surveillance'),
    (select id from sets where short_name = 'grn'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necrotic Wound'),
    (select id from sets where short_name = 'grn'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blade Instructor'),
    (select id from sets where short_name = 'grn'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Citywatch Sphinx'),
    (select id from sets where short_name = 'grn'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unexplained Disappearance'),
    (select id from sets where short_name = 'grn'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conclave Tribunal'),
    (select id from sets where short_name = 'grn'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beast Whisperer'),
    (select id from sets where short_name = 'grn'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivid Revival'),
    (select id from sets where short_name = 'grn'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hammer Dropper'),
    (select id from sets where short_name = 'grn'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dream Eater'),
    (select id from sets where short_name = 'grn'),
    '38',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dead Weight'),
    (select id from sets where short_name = 'grn'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ral''s Dispersal'),
    (select id from sets where short_name = 'grn'),
    '266',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Light of the Legion'),
    (select id from sets where short_name = 'grn'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vraska, Golgari Queen'),
    (select id from sets where short_name = 'grn'),
    '213',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Narcomoeba'),
    (select id from sets where short_name = 'grn'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunhome Stalwart'),
    (select id from sets where short_name = 'grn'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Precision Bolt'),
    (select id from sets where short_name = 'grn'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mnemonic Betrayal'),
    (select id from sets where short_name = 'grn'),
    '189',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fresh-Faced Recruit'),
    (select id from sets where short_name = 'grn'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aurelia, Exemplar of Justice'),
    (select id from sets where short_name = 'grn'),
    '153',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Passwall Adept'),
    (select id from sets where short_name = 'grn'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Citywide Bust'),
    (select id from sets where short_name = 'grn'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dazzling Lights'),
    (select id from sets where short_name = 'grn'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flight of Equenauts'),
    (select id from sets where short_name = 'grn'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Discovery // Dispersal'),
    (select id from sets where short_name = 'grn'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Assure // Assemble'),
    (select id from sets where short_name = 'grn'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ral''s Staticaster'),
    (select id from sets where short_name = 'grn'),
    '268',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Child of Night'),
    (select id from sets where short_name = 'grn'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Legion Guildmage'),
    (select id from sets where short_name = 'grn'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyline Scout'),
    (select id from sets where short_name = 'grn'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vicious Rumors'),
    (select id from sets where short_name = 'grn'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lotleth Giant'),
    (select id from sets where short_name = 'grn'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Take Heart'),
    (select id from sets where short_name = 'grn'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Urchin'),
    (select id from sets where short_name = 'grn'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Righteous Blow'),
    (select id from sets where short_name = 'grn'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Affectionate Indrik'),
    (select id from sets where short_name = 'grn'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Worldsoul Colossus'),
    (select id from sets where short_name = 'grn'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kraul Foragers'),
    (select id from sets where short_name = 'grn'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampaging Monument'),
    (select id from sets where short_name = 'grn'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vigorspore Wurm'),
    (select id from sets where short_name = 'grn'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conclave Cavalier'),
    (select id from sets where short_name = 'grn'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Guildgate'),
    (select id from sets where short_name = 'grn'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sacred Foundry'),
    (select id from sets where short_name = 'grn'),
    '254',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steam Vents'),
    (select id from sets where short_name = 'grn'),
    '257',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'grn'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molderhulk'),
    (select id from sets where short_name = 'grn'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ledev Champion'),
    (select id from sets where short_name = 'grn'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Severed Strands'),
    (select id from sets where short_name = 'grn'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'grn'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ornery Goblin'),
    (select id from sets where short_name = 'grn'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pelt Collector'),
    (select id from sets where short_name = 'grn'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hired Poisoner'),
    (select id from sets where short_name = 'grn'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drowned Secrets'),
    (select id from sets where short_name = 'grn'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crushing Canopy'),
    (select id from sets where short_name = 'grn'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'District Guide'),
    (select id from sets where short_name = 'grn'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pitiless Gorgon'),
    (select id from sets where short_name = 'grn'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erratic Cyclops'),
    (select id from sets where short_name = 'grn'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Command the Storm'),
    (select id from sets where short_name = 'grn'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ral, Caller of Storms'),
    (select id from sets where short_name = 'grn'),
    '265',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ral, Izzet Viceroy'),
    (select id from sets where short_name = 'grn'),
    '195',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Boros Guildgate'),
    (select id from sets where short_name = 'grn'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Muse Drake'),
    (select id from sets where short_name = 'grn'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Radical Idea'),
    (select id from sets where short_name = 'grn'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guildmages'' Forum'),
    (select id from sets where short_name = 'grn'),
    '250',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ledev Guardian'),
    (select id from sets where short_name = 'grn'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sonic Assault'),
    (select id from sets where short_name = 'grn'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Collar the Culprit'),
    (select id from sets where short_name = 'grn'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inspiring Unicorn'),
    (select id from sets where short_name = 'grn'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rosemane Centaur'),
    (select id from sets where short_name = 'grn'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dawn of Hope'),
    (select id from sets where short_name = 'grn'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Locksmith'),
    (select id from sets where short_name = 'grn'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Might of the Masses'),
    (select id from sets where short_name = 'grn'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Torch Courier'),
    (select id from sets where short_name = 'grn'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hypothesizzle'),
    (select id from sets where short_name = 'grn'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'grn'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runaway Steam-Kin'),
    (select id from sets where short_name = 'grn'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guild Summit'),
    (select id from sets where short_name = 'grn'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Divine Visitation'),
    (select id from sets where short_name = 'grn'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rhizome Lurcher'),
    (select id from sets where short_name = 'grn'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dimir Locket'),
    (select id from sets where short_name = 'grn'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Expansion // Explosion'),
    (select id from sets where short_name = 'grn'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Findbroker'),
    (select id from sets where short_name = 'grn'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Omnispell Adept'),
    (select id from sets where short_name = 'grn'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glaive of the Guildpact'),
    (select id from sets where short_name = 'grn'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Candlelight Vigil'),
    (select id from sets where short_name = 'grn'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bounty of Might'),
    (select id from sets where short_name = 'grn'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burglar Rat'),
    (select id from sets where short_name = 'grn'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Locket'),
    (select id from sets where short_name = 'grn'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chromatic Lantern'),
    (select id from sets where short_name = 'grn'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maximize Velocity'),
    (select id from sets where short_name = 'grn'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wojek Bodyguard'),
    (select id from sets where short_name = 'grn'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Guildgate'),
    (select id from sets where short_name = 'grn'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Parhelion Patrol'),
    (select id from sets where short_name = 'grn'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vernadi Shieldmate'),
    (select id from sets where short_name = 'grn'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overgrown Tomb'),
    (select id from sets where short_name = 'grn'),
    '253',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crackling Drake'),
    (select id from sets where short_name = 'grn'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quasiduplicate'),
    (select id from sets where short_name = 'grn'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Guildgate'),
    (select id from sets where short_name = 'grn'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darkblade Agent'),
    (select id from sets where short_name = 'grn'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ionize'),
    (select id from sets where short_name = 'grn'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Notion Rain'),
    (select id from sets where short_name = 'grn'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Creeping Chill'),
    (select id from sets where short_name = 'grn'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Impervious Greatwurm'),
    (select id from sets where short_name = 'grn'),
    '273',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lava Coil'),
    (select id from sets where short_name = 'grn'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whisper Agent'),
    (select id from sets where short_name = 'grn'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kraul Harpooner'),
    (select id from sets where short_name = 'grn'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grappling Sundew'),
    (select id from sets where short_name = 'grn'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smelt-Ward Minotaur'),
    (select id from sets where short_name = 'grn'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gird for Battle'),
    (select id from sets where short_name = 'grn'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trostani Discordant'),
    (select id from sets where short_name = 'grn'),
    '208',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Etrata, the Silencer'),
    (select id from sets where short_name = 'grn'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whispering Snitch'),
    (select id from sets where short_name = 'grn'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Watcher in the Mist'),
    (select id from sets where short_name = 'grn'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Guildgate'),
    (select id from sets where short_name = 'grn'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hitchclaw Recluse'),
    (select id from sets where short_name = 'grn'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flower // Flourish'),
    (select id from sets where short_name = 'grn'),
    '226',
    'uncommon'
) 
 on conflict do nothing;
