    insert into mtgcard(name) values ('Ooze') on conflict do nothing;
    insert into mtgcard(name) values ('Bird') on conflict do nothing;
    insert into mtgcard(name) values ('Beast') on conflict do nothing;
    insert into mtgcard(name) values ('Zombie') on conflict do nothing;
    insert into mtgcard(name) values ('Avatar') on conflict do nothing;
