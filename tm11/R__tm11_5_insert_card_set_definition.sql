insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ooze'),
    (select id from sets where short_name = 'tm11'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird'),
    (select id from sets where short_name = 'tm11'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tm11'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tm11'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avatar'),
    (select id from sets where short_name = 'tm11'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ooze'),
    (select id from sets where short_name = 'tm11'),
    '5',
    'common'
) 
 on conflict do nothing;
