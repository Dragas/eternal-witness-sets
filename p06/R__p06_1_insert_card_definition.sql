    insert into mtgcard(name) values ('Zombify') on conflict do nothing;
    insert into mtgcard(name) values ('Putrefy') on conflict do nothing;
    insert into mtgcard(name) values ('Lightning Helix') on conflict do nothing;
    insert into mtgcard(name) values ('Hinder') on conflict do nothing;
    insert into mtgcard(name) values ('Hypnotic Specter') on conflict do nothing;
    insert into mtgcard(name) values ('Giant Growth') on conflict do nothing;
    insert into mtgcard(name) values ('Pyroclasm') on conflict do nothing;
