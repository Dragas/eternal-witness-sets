insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Zombify'),
    (select id from sets where short_name = 'p06'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Putrefy'),
    (select id from sets where short_name = 'p06'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Helix'),
    (select id from sets where short_name = 'p06'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hinder'),
    (select id from sets where short_name = 'p06'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Specter'),
    (select id from sets where short_name = 'p06'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'p06'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyroclasm'),
    (select id from sets where short_name = 'p06'),
    '3',
    'rare'
) 
 on conflict do nothing;
