    insert into mtgcard(name) values ('Worm') on conflict do nothing;
    insert into mtgcard(name) values ('Beast') on conflict do nothing;
    insert into mtgcard(name) values ('Goat') on conflict do nothing;
    insert into mtgcard(name) values ('Spirit') on conflict do nothing;
    insert into mtgcard(name) values ('Elemental') on conflict do nothing;
    insert into mtgcard(name) values ('Bird') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Soldier') on conflict do nothing;
