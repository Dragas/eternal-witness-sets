insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Worm'),
    (select id from sets where short_name = 'teve'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'teve'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goat'),
    (select id from sets where short_name = 'teve'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'teve'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'teve'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird'),
    (select id from sets where short_name = 'teve'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Soldier'),
    (select id from sets where short_name = 'teve'),
    '7',
    'common'
) 
 on conflict do nothing;
