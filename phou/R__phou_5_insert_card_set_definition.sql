insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Abandoned Sarcophagus'),
    (select id from sets where short_name = 'phou'),
    '158s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Earthshaker Khenra'),
    (select id from sets where short_name = 'phou'),
    '90s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hour of Eternity'),
    (select id from sets where short_name = 'phou'),
    '36s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hostile Desert'),
    (select id from sets where short_name = 'phou'),
    '178s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kefnet''s Last Word'),
    (select id from sets where short_name = 'phou'),
    '39s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Champion of Wits'),
    (select id from sets where short_name = 'phou'),
    '31s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hazoret''s Undying Fury'),
    (select id from sets where short_name = 'phou'),
    '96s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reason // Believe'),
    (select id from sets where short_name = 'phou'),
    '154s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leave // Chance'),
    (select id from sets where short_name = 'phou'),
    '153s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pride Sovereign'),
    (select id from sets where short_name = 'phou'),
    '126s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Resilient Khenra'),
    (select id from sets where short_name = 'phou'),
    '131s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chaos Maw'),
    (select id from sets where short_name = 'phou'),
    '87s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ammit Eternal'),
    (select id from sets where short_name = 'phou'),
    '57s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scavenger Grounds'),
    (select id from sets where short_name = 'phou'),
    '182s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abrade'),
    (select id from sets where short_name = 'phou'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hollow One'),
    (select id from sets where short_name = 'phou'),
    '163s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Apocalypse Demon'),
    (select id from sets where short_name = 'phou'),
    '58s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oketra''s Last Mercy'),
    (select id from sets where short_name = 'phou'),
    '18s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Djeru, With Eyes Open'),
    (select id from sets where short_name = 'phou'),
    '10s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Adorned Pouncer'),
    (select id from sets where short_name = 'phou'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unesh, Criosphinx Sovereign'),
    (select id from sets where short_name = 'phou'),
    '52s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Neheb, the Eternal'),
    (select id from sets where short_name = 'phou'),
    '104s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ramunap Excavator'),
    (select id from sets where short_name = 'phou'),
    '129s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Scarab God'),
    (select id from sets where short_name = 'phou'),
    '145s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wildfire Eternal'),
    (select id from sets where short_name = 'phou'),
    '109s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hour of Revelation'),
    (select id from sets where short_name = 'phou'),
    '15p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hour of Promise'),
    (select id from sets where short_name = 'phou'),
    '120s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreamstealer'),
    (select id from sets where short_name = 'phou'),
    '63s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overwhelming Splendor'),
    (select id from sets where short_name = 'phou'),
    '19s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Angel of Condemnation'),
    (select id from sets where short_name = 'phou'),
    '3s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uncage the Menagerie'),
    (select id from sets where short_name = 'phou'),
    '137s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bontu''s Last Reckoning'),
    (select id from sets where short_name = 'phou'),
    '60s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fraying Sanity'),
    (select id from sets where short_name = 'phou'),
    '35s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ramunap Hydra'),
    (select id from sets where short_name = 'phou'),
    '130s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirage Mirror'),
    (select id from sets where short_name = 'phou'),
    '165s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Razaketh, the Foulblooded'),
    (select id from sets where short_name = 'phou'),
    '73s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Grind // Dust'),
    (select id from sets where short_name = 'phou'),
    '155s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Driven // Despair'),
    (select id from sets where short_name = 'phou'),
    '157s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imminent Doom'),
    (select id from sets where short_name = 'phou'),
    '98s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rhonas''s Last Stand'),
    (select id from sets where short_name = 'phou'),
    '132s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Locust God'),
    (select id from sets where short_name = 'phou'),
    '139s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hour of Revelation'),
    (select id from sets where short_name = 'phou'),
    '15s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wildfire Eternal'),
    (select id from sets where short_name = 'phou'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Refuse // Cooperate'),
    (select id from sets where short_name = 'phou'),
    '156s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Scorpion God'),
    (select id from sets where short_name = 'phou'),
    '146s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Crested Sunmare'),
    (select id from sets where short_name = 'phou'),
    '6s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Endless Sands'),
    (select id from sets where short_name = 'phou'),
    '176s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hour of Devastation'),
    (select id from sets where short_name = 'phou'),
    '97s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swarm Intelligence'),
    (select id from sets where short_name = 'phou'),
    '50s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'God-Pharaoh''s Gift'),
    (select id from sets where short_name = 'phou'),
    '161s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Majestic Myriarch'),
    (select id from sets where short_name = 'phou'),
    '122s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Samut, the Tested'),
    (select id from sets where short_name = 'phou'),
    '144s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mirage Mirror'),
    (select id from sets where short_name = 'phou'),
    '165p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Adorned Pouncer'),
    (select id from sets where short_name = 'phou'),
    '2s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, God-Pharaoh'),
    (select id from sets where short_name = 'phou'),
    '140s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ramunap Excavator'),
    (select id from sets where short_name = 'phou'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Solemnity'),
    (select id from sets where short_name = 'phou'),
    '22s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nimble Obstructionist'),
    (select id from sets where short_name = 'phou'),
    '40s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hour of Glory'),
    (select id from sets where short_name = 'phou'),
    '65s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Torment of Hailfire'),
    (select id from sets where short_name = 'phou'),
    '77s',
    'rare'
) 
 on conflict do nothing;
