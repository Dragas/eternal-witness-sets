insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tavr'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tavr'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Human'),
    (select id from sets where short_name = 'tavr'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'tavr'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon'),
    (select id from sets where short_name = 'tavr'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Human'),
    (select id from sets where short_name = 'tavr'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tavr'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tamiyo, the Moon Sage Emblem'),
    (select id from sets where short_name = 'tavr'),
    '8',
    'common'
) 
 on conflict do nothing;
