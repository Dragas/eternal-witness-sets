    insert into mtgcard(name) values ('Spirit') on conflict do nothing;
    insert into mtgcard(name) values ('Human') on conflict do nothing;
    insert into mtgcard(name) values ('Angel') on conflict do nothing;
    insert into mtgcard(name) values ('Demon') on conflict do nothing;
    insert into mtgcard(name) values ('Zombie') on conflict do nothing;
    insert into mtgcard(name) values ('Tamiyo, the Moon Sage Emblem') on conflict do nothing;
