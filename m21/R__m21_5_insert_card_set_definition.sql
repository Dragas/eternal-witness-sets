insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Mystic Skyfish'),
    (select id from sets where short_name = 'm21'),
    '326',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mangara, the Diplomat'),
    (select id from sets where short_name = 'm21'),
    '343',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm21'),
    '312',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm21'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ranger''s Guile'),
    (select id from sets where short_name = 'm21'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Radiant Fountain'),
    (select id from sets where short_name = 'm21'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Legion''s Judgment'),
    (select id from sets where short_name = 'm21'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Miscast'),
    (select id from sets where short_name = 'm21'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Subira, Tulzidi Caravanner'),
    (select id from sets where short_name = 'm21'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tome Anima'),
    (select id from sets where short_name = 'm21'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Walking Corpse'),
    (select id from sets where short_name = 'm21'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormwing Entity'),
    (select id from sets where short_name = 'm21'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm21'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sure Strike'),
    (select id from sets where short_name = 'm21'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm21'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pestilent Haze'),
    (select id from sets where short_name = 'm21'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Basri''s Acolyte'),
    (select id from sets where short_name = 'm21'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple of Triumph'),
    (select id from sets where short_name = 'm21'),
    '391',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Mystery'),
    (select id from sets where short_name = 'm21'),
    '389',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scavenging Ooze'),
    (select id from sets where short_name = 'm21'),
    '318',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Baneslayer Angel'),
    (select id from sets where short_name = 'm21'),
    '340',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fiery Emancipation'),
    (select id from sets where short_name = 'm21'),
    '143',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Discontinuity'),
    (select id from sets where short_name = 'm21'),
    '349',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Azusa, Lost but Seeking'),
    (select id from sets where short_name = 'm21'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm21'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple of Silence'),
    (select id from sets where short_name = 'm21'),
    '255',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vito, Thorn of the Dusk Rose'),
    (select id from sets where short_name = 'm21'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tranquil Cove'),
    (select id from sets where short_name = 'm21'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runed Halo'),
    (select id from sets where short_name = 'm21'),
    '346',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heartfire Immolator'),
    (select id from sets where short_name = 'm21'),
    '396',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pack Leader'),
    (select id from sets where short_name = 'm21'),
    '345',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pursued Whale'),
    (select id from sets where short_name = 'm21'),
    '351',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azusa, Lost but Seeking'),
    (select id from sets where short_name = 'm21'),
    '372',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Epiphany'),
    (select id from sets where short_name = 'm21'),
    '252',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyway Sniper'),
    (select id from sets where short_name = 'm21'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vryn Wingmare'),
    (select id from sets where short_name = 'm21'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fierce Empath'),
    (select id from sets where short_name = 'm21'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glorious Anthem'),
    (select id from sets where short_name = 'm21'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Steward'),
    (select id from sets where short_name = 'm21'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Setessan Training'),
    (select id from sets where short_name = 'm21'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Storm Caller'),
    (select id from sets where short_name = 'm21'),
    '335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ugin, the Spirit Dragon'),
    (select id from sets where short_name = 'm21'),
    '279',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Primal Might'),
    (select id from sets where short_name = 'm21'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chrome Replicator'),
    (select id from sets where short_name = 'm21'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chandra, Heart of Fire'),
    (select id from sets where short_name = 'm21'),
    '283',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chandra, Flame''s Catalyst'),
    (select id from sets where short_name = 'm21'),
    '332',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Speaker of the Heavens'),
    (select id from sets where short_name = 'm21'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Basri''s Acolyte'),
    (select id from sets where short_name = 'm21'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dire Fleet Warmonger'),
    (select id from sets where short_name = 'm21'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teferi, Master of Time'),
    (select id from sets where short_name = 'm21'),
    '292',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Temple of Triumph'),
    (select id from sets where short_name = 'm21'),
    '256',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm21'),
    '310',
    'common'
) ,
(
    (select id from mtgcard where name = 'Solemn Simulacrum'),
    (select id from sets where short_name = 'm21'),
    '319',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Basri''s Lieutenant'),
    (select id from sets where short_name = 'm21'),
    '288',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Basri, Devoted Paladin'),
    (select id from sets where short_name = 'm21'),
    '320',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Palladium Myr'),
    (select id from sets where short_name = 'm21'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghostly Pilferer'),
    (select id from sets where short_name = 'm21'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drowsing Tyrannodon'),
    (select id from sets where short_name = 'm21'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Glutton'),
    (select id from sets where short_name = 'm21'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Furious Rise'),
    (select id from sets where short_name = 'm21'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Finishing Blow'),
    (select id from sets where short_name = 'm21'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Radha, Heart of Keld'),
    (select id from sets where short_name = 'm21'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Ageless Insight'),
    (select id from sets where short_name = 'm21'),
    '294',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi, Master of Time'),
    (select id from sets where short_name = 'm21'),
    '275',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Unsubstantiate'),
    (select id from sets where short_name = 'm21'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm21'),
    '309',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm21'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cultivate'),
    (select id from sets where short_name = 'm21'),
    '317',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Makeshift Battalion'),
    (select id from sets where short_name = 'm21'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carrion Grub'),
    (select id from sets where short_name = 'm21'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Opt'),
    (select id from sets where short_name = 'm21'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sublime Epiphany'),
    (select id from sets where short_name = 'm21'),
    '355',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm21'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necromentia'),
    (select id from sets where short_name = 'm21'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gadrak, the Crown-Scourge'),
    (select id from sets where short_name = 'm21'),
    '367',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiery Emancipation'),
    (select id from sets where short_name = 'm21'),
    '366',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wishcoin Crab'),
    (select id from sets where short_name = 'm21'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctum of Tranquil Light'),
    (select id from sets where short_name = 'm21'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Watcher of the Spheres'),
    (select id from sets where short_name = 'm21'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Havoc Jester'),
    (select id from sets where short_name = 'm21'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Life Goes On'),
    (select id from sets where short_name = 'm21'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Turn to Slag'),
    (select id from sets where short_name = 'm21'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk''s Gorehorn'),
    (select id from sets where short_name = 'm21'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frost Breath'),
    (select id from sets where short_name = 'm21'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chromatic Orrery'),
    (select id from sets where short_name = 'm21'),
    '228',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tempered Veteran'),
    (select id from sets where short_name = 'm21'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Standard Bearer'),
    (select id from sets where short_name = 'm21'),
    '299',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forgotten Sentinel'),
    (select id from sets where short_name = 'm21'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lorescale Coatl'),
    (select id from sets where short_name = 'm21'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hooded Blightfang'),
    (select id from sets where short_name = 'm21'),
    '357',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burlfist Oak'),
    (select id from sets where short_name = 'm21'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Standard Bearer'),
    (select id from sets where short_name = 'm21'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fabled Passage'),
    (select id from sets where short_name = 'm21'),
    '386',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Return to Nature'),
    (select id from sets where short_name = 'm21'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rambunctious Mutt'),
    (select id from sets where short_name = 'm21'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fungal Rebirth'),
    (select id from sets where short_name = 'm21'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Predatory Wurm'),
    (select id from sets where short_name = 'm21'),
    '338',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alpine Houndmaster'),
    (select id from sets where short_name = 'm21'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terror of the Peaks'),
    (select id from sets where short_name = 'm21'),
    '164',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Heroic Intervention'),
    (select id from sets where short_name = 'm21'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gadrak, the Crown-Scourge'),
    (select id from sets where short_name = 'm21'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestial Enforcer'),
    (select id from sets where short_name = 'm21'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snarespinner'),
    (select id from sets where short_name = 'm21'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wildwood Scourge'),
    (select id from sets where short_name = 'm21'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Garruk, Savage Herald'),
    (select id from sets where short_name = 'm21'),
    '336',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sanctum of Shattered Heights'),
    (select id from sets where short_name = 'm21'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scavenging Ooze'),
    (select id from sets where short_name = 'm21'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonic Embrace'),
    (select id from sets where short_name = 'm21'),
    '356',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'm21'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cultivate'),
    (select id from sets where short_name = 'm21'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Baneslayer Angel'),
    (select id from sets where short_name = 'm21'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm21'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hobblefiend'),
    (select id from sets where short_name = 'm21'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormwing Entity'),
    (select id from sets where short_name = 'm21'),
    '354',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Idol of Endurance'),
    (select id from sets where short_name = 'm21'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silent Dart'),
    (select id from sets where short_name = 'm21'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Niambi, Esteemed Speaker'),
    (select id from sets where short_name = 'm21'),
    '379',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Invigorating Surge'),
    (select id from sets where short_name = 'm21'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Read the Tides'),
    (select id from sets where short_name = 'm21'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Protege'),
    (select id from sets where short_name = 'm21'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frantic Inventory'),
    (select id from sets where short_name = 'm21'),
    '394',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Salvo'),
    (select id from sets where short_name = 'm21'),
    '371',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonic Embrace'),
    (select id from sets where short_name = 'm21'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fetid Imp'),
    (select id from sets where short_name = 'm21'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wind-Scarred Crag'),
    (select id from sets where short_name = 'm21'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basri''s Aegis'),
    (select id from sets where short_name = 'm21'),
    '322',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mazemind Tome'),
    (select id from sets where short_name = 'm21'),
    '383',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shipwreck Dowser'),
    (select id from sets where short_name = 'm21'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cancel'),
    (select id from sets where short_name = 'm21'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'm21'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selfless Savior'),
    (select id from sets where short_name = 'm21'),
    '393',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Scrounger'),
    (select id from sets where short_name = 'm21'),
    '330',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sigiled Contender'),
    (select id from sets where short_name = 'm21'),
    '323',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Defiant Strike'),
    (select id from sets where short_name = 'm21'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctum of Fruitful Harvest'),
    (select id from sets where short_name = 'm21'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Twinblade Assassins'),
    (select id from sets where short_name = 'm21'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Conspicuous Snoop'),
    (select id from sets where short_name = 'm21'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodfell Caves'),
    (select id from sets where short_name = 'm21'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brash Taunter'),
    (select id from sets where short_name = 'm21'),
    '363',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gnarled Sage'),
    (select id from sets where short_name = 'm21'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Concordia Pegasus'),
    (select id from sets where short_name = 'm21'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bone Pit Brute'),
    (select id from sets where short_name = 'm21'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runed Halo'),
    (select id from sets where short_name = 'm21'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Sear'),
    (select id from sets where short_name = 'm21'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teferi, Timeless Voyager'),
    (select id from sets where short_name = 'm21'),
    '324',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Indulging Patrician'),
    (select id from sets where short_name = 'm21'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lofty Denial'),
    (select id from sets where short_name = 'm21'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rise Again'),
    (select id from sets where short_name = 'm21'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Devotee'),
    (select id from sets where short_name = 'm21'),
    '298',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selfless Savior'),
    (select id from sets where short_name = 'm21'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jungle Hollow'),
    (select id from sets where short_name = 'm21'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Runes'),
    (select id from sets where short_name = 'm21'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spined Megalodon'),
    (select id from sets where short_name = 'm21'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Titanic Growth'),
    (select id from sets where short_name = 'm21'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alchemist''s Gift'),
    (select id from sets where short_name = 'm21'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sporeweb Weaver'),
    (select id from sets where short_name = 'm21'),
    '378',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Griffin Aerie'),
    (select id from sets where short_name = 'm21'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Experimental Overload'),
    (select id from sets where short_name = 'm21'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Garruk''s Harbinger'),
    (select id from sets where short_name = 'm21'),
    '307',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm21'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk''s Uprising'),
    (select id from sets where short_name = 'm21'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Protege'),
    (select id from sets where short_name = 'm21'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feat of Resistance'),
    (select id from sets where short_name = 'm21'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Subira, Tulzidi Caravanner'),
    (select id from sets where short_name = 'm21'),
    '368',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Basri''s Lieutenant'),
    (select id from sets where short_name = 'm21'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eliminate'),
    (select id from sets where short_name = 'm21'),
    '395',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Double Vision'),
    (select id from sets where short_name = 'm21'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Firemaw'),
    (select id from sets where short_name = 'm21'),
    '333',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Onakke Ogre'),
    (select id from sets where short_name = 'm21'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grasp of Darkness'),
    (select id from sets where short_name = 'm21'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Radha, Heart of Keld'),
    (select id from sets where short_name = 'm21'),
    '380',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Epitaph Golem'),
    (select id from sets where short_name = 'm21'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Siege Striker'),
    (select id from sets where short_name = 'm21'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bad Deal'),
    (select id from sets where short_name = 'm21'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shacklegeist'),
    (select id from sets where short_name = 'm21'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keral Keep Disciples'),
    (select id from sets where short_name = 'm21'),
    '334',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Solemn Simulacrum'),
    (select id from sets where short_name = 'm21'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leafkin Avenger'),
    (select id from sets where short_name = 'm21'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pack Leader'),
    (select id from sets where short_name = 'm21'),
    '392',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hellkite Punisher'),
    (select id from sets where short_name = 'm21'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hunter''s Edge'),
    (select id from sets where short_name = 'm21'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shacklegeist'),
    (select id from sets where short_name = 'm21'),
    '353',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jolrael, Mwonvuli Recluse'),
    (select id from sets where short_name = 'm21'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Geyser'),
    (select id from sets where short_name = 'm21'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Transmogrify'),
    (select id from sets where short_name = 'm21'),
    '370',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Colossal Dreadmaw'),
    (select id from sets where short_name = 'm21'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctum of Calm Waters'),
    (select id from sets where short_name = 'm21'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Temple of Mystery'),
    (select id from sets where short_name = 'm21'),
    '254',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crash Through'),
    (select id from sets where short_name = 'm21'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = 'm21'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archfiend''s Vessel'),
    (select id from sets where short_name = 'm21'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feline Sovereign'),
    (select id from sets where short_name = 'm21'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaervek, the Spiteful'),
    (select id from sets where short_name = 'm21'),
    '358',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heartfire Immolator'),
    (select id from sets where short_name = 'm21'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llanowar Visionary'),
    (select id from sets where short_name = 'm21'),
    '397',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kitesail Freebooter'),
    (select id from sets where short_name = 'm21'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Malefic Scythe'),
    (select id from sets where short_name = 'm21'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm21'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Pyreling'),
    (select id from sets where short_name = 'm21'),
    '304',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trufflesnout'),
    (select id from sets where short_name = 'm21'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Roaming Ghostlight'),
    (select id from sets where short_name = 'm21'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk''s Uprising'),
    (select id from sets where short_name = 'm21'),
    '308',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Faith''s Fetters'),
    (select id from sets where short_name = 'm21'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elder Gargaroth'),
    (select id from sets where short_name = 'm21'),
    '373',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Frantic Inventory'),
    (select id from sets where short_name = 'm21'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'See the Truth'),
    (select id from sets where short_name = 'm21'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Magmutt'),
    (select id from sets where short_name = 'm21'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana, Death Mage'),
    (select id from sets where short_name = 'm21'),
    '328',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Grim Tutor'),
    (select id from sets where short_name = 'm21'),
    '103',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Necromentia'),
    (select id from sets where short_name = 'm21'),
    '359',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra, Heart of Fire'),
    (select id from sets where short_name = 'm21'),
    '301',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Short Sword'),
    (select id from sets where short_name = 'm21'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infernal Scarring'),
    (select id from sets where short_name = 'm21'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basri Ket'),
    (select id from sets where short_name = 'm21'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sanctum of All'),
    (select id from sets where short_name = 'm21'),
    '381',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mangara, the Diplomat'),
    (select id from sets where short_name = 'm21'),
    '27',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ugin, the Spirit Dragon'),
    (select id from sets where short_name = 'm21'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Teferi, Master of Time'),
    (select id from sets where short_name = 'm21'),
    '75',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Village Rites'),
    (select id from sets where short_name = 'm21'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tormod''s Crypt'),
    (select id from sets where short_name = 'm21'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anointed Chorister'),
    (select id from sets where short_name = 'm21'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rugged Highlands'),
    (select id from sets where short_name = 'm21'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk''s Harbinger'),
    (select id from sets where short_name = 'm21'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bolt Hound'),
    (select id from sets where short_name = 'm21'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mazemind Tome'),
    (select id from sets where short_name = 'm21'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Massacre Wurm'),
    (select id from sets where short_name = 'm21'),
    '316',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ugin, the Spirit Dragon'),
    (select id from sets where short_name = 'm21'),
    '285',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jeskai Elder'),
    (select id from sets where short_name = 'm21'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gloom Sower'),
    (select id from sets where short_name = 'm21'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quirion Dryad'),
    (select id from sets where short_name = 'm21'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Containment Priest'),
    (select id from sets where short_name = 'm21'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana, Waker of the Dead'),
    (select id from sets where short_name = 'm21'),
    '282',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Volcanic Salvo'),
    (select id from sets where short_name = 'm21'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conclave Mentor'),
    (select id from sets where short_name = 'm21'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glorious Anthem'),
    (select id from sets where short_name = 'm21'),
    '341',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Revitalize'),
    (select id from sets where short_name = 'm21'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Canopy Stalker'),
    (select id from sets where short_name = 'm21'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Peer into the Abyss'),
    (select id from sets where short_name = 'm21'),
    '360',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra, Heart of Fire'),
    (select id from sets where short_name = 'm21'),
    '135',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm21'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vodalian Arcanist'),
    (select id from sets where short_name = 'm21'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thieves'' Guild Enforcer'),
    (select id from sets where short_name = 'm21'),
    '361',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pack Leader'),
    (select id from sets where short_name = 'm21'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi, Master of Time'),
    (select id from sets where short_name = 'm21'),
    '276',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Teferi, Master of Time'),
    (select id from sets where short_name = 'm21'),
    '293',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swift Response'),
    (select id from sets where short_name = 'm21'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scorching Dragonfire'),
    (select id from sets where short_name = 'm21'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Valorous Steed'),
    (select id from sets where short_name = 'm21'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basri''s Solidarity'),
    (select id from sets where short_name = 'm21'),
    '289',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Steward'),
    (select id from sets where short_name = 'm21'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Peer into the Abyss'),
    (select id from sets where short_name = 'm21'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Traitorous Greed'),
    (select id from sets where short_name = 'm21'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Waker of Waves'),
    (select id from sets where short_name = 'm21'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sanguine Indulgence'),
    (select id from sets where short_name = 'm21'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terror of the Peaks'),
    (select id from sets where short_name = 'm21'),
    '369',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hooded Blightfang'),
    (select id from sets where short_name = 'm21'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sublime Epiphany'),
    (select id from sets where short_name = 'm21'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sparkhunter Masticore'),
    (select id from sets where short_name = 'm21'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Devotee'),
    (select id from sets where short_name = 'm21'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Historian of Zhalfir'),
    (select id from sets where short_name = 'm21'),
    '325',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Tutelage'),
    (select id from sets where short_name = 'm21'),
    '296',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Garruk, Unleashed'),
    (select id from sets where short_name = 'm21'),
    '183',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Animal Sanctuary'),
    (select id from sets where short_name = 'm21'),
    '385',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaervek, the Spiteful'),
    (select id from sets where short_name = 'm21'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pursued Whale'),
    (select id from sets where short_name = 'm21'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Epiphany'),
    (select id from sets where short_name = 'm21'),
    '387',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rookie Mistake'),
    (select id from sets where short_name = 'm21'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Scorn'),
    (select id from sets where short_name = 'm21'),
    '329',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Discontinuity'),
    (select id from sets where short_name = 'm21'),
    '48',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dismal Backwater'),
    (select id from sets where short_name = 'm21'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Track Down'),
    (select id from sets where short_name = 'm21'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm21'),
    '313',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rewind'),
    (select id from sets where short_name = 'm21'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Incinerator'),
    (select id from sets where short_name = 'm21'),
    '302',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm21'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctum of All'),
    (select id from sets where short_name = 'm21'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi, Master of Time'),
    (select id from sets where short_name = 'm21'),
    '281',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Spellgorger Weird'),
    (select id from sets where short_name = 'm21'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk, Unleashed'),
    (select id from sets where short_name = 'm21'),
    '284',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Run Afoul'),
    (select id from sets where short_name = 'm21'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm21'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Destructive Tampering'),
    (select id from sets where short_name = 'm21'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teferi, Master of Time'),
    (select id from sets where short_name = 'm21'),
    '291',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Crypt Lurker'),
    (select id from sets where short_name = 'm21'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Tutelage'),
    (select id from sets where short_name = 'm21'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tolarian Kraken'),
    (select id from sets where short_name = 'm21'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sparkhunter Masticore'),
    (select id from sets where short_name = 'm21'),
    '384',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ornery Dilophosaur'),
    (select id from sets where short_name = 'm21'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basri''s Solidarity'),
    (select id from sets where short_name = 'm21'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Ageless Insight'),
    (select id from sets where short_name = 'm21'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alpine Watchdog'),
    (select id from sets where short_name = 'm21'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dub'),
    (select id from sets where short_name = 'm21'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Gagglemaster'),
    (select id from sets where short_name = 'm21'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Garruk, Unleashed'),
    (select id from sets where short_name = 'm21'),
    '305',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thrashing Brontodon'),
    (select id from sets where short_name = 'm21'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rousing Read'),
    (select id from sets where short_name = 'm21'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blossoming Sands'),
    (select id from sets where short_name = 'm21'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple of Malady'),
    (select id from sets where short_name = 'm21'),
    '253',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goremand'),
    (select id from sets where short_name = 'm21'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Basri Ket'),
    (select id from sets where short_name = 'm21'),
    '286',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Skyscanner'),
    (select id from sets where short_name = 'm21'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barrin, Tolarian Archmage'),
    (select id from sets where short_name = 'm21'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Furor of the Bitten'),
    (select id from sets where short_name = 'm21'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Library Larcenist'),
    (select id from sets where short_name = 'm21'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keen Glidemaster'),
    (select id from sets where short_name = 'm21'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wildwood Patrol'),
    (select id from sets where short_name = 'm21'),
    '339',
    'common'
) ,
(
    (select id from mtgcard where name = 'Adherent of Hope'),
    (select id from sets where short_name = 'm21'),
    '321',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit of Malevolence'),
    (select id from sets where short_name = 'm21'),
    '331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heroic Intervention'),
    (select id from sets where short_name = 'm21'),
    '375',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tavern Swindler'),
    (select id from sets where short_name = 'm21'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sanctum of Stone Fangs'),
    (select id from sets where short_name = 'm21'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Portcullis Vine'),
    (select id from sets where short_name = 'm21'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Igneous Cur'),
    (select id from sets where short_name = 'm21'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gale Swooper'),
    (select id from sets where short_name = 'm21'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fabled Passage'),
    (select id from sets where short_name = 'm21'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enthralling Hold'),
    (select id from sets where short_name = 'm21'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Witch''s Cauldron'),
    (select id from sets where short_name = 'm21'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm21'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Turret Ogre'),
    (select id from sets where short_name = 'm21'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teferi, Master of Time'),
    (select id from sets where short_name = 'm21'),
    '277',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Double Vision'),
    (select id from sets where short_name = 'm21'),
    '365',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elder Gargaroth'),
    (select id from sets where short_name = 'm21'),
    '179',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Containment Priest'),
    (select id from sets where short_name = 'm21'),
    '314',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Riddleform'),
    (select id from sets where short_name = 'm21'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chromatic Orrery'),
    (select id from sets where short_name = 'm21'),
    '382',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Light of Promise'),
    (select id from sets where short_name = 'm21'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rin and Seri, Inseparable'),
    (select id from sets where short_name = 'm21'),
    '278',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Garruk''s Warsteed'),
    (select id from sets where short_name = 'm21'),
    '337',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Masked Blackguard'),
    (select id from sets where short_name = 'm21'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Secure the Scene'),
    (select id from sets where short_name = 'm21'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Wavecaster'),
    (select id from sets where short_name = 'm21'),
    '327',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Llanowar Visionary'),
    (select id from sets where short_name = 'm21'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Wizardry'),
    (select id from sets where short_name = 'm21'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistral Singer'),
    (select id from sets where short_name = 'm21'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rain of Revelation'),
    (select id from sets where short_name = 'm21'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nine Lives'),
    (select id from sets where short_name = 'm21'),
    '344',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Staunch Shieldmate'),
    (select id from sets where short_name = 'm21'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathbloom Thallid'),
    (select id from sets where short_name = 'm21'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kinetic Augur'),
    (select id from sets where short_name = 'm21'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Garruk''s Gorehorn'),
    (select id from sets where short_name = 'm21'),
    '306',
    'common'
) ,
(
    (select id from mtgcard where name = 'See the Truth'),
    (select id from sets where short_name = 'm21'),
    '352',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tide Skimmer'),
    (select id from sets where short_name = 'm21'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pridemalkin'),
    (select id from sets where short_name = 'm21'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thieves'' Guild Enforcer'),
    (select id from sets where short_name = 'm21'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primal Might'),
    (select id from sets where short_name = 'm21'),
    '377',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Niambi, Esteemed Speaker'),
    (select id from sets where short_name = 'm21'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Speaker of the Heavens'),
    (select id from sets where short_name = 'm21'),
    '347',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Massacre Wurm'),
    (select id from sets where short_name = 'm21'),
    '114',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Liliana, Waker of the Dead'),
    (select id from sets where short_name = 'm21'),
    '108',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Temple of Malady'),
    (select id from sets where short_name = 'm21'),
    '388',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Obsessive Stitcher'),
    (select id from sets where short_name = 'm21'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghostly Pilferer'),
    (select id from sets where short_name = 'm21'),
    '350',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swiftwater Cliffs'),
    (select id from sets where short_name = 'm21'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sporeweb Weaver'),
    (select id from sets where short_name = 'm21'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nine Lives'),
    (select id from sets where short_name = 'm21'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Pyreling'),
    (select id from sets where short_name = 'm21'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Idol of Endurance'),
    (select id from sets where short_name = 'm21'),
    '342',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unleash Fury'),
    (select id from sets where short_name = 'm21'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelic Ascension'),
    (select id from sets where short_name = 'm21'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana, Waker of the Dead'),
    (select id from sets where short_name = 'm21'),
    '297',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Brash Taunter'),
    (select id from sets where short_name = 'm21'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meteorite'),
    (select id from sets where short_name = 'm21'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Magmutt'),
    (select id from sets where short_name = 'm21'),
    '303',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basri Ket'),
    (select id from sets where short_name = 'm21'),
    '280',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Temple of Silence'),
    (select id from sets where short_name = 'm21'),
    '390',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm21'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Incinerator'),
    (select id from sets where short_name = 'm21'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prismite'),
    (select id from sets where short_name = 'm21'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burn Bright'),
    (select id from sets where short_name = 'm21'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jolrael, Mwonvuli Recluse'),
    (select id from sets where short_name = 'm21'),
    '376',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warden of the Woods'),
    (select id from sets where short_name = 'm21'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warded Battlements'),
    (select id from sets where short_name = 'm21'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Transmogrify'),
    (select id from sets where short_name = 'm21'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thrill of Possibility'),
    (select id from sets where short_name = 'm21'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Capture Sphere'),
    (select id from sets where short_name = 'm21'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conspicuous Snoop'),
    (select id from sets where short_name = 'm21'),
    '364',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi, Master of Time'),
    (select id from sets where short_name = 'm21'),
    '290',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Caged Zombie'),
    (select id from sets where short_name = 'm21'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thornwood Falls'),
    (select id from sets where short_name = 'm21'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vito, Thorn of the Dusk Rose'),
    (select id from sets where short_name = 'm21'),
    '362',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animal Sanctuary'),
    (select id from sets where short_name = 'm21'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eliminate'),
    (select id from sets where short_name = 'm21'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm21'),
    '311',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Arsonist'),
    (select id from sets where short_name = 'm21'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battle-Rattle Shaman'),
    (select id from sets where short_name = 'm21'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Falconer Adept'),
    (select id from sets where short_name = 'm21'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pitchburn Devils'),
    (select id from sets where short_name = 'm21'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feline Sovereign'),
    (select id from sets where short_name = 'm21'),
    '374',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grim Tutor'),
    (select id from sets where short_name = 'm21'),
    '315',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Barrin, Tolarian Archmage'),
    (select id from sets where short_name = 'm21'),
    '348',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm21'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skeleton Archer'),
    (select id from sets where short_name = 'm21'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sabertooth Mauler'),
    (select id from sets where short_name = 'm21'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scoured Barrens'),
    (select id from sets where short_name = 'm21'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seasoned Hallowblade'),
    (select id from sets where short_name = 'm21'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silversmote Ghoul'),
    (select id from sets where short_name = 'm21'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daybreak Charger'),
    (select id from sets where short_name = 'm21'),
    '14',
    'common'
) 
 on conflict do nothing;
