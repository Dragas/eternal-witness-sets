insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Homicidal Seclusion'),
    (select id from sets where short_name = 'avr'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Defy Death'),
    (select id from sets where short_name = 'avr'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Polluted Dead'),
    (select id from sets where short_name = 'avr'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crypt Creeper'),
    (select id from sets where short_name = 'avr'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gang of Devils'),
    (select id from sets where short_name = 'avr'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wingcrafter'),
    (select id from sets where short_name = 'avr'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Appetite for Brains'),
    (select id from sets where short_name = 'avr'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'avr'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul of the Harvest'),
    (select id from sets where short_name = 'avr'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Commander''s Authority'),
    (select id from sets where short_name = 'avr'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terminus'),
    (select id from sets where short_name = 'avr'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moonsilver Spear'),
    (select id from sets where short_name = 'avr'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Second Guess'),
    (select id from sets where short_name = 'avr'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mist Raven'),
    (select id from sets where short_name = 'avr'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'avr'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eaten by Spiders'),
    (select id from sets where short_name = 'avr'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fervent Cathar'),
    (select id from sets where short_name = 'avr'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hound of Griselbrand'),
    (select id from sets where short_name = 'avr'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evernight Shade'),
    (select id from sets where short_name = 'avr'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cavern of Souls'),
    (select id from sets where short_name = 'avr'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grave Exchange'),
    (select id from sets where short_name = 'avr'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silverblade Paladin'),
    (select id from sets where short_name = 'avr'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel of Jubilation'),
    (select id from sets where short_name = 'avr'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desolate Lighthouse'),
    (select id from sets where short_name = 'avr'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mad Prophet'),
    (select id from sets where short_name = 'avr'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nephalia Smuggler'),
    (select id from sets where short_name = 'avr'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gloom Surgeon'),
    (select id from sets where short_name = 'avr'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fleeting Distraction'),
    (select id from sets where short_name = 'avr'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Human Frailty'),
    (select id from sets where short_name = 'avr'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Uncanny Speed'),
    (select id from sets where short_name = 'avr'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Druid''s Familiar'),
    (select id from sets where short_name = 'avr'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghostform'),
    (select id from sets where short_name = 'avr'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thraben Valiant'),
    (select id from sets where short_name = 'avr'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scroll of Griselbrand'),
    (select id from sets where short_name = 'avr'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'avr'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Midnight Duelist'),
    (select id from sets where short_name = 'avr'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Banishing Stroke'),
    (select id from sets where short_name = 'avr'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Devastation Tide'),
    (select id from sets where short_name = 'avr'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Mauler'),
    (select id from sets where short_name = 'avr'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'avr'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temporal Mastery'),
    (select id from sets where short_name = 'avr'),
    '81',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'avr'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Geist Trappers'),
    (select id from sets where short_name = 'avr'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel''s Tomb'),
    (select id from sets where short_name = 'avr'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snare the Skies'),
    (select id from sets where short_name = 'avr'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Builder''s Blessing'),
    (select id from sets where short_name = 'avr'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Misthollow Griffin'),
    (select id from sets where short_name = 'avr'),
    '68',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lone Revenant'),
    (select id from sets where short_name = 'avr'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Artist'),
    (select id from sets where short_name = 'avr'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terrifying Presence'),
    (select id from sets where short_name = 'avr'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghoulflesh'),
    (select id from sets where short_name = 'avr'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Entreat the Angels'),
    (select id from sets where short_name = 'avr'),
    '20',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Alchemist''s Apprentice'),
    (select id from sets where short_name = 'avr'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcane Melee'),
    (select id from sets where short_name = 'avr'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'avr'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Killing Wave'),
    (select id from sets where short_name = 'avr'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloudshift'),
    (select id from sets where short_name = 'avr'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reforge the Soul'),
    (select id from sets where short_name = 'avr'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Revenge of the Hunted'),
    (select id from sets where short_name = 'avr'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archangel'),
    (select id from sets where short_name = 'avr'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bone Splinters'),
    (select id from sets where short_name = 'avr'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marrow Bats'),
    (select id from sets where short_name = 'avr'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Renegade Demon'),
    (select id from sets where short_name = 'avr'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barter in Blood'),
    (select id from sets where short_name = 'avr'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dread Slaver'),
    (select id from sets where short_name = 'avr'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Latch Seeker'),
    (select id from sets where short_name = 'avr'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghostly Touch'),
    (select id from sets where short_name = 'avr'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trusted Forcemage'),
    (select id from sets where short_name = 'avr'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Armaments'),
    (select id from sets where short_name = 'avr'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Somberwald Sage'),
    (select id from sets where short_name = 'avr'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Falkenrath Exterminator'),
    (select id from sets where short_name = 'avr'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Infinite Reflection'),
    (select id from sets where short_name = 'avr'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primal Surge'),
    (select id from sets where short_name = 'avr'),
    '189',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Outwit'),
    (select id from sets where short_name = 'avr'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battle Hymn'),
    (select id from sets where short_name = 'avr'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Butcher Ghoul'),
    (select id from sets where short_name = 'avr'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ulvenwald Tracker'),
    (select id from sets where short_name = 'avr'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death Wind'),
    (select id from sets where short_name = 'avr'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demolish'),
    (select id from sets where short_name = 'avr'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wildwood Geist'),
    (select id from sets where short_name = 'avr'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Otherworld Atlas'),
    (select id from sets where short_name = 'avr'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Farbog Explorer'),
    (select id from sets where short_name = 'avr'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Havengul Skaab'),
    (select id from sets where short_name = 'avr'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Favorable Winds'),
    (select id from sets where short_name = 'avr'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Exquisite Blood'),
    (select id from sets where short_name = 'avr'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moonlight Geist'),
    (select id from sets where short_name = 'avr'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightshade Peddler'),
    (select id from sets where short_name = 'avr'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mental Agony'),
    (select id from sets where short_name = 'avr'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harvester of Souls'),
    (select id from sets where short_name = 'avr'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necrobite'),
    (select id from sets where short_name = 'avr'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stolen Goods'),
    (select id from sets where short_name = 'avr'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Prowess'),
    (select id from sets where short_name = 'avr'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Driver of the Dead'),
    (select id from sets where short_name = 'avr'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Druids'' Repository'),
    (select id from sets where short_name = 'avr'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moorland Inquisitor'),
    (select id from sets where short_name = 'avr'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunderbolt'),
    (select id from sets where short_name = 'avr'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'avr'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seraph of Dawn'),
    (select id from sets where short_name = 'avr'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tibalt, the Fiend-Blooded'),
    (select id from sets where short_name = 'avr'),
    '161',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Into the Void'),
    (select id from sets where short_name = 'avr'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lair Delve'),
    (select id from sets where short_name = 'avr'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dangerous Wager'),
    (select id from sets where short_name = 'avr'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Holy Justiciar'),
    (select id from sets where short_name = 'avr'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diregraf Escort'),
    (select id from sets where short_name = 'avr'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Joint Assault'),
    (select id from sets where short_name = 'avr'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archwing Dragon'),
    (select id from sets where short_name = 'avr'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gisela, Blade of Goldnight'),
    (select id from sets where short_name = 'avr'),
    '209',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Conjurer''s Closet'),
    (select id from sets where short_name = 'avr'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hanweir Lancer'),
    (select id from sets where short_name = 'avr'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kruin Striker'),
    (select id from sets where short_name = 'avr'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vorstclaw'),
    (select id from sets where short_name = 'avr'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nettle Swine'),
    (select id from sets where short_name = 'avr'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cathedral Sanctifier'),
    (select id from sets where short_name = 'avr'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Herald of War'),
    (select id from sets where short_name = 'avr'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cathars'' Crusade'),
    (select id from sets where short_name = 'avr'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonlord of Ashmouth'),
    (select id from sets where short_name = 'avr'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raging Poltergeist'),
    (select id from sets where short_name = 'avr'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flowering Lumberknot'),
    (select id from sets where short_name = 'avr'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crippling Chill'),
    (select id from sets where short_name = 'avr'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel''s Mercy'),
    (select id from sets where short_name = 'avr'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emancipation Angel'),
    (select id from sets where short_name = 'avr'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel of Glory''s Rise'),
    (select id from sets where short_name = 'avr'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dual Casting'),
    (select id from sets where short_name = 'avr'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leap of Faith'),
    (select id from sets where short_name = 'avr'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Triumph of Ferocity'),
    (select id from sets where short_name = 'avr'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Essence Harvest'),
    (select id from sets where short_name = 'avr'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Haunted Guardian'),
    (select id from sets where short_name = 'avr'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Defiance'),
    (select id from sets where short_name = 'avr'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Peel from Reality'),
    (select id from sets where short_name = 'avr'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'avr'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tandem Lookout'),
    (select id from sets where short_name = 'avr'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Devout Chaplain'),
    (select id from sets where short_name = 'avr'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wolfir Silverheart'),
    (select id from sets where short_name = 'avr'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scroll of Avacyn'),
    (select id from sets where short_name = 'avr'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreadwaters'),
    (select id from sets where short_name = 'avr'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Predator''s Gambit'),
    (select id from sets where short_name = 'avr'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolfir Avenger'),
    (select id from sets where short_name = 'avr'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bladed Bracers'),
    (select id from sets where short_name = 'avr'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spectral Prison'),
    (select id from sets where short_name = 'avr'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rite of Ruin'),
    (select id from sets where short_name = 'avr'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Malignus'),
    (select id from sets where short_name = 'avr'),
    '148',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gallows at Willow Hill'),
    (select id from sets where short_name = 'avr'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Undead Executioner'),
    (select id from sets where short_name = 'avr'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fettergeist'),
    (select id from sets where short_name = 'avr'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Borderland Ranger'),
    (select id from sets where short_name = 'avr'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Impostor'),
    (select id from sets where short_name = 'avr'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Triumph of Cruelty'),
    (select id from sets where short_name = 'avr'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'avr'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slayers'' Stronghold'),
    (select id from sets where short_name = 'avr'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sheltering Word'),
    (select id from sets where short_name = 'avr'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'avr'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tormentor''s Trident'),
    (select id from sets where short_name = 'avr'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stonewright'),
    (select id from sets where short_name = 'avr'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Natural End'),
    (select id from sets where short_name = 'avr'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodflow Connoisseur'),
    (select id from sets where short_name = 'avr'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'avr'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aggravate'),
    (select id from sets where short_name = 'avr'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Righteous Blow'),
    (select id from sets where short_name = 'avr'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'avr'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Somberwald Vigilante'),
    (select id from sets where short_name = 'avr'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'avr'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gloomwidow'),
    (select id from sets where short_name = 'avr'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demonic Rising'),
    (select id from sets where short_name = 'avr'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Craterhoof Behemoth'),
    (select id from sets where short_name = 'avr'),
    '172',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rush of Blood'),
    (select id from sets where short_name = 'avr'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Banners Raised'),
    (select id from sets where short_name = 'avr'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vigilante Justice'),
    (select id from sets where short_name = 'avr'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wandering Wolf'),
    (select id from sets where short_name = 'avr'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Descendants'' Path'),
    (select id from sets where short_name = 'avr'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Timberland Guide'),
    (select id from sets where short_name = 'avr'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zealous Strike'),
    (select id from sets where short_name = 'avr'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abundant Growth'),
    (select id from sets where short_name = 'avr'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Searchlight Geist'),
    (select id from sets where short_name = 'avr'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Galvanic Alchemist'),
    (select id from sets where short_name = 'avr'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unhallowed Pact'),
    (select id from sets where short_name = 'avr'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Griselbrand'),
    (select id from sets where short_name = 'avr'),
    '106',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Geist Snatch'),
    (select id from sets where short_name = 'avr'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gryff Vanguard'),
    (select id from sets where short_name = 'avr'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voice of the Provinces'),
    (select id from sets where short_name = 'avr'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yew Spirit'),
    (select id from sets where short_name = 'avr'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tamiyo, the Moon Sage'),
    (select id from sets where short_name = 'avr'),
    '79',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bower Passage'),
    (select id from sets where short_name = 'avr'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avacyn, Angel of Hope'),
    (select id from sets where short_name = 'avr'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hunted Ghoul'),
    (select id from sets where short_name = 'avr'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scalding Devil'),
    (select id from sets where short_name = 'avr'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riders of Gavony'),
    (select id from sets where short_name = 'avr'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guise of Fire'),
    (select id from sets where short_name = 'avr'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riot Ringleader'),
    (select id from sets where short_name = 'avr'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Restoration Angel'),
    (select id from sets where short_name = 'avr'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Captain of the Mists'),
    (select id from sets where short_name = 'avr'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maalfeld Twins'),
    (select id from sets where short_name = 'avr'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goldnight Commander'),
    (select id from sets where short_name = 'avr'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cursebreak'),
    (select id from sets where short_name = 'avr'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alchemist''s Refuge'),
    (select id from sets where short_name = 'avr'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pillar of Flame'),
    (select id from sets where short_name = 'avr'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scrapskin Drake'),
    (select id from sets where short_name = 'avr'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Havengul Vampire'),
    (select id from sets where short_name = 'avr'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Call to Serve'),
    (select id from sets where short_name = 'avr'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vanguard''s Shield'),
    (select id from sets where short_name = 'avr'),
    '223',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blessings of Nature'),
    (select id from sets where short_name = 'avr'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burn at the Stake'),
    (select id from sets where short_name = 'avr'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Midvast Protector'),
    (select id from sets where short_name = 'avr'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Wall'),
    (select id from sets where short_name = 'avr'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nearheath Pilgrim'),
    (select id from sets where short_name = 'avr'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spirit Away'),
    (select id from sets where short_name = 'avr'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thunderous Wrath'),
    (select id from sets where short_name = 'avr'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Howlgeist'),
    (select id from sets where short_name = 'avr'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vexing Devil'),
    (select id from sets where short_name = 'avr'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grounded'),
    (select id from sets where short_name = 'avr'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soulcage Fiend'),
    (select id from sets where short_name = 'avr'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kessig Malcontents'),
    (select id from sets where short_name = 'avr'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vanishment'),
    (select id from sets where short_name = 'avr'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Corpse Traders'),
    (select id from sets where short_name = 'avr'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goldnight Redeemer'),
    (select id from sets where short_name = 'avr'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mass Appeal'),
    (select id from sets where short_name = 'avr'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bruna, Light of Alabaster'),
    (select id from sets where short_name = 'avr'),
    '208',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bonfire of the Damned'),
    (select id from sets where short_name = 'avr'),
    '129',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Elgaud Shieldmate'),
    (select id from sets where short_name = 'avr'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Champion of Lambholt'),
    (select id from sets where short_name = 'avr'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tyrant of Discord'),
    (select id from sets where short_name = 'avr'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thatcher Revolt'),
    (select id from sets where short_name = 'avr'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divine Deflection'),
    (select id from sets where short_name = 'avr'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vessel of Endless Rest'),
    (select id from sets where short_name = 'avr'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treacherous Pit-Dweller'),
    (select id from sets where short_name = 'avr'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'avr'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghostly Flicker'),
    (select id from sets where short_name = 'avr'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Defang'),
    (select id from sets where short_name = 'avr'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rotcrown Ghoul'),
    (select id from sets where short_name = 'avr'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Malicious Intent'),
    (select id from sets where short_name = 'avr'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lunar Mystic'),
    (select id from sets where short_name = 'avr'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'avr'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Amass the Components'),
    (select id from sets where short_name = 'avr'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spectral Gateguards'),
    (select id from sets where short_name = 'avr'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Narstad Scrapper'),
    (select id from sets where short_name = 'avr'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seraph Sanctuary'),
    (select id from sets where short_name = 'avr'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Descent into Madness'),
    (select id from sets where short_name = 'avr'),
    '97',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Deadeye Navigator'),
    (select id from sets where short_name = 'avr'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonic Taskmaster'),
    (select id from sets where short_name = 'avr'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zealous Conscripts'),
    (select id from sets where short_name = 'avr'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rain of Thorns'),
    (select id from sets where short_name = 'avr'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pathbreaker Wurm'),
    (select id from sets where short_name = 'avr'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sigarda, Host of Herons'),
    (select id from sets where short_name = 'avr'),
    '210',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Heirs of Stromkirk'),
    (select id from sets where short_name = 'avr'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stern Mentor'),
    (select id from sets where short_name = 'avr'),
    '77',
    'uncommon'
) 
 on conflict do nothing;
