insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Jace''s Ingenuity'),
    (select id from sets where short_name = 'f11'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Everflowing Chalice'),
    (select id from sets where short_name = 'f11'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Contagion Clasp'),
    (select id from sets where short_name = 'f11'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spellstutter Sprite'),
    (select id from sets where short_name = 'f11'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cultivate'),
    (select id from sets where short_name = 'f11'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Go for the Throat'),
    (select id from sets where short_name = 'f11'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Squadron Hawk'),
    (select id from sets where short_name = 'f11'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rhox War Monk'),
    (select id from sets where short_name = 'f11'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teetering Peaks'),
    (select id from sets where short_name = 'f11'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Artisan of Kozilek'),
    (select id from sets where short_name = 'f11'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savage Lands'),
    (select id from sets where short_name = 'f11'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Omens'),
    (select id from sets where short_name = 'f11'),
    '3',
    'rare'
) 
 on conflict do nothing;
