insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ball Lightning'),
    (select id from sets where short_name = 'g01'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oath of Druids'),
    (select id from sets where short_name = 'g01'),
    '2',
    'rare'
) 
 on conflict do nothing;
