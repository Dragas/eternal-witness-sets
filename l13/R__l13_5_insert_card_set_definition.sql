insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Bird'),
    (select id from sets where short_name = 'l13'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sliver'),
    (select id from sets where short_name = 'l13'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'l13'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'l13'),
    '1',
    'common'
) 
 on conflict do nothing;
