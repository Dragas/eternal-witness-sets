insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Blaring Recruiter'),
    (select id from sets where short_name = 'pbbd'),
    '13s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krav, the Unredeemed'),
    (select id from sets where short_name = 'pbbd'),
    '4s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Proud Mentor'),
    (select id from sets where short_name = 'pbbd'),
    '20s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rowan Kenrith'),
    (select id from sets where short_name = 'pbbd'),
    '256s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Zndrsplt, Eye of Wisdom'),
    (select id from sets where short_name = 'pbbd'),
    '5s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Khorvath Brightflame'),
    (select id from sets where short_name = 'pbbd'),
    '9s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Impetuous Protege'),
    (select id from sets where short_name = 'pbbd'),
    '19s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blaring Captain'),
    (select id from sets where short_name = 'pbbd'),
    '14s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pir, Imaginative Rascal'),
    (select id from sets where short_name = 'pbbd'),
    '11s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Will Kenrith'),
    (select id from sets where short_name = 'pbbd'),
    '255s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lore Weaver'),
    (select id from sets where short_name = 'pbbd'),
    '22s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvia Brightspear'),
    (select id from sets where short_name = 'pbbd'),
    '10s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Toothy, Imaginary Friend'),
    (select id from sets where short_name = 'pbbd'),
    '12s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ley Weaver'),
    (select id from sets where short_name = 'pbbd'),
    '21s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chakram Retriever'),
    (select id from sets where short_name = 'pbbd'),
    '15s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soulblade Renewer'),
    (select id from sets where short_name = 'pbbd'),
    '18s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Virtus the Veiled'),
    (select id from sets where short_name = 'pbbd'),
    '7s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chakram Slinger'),
    (select id from sets where short_name = 'pbbd'),
    '16s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Regna, the Redeemer'),
    (select id from sets where short_name = 'pbbd'),
    '3s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soulblade Corrupter'),
    (select id from sets where short_name = 'pbbd'),
    '17s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Okaun, Eye of Chaos'),
    (select id from sets where short_name = 'pbbd'),
    '6s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gorm the Great'),
    (select id from sets where short_name = 'pbbd'),
    '8s',
    'rare'
) 
 on conflict do nothing;
