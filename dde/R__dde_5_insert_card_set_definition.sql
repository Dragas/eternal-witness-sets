insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Phyrexian Processor'),
    (select id from sets where short_name = 'dde'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carrion Feeder'),
    (select id from sets where short_name = 'dde'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rith''s Charm'),
    (select id from sets where short_name = 'dde'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Greaves'),
    (select id from sets where short_name = 'dde'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Denouncer'),
    (select id from sets where short_name = 'dde'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Hulk'),
    (select id from sets where short_name = 'dde'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Exotic Curse'),
    (select id from sets where short_name = 'dde'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Armor'),
    (select id from sets where short_name = 'dde'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Ghoul'),
    (select id from sets where short_name = 'dde'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Colossus'),
    (select id from sets where short_name = 'dde'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sanguine Guard'),
    (select id from sets where short_name = 'dde'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Allied Strategies'),
    (select id from sets where short_name = 'dde'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Totem'),
    (select id from sets where short_name = 'dde'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Elder'),
    (select id from sets where short_name = 'dde'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gerrard Capashen'),
    (select id from sets where short_name = 'dde'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Order of Yawgmoth'),
    (select id from sets where short_name = 'dde'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Voltaic Key'),
    (select id from sets where short_name = 'dde'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hideous End'),
    (select id from sets where short_name = 'dde'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Puppet Strings'),
    (select id from sets where short_name = 'dde'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'dde'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armadillo Cloak'),
    (select id from sets where short_name = 'dde'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunscape Battlemage'),
    (select id from sets where short_name = 'dde'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dde'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Battleflies'),
    (select id from sets where short_name = 'dde'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Oasis'),
    (select id from sets where short_name = 'dde'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Broodlings'),
    (select id from sets where short_name = 'dde'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fertile Ground'),
    (select id from sets where short_name = 'dde'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coalition Relic'),
    (select id from sets where short_name = 'dde'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quirion Elves'),
    (select id from sets where short_name = 'dde'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treva, the Renewer'),
    (select id from sets where short_name = 'dde'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tendrils of Corruption'),
    (select id from sets where short_name = 'dde'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Rage'),
    (select id from sets where short_name = 'dde'),
    '36',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Negator'),
    (select id from sets where short_name = 'dde'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dde'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rith, the Awakener'),
    (select id from sets where short_name = 'dde'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'dde'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whispersilk Cloak'),
    (select id from sets where short_name = 'dde'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bone Shredder'),
    (select id from sets where short_name = 'dde'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thornscape Battlemage'),
    (select id from sets where short_name = 'dde'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Priest of Gix'),
    (select id from sets where short_name = 'dde'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Death'),
    (select id from sets where short_name = 'dde'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darigaaz''s Charm'),
    (select id from sets where short_name = 'dde'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darigaaz, the Igniter'),
    (select id from sets where short_name = 'dde'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slay'),
    (select id from sets where short_name = 'dde'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Charging Troll'),
    (select id from sets where short_name = 'dde'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hornet Cannon'),
    (select id from sets where short_name = 'dde'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Plaguelord'),
    (select id from sets where short_name = 'dde'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'dde'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dde'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Arena'),
    (select id from sets where short_name = 'dde'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Narrow Escape'),
    (select id from sets where short_name = 'dde'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'dde'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'dde'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'dde'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Debaser'),
    (select id from sets where short_name = 'dde'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gerrard''s Command'),
    (select id from sets where short_name = 'dde'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dde'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dde'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Verduran Emissary'),
    (select id from sets where short_name = 'dde'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tribal Flames'),
    (select id from sets where short_name = 'dde'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Worn Powerstone'),
    (select id from sets where short_name = 'dde'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harrow'),
    (select id from sets where short_name = 'dde'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evasive Action'),
    (select id from sets where short_name = 'dde'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thunderscape Battlemage'),
    (select id from sets where short_name = 'dde'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Gargantua'),
    (select id from sets where short_name = 'dde'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Vault'),
    (select id from sets where short_name = 'dde'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thornscape Apprentice'),
    (select id from sets where short_name = 'dde'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Defiler'),
    (select id from sets where short_name = 'dde'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treva''s Charm'),
    (select id from sets where short_name = 'dde'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elfhame Palace'),
    (select id from sets where short_name = 'dde'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nomadic Elf'),
    (select id from sets where short_name = 'dde'),
    '38',
    'common'
) 
 on conflict do nothing;
