insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Liliana''s Specter'),
    (select id from sets where short_name = 'pm11'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mitotic Slime'),
    (select id from sets where short_name = 'pm11'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Hellkite'),
    (select id from sets where short_name = 'pm11'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sun Titan'),
    (select id from sets where short_name = 'pm11'),
    '35',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = 'pm11'),
    '*165',
    'rare'
) 
 on conflict do nothing;
