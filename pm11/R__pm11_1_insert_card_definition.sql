    insert into mtgcard(name) values ('Liliana''s Specter') on conflict do nothing;
    insert into mtgcard(name) values ('Mitotic Slime') on conflict do nothing;
    insert into mtgcard(name) values ('Ancient Hellkite') on conflict do nothing;
    insert into mtgcard(name) values ('Sun Titan') on conflict do nothing;
    insert into mtgcard(name) values ('Birds of Paradise') on conflict do nothing;
