insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Sage of Ancient Lore // Werewolf of Ancient Hunger'),
    (select id from sets where short_name = 'psoi'),
    '225s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ravenous Bloodseeker'),
    (select id from sets where short_name = 'psoi'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tamiyo''s Journal'),
    (select id from sets where short_name = 'psoi'),
    '265s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relentless Dead'),
    (select id from sets where short_name = 'psoi'),
    '131s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Drogskol Cavalry'),
    (select id from sets where short_name = 'psoi'),
    '15s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Swallower'),
    (select id from sets where short_name = 'psoi'),
    '230s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fortified Village'),
    (select id from sets where short_name = 'psoi'),
    '274s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wolf of Devil''s Breach'),
    (select id from sets where short_name = 'psoi'),
    '192s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Flameblade Angel'),
    (select id from sets where short_name = 'psoi'),
    '157s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Startled Awake // Persistent Nightmare'),
    (select id from sets where short_name = 'psoi'),
    '88s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Epiphany at the Drownyard'),
    (select id from sets where short_name = 'psoi'),
    '59s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elusive Tormentor // Insidious Mist'),
    (select id from sets where short_name = 'psoi'),
    '108s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sigarda, Heron''s Grace'),
    (select id from sets where short_name = 'psoi'),
    '250s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Engulf the Shore'),
    (select id from sets where short_name = 'psoi'),
    '58s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slayer''s Plate'),
    (select id from sets where short_name = 'psoi'),
    '264s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tireless Tracker'),
    (select id from sets where short_name = 'psoi'),
    '233s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corrupted Grafstone'),
    (select id from sets where short_name = 'psoi'),
    '253s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anguished Unmaking'),
    (select id from sets where short_name = 'psoi'),
    '242p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prized Amalgam'),
    (select id from sets where short_name = 'psoi'),
    '249s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Olivia, Mobilized for War'),
    (select id from sets where short_name = 'psoi'),
    '248s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rattlechains'),
    (select id from sets where short_name = 'psoi'),
    '81s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thalia''s Lieutenant'),
    (select id from sets where short_name = 'psoi'),
    '43s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Second Harvest'),
    (select id from sets where short_name = 'psoi'),
    '227s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Descend upon the Sinful'),
    (select id from sets where short_name = 'psoi'),
    '13s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Always Watching'),
    (select id from sets where short_name = 'psoi'),
    '1s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Game Trail'),
    (select id from sets where short_name = 'psoi'),
    '276s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burn from Within'),
    (select id from sets where short_name = 'psoi'),
    '148s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ever After'),
    (select id from sets where short_name = 'psoi'),
    '109s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flameblade Angel'),
    (select id from sets where short_name = 'psoi'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foreboding Ruins'),
    (select id from sets where short_name = 'psoi'),
    '272s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Gitrog Monster'),
    (select id from sets where short_name = 'psoi'),
    '245s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sin Prodder'),
    (select id from sets where short_name = 'psoi'),
    '181s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Invocation of Saint Traft'),
    (select id from sets where short_name = 'psoi'),
    '246s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Incorrigible Youths'),
    (select id from sets where short_name = 'psoi'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Geralf''s Masterpiece'),
    (select id from sets where short_name = 'psoi'),
    '65s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Traverse the Ulvenwald'),
    (select id from sets where short_name = 'psoi'),
    '234s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arlinn Kord // Arlinn, Embraced by the Moon'),
    (select id from sets where short_name = 'psoi'),
    '243s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Confirm Suspicions'),
    (select id from sets where short_name = 'psoi'),
    '53s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eerie Interlude'),
    (select id from sets where short_name = 'psoi'),
    '16p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Declaration in Stone'),
    (select id from sets where short_name = 'psoi'),
    '12s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Swallower'),
    (select id from sets where short_name = 'psoi'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archangel Avacyn // Avacyn, the Purifier'),
    (select id from sets where short_name = 'psoi'),
    '5s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Markov Dreadknight'),
    (select id from sets where short_name = 'psoi'),
    '122s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nahiri, the Harbinger'),
    (select id from sets where short_name = 'psoi'),
    '247s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Harness the Storm'),
    (select id from sets where short_name = 'psoi'),
    '163s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel of Deliverance'),
    (select id from sets where short_name = 'psoi'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seasons Past'),
    (select id from sets where short_name = 'psoi'),
    '226s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Avacyn''s Judgment'),
    (select id from sets where short_name = 'psoi'),
    '145s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel of Deliverance'),
    (select id from sets where short_name = 'psoi'),
    '2s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forgotten Creation'),
    (select id from sets where short_name = 'psoi'),
    '63s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Devils'' Playground'),
    (select id from sets where short_name = 'psoi'),
    '151s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diregraf Colossus'),
    (select id from sets where short_name = 'psoi'),
    '107s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brain in a Jar'),
    (select id from sets where short_name = 'psoi'),
    '252s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Choked Estuary'),
    (select id from sets where short_name = 'psoi'),
    '270s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drogskol Cavalry'),
    (select id from sets where short_name = 'psoi'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Welcome to the Fold'),
    (select id from sets where short_name = 'psoi'),
    '96s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Behold the Beyond'),
    (select id from sets where short_name = 'psoi'),
    '101s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Triskaidekaphobia'),
    (select id from sets where short_name = 'psoi'),
    '141s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anguished Unmaking'),
    (select id from sets where short_name = 'psoi'),
    '242s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jace, Unraveler of Secrets'),
    (select id from sets where short_name = 'psoi'),
    '69s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Asylum Visitor'),
    (select id from sets where short_name = 'psoi'),
    '99s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mindwrack Demon'),
    (select id from sets where short_name = 'psoi'),
    '124s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sorin, Grim Nemesis'),
    (select id from sets where short_name = 'psoi'),
    '251s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Westvale Abbey // Ormendahl, Profane Prince'),
    (select id from sets where short_name = 'psoi'),
    '281s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inexorable Blob'),
    (select id from sets where short_name = 'psoi'),
    '212s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drownyard Temple'),
    (select id from sets where short_name = 'psoi'),
    '271s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'To the Slaughter'),
    (select id from sets where short_name = 'psoi'),
    '139s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cryptolith Rite'),
    (select id from sets where short_name = 'psoi'),
    '200s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goldnight Castigator'),
    (select id from sets where short_name = 'psoi'),
    '162s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bygone Bishop'),
    (select id from sets where short_name = 'psoi'),
    '8s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ulvenwald Hydra'),
    (select id from sets where short_name = 'psoi'),
    '235s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Falkenrath Gorger'),
    (select id from sets where short_name = 'psoi'),
    '155s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eerie Interlude'),
    (select id from sets where short_name = 'psoi'),
    '16s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Markov Dreadknight'),
    (select id from sets where short_name = 'psoi'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Altered Ego'),
    (select id from sets where short_name = 'psoi'),
    '241s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nephalia Moondrakes'),
    (select id from sets where short_name = 'psoi'),
    '75s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Geier Reach Bandit // Vildin-Pack Alpha'),
    (select id from sets where short_name = 'psoi'),
    '159s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Odric, Lunarch Marshal'),
    (select id from sets where short_name = 'psoi'),
    '31s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathcap Cultivator'),
    (select id from sets where short_name = 'psoi'),
    '202s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fevered Visions'),
    (select id from sets where short_name = 'psoi'),
    '244s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nephalia Moondrakes'),
    (select id from sets where short_name = 'psoi'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elusive Tormentor // Insidious Mist'),
    (select id from sets where short_name = 'psoi'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hanweir Militia Captain // Westvale Cult Leader'),
    (select id from sets where short_name = 'psoi'),
    '21s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tamiyo''s Journal'),
    (select id from sets where short_name = 'psoi'),
    '265s†',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anguished Unmaking'),
    (select id from sets where short_name = 'psoi'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Port Town'),
    (select id from sets where short_name = 'psoi'),
    '278s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scourge Wolf'),
    (select id from sets where short_name = 'psoi'),
    '179s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silverfur Partisan'),
    (select id from sets where short_name = 'psoi'),
    '228s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'From Under the Floorboards'),
    (select id from sets where short_name = 'psoi'),
    '111s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thing in the Ice // Awoken Horror'),
    (select id from sets where short_name = 'psoi'),
    '92s',
    'rare'
) 
 on conflict do nothing;
