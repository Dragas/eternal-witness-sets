insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Myr'),
    (select id from sets where short_name = 'tsom'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurm'),
    (select id from sets where short_name = 'tsom'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat'),
    (select id from sets where short_name = 'tsom'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'tsom'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tsom'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Poison Counter'),
    (select id from sets where short_name = 'tsom'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insect'),
    (select id from sets where short_name = 'tsom'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tsom'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golem'),
    (select id from sets where short_name = 'tsom'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurm'),
    (select id from sets where short_name = 'tsom'),
    '9',
    'common'
) 
 on conflict do nothing;
