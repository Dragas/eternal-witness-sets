    insert into mtgcard(name) values ('Myr') on conflict do nothing;
    insert into mtgcard(name) values ('Wurm') on conflict do nothing;
    insert into mtgcard(name) values ('Cat') on conflict do nothing;
    insert into mtgcard(name) values ('Wolf') on conflict do nothing;
    insert into mtgcard(name) values ('Soldier') on conflict do nothing;
    insert into mtgcard(name) values ('Poison Counter') on conflict do nothing;
    insert into mtgcard(name) values ('Insect') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin') on conflict do nothing;
    insert into mtgcard(name) values ('Golem') on conflict do nothing;
