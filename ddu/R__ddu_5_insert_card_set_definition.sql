insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Tranquil Thicket'),
    (select id from sets where short_name = 'ddu'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief, the Vastwood'),
    (select id from sets where short_name = 'ddu'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jagged-Scar Archers'),
    (select id from sets where short_name = 'ddu'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maverick Thopterist'),
    (select id from sets where short_name = 'ddu'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pia and Kiran Nalaar'),
    (select id from sets where short_name = 'ddu'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treetop Village'),
    (select id from sets where short_name = 'ddu'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scuttling Doom Engine'),
    (select id from sets where short_name = 'ddu'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Welder'),
    (select id from sets where short_name = 'ddu'),
    '35',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Elvish Archdruid'),
    (select id from sets where short_name = 'ddu'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yeva, Nature''s Herald'),
    (select id from sets where short_name = 'ddu'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seat of the Synod'),
    (select id from sets where short_name = 'ddu'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riddlesmith'),
    (select id from sets where short_name = 'ddu'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nature''s Way'),
    (select id from sets where short_name = 'ddu'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Myr Battlesphere'),
    (select id from sets where short_name = 'ddu'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treasure Mage'),
    (select id from sets where short_name = 'ddu'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Talara''s Battalion'),
    (select id from sets where short_name = 'ddu'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = 'ddu'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wildheart Invoker'),
    (select id from sets where short_name = 'ddu'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Aberration'),
    (select id from sets where short_name = 'ddu'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voyager Staff'),
    (select id from sets where short_name = 'ddu'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Advocate'),
    (select id from sets where short_name = 'ddu'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Epiphany'),
    (select id from sets where short_name = 'ddu'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gladehart Cavalry'),
    (select id from sets where short_name = 'ddu'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trophy Mage'),
    (select id from sets where short_name = 'ddu'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddu'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ichor Wellspring'),
    (select id from sets where short_name = 'ddu'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddu'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whirler Rogue'),
    (select id from sets where short_name = 'ddu'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krosan Tusker'),
    (select id from sets where short_name = 'ddu'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Etherium Sculptor'),
    (select id from sets where short_name = 'ddu'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Neurok Replica'),
    (select id from sets where short_name = 'ddu'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darksteel Plate'),
    (select id from sets where short_name = 'ddu'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foundry of the Consuls'),
    (select id from sets where short_name = 'ddu'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kujar Seedsculptor'),
    (select id from sets where short_name = 'ddu'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Vanguard'),
    (select id from sets where short_name = 'ddu'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fierce Empath'),
    (select id from sets where short_name = 'ddu'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwynen, Gilt-Leaf Daen'),
    (select id from sets where short_name = 'ddu'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddu'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ezuri, Renegade Leader'),
    (select id from sets where short_name = 'ddu'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dwynen''s Elite'),
    (select id from sets where short_name = 'ddu'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llanowar Empath'),
    (select id from sets where short_name = 'ddu'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexia''s Core'),
    (select id from sets where short_name = 'ddu'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Myr Sire'),
    (select id from sets where short_name = 'ddu'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Welding Sparks'),
    (select id from sets where short_name = 'ddu'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inventor''s Goggles'),
    (select id from sets where short_name = 'ddu'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter Assembly'),
    (select id from sets where short_name = 'ddu'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Filigree Familiar'),
    (select id from sets where short_name = 'ddu'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barrage Ogre'),
    (select id from sets where short_name = 'ddu'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trinket Mage'),
    (select id from sets where short_name = 'ddu'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swiftwater Cliffs'),
    (select id from sets where short_name = 'ddu'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Regal Force'),
    (select id from sets where short_name = 'ddu'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reclusive Artificer'),
    (select id from sets where short_name = 'ddu'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Solemn Simulacrum'),
    (select id from sets where short_name = 'ddu'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shrapnel Blast'),
    (select id from sets where short_name = 'ddu'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Reef'),
    (select id from sets where short_name = 'ddu'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lead the Stampede'),
    (select id from sets where short_name = 'ddu'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddu'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyrite Spellbomb'),
    (select id from sets where short_name = 'ddu'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Mechanist'),
    (select id from sets where short_name = 'ddu'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddu'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viridian Shaman'),
    (select id from sets where short_name = 'ddu'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddu'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Great Furnace'),
    (select id from sets where short_name = 'ddu'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Mystic'),
    (select id from sets where short_name = 'ddu'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ivy Lane Denizen'),
    (select id from sets where short_name = 'ddu'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darksteel Citadel'),
    (select id from sets where short_name = 'ddu'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddu'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Galvanic Blast'),
    (select id from sets where short_name = 'ddu'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddu'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghirapur Gearcrafter'),
    (select id from sets where short_name = 'ddu'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leaf Gilder'),
    (select id from sets where short_name = 'ddu'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nissa''s Judgment'),
    (select id from sets where short_name = 'ddu'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Branchbender'),
    (select id from sets where short_name = 'ddu'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Artificer''s Epiphany'),
    (select id from sets where short_name = 'ddu'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mycosynth Wellspring'),
    (select id from sets where short_name = 'ddu'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ezuri''s Archers'),
    (select id from sets where short_name = 'ddu'),
    '9',
    'common'
) 
 on conflict do nothing;
