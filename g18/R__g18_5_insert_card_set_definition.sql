insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Immortal Phoenix'),
    (select id from sets where short_name = 'g18'),
    'GP4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rampaging Brontodon'),
    (select id from sets where short_name = 'g18'),
    'GP5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vengeant Vampire'),
    (select id from sets where short_name = 'g18'),
    'GP3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angler Turtle'),
    (select id from sets where short_name = 'g18'),
    'GP2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angelic Guardian'),
    (select id from sets where short_name = 'g18'),
    'GP1',
    'rare'
) 
 on conflict do nothing;
