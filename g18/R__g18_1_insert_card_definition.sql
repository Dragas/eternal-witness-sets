    insert into mtgcard(name) values ('Immortal Phoenix') on conflict do nothing;
    insert into mtgcard(name) values ('Rampaging Brontodon') on conflict do nothing;
    insert into mtgcard(name) values ('Vengeant Vampire') on conflict do nothing;
    insert into mtgcard(name) values ('Angler Turtle') on conflict do nothing;
    insert into mtgcard(name) values ('Angelic Guardian') on conflict do nothing;
