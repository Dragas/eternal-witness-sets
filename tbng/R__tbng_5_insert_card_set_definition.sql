insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tbng'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'tbng'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat Soldier'),
    (select id from sets where short_name = 'tbng'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tbng'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Centaur'),
    (select id from sets where short_name = 'tbng'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tbng'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird'),
    (select id from sets where short_name = 'tbng'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird'),
    (select id from sets where short_name = 'tbng'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kiora, the Crashing Wave Emblem'),
    (select id from sets where short_name = 'tbng'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gold'),
    (select id from sets where short_name = 'tbng'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kraken'),
    (select id from sets where short_name = 'tbng'),
    '5',
    'common'
) 
 on conflict do nothing;
