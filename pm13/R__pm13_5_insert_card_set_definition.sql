insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Magmaquake'),
    (select id from sets where short_name = 'pm13'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cathedral of War'),
    (select id from sets where short_name = 'pm13'),
    '*221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Staff of Nin'),
    (select id from sets where short_name = 'pm13'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Xathrid Gorgon'),
    (select id from sets where short_name = 'pm13'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mwonvuli Beast Tracker'),
    (select id from sets where short_name = 'pm13'),
    '177',
    'uncommon'
) 
 on conflict do nothing;
