    insert into types (name) values('Instant') on conflict do nothing;
    insert into types (name) values('Land') on conflict do nothing;
    insert into types (name) values('Artifact') on conflict do nothing;
    insert into types (name) values('Creature') on conflict do nothing;
    insert into types (name) values('Gorgon') on conflict do nothing;
    insert into types (name) values('Creature') on conflict do nothing;
    insert into types (name) values('Human') on conflict do nothing;
    insert into types (name) values('Scout') on conflict do nothing;
