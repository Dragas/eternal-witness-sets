insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Glacial Ray'),
    (select id from sets where short_name = 'pal04'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Mime'),
    (select id from sets where short_name = 'pal04'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Granny''s Payback'),
    (select id from sets where short_name = 'pal04'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darksteel Ingot'),
    (select id from sets where short_name = 'pal04'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mise'),
    (select id from sets where short_name = 'pal04'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Coupon'),
    (select id from sets where short_name = 'pal04'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serum Visions'),
    (select id from sets where short_name = 'pal04'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pal04'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pal04'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pal04'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Art'),
    (select id from sets where short_name = 'pal04'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pal04'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Booster Tutor'),
    (select id from sets where short_name = 'pal04'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pal04'),
    '2',
    'rare'
) 
 on conflict do nothing;
