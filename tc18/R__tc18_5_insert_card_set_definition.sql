insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Cat Warrior'),
    (select id from sets where short_name = 'tc18'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Egg'),
    (select id from sets where short_name = 'tc18'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tc18'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tc18'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat'),
    (select id from sets where short_name = 'tc18'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tc18'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter'),
    (select id from sets where short_name = 'tc18'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr'),
    (select id from sets where short_name = 'tc18'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'tc18'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Servo'),
    (select id from sets where short_name = 'tc18'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Worm'),
    (select id from sets where short_name = 'tc18'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Construct'),
    (select id from sets where short_name = 'tc18'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plant'),
    (select id from sets where short_name = 'tc18'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Construct'),
    (select id from sets where short_name = 'tc18'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter'),
    (select id from sets where short_name = 'tc18'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clue'),
    (select id from sets where short_name = 'tc18'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter'),
    (select id from sets where short_name = 'tc18'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tc18'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horror'),
    (select id from sets where short_name = 'tc18'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mask'),
    (select id from sets where short_name = 'tc18'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tc18'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shapeshifter'),
    (select id from sets where short_name = 'tc18'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manifest'),
    (select id from sets where short_name = 'tc18'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr'),
    (select id from sets where short_name = 'tc18'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Survivor'),
    (select id from sets where short_name = 'tc18'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tc18'),
    '9',
    'common'
) 
 on conflict do nothing;
