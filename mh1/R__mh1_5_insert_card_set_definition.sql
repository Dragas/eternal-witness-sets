insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Hogaak, Arisen Necropolis'),
    (select id from sets where short_name = 'mh1'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Force of Vigor'),
    (select id from sets where short_name = 'mh1'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Blossoms'),
    (select id from sets where short_name = 'mh1'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The First Sliver'),
    (select id from sets where short_name = 'mh1'),
    '200',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Exclude'),
    (select id from sets where short_name = 'mh1'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Changeling Outcast'),
    (select id from sets where short_name = 'mh1'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Collector Ouphe'),
    (select id from sets where short_name = 'mh1'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ninja of the New Moon'),
    (select id from sets where short_name = 'mh1'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carrion Feeder'),
    (select id from sets where short_name = 'mh1'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Force of Rage'),
    (select id from sets where short_name = 'mh1'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Waterlogged Grove'),
    (select id from sets where short_name = 'mh1'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prohibit'),
    (select id from sets where short_name = 'mh1'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pashalik Mons'),
    (select id from sets where short_name = 'mh1'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vengeful Devil'),
    (select id from sets where short_name = 'mh1'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Graveshifter'),
    (select id from sets where short_name = 'mh1'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Birthing Boughs'),
    (select id from sets where short_name = 'mh1'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Face of Divinity'),
    (select id from sets where short_name = 'mh1'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Watcher for Tomorrow'),
    (select id from sets where short_name = 'mh1'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Altar of Dementia'),
    (select id from sets where short_name = 'mh1'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reckless Charge'),
    (select id from sets where short_name = 'mh1'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Answered Prayers'),
    (select id from sets where short_name = 'mh1'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kaya''s Guile'),
    (select id from sets where short_name = 'mh1'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shelter'),
    (select id from sets where short_name = 'mh1'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ayula''s Influence'),
    (select id from sets where short_name = 'mh1'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Forest'),
    (select id from sets where short_name = 'mh1'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abominable Treefolk'),
    (select id from sets where short_name = 'mh1'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winter''s Rest'),
    (select id from sets where short_name = 'mh1'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Matron'),
    (select id from sets where short_name = 'mh1'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blizzard Strix'),
    (select id from sets where short_name = 'mh1'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Geomancer''s Gambit'),
    (select id from sets where short_name = 'mh1'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nurturing Peatland'),
    (select id from sets where short_name = 'mh1'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quakefoot Cyclops'),
    (select id from sets where short_name = 'mh1'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudshredder Sliver'),
    (select id from sets where short_name = 'mh1'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thundering Djinn'),
    (select id from sets where short_name = 'mh1'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin War Party'),
    (select id from sets where short_name = 'mh1'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sword of Truth and Justice'),
    (select id from sets where short_name = 'mh1'),
    '229',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rotwidow Pack'),
    (select id from sets where short_name = 'mh1'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ephemerate'),
    (select id from sets where short_name = 'mh1'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stirring Address'),
    (select id from sets where short_name = 'mh1'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seasoned Pyromancer'),
    (select id from sets where short_name = 'mh1'),
    '145',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cunning Evasion'),
    (select id from sets where short_name = 'mh1'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vesperlark'),
    (select id from sets where short_name = 'mh1'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deep Forest Hermit'),
    (select id from sets where short_name = 'mh1'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Putrid Goblin'),
    (select id from sets where short_name = 'mh1'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Endling'),
    (select id from sets where short_name = 'mh1'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nantuko Cultivator'),
    (select id from sets where short_name = 'mh1'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Oriflamme'),
    (select id from sets where short_name = 'mh1'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tectonic Reformation'),
    (select id from sets where short_name = 'mh1'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kess, Dissident Mage'),
    (select id from sets where short_name = 'mh1'),
    '206',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Volatile Claws'),
    (select id from sets where short_name = 'mh1'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winds of Abandon'),
    (select id from sets where short_name = 'mh1'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mob'),
    (select id from sets where short_name = 'mh1'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scale Up'),
    (select id from sets where short_name = 'mh1'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rain of Revelation'),
    (select id from sets where short_name = 'mh1'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feaster of Fools'),
    (select id from sets where short_name = 'mh1'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flusterstorm'),
    (select id from sets where short_name = 'mh1'),
    '255',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Murasa Behemoth'),
    (select id from sets where short_name = 'mh1'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silumgar Scavenger'),
    (select id from sets where short_name = 'mh1'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Good-Fortune Unicorn'),
    (select id from sets where short_name = 'mh1'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eyekite'),
    (select id from sets where short_name = 'mh1'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrenn and Six'),
    (select id from sets where short_name = 'mh1'),
    '217',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dregscape Sliver'),
    (select id from sets where short_name = 'mh1'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Venomous Changeling'),
    (select id from sets where short_name = 'mh1'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talisman of Creativity'),
    (select id from sets where short_name = 'mh1'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giver of Runes'),
    (select id from sets where short_name = 'mh1'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winding Way'),
    (select id from sets where short_name = 'mh1'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diabolic Edict'),
    (select id from sets where short_name = 'mh1'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marit Lage''s Slumber'),
    (select id from sets where short_name = 'mh1'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fists of Flame'),
    (select id from sets where short_name = 'mh1'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cleaving Sliver'),
    (select id from sets where short_name = 'mh1'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravenous Giant'),
    (select id from sets where short_name = 'mh1'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Universal Automaton'),
    (select id from sets where short_name = 'mh1'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lonely Sandbar'),
    (select id from sets where short_name = 'mh1'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fact or Fiction'),
    (select id from sets where short_name = 'mh1'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Hellraiser'),
    (select id from sets where short_name = 'mh1'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Skelemental'),
    (select id from sets where short_name = 'mh1'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archmage''s Charm'),
    (select id from sets where short_name = 'mh1'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shenanigans'),
    (select id from sets where short_name = 'mh1'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruination Rioter'),
    (select id from sets where short_name = 'mh1'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sisay, Weatherlight Captain'),
    (select id from sets where short_name = 'mh1'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Defile'),
    (select id from sets where short_name = 'mh1'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Astral Drift'),
    (select id from sets where short_name = 'mh1'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Rage'),
    (select id from sets where short_name = 'mh1'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Man-o''-War'),
    (select id from sets where short_name = 'mh1'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smiting Helix'),
    (select id from sets where short_name = 'mh1'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Springbloom Druid'),
    (select id from sets where short_name = 'mh1'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhox Veteran'),
    (select id from sets where short_name = 'mh1'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krosan Tusker'),
    (select id from sets where short_name = 'mh1'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Future Sight'),
    (select id from sets where short_name = 'mh1'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sadistic Obsession'),
    (select id from sets where short_name = 'mh1'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spiteful Sliver'),
    (select id from sets where short_name = 'mh1'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mist-Syndicate Naga'),
    (select id from sets where short_name = 'mh1'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treefolk Umbra'),
    (select id from sets where short_name = 'mh1'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Rake'),
    (select id from sets where short_name = 'mh1'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tempered Sliver'),
    (select id from sets where short_name = 'mh1'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Squirrel Nest'),
    (select id from sets where short_name = 'mh1'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ice-Fang Coatl'),
    (select id from sets where short_name = 'mh1'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Throes of Chaos'),
    (select id from sets where short_name = 'mh1'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tribute Mage'),
    (select id from sets where short_name = 'mh1'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spore Frog'),
    (select id from sets where short_name = 'mh1'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of Old Benalia'),
    (select id from sets where short_name = 'mh1'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enduring Sliver'),
    (select id from sets where short_name = 'mh1'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unbound Flourishing'),
    (select id from sets where short_name = 'mh1'),
    '189',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Etchings of the Chosen'),
    (select id from sets where short_name = 'mh1'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bladeback Sliver'),
    (select id from sets where short_name = 'mh1'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goatnap'),
    (select id from sets where short_name = 'mh1'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dismantling Blow'),
    (select id from sets where short_name = 'mh1'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Talisman of Conviction'),
    (select id from sets where short_name = 'mh1'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llanowar Tribe'),
    (select id from sets where short_name = 'mh1'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Settle Beyond Reality'),
    (select id from sets where short_name = 'mh1'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rank Officer'),
    (select id from sets where short_name = 'mh1'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Plains'),
    (select id from sets where short_name = 'mh1'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Martyr''s Soul'),
    (select id from sets where short_name = 'mh1'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scour All Possibilities'),
    (select id from sets where short_name = 'mh1'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Twin-Silk Spider'),
    (select id from sets where short_name = 'mh1'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hollowhead Sliver'),
    (select id from sets where short_name = 'mh1'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shatter Assumptions'),
    (select id from sets where short_name = 'mh1'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chillerpillar'),
    (select id from sets where short_name = 'mh1'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'First Sliver''s Chosen'),
    (select id from sets where short_name = 'mh1'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Talisman of Hierarchy'),
    (select id from sets where short_name = 'mh1'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nether Spirit'),
    (select id from sets where short_name = 'mh1'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Impostor of the Sixth Pride'),
    (select id from sets where short_name = 'mh1'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conifer Wurm'),
    (select id from sets where short_name = 'mh1'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Planebound Accomplice'),
    (select id from sets where short_name = 'mh1'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yawgmoth, Thran Physician'),
    (select id from sets where short_name = 'mh1'),
    '116',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Spinehorn Minotaur'),
    (select id from sets where short_name = 'mh1'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Firebolt'),
    (select id from sets where short_name = 'mh1'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Headless Specter'),
    (select id from sets where short_name = 'mh1'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra the Benevolent'),
    (select id from sets where short_name = 'mh1'),
    '26',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Windcaller Aven'),
    (select id from sets where short_name = 'mh1'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scrapyard Recombiner'),
    (select id from sets where short_name = 'mh1'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hexdrinker'),
    (select id from sets where short_name = 'mh1'),
    '168',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nimble Mongoose'),
    (select id from sets where short_name = 'mh1'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trustworthy Scout'),
    (select id from sets where short_name = 'mh1'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Island'),
    (select id from sets where short_name = 'mh1'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stream of Thought'),
    (select id from sets where short_name = 'mh1'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rime Tender'),
    (select id from sets where short_name = 'mh1'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Amorphous Axe'),
    (select id from sets where short_name = 'mh1'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plague Engineer'),
    (select id from sets where short_name = 'mh1'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Force of Despair'),
    (select id from sets where short_name = 'mh1'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gilded Light'),
    (select id from sets where short_name = 'mh1'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fallen Shinobi'),
    (select id from sets where short_name = 'mh1'),
    '199',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Webweaver Changeling'),
    (select id from sets where short_name = 'mh1'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ingenious Infiltrator'),
    (select id from sets where short_name = 'mh1'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Irregular Cohort'),
    (select id from sets where short_name = 'mh1'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magmatic Sinkhole'),
    (select id from sets where short_name = 'mh1'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gluttonous Slug'),
    (select id from sets where short_name = 'mh1'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Champion'),
    (select id from sets where short_name = 'mh1'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aria of Flame'),
    (select id from sets where short_name = 'mh1'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crypt Rats'),
    (select id from sets where short_name = 'mh1'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Morophon, the Boundless'),
    (select id from sets where short_name = 'mh1'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Force of Virtue'),
    (select id from sets where short_name = 'mh1'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Igneous Elemental'),
    (select id from sets where short_name = 'mh1'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Generous Gift'),
    (select id from sets where short_name = 'mh1'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'String of Disappearances'),
    (select id from sets where short_name = 'mh1'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nature''s Chant'),
    (select id from sets where short_name = 'mh1'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lancer Sliver'),
    (select id from sets where short_name = 'mh1'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Genesis'),
    (select id from sets where short_name = 'mh1'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forgotten Cave'),
    (select id from sets where short_name = 'mh1'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bazaar Trademage'),
    (select id from sets where short_name = 'mh1'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza, Lord High Artificer'),
    (select id from sets where short_name = 'mh1'),
    '75',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Saddled Rimestag'),
    (select id from sets where short_name = 'mh1'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantom Ninja'),
    (select id from sets where short_name = 'mh1'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fiery Islet'),
    (select id from sets where short_name = 'mh1'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Engineer'),
    (select id from sets where short_name = 'mh1'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'First-Sphere Gargantua'),
    (select id from sets where short_name = 'mh1'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Munitions Expert'),
    (select id from sets where short_name = 'mh1'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crashing Footfalls'),
    (select id from sets where short_name = 'mh1'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cordial Vampire'),
    (select id from sets where short_name = 'mh1'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silent Clearing'),
    (select id from sets where short_name = 'mh1'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Valiant Changeling'),
    (select id from sets where short_name = 'mh1'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ranger-Captain of Eos'),
    (select id from sets where short_name = 'mh1'),
    '21',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Umezawa''s Charm'),
    (select id from sets where short_name = 'mh1'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fountain of Ichor'),
    (select id from sets where short_name = 'mh1'),
    '223',
    'common'
) ,
(
    (select id from mtgcard where name = 'Throatseeker'),
    (select id from sets where short_name = 'mh1'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trumpeting Herd'),
    (select id from sets where short_name = 'mh1'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reprobation'),
    (select id from sets where short_name = 'mh1'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lava Dart'),
    (select id from sets where short_name = 'mh1'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icehide Golem'),
    (select id from sets where short_name = 'mh1'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ransack the Lab'),
    (select id from sets where short_name = 'mh1'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hall of Heliod''s Generosity'),
    (select id from sets where short_name = 'mh1'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Segovian Angel'),
    (select id from sets where short_name = 'mh1'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unearth'),
    (select id from sets where short_name = 'mh1'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul-Strike Technique'),
    (select id from sets where short_name = 'mh1'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tranquil Thicket'),
    (select id from sets where short_name = 'mh1'),
    '248',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'On Thin Ice'),
    (select id from sets where short_name = 'mh1'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zhalfirin Decoy'),
    (select id from sets where short_name = 'mh1'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Collected Conjuring'),
    (select id from sets where short_name = 'mh1'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pillage'),
    (select id from sets where short_name = 'mh1'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frostwalk Bastion'),
    (select id from sets where short_name = 'mh1'),
    '240',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ayula, Queen Among Bears'),
    (select id from sets where short_name = 'mh1'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword of Sinew and Steel'),
    (select id from sets where short_name = 'mh1'),
    '228',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pondering Mage'),
    (select id from sets where short_name = 'mh1'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reap the Past'),
    (select id from sets where short_name = 'mh1'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcum''s Astrolabe'),
    (select id from sets where short_name = 'mh1'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunbaked Canyon'),
    (select id from sets where short_name = 'mh1'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lesser Masticore'),
    (select id from sets where short_name = 'mh1'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'King of the Pride'),
    (select id from sets where short_name = 'mh1'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Splicer''s Skill'),
    (select id from sets where short_name = 'mh1'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Return from Extinction'),
    (select id from sets where short_name = 'mh1'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undead Augur'),
    (select id from sets where short_name = 'mh1'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Swamp'),
    (select id from sets where short_name = 'mh1'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oneirophage'),
    (select id from sets where short_name = 'mh1'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Secluded Steppe'),
    (select id from sets where short_name = 'mh1'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Talisman of Resilience'),
    (select id from sets where short_name = 'mh1'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bellowing Elk'),
    (select id from sets where short_name = 'mh1'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Seer'),
    (select id from sets where short_name = 'mh1'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Mountain'),
    (select id from sets where short_name = 'mh1'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Excavating Anurid'),
    (select id from sets where short_name = 'mh1'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Regrowth'),
    (select id from sets where short_name = 'mh1'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mirrodin Besieged'),
    (select id from sets where short_name = 'mh1'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Iceberg Cancrix'),
    (select id from sets where short_name = 'mh1'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cave of Temptation'),
    (select id from sets where short_name = 'mh1'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mox Tantalite'),
    (select id from sets where short_name = 'mh1'),
    '226',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Barren Moor'),
    (select id from sets where short_name = 'mh1'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smoke Shroud'),
    (select id from sets where short_name = 'mh1'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warteye Witch'),
    (select id from sets where short_name = 'mh1'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scuttling Sliver'),
    (select id from sets where short_name = 'mh1'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treetop Ambusher'),
    (select id from sets where short_name = 'mh1'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prismatic Vista'),
    (select id from sets where short_name = 'mh1'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alpine Guide'),
    (select id from sets where short_name = 'mh1'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Fury'),
    (select id from sets where short_name = 'mh1'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Everdream'),
    (select id from sets where short_name = 'mh1'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ore-Scale Guardian'),
    (select id from sets where short_name = 'mh1'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Force of Negation'),
    (select id from sets where short_name = 'mh1'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soulherder'),
    (select id from sets where short_name = 'mh1'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eladamri''s Call'),
    (select id from sets where short_name = 'mh1'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cabal Therapist'),
    (select id from sets where short_name = 'mh1'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Battle Screech'),
    (select id from sets where short_name = 'mh1'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thornado'),
    (select id from sets where short_name = 'mh1'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unsettled Mariner'),
    (select id from sets where short_name = 'mh1'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Choking Tethers'),
    (select id from sets where short_name = 'mh1'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Farmstead Gleaner'),
    (select id from sets where short_name = 'mh1'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savage Swipe'),
    (select id from sets where short_name = 'mh1'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spell Snuff'),
    (select id from sets where short_name = 'mh1'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mother Bear'),
    (select id from sets where short_name = 'mh1'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Form'),
    (select id from sets where short_name = 'mh1'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Twisted Reflection'),
    (select id from sets where short_name = 'mh1'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dead of Winter'),
    (select id from sets where short_name = 'mh1'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moonblade Shinobi'),
    (select id from sets where short_name = 'mh1'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of One Thousand Cuts'),
    (select id from sets where short_name = 'mh1'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glacial Revelation'),
    (select id from sets where short_name = 'mh1'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azra Smokeshaper'),
    (select id from sets where short_name = 'mh1'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weather the Storm'),
    (select id from sets where short_name = 'mh1'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lavabelly Sliver'),
    (select id from sets where short_name = 'mh1'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viashino Sandsprinter'),
    (select id from sets where short_name = 'mh1'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sling-Gang Lieutenant'),
    (select id from sets where short_name = 'mh1'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wing Shards'),
    (select id from sets where short_name = 'mh1'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frostwalla'),
    (select id from sets where short_name = 'mh1'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Recruit the Worthy'),
    (select id from sets where short_name = 'mh1'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talisman of Curiosity'),
    (select id from sets where short_name = 'mh1'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rebuild'),
    (select id from sets where short_name = 'mh1'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bogardan Dragonheart'),
    (select id from sets where short_name = 'mh1'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyrophobia'),
    (select id from sets where short_name = 'mh1'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Echo of Eons'),
    (select id from sets where short_name = 'mh1'),
    '46',
    'mythic'
) 
 on conflict do nothing;
