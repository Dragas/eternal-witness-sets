insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Living Plane'),
    (select id from sets where short_name = 'leg'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pradesh Gypsies'),
    (select id from sets where short_name = 'leg'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ayesha Tanaka'),
    (select id from sets where short_name = 'leg'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voodoo Doll'),
    (select id from sets where short_name = 'leg'),
    '298',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Backfire'),
    (select id from sets where short_name = 'leg'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aerathi Berserker'),
    (select id from sets where short_name = 'leg'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Revelation'),
    (select id from sets where short_name = 'leg'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Putrid Flesh'),
    (select id from sets where short_name = 'leg'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hunding Gjornersen'),
    (select id from sets where short_name = 'leg'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dream Coat'),
    (select id from sets where short_name = 'leg'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Devouring Deep'),
    (select id from sets where short_name = 'leg'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain Yeti'),
    (select id from sets where short_name = 'leg'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arboria'),
    (select id from sets where short_name = 'leg'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Willow Satyr'),
    (select id from sets where short_name = 'leg'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deadfall'),
    (select id from sets where short_name = 'leg'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barktooth Warbeard'),
    (select id from sets where short_name = 'leg'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fallen Angel'),
    (select id from sets where short_name = 'leg'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Greed'),
    (select id from sets where short_name = 'leg'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea Kings'' Blessing'),
    (select id from sets where short_name = 'leg'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Venarian Gold'),
    (select id from sets where short_name = 'leg'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Subdue'),
    (select id from sets where short_name = 'leg'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Imprison'),
    (select id from sets where short_name = 'leg'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crimson Kobolds'),
    (select id from sets where short_name = 'leg'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Wretched'),
    (select id from sets where short_name = 'leg'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Horn of Deafening'),
    (select id from sets where short_name = 'leg'),
    '280',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hazezon Tamar'),
    (select id from sets where short_name = 'leg'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winds of Change'),
    (select id from sets where short_name = 'leg'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'All Hallow''s Eve'),
    (select id from sets where short_name = 'leg'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cosmic Horror'),
    (select id from sets where short_name = 'leg'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teleport'),
    (select id from sets where short_name = 'leg'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kobold Taskmaster'),
    (select id from sets where short_name = 'leg'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quarum Trench Gnomes'),
    (select id from sets where short_name = 'leg'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reset'),
    (select id from sets where short_name = 'leg'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Presence of the Master'),
    (select id from sets where short_name = 'leg'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghosts of the Damned'),
    (select id from sets where short_name = 'leg'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Caverns of Despair'),
    (select id from sets where short_name = 'leg'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Visions'),
    (select id from sets where short_name = 'leg'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vaevictis Asmadi'),
    (select id from sets where short_name = 'leg'),
    '269',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tor Wauki'),
    (select id from sets where short_name = 'leg'),
    '265',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Typhoon'),
    (select id from sets where short_name = 'leg'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zephyr Falcon'),
    (select id from sets where short_name = 'leg'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyrotechnics'),
    (select id from sets where short_name = 'leg'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infernal Medusa'),
    (select id from sets where short_name = 'leg'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hornet Cobra'),
    (select id from sets where short_name = 'leg'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alabaster Potion'),
    (select id from sets where short_name = 'leg'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hammerheim'),
    (select id from sets where short_name = 'leg'),
    '302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dwarven Song'),
    (select id from sets where short_name = 'leg'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lord Magnus'),
    (select id from sets where short_name = 'leg'),
    '243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brine Hag'),
    (select id from sets where short_name = 'leg'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Opposition'),
    (select id from sets where short_name = 'leg'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chains of Mephistopheles'),
    (select id from sets where short_name = 'leg'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcades Sabboth'),
    (select id from sets where short_name = 'leg'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ragnar'),
    (select id from sets where short_name = 'leg'),
    '250',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Killer Bees'),
    (select id from sets where short_name = 'leg'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nether Void'),
    (select id from sets where short_name = 'leg'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Halfdane'),
    (select id from sets where short_name = 'leg'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cleanse'),
    (select id from sets where short_name = 'leg'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pavel Maliki'),
    (select id from sets where short_name = 'leg'),
    '248',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain Stronghold'),
    (select id from sets where short_name = 'leg'),
    '304',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sir Shandlar of Eberyn'),
    (select id from sets where short_name = 'leg'),
    '257',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karakas'),
    (select id from sets where short_name = 'leg'),
    '303',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firestorm Phoenix'),
    (select id from sets where short_name = 'leg'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Adventurers'' Guildhouse'),
    (select id from sets where short_name = 'leg'),
    '300',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Transmutation'),
    (select id from sets where short_name = 'leg'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Recall'),
    (select id from sets where short_name = 'leg'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glyph of Doom'),
    (select id from sets where short_name = 'leg'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hell''s Caretaker'),
    (select id from sets where short_name = 'leg'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blue Mana Battery'),
    (select id from sets where short_name = 'leg'),
    '275',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'White Mana Battery'),
    (select id from sets where short_name = 'leg'),
    '299',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hellfire'),
    (select id from sets where short_name = 'leg'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Red Mana Battery'),
    (select id from sets where short_name = 'leg'),
    '291',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Energy Tap'),
    (select id from sets where short_name = 'leg'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gabriel Angelfire'),
    (select id from sets where short_name = 'leg'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm Seeker'),
    (select id from sets where short_name = 'leg'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Concordant Crossroads'),
    (select id from sets where short_name = 'leg'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flash Counter'),
    (select id from sets where short_name = 'leg'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Slug'),
    (select id from sets where short_name = 'leg'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hyperion Blacksmith'),
    (select id from sets where short_name = 'leg'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marble Priest'),
    (select id from sets where short_name = 'leg'),
    '286',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kasimir the Lone Wolf'),
    (select id from sets where short_name = 'leg'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heaven''s Gate'),
    (select id from sets where short_name = 'leg'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Green Mana Battery'),
    (select id from sets where short_name = 'leg'),
    '279',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gwendlyn Di Corci'),
    (select id from sets where short_name = 'leg'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cyclopean Mummy'),
    (select id from sets where short_name = 'leg'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kobold Overlord'),
    (select id from sets where short_name = 'leg'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Part Water'),
    (select id from sets where short_name = 'leg'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shimian Night Stalker'),
    (select id from sets where short_name = 'leg'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Master of the Hunt'),
    (select id from sets where short_name = 'leg'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lifeblood'),
    (select id from sets where short_name = 'leg'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Life Chisel'),
    (select id from sets where short_name = 'leg'),
    '283',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rasputin Dreamweaver'),
    (select id from sets where short_name = 'leg'),
    '253',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eureka'),
    (select id from sets where short_name = 'leg'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darkness'),
    (select id from sets where short_name = 'leg'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Tombstones'),
    (select id from sets where short_name = 'leg'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angus Mackenzie'),
    (select id from sets where short_name = 'leg'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunastian Falconer'),
    (select id from sets where short_name = 'leg'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Backdraft'),
    (select id from sets where short_name = 'leg'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flash Flood'),
    (select id from sets where short_name = 'leg'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sentinel'),
    (select id from sets where short_name = 'leg'),
    '294',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akron Legionnaire'),
    (select id from sets where short_name = 'leg'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Al-abara''s Carpet'),
    (select id from sets where short_name = 'leg'),
    '271',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Greater Realm of Preservation'),
    (select id from sets where short_name = 'leg'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spirit Link'),
    (select id from sets where short_name = 'leg'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Petra Sphinx'),
    (select id from sets where short_name = 'leg'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Torsten Von Ursus'),
    (select id from sets where short_name = 'leg'),
    '266',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lost Soul'),
    (select id from sets where short_name = 'leg'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Adun Oakenshield'),
    (select id from sets where short_name = 'leg'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Time Elemental'),
    (select id from sets where short_name = 'leg'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ring of Immortals'),
    (select id from sets where short_name = 'leg'),
    '293',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thunder Spirit'),
    (select id from sets where short_name = 'leg'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Remove Soul'),
    (select id from sets where short_name = 'leg'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aisling Leprechaun'),
    (select id from sets where short_name = 'leg'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hell Swarm'),
    (select id from sets where short_name = 'leg'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mold Demon'),
    (select id from sets where short_name = 'leg'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Tabernacle at Pendrell Vale'),
    (select id from sets where short_name = 'leg'),
    '307',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spiritual Sanctuary'),
    (select id from sets where short_name = 'leg'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knowledge Vault'),
    (select id from sets where short_name = 'leg'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Caltrops'),
    (select id from sets where short_name = 'leg'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seafarer''s Quay'),
    (select id from sets where short_name = 'leg'),
    '306',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marhault Elsdragon'),
    (select id from sets where short_name = 'leg'),
    '244',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frost Giant'),
    (select id from sets where short_name = 'leg'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kei Takahashi'),
    (select id from sets where short_name = 'leg'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Field of Dreams'),
    (select id from sets where short_name = 'leg'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampire Bats'),
    (select id from sets where short_name = 'leg'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Vapor'),
    (select id from sets where short_name = 'leg'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gauntlets of Chaos'),
    (select id from sets where short_name = 'leg'),
    '278',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forethought Amulet'),
    (select id from sets where short_name = 'leg'),
    '277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Craw Giant'),
    (select id from sets where short_name = 'leg'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Force Spike'),
    (select id from sets where short_name = 'leg'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas'),
    (select id from sets where short_name = 'leg'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'In the Eye of Chaos'),
    (select id from sets where short_name = 'leg'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kobold Drill Sergeant'),
    (select id from sets where short_name = 'leg'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spectral Cloak'),
    (select id from sets where short_name = 'leg'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pendelhaven'),
    (select id from sets where short_name = 'leg'),
    '305',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sol''kanar the Swamp King'),
    (select id from sets where short_name = 'leg'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crookshank Kobolds'),
    (select id from sets where short_name = 'leg'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serpent Generator'),
    (select id from sets where short_name = 'leg'),
    '295',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kismet'),
    (select id from sets where short_name = 'leg'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jerrard of the Closed Fist'),
    (select id from sets where short_name = 'leg'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelic Voices'),
    (select id from sets where short_name = 'leg'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ur-Drago'),
    (select id from sets where short_name = 'leg'),
    '268',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boris Devilboon'),
    (select id from sets where short_name = 'leg'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Lady of the Mountain'),
    (select id from sets where short_name = 'leg'),
    '263',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tetsuo Umezawa'),
    (select id from sets where short_name = 'leg'),
    '262',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Equinox'),
    (select id from sets where short_name = 'leg'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Abyss'),
    (select id from sets where short_name = 'leg'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Great Defender'),
    (select id from sets where short_name = 'leg'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Turtle'),
    (select id from sets where short_name = 'leg'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Untamed Wilds'),
    (select id from sets where short_name = 'leg'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chromium'),
    (select id from sets where short_name = 'leg'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Durkwood Boars'),
    (select id from sets where short_name = 'leg'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glyph of Destruction'),
    (select id from sets where short_name = 'leg'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tundra Wolves'),
    (select id from sets where short_name = 'leg'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Invoke Prejudice'),
    (select id from sets where short_name = 'leg'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blazing Effigy'),
    (select id from sets where short_name = 'leg'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gosta Dirk'),
    (select id from sets where short_name = 'leg'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avoid Fate'),
    (select id from sets where short_name = 'leg'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arena of the Ancients'),
    (select id from sets where short_name = 'leg'),
    '273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bronze Horse'),
    (select id from sets where short_name = 'leg'),
    '276',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enchanted Being'),
    (select id from sets where short_name = 'leg'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quagmire'),
    (select id from sets where short_name = 'leg'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moat'),
    (select id from sets where short_name = 'leg'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feint'),
    (select id from sets where short_name = 'leg'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reincarnation'),
    (select id from sets where short_name = 'leg'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crimson Manticore'),
    (select id from sets where short_name = 'leg'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jedit Ojanen'),
    (select id from sets where short_name = 'leg'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eternal Warrior'),
    (select id from sets where short_name = 'leg'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moss Monster'),
    (select id from sets where short_name = 'leg'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elder Spawn'),
    (select id from sets where short_name = 'leg'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Immolation'),
    (select id from sets where short_name = 'leg'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ivory Guardians'),
    (select id from sets where short_name = 'leg'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sivitri Scarzam'),
    (select id from sets where short_name = 'leg'),
    '258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Land Equilibrium'),
    (select id from sets where short_name = 'leg'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Divine Offering'),
    (select id from sets where short_name = 'leg'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blight'),
    (select id from sets where short_name = 'leg'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rabid Wombat'),
    (select id from sets where short_name = 'leg'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unholy Citadel'),
    (select id from sets where short_name = 'leg'),
    '309',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tuknir Deathlock'),
    (select id from sets where short_name = 'leg'),
    '267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Divine Intervention'),
    (select id from sets where short_name = 'leg'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rebirth'),
    (select id from sets where short_name = 'leg'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Underworld Dreams'),
    (select id from sets where short_name = 'leg'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Princess Lucrezia'),
    (select id from sets where short_name = 'leg'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gravity Sphere'),
    (select id from sets where short_name = 'leg'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stangg'),
    (select id from sets where short_name = 'leg'),
    '260',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spirit Shackle'),
    (select id from sets where short_name = 'leg'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Heat'),
    (select id from sets where short_name = 'leg'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reverberation'),
    (select id from sets where short_name = 'leg'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emerald Dragonfly'),
    (select id from sets where short_name = 'leg'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Matrix'),
    (select id from sets where short_name = 'leg'),
    '285',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crevasse'),
    (select id from sets where short_name = 'leg'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Primordial Ooze'),
    (select id from sets where short_name = 'leg'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shield Wall'),
    (select id from sets where short_name = 'leg'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kry Shield'),
    (select id from sets where short_name = 'leg'),
    '282',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glyph of Reincarnation'),
    (select id from sets where short_name = 'leg'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abomination'),
    (select id from sets where short_name = 'leg'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cocoon'),
    (select id from sets where short_name = 'leg'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Strength'),
    (select id from sets where short_name = 'leg'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divine Transformation'),
    (select id from sets where short_name = 'leg'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirror Universe'),
    (select id from sets where short_name = 'leg'),
    '287',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relic Bind'),
    (select id from sets where short_name = 'leg'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Dust'),
    (select id from sets where short_name = 'leg'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Axelrod Gunnarson'),
    (select id from sets where short_name = 'leg'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elven Riders'),
    (select id from sets where short_name = 'leg'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Active Volcano'),
    (select id from sets where short_name = 'leg'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rust'),
    (select id from sets where short_name = 'leg'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Palladia-Mors'),
    (select id from sets where short_name = 'leg'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Radjan Spirit'),
    (select id from sets where short_name = 'leg'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evil Eye of Orms-by-Gore'),
    (select id from sets where short_name = 'leg'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'D''Avenant Archer'),
    (select id from sets where short_name = 'leg'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychic Purge'),
    (select id from sets where short_name = 'leg'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lady Orca'),
    (select id from sets where short_name = 'leg'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sword of the Ages'),
    (select id from sets where short_name = 'leg'),
    '296',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chain Lightning'),
    (select id from sets where short_name = 'leg'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Puppet Master'),
    (select id from sets where short_name = 'leg'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keepers of the Faith'),
    (select id from sets where short_name = 'leg'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boomerang'),
    (select id from sets where short_name = 'leg'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Johan'),
    (select id from sets where short_name = 'leg'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Livonya Silone'),
    (select id from sets where short_name = 'leg'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pixie Queen'),
    (select id from sets where short_name = 'leg'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Horror of Horrors'),
    (select id from sets where short_name = 'leg'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glyph of Life'),
    (select id from sets where short_name = 'leg'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolverine Pack'),
    (select id from sets where short_name = 'leg'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Acid Rain'),
    (select id from sets where short_name = 'leg'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lesser Werewolf'),
    (select id from sets where short_name = 'leg'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nova Pentacle'),
    (select id from sets where short_name = 'leg'),
    '289',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Wonder'),
    (select id from sets where short_name = 'leg'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Remove Enchantments'),
    (select id from sets where short_name = 'leg'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enchantment Alteration'),
    (select id from sets where short_name = 'leg'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relic Barrier'),
    (select id from sets where short_name = 'leg'),
    '292',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Earth'),
    (select id from sets where short_name = 'leg'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Juxtapose'),
    (select id from sets where short_name = 'leg'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jovial Evil'),
    (select id from sets where short_name = 'leg'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Life Matrix'),
    (select id from sets where short_name = 'leg'),
    '284',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tobias Andrion'),
    (select id from sets where short_name = 'leg'),
    '264',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Carrion Ants'),
    (select id from sets where short_name = 'leg'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Amrou Kithkin'),
    (select id from sets where short_name = 'leg'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pit Scorpion'),
    (select id from sets where short_name = 'leg'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Xira Arien'),
    (select id from sets where short_name = 'leg'),
    '270',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Great Wall'),
    (select id from sets where short_name = 'leg'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Storm World'),
    (select id from sets where short_name = 'leg'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Triassic Egg'),
    (select id from sets where short_name = 'leg'),
    '297',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Undertow'),
    (select id from sets where short_name = 'leg'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ichneumon Druid'),
    (select id from sets where short_name = 'leg'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gaseous Form'),
    (select id from sets where short_name = 'leg'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urborg'),
    (select id from sets where short_name = 'leg'),
    '310',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Mana Battery'),
    (select id from sets where short_name = 'leg'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Riven Turnbull'),
    (select id from sets where short_name = 'leg'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Floral Spuzzem'),
    (select id from sets where short_name = 'leg'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Headless Horseman'),
    (select id from sets where short_name = 'leg'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat Warriors'),
    (select id from sets where short_name = 'leg'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Sprites'),
    (select id from sets where short_name = 'leg'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demonic Torment'),
    (select id from sets where short_name = 'leg'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tolaria'),
    (select id from sets where short_name = 'leg'),
    '308',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Land Tax'),
    (select id from sets where short_name = 'leg'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Telekinesis'),
    (select id from sets where short_name = 'leg'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lady Caleria'),
    (select id from sets where short_name = 'leg'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spinal Villain'),
    (select id from sets where short_name = 'leg'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Walking Dead'),
    (select id from sets where short_name = 'leg'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Takklemaggot'),
    (select id from sets where short_name = 'leg'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Righteous Avengers'),
    (select id from sets where short_name = 'leg'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'North Star'),
    (select id from sets where short_name = 'leg'),
    '288',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clergy of the Holy Nimbus'),
    (select id from sets where short_name = 'leg'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jasmine Boreal'),
    (select id from sets where short_name = 'leg'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Infinite Authority'),
    (select id from sets where short_name = 'leg'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fortified Area'),
    (select id from sets where short_name = 'leg'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Planar Gate'),
    (select id from sets where short_name = 'leg'),
    '290',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kobolds of Kher Keep'),
    (select id from sets where short_name = 'leg'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barbary Apes'),
    (select id from sets where short_name = 'leg'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seeker'),
    (select id from sets where short_name = 'leg'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disharmony'),
    (select id from sets where short_name = 'leg'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rapid Fire'),
    (select id from sets where short_name = 'leg'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nebuchadnezzar'),
    (select id from sets where short_name = 'leg'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Segovian Leviathan'),
    (select id from sets where short_name = 'leg'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ramirez DePietro'),
    (select id from sets where short_name = 'leg'),
    '251',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cathedral of Serra'),
    (select id from sets where short_name = 'leg'),
    '301',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Drain'),
    (select id from sets where short_name = 'leg'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silhouette'),
    (select id from sets where short_name = 'leg'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rohgahh of Kher Keep'),
    (select id from sets where short_name = 'leg'),
    '255',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Shadows'),
    (select id from sets where short_name = 'leg'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tempest Efreet'),
    (select id from sets where short_name = 'leg'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whirling Dervish'),
    (select id from sets where short_name = 'leg'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Psionic Entity'),
    (select id from sets where short_name = 'leg'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raging Bull'),
    (select id from sets where short_name = 'leg'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Indestructible Aura'),
    (select id from sets where short_name = 'leg'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Touch of Darkness'),
    (select id from sets where short_name = 'leg'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wood Elemental'),
    (select id from sets where short_name = 'leg'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glyph of Delusion'),
    (select id from sets where short_name = 'leg'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jacques le Vert'),
    (select id from sets where short_name = 'leg'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elder Land Wurm'),
    (select id from sets where short_name = 'leg'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ramses Overdark'),
    (select id from sets where short_name = 'leg'),
    '252',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rubinia Soulsinger'),
    (select id from sets where short_name = 'leg'),
    '256',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bartel Runeaxe'),
    (select id from sets where short_name = 'leg'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Falling Star'),
    (select id from sets where short_name = 'leg'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Syphon Soul'),
    (select id from sets where short_name = 'leg'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Paradise'),
    (select id from sets where short_name = 'leg'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lady Evangela'),
    (select id from sets where short_name = 'leg'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alchor''s Tomb'),
    (select id from sets where short_name = 'leg'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Library'),
    (select id from sets where short_name = 'leg'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Light'),
    (select id from sets where short_name = 'leg'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Holy Day'),
    (select id from sets where short_name = 'leg'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Osai Vultures'),
    (select id from sets where short_name = 'leg'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beasts of Bogardan'),
    (select id from sets where short_name = 'leg'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Brute'),
    (select id from sets where short_name = 'leg'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Land''s Edge'),
    (select id from sets where short_name = 'leg'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azure Drake'),
    (select id from sets where short_name = 'leg'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winter Blast'),
    (select id from sets where short_name = 'leg'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Lust'),
    (select id from sets where short_name = 'leg'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dakkon Blackblade'),
    (select id from sets where short_name = 'leg'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shelkin Brownie'),
    (select id from sets where short_name = 'leg'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anti-Magic Aura'),
    (select id from sets where short_name = 'leg'),
    '45',
    'common'
) 
 on conflict do nothing;
