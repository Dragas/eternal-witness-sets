insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Razor Barrier'),
    (select id from sets where short_name = 'ddf'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frogmite'),
    (select id from sets where short_name = 'ddf'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddf'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swell of Courage'),
    (select id from sets where short_name = 'ddf'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddf'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moonglove Extract'),
    (select id from sets where short_name = 'ddf'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seasoned Marshal'),
    (select id from sets where short_name = 'ddf'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daru Encampment'),
    (select id from sets where short_name = 'ddf'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Conclave Phalanx'),
    (select id from sets where short_name = 'ddf'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elspeth, Knight-Errant'),
    (select id from sets where short_name = 'ddf'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Angel of Salvation'),
    (select id from sets where short_name = 'ddf'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kabira Crossroads'),
    (select id from sets where short_name = 'ddf'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steel Overseer'),
    (select id from sets where short_name = 'ddf'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'ddf'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Journey to Nowhere'),
    (select id from sets where short_name = 'ddf'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saltblast'),
    (select id from sets where short_name = 'ddf'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Faerie Mechanist'),
    (select id from sets where short_name = 'ddf'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Celestial Crusader'),
    (select id from sets where short_name = 'ddf'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raise the Alarm'),
    (select id from sets where short_name = 'ddf'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddf'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddf'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Echoing Truth'),
    (select id from sets where short_name = 'ddf'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Spellbomb'),
    (select id from sets where short_name = 'ddf'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seat of the Synod'),
    (select id from sets where short_name = 'ddf'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcbound Worker'),
    (select id from sets where short_name = 'ddf'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddf'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Catapult Master'),
    (select id from sets where short_name = 'ddf'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Esperzoa'),
    (select id from sets where short_name = 'ddf'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elixir of Immortality'),
    (select id from sets where short_name = 'ddf'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Infantry Veteran'),
    (select id from sets where short_name = 'ddf'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Razormane Masticore'),
    (select id from sets where short_name = 'ddf'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thirst for Knowledge'),
    (select id from sets where short_name = 'ddf'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mosquito Guard'),
    (select id from sets where short_name = 'ddf'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kemba''s Skyguard'),
    (select id from sets where short_name = 'ddf'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Synod Centurion'),
    (select id from sets where short_name = 'ddf'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blinding Beam'),
    (select id from sets where short_name = 'ddf'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stalking Stones'),
    (select id from sets where short_name = 'ddf'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loyal Sentry'),
    (select id from sets where short_name = 'ddf'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darksteel Citadel'),
    (select id from sets where short_name = 'ddf'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abolish'),
    (select id from sets where short_name = 'ddf'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clockwork Condor'),
    (select id from sets where short_name = 'ddf'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Qumulox'),
    (select id from sets where short_name = 'ddf'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pentavus'),
    (select id from sets where short_name = 'ddf'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mighty Leap'),
    (select id from sets where short_name = 'ddf'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Juggernaut'),
    (select id from sets where short_name = 'ddf'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trip Noose'),
    (select id from sets where short_name = 'ddf'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = 'ddf'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddf'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conclave Equenaut'),
    (select id from sets where short_name = 'ddf'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Skyfisher'),
    (select id from sets where short_name = 'ddf'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Hookmaster'),
    (select id from sets where short_name = 'ddf'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crusade'),
    (select id from sets where short_name = 'ddf'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Triskelion'),
    (select id from sets where short_name = 'ddf'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steel Wall'),
    (select id from sets where short_name = 'ddf'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clockwork Hydra'),
    (select id from sets where short_name = 'ddf'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunlance'),
    (select id from sets where short_name = 'ddf'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elite Vanguard'),
    (select id from sets where short_name = 'ddf'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serrated Biskelion'),
    (select id from sets where short_name = 'ddf'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Runed Servitor'),
    (select id from sets where short_name = 'ddf'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Contagion Clasp'),
    (select id from sets where short_name = 'ddf'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddf'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Aeronaut'),
    (select id from sets where short_name = 'ddf'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Temple Acolyte'),
    (select id from sets where short_name = 'ddf'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tezzeret the Seeker'),
    (select id from sets where short_name = 'ddf'),
    '39',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goldmeadow Harrier'),
    (select id from sets where short_name = 'ddf'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormfront Riders'),
    (select id from sets where short_name = 'ddf'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glory Seeker'),
    (select id from sets where short_name = 'ddf'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Master of Etherium'),
    (select id from sets where short_name = 'ddf'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Assembly-Worker'),
    (select id from sets where short_name = 'ddf'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Everflowing Chalice'),
    (select id from sets where short_name = 'ddf'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Energy Chamber'),
    (select id from sets where short_name = 'ddf'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rustic Clachan'),
    (select id from sets where short_name = 'ddf'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddf'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thoughtcast'),
    (select id from sets where short_name = 'ddf'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silver Myr'),
    (select id from sets where short_name = 'ddf'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foil'),
    (select id from sets where short_name = 'ddf'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Argivian Restoration'),
    (select id from sets where short_name = 'ddf'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burrenton Bombardier'),
    (select id from sets where short_name = 'ddf'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trinket Mage'),
    (select id from sets where short_name = 'ddf'),
    '49',
    'common'
) 
 on conflict do nothing;
