insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Memory Lapse'),
    (select id from sets where short_name = 'g99'),
    '1',
    'rare'
) 
 on conflict do nothing;
