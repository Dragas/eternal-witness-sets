insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Memory Lapse'),
        (select types.id from types where name = 'Instant')
    ) 
 on conflict do nothing;
