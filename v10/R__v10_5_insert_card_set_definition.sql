insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'v10'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Karn, Silver Golem'),
    (select id from sets where short_name = 'v10'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Zuran Orb'),
    (select id from sets where short_name = 'v10'),
    '15',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mirari'),
    (select id from sets where short_name = 'v10'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Memory Jar'),
    (select id from sets where short_name = 'v10'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jester''s Cap'),
    (select id from sets where short_name = 'v10'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sundering Titan'),
    (select id from sets where short_name = 'v10'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Black Vise'),
    (select id from sets where short_name = 'v10'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = 'v10'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sword of Body and Mind'),
    (select id from sets where short_name = 'v10'),
    '14',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ivory Tower'),
    (select id from sets where short_name = 'v10'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Isochron Scepter'),
    (select id from sets where short_name = 'v10'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aether Vial'),
    (select id from sets where short_name = 'v10'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Masticore'),
    (select id from sets where short_name = 'v10'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mox Diamond'),
    (select id from sets where short_name = 'v10'),
    '10',
    'mythic'
) 
 on conflict do nothing;
