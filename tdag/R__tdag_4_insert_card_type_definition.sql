insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Maddened Oread'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Maddened Oread'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Maddened Oread'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Maddened Oread'),
        (select types.id from types where name = 'Nymph')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Maddened Oread'),
        (select types.id from types where name = 'Reveler')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rip to Pieces'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dance of Flame'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Xenagos''s Strike'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dance of Panic'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Impulsive Charge'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wild Maenads'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wild Maenads'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wild Maenads'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wild Maenads'),
        (select types.id from types where name = 'Reveler')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Impulsive Destruction'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rollicking Throng'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rollicking Throng'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rollicking Throng'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rollicking Throng'),
        (select types.id from types where name = 'Reveler')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ecstatic Piper'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ecstatic Piper'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ecstatic Piper'),
        (select types.id from types where name = 'Satyr')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ecstatic Piper'),
        (select types.id from types where name = 'Reveler')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pheres-Band Revelers'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pheres-Band Revelers'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pheres-Band Revelers'),
        (select types.id from types where name = 'Centaur')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pheres-Band Revelers'),
        (select types.id from types where name = 'Reveler')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Impulsive Return'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Xenagos''s Scorn'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Serpent Dancers'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Serpent Dancers'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Serpent Dancers'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Serpent Dancers'),
        (select types.id from types where name = 'Reveler')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Xenagos Ascended'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Xenagos Ascended'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Xenagos Ascended'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Xenagos Ascended'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Xenagos Ascended'),
        (select types.id from types where name = 'God')
    ) 
 on conflict do nothing;
