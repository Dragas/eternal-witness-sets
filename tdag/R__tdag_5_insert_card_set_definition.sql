insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Maddened Oread'),
    (select id from sets where short_name = 'tdag'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rip to Pieces'),
    (select id from sets where short_name = 'tdag'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dance of Flame'),
    (select id from sets where short_name = 'tdag'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Xenagos''s Strike'),
    (select id from sets where short_name = 'tdag'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dance of Panic'),
    (select id from sets where short_name = 'tdag'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Impulsive Charge'),
    (select id from sets where short_name = 'tdag'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Maenads'),
    (select id from sets where short_name = 'tdag'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Impulsive Destruction'),
    (select id from sets where short_name = 'tdag'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rollicking Throng'),
    (select id from sets where short_name = 'tdag'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ecstatic Piper'),
    (select id from sets where short_name = 'tdag'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pheres-Band Revelers'),
    (select id from sets where short_name = 'tdag'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Impulsive Return'),
    (select id from sets where short_name = 'tdag'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Xenagos''s Scorn'),
    (select id from sets where short_name = 'tdag'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serpent Dancers'),
    (select id from sets where short_name = 'tdag'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Xenagos Ascended'),
    (select id from sets where short_name = 'tdag'),
    '1',
    'common'
) 
 on conflict do nothing;
