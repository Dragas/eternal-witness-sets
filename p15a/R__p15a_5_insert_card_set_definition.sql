insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Kamahl, Pit Fighter'),
    (select id from sets where short_name = 'p15a'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Char'),
    (select id from sets where short_name = 'p15a'),
    '1',
    'rare'
) 
 on conflict do nothing;
