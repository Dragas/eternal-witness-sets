    insert into types (name) values('Legendary') on conflict do nothing;
    insert into types (name) values('Creature') on conflict do nothing;
    insert into types (name) values('Human') on conflict do nothing;
    insert into types (name) values('Barbarian') on conflict do nothing;
    insert into types (name) values('Instant') on conflict do nothing;
