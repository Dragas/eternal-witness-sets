    insert into mtgcard(name) values ('Faithless Looting') on conflict do nothing;
    insert into mtgcard(name) values ('Treasure Hunt') on conflict do nothing;
    insert into mtgcard(name) values ('Breath of Malfegor') on conflict do nothing;
    insert into mtgcard(name) values ('Feast of Blood') on conflict do nothing;
    insert into mtgcard(name) values ('Electrolyze') on conflict do nothing;
    insert into mtgcard(name) values ('Arrest') on conflict do nothing;
    insert into mtgcard(name) values ('Turnabout') on conflict do nothing;
    insert into mtgcard(name) values ('Standstill') on conflict do nothing;
    insert into mtgcard(name) values ('Consume Spirit') on conflict do nothing;
