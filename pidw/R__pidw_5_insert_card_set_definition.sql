insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Faithless Looting'),
    (select id from sets where short_name = 'pidw'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treasure Hunt'),
    (select id from sets where short_name = 'pidw'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Breath of Malfegor'),
    (select id from sets where short_name = 'pidw'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feast of Blood'),
    (select id from sets where short_name = 'pidw'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Electrolyze'),
    (select id from sets where short_name = 'pidw'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arrest'),
    (select id from sets where short_name = 'pidw'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Turnabout'),
    (select id from sets where short_name = 'pidw'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Standstill'),
    (select id from sets where short_name = 'pidw'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Consume Spirit'),
    (select id from sets where short_name = 'pidw'),
    '6',
    'rare'
) 
 on conflict do nothing;
