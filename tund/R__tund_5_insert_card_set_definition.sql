insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tund'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Teddy Bear'),
    (select id from sets where short_name = 'tund'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squirrel'),
    (select id from sets where short_name = 'tund'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tund'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beeble'),
    (select id from sets where short_name = 'tund'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Acorn Stash'),
    (select id from sets where short_name = 'tund'),
    '6',
    'common'
) 
 on conflict do nothing;
