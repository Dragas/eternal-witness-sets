    insert into mtgcard(name) values ('Goblin') on conflict do nothing;
    insert into mtgcard(name) values ('Giant Teddy Bear') on conflict do nothing;
    insert into mtgcard(name) values ('Squirrel') on conflict do nothing;
    insert into mtgcard(name) values ('Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Beeble') on conflict do nothing;
    insert into mtgcard(name) values ('Acorn Stash') on conflict do nothing;
