insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Saheeli''s Directive'),
    (select id from sets where short_name = 'pz2'),
    '70675',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ferocious Zheng'),
    (select id from sets where short_name = 'pz2'),
    '70847',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sacred White Deer'),
    (select id from sets where short_name = 'pz2'),
    '70843',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keeper of Keys'),
    (select id from sets where short_name = 'pz2'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primeval Protector'),
    (select id from sets where short_name = 'pz2'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inspired Sphinx'),
    (select id from sets where short_name = 'pz2'),
    '70779',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stream of Acid'),
    (select id from sets where short_name = 'pz2'),
    '65793',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drown in Shapelessness'),
    (select id from sets where short_name = 'pz2'),
    '70837',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirri, Weatherlight Duelist'),
    (select id from sets where short_name = 'pz2'),
    '65707',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kess, Dissident Mage'),
    (select id from sets where short_name = 'pz2'),
    '65763',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Selvala, Heart of the Wilds'),
    (select id from sets where short_name = 'pz2'),
    '37',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'The Unspeakable'),
    (select id from sets where short_name = 'pz2'),
    '65841',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mu Yanling'),
    (select id from sets where short_name = 'pz2'),
    '70795',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Arixmethes, Slumbering Isle'),
    (select id from sets where short_name = 'pz2'),
    '70715',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Runehorn Hellkite'),
    (select id from sets where short_name = 'pz2'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curse of Vengeance'),
    (select id from sets where short_name = 'pz2'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ravenous Slime'),
    (select id from sets where short_name = 'pz2'),
    '70737',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sanctum Prelate'),
    (select id from sets where short_name = 'pz2'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Feiyi Snake'),
    (select id from sets where short_name = 'pz2'),
    '70791',
    'common'
) ,
(
    (select id from mtgcard where name = 'Custodi Soulbinders'),
    (select id from sets where short_name = 'pz2'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mathas, Fiend Seeker'),
    (select id from sets where short_name = 'pz2'),
    '65759',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Subterranean Tremors'),
    (select id from sets where short_name = 'pz2'),
    '30',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Loyal Apprentice'),
    (select id from sets where short_name = 'pz2'),
    '70753',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sower of Discord'),
    (select id from sets where short_name = 'pz2'),
    '70659',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Game of Chaos'),
    (select id from sets where short_name = 'pz2'),
    '65821',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wasitora, Nekoru Queen'),
    (select id from sets where short_name = 'pz2'),
    '65705',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sima Yi, Wei Field Marshal'),
    (select id from sets where short_name = 'pz2'),
    '65815',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saheeli, the Gifted'),
    (select id from sets where short_name = 'pz2'),
    '70685',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Armory Automaton'),
    (select id from sets where short_name = 'pz2'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loyal Drake'),
    (select id from sets where short_name = 'pz2'),
    '70757',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brainwash'),
    (select id from sets where short_name = 'pz2'),
    '65795',
    'common'
) ,
(
    (select id from mtgcard where name = 'Benefactor''s Draught'),
    (select id from sets where short_name = 'pz2'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Stone Idol'),
    (select id from sets where short_name = 'pz2'),
    '70755',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fortunate Few'),
    (select id from sets where short_name = 'pz2'),
    '65673',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Empyrial Storm'),
    (select id from sets where short_name = 'pz2'),
    '70701',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kindred Boon'),
    (select id from sets where short_name = 'pz2'),
    '65717',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stormcloud Spirit'),
    (select id from sets where short_name = 'pz2'),
    '70825',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Entreat the Dead'),
    (select id from sets where short_name = 'pz2'),
    '70743',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deepglow Skate'),
    (select id from sets where short_name = 'pz2'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gempalm Incinerator'),
    (select id from sets where short_name = 'pz2'),
    '65855',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Garrulous Sycophant'),
    (select id from sets where short_name = 'pz2'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Huang Zhong, Shu General'),
    (select id from sets where short_name = 'pz2'),
    '65801',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stunt Double'),
    (select id from sets where short_name = 'pz2'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ever-Watching Threshold'),
    (select id from sets where short_name = 'pz2'),
    '70665',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heavenly Qilin'),
    (select id from sets where short_name = 'pz2'),
    '70827',
    'common'
) ,
(
    (select id from mtgcard where name = 'Entrapment Maneuver'),
    (select id from sets where short_name = 'pz2'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hungry Lynx'),
    (select id from sets where short_name = 'pz2'),
    '65711',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selvala''s Stampede'),
    (select id from sets where short_name = 'pz2'),
    '65787',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avatar of Growth'),
    (select id from sets where short_name = 'pz2'),
    '70785',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Reyhan, Last of the Abzan'),
    (select id from sets where short_name = 'pz2'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Expropriate'),
    (select id from sets where short_name = 'pz2'),
    '65785',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Alms Collector'),
    (select id from sets where short_name = 'pz2'),
    '65677',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Turntimber Sower'),
    (select id from sets where short_name = 'pz2'),
    '70711',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Throne Warden'),
    (select id from sets where short_name = 'pz2'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earth-Origin Yak'),
    (select id from sets where short_name = 'pz2'),
    '70821',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brudiclad, Telchor Engineer'),
    (select id from sets where short_name = 'pz2'),
    '70709',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Screeching Phoenix'),
    (select id from sets where short_name = 'pz2'),
    '70863',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jiang Yanggu'),
    (select id from sets where short_name = 'pz2'),
    '70793',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Burnt Offering'),
    (select id from sets where short_name = 'pz2'),
    '65833',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lady Zhurong, Warrior Queen'),
    (select id from sets where short_name = 'pz2'),
    '65811',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curse of Vitality'),
    (select id from sets where short_name = 'pz2'),
    '65765',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Xun Yu, Wei Advisor'),
    (select id from sets where short_name = 'pz2'),
    '65799',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boneyard Scourge'),
    (select id from sets where short_name = 'pz2'),
    '65687',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nazahn, Revered Bladesmith'),
    (select id from sets where short_name = 'pz2'),
    '65771',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fury Storm'),
    (select id from sets where short_name = 'pz2'),
    '70751',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Purple-Crystal Crab'),
    (select id from sets where short_name = 'pz2'),
    '70813',
    'common'
) ,
(
    (select id from mtgcard where name = 'Echo Storm'),
    (select id from sets where short_name = 'pz2'),
    '70695',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crystalline Crawler'),
    (select id from sets where short_name = 'pz2'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Night Incarnate'),
    (select id from sets where short_name = 'pz2'),
    '70749',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Excavation'),
    (select id from sets where short_name = 'pz2'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reality Scramble'),
    (select id from sets where short_name = 'pz2'),
    '70723',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heavenly Blademaster'),
    (select id from sets where short_name = 'pz2'),
    '70705',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Immortal Phoenix'),
    (select id from sets where short_name = 'pz2'),
    '70773',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Protection'),
    (select id from sets where short_name = 'pz2'),
    '65701',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vial Smasher the Fierce'),
    (select id from sets where short_name = 'pz2'),
    '49',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Loyal Subordinate'),
    (select id from sets where short_name = 'pz2'),
    '70735',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ishai, Ojutai Dragonspeaker'),
    (select id from sets where short_name = 'pz2'),
    '57',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ash Barrens'),
    (select id from sets where short_name = 'pz2'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akiri, Line-Slinger'),
    (select id from sets where short_name = 'pz2'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cao Ren, Wei Commander'),
    (select id from sets where short_name = 'pz2'),
    '65817',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyline Despot'),
    (select id from sets where short_name = 'pz2'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancestor Dragon'),
    (select id from sets where short_name = 'pz2'),
    '70829',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Qilin''s Blessing'),
    (select id from sets where short_name = 'pz2'),
    '70841',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kindred Dominance'),
    (select id from sets where short_name = 'pz2'),
    '65723',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cruel Tutor'),
    (select id from sets where short_name = 'pz2'),
    '65797',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selfless Squire'),
    (select id from sets where short_name = 'pz2'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Charging Cinderhorn'),
    (select id from sets where short_name = 'pz2'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaya, Ghost Assassin'),
    (select id from sets where short_name = 'pz2'),
    '67',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aminatou''s Augury'),
    (select id from sets where short_name = 'pz2'),
    '70745',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evolutionary Escalation'),
    (select id from sets where short_name = 'pz2'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grip of Phyresis'),
    (select id from sets where short_name = 'pz2'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ma Chao, Western Warrior'),
    (select id from sets where short_name = 'pz2'),
    '65807',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forge of Heroes'),
    (select id from sets where short_name = 'pz2'),
    '70679',
    'common'
) ,
(
    (select id from mtgcard where name = 'Breath of Fire'),
    (select id from sets where short_name = 'pz2'),
    '70859',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scalelord Reckoner'),
    (select id from sets where short_name = 'pz2'),
    '65689',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Metamorphosis'),
    (select id from sets where short_name = 'pz2'),
    '65839',
    'common'
) ,
(
    (select id from mtgcard where name = 'Queen Marchesa'),
    (select id from sets where short_name = 'pz2'),
    '50',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pz2'),
    '70805',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nylea''s Colossus'),
    (select id from sets where short_name = 'pz2'),
    '70655',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warren Instigator'),
    (select id from sets where short_name = 'pz2'),
    '65853',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vivid Flying Fish'),
    (select id from sets where short_name = 'pz2'),
    '70817',
    'common'
) ,
(
    (select id from mtgcard where name = 'Borderland Explorer'),
    (select id from sets where short_name = 'pz2'),
    '65775',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aggressive Instinct'),
    (select id from sets where short_name = 'pz2'),
    '70855',
    'common'
) ,
(
    (select id from mtgcard where name = 'Journey for the Elixir'),
    (select id from sets where short_name = 'pz2'),
    '70861',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yidris, Maelstrom Wielder'),
    (select id from sets where short_name = 'pz2'),
    '64',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'The Ur-Dragon'),
    (select id from sets where short_name = 'pz2'),
    '65675',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kestia, the Cultivator'),
    (select id from sets where short_name = 'pz2'),
    '70699',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thrasios, Triton Hero'),
    (select id from sets where short_name = 'pz2'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hardened-Scale Armor'),
    (select id from sets where short_name = 'pz2'),
    '70787',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windgrace''s Judgment'),
    (select id from sets where short_name = 'pz2'),
    '70669',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pz2'),
    '70803',
    'common'
) ,
(
    (select id from mtgcard where name = 'Qasali Slingers'),
    (select id from sets where short_name = 'pz2'),
    '65709',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloak of Mists'),
    (select id from sets where short_name = 'pz2'),
    '70835',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zuo Ci, the Mocking Sage'),
    (select id from sets where short_name = 'pz2'),
    '65805',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Parting Thoughts'),
    (select id from sets where short_name = 'pz2'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disrupt Decorum'),
    (select id from sets where short_name = 'pz2'),
    '65695',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Master of Waves'),
    (select id from sets where short_name = 'pz2'),
    '65859',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pz2'),
    '70801',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire-Omen Crane'),
    (select id from sets where short_name = 'pz2'),
    '70851',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Domesticated Hydra'),
    (select id from sets where short_name = 'pz2'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kindred Summons'),
    (select id from sets where short_name = 'pz2'),
    '65719',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skittering Crustacean'),
    (select id from sets where short_name = 'pz2'),
    '65777',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grenzo, Havoc Raiser'),
    (select id from sets where short_name = 'pz2'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firesong and Sunspeaker'),
    (select id from sets where short_name = 'pz2'),
    '68076',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jhoira of the Ghitu'),
    (select id from sets where short_name = 'pz2'),
    '65847',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Treasure Nabber'),
    (select id from sets where short_name = 'pz2'),
    '70653',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kindred Charge'),
    (select id from sets where short_name = 'pz2'),
    '65721',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kynaios and Tiro of Meletis'),
    (select id from sets where short_name = 'pz2'),
    '63',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Splitting Slime'),
    (select id from sets where short_name = 'pz2'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Taigam, Ojutai Master'),
    (select id from sets where short_name = 'pz2'),
    '65739',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Taigam, Sidisi''s Hand'),
    (select id from sets where short_name = 'pz2'),
    '65715',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon''s Presence'),
    (select id from sets where short_name = 'pz2'),
    '70831',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sidar Kondo of Jamuraa'),
    (select id from sets where short_name = 'pz2'),
    '54',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Xantcha, Sleeper Agent'),
    (select id from sets where short_name = 'pz2'),
    '70719',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enchanter''s Bane'),
    (select id from sets where short_name = 'pz2'),
    '70703',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Energy Tap'),
    (select id from sets where short_name = 'pz2'),
    '65837',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balan, Wandering Knight'),
    (select id from sets where short_name = 'pz2'),
    '65757',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frenzied Fugue'),
    (select id from sets where short_name = 'pz2'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aspect of Wolf'),
    (select id from sets where short_name = 'pz2'),
    '65819',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aminatou, the Fateshifter'),
    (select id from sets where short_name = 'pz2'),
    '70763',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Genesis Storm'),
    (select id from sets where short_name = 'pz2'),
    '70681',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Retrofitter Foundry'),
    (select id from sets where short_name = 'pz2'),
    '70697',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Spymaster'),
    (select id from sets where short_name = 'pz2'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirror of the Forebears'),
    (select id from sets where short_name = 'pz2'),
    '65751',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Estrid''s Invocation'),
    (select id from sets where short_name = 'pz2'),
    '70765',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crash of Rhino Beetles'),
    (select id from sets where short_name = 'pz2'),
    '70693',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thantis, the Warweaver'),
    (select id from sets where short_name = 'pz2'),
    '70671',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Varina, Lich Queen'),
    (select id from sets where short_name = 'pz2'),
    '70741',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thorn of the Black Rose'),
    (select id from sets where short_name = 'pz2'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shifting Shadow'),
    (select id from sets where short_name = 'pz2'),
    '65703',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silas Renn, Seeker Adept'),
    (select id from sets where short_name = 'pz2'),
    '51',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Grave Upheaval'),
    (select id from sets where short_name = 'pz2'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodtracker'),
    (select id from sets where short_name = 'pz2'),
    '70689',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rampaging Brontodon'),
    (select id from sets where short_name = 'pz2'),
    '70775',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hammer of Nazahn'),
    (select id from sets where short_name = 'pz2'),
    '65669',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guttural Response'),
    (select id from sets where short_name = 'pz2'),
    '65845',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kydele, Chosen of Kruphix'),
    (select id from sets where short_name = 'pz2'),
    '53',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Marchesa''s Decree'),
    (select id from sets where short_name = 'pz2'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yennett, Cryptic Sovereign'),
    (select id from sets where short_name = 'pz2'),
    '70747',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tuvasa the Sunlit'),
    (select id from sets where short_name = 'pz2'),
    '70691',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Magus of the Balance'),
    (select id from sets where short_name = 'pz2'),
    '70677',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Izzet Chemister'),
    (select id from sets where short_name = 'pz2'),
    '65741',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tawnos, Urza''s Apprentice'),
    (select id from sets where short_name = 'pz2'),
    '70687',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Path of Ancestry'),
    (select id from sets where short_name = 'pz2'),
    '65679',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desperate Ritual'),
    (select id from sets where short_name = 'pz2'),
    '65851',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prismatic Geoscope'),
    (select id from sets where short_name = 'pz2'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magus of the Mind'),
    (select id from sets where short_name = 'pz2'),
    '65685',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fractured Identity'),
    (select id from sets where short_name = 'pz2'),
    '65681',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curse of Disturbance'),
    (select id from sets where short_name = 'pz2'),
    '65743',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cleansing Screech'),
    (select id from sets where short_name = 'pz2'),
    '70865',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plant Elemental'),
    (select id from sets where short_name = 'pz2'),
    '65825',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ikra Shidiqi, the Usurper'),
    (select id from sets where short_name = 'pz2'),
    '58',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Recruiter of the Guard'),
    (select id from sets where short_name = 'pz2'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stalking Leonin'),
    (select id from sets where short_name = 'pz2'),
    '65769',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Custodi Squire'),
    (select id from sets where short_name = 'pz2'),
    '65781',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodsworn Steward'),
    (select id from sets where short_name = 'pz2'),
    '65699',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gyrus, Waker of Corpses'),
    (select id from sets where short_name = 'pz2'),
    '70661',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Flood'),
    (select id from sets where short_name = 'pz2'),
    '65829',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rot Hulk'),
    (select id from sets where short_name = 'pz2'),
    '70781',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Leovold, Emissary of Trest'),
    (select id from sets where short_name = 'pz2'),
    '60',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Curse of Verbosity'),
    (select id from sets where short_name = 'pz2'),
    '65767',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Patron of the Vein'),
    (select id from sets where short_name = 'pz2'),
    '65727',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whiptongue Hydra'),
    (select id from sets where short_name = 'pz2'),
    '70657',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stonehoof Chieftain'),
    (select id from sets where short_name = 'pz2'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moon-Eating Dog'),
    (select id from sets where short_name = 'pz2'),
    '70823',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arahbo, Roar of the World'),
    (select id from sets where short_name = 'pz2'),
    '65735',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Drafna''s Restoration'),
    (select id from sets where short_name = 'pz2'),
    '65827',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nine-Tail White Fox'),
    (select id from sets where short_name = 'pz2'),
    '70819',
    'common'
) ,
(
    (select id from mtgcard where name = 'Welkin Tern'),
    (select id from sets where short_name = 'pz2'),
    '70815',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emissary of Grudges'),
    (select id from sets where short_name = 'pz2'),
    '70761',
    'rare'
) ,
(
    (select id from mtgcard where name = 'O-Kagachi, Vengeful Kami'),
    (select id from sets where short_name = 'pz2'),
    '65733',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Edgar Markov'),
    (select id from sets where short_name = 'pz2'),
    '65693',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Inalla, Archmage Ritualist'),
    (select id from sets where short_name = 'pz2'),
    '65737',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Throne of the High City'),
    (select id from sets where short_name = 'pz2'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Custodi Lich'),
    (select id from sets where short_name = 'pz2'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kheru Mind-Eater'),
    (select id from sets where short_name = 'pz2'),
    '65761',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lonely Sandbar'),
    (select id from sets where short_name = 'pz2'),
    '65863',
    'common'
) ,
(
    (select id from mtgcard where name = 'Palace Jailer'),
    (select id from sets where short_name = 'pz2'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Saskia the Unyielding'),
    (select id from sets where short_name = 'pz2'),
    '61',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Skull Storm'),
    (select id from sets where short_name = 'pz2'),
    '70713',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Palace Sentinels'),
    (select id from sets where short_name = 'pz2'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boreas Charger'),
    (select id from sets where short_name = 'pz2'),
    '70717',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pz2'),
    '70807',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earthshaking Si'),
    (select id from sets where short_name = 'pz2'),
    '70853',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lord Windgrace'),
    (select id from sets where short_name = 'pz2'),
    '70667',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Meandering River'),
    (select id from sets where short_name = 'pz2'),
    '70799',
    'common'
) ,
(
    (select id from mtgcard where name = 'Traverse the Outlands'),
    (select id from sets where short_name = 'pz2'),
    '65671',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faerie Artisans'),
    (select id from sets where short_name = 'pz2'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Goon'),
    (select id from sets where short_name = 'pz2'),
    '65857',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Geode Golem'),
    (select id from sets where short_name = 'pz2'),
    '70727',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pit Scorpion'),
    (select id from sets where short_name = 'pz2'),
    '65823',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodline Necromancer'),
    (select id from sets where short_name = 'pz2'),
    '65753',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Breya, Etherium Shaper'),
    (select id from sets where short_name = 'pz2'),
    '65',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Loyal Guardian'),
    (select id from sets where short_name = 'pz2'),
    '70721',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ravos, Soultender'),
    (select id from sets where short_name = 'pz2'),
    '59',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sinuous Vermin'),
    (select id from sets where short_name = 'pz2'),
    '65773',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curse of Opulence'),
    (select id from sets where short_name = 'pz2'),
    '65745',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kraum, Ludevic''s Opus'),
    (select id from sets where short_name = 'pz2'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zo-Zu the Punisher'),
    (select id from sets where short_name = 'pz2'),
    '65843',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reckless Pangolin'),
    (select id from sets where short_name = 'pz2'),
    '70845',
    'common'
) ,
(
    (select id from mtgcard where name = 'Timber Gorge'),
    (select id from sets where short_name = 'pz2'),
    '70797',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Guardian'),
    (select id from sets where short_name = 'pz2'),
    '70767',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leopard-Spotted Jiao'),
    (select id from sets where short_name = 'pz2'),
    '70789',
    'common'
) ,
(
    (select id from mtgcard where name = 'Isolated Watchtower'),
    (select id from sets where short_name = 'pz2'),
    '70707',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armored Whirl Turtle'),
    (select id from sets where short_name = 'pz2'),
    '70809',
    'common'
) ,
(
    (select id from mtgcard where name = 'Militant Angel'),
    (select id from sets where short_name = 'pz2'),
    '70777',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = 'pz2'),
    '70849',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ramos, Dragon Engine'),
    (select id from sets where short_name = 'pz2'),
    '65749',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Entourage of Trest'),
    (select id from sets where short_name = 'pz2'),
    '65783',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lu Su, Wu Advisor'),
    (select id from sets where short_name = 'pz2'),
    '65809',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cemetery Gate'),
    (select id from sets where short_name = 'pz2'),
    '65835',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boompile'),
    (select id from sets where short_name = 'pz2'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tymna the Weaver'),
    (select id from sets where short_name = 'pz2'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Field of Dreams'),
    (select id from sets where short_name = 'pz2'),
    '65831',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Protector of the Crown'),
    (select id from sets where short_name = 'pz2'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Atraxa, Praetors'' Voice'),
    (select id from sets where short_name = 'pz2'),
    '62',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Orzhov Advokist'),
    (select id from sets where short_name = 'pz2'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodforged Battle-Axe'),
    (select id from sets where short_name = 'pz2'),
    '65713',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Varchild, Betrayer of Kjeldor'),
    (select id from sets where short_name = 'pz2'),
    '70759',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rhythmic Water Vortex'),
    (select id from sets where short_name = 'pz2'),
    '70839',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Reclamation'),
    (select id from sets where short_name = 'pz2'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Migratory Route'),
    (select id from sets where short_name = 'pz2'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Adriana, Captain of the Guard'),
    (select id from sets where short_name = 'pz2'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Daretti, Ingenious Iconoclast'),
    (select id from sets where short_name = 'pz2'),
    '66',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pang Tong, "Young Phoenix"'),
    (select id from sets where short_name = 'pz2'),
    '65813',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magus of the Will'),
    (select id from sets where short_name = 'pz2'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treasonous Ogre'),
    (select id from sets where short_name = 'pz2'),
    '65779',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Myth Unbound'),
    (select id from sets where short_name = 'pz2'),
    '70663',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kindred Discovery'),
    (select id from sets where short_name = 'pz2'),
    '65725',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yuriko, the Tiger''s Shadow'),
    (select id from sets where short_name = 'pz2'),
    '70651',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duelist''s Heritage'),
    (select id from sets where short_name = 'pz2'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Octopus Umbra'),
    (select id from sets where short_name = 'pz2'),
    '70725',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coveted Jewel'),
    (select id from sets where short_name = 'pz2'),
    '70673',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tidal Courier'),
    (select id from sets where short_name = 'pz2'),
    '65861',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brilliant Plan'),
    (select id from sets where short_name = 'pz2'),
    '70833',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Primordial Mist'),
    (select id from sets where short_name = 'pz2'),
    '70733',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nesting Dragon'),
    (select id from sets where short_name = 'pz2'),
    '70649',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vedalken Humiliator'),
    (select id from sets where short_name = 'pz2'),
    '70731',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lady Sun'),
    (select id from sets where short_name = 'pz2'),
    '65803',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Confidence from Strength'),
    (select id from sets where short_name = 'pz2'),
    '70857',
    'common'
) ,
(
    (select id from mtgcard where name = 'Colorful Feiyi Sparrow'),
    (select id from sets where short_name = 'pz2'),
    '70811',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conqueror''s Flail'),
    (select id from sets where short_name = 'pz2'),
    '65791',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Salvage'),
    (select id from sets where short_name = 'pz2'),
    '65789',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angler Turtle'),
    (select id from sets where short_name = 'pz2'),
    '70769',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curse of Bounty'),
    (select id from sets where short_name = 'pz2'),
    '65747',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aquitect''s Will'),
    (select id from sets where short_name = 'pz2'),
    '65865',
    'common'
) ,
(
    (select id from mtgcard where name = 'Licia, Sanguine Tribune'),
    (select id from sets where short_name = 'pz2'),
    '65729',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Loyal Unicorn'),
    (select id from sets where short_name = 'pz2'),
    '70729',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tana, the Bloodsower'),
    (select id from sets where short_name = 'pz2'),
    '55',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Heirloom Blade'),
    (select id from sets where short_name = 'pz2'),
    '65683',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bruse Tarl, Boorish Herder'),
    (select id from sets where short_name = 'pz2'),
    '56',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Territorial Hellkite'),
    (select id from sets where short_name = 'pz2'),
    '65691',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ludevic, Necro-Alchemist'),
    (select id from sets where short_name = 'pz2'),
    '52',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Estrid, the Masked'),
    (select id from sets where short_name = 'pz2'),
    '70739',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lovisa Coldeyes'),
    (select id from sets where short_name = 'pz2'),
    '65849',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goblin Goliath'),
    (select id from sets where short_name = 'pz2'),
    '70783',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Galecaster Colossus'),
    (select id from sets where short_name = 'pz2'),
    '65755',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crimson Honor Guard'),
    (select id from sets where short_name = 'pz2'),
    '65697',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treacherous Terrain'),
    (select id from sets where short_name = 'pz2'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Endless Atlas'),
    (select id from sets where short_name = 'pz2'),
    '70683',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Herald''s Horn'),
    (select id from sets where short_name = 'pz2'),
    '65731',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vengeant Vampire'),
    (select id from sets where short_name = 'pz2'),
    '70771',
    'rare'
) 
 on conflict do nothing;
