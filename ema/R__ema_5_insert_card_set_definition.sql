insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Thornwood Falls'),
    (select id from sets where short_name = 'ema'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Intangible Virtue'),
    (select id from sets where short_name = 'ema'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Green Sun''s Zenith'),
    (select id from sets where short_name = 'ema'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Field of Souls'),
    (select id from sets where short_name = 'ema'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Xantid Swarm'),
    (select id from sets where short_name = 'ema'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Torrent of Souls'),
    (select id from sets where short_name = 'ema'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Visara the Dreadful'),
    (select id from sets where short_name = 'ema'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sulfuric Vortex'),
    (select id from sets where short_name = 'ema'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Trenches'),
    (select id from sets where short_name = 'ema'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Undying Rage'),
    (select id from sets where short_name = 'ema'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tranquil Cove'),
    (select id from sets where short_name = 'ema'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wake of Vultures'),
    (select id from sets where short_name = 'ema'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'ema'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enlightened Tutor'),
    (select id from sets where short_name = 'ema'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Crypt'),
    (select id from sets where short_name = 'ema'),
    '225',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Screeching Skaab'),
    (select id from sets where short_name = 'ema'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Borderland Marauder'),
    (select id from sets where short_name = 'ema'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dack Fayden'),
    (select id from sets where short_name = 'ema'),
    '199',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Malicious Affliction'),
    (select id from sets where short_name = 'ema'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Honden of Night''s Reach'),
    (select id from sets where short_name = 'ema'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Calciderm'),
    (select id from sets where short_name = 'ema'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Innocent Blood'),
    (select id from sets where short_name = 'ema'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Honden of Infinite Rage'),
    (select id from sets where short_name = 'ema'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Extract from Darkness'),
    (select id from sets where short_name = 'ema'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inkwell Leviathan'),
    (select id from sets where short_name = 'ema'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prismatic Lens'),
    (select id from sets where short_name = 'ema'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desperate Ravings'),
    (select id from sets where short_name = 'ema'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karmic Guide'),
    (select id from sets where short_name = 'ema'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Gargantua'),
    (select id from sets where short_name = 'ema'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'ema'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urborg Uprising'),
    (select id from sets where short_name = 'ema'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'ema'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sneak Attack'),
    (select id from sets where short_name = 'ema'),
    '148',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Werebear'),
    (select id from sets where short_name = 'ema'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Monster'),
    (select id from sets where short_name = 'ema'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Benevolent Bodyguard'),
    (select id from sets where short_name = 'ema'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jetting Glasskite'),
    (select id from sets where short_name = 'ema'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nausea'),
    (select id from sets where short_name = 'ema'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyrokinesis'),
    (select id from sets where short_name = 'ema'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lys Alana Scarblade'),
    (select id from sets where short_name = 'ema'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shoreline Ranger'),
    (select id from sets where short_name = 'ema'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skulking Ghost'),
    (select id from sets where short_name = 'ema'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elite Vanguard'),
    (select id from sets where short_name = 'ema'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodbraid Elf'),
    (select id from sets where short_name = 'ema'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warden of Evos Isle'),
    (select id from sets where short_name = 'ema'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Charge'),
    (select id from sets where short_name = 'ema'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Man-o''-War'),
    (select id from sets where short_name = 'ema'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Regal Force'),
    (select id from sets where short_name = 'ema'),
    '181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coalition Honor Guard'),
    (select id from sets where short_name = 'ema'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keldon Champion'),
    (select id from sets where short_name = 'ema'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Peregrine Drake'),
    (select id from sets where short_name = 'ema'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Second Thoughts'),
    (select id from sets where short_name = 'ema'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'ema'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'ema'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Imperious Perfect'),
    (select id from sets where short_name = 'ema'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cabal Therapy'),
    (select id from sets where short_name = 'ema'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Memory Lapse'),
    (select id from sets where short_name = 'ema'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Vanguard'),
    (select id from sets where short_name = 'ema'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Hookmaster'),
    (select id from sets where short_name = 'ema'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Monk Idealist'),
    (select id from sets where short_name = 'ema'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nimble Mongoose'),
    (select id from sets where short_name = 'ema'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soulcatcher'),
    (select id from sets where short_name = 'ema'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Millikin'),
    (select id from sets where short_name = 'ema'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jace, the Mind Sculptor'),
    (select id from sets where short_name = 'ema'),
    '57',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Raise the Alarm'),
    (select id from sets where short_name = 'ema'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'ema'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blossoming Sands'),
    (select id from sets where short_name = 'ema'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hymn to Tourach'),
    (select id from sets where short_name = 'ema'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avarax'),
    (select id from sets where short_name = 'ema'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shelter'),
    (select id from sets where short_name = 'ema'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jareth, Leonine Titan'),
    (select id from sets where short_name = 'ema'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relic of Progenitus'),
    (select id from sets where short_name = 'ema'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oona''s Grace'),
    (select id from sets where short_name = 'ema'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Charbelcher'),
    (select id from sets where short_name = 'ema'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Isochron Scepter'),
    (select id from sets where short_name = 'ema'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mindless Automaton'),
    (select id from sets where short_name = 'ema'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maze of Ith'),
    (select id from sets where short_name = 'ema'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hydroblast'),
    (select id from sets where short_name = 'ema'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Egg'),
    (select id from sets where short_name = 'ema'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prodigal Sorcerer'),
    (select id from sets where short_name = 'ema'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Tortoise'),
    (select id from sets where short_name = 'ema'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scoured Barrens'),
    (select id from sets where short_name = 'ema'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seal of Strength'),
    (select id from sets where short_name = 'ema'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heritage Druid'),
    (select id from sets where short_name = 'ema'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necropotence'),
    (select id from sets where short_name = 'ema'),
    '98',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wonder'),
    (select id from sets where short_name = 'ema'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Havoc Demon'),
    (select id from sets where short_name = 'ema'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodfell Caves'),
    (select id from sets where short_name = 'ema'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nekrataal'),
    (select id from sets where short_name = 'ema'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mesa Enchantress'),
    (select id from sets where short_name = 'ema'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Timberwatch Elf'),
    (select id from sets where short_name = 'ema'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Entomb'),
    (select id from sets where short_name = 'ema'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karakas'),
    (select id from sets where short_name = 'ema'),
    '240',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thornweald Archer'),
    (select id from sets where short_name = 'ema'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stingscourger'),
    (select id from sets where short_name = 'ema'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glimmerpoint Stag'),
    (select id from sets where short_name = 'ema'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stupefying Touch'),
    (select id from sets where short_name = 'ema'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rugged Highlands'),
    (select id from sets where short_name = 'ema'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunderclap Wyvern'),
    (select id from sets where short_name = 'ema'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aven Riftwatcher'),
    (select id from sets where short_name = 'ema'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nature''s Claim'),
    (select id from sets where short_name = 'ema'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dismal Backwater'),
    (select id from sets where short_name = 'ema'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Commune with the Gods'),
    (select id from sets where short_name = 'ema'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vindicate'),
    (select id from sets where short_name = 'ema'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Wanderer'),
    (select id from sets where short_name = 'ema'),
    '204',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Emperor Crocodile'),
    (select id from sets where short_name = 'ema'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Toxic Deluge'),
    (select id from sets where short_name = 'ema'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wee Dragonauts'),
    (select id from sets where short_name = 'ema'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swiftwater Cliffs'),
    (select id from sets where short_name = 'ema'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lys Alana Huntmaster'),
    (select id from sets where short_name = 'ema'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faithless Looting'),
    (select id from sets where short_name = 'ema'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Young Pyromancer'),
    (select id from sets where short_name = 'ema'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Enchantress'),
    (select id from sets where short_name = 'ema'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gamble'),
    (select id from sets where short_name = 'ema'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Honden of Life''s Web'),
    (select id from sets where short_name = 'ema'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beetleback Chief'),
    (select id from sets where short_name = 'ema'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plague Witch'),
    (select id from sets where short_name = 'ema'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zealous Persecution'),
    (select id from sets where short_name = 'ema'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Roar of the Wurm'),
    (select id from sets where short_name = 'ema'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Animate Dead'),
    (select id from sets where short_name = 'ema'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battle Squadron'),
    (select id from sets where short_name = 'ema'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deep Analysis'),
    (select id from sets where short_name = 'ema'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sensei''s Divining Top'),
    (select id from sets where short_name = 'ema'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sengir Autocrat'),
    (select id from sets where short_name = 'ema'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'ema'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wirewood Symbiote'),
    (select id from sets where short_name = 'ema'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harmonize'),
    (select id from sets where short_name = 'ema'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flinthoof Boar'),
    (select id from sets where short_name = 'ema'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Altar'),
    (select id from sets where short_name = 'ema'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chain Lightning'),
    (select id from sets where short_name = 'ema'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Humble'),
    (select id from sets where short_name = 'ema'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brago, King Eternal'),
    (select id from sets where short_name = 'ema'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duplicant'),
    (select id from sets where short_name = 'ema'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rorix Bladewing'),
    (select id from sets where short_name = 'ema'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = 'ema'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firebolt'),
    (select id from sets where short_name = 'ema'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghitu Slinger'),
    (select id from sets where short_name = 'ema'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mystical Tutor'),
    (select id from sets where short_name = 'ema'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Honden of Cleansing Fire'),
    (select id from sets where short_name = 'ema'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eyeblight''s Ending'),
    (select id from sets where short_name = 'ema'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Call the Skybreaker'),
    (select id from sets where short_name = 'ema'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Honden of Seeing Winds'),
    (select id from sets where short_name = 'ema'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silent Departure'),
    (select id from sets where short_name = 'ema'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wind-Scarred Crag'),
    (select id from sets where short_name = 'ema'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Blessing'),
    (select id from sets where short_name = 'ema'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Oriflamme'),
    (select id from sets where short_name = 'ema'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burning Vengeance'),
    (select id from sets where short_name = 'ema'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fervent Cathar'),
    (select id from sets where short_name = 'ema'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Artist'),
    (select id from sets where short_name = 'ema'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'ema'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Worldgorger Dragon'),
    (select id from sets where short_name = 'ema'),
    '154',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shaman of the Pack'),
    (select id from sets where short_name = 'ema'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wildfire Emissary'),
    (select id from sets where short_name = 'ema'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Might'),
    (select id from sets where short_name = 'ema'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx of the Steel Wind'),
    (select id from sets where short_name = 'ema'),
    '207',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shardless Agent'),
    (select id from sets where short_name = 'ema'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sinkhole'),
    (select id from sets where short_name = 'ema'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twisted Abomination'),
    (select id from sets where short_name = 'ema'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dualcaster Mage'),
    (select id from sets where short_name = 'ema'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faith''s Fetters'),
    (select id from sets where short_name = 'ema'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tooth and Claw'),
    (select id from sets where short_name = 'ema'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dream Twist'),
    (select id from sets where short_name = 'ema'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wasteland'),
    (select id from sets where short_name = 'ema'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winter Orb'),
    (select id from sets where short_name = 'ema'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rancor'),
    (select id from sets where short_name = 'ema'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mistral Charger'),
    (select id from sets where short_name = 'ema'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squadron Hawk'),
    (select id from sets where short_name = 'ema'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyroblast'),
    (select id from sets where short_name = 'ema'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Price of Progress'),
    (select id from sets where short_name = 'ema'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'War Priest of Thune'),
    (select id from sets where short_name = 'ema'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brawn'),
    (select id from sets where short_name = 'ema'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'ema'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prowling Pangolin'),
    (select id from sets where short_name = 'ema'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Argothian Enchantress'),
    (select id from sets where short_name = 'ema'),
    '158',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rally the Peasants'),
    (select id from sets where short_name = 'ema'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fog'),
    (select id from sets where short_name = 'ema'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whitemane Lion'),
    (select id from sets where short_name = 'ema'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keldon Marauders'),
    (select id from sets where short_name = 'ema'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abundant Growth'),
    (select id from sets where short_name = 'ema'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Worn Powerstone'),
    (select id from sets where short_name = 'ema'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Natural Order'),
    (select id from sets where short_name = 'ema'),
    '177',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Glacial Wall'),
    (select id from sets where short_name = 'ema'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Void'),
    (select id from sets where short_name = 'ema'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tragic Slip'),
    (select id from sets where short_name = 'ema'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Annihilate'),
    (select id from sets where short_name = 'ema'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gaseous Form'),
    (select id from sets where short_name = 'ema'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancestral Mask'),
    (select id from sets where short_name = 'ema'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Control Magic'),
    (select id from sets where short_name = 'ema'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fact or Fiction'),
    (select id from sets where short_name = 'ema'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elephant Guide'),
    (select id from sets where short_name = 'ema'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sentinel Spider'),
    (select id from sets where short_name = 'ema'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Centaur Chieftain'),
    (select id from sets where short_name = 'ema'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Ingester'),
    (select id from sets where short_name = 'ema'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quiet Speculation'),
    (select id from sets where short_name = 'ema'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ichorid'),
    (select id from sets where short_name = 'ema'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Victimize'),
    (select id from sets where short_name = 'ema'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trygon Predator'),
    (select id from sets where short_name = 'ema'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crater Hellion'),
    (select id from sets where short_name = 'ema'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balance'),
    (select id from sets where short_name = 'ema'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Civic Wayfinder'),
    (select id from sets where short_name = 'ema'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathrite Shaman'),
    (select id from sets where short_name = 'ema'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ballynock Cohort'),
    (select id from sets where short_name = 'ema'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blightsoil Druid'),
    (select id from sets where short_name = 'ema'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Siege-Gang Commander'),
    (select id from sets where short_name = 'ema'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carrion Feeder'),
    (select id from sets where short_name = 'ema'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mogg Fanatic'),
    (select id from sets where short_name = 'ema'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carbonize'),
    (select id from sets where short_name = 'ema'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Solifuge'),
    (select id from sets where short_name = 'ema'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Library'),
    (select id from sets where short_name = 'ema'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seismic Stomp'),
    (select id from sets where short_name = 'ema'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Hollow'),
    (select id from sets where short_name = 'ema'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pilgrim''s Eye'),
    (select id from sets where short_name = 'ema'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Juggernaut'),
    (select id from sets where short_name = 'ema'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ticking Gnomes'),
    (select id from sets where short_name = 'ema'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Invigorate'),
    (select id from sets where short_name = 'ema'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Emmessi Tome'),
    (select id from sets where short_name = 'ema'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mogg War Marshal'),
    (select id from sets where short_name = 'ema'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk Looter'),
    (select id from sets where short_name = 'ema'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Baleful Strix'),
    (select id from sets where short_name = 'ema'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roots'),
    (select id from sets where short_name = 'ema'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kird Ape'),
    (select id from sets where short_name = 'ema'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glare of Subdual'),
    (select id from sets where short_name = 'ema'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flame Jab'),
    (select id from sets where short_name = 'ema'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sprite Noble'),
    (select id from sets where short_name = 'ema'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Night''s Whisper'),
    (select id from sets where short_name = 'ema'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcanis the Omnipotent'),
    (select id from sets where short_name = 'ema'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chrome Mox'),
    (select id from sets where short_name = 'ema'),
    '219',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vampiric Tutor'),
    (select id from sets where short_name = 'ema'),
    '112',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wakedancer'),
    (select id from sets where short_name = 'ema'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tidal Wave'),
    (select id from sets where short_name = 'ema'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unexpectedly Absent'),
    (select id from sets where short_name = 'ema'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serendib Efreet'),
    (select id from sets where short_name = 'ema'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Force of Will'),
    (select id from sets where short_name = 'ema'),
    '49',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Welkin Guide'),
    (select id from sets where short_name = 'ema'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daze'),
    (select id from sets where short_name = 'ema'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eight-and-a-Half-Tails'),
    (select id from sets where short_name = 'ema'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deadbridge Shaman'),
    (select id from sets where short_name = 'ema'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silvos, Rogue Elemental'),
    (select id from sets where short_name = 'ema'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Rager'),
    (select id from sets where short_name = 'ema'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = 'ema'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cephalid Sage'),
    (select id from sets where short_name = 'ema'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Braids, Cabal Minion'),
    (select id from sets where short_name = 'ema'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Omens'),
    (select id from sets where short_name = 'ema'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diminishing Returns'),
    (select id from sets where short_name = 'ema'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armadillo Cloak'),
    (select id from sets where short_name = 'ema'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seal of Cleansing'),
    (select id from sets where short_name = 'ema'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mother of Runes'),
    (select id from sets where short_name = 'ema'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Future Sight'),
    (select id from sets where short_name = 'ema'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flame-Kin Zealot'),
    (select id from sets where short_name = 'ema'),
    '201',
    'uncommon'
) 
 on conflict do nothing;
