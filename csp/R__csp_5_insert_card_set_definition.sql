insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Diamond Faerie'),
    (select id from sets where short_name = 'csp'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Rimerunner'),
    (select id from sets where short_name = 'csp'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boreal Centaur'),
    (select id from sets where short_name = 'csp'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Panglacial Wurm'),
    (select id from sets where short_name = 'csp'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Perilous Research'),
    (select id from sets where short_name = 'csp'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hibernation''s End'),
    (select id from sets where short_name = 'csp'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Mountain'),
    (select id from sets where short_name = 'csp'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Controvert'),
    (select id from sets where short_name = 'csp'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Haakon, Stromgald Scourge'),
    (select id from sets where short_name = 'csp'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rimescale Dragon'),
    (select id from sets where short_name = 'csp'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jötun Owl Keeper'),
    (select id from sets where short_name = 'csp'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krovikan Rot'),
    (select id from sets where short_name = 'csp'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balduvian Frostwaker'),
    (select id from sets where short_name = 'csp'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scrying Sheets'),
    (select id from sets where short_name = 'csp'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ursine Fylgja'),
    (select id from sets where short_name = 'csp'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Surging Dementia'),
    (select id from sets where short_name = 'csp'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swift Maneuver'),
    (select id from sets where short_name = 'csp'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brooding Saurian'),
    (select id from sets where short_name = 'csp'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Surging Sentinels'),
    (select id from sets where short_name = 'csp'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feast of Flesh'),
    (select id from sets where short_name = 'csp'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bull Aurochs'),
    (select id from sets where short_name = 'csp'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mouth of Ronom'),
    (select id from sets where short_name = 'csp'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chill to the Bone'),
    (select id from sets where short_name = 'csp'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrumming Stone'),
    (select id from sets where short_name = 'csp'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran War Cry'),
    (select id from sets where short_name = 'csp'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garza''s Assassin'),
    (select id from sets where short_name = 'csp'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Garza Zol, Plague Queen'),
    (select id from sets where short_name = 'csp'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boreal Griffin'),
    (select id from sets where short_name = 'csp'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'White Shield Crusader'),
    (select id from sets where short_name = 'csp'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Martyr of Spores'),
    (select id from sets where short_name = 'csp'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rimehorn Aurochs'),
    (select id from sets where short_name = 'csp'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Highland Weald'),
    (select id from sets where short_name = 'csp'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Luminesce'),
    (select id from sets where short_name = 'csp'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heidar, Rimewind Master'),
    (select id from sets where short_name = 'csp'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ronom Serpent'),
    (select id from sets where short_name = 'csp'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boreal Shelf'),
    (select id from sets where short_name = 'csp'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Braid of Fire'),
    (select id from sets where short_name = 'csp'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Field Marshal'),
    (select id from sets where short_name = 'csp'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krovikan Mist'),
    (select id from sets where short_name = 'csp'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rimewind Cryomancer'),
    (select id from sets where short_name = 'csp'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunscour'),
    (select id from sets where short_name = 'csp'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karplusan Strider'),
    (select id from sets where short_name = 'csp'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rune Snag'),
    (select id from sets where short_name = 'csp'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boreal Druid'),
    (select id from sets where short_name = 'csp'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Gargoyle'),
    (select id from sets where short_name = 'csp'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frostweb Spider'),
    (select id from sets where short_name = 'csp'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Javelineer'),
    (select id from sets where short_name = 'csp'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Ironfoot'),
    (select id from sets where short_name = 'csp'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frost Raptor'),
    (select id from sets where short_name = 'csp'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stalking Yeti'),
    (select id from sets where short_name = 'csp'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zur the Enchanter'),
    (select id from sets where short_name = 'csp'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Spike'),
    (select id from sets where short_name = 'csp'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Freyalise''s Radiance'),
    (select id from sets where short_name = 'csp'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tresserhorn Sinks'),
    (select id from sets where short_name = 'csp'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Furrier'),
    (select id from sets where short_name = 'csp'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Musher'),
    (select id from sets where short_name = 'csp'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thermal Flux'),
    (select id from sets where short_name = 'csp'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shape of the Wiitigo'),
    (select id from sets where short_name = 'csp'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jötun Grunt'),
    (select id from sets where short_name = 'csp'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glacial Plating'),
    (select id from sets where short_name = 'csp'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Martyr of Sands'),
    (select id from sets where short_name = 'csp'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darien, King of Kjeldor'),
    (select id from sets where short_name = 'csp'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Martyr of Ashes'),
    (select id from sets where short_name = 'csp'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karplusan Wolverine'),
    (select id from sets where short_name = 'csp'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arctic Flats'),
    (select id from sets where short_name = 'csp'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balduvian Rage'),
    (select id from sets where short_name = 'csp'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rimebound Dead'),
    (select id from sets where short_name = 'csp'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balduvian Fallen'),
    (select id from sets where short_name = 'csp'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cryoclasm'),
    (select id from sets where short_name = 'csp'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Island'),
    (select id from sets where short_name = 'csp'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icefall'),
    (select id from sets where short_name = 'csp'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tamanoa'),
    (select id from sets where short_name = 'csp'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vanish into Memory'),
    (select id from sets where short_name = 'csp'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jester''s Scepter'),
    (select id from sets where short_name = 'csp'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frozen Solid'),
    (select id from sets where short_name = 'csp'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Bloodpainter'),
    (select id from sets where short_name = 'csp'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greater Stone Spirit'),
    (select id from sets where short_name = 'csp'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Into the North'),
    (select id from sets where short_name = 'csp'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Commandeer'),
    (select id from sets where short_name = 'csp'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rimefeather Owl'),
    (select id from sets where short_name = 'csp'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vexing Sphinx'),
    (select id from sets where short_name = 'csp'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Forest'),
    (select id from sets where short_name = 'csp'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Shards'),
    (select id from sets where short_name = 'csp'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Survivor of the Unseen'),
    (select id from sets where short_name = 'csp'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Bauble'),
    (select id from sets where short_name = 'csp'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steam Spitter'),
    (select id from sets where short_name = 'csp'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Martyr of Bones'),
    (select id from sets where short_name = 'csp'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ohran Yeti'),
    (select id from sets where short_name = 'csp'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arctic Nishoba'),
    (select id from sets where short_name = 'csp'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disciple of Tevesh Szat'),
    (select id from sets where short_name = 'csp'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cover of Winter'),
    (select id from sets where short_name = 'csp'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Depths'),
    (select id from sets where short_name = 'csp'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sek''Kuar, Deathkeeper'),
    (select id from sets where short_name = 'csp'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drelnoch'),
    (select id from sets where short_name = 'csp'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woolly Razorback'),
    (select id from sets where short_name = 'csp'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karplusan Minotaur'),
    (select id from sets where short_name = 'csp'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Etchings'),
    (select id from sets where short_name = 'csp'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magmatic Core'),
    (select id from sets where short_name = 'csp'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Surging Flame'),
    (select id from sets where short_name = 'csp'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Soulgorger'),
    (select id from sets where short_name = 'csp'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Snowcrusher'),
    (select id from sets where short_name = 'csp'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phobian Phantasm'),
    (select id from sets where short_name = 'csp'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deepfire Elemental'),
    (select id from sets where short_name = 'csp'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balduvian Warlord'),
    (select id from sets where short_name = 'csp'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stromgald Crusader'),
    (select id from sets where short_name = 'csp'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jokulmorder'),
    (select id from sets where short_name = 'csp'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Surging Might'),
    (select id from sets where short_name = 'csp'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Herald of Leshrac'),
    (select id from sets where short_name = 'csp'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ronom Unicorn'),
    (select id from sets where short_name = 'csp'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sound the Call'),
    (select id from sets where short_name = 'csp'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lovisa Coldeyes'),
    (select id from sets where short_name = 'csp'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Plains'),
    (select id from sets where short_name = 'csp'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Martyr of Frost'),
    (select id from sets where short_name = 'csp'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earthen Goo'),
    (select id from sets where short_name = 'csp'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skred'),
    (select id from sets where short_name = 'csp'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squall Drifter'),
    (select id from sets where short_name = 'csp'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wilderness Elemental'),
    (select id from sets where short_name = 'csp'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tresserhorn Skyknight'),
    (select id from sets where short_name = 'csp'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gutless Ghoul'),
    (select id from sets where short_name = 'csp'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Void Maw'),
    (select id from sets where short_name = 'csp'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ohran Viper'),
    (select id from sets where short_name = 'csp'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Outrider'),
    (select id from sets where short_name = 'csp'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krovikan Scoundrel'),
    (select id from sets where short_name = 'csp'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grim Harvest'),
    (select id from sets where short_name = 'csp'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sun''s Bounty'),
    (select id from sets where short_name = 'csp'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Melting'),
    (select id from sets where short_name = 'csp'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Surging Aether'),
    (select id from sets where short_name = 'csp'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rite of Flame'),
    (select id from sets where short_name = 'csp'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thermopod'),
    (select id from sets where short_name = 'csp'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Adarkar Windform'),
    (select id from sets where short_name = 'csp'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Counterbalance'),
    (select id from sets where short_name = 'csp'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Swamp'),
    (select id from sets where short_name = 'csp'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Resize'),
    (select id from sets where short_name = 'csp'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Storm'),
    (select id from sets where short_name = 'csp'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coldsteel Heart'),
    (select id from sets where short_name = 'csp'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Juniper Order Ranger'),
    (select id from sets where short_name = 'csp'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flashfreeze'),
    (select id from sets where short_name = 'csp'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chilling Shade'),
    (select id from sets where short_name = 'csp'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sheltering Ancient'),
    (select id from sets where short_name = 'csp'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gelid Shackles'),
    (select id from sets where short_name = 'csp'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aurochs Herd'),
    (select id from sets where short_name = 'csp'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blizzard Specter'),
    (select id from sets where short_name = 'csp'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcum Dagsson'),
    (select id from sets where short_name = 'csp'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simian Brawler'),
    (select id from sets where short_name = 'csp'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Allosaurus Rider'),
    (select id from sets where short_name = 'csp'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rime Transfusion'),
    (select id from sets where short_name = 'csp'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ronom Hulk'),
    (select id from sets where short_name = 'csp'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathmark'),
    (select id from sets where short_name = 'csp'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frost Marsh'),
    (select id from sets where short_name = 'csp'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krovikan Whispers'),
    (select id from sets where short_name = 'csp'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rimewind Taskmage'),
    (select id from sets where short_name = 'csp'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Adarkar Valkyrie'),
    (select id from sets where short_name = 'csp'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fury of the Horde'),
    (select id from sets where short_name = 'csp'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Serpent'),
    (select id from sets where short_name = 'csp'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gristle Grinner'),
    (select id from sets where short_name = 'csp'),
    '59',
    'uncommon'
) 
 on conflict do nothing;
