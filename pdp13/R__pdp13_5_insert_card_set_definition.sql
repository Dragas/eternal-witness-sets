insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Vampire Nocturnus'),
    (select id from sets where short_name = 'pdp13'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Serra Avatar'),
    (select id from sets where short_name = 'pdp13'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Primordial Hydra'),
    (select id from sets where short_name = 'pdp13'),
    '1',
    'mythic'
) 
 on conflict do nothing;
