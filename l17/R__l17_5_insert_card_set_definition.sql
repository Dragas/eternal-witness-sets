insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Gremlin // Energy Reserve'),
    (select id from sets where short_name = 'l17'),
    '1',
    'common'
) 
 on conflict do nothing;
