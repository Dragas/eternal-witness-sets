insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Gremlin // Energy Reserve'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gremlin // Energy Reserve'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gremlin // Energy Reserve'),
        (select types.id from types where name = 'Gremlin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gremlin // Energy Reserve'),
        (select types.id from types where name = '//')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gremlin // Energy Reserve'),
        (select types.id from types where name = 'Card')
    ) 
 on conflict do nothing;
