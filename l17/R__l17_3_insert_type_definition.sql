    insert into types (name) values('Token') on conflict do nothing;
    insert into types (name) values('Creature') on conflict do nothing;
    insert into types (name) values('Gremlin') on conflict do nothing;
    insert into types (name) values('//') on conflict do nothing;
    insert into types (name) values('Card') on conflict do nothing;
