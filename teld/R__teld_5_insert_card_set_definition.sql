insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Food'),
    (select id from sets where short_name = 'teld'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goat'),
    (select id from sets where short_name = 'teld'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Food'),
    (select id from sets where short_name = 'teld'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boar'),
    (select id from sets where short_name = 'teld'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant'),
    (select id from sets where short_name = 'teld'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Food'),
    (select id from sets where short_name = 'teld'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight'),
    (select id from sets where short_name = 'teld'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'teld'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Human'),
    (select id from sets where short_name = 'teld'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'On an Adventure'),
    (select id from sets where short_name = 'teld'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bear'),
    (select id from sets where short_name = 'teld'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie'),
    (select id from sets where short_name = 'teld'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Food'),
    (select id from sets where short_name = 'teld'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Human Warrior'),
    (select id from sets where short_name = 'teld'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarf'),
    (select id from sets where short_name = 'teld'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Human Rogue'),
    (select id from sets where short_name = 'teld'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk, Cursed Huntsman Emblem'),
    (select id from sets where short_name = 'teld'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Human Cleric'),
    (select id from sets where short_name = 'teld'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rat'),
    (select id from sets where short_name = 'teld'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mouse'),
    (select id from sets where short_name = 'teld'),
    '4',
    'common'
) 
 on conflict do nothing;
