insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 't10e'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 't10e'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 't10e'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wasp'),
    (select id from sets where short_name = 't10e'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 't10e'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 't10e'),
    '4',
    'common'
) 
 on conflict do nothing;
