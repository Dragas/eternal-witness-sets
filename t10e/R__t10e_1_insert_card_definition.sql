    insert into mtgcard(name) values ('Saproling') on conflict do nothing;
    insert into mtgcard(name) values ('Zombie') on conflict do nothing;
    insert into mtgcard(name) values ('Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Wasp') on conflict do nothing;
    insert into mtgcard(name) values ('Soldier') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin') on conflict do nothing;
