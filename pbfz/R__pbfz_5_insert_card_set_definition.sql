insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Felidar Sovereign'),
    (select id from sets where short_name = 'pbfz'),
    '26s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serpentine Spike'),
    (select id from sets where short_name = 'pbfz'),
    '133s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gideon, Ally of Zendikar'),
    (select id from sets where short_name = 'pbfz'),
    '29s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Brutal Expulsion'),
    (select id from sets where short_name = 'pbfz'),
    '200s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drowner of Hope'),
    (select id from sets where short_name = 'pbfz'),
    '57s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noyan Dar, Roil Shaper'),
    (select id from sets where short_name = 'pbfz'),
    '216s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief Hydra'),
    (select id from sets where short_name = 'pbfz'),
    '181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scythe Leopard'),
    (select id from sets where short_name = 'pbfz'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blight Herder'),
    (select id from sets where short_name = 'pbfz'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Defiant Bloodlord'),
    (select id from sets where short_name = 'pbfz'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Radiant Flames'),
    (select id from sets where short_name = 'pbfz'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prairie Stream'),
    (select id from sets where short_name = 'pbfz'),
    '241s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woodland Wanderer'),
    (select id from sets where short_name = 'pbfz'),
    '198s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scatter to the Winds'),
    (select id from sets where short_name = 'pbfz'),
    '85s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emeria Shepherd'),
    (select id from sets where short_name = 'pbfz'),
    '22s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunken Hollow'),
    (select id from sets where short_name = 'pbfz'),
    '249s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akoum Firebird'),
    (select id from sets where short_name = 'pbfz'),
    '138s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Undergrowth Champion'),
    (select id from sets where short_name = 'pbfz'),
    '197s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shrine of the Forsaken Gods'),
    (select id from sets where short_name = 'pbfz'),
    '245s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zada, Hedron Grinder'),
    (select id from sets where short_name = 'pbfz'),
    '162s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guardian of Tazeem'),
    (select id from sets where short_name = 'pbfz'),
    '78s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dust Stalker'),
    (select id from sets where short_name = 'pbfz'),
    '202s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lantern Scout'),
    (select id from sets where short_name = 'pbfz'),
    '37s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Defiant Bloodlord'),
    (select id from sets where short_name = 'pbfz'),
    '107s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis Reignited'),
    (select id from sets where short_name = 'pbfz'),
    '119p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kiora, Master of the Depths'),
    (select id from sets where short_name = 'pbfz'),
    '213s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis Reignited'),
    (select id from sets where short_name = 'pbfz'),
    '119s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Prism Array'),
    (select id from sets where short_name = 'pbfz'),
    '81s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barrage Tyrant'),
    (select id from sets where short_name = 'pbfz'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desolation Twin'),
    (select id from sets where short_name = 'pbfz'),
    '6s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Painful Truths'),
    (select id from sets where short_name = 'pbfz'),
    '120p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Omnath, Locus of Rage'),
    (select id from sets where short_name = 'pbfz'),
    '217s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Beastcaller Savant'),
    (select id from sets where short_name = 'pbfz'),
    '170s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Endless One'),
    (select id from sets where short_name = 'pbfz'),
    '8s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonmaster Outcast'),
    (select id from sets where short_name = 'pbfz'),
    '144p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cinder Glade'),
    (select id from sets where short_name = 'pbfz'),
    '235s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brood Butcher'),
    (select id from sets where short_name = 'pbfz'),
    '199s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Canopy Vista'),
    (select id from sets where short_name = 'pbfz'),
    '234s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Planar Outburst'),
    (select id from sets where short_name = 'pbfz'),
    '42s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'From Beyond'),
    (select id from sets where short_name = 'pbfz'),
    '167s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hero of Goma Fada'),
    (select id from sets where short_name = 'pbfz'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'March from the Tomb'),
    (select id from sets where short_name = 'pbfz'),
    '214s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Munda, Ambush Leader'),
    (select id from sets where short_name = 'pbfz'),
    '215s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa''s Renewal'),
    (select id from sets where short_name = 'pbfz'),
    '180s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fathom Feeder'),
    (select id from sets where short_name = 'pbfz'),
    '203s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruinous Path'),
    (select id from sets where short_name = 'pbfz'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Painful Truths'),
    (select id from sets where short_name = 'pbfz'),
    '120s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shambling Vent'),
    (select id from sets where short_name = 'pbfz'),
    '244s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ulamog, the Ceaseless Hunger'),
    (select id from sets where short_name = 'pbfz'),
    '15s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Conduit of Ruin'),
    (select id from sets where short_name = 'pbfz'),
    '4s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bring to Light'),
    (select id from sets where short_name = 'pbfz'),
    '209s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief Hydra'),
    (select id from sets where short_name = 'pbfz'),
    '181s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akoum Hellkite'),
    (select id from sets where short_name = 'pbfz'),
    '139s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Veteran Warleader'),
    (select id from sets where short_name = 'pbfz'),
    '221s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Radiant Flames'),
    (select id from sets where short_name = 'pbfz'),
    '151s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barrage Tyrant'),
    (select id from sets where short_name = 'pbfz'),
    '127s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ally Encampment'),
    (select id from sets where short_name = 'pbfz'),
    '228s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blight Herder'),
    (select id from sets where short_name = 'pbfz'),
    '2s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aligned Hedron Network'),
    (select id from sets where short_name = 'pbfz'),
    '222s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oblivion Sower'),
    (select id from sets where short_name = 'pbfz'),
    '11s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Quarantine Field'),
    (select id from sets where short_name = 'pbfz'),
    '43s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lumbering Falls'),
    (select id from sets where short_name = 'pbfz'),
    '239s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angelic Captain'),
    (select id from sets where short_name = 'pbfz'),
    '208s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruinous Path'),
    (select id from sets where short_name = 'pbfz'),
    '123s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smoldering Marsh'),
    (select id from sets where short_name = 'pbfz'),
    '247s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sanctum of Ugin'),
    (select id from sets where short_name = 'pbfz'),
    '242s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonmaster Outcast'),
    (select id from sets where short_name = 'pbfz'),
    '144s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gruesome Slaughter'),
    (select id from sets where short_name = 'pbfz'),
    '9s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guul Draz Overseer'),
    (select id from sets where short_name = 'pbfz'),
    '112s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ugin''s Insight'),
    (select id from sets where short_name = 'pbfz'),
    '87s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drana, Liberator of Malakir'),
    (select id from sets where short_name = 'pbfz'),
    '109s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Exert Influence'),
    (select id from sets where short_name = 'pbfz'),
    '77s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wasteland Strangler'),
    (select id from sets where short_name = 'pbfz'),
    '102s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drowner of Hope'),
    (select id from sets where short_name = 'pbfz'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Part the Waterveil'),
    (select id from sets where short_name = 'pbfz'),
    '80s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Greenwarden of Murasa'),
    (select id from sets where short_name = 'pbfz'),
    '174s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Greenwarden of Murasa'),
    (select id from sets where short_name = 'pbfz'),
    '174p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Smothering Abomination'),
    (select id from sets where short_name = 'pbfz'),
    '99s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hero of Goma Fada'),
    (select id from sets where short_name = 'pbfz'),
    '31s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Void Winnower'),
    (select id from sets where short_name = 'pbfz'),
    '17s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stasis Snare'),
    (select id from sets where short_name = 'pbfz'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sire of Stagnation'),
    (select id from sets where short_name = 'pbfz'),
    '206s',
    'mythic'
) 
 on conflict do nothing;
