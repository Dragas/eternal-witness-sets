insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Vindicate'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ravenous Baloth'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ravenous Baloth'),
        (select types.id from types where name = 'Beast')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Yawgmoth''s Will'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cunning Wish'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Decree of Justice'),
        (select types.id from types where name = 'Sorcery')
    ) 
 on conflict do nothing;
