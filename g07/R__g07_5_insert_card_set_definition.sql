insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Vindicate'),
    (select id from sets where short_name = 'g07'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ravenous Baloth'),
    (select id from sets where short_name = 'g07'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yawgmoth''s Will'),
    (select id from sets where short_name = 'g07'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cunning Wish'),
    (select id from sets where short_name = 'g07'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Decree of Justice'),
    (select id from sets where short_name = 'g07'),
    '5',
    'rare'
) 
 on conflict do nothing;
