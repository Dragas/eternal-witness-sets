    insert into mtgcard(name) values ('Vindicate') on conflict do nothing;
    insert into mtgcard(name) values ('Ravenous Baloth') on conflict do nothing;
    insert into mtgcard(name) values ('Yawgmoth''s Will') on conflict do nothing;
    insert into mtgcard(name) values ('Cunning Wish') on conflict do nothing;
    insert into mtgcard(name) values ('Decree of Justice') on conflict do nothing;
