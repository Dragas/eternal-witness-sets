insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'City of Shadows'),
    (select id from sets where short_name = 'me3'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grim Tutor'),
    (select id from sets where short_name = 'me3'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kobold Overlord'),
    (select id from sets where short_name = 'me3'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Capture of Jingzhou'),
    (select id from sets where short_name = 'me3'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Killer Bees'),
    (select id from sets where short_name = 'me3'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ragnar'),
    (select id from sets where short_name = 'me3'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wei Strike Force'),
    (select id from sets where short_name = 'me3'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Living Plane'),
    (select id from sets where short_name = 'me3'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anaba Spirit Crafter'),
    (select id from sets where short_name = 'me3'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Island'),
    (select id from sets where short_name = 'me3'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nova Pentacle'),
    (select id from sets where short_name = 'me3'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'me3'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forced Retreat'),
    (select id from sets where short_name = 'me3'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Banshee'),
    (select id from sets where short_name = 'me3'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bone Flute'),
    (select id from sets where short_name = 'me3'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Firestorm Phoenix'),
    (select id from sets where short_name = 'me3'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rohgahh of Kher Keep'),
    (select id from sets where short_name = 'me3'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'me3'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sivitri Scarzam'),
    (select id from sets where short_name = 'me3'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chromium'),
    (select id from sets where short_name = 'me3'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shu General'),
    (select id from sets where short_name = 'me3'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stolen Grain'),
    (select id from sets where short_name = 'me3'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Call to Arms'),
    (select id from sets where short_name = 'me3'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flash Flood'),
    (select id from sets where short_name = 'me3'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zodiac Dragon'),
    (select id from sets where short_name = 'me3'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nebuchadnezzar'),
    (select id from sets where short_name = 'me3'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plateau'),
    (select id from sets where short_name = 'me3'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lu Meng, Wu General'),
    (select id from sets where short_name = 'me3'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scryb Sprites'),
    (select id from sets where short_name = 'me3'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Amrou Kithkin'),
    (select id from sets where short_name = 'me3'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reveka, Wizard Savant'),
    (select id from sets where short_name = 'me3'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rubinia Soulsinger'),
    (select id from sets where short_name = 'me3'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guan Yu, Sainted Warrior'),
    (select id from sets where short_name = 'me3'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sir Shandlar of Eberyn'),
    (select id from sets where short_name = 'me3'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Drake'),
    (select id from sets where short_name = 'me3'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Willow Satyr'),
    (select id from sets where short_name = 'me3'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strategic Planning'),
    (select id from sets where short_name = 'me3'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Blow'),
    (select id from sets where short_name = 'me3'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gabriel Angelfire'),
    (select id from sets where short_name = 'me3'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marhault Elsdragon'),
    (select id from sets where short_name = 'me3'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kobold Drill Sergeant'),
    (select id from sets where short_name = 'me3'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kongming, "Sleeping Dragon"'),
    (select id from sets where short_name = 'me3'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Land Tax'),
    (select id from sets where short_name = 'me3'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Touch'),
    (select id from sets where short_name = 'me3'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karakas'),
    (select id from sets where short_name = 'me3'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desperate Charge'),
    (select id from sets where short_name = 'me3'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'me3'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wei Infantry'),
    (select id from sets where short_name = 'me3'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Willow Priestess'),
    (select id from sets where short_name = 'me3'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Light'),
    (select id from sets where short_name = 'me3'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Active Volcano'),
    (select id from sets where short_name = 'me3'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disharmony'),
    (select id from sets where short_name = 'me3'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heavy Fog'),
    (select id from sets where short_name = 'me3'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'me3'),
    '227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bartel Runeaxe'),
    (select id from sets where short_name = 'me3'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wormwood Treefolk'),
    (select id from sets where short_name = 'me3'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hunding Gjornersen'),
    (select id from sets where short_name = 'me3'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meng Huo, Barbarian King'),
    (select id from sets where short_name = 'me3'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sun Quan, Lord of Wu'),
    (select id from sets where short_name = 'me3'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forked Lightning'),
    (select id from sets where short_name = 'me3'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Xiahou Dun, the One-Eyed'),
    (select id from sets where short_name = 'me3'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hazezon Tamar'),
    (select id from sets where short_name = 'me3'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Immolation'),
    (select id from sets where short_name = 'me3'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Noble'),
    (select id from sets where short_name = 'me3'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'me3'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Frostbeast'),
    (select id from sets where short_name = 'me3'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shu Elite Companions'),
    (select id from sets where short_name = 'me3'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Land Equilibrium'),
    (select id from sets where short_name = 'me3'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Lust'),
    (select id from sets where short_name = 'me3'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shu Soldier-Farmers'),
    (select id from sets where short_name = 'me3'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urborg'),
    (select id from sets where short_name = 'me3'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dance of Many'),
    (select id from sets where short_name = 'me3'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Riding the Dilu Horse'),
    (select id from sets where short_name = 'me3'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spectral Shield'),
    (select id from sets where short_name = 'me3'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tuknir Deathlock'),
    (select id from sets where short_name = 'me3'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chain Lightning'),
    (select id from sets where short_name = 'me3'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heal'),
    (select id from sets where short_name = 'me3'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burning of Xinye'),
    (select id from sets where short_name = 'me3'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'me3'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ramses Overdark'),
    (select id from sets where short_name = 'me3'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jerrard of the Closed Fist'),
    (select id from sets where short_name = 'me3'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zhang Fei, Fierce Warrior'),
    (select id from sets where short_name = 'me3'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cosmic Horror'),
    (select id from sets where short_name = 'me3'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Peach Garden Oath'),
    (select id from sets where short_name = 'me3'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit Shackle'),
    (select id from sets where short_name = 'me3'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arena of the Ancients'),
    (select id from sets where short_name = 'me3'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trip Wire'),
    (select id from sets where short_name = 'me3'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Life Chisel'),
    (select id from sets where short_name = 'me3'),
    '199',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carrion Ants'),
    (select id from sets where short_name = 'me3'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hua Tuo, Honored Physician'),
    (select id from sets where short_name = 'me3'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sun Ce, Young Conquerer'),
    (select id from sets where short_name = 'me3'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Misfortune''s Gain'),
    (select id from sets where short_name = 'me3'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jedit Ojanen'),
    (select id from sets where short_name = 'me3'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Lady of the Mountain'),
    (select id from sets where short_name = 'me3'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Lion'),
    (select id from sets where short_name = 'me3'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stangg'),
    (select id from sets where short_name = 'me3'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spoils of Victory'),
    (select id from sets where short_name = 'me3'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tracker'),
    (select id from sets where short_name = 'me3'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Infuse'),
    (select id from sets where short_name = 'me3'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wandering Mage'),
    (select id from sets where short_name = 'me3'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Remove Soul'),
    (select id from sets where short_name = 'me3'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brilliant Plan'),
    (select id from sets where short_name = 'me3'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hammerheim'),
    (select id from sets where short_name = 'me3'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rasputin Dreamweaver'),
    (select id from sets where short_name = 'me3'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'me3'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Sprites'),
    (select id from sets where short_name = 'me3'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wei Night Raiders'),
    (select id from sets where short_name = 'me3'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cinder Storm'),
    (select id from sets where short_name = 'me3'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kei Takahashi'),
    (select id from sets where short_name = 'me3'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'me3'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lady Caleria'),
    (select id from sets where short_name = 'me3'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcades Sabboth'),
    (select id from sets where short_name = 'me3'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reset'),
    (select id from sets where short_name = 'me3'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kobold Taskmaster'),
    (select id from sets where short_name = 'me3'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wu Longbowman'),
    (select id from sets where short_name = 'me3'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crimson Kobolds'),
    (select id from sets where short_name = 'me3'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evil Presence'),
    (select id from sets where short_name = 'me3'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arboria'),
    (select id from sets where short_name = 'me3'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alabaster Potion'),
    (select id from sets where short_name = 'me3'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Didgeridoo'),
    (select id from sets where short_name = 'me3'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Force Spike'),
    (select id from sets where short_name = 'me3'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gauntlets of Chaos'),
    (select id from sets where short_name = 'me3'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Old Man of the Sea'),
    (select id from sets where short_name = 'me3'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bazaar of Baghdad'),
    (select id from sets where short_name = 'me3'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lu Bu, Master-at-Arms'),
    (select id from sets where short_name = 'me3'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tropical Island'),
    (select id from sets where short_name = 'me3'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sorrow''s Path'),
    (select id from sets where short_name = 'me3'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wu Warship'),
    (select id from sets where short_name = 'me3'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illusionary Mask'),
    (select id from sets where short_name = 'me3'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tobias Andrion'),
    (select id from sets where short_name = 'me3'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bayou'),
    (select id from sets where short_name = 'me3'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Twist'),
    (select id from sets where short_name = 'me3'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Axelrod Gunnarson'),
    (select id from sets where short_name = 'me3'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lu Xun, Scholar General'),
    (select id from sets where short_name = 'me3'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Corrupt Eunuchs'),
    (select id from sets where short_name = 'me3'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hunting Cheetah'),
    (select id from sets where short_name = 'me3'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meng Huo''s Horde'),
    (select id from sets where short_name = 'me3'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Young Wei Recruits'),
    (select id from sets where short_name = 'me3'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'me3'),
    '222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crimson Manticore'),
    (select id from sets where short_name = 'me3'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashes to Ashes'),
    (select id from sets where short_name = 'me3'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Three Visits'),
    (select id from sets where short_name = 'me3'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sol Grail'),
    (select id from sets where short_name = 'me3'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fellwar Stone'),
    (select id from sets where short_name = 'me3'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lady Evangela'),
    (select id from sets where short_name = 'me3'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Takklemaggot'),
    (select id from sets where short_name = 'me3'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nether Void'),
    (select id from sets where short_name = 'me3'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Astrolabe'),
    (select id from sets where short_name = 'me3'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gwendlyn Di Corci'),
    (select id from sets where short_name = 'me3'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Freyalise''s Winds'),
    (select id from sets where short_name = 'me3'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kobolds of Kher Keep'),
    (select id from sets where short_name = 'me3'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frost Giant'),
    (select id from sets where short_name = 'me3'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sword of the Ages'),
    (select id from sets where short_name = 'me3'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghostly Visit'),
    (select id from sets where short_name = 'me3'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demonic Torment'),
    (select id from sets where short_name = 'me3'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Storm World'),
    (select id from sets where short_name = 'me3'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'me3'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'All Hallow''s Eve'),
    (select id from sets where short_name = 'me3'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'me3'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cleanse'),
    (select id from sets where short_name = 'me3'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loyal Retainers'),
    (select id from sets where short_name = 'me3'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spiny Starfish'),
    (select id from sets where short_name = 'me3'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'me3'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tor Wauki'),
    (select id from sets where short_name = 'me3'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Akron Legionnaire'),
    (select id from sets where short_name = 'me3'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boris Devilboon'),
    (select id from sets where short_name = 'me3'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'me3'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shu Cavalry'),
    (select id from sets where short_name = 'me3'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Vortex'),
    (select id from sets where short_name = 'me3'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angus Mackenzie'),
    (select id from sets where short_name = 'me3'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Black Vise'),
    (select id from sets where short_name = 'me3'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunastian Falconer'),
    (select id from sets where short_name = 'me3'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coal Golem'),
    (select id from sets where short_name = 'me3'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dong Zhou, the Tyrant'),
    (select id from sets where short_name = 'me3'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scrubland'),
    (select id from sets where short_name = 'me3'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Princess Lucrezia'),
    (select id from sets where short_name = 'me3'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barktooth Warbeard'),
    (select id from sets where short_name = 'me3'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anaba Ancestor'),
    (select id from sets where short_name = 'me3'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knowledge Vault'),
    (select id from sets where short_name = 'me3'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slashing Tiger'),
    (select id from sets where short_name = 'me3'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barl''s Cage'),
    (select id from sets where short_name = 'me3'),
    '190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raging Minotaur'),
    (select id from sets where short_name = 'me3'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voodoo Doll'),
    (select id from sets where short_name = 'me3'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pavel Maliki'),
    (select id from sets where short_name = 'me3'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rolling Earthquake'),
    (select id from sets where short_name = 'me3'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ramirez DePietro'),
    (select id from sets where short_name = 'me3'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Famine'),
    (select id from sets where short_name = 'me3'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Halfdane'),
    (select id from sets where short_name = 'me3'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desert Twister'),
    (select id from sets where short_name = 'me3'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elves of Deep Shadow'),
    (select id from sets where short_name = 'me3'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Tabernacle at Pendrell Vale'),
    (select id from sets where short_name = 'me3'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guan Yu''s 1,000-Li March'),
    (select id from sets where short_name = 'me3'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wu Elite Cavalry'),
    (select id from sets where short_name = 'me3'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eightfold Maze'),
    (select id from sets where short_name = 'me3'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghosts of the Damned'),
    (select id from sets where short_name = 'me3'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Livonya Silone'),
    (select id from sets where short_name = 'me3'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fevered Strength'),
    (select id from sets where short_name = 'me3'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Benthic Explorers'),
    (select id from sets where short_name = 'me3'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'me3'),
    '223',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lady Orca'),
    (select id from sets where short_name = 'me3'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lesser Werewolf'),
    (select id from sets where short_name = 'me3'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Recall'),
    (select id from sets where short_name = 'me3'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liu Bei, Lord of Shu'),
    (select id from sets where short_name = 'me3'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vaevictis Asmadi'),
    (select id from sets where short_name = 'me3'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'me3'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'D''Avenant Archer'),
    (select id from sets where short_name = 'me3'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reincarnation'),
    (select id from sets where short_name = 'me3'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Divine Intervention'),
    (select id from sets where short_name = 'me3'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Labyrinth Minotaur'),
    (select id from sets where short_name = 'me3'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hellfire'),
    (select id from sets where short_name = 'me3'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Torsten Von Ursus'),
    (select id from sets where short_name = 'me3'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas'),
    (select id from sets where short_name = 'me3'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fire Ambush'),
    (select id from sets where short_name = 'me3'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ivory Guardians'),
    (select id from sets where short_name = 'me3'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'False Defeat'),
    (select id from sets where short_name = 'me3'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boomerang'),
    (select id from sets where short_name = 'me3'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hurloon Minotaur'),
    (select id from sets where short_name = 'me3'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Borrowing 100,000 Arrows'),
    (select id from sets where short_name = 'me3'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tetsuo Umezawa'),
    (select id from sets where short_name = 'me3'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'me3'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Drain'),
    (select id from sets where short_name = 'me3'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Riven Turnbull'),
    (select id from sets where short_name = 'me3'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Palladia-Mors'),
    (select id from sets where short_name = 'me3'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Wretched'),
    (select id from sets where short_name = 'me3'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Xira Arien'),
    (select id from sets where short_name = 'me3'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Abyss'),
    (select id from sets where short_name = 'me3'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wei Elite Companions'),
    (select id from sets where short_name = 'me3'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exorcist'),
    (select id from sets where short_name = 'me3'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Concordant Crossroads'),
    (select id from sets where short_name = 'me3'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'me3'),
    '228',
    'common'
) 
 on conflict do nothing;
