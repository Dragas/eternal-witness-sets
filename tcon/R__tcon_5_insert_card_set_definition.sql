insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'tcon'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tcon'),
    '2',
    'common'
) 
 on conflict do nothing;
