insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Nalathni Dragon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nalathni Dragon'),
        (select types.id from types where name = 'Dragon')
    ) 
 on conflict do nothing;
