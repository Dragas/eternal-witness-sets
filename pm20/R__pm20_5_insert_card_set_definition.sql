insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Dungeon Geists'),
    (select id from sets where short_name = 'pm20'),
    '57s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bag of Holding'),
    (select id from sets where short_name = 'pm20'),
    '222s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rotting Regisaur'),
    (select id from sets where short_name = 'pm20'),
    '111s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cavalier of Dawn'),
    (select id from sets where short_name = 'pm20'),
    '10p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Legion''s End'),
    (select id from sets where short_name = 'pm20'),
    '106s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lotus Field'),
    (select id from sets where short_name = 'pm20'),
    '249s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dungeon Geists'),
    (select id from sets where short_name = 'pm20'),
    '57p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'pm20'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steel Overseer'),
    (select id from sets where short_name = 'pm20'),
    '239p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scheming Symmetry'),
    (select id from sets where short_name = 'pm20'),
    '113p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thunderkin Awakener'),
    (select id from sets where short_name = 'pm20'),
    '162p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Triumph'),
    (select id from sets where short_name = 'pm20'),
    '257p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disfigure'),
    (select id from sets where short_name = 'pm20'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cavalier of Thorns'),
    (select id from sets where short_name = 'pm20'),
    '167p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Drawn from Dreams'),
    (select id from sets where short_name = 'pm20'),
    '56p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra, Acolyte of Flame'),
    (select id from sets where short_name = 'pm20'),
    '126p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bishop of Wings'),
    (select id from sets where short_name = 'pm20'),
    '8s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Triumph'),
    (select id from sets where short_name = 'pm20'),
    '257s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Embodiment of Agonies'),
    (select id from sets where short_name = 'pm20'),
    '98p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wakeroot Elemental'),
    (select id from sets where short_name = 'pm20'),
    '202s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yarok, the Desecrated'),
    (select id from sets where short_name = 'pm20'),
    '220s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cavalier of Flame'),
    (select id from sets where short_name = 'pm20'),
    '125s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tale''s End'),
    (select id from sets where short_name = 'pm20'),
    '77s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yarok, the Desecrated'),
    (select id from sets where short_name = 'pm20'),
    '220p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Leyline of the Void'),
    (select id from sets where short_name = 'pm20'),
    '107p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Epiphany'),
    (select id from sets where short_name = 'pm20'),
    '253p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voracious Hydra'),
    (select id from sets where short_name = 'pm20'),
    '200s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drakuseth, Maw of Flames'),
    (select id from sets where short_name = 'pm20'),
    '136p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leyline of Abundance'),
    (select id from sets where short_name = 'pm20'),
    '179p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loxodon Lifechanter'),
    (select id from sets where short_name = 'pm20'),
    '27s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leyline of Combustion'),
    (select id from sets where short_name = 'pm20'),
    '148s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brought Back'),
    (select id from sets where short_name = 'pm20'),
    '9p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cavalier of Dawn'),
    (select id from sets where short_name = 'pm20'),
    '10s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Knight of the Ebon Legion'),
    (select id from sets where short_name = 'pm20'),
    '105p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sorin, Imperious Bloodlord'),
    (select id from sets where short_name = 'pm20'),
    '115s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Corpse Knight'),
    (select id from sets where short_name = 'pm20'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Temple of Mystery'),
    (select id from sets where short_name = 'pm20'),
    '255s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystic Forge'),
    (select id from sets where short_name = 'pm20'),
    '233p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Reclaimer'),
    (select id from sets where short_name = 'pm20'),
    '169s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystic Forge'),
    (select id from sets where short_name = 'pm20'),
    '233s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glint-Horn Buccaneer'),
    (select id from sets where short_name = 'pm20'),
    '141s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mu Yanling, Sky Dancer'),
    (select id from sets where short_name = 'pm20'),
    '68p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sorin, Imperious Bloodlord'),
    (select id from sets where short_name = 'pm20'),
    '115p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Leyline of Anticipation'),
    (select id from sets where short_name = 'pm20'),
    '64p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightpack Ambusher'),
    (select id from sets where short_name = 'pm20'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gargos, Vicious Watcher'),
    (select id from sets where short_name = 'pm20'),
    '172p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leyline of Anticipation'),
    (select id from sets where short_name = 'pm20'),
    '64s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kykar, Wind''s Fury'),
    (select id from sets where short_name = 'pm20'),
    '212s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Field of the Dead'),
    (select id from sets where short_name = 'pm20'),
    '247s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sephara, Sky''s Blade'),
    (select id from sets where short_name = 'pm20'),
    '36p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knight of the Ebon Legion'),
    (select id from sets where short_name = 'pm20'),
    '105s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightpack Ambusher'),
    (select id from sets where short_name = 'pm20'),
    '185s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cavalier of Night'),
    (select id from sets where short_name = 'pm20'),
    '94p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Temple of Epiphany'),
    (select id from sets where short_name = 'pm20'),
    '253s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Icon of Ancestry'),
    (select id from sets where short_name = 'pm20'),
    '229p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivien, Arkbow Ranger'),
    (select id from sets where short_name = 'pm20'),
    '199s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Agent of Treachery'),
    (select id from sets where short_name = 'pm20'),
    '43s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani, Strength of the Pride'),
    (select id from sets where short_name = 'pm20'),
    '2p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Drawn from Dreams'),
    (select id from sets where short_name = 'pm20'),
    '56s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Repeated Reverberation'),
    (select id from sets where short_name = 'pm20'),
    '156p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Field of the Dead'),
    (select id from sets where short_name = 'pm20'),
    '247p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Malady'),
    (select id from sets where short_name = 'pm20'),
    '254p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Malady'),
    (select id from sets where short_name = 'pm20'),
    '254s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cavalier of Gales'),
    (select id from sets where short_name = 'pm20'),
    '52p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cavalier of Night'),
    (select id from sets where short_name = 'pm20'),
    '94s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vilis, Broker of Blood'),
    (select id from sets where short_name = 'pm20'),
    '122p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaalia, Zenith Seeker'),
    (select id from sets where short_name = 'pm20'),
    '210p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Golos, Tireless Pilgrim'),
    (select id from sets where short_name = 'pm20'),
    '226p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flood of Tears'),
    (select id from sets where short_name = 'pm20'),
    '59p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightpack Ambusher'),
    (select id from sets where short_name = 'pm20'),
    '185p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cavalier of Gales'),
    (select id from sets where short_name = 'pm20'),
    '52s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Glint-Horn Buccaneer'),
    (select id from sets where short_name = 'pm20'),
    '141p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bishop of Wings'),
    (select id from sets where short_name = 'pm20'),
    '8p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cavalier of Thorns'),
    (select id from sets where short_name = 'pm20'),
    '167s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gargos, Vicious Watcher'),
    (select id from sets where short_name = 'pm20'),
    '172s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Legion''s End'),
    (select id from sets where short_name = 'pm20'),
    '106p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grafdigger''s Cage'),
    (select id from sets where short_name = 'pm20'),
    '227p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marauding Raptor'),
    (select id from sets where short_name = 'pm20'),
    '150s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Planar Cleansing'),
    (select id from sets where short_name = 'pm20'),
    '33s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leyline of the Void'),
    (select id from sets where short_name = 'pm20'),
    '107s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loxodon Lifechanter'),
    (select id from sets where short_name = 'pm20'),
    '27p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flame Sweep'),
    (select id from sets where short_name = 'pm20'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dread Presence'),
    (select id from sets where short_name = 'pm20'),
    '96p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leyline of Sanctity'),
    (select id from sets where short_name = 'pm20'),
    '26p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Mystery'),
    (select id from sets where short_name = 'pm20'),
    '255p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra, Awakened Inferno'),
    (select id from sets where short_name = 'pm20'),
    '127s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thrashing Brontodon'),
    (select id from sets where short_name = 'pm20'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shifting Ceratops'),
    (select id from sets where short_name = 'pm20'),
    '194s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivien, Arkbow Ranger'),
    (select id from sets where short_name = 'pm20'),
    '199p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kethis, the Hidden Hand'),
    (select id from sets where short_name = 'pm20'),
    '211p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thunderkin Awakener'),
    (select id from sets where short_name = 'pm20'),
    '162s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sephara, Sky''s Blade'),
    (select id from sets where short_name = 'pm20'),
    '36s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lotus Field'),
    (select id from sets where short_name = 'pm20'),
    '249p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bag of Holding'),
    (select id from sets where short_name = 'pm20'),
    '222p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drakuseth, Maw of Flames'),
    (select id from sets where short_name = 'pm20'),
    '136s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Silence'),
    (select id from sets where short_name = 'pm20'),
    '256s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vilis, Broker of Blood'),
    (select id from sets where short_name = 'pm20'),
    '122s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cavalier of Flame'),
    (select id from sets where short_name = 'pm20'),
    '125p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Grafdigger''s Cage'),
    (select id from sets where short_name = 'pm20'),
    '227s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Starfield Mystic'),
    (select id from sets where short_name = 'pm20'),
    '39p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shifting Ceratops'),
    (select id from sets where short_name = 'pm20'),
    '194p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Masterful Replication'),
    (select id from sets where short_name = 'pm20'),
    '65p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Atemsis, All-Seeing'),
    (select id from sets where short_name = 'pm20'),
    '46p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shared Summons'),
    (select id from sets where short_name = 'pm20'),
    '193s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Regulator'),
    (select id from sets where short_name = 'pm20'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hanged Executioner'),
    (select id from sets where short_name = 'pm20'),
    '22p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Embodiment of Agonies'),
    (select id from sets where short_name = 'pm20'),
    '98s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Omnath, Locus of the Roil'),
    (select id from sets where short_name = 'pm20'),
    '216s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dread Presence'),
    (select id from sets where short_name = 'pm20'),
    '96s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mu Yanling, Sky Dancer'),
    (select id from sets where short_name = 'pm20'),
    '68s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Brought Back'),
    (select id from sets where short_name = 'pm20'),
    '9s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kethis, the Hidden Hand'),
    (select id from sets where short_name = 'pm20'),
    '211s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chandra, Acolyte of Flame'),
    (select id from sets where short_name = 'pm20'),
    '126s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Regulator'),
    (select id from sets where short_name = 'pm20'),
    '131s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rotting Regisaur'),
    (select id from sets where short_name = 'pm20'),
    '111p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flood of Tears'),
    (select id from sets where short_name = 'pm20'),
    '59s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Masterful Replication'),
    (select id from sets where short_name = 'pm20'),
    '65s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Regulator'),
    (select id from sets where short_name = 'pm20'),
    '131p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaalia, Zenith Seeker'),
    (select id from sets where short_name = 'pm20'),
    '210s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Leyline of Abundance'),
    (select id from sets where short_name = 'pm20'),
    '179s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tale''s End'),
    (select id from sets where short_name = 'pm20'),
    '77p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golos, Tireless Pilgrim'),
    (select id from sets where short_name = 'pm20'),
    '226s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Atemsis, All-Seeing'),
    (select id from sets where short_name = 'pm20'),
    '46s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Silence'),
    (select id from sets where short_name = 'pm20'),
    '256p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Reclaimer'),
    (select id from sets where short_name = 'pm20'),
    '169p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scheming Symmetry'),
    (select id from sets where short_name = 'pm20'),
    '113s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Planar Cleansing'),
    (select id from sets where short_name = 'pm20'),
    '33p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steel Overseer'),
    (select id from sets where short_name = 'pm20'),
    '239s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kykar, Wind''s Fury'),
    (select id from sets where short_name = 'pm20'),
    '212p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wakeroot Elemental'),
    (select id from sets where short_name = 'pm20'),
    '202p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leyline of Combustion'),
    (select id from sets where short_name = 'pm20'),
    '148p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Repeated Reverberation'),
    (select id from sets where short_name = 'pm20'),
    '156s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marauding Raptor'),
    (select id from sets where short_name = 'pm20'),
    '150p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voracious Hydra'),
    (select id from sets where short_name = 'pm20'),
    '200p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Icon of Ancestry'),
    (select id from sets where short_name = 'pm20'),
    '229s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leyline of Sanctity'),
    (select id from sets where short_name = 'pm20'),
    '26s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Agent of Treachery'),
    (select id from sets where short_name = 'pm20'),
    '43p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Starfield Mystic'),
    (select id from sets where short_name = 'pm20'),
    '39s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra, Awakened Inferno'),
    (select id from sets where short_name = 'pm20'),
    '127p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shared Summons'),
    (select id from sets where short_name = 'pm20'),
    '193p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Omnath, Locus of the Roil'),
    (select id from sets where short_name = 'pm20'),
    '216p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ajani, Strength of the Pride'),
    (select id from sets where short_name = 'pm20'),
    '2s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hanged Executioner'),
    (select id from sets where short_name = 'pm20'),
    '22s',
    'rare'
) 
 on conflict do nothing;
