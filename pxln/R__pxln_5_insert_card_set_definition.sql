insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Thaumatic Compass // Spires of Orazca'),
    (select id from sets where short_name = 'pxln'),
    '249s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Axis of Mortality'),
    (select id from sets where short_name = 'pxln'),
    '3s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vraska, Relic Seeker'),
    (select id from sets where short_name = 'pxln'),
    '232s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Treasure Map // Treasure Cove'),
    (select id from sets where short_name = 'pxln'),
    '250s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunbird''s Invocation'),
    (select id from sets where short_name = 'pxln'),
    '165p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goring Ceratops'),
    (select id from sets where short_name = 'pxln'),
    '13s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Regisaur Alpha'),
    (select id from sets where short_name = 'pxln'),
    '227s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Entrancing Melody'),
    (select id from sets where short_name = 'pxln'),
    '55p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vance''s Blasting Cannons // Spitfire Bastion'),
    (select id from sets where short_name = 'pxln'),
    '173s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glacial Fortress'),
    (select id from sets where short_name = 'pxln'),
    '255p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carnage Tyrant'),
    (select id from sets where short_name = 'pxln'),
    '179p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sword-Point Diplomacy'),
    (select id from sets where short_name = 'pxln'),
    '126p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sorcerous Spyglass'),
    (select id from sets where short_name = 'pxln'),
    '248s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rootbound Crag'),
    (select id from sets where short_name = 'pxln'),
    '256s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vanquisher''s Banner'),
    (select id from sets where short_name = 'pxln'),
    '251s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wakening Sun''s Avatar'),
    (select id from sets where short_name = 'pxln'),
    '44s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Verdant Sun''s Avatar'),
    (select id from sets where short_name = 'pxln'),
    '213p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dire Fleet Ravager'),
    (select id from sets where short_name = 'pxln'),
    '104s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bishop of Rebirth'),
    (select id from sets where short_name = 'pxln'),
    '5s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Priest of the Wakening Sun'),
    (select id from sets where short_name = 'pxln'),
    '27s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword-Point Diplomacy'),
    (select id from sets where short_name = 'pxln'),
    '126s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shapers'' Sanctuary'),
    (select id from sets where short_name = 'pxln'),
    '206p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Daring Saboteur'),
    (select id from sets where short_name = 'pxln'),
    '49s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Star of Extinction'),
    (select id from sets where short_name = 'pxln'),
    '161s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dowsing Dagger // Lost Vale'),
    (select id from sets where short_name = 'pxln'),
    '235s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deadeye Tracker'),
    (select id from sets where short_name = 'pxln'),
    '99s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Entrancing Melody'),
    (select id from sets where short_name = 'pxln'),
    '55s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tocatli Honor Guard'),
    (select id from sets where short_name = 'pxln'),
    '42p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunpetal Grove'),
    (select id from sets where short_name = 'pxln'),
    '257p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deeproot Champion'),
    (select id from sets where short_name = 'pxln'),
    '185p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodcrazed Paladin'),
    (select id from sets where short_name = 'pxln'),
    '93s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hostage Taker'),
    (select id from sets where short_name = 'pxln'),
    '223s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ripjaw Raptor'),
    (select id from sets where short_name = 'pxln'),
    '203p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Herald of Secret Streams'),
    (select id from sets where short_name = 'pxln'),
    '59s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Legion''s Landing // Adanto, the First Fort'),
    (select id from sets where short_name = 'pxln'),
    '22s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boneyard Parley'),
    (select id from sets where short_name = 'pxln'),
    '94s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ruin Raider'),
    (select id from sets where short_name = 'pxln'),
    '118p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Revel in Riches'),
    (select id from sets where short_name = 'pxln'),
    '117p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashes of the Abhorrent'),
    (select id from sets where short_name = 'pxln'),
    '2p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overflowing Insight'),
    (select id from sets where short_name = 'pxln'),
    '66s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Primal Amulet // Primal Wellspring'),
    (select id from sets where short_name = 'pxln'),
    '243s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deadeye Tracker'),
    (select id from sets where short_name = 'pxln'),
    '99p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burning Sun''s Avatar'),
    (select id from sets where short_name = 'pxln'),
    '135s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vraska''s Contempt'),
    (select id from sets where short_name = 'pxln'),
    '129p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angrath''s Marauders'),
    (select id from sets where short_name = 'pxln'),
    '132s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mavren Fein, Dusk Apostle'),
    (select id from sets where short_name = 'pxln'),
    '24s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Old-Growth Dryads'),
    (select id from sets where short_name = 'pxln'),
    '199s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fathom Fleet Captain'),
    (select id from sets where short_name = 'pxln'),
    '106s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drowned Catacomb'),
    (select id from sets where short_name = 'pxln'),
    '253p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcane Adaptation'),
    (select id from sets where short_name = 'pxln'),
    '46s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Captain Lannery Storm'),
    (select id from sets where short_name = 'pxln'),
    '136s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bishop of Rebirth'),
    (select id from sets where short_name = 'pxln'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burning Sun''s Avatar'),
    (select id from sets where short_name = 'pxln'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreamcaller Siren'),
    (select id from sets where short_name = 'pxln'),
    '54s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spell Swindle'),
    (select id from sets where short_name = 'pxln'),
    '82p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Growing Rites of Itlimoc // Itlimoc, Cradle of the Sun'),
    (select id from sets where short_name = 'pxln'),
    '191s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unclaimed Territory'),
    (select id from sets where short_name = 'pxln'),
    '258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rampaging Ferocidon'),
    (select id from sets where short_name = 'pxln'),
    '154s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcane Adaptation'),
    (select id from sets where short_name = 'pxln'),
    '46p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'River''s Rebuke'),
    (select id from sets where short_name = 'pxln'),
    '71p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Captivating Crew'),
    (select id from sets where short_name = 'pxln'),
    '137p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fleet Swallower'),
    (select id from sets where short_name = 'pxln'),
    '57s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Settle the Wreckage'),
    (select id from sets where short_name = 'pxln'),
    '34p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rowdy Crew'),
    (select id from sets where short_name = 'pxln'),
    '159s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Captivating Crew'),
    (select id from sets where short_name = 'pxln'),
    '137s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruin Raider'),
    (select id from sets where short_name = 'pxln'),
    '118s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sorcerous Spyglass'),
    (select id from sets where short_name = 'pxln'),
    '248p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carnage Tyrant'),
    (select id from sets where short_name = 'pxln'),
    '179s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Repeating Barrage'),
    (select id from sets where short_name = 'pxln'),
    '156s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deeproot Champion'),
    (select id from sets where short_name = 'pxln'),
    '185s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vanquisher''s Banner'),
    (select id from sets where short_name = 'pxln'),
    '251p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathgorge Scavenger'),
    (select id from sets where short_name = 'pxln'),
    '184s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fell Flagship'),
    (select id from sets where short_name = 'pxln'),
    '238s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sanctum Seeker'),
    (select id from sets where short_name = 'pxln'),
    '120s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Herald of Secret Streams'),
    (select id from sets where short_name = 'pxln'),
    '59p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Revel in Riches'),
    (select id from sets where short_name = 'pxln'),
    '117s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonskull Summit'),
    (select id from sets where short_name = 'pxln'),
    '252s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tishana, Voice of Thunder'),
    (select id from sets where short_name = 'pxln'),
    '230s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Huatli, Warrior Poet'),
    (select id from sets where short_name = 'pxln'),
    '224s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mavren Fein, Dusk Apostle'),
    (select id from sets where short_name = 'pxln'),
    '24p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kinjalli''s Sunwing'),
    (select id from sets where short_name = 'pxln'),
    '19p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vona, Butcher of Magan'),
    (select id from sets where short_name = 'pxln'),
    '231s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fathom Fleet Captain'),
    (select id from sets where short_name = 'pxln'),
    '106p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ripjaw Raptor'),
    (select id from sets where short_name = 'pxln'),
    '203s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spell Swindle'),
    (select id from sets where short_name = 'pxln'),
    '82s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vraska, Relic Seeker'),
    (select id from sets where short_name = 'pxln'),
    '232p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Drowned Catacomb'),
    (select id from sets where short_name = 'pxln'),
    '253s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunbird''s Invocation'),
    (select id from sets where short_name = 'pxln'),
    '165s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emperor''s Vanguard'),
    (select id from sets where short_name = 'pxln'),
    '189s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conqueror''s Galleon // Conqueror''s Foothold'),
    (select id from sets where short_name = 'pxln'),
    '234s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deeproot Champion'),
    (select id from sets where short_name = 'pxln'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shapers'' Sanctuary'),
    (select id from sets where short_name = 'pxln'),
    '206s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'River''s Rebuke'),
    (select id from sets where short_name = 'pxln'),
    '71s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shadowed Caravel'),
    (select id from sets where short_name = 'pxln'),
    '246s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunpetal Grove'),
    (select id from sets where short_name = 'pxln'),
    '257s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glacial Fortress'),
    (select id from sets where short_name = 'pxln'),
    '255s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vraska''s Contempt'),
    (select id from sets where short_name = 'pxln'),
    '129s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sanguine Sacrament'),
    (select id from sets where short_name = 'pxln'),
    '33s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Walk the Plank'),
    (select id from sets where short_name = 'pxln'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Regisaur Alpha'),
    (select id from sets where short_name = 'pxln'),
    '227p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gishath, Sun''s Avatar'),
    (select id from sets where short_name = 'pxln'),
    '222s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Search for Azcanta // Azcanta, the Sunken Ruin'),
    (select id from sets where short_name = 'pxln'),
    '74s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kopala, Warden of Waves'),
    (select id from sets where short_name = 'pxln'),
    '61p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonskull Summit'),
    (select id from sets where short_name = 'pxln'),
    '252p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jace, Cunning Castaway'),
    (select id from sets where short_name = 'pxln'),
    '60s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Settle the Wreckage'),
    (select id from sets where short_name = 'pxln'),
    '34s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Waker of the Wilds'),
    (select id from sets where short_name = 'pxln'),
    '215s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathgorge Scavenger'),
    (select id from sets where short_name = 'pxln'),
    '184p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kinjalli''s Sunwing'),
    (select id from sets where short_name = 'pxln'),
    '19s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tilonalli''s Skinshifter'),
    (select id from sets where short_name = 'pxln'),
    '170s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arguel''s Blood Fast // Temple of Aclazotz'),
    (select id from sets where short_name = 'pxln'),
    '90s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashes of the Abhorrent'),
    (select id from sets where short_name = 'pxln'),
    '2s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rootbound Crag'),
    (select id from sets where short_name = 'pxln'),
    '256p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Captain Lannery Storm'),
    (select id from sets where short_name = 'pxln'),
    '136p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sanctum Seeker'),
    (select id from sets where short_name = 'pxln'),
    '120p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tocatli Honor Guard'),
    (select id from sets where short_name = 'pxln'),
    '42s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kopala, Warden of Waves'),
    (select id from sets where short_name = 'pxln'),
    '61s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verdant Sun''s Avatar'),
    (select id from sets where short_name = 'pxln'),
    '213s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Admiral Beckett Brass'),
    (select id from sets where short_name = 'pxln'),
    '217s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Star of Extinction'),
    (select id from sets where short_name = 'pxln'),
    '161p',
    'mythic'
) 
 on conflict do nothing;
