    insert into mtgcard(name) values ('Thaumatic Compass // Spires of Orazca') on conflict do nothing;
    insert into mtgcard(name) values ('Axis of Mortality') on conflict do nothing;
    insert into mtgcard(name) values ('Vraska, Relic Seeker') on conflict do nothing;
    insert into mtgcard(name) values ('Treasure Map // Treasure Cove') on conflict do nothing;
    insert into mtgcard(name) values ('Sunbird''s Invocation') on conflict do nothing;
    insert into mtgcard(name) values ('Goring Ceratops') on conflict do nothing;
    insert into mtgcard(name) values ('Regisaur Alpha') on conflict do nothing;
    insert into mtgcard(name) values ('Entrancing Melody') on conflict do nothing;
    insert into mtgcard(name) values ('Vance''s Blasting Cannons // Spitfire Bastion') on conflict do nothing;
    insert into mtgcard(name) values ('Glacial Fortress') on conflict do nothing;
    insert into mtgcard(name) values ('Carnage Tyrant') on conflict do nothing;
    insert into mtgcard(name) values ('Sword-Point Diplomacy') on conflict do nothing;
    insert into mtgcard(name) values ('Sorcerous Spyglass') on conflict do nothing;
    insert into mtgcard(name) values ('Rootbound Crag') on conflict do nothing;
    insert into mtgcard(name) values ('Vanquisher''s Banner') on conflict do nothing;
    insert into mtgcard(name) values ('Wakening Sun''s Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Verdant Sun''s Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Dire Fleet Ravager') on conflict do nothing;
    insert into mtgcard(name) values ('Bishop of Rebirth') on conflict do nothing;
    insert into mtgcard(name) values ('Priest of the Wakening Sun') on conflict do nothing;
    insert into mtgcard(name) values ('Shapers'' Sanctuary') on conflict do nothing;
    insert into mtgcard(name) values ('Daring Saboteur') on conflict do nothing;
    insert into mtgcard(name) values ('Star of Extinction') on conflict do nothing;
    insert into mtgcard(name) values ('Dowsing Dagger // Lost Vale') on conflict do nothing;
    insert into mtgcard(name) values ('Deadeye Tracker') on conflict do nothing;
    insert into mtgcard(name) values ('Tocatli Honor Guard') on conflict do nothing;
    insert into mtgcard(name) values ('Sunpetal Grove') on conflict do nothing;
    insert into mtgcard(name) values ('Deeproot Champion') on conflict do nothing;
    insert into mtgcard(name) values ('Bloodcrazed Paladin') on conflict do nothing;
    insert into mtgcard(name) values ('Hostage Taker') on conflict do nothing;
    insert into mtgcard(name) values ('Ripjaw Raptor') on conflict do nothing;
    insert into mtgcard(name) values ('Herald of Secret Streams') on conflict do nothing;
    insert into mtgcard(name) values ('Legion''s Landing // Adanto, the First Fort') on conflict do nothing;
    insert into mtgcard(name) values ('Boneyard Parley') on conflict do nothing;
    insert into mtgcard(name) values ('Ruin Raider') on conflict do nothing;
    insert into mtgcard(name) values ('Revel in Riches') on conflict do nothing;
    insert into mtgcard(name) values ('Ashes of the Abhorrent') on conflict do nothing;
    insert into mtgcard(name) values ('Overflowing Insight') on conflict do nothing;
    insert into mtgcard(name) values ('Primal Amulet // Primal Wellspring') on conflict do nothing;
    insert into mtgcard(name) values ('Burning Sun''s Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Vraska''s Contempt') on conflict do nothing;
    insert into mtgcard(name) values ('Angrath''s Marauders') on conflict do nothing;
    insert into mtgcard(name) values ('Mavren Fein, Dusk Apostle') on conflict do nothing;
    insert into mtgcard(name) values ('Old-Growth Dryads') on conflict do nothing;
    insert into mtgcard(name) values ('Fathom Fleet Captain') on conflict do nothing;
    insert into mtgcard(name) values ('Drowned Catacomb') on conflict do nothing;
    insert into mtgcard(name) values ('Arcane Adaptation') on conflict do nothing;
    insert into mtgcard(name) values ('Captain Lannery Storm') on conflict do nothing;
    insert into mtgcard(name) values ('Dreamcaller Siren') on conflict do nothing;
    insert into mtgcard(name) values ('Spell Swindle') on conflict do nothing;
    insert into mtgcard(name) values ('Growing Rites of Itlimoc // Itlimoc, Cradle of the Sun') on conflict do nothing;
    insert into mtgcard(name) values ('Unclaimed Territory') on conflict do nothing;
    insert into mtgcard(name) values ('Rampaging Ferocidon') on conflict do nothing;
    insert into mtgcard(name) values ('River''s Rebuke') on conflict do nothing;
    insert into mtgcard(name) values ('Captivating Crew') on conflict do nothing;
    insert into mtgcard(name) values ('Fleet Swallower') on conflict do nothing;
    insert into mtgcard(name) values ('Settle the Wreckage') on conflict do nothing;
    insert into mtgcard(name) values ('Rowdy Crew') on conflict do nothing;
    insert into mtgcard(name) values ('Repeating Barrage') on conflict do nothing;
    insert into mtgcard(name) values ('Deathgorge Scavenger') on conflict do nothing;
    insert into mtgcard(name) values ('Fell Flagship') on conflict do nothing;
    insert into mtgcard(name) values ('Sanctum Seeker') on conflict do nothing;
    insert into mtgcard(name) values ('Dragonskull Summit') on conflict do nothing;
    insert into mtgcard(name) values ('Tishana, Voice of Thunder') on conflict do nothing;
    insert into mtgcard(name) values ('Huatli, Warrior Poet') on conflict do nothing;
    insert into mtgcard(name) values ('Kinjalli''s Sunwing') on conflict do nothing;
    insert into mtgcard(name) values ('Vona, Butcher of Magan') on conflict do nothing;
    insert into mtgcard(name) values ('Emperor''s Vanguard') on conflict do nothing;
    insert into mtgcard(name) values ('Conqueror''s Galleon // Conqueror''s Foothold') on conflict do nothing;
    insert into mtgcard(name) values ('Shadowed Caravel') on conflict do nothing;
    insert into mtgcard(name) values ('Sanguine Sacrament') on conflict do nothing;
    insert into mtgcard(name) values ('Walk the Plank') on conflict do nothing;
    insert into mtgcard(name) values ('Gishath, Sun''s Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Search for Azcanta // Azcanta, the Sunken Ruin') on conflict do nothing;
    insert into mtgcard(name) values ('Kopala, Warden of Waves') on conflict do nothing;
    insert into mtgcard(name) values ('Jace, Cunning Castaway') on conflict do nothing;
    insert into mtgcard(name) values ('Waker of the Wilds') on conflict do nothing;
    insert into mtgcard(name) values ('Tilonalli''s Skinshifter') on conflict do nothing;
    insert into mtgcard(name) values ('Arguel''s Blood Fast // Temple of Aclazotz') on conflict do nothing;
    insert into mtgcard(name) values ('Admiral Beckett Brass') on conflict do nothing;
