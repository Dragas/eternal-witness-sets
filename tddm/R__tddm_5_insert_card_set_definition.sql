insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Assassin'),
    (select id from sets where short_name = 'tddm'),
    '1',
    'common'
) 
 on conflict do nothing;
