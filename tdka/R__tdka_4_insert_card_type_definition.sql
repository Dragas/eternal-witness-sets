insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Vampire'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vampire'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vampire'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sorin, Lord of Innistrad Emblem'),
        (select types.id from types where name = 'Emblem')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sorin, Lord of Innistrad Emblem'),
        (select types.id from types where name = 'Sorin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dark Ascension Checklist'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Human'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Human'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Human'),
        (select types.id from types where name = 'Human')
    ) 
 on conflict do nothing;
