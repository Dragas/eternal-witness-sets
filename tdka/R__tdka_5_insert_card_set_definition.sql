insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Vampire'),
    (select id from sets where short_name = 'tdka'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sorin, Lord of Innistrad Emblem'),
    (select id from sets where short_name = 'tdka'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Ascension Checklist'),
    (select id from sets where short_name = 'tdka'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Human'),
    (select id from sets where short_name = 'tdka'),
    '1',
    'common'
) 
 on conflict do nothing;
