insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Sylvan Tutor'),
    (select id from sets where short_name = 'j20'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sterling Grove'),
    (select id from sets where short_name = 'j20'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infernal Tutor'),
    (select id from sets where short_name = 'j20'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spellseeker'),
    (select id from sets where short_name = 'j20'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eye of Ugin'),
    (select id from sets where short_name = 'j20'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Birthing Pod'),
    (select id from sets where short_name = 'j20'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arena Rector'),
    (select id from sets where short_name = 'j20'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enlightened Tutor'),
    (select id from sets where short_name = 'j20'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gamble'),
    (select id from sets where short_name = 'j20'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonic Tutor'),
    (select id from sets where short_name = 'j20'),
    '4',
    'rare'
) 
 on conflict do nothing;
