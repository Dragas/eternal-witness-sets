insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Eldrazi Spawn'),
    (select id from sets where short_name = 't2xm'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ooze'),
    (select id from sets where short_name = 't2xm'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 't2xm'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon'),
    (select id from sets where short_name = 't2xm'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr'),
    (select id from sets where short_name = 't2xm'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Copy'),
    (select id from sets where short_name = 't2xm'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter'),
    (select id from sets where short_name = 't2xm'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure'),
    (select id from sets where short_name = 't2xm'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shapeshifter'),
    (select id from sets where short_name = 't2xm'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golem'),
    (select id from sets where short_name = 't2xm'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 't2xm'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 't2xm'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elephant'),
    (select id from sets where short_name = 't2xm'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Germ'),
    (select id from sets where short_name = 't2xm'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurm'),
    (select id from sets where short_name = 't2xm'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squirrel'),
    (select id from sets where short_name = 't2xm'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Servo'),
    (select id from sets where short_name = 't2xm'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plant'),
    (select id from sets where short_name = 't2xm'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurm'),
    (select id from sets where short_name = 't2xm'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tuktuk the Returned'),
    (select id from sets where short_name = 't2xm'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elf Warrior'),
    (select id from sets where short_name = 't2xm'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 't2xm'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clue'),
    (select id from sets where short_name = 't2xm'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 't2xm'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr'),
    (select id from sets where short_name = 't2xm'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Human Soldier'),
    (select id from sets where short_name = 't2xm'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ape'),
    (select id from sets where short_name = 't2xm'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter'),
    (select id from sets where short_name = 't2xm'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat'),
    (select id from sets where short_name = 't2xm'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marit Lage'),
    (select id from sets where short_name = 't2xm'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 't2xm'),
    '3',
    'common'
) 
 on conflict do nothing;
