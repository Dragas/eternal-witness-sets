    insert into mtgcard(name) values ('Man-o''-War') on conflict do nothing;
    insert into mtgcard(name) values ('Dauthi Slayer') on conflict do nothing;
    insert into mtgcard(name) values ('Mana Leak') on conflict do nothing;
    insert into mtgcard(name) values ('Island') on conflict do nothing;
    insert into mtgcard(name) values ('Arc Lightning') on conflict do nothing;
