insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Man-o''-War'),
    (select id from sets where short_name = 'pal02'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dauthi Slayer'),
    (select id from sets where short_name = 'pal02'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Leak'),
    (select id from sets where short_name = 'pal02'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pal02'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arc Lightning'),
    (select id from sets where short_name = 'pal02'),
    '3',
    'rare'
) 
 on conflict do nothing;
