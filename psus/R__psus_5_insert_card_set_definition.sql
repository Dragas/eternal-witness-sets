insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Thran Quarry'),
    (select id from sets where short_name = 'psus'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Royal Assassin'),
    (select id from sets where short_name = 'psus'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Champion'),
    (select id from sets where short_name = 'psus'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'psus'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glorious Anthem'),
    (select id from sets where short_name = 'psus'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lord of Atlantis'),
    (select id from sets where short_name = 'psus'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soltari Priest'),
    (select id from sets where short_name = 'psus'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slith Firewalker'),
    (select id from sets where short_name = 'psus'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'psus'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Hammer'),
    (select id from sets where short_name = 'psus'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Lyrist'),
    (select id from sets where short_name = 'psus'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Two-Headed Dragon'),
    (select id from sets where short_name = 'psus'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whirling Dervish'),
    (select id from sets where short_name = 'psus'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mad Auntie'),
    (select id from sets where short_name = 'psus'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crusade'),
    (select id from sets where short_name = 'psus'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shard Phoenix'),
    (select id from sets where short_name = 'psus'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Elder'),
    (select id from sets where short_name = 'psus'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Avatar'),
    (select id from sets where short_name = 'psus'),
    '2',
    'rare'
) 
 on conflict do nothing;
