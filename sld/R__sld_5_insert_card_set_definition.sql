insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ajani, the Greathearted'),
    (select id from sets where short_name = 'sld'),
    '520',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'sld'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dig Through Time'),
    (select id from sets where short_name = 'sld'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana, Dreadhorde General'),
    (select id from sets where short_name = 'sld'),
    '510',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ashiok, Dream Render'),
    (select id from sets where short_name = 'sld'),
    '528',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Explore'),
    (select id from sets where short_name = 'sld'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rick, Steadfast Leader'),
    (select id from sets where short_name = 'sld'),
    '143',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Opt'),
    (select id from sets where short_name = 'sld'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Daryl, Hunter of Walkers'),
    (select id from sets where short_name = 'sld'),
    '144',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Iroas, God of Victory'),
    (select id from sets where short_name = 'sld'),
    '70',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Faerie Rogue'),
    (select id from sets where short_name = 'sld'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pharika, God of Affliction'),
    (select id from sets where short_name = 'sld'),
    '82',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chandra, Fire Artisan'),
    (select id from sets where short_name = 'sld'),
    '512',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angrath, Captain of Chaos'),
    (select id from sets where short_name = 'sld'),
    '527',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Mimeoplasm'),
    (select id from sets where short_name = 'sld'),
    '136',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ink-Eyes, Servant of Oni'),
    (select id from sets where short_name = 'sld'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Greaves'),
    (select id from sets where short_name = 'sld'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faerie Rogue'),
    (select id from sets where short_name = 'sld'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reaper King'),
    (select id from sets where short_name = 'sld'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thalia, Guardian of Thraben'),
    (select id from sets where short_name = 'sld'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Acidic Slime'),
    (select id from sets where short_name = 'sld'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'sld'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cat'),
    (select id from sets where short_name = 'sld'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karn, the Great Creator'),
    (select id from sets where short_name = 'sld'),
    '501',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zulaport Cutthroat'),
    (select id from sets where short_name = 'sld'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mogis, God of Slaughter'),
    (select id from sets where short_name = 'sld'),
    '78',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gideon Blackblade'),
    (select id from sets where short_name = 'sld'),
    '503',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Treasure'),
    (select id from sets where short_name = 'sld'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karametra, God of Harvests'),
    (select id from sets where short_name = 'sld'),
    '69',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Plains'),
    (select id from sets where short_name = 'sld'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Narset, Enlightened Master'),
    (select id from sets where short_name = 'sld'),
    '53',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Athreos, God of Passage'),
    (select id from sets where short_name = 'sld'),
    '76',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cat'),
    (select id from sets where short_name = 'sld'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Negan, the Cold-Blooded'),
    (select id from sets where short_name = 'sld'),
    '147',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ajani Steadfast'),
    (select id from sets where short_name = 'sld'),
    '87',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Inkmoth Nexus'),
    (select id from sets where short_name = 'sld'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis, the Hate-Twisted'),
    (select id from sets where short_name = 'sld'),
    '511',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warren Instigator'),
    (select id from sets where short_name = 'sld'),
    '157',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Marrow-Gnawer'),
    (select id from sets where short_name = 'sld'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Island'),
    (select id from sets where short_name = 'sld'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaya, Bane of the Dead'),
    (select id from sets where short_name = 'sld'),
    '531',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serum Visions'),
    (select id from sets where short_name = 'sld'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swan Song'),
    (select id from sets where short_name = 'sld'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darksteel Colossus'),
    (select id from sets where short_name = 'sld'),
    '57',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'sld'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin King'),
    (select id from sets where short_name = 'sld'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Admonition Angel'),
    (select id from sets where short_name = 'sld'),
    '154',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Domri, Anarch of Bolas'),
    (select id from sets where short_name = 'sld'),
    '521',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Walker'),
    (select id from sets where short_name = 'sld'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nylea, God of the Hunt'),
    (select id from sets where short_name = 'sld'),
    '80',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Serum Visions'),
    (select id from sets where short_name = 'sld'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fatal Push'),
    (select id from sets where short_name = 'sld'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serum Visions'),
    (select id from sets where short_name = 'sld'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kasmina, Enigmatic Mentor'),
    (select id from sets where short_name = 'sld'),
    '507',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eternal Witness'),
    (select id from sets where short_name = 'sld'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necrotic Ooze'),
    (select id from sets where short_name = 'sld'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Snowman'),
    (select id from sets where short_name = 'sld'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avenger of Zendikar'),
    (select id from sets where short_name = 'sld'),
    '158',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Michonne, Ruthless Survivor'),
    (select id from sets where short_name = 'sld'),
    '146',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Swamp'),
    (select id from sets where short_name = 'sld'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'sld'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Walking Ballista'),
    (select id from sets where short_name = 'sld'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roil Elemental'),
    (select id from sets where short_name = 'sld'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Narset, Parter of Veils'),
    (select id from sets where short_name = 'sld'),
    '508',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, Dragon-God'),
    (select id from sets where short_name = 'sld'),
    '522',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Mountain'),
    (select id from sets where short_name = 'sld'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcbound Ravager'),
    (select id from sets where short_name = 'sld'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'sld'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Wanderer'),
    (select id from sets where short_name = 'sld'),
    '505',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serum Visions'),
    (select id from sets where short_name = 'sld'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi, Time Raveler'),
    (select id from sets where short_name = 'sld'),
    '526',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glenn, the Voice of Calm'),
    (select id from sets where short_name = 'sld'),
    '145',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sliver Overlord'),
    (select id from sets where short_name = 'sld'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'sld'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thalia, Guardian of Thraben'),
    (select id from sets where short_name = 'sld'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phenax, God of Deception'),
    (select id from sets where short_name = 'sld'),
    '75',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goblin Bushwhacker'),
    (select id from sets where short_name = 'sld'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Huatli, the Sun''s Heart'),
    (select id from sets where short_name = 'sld'),
    '530',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Davriel, Rogue Shadowmage'),
    (select id from sets where short_name = 'sld'),
    '509',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Grudge'),
    (select id from sets where short_name = 'sld'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faerie Rogue'),
    (select id from sets where short_name = 'sld'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Storm Crow'),
    (select id from sets where short_name = 'sld'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Baleful Strix'),
    (select id from sets where short_name = 'sld'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tibalt, Rakish Instigator'),
    (select id from sets where short_name = 'sld'),
    '515',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jace, Wielder of Mysteries'),
    (select id from sets where short_name = 'sld'),
    '506',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bitterblossom'),
    (select id from sets where short_name = 'sld'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ral, Storm Conduit'),
    (select id from sets where short_name = 'sld'),
    '523',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thassa, God of the Sea'),
    (select id from sets where short_name = 'sld'),
    '71',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ugin, the Ineffable'),
    (select id from sets where short_name = 'sld'),
    '502',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodghast'),
    (select id from sets where short_name = 'sld'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Life from the Loam'),
    (select id from sets where short_name = 'sld'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Artist'),
    (select id from sets where short_name = 'sld'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dovescape'),
    (select id from sets where short_name = 'sld'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dovin, Hand of Control'),
    (select id from sets where short_name = 'sld'),
    '529',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rest in Peace'),
    (select id from sets where short_name = 'sld'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meren of Clan Nel Toth'),
    (select id from sets where short_name = 'sld'),
    '52',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Forest'),
    (select id from sets where short_name = 'sld'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keranos, God of Storms'),
    (select id from sets where short_name = 'sld'),
    '79',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vivien, Champion of the Wilds'),
    (select id from sets where short_name = 'sld'),
    '519',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Samut, Tyrant Smasher'),
    (select id from sets where short_name = 'sld'),
    '535',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Piledriver'),
    (select id from sets where short_name = 'sld'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kruphix, God of Horizons'),
    (select id from sets where short_name = 'sld'),
    '73',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jiang Yanggu, Wildcrafter'),
    (select id from sets where short_name = 'sld'),
    '517',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teyo, the Shieldmage'),
    (select id from sets where short_name = 'sld'),
    '504',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Walker'),
    (select id from sets where short_name = 'sld'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rat Colony'),
    (select id from sets where short_name = 'sld'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spell Pierce'),
    (select id from sets where short_name = 'sld'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oona, Queen of the Fae'),
    (select id from sets where short_name = 'sld'),
    '54',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Saskia the Unyielding'),
    (select id from sets where short_name = 'sld'),
    '55',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Erebos, God of the Dead'),
    (select id from sets where short_name = 'sld'),
    '74',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goblin Sharpshooter'),
    (select id from sets where short_name = 'sld'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scavenging Ooze'),
    (select id from sets where short_name = 'sld'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thalia, Guardian of Thraben'),
    (select id from sets where short_name = 'sld'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Walker'),
    (select id from sets where short_name = 'sld'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'sld'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arlinn, Voice of the Pack'),
    (select id from sets where short_name = 'sld'),
    '516',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leonin Warleader'),
    (select id from sets where short_name = 'sld'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'sld'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sorin, Vengeful Bloodlord'),
    (select id from sets where short_name = 'sld'),
    '524',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kiora, Behemoth Beckoner'),
    (select id from sets where short_name = 'sld'),
    '532',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nahiri, Storm of Stone'),
    (select id from sets where short_name = 'sld'),
    '533',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tamiyo, Field Researcher'),
    (select id from sets where short_name = 'sld'),
    '89',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Saheeli, Sublime Artificer'),
    (select id from sets where short_name = 'sld'),
    '534',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nissa, Who Shakes the World'),
    (select id from sets where short_name = 'sld'),
    '518',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Regal Caracal'),
    (select id from sets where short_name = 'sld'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vraska, Golgari Queen'),
    (select id from sets where short_name = 'sld'),
    '90',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Purphoros, God of the Forge'),
    (select id from sets where short_name = 'sld'),
    '77',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Faerie Rogue'),
    (select id from sets where short_name = 'sld'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sarkhan the Masterless'),
    (select id from sets where short_name = 'sld'),
    '514',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thalia, Guardian of Thraben'),
    (select id from sets where short_name = 'sld'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lucille'),
    (select id from sets where short_name = 'sld'),
    '581',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goblin Lackey'),
    (select id from sets where short_name = 'sld'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Qasali Slingers'),
    (select id from sets where short_name = 'sld'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jaya, Venerated Firemage'),
    (select id from sets where short_name = 'sld'),
    '513',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Xenagos, God of Revels'),
    (select id from sets where short_name = 'sld'),
    '81',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pithing Needle'),
    (select id from sets where short_name = 'sld'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ephara, God of the Polis'),
    (select id from sets where short_name = 'sld'),
    '72',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Voidslime'),
    (select id from sets where short_name = 'sld'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Domri Rade'),
    (select id from sets where short_name = 'sld'),
    '88',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mirri, Weatherlight Duelist'),
    (select id from sets where short_name = 'sld'),
    '26',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'The Ur-Dragon'),
    (select id from sets where short_name = 'sld'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gilded Goose'),
    (select id from sets where short_name = 'sld'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anger of the Gods'),
    (select id from sets where short_name = 'sld'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'sld'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arahbo, Roar of the World'),
    (select id from sets where short_name = 'sld'),
    '25',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mudhole'),
    (select id from sets where short_name = 'sld'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vraska, Swarm''s Eminence'),
    (select id from sets where short_name = 'sld'),
    '536',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pack Rat'),
    (select id from sets where short_name = 'sld'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tibalt, the Fiend-Blooded'),
    (select id from sets where short_name = 'sld'),
    '537',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Captain Sisay'),
    (select id from sets where short_name = 'sld'),
    '51',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Heliod, God of the Sun'),
    (select id from sets where short_name = 'sld'),
    '68',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tamiyo, Collector of Tales'),
    (select id from sets where short_name = 'sld'),
    '525',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'sld'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Thug'),
    (select id from sets where short_name = 'sld'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = 'sld'),
    '92',
    'rare'
) 
 on conflict do nothing;
