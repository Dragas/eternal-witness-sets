insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Spark Spray'),
    (select id from sets where short_name = 'vma'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grizzly Fate'),
    (select id from sets where short_name = 'vma'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Predatory Nightstalker'),
    (select id from sets where short_name = 'vma'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rorix Bladewing'),
    (select id from sets where short_name = 'vma'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silvos, Rogue Elemental'),
    (select id from sets where short_name = 'vma'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brago, King Eternal'),
    (select id from sets where short_name = 'vma'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brindle Shoat'),
    (select id from sets where short_name = 'vma'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blazing Specter'),
    (select id from sets where short_name = 'vma'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karn, Silver Golem'),
    (select id from sets where short_name = 'vma'),
    '270',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Afterlife'),
    (select id from sets where short_name = 'vma'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chimeric Idol'),
    (select id from sets where short_name = 'vma'),
    '264',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Choking Tethers'),
    (select id from sets where short_name = 'vma'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistmoon Griffin'),
    (select id from sets where short_name = 'vma'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fledgling Djinn'),
    (select id from sets where short_name = 'vma'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Aberration'),
    (select id from sets where short_name = 'vma'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Claws of Wirewood'),
    (select id from sets where short_name = 'vma'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Library'),
    (select id from sets where short_name = 'vma'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Taiga'),
    (select id from sets where short_name = 'vma'),
    '317',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pine Barrens'),
    (select id from sets where short_name = 'vma'),
    '307',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Death'),
    (select id from sets where short_name = 'vma'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rescind'),
    (select id from sets where short_name = 'vma'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Future Sight'),
    (select id from sets where short_name = 'vma'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Academy Elite'),
    (select id from sets where short_name = 'vma'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Man-o''-War'),
    (select id from sets where short_name = 'vma'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'vma'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Lumberjack'),
    (select id from sets where short_name = 'vma'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simian Grunts'),
    (select id from sets where short_name = 'vma'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling Burst'),
    (select id from sets where short_name = 'vma'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Genesis'),
    (select id from sets where short_name = 'vma'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benevolent Bodyguard'),
    (select id from sets where short_name = 'vma'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'vma'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coercive Portal'),
    (select id from sets where short_name = 'vma'),
    '266',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Control Magic'),
    (select id from sets where short_name = 'vma'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lake of the Dead'),
    (select id from sets where short_name = 'vma'),
    '302',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Expunge'),
    (select id from sets where short_name = 'vma'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'vma'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Decree of Justice'),
    (select id from sets where short_name = 'vma'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreampod Druid'),
    (select id from sets where short_name = 'vma'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantom Nomad'),
    (select id from sets where short_name = 'vma'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dauthi Mercenary'),
    (select id from sets where short_name = 'vma'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'High Tide'),
    (select id from sets where short_name = 'vma'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spinal Graft'),
    (select id from sets where short_name = 'vma'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tranquil Thicket'),
    (select id from sets where short_name = 'vma'),
    '320',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancestral Recall'),
    (select id from sets where short_name = 'vma'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mystic Zealot'),
    (select id from sets where short_name = 'vma'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin General'),
    (select id from sets where short_name = 'vma'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Realm Seekers'),
    (select id from sets where short_name = 'vma'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Renewed Faith'),
    (select id from sets where short_name = 'vma'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wheel of Fortune'),
    (select id from sets where short_name = 'vma'),
    '192',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thopter Squadron'),
    (select id from sets where short_name = 'vma'),
    '286',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Recurring Nightmare'),
    (select id from sets where short_name = 'vma'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chain Lightning'),
    (select id from sets where short_name = 'vma'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scrivener'),
    (select id from sets where short_name = 'vma'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Underground Sea'),
    (select id from sets where short_name = 'vma'),
    '323',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soltari Trooper'),
    (select id from sets where short_name = 'vma'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu'),
    (select id from sets where short_name = 'vma'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Provoke'),
    (select id from sets where short_name = 'vma'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gerrard''s Battle Cry'),
    (select id from sets where short_name = 'vma'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prophetic Bolt'),
    (select id from sets where short_name = 'vma'),
    '257',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nostalgic Dreams'),
    (select id from sets where short_name = 'vma'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kongming, "Sleeping Dragon"'),
    (select id from sets where short_name = 'vma'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Visara the Dreadful'),
    (select id from sets where short_name = 'vma'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arrogant Wurm'),
    (select id from sets where short_name = 'vma'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crater Hellion'),
    (select id from sets where short_name = 'vma'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barren Moor'),
    (select id from sets where short_name = 'vma'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'Benalish Trapper'),
    (select id from sets where short_name = 'vma'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea Drake'),
    (select id from sets where short_name = 'vma'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Island'),
    (select id from sets where short_name = 'vma'),
    '324',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skywing Aven'),
    (select id from sets where short_name = 'vma'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Lackey'),
    (select id from sets where short_name = 'vma'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Goon'),
    (select id from sets where short_name = 'vma'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krosan Vorine'),
    (select id from sets where short_name = 'vma'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yawgmoth''s Will'),
    (select id from sets where short_name = 'vma'),
    '148',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lightning Rift'),
    (select id from sets where short_name = 'vma'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gigapede'),
    (select id from sets where short_name = 'vma'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hymn to Tourach'),
    (select id from sets where short_name = 'vma'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urborg Uprising'),
    (select id from sets where short_name = 'vma'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Dragon'),
    (select id from sets where short_name = 'vma'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Laquatus''s Champion'),
    (select id from sets where short_name = 'vma'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thawing Glaciers'),
    (select id from sets where short_name = 'vma'),
    '318',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rocky Tar Pit'),
    (select id from sets where short_name = 'vma'),
    '309',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brain Freeze'),
    (select id from sets where short_name = 'vma'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Roar of the Wurm'),
    (select id from sets where short_name = 'vma'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Basking Rootwalla'),
    (select id from sets where short_name = 'vma'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Masticore'),
    (select id from sets where short_name = 'vma'),
    '275',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain Valley'),
    (select id from sets where short_name = 'vma'),
    '306',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lurking Evil'),
    (select id from sets where short_name = 'vma'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frantic Search'),
    (select id from sets where short_name = 'vma'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Salt Flats'),
    (select id from sets where short_name = 'vma'),
    '310',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flowstone Sculpture'),
    (select id from sets where short_name = 'vma'),
    '268',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fireblast'),
    (select id from sets where short_name = 'vma'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fyndhorn Elves'),
    (select id from sets where short_name = 'vma'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mox Ruby'),
    (select id from sets where short_name = 'vma'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tropical Island'),
    (select id from sets where short_name = 'vma'),
    '321',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Putrid Imp'),
    (select id from sets where short_name = 'vma'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temporal Fissure'),
    (select id from sets where short_name = 'vma'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reanimate'),
    (select id from sets where short_name = 'vma'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Diffusion'),
    (select id from sets where short_name = 'vma'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Prism'),
    (select id from sets where short_name = 'vma'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basandra, Battle Seraph'),
    (select id from sets where short_name = 'vma'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Radiant, Archangel'),
    (select id from sets where short_name = 'vma'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Forest'),
    (select id from sets where short_name = 'vma'),
    '315',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Settler'),
    (select id from sets where short_name = 'vma'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Killer Whale'),
    (select id from sets where short_name = 'vma'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Mongrel'),
    (select id from sets where short_name = 'vma'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Force of Will'),
    (select id from sets where short_name = 'vma'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aether Mutation'),
    (select id from sets where short_name = 'vma'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hulking Goblin'),
    (select id from sets where short_name = 'vma'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Radiant''s Judgment'),
    (select id from sets where short_name = 'vma'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carnophage'),
    (select id from sets where short_name = 'vma'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Falter'),
    (select id from sets where short_name = 'vma'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blastoderm'),
    (select id from sets where short_name = 'vma'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scourge of the Throne'),
    (select id from sets where short_name = 'vma'),
    '184',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = 'vma'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tyrant''s Choice'),
    (select id from sets where short_name = 'vma'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tribute to the Wild'),
    (select id from sets where short_name = 'vma'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Survival of the Fittest'),
    (select id from sets where short_name = 'vma'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mox Jet'),
    (select id from sets where short_name = 'vma'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = 'vma'),
    '277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mesmeric Fiend'),
    (select id from sets where short_name = 'vma'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Memory Jar'),
    (select id from sets where short_name = 'vma'),
    '276',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Krovikan Sorcerer'),
    (select id from sets where short_name = 'vma'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obsessive Search'),
    (select id from sets where short_name = 'vma'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tangle'),
    (select id from sets where short_name = 'vma'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ophidian'),
    (select id from sets where short_name = 'vma'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Empyrial Armor'),
    (select id from sets where short_name = 'vma'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cruel Bargain'),
    (select id from sets where short_name = 'vma'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scabland'),
    (select id from sets where short_name = 'vma'),
    '312',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Secluded Steppe'),
    (select id from sets where short_name = 'vma'),
    '314',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Defiler'),
    (select id from sets where short_name = 'vma'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tradewind Rider'),
    (select id from sets where short_name = 'vma'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brilliant Halo'),
    (select id from sets where short_name = 'vma'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rofellos, Llanowar Emissary'),
    (select id from sets where short_name = 'vma'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skullclamp'),
    (select id from sets where short_name = 'vma'),
    '281',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Necropotence'),
    (select id from sets where short_name = 'vma'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tendrils of Agony'),
    (select id from sets where short_name = 'vma'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'vma'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time Walk'),
    (select id from sets where short_name = 'vma'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Battle Screech'),
    (select id from sets where short_name = 'vma'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eureka'),
    (select id from sets where short_name = 'vma'),
    '208',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Spiritmonger'),
    (select id from sets where short_name = 'vma'),
    '262',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Badlands'),
    (select id from sets where short_name = 'vma'),
    '291',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Trenches'),
    (select id from sets where short_name = 'vma'),
    '253',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Null Rod'),
    (select id from sets where short_name = 'vma'),
    '278',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dack Fayden'),
    (select id from sets where short_name = 'vma'),
    '247',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lion''s Eye Diamond'),
    (select id from sets where short_name = 'vma'),
    '271',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Edric, Spymaster of Trest'),
    (select id from sets where short_name = 'vma'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circular Logic'),
    (select id from sets where short_name = 'vma'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sarcomancy'),
    (select id from sets where short_name = 'vma'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Upheaval'),
    (select id from sets where short_name = 'vma'),
    '100',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Noble Templar'),
    (select id from sets where short_name = 'vma'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reviving Vapors'),
    (select id from sets where short_name = 'vma'),
    '259',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skirk Prospector'),
    (select id from sets where short_name = 'vma'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sulfuric Vortex'),
    (select id from sets where short_name = 'vma'),
    '190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jareth, Leonine Titan'),
    (select id from sets where short_name = 'vma'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bad River'),
    (select id from sets where short_name = 'vma'),
    '290',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Crypt'),
    (select id from sets where short_name = 'vma'),
    '272',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sudden Strength'),
    (select id from sets where short_name = 'vma'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lonely Sandbar'),
    (select id from sets where short_name = 'vma'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grasslands'),
    (select id from sets where short_name = 'vma'),
    '299',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Solar Blast'),
    (select id from sets where short_name = 'vma'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Repel'),
    (select id from sets where short_name = 'vma'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shelter'),
    (select id from sets where short_name = 'vma'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Predator, Flagship'),
    (select id from sets where short_name = 'vma'),
    '279',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaervek''s Torch'),
    (select id from sets where short_name = 'vma'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'vma'),
    '283',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Marchesa, the Black Rose'),
    (select id from sets where short_name = 'vma'),
    '256',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Turnabout'),
    (select id from sets where short_name = 'vma'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forgotten Cave'),
    (select id from sets where short_name = 'vma'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ephemeron'),
    (select id from sets where short_name = 'vma'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thalakos Drifters'),
    (select id from sets where short_name = 'vma'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Ringleader'),
    (select id from sets where short_name = 'vma'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chartooth Cougar'),
    (select id from sets where short_name = 'vma'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathreap Ritual'),
    (select id from sets where short_name = 'vma'),
    '250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampiric Tutor'),
    (select id from sets where short_name = 'vma'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balance'),
    (select id from sets where short_name = 'vma'),
    '14',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gilded Light'),
    (select id from sets where short_name = 'vma'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keldon Necropolis'),
    (select id from sets where short_name = 'vma'),
    '300',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Starstorm'),
    (select id from sets where short_name = 'vma'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Council''s Judgment'),
    (select id from sets where short_name = 'vma'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Regrowth'),
    (select id from sets where short_name = 'vma'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flowstone Hellion'),
    (select id from sets where short_name = 'vma'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volrath''s Shapeshifter'),
    (select id from sets where short_name = 'vma'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Worldgorger Dragon'),
    (select id from sets where short_name = 'vma'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flood Plain'),
    (select id from sets where short_name = 'vma'),
    '296',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winds of Rath'),
    (select id from sets where short_name = 'vma'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chainer''s Edict'),
    (select id from sets where short_name = 'vma'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baleful Strix'),
    (select id from sets where short_name = 'vma'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skirk Drill Sergeant'),
    (select id from sets where short_name = 'vma'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Sink'),
    (select id from sets where short_name = 'vma'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burning of Xinye'),
    (select id from sets where short_name = 'vma'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Patrol'),
    (select id from sets where short_name = 'vma'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burning Wish'),
    (select id from sets where short_name = 'vma'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Famine'),
    (select id from sets where short_name = 'vma'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scrubland'),
    (select id from sets where short_name = 'vma'),
    '313',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphere of Resistance'),
    (select id from sets where short_name = 'vma'),
    '284',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plea for Power'),
    (select id from sets where short_name = 'vma'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Caldera Lake'),
    (select id from sets where short_name = 'vma'),
    '295',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Morphling'),
    (select id from sets where short_name = 'vma'),
    '81',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Erhnam Djinn'),
    (select id from sets where short_name = 'vma'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Mantis'),
    (select id from sets where short_name = 'vma'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cabal Ritual'),
    (select id from sets where short_name = 'vma'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Baleful Force'),
    (select id from sets where short_name = 'vma'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flusterstorm'),
    (select id from sets where short_name = 'vma'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kezzerdrix'),
    (select id from sets where short_name = 'vma'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Penumbra Wurm'),
    (select id from sets where short_name = 'vma'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nature''s Lore'),
    (select id from sets where short_name = 'vma'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ankh of Mishra'),
    (select id from sets where short_name = 'vma'),
    '263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reign of the Pit'),
    (select id from sets where short_name = 'vma'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gustcloak Harrier'),
    (select id from sets where short_name = 'vma'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Piledriver'),
    (select id from sets where short_name = 'vma'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fallen Askari'),
    (select id from sets where short_name = 'vma'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deep Analysis'),
    (select id from sets where short_name = 'vma'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pillaging Horde'),
    (select id from sets where short_name = 'vma'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Workshop'),
    (select id from sets where short_name = 'vma'),
    '305',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ivory Tower'),
    (select id from sets where short_name = 'vma'),
    '269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armadillo Cloak'),
    (select id from sets where short_name = 'vma'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mox Emerald'),
    (select id from sets where short_name = 'vma'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fact or Fiction'),
    (select id from sets where short_name = 'vma'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savannah'),
    (select id from sets where short_name = 'vma'),
    '311',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skirge Familiar'),
    (select id from sets where short_name = 'vma'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grenzo, Dungeon Warden'),
    (select id from sets where short_name = 'vma'),
    '254',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ring of Gix'),
    (select id from sets where short_name = 'vma'),
    '280',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightscape Familiar'),
    (select id from sets where short_name = 'vma'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eternal Dragon'),
    (select id from sets where short_name = 'vma'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aftershock'),
    (select id from sets where short_name = 'vma'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drakestown Forgotten'),
    (select id from sets where short_name = 'vma'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animate Dead'),
    (select id from sets where short_name = 'vma'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ichorid'),
    (select id from sets where short_name = 'vma'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Astral Slide'),
    (select id from sets where short_name = 'vma'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cloud Djinn'),
    (select id from sets where short_name = 'vma'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demonic Tutor'),
    (select id from sets where short_name = 'vma'),
    '116',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Plateau'),
    (select id from sets where short_name = 'vma'),
    '308',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dack''s Duplicate'),
    (select id from sets where short_name = 'vma'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bayou'),
    (select id from sets where short_name = 'vma'),
    '293',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Black Lotus'),
    (select id from sets where short_name = 'vma'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Akroma''s Blessing'),
    (select id from sets where short_name = 'vma'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aquamoeba'),
    (select id from sets where short_name = 'vma'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beetleback Chief'),
    (select id from sets where short_name = 'vma'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oath of Druids'),
    (select id from sets where short_name = 'vma'),
    '223',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Deranged Hermit'),
    (select id from sets where short_name = 'vma'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armor of Thorns'),
    (select id from sets where short_name = 'vma'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time Vault'),
    (select id from sets where short_name = 'vma'),
    '287',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Magister of Worth'),
    (select id from sets where short_name = 'vma'),
    '255',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karmic Guide'),
    (select id from sets where short_name = 'vma'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sidar Jabari'),
    (select id from sets where short_name = 'vma'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bazaar of Baghdad'),
    (select id from sets where short_name = 'vma'),
    '294',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dark Hatchling'),
    (select id from sets where short_name = 'vma'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reckless Charge'),
    (select id from sets where short_name = 'vma'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind''s Desire'),
    (select id from sets where short_name = 'vma'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exile'),
    (select id from sets where short_name = 'vma'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elephant Guide'),
    (select id from sets where short_name = 'vma'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Drain'),
    (select id from sets where short_name = 'vma'),
    '78',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stoic Champion'),
    (select id from sets where short_name = 'vma'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Berserk'),
    (select id from sets where short_name = 'vma'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teroh''s Faithful'),
    (select id from sets where short_name = 'vma'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Warchief'),
    (select id from sets where short_name = 'vma'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'City in a Bottle'),
    (select id from sets where short_name = 'vma'),
    '265',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hermit Druid'),
    (select id from sets where short_name = 'vma'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smokestack'),
    (select id from sets where short_name = 'vma'),
    '282',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crescendo of War'),
    (select id from sets where short_name = 'vma'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Tomb'),
    (select id from sets where short_name = 'vma'),
    '289',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Channel'),
    (select id from sets where short_name = 'vma'),
    '200',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Muzzio, Visionary Architect'),
    (select id from sets where short_name = 'vma'),
    '82',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Waterfront Bouncer'),
    (select id from sets where short_name = 'vma'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deftblade Elite'),
    (select id from sets where short_name = 'vma'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keeneye Aven'),
    (select id from sets where short_name = 'vma'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Choking Sands'),
    (select id from sets where short_name = 'vma'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kindle'),
    (select id from sets where short_name = 'vma'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Embrace'),
    (select id from sets where short_name = 'vma'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Breath of Life'),
    (select id from sets where short_name = 'vma'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Vault'),
    (select id from sets where short_name = 'vma'),
    '274',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Elder'),
    (select id from sets where short_name = 'vma'),
    '240',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Hollow'),
    (select id from sets where short_name = 'vma'),
    '325',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Matron'),
    (select id from sets where short_name = 'vma'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fastbond'),
    (select id from sets where short_name = 'vma'),
    '209',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Clickslither'),
    (select id from sets where short_name = 'vma'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Outpost'),
    (select id from sets where short_name = 'vma'),
    '301',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crovax the Cursed'),
    (select id from sets where short_name = 'vma'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Norwood Priestess'),
    (select id from sets where short_name = 'vma'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stroke of Genius'),
    (select id from sets where short_name = 'vma'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Symbiotic Wurm'),
    (select id from sets where short_name = 'vma'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jace, the Mind Sculptor'),
    (select id from sets where short_name = 'vma'),
    '74',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cursed Scroll'),
    (select id from sets where short_name = 'vma'),
    '267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Tusker'),
    (select id from sets where short_name = 'vma'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desert Twister'),
    (select id from sets where short_name = 'vma'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chaos Warp'),
    (select id from sets where short_name = 'vma'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Library of Alexandria'),
    (select id from sets where short_name = 'vma'),
    '303',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Seal of Cleansing'),
    (select id from sets where short_name = 'vma'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit Cairn'),
    (select id from sets where short_name = 'vma'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gamble'),
    (select id from sets where short_name = 'vma'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mox Sapphire'),
    (select id from sets where short_name = 'vma'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Giant Strength'),
    (select id from sets where short_name = 'vma'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloud of Faeries'),
    (select id from sets where short_name = 'vma'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mox Pearl'),
    (select id from sets where short_name = 'vma'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Death Grasp'),
    (select id from sets where short_name = 'vma'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yawgmoth''s Bargain'),
    (select id from sets where short_name = 'vma'),
    '147',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Palinchron'),
    (select id from sets where short_name = 'vma'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tundra'),
    (select id from sets where short_name = 'vma'),
    '322',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shivan Wurm'),
    (select id from sets where short_name = 'vma'),
    '261',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nature''s Ruin'),
    (select id from sets where short_name = 'vma'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rites of Initiation'),
    (select id from sets where short_name = 'vma'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pianna, Nomad Captain'),
    (select id from sets where short_name = 'vma'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serendib Efreet'),
    (select id from sets where short_name = 'vma'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Su-Chi'),
    (select id from sets where short_name = 'vma'),
    '285',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death''s-Head Buzzard'),
    (select id from sets where short_name = 'vma'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zhalfirin Crusader'),
    (select id from sets where short_name = 'vma'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strip Mine'),
    (select id from sets where short_name = 'vma'),
    '316',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Triangle of War'),
    (select id from sets where short_name = 'vma'),
    '288',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gush'),
    (select id from sets where short_name = 'vma'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Psychatog'),
    (select id from sets where short_name = 'vma'),
    '258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jungle Wurm'),
    (select id from sets where short_name = 'vma'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soltari Emissary'),
    (select id from sets where short_name = 'vma'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Addle'),
    (select id from sets where short_name = 'vma'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grand Coliseum'),
    (select id from sets where short_name = 'vma'),
    '298',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Owl Familiar'),
    (select id from sets where short_name = 'vma'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit Mirror'),
    (select id from sets where short_name = 'vma'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Timetwister'),
    (select id from sets where short_name = 'vma'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goblin Commando'),
    (select id from sets where short_name = 'vma'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selvala, Explorer Returned'),
    (select id from sets where short_name = 'vma'),
    '260',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fires of Yavimaya'),
    (select id from sets where short_name = 'vma'),
    '252',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tolarian Academy'),
    (select id from sets where short_name = 'vma'),
    '319',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Devout Witness'),
    (select id from sets where short_name = 'vma'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Parallax Wave'),
    (select id from sets where short_name = 'vma'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Paralyze'),
    (select id from sets where short_name = 'vma'),
    '132',
    'common'
) 
 on conflict do nothing;
