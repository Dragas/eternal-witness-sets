insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Duelist''s Heritage'),
    (select id from sets where short_name = 'c16'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Advokist'),
    (select id from sets where short_name = 'c16'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prismatic Geoscope'),
    (select id from sets where short_name = 'c16'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selesnya Sanctuary'),
    (select id from sets where short_name = 'c16'),
    '324',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merciless Eviction'),
    (select id from sets where short_name = 'c16'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aura Mutation'),
    (select id from sets where short_name = 'c16'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whispersilk Cloak'),
    (select id from sets where short_name = 'c16'),
    '280',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rubblehulk'),
    (select id from sets where short_name = 'c16'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Transguild Promenade'),
    (select id from sets where short_name = 'c16'),
    '334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dispeller''s Capsule'),
    (select id from sets where short_name = 'c16'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terminate'),
    (select id from sets where short_name = 'c16'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Hollow'),
    (select id from sets where short_name = 'c16'),
    '303',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thelonite Hermit'),
    (select id from sets where short_name = 'c16'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Utter End'),
    (select id from sets where short_name = 'c16'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Elder'),
    (select id from sets where short_name = 'c16'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dismal Backwater'),
    (select id from sets where short_name = 'c16'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakdos Signet'),
    (select id from sets where short_name = 'c16'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selfless Squire'),
    (select id from sets where short_name = 'c16'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'c16'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grand Coliseum'),
    (select id from sets where short_name = 'c16'),
    '299',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'c16'),
    '272',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deepglow Skate'),
    (select id from sets where short_name = 'c16'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kydele, Chosen of Kruphix'),
    (select id from sets where short_name = 'c16'),
    '35',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sunforger'),
    (select id from sets where short_name = 'c16'),
    '275',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blinkmoth Urn'),
    (select id from sets where short_name = 'c16'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bruse Tarl, Boorish Herder'),
    (select id from sets where short_name = 'c16'),
    '30',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Treacherous Terrain'),
    (select id from sets where short_name = 'c16'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Temple of the False God'),
    (select id from sets where short_name = 'c16'),
    '331',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Etherium-Horn Sorcerer'),
    (select id from sets where short_name = 'c16'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selesnya Guildmage'),
    (select id from sets where short_name = 'c16'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azorius Chancery'),
    (select id from sets where short_name = 'c16'),
    '282',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Conqueror''s Flail'),
    (select id from sets where short_name = 'c16'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hellkite Tyrant'),
    (select id from sets where short_name = 'c16'),
    '128',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cranial Plating'),
    (select id from sets where short_name = 'c16'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kalonian Hydra'),
    (select id from sets where short_name = 'c16'),
    '154',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Opal Palace'),
    (select id from sets where short_name = 'c16'),
    '312',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancient Excavation'),
    (select id from sets where short_name = 'c16'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sidar Kondo of Jamuraa'),
    (select id from sets where short_name = 'c16'),
    '42',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Daretti, Scrap Savant'),
    (select id from sets where short_name = 'c16'),
    '123',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Simic Growth Chamber'),
    (select id from sets where short_name = 'c16'),
    '326',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c16'),
    '347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Astral Cornucopia'),
    (select id from sets where short_name = 'c16'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c16'),
    '348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loxodon Warhammer'),
    (select id from sets where short_name = 'c16'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Edric, Spymaster of Trest'),
    (select id from sets where short_name = 'c16'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thrasios, Triton Hero'),
    (select id from sets where short_name = 'c16'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Verge'),
    (select id from sets where short_name = 'c16'),
    '306',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thornwood Falls'),
    (select id from sets where short_name = 'c16'),
    '333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curse of Vengeance'),
    (select id from sets where short_name = 'c16'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reforge the Soul'),
    (select id from sets where short_name = 'c16'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wilderness Elemental'),
    (select id from sets where short_name = 'c16'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mystic Monastery'),
    (select id from sets where short_name = 'c16'),
    '310',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wave of Reckoning'),
    (select id from sets where short_name = 'c16'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c16'),
    '339',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spellheart Chimera'),
    (select id from sets where short_name = 'c16'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Commander''s Sphere'),
    (select id from sets where short_name = 'c16'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Everlasting Torment'),
    (select id from sets where short_name = 'c16'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nath of the Gilt-Leaf'),
    (select id from sets where short_name = 'c16'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Order // Chaos'),
    (select id from sets where short_name = 'c16'),
    '240',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'c16'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chaos Warp'),
    (select id from sets where short_name = 'c16'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Korozda Guildmage'),
    (select id from sets where short_name = 'c16'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disdainful Stroke'),
    (select id from sets where short_name = 'c16'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whipflare'),
    (select id from sets where short_name = 'c16'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Parting Thoughts'),
    (select id from sets where short_name = 'c16'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Waste Not'),
    (select id from sets where short_name = 'c16'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Army of the Damned'),
    (select id from sets where short_name = 'c16'),
    '105',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Divergent Transformations'),
    (select id from sets where short_name = 'c16'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghave, Guru of Spores'),
    (select id from sets where short_name = 'c16'),
    '200',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c16'),
    '337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akroan Horse'),
    (select id from sets where short_name = 'c16'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alesha, Who Smiles at Death'),
    (select id from sets where short_name = 'c16'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necrogenesis'),
    (select id from sets where short_name = 'c16'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grave Upheaval'),
    (select id from sets where short_name = 'c16'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghostly Prison'),
    (select id from sets where short_name = 'c16'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c16'),
    '341',
    'common'
) ,
(
    (select id from mtgcard where name = 'Underground River'),
    (select id from sets where short_name = 'c16'),
    '335',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Master of Etherium'),
    (select id from sets where short_name = 'c16'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rootbound Crag'),
    (select id from sets where short_name = 'c16'),
    '317',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tezzeret''s Gambit'),
    (select id from sets where short_name = 'c16'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trial // Error'),
    (select id from sets where short_name = 'c16'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvok Explorer'),
    (select id from sets where short_name = 'c16'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chain of Vapor'),
    (select id from sets where short_name = 'c16'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Beastmaster'),
    (select id from sets where short_name = 'c16'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evolutionary Escalation'),
    (select id from sets where short_name = 'c16'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Carnarium'),
    (select id from sets where short_name = 'c16'),
    '315',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Far Wanderings'),
    (select id from sets where short_name = 'c16'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magus of the Will'),
    (select id from sets where short_name = 'c16'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c16'),
    '342',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akiri, Line-Slinger'),
    (select id from sets where short_name = 'c16'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rites of Flourishing'),
    (select id from sets where short_name = 'c16'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coiling Oracle'),
    (select id from sets where short_name = 'c16'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blazing Archon'),
    (select id from sets where short_name = 'c16'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Psychosis Crawler'),
    (select id from sets where short_name = 'c16'),
    '267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cruel Entertainment'),
    (select id from sets where short_name = 'c16'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Collective Voyage'),
    (select id from sets where short_name = 'c16'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonskull Summit'),
    (select id from sets where short_name = 'c16'),
    '292',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hardened Scales'),
    (select id from sets where short_name = 'c16'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Windfall'),
    (select id from sets where short_name = 'c16'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Myr Battlesphere'),
    (select id from sets where short_name = 'c16'),
    '263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crystalline Crawler'),
    (select id from sets where short_name = 'c16'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Mage'),
    (select id from sets where short_name = 'c16'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spelltwine'),
    (select id from sets where short_name = 'c16'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slobad, Goblin Tinkerer'),
    (select id from sets where short_name = 'c16'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sangromancer'),
    (select id from sets where short_name = 'c16'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blasphemous Act'),
    (select id from sets where short_name = 'c16'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keening Stone'),
    (select id from sets where short_name = 'c16'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Citadel Siege'),
    (select id from sets where short_name = 'c16'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ikra Shidiqi, the Usurper'),
    (select id from sets where short_name = 'c16'),
    '32',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stonehoof Chieftain'),
    (select id from sets where short_name = 'c16'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Reclamation'),
    (select id from sets where short_name = 'c16'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Primeval Protector'),
    (select id from sets where short_name = 'c16'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scavenging Ooze'),
    (select id from sets where short_name = 'c16'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spinerock Knoll'),
    (select id from sets where short_name = 'c16'),
    '327',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darksteel Citadel'),
    (select id from sets where short_name = 'c16'),
    '288',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Minds Aglow'),
    (select id from sets where short_name = 'c16'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silas Renn, Seeker Adept'),
    (select id from sets where short_name = 'c16'),
    '43',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Assault Suit'),
    (select id from sets where short_name = 'c16'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ankle Shanker'),
    (select id from sets where short_name = 'c16'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Signet'),
    (select id from sets where short_name = 'c16'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Homeward Path'),
    (select id from sets where short_name = 'c16'),
    '301',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forbidden Orchard'),
    (select id from sets where short_name = 'c16'),
    '296',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kraum, Ludevic''s Opus'),
    (select id from sets where short_name = 'c16'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reyhan, Last of the Abzan'),
    (select id from sets where short_name = 'c16'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nomad Outpost'),
    (select id from sets where short_name = 'c16'),
    '311',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Farseek'),
    (select id from sets where short_name = 'c16'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c16'),
    '340',
    'common'
) ,
(
    (select id from mtgcard where name = 'Den Protector'),
    (select id from sets where short_name = 'c16'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whispering Madness'),
    (select id from sets where short_name = 'c16'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Putrefy'),
    (select id from sets where short_name = 'c16'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Saskia the Unyielding'),
    (select id from sets where short_name = 'c16'),
    '41',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Clan Defiance'),
    (select id from sets where short_name = 'c16'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beastmaster Ascension'),
    (select id from sets where short_name = 'c16'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hushwing Gryff'),
    (select id from sets where short_name = 'c16'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treasure Cruise'),
    (select id from sets where short_name = 'c16'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hellkite Igniter'),
    (select id from sets where short_name = 'c16'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Izzet Boilerworks'),
    (select id from sets where short_name = 'c16'),
    '302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Simic Signet'),
    (select id from sets where short_name = 'c16'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spitting Image'),
    (select id from sets where short_name = 'c16'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Everflowing Chalice'),
    (select id from sets where short_name = 'c16'),
    '253',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duneblast'),
    (select id from sets where short_name = 'c16'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forgotten Ancient'),
    (select id from sets where short_name = 'c16'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Command Tower'),
    (select id from sets where short_name = 'c16'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple Bell'),
    (select id from sets where short_name = 'c16'),
    '277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Blossoms'),
    (select id from sets where short_name = 'c16'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burgeoning'),
    (select id from sets where short_name = 'c16'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darksteel Ingot'),
    (select id from sets where short_name = 'c16'),
    '250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Rot Farm'),
    (select id from sets where short_name = 'c16'),
    '298',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lurking Predators'),
    (select id from sets where short_name = 'c16'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trading Post'),
    (select id from sets where short_name = 'c16'),
    '278',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tempt with Discovery'),
    (select id from sets where short_name = 'c16'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c16'),
    '350',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karplusan Forest'),
    (select id from sets where short_name = 'c16'),
    '305',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darkwater Catacombs'),
    (select id from sets where short_name = 'c16'),
    '289',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Horizon Chimera'),
    (select id from sets where short_name = 'c16'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Past in Flames'),
    (select id from sets where short_name = 'c16'),
    '131',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sydri, Galvanic Genius'),
    (select id from sets where short_name = 'c16'),
    '224',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vial Smasher the Fierce'),
    (select id from sets where short_name = 'c16'),
    '49',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Myriad Landscape'),
    (select id from sets where short_name = 'c16'),
    '309',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Godo, Bandit Warlord'),
    (select id from sets where short_name = 'c16'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Signet'),
    (select id from sets where short_name = 'c16'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul of New Phyrexia'),
    (select id from sets where short_name = 'c16'),
    '274',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Read the Runes'),
    (select id from sets where short_name = 'c16'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bred for the Hunt'),
    (select id from sets where short_name = 'c16'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Aqueduct'),
    (select id from sets where short_name = 'c16'),
    '290',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reliquary Tower'),
    (select id from sets where short_name = 'c16'),
    '316',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Venser''s Journal'),
    (select id from sets where short_name = 'c16'),
    '279',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Languish'),
    (select id from sets where short_name = 'c16'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sandsteppe Citadel'),
    (select id from sets where short_name = 'c16'),
    '320',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orzhov Basilica'),
    (select id from sets where short_name = 'c16'),
    '314',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chromatic Lantern'),
    (select id from sets where short_name = 'c16'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crumbling Necropolis'),
    (select id from sets where short_name = 'c16'),
    '287',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Opulent Palace'),
    (select id from sets where short_name = 'c16'),
    '313',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Executioner''s Capsule'),
    (select id from sets where short_name = 'c16'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodbraid Elf'),
    (select id from sets where short_name = 'c16'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rugged Highlands'),
    (select id from sets where short_name = 'c16'),
    '318',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx Summoner'),
    (select id from sets where short_name = 'c16'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c16'),
    '343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shimmer Myr'),
    (select id from sets where short_name = 'c16'),
    '269',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kodama''s Reach'),
    (select id from sets where short_name = 'c16'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Etherium Sculptor'),
    (select id from sets where short_name = 'c16'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sungrass Prairie'),
    (select id from sets where short_name = 'c16'),
    '328',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Greaves'),
    (select id from sets where short_name = 'c16'),
    '260',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Etched Oracle'),
    (select id from sets where short_name = 'c16'),
    '252',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cauldron of Souls'),
    (select id from sets where short_name = 'c16'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Baleful Strix'),
    (select id from sets where short_name = 'c16'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Breya, Etherium Shaper'),
    (select id from sets where short_name = 'c16'),
    '29',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chief Engineer'),
    (select id from sets where short_name = 'c16'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aeon Chronicler'),
    (select id from sets where short_name = 'c16'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'In Garruk''s Wake'),
    (select id from sets where short_name = 'c16'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Open the Vaults'),
    (select id from sets where short_name = 'c16'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abzan Falconer'),
    (select id from sets where short_name = 'c16'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Garrison'),
    (select id from sets where short_name = 'c16'),
    '283',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Migratory Route'),
    (select id from sets where short_name = 'c16'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bonehoard'),
    (select id from sets where short_name = 'c16'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skullclamp'),
    (select id from sets where short_name = 'c16'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cathars'' Crusade'),
    (select id from sets where short_name = 'c16'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vorel of the Hull Clade'),
    (select id from sets where short_name = 'c16'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Realm Seekers'),
    (select id from sets where short_name = 'c16'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Buried Ruin'),
    (select id from sets where short_name = 'c16'),
    '284',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ichor Wellspring'),
    (select id from sets where short_name = 'c16'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vulturous Zombie'),
    (select id from sets where short_name = 'c16'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Veteran Explorer'),
    (select id from sets where short_name = 'c16'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chasm Skulker'),
    (select id from sets where short_name = 'c16'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kynaios and Tiro of Meletis'),
    (select id from sets where short_name = 'c16'),
    '36',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Selvala, Explorer Returned'),
    (select id from sets where short_name = 'c16'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Rebirth'),
    (select id from sets where short_name = 'c16'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Consuming Aberration'),
    (select id from sets where short_name = 'c16'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mortify'),
    (select id from sets where short_name = 'c16'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'c16'),
    '332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blind Obedience'),
    (select id from sets where short_name = 'c16'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inspiring Call'),
    (select id from sets where short_name = 'c16'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dreadship Reef'),
    (select id from sets where short_name = 'c16'),
    '293',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ash Barrens'),
    (select id from sets where short_name = 'c16'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swan Song'),
    (select id from sets where short_name = 'c16'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oath of Druids'),
    (select id from sets where short_name = 'c16'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savage Lands'),
    (select id from sets where short_name = 'c16'),
    '321',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sphere of Safety'),
    (select id from sets where short_name = 'c16'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Solemn Simulacrum'),
    (select id from sets where short_name = 'c16'),
    '273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Breath of Fury'),
    (select id from sets where short_name = 'c16'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruul Signet'),
    (select id from sets where short_name = 'c16'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tana, the Bloodsower'),
    (select id from sets where short_name = 'c16'),
    '45',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Corpsejack Menace'),
    (select id from sets where short_name = 'c16'),
    '190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seaside Citadel'),
    (select id from sets where short_name = 'c16'),
    '322',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mycoloth'),
    (select id from sets where short_name = 'c16'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcane Denial'),
    (select id from sets where short_name = 'c16'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brave the Sands'),
    (select id from sets where short_name = 'c16'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frontier Bivouac'),
    (select id from sets where short_name = 'c16'),
    '297',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Master Biomancer'),
    (select id from sets where short_name = 'c16'),
    '210',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Enduring Scalelord'),
    (select id from sets where short_name = 'c16'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dauntless Escort'),
    (select id from sets where short_name = 'c16'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shadowblood Ridge'),
    (select id from sets where short_name = 'c16'),
    '325',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thopter Foundry'),
    (select id from sets where short_name = 'c16'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c16'),
    '345',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul Turf'),
    (select id from sets where short_name = 'c16'),
    '300',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Exotic Orchard'),
    (select id from sets where short_name = 'c16'),
    '295',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c16'),
    '351',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swiftwater Cliffs'),
    (select id from sets where short_name = 'c16'),
    '330',
    'common'
) ,
(
    (select id from mtgcard where name = 'Progenitor Mimic'),
    (select id from sets where short_name = 'c16'),
    '216',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kazuul, Tyrant of the Cliffs'),
    (select id from sets where short_name = 'c16'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Tyrant'),
    (select id from sets where short_name = 'c16'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vedalken Engineer'),
    (select id from sets where short_name = 'c16'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sublime Exhalation'),
    (select id from sets where short_name = 'c16'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Artifact Mutation'),
    (select id from sets where short_name = 'c16'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Manifold Insights'),
    (select id from sets where short_name = 'c16'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whims of the Fates'),
    (select id from sets where short_name = 'c16'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gwafa Hazid, Profiteer'),
    (select id from sets where short_name = 'c16'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Filigree Angel'),
    (select id from sets where short_name = 'c16'),
    '199',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Iroas, God of Victory'),
    (select id from sets where short_name = 'c16'),
    '205',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Windborn Muse'),
    (select id from sets where short_name = 'c16'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wight of Precinct Six'),
    (select id from sets where short_name = 'c16'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gamekeeper'),
    (select id from sets where short_name = 'c16'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tuskguard Captain'),
    (select id from sets where short_name = 'c16'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reverse the Sands'),
    (select id from sets where short_name = 'c16'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zhur-Taa Druid'),
    (select id from sets where short_name = 'c16'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seeds of Renewal'),
    (select id from sets where short_name = 'c16'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beacon of Unrest'),
    (select id from sets where short_name = 'c16'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evacuation'),
    (select id from sets where short_name = 'c16'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c16'),
    '338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tymna the Weaver'),
    (select id from sets where short_name = 'c16'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Spymaster'),
    (select id from sets where short_name = 'c16'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Taurean Mauler'),
    (select id from sets where short_name = 'c16'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reveillark'),
    (select id from sets where short_name = 'c16'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coastal Breach'),
    (select id from sets where short_name = 'c16'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Vision'),
    (select id from sets where short_name = 'c16'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beast Within'),
    (select id from sets where short_name = 'c16'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghastly Conscription'),
    (select id from sets where short_name = 'c16'),
    '111',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c16'),
    '344',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abzan Charm'),
    (select id from sets where short_name = 'c16'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Propaganda'),
    (select id from sets where short_name = 'c16'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yidris, Maelstrom Wielder'),
    (select id from sets where short_name = 'c16'),
    '50',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hanna, Ship''s Navigator'),
    (select id from sets where short_name = 'c16'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Atraxa, Praetors'' Voice'),
    (select id from sets where short_name = 'c16'),
    '28',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = 'c16'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctum Gargoyle'),
    (select id from sets where short_name = 'c16'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fathom Mage'),
    (select id from sets where short_name = 'c16'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Worm Harvest'),
    (select id from sets where short_name = 'c16'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reins of Power'),
    (select id from sets where short_name = 'c16'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oblation'),
    (select id from sets where short_name = 'c16'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Devastation Tide'),
    (select id from sets where short_name = 'c16'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jungle Shrine'),
    (select id from sets where short_name = 'c16'),
    '304',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armory Automaton'),
    (select id from sets where short_name = 'c16'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Windbrisk Heights'),
    (select id from sets where short_name = 'c16'),
    '336',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seat of the Synod'),
    (select id from sets where short_name = 'c16'),
    '323',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mentor of the Meek'),
    (select id from sets where short_name = 'c16'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Howling Mine'),
    (select id from sets where short_name = 'c16'),
    '257',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirror Entity'),
    (select id from sets where short_name = 'c16'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jor Kadeen, the Prevailer'),
    (select id from sets where short_name = 'c16'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bane of the Living'),
    (select id from sets where short_name = 'c16'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Murmuring Bosk'),
    (select id from sets where short_name = 'c16'),
    '308',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frenzied Fugue'),
    (select id from sets where short_name = 'c16'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guiltfeeder'),
    (select id from sets where short_name = 'c16'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fellwar Stone'),
    (select id from sets where short_name = 'c16'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcane Sanctum'),
    (select id from sets where short_name = 'c16'),
    '281',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grip of Phyresis'),
    (select id from sets where short_name = 'c16'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quirion Explorer'),
    (select id from sets where short_name = 'c16'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trinket Mage'),
    (select id from sets where short_name = 'c16'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirrorweave'),
    (select id from sets where short_name = 'c16'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bituminous Blast'),
    (select id from sets where short_name = 'c16'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grab the Reins'),
    (select id from sets where short_name = 'c16'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mosswort Bridge'),
    (select id from sets where short_name = 'c16'),
    '307',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Empyrial Plate'),
    (select id from sets where short_name = 'c16'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thunderfoot Baloth'),
    (select id from sets where short_name = 'c16'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swiftfoot Boots'),
    (select id from sets where short_name = 'c16'),
    '276',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shamanic Revelation'),
    (select id from sets where short_name = 'c16'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elite Scaleguard'),
    (select id from sets where short_name = 'c16'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Curtains'' Call'),
    (select id from sets where short_name = 'c16'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crackling Doom'),
    (select id from sets where short_name = 'c16'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Decimate'),
    (select id from sets where short_name = 'c16'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Charm'),
    (select id from sets where short_name = 'c16'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brutal Hordechief'),
    (select id from sets where short_name = 'c16'),
    '108',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Necroplasm'),
    (select id from sets where short_name = 'c16'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boompile'),
    (select id from sets where short_name = 'c16'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ishai, Ojutai Dragonspeaker'),
    (select id from sets where short_name = 'c16'),
    '33',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Benefactor''s Draught'),
    (select id from sets where short_name = 'c16'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c16'),
    '349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c16'),
    '346',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rupture Spire'),
    (select id from sets where short_name = 'c16'),
    '319',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zedruu the Greathearted'),
    (select id from sets where short_name = 'c16'),
    '231',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Custodi Soulbinders'),
    (select id from sets where short_name = 'c16'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Juniper Order Ranger'),
    (select id from sets where short_name = 'c16'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunpetal Grove'),
    (select id from sets where short_name = 'c16'),
    '329',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mycosynth Wellspring'),
    (select id from sets where short_name = 'c16'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Festercreep'),
    (select id from sets where short_name = 'c16'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cultivate'),
    (select id from sets where short_name = 'c16'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hoofprints of the Stag'),
    (select id from sets where short_name = 'c16'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Humble Defector'),
    (select id from sets where short_name = 'c16'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trash for Treasure'),
    (select id from sets where short_name = 'c16'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ravos, Soultender'),
    (select id from sets where short_name = 'c16'),
    '39',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = 'c16'),
    '265',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Managorger Hydra'),
    (select id from sets where short_name = 'c16'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Academy Elite'),
    (select id from sets where short_name = 'c16'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ludevic, Necro-Alchemist'),
    (select id from sets where short_name = 'c16'),
    '37',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Glint-Eye Nephilim'),
    (select id from sets where short_name = 'c16'),
    '201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Entrapment Maneuver'),
    (select id from sets where short_name = 'c16'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thrummingbird'),
    (select id from sets where short_name = 'c16'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wheel of Fate'),
    (select id from sets where short_name = 'c16'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stalking Vengeance'),
    (select id from sets where short_name = 'c16'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Runehorn Hellkite'),
    (select id from sets where short_name = 'c16'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lavalanche'),
    (select id from sets where short_name = 'c16'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Naya Charm'),
    (select id from sets where short_name = 'c16'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Caves of Koilos'),
    (select id from sets where short_name = 'c16'),
    '285',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ethersworn Adjudicator'),
    (select id from sets where short_name = 'c16'),
    '90',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sharuum the Hegemon'),
    (select id from sets where short_name = 'c16'),
    '221',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Charging Cinderhorn'),
    (select id from sets where short_name = 'c16'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Champion of Lambholt'),
    (select id from sets where short_name = 'c16'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myr Retriever'),
    (select id from sets where short_name = 'c16'),
    '264',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Solidarity of Heroes'),
    (select id from sets where short_name = 'c16'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Faerie Artisans'),
    (select id from sets where short_name = 'c16'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Charm'),
    (select id from sets where short_name = 'c16'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Satyr Wayfinder'),
    (select id from sets where short_name = 'c16'),
    '165',
    'common'
) 
 on conflict do nothing;
