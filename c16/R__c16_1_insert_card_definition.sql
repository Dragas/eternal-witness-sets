    insert into mtgcard(name) values ('Duelist''s Heritage') on conflict do nothing;
    insert into mtgcard(name) values ('Orzhov Advokist') on conflict do nothing;
    insert into mtgcard(name) values ('Prismatic Geoscope') on conflict do nothing;
    insert into mtgcard(name) values ('Selesnya Sanctuary') on conflict do nothing;
    insert into mtgcard(name) values ('Merciless Eviction') on conflict do nothing;
    insert into mtgcard(name) values ('Aura Mutation') on conflict do nothing;
    insert into mtgcard(name) values ('Whispersilk Cloak') on conflict do nothing;
    insert into mtgcard(name) values ('Rubblehulk') on conflict do nothing;
    insert into mtgcard(name) values ('Transguild Promenade') on conflict do nothing;
    insert into mtgcard(name) values ('Dispeller''s Capsule') on conflict do nothing;
    insert into mtgcard(name) values ('Terminate') on conflict do nothing;
    insert into mtgcard(name) values ('Jungle Hollow') on conflict do nothing;
    insert into mtgcard(name) values ('Thelonite Hermit') on conflict do nothing;
    insert into mtgcard(name) values ('Utter End') on conflict do nothing;
    insert into mtgcard(name) values ('Sakura-Tribe Elder') on conflict do nothing;
    insert into mtgcard(name) values ('Dismal Backwater') on conflict do nothing;
    insert into mtgcard(name) values ('Rakdos Signet') on conflict do nothing;
    insert into mtgcard(name) values ('Selfless Squire') on conflict do nothing;
    insert into mtgcard(name) values ('Swords to Plowshares') on conflict do nothing;
    insert into mtgcard(name) values ('Grand Coliseum') on conflict do nothing;
    insert into mtgcard(name) values ('Sol Ring') on conflict do nothing;
    insert into mtgcard(name) values ('Deepglow Skate') on conflict do nothing;
    insert into mtgcard(name) values ('Kydele, Chosen of Kruphix') on conflict do nothing;
    insert into mtgcard(name) values ('Sunforger') on conflict do nothing;
    insert into mtgcard(name) values ('Blinkmoth Urn') on conflict do nothing;
    insert into mtgcard(name) values ('Bruse Tarl, Boorish Herder') on conflict do nothing;
    insert into mtgcard(name) values ('Treacherous Terrain') on conflict do nothing;
    insert into mtgcard(name) values ('Temple of the False God') on conflict do nothing;
    insert into mtgcard(name) values ('Etherium-Horn Sorcerer') on conflict do nothing;
    insert into mtgcard(name) values ('Selesnya Guildmage') on conflict do nothing;
    insert into mtgcard(name) values ('Azorius Chancery') on conflict do nothing;
    insert into mtgcard(name) values ('Conqueror''s Flail') on conflict do nothing;
    insert into mtgcard(name) values ('Hellkite Tyrant') on conflict do nothing;
    insert into mtgcard(name) values ('Cranial Plating') on conflict do nothing;
    insert into mtgcard(name) values ('Kalonian Hydra') on conflict do nothing;
    insert into mtgcard(name) values ('Opal Palace') on conflict do nothing;
    insert into mtgcard(name) values ('Ancient Excavation') on conflict do nothing;
    insert into mtgcard(name) values ('Sidar Kondo of Jamuraa') on conflict do nothing;
    insert into mtgcard(name) values ('Daretti, Scrap Savant') on conflict do nothing;
    insert into mtgcard(name) values ('Simic Growth Chamber') on conflict do nothing;
    insert into mtgcard(name) values ('Mountain') on conflict do nothing;
    insert into mtgcard(name) values ('Astral Cornucopia') on conflict do nothing;
    insert into mtgcard(name) values ('Loxodon Warhammer') on conflict do nothing;
    insert into mtgcard(name) values ('Edric, Spymaster of Trest') on conflict do nothing;
    insert into mtgcard(name) values ('Thrasios, Triton Hero') on conflict do nothing;
    insert into mtgcard(name) values ('Krosan Verge') on conflict do nothing;
    insert into mtgcard(name) values ('Thornwood Falls') on conflict do nothing;
    insert into mtgcard(name) values ('Curse of Vengeance') on conflict do nothing;
    insert into mtgcard(name) values ('Reforge the Soul') on conflict do nothing;
    insert into mtgcard(name) values ('Wilderness Elemental') on conflict do nothing;
    insert into mtgcard(name) values ('Mystic Monastery') on conflict do nothing;
    insert into mtgcard(name) values ('Wave of Reckoning') on conflict do nothing;
    insert into mtgcard(name) values ('Plains') on conflict do nothing;
    insert into mtgcard(name) values ('Spellheart Chimera') on conflict do nothing;
    insert into mtgcard(name) values ('Commander''s Sphere') on conflict do nothing;
    insert into mtgcard(name) values ('Everlasting Torment') on conflict do nothing;
    insert into mtgcard(name) values ('Nath of the Gilt-Leaf') on conflict do nothing;
    insert into mtgcard(name) values ('Order // Chaos') on conflict do nothing;
    insert into mtgcard(name) values ('Evolving Wilds') on conflict do nothing;
    insert into mtgcard(name) values ('Chaos Warp') on conflict do nothing;
    insert into mtgcard(name) values ('Korozda Guildmage') on conflict do nothing;
    insert into mtgcard(name) values ('Disdainful Stroke') on conflict do nothing;
    insert into mtgcard(name) values ('Whipflare') on conflict do nothing;
    insert into mtgcard(name) values ('Parting Thoughts') on conflict do nothing;
    insert into mtgcard(name) values ('Waste Not') on conflict do nothing;
    insert into mtgcard(name) values ('Army of the Damned') on conflict do nothing;
    insert into mtgcard(name) values ('Divergent Transformations') on conflict do nothing;
    insert into mtgcard(name) values ('Ghave, Guru of Spores') on conflict do nothing;
    insert into mtgcard(name) values ('Akroan Horse') on conflict do nothing;
    insert into mtgcard(name) values ('Alesha, Who Smiles at Death') on conflict do nothing;
    insert into mtgcard(name) values ('Necrogenesis') on conflict do nothing;
    insert into mtgcard(name) values ('Grave Upheaval') on conflict do nothing;
    insert into mtgcard(name) values ('Ghostly Prison') on conflict do nothing;
    insert into mtgcard(name) values ('Island') on conflict do nothing;
    insert into mtgcard(name) values ('Underground River') on conflict do nothing;
    insert into mtgcard(name) values ('Master of Etherium') on conflict do nothing;
    insert into mtgcard(name) values ('Rootbound Crag') on conflict do nothing;
    insert into mtgcard(name) values ('Tezzeret''s Gambit') on conflict do nothing;
    insert into mtgcard(name) values ('Trial // Error') on conflict do nothing;
    insert into mtgcard(name) values ('Sylvok Explorer') on conflict do nothing;
    insert into mtgcard(name) values ('Chain of Vapor') on conflict do nothing;
    insert into mtgcard(name) values ('Wild Beastmaster') on conflict do nothing;
    insert into mtgcard(name) values ('Evolutionary Escalation') on conflict do nothing;
    insert into mtgcard(name) values ('Rakdos Carnarium') on conflict do nothing;
    insert into mtgcard(name) values ('Far Wanderings') on conflict do nothing;
    insert into mtgcard(name) values ('Magus of the Will') on conflict do nothing;
    insert into mtgcard(name) values ('Akiri, Line-Slinger') on conflict do nothing;
    insert into mtgcard(name) values ('Rites of Flourishing') on conflict do nothing;
    insert into mtgcard(name) values ('Coiling Oracle') on conflict do nothing;
    insert into mtgcard(name) values ('Blazing Archon') on conflict do nothing;
    insert into mtgcard(name) values ('Psychosis Crawler') on conflict do nothing;
    insert into mtgcard(name) values ('Cruel Entertainment') on conflict do nothing;
    insert into mtgcard(name) values ('Collective Voyage') on conflict do nothing;
    insert into mtgcard(name) values ('Dragonskull Summit') on conflict do nothing;
    insert into mtgcard(name) values ('Hardened Scales') on conflict do nothing;
    insert into mtgcard(name) values ('Windfall') on conflict do nothing;
    insert into mtgcard(name) values ('Myr Battlesphere') on conflict do nothing;
    insert into mtgcard(name) values ('Crystalline Crawler') on conflict do nothing;
    insert into mtgcard(name) values ('Dragon Mage') on conflict do nothing;
    insert into mtgcard(name) values ('Spelltwine') on conflict do nothing;
    insert into mtgcard(name) values ('Slobad, Goblin Tinkerer') on conflict do nothing;
    insert into mtgcard(name) values ('Sangromancer') on conflict do nothing;
    insert into mtgcard(name) values ('Blasphemous Act') on conflict do nothing;
    insert into mtgcard(name) values ('Keening Stone') on conflict do nothing;
    insert into mtgcard(name) values ('Citadel Siege') on conflict do nothing;
    insert into mtgcard(name) values ('Ikra Shidiqi, the Usurper') on conflict do nothing;
    insert into mtgcard(name) values ('Stonehoof Chieftain') on conflict do nothing;
    insert into mtgcard(name) values ('Sylvan Reclamation') on conflict do nothing;
    insert into mtgcard(name) values ('Primeval Protector') on conflict do nothing;
    insert into mtgcard(name) values ('Scavenging Ooze') on conflict do nothing;
    insert into mtgcard(name) values ('Spinerock Knoll') on conflict do nothing;
    insert into mtgcard(name) values ('Darksteel Citadel') on conflict do nothing;
    insert into mtgcard(name) values ('Minds Aglow') on conflict do nothing;
    insert into mtgcard(name) values ('Silas Renn, Seeker Adept') on conflict do nothing;
    insert into mtgcard(name) values ('Assault Suit') on conflict do nothing;
    insert into mtgcard(name) values ('Ankle Shanker') on conflict do nothing;
    insert into mtgcard(name) values ('Orzhov Signet') on conflict do nothing;
    insert into mtgcard(name) values ('Homeward Path') on conflict do nothing;
    insert into mtgcard(name) values ('Forbidden Orchard') on conflict do nothing;
    insert into mtgcard(name) values ('Kraum, Ludevic''s Opus') on conflict do nothing;
    insert into mtgcard(name) values ('Reyhan, Last of the Abzan') on conflict do nothing;
    insert into mtgcard(name) values ('Nomad Outpost') on conflict do nothing;
    insert into mtgcard(name) values ('Farseek') on conflict do nothing;
    insert into mtgcard(name) values ('Den Protector') on conflict do nothing;
    insert into mtgcard(name) values ('Whispering Madness') on conflict do nothing;
    insert into mtgcard(name) values ('Putrefy') on conflict do nothing;
    insert into mtgcard(name) values ('Saskia the Unyielding') on conflict do nothing;
    insert into mtgcard(name) values ('Clan Defiance') on conflict do nothing;
    insert into mtgcard(name) values ('Beastmaster Ascension') on conflict do nothing;
    insert into mtgcard(name) values ('Hushwing Gryff') on conflict do nothing;
    insert into mtgcard(name) values ('Treasure Cruise') on conflict do nothing;
    insert into mtgcard(name) values ('Hellkite Igniter') on conflict do nothing;
    insert into mtgcard(name) values ('Izzet Boilerworks') on conflict do nothing;
    insert into mtgcard(name) values ('Simic Signet') on conflict do nothing;
    insert into mtgcard(name) values ('Spitting Image') on conflict do nothing;
    insert into mtgcard(name) values ('Everflowing Chalice') on conflict do nothing;
    insert into mtgcard(name) values ('Duneblast') on conflict do nothing;
    insert into mtgcard(name) values ('Forgotten Ancient') on conflict do nothing;
    insert into mtgcard(name) values ('Command Tower') on conflict do nothing;
    insert into mtgcard(name) values ('Temple Bell') on conflict do nothing;
    insert into mtgcard(name) values ('Wall of Blossoms') on conflict do nothing;
    insert into mtgcard(name) values ('Burgeoning') on conflict do nothing;
    insert into mtgcard(name) values ('Darksteel Ingot') on conflict do nothing;
    insert into mtgcard(name) values ('Golgari Rot Farm') on conflict do nothing;
    insert into mtgcard(name) values ('Lurking Predators') on conflict do nothing;
    insert into mtgcard(name) values ('Trading Post') on conflict do nothing;
    insert into mtgcard(name) values ('Tempt with Discovery') on conflict do nothing;
    insert into mtgcard(name) values ('Forest') on conflict do nothing;
    insert into mtgcard(name) values ('Karplusan Forest') on conflict do nothing;
    insert into mtgcard(name) values ('Darkwater Catacombs') on conflict do nothing;
    insert into mtgcard(name) values ('Horizon Chimera') on conflict do nothing;
    insert into mtgcard(name) values ('Past in Flames') on conflict do nothing;
    insert into mtgcard(name) values ('Sydri, Galvanic Genius') on conflict do nothing;
    insert into mtgcard(name) values ('Vial Smasher the Fierce') on conflict do nothing;
    insert into mtgcard(name) values ('Myriad Landscape') on conflict do nothing;
    insert into mtgcard(name) values ('Godo, Bandit Warlord') on conflict do nothing;
    insert into mtgcard(name) values ('Golgari Signet') on conflict do nothing;
    insert into mtgcard(name) values ('Soul of New Phyrexia') on conflict do nothing;
    insert into mtgcard(name) values ('Read the Runes') on conflict do nothing;
    insert into mtgcard(name) values ('Bred for the Hunt') on conflict do nothing;
    insert into mtgcard(name) values ('Dimir Aqueduct') on conflict do nothing;
    insert into mtgcard(name) values ('Reliquary Tower') on conflict do nothing;
    insert into mtgcard(name) values ('Venser''s Journal') on conflict do nothing;
    insert into mtgcard(name) values ('Languish') on conflict do nothing;
    insert into mtgcard(name) values ('Sandsteppe Citadel') on conflict do nothing;
    insert into mtgcard(name) values ('Orzhov Basilica') on conflict do nothing;
    insert into mtgcard(name) values ('Chromatic Lantern') on conflict do nothing;
    insert into mtgcard(name) values ('Crumbling Necropolis') on conflict do nothing;
    insert into mtgcard(name) values ('Opulent Palace') on conflict do nothing;
    insert into mtgcard(name) values ('Executioner''s Capsule') on conflict do nothing;
    insert into mtgcard(name) values ('Bloodbraid Elf') on conflict do nothing;
    insert into mtgcard(name) values ('Rugged Highlands') on conflict do nothing;
    insert into mtgcard(name) values ('Sphinx Summoner') on conflict do nothing;
    insert into mtgcard(name) values ('Swamp') on conflict do nothing;
    insert into mtgcard(name) values ('Shimmer Myr') on conflict do nothing;
    insert into mtgcard(name) values ('Kodama''s Reach') on conflict do nothing;
    insert into mtgcard(name) values ('Etherium Sculptor') on conflict do nothing;
    insert into mtgcard(name) values ('Sungrass Prairie') on conflict do nothing;
    insert into mtgcard(name) values ('Lightning Greaves') on conflict do nothing;
    insert into mtgcard(name) values ('Etched Oracle') on conflict do nothing;
    insert into mtgcard(name) values ('Cauldron of Souls') on conflict do nothing;
    insert into mtgcard(name) values ('Baleful Strix') on conflict do nothing;
    insert into mtgcard(name) values ('Breya, Etherium Shaper') on conflict do nothing;
    insert into mtgcard(name) values ('Chief Engineer') on conflict do nothing;
    insert into mtgcard(name) values ('Aeon Chronicler') on conflict do nothing;
    insert into mtgcard(name) values ('In Garruk''s Wake') on conflict do nothing;
    insert into mtgcard(name) values ('Open the Vaults') on conflict do nothing;
    insert into mtgcard(name) values ('Abzan Falconer') on conflict do nothing;
    insert into mtgcard(name) values ('Boros Garrison') on conflict do nothing;
    insert into mtgcard(name) values ('Migratory Route') on conflict do nothing;
    insert into mtgcard(name) values ('Bonehoard') on conflict do nothing;
    insert into mtgcard(name) values ('Skullclamp') on conflict do nothing;
    insert into mtgcard(name) values ('Cathars'' Crusade') on conflict do nothing;
    insert into mtgcard(name) values ('Vorel of the Hull Clade') on conflict do nothing;
    insert into mtgcard(name) values ('Realm Seekers') on conflict do nothing;
    insert into mtgcard(name) values ('Buried Ruin') on conflict do nothing;
    insert into mtgcard(name) values ('Ichor Wellspring') on conflict do nothing;
    insert into mtgcard(name) values ('Vulturous Zombie') on conflict do nothing;
    insert into mtgcard(name) values ('Veteran Explorer') on conflict do nothing;
    insert into mtgcard(name) values ('Chasm Skulker') on conflict do nothing;
    insert into mtgcard(name) values ('Kynaios and Tiro of Meletis') on conflict do nothing;
    insert into mtgcard(name) values ('Selvala, Explorer Returned') on conflict do nothing;
    insert into mtgcard(name) values ('Phyrexian Rebirth') on conflict do nothing;
    insert into mtgcard(name) values ('Consuming Aberration') on conflict do nothing;
    insert into mtgcard(name) values ('Mortify') on conflict do nothing;
    insert into mtgcard(name) values ('Terramorphic Expanse') on conflict do nothing;
    insert into mtgcard(name) values ('Blind Obedience') on conflict do nothing;
    insert into mtgcard(name) values ('Inspiring Call') on conflict do nothing;
    insert into mtgcard(name) values ('Dreadship Reef') on conflict do nothing;
    insert into mtgcard(name) values ('Ash Barrens') on conflict do nothing;
    insert into mtgcard(name) values ('Swan Song') on conflict do nothing;
    insert into mtgcard(name) values ('Oath of Druids') on conflict do nothing;
    insert into mtgcard(name) values ('Savage Lands') on conflict do nothing;
    insert into mtgcard(name) values ('Sphere of Safety') on conflict do nothing;
    insert into mtgcard(name) values ('Solemn Simulacrum') on conflict do nothing;
    insert into mtgcard(name) values ('Breath of Fury') on conflict do nothing;
    insert into mtgcard(name) values ('Gruul Signet') on conflict do nothing;
    insert into mtgcard(name) values ('Tana, the Bloodsower') on conflict do nothing;
    insert into mtgcard(name) values ('Corpsejack Menace') on conflict do nothing;
    insert into mtgcard(name) values ('Seaside Citadel') on conflict do nothing;
    insert into mtgcard(name) values ('Mycoloth') on conflict do nothing;
    insert into mtgcard(name) values ('Arcane Denial') on conflict do nothing;
    insert into mtgcard(name) values ('Brave the Sands') on conflict do nothing;
    insert into mtgcard(name) values ('Frontier Bivouac') on conflict do nothing;
    insert into mtgcard(name) values ('Master Biomancer') on conflict do nothing;
    insert into mtgcard(name) values ('Enduring Scalelord') on conflict do nothing;
    insert into mtgcard(name) values ('Dauntless Escort') on conflict do nothing;
    insert into mtgcard(name) values ('Shadowblood Ridge') on conflict do nothing;
    insert into mtgcard(name) values ('Thopter Foundry') on conflict do nothing;
    insert into mtgcard(name) values ('Gruul Turf') on conflict do nothing;
    insert into mtgcard(name) values ('Exotic Orchard') on conflict do nothing;
    insert into mtgcard(name) values ('Swiftwater Cliffs') on conflict do nothing;
    insert into mtgcard(name) values ('Progenitor Mimic') on conflict do nothing;
    insert into mtgcard(name) values ('Kazuul, Tyrant of the Cliffs') on conflict do nothing;
    insert into mtgcard(name) values ('Blood Tyrant') on conflict do nothing;
    insert into mtgcard(name) values ('Vedalken Engineer') on conflict do nothing;
    insert into mtgcard(name) values ('Sublime Exhalation') on conflict do nothing;
    insert into mtgcard(name) values ('Artifact Mutation') on conflict do nothing;
    insert into mtgcard(name) values ('Manifold Insights') on conflict do nothing;
    insert into mtgcard(name) values ('Whims of the Fates') on conflict do nothing;
    insert into mtgcard(name) values ('Gwafa Hazid, Profiteer') on conflict do nothing;
    insert into mtgcard(name) values ('Filigree Angel') on conflict do nothing;
    insert into mtgcard(name) values ('Iroas, God of Victory') on conflict do nothing;
    insert into mtgcard(name) values ('Windborn Muse') on conflict do nothing;
    insert into mtgcard(name) values ('Wight of Precinct Six') on conflict do nothing;
    insert into mtgcard(name) values ('Gamekeeper') on conflict do nothing;
    insert into mtgcard(name) values ('Tuskguard Captain') on conflict do nothing;
    insert into mtgcard(name) values ('Reverse the Sands') on conflict do nothing;
    insert into mtgcard(name) values ('Zhur-Taa Druid') on conflict do nothing;
    insert into mtgcard(name) values ('Seeds of Renewal') on conflict do nothing;
    insert into mtgcard(name) values ('Beacon of Unrest') on conflict do nothing;
    insert into mtgcard(name) values ('Evacuation') on conflict do nothing;
    insert into mtgcard(name) values ('Tymna the Weaver') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Spymaster') on conflict do nothing;
    insert into mtgcard(name) values ('Taurean Mauler') on conflict do nothing;
    insert into mtgcard(name) values ('Reveillark') on conflict do nothing;
    insert into mtgcard(name) values ('Coastal Breach') on conflict do nothing;
    insert into mtgcard(name) values ('Volcanic Vision') on conflict do nothing;
    insert into mtgcard(name) values ('Beast Within') on conflict do nothing;
    insert into mtgcard(name) values ('Ghastly Conscription') on conflict do nothing;
    insert into mtgcard(name) values ('Abzan Charm') on conflict do nothing;
    insert into mtgcard(name) values ('Propaganda') on conflict do nothing;
    insert into mtgcard(name) values ('Yidris, Maelstrom Wielder') on conflict do nothing;
    insert into mtgcard(name) values ('Hanna, Ship''s Navigator') on conflict do nothing;
    insert into mtgcard(name) values ('Atraxa, Praetors'' Voice') on conflict do nothing;
    insert into mtgcard(name) values ('Rampant Growth') on conflict do nothing;
    insert into mtgcard(name) values ('Sanctum Gargoyle') on conflict do nothing;
    insert into mtgcard(name) values ('Fathom Mage') on conflict do nothing;
    insert into mtgcard(name) values ('Worm Harvest') on conflict do nothing;
    insert into mtgcard(name) values ('Reins of Power') on conflict do nothing;
    insert into mtgcard(name) values ('Oblation') on conflict do nothing;
    insert into mtgcard(name) values ('Devastation Tide') on conflict do nothing;
    insert into mtgcard(name) values ('Jungle Shrine') on conflict do nothing;
    insert into mtgcard(name) values ('Armory Automaton') on conflict do nothing;
    insert into mtgcard(name) values ('Windbrisk Heights') on conflict do nothing;
    insert into mtgcard(name) values ('Seat of the Synod') on conflict do nothing;
    insert into mtgcard(name) values ('Mentor of the Meek') on conflict do nothing;
    insert into mtgcard(name) values ('Howling Mine') on conflict do nothing;
    insert into mtgcard(name) values ('Mirror Entity') on conflict do nothing;
    insert into mtgcard(name) values ('Jor Kadeen, the Prevailer') on conflict do nothing;
    insert into mtgcard(name) values ('Bane of the Living') on conflict do nothing;
    insert into mtgcard(name) values ('Murmuring Bosk') on conflict do nothing;
    insert into mtgcard(name) values ('Frenzied Fugue') on conflict do nothing;
    insert into mtgcard(name) values ('Guiltfeeder') on conflict do nothing;
    insert into mtgcard(name) values ('Fellwar Stone') on conflict do nothing;
    insert into mtgcard(name) values ('Arcane Sanctum') on conflict do nothing;
    insert into mtgcard(name) values ('Grip of Phyresis') on conflict do nothing;
    insert into mtgcard(name) values ('Quirion Explorer') on conflict do nothing;
    insert into mtgcard(name) values ('Trinket Mage') on conflict do nothing;
    insert into mtgcard(name) values ('Mirrorweave') on conflict do nothing;
    insert into mtgcard(name) values ('Bituminous Blast') on conflict do nothing;
    insert into mtgcard(name) values ('Grab the Reins') on conflict do nothing;
    insert into mtgcard(name) values ('Mosswort Bridge') on conflict do nothing;
    insert into mtgcard(name) values ('Empyrial Plate') on conflict do nothing;
    insert into mtgcard(name) values ('Thunderfoot Baloth') on conflict do nothing;
    insert into mtgcard(name) values ('Swiftfoot Boots') on conflict do nothing;
    insert into mtgcard(name) values ('Shamanic Revelation') on conflict do nothing;
    insert into mtgcard(name) values ('Elite Scaleguard') on conflict do nothing;
    insert into mtgcard(name) values ('Curtains'' Call') on conflict do nothing;
    insert into mtgcard(name) values ('Crackling Doom') on conflict do nothing;
    insert into mtgcard(name) values ('Decimate') on conflict do nothing;
    insert into mtgcard(name) values ('Boros Charm') on conflict do nothing;
    insert into mtgcard(name) values ('Brutal Hordechief') on conflict do nothing;
    insert into mtgcard(name) values ('Necroplasm') on conflict do nothing;
    insert into mtgcard(name) values ('Boompile') on conflict do nothing;
    insert into mtgcard(name) values ('Ishai, Ojutai Dragonspeaker') on conflict do nothing;
    insert into mtgcard(name) values ('Benefactor''s Draught') on conflict do nothing;
    insert into mtgcard(name) values ('Rupture Spire') on conflict do nothing;
    insert into mtgcard(name) values ('Zedruu the Greathearted') on conflict do nothing;
    insert into mtgcard(name) values ('Custodi Soulbinders') on conflict do nothing;
    insert into mtgcard(name) values ('Juniper Order Ranger') on conflict do nothing;
    insert into mtgcard(name) values ('Sunpetal Grove') on conflict do nothing;
    insert into mtgcard(name) values ('Mycosynth Wellspring') on conflict do nothing;
    insert into mtgcard(name) values ('Festercreep') on conflict do nothing;
    insert into mtgcard(name) values ('Cultivate') on conflict do nothing;
    insert into mtgcard(name) values ('Hoofprints of the Stag') on conflict do nothing;
    insert into mtgcard(name) values ('Humble Defector') on conflict do nothing;
    insert into mtgcard(name) values ('Trash for Treasure') on conflict do nothing;
    insert into mtgcard(name) values ('Ravos, Soultender') on conflict do nothing;
    insert into mtgcard(name) values ('Nevinyrral''s Disk') on conflict do nothing;
    insert into mtgcard(name) values ('Managorger Hydra') on conflict do nothing;
    insert into mtgcard(name) values ('Academy Elite') on conflict do nothing;
    insert into mtgcard(name) values ('Ludevic, Necro-Alchemist') on conflict do nothing;
    insert into mtgcard(name) values ('Glint-Eye Nephilim') on conflict do nothing;
    insert into mtgcard(name) values ('Entrapment Maneuver') on conflict do nothing;
    insert into mtgcard(name) values ('Thrummingbird') on conflict do nothing;
    insert into mtgcard(name) values ('Wheel of Fate') on conflict do nothing;
    insert into mtgcard(name) values ('Stalking Vengeance') on conflict do nothing;
    insert into mtgcard(name) values ('Runehorn Hellkite') on conflict do nothing;
    insert into mtgcard(name) values ('Lavalanche') on conflict do nothing;
    insert into mtgcard(name) values ('Naya Charm') on conflict do nothing;
    insert into mtgcard(name) values ('Caves of Koilos') on conflict do nothing;
    insert into mtgcard(name) values ('Ethersworn Adjudicator') on conflict do nothing;
    insert into mtgcard(name) values ('Sharuum the Hegemon') on conflict do nothing;
    insert into mtgcard(name) values ('Charging Cinderhorn') on conflict do nothing;
    insert into mtgcard(name) values ('Champion of Lambholt') on conflict do nothing;
    insert into mtgcard(name) values ('Myr Retriever') on conflict do nothing;
    insert into mtgcard(name) values ('Solidarity of Heroes') on conflict do nothing;
    insert into mtgcard(name) values ('Faerie Artisans') on conflict do nothing;
    insert into mtgcard(name) values ('Rakdos Charm') on conflict do nothing;
    insert into mtgcard(name) values ('Satyr Wayfinder') on conflict do nothing;
