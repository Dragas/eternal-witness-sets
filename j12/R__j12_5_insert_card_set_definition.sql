insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Noble Hierarch'),
    (select id from sets where short_name = 'j12'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Xiahou Dun, the One-Eyed'),
    (select id from sets where short_name = 'j12'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sneak Attack'),
    (select id from sets where short_name = 'j12'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flusterstorm'),
    (select id from sets where short_name = 'j12'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karmic Guide'),
    (select id from sets where short_name = 'j12'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Command Tower'),
    (select id from sets where short_name = 'j12'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword of Light and Shadow'),
    (select id from sets where short_name = 'j12'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karakas'),
    (select id from sets where short_name = 'j12'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Centaur'),
    (select id from sets where short_name = 'j12'),
    '9',
    'common'
) 
 on conflict do nothing;
