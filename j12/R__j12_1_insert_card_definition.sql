    insert into mtgcard(name) values ('Noble Hierarch') on conflict do nothing;
    insert into mtgcard(name) values ('Xiahou Dun, the One-Eyed') on conflict do nothing;
    insert into mtgcard(name) values ('Sneak Attack') on conflict do nothing;
    insert into mtgcard(name) values ('Flusterstorm') on conflict do nothing;
    insert into mtgcard(name) values ('Karmic Guide') on conflict do nothing;
    insert into mtgcard(name) values ('Command Tower') on conflict do nothing;
    insert into mtgcard(name) values ('Sword of Light and Shadow') on conflict do nothing;
    insert into mtgcard(name) values ('Karakas') on conflict do nothing;
    insert into mtgcard(name) values ('Centaur') on conflict do nothing;
