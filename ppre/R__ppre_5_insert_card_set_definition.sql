insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Helm of Kaldra'),
    (select id from sets where short_name = 'ppre'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Malfegor'),
    (select id from sets where short_name = 'ppre'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fungal Shambler'),
    (select id from sets where short_name = 'ppre'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silent Specter'),
    (select id from sets where short_name = 'ppre'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glory'),
    (select id from sets where short_name = 'ppre'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lu Bu, Master-at-Arms'),
    (select id from sets where short_name = 'ppre'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'False Prophet'),
    (select id from sets where short_name = 'ppre'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lotus Bloom'),
    (select id from sets where short_name = 'ppre'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beast of Burden'),
    (select id from sets where short_name = 'ppre'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Allosaurus Rider'),
    (select id from sets where short_name = 'ppre'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avatar of Discord'),
    (select id from sets where short_name = 'ppre'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feral Throwback'),
    (select id from sets where short_name = 'ppre'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dirtcowl Wurm'),
    (select id from sets where short_name = 'ppre'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rathi Assassin'),
    (select id from sets where short_name = 'ppre'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lu Bu, Master-at-Arms'),
    (select id from sets where short_name = 'ppre'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Revenant'),
    (select id from sets where short_name = 'ppre'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beast of Burden'),
    (select id from sets where short_name = 'ppre'),
    '5†',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Monstrous Hound'),
    (select id from sets where short_name = 'ppre'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Broodmother'),
    (select id from sets where short_name = 'ppre'),
    '40',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Djinn Illuminatus'),
    (select id from sets where short_name = 'ppre'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overtaker'),
    (select id from sets where short_name = 'ppre'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overbeing of Myth'),
    (select id from sets where short_name = 'ppre'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone-Tongue Basilisk'),
    (select id from sets where short_name = 'ppre'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Korlash, Heir to Blackblade'),
    (select id from sets where short_name = 'ppre'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kiyomaro, First to Stand'),
    (select id from sets where short_name = 'ppre'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ink-Eyes, Servant of Oni'),
    (select id from sets where short_name = 'ppre'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Collector'),
    (select id from sets where short_name = 'ppre'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Questing Phelddagrif'),
    (select id from sets where short_name = 'ppre'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demigod of Revenge'),
    (select id from sets where short_name = 'ppre'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Dragon'),
    (select id from sets where short_name = 'ppre'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oros, the Avenger'),
    (select id from sets where short_name = 'ppre'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword of Kaldra'),
    (select id from sets where short_name = 'ppre'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gleancrawler'),
    (select id from sets where short_name = 'ppre'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani Vengeant'),
    (select id from sets where short_name = 'ppre'),
    '38',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Raging Kavu'),
    (select id from sets where short_name = 'ppre'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Laquatus''s Champion'),
    (select id from sets where short_name = 'ppre'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Door of Destinies'),
    (select id from sets where short_name = 'ppre'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shield of Kaldra'),
    (select id from sets where short_name = 'ppre'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wren''s Run Packmaster'),
    (select id from sets where short_name = 'ppre'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ryusei, the Falling Star'),
    (select id from sets where short_name = 'ppre'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Laquatus''s Champion'),
    (select id from sets where short_name = 'ppre'),
    '16†',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avatar of Hope'),
    (select id from sets where short_name = 'ppre'),
    '11',
    'rare'
) 
 on conflict do nothing;
