insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Mindslaver'),
    (select id from sets where short_name = 'som'),
    '176',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Auriok Replica'),
    (select id from sets where short_name = 'som'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Semblance Anvil'),
    (select id from sets where short_name = 'som'),
    '201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barbed Battlegear'),
    (select id from sets where short_name = 'som'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sword of Body and Mind'),
    (select id from sets where short_name = 'som'),
    '208',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = 'som'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wing Puncture'),
    (select id from sets where short_name = 'som'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ezuri''s Brigade'),
    (select id from sets where short_name = 'som'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exsanguinate'),
    (select id from sets where short_name = 'som'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Riddlesmith'),
    (select id from sets where short_name = 'som'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glint Hawk Idol'),
    (select id from sets where short_name = 'som'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darksteel Myr'),
    (select id from sets where short_name = 'som'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Auriok Edgewright'),
    (select id from sets where short_name = 'som'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flight Spellbomb'),
    (select id from sets where short_name = 'som'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prototype Portal'),
    (select id from sets where short_name = 'som'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trigon of Rage'),
    (select id from sets where short_name = 'som'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Argentum Armor'),
    (select id from sets where short_name = 'som'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myrsmith'),
    (select id from sets where short_name = 'som'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disperse'),
    (select id from sets where short_name = 'som'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Screeching Silcaw'),
    (select id from sets where short_name = 'som'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Untamed Might'),
    (select id from sets where short_name = 'som'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Revoke Existence'),
    (select id from sets where short_name = 'som'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Palladium Myr'),
    (select id from sets where short_name = 'som'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kemba, Kha Regent'),
    (select id from sets where short_name = 'som'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dross Hopper'),
    (select id from sets where short_name = 'som'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infiltration Lens'),
    (select id from sets where short_name = 'som'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Precursor Golem'),
    (select id from sets where short_name = 'som'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rust Tick'),
    (select id from sets where short_name = 'som'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Melt Terrain'),
    (select id from sets where short_name = 'som'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ichor Rats'),
    (select id from sets where short_name = 'som'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunspear Shikari'),
    (select id from sets where short_name = 'som'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darkslick Drake'),
    (select id from sets where short_name = 'som'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'som'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'True Conviction'),
    (select id from sets where short_name = 'som'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trigon of Mending'),
    (select id from sets where short_name = 'som'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dissipation Field'),
    (select id from sets where short_name = 'som'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trigon of Corruption'),
    (select id from sets where short_name = 'som'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Venser''s Journal'),
    (select id from sets where short_name = 'som'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'som'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Argent Sphinx'),
    (select id from sets where short_name = 'som'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glint Hawk'),
    (select id from sets where short_name = 'som'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lifesmith'),
    (select id from sets where short_name = 'som'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Copperline Gorge'),
    (select id from sets where short_name = 'som'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viridian Revel'),
    (select id from sets where short_name = 'som'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunblast Angel'),
    (select id from sets where short_name = 'som'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Copper Myr'),
    (select id from sets where short_name = 'som'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vulshok Replica'),
    (select id from sets where short_name = 'som'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ezuri''s Archers'),
    (select id from sets where short_name = 'som'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurmcoil Engine'),
    (select id from sets where short_name = 'som'),
    '223',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Razorverge Thicket'),
    (select id from sets where short_name = 'som'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'som'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loxodon Wayfarer'),
    (select id from sets where short_name = 'som'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leaden Myr'),
    (select id from sets where short_name = 'som'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golem Foundry'),
    (select id from sets where short_name = 'som'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Venser, the Sojourner'),
    (select id from sets where short_name = 'som'),
    '135',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blackcleave Cliffs'),
    (select id from sets where short_name = 'som'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'som'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'som'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liquimetal Coating'),
    (select id from sets where short_name = 'som'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'som'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arc Trail'),
    (select id from sets where short_name = 'som'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bellowing Tanglewurm'),
    (select id from sets where short_name = 'som'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Memnite'),
    (select id from sets where short_name = 'som'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'som'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'som'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moriok Replica'),
    (select id from sets where short_name = 'som'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molten-Tail Masticore'),
    (select id from sets where short_name = 'som'),
    '177',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Echo Circlet'),
    (select id from sets where short_name = 'som'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alpha Tyrranax'),
    (select id from sets where short_name = 'som'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vigil for the Lost'),
    (select id from sets where short_name = 'som'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leonin Arbiter'),
    (select id from sets where short_name = 'som'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oxidda Daredevil'),
    (select id from sets where short_name = 'som'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Perilous Myr'),
    (select id from sets where short_name = 'som'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Neurok Replica'),
    (select id from sets where short_name = 'som'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vault Skyward'),
    (select id from sets where short_name = 'som'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Copperhorn Scout'),
    (select id from sets where short_name = 'som'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necrogen Censer'),
    (select id from sets where short_name = 'som'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blistergrub'),
    (select id from sets where short_name = 'som'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Turn Aside'),
    (select id from sets where short_name = 'som'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heavy Arbalest'),
    (select id from sets where short_name = 'som'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Molten Psyche'),
    (select id from sets where short_name = 'som'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Throne of Geth'),
    (select id from sets where short_name = 'som'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fume Spitter'),
    (select id from sets where short_name = 'som'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'som'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Salvage Scout'),
    (select id from sets where short_name = 'som'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iron Myr'),
    (select id from sets where short_name = 'som'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sky-Eel School'),
    (select id from sets where short_name = 'som'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glimmerpoint Stag'),
    (select id from sets where short_name = 'som'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nihil Spellbomb'),
    (select id from sets where short_name = 'som'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Reservoir'),
    (select id from sets where short_name = 'som'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tel-Jilad Fallen'),
    (select id from sets where short_name = 'som'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golem''s Heart'),
    (select id from sets where short_name = 'som'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Saberclaw Golem'),
    (select id from sets where short_name = 'som'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tempered Steel'),
    (select id from sets where short_name = 'som'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Turn to Slag'),
    (select id from sets where short_name = 'som'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kuldotha Phoenix'),
    (select id from sets where short_name = 'som'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mox Opal'),
    (select id from sets where short_name = 'som'),
    '179',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Seize the Initiative'),
    (select id from sets where short_name = 'som'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'som'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blunt the Assault'),
    (select id from sets where short_name = 'som'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Gaveleer'),
    (select id from sets where short_name = 'som'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steady Progress'),
    (select id from sets where short_name = 'som'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mimic Vat'),
    (select id from sets where short_name = 'som'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darksteel Sentinel'),
    (select id from sets where short_name = 'som'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cerebral Eruption'),
    (select id from sets where short_name = 'som'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Tanglecord'),
    (select id from sets where short_name = 'som'),
    '222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'som'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dispense Justice'),
    (select id from sets where short_name = 'som'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soliton'),
    (select id from sets where short_name = 'som'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvok Replica'),
    (select id from sets where short_name = 'som'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golem Artisan'),
    (select id from sets where short_name = 'som'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necrogen Scudder'),
    (select id from sets where short_name = 'som'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kuldotha Rebirth'),
    (select id from sets where short_name = 'som'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Contagion Clasp'),
    (select id from sets where short_name = 'som'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Halt Order'),
    (select id from sets where short_name = 'som'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vector Asp'),
    (select id from sets where short_name = 'som'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Instill Infection'),
    (select id from sets where short_name = 'som'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Propagator'),
    (select id from sets where short_name = 'som'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flameborn Hellion'),
    (select id from sets where short_name = 'som'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tower of Calamities'),
    (select id from sets where short_name = 'som'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Engulfing Slagwurm'),
    (select id from sets where short_name = 'som'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necrotic Ooze'),
    (select id from sets where short_name = 'som'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glimmerpost'),
    (select id from sets where short_name = 'som'),
    '227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scrapdiver Serpent'),
    (select id from sets where short_name = 'som'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necropede'),
    (select id from sets where short_name = 'som'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ferrovore'),
    (select id from sets where short_name = 'som'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strata Scythe'),
    (select id from sets where short_name = 'som'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inexorable Tide'),
    (select id from sets where short_name = 'som'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Origin Spellbomb'),
    (select id from sets where short_name = 'som'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hand of the Praetors'),
    (select id from sets where short_name = 'som'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blackcleave Goblin'),
    (select id from sets where short_name = 'som'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lux Cannon'),
    (select id from sets where short_name = 'som'),
    '173',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Acid Web Spider'),
    (select id from sets where short_name = 'som'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plated Seastrider'),
    (select id from sets where short_name = 'som'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seachrome Coast'),
    (select id from sets where short_name = 'som'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grafted Exoskeleton'),
    (select id from sets where short_name = 'som'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Assault Strobe'),
    (select id from sets where short_name = 'som'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corpse Cur'),
    (select id from sets where short_name = 'som'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flesh Allergy'),
    (select id from sets where short_name = 'som'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vulshok Heartstoker'),
    (select id from sets where short_name = 'som'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bleak Coven Vampires'),
    (select id from sets where short_name = 'som'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strider Harness'),
    (select id from sets where short_name = 'som'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horizon Spellbomb'),
    (select id from sets where short_name = 'som'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abuna Acolyte'),
    (select id from sets where short_name = 'som'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Furnace Celebration'),
    (select id from sets where short_name = 'som'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'som'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Indomitable Archangel'),
    (select id from sets where short_name = 'som'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'som'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grindclock'),
    (select id from sets where short_name = 'som'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Accorder''s Shield'),
    (select id from sets where short_name = 'som'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whitesun''s Passage'),
    (select id from sets where short_name = 'som'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Twisted Image'),
    (select id from sets where short_name = 'som'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volition Reins'),
    (select id from sets where short_name = 'som'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plague Stinger'),
    (select id from sets where short_name = 'som'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Etched Champion'),
    (select id from sets where short_name = 'som'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvok Lifestaff'),
    (select id from sets where short_name = 'som'),
    '209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gold Myr'),
    (select id from sets where short_name = 'som'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Memoricide'),
    (select id from sets where short_name = 'som'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blade-Tribe Berserkers'),
    (select id from sets where short_name = 'som'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kuldotha Forgemaster'),
    (select id from sets where short_name = 'som'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'som'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Koth of the Hammer'),
    (select id from sets where short_name = 'som'),
    '94',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Withstand Death'),
    (select id from sets where short_name = 'som'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Razorfield Thresher'),
    (select id from sets where short_name = 'som'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Battlesphere'),
    (select id from sets where short_name = 'som'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steel Hellkite'),
    (select id from sets where short_name = 'som'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Putrefax'),
    (select id from sets where short_name = 'som'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shape Anew'),
    (select id from sets where short_name = 'som'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corrupted Harvester'),
    (select id from sets where short_name = 'som'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'som'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tangle Angler'),
    (select id from sets where short_name = 'som'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cystbearer'),
    (select id from sets where short_name = 'som'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Platinum Emperion'),
    (select id from sets where short_name = 'som'),
    '193',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Quicksilver Gargantuan'),
    (select id from sets where short_name = 'som'),
    '39',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Painsmith'),
    (select id from sets where short_name = 'som'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darksteel Axe'),
    (select id from sets where short_name = 'som'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kemba''s Skyguard'),
    (select id from sets where short_name = 'som'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Culling Dais'),
    (select id from sets where short_name = 'som'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Livewire Lash'),
    (select id from sets where short_name = 'som'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carrion Call'),
    (select id from sets where short_name = 'som'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chimeric Mass'),
    (select id from sets where short_name = 'som'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skinrender'),
    (select id from sets where short_name = 'som'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trigon of Infestation'),
    (select id from sets where short_name = 'som'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bladed Pinions'),
    (select id from sets where short_name = 'som'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bonds of Quicksilver'),
    (select id from sets where short_name = 'som'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carnifex Demon'),
    (select id from sets where short_name = 'som'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodshot Trainee'),
    (select id from sets where short_name = 'som'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tunnel Ignus'),
    (select id from sets where short_name = 'som'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fulgent Distraction'),
    (select id from sets where short_name = 'som'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ratchet Bomb'),
    (select id from sets where short_name = 'som'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grand Architect'),
    (select id from sets where short_name = 'som'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oxidda Scrapmelter'),
    (select id from sets where short_name = 'som'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Genesis Wave'),
    (select id from sets where short_name = 'som'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skithiryx, the Blight Dragon'),
    (select id from sets where short_name = 'som'),
    '79',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scoria Elemental'),
    (select id from sets where short_name = 'som'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relic Putrescence'),
    (select id from sets where short_name = 'som'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trinket Mage'),
    (select id from sets where short_name = 'som'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clone Shell'),
    (select id from sets where short_name = 'som'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grasp of Darkness'),
    (select id from sets where short_name = 'som'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chrome Steed'),
    (select id from sets where short_name = 'som'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blight Mamba'),
    (select id from sets where short_name = 'som'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tainted Strike'),
    (select id from sets where short_name = 'som'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molder Beast'),
    (select id from sets where short_name = 'som'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rusted Relic'),
    (select id from sets where short_name = 'som'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'som'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrummingbird'),
    (select id from sets where short_name = 'som'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Contagion Engine'),
    (select id from sets where short_name = 'som'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slice in Twain'),
    (select id from sets where short_name = 'som'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barrage Ogre'),
    (select id from sets where short_name = 'som'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'som'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Parry'),
    (select id from sets where short_name = 'som'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Auriok Sunchaser'),
    (select id from sets where short_name = 'som'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moriok Reaver'),
    (select id from sets where short_name = 'som'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'som'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darkslick Shores'),
    (select id from sets where short_name = 'som'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myr Galvanizer'),
    (select id from sets where short_name = 'som'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tumble Magnet'),
    (select id from sets where short_name = 'som'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Contagious Nim'),
    (select id from sets where short_name = 'som'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Neurok Invisimancer'),
    (select id from sets where short_name = 'som'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ezuri, Renegade Leader'),
    (select id from sets where short_name = 'som'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carapace Forger'),
    (select id from sets where short_name = 'som'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Panic Spellbomb'),
    (select id from sets where short_name = 'som'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elspeth Tirel'),
    (select id from sets where short_name = 'som'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Asceticism'),
    (select id from sets where short_name = 'som'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golden Urn'),
    (select id from sets where short_name = 'som'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darksteel Juggernaut'),
    (select id from sets where short_name = 'som'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'som'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Embersmith'),
    (select id from sets where short_name = 'som'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tel-Jilad Defiance'),
    (select id from sets where short_name = 'som'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghalma''s Warden'),
    (select id from sets where short_name = 'som'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ogre Geargrabber'),
    (select id from sets where short_name = 'som'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stoic Rebuttal'),
    (select id from sets where short_name = 'som'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lumengrid Drake'),
    (select id from sets where short_name = 'som'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arrest'),
    (select id from sets where short_name = 'som'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Galvanic Blast'),
    (select id from sets where short_name = 'som'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nim Deathmantle'),
    (select id from sets where short_name = 'som'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liege of the Tangle'),
    (select id from sets where short_name = 'som'),
    '123',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Trigon of Thought'),
    (select id from sets where short_name = 'som'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spikeshot Elder'),
    (select id from sets where short_name = 'som'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ichorclaw Myr'),
    (select id from sets where short_name = 'som'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Razor Hippogriff'),
    (select id from sets where short_name = 'som'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snapsail Glider'),
    (select id from sets where short_name = 'som'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hoard-Smelter Dragon'),
    (select id from sets where short_name = 'som'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'som'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychic Miasma'),
    (select id from sets where short_name = 'som'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silver Myr'),
    (select id from sets where short_name = 'som'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Painful Quandary'),
    (select id from sets where short_name = 'som'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Geth, Lord of the Vault'),
    (select id from sets where short_name = 'som'),
    '64',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vedalken Certarch'),
    (select id from sets where short_name = 'som'),
    '52',
    'common'
) 
 on conflict do nothing;
