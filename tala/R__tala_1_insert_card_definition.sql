    insert into mtgcard(name) values ('Goblin') on conflict do nothing;
    insert into mtgcard(name) values ('Homunculus') on conflict do nothing;
    insert into mtgcard(name) values ('Ooze') on conflict do nothing;
    insert into mtgcard(name) values ('Thopter') on conflict do nothing;
    insert into mtgcard(name) values ('Saproling') on conflict do nothing;
    insert into mtgcard(name) values ('Skeleton') on conflict do nothing;
    insert into mtgcard(name) values ('Soldier') on conflict do nothing;
    insert into mtgcard(name) values ('Beast') on conflict do nothing;
    insert into mtgcard(name) values ('Zombie') on conflict do nothing;
    insert into mtgcard(name) values ('Dragon') on conflict do nothing;
