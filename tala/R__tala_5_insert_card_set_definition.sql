insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tala'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Homunculus'),
    (select id from sets where short_name = 'tala'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ooze'),
    (select id from sets where short_name = 'tala'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter'),
    (select id from sets where short_name = 'tala'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tala'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skeleton'),
    (select id from sets where short_name = 'tala'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tala'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tala'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tala'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tala'),
    '6',
    'common'
) 
 on conflict do nothing;
