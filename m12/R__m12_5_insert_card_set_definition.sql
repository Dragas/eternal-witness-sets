insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Drifting Shade'),
    (select id from sets where short_name = 'm12'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'm12'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Peregrine Griffin'),
    (select id from sets where short_name = 'm12'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quicksilver Amulet'),
    (select id from sets where short_name = 'm12'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Bear'),
    (select id from sets where short_name = 'm12'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doom Blade'),
    (select id from sets where short_name = 'm12'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Piker'),
    (select id from sets where short_name = 'm12'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arbalest Elite'),
    (select id from sets where short_name = 'm12'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Benalish Veteran'),
    (select id from sets where short_name = 'm12'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stampeding Rhino'),
    (select id from sets where short_name = 'm12'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunpetal Grove'),
    (select id from sets where short_name = 'm12'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon''s Claw'),
    (select id from sets where short_name = 'm12'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rites of Flourishing'),
    (select id from sets where short_name = 'm12'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Act of Treason'),
    (select id from sets where short_name = 'm12'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crimson Mage'),
    (select id from sets where short_name = 'm12'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gorehorn Minotaurs'),
    (select id from sets where short_name = 'm12'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'm12'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divination'),
    (select id from sets where short_name = 'm12'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodlord of Vaasgoth'),
    (select id from sets where short_name = 'm12'),
    '82',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vampire Outcasts'),
    (select id from sets where short_name = 'm12'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vengeful Pharaoh'),
    (select id from sets where short_name = 'm12'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flight'),
    (select id from sets where short_name = 'm12'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frost Breath'),
    (select id from sets where short_name = 'm12'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manalith'),
    (select id from sets where short_name = 'm12'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Goliath'),
    (select id from sets where short_name = 'm12'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kite Shield'),
    (select id from sets where short_name = 'm12'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Assault Griffin'),
    (select id from sets where short_name = 'm12'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm12'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = 'm12'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Outrage'),
    (select id from sets where short_name = 'm12'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm12'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gideon Jura'),
    (select id from sets where short_name = 'm12'),
    '16',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gideon''s Lawkeeper'),
    (select id from sets where short_name = 'm12'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Day of Judgment'),
    (select id from sets where short_name = 'm12'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sorin''s Thirst'),
    (select id from sets where short_name = 'm12'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divine Favor'),
    (select id from sets where short_name = 'm12'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk Mesmerist'),
    (select id from sets where short_name = 'm12'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arachnus Spinner'),
    (select id from sets where short_name = 'm12'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Solemn Simulacrum'),
    (select id from sets where short_name = 'm12'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Chieftain'),
    (select id from sets where short_name = 'm12'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hideous Visage'),
    (select id from sets where short_name = 'm12'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gladecover Scout'),
    (select id from sets where short_name = 'm12'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rootbound Crag'),
    (select id from sets where short_name = 'm12'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Taste of Blood'),
    (select id from sets where short_name = 'm12'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scrambleverse'),
    (select id from sets where short_name = 'm12'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sorin''s Vengeance'),
    (select id from sets where short_name = 'm12'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lava Axe'),
    (select id from sets where short_name = 'm12'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time Reversal'),
    (select id from sets where short_name = 'm12'),
    '77',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Diabolic Tutor'),
    (select id from sets where short_name = 'm12'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = 'm12'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trollhide'),
    (select id from sets where short_name = 'm12'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jade Mage'),
    (select id from sets where short_name = 'm12'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frost Titan'),
    (select id from sets where short_name = 'm12'),
    '55',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sundial of the Infinite'),
    (select id from sets where short_name = 'm12'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Grenade'),
    (select id from sets where short_name = 'm12'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel''s Feather'),
    (select id from sets where short_name = 'm12'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drowned Catacomb'),
    (select id from sets where short_name = 'm12'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Siege Mastodon'),
    (select id from sets where short_name = 'm12'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fling'),
    (select id from sets where short_name = 'm12'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stingerfling Spider'),
    (select id from sets where short_name = 'm12'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stave Off'),
    (select id from sets where short_name = 'm12'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Fireslinger'),
    (select id from sets where short_name = 'm12'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'm12'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Griffin Sentinel'),
    (select id from sets where short_name = 'm12'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primordial Hydra'),
    (select id from sets where short_name = 'm12'),
    '189',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Griffin Rider'),
    (select id from sets where short_name = 'm12'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon''s Horn'),
    (select id from sets where short_name = 'm12'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Flame'),
    (select id from sets where short_name = 'm12'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mighty Leap'),
    (select id from sets where short_name = 'm12'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harbor Serpent'),
    (select id from sets where short_name = 'm12'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duskhunter Bat'),
    (select id from sets where short_name = 'm12'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = 'm12'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace''s Archivist'),
    (select id from sets where short_name = 'm12'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kraken''s Eye'),
    (select id from sets where short_name = 'm12'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = 'm12'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lurking Crocodile'),
    (select id from sets where short_name = 'm12'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aegis Angel'),
    (select id from sets where short_name = 'm12'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm12'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel''s Mercy'),
    (select id from sets where short_name = 'm12'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rune-Scarred Demon'),
    (select id from sets where short_name = 'm12'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armored Warhorse'),
    (select id from sets where short_name = 'm12'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plummet'),
    (select id from sets where short_name = 'm12'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scepter of Empires'),
    (select id from sets where short_name = 'm12'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volcanic Dragon'),
    (select id from sets where short_name = 'm12'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Garruk''s Horde'),
    (select id from sets where short_name = 'm12'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cemetery Reaper'),
    (select id from sets where short_name = 'm12'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Personal Sanctuary'),
    (select id from sets where short_name = 'm12'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aven Fleetwing'),
    (select id from sets where short_name = 'm12'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm12'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm12'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grave Titan'),
    (select id from sets where short_name = 'm12'),
    '98',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Call to the Grave'),
    (select id from sets where short_name = 'm12'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chasm Drake'),
    (select id from sets where short_name = 'm12'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skywinder Drake'),
    (select id from sets where short_name = 'm12'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Distress'),
    (select id from sets where short_name = 'm12'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greatsword'),
    (select id from sets where short_name = 'm12'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Levitation'),
    (select id from sets where short_name = 'm12'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Honor of the Pure'),
    (select id from sets where short_name = 'm12'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm12'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slaughter Cry'),
    (select id from sets where short_name = 'm12'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wring Flesh'),
    (select id from sets where short_name = 'm12'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grand Abolisher'),
    (select id from sets where short_name = 'm12'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mesa Enchantress'),
    (select id from sets where short_name = 'm12'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Timely Reinforcements'),
    (select id from sets where short_name = 'm12'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Monomania'),
    (select id from sets where short_name = 'm12'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Leak'),
    (select id from sets where short_name = 'm12'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runeclaw Bear'),
    (select id from sets where short_name = 'm12'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Firebreathing'),
    (select id from sets where short_name = 'm12'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reverberate'),
    (select id from sets where short_name = 'm12'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Devouring Swarm'),
    (select id from sets where short_name = 'm12'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Djinn of Wishes'),
    (select id from sets where short_name = 'm12'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Visions of Beyond'),
    (select id from sets where short_name = 'm12'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reassembling Skeleton'),
    (select id from sets where short_name = 'm12'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Celestial Purge'),
    (select id from sets where short_name = 'm12'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Torches'),
    (select id from sets where short_name = 'm12'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Master Thief'),
    (select id from sets where short_name = 'm12'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'm12'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reclaim'),
    (select id from sets where short_name = 'm12'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Adaptive Automaton'),
    (select id from sets where short_name = 'm12'),
    '201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiery Hellhound'),
    (select id from sets where short_name = 'm12'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'm12'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Seeker'),
    (select id from sets where short_name = 'm12'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bountiful Harvest'),
    (select id from sets where short_name = 'm12'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm12'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ponder'),
    (select id from sets where short_name = 'm12'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk, Primal Hunter'),
    (select id from sets where short_name = 'm12'),
    '174',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sutured Ghoul'),
    (select id from sets where short_name = 'm12'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fog'),
    (select id from sets where short_name = 'm12'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Throne of Empires'),
    (select id from sets where short_name = 'm12'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = 'm12'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glacial Fortress'),
    (select id from sets where short_name = 'm12'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swiftfoot Boots'),
    (select id from sets where short_name = 'm12'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demystify'),
    (select id from sets where short_name = 'm12'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Roc Egg'),
    (select id from sets where short_name = 'm12'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spirit Mantle'),
    (select id from sets where short_name = 'm12'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smallpox'),
    (select id from sets where short_name = 'm12'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warpath Ghoul'),
    (select id from sets where short_name = 'm12'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace''s Erasure'),
    (select id from sets where short_name = 'm12'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manic Vandal'),
    (select id from sets where short_name = 'm12'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Image'),
    (select id from sets where short_name = 'm12'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Greater Basilisk'),
    (select id from sets where short_name = 'm12'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Archdruid'),
    (select id from sets where short_name = 'm12'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm12'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arachnus Web'),
    (select id from sets where short_name = 'm12'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oblivion Ring'),
    (select id from sets where short_name = 'm12'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodrage Vampire'),
    (select id from sets where short_name = 'm12'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Destiny'),
    (select id from sets where short_name = 'm12'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dark Favor'),
    (select id from sets where short_name = 'm12'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx of Uthuun'),
    (select id from sets where short_name = 'm12'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Dragon'),
    (select id from sets where short_name = 'm12'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azure Mage'),
    (select id from sets where short_name = 'm12'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ice Cage'),
    (select id from sets where short_name = 'm12'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alluring Siren'),
    (select id from sets where short_name = 'm12'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm12'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lifelink'),
    (select id from sets where short_name = 'm12'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk Looter'),
    (select id from sets where short_name = 'm12'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manabarbs'),
    (select id from sets where short_name = 'm12'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overrun'),
    (select id from sets where short_name = 'm12'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Acidic Slime'),
    (select id from sets where short_name = 'm12'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lord of the Unreal'),
    (select id from sets where short_name = 'm12'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crumbling Colossus'),
    (select id from sets where short_name = 'm12'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deathmark'),
    (select id from sets where short_name = 'm12'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warstorm Surge'),
    (select id from sets where short_name = 'm12'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm12'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Turn to Frog'),
    (select id from sets where short_name = 'm12'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Bangchuckers'),
    (select id from sets where short_name = 'm12'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Furyborn Hellkite'),
    (select id from sets where short_name = 'm12'),
    '135',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm12'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Auramancer'),
    (select id from sets where short_name = 'm12'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Ogre'),
    (select id from sets where short_name = 'm12'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dungrove Elder'),
    (select id from sets where short_name = 'm12'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Garruk''s Companion'),
    (select id from sets where short_name = 'm12'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'm12'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Autumn''s Veil'),
    (select id from sets where short_name = 'm12'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm12'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gideon''s Avenger'),
    (select id from sets where short_name = 'm12'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm12'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormfront Pegasus'),
    (select id from sets where short_name = 'm12'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flameblast Dragon'),
    (select id from sets where short_name = 'm12'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carnage Wurm'),
    (select id from sets where short_name = 'm12'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm12'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Arsonist'),
    (select id from sets where short_name = 'm12'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm12'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stonehorn Dignitary'),
    (select id from sets where short_name = 'm12'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flashfreeze'),
    (select id from sets where short_name = 'm12'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cancel'),
    (select id from sets where short_name = 'm12'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Tunneler'),
    (select id from sets where short_name = 'm12'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Child of Night'),
    (select id from sets where short_name = 'm12'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elixir of Immortality'),
    (select id from sets where short_name = 'm12'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zombie Infestation'),
    (select id from sets where short_name = 'm12'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pride Guardian'),
    (select id from sets where short_name = 'm12'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lure'),
    (select id from sets where short_name = 'm12'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coral Merfolk'),
    (select id from sets where short_name = 'm12'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disentomb'),
    (select id from sets where short_name = 'm12'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rusted Sentinel'),
    (select id from sets where short_name = 'm12'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bonebreaker Giant'),
    (select id from sets where short_name = 'm12'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vastwood Gorger'),
    (select id from sets where short_name = 'm12'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm12'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Royal Assassin'),
    (select id from sets where short_name = 'm12'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tormented Soul'),
    (select id from sets where short_name = 'm12'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crown of Empires'),
    (select id from sets where short_name = 'm12'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skinshifter'),
    (select id from sets where short_name = 'm12'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Belltower Sphinx'),
    (select id from sets where short_name = 'm12'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grim Lavamancer'),
    (select id from sets where short_name = 'm12'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wurm''s Tooth'),
    (select id from sets where short_name = 'm12'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Titanic Growth'),
    (select id from sets where short_name = 'm12'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Worldslayer'),
    (select id from sets where short_name = 'm12'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brink of Disaster'),
    (select id from sets where short_name = 'm12'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archon of Justice'),
    (select id from sets where short_name = 'm12'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inferno Titan'),
    (select id from sets where short_name = 'm12'),
    '147',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = 'm12'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Elemental'),
    (select id from sets where short_name = 'm12'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cudgel Troll'),
    (select id from sets where short_name = 'm12'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sorin Markov'),
    (select id from sets where short_name = 'm12'),
    '109',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hunter''s Insight'),
    (select id from sets where short_name = 'm12'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = 'm12'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guardians'' Pledge'),
    (select id from sets where short_name = 'm12'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Phoenix'),
    (select id from sets where short_name = 'm12'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm12'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Buried Ruin'),
    (select id from sets where short_name = 'm12'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tectonic Rift'),
    (select id from sets where short_name = 'm12'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sun Titan'),
    (select id from sets where short_name = 'm12'),
    '39',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Onyx Mage'),
    (select id from sets where short_name = 'm12'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pentavus'),
    (select id from sets where short_name = 'm12'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Control'),
    (select id from sets where short_name = 'm12'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Consume Spirit'),
    (select id from sets where short_name = 'm12'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chandra, the Firebrand'),
    (select id from sets where short_name = 'm12'),
    '124',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Redirect'),
    (select id from sets where short_name = 'm12'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thran Golem'),
    (select id from sets where short_name = 'm12'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elite Vanguard'),
    (select id from sets where short_name = 'm12'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Combust'),
    (select id from sets where short_name = 'm12'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jace, Memory Adept'),
    (select id from sets where short_name = 'm12'),
    '58',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm12'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm12'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'm12'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doubling Chant'),
    (select id from sets where short_name = 'm12'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm12'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alabaster Mage'),
    (select id from sets where short_name = 'm12'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragonskull Summit'),
    (select id from sets where short_name = 'm12'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aether Adept'),
    (select id from sets where short_name = 'm12'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brindle Boar'),
    (select id from sets where short_name = 'm12'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormblood Berserker'),
    (select id from sets where short_name = 'm12'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Amphin Cutthroat'),
    (select id from sets where short_name = 'm12'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primeval Titan'),
    (select id from sets where short_name = 'm12'),
    '188',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mind Unbound'),
    (select id from sets where short_name = 'm12'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Druidic Satchel'),
    (select id from sets where short_name = 'm12'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin War Paint'),
    (select id from sets where short_name = 'm12'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = 'm12'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sacred Wolf'),
    (select id from sets where short_name = 'm12'),
    '194',
    'common'
) 
 on conflict do nothing;
