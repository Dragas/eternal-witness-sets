insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Glorious Anthem'),
    (select id from sets where short_name = 'pjjt'),
    '1N08',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Two-Headed Dragon'),
    (select id from sets where short_name = 'pjjt'),
    '2N04',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'pjjt'),
    '1N04',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soltari Priest'),
    (select id from sets where short_name = 'pjjt'),
    '1N07',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Champion'),
    (select id from sets where short_name = 'pjjt'),
    '2N08',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slith Firewalker'),
    (select id from sets where short_name = 'pjjt'),
    '1N05',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mad Auntie'),
    (select id from sets where short_name = 'pjjt'),
    '3N08',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Elder'),
    (select id from sets where short_name = 'pjjt'),
    '1N06',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Royal Assassin'),
    (select id from sets where short_name = 'pjjt'),
    '2N05',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whirling Dervish'),
    (select id from sets where short_name = 'pjjt'),
    '2N07',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shard Phoenix'),
    (select id from sets where short_name = 'pjjt'),
    '2N06',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Hammer'),
    (select id from sets where short_name = 'pjjt'),
    '1N03',
    'rare'
) 
 on conflict do nothing;
