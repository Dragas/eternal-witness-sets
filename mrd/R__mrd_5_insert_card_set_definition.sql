insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Solemn Simulacrum'),
    (select id from sets where short_name = 'mrd'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bottle Gnomes'),
    (select id from sets where short_name = 'mrd'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slith Ascendant'),
    (select id from sets where short_name = 'mrd'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clockwork Condor'),
    (select id from sets where short_name = 'mrd'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tooth and Nail'),
    (select id from sets where short_name = 'mrd'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leveler'),
    (select id from sets where short_name = 'mrd'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shrapnel Blast'),
    (select id from sets where short_name = 'mrd'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Broodstar'),
    (select id from sets where short_name = 'mrd'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vulshok Battlemaster'),
    (select id from sets where short_name = 'mrd'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind''s Eye'),
    (select id from sets where short_name = 'mrd'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Iron Myr'),
    (select id from sets where short_name = 'mrd'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necrogen Spellbomb'),
    (select id from sets where short_name = 'mrd'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chimney Imp'),
    (select id from sets where short_name = 'mrd'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lifespark Spellbomb'),
    (select id from sets where short_name = 'mrd'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Adapter'),
    (select id from sets where short_name = 'mrd'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tower of Eons'),
    (select id from sets where short_name = 'mrd'),
    '266',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moriok Scavenger'),
    (select id from sets where short_name = 'mrd'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Neurok Hoversail'),
    (select id from sets where short_name = 'mrd'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thirst for Knowledge'),
    (select id from sets where short_name = 'mrd'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blinkmoth Urn'),
    (select id from sets where short_name = 'mrd'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Foundry'),
    (select id from sets where short_name = 'mrd'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serum Tank'),
    (select id from sets where short_name = 'mrd'),
    '240',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'mrd'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scale of Chiss-Goria'),
    (select id from sets where short_name = 'mrd'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vulshok Battlegear'),
    (select id from sets where short_name = 'mrd'),
    '272',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mass Hysteria'),
    (select id from sets where short_name = 'mrd'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scythe of the Wretched'),
    (select id from sets where short_name = 'mrd'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spikeshot Goblin'),
    (select id from sets where short_name = 'mrd'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vulshok Gauntlets'),
    (select id from sets where short_name = 'mrd'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slith Predator'),
    (select id from sets where short_name = 'mrd'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Blood'),
    (select id from sets where short_name = 'mrd'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'mrd'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necrogen Mists'),
    (select id from sets where short_name = 'mrd'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chalice of the Void'),
    (select id from sets where short_name = 'mrd'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brown Ouphe'),
    (select id from sets where short_name = 'mrd'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dead-Iron Sledge'),
    (select id from sets where short_name = 'mrd'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dross Prowler'),
    (select id from sets where short_name = 'mrd'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'mrd'),
    '302',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin War Wagon'),
    (select id from sets where short_name = 'mrd'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slith Bloodletter'),
    (select id from sets where short_name = 'mrd'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vulshok Berserker'),
    (select id from sets where short_name = 'mrd'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Detonate'),
    (select id from sets where short_name = 'mrd'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rule of Law'),
    (select id from sets where short_name = 'mrd'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plated Slagwurm'),
    (select id from sets where short_name = 'mrd'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nuisance Engine'),
    (select id from sets where short_name = 'mrd'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leonin Skyhunter'),
    (select id from sets where short_name = 'mrd'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dross Scorpion'),
    (select id from sets where short_name = 'mrd'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forge Armor'),
    (select id from sets where short_name = 'mrd'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Great Furnace'),
    (select id from sets where short_name = 'mrd'),
    '282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pewter Golem'),
    (select id from sets where short_name = 'mrd'),
    '227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quicksilver Elemental'),
    (select id from sets where short_name = 'mrd'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mesmeric Orb'),
    (select id from sets where short_name = 'mrd'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Looming Hoverguard'),
    (select id from sets where short_name = 'mrd'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battlegrowth'),
    (select id from sets where short_name = 'mrd'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fangren Hunter'),
    (select id from sets where short_name = 'mrd'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Megatog'),
    (select id from sets where short_name = 'mrd'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloudpost'),
    (select id from sets where short_name = 'mrd'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Incubator'),
    (select id from sets where short_name = 'mrd'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hum of the Radix'),
    (select id from sets where short_name = 'mrd'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pentavus'),
    (select id from sets where short_name = 'mrd'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mindstorm Crown'),
    (select id from sets where short_name = 'mrd'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Override'),
    (select id from sets where short_name = 'mrd'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grim Reminder'),
    (select id from sets where short_name = 'mrd'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tanglebloom'),
    (select id from sets where short_name = 'mrd'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrench Mind'),
    (select id from sets where short_name = 'mrd'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'mrd'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphere of Purity'),
    (select id from sets where short_name = 'mrd'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Sun Standard'),
    (select id from sets where short_name = 'mrd'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tel-Jilad Archers'),
    (select id from sets where short_name = 'mrd'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sword of Kaldra'),
    (select id from sets where short_name = 'mrd'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sculpting Steel'),
    (select id from sets where short_name = 'mrd'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spoils of the Vault'),
    (select id from sets where short_name = 'mrd'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Worldslayer'),
    (select id from sets where short_name = 'mrd'),
    '276',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hematite Golem'),
    (select id from sets where short_name = 'mrd'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icy Manipulator'),
    (select id from sets where short_name = 'mrd'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steel Wall'),
    (select id from sets where short_name = 'mrd'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rustmouth Ogre'),
    (select id from sets where short_name = 'mrd'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Hive'),
    (select id from sets where short_name = 'mrd'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thought Prison'),
    (select id from sets where short_name = 'mrd'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scrabbling Claws'),
    (select id from sets where short_name = 'mrd'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Talisman of Progress'),
    (select id from sets where short_name = 'mrd'),
    '256',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disarm'),
    (select id from sets where short_name = 'mrd'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Welding Jar'),
    (select id from sets where short_name = 'mrd'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tower of Fortunes'),
    (select id from sets where short_name = 'mrd'),
    '267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'mrd'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lumengrid Warden'),
    (select id from sets where short_name = 'mrd'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'mrd'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Synod Sanctum'),
    (select id from sets where short_name = 'mrd'),
    '252',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bonesplitter'),
    (select id from sets where short_name = 'mrd'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Neurok Spy'),
    (select id from sets where short_name = 'mrd'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lumengrid Sentinel'),
    (select id from sets where short_name = 'mrd'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raise the Alarm'),
    (select id from sets where short_name = 'mrd'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Auriok Steelshaper'),
    (select id from sets where short_name = 'mrd'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Second Sunrise'),
    (select id from sets where short_name = 'mrd'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Psychic Membrane'),
    (select id from sets where short_name = 'mrd'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mask of Memory'),
    (select id from sets where short_name = 'mrd'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Auriok Bladewarden'),
    (select id from sets where short_name = 'mrd'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tel-Jilad Stylus'),
    (select id from sets where short_name = 'mrd'),
    '260',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wanderguard Sentry'),
    (select id from sets where short_name = 'mrd'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Neurok Familiar'),
    (select id from sets where short_name = 'mrd'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deconstruct'),
    (select id from sets where short_name = 'mrd'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'mrd'),
    '290',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leaden Myr'),
    (select id from sets where short_name = 'mrd'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Proteus Staff'),
    (select id from sets where short_name = 'mrd'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Timesifter'),
    (select id from sets where short_name = 'mrd'),
    '262',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grab the Reins'),
    (select id from sets where short_name = 'mrd'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fatespinner'),
    (select id from sets where short_name = 'mrd'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Solar Tide'),
    (select id from sets where short_name = 'mrd'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Platinum Angel'),
    (select id from sets where short_name = 'mrd'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'mrd'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fractured Loyalty'),
    (select id from sets where short_name = 'mrd'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rustspore Ram'),
    (select id from sets where short_name = 'mrd'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seat of the Synod'),
    (select id from sets where short_name = 'mrd'),
    '283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Blood'),
    (select id from sets where short_name = 'mrd'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Greaves'),
    (select id from sets where short_name = 'mrd'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Extraplanar Lens'),
    (select id from sets where short_name = 'mrd'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Replica'),
    (select id from sets where short_name = 'mrd'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disciple of the Vault'),
    (select id from sets where short_name = 'mrd'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trolls of Tel-Jilad'),
    (select id from sets where short_name = 'mrd'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lumengrid Augur'),
    (select id from sets where short_name = 'mrd'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liar''s Pendulum'),
    (select id from sets where short_name = 'mrd'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glissa Sunseeker'),
    (select id from sets where short_name = 'mrd'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Razor Barrier'),
    (select id from sets where short_name = 'mrd'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slagwurm Armor'),
    (select id from sets where short_name = 'mrd'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chrome Mox'),
    (select id from sets where short_name = 'mrd'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'mrd'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Prototype'),
    (select id from sets where short_name = 'mrd'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inertia Bubble'),
    (select id from sets where short_name = 'mrd'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vault of Whispers'),
    (select id from sets where short_name = 'mrd'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slith Firewalker'),
    (select id from sets where short_name = 'mrd'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Predator''s Strike'),
    (select id from sets where short_name = 'mrd'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Groffskithur'),
    (select id from sets where short_name = 'mrd'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tower of Murmurs'),
    (select id from sets where short_name = 'mrd'),
    '268',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'mrd'),
    '303',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Bladetrap'),
    (select id from sets where short_name = 'mrd'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mourner''s Shield'),
    (select id from sets where short_name = 'mrd'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krark''s Thumb'),
    (select id from sets where short_name = 'mrd'),
    '190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nim Lasher'),
    (select id from sets where short_name = 'mrd'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'mrd'),
    '306',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duskworker'),
    (select id from sets where short_name = 'mrd'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Irradiate'),
    (select id from sets where short_name = 'mrd'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clockwork Vorrac'),
    (select id from sets where short_name = 'mrd'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightmare Lash'),
    (select id from sets where short_name = 'mrd'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dream''s Grip'),
    (select id from sets where short_name = 'mrd'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Copperhoof Vorrac'),
    (select id from sets where short_name = 'mrd'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Somber Hoverguard'),
    (select id from sets where short_name = 'mrd'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = 'mrd'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slith Strider'),
    (select id from sets where short_name = 'mrd'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Isochron Scepter'),
    (select id from sets where short_name = 'mrd'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tangleroot'),
    (select id from sets where short_name = 'mrd'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Taj-Nar Swordsmith'),
    (select id from sets where short_name = 'mrd'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leonin Scimitar'),
    (select id from sets where short_name = 'mrd'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vedalken Archmage'),
    (select id from sets where short_name = 'mrd'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Triskelion'),
    (select id from sets where short_name = 'mrd'),
    '269',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Annul'),
    (select id from sets where short_name = 'mrd'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireshrieker'),
    (select id from sets where short_name = 'mrd'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Titanium Golem'),
    (select id from sets where short_name = 'mrd'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seething Song'),
    (select id from sets where short_name = 'mrd'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viridian Shaman'),
    (select id from sets where short_name = 'mrd'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Galvanic Key'),
    (select id from sets where short_name = 'mrd'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yotian Soldier'),
    (select id from sets where short_name = 'mrd'),
    '277',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'mrd'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tree of Tales'),
    (select id from sets where short_name = 'mrd'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rust Elemental'),
    (select id from sets where short_name = 'mrd'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ornithopter'),
    (select id from sets where short_name = 'mrd'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krark-Clan Shaman'),
    (select id from sets where short_name = 'mrd'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyrite Spellbomb'),
    (select id from sets where short_name = 'mrd'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Farsight Mask'),
    (select id from sets where short_name = 'mrd'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jinxed Choker'),
    (select id from sets where short_name = 'mrd'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mindslaver'),
    (select id from sets where short_name = 'mrd'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Talisman of Dominance'),
    (select id from sets where short_name = 'mrd'),
    '253',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'mrd'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Luminous Angel'),
    (select id from sets where short_name = 'mrd'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vermiculos'),
    (select id from sets where short_name = 'mrd'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'March of the Machines'),
    (select id from sets where short_name = 'mrd'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sun Droplet'),
    (select id from sets where short_name = 'mrd'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bosh, Iron Golem'),
    (select id from sets where short_name = 'mrd'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loxodon Peacekeeper'),
    (select id from sets where short_name = 'mrd'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relic Bane'),
    (select id from sets where short_name = 'mrd'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'War Elemental'),
    (select id from sets where short_name = 'mrd'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Confusion in the Ranks'),
    (select id from sets where short_name = 'mrd'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'mrd'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Auriok Transfixer'),
    (select id from sets where short_name = 'mrd'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talisman of Impulse'),
    (select id from sets where short_name = 'mrd'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nim Devourer'),
    (select id from sets where short_name = 'mrd'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Atog'),
    (select id from sets where short_name = 'mrd'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Contaminated Bond'),
    (select id from sets where short_name = 'mrd'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loxodon Warhammer'),
    (select id from sets where short_name = 'mrd'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gilded Lotus'),
    (select id from sets where short_name = 'mrd'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Journey of Discovery'),
    (select id from sets where short_name = 'mrd'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spellweaver Helix'),
    (select id from sets where short_name = 'mrd'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Copper Myr'),
    (select id from sets where short_name = 'mrd'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arrest'),
    (select id from sets where short_name = 'mrd'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Awe Strike'),
    (select id from sets where short_name = 'mrd'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fabricate'),
    (select id from sets where short_name = 'mrd'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Coils'),
    (select id from sets where short_name = 'mrd'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wail of the Nim'),
    (select id from sets where short_name = 'mrd'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fiery Gambit'),
    (select id from sets where short_name = 'mrd'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golem-Skin Gauntlets'),
    (select id from sets where short_name = 'mrd'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Betrayal of Flesh'),
    (select id from sets where short_name = 'mrd'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pearl Shard'),
    (select id from sets where short_name = 'mrd'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shared Fate'),
    (select id from sets where short_name = 'mrd'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'mrd'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dross Harvester'),
    (select id from sets where short_name = 'mrd'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Power Conduit'),
    (select id from sets where short_name = 'mrd'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heartwood Shard'),
    (select id from sets where short_name = 'mrd'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vorrac Battlehorns'),
    (select id from sets where short_name = 'mrd'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crystal Shard'),
    (select id from sets where short_name = 'mrd'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quicksilver Fountain'),
    (select id from sets where short_name = 'mrd'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Scrying'),
    (select id from sets where short_name = 'mrd'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viridian Joiner'),
    (select id from sets where short_name = 'mrd'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Charbelcher'),
    (select id from sets where short_name = 'mrd'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blinding Beam'),
    (select id from sets where short_name = 'mrd'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Banshee''s Blade'),
    (select id from sets where short_name = 'mrd'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silver Myr'),
    (select id from sets where short_name = 'mrd'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elf Replica'),
    (select id from sets where short_name = 'mrd'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'mrd'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clockwork Beetle'),
    (select id from sets where short_name = 'mrd'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viridian Longbow'),
    (select id from sets where short_name = 'mrd'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barter in Blood'),
    (select id from sets where short_name = 'mrd'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Domineer'),
    (select id from sets where short_name = 'mrd'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'mrd'),
    '288',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Patrol'),
    (select id from sets where short_name = 'mrd'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loxodon Mender'),
    (select id from sets where short_name = 'mrd'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gate to the Aether'),
    (select id from sets where short_name = 'mrd'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'One Dozen Eyes'),
    (select id from sets where short_name = 'mrd'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nim Shrieker'),
    (select id from sets where short_name = 'mrd'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Electrostatic Bolt'),
    (select id from sets where short_name = 'mrd'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grid Monitor'),
    (select id from sets where short_name = 'mrd'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'mrd'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Abunas'),
    (select id from sets where short_name = 'mrd'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woebearer'),
    (select id from sets where short_name = 'mrd'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stalking Stones'),
    (select id from sets where short_name = 'mrd'),
    '284',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leonin Den-Guard'),
    (select id from sets where short_name = 'mrd'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clockwork Dragon'),
    (select id from sets where short_name = 'mrd'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Den'),
    (select id from sets where short_name = 'mrd'),
    '278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Cub'),
    (select id from sets where short_name = 'mrd'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reiver Demon'),
    (select id from sets where short_name = 'mrd'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Malachite Golem'),
    (select id from sets where short_name = 'mrd'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nim Replica'),
    (select id from sets where short_name = 'mrd'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunbeam Spellbomb'),
    (select id from sets where short_name = 'mrd'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Dirigible'),
    (select id from sets where short_name = 'mrd'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arc-Slogger'),
    (select id from sets where short_name = 'mrd'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duplicant'),
    (select id from sets where short_name = 'mrd'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Needlebug'),
    (select id from sets where short_name = 'mrd'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tempest of Light'),
    (select id from sets where short_name = 'mrd'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Creeping Mold'),
    (select id from sets where short_name = 'mrd'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Talisman of Unity'),
    (select id from sets where short_name = 'mrd'),
    '257',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mirror Golem'),
    (select id from sets where short_name = 'mrd'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Promise of Power'),
    (select id from sets where short_name = 'mrd'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temporal Cascade'),
    (select id from sets where short_name = 'mrd'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tooth of Chiss-Goria'),
    (select id from sets where short_name = 'mrd'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Culling Scales'),
    (select id from sets where short_name = 'mrd'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roar of the Kha'),
    (select id from sets where short_name = 'mrd'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodscent'),
    (select id from sets where short_name = 'mrd'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Altar''s Light'),
    (select id from sets where short_name = 'mrd'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Omega Myr'),
    (select id from sets where short_name = 'mrd'),
    '223',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oblivion Stone'),
    (select id from sets where short_name = 'mrd'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'mrd'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loxodon Punisher'),
    (select id from sets where short_name = 'mrd'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Talisman of Indulgence'),
    (select id from sets where short_name = 'mrd'),
    '255',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Empyrial Plate'),
    (select id from sets where short_name = 'mrd'),
    '168',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Granite Shard'),
    (select id from sets where short_name = 'mrd'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Damping Matrix'),
    (select id from sets where short_name = 'mrd'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cathodion'),
    (select id from sets where short_name = 'mrd'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glimmervoid'),
    (select id from sets where short_name = 'mrd'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soldier Replica'),
    (select id from sets where short_name = 'mrd'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tel-Jilad Exile'),
    (select id from sets where short_name = 'mrd'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ogre Leadfoot'),
    (select id from sets where short_name = 'mrd'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blinkmoth Well'),
    (select id from sets where short_name = 'mrd'),
    '279',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nim Shambler'),
    (select id from sets where short_name = 'mrd'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wizard Replica'),
    (select id from sets where short_name = 'mrd'),
    '275',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychogenic Probe'),
    (select id from sets where short_name = 'mrd'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Regress'),
    (select id from sets where short_name = 'mrd'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Elder'),
    (select id from sets where short_name = 'mrd'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'mrd'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fists of the Anvil'),
    (select id from sets where short_name = 'mrd'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Altar of Shadows'),
    (select id from sets where short_name = 'mrd'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trash for Treasure'),
    (select id from sets where short_name = 'mrd'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cobalt Golem'),
    (select id from sets where short_name = 'mrd'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Incite War'),
    (select id from sets where short_name = 'mrd'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molder Slug'),
    (select id from sets where short_name = 'mrd'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myr Retriever'),
    (select id from sets where short_name = 'mrd'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Myr Enforcer'),
    (select id from sets where short_name = 'mrd'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tel-Jilad Chosen'),
    (select id from sets where short_name = 'mrd'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skeleton Shard'),
    (select id from sets where short_name = 'mrd'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thoughtcast'),
    (select id from sets where short_name = 'mrd'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Troll Ascetic'),
    (select id from sets where short_name = 'mrd'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wurmskin Forger'),
    (select id from sets where short_name = 'mrd'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chromatic Sphere'),
    (select id from sets where short_name = 'mrd'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Consume Spirit'),
    (select id from sets where short_name = 'mrd'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Spellbomb'),
    (select id from sets where short_name = 'mrd'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alpha Myr'),
    (select id from sets where short_name = 'mrd'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = 'mrd'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tower of Champions'),
    (select id from sets where short_name = 'mrd'),
    '265',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Striker'),
    (select id from sets where short_name = 'mrd'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Nova'),
    (select id from sets where short_name = 'mrd'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Molten Rain'),
    (select id from sets where short_name = 'mrd'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Mindservant'),
    (select id from sets where short_name = 'mrd'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gold Myr'),
    (select id from sets where short_name = 'mrd'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lodestone Myr'),
    (select id from sets where short_name = 'mrd'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Assert Authority'),
    (select id from sets where short_name = 'mrd'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Turn to Dust'),
    (select id from sets where short_name = 'mrd'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flayed Nim'),
    (select id from sets where short_name = 'mrd'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frogmite'),
    (select id from sets where short_name = 'mrd'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krark-Clan Grunt'),
    (select id from sets where short_name = 'mrd'),
    '97',
    'common'
) 
 on conflict do nothing;
