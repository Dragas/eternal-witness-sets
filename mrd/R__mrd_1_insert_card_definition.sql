    insert into mtgcard(name) values ('Solemn Simulacrum') on conflict do nothing;
    insert into mtgcard(name) values ('Bottle Gnomes') on conflict do nothing;
    insert into mtgcard(name) values ('Slith Ascendant') on conflict do nothing;
    insert into mtgcard(name) values ('Clockwork Condor') on conflict do nothing;
    insert into mtgcard(name) values ('Tooth and Nail') on conflict do nothing;
    insert into mtgcard(name) values ('Leveler') on conflict do nothing;
    insert into mtgcard(name) values ('Shrapnel Blast') on conflict do nothing;
    insert into mtgcard(name) values ('Broodstar') on conflict do nothing;
    insert into mtgcard(name) values ('Vulshok Battlemaster') on conflict do nothing;
    insert into mtgcard(name) values ('Mind''s Eye') on conflict do nothing;
    insert into mtgcard(name) values ('Iron Myr') on conflict do nothing;
    insert into mtgcard(name) values ('Necrogen Spellbomb') on conflict do nothing;
    insert into mtgcard(name) values ('Chimney Imp') on conflict do nothing;
    insert into mtgcard(name) values ('Lifespark Spellbomb') on conflict do nothing;
    insert into mtgcard(name) values ('Myr Adapter') on conflict do nothing;
    insert into mtgcard(name) values ('Tower of Eons') on conflict do nothing;
    insert into mtgcard(name) values ('Moriok Scavenger') on conflict do nothing;
    insert into mtgcard(name) values ('Neurok Hoversail') on conflict do nothing;
    insert into mtgcard(name) values ('Thirst for Knowledge') on conflict do nothing;
    insert into mtgcard(name) values ('Blinkmoth Urn') on conflict do nothing;
    insert into mtgcard(name) values ('Soul Foundry') on conflict do nothing;
    insert into mtgcard(name) values ('Serum Tank') on conflict do nothing;
    insert into mtgcard(name) values ('Island') on conflict do nothing;
    insert into mtgcard(name) values ('Scale of Chiss-Goria') on conflict do nothing;
    insert into mtgcard(name) values ('Vulshok Battlegear') on conflict do nothing;
    insert into mtgcard(name) values ('Mass Hysteria') on conflict do nothing;
    insert into mtgcard(name) values ('Scythe of the Wretched') on conflict do nothing;
    insert into mtgcard(name) values ('Spikeshot Goblin') on conflict do nothing;
    insert into mtgcard(name) values ('Vulshok Gauntlets') on conflict do nothing;
    insert into mtgcard(name) values ('Slith Predator') on conflict do nothing;
    insert into mtgcard(name) values ('Wall of Blood') on conflict do nothing;
    insert into mtgcard(name) values ('Forest') on conflict do nothing;
    insert into mtgcard(name) values ('Necrogen Mists') on conflict do nothing;
    insert into mtgcard(name) values ('Chalice of the Void') on conflict do nothing;
    insert into mtgcard(name) values ('Brown Ouphe') on conflict do nothing;
    insert into mtgcard(name) values ('Dead-Iron Sledge') on conflict do nothing;
    insert into mtgcard(name) values ('Dross Prowler') on conflict do nothing;
    insert into mtgcard(name) values ('Mountain') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin War Wagon') on conflict do nothing;
    insert into mtgcard(name) values ('Slith Bloodletter') on conflict do nothing;
    insert into mtgcard(name) values ('Vulshok Berserker') on conflict do nothing;
    insert into mtgcard(name) values ('Detonate') on conflict do nothing;
    insert into mtgcard(name) values ('Rule of Law') on conflict do nothing;
    insert into mtgcard(name) values ('Plated Slagwurm') on conflict do nothing;
    insert into mtgcard(name) values ('Nuisance Engine') on conflict do nothing;
    insert into mtgcard(name) values ('Leonin Skyhunter') on conflict do nothing;
    insert into mtgcard(name) values ('Dross Scorpion') on conflict do nothing;
    insert into mtgcard(name) values ('Forge Armor') on conflict do nothing;
    insert into mtgcard(name) values ('Great Furnace') on conflict do nothing;
    insert into mtgcard(name) values ('Pewter Golem') on conflict do nothing;
    insert into mtgcard(name) values ('Quicksilver Elemental') on conflict do nothing;
    insert into mtgcard(name) values ('Mesmeric Orb') on conflict do nothing;
    insert into mtgcard(name) values ('Looming Hoverguard') on conflict do nothing;
    insert into mtgcard(name) values ('Battlegrowth') on conflict do nothing;
    insert into mtgcard(name) values ('Fangren Hunter') on conflict do nothing;
    insert into mtgcard(name) values ('Megatog') on conflict do nothing;
    insert into mtgcard(name) values ('Cloudpost') on conflict do nothing;
    insert into mtgcard(name) values ('Myr Incubator') on conflict do nothing;
    insert into mtgcard(name) values ('Hum of the Radix') on conflict do nothing;
    insert into mtgcard(name) values ('Pentavus') on conflict do nothing;
    insert into mtgcard(name) values ('Mindstorm Crown') on conflict do nothing;
    insert into mtgcard(name) values ('Override') on conflict do nothing;
    insert into mtgcard(name) values ('Grim Reminder') on conflict do nothing;
    insert into mtgcard(name) values ('Tanglebloom') on conflict do nothing;
    insert into mtgcard(name) values ('Wrench Mind') on conflict do nothing;
    insert into mtgcard(name) values ('Sphere of Purity') on conflict do nothing;
    insert into mtgcard(name) values ('Leonin Sun Standard') on conflict do nothing;
    insert into mtgcard(name) values ('Tel-Jilad Archers') on conflict do nothing;
    insert into mtgcard(name) values ('Sword of Kaldra') on conflict do nothing;
    insert into mtgcard(name) values ('Sculpting Steel') on conflict do nothing;
    insert into mtgcard(name) values ('Spoils of the Vault') on conflict do nothing;
    insert into mtgcard(name) values ('Worldslayer') on conflict do nothing;
    insert into mtgcard(name) values ('Hematite Golem') on conflict do nothing;
    insert into mtgcard(name) values ('Icy Manipulator') on conflict do nothing;
    insert into mtgcard(name) values ('Steel Wall') on conflict do nothing;
    insert into mtgcard(name) values ('Rustmouth Ogre') on conflict do nothing;
    insert into mtgcard(name) values ('Living Hive') on conflict do nothing;
    insert into mtgcard(name) values ('Thought Prison') on conflict do nothing;
    insert into mtgcard(name) values ('Scrabbling Claws') on conflict do nothing;
    insert into mtgcard(name) values ('Talisman of Progress') on conflict do nothing;
    insert into mtgcard(name) values ('Disarm') on conflict do nothing;
    insert into mtgcard(name) values ('Welding Jar') on conflict do nothing;
    insert into mtgcard(name) values ('Tower of Fortunes') on conflict do nothing;
    insert into mtgcard(name) values ('Lumengrid Warden') on conflict do nothing;
    insert into mtgcard(name) values ('Swamp') on conflict do nothing;
    insert into mtgcard(name) values ('Synod Sanctum') on conflict do nothing;
    insert into mtgcard(name) values ('Bonesplitter') on conflict do nothing;
    insert into mtgcard(name) values ('Neurok Spy') on conflict do nothing;
    insert into mtgcard(name) values ('Lumengrid Sentinel') on conflict do nothing;
    insert into mtgcard(name) values ('Raise the Alarm') on conflict do nothing;
    insert into mtgcard(name) values ('Auriok Steelshaper') on conflict do nothing;
    insert into mtgcard(name) values ('Second Sunrise') on conflict do nothing;
    insert into mtgcard(name) values ('Psychic Membrane') on conflict do nothing;
    insert into mtgcard(name) values ('Mask of Memory') on conflict do nothing;
    insert into mtgcard(name) values ('Auriok Bladewarden') on conflict do nothing;
    insert into mtgcard(name) values ('Tel-Jilad Stylus') on conflict do nothing;
    insert into mtgcard(name) values ('Wanderguard Sentry') on conflict do nothing;
    insert into mtgcard(name) values ('Neurok Familiar') on conflict do nothing;
    insert into mtgcard(name) values ('Deconstruct') on conflict do nothing;
    insert into mtgcard(name) values ('Plains') on conflict do nothing;
    insert into mtgcard(name) values ('Leaden Myr') on conflict do nothing;
    insert into mtgcard(name) values ('Proteus Staff') on conflict do nothing;
    insert into mtgcard(name) values ('Timesifter') on conflict do nothing;
    insert into mtgcard(name) values ('Grab the Reins') on conflict do nothing;
    insert into mtgcard(name) values ('Fatespinner') on conflict do nothing;
    insert into mtgcard(name) values ('Solar Tide') on conflict do nothing;
    insert into mtgcard(name) values ('Platinum Angel') on conflict do nothing;
    insert into mtgcard(name) values ('Fractured Loyalty') on conflict do nothing;
    insert into mtgcard(name) values ('Rustspore Ram') on conflict do nothing;
    insert into mtgcard(name) values ('Seat of the Synod') on conflict do nothing;
    insert into mtgcard(name) values ('Dragon Blood') on conflict do nothing;
    insert into mtgcard(name) values ('Lightning Greaves') on conflict do nothing;
    insert into mtgcard(name) values ('Extraplanar Lens') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Replica') on conflict do nothing;
    insert into mtgcard(name) values ('Disciple of the Vault') on conflict do nothing;
    insert into mtgcard(name) values ('Trolls of Tel-Jilad') on conflict do nothing;
    insert into mtgcard(name) values ('Lumengrid Augur') on conflict do nothing;
    insert into mtgcard(name) values ('Liar''s Pendulum') on conflict do nothing;
    insert into mtgcard(name) values ('Glissa Sunseeker') on conflict do nothing;
    insert into mtgcard(name) values ('Razor Barrier') on conflict do nothing;
    insert into mtgcard(name) values ('Slagwurm Armor') on conflict do nothing;
    insert into mtgcard(name) values ('Chrome Mox') on conflict do nothing;
    insert into mtgcard(name) values ('Myr Prototype') on conflict do nothing;
    insert into mtgcard(name) values ('Inertia Bubble') on conflict do nothing;
    insert into mtgcard(name) values ('Vault of Whispers') on conflict do nothing;
    insert into mtgcard(name) values ('Slith Firewalker') on conflict do nothing;
    insert into mtgcard(name) values ('Predator''s Strike') on conflict do nothing;
    insert into mtgcard(name) values ('Groffskithur') on conflict do nothing;
    insert into mtgcard(name) values ('Tower of Murmurs') on conflict do nothing;
    insert into mtgcard(name) values ('Leonin Bladetrap') on conflict do nothing;
    insert into mtgcard(name) values ('Mourner''s Shield') on conflict do nothing;
    insert into mtgcard(name) values ('Krark''s Thumb') on conflict do nothing;
    insert into mtgcard(name) values ('Nim Lasher') on conflict do nothing;
    insert into mtgcard(name) values ('Duskworker') on conflict do nothing;
    insert into mtgcard(name) values ('Irradiate') on conflict do nothing;
    insert into mtgcard(name) values ('Clockwork Vorrac') on conflict do nothing;
    insert into mtgcard(name) values ('Nightmare Lash') on conflict do nothing;
    insert into mtgcard(name) values ('Dream''s Grip') on conflict do nothing;
    insert into mtgcard(name) values ('Copperhoof Vorrac') on conflict do nothing;
    insert into mtgcard(name) values ('Somber Hoverguard') on conflict do nothing;
    insert into mtgcard(name) values ('Shatter') on conflict do nothing;
    insert into mtgcard(name) values ('Slith Strider') on conflict do nothing;
    insert into mtgcard(name) values ('Isochron Scepter') on conflict do nothing;
    insert into mtgcard(name) values ('Tangleroot') on conflict do nothing;
    insert into mtgcard(name) values ('Taj-Nar Swordsmith') on conflict do nothing;
    insert into mtgcard(name) values ('Leonin Scimitar') on conflict do nothing;
    insert into mtgcard(name) values ('Vedalken Archmage') on conflict do nothing;
    insert into mtgcard(name) values ('Triskelion') on conflict do nothing;
    insert into mtgcard(name) values ('Annul') on conflict do nothing;
    insert into mtgcard(name) values ('Fireshrieker') on conflict do nothing;
    insert into mtgcard(name) values ('Titanium Golem') on conflict do nothing;
    insert into mtgcard(name) values ('Seething Song') on conflict do nothing;
    insert into mtgcard(name) values ('Viridian Shaman') on conflict do nothing;
    insert into mtgcard(name) values ('Galvanic Key') on conflict do nothing;
    insert into mtgcard(name) values ('Yotian Soldier') on conflict do nothing;
    insert into mtgcard(name) values ('Tree of Tales') on conflict do nothing;
    insert into mtgcard(name) values ('Rust Elemental') on conflict do nothing;
    insert into mtgcard(name) values ('Ornithopter') on conflict do nothing;
    insert into mtgcard(name) values ('Krark-Clan Shaman') on conflict do nothing;
    insert into mtgcard(name) values ('Pyrite Spellbomb') on conflict do nothing;
    insert into mtgcard(name) values ('Farsight Mask') on conflict do nothing;
    insert into mtgcard(name) values ('Jinxed Choker') on conflict do nothing;
    insert into mtgcard(name) values ('Mindslaver') on conflict do nothing;
    insert into mtgcard(name) values ('Talisman of Dominance') on conflict do nothing;
    insert into mtgcard(name) values ('Luminous Angel') on conflict do nothing;
    insert into mtgcard(name) values ('Vermiculos') on conflict do nothing;
    insert into mtgcard(name) values ('March of the Machines') on conflict do nothing;
    insert into mtgcard(name) values ('Sun Droplet') on conflict do nothing;
    insert into mtgcard(name) values ('Bosh, Iron Golem') on conflict do nothing;
    insert into mtgcard(name) values ('Loxodon Peacekeeper') on conflict do nothing;
    insert into mtgcard(name) values ('Relic Bane') on conflict do nothing;
    insert into mtgcard(name) values ('War Elemental') on conflict do nothing;
    insert into mtgcard(name) values ('Confusion in the Ranks') on conflict do nothing;
    insert into mtgcard(name) values ('Auriok Transfixer') on conflict do nothing;
    insert into mtgcard(name) values ('Talisman of Impulse') on conflict do nothing;
    insert into mtgcard(name) values ('Nim Devourer') on conflict do nothing;
    insert into mtgcard(name) values ('Atog') on conflict do nothing;
    insert into mtgcard(name) values ('Contaminated Bond') on conflict do nothing;
    insert into mtgcard(name) values ('Loxodon Warhammer') on conflict do nothing;
    insert into mtgcard(name) values ('Gilded Lotus') on conflict do nothing;
    insert into mtgcard(name) values ('Journey of Discovery') on conflict do nothing;
    insert into mtgcard(name) values ('Spellweaver Helix') on conflict do nothing;
    insert into mtgcard(name) values ('Copper Myr') on conflict do nothing;
    insert into mtgcard(name) values ('Arrest') on conflict do nothing;
    insert into mtgcard(name) values ('Awe Strike') on conflict do nothing;
    insert into mtgcard(name) values ('Fabricate') on conflict do nothing;
    insert into mtgcard(name) values ('Lightning Coils') on conflict do nothing;
    insert into mtgcard(name) values ('Wail of the Nim') on conflict do nothing;
    insert into mtgcard(name) values ('Fiery Gambit') on conflict do nothing;
    insert into mtgcard(name) values ('Golem-Skin Gauntlets') on conflict do nothing;
    insert into mtgcard(name) values ('Betrayal of Flesh') on conflict do nothing;
    insert into mtgcard(name) values ('Pearl Shard') on conflict do nothing;
    insert into mtgcard(name) values ('Shared Fate') on conflict do nothing;
    insert into mtgcard(name) values ('Dross Harvester') on conflict do nothing;
    insert into mtgcard(name) values ('Power Conduit') on conflict do nothing;
    insert into mtgcard(name) values ('Heartwood Shard') on conflict do nothing;
    insert into mtgcard(name) values ('Vorrac Battlehorns') on conflict do nothing;
    insert into mtgcard(name) values ('Crystal Shard') on conflict do nothing;
    insert into mtgcard(name) values ('Quicksilver Fountain') on conflict do nothing;
    insert into mtgcard(name) values ('Sylvan Scrying') on conflict do nothing;
    insert into mtgcard(name) values ('Viridian Joiner') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Charbelcher') on conflict do nothing;
    insert into mtgcard(name) values ('Blinding Beam') on conflict do nothing;
    insert into mtgcard(name) values ('Banshee''s Blade') on conflict do nothing;
    insert into mtgcard(name) values ('Silver Myr') on conflict do nothing;
    insert into mtgcard(name) values ('Elf Replica') on conflict do nothing;
    insert into mtgcard(name) values ('Clockwork Beetle') on conflict do nothing;
    insert into mtgcard(name) values ('Viridian Longbow') on conflict do nothing;
    insert into mtgcard(name) values ('Barter in Blood') on conflict do nothing;
    insert into mtgcard(name) values ('Domineer') on conflict do nothing;
    insert into mtgcard(name) values ('Skyhunter Patrol') on conflict do nothing;
    insert into mtgcard(name) values ('Loxodon Mender') on conflict do nothing;
    insert into mtgcard(name) values ('Gate to the Aether') on conflict do nothing;
    insert into mtgcard(name) values ('One Dozen Eyes') on conflict do nothing;
    insert into mtgcard(name) values ('Nim Shrieker') on conflict do nothing;
    insert into mtgcard(name) values ('Electrostatic Bolt') on conflict do nothing;
    insert into mtgcard(name) values ('Grid Monitor') on conflict do nothing;
    insert into mtgcard(name) values ('Leonin Abunas') on conflict do nothing;
    insert into mtgcard(name) values ('Woebearer') on conflict do nothing;
    insert into mtgcard(name) values ('Stalking Stones') on conflict do nothing;
    insert into mtgcard(name) values ('Leonin Den-Guard') on conflict do nothing;
    insert into mtgcard(name) values ('Clockwork Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Ancient Den') on conflict do nothing;
    insert into mtgcard(name) values ('Skyhunter Cub') on conflict do nothing;
    insert into mtgcard(name) values ('Reiver Demon') on conflict do nothing;
    insert into mtgcard(name) values ('Malachite Golem') on conflict do nothing;
    insert into mtgcard(name) values ('Nim Replica') on conflict do nothing;
    insert into mtgcard(name) values ('Sunbeam Spellbomb') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Dirigible') on conflict do nothing;
    insert into mtgcard(name) values ('Arc-Slogger') on conflict do nothing;
    insert into mtgcard(name) values ('Duplicant') on conflict do nothing;
    insert into mtgcard(name) values ('Needlebug') on conflict do nothing;
    insert into mtgcard(name) values ('Tempest of Light') on conflict do nothing;
    insert into mtgcard(name) values ('Creeping Mold') on conflict do nothing;
    insert into mtgcard(name) values ('Talisman of Unity') on conflict do nothing;
    insert into mtgcard(name) values ('Mirror Golem') on conflict do nothing;
    insert into mtgcard(name) values ('Promise of Power') on conflict do nothing;
    insert into mtgcard(name) values ('Temporal Cascade') on conflict do nothing;
    insert into mtgcard(name) values ('Tooth of Chiss-Goria') on conflict do nothing;
    insert into mtgcard(name) values ('Culling Scales') on conflict do nothing;
    insert into mtgcard(name) values ('Roar of the Kha') on conflict do nothing;
    insert into mtgcard(name) values ('Bloodscent') on conflict do nothing;
    insert into mtgcard(name) values ('Altar''s Light') on conflict do nothing;
    insert into mtgcard(name) values ('Omega Myr') on conflict do nothing;
    insert into mtgcard(name) values ('Oblivion Stone') on conflict do nothing;
    insert into mtgcard(name) values ('Loxodon Punisher') on conflict do nothing;
    insert into mtgcard(name) values ('Talisman of Indulgence') on conflict do nothing;
    insert into mtgcard(name) values ('Empyrial Plate') on conflict do nothing;
    insert into mtgcard(name) values ('Granite Shard') on conflict do nothing;
    insert into mtgcard(name) values ('Damping Matrix') on conflict do nothing;
    insert into mtgcard(name) values ('Cathodion') on conflict do nothing;
    insert into mtgcard(name) values ('Glimmervoid') on conflict do nothing;
    insert into mtgcard(name) values ('Soldier Replica') on conflict do nothing;
    insert into mtgcard(name) values ('Tel-Jilad Exile') on conflict do nothing;
    insert into mtgcard(name) values ('Ogre Leadfoot') on conflict do nothing;
    insert into mtgcard(name) values ('Blinkmoth Well') on conflict do nothing;
    insert into mtgcard(name) values ('Nim Shambler') on conflict do nothing;
    insert into mtgcard(name) values ('Wizard Replica') on conflict do nothing;
    insert into mtgcard(name) values ('Psychogenic Probe') on conflict do nothing;
    insert into mtgcard(name) values ('Regress') on conflict do nothing;
    insert into mtgcard(name) values ('Leonin Elder') on conflict do nothing;
    insert into mtgcard(name) values ('Fists of the Anvil') on conflict do nothing;
    insert into mtgcard(name) values ('Altar of Shadows') on conflict do nothing;
    insert into mtgcard(name) values ('Trash for Treasure') on conflict do nothing;
    insert into mtgcard(name) values ('Cobalt Golem') on conflict do nothing;
    insert into mtgcard(name) values ('Incite War') on conflict do nothing;
    insert into mtgcard(name) values ('Molder Slug') on conflict do nothing;
    insert into mtgcard(name) values ('Myr Retriever') on conflict do nothing;
    insert into mtgcard(name) values ('Myr Enforcer') on conflict do nothing;
    insert into mtgcard(name) values ('Tel-Jilad Chosen') on conflict do nothing;
    insert into mtgcard(name) values ('Skeleton Shard') on conflict do nothing;
    insert into mtgcard(name) values ('Thoughtcast') on conflict do nothing;
    insert into mtgcard(name) values ('Troll Ascetic') on conflict do nothing;
    insert into mtgcard(name) values ('Wurmskin Forger') on conflict do nothing;
    insert into mtgcard(name) values ('Chromatic Sphere') on conflict do nothing;
    insert into mtgcard(name) values ('Consume Spirit') on conflict do nothing;
    insert into mtgcard(name) values ('Aether Spellbomb') on conflict do nothing;
    insert into mtgcard(name) values ('Alpha Myr') on conflict do nothing;
    insert into mtgcard(name) values ('Terror') on conflict do nothing;
    insert into mtgcard(name) values ('Tower of Champions') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Striker') on conflict do nothing;
    insert into mtgcard(name) values ('Soul Nova') on conflict do nothing;
    insert into mtgcard(name) values ('Molten Rain') on conflict do nothing;
    insert into mtgcard(name) values ('Myr Mindservant') on conflict do nothing;
    insert into mtgcard(name) values ('Gold Myr') on conflict do nothing;
    insert into mtgcard(name) values ('Lodestone Myr') on conflict do nothing;
    insert into mtgcard(name) values ('Assert Authority') on conflict do nothing;
    insert into mtgcard(name) values ('Turn to Dust') on conflict do nothing;
    insert into mtgcard(name) values ('Flayed Nim') on conflict do nothing;
    insert into mtgcard(name) values ('Frogmite') on conflict do nothing;
    insert into mtgcard(name) values ('Krark-Clan Grunt') on conflict do nothing;
