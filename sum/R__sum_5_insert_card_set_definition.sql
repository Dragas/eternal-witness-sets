insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Disintegrate'),
    (select id from sets where short_name = 'sum'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serendib Efreet'),
    (select id from sets where short_name = 'sum'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armageddon Clock'),
    (select id from sets where short_name = 'sum'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Underground Sea'),
    (select id from sets where short_name = 'sum'),
    '290',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guardian Angel'),
    (select id from sets where short_name = 'sum'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wanderlust'),
    (select id from sets where short_name = 'sum'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Siren''s Call'),
    (select id from sets where short_name = 'sum'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fork'),
    (select id from sets where short_name = 'sum'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Holy Strength'),
    (select id from sets where short_name = 'sum'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Regrowth'),
    (select id from sets where short_name = 'sum'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lifetap'),
    (select id from sets where short_name = 'sum'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Bone'),
    (select id from sets where short_name = 'sum'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Conservator'),
    (select id from sets where short_name = 'sum'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magnetic Mountain'),
    (select id from sets where short_name = 'sum'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'sum'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basalt Monolith'),
    (select id from sets where short_name = 'sum'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'sum'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shanodin Dryads'),
    (select id from sets where short_name = 'sum'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'White Knight'),
    (select id from sets where short_name = 'sum'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'sum'),
    '303',
    'common'
) ,
(
    (select id from mtgcard where name = 'Living Wall'),
    (select id from sets where short_name = 'sum'),
    '262',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'sum'),
    '302',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Brambles'),
    (select id from sets where short_name = 'sum'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bad Moon'),
    (select id from sets where short_name = 'sum'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Will-o''-the-Wisp'),
    (select id from sets where short_name = 'sum'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Vault'),
    (select id from sets where short_name = 'sum'),
    '263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Righteousness'),
    (select id from sets where short_name = 'sum'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unholy Strength'),
    (select id from sets where short_name = 'sum'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Artillery'),
    (select id from sets where short_name = 'sum'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smoke'),
    (select id from sets where short_name = 'sum'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonic Hordes'),
    (select id from sets where short_name = 'sum'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Regeneration'),
    (select id from sets where short_name = 'sum'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evil Presence'),
    (select id from sets where short_name = 'sum'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mishra''s War Machine'),
    (select id from sets where short_name = 'sum'),
    '266',
    'rare'
) ,
(
    (select id from mtgcard where name = 'White Ward'),
    (select id from sets where short_name = 'sum'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grizzly Bears'),
    (select id from sets where short_name = 'sum'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychic Venom'),
    (select id from sets where short_name = 'sum'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reverse Polarity'),
    (select id from sets where short_name = 'sum'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hurkyl''s Recall'),
    (select id from sets where short_name = 'sum'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Rack'),
    (select id from sets where short_name = 'sum'),
    '278',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Copy Artifact'),
    (select id from sets where short_name = 'sum'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonic Attorney'),
    (select id from sets where short_name = 'sum'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fungusaur'),
    (select id from sets where short_name = 'sum'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Braingeyser'),
    (select id from sets where short_name = 'sum'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reconstruction'),
    (select id from sets where short_name = 'sum'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reverse Damage'),
    (select id from sets where short_name = 'sum'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Eruption'),
    (select id from sets where short_name = 'sum'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tsunami'),
    (select id from sets where short_name = 'sum'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feedback'),
    (select id from sets where short_name = 'sum'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Celestial Prism'),
    (select id from sets where short_name = 'sum'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scryb Sprites'),
    (select id from sets where short_name = 'sum'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Instill Energy'),
    (select id from sets where short_name = 'sum'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plateau'),
    (select id from sets where short_name = 'sum'),
    '284',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scathe Zombies'),
    (select id from sets where short_name = 'sum'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magical Hack'),
    (select id from sets where short_name = 'sum'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balance'),
    (select id from sets where short_name = 'sum'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Red Elemental Blast'),
    (select id from sets where short_name = 'sum'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Engine'),
    (select id from sets where short_name = 'sum'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Badlands'),
    (select id from sets where short_name = 'sum'),
    '282',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin King'),
    (select id from sets where short_name = 'sum'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dingus Egg'),
    (select id from sets where short_name = 'sum'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drain Power'),
    (select id from sets where short_name = 'sum'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Green'),
    (select id from sets where short_name = 'sum'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crumble'),
    (select id from sets where short_name = 'sum'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lord of the Pit'),
    (select id from sets where short_name = 'sum'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animate Wall'),
    (select id from sets where short_name = 'sum'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merfolk of the Pearl Trident'),
    (select id from sets where short_name = 'sum'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ankh of Mishra'),
    (select id from sets where short_name = 'sum'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wheel of Fortune'),
    (select id from sets where short_name = 'sum'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fear'),
    (select id from sets where short_name = 'sum'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lance'),
    (select id from sets where short_name = 'sum'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = 'sum'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glasses of Urza'),
    (select id from sets where short_name = 'sum'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blue Ward'),
    (select id from sets where short_name = 'sum'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Web'),
    (select id from sets where short_name = 'sum'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unstable Mutation'),
    (select id from sets where short_name = 'sum'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erg Raiders'),
    (select id from sets where short_name = 'sum'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'sum'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Red Ward'),
    (select id from sets where short_name = 'sum'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = 'sum'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Howling Mine'),
    (select id from sets where short_name = 'sum'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sedge Troll'),
    (select id from sets where short_name = 'sum'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tundra'),
    (select id from sets where short_name = 'sum'),
    '289',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roc of Kher Ridges'),
    (select id from sets where short_name = 'sum'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = 'sum'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = 'sum'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Titania''s Song'),
    (select id from sets where short_name = 'sum'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = 'sum'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Stone'),
    (select id from sets where short_name = 'sum'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'sum'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea Serpent'),
    (select id from sets where short_name = 'sum'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nettling Imp'),
    (select id from sets where short_name = 'sum'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bottle of Suleiman'),
    (select id from sets where short_name = 'sum'),
    '237',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hurloon Minotaur'),
    (select id from sets where short_name = 'sum'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'War Mammoth'),
    (select id from sets where short_name = 'sum'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rocket Launcher'),
    (select id from sets where short_name = 'sum'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Specter'),
    (select id from sets where short_name = 'sum'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Verduran Enchantress'),
    (select id from sets where short_name = 'sum'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = 'sum'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathgrip'),
    (select id from sets where short_name = 'sum'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gloom'),
    (select id from sets where short_name = 'sum'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pirate Ship'),
    (select id from sets where short_name = 'sum'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savannah Lions'),
    (select id from sets where short_name = 'sum'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Water'),
    (select id from sets where short_name = 'sum'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pearled Unicorn'),
    (select id from sets where short_name = 'sum'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howl from Beyond'),
    (select id from sets where short_name = 'sum'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jump'),
    (select id from sets where short_name = 'sum'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prodigal Sorcerer'),
    (select id from sets where short_name = 'sum'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'sum'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Growth'),
    (select id from sets where short_name = 'sum'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = 'sum'),
    '267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'sum'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scrubland'),
    (select id from sets where short_name = 'sum'),
    '286',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Atog'),
    (select id from sets where short_name = 'sum'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sacrifice'),
    (select id from sets where short_name = 'sum'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Wood'),
    (select id from sets where short_name = 'sum'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kormus Bell'),
    (select id from sets where short_name = 'sum'),
    '260',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Living Lands'),
    (select id from sets where short_name = 'sum'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'sum'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Creature Bond'),
    (select id from sets where short_name = 'sum'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Giant'),
    (select id from sets where short_name = 'sum'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Knight'),
    (select id from sets where short_name = 'sum'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Uthden Troll'),
    (select id from sets where short_name = 'sum'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Force of Nature'),
    (select id from sets where short_name = 'sum'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Twist'),
    (select id from sets where short_name = 'sum'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drudge Skeletons'),
    (select id from sets where short_name = 'sum'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earth Elemental'),
    (select id from sets where short_name = 'sum'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Veteran Bodyguard'),
    (select id from sets where short_name = 'sum'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Personal Incarnation'),
    (select id from sets where short_name = 'sum'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Liege'),
    (select id from sets where short_name = 'sum'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Purelace'),
    (select id from sets where short_name = 'sum'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Whelp'),
    (select id from sets where short_name = 'sum'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brass Man'),
    (select id from sets where short_name = 'sum'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desert Twister'),
    (select id from sets where short_name = 'sum'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Power Sink'),
    (select id from sets where short_name = 'sum'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earthbind'),
    (select id from sets where short_name = 'sum'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lure'),
    (select id from sets where short_name = 'sum'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'El-Hajjâj'),
    (select id from sets where short_name = 'sum'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benalish Hero'),
    (select id from sets where short_name = 'sum'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nether Shadow'),
    (select id from sets where short_name = 'sum'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Taiga'),
    (select id from sets where short_name = 'sum'),
    '287',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: White'),
    (select id from sets where short_name = 'sum'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Royal Assassin'),
    (select id from sets where short_name = 'sum'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatterstorm'),
    (select id from sets where short_name = 'sum'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Vise'),
    (select id from sets where short_name = 'sum'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island Fish Jasconius'),
    (select id from sets where short_name = 'sum'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Onulet'),
    (select id from sets where short_name = 'sum'),
    '269',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Blue'),
    (select id from sets where short_name = 'sum'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aladdin''s Lamp'),
    (select id from sets where short_name = 'sum'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Terrain'),
    (select id from sets where short_name = 'sum'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Juggernaut'),
    (select id from sets where short_name = 'sum'),
    '259',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flashfires'),
    (select id from sets where short_name = 'sum'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bog Wraith'),
    (select id from sets where short_name = 'sum'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Fire'),
    (select id from sets where short_name = 'sum'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flying Carpet'),
    (select id from sets where short_name = 'sum'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'sum'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Water Elemental'),
    (select id from sets where short_name = 'sum'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island Sanctuary'),
    (select id from sets where short_name = 'sum'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karma'),
    (select id from sets where short_name = 'sum'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'sum'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paralyze'),
    (select id from sets where short_name = 'sum'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Farmstead'),
    (select id from sets where short_name = 'sum'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Flare'),
    (select id from sets where short_name = 'sum'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'sum'),
    '306',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'sum'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'sum'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'sum'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vesuvan Doppelganger'),
    (select id from sets where short_name = 'sum'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Power Surge'),
    (select id from sets where short_name = 'sum'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death Ward'),
    (select id from sets where short_name = 'sum'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fog'),
    (select id from sets where short_name = 'sum'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crystal Rod'),
    (select id from sets where short_name = 'sum'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chaoslace'),
    (select id from sets where short_name = 'sum'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = 'sum'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Balloon Brigade'),
    (select id from sets where short_name = 'sum'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firebreathing'),
    (select id from sets where short_name = 'sum'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stream of Life'),
    (select id from sets where short_name = 'sum'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Leak'),
    (select id from sets where short_name = 'sum'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fastbond'),
    (select id from sets where short_name = 'sum'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'sum'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Timber Wolves'),
    (select id from sets where short_name = 'sum'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Forces'),
    (select id from sets where short_name = 'sum'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aspect of Wolf'),
    (select id from sets where short_name = 'sum'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crusade'),
    (select id from sets where short_name = 'sum'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Healing Salve'),
    (select id from sets where short_name = 'sum'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clockwork Beast'),
    (select id from sets where short_name = 'sum'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drain Life'),
    (select id from sets where short_name = 'sum'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'sum'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savannah'),
    (select id from sets where short_name = 'sum'),
    '285',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meekstone'),
    (select id from sets where short_name = 'sum'),
    '264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ivory Tower'),
    (select id from sets where short_name = 'sum'),
    '254',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conversion'),
    (select id from sets where short_name = 'sum'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burrowing'),
    (select id from sets where short_name = 'sum'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sorceress Queen'),
    (select id from sets where short_name = 'sum'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fire Elemental'),
    (select id from sets where short_name = 'sum'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dwarven Weaponsmith'),
    (select id from sets where short_name = 'sum'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deathlace'),
    (select id from sets where short_name = 'sum'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'sum'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'sum'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Channel'),
    (select id from sets where short_name = 'sum'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Holy Armor'),
    (select id from sets where short_name = 'sum'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Black'),
    (select id from sets where short_name = 'sum'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thicket Basilisk'),
    (select id from sets where short_name = 'sum'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jandor''s Ring'),
    (select id from sets where short_name = 'sum'),
    '256',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mons''s Goblin Raiders'),
    (select id from sets where short_name = 'sum'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Archers'),
    (select id from sets where short_name = 'sum'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = 'sum'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = 'sum'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Iron Star'),
    (select id from sets where short_name = 'sum'),
    '252',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wooden Sphere'),
    (select id from sets where short_name = 'sum'),
    '281',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steal Artifact'),
    (select id from sets where short_name = 'sum'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Ward'),
    (select id from sets where short_name = 'sum'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Millstone'),
    (select id from sets where short_name = 'sum'),
    '265',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunglasses of Urza'),
    (select id from sets where short_name = 'sum'),
    '276',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Short'),
    (select id from sets where short_name = 'sum'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keldon Warlord'),
    (select id from sets where short_name = 'sum'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spell Blast'),
    (select id from sets where short_name = 'sum'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'sum'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mahamoti Djinn'),
    (select id from sets where short_name = 'sum'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'sum'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Control Magic'),
    (select id from sets where short_name = 'sum'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Swords'),
    (select id from sets where short_name = 'sum'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cockatrice'),
    (select id from sets where short_name = 'sum'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jade Monolith'),
    (select id from sets where short_name = 'sum'),
    '255',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animate Dead'),
    (select id from sets where short_name = 'sum'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plague Rats'),
    (select id from sets where short_name = 'sum'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Samite Healer'),
    (select id from sets where short_name = 'sum'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warp Artifact'),
    (select id from sets where short_name = 'sum'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gray Ogre'),
    (select id from sets where short_name = 'sum'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'sum'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ebony Horse'),
    (select id from sets where short_name = 'sum'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simulacrum'),
    (select id from sets where short_name = 'sum'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dwarven Warriors'),
    (select id from sets where short_name = 'sum'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thoughtlace'),
    (select id from sets where short_name = 'sum'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scavenging Ghoul'),
    (select id from sets where short_name = 'sum'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kudzu'),
    (select id from sets where short_name = 'sum'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonic Tutor'),
    (select id from sets where short_name = 'sum'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kird Ape'),
    (select id from sets where short_name = 'sum'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = 'sum'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ley Druid'),
    (select id from sets where short_name = 'sum'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stasis'),
    (select id from sets where short_name = 'sum'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hill Giant'),
    (select id from sets where short_name = 'sum'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hurricane'),
    (select id from sets where short_name = 'sum'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rock Hydra'),
    (select id from sets where short_name = 'sum'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lifelace'),
    (select id from sets where short_name = 'sum'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aladdin''s Ring'),
    (select id from sets where short_name = 'sum'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = 'sum'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mesa Pegasus'),
    (select id from sets where short_name = 'sum'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Energy Flux'),
    (select id from sets where short_name = 'sum'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jayemdae Tome'),
    (select id from sets where short_name = 'sum'),
    '258',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jandor''s Saddlebags'),
    (select id from sets where short_name = 'sum'),
    '257',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cursed Land'),
    (select id from sets where short_name = 'sum'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weakness'),
    (select id from sets where short_name = 'sum'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pestilence'),
    (select id from sets where short_name = 'sum'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = 'sum'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Living Artifact'),
    (select id from sets where short_name = 'sum'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lifeforce'),
    (select id from sets where short_name = 'sum'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clone'),
    (select id from sets where short_name = 'sum'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Resurrection'),
    (select id from sets where short_name = 'sum'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dancing Scimitar'),
    (select id from sets where short_name = 'sum'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ornithopter'),
    (select id from sets where short_name = 'sum'),
    '270',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Hive'),
    (select id from sets where short_name = 'sum'),
    '277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primal Clay'),
    (select id from sets where short_name = 'sum'),
    '271',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lord of Atlantis'),
    (select id from sets where short_name = 'sum'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zombie Master'),
    (select id from sets where short_name = 'sum'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Green Ward'),
    (select id from sets where short_name = 'sum'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Library of Leng'),
    (select id from sets where short_name = 'sum'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Craw Wurm'),
    (select id from sets where short_name = 'sum'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Contract from Below'),
    (select id from sets where short_name = 'sum'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Granite Gargoyle'),
    (select id from sets where short_name = 'sum'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tunnel'),
    (select id from sets where short_name = 'sum'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'sum'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disrupting Scepter'),
    (select id from sets where short_name = 'sum'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flight'),
    (select id from sets where short_name = 'sum'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rod of Ruin'),
    (select id from sets where short_name = 'sum'),
    '273',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winter Orb'),
    (select id from sets where short_name = 'sum'),
    '280',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'sum'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Oriflamme'),
    (select id from sets where short_name = 'sum'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bayou'),
    (select id from sets where short_name = 'sum'),
    '283',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantom Monster'),
    (select id from sets where short_name = 'sum'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Throne of Bone'),
    (select id from sets where short_name = 'sum'),
    '279',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquility'),
    (select id from sets where short_name = 'sum'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = 'sum'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Manabarbs'),
    (select id from sets where short_name = 'sum'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Northern Paladin'),
    (select id from sets where short_name = 'sum'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animate Artifact'),
    (select id from sets where short_name = 'sum'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Ice'),
    (select id from sets where short_name = 'sum'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Net'),
    (select id from sets where short_name = 'sum'),
    '275',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eye for an Eye'),
    (select id from sets where short_name = 'sum'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Castle'),
    (select id from sets where short_name = 'sum'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frozen Shade'),
    (select id from sets where short_name = 'sum'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Island'),
    (select id from sets where short_name = 'sum'),
    '291',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ironroot Treefolk'),
    (select id from sets where short_name = 'sum'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = 'sum'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darkpact'),
    (select id from sets where short_name = 'sum'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ivory Cup'),
    (select id from sets where short_name = 'sum'),
    '253',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mijae Djinn'),
    (select id from sets where short_name = 'sum'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Helm of Chatzuk'),
    (select id from sets where short_name = 'sum'),
    '250',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sleight of Mind'),
    (select id from sets where short_name = 'sum'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'sum'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'sum'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tropical Island'),
    (select id from sets where short_name = 'sum'),
    '288',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blue Elemental Blast'),
    (select id from sets where short_name = 'sum'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obsianus Golem'),
    (select id from sets where short_name = 'sum'),
    '268',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blessing'),
    (select id from sets where short_name = 'sum'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Air'),
    (select id from sets where short_name = 'sum'),
    '90',
    'uncommon'
) 
 on conflict do nothing;
