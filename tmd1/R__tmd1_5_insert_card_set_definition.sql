insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tmd1'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tmd1'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elspeth, Knight-Errant Emblem'),
    (select id from sets where short_name = 'tmd1'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr'),
    (select id from sets where short_name = 'tmd1'),
    '3',
    'common'
) 
 on conflict do nothing;
