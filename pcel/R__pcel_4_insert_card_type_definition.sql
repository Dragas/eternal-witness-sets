insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Robot Chicken'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Robot Chicken'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Robot Chicken'),
        (select types.id from types where name = 'Chicken')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Robot Chicken'),
        (select types.id from types where name = 'Construct')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fraternal Exaltation'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shichifukujin Dragon'),
        (select types.id from types where name = 'Summon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shichifukujin Dragon'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Phoenix Heart'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Proposal'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = '1996 World Champion'),
        (select types.id from types where name = 'Summon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = '1996 World Champion'),
        (select types.id from types where name = 'Legend')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Splendid Genesis'),
        (select types.id from types where name = 'Sorcery')
    ) 
 on conflict do nothing;
