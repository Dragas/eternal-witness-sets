    insert into mtgcard(name) values ('Robot Chicken') on conflict do nothing;
    insert into mtgcard(name) values ('Fraternal Exaltation') on conflict do nothing;
    insert into mtgcard(name) values ('Shichifukujin Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Phoenix Heart') on conflict do nothing;
    insert into mtgcard(name) values ('Proposal') on conflict do nothing;
    insert into mtgcard(name) values ('1996 World Champion') on conflict do nothing;
    insert into mtgcard(name) values ('Splendid Genesis') on conflict do nothing;
