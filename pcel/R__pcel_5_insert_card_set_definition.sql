insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Robot Chicken'),
    (select id from sets where short_name = 'pcel'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fraternal Exaltation'),
    (select id from sets where short_name = 'pcel'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shichifukujin Dragon'),
    (select id from sets where short_name = 'pcel'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phoenix Heart'),
    (select id from sets where short_name = 'pcel'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Proposal'),
    (select id from sets where short_name = 'pcel'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = '1996 World Champion'),
    (select id from sets where short_name = 'pcel'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Splendid Genesis'),
    (select id from sets where short_name = 'pcel'),
    '4',
    'rare'
) 
 on conflict do nothing;
