insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Spined Wurm'),
    (select id from sets where short_name = 'por'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fleet-Footed Monk'),
    (select id from sets where short_name = 'por'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = 'por'),
    '107s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Monstrous Growth'),
    (select id from sets where short_name = 'por'),
    '173†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lizard Warrior'),
    (select id from sets where short_name = 'por'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anaconda'),
    (select id from sets where short_name = 'por'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blaze'),
    (select id from sets where short_name = 'por'),
    '118s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Endless Cockroaches'),
    (select id from sets where short_name = 'por'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spiritual Guardian'),
    (select id from sets where short_name = 'por'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Withering Gaze'),
    (select id from sets where short_name = 'por'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serpent Assassin'),
    (select id from sets where short_name = 'por'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merfolk of the Pearl Trident'),
    (select id from sets where short_name = 'por'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fruition'),
    (select id from sets where short_name = 'por'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'por'),
    '209s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charging Bandits'),
    (select id from sets where short_name = 'por'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'por'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seasoned Marshal'),
    (select id from sets where short_name = 'por'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stern Marshal'),
    (select id from sets where short_name = 'por'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampiric Feast'),
    (select id from sets where short_name = 'por'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Touch of Brilliance'),
    (select id from sets where short_name = 'por'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primeval Force'),
    (select id from sets where short_name = 'por'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pillaging Horde'),
    (select id from sets where short_name = 'por'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Willow Dryad'),
    (select id from sets where short_name = 'por'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nature''s Cloak'),
    (select id from sets where short_name = 'por'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Personal Tutor'),
    (select id from sets where short_name = 'por'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alabaster Dragon'),
    (select id from sets where short_name = 'por'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Untamed Wilds'),
    (select id from sets where short_name = 'por'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Monstrous Growth'),
    (select id from sets where short_name = 'por'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = 'por'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Bully'),
    (select id from sets where short_name = 'por'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampiric Touch'),
    (select id from sets where short_name = 'por'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Assassin''s Blade'),
    (select id from sets where short_name = 'por'),
    '80s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bee Sting'),
    (select id from sets where short_name = 'por'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desert Drake'),
    (select id from sets where short_name = 'por'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prosperity'),
    (select id from sets where short_name = 'por'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'por'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Ranger'),
    (select id from sets where short_name = 'por'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = 'por'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hand of Death'),
    (select id from sets where short_name = 'por'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Tutor'),
    (select id from sets where short_name = 'por'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Theft of Dreams'),
    (select id from sets where short_name = 'por'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = 'por'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Needle Storm'),
    (select id from sets where short_name = 'por'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Charging Paladin'),
    (select id from sets where short_name = 'por'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elven Cache'),
    (select id from sets where short_name = 'por'),
    '164s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'por'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'por'),
    '211s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raging Minotaur'),
    (select id from sets where short_name = 'por'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bull Hippo'),
    (select id from sets where short_name = 'por'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelic Blessing'),
    (select id from sets where short_name = 'por'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain Goat'),
    (select id from sets where short_name = 'por'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Highland Giant'),
    (select id from sets where short_name = 'por'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gift of Estates'),
    (select id from sets where short_name = 'por'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coral Eel'),
    (select id from sets where short_name = 'por'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harsh Justice'),
    (select id from sets where short_name = 'por'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Starlit Angel'),
    (select id from sets where short_name = 'por'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'por'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balance of Power'),
    (select id from sets where short_name = 'por'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archangel'),
    (select id from sets where short_name = 'por'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gorilla Warrior'),
    (select id from sets where short_name = 'por'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'por'),
    '198s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'por'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Djinn of the Lamp'),
    (select id from sets where short_name = 'por'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flux'),
    (select id from sets where short_name = 'por'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'por'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Swords'),
    (select id from sets where short_name = 'por'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'por'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deep Wood'),
    (select id from sets where short_name = 'por'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skeletal Snake'),
    (select id from sets where short_name = 'por'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Wraith'),
    (select id from sets where short_name = 'por'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'por'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cruel Fate'),
    (select id from sets where short_name = 'por'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Octopus'),
    (select id from sets where short_name = 'por'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foot Soldiers'),
    (select id from sets where short_name = 'por'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'por'),
    '210s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sacred Nectar'),
    (select id from sets where short_name = 'por'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grizzly Bears'),
    (select id from sets where short_name = 'por'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charging Rhino'),
    (select id from sets where short_name = 'por'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Baleful Stare'),
    (select id from sets where short_name = 'por'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Craven Giant'),
    (select id from sets where short_name = 'por'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloud Dragon'),
    (select id from sets where short_name = 'por'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blinding Light'),
    (select id from sets where short_name = 'por'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ebon Dragon'),
    (select id from sets where short_name = 'por'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spotted Griffin'),
    (select id from sets where short_name = 'por'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Denial'),
    (select id from sets where short_name = 'por'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ardent Militia'),
    (select id from sets where short_name = 'por'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wind Drake'),
    (select id from sets where short_name = 'por'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Symbol of Unsummoning'),
    (select id from sets where short_name = 'por'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sacred Knight'),
    (select id from sets where short_name = 'por'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = 'por'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keen-Eyed Archers'),
    (select id from sets where short_name = 'por'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nature''s Ruin'),
    (select id from sets where short_name = 'por'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thundermare'),
    (select id from sets where short_name = 'por'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blaze'),
    (select id from sets where short_name = 'por'),
    '118†',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Summer Bloom'),
    (select id from sets where short_name = 'por'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temporary Truce'),
    (select id from sets where short_name = 'por'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'por'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scorching Winds'),
    (select id from sets where short_name = 'por'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jungle Lion'),
    (select id from sets where short_name = 'por'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'por'),
    '215s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hand of Death'),
    (select id from sets where short_name = 'por'),
    '96†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rain of Salt'),
    (select id from sets where short_name = 'por'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'King''s Assassin'),
    (select id from sets where short_name = 'por'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Valorous Charge'),
    (select id from sets where short_name = 'por'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elven Cache'),
    (select id from sets where short_name = 'por'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Omen'),
    (select id from sets where short_name = 'por'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cruel Tutor'),
    (select id from sets where short_name = 'por'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raging Goblin'),
    (select id from sets where short_name = 'por'),
    '145†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Warrior'),
    (select id from sets where short_name = 'por'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anaconda'),
    (select id from sets where short_name = 'por'),
    '158†',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elite Cat Warrior'),
    (select id from sets where short_name = 'por'),
    '163†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Granite'),
    (select id from sets where short_name = 'por'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Devastation'),
    (select id from sets where short_name = 'por'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Venerable Monk'),
    (select id from sets where short_name = 'por'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winds of Change'),
    (select id from sets where short_name = 'por'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elite Cat Warrior'),
    (select id from sets where short_name = 'por'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Virtue''s Ruin'),
    (select id from sets where short_name = 'por'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hulking Cyclops'),
    (select id from sets where short_name = 'por'),
    '134s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snapping Drake'),
    (select id from sets where short_name = 'por'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hurricane'),
    (select id from sets where short_name = 'por'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Déjà Vu'),
    (select id from sets where short_name = 'por'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hill Giant'),
    (select id from sets where short_name = 'por'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prosperity'),
    (select id from sets where short_name = 'por'),
    '66s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rain of Tears'),
    (select id from sets where short_name = 'por'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Python'),
    (select id from sets where short_name = 'por'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rowan Treefolk'),
    (select id from sets where short_name = 'por'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forked Lightning'),
    (select id from sets where short_name = 'por'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warrior''s Charge'),
    (select id from sets where short_name = 'por'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Imp'),
    (select id from sets where short_name = 'por'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Taunt'),
    (select id from sets where short_name = 'por'),
    '71s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Last Chance'),
    (select id from sets where short_name = 'por'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alluring Scent'),
    (select id from sets where short_name = 'por'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plant Elemental'),
    (select id from sets where short_name = 'por'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blessed Reversal'),
    (select id from sets where short_name = 'por'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lava Flow'),
    (select id from sets where short_name = 'por'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Devoted Hero'),
    (select id from sets where short_name = 'por'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thundering Wurm'),
    (select id from sets where short_name = 'por'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Panther Warriors'),
    (select id from sets where short_name = 'por'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'por'),
    '208s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'por'),
    '199s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Natural Spring'),
    (select id from sets where short_name = 'por'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hulking Goblin'),
    (select id from sets where short_name = 'por'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'por'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Starlit Angel'),
    (select id from sets where short_name = 'por'),
    '30s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fire Tempest'),
    (select id from sets where short_name = 'por'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warrior''s Charge'),
    (select id from sets where short_name = 'por'),
    '38†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Hammer'),
    (select id from sets where short_name = 'por'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cruel Bargain'),
    (select id from sets where short_name = 'por'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mobilize'),
    (select id from sets where short_name = 'por'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloak of Feathers'),
    (select id from sets where short_name = 'por'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Border Guard'),
    (select id from sets where short_name = 'por'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Shred'),
    (select id from sets where short_name = 'por'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight Errant'),
    (select id from sets where short_name = 'por'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undying Beast'),
    (select id from sets where short_name = 'por'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Defiant Stand'),
    (select id from sets where short_name = 'por'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Exhaustion'),
    (select id from sets where short_name = 'por'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampiric Feast'),
    (select id from sets where short_name = 'por'),
    '114s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scorching Spear'),
    (select id from sets where short_name = 'por'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'por'),
    '200s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloud Pirates'),
    (select id from sets where short_name = 'por'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tidal Surge'),
    (select id from sets where short_name = 'por'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'por'),
    '196s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dry Spell'),
    (select id from sets where short_name = 'por'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'por'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whiptail Wurm'),
    (select id from sets where short_name = 'por'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Path of Peace'),
    (select id from sets where short_name = 'por'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howling Fury'),
    (select id from sets where short_name = 'por'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horned Turtle'),
    (select id from sets where short_name = 'por'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armored Pegasus'),
    (select id from sets where short_name = 'por'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flashfires'),
    (select id from sets where short_name = 'por'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winter''s Grasp'),
    (select id from sets where short_name = 'por'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'por'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'por'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Noxious Toad'),
    (select id from sets where short_name = 'por'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'por'),
    '203s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wood Elves'),
    (select id from sets where short_name = 'por'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Assassin''s Blade'),
    (select id from sets where short_name = 'por'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'por'),
    '209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Capricious Sorcerer'),
    (select id from sets where short_name = 'por'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = 'por'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'por'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'por'),
    '212s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Breath of Life'),
    (select id from sets where short_name = 'por'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wicked Pact'),
    (select id from sets where short_name = 'por'),
    '117s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ingenious Thief'),
    (select id from sets where short_name = 'por'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fire Dragon'),
    (select id from sets where short_name = 'por'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'por'),
    '202s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serpent Warrior'),
    (select id from sets where short_name = 'por'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feral Shadow'),
    (select id from sets where short_name = 'por'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minotaur Warrior'),
    (select id from sets where short_name = 'por'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vengeance'),
    (select id from sets where short_name = 'por'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thing from the Deep'),
    (select id from sets where short_name = 'por'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steadfastness'),
    (select id from sets where short_name = 'por'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stalking Tiger'),
    (select id from sets where short_name = 'por'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloud Spirit'),
    (select id from sets where short_name = 'por'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'por'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Natural Order'),
    (select id from sets where short_name = 'por'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancestral Memories'),
    (select id from sets where short_name = 'por'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boiling Seas'),
    (select id from sets where short_name = 'por'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Knives'),
    (select id from sets where short_name = 'por'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'por'),
    '213s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Muck Rats'),
    (select id from sets where short_name = 'por'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'False Peace'),
    (select id from sets where short_name = 'por'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'por'),
    '201s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Snake'),
    (select id from sets where short_name = 'por'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Craven Knight'),
    (select id from sets where short_name = 'por'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nature''s Lore'),
    (select id from sets where short_name = 'por'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'por'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'por'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Owl Familiar'),
    (select id from sets where short_name = 'por'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Dragon'),
    (select id from sets where short_name = 'por'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Regal Unicorn'),
    (select id from sets where short_name = 'por'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'por'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyroclasm'),
    (select id from sets where short_name = 'por'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm Crow'),
    (select id from sets where short_name = 'por'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = 'por'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burning Cloak'),
    (select id from sets where short_name = 'por'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wicked Pact'),
    (select id from sets where short_name = 'por'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Renewing Dawn'),
    (select id from sets where short_name = 'por'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Time Ebb'),
    (select id from sets where short_name = 'por'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Command of Unsummoning'),
    (select id from sets where short_name = 'por'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fruition'),
    (select id from sets where short_name = 'por'),
    '166s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arrogant Vampire'),
    (select id from sets where short_name = 'por'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Man-o''-War'),
    (select id from sets where short_name = 'por'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Redwood Treefolk'),
    (select id from sets where short_name = 'por'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'por'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Imp'),
    (select id from sets where short_name = 'por'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'por'),
    '197s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spitting Earth'),
    (select id from sets where short_name = 'por'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dread Reaper'),
    (select id from sets where short_name = 'por'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bog Raiders'),
    (select id from sets where short_name = 'por'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skeletal Crocodile'),
    (select id from sets where short_name = 'por'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'por'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mercenary Knight'),
    (select id from sets where short_name = 'por'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Final Strike'),
    (select id from sets where short_name = 'por'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sorcerous Sight'),
    (select id from sets where short_name = 'por'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dread Charge'),
    (select id from sets where short_name = 'por'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moon Sprite'),
    (select id from sets where short_name = 'por'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blaze'),
    (select id from sets where short_name = 'por'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Horned Turtle'),
    (select id from sets where short_name = 'por'),
    '57s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hulking Cyclops'),
    (select id from sets where short_name = 'por'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lava Axe'),
    (select id from sets where short_name = 'por'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deep-Sea Serpent'),
    (select id from sets where short_name = 'por'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Taunt'),
    (select id from sets where short_name = 'por'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'por'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treetop Defense'),
    (select id from sets where short_name = 'por'),
    '190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Starlight'),
    (select id from sets where short_name = 'por'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'por'),
    '214s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raging Cougar'),
    (select id from sets where short_name = 'por'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raging Goblin'),
    (select id from sets where short_name = 'por'),
    '145',
    'common'
) 
 on conflict do nothing;
