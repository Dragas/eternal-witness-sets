insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Roar of the Wurm'),
    (select id from sets where short_name = 'f07'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cabal Coffers'),
    (select id from sets where short_name = 'f07'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deep Analysis'),
    (select id from sets where short_name = 'f07'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wing Shards'),
    (select id from sets where short_name = 'f07'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wonder'),
    (select id from sets where short_name = 'f07'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firebolt'),
    (select id from sets where short_name = 'f07'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Force Spike'),
    (select id from sets where short_name = 'f07'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Basking Rootwalla'),
    (select id from sets where short_name = 'f07'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gerrard''s Verdict'),
    (select id from sets where short_name = 'f07'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Ringleader'),
    (select id from sets where short_name = 'f07'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Legionnaire'),
    (select id from sets where short_name = 'f07'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Engineered Plague'),
    (select id from sets where short_name = 'f07'),
    '7',
    'rare'
) 
 on conflict do nothing;
