insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tdvd'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrull'),
    (select id from sets where short_name = 'tdvd'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon'),
    (select id from sets where short_name = 'tdvd'),
    '6',
    'common'
) 
 on conflict do nothing;
