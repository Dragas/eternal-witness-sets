insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'te01'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'te01'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horror'),
    (select id from sets where short_name = 'te01'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'te01'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'te01'),
    '4',
    'common'
) 
 on conflict do nothing;
