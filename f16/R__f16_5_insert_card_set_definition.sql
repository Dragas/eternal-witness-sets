insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Call the Bloodline'),
    (select id from sets where short_name = 'f16'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flaying Tendrils'),
    (select id from sets where short_name = 'f16'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Warchief'),
    (select id from sets where short_name = 'f16'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clash of Wills'),
    (select id from sets where short_name = 'f16'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crumbling Vestige'),
    (select id from sets where short_name = 'f16'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Scrying'),
    (select id from sets where short_name = 'f16'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blighted Fen'),
    (select id from sets where short_name = 'f16'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smash to Smithereens'),
    (select id from sets where short_name = 'f16'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiery Temper'),
    (select id from sets where short_name = 'f16'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rise from the Tides'),
    (select id from sets where short_name = 'f16'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spatial Contortion'),
    (select id from sets where short_name = 'f16'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa''s Pilgrimage'),
    (select id from sets where short_name = 'f16'),
    '1',
    'rare'
) 
 on conflict do nothing;
