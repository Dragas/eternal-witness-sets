insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Yule Ooze'),
    (select id from sets where short_name = 'hho'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stocking Tiger'),
    (select id from sets where short_name = 'hho'),
    '13†',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Sleigh Ride'),
    (select id from sets where short_name = 'hho'),
    '15',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Some Disassembly Required'),
    (select id from sets where short_name = 'hho'),
    '17',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stocking Tiger'),
    (select id from sets where short_name = 'hho'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Decorated Knight // Present Arms'),
    (select id from sets where short_name = 'hho'),
    '19',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Naughty // Nice'),
    (select id from sets where short_name = 'hho'),
    '12a',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thopter Pie Network'),
    (select id from sets where short_name = 'hho'),
    '16',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bog Humbugs'),
    (select id from sets where short_name = 'hho'),
    '18',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Evil Presents'),
    (select id from sets where short_name = 'hho'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gifts Given'),
    (select id from sets where short_name = 'hho'),
    '7†',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gifts Given'),
    (select id from sets where short_name = 'hho'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snow Mercy'),
    (select id from sets where short_name = 'hho'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fruitcake Elemental'),
    (select id from sets where short_name = 'hho'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Toy Workshop'),
    (select id from sets where short_name = 'hho'),
    '14',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Season''s Beatings'),
    (select id from sets where short_name = 'hho'),
    '9',
    'rare'
) 
 on conflict do nothing;
