insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddq'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddq'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Geist of Saint Traft'),
    (select id from sets where short_name = 'ddq'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Voice of the Provinces'),
    (select id from sets where short_name = 'ddq'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Human'),
    (select id from sets where short_name = 'ddq'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dearly Departed'),
    (select id from sets where short_name = 'ddq'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiend Hunter'),
    (select id from sets where short_name = 'ddq'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thraben Heretic'),
    (select id from sets where short_name = 'ddq'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slayer of the Wicked'),
    (select id from sets where short_name = 'ddq'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elder Cathar'),
    (select id from sets where short_name = 'ddq'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tower Geist'),
    (select id from sets where short_name = 'ddq'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Village Bell-Ringer'),
    (select id from sets where short_name = 'ddq'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tandem Lookout'),
    (select id from sets where short_name = 'ddq'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sharpened Pitchfork'),
    (select id from sets where short_name = 'ddq'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chapel Geist'),
    (select id from sets where short_name = 'ddq'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Butcher Ghoul'),
    (select id from sets where short_name = 'ddq'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravecrawler'),
    (select id from sets where short_name = 'ddq'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Screeching Skaab'),
    (select id from sets where short_name = 'ddq'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stitched Drake'),
    (select id from sets where short_name = 'ddq'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abattoir Ghoul'),
    (select id from sets where short_name = 'ddq'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mist Raven'),
    (select id from sets where short_name = 'ddq'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddq'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gryff Vanguard'),
    (select id from sets where short_name = 'ddq'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Falkenrath Noble'),
    (select id from sets where short_name = 'ddq'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diregraf Ghoul'),
    (select id from sets where short_name = 'ddq'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquil Cove'),
    (select id from sets where short_name = 'ddq'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddq'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cobbled Wings'),
    (select id from sets where short_name = 'ddq'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddq'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scrapskin Drake'),
    (select id from sets where short_name = 'ddq'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gather the Townsfolk'),
    (select id from sets where short_name = 'ddq'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emancipation Angel'),
    (select id from sets where short_name = 'ddq'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'ddq'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relentless Skaabs'),
    (select id from sets where short_name = 'ddq'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Human Frailty'),
    (select id from sets where short_name = 'ddq'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doomed Traveler'),
    (select id from sets where short_name = 'ddq'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Makeshift Mauler'),
    (select id from sets where short_name = 'ddq'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moorland Inquisitor'),
    (select id from sets where short_name = 'ddq'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'ddq'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spectral Gateguards'),
    (select id from sets where short_name = 'ddq'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Butcher''s Cleaver'),
    (select id from sets where short_name = 'ddq'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goldnight Redeemer'),
    (select id from sets where short_name = 'ddq'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forbidden Alchemy'),
    (select id from sets where short_name = 'ddq'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sever the Bloodline'),
    (select id from sets where short_name = 'ddq'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rebuke'),
    (select id from sets where short_name = 'ddq'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tooth Collector'),
    (select id from sets where short_name = 'ddq'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddq'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dismal Backwater'),
    (select id from sets where short_name = 'ddq'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nephalia Smuggler'),
    (select id from sets where short_name = 'ddq'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pore Over the Pages'),
    (select id from sets where short_name = 'ddq'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eerie Interlude'),
    (select id from sets where short_name = 'ddq'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Captain of the Mists'),
    (select id from sets where short_name = 'ddq'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddq'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghoulraiser'),
    (select id from sets where short_name = 'ddq'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Momentary Blink'),
    (select id from sets where short_name = 'ddq'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddq'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'ddq'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harvester of Souls'),
    (select id from sets where short_name = 'ddq'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moan of the Unhallowed'),
    (select id from sets where short_name = 'ddq'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Increasing Devotion'),
    (select id from sets where short_name = 'ddq'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Havengul Runebinder'),
    (select id from sets where short_name = 'ddq'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Victim of Night'),
    (select id from sets where short_name = 'ddq'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddq'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Topplegeist'),
    (select id from sets where short_name = 'ddq'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barter in Blood'),
    (select id from sets where short_name = 'ddq'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bonds of Faith'),
    (select id from sets where short_name = 'ddq'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Compelling Deterrence'),
    (select id from sets where short_name = 'ddq'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unbreathing Horde'),
    (select id from sets where short_name = 'ddq'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mindwrack Demon'),
    (select id from sets where short_name = 'ddq'),
    '41',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cathedral Sanctifier'),
    (select id from sets where short_name = 'ddq'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seraph Sanctuary'),
    (select id from sets where short_name = 'ddq'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddq'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Champion of the Parish'),
    (select id from sets where short_name = 'ddq'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Appetite for Brains'),
    (select id from sets where short_name = 'ddq'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddq'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddq'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diregraf Captain'),
    (select id from sets where short_name = 'ddq'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Driver of the Dead'),
    (select id from sets where short_name = 'ddq'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tribute to Hunger'),
    (select id from sets where short_name = 'ddq'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dread Return'),
    (select id from sets where short_name = 'ddq'),
    '55',
    'uncommon'
) 
 on conflict do nothing;
