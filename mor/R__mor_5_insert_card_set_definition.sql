insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Earwig Squad'),
    (select id from sets where short_name = 'mor'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cream of the Crop'),
    (select id from sets where short_name = 'mor'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Everbark Shaman'),
    (select id from sets where short_name = 'mor'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunting Triad'),
    (select id from sets where short_name = 'mor'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inspired Sprite'),
    (select id from sets where short_name = 'mor'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feudkiller''s Verdict'),
    (select id from sets where short_name = 'mor'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Idyllic Tutor'),
    (select id from sets where short_name = 'mor'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disperse'),
    (select id from sets where short_name = 'mor'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Recross the Paths'),
    (select id from sets where short_name = 'mor'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fendeep Summoner'),
    (select id from sets where short_name = 'mor'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swell of Courage'),
    (select id from sets where short_name = 'mor'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boldwyr Intimidator'),
    (select id from sets where short_name = 'mor'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyroclast Consul'),
    (select id from sets where short_name = 'mor'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oona''s Blackguard'),
    (select id from sets where short_name = 'mor'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rustic Clachan'),
    (select id from sets where short_name = 'mor'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burrenton Shield-Bearers'),
    (select id from sets where short_name = 'mor'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Release the Ants'),
    (select id from sets where short_name = 'mor'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scarblade Elite'),
    (select id from sets where short_name = 'mor'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bosk Banneret'),
    (select id from sets where short_name = 'mor'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diviner''s Wand'),
    (select id from sets where short_name = 'mor'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stingmoggie'),
    (select id from sets where short_name = 'mor'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Borderland Behemoth'),
    (select id from sets where short_name = 'mor'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Supreme Exemplar'),
    (select id from sets where short_name = 'mor'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rivals'' Duel'),
    (select id from sets where short_name = 'mor'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Declaration of Naught'),
    (select id from sets where short_name = 'mor'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slithermuse'),
    (select id from sets where short_name = 'mor'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stonehewer Giant'),
    (select id from sets where short_name = 'mor'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sage of Fables'),
    (select id from sets where short_name = 'mor'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Graceful Reprieve'),
    (select id from sets where short_name = 'mor'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cloak and Dagger'),
    (select id from sets where short_name = 'mor'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bramblewood Paragon'),
    (select id from sets where short_name = 'mor'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weight of Conscience'),
    (select id from sets where short_name = 'mor'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knowledge Exploitation'),
    (select id from sets where short_name = 'mor'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stonybrook Banneret'),
    (select id from sets where short_name = 'mor'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reins of the Vinesteed'),
    (select id from sets where short_name = 'mor'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Preeminent Captain'),
    (select id from sets where short_name = 'mor'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shard Volley'),
    (select id from sets where short_name = 'mor'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spitebellows'),
    (select id from sets where short_name = 'mor'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daily Regimen'),
    (select id from sets where short_name = 'mor'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Earthbrawn'),
    (select id from sets where short_name = 'mor'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seething Pathblazer'),
    (select id from sets where short_name = 'mor'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stinkdrinker Bandit'),
    (select id from sets where short_name = 'mor'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chameleon Colossus'),
    (select id from sets where short_name = 'mor'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stream of Unconsciousness'),
    (select id from sets where short_name = 'mor'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prickly Boggart'),
    (select id from sets where short_name = 'mor'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reveillark'),
    (select id from sets where short_name = 'mor'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grimoire Thief'),
    (select id from sets where short_name = 'mor'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Distant Melody'),
    (select id from sets where short_name = 'mor'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'mor'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Order of the Golden Cricket'),
    (select id from sets where short_name = 'mor'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mosquito Guard'),
    (select id from sets where short_name = 'mor'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Luminescent Rain'),
    (select id from sets where short_name = 'mor'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Festercreep'),
    (select id from sets where short_name = 'mor'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Offalsnout'),
    (select id from sets where short_name = 'mor'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dewdrop Spy'),
    (select id from sets where short_name = 'mor'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moonglove Changeling'),
    (select id from sets where short_name = 'mor'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stonybrook Schoolmaster'),
    (select id from sets where short_name = 'mor'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Redeem the Lost'),
    (select id from sets where short_name = 'mor'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pulling Teeth'),
    (select id from sets where short_name = 'mor'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunflare Shaman'),
    (select id from sets where short_name = 'mor'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ink Dissolver'),
    (select id from sets where short_name = 'mor'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Notorious Throng'),
    (select id from sets where short_name = 'mor'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Floodchaser'),
    (select id from sets where short_name = 'mor'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reach of Branches'),
    (select id from sets where short_name = 'mor'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sage''s Dousing'),
    (select id from sets where short_name = 'mor'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fencer Clique'),
    (select id from sets where short_name = 'mor'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frogtosser Banneret'),
    (select id from sets where short_name = 'mor'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pack''s Disdain'),
    (select id from sets where short_name = 'mor'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orchard Warden'),
    (select id from sets where short_name = 'mor'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Spring'),
    (select id from sets where short_name = 'mor'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Game-Trail Changeling'),
    (select id from sets where short_name = 'mor'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sigil Tracer'),
    (select id from sets where short_name = 'mor'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Murmuring Bosk'),
    (select id from sets where short_name = 'mor'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Waterspout Weavers'),
    (select id from sets where short_name = 'mor'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wolf-Skull Shaman'),
    (select id from sets where short_name = 'mor'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Squeaking Pie Grubfellows'),
    (select id from sets where short_name = 'mor'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bitterblossom'),
    (select id from sets where short_name = 'mor'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thieves'' Fortune'),
    (select id from sets where short_name = 'mor'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stomping Slabs'),
    (select id from sets where short_name = 'mor'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kinsbaile Borderguard'),
    (select id from sets where short_name = 'mor'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scapeshift'),
    (select id from sets where short_name = 'mor'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shinewend'),
    (select id from sets where short_name = 'mor'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kithkin Zephyrnaut'),
    (select id from sets where short_name = 'mor'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boldwyr Heavyweights'),
    (select id from sets where short_name = 'mor'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Revive the Fallen'),
    (select id from sets where short_name = 'mor'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mutavault'),
    (select id from sets where short_name = 'mor'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Countryside Crusher'),
    (select id from sets where short_name = 'mor'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coordinated Barrage'),
    (select id from sets where short_name = 'mor'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ambassador Oak'),
    (select id from sets where short_name = 'mor'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'War-Spike Changeling'),
    (select id from sets where short_name = 'mor'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Research the Deep'),
    (select id from sets where short_name = 'mor'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Noggin Whack'),
    (select id from sets where short_name = 'mor'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mothdust Changeling'),
    (select id from sets where short_name = 'mor'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deglamer'),
    (select id from sets where short_name = 'mor'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kindled Fury'),
    (select id from sets where short_name = 'mor'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battletide Alchemist'),
    (select id from sets where short_name = 'mor'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burrenton Bombardier'),
    (select id from sets where short_name = 'mor'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ballyrush Banneret'),
    (select id from sets where short_name = 'mor'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greatbow Doyen'),
    (select id from sets where short_name = 'mor'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forfend'),
    (select id from sets where short_name = 'mor'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winnower Patrol'),
    (select id from sets where short_name = 'mor'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Veteran''s Armaments'),
    (select id from sets where short_name = 'mor'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heritage Druid'),
    (select id from sets where short_name = 'mor'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meadowboon'),
    (select id from sets where short_name = 'mor'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gilt-Leaf Archdruid'),
    (select id from sets where short_name = 'mor'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Door of Destinies'),
    (select id from sets where short_name = 'mor'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fertilid'),
    (select id from sets where short_name = 'mor'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightshade Schemers'),
    (select id from sets where short_name = 'mor'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warren Weirding'),
    (select id from sets where short_name = 'mor'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Titan''s Revenge'),
    (select id from sets where short_name = 'mor'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Warrior'),
    (select id from sets where short_name = 'mor'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sensation Gorger'),
    (select id from sets where short_name = 'mor'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primal Beyond'),
    (select id from sets where short_name = 'mor'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maralen of the Mornsong'),
    (select id from sets where short_name = 'mor'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mudbutton Clanger'),
    (select id from sets where short_name = 'mor'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Violet Pall'),
    (select id from sets where short_name = 'mor'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thornbite Staff'),
    (select id from sets where short_name = 'mor'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stenchskipper'),
    (select id from sets where short_name = 'mor'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blightsoil Druid'),
    (select id from sets where short_name = 'mor'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leaf-Crowned Elder'),
    (select id from sets where short_name = 'mor'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Obsidian Battle-Axe'),
    (select id from sets where short_name = 'mor'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unstoppable Ash'),
    (select id from sets where short_name = 'mor'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Morsel Theft'),
    (select id from sets where short_name = 'mor'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brighthearth Banneret'),
    (select id from sets where short_name = 'mor'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weed-Pruner Poplar'),
    (select id from sets where short_name = 'mor'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Taurean Mauler'),
    (select id from sets where short_name = 'mor'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merrow Witsniper'),
    (select id from sets where short_name = 'mor'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cenn''s Tactician'),
    (select id from sets where short_name = 'mor'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Shatter'),
    (select id from sets where short_name = 'mor'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Auntie''s Snitch'),
    (select id from sets where short_name = 'mor'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Indomitable Ancients'),
    (select id from sets where short_name = 'mor'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hostile Realm'),
    (select id from sets where short_name = 'mor'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nevermaker'),
    (select id from sets where short_name = 'mor'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Walker of the Grove'),
    (select id from sets where short_name = 'mor'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Roar of the Crowd'),
    (select id from sets where short_name = 'mor'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vengeful Firebrand'),
    (select id from sets where short_name = 'mor'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wandering Graybeard'),
    (select id from sets where short_name = 'mor'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kinsbaile Cavalier'),
    (select id from sets where short_name = 'mor'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Latchkey Faerie'),
    (select id from sets where short_name = 'mor'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Juggler'),
    (select id from sets where short_name = 'mor'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhys the Exiled'),
    (select id from sets where short_name = 'mor'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lunk Errant'),
    (select id from sets where short_name = 'mor'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vendilion Clique'),
    (select id from sets where short_name = 'mor'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Crafter'),
    (select id from sets where short_name = 'mor'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lys Alana Bowmaster'),
    (select id from sets where short_name = 'mor'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Final-Sting Faerie'),
    (select id from sets where short_name = 'mor'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rage Forger'),
    (select id from sets where short_name = 'mor'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weirding Shaman'),
    (select id from sets where short_name = 'mor'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shared Animosity'),
    (select id from sets where short_name = 'mor'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Changeling Sentinel'),
    (select id from sets where short_name = 'mor'),
    '6',
    'common'
) 
 on conflict do nothing;
