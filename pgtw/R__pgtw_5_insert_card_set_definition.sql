insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Icatian Javelineers'),
    (select id from sets where short_name = 'pgtw'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiery Temper'),
    (select id from sets where short_name = 'pgtw'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wood Elves'),
    (select id from sets where short_name = 'pgtw'),
    '1',
    'rare'
) 
 on conflict do nothing;
