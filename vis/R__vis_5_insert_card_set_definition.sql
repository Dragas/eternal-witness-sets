insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Fallen Askari'),
    (select id from sets where short_name = 'vis'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viashino Sandstalker'),
    (select id from sets where short_name = 'vis'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Infantry Veteran'),
    (select id from sets where short_name = 'vis'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chronatog'),
    (select id from sets where short_name = 'vis'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rowen'),
    (select id from sets where short_name = 'vis'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dwarven Vigilantes'),
    (select id from sets where short_name = 'vis'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raging Gorilla'),
    (select id from sets where short_name = 'vis'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Natural Order'),
    (select id from sets where short_name = 'vis'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampiric Tutor'),
    (select id from sets where short_name = 'vis'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wand of Denial'),
    (select id from sets where short_name = 'vis'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death Watch'),
    (select id from sets where short_name = 'vis'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Uktabi Orangutan'),
    (select id from sets where short_name = 'vis'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Privilege'),
    (select id from sets where short_name = 'vis'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quicksand'),
    (select id from sets where short_name = 'vis'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Puzzle Box'),
    (select id from sets where short_name = 'vis'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Time and Tide'),
    (select id from sets where short_name = 'vis'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pillar Tombs of Aku'),
    (select id from sets where short_name = 'vis'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corrosion'),
    (select id from sets where short_name = 'vis'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magma Mine'),
    (select id from sets where short_name = 'vis'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Suq''Ata Assassin'),
    (select id from sets where short_name = 'vis'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rainbow Efreet'),
    (select id from sets where short_name = 'vis'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brass-Talon Chimera'),
    (select id from sets where short_name = 'vis'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Femeref Enchantress'),
    (select id from sets where short_name = 'vis'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feral Instinct'),
    (select id from sets where short_name = 'vis'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Resistance Fighter'),
    (select id from sets where short_name = 'vis'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Peace Talks'),
    (select id from sets where short_name = 'vis'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shimmering Efreet'),
    (select id from sets where short_name = 'vis'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bogardan Phoenix'),
    (select id from sets where short_name = 'vis'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knight of Valor'),
    (select id from sets where short_name = 'vis'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Suq''Ata Lancer'),
    (select id from sets where short_name = 'vis'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Marauder'),
    (select id from sets where short_name = 'vis'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Remedy'),
    (select id from sets where short_name = 'vis'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undo'),
    (select id from sets where short_name = 'vis'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampirism'),
    (select id from sets where short_name = 'vis'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pygmy Hippo'),
    (select id from sets where short_name = 'vis'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'River Boa'),
    (select id from sets where short_name = 'vis'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daraja Griffin'),
    (select id from sets where short_name = 'vis'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brood of Cockroaches'),
    (select id from sets where short_name = 'vis'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necromancy'),
    (select id from sets where short_name = 'vis'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Talruum Champion'),
    (select id from sets where short_name = 'vis'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tin-Wing Chimera'),
    (select id from sets where short_name = 'vis'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Freewind Falcon'),
    (select id from sets where short_name = 'vis'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Summer Bloom'),
    (select id from sets where short_name = 'vis'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Archangel'),
    (select id from sets where short_name = 'vis'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aku Djinn'),
    (select id from sets where short_name = 'vis'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Creeping Mold'),
    (select id from sets where short_name = 'vis'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight of the Mists'),
    (select id from sets where short_name = 'vis'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'King Cheetah'),
    (select id from sets where short_name = 'vis'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Suleiman''s Legacy'),
    (select id from sets where short_name = 'vis'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desolation'),
    (select id from sets where short_name = 'vis'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dream Tides'),
    (select id from sets where short_name = 'vis'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prosperity'),
    (select id from sets where short_name = 'vis'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Honor Guard'),
    (select id from sets where short_name = 'vis'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Helm of Awakening'),
    (select id from sets where short_name = 'vis'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heat Wave'),
    (select id from sets where short_name = 'vis'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Song of Blood'),
    (select id from sets where short_name = 'vis'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Swine-Rider'),
    (select id from sets where short_name = 'vis'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flooded Shoreline'),
    (select id from sets where short_name = 'vis'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Triangle of War'),
    (select id from sets where short_name = 'vis'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Man-o''-War'),
    (select id from sets where short_name = 'vis'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloud Elemental'),
    (select id from sets where short_name = 'vis'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wake of Vultures'),
    (select id from sets where short_name = 'vis'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inspiration'),
    (select id from sets where short_name = 'vis'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Equipoise'),
    (select id from sets where short_name = 'vis'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diamond Kaleidoscope'),
    (select id from sets where short_name = 'vis'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tempest Drake'),
    (select id from sets where short_name = 'vis'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quirion Ranger'),
    (select id from sets where short_name = 'vis'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iron-Heart Chimera'),
    (select id from sets where short_name = 'vis'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coral Atoll'),
    (select id from sets where short_name = 'vis'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lead-Belly Chimera'),
    (select id from sets where short_name = 'vis'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Undiscovered Paradise'),
    (select id from sets where short_name = 'vis'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guiding Spirit'),
    (select id from sets where short_name = 'vis'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shrieking Drake'),
    (select id from sets where short_name = 'vis'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simoon'),
    (select id from sets where short_name = 'vis'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Honorable Passage'),
    (select id from sets where short_name = 'vis'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Waterspout Djinn'),
    (select id from sets where short_name = 'vis'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dormant Volcano'),
    (select id from sets where short_name = 'vis'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Recruiter'),
    (select id from sets where short_name = 'vis'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Griffin Canyon'),
    (select id from sets where short_name = 'vis'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viashivan Dragon'),
    (select id from sets where short_name = 'vis'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crypt Rats'),
    (select id from sets where short_name = 'vis'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relentless Assault'),
    (select id from sets where short_name = 'vis'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Panther Warriors'),
    (select id from sets where short_name = 'vis'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lichenthrope'),
    (select id from sets where short_name = 'vis'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Miraculous Recovery'),
    (select id from sets where short_name = 'vis'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urborg Mindsucker'),
    (select id from sets where short_name = 'vis'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vision Charm'),
    (select id from sets where short_name = 'vis'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warrior''s Honor'),
    (select id from sets where short_name = 'vis'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Veil'),
    (select id from sets where short_name = 'vis'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rock Slide'),
    (select id from sets where short_name = 'vis'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Army Ants'),
    (select id from sets where short_name = 'vis'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Betrayal'),
    (select id from sets where short_name = 'vis'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Cloud'),
    (select id from sets where short_name = 'vis'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anvil of Bogardan'),
    (select id from sets where short_name = 'vis'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mortal Wound'),
    (select id from sets where short_name = 'vis'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elven Cache'),
    (select id from sets where short_name = 'vis'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Retribution of the Meek'),
    (select id from sets where short_name = 'vis'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quirion Druid'),
    (select id from sets where short_name = 'vis'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vanishing'),
    (select id from sets where short_name = 'vis'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kookus'),
    (select id from sets where short_name = 'vis'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Matopi Golem'),
    (select id from sets where short_name = 'vis'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Katabatic Winds'),
    (select id from sets where short_name = 'vis'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desertion'),
    (select id from sets where short_name = 'vis'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Impulse'),
    (select id from sets where short_name = 'vis'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Walker'),
    (select id from sets where short_name = 'vis'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sisay''s Ring'),
    (select id from sets where short_name = 'vis'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spider Climb'),
    (select id from sets where short_name = 'vis'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hope Charm'),
    (select id from sets where short_name = 'vis'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hulking Cyclops'),
    (select id from sets where short_name = 'vis'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Juju Bubble'),
    (select id from sets where short_name = 'vis'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sands of Time'),
    (select id from sets where short_name = 'vis'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Parapet'),
    (select id from sets where short_name = 'vis'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tremor'),
    (select id from sets where short_name = 'vis'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tithe'),
    (select id from sets where short_name = 'vis'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Realm'),
    (select id from sets where short_name = 'vis'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ovinomancer'),
    (select id from sets where short_name = 'vis'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ogre Enforcer'),
    (select id from sets where short_name = 'vis'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fireblast'),
    (select id from sets where short_name = 'vis'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scalebane''s Elite'),
    (select id from sets where short_name = 'vis'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kyscu Drake'),
    (select id from sets where short_name = 'vis'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Caterpillar'),
    (select id from sets where short_name = 'vis'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wind Shear'),
    (select id from sets where short_name = 'vis'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Righteous War'),
    (select id from sets where short_name = 'vis'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elkin Lair'),
    (select id from sets where short_name = 'vis'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'City of Solitude'),
    (select id from sets where short_name = 'vis'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Breezekeeper'),
    (select id from sets where short_name = 'vis'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jamuraan Lion'),
    (select id from sets where short_name = 'vis'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snake Basket'),
    (select id from sets where short_name = 'vis'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Everglades'),
    (select id from sets where short_name = 'vis'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Solfatara'),
    (select id from sets where short_name = 'vis'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spitting Drake'),
    (select id from sets where short_name = 'vis'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Talruum Piper'),
    (select id from sets where short_name = 'vis'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hearth Charm'),
    (select id from sets where short_name = 'vis'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Basin'),
    (select id from sets where short_name = 'vis'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Infernal Harvest'),
    (select id from sets where short_name = 'vis'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keeper of Kookus'),
    (select id from sets where short_name = 'vis'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kaervek''s Spite'),
    (select id from sets where short_name = 'vis'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foreshadow'),
    (select id from sets where short_name = 'vis'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mundungu'),
    (select id from sets where short_name = 'vis'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karoo'),
    (select id from sets where short_name = 'vis'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zhalfirin Crusader'),
    (select id from sets where short_name = 'vis'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nekrataal'),
    (select id from sets where short_name = 'vis'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Three Wishes'),
    (select id from sets where short_name = 'vis'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warthog'),
    (select id from sets where short_name = 'vis'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stampeding Wildebeests'),
    (select id from sets where short_name = 'vis'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firestorm Hellkite'),
    (select id from sets where short_name = 'vis'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tar Pit Warrior'),
    (select id from sets where short_name = 'vis'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Longbow Archer'),
    (select id from sets where short_name = 'vis'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sun Clasp'),
    (select id from sets where short_name = 'vis'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mob Mentality'),
    (select id from sets where short_name = 'vis'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necrosavant'),
    (select id from sets where short_name = 'vis'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Funeral Charm'),
    (select id from sets where short_name = 'vis'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Python'),
    (select id from sets where short_name = 'vis'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gossamer Chains'),
    (select id from sets where short_name = 'vis'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emerald Charm'),
    (select id from sets where short_name = 'vis'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wicked Reward'),
    (select id from sets where short_name = 'vis'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relic Ward'),
    (select id from sets where short_name = 'vis'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Mask'),
    (select id from sets where short_name = 'vis'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coercion'),
    (select id from sets where short_name = 'vis'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elephant Grass'),
    (select id from sets where short_name = 'vis'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forbidden Ritual'),
    (select id from sets where short_name = 'vis'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Breathstealer''s Crypt'),
    (select id from sets where short_name = 'vis'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bull Elephant'),
    (select id from sets where short_name = 'vis'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eye of Singularity'),
    (select id from sets where short_name = 'vis'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Squandered Resources'),
    (select id from sets where short_name = 'vis'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blanket of Night'),
    (select id from sets where short_name = 'vis'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Righteous Aura'),
    (select id from sets where short_name = 'vis'),
    '20',
    'common'
) 
 on conflict do nothing;
