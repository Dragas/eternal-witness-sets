insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Whirlpool Warrior'),
    (select id from sets where short_name = 'apc'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dwarven Patrol'),
    (select id from sets where short_name = 'apc'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Minotaur Tactician'),
    (select id from sets where short_name = 'apc'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guided Passage'),
    (select id from sets where short_name = 'apc'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whirlpool Rider'),
    (select id from sets where short_name = 'apc'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coalition Flag'),
    (select id from sets where short_name = 'apc'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death Grasp'),
    (select id from sets where short_name = 'apc'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Last Caress'),
    (select id from sets where short_name = 'apc'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spectral Lynx'),
    (select id from sets where short_name = 'apc'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodfire Kavu'),
    (select id from sets where short_name = 'apc'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Caves of Koilos'),
    (select id from sets where short_name = 'apc'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Order // Chaos'),
    (select id from sets where short_name = 'apc'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kavu Glider'),
    (select id from sets where short_name = 'apc'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'False Dawn'),
    (select id from sets where short_name = 'apc'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Coast'),
    (select id from sets where short_name = 'apc'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Putrid Warrior'),
    (select id from sets where short_name = 'apc'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fungal Shambler'),
    (select id from sets where short_name = 'apc'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urborg Elf'),
    (select id from sets where short_name = 'apc'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diversionary Tactics'),
    (select id from sets where short_name = 'apc'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zombie Boa'),
    (select id from sets where short_name = 'apc'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Trenches'),
    (select id from sets where short_name = 'apc'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Penumbra Bobcat'),
    (select id from sets where short_name = 'apc'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necravolver'),
    (select id from sets where short_name = 'apc'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodfire Infusion'),
    (select id from sets where short_name = 'apc'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vindicate'),
    (select id from sets where short_name = 'apc'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Squee''s Revenge'),
    (select id from sets where short_name = 'apc'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ebony Treefolk'),
    (select id from sets where short_name = 'apc'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquil Path'),
    (select id from sets where short_name = 'apc'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desolation Giant'),
    (select id from sets where short_name = 'apc'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savage Gorilla'),
    (select id from sets where short_name = 'apc'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ceta Sanctuary'),
    (select id from sets where short_name = 'apc'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Degavolver'),
    (select id from sets where short_name = 'apc'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yavimaya''s Embrace'),
    (select id from sets where short_name = 'apc'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enlistment Officer'),
    (select id from sets where short_name = 'apc'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Legacy Weapon'),
    (select id from sets where short_name = 'apc'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Helionaut'),
    (select id from sets where short_name = 'apc'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Planar Despair'),
    (select id from sets where short_name = 'apc'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Rager'),
    (select id from sets where short_name = 'apc'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Gnarr'),
    (select id from sets where short_name = 'apc'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raka Disciple'),
    (select id from sets where short_name = 'apc'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Link'),
    (select id from sets where short_name = 'apc'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakavolver'),
    (select id from sets where short_name = 'apc'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tahngarth''s Glare'),
    (select id from sets where short_name = 'apc'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desolation Angel'),
    (select id from sets where short_name = 'apc'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Gargantua'),
    (select id from sets where short_name = 'apc'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dwarven Landslide'),
    (select id from sets where short_name = 'apc'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smash'),
    (select id from sets where short_name = 'apc'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Mutation'),
    (select id from sets where short_name = 'apc'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shield of Duty and Reason'),
    (select id from sets where short_name = 'apc'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Barrier'),
    (select id from sets where short_name = 'apc'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodfire Colossus'),
    (select id from sets where short_name = 'apc'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gerrard''s Verdict'),
    (select id from sets where short_name = 'apc'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necra Sanctuary'),
    (select id from sets where short_name = 'apc'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quagmire Druid'),
    (select id from sets where short_name = 'apc'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evasive Action'),
    (select id from sets where short_name = 'apc'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anavolver'),
    (select id from sets where short_name = 'apc'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kavu Howler'),
    (select id from sets where short_name = 'apc'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llanowar Wastes'),
    (select id from sets where short_name = 'apc'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raka Sanctuary'),
    (select id from sets where short_name = 'apc'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Index'),
    (select id from sets where short_name = 'apc'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Suppress'),
    (select id from sets where short_name = 'apc'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Angel'),
    (select id from sets where short_name = 'apc'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cetavolver'),
    (select id from sets where short_name = 'apc'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jaded Response'),
    (select id from sets where short_name = 'apc'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whirlpool Drake'),
    (select id from sets where short_name = 'apc'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shimmering Mirage'),
    (select id from sets where short_name = 'apc'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Haunted Angel'),
    (select id from sets where short_name = 'apc'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kavu Mauler'),
    (select id from sets where short_name = 'apc'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prophetic Bolt'),
    (select id from sets where short_name = 'apc'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Life // Death'),
    (select id from sets where short_name = 'apc'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Foul Presence'),
    (select id from sets where short_name = 'apc'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Extraction'),
    (select id from sets where short_name = 'apc'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cromat'),
    (select id from sets where short_name = 'apc'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Last Stand'),
    (select id from sets where short_name = 'apc'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ana Disciple'),
    (select id from sets where short_name = 'apc'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tidal Courier'),
    (select id from sets where short_name = 'apc'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vodalian Mystic'),
    (select id from sets where short_name = 'apc'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quicksilver Dagger'),
    (select id from sets where short_name = 'apc'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Arena'),
    (select id from sets where short_name = 'apc'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodfire Dwarf'),
    (select id from sets where short_name = 'apc'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strength of Night'),
    (select id from sets where short_name = 'apc'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brass Herald'),
    (select id from sets where short_name = 'apc'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Skyfolk'),
    (select id from sets where short_name = 'apc'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illusion // Reality'),
    (select id from sets where short_name = 'apc'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Research'),
    (select id from sets where short_name = 'apc'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Divine Light'),
    (select id from sets where short_name = 'apc'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lay of the Land'),
    (select id from sets where short_name = 'apc'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urborg Uprising'),
    (select id from sets where short_name = 'apc'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emblazoned Golem'),
    (select id from sets where short_name = 'apc'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Razorfin Hunter'),
    (select id from sets where short_name = 'apc'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dead Ringers'),
    (select id from sets where short_name = 'apc'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mournful Zombie'),
    (select id from sets where short_name = 'apc'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ana Sanctuary'),
    (select id from sets where short_name = 'apc'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jilt'),
    (select id from sets where short_name = 'apc'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Symbiotic Deployment'),
    (select id from sets where short_name = 'apc'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angelfire Crusader'),
    (select id from sets where short_name = 'apc'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Martyrs'' Tomb'),
    (select id from sets where short_name = 'apc'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aether Mutation'),
    (select id from sets where short_name = 'apc'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battlefield Forge'),
    (select id from sets where short_name = 'apc'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Penumbra Wurm'),
    (select id from sets where short_name = 'apc'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pernicious Deed'),
    (select id from sets where short_name = 'apc'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necra Disciple'),
    (select id from sets where short_name = 'apc'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Living Airship'),
    (select id from sets where short_name = 'apc'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Powerstone Minefield'),
    (select id from sets where short_name = 'apc'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ceta Disciple'),
    (select id from sets where short_name = 'apc'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temporal Spring'),
    (select id from sets where short_name = 'apc'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Ringleader'),
    (select id from sets where short_name = 'apc'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dega Sanctuary'),
    (select id from sets where short_name = 'apc'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spiritmonger'),
    (select id from sets where short_name = 'apc'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overgrown Estate'),
    (select id from sets where short_name = 'apc'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Suffocating Blast'),
    (select id from sets where short_name = 'apc'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shivan Reef'),
    (select id from sets where short_name = 'apc'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unnatural Selection'),
    (select id from sets where short_name = 'apc'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flowstone Charger'),
    (select id from sets where short_name = 'apc'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coalition Honor Guard'),
    (select id from sets where short_name = 'apc'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Legionnaire'),
    (select id from sets where short_name = 'apc'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reef Shaman'),
    (select id from sets where short_name = 'apc'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gerrard Capashen'),
    (select id from sets where short_name = 'apc'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Illuminate'),
    (select id from sets where short_name = 'apc'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orim''s Thunder'),
    (select id from sets where short_name = 'apc'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fervent Charge'),
    (select id from sets where short_name = 'apc'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Minotaur Illusionist'),
    (select id from sets where short_name = 'apc'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dodecapod'),
    (select id from sets where short_name = 'apc'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Standard Bearer'),
    (select id from sets where short_name = 'apc'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squee''s Embrace'),
    (select id from sets where short_name = 'apc'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Night // Day'),
    (select id from sets where short_name = 'apc'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Penumbra Kavu'),
    (select id from sets where short_name = 'apc'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glade Gnarr'),
    (select id from sets where short_name = 'apc'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Arch'),
    (select id from sets where short_name = 'apc'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Consume Strength'),
    (select id from sets where short_name = 'apc'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Snake'),
    (select id from sets where short_name = 'apc'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Balance'),
    (select id from sets where short_name = 'apc'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llanowar Dead'),
    (select id from sets where short_name = 'apc'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manacles of Decay'),
    (select id from sets where short_name = 'apc'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mask of Intolerance'),
    (select id from sets where short_name = 'apc'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coastal Drake'),
    (select id from sets where short_name = 'apc'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grave Defiler'),
    (select id from sets where short_name = 'apc'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fire // Ice'),
    (select id from sets where short_name = 'apc'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Captain''s Maneuver'),
    (select id from sets where short_name = 'apc'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dega Disciple'),
    (select id from sets where short_name = 'apc'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ice Cave'),
    (select id from sets where short_name = 'apc'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tundra Kavu'),
    (select id from sets where short_name = 'apc'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Messenger'),
    (select id from sets where short_name = 'apc'),
    '87',
    'uncommon'
) 
 on conflict do nothing;
