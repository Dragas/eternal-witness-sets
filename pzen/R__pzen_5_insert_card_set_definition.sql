insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Emeria Angel'),
    (select id from sets where short_name = 'pzen'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Valakut, the Molten Pinnacle'),
    (select id from sets where short_name = 'pzen'),
    '*228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Day of Judgment'),
    (select id from sets where short_name = 'pzen'),
    '*9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rampaging Baloths'),
    (select id from sets where short_name = 'pzen'),
    '*178',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nissa''s Chosen'),
    (select id from sets where short_name = 'pzen'),
    '34',
    'rare'
) 
 on conflict do nothing;
