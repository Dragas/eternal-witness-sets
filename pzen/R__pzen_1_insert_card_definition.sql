    insert into mtgcard(name) values ('Emeria Angel') on conflict do nothing;
    insert into mtgcard(name) values ('Valakut, the Molten Pinnacle') on conflict do nothing;
    insert into mtgcard(name) values ('Day of Judgment') on conflict do nothing;
    insert into mtgcard(name) values ('Rampaging Baloths') on conflict do nothing;
    insert into mtgcard(name) values ('Nissa''s Chosen') on conflict do nothing;
