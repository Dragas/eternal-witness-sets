    insert into mtgcard(name) values ('Liliana of the Dark Realms') on conflict do nothing;
    insert into mtgcard(name) values ('Chandra, Pyromaster') on conflict do nothing;
    insert into mtgcard(name) values ('Ajani, Caller of the Pride') on conflict do nothing;
    insert into mtgcard(name) values ('Jace, Memory Adept') on conflict do nothing;
    insert into mtgcard(name) values ('Garruk, Caller of Beasts') on conflict do nothing;
