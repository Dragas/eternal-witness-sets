insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Liliana of the Dark Realms'),
    (select id from sets where short_name = 'psdc'),
    '102',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chandra, Pyromaster'),
    (select id from sets where short_name = 'psdc'),
    '132',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ajani, Caller of the Pride'),
    (select id from sets where short_name = 'psdc'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jace, Memory Adept'),
    (select id from sets where short_name = 'psdc'),
    '60',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Garruk, Caller of Beasts'),
    (select id from sets where short_name = 'psdc'),
    '172',
    'mythic'
) 
 on conflict do nothing;
