insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Blockbuster'),
    (select id from sets where short_name = 'rav'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dromad Purebred'),
    (select id from sets where short_name = 'rav'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lurking Informant'),
    (select id from sets where short_name = 'rav'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terrarion'),
    (select id from sets where short_name = 'rav'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vedalken Dismisser'),
    (select id from sets where short_name = 'rav'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gate Hound'),
    (select id from sets where short_name = 'rav'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tolsimir Wolfblood'),
    (select id from sets where short_name = 'rav'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Guildmage'),
    (select id from sets where short_name = 'rav'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keening Banshee'),
    (select id from sets where short_name = 'rav'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Putrefy'),
    (select id from sets where short_name = 'rav'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necromantic Thirst'),
    (select id from sets where short_name = 'rav'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circu, Dimir Lobotomist'),
    (select id from sets where short_name = 'rav'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Caregiver'),
    (select id from sets where short_name = 'rav'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunted Horror'),
    (select id from sets where short_name = 'rav'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mindmoil'),
    (select id from sets where short_name = 'rav'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'rav'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Perplex'),
    (select id from sets where short_name = 'rav'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darkblast'),
    (select id from sets where short_name = 'rav'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dream Leash'),
    (select id from sets where short_name = 'rav'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Suppression Field'),
    (select id from sets where short_name = 'rav'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Cutpurse'),
    (select id from sets where short_name = 'rav'),
    '201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carrion Howler'),
    (select id from sets where short_name = 'rav'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Rot Farm'),
    (select id from sets where short_name = 'rav'),
    '278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infectious Host'),
    (select id from sets where short_name = 'rav'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nullmage Shepherd'),
    (select id from sets where short_name = 'rav'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leashling'),
    (select id from sets where short_name = 'rav'),
    '265',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Watery Grave'),
    (select id from sets where short_name = 'rav'),
    '286',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dimir Machinations'),
    (select id from sets where short_name = 'rav'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Veteran Armorer'),
    (select id from sets where short_name = 'rav'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barbarian Riftcutter'),
    (select id from sets where short_name = 'rav'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savra, Queen of the Golgari'),
    (select id from sets where short_name = 'rav'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sacred Foundry'),
    (select id from sets where short_name = 'rav'),
    '280',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaze of the Gorgon'),
    (select id from sets where short_name = 'rav'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stoneshaker Shaman'),
    (select id from sets where short_name = 'rav'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selesnya Evangel'),
    (select id from sets where short_name = 'rav'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sundering Vitae'),
    (select id from sets where short_name = 'rav'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Helix'),
    (select id from sets where short_name = 'rav'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spectral Searchlight'),
    (select id from sets where short_name = 'rav'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glass Golem'),
    (select id from sets where short_name = 'rav'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Signet'),
    (select id from sets where short_name = 'rav'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guardian of Vitu-Ghazi'),
    (select id from sets where short_name = 'rav'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'rav'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nullstone Gargoyle'),
    (select id from sets where short_name = 'rav'),
    '266',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Three Dreams'),
    (select id from sets where short_name = 'rav'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mnemonic Nexus'),
    (select id from sets where short_name = 'rav'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightguard Patrol'),
    (select id from sets where short_name = 'rav'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Compulsive Research'),
    (select id from sets where short_name = 'rav'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Guildmage'),
    (select id from sets where short_name = 'rav'),
    '252',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cyclopean Snare'),
    (select id from sets where short_name = 'rav'),
    '259',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chant of Vitu-Ghazi'),
    (select id from sets where short_name = 'rav'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vindictive Mob'),
    (select id from sets where short_name = 'rav'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grave-Shell Scarab'),
    (select id from sets where short_name = 'rav'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clutch of the Undercity'),
    (select id from sets where short_name = 'rav'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Germination'),
    (select id from sets where short_name = 'rav'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'War-Torch Goblin'),
    (select id from sets where short_name = 'rav'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viashino Fangtail'),
    (select id from sets where short_name = 'rav'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dogpile'),
    (select id from sets where short_name = 'rav'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flow of Ideas'),
    (select id from sets where short_name = 'rav'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'rav'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Szadek, Lord of Secrets'),
    (select id from sets where short_name = 'rav'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clinging Darkness'),
    (select id from sets where short_name = 'rav'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oathsworn Giant'),
    (select id from sets where short_name = 'rav'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Rotwurm'),
    (select id from sets where short_name = 'rav'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duskmantle, House of Shadow'),
    (select id from sets where short_name = 'rav'),
    '277',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rain of Embers'),
    (select id from sets where short_name = 'rav'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Congregation at Dawn'),
    (select id from sets where short_name = 'rav'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Helldozer'),
    (select id from sets where short_name = 'rav'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Transluminant'),
    (select id from sets where short_name = 'rav'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seed Spark'),
    (select id from sets where short_name = 'rav'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Searing Meditation'),
    (select id from sets where short_name = 'rav'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vinelasher Kudzu'),
    (select id from sets where short_name = 'rav'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Last Gasp'),
    (select id from sets where short_name = 'rav'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necroplasm'),
    (select id from sets where short_name = 'rav'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pollenbright Wings'),
    (select id from sets where short_name = 'rav'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'rav'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dimir Infiltrator'),
    (select id from sets where short_name = 'rav'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moroii'),
    (select id from sets where short_name = 'rav'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quickchange'),
    (select id from sets where short_name = 'rav'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sadistic Augermage'),
    (select id from sets where short_name = 'rav'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Perilous Forays'),
    (select id from sets where short_name = 'rav'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reroute'),
    (select id from sets where short_name = 'rav'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'rav'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sandsower'),
    (select id from sets where short_name = 'rav'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hammerfist Giant'),
    (select id from sets where short_name = 'rav'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Belltower Sphinx'),
    (select id from sets where short_name = 'rav'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bramble Elemental'),
    (select id from sets where short_name = 'rav'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudstone Curio'),
    (select id from sets where short_name = 'rav'),
    '257',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glimpse the Unthinkable'),
    (select id from sets where short_name = 'rav'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firemane Angel'),
    (select id from sets where short_name = 'rav'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eye of the Storm'),
    (select id from sets where short_name = 'rav'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Concerted Effort'),
    (select id from sets where short_name = 'rav'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shambling Shell'),
    (select id from sets where short_name = 'rav'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Muddle the Mixture'),
    (select id from sets where short_name = 'rav'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'rav'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mausoleum Turnkey'),
    (select id from sets where short_name = 'rav'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flash Conscription'),
    (select id from sets where short_name = 'rav'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Recollect'),
    (select id from sets where short_name = 'rav'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Devouring Light'),
    (select id from sets where short_name = 'rav'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Telling Time'),
    (select id from sets where short_name = 'rav'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Remand'),
    (select id from sets where short_name = 'rav'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leave No Trace'),
    (select id from sets where short_name = 'rav'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boros Recruit'),
    (select id from sets where short_name = 'rav'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faith''s Fetters'),
    (select id from sets where short_name = 'rav'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bottled Cloister'),
    (select id from sets where short_name = 'rav'),
    '256',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sins of the Past'),
    (select id from sets where short_name = 'rav'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Svogthos, the Restless Tomb'),
    (select id from sets where short_name = 'rav'),
    '283',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Primordial Sage'),
    (select id from sets where short_name = 'rav'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Junktroller'),
    (select id from sets where short_name = 'rav'),
    '264',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Screeching Griffin'),
    (select id from sets where short_name = 'rav'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Funnel'),
    (select id from sets where short_name = 'rav'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viashino Slasher'),
    (select id from sets where short_name = 'rav'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flickerform'),
    (select id from sets where short_name = 'rav'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stinkweed Imp'),
    (select id from sets where short_name = 'rav'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cleansing Beam'),
    (select id from sets where short_name = 'rav'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'rav'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Centaur Safeguard'),
    (select id from sets where short_name = 'rav'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Fire Fiend'),
    (select id from sets where short_name = 'rav'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Galvanic Arc'),
    (select id from sets where short_name = 'rav'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Civic Wayfinder'),
    (select id from sets where short_name = 'rav'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undercity Shade'),
    (select id from sets where short_name = 'rav'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overwhelm'),
    (select id from sets where short_name = 'rav'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tidewater Minion'),
    (select id from sets where short_name = 'rav'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golgari Brownscale'),
    (select id from sets where short_name = 'rav'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smash'),
    (select id from sets where short_name = 'rav'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dimir Guildmage'),
    (select id from sets where short_name = 'rav'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gleancrawler'),
    (select id from sets where short_name = 'rav'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Spelunkers'),
    (select id from sets where short_name = 'rav'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sparkmage Apprentice'),
    (select id from sets where short_name = 'rav'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dizzy Spell'),
    (select id from sets where short_name = 'rav'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Consult the Necrosages'),
    (select id from sets where short_name = 'rav'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Autochthon Wurm'),
    (select id from sets where short_name = 'rav'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'rav'),
    '303',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fists of Ironwood'),
    (select id from sets where short_name = 'rav'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ivy Dancer'),
    (select id from sets where short_name = 'rav'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Razia''s Purification'),
    (select id from sets where short_name = 'rav'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thundersong Trumpeter'),
    (select id from sets where short_name = 'rav'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voyager Staff'),
    (select id from sets where short_name = 'rav'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loxodon Gatekeeper'),
    (select id from sets where short_name = 'rav'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Torpid Moloch'),
    (select id from sets where short_name = 'rav'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple Garden'),
    (select id from sets where short_name = 'rav'),
    '284',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moonlight Bargain'),
    (select id from sets where short_name = 'rav'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'rav'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Life from the Loam'),
    (select id from sets where short_name = 'rav'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vulturous Zombie'),
    (select id from sets where short_name = 'rav'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elves of Deep Shadow'),
    (select id from sets where short_name = 'rav'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'rav'),
    '290',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grifter''s Blade'),
    (select id from sets where short_name = 'rav'),
    '263',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flight of Fancy'),
    (select id from sets where short_name = 'rav'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mindleech Mass'),
    (select id from sets where short_name = 'rav'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tattered Drake'),
    (select id from sets where short_name = 'rav'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shadow of Doubt'),
    (select id from sets where short_name = 'rav'),
    '253',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Garrison'),
    (select id from sets where short_name = 'rav'),
    '275',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flame Fusillade'),
    (select id from sets where short_name = 'rav'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Farseek'),
    (select id from sets where short_name = 'rav'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Signet'),
    (select id from sets where short_name = 'rav'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conclave Phalanx'),
    (select id from sets where short_name = 'rav'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'rav'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conclave''s Blessing'),
    (select id from sets where short_name = 'rav'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phytohydra'),
    (select id from sets where short_name = 'rav'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moldervine Cloak'),
    (select id from sets where short_name = 'rav'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Thug'),
    (select id from sets where short_name = 'rav'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plague Boiler'),
    (select id from sets where short_name = 'rav'),
    '269',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightmare Void'),
    (select id from sets where short_name = 'rav'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grayscaled Gharial'),
    (select id from sets where short_name = 'rav'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divebomber Griffin'),
    (select id from sets where short_name = 'rav'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dryad''s Caress'),
    (select id from sets where short_name = 'rav'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Roofstalker Wight'),
    (select id from sets where short_name = 'rav'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vigor Mortis'),
    (select id from sets where short_name = 'rav'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ethereal Usher'),
    (select id from sets where short_name = 'rav'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = 'rav'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Peregrine Mask'),
    (select id from sets where short_name = 'rav'),
    '268',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gather Courage'),
    (select id from sets where short_name = 'rav'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strands of Undeath'),
    (select id from sets where short_name = 'rav'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Auratouched Mage'),
    (select id from sets where short_name = 'rav'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mortipede'),
    (select id from sets where short_name = 'rav'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Confidant'),
    (select id from sets where short_name = 'rav'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Watchwolf'),
    (select id from sets where short_name = 'rav'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grozoth'),
    (select id from sets where short_name = 'rav'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'rav'),
    '288',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dimir House Guard'),
    (select id from sets where short_name = 'rav'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brightflame'),
    (select id from sets where short_name = 'rav'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selesnya Sagittars'),
    (select id from sets where short_name = 'rav'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Signet'),
    (select id from sets where short_name = 'rav'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Votary of the Conclave'),
    (select id from sets where short_name = 'rav'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Master Warcraft'),
    (select id from sets where short_name = 'rav'),
    '250',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rally the Righteous'),
    (select id from sets where short_name = 'rav'),
    '222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terraformer'),
    (select id from sets where short_name = 'rav'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loxodon Hierarch'),
    (select id from sets where short_name = 'rav'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Incite Hysteria'),
    (select id from sets where short_name = 'rav'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Excruciator'),
    (select id from sets where short_name = 'rav'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dimir Doppelganger'),
    (select id from sets where short_name = 'rav'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wojek Siren'),
    (select id from sets where short_name = 'rav'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bathe in Light'),
    (select id from sets where short_name = 'rav'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunhome, Fortress of the Legion'),
    (select id from sets where short_name = 'rav'),
    '282',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Molten Sentry'),
    (select id from sets where short_name = 'rav'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ordruun Commando'),
    (select id from sets where short_name = 'rav'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fiery Conclusion'),
    (select id from sets where short_name = 'rav'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Copy Enchantment'),
    (select id from sets where short_name = 'rav'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'rav'),
    '306',
    'common'
) ,
(
    (select id from mtgcard where name = 'Siege Wurm'),
    (select id from sets where short_name = 'rav'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tunnel Vision'),
    (select id from sets where short_name = 'rav'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mark of Eviction'),
    (select id from sets where short_name = 'rav'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Surveilling Sprite'),
    (select id from sets where short_name = 'rav'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flame-Kin Zealot'),
    (select id from sets where short_name = 'rav'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Indentured Oaf'),
    (select id from sets where short_name = 'rav'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vitu-Ghazi, the City-Tree'),
    (select id from sets where short_name = 'rav'),
    '285',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sewerdreg'),
    (select id from sets where short_name = 'rav'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dowsing Shaman'),
    (select id from sets where short_name = 'rav'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drake Familiar'),
    (select id from sets where short_name = 'rav'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Netherborn Phalanx'),
    (select id from sets where short_name = 'rav'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Surge of Zeal'),
    (select id from sets where short_name = 'rav'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thoughtpicker Witch'),
    (select id from sets where short_name = 'rav'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunted Lammasu'),
    (select id from sets where short_name = 'rav'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodletter Quill'),
    (select id from sets where short_name = 'rav'),
    '254',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seismic Spike'),
    (select id from sets where short_name = 'rav'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sabertooth Alley Cat'),
    (select id from sets where short_name = 'rav'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carven Caryatid'),
    (select id from sets where short_name = 'rav'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ursapine'),
    (select id from sets where short_name = 'rav'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Heart of the Wood'),
    (select id from sets where short_name = 'rav'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Twilight Drover'),
    (select id from sets where short_name = 'rav'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coalhauler Swine'),
    (select id from sets where short_name = 'rav'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Induce Paranoia'),
    (select id from sets where short_name = 'rav'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golgari Signet'),
    (select id from sets where short_name = 'rav'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woodwraith Strangler'),
    (select id from sets where short_name = 'rav'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hour of Reckoning'),
    (select id from sets where short_name = 'rav'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Swiftblade'),
    (select id from sets where short_name = 'rav'),
    '193',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wizened Snitches'),
    (select id from sets where short_name = 'rav'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seeds of Strength'),
    (select id from sets where short_name = 'rav'),
    '227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drift of Phantasms'),
    (select id from sets where short_name = 'rav'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crown of Convergence'),
    (select id from sets where short_name = 'rav'),
    '258',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disembowel'),
    (select id from sets where short_name = 'rav'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Agrus Kos, Wojek Veteran'),
    (select id from sets where short_name = 'rav'),
    '190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brainspoil'),
    (select id from sets where short_name = 'rav'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scatter the Seeds'),
    (select id from sets where short_name = 'rav'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Razia, Boros Archangel'),
    (select id from sets where short_name = 'rav'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Fury-Shield'),
    (select id from sets where short_name = 'rav'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunhome Enforcer'),
    (select id from sets where short_name = 'rav'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghosts of the Innocent'),
    (select id from sets where short_name = 'rav'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selesnya Sanctuary'),
    (select id from sets where short_name = 'rav'),
    '281',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lore Broker'),
    (select id from sets where short_name = 'rav'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ribbons of Night'),
    (select id from sets where short_name = 'rav'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doubling Season'),
    (select id from sets where short_name = 'rav'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunforger'),
    (select id from sets where short_name = 'rav'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pariah''s Shield'),
    (select id from sets where short_name = 'rav'),
    '267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blazing Archon'),
    (select id from sets where short_name = 'rav'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snapping Drake'),
    (select id from sets where short_name = 'rav'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conclave Equenaut'),
    (select id from sets where short_name = 'rav'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spawnbroker'),
    (select id from sets where short_name = 'rav'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goliath Spider'),
    (select id from sets where short_name = 'rav'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Followed Footsteps'),
    (select id from sets where short_name = 'rav'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Psychic Drain'),
    (select id from sets where short_name = 'rav'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Courier Hawk'),
    (select id from sets where short_name = 'rav'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Festival of the Guildpact'),
    (select id from sets where short_name = 'rav'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woodwraith Corrupter'),
    (select id from sets where short_name = 'rav'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sisters of Stone Death'),
    (select id from sets where short_name = 'rav'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chorus of the Conclave'),
    (select id from sets where short_name = 'rav'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hunted Dragon'),
    (select id from sets where short_name = 'rav'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frenzied Goblin'),
    (select id from sets where short_name = 'rav'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyknight Legionnaire'),
    (select id from sets where short_name = 'rav'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Twisted Justice'),
    (select id from sets where short_name = 'rav'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Guildmage'),
    (select id from sets where short_name = 'rav'),
    '248',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'rav'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Light of Sanction'),
    (select id from sets where short_name = 'rav'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sell-Sword Brute'),
    (select id from sets where short_name = 'rav'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'rav'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Breath of Fury'),
    (select id from sets where short_name = 'rav'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cerulean Sphinx'),
    (select id from sets where short_name = 'rav'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drooling Groodion'),
    (select id from sets where short_name = 'rav'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Aqueduct'),
    (select id from sets where short_name = 'rav'),
    '276',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chord of Calling'),
    (select id from sets where short_name = 'rav'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'rav'),
    '302',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wojek Embermage'),
    (select id from sets where short_name = 'rav'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Greater Mossdog'),
    (select id from sets where short_name = 'rav'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shred Memory'),
    (select id from sets where short_name = 'rav'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Empty the Catacombs'),
    (select id from sets where short_name = 'rav'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Root-Kin Ally'),
    (select id from sets where short_name = 'rav'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Peel from Reality'),
    (select id from sets where short_name = 'rav'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rolling Spoil'),
    (select id from sets where short_name = 'rav'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zephyr Spirit'),
    (select id from sets where short_name = 'rav'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stasis Cell'),
    (select id from sets where short_name = 'rav'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Skysweeper'),
    (select id from sets where short_name = 'rav'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glare of Subdual'),
    (select id from sets where short_name = 'rav'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Greater Forgeling'),
    (select id from sets where short_name = 'rav'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wojek Apothecary'),
    (select id from sets where short_name = 'rav'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woebringer Demon'),
    (select id from sets where short_name = 'rav'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trophy Hunter'),
    (select id from sets where short_name = 'rav'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'rav'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Benevolent Ancestor'),
    (select id from sets where short_name = 'rav'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone-Seeder Hierophant'),
    (select id from sets where short_name = 'rav'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Instill Furor'),
    (select id from sets where short_name = 'rav'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Grave-Troll'),
    (select id from sets where short_name = 'rav'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hex'),
    (select id from sets where short_name = 'rav'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Privileged Position'),
    (select id from sets where short_name = 'rav'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'rav'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Convolute'),
    (select id from sets where short_name = 'rav'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunted Phantasm'),
    (select id from sets where short_name = 'rav'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vedalken Entrancer'),
    (select id from sets where short_name = 'rav'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Halcyon Glaze'),
    (select id from sets where short_name = 'rav'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodbond March'),
    (select id from sets where short_name = 'rav'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'rav'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overgrown Tomb'),
    (select id from sets where short_name = 'rav'),
    '279',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hunted Troll'),
    (select id from sets where short_name = 'rav'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warp World'),
    (select id from sets where short_name = 'rav'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'rav'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scion of the Wild'),
    (select id from sets where short_name = 'rav'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Char'),
    (select id from sets where short_name = 'rav'),
    '117',
    'rare'
) 
 on conflict do nothing;
