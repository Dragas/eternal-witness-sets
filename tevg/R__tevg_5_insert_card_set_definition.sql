insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Elf Warrior'),
    (select id from sets where short_name = 'tevg'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tevg'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tevg'),
    '3',
    'common'
) 
 on conflict do nothing;
