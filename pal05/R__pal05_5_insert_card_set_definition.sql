insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pal05'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Genju of the Spires'),
    (select id from sets where short_name = 'pal05'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pal05'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Okina Nightwatch'),
    (select id from sets where short_name = 'pal05'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pal05'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pal05'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pal05'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyknight Legionnaire'),
    (select id from sets where short_name = 'pal05'),
    '8',
    'rare'
) 
 on conflict do nothing;
