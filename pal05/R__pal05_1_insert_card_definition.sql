    insert into mtgcard(name) values ('Forest') on conflict do nothing;
    insert into mtgcard(name) values ('Genju of the Spires') on conflict do nothing;
    insert into mtgcard(name) values ('Swamp') on conflict do nothing;
    insert into mtgcard(name) values ('Okina Nightwatch') on conflict do nothing;
    insert into mtgcard(name) values ('Plains') on conflict do nothing;
    insert into mtgcard(name) values ('Island') on conflict do nothing;
    insert into mtgcard(name) values ('Mountain') on conflict do nothing;
    insert into mtgcard(name) values ('Skyknight Legionnaire') on conflict do nothing;
