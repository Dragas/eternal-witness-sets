insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Inventor''s Goggles'),
    (select id from sets where short_name = 'kld'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cogworker''s Puzzleknot'),
    (select id from sets where short_name = 'kld'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Verdurous Gearhulk'),
    (select id from sets where short_name = 'kld'),
    '172',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Terror of the Fairgrounds'),
    (select id from sets where short_name = 'kld'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Pyrohelix'),
    (select id from sets where short_name = 'kld'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Madcap Experiment'),
    (select id from sets where short_name = 'kld'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prophetic Prism'),
    (select id from sets where short_name = 'kld'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diabolic Tutor'),
    (select id from sets where short_name = 'kld'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terrain Elemental'),
    (select id from sets where short_name = 'kld'),
    '272†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'kld'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ceremonious Rejection'),
    (select id from sets where short_name = 'kld'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sage of Shaila''s Claim'),
    (select id from sets where short_name = 'kld'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trusty Companion'),
    (select id from sets where short_name = 'kld'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tezzeret''s Ambition'),
    (select id from sets where short_name = 'kld'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Wanderer'),
    (select id from sets where short_name = 'kld'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minister of Inquiries'),
    (select id from sets where short_name = 'kld'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hijack'),
    (select id from sets where short_name = 'kld'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Incendiary Sabotage'),
    (select id from sets where short_name = 'kld'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bomat Bazaar Barge'),
    (select id from sets where short_name = 'kld'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ninth Bridge Patrol'),
    (select id from sets where short_name = 'kld'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Select for Inspection'),
    (select id from sets where short_name = 'kld'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lathnu Hellion'),
    (select id from sets where short_name = 'kld'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Midnight Oil'),
    (select id from sets where short_name = 'kld'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Panharmonicon'),
    (select id from sets where short_name = 'kld'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inspiring Vantage'),
    (select id from sets where short_name = 'kld'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unlicensed Disintegration'),
    (select id from sets where short_name = 'kld'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Paradoxical Outcome'),
    (select id from sets where short_name = 'kld'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'kld'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glimmer of Genius'),
    (select id from sets where short_name = 'kld'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lawless Broker'),
    (select id from sets where short_name = 'kld'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skysovereign, Consul Flagship'),
    (select id from sets where short_name = 'kld'),
    '234',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rush of Vitality'),
    (select id from sets where short_name = 'kld'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ovalchase Dragster'),
    (select id from sets where short_name = 'kld'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scrapheap Scrounger'),
    (select id from sets where short_name = 'kld'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'kld'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Restoration Gearsmith'),
    (select id from sets where short_name = 'kld'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dovin Baan'),
    (select id from sets where short_name = 'kld'),
    '179',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aether Hub'),
    (select id from sets where short_name = 'kld'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aether Tradewinds'),
    (select id from sets where short_name = 'kld'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inventors'' Fair'),
    (select id from sets where short_name = 'kld'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Creeping Mold'),
    (select id from sets where short_name = 'kld'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oviya Pashiri, Sage Lifecrafter'),
    (select id from sets where short_name = 'kld'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Durable Handicraft'),
    (select id from sets where short_name = 'kld'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Impeccable Timing'),
    (select id from sets where short_name = 'kld'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'kld'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Peema Outrider'),
    (select id from sets where short_name = 'kld'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demolish'),
    (select id from sets where short_name = 'kld'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gearshift Ace'),
    (select id from sets where short_name = 'kld'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Morbid Curiosity'),
    (select id from sets where short_name = 'kld'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Authority of the Consuls'),
    (select id from sets where short_name = 'kld'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'kld'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Attune with Aether'),
    (select id from sets where short_name = 'kld'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terrain Elemental'),
    (select id from sets where short_name = 'kld'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = 'kld'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Animation Module'),
    (select id from sets where short_name = 'kld'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elegant Edgecrafters'),
    (select id from sets where short_name = 'kld'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marionette Master'),
    (select id from sets where short_name = 'kld'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'kld'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shrewd Negotiation'),
    (select id from sets where short_name = 'kld'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eliminate the Competition'),
    (select id from sets where short_name = 'kld'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloudblazer'),
    (select id from sets where short_name = 'kld'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rashmi, Eternities Crafter'),
    (select id from sets where short_name = 'kld'),
    '184',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Self-Assembler'),
    (select id from sets where short_name = 'kld'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Larger Than Life'),
    (select id from sets where short_name = 'kld'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Veteran Motorist'),
    (select id from sets where short_name = 'kld'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Saheeli''s Artistry'),
    (select id from sets where short_name = 'kld'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bomat Courier'),
    (select id from sets where short_name = 'kld'),
    '199',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foundry Screecher'),
    (select id from sets where short_name = 'kld'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Speedway Fanatic'),
    (select id from sets where short_name = 'kld'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Underhanded Designs'),
    (select id from sets where short_name = 'kld'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Empyreal Voyager'),
    (select id from sets where short_name = 'kld'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fretwork Colony'),
    (select id from sets where short_name = 'kld'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Metalwork Colossus'),
    (select id from sets where short_name = 'kld'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'kld'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fairgrounds Warden'),
    (select id from sets where short_name = 'kld'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kambal, Consul of Allocation'),
    (select id from sets where short_name = 'kld'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Renegade Firebrand'),
    (select id from sets where short_name = 'kld'),
    '268',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bristling Hydra'),
    (select id from sets where short_name = 'kld'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spirebluff Canal'),
    (select id from sets where short_name = 'kld'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Night Market Lookout'),
    (select id from sets where short_name = 'kld'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Workshop Assistant'),
    (select id from sets where short_name = 'kld'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Consul''s Shieldguard'),
    (select id from sets where short_name = 'kld'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thriving Rats'),
    (select id from sets where short_name = 'kld'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maulfist Doorbuster'),
    (select id from sets where short_name = 'kld'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skywhaler''s Shot'),
    (select id from sets where short_name = 'kld'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aethersquall Ancient'),
    (select id from sets where short_name = 'kld'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'kld'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vedalken Blademaster'),
    (select id from sets where short_name = 'kld'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wispweaver Angel'),
    (select id from sets where short_name = 'kld'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Depala, Pilot Exemplar'),
    (select id from sets where short_name = 'kld'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra, Pyrogenius'),
    (select id from sets where short_name = 'kld'),
    '265',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thriving Rhino'),
    (select id from sets where short_name = 'kld'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fragmentize'),
    (select id from sets where short_name = 'kld'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blossoming Defense'),
    (select id from sets where short_name = 'kld'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cultivator of Blades'),
    (select id from sets where short_name = 'kld'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pressure Point'),
    (select id from sets where short_name = 'kld'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Era of Innovation'),
    (select id from sets where short_name = 'kld'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wind Drake'),
    (select id from sets where short_name = 'kld'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maulfist Squad'),
    (select id from sets where short_name = 'kld'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Embraal Bruiser'),
    (select id from sets where short_name = 'kld'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aradara Express'),
    (select id from sets where short_name = 'kld'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fleetwheel Cruiser'),
    (select id from sets where short_name = 'kld'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brazen Scourge'),
    (select id from sets where short_name = 'kld'),
    '107†',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thriving Turtle'),
    (select id from sets where short_name = 'kld'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Subtle Strike'),
    (select id from sets where short_name = 'kld'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Accomplished Automaton'),
    (select id from sets where short_name = 'kld'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Fireweaver'),
    (select id from sets where short_name = 'kld'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fairgrounds Trumpeter'),
    (select id from sets where short_name = 'kld'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Key to the City'),
    (select id from sets where short_name = 'kld'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eager Construct'),
    (select id from sets where short_name = 'kld'),
    '209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Engineered Might'),
    (select id from sets where short_name = 'kld'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snare Thopter'),
    (select id from sets where short_name = 'kld'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brazen Scourge'),
    (select id from sets where short_name = 'kld'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Contraband Kingpin'),
    (select id from sets where short_name = 'kld'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hightide Hermit'),
    (select id from sets where short_name = 'kld'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pia Nalaar'),
    (select id from sets where short_name = 'kld'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Renegade Tactics'),
    (select id from sets where short_name = 'kld'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Confiscation Coup'),
    (select id from sets where short_name = 'kld'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Revolutionary Rebuff'),
    (select id from sets where short_name = 'kld'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Built to Smash'),
    (select id from sets where short_name = 'kld'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glint-Nest Crane'),
    (select id from sets where short_name = 'kld'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spark of Creativity'),
    (select id from sets where short_name = 'kld'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guardian of the Great Conduit'),
    (select id from sets where short_name = 'kld'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Highspire Artisan'),
    (select id from sets where short_name = 'kld'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Consulate Surveillance'),
    (select id from sets where short_name = 'kld'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Acrobatic Maneuver'),
    (select id from sets where short_name = 'kld'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weldfast Monitor'),
    (select id from sets where short_name = 'kld'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armorcraft Judge'),
    (select id from sets where short_name = 'kld'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wind Drake'),
    (select id from sets where short_name = 'kld'),
    '70†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fortuitous Find'),
    (select id from sets where short_name = 'kld'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Long-Finned Skywhale'),
    (select id from sets where short_name = 'kld'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gonti, Lord of Luxury'),
    (select id from sets where short_name = 'kld'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa, Nature''s Artisan'),
    (select id from sets where short_name = 'kld'),
    '270',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Smuggler''s Copter'),
    (select id from sets where short_name = 'kld'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arborback Stomper'),
    (select id from sets where short_name = 'kld'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aetherstorm Roc'),
    (select id from sets where short_name = 'kld'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Renegade Freighter'),
    (select id from sets where short_name = 'kld'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireforger''s Puzzleknot'),
    (select id from sets where short_name = 'kld'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyship Stalker'),
    (select id from sets where short_name = 'kld'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cataclysmic Gearhulk'),
    (select id from sets where short_name = 'kld'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Architect of the Untamed'),
    (select id from sets where short_name = 'kld'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Spectacle'),
    (select id from sets where short_name = 'kld'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glassblower''s Puzzleknot'),
    (select id from sets where short_name = 'kld'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glint-Sleeve Artisan'),
    (select id from sets where short_name = 'kld'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ambitious Aetherborn'),
    (select id from sets where short_name = 'kld'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dukhara Scavenger'),
    (select id from sets where short_name = 'kld'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eddytrail Hawk'),
    (select id from sets where short_name = 'kld'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Quarry'),
    (select id from sets where short_name = 'kld'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Janjeet Sentry'),
    (select id from sets where short_name = 'kld'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Revoke Privileges'),
    (select id from sets where short_name = 'kld'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sequestered Stash'),
    (select id from sets where short_name = 'kld'),
    '248',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Botanical Sanctum'),
    (select id from sets where short_name = 'kld'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'kld'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruinous Gremlin'),
    (select id from sets where short_name = 'kld'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Theorist'),
    (select id from sets where short_name = 'kld'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aetherworks Marvel'),
    (select id from sets where short_name = 'kld'),
    '193',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Narnam Cobra'),
    (select id from sets where short_name = 'kld'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woodweaver''s Puzzleknot'),
    (select id from sets where short_name = 'kld'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cowl Prowler'),
    (select id from sets where short_name = 'kld'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weldfast Wingsmith'),
    (select id from sets where short_name = 'kld'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'kld'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insidious Will'),
    (select id from sets where short_name = 'kld'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thriving Ibex'),
    (select id from sets where short_name = 'kld'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blooming Marsh'),
    (select id from sets where short_name = 'kld'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Visionary Augmenter'),
    (select id from sets where short_name = 'kld'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aether Meltdown'),
    (select id from sets where short_name = 'kld'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dubious Challenge'),
    (select id from sets where short_name = 'kld'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghirapur Guide'),
    (select id from sets where short_name = 'kld'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'kld'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foundry Inspector'),
    (select id from sets where short_name = 'kld'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Saheeli Rai'),
    (select id from sets where short_name = 'kld'),
    '186',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aerial Responder'),
    (select id from sets where short_name = 'kld'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Experimental Aviator'),
    (select id from sets where short_name = 'kld'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aetherflux Reservoir'),
    (select id from sets where short_name = 'kld'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dhund Operative'),
    (select id from sets where short_name = 'kld'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon of Dark Schemes'),
    (select id from sets where short_name = 'kld'),
    '73',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Disappearing Act'),
    (select id from sets where short_name = 'kld'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Consulate Skygate'),
    (select id from sets where short_name = 'kld'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Servo Exhibition'),
    (select id from sets where short_name = 'kld'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Metalspinner''s Puzzleknot'),
    (select id from sets where short_name = 'kld'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prakhata Pillar-Bug'),
    (select id from sets where short_name = 'kld'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Filigree Familiar'),
    (select id from sets where short_name = 'kld'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Servant of the Conduit'),
    (select id from sets where short_name = 'kld'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tasseled Dromedary'),
    (select id from sets where short_name = 'kld'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Multiform Wonder'),
    (select id from sets where short_name = 'kld'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noxious Gearhulk'),
    (select id from sets where short_name = 'kld'),
    '96',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wildest Dreams'),
    (select id from sets where short_name = 'kld'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fateful Showdown'),
    (select id from sets where short_name = 'kld'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ovalchase Daredevil'),
    (select id from sets where short_name = 'kld'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woodland Stream'),
    (select id from sets where short_name = 'kld'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kujar Seedsculptor'),
    (select id from sets where short_name = 'kld'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flame Lash'),
    (select id from sets where short_name = 'kld'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Live Fast'),
    (select id from sets where short_name = 'kld'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whirler Virtuoso'),
    (select id from sets where short_name = 'kld'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Decoction Module'),
    (select id from sets where short_name = 'kld'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Salivating Gremlins'),
    (select id from sets where short_name = 'kld'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Territorial Gorger'),
    (select id from sets where short_name = 'kld'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cultivator''s Caravan'),
    (select id from sets where short_name = 'kld'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Appetite for the Unnatural'),
    (select id from sets where short_name = 'kld'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spontaneous Artist'),
    (select id from sets where short_name = 'kld'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyswirl Harrier'),
    (select id from sets where short_name = 'kld'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spireside Infiltrator'),
    (select id from sets where short_name = 'kld'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aviary Mechanic'),
    (select id from sets where short_name = 'kld'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nissa, Vital Force'),
    (select id from sets where short_name = 'kld'),
    '163',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Start Your Engines'),
    (select id from sets where short_name = 'kld'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thriving Grubs'),
    (select id from sets where short_name = 'kld'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Perpetual Timepiece'),
    (select id from sets where short_name = 'kld'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'kld'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prakhata Club Security'),
    (select id from sets where short_name = 'kld'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bastion Mastodon'),
    (select id from sets where short_name = 'kld'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Malfunction'),
    (select id from sets where short_name = 'kld'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dynavolt Tower'),
    (select id from sets where short_name = 'kld'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Iron League Steed'),
    (select id from sets where short_name = 'kld'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Longtusk Cub'),
    (select id from sets where short_name = 'kld'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Electrostatic Pummeler'),
    (select id from sets where short_name = 'kld'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curio Vendor'),
    (select id from sets where short_name = 'kld'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cathartic Reunion'),
    (select id from sets where short_name = 'kld'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Concealed Courtyard'),
    (select id from sets where short_name = 'kld'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghirapur Orrery'),
    (select id from sets where short_name = 'kld'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liberating Combustion'),
    (select id from sets where short_name = 'kld'),
    '267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quicksmith Genius'),
    (select id from sets where short_name = 'kld'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wily Bandar'),
    (select id from sets where short_name = 'kld'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voltaic Brawler'),
    (select id from sets where short_name = 'kld'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Riparian Tiger'),
    (select id from sets where short_name = 'kld'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chief of the Foundry'),
    (select id from sets where short_name = 'kld'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Master Trinketeer'),
    (select id from sets where short_name = 'kld'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Herald of the Fair'),
    (select id from sets where short_name = 'kld'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Propeller Pioneer'),
    (select id from sets where short_name = 'kld'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sky Skiff'),
    (select id from sets where short_name = 'kld'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arborback Stomper'),
    (select id from sets where short_name = 'kld'),
    '142†',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gearseeker Serpent'),
    (select id from sets where short_name = 'kld'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Torrential Gearhulk'),
    (select id from sets where short_name = 'kld'),
    '67',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lost Legacy'),
    (select id from sets where short_name = 'kld'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'kld'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Essence Extraction'),
    (select id from sets where short_name = 'kld'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Syndicate Trafficker'),
    (select id from sets where short_name = 'kld'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hazardous Conditions'),
    (select id from sets where short_name = 'kld'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Toolcraft Exemplar'),
    (select id from sets where short_name = 'kld'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Die Young'),
    (select id from sets where short_name = 'kld'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wayward Giant'),
    (select id from sets where short_name = 'kld'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dramatic Reversal'),
    (select id from sets where short_name = 'kld'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tidy Conclusion'),
    (select id from sets where short_name = 'kld'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Torch Gauntlet'),
    (select id from sets where short_name = 'kld'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Make Obsolete'),
    (select id from sets where short_name = 'kld'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Captured by the Consulate'),
    (select id from sets where short_name = 'kld'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Combustible Gearhulk'),
    (select id from sets where short_name = 'kld'),
    '112',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Padeem, Consul of Innovation'),
    (select id from sets where short_name = 'kld'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Furious Reprisal'),
    (select id from sets where short_name = 'kld'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aethertorch Renegade'),
    (select id from sets where short_name = 'kld'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dukhara Peafowl'),
    (select id from sets where short_name = 'kld'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demolition Stomper'),
    (select id from sets where short_name = 'kld'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Verdant Crescendo'),
    (select id from sets where short_name = 'kld'),
    '273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ornamental Courage'),
    (select id from sets where short_name = 'kld'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nature''s Way'),
    (select id from sets where short_name = 'kld'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inspired Charge'),
    (select id from sets where short_name = 'kld'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Built to Last'),
    (select id from sets where short_name = 'kld'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fumigate'),
    (select id from sets where short_name = 'kld'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Metallurgic Summonings'),
    (select id from sets where short_name = 'kld'),
    '56',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Commencement of Festivities'),
    (select id from sets where short_name = 'kld'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'kld'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Invention'),
    (select id from sets where short_name = 'kld'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'kld'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Refurbish'),
    (select id from sets where short_name = 'kld'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hunt the Weak'),
    (select id from sets where short_name = 'kld'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nimble Innovator'),
    (select id from sets where short_name = 'kld'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inventor''s Apprentice'),
    (select id from sets where short_name = 'kld'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Failed Inspection'),
    (select id from sets where short_name = 'kld'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Take Down'),
    (select id from sets where short_name = 'kld'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fabrication Module'),
    (select id from sets where short_name = 'kld'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deadlock Trap'),
    (select id from sets where short_name = 'kld'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harnessed Lightning'),
    (select id from sets where short_name = 'kld'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ballista Charger'),
    (select id from sets where short_name = 'kld'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aetherborn Marauder'),
    (select id from sets where short_name = 'kld'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Welding Sparks'),
    (select id from sets where short_name = 'kld'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harsh Scrutiny'),
    (select id from sets where short_name = 'kld'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weaponcraft Enthusiast'),
    (select id from sets where short_name = 'kld'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whirlermaker'),
    (select id from sets where short_name = 'kld'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chandra, Torch of Defiance'),
    (select id from sets where short_name = 'kld'),
    '110',
    'mythic'
) 
 on conflict do nothing;
