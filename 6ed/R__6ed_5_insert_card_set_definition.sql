insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Staunch Defenders'),
    (select id from sets where short_name = '6ed'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daring Apprentice'),
    (select id from sets where short_name = '6ed'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primal Clay'),
    (select id from sets where short_name = '6ed'),
    '308',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cursed Totem'),
    (select id from sets where short_name = '6ed'),
    '278',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Daraja Griffin'),
    (select id from sets where short_name = '6ed'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feral Shadow'),
    (select id from sets where short_name = '6ed'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shanodin Dryads'),
    (select id from sets where short_name = '6ed'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hulking Cyclops'),
    (select id from sets where short_name = '6ed'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Power Sink'),
    (select id from sets where short_name = '6ed'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ornithopter'),
    (select id from sets where short_name = '6ed'),
    '304',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gorilla Chieftain'),
    (select id from sets where short_name = '6ed'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vertigo'),
    (select id from sets where short_name = '6ed'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = '6ed'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Psychic Venom'),
    (select id from sets where short_name = '6ed'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leshrac''s Rite'),
    (select id from sets where short_name = '6ed'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bog Rats'),
    (select id from sets where short_name = '6ed'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '6ed'),
    '339',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aladdin''s Ring'),
    (select id from sets where short_name = '6ed'),
    '271',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flight'),
    (select id from sets where short_name = '6ed'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balduvian Horde'),
    (select id from sets where short_name = '6ed'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reverse Damage'),
    (select id from sets where short_name = '6ed'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grizzly Bears'),
    (select id from sets where short_name = '6ed'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elven Cache'),
    (select id from sets where short_name = '6ed'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Prism'),
    (select id from sets where short_name = '6ed'),
    '297',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Oriflamme'),
    (select id from sets where short_name = '6ed'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bog Wraith'),
    (select id from sets where short_name = '6ed'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquil Grove'),
    (select id from sets where short_name = '6ed'),
    '258',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Warrens'),
    (select id from sets where short_name = '6ed'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = '6ed'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strands of Night'),
    (select id from sets where short_name = '6ed'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inspiration'),
    (select id from sets where short_name = '6ed'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anaba Bodyguard'),
    (select id from sets where short_name = '6ed'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Underground River'),
    (select id from sets where short_name = '6ed'),
    '330',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystical Tutor'),
    (select id from sets where short_name = '6ed'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ebon Stronghold'),
    (select id from sets where short_name = '6ed'),
    '324',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necrosavant'),
    (select id from sets where short_name = '6ed'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Archers'),
    (select id from sets where short_name = '6ed'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = '6ed'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Summer Bloom'),
    (select id from sets where short_name = '6ed'),
    '255',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hecatomb'),
    (select id from sets where short_name = '6ed'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Remove Soul'),
    (select id from sets where short_name = '6ed'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunweb'),
    (select id from sets where short_name = '6ed'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Air'),
    (select id from sets where short_name = '6ed'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Resistance Fighter'),
    (select id from sets where short_name = '6ed'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blighted Shaman'),
    (select id from sets where short_name = '6ed'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stromgald Cabal'),
    (select id from sets where short_name = '6ed'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = '6ed'),
    '209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tremor'),
    (select id from sets where short_name = '6ed'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = '6ed'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = '6ed'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Warrior'),
    (select id from sets where short_name = '6ed'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thicket Basilisk'),
    (select id from sets where short_name = '6ed'),
    '256',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reprisal'),
    (select id from sets where short_name = '6ed'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Untamed Wilds'),
    (select id from sets where short_name = '6ed'),
    '263',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sengir Autocrat'),
    (select id from sets where short_name = '6ed'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chill'),
    (select id from sets where short_name = '6ed'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soldevi Sage'),
    (select id from sets where short_name = '6ed'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flashfires'),
    (select id from sets where short_name = '6ed'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Archangel'),
    (select id from sets where short_name = '6ed'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firebreathing'),
    (select id from sets where short_name = '6ed'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Blue'),
    (select id from sets where short_name = '6ed'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pestilence'),
    (select id from sets where short_name = '6ed'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blight'),
    (select id from sets where short_name = '6ed'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crystal Vein'),
    (select id from sets where short_name = '6ed'),
    '322',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Juxtapose'),
    (select id from sets where short_name = '6ed'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Syphon Soul'),
    (select id from sets where short_name = '6ed'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Short'),
    (select id from sets where short_name = '6ed'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea Monster'),
    (select id from sets where short_name = '6ed'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inferno'),
    (select id from sets where short_name = '6ed'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Geyser'),
    (select id from sets where short_name = '6ed'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Longbow Archer'),
    (select id from sets where short_name = '6ed'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = '6ed'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Master'),
    (select id from sets where short_name = '6ed'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stalking Tiger'),
    (select id from sets where short_name = '6ed'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relearn'),
    (select id from sets where short_name = '6ed'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = '6ed'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abyssal Specter'),
    (select id from sets where short_name = '6ed'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Dead'),
    (select id from sets where short_name = '6ed'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fog'),
    (select id from sets where short_name = '6ed'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Artillery'),
    (select id from sets where short_name = '6ed'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Storm Crow'),
    (select id from sets where short_name = '6ed'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Celestial Dawn'),
    (select id from sets where short_name = '6ed'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snake Basket'),
    (select id from sets where short_name = '6ed'),
    '312',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serenity'),
    (select id from sets where short_name = '6ed'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = '6ed'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abyssal Hunter'),
    (select id from sets where short_name = '6ed'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Light of Day'),
    (select id from sets where short_name = '6ed'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pearl Dragon'),
    (select id from sets where short_name = '6ed'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Browse'),
    (select id from sets where short_name = '6ed'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lost Soul'),
    (select id from sets where short_name = '6ed'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vitalize'),
    (select id from sets where short_name = '6ed'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iron Star'),
    (select id from sets where short_name = '6ed'),
    '291',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin King'),
    (select id from sets where short_name = '6ed'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fallow Earth'),
    (select id from sets where short_name = '6ed'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '6ed'),
    '348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sage Owl'),
    (select id from sets where short_name = '6ed'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = '6ed'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain Goat'),
    (select id from sets where short_name = '6ed'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Living Lands'),
    (select id from sets where short_name = '6ed'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Healing Salve'),
    (select id from sets where short_name = '6ed'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Altar'),
    (select id from sets where short_name = '6ed'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Talruum Minotaur'),
    (select id from sets where short_name = '6ed'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coercion'),
    (select id from sets where short_name = '6ed'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Regeneration'),
    (select id from sets where short_name = '6ed'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Elite Infantry'),
    (select id from sets where short_name = '6ed'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deflection'),
    (select id from sets where short_name = '6ed'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spell Blast'),
    (select id from sets where short_name = '6ed'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '6ed'),
    '350',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '6ed'),
    '337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boil'),
    (select id from sets where short_name = '6ed'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '6ed'),
    '344',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tranquility'),
    (select id from sets where short_name = '6ed'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hurricane'),
    (select id from sets where short_name = '6ed'),
    '237',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spirit Link'),
    (select id from sets where short_name = '6ed'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Derelor'),
    (select id from sets where short_name = '6ed'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sulfurous Springs'),
    (select id from sets where short_name = '6ed'),
    '328',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Regal Unicorn'),
    (select id from sets where short_name = '6ed'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Remedy'),
    (select id from sets where short_name = '6ed'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Vault'),
    (select id from sets where short_name = '6ed'),
    '307',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Blast'),
    (select id from sets where short_name = '6ed'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wyluli Wolf'),
    (select id from sets where short_name = '6ed'),
    '270',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = '6ed'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nature''s Resurgence'),
    (select id from sets where short_name = '6ed'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Razortooth Rats'),
    (select id from sets where short_name = '6ed'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = '6ed'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tidal Surge'),
    (select id from sets where short_name = '6ed'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Elemental'),
    (select id from sets where short_name = '6ed'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unyaro Griffin'),
    (select id from sets where short_name = '6ed'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prosperity'),
    (select id from sets where short_name = '6ed'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crusade'),
    (select id from sets where short_name = '6ed'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Black'),
    (select id from sets where short_name = '6ed'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tariff'),
    (select id from sets where short_name = '6ed'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Recruiter'),
    (select id from sets where short_name = '6ed'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Final Fortune'),
    (select id from sets where short_name = '6ed'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enfeeblement'),
    (select id from sets where short_name = '6ed'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sky Diamond'),
    (select id from sets where short_name = '6ed'),
    '311',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abduction'),
    (select id from sets where short_name = '6ed'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dream Cache'),
    (select id from sets where short_name = '6ed'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howling Mine'),
    (select id from sets where short_name = '6ed'),
    '290',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '6ed'),
    '338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Agonizing Memories'),
    (select id from sets where short_name = '6ed'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fatal Blow'),
    (select id from sets where short_name = '6ed'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = '6ed'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feast of the Unicorn'),
    (select id from sets where short_name = '6ed'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Castle'),
    (select id from sets where short_name = '6ed'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '6ed'),
    '333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diminishing Returns'),
    (select id from sets where short_name = '6ed'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scaled Wurm'),
    (select id from sets where short_name = '6ed'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scathe Zombies'),
    (select id from sets where short_name = '6ed'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hero''s Resolve'),
    (select id from sets where short_name = '6ed'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = '6ed'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk of the Pearl Trident'),
    (select id from sets where short_name = '6ed'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Early Harvest'),
    (select id from sets where short_name = '6ed'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warrior''s Honor'),
    (select id from sets where short_name = '6ed'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flame Spirit'),
    (select id from sets where short_name = '6ed'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crystal Rod'),
    (select id from sets where short_name = '6ed'),
    '277',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Order of the Sacred Torch'),
    (select id from sets where short_name = '6ed'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Adarkar Wastes'),
    (select id from sets where short_name = '6ed'),
    '319',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyrotechnics'),
    (select id from sets where short_name = '6ed'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forbidden Crypt'),
    (select id from sets where short_name = '6ed'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verduran Enchantress'),
    (select id from sets where short_name = '6ed'),
    '264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Perish'),
    (select id from sets where short_name = '6ed'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Venerable Monk'),
    (select id from sets where short_name = '6ed'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hidden Horror'),
    (select id from sets where short_name = '6ed'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Recall'),
    (select id from sets where short_name = '6ed'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brushland'),
    (select id from sets where short_name = '6ed'),
    '320',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '6ed'),
    '334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Familiar Ground'),
    (select id from sets where short_name = '6ed'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '6ed'),
    '349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skull Catapult'),
    (select id from sets where short_name = '6ed'),
    '310',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ethereal Champion'),
    (select id from sets where short_name = '6ed'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Charcoal Diamond'),
    (select id from sets where short_name = '6ed'),
    '276',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Waiting in the Weeds'),
    (select id from sets where short_name = '6ed'),
    '266',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rag Man'),
    (select id from sets where short_name = '6ed'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Terrain'),
    (select id from sets where short_name = '6ed'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dancing Scimitar'),
    (select id from sets where short_name = '6ed'),
    '279',
    'rare'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = '6ed'),
    '321',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Millstone'),
    (select id from sets where short_name = '6ed'),
    '300',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Digging Team'),
    (select id from sets where short_name = '6ed'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Ruins'),
    (select id from sets where short_name = '6ed'),
    '323',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Python'),
    (select id from sets where short_name = '6ed'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruins of Trokair'),
    (select id from sets where short_name = '6ed'),
    '327',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fyndhorn Elder'),
    (select id from sets where short_name = '6ed'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crimson Hellkite'),
    (select id from sets where short_name = '6ed'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meekstone'),
    (select id from sets where short_name = '6ed'),
    '299',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tundra Wolves'),
    (select id from sets where short_name = '6ed'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Call of the Wild'),
    (select id from sets where short_name = '6ed'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forget'),
    (select id from sets where short_name = '6ed'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Svyelunite Temple'),
    (select id from sets where short_name = '6ed'),
    '329',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Royal Guard'),
    (select id from sets where short_name = '6ed'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jade Monolith'),
    (select id from sets where short_name = '6ed'),
    '293',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = '6ed'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trained Armodon'),
    (select id from sets where short_name = '6ed'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Segovian Leviathan'),
    (select id from sets where short_name = '6ed'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '6ed'),
    '341',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat Warriors'),
    (select id from sets where short_name = '6ed'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fog Elemental'),
    (select id from sets where short_name = '6ed'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wind Drake'),
    (select id from sets where short_name = '6ed'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '6ed'),
    '340',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flying Carpet'),
    (select id from sets where short_name = '6ed'),
    '285',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pentagram of the Ages'),
    (select id from sets where short_name = '6ed'),
    '306',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infernal Contract'),
    (select id from sets where short_name = '6ed'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grinning Totem'),
    (select id from sets where short_name = '6ed'),
    '288',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '6ed'),
    '346',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ardent Militia'),
    (select id from sets where short_name = '6ed'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rowen'),
    (select id from sets where short_name = '6ed'),
    '250',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Obsianus Golem'),
    (select id from sets where short_name = '6ed'),
    '303',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drudge Skeletons'),
    (select id from sets where short_name = '6ed'),
    '123s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greed'),
    (select id from sets where short_name = '6ed'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stream of Life'),
    (select id from sets where short_name = '6ed'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illicit Auction'),
    (select id from sets where short_name = '6ed'),
    '190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancestral Memories'),
    (select id from sets where short_name = '6ed'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Engine'),
    (select id from sets where short_name = '6ed'),
    '282',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prodigal Sorcerer'),
    (select id from sets where short_name = '6ed'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Creeping Mold'),
    (select id from sets where short_name = '6ed'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heavy Ballista'),
    (select id from sets where short_name = '6ed'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Library of Lat-Nam'),
    (select id from sets where short_name = '6ed'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jokulhaups'),
    (select id from sets where short_name = '6ed'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blaze'),
    (select id from sets where short_name = '6ed'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jalum Tome'),
    (select id from sets where short_name = '6ed'),
    '294',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ekundu Griffin'),
    (select id from sets where short_name = '6ed'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Green'),
    (select id from sets where short_name = '6ed'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dread of Night'),
    (select id from sets where short_name = '6ed'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dingus Egg'),
    (select id from sets where short_name = '6ed'),
    '280',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pradesh Gypsies'),
    (select id from sets where short_name = '6ed'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '6ed'),
    '345',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Fire'),
    (select id from sets where short_name = '6ed'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blood Pet'),
    (select id from sets where short_name = '6ed'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elven Riders'),
    (select id from sets where short_name = '6ed'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mesa Falcon'),
    (select id from sets where short_name = '6ed'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Redwood Treefolk'),
    (select id from sets where short_name = '6ed'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Storm Cauldron'),
    (select id from sets where short_name = '6ed'),
    '314',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glasses of Urza'),
    (select id from sets where short_name = '6ed'),
    '287',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desertion'),
    (select id from sets where short_name = '6ed'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wild Growth'),
    (select id from sets where short_name = '6ed'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divine Transformation'),
    (select id from sets where short_name = '6ed'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Femeref Archers'),
    (select id from sets where short_name = '6ed'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Havenwood Battleground'),
    (select id from sets where short_name = '6ed'),
    '325',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ivory Cup'),
    (select id from sets where short_name = '6ed'),
    '292',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lead Golem'),
    (select id from sets where short_name = '6ed'),
    '296',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Worldly Tutor'),
    (select id from sets where short_name = '6ed'),
    '269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Hero'),
    (select id from sets where short_name = '6ed'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '6ed'),
    '335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insight'),
    (select id from sets where short_name = '6ed'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sabretooth Tiger'),
    (select id from sets where short_name = '6ed'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Mask'),
    (select id from sets where short_name = '6ed'),
    '283',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Horned Turtle'),
    (select id from sets where short_name = '6ed'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Uktabi Orangutan'),
    (select id from sets where short_name = '6ed'),
    '260',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Uktabi Wildcats'),
    (select id from sets where short_name = '6ed'),
    '261',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hammer of Bogardan'),
    (select id from sets where short_name = '6ed'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fit of Rage'),
    (select id from sets where short_name = '6ed'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Standing Troops'),
    (select id from sets where short_name = '6ed'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Embermage'),
    (select id from sets where short_name = '6ed'),
    '201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Dragon'),
    (select id from sets where short_name = '6ed'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Panther Warriors'),
    (select id from sets where short_name = '6ed'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viashino Warrior'),
    (select id from sets where short_name = '6ed'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fear'),
    (select id from sets where short_name = '6ed'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = '6ed'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unseen Walker'),
    (select id from sets where short_name = '6ed'),
    '262',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fyndhorn Brownie'),
    (select id from sets where short_name = '6ed'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Puzzle Box'),
    (select id from sets where short_name = '6ed'),
    '315',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fervor'),
    (select id from sets where short_name = '6ed'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Throne of Bone'),
    (select id from sets where short_name = '6ed'),
    '316',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Manabarbs'),
    (select id from sets where short_name = '6ed'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fallen Angel'),
    (select id from sets where short_name = '6ed'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Painful Memories'),
    (select id from sets where short_name = '6ed'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stupor'),
    (select id from sets where short_name = '6ed'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Exile'),
    (select id from sets where short_name = '6ed'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Howl from Beyond'),
    (select id from sets where short_name = '6ed'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ankh of Mishra'),
    (select id from sets where short_name = '6ed'),
    '273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spitting Earth'),
    (select id from sets where short_name = '6ed'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marble Diamond'),
    (select id from sets where short_name = '6ed'),
    '298',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disrupting Scepter'),
    (select id from sets where short_name = '6ed'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evil Eye of Orms-by-Gore'),
    (select id from sets where short_name = '6ed'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fountain of Youth'),
    (select id from sets where short_name = '6ed'),
    '286',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '6ed'),
    '342',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Flash'),
    (select id from sets where short_name = '6ed'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burrowing'),
    (select id from sets where short_name = '6ed'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra''s Blessing'),
    (select id from sets where short_name = '6ed'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '6ed'),
    '332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampiric Tutor'),
    (select id from sets where short_name = '6ed'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = '6ed'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spitting Drake'),
    (select id from sets where short_name = '6ed'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = '6ed'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warmth'),
    (select id from sets where short_name = '6ed'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maro'),
    (select id from sets where short_name = '6ed'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pillage'),
    (select id from sets where short_name = '6ed'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mischievous Poltergeist'),
    (select id from sets where short_name = '6ed'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kismet'),
    (select id from sets where short_name = '6ed'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anaba Shaman'),
    (select id from sets where short_name = '6ed'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Net'),
    (select id from sets where short_name = '6ed'),
    '313',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '6ed'),
    '331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rod of Ruin'),
    (select id from sets where short_name = '6ed'),
    '309',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Patagia Golem'),
    (select id from sets where short_name = '6ed'),
    '305',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warthog'),
    (select id from sets where short_name = '6ed'),
    '267',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashen Powder'),
    (select id from sets where short_name = '6ed'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lord of Atlantis'),
    (select id from sets where short_name = '6ed'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaseous Form'),
    (select id from sets where short_name = '6ed'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychic Transfer'),
    (select id from sets where short_name = '6ed'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harmattan Efreet'),
    (select id from sets where short_name = '6ed'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wind Spirit'),
    (select id from sets where short_name = '6ed'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elder Druid'),
    (select id from sets where short_name = '6ed'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Amber Prison'),
    (select id from sets where short_name = '6ed'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '6ed'),
    '347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doomsday'),
    (select id from sets where short_name = '6ed'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balduvian Barbarians'),
    (select id from sets where short_name = '6ed'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relentless Assault'),
    (select id from sets where short_name = '6ed'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conquer'),
    (select id from sets where short_name = '6ed'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gravebane Zombie'),
    (select id from sets where short_name = '6ed'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Samite Healer'),
    (select id from sets where short_name = '6ed'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Radjan Spirit'),
    (select id from sets where short_name = '6ed'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Warp'),
    (select id from sets where short_name = '6ed'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moss Diamond'),
    (select id from sets where short_name = '6ed'),
    '301',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Strength'),
    (select id from sets where short_name = '6ed'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: White'),
    (select id from sets where short_name = '6ed'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Polymorph'),
    (select id from sets where short_name = '6ed'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = '6ed'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infantry Veteran'),
    (select id from sets where short_name = '6ed'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drudge Skeletons'),
    (select id from sets where short_name = '6ed'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Compass'),
    (select id from sets where short_name = '6ed'),
    '302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enlightened Tutor'),
    (select id from sets where short_name = '6ed'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'D''Avenant Archer'),
    (select id from sets where short_name = '6ed'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jayemdae Tome'),
    (select id from sets where short_name = '6ed'),
    '295',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glacial Wall'),
    (select id from sets where short_name = '6ed'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zur''s Weirding'),
    (select id from sets where short_name = '6ed'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Memory Lapse'),
    (select id from sets where short_name = '6ed'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Animate Wall'),
    (select id from sets where short_name = '6ed'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armored Pegasus'),
    (select id from sets where short_name = '6ed'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = '6ed'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karplusan Forest'),
    (select id from sets where short_name = '6ed'),
    '326',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boomerang'),
    (select id from sets where short_name = '6ed'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wooden Sphere'),
    (select id from sets where short_name = '6ed'),
    '318',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '6ed'),
    '343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raging Goblin'),
    (select id from sets where short_name = '6ed'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flash'),
    (select id from sets where short_name = '6ed'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatterstorm'),
    (select id from sets where short_name = '6ed'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fire Diamond'),
    (select id from sets where short_name = '6ed'),
    '284',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '6ed'),
    '336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dense Foliage'),
    (select id from sets where short_name = '6ed'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wand of Denial'),
    (select id from sets where short_name = '6ed'),
    '317',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Icatian Town'),
    (select id from sets where short_name = '6ed'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dry Spell'),
    (select id from sets where short_name = '6ed'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lure'),
    (select id from sets where short_name = '6ed'),
    '240',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Swords'),
    (select id from sets where short_name = '6ed'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vodalian Soldiers'),
    (select id from sets where short_name = '6ed'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Imp'),
    (select id from sets where short_name = '6ed'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Hive'),
    (select id from sets where short_name = '6ed'),
    '289',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sibilant Spirit'),
    (select id from sets where short_name = '6ed'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = '6ed'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bottle of Suleiman'),
    (select id from sets where short_name = '6ed'),
    '275',
    'rare'
) ,
(
    (select id from mtgcard where name = 'River Boa'),
    (select id from sets where short_name = '6ed'),
    '249',
    'uncommon'
) 
 on conflict do nothing;
