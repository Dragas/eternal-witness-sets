    insert into types (name) values('Enchantment') on conflict do nothing;
    insert into types (name) values('Aura') on conflict do nothing;
    insert into types (name) values('Curse') on conflict do nothing;
    insert into types (name) values('Creature') on conflict do nothing;
    insert into types (name) values('Vampire') on conflict do nothing;
    insert into types (name) values('Rogue') on conflict do nothing;
    insert into types (name) values('Sorcery') on conflict do nothing;
