insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Curse of Thirst'),
    (select id from sets where short_name = 'pwp12'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nearheath Stalker'),
    (select id from sets where short_name = 'pwp12'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gather the Townsfolk'),
    (select id from sets where short_name = 'pwp12'),
    '79',
    'rare'
) 
 on conflict do nothing;
