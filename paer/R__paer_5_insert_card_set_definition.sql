insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Lightning Runner'),
    (select id from sets where short_name = 'paer'),
    '90s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aethersphere Harvester'),
    (select id from sets where short_name = 'paer'),
    '142s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yahenni''s Expertise'),
    (select id from sets where short_name = 'paer'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rishkar, Peema Renegade'),
    (select id from sets where short_name = 'paer'),
    '122s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Indomitable Creativity'),
    (select id from sets where short_name = 'paer'),
    '85s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aid from the Cowl'),
    (select id from sets where short_name = 'paer'),
    '105s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gonti''s Aether Heart'),
    (select id from sets where short_name = 'paer'),
    '152s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Exquisite Archangel'),
    (select id from sets where short_name = 'paer'),
    '18s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Greenwheel Liberator'),
    (select id from sets where short_name = 'paer'),
    '108s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trophy Mage'),
    (select id from sets where short_name = 'paer'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yahenni''s Expertise'),
    (select id from sets where short_name = 'paer'),
    '75s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disallow'),
    (select id from sets where short_name = 'paer'),
    '31s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Walking Ballista'),
    (select id from sets where short_name = 'paer'),
    '181s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aetherwind Basker'),
    (select id from sets where short_name = 'paer'),
    '104s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Yahenni, Undying Partisan'),
    (select id from sets where short_name = 'paer'),
    '74p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sram''s Expertise'),
    (select id from sets where short_name = 'paer'),
    '24s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scrap Trawler'),
    (select id from sets where short_name = 'paer'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merchant''s Dockhand'),
    (select id from sets where short_name = 'paer'),
    '163s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kari Zev, Skyship Raider'),
    (select id from sets where short_name = 'paer'),
    '87s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Peacewalker Colossus'),
    (select id from sets where short_name = 'paer'),
    '170s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Call for Unity'),
    (select id from sets where short_name = 'paer'),
    '9s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Greenbelt Rampager'),
    (select id from sets where short_name = 'paer'),
    '107s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kari Zev''s Expertise'),
    (select id from sets where short_name = 'paer'),
    '88s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scrap Trawler'),
    (select id from sets where short_name = 'paer'),
    '175s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quicksmith Rebel'),
    (select id from sets where short_name = 'paer'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lifecrafter''s Bestiary'),
    (select id from sets where short_name = 'paer'),
    '162p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inspiring Statuary'),
    (select id from sets where short_name = 'paer'),
    '160s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heroic Intervention'),
    (select id from sets where short_name = 'paer'),
    '109s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Baral, Chief of Compliance'),
    (select id from sets where short_name = 'paer'),
    '28s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Release the Gremlins'),
    (select id from sets where short_name = 'paer'),
    '96s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heroic Intervention'),
    (select id from sets where short_name = 'paer'),
    '109p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Paradox Engine'),
    (select id from sets where short_name = 'paer'),
    '169s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Whir of Invention'),
    (select id from sets where short_name = 'paer'),
    '49s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oath of Ajani'),
    (select id from sets where short_name = 'paer'),
    '131s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Freejam Regent'),
    (select id from sets where short_name = 'paer'),
    '81s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Intimations'),
    (select id from sets where short_name = 'paer'),
    '128s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Planar Bridge'),
    (select id from sets where short_name = 'paer'),
    '171p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Baral''s Expertise'),
    (select id from sets where short_name = 'paer'),
    '29s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hope of Ghirapur'),
    (select id from sets where short_name = 'paer'),
    '154s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whir of Invention'),
    (select id from sets where short_name = 'paer'),
    '49p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rishkar''s Expertise'),
    (select id from sets where short_name = 'paer'),
    '123s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mechanized Production'),
    (select id from sets where short_name = 'paer'),
    '38s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Spire of Industry'),
    (select id from sets where short_name = 'paer'),
    '184s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Secret Salvage'),
    (select id from sets where short_name = 'paer'),
    '71s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lifecrafter''s Bestiary'),
    (select id from sets where short_name = 'paer'),
    '162s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Planar Bridge'),
    (select id from sets where short_name = 'paer'),
    '171s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Herald of Anguish'),
    (select id from sets where short_name = 'paer'),
    '64s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Battle at the Bridge'),
    (select id from sets where short_name = 'paer'),
    '53s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aethergeode Miner'),
    (select id from sets where short_name = 'paer'),
    '4s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Metallic Mimic'),
    (select id from sets where short_name = 'paer'),
    '164s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quicksmith Rebel'),
    (select id from sets where short_name = 'paer'),
    '93s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pia''s Revolution'),
    (select id from sets where short_name = 'paer'),
    '91s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Consulate Crackdown'),
    (select id from sets where short_name = 'paer'),
    '11s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Midnight Entourage'),
    (select id from sets where short_name = 'paer'),
    '66s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glint-Sleeve Siphoner'),
    (select id from sets where short_name = 'paer'),
    '62s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aethertide Whale'),
    (select id from sets where short_name = 'paer'),
    '27s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani Unyielding'),
    (select id from sets where short_name = 'paer'),
    '127s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Disallow'),
    (select id from sets where short_name = 'paer'),
    '31p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heart of Kiran'),
    (select id from sets where short_name = 'paer'),
    '153s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Quicksmith Spy'),
    (select id from sets where short_name = 'paer'),
    '41s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yahenni, Undying Partisan'),
    (select id from sets where short_name = 'paer'),
    '74s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Solemn Recruit'),
    (select id from sets where short_name = 'paer'),
    '22s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sram, Senior Edificer'),
    (select id from sets where short_name = 'paer'),
    '23s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tezzeret the Schemer'),
    (select id from sets where short_name = 'paer'),
    '137s',
    'mythic'
) 
 on conflict do nothing;
