insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Weight of Memory'),
    (select id from sets where short_name = 'ajmp'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Carnifex Demon'),
    (select id from sets where short_name = 'ajmp'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Doomed Necromancer'),
    (select id from sets where short_name = 'ajmp'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Oriflamme'),
    (select id from sets where short_name = 'ajmp'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pollenbright Druid'),
    (select id from sets where short_name = 'ajmp'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dryad Greenseeker'),
    (select id from sets where short_name = 'ajmp'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Serpent'),
    (select id from sets where short_name = 'ajmp'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bond of Revival'),
    (select id from sets where short_name = 'ajmp'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Archon of Sun''s Grace'),
    (select id from sets where short_name = 'ajmp'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fanatic of Mogis'),
    (select id from sets where short_name = 'ajmp'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prey Upon'),
    (select id from sets where short_name = 'ajmp'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Strike'),
    (select id from sets where short_name = 'ajmp'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gadwick, the Wizened'),
    (select id from sets where short_name = 'ajmp'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scorching Dragonfire'),
    (select id from sets where short_name = 'ajmp'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woe Strider'),
    (select id from sets where short_name = 'ajmp'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Audacious Thief'),
    (select id from sets where short_name = 'ajmp'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Banishing Light'),
    (select id from sets where short_name = 'ajmp'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra''s Guardian'),
    (select id from sets where short_name = 'ajmp'),
    '310',
    'rare'
) 
 on conflict do nothing;
