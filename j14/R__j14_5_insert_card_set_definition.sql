insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Karador, Ghost Chieftain'),
    (select id from sets where short_name = 'j14'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword of Feast and Famine'),
    (select id from sets where short_name = 'j14'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Force of Will'),
    (select id from sets where short_name = 'j14'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'j14'),
    '2★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Riku of Two Reflections'),
    (select id from sets where short_name = 'j14'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'j14'),
    '3★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Greater Good'),
    (select id from sets where short_name = 'j14'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'j14'),
    '1★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oloro, Ageless Ascetic'),
    (select id from sets where short_name = 'j14'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nekusar, the Mindrazer'),
    (select id from sets where short_name = 'j14'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'j14'),
    '5★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'j14'),
    '4★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hanna, Ship''s Navigator'),
    (select id from sets where short_name = 'j14'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elesh Norn, Grand Cenobite'),
    (select id from sets where short_name = 'j14'),
    '8',
    'rare'
) 
 on conflict do nothing;
