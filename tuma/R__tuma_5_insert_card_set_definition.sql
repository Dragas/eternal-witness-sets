insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tuma'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tuma'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tuma'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marit Lage'),
    (select id from sets where short_name = 'tuma'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Citizen'),
    (select id from sets where short_name = 'tuma'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drake'),
    (select id from sets where short_name = 'tuma'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurm'),
    (select id from sets where short_name = 'tuma'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tuma'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Rogue'),
    (select id from sets where short_name = 'tuma'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tuma'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spark Elemental'),
    (select id from sets where short_name = 'tuma'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spider'),
    (select id from sets where short_name = 'tuma'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tuma'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ooze'),
    (select id from sets where short_name = 'tuma'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Homunculus'),
    (select id from sets where short_name = 'tuma'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tuma'),
    '13',
    'common'
) 
 on conflict do nothing;
