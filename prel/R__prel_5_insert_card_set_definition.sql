insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Hedge Troll'),
    (select id from sets where short_name = 'prel'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ass Whuppin'''),
    (select id from sets where short_name = 'prel'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Budoka Pupil // Ichiga, Who Topples Oaks'),
    (select id from sets where short_name = 'prel'),
    '3a',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rukh Egg'),
    (select id from sets where short_name = 'prel'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghost-Lit Raider'),
    (select id from sets where short_name = 'prel'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Force of Nature'),
    (select id from sets where short_name = 'prel'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shriekmaw'),
    (select id from sets where short_name = 'prel'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruul Guildmage'),
    (select id from sets where short_name = 'prel'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sudden Shock'),
    (select id from sets where short_name = 'prel'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = 'prel'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dimir Guildmage'),
    (select id from sets where short_name = 'prel'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azorius Guildmage'),
    (select id from sets where short_name = 'prel'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm Entity'),
    (select id from sets where short_name = 'prel'),
    '12',
    'rare'
) 
 on conflict do nothing;
