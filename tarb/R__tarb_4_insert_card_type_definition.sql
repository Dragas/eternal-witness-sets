insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Dragon'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dragon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dragon'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lizard'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lizard'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lizard'),
        (select types.id from types where name = 'Lizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bird Soldier'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bird Soldier'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bird Soldier'),
        (select types.id from types where name = 'Bird')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bird Soldier'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie Wizard'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie Wizard'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie Wizard'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie Wizard'),
        (select types.id from types where name = 'Wizard')
    ) 
 on conflict do nothing;
