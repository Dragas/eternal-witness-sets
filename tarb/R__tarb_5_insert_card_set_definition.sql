insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tarb'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lizard'),
    (select id from sets where short_name = 'tarb'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird Soldier'),
    (select id from sets where short_name = 'tarb'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Wizard'),
    (select id from sets where short_name = 'tarb'),
    '4',
    'common'
) 
 on conflict do nothing;
