insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Merfolk // Treasure'),
    (select id from sets where short_name = 'f18'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illusion // Saproling'),
    (select id from sets where short_name = 'f18'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'City''s Blessing // Elemental'),
    (select id from sets where short_name = 'f18'),
    '2',
    'common'
) 
 on conflict do nothing;
