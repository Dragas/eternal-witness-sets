insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ranger''s Guile'),
    (select id from sets where short_name = 'm15'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overwhelm'),
    (select id from sets where short_name = 'm15'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Will-Forged Golem'),
    (select id from sets where short_name = 'm15'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pillar of Light'),
    (select id from sets where short_name = 'm15'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul of Shandalar'),
    (select id from sets where short_name = 'm15'),
    '163',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mind Sculpt'),
    (select id from sets where short_name = 'm15'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Chain Veil'),
    (select id from sets where short_name = 'm15'),
    '215',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm15'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ephemeral Shields'),
    (select id from sets where short_name = 'm15'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necrobite'),
    (select id from sets where short_name = 'm15'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nissa, Worldwaker'),
    (select id from sets where short_name = 'm15'),
    '187',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tyrant''s Machine'),
    (select id from sets where short_name = 'm15'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foundry Street Denizen'),
    (select id from sets where short_name = 'm15'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brood Keeper'),
    (select id from sets where short_name = 'm15'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chord of Calling'),
    (select id from sets where short_name = 'm15'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Staff of the Flame Magus'),
    (select id from sets where short_name = 'm15'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis, Unshackled'),
    (select id from sets where short_name = 'm15'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightfire Giant'),
    (select id from sets where short_name = 'm15'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Siege Dragon'),
    (select id from sets where short_name = 'm15'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Borderland Marauder'),
    (select id from sets where short_name = 'm15'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm15'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chasm Skulker'),
    (select id from sets where short_name = 'm15'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Host'),
    (select id from sets where short_name = 'm15'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Divine Verdict'),
    (select id from sets where short_name = 'm15'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flesh to Dust'),
    (select id from sets where short_name = 'm15'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inspired Charge'),
    (select id from sets where short_name = 'm15'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm15'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stoke the Flames'),
    (select id from sets where short_name = 'm15'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battlefield Forge'),
    (select id from sets where short_name = 'm15'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hydrosurge'),
    (select id from sets where short_name = 'm15'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burning Anger'),
    (select id from sets where short_name = 'm15'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Devouring Light'),
    (select id from sets where short_name = 'm15'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rogue''s Gloves'),
    (select id from sets where short_name = 'm15'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Waste Not'),
    (select id from sets where short_name = 'm15'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Solemn Offering'),
    (select id from sets where short_name = 'm15'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Accursed Spirit'),
    (select id from sets where short_name = 'm15'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'm15'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quickling'),
    (select id from sets where short_name = 'm15'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm15'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Peel from Reality'),
    (select id from sets where short_name = 'm15'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necromancer''s Stockpile'),
    (select id from sets where short_name = 'm15'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oreskos Swiftclaw'),
    (select id from sets where short_name = 'm15'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ulcerate'),
    (select id from sets where short_name = 'm15'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kurkesh, Onakke Ancient'),
    (select id from sets where short_name = 'm15'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tormod''s Crypt'),
    (select id from sets where short_name = 'm15'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mass Calcify'),
    (select id from sets where short_name = 'm15'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Black Cat'),
    (select id from sets where short_name = 'm15'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Preeminent Captain'),
    (select id from sets where short_name = 'm15'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selfless Cathar'),
    (select id from sets where short_name = 'm15'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paragon of Open Graves'),
    (select id from sets where short_name = 'm15'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Restock'),
    (select id from sets where short_name = 'm15'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blastfire Bolt'),
    (select id from sets where short_name = 'm15'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Invisibility'),
    (select id from sets where short_name = 'm15'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paragon of New Dawns'),
    (select id from sets where short_name = 'm15'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'First Response'),
    (select id from sets where short_name = 'm15'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seraph of the Masses'),
    (select id from sets where short_name = 'm15'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clear a Path'),
    (select id from sets where short_name = 'm15'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raise the Alarm'),
    (select id from sets where short_name = 'm15'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paragon of Fierce Defiance'),
    (select id from sets where short_name = 'm15'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Centaur Courser'),
    (select id from sets where short_name = 'm15'),
    '282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Triplicate Spirits'),
    (select id from sets where short_name = 'm15'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Torch Fiend'),
    (select id from sets where short_name = 'm15'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crippling Blight'),
    (select id from sets where short_name = 'm15'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm15'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kird Chieftain'),
    (select id from sets where short_name = 'm15'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jalira, Master Polymorphist'),
    (select id from sets where short_name = 'm15'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Divine Favor'),
    (select id from sets where short_name = 'm15'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vineweft'),
    (select id from sets where short_name = 'm15'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aetherspouts'),
    (select id from sets where short_name = 'm15'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reclamation Sage'),
    (select id from sets where short_name = 'm15'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hornet Nest'),
    (select id from sets where short_name = 'm15'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chief Engineer'),
    (select id from sets where short_name = 'm15'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hornet Queen'),
    (select id from sets where short_name = 'm15'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roaring Primadox'),
    (select id from sets where short_name = 'm15'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm15'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Research Assistant'),
    (select id from sets where short_name = 'm15'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shadowcloak Vampire'),
    (select id from sets where short_name = 'm15'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm15'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Indulgent Tormentor'),
    (select id from sets where short_name = 'm15'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Congregate'),
    (select id from sets where short_name = 'm15'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Undergrowth Scavenger'),
    (select id from sets where short_name = 'm15'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm15'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fugitive Wizard'),
    (select id from sets where short_name = 'm15'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sungrace Pegasus'),
    (select id from sets where short_name = 'm15'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grindclock'),
    (select id from sets where short_name = 'm15'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oppressive Rays'),
    (select id from sets where short_name = 'm15'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crowd''s Favor'),
    (select id from sets where short_name = 'm15'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunt the Weak'),
    (select id from sets where short_name = 'm15'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Midnight Guard'),
    (select id from sets where short_name = 'm15'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illusory Angel'),
    (select id from sets where short_name = 'm15'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm15'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = 'm15'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paragon of Gathering Mists'),
    (select id from sets where short_name = 'm15'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forge Devil'),
    (select id from sets where short_name = 'm15'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rotfeaster Maggot'),
    (select id from sets where short_name = 'm15'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nimbus of the Isles'),
    (select id from sets where short_name = 'm15'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Life''s Legacy'),
    (select id from sets where short_name = 'm15'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunblade Elf'),
    (select id from sets where short_name = 'm15'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'm15'),
    '273',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm15'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana Vess'),
    (select id from sets where short_name = 'm15'),
    '103',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Frost Lynx'),
    (select id from sets where short_name = 'm15'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scuttling Doom Engine'),
    (select id from sets where short_name = 'm15'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lava Axe'),
    (select id from sets where short_name = 'm15'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm15'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul of Theros'),
    (select id from sets where short_name = 'm15'),
    '34',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sanctified Charge'),
    (select id from sets where short_name = 'm15'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'm15'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Genesis Hydra'),
    (select id from sets where short_name = 'm15'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Frost'),
    (select id from sets where short_name = 'm15'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aegis Angel'),
    (select id from sets where short_name = 'm15'),
    '270',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yisan, the Wanderer Bard'),
    (select id from sets where short_name = 'm15'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Razorfoot Griffin'),
    (select id from sets where short_name = 'm15'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cone of Flame'),
    (select id from sets where short_name = 'm15'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Limbs'),
    (select id from sets where short_name = 'm15'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spectra Ward'),
    (select id from sets where short_name = 'm15'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gargoyle Sentinel'),
    (select id from sets where short_name = 'm15'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Fire'),
    (select id from sets where short_name = 'm15'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm15'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Into the Void'),
    (select id from sets where short_name = 'm15'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = 'm15'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm15'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inferno Fist'),
    (select id from sets where short_name = 'm15'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Essence'),
    (select id from sets where short_name = 'm15'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Generator Servant'),
    (select id from sets where short_name = 'm15'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Covenant of Blood'),
    (select id from sets where short_name = 'm15'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Might Makes Right'),
    (select id from sets where short_name = 'm15'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heat Ray'),
    (select id from sets where short_name = 'm15'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thundering Giant'),
    (select id from sets where short_name = 'm15'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battle Mastery'),
    (select id from sets where short_name = 'm15'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jorubai Murk Lurker'),
    (select id from sets where short_name = 'm15'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Silverback'),
    (select id from sets where short_name = 'm15'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hushwing Gryff'),
    (select id from sets where short_name = 'm15'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa''s Expedition'),
    (select id from sets where short_name = 'm15'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Juggernaut'),
    (select id from sets where short_name = 'm15'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Satyr Wayfinder'),
    (select id from sets where short_name = 'm15'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hoarding Dragon'),
    (select id from sets where short_name = 'm15'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frenzied Goblin'),
    (select id from sets where short_name = 'm15'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heliod''s Pilgrim'),
    (select id from sets where short_name = 'm15'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Radiant Fountain'),
    (select id from sets where short_name = 'm15'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Kaboomist'),
    (select id from sets where short_name = 'm15'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Caustic Tar'),
    (select id from sets where short_name = 'm15'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ensoul Artifact'),
    (select id from sets where short_name = 'm15'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Staff of the Death Magus'),
    (select id from sets where short_name = 'm15'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boonweaver Giant'),
    (select id from sets where short_name = 'm15'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul of Zendikar'),
    (select id from sets where short_name = 'm15'),
    '201',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Leeching Sliver'),
    (select id from sets where short_name = 'm15'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necromancer''s Assistant'),
    (select id from sets where short_name = 'm15'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Witch''s Familiar'),
    (select id from sets where short_name = 'm15'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Venom Sliver'),
    (select id from sets where short_name = 'm15'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shaman of Spring'),
    (select id from sets where short_name = 'm15'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charging Rhino'),
    (select id from sets where short_name = 'm15'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Geist of the Moors'),
    (select id from sets where short_name = 'm15'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Roughrider'),
    (select id from sets where short_name = 'm15'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Xathrid Slyblade'),
    (select id from sets where short_name = 'm15'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soulmender'),
    (select id from sets where short_name = 'm15'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul of Innistrad'),
    (select id from sets where short_name = 'm15'),
    '115',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sliver Hive'),
    (select id from sets where short_name = 'm15'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eternal Thirst'),
    (select id from sets where short_name = 'm15'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Revoker'),
    (select id from sets where short_name = 'm15'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Void Snare'),
    (select id from sets where short_name = 'm15'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Flame'),
    (select id from sets where short_name = 'm15'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seismic Strike'),
    (select id from sets where short_name = 'm15'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shield of the Avatar'),
    (select id from sets where short_name = 'm15'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Paragon of Eternal Wilds'),
    (select id from sets where short_name = 'm15'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Carrion Crow'),
    (select id from sets where short_name = 'm15'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'm15'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zof Shade'),
    (select id from sets where short_name = 'm15'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace, the Living Guildpact'),
    (select id from sets where short_name = 'm15'),
    '62',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Perilous Vault'),
    (select id from sets where short_name = 'm15'),
    '224',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Coast'),
    (select id from sets where short_name = 'm15'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Typhoid Rats'),
    (select id from sets where short_name = 'm15'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sign in Blood'),
    (select id from sets where short_name = 'm15'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Return to the Ranks'),
    (select id from sets where short_name = 'm15'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stormtide Leviathan'),
    (select id from sets where short_name = 'm15'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cruel Sadist'),
    (select id from sets where short_name = 'm15'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Garruk, Apex Predator'),
    (select id from sets where short_name = 'm15'),
    '210',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm15'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Strike'),
    (select id from sets where short_name = 'm15'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul of Ravnica'),
    (select id from sets where short_name = 'm15'),
    '78',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wall of Mulch'),
    (select id from sets where short_name = 'm15'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avacyn, Guardian Angel'),
    (select id from sets where short_name = 'm15'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Polymorphist''s Jest'),
    (select id from sets where short_name = 'm15'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Netcaster Spider'),
    (select id from sets where short_name = 'm15'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm15'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kapsho Kitefins'),
    (select id from sets where short_name = 'm15'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spirit Bonds'),
    (select id from sets where short_name = 'm15'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Caves of Koilos'),
    (select id from sets where short_name = 'm15'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Siege Wurm'),
    (select id from sets where short_name = 'm15'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace''s Ingenuity'),
    (select id from sets where short_name = 'm15'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Constricting Sliver'),
    (select id from sets where short_name = 'm15'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Statute of Denial'),
    (select id from sets where short_name = 'm15'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm15'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sacred Armory'),
    (select id from sets where short_name = 'm15'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darksteel Citadel'),
    (select id from sets where short_name = 'm15'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = 'm15'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mercurial Pretender'),
    (select id from sets where short_name = 'm15'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = 'm15'),
    '277',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Titanic Growth'),
    (select id from sets where short_name = 'm15'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Back to Nature'),
    (select id from sets where short_name = 'm15'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rummaging Goblin'),
    (select id from sets where short_name = 'm15'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Belligerent Sliver'),
    (select id from sets where short_name = 'm15'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Garruk''s Packleader'),
    (select id from sets where short_name = 'm15'),
    '283',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sliver Hivelord'),
    (select id from sets where short_name = 'm15'),
    '211',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chronostutter'),
    (select id from sets where short_name = 'm15'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carnivorous Moss-Beast'),
    (select id from sets where short_name = 'm15'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Verdant Haven'),
    (select id from sets where short_name = 'm15'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Act on Impulse'),
    (select id from sets where short_name = 'm15'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Child of Night'),
    (select id from sets where short_name = 'm15'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = 'm15'),
    '276',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stab Wound'),
    (select id from sets where short_name = 'm15'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Invasive Species'),
    (select id from sets where short_name = 'm15'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necrogen Scudder'),
    (select id from sets where short_name = 'm15'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tireless Missionaries'),
    (select id from sets where short_name = 'm15'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul of New Phyrexia'),
    (select id from sets where short_name = 'm15'),
    '231',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Miner''s Bane'),
    (select id from sets where short_name = 'm15'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Endless Obedience'),
    (select id from sets where short_name = 'm15'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stain the Mind'),
    (select id from sets where short_name = 'm15'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mahamoti Djinn'),
    (select id from sets where short_name = 'm15'),
    '275',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Living Totem'),
    (select id from sets where short_name = 'm15'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hot Soup'),
    (select id from sets where short_name = 'm15'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Walking Corpse'),
    (select id from sets where short_name = 'm15'),
    '278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terra Stomper'),
    (select id from sets where short_name = 'm15'),
    '284',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crucible of Fire'),
    (select id from sets where short_name = 'm15'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krenko''s Enforcer'),
    (select id from sets where short_name = 'm15'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plummet'),
    (select id from sets where short_name = 'm15'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Staff of the Mind Magus'),
    (select id from sets where short_name = 'm15'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Reef'),
    (select id from sets where short_name = 'm15'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warden of the Beyond'),
    (select id from sets where short_name = 'm15'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cancel'),
    (select id from sets where short_name = 'm15'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runeclaw Bear'),
    (select id from sets where short_name = 'm15'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunter''s Ambush'),
    (select id from sets where short_name = 'm15'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Furnace Whelp'),
    (select id from sets where short_name = 'm15'),
    '279',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unmake the Graves'),
    (select id from sets where short_name = 'm15'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm15'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Wastes'),
    (select id from sets where short_name = 'm15'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meteorite'),
    (select id from sets where short_name = 'm15'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feral Incarnation'),
    (select id from sets where short_name = 'm15'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm15'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feast on the Fallen'),
    (select id from sets where short_name = 'm15'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kalonian Twingrove'),
    (select id from sets where short_name = 'm15'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marked by Honor'),
    (select id from sets where short_name = 'm15'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dissipate'),
    (select id from sets where short_name = 'm15'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Divination'),
    (select id from sets where short_name = 'm15'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Mystic'),
    (select id from sets where short_name = 'm15'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shrapnel Blast'),
    (select id from sets where short_name = 'm15'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phytotitan'),
    (select id from sets where short_name = 'm15'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aeronaut Tinkerer'),
    (select id from sets where short_name = 'm15'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aggressive Mining'),
    (select id from sets where short_name = 'm15'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Staff of the Sun Magus'),
    (select id from sets where short_name = 'm15'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Resolute Archangel'),
    (select id from sets where short_name = 'm15'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Pridemate'),
    (select id from sets where short_name = 'm15'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gather Courage'),
    (select id from sets where short_name = 'm15'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Staff of the Wild Magus'),
    (select id from sets where short_name = 'm15'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hammerhand'),
    (select id from sets where short_name = 'm15'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bronze Sable'),
    (select id from sets where short_name = 'm15'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glacial Crasher'),
    (select id from sets where short_name = 'm15'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra, Pyromaster'),
    (select id from sets where short_name = 'm15'),
    '134',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Welkin Tern'),
    (select id from sets where short_name = 'm15'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Amphin Pathmage'),
    (select id from sets where short_name = 'm15'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scrapyard Mongrel'),
    (select id from sets where short_name = 'm15'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avarice Amulet'),
    (select id from sets where short_name = 'm15'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Urd'),
    (select id from sets where short_name = 'm15'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brawler''s Plate'),
    (select id from sets where short_name = 'm15'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ornithopter'),
    (select id from sets where short_name = 'm15'),
    '223',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Rabblemaster'),
    (select id from sets where short_name = 'm15'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Turn to Frog'),
    (select id from sets where short_name = 'm15'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dauntless River Marshal'),
    (select id from sets where short_name = 'm15'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Master of Predicaments'),
    (select id from sets where short_name = 'm15'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Military Intelligence'),
    (select id from sets where short_name = 'm15'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm15'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Profane Memento'),
    (select id from sets where short_name = 'm15'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Festergloom'),
    (select id from sets where short_name = 'm15'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meditation Puzzle'),
    (select id from sets where short_name = 'm15'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coral Barrier'),
    (select id from sets where short_name = 'm15'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Encrust'),
    (select id from sets where short_name = 'm15'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Altac Bloodseeker'),
    (select id from sets where short_name = 'm15'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'In Garruk''s Wake'),
    (select id from sets where short_name = 'm15'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Haunted Plate Mail'),
    (select id from sets where short_name = 'm15'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani Steadfast'),
    (select id from sets where short_name = 'm15'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Urborg, Tomb of Yawgmoth'),
    (select id from sets where short_name = 'm15'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm15'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diffusion Sliver'),
    (select id from sets where short_name = 'm15'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kinsbaile Skirmisher'),
    (select id from sets where short_name = 'm15'),
    '16',
    'common'
) 
 on conflict do nothing;
