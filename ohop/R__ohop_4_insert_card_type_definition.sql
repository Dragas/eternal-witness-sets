insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Krosa'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Krosa'),
        (select types.id from types where name = 'Dominaria')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fields of Summer'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fields of Summer'),
        (select types.id from types where name = 'Moag')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Great Forest'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Great Forest'),
        (select types.id from types where name = 'Lorwyn')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shiv'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shiv'),
        (select types.id from types where name = 'Dominaria')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Feeding Grounds'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Feeding Grounds'),
        (select types.id from types where name = 'Muraganda')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lethe Lake'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lethe Lake'),
        (select types.id from types where name = 'Arkhos')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Hippodrome'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Hippodrome'),
        (select types.id from types where name = 'Segovia')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Naar Isle'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Naar Isle'),
        (select types.id from types where name = 'Wildfire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Dark Barony'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Dark Barony'),
        (select types.id from types where name = 'Ulgrotha')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Agyrem'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Agyrem'),
        (select types.id from types where name = 'Ravnica')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Academy at Tolaria West'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Academy at Tolaria West'),
        (select types.id from types where name = 'Dominaria')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Immersturm'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Immersturm'),
        (select types.id from types where name = 'Valla')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Glimmervoid Basin'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Glimmervoid Basin'),
        (select types.id from types where name = 'Mirrodin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Llanowar'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Llanowar'),
        (select types.id from types where name = 'Dominaria')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sanctum of Serra'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sanctum of Serra'),
        (select types.id from types where name = 'Serra’s')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sanctum of Serra'),
        (select types.id from types where name = 'Realm')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Minamo'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Minamo'),
        (select types.id from types where name = 'Kamigawa')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eloren Wilds'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eloren Wilds'),
        (select types.id from types where name = 'Shandalar')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sokenzan'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sokenzan'),
        (select types.id from types where name = 'Kamigawa')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Naya'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Naya'),
        (select types.id from types where name = 'Alara')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pools of Becoming'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pools of Becoming'),
        (select types.id from types where name = 'Bolas’s')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pools of Becoming'),
        (select types.id from types where name = 'Meditation')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pools of Becoming'),
        (select types.id from types where name = 'Realm')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sea of Sand'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sea of Sand'),
        (select types.id from types where name = 'Rabiah')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Aether Flues'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Aether Flues'),
        (select types.id from types where name = 'Iquatana')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Fourth Sphere'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Fourth Sphere'),
        (select types.id from types where name = 'Phyrexia')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Otaria'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Otaria'),
        (select types.id from types where name = 'Dominaria')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Murasa'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Murasa'),
        (select types.id from types where name = 'Zendikar')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cliffside Market'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cliffside Market'),
        (select types.id from types where name = 'Mercadia')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stronghold Furnace'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stronghold Furnace'),
        (select types.id from types where name = 'Rath')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bant'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bant'),
        (select types.id from types where name = 'Alara')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Raven''s Run'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Raven''s Run'),
        (select types.id from types where name = 'Shadowmoor')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Isle of Vesuva'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Isle of Vesuva'),
        (select types.id from types where name = 'Dominaria')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Turri Island'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Turri Island'),
        (select types.id from types where name = 'Ir')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goldmeadow'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goldmeadow'),
        (select types.id from types where name = 'Lorwyn')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Eon Fog'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Eon Fog'),
        (select types.id from types where name = 'Equilor')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Izzet Steam Maze'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Izzet Steam Maze'),
        (select types.id from types where name = 'Ravnica')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Velis Vel'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Velis Vel'),
        (select types.id from types where name = 'Lorwyn')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Maelstrom'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Maelstrom'),
        (select types.id from types where name = 'Alara')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grixis'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grixis'),
        (select types.id from types where name = 'Alara')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Undercity Reaches'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Undercity Reaches'),
        (select types.id from types where name = 'Ravnica')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Panopticon'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Panopticon'),
        (select types.id from types where name = 'Mirrodin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skybreen'),
        (select types.id from types where name = 'Plane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skybreen'),
        (select types.id from types where name = 'Kaldheim')
    ) 
 on conflict do nothing;
