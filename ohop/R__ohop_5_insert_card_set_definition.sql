insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Krosa'),
    (select id from sets where short_name = 'ohop'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fields of Summer'),
    (select id from sets where short_name = 'ohop'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Great Forest'),
    (select id from sets where short_name = 'ohop'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shiv'),
    (select id from sets where short_name = 'ohop'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feeding Grounds'),
    (select id from sets where short_name = 'ohop'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lethe Lake'),
    (select id from sets where short_name = 'ohop'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Hippodrome'),
    (select id from sets where short_name = 'ohop'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naar Isle'),
    (select id from sets where short_name = 'ohop'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Dark Barony'),
    (select id from sets where short_name = 'ohop'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Agyrem'),
    (select id from sets where short_name = 'ohop'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Academy at Tolaria West'),
    (select id from sets where short_name = 'ohop'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Immersturm'),
    (select id from sets where short_name = 'ohop'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glimmervoid Basin'),
    (select id from sets where short_name = 'ohop'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar'),
    (select id from sets where short_name = 'ohop'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctum of Serra'),
    (select id from sets where short_name = 'ohop'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minamo'),
    (select id from sets where short_name = 'ohop'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eloren Wilds'),
    (select id from sets where short_name = 'ohop'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sokenzan'),
    (select id from sets where short_name = 'ohop'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naya'),
    (select id from sets where short_name = 'ohop'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pools of Becoming'),
    (select id from sets where short_name = 'ohop'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea of Sand'),
    (select id from sets where short_name = 'ohop'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Aether Flues'),
    (select id from sets where short_name = 'ohop'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Fourth Sphere'),
    (select id from sets where short_name = 'ohop'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Otaria'),
    (select id from sets where short_name = 'ohop'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murasa'),
    (select id from sets where short_name = 'ohop'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cliffside Market'),
    (select id from sets where short_name = 'ohop'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stronghold Furnace'),
    (select id from sets where short_name = 'ohop'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bant'),
    (select id from sets where short_name = 'ohop'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raven''s Run'),
    (select id from sets where short_name = 'ohop'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Isle of Vesuva'),
    (select id from sets where short_name = 'ohop'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Turri Island'),
    (select id from sets where short_name = 'ohop'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goldmeadow'),
    (select id from sets where short_name = 'ohop'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Eon Fog'),
    (select id from sets where short_name = 'ohop'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Steam Maze'),
    (select id from sets where short_name = 'ohop'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Velis Vel'),
    (select id from sets where short_name = 'ohop'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Maelstrom'),
    (select id from sets where short_name = 'ohop'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grixis'),
    (select id from sets where short_name = 'ohop'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undercity Reaches'),
    (select id from sets where short_name = 'ohop'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Panopticon'),
    (select id from sets where short_name = 'ohop'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skybreen'),
    (select id from sets where short_name = 'ohop'),
    '35',
    'common'
) 
 on conflict do nothing;
