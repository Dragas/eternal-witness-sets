insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Jeleva, Nephalia''s Scourge'),
    (select id from sets where short_name = 'oc13'),
    '194',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Roon of the Hidden Realm'),
    (select id from sets where short_name = 'oc13'),
    '206',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sek''Kuar, Deathkeeper'),
    (select id from sets where short_name = 'oc13'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mayael the Anima'),
    (select id from sets where short_name = 'oc13'),
    '199',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Oloro, Ageless Ascetic'),
    (select id from sets where short_name = 'oc13'),
    '203',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nekusar, the Mindrazer'),
    (select id from sets where short_name = 'oc13'),
    '201',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sydri, Galvanic Genius'),
    (select id from sets where short_name = 'oc13'),
    '220',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thraximundar'),
    (select id from sets where short_name = 'oc13'),
    '221',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Derevi, Empyrial Tactician'),
    (select id from sets where short_name = 'oc13'),
    '186',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gahiji, Honored One'),
    (select id from sets where short_name = 'oc13'),
    '191',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sharuum the Hegemon'),
    (select id from sets where short_name = 'oc13'),
    '212',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Marath, Will of the Wild'),
    (select id from sets where short_name = 'oc13'),
    '198',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shattergang Brothers'),
    (select id from sets where short_name = 'oc13'),
    '213',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Prossh, Skyraider of Kher'),
    (select id from sets where short_name = 'oc13'),
    '204',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rubinia Soulsinger'),
    (select id from sets where short_name = 'oc13'),
    '207',
    'rare'
) 
 on conflict do nothing;
