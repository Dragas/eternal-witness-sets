insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Nocturnal Raid'),
    (select id from sets where short_name = 'mir'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dread Specter'),
    (select id from sets where short_name = 'mir'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meddle'),
    (select id from sets where short_name = 'mir'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Amber Prison'),
    (select id from sets where short_name = 'mir'),
    '292',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Imp'),
    (select id from sets where short_name = 'mir'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Consuming Ferocity'),
    (select id from sets where short_name = 'mir'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shauku, Endbringer'),
    (select id from sets where short_name = 'mir'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cadaverous Knight'),
    (select id from sets where short_name = 'mir'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ivory Charm'),
    (select id from sets where short_name = 'mir'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unseen Walker'),
    (select id from sets where short_name = 'mir'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abyssal Hunter'),
    (select id from sets where short_name = 'mir'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Telim''Tor''s Edict'),
    (select id from sets where short_name = 'mir'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coral Fighters'),
    (select id from sets where short_name = 'mir'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Vault'),
    (select id from sets where short_name = 'mir'),
    '316',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kaervek''s Hex'),
    (select id from sets where short_name = 'mir'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kaervek''s Torch'),
    (select id from sets where short_name = 'mir'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ether Well'),
    (select id from sets where short_name = 'mir'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reparations'),
    (select id from sets where short_name = 'mir'),
    '278',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Breathstealer'),
    (select id from sets where short_name = 'mir'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Healing Salve'),
    (select id from sets where short_name = 'mir'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shadowbane'),
    (select id from sets where short_name = 'mir'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stupor'),
    (select id from sets where short_name = 'mir'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blind Fury'),
    (select id from sets where short_name = 'mir'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wave Elemental'),
    (select id from sets where short_name = 'mir'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mire Shade'),
    (select id from sets where short_name = 'mir'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barbed Foliage'),
    (select id from sets where short_name = 'mir'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sea Scryer'),
    (select id from sets where short_name = 'mir'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thirst'),
    (select id from sets where short_name = 'mir'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Elite Infantry'),
    (select id from sets where short_name = 'mir'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Subterranean Spirit'),
    (select id from sets where short_name = 'mir'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fallow Earth'),
    (select id from sets where short_name = 'mir'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alarum'),
    (select id from sets where short_name = 'mir'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mtenda Griffin'),
    (select id from sets where short_name = 'mir'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karoo Meerkat'),
    (select id from sets where short_name = 'mir'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spatial Binding'),
    (select id from sets where short_name = 'mir'),
    '284',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hall of Gemstone'),
    (select id from sets where short_name = 'mir'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Torrent of Lava'),
    (select id from sets where short_name = 'mir'),
    '199',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crystal Vein'),
    (select id from sets where short_name = 'mir'),
    '325',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'mir'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barbed-Back Wurm'),
    (select id from sets where short_name = 'mir'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flare'),
    (select id from sets where short_name = 'mir'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'mir'),
    '346',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bone Harvest'),
    (select id from sets where short_name = 'mir'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rocky Tar Pit'),
    (select id from sets where short_name = 'mir'),
    '329',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'mir'),
    '337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bone Mask'),
    (select id from sets where short_name = 'mir'),
    '295',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'mir'),
    '344',
    'common'
) ,
(
    (select id from mtgcard where name = 'Canopy Dragon'),
    (select id from sets where short_name = 'mir'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ritual of Steel'),
    (select id from sets where short_name = 'mir'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Village Elder'),
    (select id from sets where short_name = 'mir'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Roots of Life'),
    (select id from sets where short_name = 'mir'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Reflexes'),
    (select id from sets where short_name = 'mir'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sapphire Charm'),
    (select id from sets where short_name = 'mir'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spectral Guardian'),
    (select id from sets where short_name = 'mir'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benevolent Unicorn'),
    (select id from sets where short_name = 'mir'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charcoal Diamond'),
    (select id from sets where short_name = 'mir'),
    '296',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Mantis'),
    (select id from sets where short_name = 'mir'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'mir'),
    '340',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chariot of the Sun'),
    (select id from sets where short_name = 'mir'),
    '297',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crimson Roc'),
    (select id from sets where short_name = 'mir'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dwarven Nomad'),
    (select id from sets where short_name = 'mir'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Purge'),
    (select id from sets where short_name = 'mir'),
    '273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jolrael''s Centaur'),
    (select id from sets where short_name = 'mir'),
    '222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harbinger of Night'),
    (select id from sets where short_name = 'mir'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unfulfilled Desires'),
    (select id from sets where short_name = 'mir'),
    '285',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Telim''Tor'),
    (select id from sets where short_name = 'mir'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Paupers'' Cage'),
    (select id from sets where short_name = 'mir'),
    '314',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sealed Fate'),
    (select id from sets where short_name = 'mir'),
    '282',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dissipate'),
    (select id from sets where short_name = 'mir'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reflect Damage'),
    (select id from sets where short_name = 'mir'),
    '277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ekundu Griffin'),
    (select id from sets where short_name = 'mir'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disempower'),
    (select id from sets where short_name = 'mir'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mist Dragon'),
    (select id from sets where short_name = 'mir'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jolt'),
    (select id from sets where short_name = 'mir'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Political Trickery'),
    (select id from sets where short_name = 'mir'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Granger Guildmage'),
    (select id from sets where short_name = 'mir'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloak of Invisibility'),
    (select id from sets where short_name = 'mir'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = 'mir'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pearl Dragon'),
    (select id from sets where short_name = 'mir'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sewer Rats'),
    (select id from sets where short_name = 'mir'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ray of Command'),
    (select id from sets where short_name = 'mir'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Superior Numbers'),
    (select id from sets where short_name = 'mir'),
    '244',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sandbar Crocodile'),
    (select id from sets where short_name = 'mir'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Afterlife'),
    (select id from sets where short_name = 'mir'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Catacomb Dragon'),
    (select id from sets where short_name = 'mir'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Purraj of Urborg'),
    (select id from sets where short_name = 'mir'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marble Diamond'),
    (select id from sets where short_name = 'mir'),
    '310',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burning Shield Askari'),
    (select id from sets where short_name = 'mir'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sabertooth Cobra'),
    (select id from sets where short_name = 'mir'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viashino Warrior'),
    (select id from sets where short_name = 'mir'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Sink'),
    (select id from sets where short_name = 'mir'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flame Elemental'),
    (select id from sets where short_name = 'mir'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tidal Wave'),
    (select id from sets where short_name = 'mir'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cycle of Life'),
    (select id from sets where short_name = 'mir'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shaper Guildmage'),
    (select id from sets where short_name = 'mir'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crystal Golem'),
    (select id from sets where short_name = 'mir'),
    '298',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunweb'),
    (select id from sets where short_name = 'mir'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Divine Offering'),
    (select id from sets where short_name = 'mir'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fetid Horror'),
    (select id from sets where short_name = 'mir'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Early Harvest'),
    (select id from sets where short_name = 'mir'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crypt Cobra'),
    (select id from sets where short_name = 'mir'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'mir'),
    '342',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frenetic Efreet'),
    (select id from sets where short_name = 'mir'),
    '264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Talruum Minotaur'),
    (select id from sets where short_name = 'mir'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mtenda Herder'),
    (select id from sets where short_name = 'mir'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ventifact Bottle'),
    (select id from sets where short_name = 'mir'),
    '323',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Psychic Transfer'),
    (select id from sets where short_name = 'mir'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mangara''s Tome'),
    (select id from sets where short_name = 'mir'),
    '309',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Agility'),
    (select id from sets where short_name = 'mir'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blistering Barrier'),
    (select id from sets where short_name = 'mir'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teeka''s Dragon'),
    (select id from sets where short_name = 'mir'),
    '320',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Telim''Tor''s Darts'),
    (select id from sets where short_name = 'mir'),
    '321',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Preferred Selection'),
    (select id from sets where short_name = 'mir'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jungle Troll'),
    (select id from sets where short_name = 'mir'),
    '269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ersatz Gnomes'),
    (select id from sets where short_name = 'mir'),
    '301',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Harness'),
    (select id from sets where short_name = 'mir'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soulshriek'),
    (select id from sets where short_name = 'mir'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cerulean Wyvern'),
    (select id from sets where short_name = 'mir'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shimmer'),
    (select id from sets where short_name = 'mir'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Searing Spear Askari'),
    (select id from sets where short_name = 'mir'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystical Tutor'),
    (select id from sets where short_name = 'mir'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ravenous Vampire'),
    (select id from sets where short_name = 'mir'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Waiting in the Weeds'),
    (select id from sets where short_name = 'mir'),
    '252',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Banishing'),
    (select id from sets where short_name = 'mir'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vitalizing Cascade'),
    (select id from sets where short_name = 'mir'),
    '286',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Basalt Golem'),
    (select id from sets where short_name = 'mir'),
    '294',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Femeref Scouts'),
    (select id from sets where short_name = 'mir'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elixir of Vitality'),
    (select id from sets where short_name = 'mir'),
    '300',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merfolk Seer'),
    (select id from sets where short_name = 'mir'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Memory Lapse'),
    (select id from sets where short_name = 'mir'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flash'),
    (select id from sets where short_name = 'mir'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lion''s Eye Diamond'),
    (select id from sets where short_name = 'mir'),
    '307',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'mir'),
    '341',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windreaper Falcon'),
    (select id from sets where short_name = 'mir'),
    '289',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Noble Elephant'),
    (select id from sets where short_name = 'mir'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grasslands'),
    (select id from sets where short_name = 'mir'),
    '327',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sacred Mesa'),
    (select id from sets where short_name = 'mir'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feral Shadow'),
    (select id from sets where short_name = 'mir'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'mir'),
    '345',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashen Powder'),
    (select id from sets where short_name = 'mir'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Soothsayer'),
    (select id from sets where short_name = 'mir'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hazerider Drake'),
    (select id from sets where short_name = 'mir'),
    '268',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crimson Hellkite'),
    (select id from sets where short_name = 'mir'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brushwagg'),
    (select id from sets where short_name = 'mir'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ward of Lights'),
    (select id from sets where short_name = 'mir'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mangara''s Equity'),
    (select id from sets where short_name = 'mir'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fog'),
    (select id from sets where short_name = 'mir'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wildfire Emissary'),
    (select id from sets where short_name = 'mir'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Acidic Dagger'),
    (select id from sets where short_name = 'mir'),
    '291',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gravebane Zombie'),
    (select id from sets where short_name = 'mir'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Withering Boon'),
    (select id from sets where short_name = 'mir'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shauku''s Minion'),
    (select id from sets where short_name = 'mir'),
    '283',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Energy Bolt'),
    (select id from sets where short_name = 'mir'),
    '263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lure of Prey'),
    (select id from sets where short_name = 'mir'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unyaro Bee Sting'),
    (select id from sets where short_name = 'mir'),
    '250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'mir'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Razor Pendulum'),
    (select id from sets where short_name = 'mir'),
    '317',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Taniwha'),
    (select id from sets where short_name = 'mir'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aleatory'),
    (select id from sets where short_name = 'mir'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sidar Jabari'),
    (select id from sets where short_name = 'mir'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tainted Specter'),
    (select id from sets where short_name = 'mir'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Divine Retribution'),
    (select id from sets where short_name = 'mir'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Malignant Growth'),
    (select id from sets where short_name = 'mir'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'mir'),
    '336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Elephant'),
    (select id from sets where short_name = 'mir'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harbor Guardian'),
    (select id from sets where short_name = 'mir'),
    '266',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bazaar of Wonders'),
    (select id from sets where short_name = 'mir'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Painful Memories'),
    (select id from sets where short_name = 'mir'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rock Basilisk'),
    (select id from sets where short_name = 'mir'),
    '279',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaervek''s Purge'),
    (select id from sets where short_name = 'mir'),
    '270',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bad River'),
    (select id from sets where short_name = 'mir'),
    '324',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nettletooth Djinn'),
    (select id from sets where short_name = 'mir'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flood Plain'),
    (select id from sets where short_name = 'mir'),
    '326',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Dreadnought'),
    (select id from sets where short_name = 'mir'),
    '315',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reign of Terror'),
    (select id from sets where short_name = 'mir'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyric Salamander'),
    (select id from sets where short_name = 'mir'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Auspicious Ancestor'),
    (select id from sets where short_name = 'mir'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Rend'),
    (select id from sets where short_name = 'mir'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquil Domain'),
    (select id from sets where short_name = 'mir'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Prism'),
    (select id from sets where short_name = 'mir'),
    '308',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Null Chamber'),
    (select id from sets where short_name = 'mir'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hakim, Loreweaver'),
    (select id from sets where short_name = 'mir'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'mir'),
    '334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Melesse Spirit'),
    (select id from sets where short_name = 'mir'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Final Fortune'),
    (select id from sets where short_name = 'mir'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spirit of the Night'),
    (select id from sets where short_name = 'mir'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sand Golem'),
    (select id from sets where short_name = 'mir'),
    '318',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azimaet Drake'),
    (select id from sets where short_name = 'mir'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moss Diamond'),
    (select id from sets where short_name = 'mir'),
    '312',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Energy Vortex'),
    (select id from sets where short_name = 'mir'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'mir'),
    '331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ethereal Champion'),
    (select id from sets where short_name = 'mir'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sky Diamond'),
    (select id from sets where short_name = 'mir'),
    '319',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Patagia Golem'),
    (select id from sets where short_name = 'mir'),
    '313',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Celestial Dawn'),
    (select id from sets where short_name = 'mir'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Haunting Apparition'),
    (select id from sets where short_name = 'mir'),
    '267',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firebreathing'),
    (select id from sets where short_name = 'mir'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prismatic Circle'),
    (select id from sets where short_name = 'mir'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ekundu Cyclops'),
    (select id from sets where short_name = 'mir'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kukemssa Serpent'),
    (select id from sets where short_name = 'mir'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Femeref Healer'),
    (select id from sets where short_name = 'mir'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Geyser'),
    (select id from sets where short_name = 'mir'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Femeref Knight'),
    (select id from sets where short_name = 'mir'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Patrol'),
    (select id from sets where short_name = 'mir'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reign of Chaos'),
    (select id from sets where short_name = 'mir'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ebony Charm'),
    (select id from sets where short_name = 'mir'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Bend'),
    (select id from sets where short_name = 'mir'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Emberwilde Caliph'),
    (select id from sets where short_name = 'mir'),
    '262',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benthic Djinn'),
    (select id from sets where short_name = 'mir'),
    '257',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'mir'),
    '350',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teremko Griffin'),
    (select id from sets where short_name = 'mir'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'mir'),
    '343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blinding Light'),
    (select id from sets where short_name = 'mir'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yare'),
    (select id from sets where short_name = 'mir'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Illicit Auction'),
    (select id from sets where short_name = 'mir'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reckless Embermage'),
    (select id from sets where short_name = 'mir'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carrion'),
    (select id from sets where short_name = 'mir'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Purgatory'),
    (select id from sets where short_name = 'mir'),
    '275',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vigilant Martyr'),
    (select id from sets where short_name = 'mir'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leering Gargoyle'),
    (select id from sets where short_name = 'mir'),
    '271',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'mir'),
    '347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zirilan of the Claw'),
    (select id from sets where short_name = 'mir'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burning Palm Efreet'),
    (select id from sets where short_name = 'mir'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raging Spirit'),
    (select id from sets where short_name = 'mir'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Restless Dead'),
    (select id from sets where short_name = 'mir'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gibbering Hyenas'),
    (select id from sets where short_name = 'mir'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warping Wurm'),
    (select id from sets where short_name = 'mir'),
    '287',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'mir'),
    '338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barreling Attack'),
    (select id from sets where short_name = 'mir'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Scouts'),
    (select id from sets where short_name = 'mir'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zebra Unicorn'),
    (select id from sets where short_name = 'mir'),
    '290',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grim Feast'),
    (select id from sets where short_name = 'mir'),
    '265',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Asmira, Holy Avenger'),
    (select id from sets where short_name = 'mir'),
    '256',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Civic Guildmage'),
    (select id from sets where short_name = 'mir'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = 'mir'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Mob'),
    (select id from sets where short_name = 'mir'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'mir'),
    '333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dazzling Beauty'),
    (select id from sets where short_name = 'mir'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Roots'),
    (select id from sets where short_name = 'mir'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dream Fighter'),
    (select id from sets where short_name = 'mir'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serene Heart'),
    (select id from sets where short_name = 'mir'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mangara''s Blessing'),
    (select id from sets where short_name = 'mir'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'mir'),
    '332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Decomposition'),
    (select id from sets where short_name = 'mir'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Natural Balance'),
    (select id from sets where short_name = 'mir'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancestral Memories'),
    (select id from sets where short_name = 'mir'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foratog'),
    (select id from sets where short_name = 'mir'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dream Cache'),
    (select id from sets where short_name = 'mir'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blighted Shaman'),
    (select id from sets where short_name = 'mir'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Binding Agony'),
    (select id from sets where short_name = 'mir'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harmattan Efreet'),
    (select id from sets where short_name = 'mir'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Isle'),
    (select id from sets where short_name = 'mir'),
    '330',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dwarven Miner'),
    (select id from sets where short_name = 'mir'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Suq''Ata Firewalker'),
    (select id from sets where short_name = 'mir'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shadow Guildmage'),
    (select id from sets where short_name = 'mir'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'mir'),
    '348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Diamond'),
    (select id from sets where short_name = 'mir'),
    '302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maro'),
    (select id from sets where short_name = 'mir'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Locust Swarm'),
    (select id from sets where short_name = 'mir'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soar'),
    (select id from sets where short_name = 'mir'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chaosphere'),
    (select id from sets where short_name = 'mir'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Dragon'),
    (select id from sets where short_name = 'mir'),
    '201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Discordant Spirit'),
    (select id from sets where short_name = 'mir'),
    '261',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quirion Elves'),
    (select id from sets where short_name = 'mir'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enfeeblement'),
    (select id from sets where short_name = 'mir'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Tribute'),
    (select id from sets where short_name = 'mir'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zuberi, Golden Feather'),
    (select id from sets where short_name = 'mir'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Drake'),
    (select id from sets where short_name = 'mir'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sawback Manticore'),
    (select id from sets where short_name = 'mir'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uktabi Faerie'),
    (select id from sets where short_name = 'mir'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Afiya Grove'),
    (select id from sets where short_name = 'mir'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Delirium'),
    (select id from sets where short_name = 'mir'),
    '260',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reality Ripple'),
    (select id from sets where short_name = 'mir'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forbidden Crypt'),
    (select id from sets where short_name = 'mir'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wellspring'),
    (select id from sets where short_name = 'mir'),
    '288',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'mir'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cadaverous Bloom'),
    (select id from sets where short_name = 'mir'),
    '258',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forsaken Wastes'),
    (select id from sets where short_name = 'mir'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emberwilde Djinn'),
    (select id from sets where short_name = 'mir'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jabari''s Influence'),
    (select id from sets where short_name = 'mir'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mtenda Lion'),
    (select id from sets where short_name = 'mir'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zhalfirin Commander'),
    (select id from sets where short_name = 'mir'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Echo'),
    (select id from sets where short_name = 'mir'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enlightened Tutor'),
    (select id from sets where short_name = 'mir'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Regeneration'),
    (select id from sets where short_name = 'mir'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cursed Totem'),
    (select id from sets where short_name = 'mir'),
    '299',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tropical Storm'),
    (select id from sets where short_name = 'mir'),
    '246',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sandstorm'),
    (select id from sets where short_name = 'mir'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sirocco'),
    (select id from sets where short_name = 'mir'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Horrible Hordes'),
    (select id from sets where short_name = 'mir'),
    '304',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Uktabi Wildcats'),
    (select id from sets where short_name = 'mir'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merfolk Raiders'),
    (select id from sets where short_name = 'mir'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kukemssa Pirates'),
    (select id from sets where short_name = 'mir'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prismatic Lace'),
    (select id from sets where short_name = 'mir'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reality Ripple'),
    (select id from sets where short_name = 'mir'),
    '87†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shallow Grave'),
    (select id from sets where short_name = 'mir'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unyaro Griffin'),
    (select id from sets where short_name = 'mir'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crash of Rhinos'),
    (select id from sets where short_name = 'mir'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iron Tusk Elephant'),
    (select id from sets where short_name = 'mir'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'mir'),
    '349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Radiant Essence'),
    (select id from sets where short_name = 'mir'),
    '276',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armor of Thorns'),
    (select id from sets where short_name = 'mir'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lead Golem'),
    (select id from sets where short_name = 'mir'),
    '306',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Corpses'),
    (select id from sets where short_name = 'mir'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain Valley'),
    (select id from sets where short_name = 'mir'),
    '328',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bay Falcon'),
    (select id from sets where short_name = 'mir'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'mir'),
    '335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Choking Sands'),
    (select id from sets where short_name = 'mir'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seedling Charm'),
    (select id from sets where short_name = 'mir'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Favorable Destiny'),
    (select id from sets where short_name = 'mir'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Infernal Contract'),
    (select id from sets where short_name = 'mir'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = 'mir'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Tinkerer'),
    (select id from sets where short_name = 'mir'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spitting Earth'),
    (select id from sets where short_name = 'mir'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vaporous Djinn'),
    (select id from sets where short_name = 'mir'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Femeref Archers'),
    (select id from sets where short_name = 'mir'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drain Life'),
    (select id from sets where short_name = 'mir'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armorer Guildmage'),
    (select id from sets where short_name = 'mir'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illumination'),
    (select id from sets where short_name = 'mir'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zhalfirin Knight'),
    (select id from sets where short_name = 'mir'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savage Twister'),
    (select id from sets where short_name = 'mir'),
    '280',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rashida Scalebane'),
    (select id from sets where short_name = 'mir'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chaos Charm'),
    (select id from sets where short_name = 'mir'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unerring Sling'),
    (select id from sets where short_name = 'mir'),
    '322',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urborg Panther'),
    (select id from sets where short_name = 'mir'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hivis of the Scale'),
    (select id from sets where short_name = 'mir'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Despair'),
    (select id from sets where short_name = 'mir'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Worldly Tutor'),
    (select id from sets where short_name = 'mir'),
    '255',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cinder Cloud'),
    (select id from sets where short_name = 'mir'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stalking Tiger'),
    (select id from sets where short_name = 'mir'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Wurm'),
    (select id from sets where short_name = 'mir'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'mir'),
    '339',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grinning Totem'),
    (select id from sets where short_name = 'mir'),
    '303',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mindbender Spores'),
    (select id from sets where short_name = 'mir'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Igneous Golem'),
    (select id from sets where short_name = 'mir'),
    '305',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Misers'' Cage'),
    (select id from sets where short_name = 'mir'),
    '311',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boomerang'),
    (select id from sets where short_name = 'mir'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hammer of Bogardan'),
    (select id from sets where short_name = 'mir'),
    '181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Daring Apprentice'),
    (select id from sets where short_name = 'mir'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skulking Ghost'),
    (select id from sets where short_name = 'mir'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tombstone Stairwell'),
    (select id from sets where short_name = 'mir'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seeds of Innocence'),
    (select id from sets where short_name = 'mir'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Floodgate'),
    (select id from sets where short_name = 'mir'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Builder''s Bane'),
    (select id from sets where short_name = 'mir'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prismatic Boon'),
    (select id from sets where short_name = 'mir'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Curse'),
    (select id from sets where short_name = 'mir'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Polymorph'),
    (select id from sets where short_name = 'mir'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grave Servitude'),
    (select id from sets where short_name = 'mir'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dirtwater Wraith'),
    (select id from sets where short_name = 'mir'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Resistance'),
    (select id from sets where short_name = 'mir'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Amulet of Unmaking'),
    (select id from sets where short_name = 'mir'),
    '293',
    'rare'
) 
 on conflict do nothing;
