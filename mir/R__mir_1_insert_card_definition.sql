    insert into mtgcard(name) values ('Nocturnal Raid') on conflict do nothing;
    insert into mtgcard(name) values ('Dread Specter') on conflict do nothing;
    insert into mtgcard(name) values ('Meddle') on conflict do nothing;
    insert into mtgcard(name) values ('Amber Prison') on conflict do nothing;
    insert into mtgcard(name) values ('Teferi''s Imp') on conflict do nothing;
    insert into mtgcard(name) values ('Consuming Ferocity') on conflict do nothing;
    insert into mtgcard(name) values ('Shauku, Endbringer') on conflict do nothing;
    insert into mtgcard(name) values ('Cadaverous Knight') on conflict do nothing;
    insert into mtgcard(name) values ('Ivory Charm') on conflict do nothing;
    insert into mtgcard(name) values ('Unseen Walker') on conflict do nothing;
    insert into mtgcard(name) values ('Abyssal Hunter') on conflict do nothing;
    insert into mtgcard(name) values ('Telim''Tor''s Edict') on conflict do nothing;
    insert into mtgcard(name) values ('Coral Fighters') on conflict do nothing;
    insert into mtgcard(name) values ('Phyrexian Vault') on conflict do nothing;
    insert into mtgcard(name) values ('Kaervek''s Hex') on conflict do nothing;
    insert into mtgcard(name) values ('Kaervek''s Torch') on conflict do nothing;
    insert into mtgcard(name) values ('Ether Well') on conflict do nothing;
    insert into mtgcard(name) values ('Reparations') on conflict do nothing;
    insert into mtgcard(name) values ('Breathstealer') on conflict do nothing;
    insert into mtgcard(name) values ('Healing Salve') on conflict do nothing;
    insert into mtgcard(name) values ('Shadowbane') on conflict do nothing;
    insert into mtgcard(name) values ('Stupor') on conflict do nothing;
    insert into mtgcard(name) values ('Blind Fury') on conflict do nothing;
    insert into mtgcard(name) values ('Wave Elemental') on conflict do nothing;
    insert into mtgcard(name) values ('Mire Shade') on conflict do nothing;
    insert into mtgcard(name) values ('Barbed Foliage') on conflict do nothing;
    insert into mtgcard(name) values ('Sea Scryer') on conflict do nothing;
    insert into mtgcard(name) values ('Thirst') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Elite Infantry') on conflict do nothing;
    insert into mtgcard(name) values ('Subterranean Spirit') on conflict do nothing;
    insert into mtgcard(name) values ('Fallow Earth') on conflict do nothing;
    insert into mtgcard(name) values ('Alarum') on conflict do nothing;
    insert into mtgcard(name) values ('Mtenda Griffin') on conflict do nothing;
    insert into mtgcard(name) values ('Karoo Meerkat') on conflict do nothing;
    insert into mtgcard(name) values ('Spatial Binding') on conflict do nothing;
    insert into mtgcard(name) values ('Hall of Gemstone') on conflict do nothing;
    insert into mtgcard(name) values ('Torrent of Lava') on conflict do nothing;
    insert into mtgcard(name) values ('Crystal Vein') on conflict do nothing;
    insert into mtgcard(name) values ('Dark Ritual') on conflict do nothing;
    insert into mtgcard(name) values ('Barbed-Back Wurm') on conflict do nothing;
    insert into mtgcard(name) values ('Flare') on conflict do nothing;
    insert into mtgcard(name) values ('Mountain') on conflict do nothing;
    insert into mtgcard(name) values ('Bone Harvest') on conflict do nothing;
    insert into mtgcard(name) values ('Rocky Tar Pit') on conflict do nothing;
    insert into mtgcard(name) values ('Island') on conflict do nothing;
    insert into mtgcard(name) values ('Bone Mask') on conflict do nothing;
    insert into mtgcard(name) values ('Canopy Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Ritual of Steel') on conflict do nothing;
    insert into mtgcard(name) values ('Village Elder') on conflict do nothing;
    insert into mtgcard(name) values ('Roots of Life') on conflict do nothing;
    insert into mtgcard(name) values ('Lightning Reflexes') on conflict do nothing;
    insert into mtgcard(name) values ('Sapphire Charm') on conflict do nothing;
    insert into mtgcard(name) values ('Spectral Guardian') on conflict do nothing;
    insert into mtgcard(name) values ('Benevolent Unicorn') on conflict do nothing;
    insert into mtgcard(name) values ('Charcoal Diamond') on conflict do nothing;
    insert into mtgcard(name) values ('Giant Mantis') on conflict do nothing;
    insert into mtgcard(name) values ('Swamp') on conflict do nothing;
    insert into mtgcard(name) values ('Chariot of the Sun') on conflict do nothing;
    insert into mtgcard(name) values ('Crimson Roc') on conflict do nothing;
    insert into mtgcard(name) values ('Dwarven Nomad') on conflict do nothing;
    insert into mtgcard(name) values ('Phyrexian Purge') on conflict do nothing;
    insert into mtgcard(name) values ('Jolrael''s Centaur') on conflict do nothing;
    insert into mtgcard(name) values ('Harbinger of Night') on conflict do nothing;
    insert into mtgcard(name) values ('Unfulfilled Desires') on conflict do nothing;
    insert into mtgcard(name) values ('Telim''Tor') on conflict do nothing;
    insert into mtgcard(name) values ('Paupers'' Cage') on conflict do nothing;
    insert into mtgcard(name) values ('Sealed Fate') on conflict do nothing;
    insert into mtgcard(name) values ('Dissipate') on conflict do nothing;
    insert into mtgcard(name) values ('Reflect Damage') on conflict do nothing;
    insert into mtgcard(name) values ('Ekundu Griffin') on conflict do nothing;
    insert into mtgcard(name) values ('Disempower') on conflict do nothing;
    insert into mtgcard(name) values ('Mist Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Jolt') on conflict do nothing;
    insert into mtgcard(name) values ('Political Trickery') on conflict do nothing;
    insert into mtgcard(name) values ('Granger Guildmage') on conflict do nothing;
    insert into mtgcard(name) values ('Cloak of Invisibility') on conflict do nothing;
    insert into mtgcard(name) values ('Incinerate') on conflict do nothing;
    insert into mtgcard(name) values ('Pearl Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Sewer Rats') on conflict do nothing;
    insert into mtgcard(name) values ('Ray of Command') on conflict do nothing;
    insert into mtgcard(name) values ('Superior Numbers') on conflict do nothing;
    insert into mtgcard(name) values ('Sandbar Crocodile') on conflict do nothing;
    insert into mtgcard(name) values ('Afterlife') on conflict do nothing;
    insert into mtgcard(name) values ('Catacomb Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Purraj of Urborg') on conflict do nothing;
    insert into mtgcard(name) values ('Marble Diamond') on conflict do nothing;
    insert into mtgcard(name) values ('Burning Shield Askari') on conflict do nothing;
    insert into mtgcard(name) values ('Sabertooth Cobra') on conflict do nothing;
    insert into mtgcard(name) values ('Viashino Warrior') on conflict do nothing;
    insert into mtgcard(name) values ('Power Sink') on conflict do nothing;
    insert into mtgcard(name) values ('Flame Elemental') on conflict do nothing;
    insert into mtgcard(name) values ('Tidal Wave') on conflict do nothing;
    insert into mtgcard(name) values ('Cycle of Life') on conflict do nothing;
    insert into mtgcard(name) values ('Shaper Guildmage') on conflict do nothing;
    insert into mtgcard(name) values ('Crystal Golem') on conflict do nothing;
    insert into mtgcard(name) values ('Sunweb') on conflict do nothing;
    insert into mtgcard(name) values ('Divine Offering') on conflict do nothing;
    insert into mtgcard(name) values ('Fetid Horror') on conflict do nothing;
    insert into mtgcard(name) values ('Early Harvest') on conflict do nothing;
    insert into mtgcard(name) values ('Crypt Cobra') on conflict do nothing;
    insert into mtgcard(name) values ('Frenetic Efreet') on conflict do nothing;
    insert into mtgcard(name) values ('Talruum Minotaur') on conflict do nothing;
    insert into mtgcard(name) values ('Mtenda Herder') on conflict do nothing;
    insert into mtgcard(name) values ('Ventifact Bottle') on conflict do nothing;
    insert into mtgcard(name) values ('Psychic Transfer') on conflict do nothing;
    insert into mtgcard(name) values ('Mangara''s Tome') on conflict do nothing;
    insert into mtgcard(name) values ('Agility') on conflict do nothing;
    insert into mtgcard(name) values ('Blistering Barrier') on conflict do nothing;
    insert into mtgcard(name) values ('Teeka''s Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Telim''Tor''s Darts') on conflict do nothing;
    insert into mtgcard(name) values ('Preferred Selection') on conflict do nothing;
    insert into mtgcard(name) values ('Jungle Troll') on conflict do nothing;
    insert into mtgcard(name) values ('Ersatz Gnomes') on conflict do nothing;
    insert into mtgcard(name) values ('Mind Harness') on conflict do nothing;
    insert into mtgcard(name) values ('Soulshriek') on conflict do nothing;
    insert into mtgcard(name) values ('Cerulean Wyvern') on conflict do nothing;
    insert into mtgcard(name) values ('Shimmer') on conflict do nothing;
    insert into mtgcard(name) values ('Searing Spear Askari') on conflict do nothing;
    insert into mtgcard(name) values ('Mystical Tutor') on conflict do nothing;
    insert into mtgcard(name) values ('Ravenous Vampire') on conflict do nothing;
    insert into mtgcard(name) values ('Waiting in the Weeds') on conflict do nothing;
    insert into mtgcard(name) values ('Dark Banishing') on conflict do nothing;
    insert into mtgcard(name) values ('Vitalizing Cascade') on conflict do nothing;
    insert into mtgcard(name) values ('Basalt Golem') on conflict do nothing;
    insert into mtgcard(name) values ('Femeref Scouts') on conflict do nothing;
    insert into mtgcard(name) values ('Elixir of Vitality') on conflict do nothing;
    insert into mtgcard(name) values ('Merfolk Seer') on conflict do nothing;
    insert into mtgcard(name) values ('Memory Lapse') on conflict do nothing;
    insert into mtgcard(name) values ('Flash') on conflict do nothing;
    insert into mtgcard(name) values ('Lion''s Eye Diamond') on conflict do nothing;
    insert into mtgcard(name) values ('Windreaper Falcon') on conflict do nothing;
    insert into mtgcard(name) values ('Noble Elephant') on conflict do nothing;
    insert into mtgcard(name) values ('Grasslands') on conflict do nothing;
    insert into mtgcard(name) values ('Sacred Mesa') on conflict do nothing;
    insert into mtgcard(name) values ('Feral Shadow') on conflict do nothing;
    insert into mtgcard(name) values ('Ashen Powder') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Soothsayer') on conflict do nothing;
    insert into mtgcard(name) values ('Hazerider Drake') on conflict do nothing;
    insert into mtgcard(name) values ('Crimson Hellkite') on conflict do nothing;
    insert into mtgcard(name) values ('Brushwagg') on conflict do nothing;
    insert into mtgcard(name) values ('Ward of Lights') on conflict do nothing;
    insert into mtgcard(name) values ('Mangara''s Equity') on conflict do nothing;
    insert into mtgcard(name) values ('Fog') on conflict do nothing;
    insert into mtgcard(name) values ('Wildfire Emissary') on conflict do nothing;
    insert into mtgcard(name) values ('Acidic Dagger') on conflict do nothing;
    insert into mtgcard(name) values ('Gravebane Zombie') on conflict do nothing;
    insert into mtgcard(name) values ('Withering Boon') on conflict do nothing;
    insert into mtgcard(name) values ('Shauku''s Minion') on conflict do nothing;
    insert into mtgcard(name) values ('Energy Bolt') on conflict do nothing;
    insert into mtgcard(name) values ('Lure of Prey') on conflict do nothing;
    insert into mtgcard(name) values ('Unyaro Bee Sting') on conflict do nothing;
    insert into mtgcard(name) values ('Disenchant') on conflict do nothing;
    insert into mtgcard(name) values ('Razor Pendulum') on conflict do nothing;
    insert into mtgcard(name) values ('Taniwha') on conflict do nothing;
    insert into mtgcard(name) values ('Aleatory') on conflict do nothing;
    insert into mtgcard(name) values ('Sidar Jabari') on conflict do nothing;
    insert into mtgcard(name) values ('Tainted Specter') on conflict do nothing;
    insert into mtgcard(name) values ('Divine Retribution') on conflict do nothing;
    insert into mtgcard(name) values ('Malignant Growth') on conflict do nothing;
    insert into mtgcard(name) values ('Wild Elephant') on conflict do nothing;
    insert into mtgcard(name) values ('Harbor Guardian') on conflict do nothing;
    insert into mtgcard(name) values ('Bazaar of Wonders') on conflict do nothing;
    insert into mtgcard(name) values ('Painful Memories') on conflict do nothing;
    insert into mtgcard(name) values ('Rock Basilisk') on conflict do nothing;
    insert into mtgcard(name) values ('Kaervek''s Purge') on conflict do nothing;
    insert into mtgcard(name) values ('Bad River') on conflict do nothing;
    insert into mtgcard(name) values ('Nettletooth Djinn') on conflict do nothing;
    insert into mtgcard(name) values ('Flood Plain') on conflict do nothing;
    insert into mtgcard(name) values ('Phyrexian Dreadnought') on conflict do nothing;
    insert into mtgcard(name) values ('Reign of Terror') on conflict do nothing;
    insert into mtgcard(name) values ('Pyric Salamander') on conflict do nothing;
    insert into mtgcard(name) values ('Auspicious Ancestor') on conflict do nothing;
    insert into mtgcard(name) values ('Soul Rend') on conflict do nothing;
    insert into mtgcard(name) values ('Tranquil Domain') on conflict do nothing;
    insert into mtgcard(name) values ('Mana Prism') on conflict do nothing;
    insert into mtgcard(name) values ('Null Chamber') on conflict do nothing;
    insert into mtgcard(name) values ('Hakim, Loreweaver') on conflict do nothing;
    insert into mtgcard(name) values ('Plains') on conflict do nothing;
    insert into mtgcard(name) values ('Melesse Spirit') on conflict do nothing;
    insert into mtgcard(name) values ('Final Fortune') on conflict do nothing;
    insert into mtgcard(name) values ('Spirit of the Night') on conflict do nothing;
    insert into mtgcard(name) values ('Sand Golem') on conflict do nothing;
    insert into mtgcard(name) values ('Azimaet Drake') on conflict do nothing;
    insert into mtgcard(name) values ('Moss Diamond') on conflict do nothing;
    insert into mtgcard(name) values ('Energy Vortex') on conflict do nothing;
    insert into mtgcard(name) values ('Ethereal Champion') on conflict do nothing;
    insert into mtgcard(name) values ('Sky Diamond') on conflict do nothing;
    insert into mtgcard(name) values ('Patagia Golem') on conflict do nothing;
    insert into mtgcard(name) values ('Celestial Dawn') on conflict do nothing;
    insert into mtgcard(name) values ('Haunting Apparition') on conflict do nothing;
    insert into mtgcard(name) values ('Firebreathing') on conflict do nothing;
    insert into mtgcard(name) values ('Prismatic Circle') on conflict do nothing;
    insert into mtgcard(name) values ('Ekundu Cyclops') on conflict do nothing;
    insert into mtgcard(name) values ('Kukemssa Serpent') on conflict do nothing;
    insert into mtgcard(name) values ('Femeref Healer') on conflict do nothing;
    insert into mtgcard(name) values ('Volcanic Geyser') on conflict do nothing;
    insert into mtgcard(name) values ('Femeref Knight') on conflict do nothing;
    insert into mtgcard(name) values ('Jungle Patrol') on conflict do nothing;
    insert into mtgcard(name) values ('Reign of Chaos') on conflict do nothing;
    insert into mtgcard(name) values ('Ebony Charm') on conflict do nothing;
    insert into mtgcard(name) values ('Mind Bend') on conflict do nothing;
    insert into mtgcard(name) values ('Emberwilde Caliph') on conflict do nothing;
    insert into mtgcard(name) values ('Benthic Djinn') on conflict do nothing;
    insert into mtgcard(name) values ('Forest') on conflict do nothing;
    insert into mtgcard(name) values ('Teremko Griffin') on conflict do nothing;
    insert into mtgcard(name) values ('Blinding Light') on conflict do nothing;
    insert into mtgcard(name) values ('Yare') on conflict do nothing;
    insert into mtgcard(name) values ('Illicit Auction') on conflict do nothing;
    insert into mtgcard(name) values ('Reckless Embermage') on conflict do nothing;
    insert into mtgcard(name) values ('Carrion') on conflict do nothing;
    insert into mtgcard(name) values ('Purgatory') on conflict do nothing;
    insert into mtgcard(name) values ('Vigilant Martyr') on conflict do nothing;
    insert into mtgcard(name) values ('Leering Gargoyle') on conflict do nothing;
    insert into mtgcard(name) values ('Zirilan of the Claw') on conflict do nothing;
    insert into mtgcard(name) values ('Burning Palm Efreet') on conflict do nothing;
    insert into mtgcard(name) values ('Raging Spirit') on conflict do nothing;
    insert into mtgcard(name) values ('Restless Dead') on conflict do nothing;
    insert into mtgcard(name) values ('Gibbering Hyenas') on conflict do nothing;
    insert into mtgcard(name) values ('Warping Wurm') on conflict do nothing;
    insert into mtgcard(name) values ('Barreling Attack') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Scouts') on conflict do nothing;
    insert into mtgcard(name) values ('Zebra Unicorn') on conflict do nothing;
    insert into mtgcard(name) values ('Grim Feast') on conflict do nothing;
    insert into mtgcard(name) values ('Asmira, Holy Avenger') on conflict do nothing;
    insert into mtgcard(name) values ('Civic Guildmage') on conflict do nothing;
    insert into mtgcard(name) values ('Rampant Growth') on conflict do nothing;
    insert into mtgcard(name) values ('Zombie Mob') on conflict do nothing;
    insert into mtgcard(name) values ('Dazzling Beauty') on conflict do nothing;
    insert into mtgcard(name) values ('Wall of Roots') on conflict do nothing;
    insert into mtgcard(name) values ('Dream Fighter') on conflict do nothing;
    insert into mtgcard(name) values ('Serene Heart') on conflict do nothing;
    insert into mtgcard(name) values ('Mangara''s Blessing') on conflict do nothing;
    insert into mtgcard(name) values ('Decomposition') on conflict do nothing;
    insert into mtgcard(name) values ('Natural Balance') on conflict do nothing;
    insert into mtgcard(name) values ('Ancestral Memories') on conflict do nothing;
    insert into mtgcard(name) values ('Foratog') on conflict do nothing;
    insert into mtgcard(name) values ('Dream Cache') on conflict do nothing;
    insert into mtgcard(name) values ('Blighted Shaman') on conflict do nothing;
    insert into mtgcard(name) values ('Binding Agony') on conflict do nothing;
    insert into mtgcard(name) values ('Harmattan Efreet') on conflict do nothing;
    insert into mtgcard(name) values ('Teferi''s Isle') on conflict do nothing;
    insert into mtgcard(name) values ('Dwarven Miner') on conflict do nothing;
    insert into mtgcard(name) values ('Suq''Ata Firewalker') on conflict do nothing;
    insert into mtgcard(name) values ('Shadow Guildmage') on conflict do nothing;
    insert into mtgcard(name) values ('Fire Diamond') on conflict do nothing;
    insert into mtgcard(name) values ('Maro') on conflict do nothing;
    insert into mtgcard(name) values ('Locust Swarm') on conflict do nothing;
    insert into mtgcard(name) values ('Soar') on conflict do nothing;
    insert into mtgcard(name) values ('Chaosphere') on conflict do nothing;
    insert into mtgcard(name) values ('Volcanic Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Discordant Spirit') on conflict do nothing;
    insert into mtgcard(name) values ('Quirion Elves') on conflict do nothing;
    insert into mtgcard(name) values ('Enfeeblement') on conflict do nothing;
    insert into mtgcard(name) values ('Phyrexian Tribute') on conflict do nothing;
    insert into mtgcard(name) values ('Zuberi, Golden Feather') on conflict do nothing;
    insert into mtgcard(name) values ('Teferi''s Drake') on conflict do nothing;
    insert into mtgcard(name) values ('Sawback Manticore') on conflict do nothing;
    insert into mtgcard(name) values ('Uktabi Faerie') on conflict do nothing;
    insert into mtgcard(name) values ('Afiya Grove') on conflict do nothing;
    insert into mtgcard(name) values ('Delirium') on conflict do nothing;
    insert into mtgcard(name) values ('Reality Ripple') on conflict do nothing;
    insert into mtgcard(name) values ('Forbidden Crypt') on conflict do nothing;
    insert into mtgcard(name) values ('Wellspring') on conflict do nothing;
    insert into mtgcard(name) values ('Pacifism') on conflict do nothing;
    insert into mtgcard(name) values ('Cadaverous Bloom') on conflict do nothing;
    insert into mtgcard(name) values ('Forsaken Wastes') on conflict do nothing;
    insert into mtgcard(name) values ('Emberwilde Djinn') on conflict do nothing;
    insert into mtgcard(name) values ('Jabari''s Influence') on conflict do nothing;
    insert into mtgcard(name) values ('Mtenda Lion') on conflict do nothing;
    insert into mtgcard(name) values ('Zhalfirin Commander') on conflict do nothing;
    insert into mtgcard(name) values ('Soul Echo') on conflict do nothing;
    insert into mtgcard(name) values ('Enlightened Tutor') on conflict do nothing;
    insert into mtgcard(name) values ('Regeneration') on conflict do nothing;
    insert into mtgcard(name) values ('Cursed Totem') on conflict do nothing;
    insert into mtgcard(name) values ('Tropical Storm') on conflict do nothing;
    insert into mtgcard(name) values ('Sandstorm') on conflict do nothing;
    insert into mtgcard(name) values ('Sirocco') on conflict do nothing;
    insert into mtgcard(name) values ('Horrible Hordes') on conflict do nothing;
    insert into mtgcard(name) values ('Uktabi Wildcats') on conflict do nothing;
    insert into mtgcard(name) values ('Merfolk Raiders') on conflict do nothing;
    insert into mtgcard(name) values ('Kukemssa Pirates') on conflict do nothing;
    insert into mtgcard(name) values ('Prismatic Lace') on conflict do nothing;
    insert into mtgcard(name) values ('Shallow Grave') on conflict do nothing;
    insert into mtgcard(name) values ('Unyaro Griffin') on conflict do nothing;
    insert into mtgcard(name) values ('Crash of Rhinos') on conflict do nothing;
    insert into mtgcard(name) values ('Iron Tusk Elephant') on conflict do nothing;
    insert into mtgcard(name) values ('Radiant Essence') on conflict do nothing;
    insert into mtgcard(name) values ('Armor of Thorns') on conflict do nothing;
    insert into mtgcard(name) values ('Lead Golem') on conflict do nothing;
    insert into mtgcard(name) values ('Wall of Corpses') on conflict do nothing;
    insert into mtgcard(name) values ('Mountain Valley') on conflict do nothing;
    insert into mtgcard(name) values ('Bay Falcon') on conflict do nothing;
    insert into mtgcard(name) values ('Choking Sands') on conflict do nothing;
    insert into mtgcard(name) values ('Seedling Charm') on conflict do nothing;
    insert into mtgcard(name) values ('Favorable Destiny') on conflict do nothing;
    insert into mtgcard(name) values ('Infernal Contract') on conflict do nothing;
    insert into mtgcard(name) values ('Stone Rain') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Tinkerer') on conflict do nothing;
    insert into mtgcard(name) values ('Spitting Earth') on conflict do nothing;
    insert into mtgcard(name) values ('Vaporous Djinn') on conflict do nothing;
    insert into mtgcard(name) values ('Femeref Archers') on conflict do nothing;
    insert into mtgcard(name) values ('Drain Life') on conflict do nothing;
    insert into mtgcard(name) values ('Armorer Guildmage') on conflict do nothing;
    insert into mtgcard(name) values ('Illumination') on conflict do nothing;
    insert into mtgcard(name) values ('Zhalfirin Knight') on conflict do nothing;
    insert into mtgcard(name) values ('Savage Twister') on conflict do nothing;
    insert into mtgcard(name) values ('Rashida Scalebane') on conflict do nothing;
    insert into mtgcard(name) values ('Chaos Charm') on conflict do nothing;
    insert into mtgcard(name) values ('Unerring Sling') on conflict do nothing;
    insert into mtgcard(name) values ('Urborg Panther') on conflict do nothing;
    insert into mtgcard(name) values ('Hivis of the Scale') on conflict do nothing;
    insert into mtgcard(name) values ('Circle of Despair') on conflict do nothing;
    insert into mtgcard(name) values ('Worldly Tutor') on conflict do nothing;
    insert into mtgcard(name) values ('Cinder Cloud') on conflict do nothing;
    insert into mtgcard(name) values ('Stalking Tiger') on conflict do nothing;
    insert into mtgcard(name) values ('Jungle Wurm') on conflict do nothing;
    insert into mtgcard(name) values ('Grinning Totem') on conflict do nothing;
    insert into mtgcard(name) values ('Mindbender Spores') on conflict do nothing;
    insert into mtgcard(name) values ('Igneous Golem') on conflict do nothing;
    insert into mtgcard(name) values ('Misers'' Cage') on conflict do nothing;
    insert into mtgcard(name) values ('Boomerang') on conflict do nothing;
    insert into mtgcard(name) values ('Hammer of Bogardan') on conflict do nothing;
    insert into mtgcard(name) values ('Daring Apprentice') on conflict do nothing;
    insert into mtgcard(name) values ('Skulking Ghost') on conflict do nothing;
    insert into mtgcard(name) values ('Tombstone Stairwell') on conflict do nothing;
    insert into mtgcard(name) values ('Seeds of Innocence') on conflict do nothing;
    insert into mtgcard(name) values ('Floodgate') on conflict do nothing;
    insert into mtgcard(name) values ('Builder''s Bane') on conflict do nothing;
    insert into mtgcard(name) values ('Prismatic Boon') on conflict do nothing;
    insert into mtgcard(name) values ('Teferi''s Curse') on conflict do nothing;
    insert into mtgcard(name) values ('Polymorph') on conflict do nothing;
    insert into mtgcard(name) values ('Grave Servitude') on conflict do nothing;
    insert into mtgcard(name) values ('Dirtwater Wraith') on conflict do nothing;
    insert into mtgcard(name) values ('Wall of Resistance') on conflict do nothing;
    insert into mtgcard(name) values ('Amulet of Unmaking') on conflict do nothing;
