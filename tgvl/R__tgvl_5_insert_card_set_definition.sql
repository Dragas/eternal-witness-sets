insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tgvl'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elephant'),
    (select id from sets where short_name = 'tgvl'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bat'),
    (select id from sets where short_name = 'tgvl'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tgvl'),
    '9',
    'common'
) 
 on conflict do nothing;
