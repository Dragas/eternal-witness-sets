insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Voltaic Key'),
    (select id from sets where short_name = 'pmei'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kuldotha Phoenix'),
    (select id from sets where short_name = 'pmei'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thorn Elemental'),
    (select id from sets where short_name = 'pmei'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heliod''s Pilgrim'),
    (select id from sets where short_name = 'pmei'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blue Elemental Blast'),
    (select id from sets where short_name = 'pmei'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sprite Dragon'),
    (select id from sets where short_name = 'pmei'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warmonger'),
    (select id from sets where short_name = 'pmei'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Staggering Insight'),
    (select id from sets where short_name = 'pmei'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sandbar Crocodile'),
    (select id from sets where short_name = 'pmei'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'pmei'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ascendant Evincar'),
    (select id from sets where short_name = 'pmei'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Parallax Dementia'),
    (select id from sets where short_name = 'pmei'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Dragon'),
    (select id from sets where short_name = 'pmei'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silver Drake'),
    (select id from sets where short_name = 'pmei'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Sprite // Mesmeric Glare'),
    (select id from sets where short_name = 'pmei'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'pmei'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Daxos, Blessed by the Sun'),
    (select id from sets where short_name = 'pmei'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darksteel Juggernaut'),
    (select id from sets where short_name = 'pmei'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cast Down'),
    (select id from sets where short_name = 'pmei'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cunning Sparkmage'),
    (select id from sets where short_name = 'pmei'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diabolic Edict'),
    (select id from sets where short_name = 'pmei'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jamuraan Lion'),
    (select id from sets where short_name = 'pmei'),
    '10*',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Spitfire'),
    (select id from sets where short_name = 'pmei'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Outrage'),
    (select id from sets where short_name = 'pmei'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'pmei'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Hounds'),
    (select id from sets where short_name = 'pmei'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zhalfirin Knight'),
    (select id from sets where short_name = 'pmei'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Rager'),
    (select id from sets where short_name = 'pmei'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scent of Cinder'),
    (select id from sets where short_name = 'pmei'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shrieking Drake'),
    (select id from sets where short_name = 'pmei'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spined Wurm'),
    (select id from sets where short_name = 'pmei'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lava Coil'),
    (select id from sets where short_name = 'pmei'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stream of Life'),
    (select id from sets where short_name = 'pmei'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archangel'),
    (select id from sets where short_name = 'pmei'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = 'pmei'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'pmei'),
    '34',
    'rare'
) 
 on conflict do nothing;
