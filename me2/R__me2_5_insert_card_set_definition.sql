insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Wings of Aesthir'),
    (select id from sets where short_name = 'me2'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mudslide'),
    (select id from sets where short_name = 'me2'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Elite Guard'),
    (select id from sets where short_name = 'me2'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Errantry'),
    (select id from sets where short_name = 'me2'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infernal Darkness'),
    (select id from sets where short_name = 'me2'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armored Griffin'),
    (select id from sets where short_name = 'me2'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marjhan'),
    (select id from sets where short_name = 'me2'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skeleton Ship'),
    (select id from sets where short_name = 'me2'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Underground Sea'),
    (select id from sets where short_name = 'me2'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ivory Gargoyle'),
    (select id from sets where short_name = 'me2'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conquer'),
    (select id from sets where short_name = 'me2'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Icatian Scout'),
    (select id from sets where short_name = 'me2'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Ruins'),
    (select id from sets where short_name = 'me2'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glacial Chasm'),
    (select id from sets where short_name = 'me2'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spore Flower'),
    (select id from sets where short_name = 'me2'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Despotic Scepter'),
    (select id from sets where short_name = 'me2'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Kiss'),
    (select id from sets where short_name = 'me2'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Songs of the Damned'),
    (select id from sets where short_name = 'me2'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lim-Dûl''s High Guard'),
    (select id from sets where short_name = 'me2'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aether Storm'),
    (select id from sets where short_name = 'me2'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aysen Bureaucrats'),
    (select id from sets where short_name = 'me2'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thallid Devourer'),
    (select id from sets where short_name = 'me2'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demonic Consultation'),
    (select id from sets where short_name = 'me2'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Taiga'),
    (select id from sets where short_name = 'me2'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tinder Wall'),
    (select id from sets where short_name = 'me2'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jeweled Amulet'),
    (select id from sets where short_name = 'me2'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'me2'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Svyelunite Temple'),
    (select id from sets where short_name = 'me2'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elemental Augury'),
    (select id from sets where short_name = 'me2'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Portent'),
    (select id from sets where short_name = 'me2'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woolly Spider'),
    (select id from sets where short_name = 'me2'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Farmer'),
    (select id from sets where short_name = 'me2'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Storm Spirit'),
    (select id from sets where short_name = 'me2'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Righteous Fury'),
    (select id from sets where short_name = 'me2'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carapace'),
    (select id from sets where short_name = 'me2'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Binding Grasp'),
    (select id from sets where short_name = 'me2'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Crypt'),
    (select id from sets where short_name = 'me2'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm Elemental'),
    (select id from sets where short_name = 'me2'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zuran Spellcaster'),
    (select id from sets where short_name = 'me2'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rogue Skycaptain'),
    (select id from sets where short_name = 'me2'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drift of the Dead'),
    (select id from sets where short_name = 'me2'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elven Lyre'),
    (select id from sets where short_name = 'me2'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of Stromgald'),
    (select id from sets where short_name = 'me2'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necropotence'),
    (select id from sets where short_name = 'me2'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wolf Pack'),
    (select id from sets where short_name = 'me2'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Order of the White Shield'),
    (select id from sets where short_name = 'me2'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Island'),
    (select id from sets where short_name = 'me2'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forbidden Lore'),
    (select id from sets where short_name = 'me2'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reinforcements'),
    (select id from sets where short_name = 'me2'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Order of the Sacred Torch'),
    (select id from sets where short_name = 'me2'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brimstone Dragon'),
    (select id from sets where short_name = 'me2'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Joven''s Ferrets'),
    (select id from sets where short_name = 'me2'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Funeral March'),
    (select id from sets where short_name = 'me2'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Adarkar Sentinel'),
    (select id from sets where short_name = 'me2'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Essence Flare'),
    (select id from sets where short_name = 'me2'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Outpost'),
    (select id from sets where short_name = 'me2'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ritual of Subdual'),
    (select id from sets where short_name = 'me2'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reprisal'),
    (select id from sets where short_name = 'me2'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bounty of the Hunt'),
    (select id from sets where short_name = 'me2'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Royal Decree'),
    (select id from sets where short_name = 'me2'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stunted Growth'),
    (select id from sets where short_name = 'me2'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winter''s Night'),
    (select id from sets where short_name = 'me2'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Mountain'),
    (select id from sets where short_name = 'me2'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dance of the Dead'),
    (select id from sets where short_name = 'me2'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dystopia'),
    (select id from sets where short_name = 'me2'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stampede'),
    (select id from sets where short_name = 'me2'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Juniper Order Advocate'),
    (select id from sets where short_name = 'me2'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lava Burst'),
    (select id from sets where short_name = 'me2'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soldevi Excavations'),
    (select id from sets where short_name = 'me2'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soldevi Digger'),
    (select id from sets where short_name = 'me2'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armor of Faith'),
    (select id from sets where short_name = 'me2'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brine Shaman'),
    (select id from sets where short_name = 'me2'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forgotten Lore'),
    (select id from sets where short_name = 'me2'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skull Catapult'),
    (select id from sets where short_name = 'me2'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyrokinesis'),
    (select id from sets where short_name = 'me2'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Ancients'),
    (select id from sets where short_name = 'me2'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sea Drake'),
    (select id from sets where short_name = 'me2'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruins of Trokair'),
    (select id from sets where short_name = 'me2'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aeolipile'),
    (select id from sets where short_name = 'me2'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Farrel''s Zealot'),
    (select id from sets where short_name = 'me2'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dry Spell'),
    (select id from sets where short_name = 'me2'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thought Lash'),
    (select id from sets where short_name = 'me2'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Icequake'),
    (select id from sets where short_name = 'me2'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Retribution'),
    (select id from sets where short_name = 'me2'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Personal Tutor'),
    (select id from sets where short_name = 'me2'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barbed Sextant'),
    (select id from sets where short_name = 'me2'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Veteran'),
    (select id from sets where short_name = 'me2'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Dead'),
    (select id from sets where short_name = 'me2'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Conscripts'),
    (select id from sets where short_name = 'me2'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Withering Wisps'),
    (select id from sets where short_name = 'me2'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diabolic Vision'),
    (select id from sets where short_name = 'me2'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whirling Catapult'),
    (select id from sets where short_name = 'me2'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ritual of the Machine'),
    (select id from sets where short_name = 'me2'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gangrenous Zombies'),
    (select id from sets where short_name = 'me2'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warning'),
    (select id from sets where short_name = 'me2'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Banishing'),
    (select id from sets where short_name = 'me2'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Ranger'),
    (select id from sets where short_name = 'me2'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icy Prison'),
    (select id from sets where short_name = 'me2'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Imperial Seal'),
    (select id from sets where short_name = 'me2'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foul Familiar'),
    (select id from sets where short_name = 'me2'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ebon Praetor'),
    (select id from sets where short_name = 'me2'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thelonite Druid'),
    (select id from sets where short_name = 'me2'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Narwhal'),
    (select id from sets where short_name = 'me2'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Night Soil'),
    (select id from sets where short_name = 'me2'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashen Ghoul'),
    (select id from sets where short_name = 'me2'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Exchange'),
    (select id from sets where short_name = 'me2'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fumarole'),
    (select id from sets where short_name = 'me2'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Musician'),
    (select id from sets where short_name = 'me2'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karplusan Giant'),
    (select id from sets where short_name = 'me2'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Ski Patrol'),
    (select id from sets where short_name = 'me2'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jester''s Mask'),
    (select id from sets where short_name = 'me2'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woolly Mammoths'),
    (select id from sets where short_name = 'me2'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Browse'),
    (select id from sets where short_name = 'me2'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'me2'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'An-Zerrin Ruins'),
    (select id from sets where short_name = 'me2'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balduvian Hydra'),
    (select id from sets where short_name = 'me2'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clockwork Steed'),
    (select id from sets where short_name = 'me2'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sea Spirit'),
    (select id from sets where short_name = 'me2'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Badlands'),
    (select id from sets where short_name = 'me2'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fire Dragon'),
    (select id from sets where short_name = 'me2'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feral Thallid'),
    (select id from sets where short_name = 'me2'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magus of the Unseen'),
    (select id from sets where short_name = 'me2'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krovikan Fetish'),
    (select id from sets where short_name = 'me2'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thermokarst'),
    (select id from sets where short_name = 'me2'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Spark'),
    (select id from sets where short_name = 'me2'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spore Cloud'),
    (select id from sets where short_name = 'me2'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pillage'),
    (select id from sets where short_name = 'me2'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fyndhorn Pollen'),
    (select id from sets where short_name = 'me2'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Earthlink'),
    (select id from sets where short_name = 'me2'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gustha''s Scepter'),
    (select id from sets where short_name = 'me2'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Forest'),
    (select id from sets where short_name = 'me2'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balduvian Trading Post'),
    (select id from sets where short_name = 'me2'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ray of Command'),
    (select id from sets where short_name = 'me2'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anarchy'),
    (select id from sets where short_name = 'me2'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Spirit Guide'),
    (select id from sets where short_name = 'me2'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leaping Lizard'),
    (select id from sets where short_name = 'me2'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krovikan Horror'),
    (select id from sets where short_name = 'me2'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gorilla Shaman'),
    (select id from sets where short_name = 'me2'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necrite'),
    (select id from sets where short_name = 'me2'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Roterothopter'),
    (select id from sets where short_name = 'me2'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Errand of Duty'),
    (select id from sets where short_name = 'me2'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stonehands'),
    (select id from sets where short_name = 'me2'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Kelp'),
    (select id from sets where short_name = 'me2'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burnout'),
    (select id from sets where short_name = 'me2'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abbey Gargoyles'),
    (select id from sets where short_name = 'me2'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Screeching Drake'),
    (select id from sets where short_name = 'me2'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kaysa'),
    (select id from sets where short_name = 'me2'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'me2'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Misinformation'),
    (select id from sets where short_name = 'me2'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Time Bomb'),
    (select id from sets where short_name = 'me2'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mesmeric Trance'),
    (select id from sets where short_name = 'me2'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nature''s Wrath'),
    (select id from sets where short_name = 'me2'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lodestone Bauble'),
    (select id from sets where short_name = 'me2'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ambush Party'),
    (select id from sets where short_name = 'me2'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Phalanx'),
    (select id from sets where short_name = 'me2'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Captain'),
    (select id from sets where short_name = 'me2'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Combat Medic'),
    (select id from sets where short_name = 'me2'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Fiend'),
    (select id from sets where short_name = 'me2'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viscerid Drone'),
    (select id from sets where short_name = 'me2'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ice Floe'),
    (select id from sets where short_name = 'me2'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soldevi Simulacrum'),
    (select id from sets where short_name = 'me2'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel of Light'),
    (select id from sets where short_name = 'me2'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enervate'),
    (select id from sets where short_name = 'me2'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Farmer'),
    (select id from sets where short_name = 'me2'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Broken Visage'),
    (select id from sets where short_name = 'me2'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sustaining Spirit'),
    (select id from sets where short_name = 'me2'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lost Order of Jarkeld'),
    (select id from sets where short_name = 'me2'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Squatters'),
    (select id from sets where short_name = 'me2'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Havenwood Battleground'),
    (select id from sets where short_name = 'me2'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Royal Trooper'),
    (select id from sets where short_name = 'me2'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minion of Leshrac'),
    (select id from sets where short_name = 'me2'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nature''s Blessing'),
    (select id from sets where short_name = 'me2'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Imperial Recruiter'),
    (select id from sets where short_name = 'me2'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temporal Manipulation'),
    (select id from sets where short_name = 'me2'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Caribou Range'),
    (select id from sets where short_name = 'me2'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Energy Storm'),
    (select id from sets where short_name = 'me2'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stromgald Cabal'),
    (select id from sets where short_name = 'me2'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viscerid Armor'),
    (select id from sets where short_name = 'me2'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savannah'),
    (select id from sets where short_name = 'me2'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'me2'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ironclaw Orcs'),
    (select id from sets where short_name = 'me2'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Lumberjack'),
    (select id from sets where short_name = 'me2'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreams of the Dead'),
    (select id from sets where short_name = 'me2'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ebon Stronghold'),
    (select id from sets where short_name = 'me2'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sibilant Spirit'),
    (select id from sets where short_name = 'me2'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Home Guard'),
    (select id from sets where short_name = 'me2'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grandmother Sengir'),
    (select id from sets where short_name = 'me2'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elkin Bottle'),
    (select id from sets where short_name = 'me2'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Cylix'),
    (select id from sets where short_name = 'me2'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Trap Door Spider'),
    (select id from sets where short_name = 'me2'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aysen Crusader'),
    (select id from sets where short_name = 'me2'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flame Spirit'),
    (select id from sets where short_name = 'me2'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thallid'),
    (select id from sets where short_name = 'me2'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Hunter'),
    (select id from sets where short_name = 'me2'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balduvian Conjurer'),
    (select id from sets where short_name = 'me2'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravages of War'),
    (select id from sets where short_name = 'me2'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone Spirit'),
    (select id from sets where short_name = 'me2'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heart of Yavimaya'),
    (select id from sets where short_name = 'me2'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Portal'),
    (select id from sets where short_name = 'me2'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Panic'),
    (select id from sets where short_name = 'me2'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Javelineers'),
    (select id from sets where short_name = 'me2'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shrink'),
    (select id from sets where short_name = 'me2'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balduvian Dead'),
    (select id from sets where short_name = 'me2'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Devourer'),
    (select id from sets where short_name = 'me2'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brassclaw Orcs'),
    (select id from sets where short_name = 'me2'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Farrel''s Mantle'),
    (select id from sets where short_name = 'me2'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lat-Nam''s Legacy'),
    (select id from sets where short_name = 'me2'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunder Wall'),
    (select id from sets where short_name = 'me2'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Red Cliffs Armada'),
    (select id from sets where short_name = 'me2'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meteor Shower'),
    (select id from sets where short_name = 'me2'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scars of the Veteran'),
    (select id from sets where short_name = 'me2'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Cannoneers'),
    (select id from sets where short_name = 'me2'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'me2'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shyft'),
    (select id from sets where short_name = 'me2'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wind Spirit'),
    (select id from sets where short_name = 'me2'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Skycaptain'),
    (select id from sets where short_name = 'me2'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iceberg'),
    (select id from sets where short_name = 'me2'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Essence Filter'),
    (select id from sets where short_name = 'me2'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aurochs'),
    (select id from sets where short_name = 'me2'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Swamp'),
    (select id from sets where short_name = 'me2'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snow Fortress'),
    (select id from sets where short_name = 'me2'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Plains'),
    (select id from sets where short_name = 'me2'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Varchild''s Crusader'),
    (select id from sets where short_name = 'me2'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deep Spawn'),
    (select id from sets where short_name = 'me2'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fungal Bloom'),
    (select id from sets where short_name = 'me2'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Groundbreaker'),
    (select id from sets where short_name = 'me2'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Helm of Obedience'),
    (select id from sets where short_name = 'me2'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wiitigo'),
    (select id from sets where short_name = 'me2'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shield Bearer'),
    (select id from sets where short_name = 'me2'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Folk of the Pines'),
    (select id from sets where short_name = 'me2'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whiteout'),
    (select id from sets where short_name = 'me2'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orc General'),
    (select id from sets where short_name = 'me2'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tundra'),
    (select id from sets where short_name = 'me2'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel of Fury'),
    (select id from sets where short_name = 'me2'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Mount'),
    (select id from sets where short_name = 'me2'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloak of Confusion'),
    (select id from sets where short_name = 'me2'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inheritance'),
    (select id from sets where short_name = 'me2'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glacial Crevasses'),
    (select id from sets where short_name = 'me2'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sacred Boon'),
    (select id from sets where short_name = 'me2'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = 'me2'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krovikan Vampire'),
    (select id from sets where short_name = 'me2'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ihsan''s Shade'),
    (select id from sets where short_name = 'me2'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armor Thrull'),
    (select id from sets where short_name = 'me2'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Johtull Wurm'),
    (select id from sets where short_name = 'me2'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krovikan Sorcerer'),
    (select id from sets where short_name = 'me2'),
    '51',
    'common'
) 
 on conflict do nothing;
