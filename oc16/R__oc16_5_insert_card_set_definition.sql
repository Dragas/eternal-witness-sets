insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Saskia the Unyielding'),
    (select id from sets where short_name = 'oc16'),
    '41',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Atraxa, Praetors'' Voice'),
    (select id from sets where short_name = 'oc16'),
    '28',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kynaios and Tiro of Meletis'),
    (select id from sets where short_name = 'oc16'),
    '36',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Yidris, Maelstrom Wielder'),
    (select id from sets where short_name = 'oc16'),
    '50',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Breya, Etherium Shaper'),
    (select id from sets where short_name = 'oc16'),
    '29',
    'mythic'
) 
 on conflict do nothing;
