    insert into mtgcard(name) values ('Saskia the Unyielding') on conflict do nothing;
    insert into mtgcard(name) values ('Atraxa, Praetors'' Voice') on conflict do nothing;
    insert into mtgcard(name) values ('Kynaios and Tiro of Meletis') on conflict do nothing;
    insert into mtgcard(name) values ('Yidris, Maelstrom Wielder') on conflict do nothing;
    insert into mtgcard(name) values ('Breya, Etherium Shaper') on conflict do nothing;
