insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Snapdax, Apex of the Hunt'),
    (select id from sets where short_name = 'piko'),
    '209p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Drannith Magistrate'),
    (select id from sets where short_name = 'piko'),
    '11s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rielle, the Everwise'),
    (select id from sets where short_name = 'piko'),
    '203s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vadrok, Apex of Thunder'),
    (select id from sets where short_name = 'piko'),
    '214s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Whirlwind of Thought'),
    (select id from sets where short_name = 'piko'),
    '215p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raugrin Triome'),
    (select id from sets where short_name = 'piko'),
    '251p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crystalline Giant'),
    (select id from sets where short_name = 'piko'),
    '234s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ketria Triome'),
    (select id from sets where short_name = 'piko'),
    '250s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jegantha, the Wellspring'),
    (select id from sets where short_name = 'piko'),
    '222p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inspired Ultimatum'),
    (select id from sets where short_name = 'piko'),
    '191s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yorion, Sky Nomad'),
    (select id from sets where short_name = 'piko'),
    '232s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unpredictable Cyclone'),
    (select id from sets where short_name = 'piko'),
    '139p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Labyrinth Raptor'),
    (select id from sets where short_name = 'piko'),
    '193s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brokkos, Apex of Forever'),
    (select id from sets where short_name = 'piko'),
    '179s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Colossification'),
    (select id from sets where short_name = 'piko'),
    '148s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vadrok, Apex of Thunder'),
    (select id from sets where short_name = 'piko'),
    '214p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kogla, the Titan Ape'),
    (select id from sets where short_name = 'piko'),
    '162p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hunted Nightmare'),
    (select id from sets where short_name = 'piko'),
    '92s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eerie Ultimatum'),
    (select id from sets where short_name = 'piko'),
    '184s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mythos of Vadrok'),
    (select id from sets where short_name = 'piko'),
    '127s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Indatha Triome'),
    (select id from sets where short_name = 'piko'),
    '248s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yorion, Sky Nomad'),
    (select id from sets where short_name = 'piko'),
    '232p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Extinction Event'),
    (select id from sets where short_name = 'piko'),
    '88p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lukka, Coppercoat Outcast'),
    (select id from sets where short_name = 'piko'),
    '125s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Death''s Oasis'),
    (select id from sets where short_name = 'piko'),
    '182p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frondland Felidar'),
    (select id from sets where short_name = 'piko'),
    '186s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dirge Bat'),
    (select id from sets where short_name = 'piko'),
    '84p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slitherwisp'),
    (select id from sets where short_name = 'piko'),
    '208s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chevill, Bane of Monsters'),
    (select id from sets where short_name = 'piko'),
    '181p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cubwarden'),
    (select id from sets where short_name = 'piko'),
    '7p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Colossification'),
    (select id from sets where short_name = 'piko'),
    '148p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winota, Joiner of Forces'),
    (select id from sets where short_name = 'piko'),
    '216s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Umori, the Collector'),
    (select id from sets where short_name = 'piko'),
    '231p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lutri, the Spellchaser'),
    (select id from sets where short_name = 'piko'),
    '227p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lutri, the Spellchaser'),
    (select id from sets where short_name = 'piko'),
    '227s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yidaro, Wandering Monster'),
    (select id from sets where short_name = 'piko'),
    '141s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mythos of Snapdax'),
    (select id from sets where short_name = 'piko'),
    '24p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lavabrink Venturer'),
    (select id from sets where short_name = 'piko'),
    '19p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mythos of Illuna'),
    (select id from sets where short_name = 'piko'),
    '58s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiend Artisan'),
    (select id from sets where short_name = 'piko'),
    '220s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mythos of Illuna'),
    (select id from sets where short_name = 'piko'),
    '58p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brokkos, Apex of Forever'),
    (select id from sets where short_name = 'piko'),
    '179p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lurrus of the Dream-Den'),
    (select id from sets where short_name = 'piko'),
    '226s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lukka, Coppercoat Outcast'),
    (select id from sets where short_name = 'piko'),
    '125p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Savai Triome'),
    (select id from sets where short_name = 'piko'),
    '253s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inspired Ultimatum'),
    (select id from sets where short_name = 'piko'),
    '191p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiend Artisan'),
    (select id from sets where short_name = 'piko'),
    '220p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shark Typhoon'),
    (select id from sets where short_name = 'piko'),
    '67s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death''s Oasis'),
    (select id from sets where short_name = 'piko'),
    '182s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Indatha Triome'),
    (select id from sets where short_name = 'piko'),
    '248p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zagoth Triome'),
    (select id from sets where short_name = 'piko'),
    '259s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'General Kudro of Drannith'),
    (select id from sets where short_name = 'piko'),
    '187p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mythos of Nethroi'),
    (select id from sets where short_name = 'piko'),
    '97p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keruga, the Macrosage'),
    (select id from sets where short_name = 'piko'),
    '225s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Illuna, Apex of Wishes'),
    (select id from sets where short_name = 'piko'),
    '190s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dirge Bat'),
    (select id from sets where short_name = 'piko'),
    '84s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gyruda, Doom of Depths'),
    (select id from sets where short_name = 'piko'),
    '221p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivien, Monsters'' Advocate'),
    (select id from sets where short_name = 'piko'),
    '175s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gemrazer'),
    (select id from sets where short_name = 'piko'),
    '155p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drannith Magistrate'),
    (select id from sets where short_name = 'piko'),
    '11p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lavabrink Venturer'),
    (select id from sets where short_name = 'piko'),
    '19s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Titans'' Nest'),
    (select id from sets where short_name = 'piko'),
    '212p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea-Dasher Octopus'),
    (select id from sets where short_name = 'piko'),
    '66p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mythos of Brokkos'),
    (select id from sets where short_name = 'piko'),
    '168s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Illuna, Apex of Wishes'),
    (select id from sets where short_name = 'piko'),
    '190p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Offspring''s Revenge'),
    (select id from sets where short_name = 'piko'),
    '198p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruinous Ultimatum'),
    (select id from sets where short_name = 'piko'),
    '204p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crystalline Giant'),
    (select id from sets where short_name = 'piko'),
    '234p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unpredictable Cyclone'),
    (select id from sets where short_name = 'piko'),
    '139s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kogla, the Titan Ape'),
    (select id from sets where short_name = 'piko'),
    '162s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yidaro, Wandering Monster'),
    (select id from sets where short_name = 'piko'),
    '141p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mythos of Snapdax'),
    (select id from sets where short_name = 'piko'),
    '24s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zirda, the Dawnwaker'),
    (select id from sets where short_name = 'piko'),
    '233p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eerie Ultimatum'),
    (select id from sets where short_name = 'piko'),
    '184p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Ozolith'),
    (select id from sets where short_name = 'piko'),
    '237p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voracious Greatshark'),
    (select id from sets where short_name = 'piko'),
    '70s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kinnan, Bonder Prodigy'),
    (select id from sets where short_name = 'piko'),
    '192s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Labyrinth Raptor'),
    (select id from sets where short_name = 'piko'),
    '193p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kinnan, Bonder Prodigy'),
    (select id from sets where short_name = 'piko'),
    '192p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ruinous Ultimatum'),
    (select id from sets where short_name = 'piko'),
    '204s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Luminous Broodmoth'),
    (select id from sets where short_name = 'piko'),
    '21s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shark Typhoon'),
    (select id from sets where short_name = 'piko'),
    '67p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rielle, the Everwise'),
    (select id from sets where short_name = 'piko'),
    '203p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Skycat Sovereign'),
    (select id from sets where short_name = 'piko'),
    '207p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Umori, the Collector'),
    (select id from sets where short_name = 'piko'),
    '231s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mythos of Brokkos'),
    (select id from sets where short_name = 'piko'),
    '168p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zirda, the Dawnwaker'),
    (select id from sets where short_name = 'piko'),
    '233s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Narset of the Ancient Way'),
    (select id from sets where short_name = 'piko'),
    '195s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Genesis Ultimatum'),
    (select id from sets where short_name = 'piko'),
    '189p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Narset of the Ancient Way'),
    (select id from sets where short_name = 'piko'),
    '195p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Emergent Ultimatum'),
    (select id from sets where short_name = 'piko'),
    '185s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Song of Creation'),
    (select id from sets where short_name = 'piko'),
    '210p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mythos of Vadrok'),
    (select id from sets where short_name = 'piko'),
    '127p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savai Triome'),
    (select id from sets where short_name = 'piko'),
    '253p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quartzwood Crasher'),
    (select id from sets where short_name = 'piko'),
    '201s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Everquill Phoenix'),
    (select id from sets where short_name = 'piko'),
    '114p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whirlwind of Thought'),
    (select id from sets where short_name = 'piko'),
    '215s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Obosh, the Preypiercer'),
    (select id from sets where short_name = 'piko'),
    '228p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Luminous Broodmoth'),
    (select id from sets where short_name = 'piko'),
    '21p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mythos of Nethroi'),
    (select id from sets where short_name = 'piko'),
    '97s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaheera, the Orphanguard'),
    (select id from sets where short_name = 'piko'),
    '224p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Titans'' Nest'),
    (select id from sets where short_name = 'piko'),
    '212s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Obosh, the Preypiercer'),
    (select id from sets where short_name = 'piko'),
    '228s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keruga, the Macrosage'),
    (select id from sets where short_name = 'piko'),
    '225p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frondland Felidar'),
    (select id from sets where short_name = 'piko'),
    '186p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snapdax, Apex of the Hunt'),
    (select id from sets where short_name = 'piko'),
    '209s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gyruda, Doom of Depths'),
    (select id from sets where short_name = 'piko'),
    '221s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jegantha, the Wellspring'),
    (select id from sets where short_name = 'piko'),
    '222s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bonders'' Enclave'),
    (select id from sets where short_name = 'piko'),
    '245s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Ozolith'),
    (select id from sets where short_name = 'piko'),
    '237s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winota, Joiner of Forces'),
    (select id from sets where short_name = 'piko'),
    '216p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lurrus of the Dream-Den'),
    (select id from sets where short_name = 'piko'),
    '226p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Genesis Ultimatum'),
    (select id from sets where short_name = 'piko'),
    '189s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ketria Triome'),
    (select id from sets where short_name = 'piko'),
    '250p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nethroi, Apex of Death'),
    (select id from sets where short_name = 'piko'),
    '197s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gemrazer'),
    (select id from sets where short_name = 'piko'),
    '155s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slitherwisp'),
    (select id from sets where short_name = 'piko'),
    '208p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zagoth Triome'),
    (select id from sets where short_name = 'piko'),
    '259p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emergent Ultimatum'),
    (select id from sets where short_name = 'piko'),
    '185p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nethroi, Apex of Death'),
    (select id from sets where short_name = 'piko'),
    '197p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Voracious Greatshark'),
    (select id from sets where short_name = 'piko'),
    '70p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'General Kudro of Drannith'),
    (select id from sets where short_name = 'piko'),
    '187s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bonders'' Enclave'),
    (select id from sets where short_name = 'piko'),
    '245p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raugrin Triome'),
    (select id from sets where short_name = 'piko'),
    '251s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Song of Creation'),
    (select id from sets where short_name = 'piko'),
    '210s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skycat Sovereign'),
    (select id from sets where short_name = 'piko'),
    '207s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chevill, Bane of Monsters'),
    (select id from sets where short_name = 'piko'),
    '181s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vivien, Monsters'' Advocate'),
    (select id from sets where short_name = 'piko'),
    '175p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hunted Nightmare'),
    (select id from sets where short_name = 'piko'),
    '92p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cubwarden'),
    (select id from sets where short_name = 'piko'),
    '7s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Everquill Phoenix'),
    (select id from sets where short_name = 'piko'),
    '114s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quartzwood Crasher'),
    (select id from sets where short_name = 'piko'),
    '201p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaheera, the Orphanguard'),
    (select id from sets where short_name = 'piko'),
    '224s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Extinction Event'),
    (select id from sets where short_name = 'piko'),
    '88s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea-Dasher Octopus'),
    (select id from sets where short_name = 'piko'),
    '66s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Offspring''s Revenge'),
    (select id from sets where short_name = 'piko'),
    '198s',
    'rare'
) 
 on conflict do nothing;
