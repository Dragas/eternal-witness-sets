insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Temple of Epiphany'),
    (select id from sets where short_name = 'm20'),
    '253',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm20'),
    '278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scheming Symmetry'),
    (select id from sets where short_name = 'm20'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Burglar'),
    (select id from sets where short_name = 'm20'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eternal Isolation'),
    (select id from sets where short_name = 'm20'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mu Yanling, Sky Dancer'),
    (select id from sets where short_name = 'm20'),
    '68',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wakeroot Elemental'),
    (select id from sets where short_name = 'm20'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dread Presence'),
    (select id from sets where short_name = 'm20'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leyline of the Void'),
    (select id from sets where short_name = 'm20'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sage''s Row Denizen'),
    (select id from sets where short_name = 'm20'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fry'),
    (select id from sets where short_name = 'm20'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Netcaster Spider'),
    (select id from sets where short_name = 'm20'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brineborn Cutthroat'),
    (select id from sets where short_name = 'm20'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grafdigger''s Cage'),
    (select id from sets where short_name = 'm20'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ogre Siegebreaker'),
    (select id from sets where short_name = 'm20'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Canopy Spider'),
    (select id from sets where short_name = 'm20'),
    '339',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cavalier of Flame'),
    (select id from sets where short_name = 'm20'),
    '125',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Temple of Mystery'),
    (select id from sets where short_name = 'm20'),
    '255',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Daybreak Chaplain'),
    (select id from sets where short_name = 'm20'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barkhide Troll'),
    (select id from sets where short_name = 'm20'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Agonizing Syphon'),
    (select id from sets where short_name = 'm20'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Titanic Growth'),
    (select id from sets where short_name = 'm20'),
    '343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Imperial Outrider'),
    (select id from sets where short_name = 'm20'),
    '307',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lotus Field'),
    (select id from sets where short_name = 'm20'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hostile Minotaur'),
    (select id from sets where short_name = 'm20'),
    '331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Master Splicer'),
    (select id from sets where short_name = 'm20'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'm20'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gruesome Scourger'),
    (select id from sets where short_name = 'm20'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantom Warrior'),
    (select id from sets where short_name = 'm20'),
    '316',
    'common'
) ,
(
    (select id from mtgcard where name = 'Octoprophet'),
    (select id from sets where short_name = 'm20'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Befuddle'),
    (select id from sets where short_name = 'm20'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tale''s End'),
    (select id from sets where short_name = 'm20'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ember Hauler'),
    (select id from sets where short_name = 'm20'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yarok''s Fenlurker'),
    (select id from sets where short_name = 'm20'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disfigure'),
    (select id from sets where short_name = 'm20'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm20'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Waterkin Shaman'),
    (select id from sets where short_name = 'm20'),
    '288',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loxodon Lifechanter'),
    (select id from sets where short_name = 'm20'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Show of Valor'),
    (select id from sets where short_name = 'm20'),
    '311',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Golem'),
    (select id from sets where short_name = 'm20'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'm20'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battalion Foot Soldier'),
    (select id from sets where short_name = 'm20'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shared Summons'),
    (select id from sets where short_name = 'm20'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Growth Cycle'),
    (select id from sets where short_name = 'm20'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prowling Caracal'),
    (select id from sets where short_name = 'm20'),
    '309',
    'common'
) ,
(
    (select id from mtgcard where name = 'Repeated Reverberation'),
    (select id from sets where short_name = 'm20'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mammoth Spider'),
    (select id from sets where short_name = 'm20'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kaalia, Zenith Seeker'),
    (select id from sets where short_name = 'm20'),
    '210',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Golos, Tireless Pilgrim'),
    (select id from sets where short_name = 'm20'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rienne, Angel of Rebirth'),
    (select id from sets where short_name = 'm20'),
    '281',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Act of Treason'),
    (select id from sets where short_name = 'm20'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anticipate'),
    (select id from sets where short_name = 'm20'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = 'm20'),
    '335',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ferocious Pup'),
    (select id from sets where short_name = 'm20'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woodland Champion'),
    (select id from sets where short_name = 'm20'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wildfire Elemental'),
    (select id from sets where short_name = 'm20'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunderkin Awakener'),
    (select id from sets where short_name = 'm20'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faerie Miscreant'),
    (select id from sets where short_name = 'm20'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodthirsty Aerialist'),
    (select id from sets where short_name = 'm20'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight of the Ebon Legion'),
    (select id from sets where short_name = 'm20'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maniacal Rage'),
    (select id from sets where short_name = 'm20'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leyline of Anticipation'),
    (select id from sets where short_name = 'm20'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancestral Blade'),
    (select id from sets where short_name = 'm20'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sleep Paralysis'),
    (select id from sets where short_name = 'm20'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolfkin Bond'),
    (select id from sets where short_name = 'm20'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightpack Ambusher'),
    (select id from sets where short_name = 'm20'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rapacious Dragon'),
    (select id from sets where short_name = 'm20'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chandra, Acolyte of Flame'),
    (select id from sets where short_name = 'm20'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyroclastic Elemental'),
    (select id from sets where short_name = 'm20'),
    '296',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woodland Mystic'),
    (select id from sets where short_name = 'm20'),
    '344',
    'common'
) ,
(
    (select id from mtgcard where name = 'Devout Decree'),
    (select id from sets where short_name = 'm20'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sorcerer of the Fang'),
    (select id from sets where short_name = 'm20'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Embercat'),
    (select id from sets where short_name = 'm20'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Take Vengeance'),
    (select id from sets where short_name = 'm20'),
    '313',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'm20'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blossoming Sands'),
    (select id from sets where short_name = 'm20'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soulmender'),
    (select id from sets where short_name = 'm20'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unholy Indenture'),
    (select id from sets where short_name = 'm20'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fortress Crab'),
    (select id from sets where short_name = 'm20'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'm20'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Gift'),
    (select id from sets where short_name = 'm20'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Portal of Sanctuary'),
    (select id from sets where short_name = 'm20'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gorging Vulture'),
    (select id from sets where short_name = 'm20'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pack Mastiff'),
    (select id from sets where short_name = 'm20'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm20'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Reclaimer'),
    (select id from sets where short_name = 'm20'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scuttlemutt'),
    (select id from sets where short_name = 'm20'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leyline of Combustion'),
    (select id from sets where short_name = 'm20'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scoured Barrens'),
    (select id from sets where short_name = 'm20'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'm20'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skeleton Archer'),
    (select id from sets where short_name = 'm20'),
    '324',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lavakin Brawler'),
    (select id from sets where short_name = 'm20'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ethereal Elk'),
    (select id from sets where short_name = 'm20'),
    '299',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sorin''s Guide'),
    (select id from sets where short_name = 'm20'),
    '292',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm20'),
    '277',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inspired Charge'),
    (select id from sets where short_name = 'm20'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Stormkin'),
    (select id from sets where short_name = 'm20'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm20'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bartizan Bats'),
    (select id from sets where short_name = 'm20'),
    '319',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flame Sweep'),
    (select id from sets where short_name = 'm20'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Field of the Dead'),
    (select id from sets where short_name = 'm20'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boreal Elemental'),
    (select id from sets where short_name = 'm20'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bag of Holding'),
    (select id from sets where short_name = 'm20'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Legion''s End'),
    (select id from sets where short_name = 'm20'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra, Awakened Inferno'),
    (select id from sets where short_name = 'm20'),
    '127',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Plummet'),
    (select id from sets where short_name = 'm20'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Epicure of Blood'),
    (select id from sets where short_name = 'm20'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm20'),
    '275',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Elemental'),
    (select id from sets where short_name = 'm20'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gargos, Vicious Watcher'),
    (select id from sets where short_name = 'm20'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aggressive Mammoth'),
    (select id from sets where short_name = 'm20'),
    '337',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silverback Shaman'),
    (select id from sets where short_name = 'm20'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm20'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dawning Angel'),
    (select id from sets where short_name = 'm20'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravewaker'),
    (select id from sets where short_name = 'm20'),
    '323',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nimble Birdsticker'),
    (select id from sets where short_name = 'm20'),
    '333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm20'),
    '276',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Mage'),
    (select id from sets where short_name = 'm20'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thicket Crasher'),
    (select id from sets where short_name = 'm20'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fencing Ace'),
    (select id from sets where short_name = 'm20'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moat Piranhas'),
    (select id from sets where short_name = 'm20'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyknight Vanguard'),
    (select id from sets where short_name = 'm20'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Bird-Grabber'),
    (select id from sets where short_name = 'm20'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Retributive Wand'),
    (select id from sets where short_name = 'm20'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Concordia Pegasus'),
    (select id from sets where short_name = 'm20'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drawn from Dreams'),
    (select id from sets where short_name = 'm20'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm20'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vial of Dragonfire'),
    (select id from sets where short_name = 'm20'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivien, Arkbow Ranger'),
    (select id from sets where short_name = 'm20'),
    '199',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kykar, Wind''s Fury'),
    (select id from sets where short_name = 'm20'),
    '212',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thirsting Bloodlord'),
    (select id from sets where short_name = 'm20'),
    '293',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daggersail Aeronaut'),
    (select id from sets where short_name = 'm20'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savannah Sage'),
    (select id from sets where short_name = 'm20'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Griffin Protector'),
    (select id from sets where short_name = 'm20'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Remedy'),
    (select id from sets where short_name = 'm20'),
    '321',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prismite'),
    (select id from sets where short_name = 'm20'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra, Flame''s Fury'),
    (select id from sets where short_name = 'm20'),
    '294',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Soul Salvage'),
    (select id from sets where short_name = 'm20'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Planar Cleansing'),
    (select id from sets where short_name = 'm20'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Flame Wave'),
    (select id from sets where short_name = 'm20'),
    '295',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tomebound Lich'),
    (select id from sets where short_name = 'm20'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moldervine Reclamation'),
    (select id from sets where short_name = 'm20'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Apostle of Purifying Light'),
    (select id from sets where short_name = 'm20'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Masterful Replication'),
    (select id from sets where short_name = 'm20'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twinblade Paladin'),
    (select id from sets where short_name = 'm20'),
    '285',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Temple of Silence'),
    (select id from sets where short_name = 'm20'),
    '256',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aerial Assault'),
    (select id from sets where short_name = 'm20'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frost Lynx'),
    (select id from sets where short_name = 'm20'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marauding Raptor'),
    (select id from sets where short_name = 'm20'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fathom Fleet Cutthroat'),
    (select id from sets where short_name = 'm20'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Season of Growth'),
    (select id from sets where short_name = 'm20'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spectral Sailor'),
    (select id from sets where short_name = 'm20'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goldmane Griffin'),
    (select id from sets where short_name = 'm20'),
    '283',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reckless Air Strike'),
    (select id from sets where short_name = 'm20'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gnarlback Rhino'),
    (select id from sets where short_name = 'm20'),
    '300',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Ringleader'),
    (select id from sets where short_name = 'm20'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Impassioned Orator'),
    (select id from sets where short_name = 'm20'),
    '306',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murder'),
    (select id from sets where short_name = 'm20'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm20'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple of Malady'),
    (select id from sets where short_name = 'm20'),
    '254',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steel Overseer'),
    (select id from sets where short_name = 'm20'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trusted Pegasus'),
    (select id from sets where short_name = 'm20'),
    '314',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manifold Key'),
    (select id from sets where short_name = 'm20'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overcome'),
    (select id from sets where short_name = 'm20'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Siege Mastodon'),
    (select id from sets where short_name = 'm20'),
    '312',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marauder''s Axe'),
    (select id from sets where short_name = 'm20'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frilled Sandwalla'),
    (select id from sets where short_name = 'm20'),
    '340',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm20'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voracious Hydra'),
    (select id from sets where short_name = 'm20'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tranquil Cove'),
    (select id from sets where short_name = 'm20'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raise the Alarm'),
    (select id from sets where short_name = 'm20'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Embodiment of Agonies'),
    (select id from sets where short_name = 'm20'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bristling Boar'),
    (select id from sets where short_name = 'm20'),
    '338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Gust'),
    (select id from sets where short_name = 'm20'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blood for Bones'),
    (select id from sets where short_name = 'm20'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vorstclaw'),
    (select id from sets where short_name = 'm20'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riddlemaster Sphinx'),
    (select id from sets where short_name = 'm20'),
    '317',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani, Strength of the Pride'),
    (select id from sets where short_name = 'm20'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = 'm20'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infuriate'),
    (select id from sets where short_name = 'm20'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moment of Heroism'),
    (select id from sets where short_name = 'm20'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivien''s Crocodile'),
    (select id from sets where short_name = 'm20'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overgrowth Elemental'),
    (select id from sets where short_name = 'm20'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loyal Pegasus'),
    (select id from sets where short_name = 'm20'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bastion Enforcer'),
    (select id from sets where short_name = 'm20'),
    '303',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disentomb'),
    (select id from sets where short_name = 'm20'),
    '322',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bone Splinters'),
    (select id from sets where short_name = 'm20'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scholar of the Ages'),
    (select id from sets where short_name = 'm20'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Starfield Mystic'),
    (select id from sets where short_name = 'm20'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swiftwater Cliffs'),
    (select id from sets where short_name = 'm20'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Risen Reef'),
    (select id from sets where short_name = 'm20'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Uncaged Fury'),
    (select id from sets where short_name = 'm20'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra''s Guardian'),
    (select id from sets where short_name = 'm20'),
    '310',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mu Yanling, Celestial Wind'),
    (select id from sets where short_name = 'm20'),
    '286',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Natural End'),
    (select id from sets where short_name = 'm20'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keldon Raider'),
    (select id from sets where short_name = 'm20'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leafkin Druid'),
    (select id from sets where short_name = 'm20'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Celestial Messenger'),
    (select id from sets where short_name = 'm20'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fearless Halberdier'),
    (select id from sets where short_name = 'm20'),
    '329',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Spitfire'),
    (select id from sets where short_name = 'm20'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Corpse Knight'),
    (select id from sets where short_name = 'm20'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Assailant'),
    (select id from sets where short_name = 'm20'),
    '330',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steadfast Sentry'),
    (select id from sets where short_name = 'm20'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Salvager of Ruin'),
    (select id from sets where short_name = 'm20'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shifting Ceratops'),
    (select id from sets where short_name = 'm20'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noxious Grasp'),
    (select id from sets where short_name = 'm20'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Regulator'),
    (select id from sets where short_name = 'm20'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm20'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gods Willing'),
    (select id from sets where short_name = 'm20'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wind-Scarred Crag'),
    (select id from sets where short_name = 'm20'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Forge'),
    (select id from sets where short_name = 'm20'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yanling''s Harbinger'),
    (select id from sets where short_name = 'm20'),
    '289',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Smuggler'),
    (select id from sets where short_name = 'm20'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coral Merfolk'),
    (select id from sets where short_name = 'm20'),
    '315',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sedge Scorpion'),
    (select id from sets where short_name = 'm20'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inspiring Captain'),
    (select id from sets where short_name = 'm20'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pulse of Murasa'),
    (select id from sets where short_name = 'm20'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jungle Hollow'),
    (select id from sets where short_name = 'm20'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icon of Ancestry'),
    (select id from sets where short_name = 'm20'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodfell Caves'),
    (select id from sets where short_name = 'm20'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Herald of the Sun'),
    (select id from sets where short_name = 'm20'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moorland Inquisitor'),
    (select id from sets where short_name = 'm20'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm20'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sorin, Imperious Bloodlord'),
    (select id from sets where short_name = 'm20'),
    '115',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Yarok, the Desecrated'),
    (select id from sets where short_name = 'm20'),
    '220',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rule of Law'),
    (select id from sets where short_name = 'm20'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savage Gorger'),
    (select id from sets where short_name = 'm20'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = 'm20'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Captivating Gyre'),
    (select id from sets where short_name = 'm20'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cavalier of Gales'),
    (select id from sets where short_name = 'm20'),
    '52',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dismal Backwater'),
    (select id from sets where short_name = 'm20'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yoked Ox'),
    (select id from sets where short_name = 'm20'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple of Triumph'),
    (select id from sets where short_name = 'm20'),
    '257',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm20'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Veil of Summer'),
    (select id from sets where short_name = 'm20'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anvilwrought Raptor'),
    (select id from sets where short_name = 'm20'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boneclad Necromancer'),
    (select id from sets where short_name = 'm20'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tectonic Rift'),
    (select id from sets where short_name = 'm20'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Empyrean Eagle'),
    (select id from sets where short_name = 'm20'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bogstomper'),
    (select id from sets where short_name = 'm20'),
    '320',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'm20'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pattern Matcher'),
    (select id from sets where short_name = 'm20'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brought Back'),
    (select id from sets where short_name = 'm20'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bladebrand'),
    (select id from sets where short_name = 'm20'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Immortal Phoenix'),
    (select id from sets where short_name = 'm20'),
    '332',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barony Vampire'),
    (select id from sets where short_name = 'm20'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rubblebelt Recluse'),
    (select id from sets where short_name = 'm20'),
    '334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cerulean Drake'),
    (select id from sets where short_name = 'm20'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feral Abomination'),
    (select id from sets where short_name = 'm20'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire of the Dire Moon'),
    (select id from sets where short_name = 'm20'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frilled Sea Serpent'),
    (select id from sets where short_name = 'm20'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrashing Brontodon'),
    (select id from sets where short_name = 'm20'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meteor Golem'),
    (select id from sets where short_name = 'm20'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Convolute'),
    (select id from sets where short_name = 'm20'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Atemsis, All-Seeing'),
    (select id from sets where short_name = 'm20'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodsoaked Altar'),
    (select id from sets where short_name = 'm20'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = 'm20'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warden of Evos Isle'),
    (select id from sets where short_name = 'm20'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wolfrider''s Saddle'),
    (select id from sets where short_name = 'm20'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prized Unicorn'),
    (select id from sets where short_name = 'm20'),
    '342',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'm20'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scorch Spitter'),
    (select id from sets where short_name = 'm20'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sephara, Sky''s Blade'),
    (select id from sets where short_name = 'm20'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blightbeetle'),
    (select id from sets where short_name = 'm20'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Squad Captain'),
    (select id from sets where short_name = 'm20'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mask of Immolation'),
    (select id from sets where short_name = 'm20'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leyline of Sanctity'),
    (select id from sets where short_name = 'm20'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sorin, Vampire Lord'),
    (select id from sets where short_name = 'm20'),
    '290',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Undead Servant'),
    (select id from sets where short_name = 'm20'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sorin''s Thirst'),
    (select id from sets where short_name = 'm20'),
    '325',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feral Invocation'),
    (select id from sets where short_name = 'm20'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flood of Tears'),
    (select id from sets where short_name = 'm20'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leyline of Abundance'),
    (select id from sets where short_name = 'm20'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yarok''s Wavecrasher'),
    (select id from sets where short_name = 'm20'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm20'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ripscale Predator'),
    (select id from sets where short_name = 'm20'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bishop of Wings'),
    (select id from sets where short_name = 'm20'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Griffin Sentinel'),
    (select id from sets where short_name = 'm20'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Omnath, Locus of the Roil'),
    (select id from sets where short_name = 'm20'),
    '216',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Healer of the Glade'),
    (select id from sets where short_name = 'm20'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm20'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Agent of Treachery'),
    (select id from sets where short_name = 'm20'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampire Opportunist'),
    (select id from sets where short_name = 'm20'),
    '326',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corpse Knight'),
    (select id from sets where short_name = 'm20'),
    '206†',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drakuseth, Maw of Flames'),
    (select id from sets where short_name = 'm20'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oakenform'),
    (select id from sets where short_name = 'm20'),
    '341',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dungeon Geists'),
    (select id from sets where short_name = 'm20'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vengeful Warchief'),
    (select id from sets where short_name = 'm20'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chandra, Novice Pyromancer'),
    (select id from sets where short_name = 'm20'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gift of Paradise'),
    (select id from sets where short_name = 'm20'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rotting Regisaur'),
    (select id from sets where short_name = 'm20'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Destructive Digger'),
    (select id from sets where short_name = 'm20'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gauntlets of Light'),
    (select id from sets where short_name = 'm20'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diviner''s Lockbox'),
    (select id from sets where short_name = 'm20'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zephyr Charge'),
    (select id from sets where short_name = 'm20'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glaring Aegis'),
    (select id from sets where short_name = 'm20'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unchained Berserker'),
    (select id from sets where short_name = 'm20'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ajani, Inspiring Leader'),
    (select id from sets where short_name = 'm20'),
    '282',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scampering Scorcher'),
    (select id from sets where short_name = 'm20'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glint-Horn Buccaneer'),
    (select id from sets where short_name = 'm20'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bone to Ash'),
    (select id from sets where short_name = 'm20'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm20'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Guardian'),
    (select id from sets where short_name = 'm20'),
    '302',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hanged Executioner'),
    (select id from sets where short_name = 'm20'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hard Cover'),
    (select id from sets where short_name = 'm20'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brightwood Tracker'),
    (select id from sets where short_name = 'm20'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Audacious Thief'),
    (select id from sets where short_name = 'm20'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudkin Seer'),
    (select id from sets where short_name = 'm20'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heart-Piercer Bow'),
    (select id from sets where short_name = 'm20'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loaming Shaman'),
    (select id from sets where short_name = 'm20'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel of Vitality'),
    (select id from sets where short_name = 'm20'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cavalier of Thorns'),
    (select id from sets where short_name = 'm20'),
    '167',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ironclad Krovod'),
    (select id from sets where short_name = 'm20'),
    '308',
    'common'
) ,
(
    (select id from mtgcard where name = 'Centaur Courser'),
    (select id from sets where short_name = 'm20'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm20'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greenwood Sentinel'),
    (select id from sets where short_name = 'm20'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howling Giant'),
    (select id from sets where short_name = 'm20'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Renowned Weaponsmith'),
    (select id from sets where short_name = 'm20'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vilis, Broker of Blood'),
    (select id from sets where short_name = 'm20'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Outrage'),
    (select id from sets where short_name = 'm20'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ironroot Warlord'),
    (select id from sets where short_name = 'm20'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diamond Knight'),
    (select id from sets where short_name = 'm20'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thornwood Falls'),
    (select id from sets where short_name = 'm20'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snapping Drake'),
    (select id from sets where short_name = 'm20'),
    '318',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanitarium Skeleton'),
    (select id from sets where short_name = 'm20'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cavalier of Night'),
    (select id from sets where short_name = 'm20'),
    '94',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rugged Highlands'),
    (select id from sets where short_name = 'm20'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Creeping Trailblazer'),
    (select id from sets where short_name = 'm20'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rabid Bite'),
    (select id from sets where short_name = 'm20'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Engulfing Eruption'),
    (select id from sets where short_name = 'm20'),
    '328',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm20'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Dragon'),
    (select id from sets where short_name = 'm20'),
    '336',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Walking Corpse'),
    (select id from sets where short_name = 'm20'),
    '327',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thought Distortion'),
    (select id from sets where short_name = 'm20'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Haazda Officer'),
    (select id from sets where short_name = 'm20'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Might of the Masses'),
    (select id from sets where short_name = 'm20'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cavalier of Dawn'),
    (select id from sets where short_name = 'm20'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vivien, Nature''s Avenger'),
    (select id from sets where short_name = 'm20'),
    '298',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Colossus Hammer'),
    (select id from sets where short_name = 'm20'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Metropolis Sprite'),
    (select id from sets where short_name = 'm20'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cryptic Caves'),
    (select id from sets where short_name = 'm20'),
    '244',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kethis, the Hidden Hand'),
    (select id from sets where short_name = 'm20'),
    '211',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Reduce to Ashes'),
    (select id from sets where short_name = 'm20'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm20'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winged Words'),
    (select id from sets where short_name = 'm20'),
    '80',
    'common'
) 
 on conflict do nothing;
