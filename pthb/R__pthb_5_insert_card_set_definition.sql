insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Atris, Oracle of Half-Truths'),
    (select id from sets where short_name = 'pthb'),
    '209s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gravebreaker Lamia'),
    (select id from sets where short_name = 'pthb'),
    '98s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Purphoros''s Intervention'),
    (select id from sets where short_name = 'pthb'),
    '151p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kroxa, Titan of Death''s Hunger'),
    (select id from sets where short_name = 'pthb'),
    '221p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dryad of the Ilysian Grove'),
    (select id from sets where short_name = 'pthb'),
    '169s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Atris, Oracle of Half-Truths'),
    (select id from sets where short_name = 'pthb'),
    '209p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nylea''s Intervention'),
    (select id from sets where short_name = 'pthb'),
    '188s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashiok, Nightmare Muse'),
    (select id from sets where short_name = 'pthb'),
    '208p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Purphoros, Bronze-Blooded'),
    (select id from sets where short_name = 'pthb'),
    '150s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Haktos the Unscarred'),
    (select id from sets where short_name = 'pthb'),
    '218p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nylea, Keen-Eyed'),
    (select id from sets where short_name = 'pthb'),
    '185p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bronzehide Lion'),
    (select id from sets where short_name = 'pthb'),
    '210p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eidolon of Obstruction'),
    (select id from sets where short_name = 'pthb'),
    '12p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Enlightenment'),
    (select id from sets where short_name = 'pthb'),
    '246p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thassa''s Intervention'),
    (select id from sets where short_name = 'pthb'),
    '72s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shadowspear'),
    (select id from sets where short_name = 'pthb'),
    '236p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Taranika, Akroan Veteran'),
    (select id from sets where short_name = 'pthb'),
    '39p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gallia of the Endless Dance'),
    (select id from sets where short_name = 'pthb'),
    '217p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Underworld Breach'),
    (select id from sets where short_name = 'pthb'),
    '161s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Taranika, Akroan Veteran'),
    (select id from sets where short_name = 'pthb'),
    '39s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Calix, Destiny''s Hand'),
    (select id from sets where short_name = 'pthb'),
    '211p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'The First Iroan Games'),
    (select id from sets where short_name = 'pthb'),
    '170p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uro, Titan of Nature''s Wrath'),
    (select id from sets where short_name = 'pthb'),
    '229s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Underworld Breach'),
    (select id from sets where short_name = 'pthb'),
    '161p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Akroan War'),
    (select id from sets where short_name = 'pthb'),
    '124s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Abandon'),
    (select id from sets where short_name = 'pthb'),
    '244s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Klothys, God of Destiny'),
    (select id from sets where short_name = 'pthb'),
    '220p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Purphoros, Bronze-Blooded'),
    (select id from sets where short_name = 'pthb'),
    '150p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Heliod''s Intervention'),
    (select id from sets where short_name = 'pthb'),
    '19s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashiok''s Erasure'),
    (select id from sets where short_name = 'pthb'),
    '43p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dalakos, Crafter of Wonders'),
    (select id from sets where short_name = 'pthb'),
    '212s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eat to Extinction'),
    (select id from sets where short_name = 'pthb'),
    '90p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dream Trawler'),
    (select id from sets where short_name = 'pthb'),
    '214p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Deceit'),
    (select id from sets where short_name = 'pthb'),
    '245p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phoenix of Ash'),
    (select id from sets where short_name = 'pthb'),
    '148s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nylea''s Intervention'),
    (select id from sets where short_name = 'pthb'),
    '188p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archon of Sun''s Grace'),
    (select id from sets where short_name = 'pthb'),
    '3s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Labyrinth of Skophos'),
    (select id from sets where short_name = 'pthb'),
    '243p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elspeth, Sun''s Nemesis'),
    (select id from sets where short_name = 'pthb'),
    '14s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Erebos''s Intervention'),
    (select id from sets where short_name = 'pthb'),
    '94s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Protean Thaumaturge'),
    (select id from sets where short_name = 'pthb'),
    '60s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Plenty'),
    (select id from sets where short_name = 'pthb'),
    '248s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Allure of the Unknown'),
    (select id from sets where short_name = 'pthb'),
    '207s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Labyrinth of Skophos'),
    (select id from sets where short_name = 'pthb'),
    '243s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Purphoros''s Intervention'),
    (select id from sets where short_name = 'pthb'),
    '151s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Setessan Champion'),
    (select id from sets where short_name = 'pthb'),
    '198p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm Herald'),
    (select id from sets where short_name = 'pthb'),
    '156p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eidolon of Obstruction'),
    (select id from sets where short_name = 'pthb'),
    '12s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Setessan Champion'),
    (select id from sets where short_name = 'pthb'),
    '198s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elspeth Conquers Death'),
    (select id from sets where short_name = 'pthb'),
    '13s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Akroan War'),
    (select id from sets where short_name = 'pthb'),
    '124p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tectonic Giant'),
    (select id from sets where short_name = 'pthb'),
    '158p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dalakos, Crafter of Wonders'),
    (select id from sets where short_name = 'pthb'),
    '212p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Enlightenment'),
    (select id from sets where short_name = 'pthb'),
    '246s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thassa, Deep-Dwelling'),
    (select id from sets where short_name = 'pthb'),
    '71s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Erebos, Bleak-Hearted'),
    (select id from sets where short_name = 'pthb'),
    '93p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shatter the Sky'),
    (select id from sets where short_name = 'pthb'),
    '37s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Deceit'),
    (select id from sets where short_name = 'pthb'),
    '245s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shadowspear'),
    (select id from sets where short_name = 'pthb'),
    '236s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Klothys, God of Destiny'),
    (select id from sets where short_name = 'pthb'),
    '220s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Archon of Sun''s Grace'),
    (select id from sets where short_name = 'pthb'),
    '3p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mantle of the Wolf'),
    (select id from sets where short_name = 'pthb'),
    '178p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Polukranos, Unchained'),
    (select id from sets where short_name = 'pthb'),
    '224s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'The First Iroan Games'),
    (select id from sets where short_name = 'pthb'),
    '170s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enigmatic Incarnation'),
    (select id from sets where short_name = 'pthb'),
    '215p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woe Strider'),
    (select id from sets where short_name = 'pthb'),
    '123s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arasta of the Endless Web'),
    (select id from sets where short_name = 'pthb'),
    '165s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Malice'),
    (select id from sets where short_name = 'pthb'),
    '247p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightmare Shepherd'),
    (select id from sets where short_name = 'pthb'),
    '108p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatter the Sky'),
    (select id from sets where short_name = 'pthb'),
    '37p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kunoros, Hound of Athreos'),
    (select id from sets where short_name = 'pthb'),
    '222p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nessian Boar'),
    (select id from sets where short_name = 'pthb'),
    '181s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashiok''s Erasure'),
    (select id from sets where short_name = 'pthb'),
    '43s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kiora Bests the Sea God'),
    (select id from sets where short_name = 'pthb'),
    '52p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ox of Agonas'),
    (select id from sets where short_name = 'pthb'),
    '147s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nyx Lotus'),
    (select id from sets where short_name = 'pthb'),
    '235s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thryx, the Sudden Storm'),
    (select id from sets where short_name = 'pthb'),
    '76p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nessian Boar'),
    (select id from sets where short_name = 'pthb'),
    '181p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thassa''s Intervention'),
    (select id from sets where short_name = 'pthb'),
    '72p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Polukranos, Unchained'),
    (select id from sets where short_name = 'pthb'),
    '224p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Heliod, Sun-Crowned'),
    (select id from sets where short_name = 'pthb'),
    '18s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thassa''s Oracle'),
    (select id from sets where short_name = 'pthb'),
    '73s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nadir Kraken'),
    (select id from sets where short_name = 'pthb'),
    '55p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nyxbloom Ancient'),
    (select id from sets where short_name = 'pthb'),
    '190s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thassa, Deep-Dwelling'),
    (select id from sets where short_name = 'pthb'),
    '71p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Idyllic Tutor'),
    (select id from sets where short_name = 'pthb'),
    '24p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm''s Wrath'),
    (select id from sets where short_name = 'pthb'),
    '157p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Protean Thaumaturge'),
    (select id from sets where short_name = 'pthb'),
    '60p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eat to Extinction'),
    (select id from sets where short_name = 'pthb'),
    '90s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Erebos''s Intervention'),
    (select id from sets where short_name = 'pthb'),
    '94p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Malice'),
    (select id from sets where short_name = 'pthb'),
    '247s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arasta of the Endless Web'),
    (select id from sets where short_name = 'pthb'),
    '165p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gravebreaker Lamia'),
    (select id from sets where short_name = 'pthb'),
    '98p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ox of Agonas'),
    (select id from sets where short_name = 'pthb'),
    '147p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bronzehide Lion'),
    (select id from sets where short_name = 'pthb'),
    '210s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elspeth Conquers Death'),
    (select id from sets where short_name = 'pthb'),
    '13p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wavebreak Hippocamp'),
    (select id from sets where short_name = 'pthb'),
    '80p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Calix, Destiny''s Hand'),
    (select id from sets where short_name = 'pthb'),
    '211s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wavebreak Hippocamp'),
    (select id from sets where short_name = 'pthb'),
    '80s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kunoros, Hound of Athreos'),
    (select id from sets where short_name = 'pthb'),
    '222s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kroxa, Titan of Death''s Hunger'),
    (select id from sets where short_name = 'pthb'),
    '221s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Haktos the Unscarred'),
    (select id from sets where short_name = 'pthb'),
    '218s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treacherous Blessing'),
    (select id from sets where short_name = 'pthb'),
    '117p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woe Strider'),
    (select id from sets where short_name = 'pthb'),
    '123p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightmare Shepherd'),
    (select id from sets where short_name = 'pthb'),
    '108s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treacherous Blessing'),
    (select id from sets where short_name = 'pthb'),
    '117s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Erebos, Bleak-Hearted'),
    (select id from sets where short_name = 'pthb'),
    '93s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Enigmatic Incarnation'),
    (select id from sets where short_name = 'pthb'),
    '215s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phoenix of Ash'),
    (select id from sets where short_name = 'pthb'),
    '148p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aphemia, the Cacophony'),
    (select id from sets where short_name = 'pthb'),
    '84s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Allure of the Unknown'),
    (select id from sets where short_name = 'pthb'),
    '207p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heliod''s Intervention'),
    (select id from sets where short_name = 'pthb'),
    '19p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nylea, Keen-Eyed'),
    (select id from sets where short_name = 'pthb'),
    '185s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nyx Lotus'),
    (select id from sets where short_name = 'pthb'),
    '235p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Idyllic Tutor'),
    (select id from sets where short_name = 'pthb'),
    '24s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Abandon'),
    (select id from sets where short_name = 'pthb'),
    '244p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tymaret Calls the Dead'),
    (select id from sets where short_name = 'pthb'),
    '118p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm''s Wrath'),
    (select id from sets where short_name = 'pthb'),
    '157s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm Herald'),
    (select id from sets where short_name = 'pthb'),
    '156s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elspeth, Sun''s Nemesis'),
    (select id from sets where short_name = 'pthb'),
    '14p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thryx, the Sudden Storm'),
    (select id from sets where short_name = 'pthb'),
    '76s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mantle of the Wolf'),
    (select id from sets where short_name = 'pthb'),
    '178s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tymaret Calls the Dead'),
    (select id from sets where short_name = 'pthb'),
    '118s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nyxbloom Ancient'),
    (select id from sets where short_name = 'pthb'),
    '190p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Uro, Titan of Nature''s Wrath'),
    (select id from sets where short_name = 'pthb'),
    '229p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ashiok, Nightmare Muse'),
    (select id from sets where short_name = 'pthb'),
    '208s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nadir Kraken'),
    (select id from sets where short_name = 'pthb'),
    '55s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kiora Bests the Sea God'),
    (select id from sets where short_name = 'pthb'),
    '52s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thassa''s Oracle'),
    (select id from sets where short_name = 'pthb'),
    '73p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tectonic Giant'),
    (select id from sets where short_name = 'pthb'),
    '158s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aphemia, the Cacophony'),
    (select id from sets where short_name = 'pthb'),
    '84p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dryad of the Ilysian Grove'),
    (select id from sets where short_name = 'pthb'),
    '169p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dream Trawler'),
    (select id from sets where short_name = 'pthb'),
    '214s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heliod, Sun-Crowned'),
    (select id from sets where short_name = 'pthb'),
    '18p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Temple of Plenty'),
    (select id from sets where short_name = 'pthb'),
    '248p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gallia of the Endless Dance'),
    (select id from sets where short_name = 'pthb'),
    '217s',
    'rare'
) 
 on conflict do nothing;
