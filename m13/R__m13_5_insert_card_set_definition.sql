insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Mwonvuli Beast Tracker'),
    (select id from sets where short_name = 'm13'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prized Elephant'),
    (select id from sets where short_name = 'm13'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hamletback Goliath'),
    (select id from sets where short_name = 'm13'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ravenous Rats'),
    (select id from sets where short_name = 'm13'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arctic Aven'),
    (select id from sets where short_name = 'm13'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jayemdae Tome'),
    (select id from sets where short_name = 'm13'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm13'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mark of Mutiny'),
    (select id from sets where short_name = 'm13'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gem of Becoming'),
    (select id from sets where short_name = 'm13'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serpent''s Gift'),
    (select id from sets where short_name = 'm13'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, Planeswalker'),
    (select id from sets where short_name = 'm13'),
    '199',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Redirect'),
    (select id from sets where short_name = 'm13'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mindclaw Shaman'),
    (select id from sets where short_name = 'm13'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warclamp Mastiff'),
    (select id from sets where short_name = 'm13'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Avatar'),
    (select id from sets where short_name = 'm13'),
    '32',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Omniscience'),
    (select id from sets where short_name = 'm13'),
    '63',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Searing Spear'),
    (select id from sets where short_name = 'm13'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diabolic Revelation'),
    (select id from sets where short_name = 'm13'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunpetal Grove'),
    (select id from sets where short_name = 'm13'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knight of Glory'),
    (select id from sets where short_name = 'm13'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jace''s Phantasm'),
    (select id from sets where short_name = 'm13'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm13'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fog'),
    (select id from sets where short_name = 'm13'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Roaring Primadox'),
    (select id from sets where short_name = 'm13'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm13'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hydrosurge'),
    (select id from sets where short_name = 'm13'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shimian Specter'),
    (select id from sets where short_name = 'm13'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Shade'),
    (select id from sets where short_name = 'm13'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oblivion Ring'),
    (select id from sets where short_name = 'm13'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sleep'),
    (select id from sets where short_name = 'm13'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ring of Thune'),
    (select id from sets where short_name = 'm13'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trading Post'),
    (select id from sets where short_name = 'm13'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ring of Kalonia'),
    (select id from sets where short_name = 'm13'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elderscale Wurm'),
    (select id from sets where short_name = 'm13'),
    '167',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Angelic Benediction'),
    (select id from sets where short_name = 'm13'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stuffy Doll'),
    (select id from sets where short_name = 'm13'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Reckoning'),
    (select id from sets where short_name = 'm13'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Fury'),
    (select id from sets where short_name = 'm13'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ranger''s Path'),
    (select id from sets where short_name = 'm13'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slumbering Dragon'),
    (select id from sets where short_name = 'm13'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thragtusk'),
    (select id from sets where short_name = 'm13'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crusader of Odric'),
    (select id from sets where short_name = 'm13'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana of the Dark Realms'),
    (select id from sets where short_name = 'm13'),
    '97',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Augur of Bolas'),
    (select id from sets where short_name = 'm13'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Worldfire'),
    (select id from sets where short_name = 'm13'),
    '158',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kitesail'),
    (select id from sets where short_name = 'm13'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm13'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gilded Lotus'),
    (select id from sets where short_name = 'm13'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pillarfield Ox'),
    (select id from sets where short_name = 'm13'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kindled Fury'),
    (select id from sets where short_name = 'm13'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Sunstriker'),
    (select id from sets where short_name = 'm13'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Healer of the Pride'),
    (select id from sets where short_name = 'm13'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silklash Spider'),
    (select id from sets where short_name = 'm13'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guardians of Akrasa'),
    (select id from sets where short_name = 'm13'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Revive'),
    (select id from sets where short_name = 'm13'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm13'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guardian Lions'),
    (select id from sets where short_name = 'm13'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Predatory Rampage'),
    (select id from sets where short_name = 'm13'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yeva''s Forcemage'),
    (select id from sets where short_name = 'm13'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Scorpion'),
    (select id from sets where short_name = 'm13'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battleflight Eagle'),
    (select id from sets where short_name = 'm13'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Furnace Whelp'),
    (select id from sets where short_name = 'm13'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel''s Mercy'),
    (select id from sets where short_name = 'm13'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Intrepid Hero'),
    (select id from sets where short_name = 'm13'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphinx of Uthuun'),
    (select id from sets where short_name = 'm13'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Centaur Courser'),
    (select id from sets where short_name = 'm13'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ring of Xathrid'),
    (select id from sets where short_name = 'm13'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Public Execution'),
    (select id from sets where short_name = 'm13'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trumpet Blast'),
    (select id from sets where short_name = 'm13'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phylactery Lich'),
    (select id from sets where short_name = 'm13'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Arsonist'),
    (select id from sets where short_name = 'm13'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quirion Dryad'),
    (select id from sets where short_name = 'm13'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duty-Bound Dead'),
    (select id from sets where short_name = 'm13'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm13'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spiked Baloth'),
    (select id from sets where short_name = 'm13'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ring of Valkas'),
    (select id from sets where short_name = 'm13'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Touch of the Eternal'),
    (select id from sets where short_name = 'm13'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Door to Nothingness'),
    (select id from sets where short_name = 'm13'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Sculpt'),
    (select id from sets where short_name = 'm13'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Essence Drain'),
    (select id from sets where short_name = 'm13'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reverberate'),
    (select id from sets where short_name = 'm13'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kraken Hatchling'),
    (select id from sets where short_name = 'm13'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Favor'),
    (select id from sets where short_name = 'm13'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clock of Omens'),
    (select id from sets where short_name = 'm13'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rise from the Grave'),
    (select id from sets where short_name = 'm13'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragonskull Summit'),
    (select id from sets where short_name = 'm13'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani, Caller of the Pride'),
    (select id from sets where short_name = 'm13'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Garruk''s Packleader'),
    (select id from sets where short_name = 'm13'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Walking Corpse'),
    (select id from sets where short_name = 'm13'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Titanic Growth'),
    (select id from sets where short_name = 'm13'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sentinel Spider'),
    (select id from sets where short_name = 'm13'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Brute'),
    (select id from sets where short_name = 'm13'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sign in Blood'),
    (select id from sets where short_name = 'm13'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm13'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Visionary'),
    (select id from sets where short_name = 'm13'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cleaver Riot'),
    (select id from sets where short_name = 'm13'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ground Seal'),
    (select id from sets where short_name = 'm13'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Staff of Nin'),
    (select id from sets where short_name = 'm13'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crippling Blight'),
    (select id from sets where short_name = 'm13'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clone'),
    (select id from sets where short_name = 'm13'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vastwood Gorger'),
    (select id from sets where short_name = 'm13'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wit''s End'),
    (select id from sets where short_name = 'm13'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smelt'),
    (select id from sets where short_name = 'm13'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Turn to Slag'),
    (select id from sets where short_name = 'm13'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archaeomancer'),
    (select id from sets where short_name = 'm13'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rootbound Crag'),
    (select id from sets where short_name = 'm13'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primal Clay'),
    (select id from sets where short_name = 'm13'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Index'),
    (select id from sets where short_name = 'm13'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sands of Delirium'),
    (select id from sets where short_name = 'm13'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Captain''s Call'),
    (select id from sets where short_name = 'm13'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faith''s Reward'),
    (select id from sets where short_name = 'm13'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm13'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodthrone Vampire'),
    (select id from sets where short_name = 'm13'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duskdale Wurm'),
    (select id from sets where short_name = 'm13'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Switcheroo'),
    (select id from sets where short_name = 'm13'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'War Falcon'),
    (select id from sets where short_name = 'm13'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flinthoof Boar'),
    (select id from sets where short_name = 'm13'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Void Stalker'),
    (select id from sets where short_name = 'm13'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yeva, Nature''s Herald'),
    (select id from sets where short_name = 'm13'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elixir of Immortality'),
    (select id from sets where short_name = 'm13'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm13'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harbor Bandit'),
    (select id from sets where short_name = 'm13'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = 'm13'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krenko''s Command'),
    (select id from sets where short_name = 'm13'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhox Faithmender'),
    (select id from sets where short_name = 'm13'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Erase'),
    (select id from sets where short_name = 'm13'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Goliath'),
    (select id from sets where short_name = 'm13'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tormented Soul'),
    (select id from sets where short_name = 'm13'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fog Bank'),
    (select id from sets where short_name = 'm13'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drowned Catacomb'),
    (select id from sets where short_name = 'm13'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tricks of the Trade'),
    (select id from sets where short_name = 'm13'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bountiful Harvest'),
    (select id from sets where short_name = 'm13'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'm13'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Nocturnus'),
    (select id from sets where short_name = 'm13'),
    '113',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Captain of the Watch'),
    (select id from sets where short_name = 'm13'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mark of the Vampire'),
    (select id from sets where short_name = 'm13'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arms Dealer'),
    (select id from sets where short_name = 'm13'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm13'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm13'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primordial Hydra'),
    (select id from sets where short_name = 'm13'),
    '183',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm13'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vile Rebirth'),
    (select id from sets where short_name = 'm13'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace, Memory Adept'),
    (select id from sets where short_name = 'm13'),
    '56',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fungal Sprouting'),
    (select id from sets where short_name = 'm13'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Garruk, Primal Hunter'),
    (select id from sets where short_name = 'm13'),
    '174',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bloodhunter Bat'),
    (select id from sets where short_name = 'm13'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Welkin Tern'),
    (select id from sets where short_name = 'm13'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Nighthawk'),
    (select id from sets where short_name = 'm13'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silvercoat Lion'),
    (select id from sets where short_name = 'm13'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of Infamy'),
    (select id from sets where short_name = 'm13'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Safe Passage'),
    (select id from sets where short_name = 'm13'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crimson Muckwader'),
    (select id from sets where short_name = 'm13'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Watercourser'),
    (select id from sets where short_name = 'm13'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadly Recluse'),
    (select id from sets where short_name = 'm13'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk of the Pearl Trident'),
    (select id from sets where short_name = 'm13'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divination'),
    (select id from sets where short_name = 'm13'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Guess'),
    (select id from sets where short_name = 'm13'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Hulk'),
    (select id from sets where short_name = 'm13'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ring of Evos Isle'),
    (select id from sets where short_name = 'm13'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fervor'),
    (select id from sets where short_name = 'm13'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plummet'),
    (select id from sets where short_name = 'm13'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krenko, Mob Boss'),
    (select id from sets where short_name = 'm13'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm13'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chronomaton'),
    (select id from sets where short_name = 'm13'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = 'm13'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nefarox, Overlord of Grixis'),
    (select id from sets where short_name = 'm13'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Show of Valor'),
    (select id from sets where short_name = 'm13'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magmaquake'),
    (select id from sets where short_name = 'm13'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flames of the Firebrand'),
    (select id from sets where short_name = 'm13'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hellion Crucible'),
    (select id from sets where short_name = 'm13'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Avenger'),
    (select id from sets where short_name = 'm13'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harbor Serpent'),
    (select id from sets where short_name = 'm13'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rewind'),
    (select id from sets where short_name = 'm13'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Fire'),
    (select id from sets where short_name = 'm13'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battle of Wits'),
    (select id from sets where short_name = 'm13'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm13'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Planar Cleansing'),
    (select id from sets where short_name = 'm13'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Divine Favor'),
    (select id from sets where short_name = 'm13'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arbor Elf'),
    (select id from sets where short_name = 'm13'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Firewing Phoenix'),
    (select id from sets where short_name = 'm13'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rancor'),
    (select id from sets where short_name = 'm13'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Courtly Provocateur'),
    (select id from sets where short_name = 'm13'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Odric, Master Tactician'),
    (select id from sets where short_name = 'm13'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Talrand, Sky Summoner'),
    (select id from sets where short_name = 'm13'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glacial Fortress'),
    (select id from sets where short_name = 'm13'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duskmantle Prowler'),
    (select id from sets where short_name = 'm13'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Acidic Slime'),
    (select id from sets where short_name = 'm13'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chandra, the Firebrand'),
    (select id from sets where short_name = 'm13'),
    '123',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cower in Fear'),
    (select id from sets where short_name = 'm13'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Archdruid'),
    (select id from sets where short_name = 'm13'),
    '168',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Attended Knight'),
    (select id from sets where short_name = 'm13'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Battle Jester'),
    (select id from sets where short_name = 'm13'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talrand''s Invocation'),
    (select id from sets where short_name = 'm13'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mutilate'),
    (select id from sets where short_name = 'm13'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disciple of Bolas'),
    (select id from sets where short_name = 'm13'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm13'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm13'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murder'),
    (select id from sets where short_name = 'm13'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm13'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wind Drake'),
    (select id from sets where short_name = 'm13'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Torch Fiend'),
    (select id from sets where short_name = 'm13'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Divine Verdict'),
    (select id from sets where short_name = 'm13'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rummaging Goblin'),
    (select id from sets where short_name = 'm13'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm13'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disentomb'),
    (select id from sets where short_name = 'm13'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akroma''s Memorial'),
    (select id from sets where short_name = 'm13'),
    '200',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thundermaw Hellkite'),
    (select id from sets where short_name = 'm13'),
    '150',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Timberpack Wolf'),
    (select id from sets where short_name = 'm13'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bladetusk Boar'),
    (select id from sets where short_name = 'm13'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'War Priest of Thune'),
    (select id from sets where short_name = 'm13'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fire Elemental'),
    (select id from sets where short_name = 'm13'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Veilborn Ghoul'),
    (select id from sets where short_name = 'm13'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm13'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'm13'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm13'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scroll Thief'),
    (select id from sets where short_name = 'm13'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vedalken Entrancer'),
    (select id from sets where short_name = 'm13'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cathedral of War'),
    (select id from sets where short_name = 'm13'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Griffin Protector'),
    (select id from sets where short_name = 'm13'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Geyser'),
    (select id from sets where short_name = 'm13'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Encrust'),
    (select id from sets where short_name = 'm13'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Servant of Nefarox'),
    (select id from sets where short_name = 'm13'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prey Upon'),
    (select id from sets where short_name = 'm13'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Xathrid Gorgon'),
    (select id from sets where short_name = 'm13'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = 'm13'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boundless Realms'),
    (select id from sets where short_name = 'm13'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spelltwine'),
    (select id from sets where short_name = 'm13'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Craterize'),
    (select id from sets where short_name = 'm13'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Squire'),
    (select id from sets where short_name = 'm13'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Master of the Pearl Trident'),
    (select id from sets where short_name = 'm13'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primal Huntbeast'),
    (select id from sets where short_name = 'm13'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mogg Flunkies'),
    (select id from sets where short_name = 'm13'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Hatchling'),
    (select id from sets where short_name = 'm13'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tormod''s Crypt'),
    (select id from sets where short_name = 'm13'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volcanic Strength'),
    (select id from sets where short_name = 'm13'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Downpour'),
    (select id from sets where short_name = 'm13'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bond Beetle'),
    (select id from sets where short_name = 'm13'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rain of Blades'),
    (select id from sets where short_name = 'm13'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'm13'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sublime Archangel'),
    (select id from sets where short_name = 'm13'),
    '36',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'm13'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glorious Charge'),
    (select id from sets where short_name = 'm13'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Canyon Minotaur'),
    (select id from sets where short_name = 'm13'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reliquary Tower'),
    (select id from sets where short_name = 'm13'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stormtide Leviathan'),
    (select id from sets where short_name = 'm13'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Farseek'),
    (select id from sets where short_name = 'm13'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Invaders'),
    (select id from sets where short_name = 'm13'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Essence Scatter'),
    (select id from sets where short_name = 'm13'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'm13'),
    '31',
    'uncommon'
) 
 on conflict do nothing;
