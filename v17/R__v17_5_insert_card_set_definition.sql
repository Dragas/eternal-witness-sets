insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Archangel Avacyn // Avacyn, the Purifier'),
    (select id from sets where short_name = 'v17'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Delver of Secrets // Insectile Aberration'),
    (select id from sets where short_name = 'v17'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bruna, the Fading Light'),
    (select id from sets where short_name = 'v17'),
    '5a',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bloodline Keeper // Lord of Lineage'),
    (select id from sets where short_name = 'v17'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Huntmaster of the Fells // Ravager of the Fells'),
    (select id from sets where short_name = 'v17'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nissa, Vastwood Seer // Nissa, Sage Animist'),
    (select id from sets where short_name = 'v17'),
    '15',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kytheon, Hero of Akros // Gideon, Battle-Forged'),
    (select id from sets where short_name = 'v17'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chandra, Fire of Kaladesh // Chandra, Roaring Flame'),
    (select id from sets where short_name = 'v17'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Elbrus, the Binding Blade // Withengar Unbound'),
    (select id from sets where short_name = 'v17'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Liliana, Heretical Healer // Liliana, Defiant Necromancer'),
    (select id from sets where short_name = 'v17'),
    '14',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gisela, the Broken Blade'),
    (select id from sets where short_name = 'v17'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Garruk Relentless // Garruk, the Veil-Cursed'),
    (select id from sets where short_name = 'v17'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jace, Vryn''s Prodigy // Jace, Telepath Unbound'),
    (select id from sets where short_name = 'v17'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Arlinn Kord // Arlinn, Embraced by the Moon'),
    (select id from sets where short_name = 'v17'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Arguel''s Blood Fast // Temple of Aclazotz'),
    (select id from sets where short_name = 'v17'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Brisela, Voice of Nightmares'),
    (select id from sets where short_name = 'v17'),
    '5b',
    'mythic'
) 
 on conflict do nothing;
