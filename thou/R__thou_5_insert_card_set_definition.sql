insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Punchcard'),
    (select id from sets where short_name = 'thou'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunscourge Champion'),
    (select id from sets where short_name = 'thou'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Punchcard'),
    (select id from sets where short_name = 'thou'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sinuous Striker'),
    (select id from sets where short_name = 'thou'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snake'),
    (select id from sets where short_name = 'thou'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Champion of Wits'),
    (select id from sets where short_name = 'thou'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Adorned Pouncer'),
    (select id from sets where short_name = 'thou'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Proven Combatant'),
    (select id from sets where short_name = 'thou'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steadfast Sentinel'),
    (select id from sets where short_name = 'thou'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreamstealer'),
    (select id from sets where short_name = 'thou'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insect'),
    (select id from sets where short_name = 'thou'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Resilient Khenra'),
    (select id from sets where short_name = 'thou'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earthshaker Khenra'),
    (select id from sets where short_name = 'thou'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horse'),
    (select id from sets where short_name = 'thou'),
    '10',
    'common'
) 
 on conflict do nothing;
