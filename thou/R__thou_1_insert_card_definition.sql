    insert into mtgcard(name) values ('Punchcard') on conflict do nothing;
    insert into mtgcard(name) values ('Sunscourge Champion') on conflict do nothing;
    insert into mtgcard(name) values ('Sinuous Striker') on conflict do nothing;
    insert into mtgcard(name) values ('Snake') on conflict do nothing;
    insert into mtgcard(name) values ('Champion of Wits') on conflict do nothing;
    insert into mtgcard(name) values ('Adorned Pouncer') on conflict do nothing;
    insert into mtgcard(name) values ('Proven Combatant') on conflict do nothing;
    insert into mtgcard(name) values ('Steadfast Sentinel') on conflict do nothing;
    insert into mtgcard(name) values ('Dreamstealer') on conflict do nothing;
    insert into mtgcard(name) values ('Insect') on conflict do nothing;
    insert into mtgcard(name) values ('Resilient Khenra') on conflict do nothing;
    insert into mtgcard(name) values ('Earthshaker Khenra') on conflict do nothing;
    insert into mtgcard(name) values ('Horse') on conflict do nothing;
