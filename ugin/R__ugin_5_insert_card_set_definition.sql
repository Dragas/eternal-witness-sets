insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Wildcall'),
    (select id from sets where short_name = 'ugin'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jeering Instigator'),
    (select id from sets where short_name = 'ugin'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Formless Nurturing'),
    (select id from sets where short_name = 'ugin'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jeskai Infiltrator'),
    (select id from sets where short_name = 'ugin'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arc Lightning'),
    (select id from sets where short_name = 'ugin'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smite the Monstrous'),
    (select id from sets where short_name = 'ugin'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Watcher of the Roost'),
    (select id from sets where short_name = 'ugin'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reality Shift'),
    (select id from sets where short_name = 'ugin'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Altar of the Brood'),
    (select id from sets where short_name = 'ugin'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fierce Invocation'),
    (select id from sets where short_name = 'ugin'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragonscale Boon'),
    (select id from sets where short_name = 'ugin'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hewed Stone Retainers'),
    (select id from sets where short_name = 'ugin'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arashin War Beast'),
    (select id from sets where short_name = 'ugin'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Briber''s Purse'),
    (select id from sets where short_name = 'ugin'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sultai Emissary'),
    (select id from sets where short_name = 'ugin'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grim Haruspex'),
    (select id from sets where short_name = 'ugin'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Debilitating Injury'),
    (select id from sets where short_name = 'ugin'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic of the Hidden Way'),
    (select id from sets where short_name = 'ugin'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mastery of the Unseen'),
    (select id from sets where short_name = 'ugin'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ugin, the Spirit Dragon'),
    (select id from sets where short_name = 'ugin'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ainok Tracker'),
    (select id from sets where short_name = 'ugin'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghostfire Blade'),
    (select id from sets where short_name = 'ugin'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ugin''s Construct'),
    (select id from sets where short_name = 'ugin'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Write into Being'),
    (select id from sets where short_name = 'ugin'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruthless Ripper'),
    (select id from sets where short_name = 'ugin'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Summons'),
    (select id from sets where short_name = 'ugin'),
    '26',
    'common'
) 
 on conflict do nothing;
