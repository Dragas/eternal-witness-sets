insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tddk'),
    '1',
    'common'
) 
 on conflict do nothing;
