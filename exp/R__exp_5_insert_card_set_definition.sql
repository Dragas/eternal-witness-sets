insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Fire-Lit Thicket'),
    (select id from sets where short_name = 'exp'),
    '29',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Breeding Pool'),
    (select id from sets where short_name = 'exp'),
    '15',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Flooded Grove'),
    (select id from sets where short_name = 'exp'),
    '35',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cinder Glade'),
    (select id from sets where short_name = 'exp'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Strip Mine'),
    (select id from sets where short_name = 'exp'),
    '43',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Misty Rainforest'),
    (select id from sets where short_name = 'exp'),
    '25',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bloodstained Mire'),
    (select id from sets where short_name = 'exp'),
    '18',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Arid Mesa'),
    (select id from sets where short_name = 'exp'),
    '24',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Steam Vents'),
    (select id from sets where short_name = 'exp'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kor Haven'),
    (select id from sets where short_name = 'exp'),
    '41',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Twilight Mire'),
    (select id from sets where short_name = 'exp'),
    '33',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mystic Gate'),
    (select id from sets where short_name = 'exp'),
    '26',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Smoldering Marsh'),
    (select id from sets where short_name = 'exp'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Verdant Catacombs'),
    (select id from sets where short_name = 'exp'),
    '23',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Canopy Vista'),
    (select id from sets where short_name = 'exp'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sacred Foundry'),
    (select id from sets where short_name = 'exp'),
    '14',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fetid Heath'),
    (select id from sets where short_name = 'exp'),
    '31',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blood Crypt'),
    (select id from sets where short_name = 'exp'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hallowed Fountain'),
    (select id from sets where short_name = 'exp'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rugged Prairie'),
    (select id from sets where short_name = 'exp'),
    '34',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wooded Bastion'),
    (select id from sets where short_name = 'exp'),
    '30',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Windswept Heath'),
    (select id from sets where short_name = 'exp'),
    '20',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stomping Ground'),
    (select id from sets where short_name = 'exp'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Flooded Strand'),
    (select id from sets where short_name = 'exp'),
    '16',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mana Confluence'),
    (select id from sets where short_name = 'exp'),
    '42',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scalding Tarn'),
    (select id from sets where short_name = 'exp'),
    '22',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cascade Bluffs'),
    (select id from sets where short_name = 'exp'),
    '32',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Horizon Canopy'),
    (select id from sets where short_name = 'exp'),
    '40',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sunken Hollow'),
    (select id from sets where short_name = 'exp'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tectonic Edge'),
    (select id from sets where short_name = 'exp'),
    '44',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sunken Ruins'),
    (select id from sets where short_name = 'exp'),
    '27',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Graven Cairns'),
    (select id from sets where short_name = 'exp'),
    '28',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Eye of Ugin'),
    (select id from sets where short_name = 'exp'),
    '38',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Watery Grave'),
    (select id from sets where short_name = 'exp'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Temple Garden'),
    (select id from sets where short_name = 'exp'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dust Bowl'),
    (select id from sets where short_name = 'exp'),
    '37',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Overgrown Tomb'),
    (select id from sets where short_name = 'exp'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forbidden Orchard'),
    (select id from sets where short_name = 'exp'),
    '39',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Prairie Stream'),
    (select id from sets where short_name = 'exp'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Polluted Delta'),
    (select id from sets where short_name = 'exp'),
    '17',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wasteland'),
    (select id from sets where short_name = 'exp'),
    '45',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Godless Shrine'),
    (select id from sets where short_name = 'exp'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Marsh Flats'),
    (select id from sets where short_name = 'exp'),
    '21',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ancient Tomb'),
    (select id from sets where short_name = 'exp'),
    '36',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wooded Foothills'),
    (select id from sets where short_name = 'exp'),
    '19',
    'mythic'
) 
 on conflict do nothing;
