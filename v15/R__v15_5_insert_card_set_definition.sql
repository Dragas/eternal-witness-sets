insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Iona, Shield of Emeria'),
    (select id from sets where short_name = 'v15'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jenara, Asura of War'),
    (select id from sets where short_name = 'v15'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Archangel of Strife'),
    (select id from sets where short_name = 'v15'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Platinum Angel'),
    (select id from sets where short_name = 'v15'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Entreat the Angels'),
    (select id from sets where short_name = 'v15'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Avacyn, Angel of Hope'),
    (select id from sets where short_name = 'v15'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Akroma, Angel of Fury'),
    (select id from sets where short_name = 'v15'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Akroma, Angel of Wrath'),
    (select id from sets where short_name = 'v15'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tariel, Reckoner of Souls'),
    (select id from sets where short_name = 'v15'),
    '15',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Exalted Angel'),
    (select id from sets where short_name = 'v15'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Iridescent Angel'),
    (select id from sets where short_name = 'v15'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Baneslayer Angel'),
    (select id from sets where short_name = 'v15'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'v15'),
    '14',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lightning Angel'),
    (select id from sets where short_name = 'v15'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aurelia, the Warleader'),
    (select id from sets where short_name = 'v15'),
    '4',
    'mythic'
) 
 on conflict do nothing;
