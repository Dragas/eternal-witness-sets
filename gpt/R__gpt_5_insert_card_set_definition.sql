insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ghor-Clan Bloodscale'),
    (select id from sets where short_name = 'gpt'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yore-Tiller Nephilim'),
    (select id from sets where short_name = 'gpt'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shadow Lance'),
    (select id from sets where short_name = 'gpt'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smogsteed Rider'),
    (select id from sets where short_name = 'gpt'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sky Swallower'),
    (select id from sets where short_name = 'gpt'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steam Vents'),
    (select id from sets where short_name = 'gpt'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glint-Eye Nephilim'),
    (select id from sets where short_name = 'gpt'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dryad Sophisticate'),
    (select id from sets where short_name = 'gpt'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Torch Drake'),
    (select id from sets where short_name = 'gpt'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wreak Havoc'),
    (select id from sets where short_name = 'gpt'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spelltithe Enforcer'),
    (select id from sets where short_name = 'gpt'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Euthanist'),
    (select id from sets where short_name = 'gpt'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Castigate'),
    (select id from sets where short_name = 'gpt'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cryptwailing'),
    (select id from sets where short_name = 'gpt'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burning-Tree Shaman'),
    (select id from sets where short_name = 'gpt'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tibor and Lumia'),
    (select id from sets where short_name = 'gpt'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wee Dragonauts'),
    (select id from sets where short_name = 'gpt'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dune-Brood Nephilim'),
    (select id from sets where short_name = 'gpt'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Witch-Maw Nephilim'),
    (select id from sets where short_name = 'gpt'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruul Signet'),
    (select id from sets where short_name = 'gpt'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Living Inferno'),
    (select id from sets where short_name = 'gpt'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mourning Thrull'),
    (select id from sets where short_name = 'gpt'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leyline of Lightning'),
    (select id from sets where short_name = 'gpt'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wild Cantor'),
    (select id from sets where short_name = 'gpt'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Souls of the Faultless'),
    (select id from sets where short_name = 'gpt'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Starved Rusalka'),
    (select id from sets where short_name = 'gpt'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ostiary Thrull'),
    (select id from sets where short_name = 'gpt'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harrier Griffin'),
    (select id from sets where short_name = 'gpt'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Primeval Light'),
    (select id from sets where short_name = 'gpt'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seize the Soul'),
    (select id from sets where short_name = 'gpt'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skarrgan Skybreaker'),
    (select id from sets where short_name = 'gpt'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daggerclaw Imp'),
    (select id from sets where short_name = 'gpt'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'To Arms!'),
    (select id from sets where short_name = 'gpt'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fencer''s Magemark'),
    (select id from sets where short_name = 'gpt'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earth Surge'),
    (select id from sets where short_name = 'gpt'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skarrg, the Rage Pits'),
    (select id from sets where short_name = 'gpt'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pillory of the Sleepless'),
    (select id from sets where short_name = 'gpt'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runeboggle'),
    (select id from sets where short_name = 'gpt'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leyline of the Void'),
    (select id from sets where short_name = 'gpt'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nivix, Aerie of the Firemind'),
    (select id from sets where short_name = 'gpt'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Conjurer''s Ban'),
    (select id from sets where short_name = 'gpt'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cremate'),
    (select id from sets where short_name = 'gpt'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necromancer''s Magemark'),
    (select id from sets where short_name = 'gpt'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mortify'),
    (select id from sets where short_name = 'gpt'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shrieking Grotesque'),
    (select id from sets where short_name = 'gpt'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Order of the Stars'),
    (select id from sets where short_name = 'gpt'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghost Warden'),
    (select id from sets where short_name = 'gpt'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Restless Bones'),
    (select id from sets where short_name = 'gpt'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Siege of Towers'),
    (select id from sets where short_name = 'gpt'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Douse in Gloom'),
    (select id from sets where short_name = 'gpt'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crystal Seer'),
    (select id from sets where short_name = 'gpt'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul Turf'),
    (select id from sets where short_name = 'gpt'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moratorium Stone'),
    (select id from sets where short_name = 'gpt'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Belfry Spirit'),
    (select id from sets where short_name = 'gpt'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thunderheads'),
    (select id from sets where short_name = 'gpt'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Invoke the Firemind'),
    (select id from sets where short_name = 'gpt'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Niv-Mizzet, the Firemind'),
    (select id from sets where short_name = 'gpt'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Battering Wurm'),
    (select id from sets where short_name = 'gpt'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyrider Trainee'),
    (select id from sets where short_name = 'gpt'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Culling Sun'),
    (select id from sets where short_name = 'gpt'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skarrgan Firebird'),
    (select id from sets where short_name = 'gpt'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cry of Contrition'),
    (select id from sets where short_name = 'gpt'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Debtors'' Knell'),
    (select id from sets where short_name = 'gpt'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wurmweaver Coil'),
    (select id from sets where short_name = 'gpt'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Izzet Boilerworks'),
    (select id from sets where short_name = 'gpt'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savage Twister'),
    (select id from sets where short_name = 'gpt'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drowned Rusalka'),
    (select id from sets where short_name = 'gpt'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frazzle'),
    (select id from sets where short_name = 'gpt'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gatherer of Graces'),
    (select id from sets where short_name = 'gpt'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skeletal Vampire'),
    (select id from sets where short_name = 'gpt'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Petrahydrox'),
    (select id from sets where short_name = 'gpt'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Chronarch'),
    (select id from sets where short_name = 'gpt'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lionheart Maverick'),
    (select id from sets where short_name = 'gpt'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Absolver Thrull'),
    (select id from sets where short_name = 'gpt'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bioplasm'),
    (select id from sets where short_name = 'gpt'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Caustic Rain'),
    (select id from sets where short_name = 'gpt'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Droning Bureaucrats'),
    (select id from sets where short_name = 'gpt'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cerebral Vortex'),
    (select id from sets where short_name = 'gpt'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quicken'),
    (select id from sets where short_name = 'gpt'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beastmaster''s Magemark'),
    (select id from sets where short_name = 'gpt'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Borborygmos'),
    (select id from sets where short_name = 'gpt'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stratozeppelid'),
    (select id from sets where short_name = 'gpt'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Agent of Masks'),
    (select id from sets where short_name = 'gpt'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stitch in Time'),
    (select id from sets where short_name = 'gpt'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Flectomancer'),
    (select id from sets where short_name = 'gpt'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodscale Prowler'),
    (select id from sets where short_name = 'gpt'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Martyred Rusalka'),
    (select id from sets where short_name = 'gpt'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sanguine Praetor'),
    (select id from sets where short_name = 'gpt'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gelectrode'),
    (select id from sets where short_name = 'gpt'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guardian''s Magemark'),
    (select id from sets where short_name = 'gpt'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vacuumelt'),
    (select id from sets where short_name = 'gpt'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Parallectric Feedback'),
    (select id from sets where short_name = 'gpt'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Solifuge'),
    (select id from sets where short_name = 'gpt'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghostway'),
    (select id from sets where short_name = 'gpt'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Guildmage'),
    (select id from sets where short_name = 'gpt'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orzhova, the Church of Deals'),
    (select id from sets where short_name = 'gpt'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gruul War Plow'),
    (select id from sets where short_name = 'gpt'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scab-Clan Mauler'),
    (select id from sets where short_name = 'gpt'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aetherplasm'),
    (select id from sets where short_name = 'gpt'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Schismotivate'),
    (select id from sets where short_name = 'gpt'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gruul Nodorog'),
    (select id from sets where short_name = 'gpt'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Predatory Focus'),
    (select id from sets where short_name = 'gpt'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel of Despair'),
    (select id from sets where short_name = 'gpt'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Repeal'),
    (select id from sets where short_name = 'gpt'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teysa, Orzhov Scion'),
    (select id from sets where short_name = 'gpt'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scorched Rusalka'),
    (select id from sets where short_name = 'gpt'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ulasht, the Hate Seed'),
    (select id from sets where short_name = 'gpt'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Poisonbelly Ogre'),
    (select id from sets where short_name = 'gpt'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vertigo Spawn'),
    (select id from sets where short_name = 'gpt'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sword of the Paruns'),
    (select id from sets where short_name = 'gpt'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stomping Ground'),
    (select id from sets where short_name = 'gpt'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Petrified Wood-Kin'),
    (select id from sets where short_name = 'gpt'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sinstriker''s Will'),
    (select id from sets where short_name = 'gpt'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vedalken Plotter'),
    (select id from sets where short_name = 'gpt'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tin Street Hooligan'),
    (select id from sets where short_name = 'gpt'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feral Animist'),
    (select id from sets where short_name = 'gpt'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Graven Dominator'),
    (select id from sets where short_name = 'gpt'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gristleback'),
    (select id from sets where short_name = 'gpt'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Withstand'),
    (select id from sets where short_name = 'gpt'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Godless Shrine'),
    (select id from sets where short_name = 'gpt'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blind Hunter'),
    (select id from sets where short_name = 'gpt'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gigadrowse'),
    (select id from sets where short_name = 'gpt'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leap of Flame'),
    (select id from sets where short_name = 'gpt'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyromatics'),
    (select id from sets where short_name = 'gpt'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burning-Tree Bloodscale'),
    (select id from sets where short_name = 'gpt'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rabble-Rouser'),
    (select id from sets where short_name = 'gpt'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Revenant Patriarch'),
    (select id from sets where short_name = 'gpt'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Killer Instinct'),
    (select id from sets where short_name = 'gpt'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exhumer Thrull'),
    (select id from sets where short_name = 'gpt'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Storm Herd'),
    (select id from sets where short_name = 'gpt'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silhana Ledgewalker'),
    (select id from sets where short_name = 'gpt'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul Guildmage'),
    (select id from sets where short_name = 'gpt'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skarrgan Pit-Skulk'),
    (select id from sets where short_name = 'gpt'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plagued Rusalka'),
    (select id from sets where short_name = 'gpt'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ink-Treader Nephilim'),
    (select id from sets where short_name = 'gpt'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mizzium Transreliquat'),
    (select id from sets where short_name = 'gpt'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leyline of Singularity'),
    (select id from sets where short_name = 'gpt'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Streetbreaker Wurm'),
    (select id from sets where short_name = 'gpt'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shattering Spree'),
    (select id from sets where short_name = 'gpt'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghor-Clan Savage'),
    (select id from sets where short_name = 'gpt'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghost Council of Orzhova'),
    (select id from sets where short_name = 'gpt'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rumbling Slum'),
    (select id from sets where short_name = 'gpt'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benediction of Moons'),
    (select id from sets where short_name = 'gpt'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infiltrator''s Magemark'),
    (select id from sets where short_name = 'gpt'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Train of Thought'),
    (select id from sets where short_name = 'gpt'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Djinn Illuminatus'),
    (select id from sets where short_name = 'gpt'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Izzet Guildmage'),
    (select id from sets where short_name = 'gpt'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wildsize'),
    (select id from sets where short_name = 'gpt'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hatching Plans'),
    (select id from sets where short_name = 'gpt'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Electrolyze'),
    (select id from sets where short_name = 'gpt'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leyline of the Meek'),
    (select id from sets where short_name = 'gpt'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Pontiff'),
    (select id from sets where short_name = 'gpt'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hissing Miasma'),
    (select id from sets where short_name = 'gpt'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mimeofacture'),
    (select id from sets where short_name = 'gpt'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ogre Savant'),
    (select id from sets where short_name = 'gpt'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leyline of Lifeforce'),
    (select id from sets where short_name = 'gpt'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruul Scrapper'),
    (select id from sets where short_name = 'gpt'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Signet'),
    (select id from sets where short_name = 'gpt'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crash Landing'),
    (select id from sets where short_name = 'gpt'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steamcore Weird'),
    (select id from sets where short_name = 'gpt'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orzhov Basilica'),
    (select id from sets where short_name = 'gpt'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orzhov Signet'),
    (select id from sets where short_name = 'gpt'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silhana Starfletcher'),
    (select id from sets where short_name = 'gpt'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hypervolt Grasp'),
    (select id from sets where short_name = 'gpt'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abyssal Nocturnus'),
    (select id from sets where short_name = 'gpt'),
    '43',
    'rare'
) 
 on conflict do nothing;
