    insert into mtgcard(name) values ('Nahiri, the Lithomancer') on conflict do nothing;
    insert into mtgcard(name) values ('Ob Nixilis of the Black Oath') on conflict do nothing;
    insert into mtgcard(name) values ('Teferi, Temporal Archmage') on conflict do nothing;
    insert into mtgcard(name) values ('Freyalise, Llanowar''s Fury') on conflict do nothing;
    insert into mtgcard(name) values ('Daretti, Scrap Savant') on conflict do nothing;
