insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Nahiri, the Lithomancer'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nahiri, the Lithomancer'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nahiri, the Lithomancer'),
        (select types.id from types where name = 'Nahiri')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ob Nixilis of the Black Oath'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ob Nixilis of the Black Oath'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ob Nixilis of the Black Oath'),
        (select types.id from types where name = 'Nixilis')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Teferi, Temporal Archmage'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Teferi, Temporal Archmage'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Teferi, Temporal Archmage'),
        (select types.id from types where name = 'Teferi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Freyalise, Llanowar''s Fury'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Freyalise, Llanowar''s Fury'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Freyalise, Llanowar''s Fury'),
        (select types.id from types where name = 'Freyalise')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Daretti, Scrap Savant'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Daretti, Scrap Savant'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Daretti, Scrap Savant'),
        (select types.id from types where name = 'Daretti')
    ) 
 on conflict do nothing;
