insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Nahiri, the Lithomancer'),
    (select id from sets where short_name = 'oc14'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis of the Black Oath'),
    (select id from sets where short_name = 'oc14'),
    '27',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Teferi, Temporal Archmage'),
    (select id from sets where short_name = 'oc14'),
    '19',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Freyalise, Llanowar''s Fury'),
    (select id from sets where short_name = 'oc14'),
    '43',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Daretti, Scrap Savant'),
    (select id from sets where short_name = 'oc14'),
    '33',
    'mythic'
) 
 on conflict do nothing;
