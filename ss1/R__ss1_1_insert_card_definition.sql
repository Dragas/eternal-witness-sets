    insert into mtgcard(name) values ('Gifts Ungiven') on conflict do nothing;
    insert into mtgcard(name) values ('Counterspell') on conflict do nothing;
    insert into mtgcard(name) values ('Brainstorm') on conflict do nothing;
    insert into mtgcard(name) values ('Blue Elemental Blast') on conflict do nothing;
    insert into mtgcard(name) values ('Mystical Tutor') on conflict do nothing;
    insert into mtgcard(name) values ('Negate') on conflict do nothing;
    insert into mtgcard(name) values ('Jace Beleren') on conflict do nothing;
    insert into mtgcard(name) values ('Threads of Disloyalty') on conflict do nothing;
