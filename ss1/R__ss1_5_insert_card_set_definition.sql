insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Gifts Ungiven'),
    (select id from sets where short_name = 'ss1'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'ss1'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'ss1'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blue Elemental Blast'),
    (select id from sets where short_name = 'ss1'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystical Tutor'),
    (select id from sets where short_name = 'ss1'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'ss1'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jace Beleren'),
    (select id from sets where short_name = 'ss1'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Threads of Disloyalty'),
    (select id from sets where short_name = 'ss1'),
    '8',
    'rare'
) 
 on conflict do nothing;
