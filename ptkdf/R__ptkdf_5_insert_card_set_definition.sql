insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'ptkdf'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Fodder'),
    (select id from sets where short_name = 'ptkdf'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foe-Razer Regent'),
    (select id from sets where short_name = 'ptkdf'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonlord''s Servant'),
    (select id from sets where short_name = 'ptkdf'),
    '138',
    'uncommon'
) 
 on conflict do nothing;
