    insert into mtgcard(name) values ('Evolving Wilds') on conflict do nothing;
    insert into mtgcard(name) values ('Dragon Fodder') on conflict do nothing;
    insert into mtgcard(name) values ('Foe-Razer Regent') on conflict do nothing;
    insert into mtgcard(name) values ('Dragonlord''s Servant') on conflict do nothing;
