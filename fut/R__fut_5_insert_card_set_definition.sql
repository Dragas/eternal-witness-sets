insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Steamflogger Boss'),
    (select id from sets where short_name = 'fut'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Edge of Autumn'),
    (select id from sets where short_name = 'fut'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magus of the Vineyard'),
    (select id from sets where short_name = 'fut'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emblem of the Warmind'),
    (select id from sets where short_name = 'fut'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llanowar Reborn'),
    (select id from sets where short_name = 'fut'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unblinking Bleb'),
    (select id from sets where short_name = 'fut'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Second Wind'),
    (select id from sets where short_name = 'fut'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wrap in Vigor'),
    (select id from sets where short_name = 'fut'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spin into Myth'),
    (select id from sets where short_name = 'fut'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fleshwrither'),
    (select id from sets where short_name = 'fut'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pooling Venom'),
    (select id from sets where short_name = 'fut'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tarox Bladewing'),
    (select id from sets where short_name = 'fut'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sprout Swarm'),
    (select id from sets where short_name = 'fut'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Homing Sliver'),
    (select id from sets where short_name = 'fut'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oblivion Crown'),
    (select id from sets where short_name = 'fut'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Venser, Shaper Savant'),
    (select id from sets where short_name = 'fut'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grave Scrabbler'),
    (select id from sets where short_name = 'fut'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magus of the Abyss'),
    (select id from sets where short_name = 'fut'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whip-Spine Drake'),
    (select id from sets where short_name = 'fut'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Petrified Plating'),
    (select id from sets where short_name = 'fut'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nihilith'),
    (select id from sets where short_name = 'fut'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barren Glory'),
    (select id from sets where short_name = 'fut'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Putrid Cyclops'),
    (select id from sets where short_name = 'fut'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boldwyr Intimidator'),
    (select id from sets where short_name = 'fut'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'New Benalia'),
    (select id from sets where short_name = 'fut'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cutthroat il-Dal'),
    (select id from sets where short_name = 'fut'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baru, Fist of Krosa'),
    (select id from sets where short_name = 'fut'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm Entity'),
    (select id from sets where short_name = 'fut'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Augur il-Vec'),
    (select id from sets where short_name = 'fut'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Augur'),
    (select id from sets where short_name = 'fut'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dust of Moments'),
    (select id from sets where short_name = 'fut'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jhoira of the Ghitu'),
    (select id from sets where short_name = 'fut'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seht''s Tiger'),
    (select id from sets where short_name = 'fut'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mass of Ghouls'),
    (select id from sets where short_name = 'fut'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magus of the Moat'),
    (select id from sets where short_name = 'fut'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spellwild Ouphe'),
    (select id from sets where short_name = 'fut'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ramosian Revivalist'),
    (select id from sets where short_name = 'fut'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bitter Ordeal'),
    (select id from sets where short_name = 'fut'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel of Salvation'),
    (select id from sets where short_name = 'fut'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Festering March'),
    (select id from sets where short_name = 'fut'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daybreak Coronet'),
    (select id from sets where short_name = 'fut'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grinning Ignus'),
    (select id from sets where short_name = 'fut'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molten Disaster'),
    (select id from sets where short_name = 'fut'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Korlash, Heir to Blackblade'),
    (select id from sets where short_name = 'fut'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quagnoth'),
    (select id from sets where short_name = 'fut'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Riddle of Lightning'),
    (select id from sets where short_name = 'fut'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ichor Slick'),
    (select id from sets where short_name = 'fut'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Veilstone Amulet'),
    (select id from sets where short_name = 'fut'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keldon Megaliths'),
    (select id from sets where short_name = 'fut'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Henchfiend of Ukor'),
    (select id from sets where short_name = 'fut'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Imperiosaur'),
    (select id from sets where short_name = 'fut'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nix'),
    (select id from sets where short_name = 'fut'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Muraganda Petroglyphs'),
    (select id from sets where short_name = 'fut'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slaughter Pact'),
    (select id from sets where short_name = 'fut'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bogardan Lancer'),
    (select id from sets where short_name = 'fut'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyromancer''s Swath'),
    (select id from sets where short_name = 'fut'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Judge Unworthy'),
    (select id from sets where short_name = 'fut'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcanum Wings'),
    (select id from sets where short_name = 'fut'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shapeshifter''s Marrow'),
    (select id from sets where short_name = 'fut'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vedalken Aethermage'),
    (select id from sets where short_name = 'fut'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skizzik Surger'),
    (select id from sets where short_name = 'fut'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arc Blade'),
    (select id from sets where short_name = 'fut'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lost Hours'),
    (select id from sets where short_name = 'fut'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skirk Ridge Exhumer'),
    (select id from sets where short_name = 'fut'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Logic Knot'),
    (select id from sets where short_name = 'fut'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Patrician''s Scorn'),
    (select id from sets where short_name = 'fut'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nacatl War-Pride'),
    (select id from sets where short_name = 'fut'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bridge from Below'),
    (select id from sets where short_name = 'fut'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quiet Disrepair'),
    (select id from sets where short_name = 'fut'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Centaur Omenreader'),
    (select id from sets where short_name = 'fut'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aven Mindcensor'),
    (select id from sets where short_name = 'fut'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ravaging Riftwurm'),
    (select id from sets where short_name = 'fut'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Infiltrator il-Kor'),
    (select id from sets where short_name = 'fut'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stronghold Rats'),
    (select id from sets where short_name = 'fut'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Witch''s Mist'),
    (select id from sets where short_name = 'fut'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Virulent Sliver'),
    (select id from sets where short_name = 'fut'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rift Elemental'),
    (select id from sets where short_name = 'fut'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Street Wraith'),
    (select id from sets where short_name = 'fut'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cryptic Annelid'),
    (select id from sets where short_name = 'fut'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blind Phantasm'),
    (select id from sets where short_name = 'fut'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bound in Silence'),
    (select id from sets where short_name = 'fut'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soultether Golem'),
    (select id from sets where short_name = 'fut'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magus of the Future'),
    (select id from sets where short_name = 'fut'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloudseeder'),
    (select id from sets where short_name = 'fut'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Sand-Mage'),
    (select id from sets where short_name = 'fut'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bonded Fetch'),
    (select id from sets where short_name = 'fut'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lumithread Field'),
    (select id from sets where short_name = 'fut'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sarcomite Myr'),
    (select id from sets where short_name = 'fut'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Take Possession'),
    (select id from sets where short_name = 'fut'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thornweald Archer'),
    (select id from sets where short_name = 'fut'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Epochrasite'),
    (select id from sets where short_name = 'fut'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Linessa, Zephyr Mage'),
    (select id from sets where short_name = 'fut'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spellweaver Volute'),
    (select id from sets where short_name = 'fut'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coalition Relic'),
    (select id from sets where short_name = 'fut'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oriss, Samite Guardian'),
    (select id from sets where short_name = 'fut'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phosphorescent Feast'),
    (select id from sets where short_name = 'fut'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Samite Censer-Bearer'),
    (select id from sets where short_name = 'fut'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nimbus Maze'),
    (select id from sets where short_name = 'fut'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rites of Flourishing'),
    (select id from sets where short_name = 'fut'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Graven Cairns'),
    (select id from sets where short_name = 'fut'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fomori Nomad'),
    (select id from sets where short_name = 'fut'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saltskitter'),
    (select id from sets where short_name = 'fut'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lost Auramancers'),
    (select id from sets where short_name = 'fut'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Haze of Rage'),
    (select id from sets where short_name = 'fut'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Riftsweeper'),
    (select id from sets where short_name = 'fut'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight of Sursi'),
    (select id from sets where short_name = 'fut'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunderblade Charge'),
    (select id from sets where short_name = 'fut'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nessian Courser'),
    (select id from sets where short_name = 'fut'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gathan Raiders'),
    (select id from sets where short_name = 'fut'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sliver Legion'),
    (select id from sets where short_name = 'fut'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tombstalker'),
    (select id from sets where short_name = 'fut'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death Rattle'),
    (select id from sets where short_name = 'fut'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dryad Arbor'),
    (select id from sets where short_name = 'fut'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'River of Tears'),
    (select id from sets where short_name = 'fut'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reality Strobe'),
    (select id from sets where short_name = 'fut'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pact of the Titan'),
    (select id from sets where short_name = 'fut'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dakmor Salvage'),
    (select id from sets where short_name = 'fut'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sparkspitter'),
    (select id from sets where short_name = 'fut'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marshaling Cry'),
    (select id from sets where short_name = 'fut'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Speculation'),
    (select id from sets where short_name = 'fut'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fatal Attraction'),
    (select id from sets where short_name = 'fut'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snake Cult Initiation'),
    (select id from sets where short_name = 'fut'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Augur of Skulls'),
    (select id from sets where short_name = 'fut'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Summoner''s Pact'),
    (select id from sets where short_name = 'fut'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goldmeadow Lookout'),
    (select id from sets where short_name = 'fut'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Djinn'),
    (select id from sets where short_name = 'fut'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heartwood Storyteller'),
    (select id from sets where short_name = 'fut'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghostfire'),
    (select id from sets where short_name = 'fut'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tolaria West'),
    (select id from sets where short_name = 'fut'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Char-Rumbler'),
    (select id from sets where short_name = 'fut'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llanowar Mentor'),
    (select id from sets where short_name = 'fut'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sliversmith'),
    (select id from sets where short_name = 'fut'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Force of Savagery'),
    (select id from sets where short_name = 'fut'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deepcavern Imp'),
    (select id from sets where short_name = 'fut'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tarmogoyf'),
    (select id from sets where short_name = 'fut'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloud Key'),
    (select id from sets where short_name = 'fut'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mesmeric Sliver'),
    (select id from sets where short_name = 'fut'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodshot Trainee'),
    (select id from sets where short_name = 'fut'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Utopia Mycon'),
    (select id from sets where short_name = 'fut'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Imperial Mask'),
    (select id from sets where short_name = 'fut'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Intervention Pact'),
    (select id from sets where short_name = 'fut'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leaden Fists'),
    (select id from sets where short_name = 'fut'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scourge of Kher Ridges'),
    (select id from sets where short_name = 'fut'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Llanowar Empath'),
    (select id from sets where short_name = 'fut'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magus of the Moon'),
    (select id from sets where short_name = 'fut'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foresee'),
    (select id from sets where short_name = 'fut'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zoetic Cavern'),
    (select id from sets where short_name = 'fut'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aven Augur'),
    (select id from sets where short_name = 'fut'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chronomantic Escape'),
    (select id from sets where short_name = 'fut'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Emberwilde Augur'),
    (select id from sets where short_name = 'fut'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pact of Negation'),
    (select id from sets where short_name = 'fut'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lucent Liminid'),
    (select id from sets where short_name = 'fut'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sporoloth Ancient'),
    (select id from sets where short_name = 'fut'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shah of Naar Isle'),
    (select id from sets where short_name = 'fut'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kavu Primarch'),
    (select id from sets where short_name = 'fut'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akroma''s Memorial'),
    (select id from sets where short_name = 'fut'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gibbering Descent'),
    (select id from sets where short_name = 'fut'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Horizon Canopy'),
    (select id from sets where short_name = 'fut'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Minions'' Murmurs'),
    (select id from sets where short_name = 'fut'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frenzy Sliver'),
    (select id from sets where short_name = 'fut'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gift of Granite'),
    (select id from sets where short_name = 'fut'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lymph Sliver'),
    (select id from sets where short_name = 'fut'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whetwheel'),
    (select id from sets where short_name = 'fut'),
    '168',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grove of the Burnwillows'),
    (select id from sets where short_name = 'fut'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grave Peril'),
    (select id from sets where short_name = 'fut'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scout''s Warning'),
    (select id from sets where short_name = 'fut'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shimian Specter'),
    (select id from sets where short_name = 'fut'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darksteel Garrison'),
    (select id from sets where short_name = 'fut'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Delay'),
    (select id from sets where short_name = 'fut'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sword of the Meek'),
    (select id from sets where short_name = 'fut'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spirit en-Dal'),
    (select id from sets where short_name = 'fut'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glittering Wish'),
    (select id from sets where short_name = 'fut'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mistmeadow Skulk'),
    (select id from sets where short_name = 'fut'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cyclical Evolution'),
    (select id from sets where short_name = 'fut'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flowstone Embrace'),
    (select id from sets where short_name = 'fut'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blade of the Sixth Pride'),
    (select id from sets where short_name = 'fut'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Even the Odds'),
    (select id from sets where short_name = 'fut'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Narcomoeba'),
    (select id from sets where short_name = 'fut'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Venser''s Diffusion'),
    (select id from sets where short_name = 'fut'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yixlid Jailer'),
    (select id from sets where short_name = 'fut'),
    '93',
    'uncommon'
) 
 on conflict do nothing;
