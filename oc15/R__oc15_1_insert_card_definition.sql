    insert into mtgcard(name) values ('Ezuri, Claw of Progress') on conflict do nothing;
    insert into mtgcard(name) values ('Daxos the Returned') on conflict do nothing;
    insert into mtgcard(name) values ('Mizzix of the Izmagnus') on conflict do nothing;
    insert into mtgcard(name) values ('Meren of Clan Nel Toth') on conflict do nothing;
    insert into mtgcard(name) values ('Kalemne, Disciple of Iroas') on conflict do nothing;
