insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ezuri, Claw of Progress'),
    (select id from sets where short_name = 'oc15'),
    '44',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Daxos the Returned'),
    (select id from sets where short_name = 'oc15'),
    '43',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mizzix of the Izmagnus'),
    (select id from sets where short_name = 'oc15'),
    '50',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Meren of Clan Nel Toth'),
    (select id from sets where short_name = 'oc15'),
    '49',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kalemne, Disciple of Iroas'),
    (select id from sets where short_name = 'oc15'),
    '45',
    'mythic'
) 
 on conflict do nothing;
