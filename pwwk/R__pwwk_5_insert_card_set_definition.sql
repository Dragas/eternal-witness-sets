insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Hada Freeblade'),
    (select id from sets where short_name = 'pwwk'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Comet Storm'),
    (select id from sets where short_name = 'pwwk'),
    '76',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kalastria Highborn'),
    (select id from sets where short_name = 'pwwk'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Joraga Warcaller'),
    (select id from sets where short_name = 'pwwk'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruthless Cullblade'),
    (select id from sets where short_name = 'pwwk'),
    '65*',
    'common'
) ,
(
    (select id from mtgcard where name = 'Celestial Colonnade'),
    (select id from sets where short_name = 'pwwk'),
    '133',
    'rare'
) 
 on conflict do nothing;
