    insert into mtgcard(name) values ('Hada Freeblade') on conflict do nothing;
    insert into mtgcard(name) values ('Comet Storm') on conflict do nothing;
    insert into mtgcard(name) values ('Kalastria Highborn') on conflict do nothing;
    insert into mtgcard(name) values ('Joraga Warcaller') on conflict do nothing;
    insert into mtgcard(name) values ('Ruthless Cullblade') on conflict do nothing;
    insert into mtgcard(name) values ('Celestial Colonnade') on conflict do nothing;
