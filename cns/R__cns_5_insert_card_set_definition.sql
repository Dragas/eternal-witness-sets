insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Selvala''s Enforcer'),
    (select id from sets where short_name = 'cns'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Edric, Spymaster of Trest'),
    (select id from sets where short_name = 'cns'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pernicious Deed'),
    (select id from sets where short_name = 'cns'),
    '191',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Typhoid Rats'),
    (select id from sets where short_name = 'cns'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Assassinate'),
    (select id from sets where short_name = 'cns'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flowstone Blade'),
    (select id from sets where short_name = 'cns'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power of Fire'),
    (select id from sets where short_name = 'cns'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howling Wolf'),
    (select id from sets where short_name = 'cns'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Fallout'),
    (select id from sets where short_name = 'cns'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Runed Servitor'),
    (select id from sets where short_name = 'cns'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brimstone Volley'),
    (select id from sets where short_name = 'cns'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathforge Shaman'),
    (select id from sets where short_name = 'cns'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shoreline Ranger'),
    (select id from sets where short_name = 'cns'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Secrets of Paradise'),
    (select id from sets where short_name = 'cns'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enclave Elite'),
    (select id from sets where short_name = 'cns'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Immediate Action'),
    (select id from sets where short_name = 'cns'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brago, King Eternal'),
    (select id from sets where short_name = 'cns'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Muzzio, Visionary Architect'),
    (select id from sets where short_name = 'cns'),
    '23',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vedalken Orrery'),
    (select id from sets where short_name = 'cns'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Council''s Judgment'),
    (select id from sets where short_name = 'cns'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dream Fracture'),
    (select id from sets where short_name = 'cns'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grenzo''s Rebuttal'),
    (select id from sets where short_name = 'cns'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rousing of Souls'),
    (select id from sets where short_name = 'cns'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Scholar'),
    (select id from sets where short_name = 'cns'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Provoke'),
    (select id from sets where short_name = 'cns'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iterative Analysis'),
    (select id from sets where short_name = 'cns'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magister of Worth'),
    (select id from sets where short_name = 'cns'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smallpox'),
    (select id from sets where short_name = 'cns'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hunger of the Howlpack'),
    (select id from sets where short_name = 'cns'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bite of the Black Rose'),
    (select id from sets where short_name = 'cns'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Squirrel Nest'),
    (select id from sets where short_name = 'cns'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Intangible Virtue'),
    (select id from sets where short_name = 'cns'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warmonger''s Chariot'),
    (select id from sets where short_name = 'cns'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Breakthrough'),
    (select id from sets where short_name = 'cns'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jetting Glasskite'),
    (select id from sets where short_name = 'cns'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plagued Rusalka'),
    (select id from sets where short_name = 'cns'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cogwork Tracker'),
    (select id from sets where short_name = 'cns'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Exploration'),
    (select id from sets where short_name = 'cns'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sentinel Dispatch'),
    (select id from sets where short_name = 'cns'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Favorable Winds'),
    (select id from sets where short_name = 'cns'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Apex Hawks'),
    (select id from sets where short_name = 'cns'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vow of Duty'),
    (select id from sets where short_name = 'cns'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Twisted Abomination'),
    (select id from sets where short_name = 'cns'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Geyser'),
    (select id from sets where short_name = 'cns'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Cannonade'),
    (select id from sets where short_name = 'cns'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fires of Yavimaya'),
    (select id from sets where short_name = 'cns'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Council Guardian'),
    (select id from sets where short_name = 'cns'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Decimate'),
    (select id from sets where short_name = 'cns'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Sunstriker'),
    (select id from sets where short_name = 'cns'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pristine Angel'),
    (select id from sets where short_name = 'cns'),
    '78',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wind Dancer'),
    (select id from sets where short_name = 'cns'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dack Fayden'),
    (select id from sets where short_name = 'cns'),
    '42',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Silent Arbiter'),
    (select id from sets where short_name = 'cns'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mortify'),
    (select id from sets where short_name = 'cns'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selvala''s Charge'),
    (select id from sets where short_name = 'cns'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deal Broker'),
    (select id from sets where short_name = 'cns'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barbed Shocker'),
    (select id from sets where short_name = 'cns'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cogwork Librarian'),
    (select id from sets where short_name = 'cns'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boldwyr Intimidator'),
    (select id from sets where short_name = 'cns'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stronghold Discipline'),
    (select id from sets where short_name = 'cns'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heartless Hidetsugu'),
    (select id from sets where short_name = 'cns'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Misdirection'),
    (select id from sets where short_name = 'cns'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grenzo, Dungeon Warden'),
    (select id from sets where short_name = 'cns'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moment of Heroism'),
    (select id from sets where short_name = 'cns'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stasis Cell'),
    (select id from sets where short_name = 'cns'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lead the Stampede'),
    (select id from sets where short_name = 'cns'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Air Servant'),
    (select id from sets where short_name = 'cns'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cinder Wall'),
    (select id from sets where short_name = 'cns'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Realm Seekers'),
    (select id from sets where short_name = 'cns'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathreap Ritual'),
    (select id from sets where short_name = 'cns'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plea for Power'),
    (select id from sets where short_name = 'cns'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nature''s Claim'),
    (select id from sets where short_name = 'cns'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tyrant''s Choice'),
    (select id from sets where short_name = 'cns'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pillarfield Ox'),
    (select id from sets where short_name = 'cns'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spontaneous Combustion'),
    (select id from sets where short_name = 'cns'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampire Hexmage'),
    (select id from sets where short_name = 'cns'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pitchburn Devils'),
    (select id from sets where short_name = 'cns'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marchesa''s Emissary'),
    (select id from sets where short_name = 'cns'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pride Guardian'),
    (select id from sets where short_name = 'cns'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Muzzio''s Preparations'),
    (select id from sets where short_name = 'cns'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolfbriar Elemental'),
    (select id from sets where short_name = 'cns'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fact or Fiction'),
    (select id from sets where short_name = 'cns'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stifle'),
    (select id from sets where short_name = 'cns'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reign of the Pit'),
    (select id from sets where short_name = 'cns'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wakestone Gargoyle'),
    (select id from sets where short_name = 'cns'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Doppelganger'),
    (select id from sets where short_name = 'cns'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sky Spirit'),
    (select id from sets where short_name = 'cns'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cogwork Spy'),
    (select id from sets where short_name = 'cns'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Peace Strider'),
    (select id from sets where short_name = 'cns'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quicksand'),
    (select id from sets where short_name = 'cns'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rout'),
    (select id from sets where short_name = 'cns'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lizard Warrior'),
    (select id from sets where short_name = 'cns'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Specter'),
    (select id from sets where short_name = 'cns'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Morkrut Banshee'),
    (select id from sets where short_name = 'cns'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guardian Zendikon'),
    (select id from sets where short_name = 'cns'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Secret Summoning'),
    (select id from sets where short_name = 'cns'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Galvanic Juggernaut'),
    (select id from sets where short_name = 'cns'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Aberration'),
    (select id from sets where short_name = 'cns'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'cns'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Worldknit'),
    (select id from sets where short_name = 'cns'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Turn the Tide'),
    (select id from sets where short_name = 'cns'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unhallowed Pact'),
    (select id from sets where short_name = 'cns'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cogwork Grinder'),
    (select id from sets where short_name = 'cns'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magus of the Mirror'),
    (select id from sets where short_name = 'cns'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elephant Guide'),
    (select id from sets where short_name = 'cns'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Minamo Scrollkeeper'),
    (select id from sets where short_name = 'cns'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireshrieker'),
    (select id from sets where short_name = 'cns'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Split Decision'),
    (select id from sets where short_name = 'cns'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Backup Plan'),
    (select id from sets where short_name = 'cns'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relic Crush'),
    (select id from sets where short_name = 'cns'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Charging Rhino'),
    (select id from sets where short_name = 'cns'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plummet'),
    (select id from sets where short_name = 'cns'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unexpected Potential'),
    (select id from sets where short_name = 'cns'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gnarlid Pack'),
    (select id from sets where short_name = 'cns'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selvala, Explorer Returned'),
    (select id from sets where short_name = 'cns'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woodvine Elemental'),
    (select id from sets where short_name = 'cns'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necromantic Thirst'),
    (select id from sets where short_name = 'cns'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Chant'),
    (select id from sets where short_name = 'cns'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reflecting Pool'),
    (select id from sets where short_name = 'cns'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wood Sage'),
    (select id from sets where short_name = 'cns'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wrap in Vigor'),
    (select id from sets where short_name = 'cns'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Courier Hawk'),
    (select id from sets where short_name = 'cns'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Searcher'),
    (select id from sets where short_name = 'cns'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tragic Slip'),
    (select id from sets where short_name = 'cns'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spiritmonger'),
    (select id from sets where short_name = 'cns'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pelakka Wurm'),
    (select id from sets where short_name = 'cns'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lore Seeker'),
    (select id from sets where short_name = 'cns'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grenzo''s Cutthroat'),
    (select id from sets where short_name = 'cns'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brago''s Favor'),
    (select id from sets where short_name = 'cns'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ignition Team'),
    (select id from sets where short_name = 'cns'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aether Tradewinds'),
    (select id from sets where short_name = 'cns'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scourge of the Throne'),
    (select id from sets where short_name = 'cns'),
    '35',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Custodi Soulbinders'),
    (select id from sets where short_name = 'cns'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zombie Goliath'),
    (select id from sets where short_name = 'cns'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Canal Dredger'),
    (select id from sets where short_name = 'cns'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quag Vampires'),
    (select id from sets where short_name = 'cns'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trumpet Blast'),
    (select id from sets where short_name = 'cns'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Custodi Squire'),
    (select id from sets where short_name = 'cns'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unquestioned Authority'),
    (select id from sets where short_name = 'cns'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Copperhorn Scout'),
    (select id from sets where short_name = 'cns'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Uncontrollable Anger'),
    (select id from sets where short_name = 'cns'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heckling Fiends'),
    (select id from sets where short_name = 'cns'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Academy Elite'),
    (select id from sets where short_name = 'cns'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treasonous Ogre'),
    (select id from sets where short_name = 'cns'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coercive Portal'),
    (select id from sets where short_name = 'cns'),
    '56',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wakedancer'),
    (select id from sets where short_name = 'cns'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Double Stroke'),
    (select id from sets where short_name = 'cns'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sporecap Spider'),
    (select id from sets where short_name = 'cns'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drakestown Forgotten'),
    (select id from sets where short_name = 'cns'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Valor Made Real'),
    (select id from sets where short_name = 'cns'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrap in Flames'),
    (select id from sets where short_name = 'cns'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Play'),
    (select id from sets where short_name = 'cns'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scaled Wurm'),
    (select id from sets where short_name = 'cns'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'cns'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Altar''s Reap'),
    (select id from sets where short_name = 'cns'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chartooth Cougar'),
    (select id from sets where short_name = 'cns'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brago''s Representative'),
    (select id from sets where short_name = 'cns'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Victimize'),
    (select id from sets where short_name = 'cns'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stave Off'),
    (select id from sets where short_name = 'cns'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Altar of Dementia'),
    (select id from sets where short_name = 'cns'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Basandra, Battle Seraph'),
    (select id from sets where short_name = 'cns'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Traveler''s Cloak'),
    (select id from sets where short_name = 'cns'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hydra Omnivore'),
    (select id from sets where short_name = 'cns'),
    '169',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Elder'),
    (select id from sets where short_name = 'cns'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Agent of Acquisitions'),
    (select id from sets where short_name = 'cns'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dack''s Duplicate'),
    (select id from sets where short_name = 'cns'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Doomed Traveler'),
    (select id from sets where short_name = 'cns'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glimmerpoint Stag'),
    (select id from sets where short_name = 'cns'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spectral Searchlight'),
    (select id from sets where short_name = 'cns'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Predator''s Howl'),
    (select id from sets where short_name = 'cns'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skitter of Lizards'),
    (select id from sets where short_name = 'cns'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flaring Flame-Kin'),
    (select id from sets where short_name = 'cns'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grudge Keeper'),
    (select id from sets where short_name = 'cns'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crookclaw Transmuter'),
    (select id from sets where short_name = 'cns'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ill-Gotten Gains'),
    (select id from sets where short_name = 'cns'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Echoing Courage'),
    (select id from sets where short_name = 'cns'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sulfuric Vortex'),
    (select id from sets where short_name = 'cns'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Compulsive Research'),
    (select id from sets where short_name = 'cns'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phage the Untouchable'),
    (select id from sets where short_name = 'cns'),
    '120',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Reya Dawnbringer'),
    (select id from sets where short_name = 'cns'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gamekeeper'),
    (select id from sets where short_name = 'cns'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Respite'),
    (select id from sets where short_name = 'cns'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Syphon Soul'),
    (select id from sets where short_name = 'cns'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Noble Templar'),
    (select id from sets where short_name = 'cns'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Extract from Darkness'),
    (select id from sets where short_name = 'cns'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enraged Revolutionary'),
    (select id from sets where short_name = 'cns'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lurking Automaton'),
    (select id from sets where short_name = 'cns'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whispergear Sneak'),
    (select id from sets where short_name = 'cns'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Torch Fiend'),
    (select id from sets where short_name = 'cns'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marchesa, the Black Rose'),
    (select id from sets where short_name = 'cns'),
    '49',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mirari''s Wake'),
    (select id from sets where short_name = 'cns'),
    '189',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Plated Seastrider'),
    (select id from sets where short_name = 'cns'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silverchase Fox'),
    (select id from sets where short_name = 'cns'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marchesa''s Infiltrator'),
    (select id from sets where short_name = 'cns'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deathrender'),
    (select id from sets where short_name = 'cns'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Paliano, the High City'),
    (select id from sets where short_name = 'cns'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grixis Illusionist'),
    (select id from sets where short_name = 'cns'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terastodon'),
    (select id from sets where short_name = 'cns'),
    '181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vent Sentinel'),
    (select id from sets where short_name = 'cns'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skeletal Scrying'),
    (select id from sets where short_name = 'cns'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Infectious Horror'),
    (select id from sets where short_name = 'cns'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soulcatcher'),
    (select id from sets where short_name = 'cns'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reckless Spite'),
    (select id from sets where short_name = 'cns'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mirrodin''s Core'),
    (select id from sets where short_name = 'cns'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Advantageous Proclamation'),
    (select id from sets where short_name = 'cns'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Explorer''s Scope'),
    (select id from sets where short_name = 'cns'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flamewright'),
    (select id from sets where short_name = 'cns'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Screaming Seahawk'),
    (select id from sets where short_name = 'cns'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reito Lantern'),
    (select id from sets where short_name = 'cns'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marchesa''s Smuggler'),
    (select id from sets where short_name = 'cns'),
    '50',
    'uncommon'
) 
 on conflict do nothing;
