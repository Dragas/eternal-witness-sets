insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Lavinia, Azorius Renegade'),
    (select id from sets where short_name = 'prna'),
    '189s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Priest of Forgotten Gods'),
    (select id from sets where short_name = 'prna'),
    '83s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deputy of Detention'),
    (select id from sets where short_name = 'prna'),
    '165p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Incubation Druid'),
    (select id from sets where short_name = 'prna'),
    '131s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bedevil'),
    (select id from sets where short_name = 'prna'),
    '157p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bedevil'),
    (select id from sets where short_name = 'prna'),
    '157s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hero of Precinct One'),
    (select id from sets where short_name = 'prna'),
    '11p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Godless Shrine'),
    (select id from sets where short_name = 'prna'),
    '248p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Growth-Chamber Guardian'),
    (select id from sets where short_name = 'prna'),
    '128p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Judith, the Scourge Diva'),
    (select id from sets where short_name = 'prna'),
    '185s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ethereal Absolution'),
    (select id from sets where short_name = 'prna'),
    '170s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teysa Karlov'),
    (select id from sets where short_name = 'prna'),
    '212p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hydroid Krasis'),
    (select id from sets where short_name = 'prna'),
    '183s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Seraph of the Scales'),
    (select id from sets where short_name = 'prna'),
    '205p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Priest of Forgotten Gods'),
    (select id from sets where short_name = 'prna'),
    '83p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Growth Spiral'),
    (select id from sets where short_name = 'prna'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kaya''s Wrath'),
    (select id from sets where short_name = 'prna'),
    '187s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cindervines'),
    (select id from sets where short_name = 'prna'),
    '161s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lavinia, Azorius Renegade'),
    (select id from sets where short_name = 'prna'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gate Colossus'),
    (select id from sets where short_name = 'prna'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Biomancer''s Familiar'),
    (select id from sets where short_name = 'prna'),
    '158s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Crypt'),
    (select id from sets where short_name = 'prna'),
    '245p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaya, Orzhov Usurper'),
    (select id from sets where short_name = 'prna'),
    '186p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bedeck // Bedazzle'),
    (select id from sets where short_name = 'prna'),
    '221p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Growth-Chamber Guardian'),
    (select id from sets where short_name = 'prna'),
    '128s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'End-Raze Forerunners'),
    (select id from sets where short_name = 'prna'),
    '124s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Theater of Horrors'),
    (select id from sets where short_name = 'prna'),
    '213s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rix Maadi Reveler'),
    (select id from sets where short_name = 'prna'),
    '109s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tithe Taker'),
    (select id from sets where short_name = 'prna'),
    '27s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teysa Karlov'),
    (select id from sets where short_name = 'prna'),
    '212s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Absorb'),
    (select id from sets where short_name = 'prna'),
    '151p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deputy of Detention'),
    (select id from sets where short_name = 'prna'),
    '165s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gutterbones'),
    (select id from sets where short_name = 'prna'),
    '76s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emergency Powers'),
    (select id from sets where short_name = 'prna'),
    '169s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Electrodominance'),
    (select id from sets where short_name = 'prna'),
    '99p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Ascendancy'),
    (select id from sets where short_name = 'prna'),
    '207s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mortify'),
    (select id from sets where short_name = 'prna'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kaya''s Wrath'),
    (select id from sets where short_name = 'prna'),
    '187p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos, the Showstopper'),
    (select id from sets where short_name = 'prna'),
    '199s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Guardian Project'),
    (select id from sets where short_name = 'prna'),
    '130s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skarrgan Hellkite'),
    (select id from sets where short_name = 'prna'),
    '114p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lumbering Battlement'),
    (select id from sets where short_name = 'prna'),
    '15s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Firewheeler'),
    (select id from sets where short_name = 'prna'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unbreakable Formation'),
    (select id from sets where short_name = 'prna'),
    '29p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirror March'),
    (select id from sets where short_name = 'prna'),
    '108s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plaza of Harmony'),
    (select id from sets where short_name = 'prna'),
    '254p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tithe Taker'),
    (select id from sets where short_name = 'prna'),
    '27p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Amplifire'),
    (select id from sets where short_name = 'prna'),
    '92s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Precognitive Perception'),
    (select id from sets where short_name = 'prna'),
    '45s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nikya of the Old Ways'),
    (select id from sets where short_name = 'prna'),
    '193s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unbreakable Formation'),
    (select id from sets where short_name = 'prna'),
    '29s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stomping Ground'),
    (select id from sets where short_name = 'prna'),
    '259p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zegana, Utopian Speaker'),
    (select id from sets where short_name = 'prna'),
    '214s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Awaken the Erstwhile'),
    (select id from sets where short_name = 'prna'),
    '61s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruul Spellbreaker'),
    (select id from sets where short_name = 'prna'),
    '179s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Font of Agonies'),
    (select id from sets where short_name = 'prna'),
    '74s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Judith, the Scourge Diva'),
    (select id from sets where short_name = 'prna'),
    '185p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hero of Precinct One'),
    (select id from sets where short_name = 'prna'),
    '11s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smothering Tithe'),
    (select id from sets where short_name = 'prna'),
    '22p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guardian Project'),
    (select id from sets where short_name = 'prna'),
    '130p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mass Manipulation'),
    (select id from sets where short_name = 'prna'),
    '42p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Absorb'),
    (select id from sets where short_name = 'prna'),
    '151s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphinx of Foresight'),
    (select id from sets where short_name = 'prna'),
    '55s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cindervines'),
    (select id from sets where short_name = 'prna'),
    '161p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spawn of Mayhem'),
    (select id from sets where short_name = 'prna'),
    '85p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hallowed Fountain'),
    (select id from sets where short_name = 'prna'),
    '251p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seraph of the Scales'),
    (select id from sets where short_name = 'prna'),
    '205s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hydroid Krasis'),
    (select id from sets where short_name = 'prna'),
    '183p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Incubation Druid'),
    (select id from sets where short_name = 'prna'),
    '131p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benthic Biomancer'),
    (select id from sets where short_name = 'prna'),
    '32s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Biogenic Ooze'),
    (select id from sets where short_name = 'prna'),
    '122p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Immolation Shaman'),
    (select id from sets where short_name = 'prna'),
    '106s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ravager Wurm'),
    (select id from sets where short_name = 'prna'),
    '200s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Breeding Pool'),
    (select id from sets where short_name = 'prna'),
    '246p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rampage of the Clans'),
    (select id from sets where short_name = 'prna'),
    '134s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Ascendancy'),
    (select id from sets where short_name = 'prna'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruul Spellbreaker'),
    (select id from sets where short_name = 'prna'),
    '179p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Light Up the Stage'),
    (select id from sets where short_name = 'prna'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Consecrate // Consume'),
    (select id from sets where short_name = 'prna'),
    '224p',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gutterbones'),
    (select id from sets where short_name = 'prna'),
    '76p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benthic Biomancer'),
    (select id from sets where short_name = 'prna'),
    '32p',
    'rare'
) 
 on conflict do nothing;
