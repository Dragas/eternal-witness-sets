    insert into mtgcard(name) values ('Djinn Monk') on conflict do nothing;
    insert into mtgcard(name) values ('Angel') on conflict do nothing;
    insert into mtgcard(name) values ('Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Spirit') on conflict do nothing;
    insert into mtgcard(name) values ('Beast') on conflict do nothing;
    insert into mtgcard(name) values ('Bird') on conflict do nothing;
