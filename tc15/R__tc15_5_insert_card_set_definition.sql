insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tc15'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gold'),
    (select id from sets where short_name = 'tc15'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tc15'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental Shaman'),
    (select id from sets where short_name = 'tc15'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snake'),
    (select id from sets where short_name = 'tc15'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight'),
    (select id from sets where short_name = 'tc15'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frog Lizard'),
    (select id from sets where short_name = 'tc15'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elephant'),
    (select id from sets where short_name = 'tc15'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shapeshifter'),
    (select id from sets where short_name = 'tc15'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Experience'),
    (select id from sets where short_name = 'tc15'),
    '0',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'tc15'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'tc15'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drake'),
    (select id from sets where short_name = 'tc15'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tc15'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tc15'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat'),
    (select id from sets where short_name = 'tc15'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tc15'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tc15'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bear'),
    (select id from sets where short_name = 'tc15'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Rager'),
    (select id from sets where short_name = 'tc15'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Germ'),
    (select id from sets where short_name = 'tc15'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tc15'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spider'),
    (select id from sets where short_name = 'tc15'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snake'),
    (select id from sets where short_name = 'tc15'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight'),
    (select id from sets where short_name = 'tc15'),
    '5',
    'common'
) 
 on conflict do nothing;
