insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Faerie Conclave'),
    (select id from sets where short_name = 'psum'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treetop Village'),
    (select id from sets where short_name = 'psum'),
    '2',
    'rare'
) 
 on conflict do nothing;
