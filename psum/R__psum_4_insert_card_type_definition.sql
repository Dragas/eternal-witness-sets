insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Faerie Conclave'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Treetop Village'),
        (select types.id from types where name = 'Land')
    ) 
 on conflict do nothing;
