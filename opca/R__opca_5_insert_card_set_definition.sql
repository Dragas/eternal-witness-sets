insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Llanowar'),
    (select id from sets where short_name = 'opca'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea of Sand'),
    (select id from sets where short_name = 'opca'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quicksilver Sea'),
    (select id from sets where short_name = 'opca'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talon Gates'),
    (select id from sets where short_name = 'opca'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Dark Barony'),
    (select id from sets where short_name = 'opca'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Celestine Reef'),
    (select id from sets where short_name = 'opca'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Takenuma'),
    (select id from sets where short_name = 'opca'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kharasha Foothills'),
    (select id from sets where short_name = 'opca'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Planewide Disaster'),
    (select id from sets where short_name = 'opca'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naya'),
    (select id from sets where short_name = 'opca'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tazeem'),
    (select id from sets where short_name = 'opca'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feeding Grounds'),
    (select id from sets where short_name = 'opca'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Norn''s Dominion'),
    (select id from sets where short_name = 'opca'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reality Shaping'),
    (select id from sets where short_name = 'opca'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skybreen'),
    (select id from sets where short_name = 'opca'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodhill Bastion'),
    (select id from sets where short_name = 'opca'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windriddle Palaces'),
    (select id from sets where short_name = 'opca'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Onakke Catacomb'),
    (select id from sets where short_name = 'opca'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mutual Epiphany'),
    (select id from sets where short_name = 'opca'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Otaria'),
    (select id from sets where short_name = 'opca'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Aether Flues'),
    (select id from sets where short_name = 'opca'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grand Ossuary'),
    (select id from sets where short_name = 'opca'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chaotic Aether'),
    (select id from sets where short_name = 'opca'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bant'),
    (select id from sets where short_name = 'opca'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murasa'),
    (select id from sets where short_name = 'opca'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Hippodrome'),
    (select id from sets where short_name = 'opca'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctum of Serra'),
    (select id from sets where short_name = 'opca'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Zephyr Maze'),
    (select id from sets where short_name = 'opca'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Eon Fog'),
    (select id from sets where short_name = 'opca'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Isle of Vesuva'),
    (select id from sets where short_name = 'opca'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kessig'),
    (select id from sets where short_name = 'opca'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stensia'),
    (select id from sets where short_name = 'opca'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Morphic Tide'),
    (select id from sets where short_name = 'opca'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pools of Becoming'),
    (select id from sets where short_name = 'opca'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glimmervoid Basin'),
    (select id from sets where short_name = 'opca'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Astral Arena'),
    (select id from sets where short_name = 'opca'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Interplanar Tunnel'),
    (select id from sets where short_name = 'opca'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hedron Fields of Agadeem'),
    (select id from sets where short_name = 'opca'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horizon Boughs'),
    (select id from sets where short_name = 'opca'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Velis Vel'),
    (select id from sets where short_name = 'opca'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Maelstrom'),
    (select id from sets where short_name = 'opca'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undercity Reaches'),
    (select id from sets where short_name = 'opca'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Agyrem'),
    (select id from sets where short_name = 'opca'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Panopticon'),
    (select id from sets where short_name = 'opca'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lethe Lake'),
    (select id from sets where short_name = 'opca'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grove of the Dreampods'),
    (select id from sets where short_name = 'opca'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goldmeadow'),
    (select id from sets where short_name = 'opca'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prahv'),
    (select id from sets where short_name = 'opca'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Great Forest'),
    (select id from sets where short_name = 'opca'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shiv'),
    (select id from sets where short_name = 'opca'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kilnspire District'),
    (select id from sets where short_name = 'opca'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Truga Jungle'),
    (select id from sets where short_name = 'opca'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Loft Gardens'),
    (select id from sets where short_name = 'opca'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krosa'),
    (select id from sets where short_name = 'opca'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gavony'),
    (select id from sets where short_name = 'opca'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minamo'),
    (select id from sets where short_name = 'opca'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nephalia'),
    (select id from sets where short_name = 'opca'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akoum'),
    (select id from sets where short_name = 'opca'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mount Keralia'),
    (select id from sets where short_name = 'opca'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fields of Summer'),
    (select id from sets where short_name = 'opca'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lair of the Ashen Idol'),
    (select id from sets where short_name = 'opca'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time Distortion'),
    (select id from sets where short_name = 'opca'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orzhova'),
    (select id from sets where short_name = 'opca'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eloren Wilds'),
    (select id from sets where short_name = 'opca'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Edge of Malacol'),
    (select id from sets where short_name = 'opca'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Steam Maze'),
    (select id from sets where short_name = 'opca'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Turri Island'),
    (select id from sets where short_name = 'opca'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Immersturm'),
    (select id from sets where short_name = 'opca'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stronghold Furnace'),
    (select id from sets where short_name = 'opca'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aretopolis'),
    (select id from sets where short_name = 'opca'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirrored Depths'),
    (select id from sets where short_name = 'opca'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Furnace Layer'),
    (select id from sets where short_name = 'opca'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jund'),
    (select id from sets where short_name = 'opca'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spatial Merging'),
    (select id from sets where short_name = 'opca'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raven''s Run'),
    (select id from sets where short_name = 'opca'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Fourth Sphere'),
    (select id from sets where short_name = 'opca'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tember City'),
    (select id from sets where short_name = 'opca'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grixis'),
    (select id from sets where short_name = 'opca'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stairs to Infinity'),
    (select id from sets where short_name = 'opca'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Academy at Tolaria West'),
    (select id from sets where short_name = 'opca'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trail of the Mage-Rings'),
    (select id from sets where short_name = 'opca'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naar Isle'),
    (select id from sets where short_name = 'opca'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orochi Colony'),
    (select id from sets where short_name = 'opca'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sokenzan'),
    (select id from sets where short_name = 'opca'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glen Elendra'),
    (select id from sets where short_name = 'opca'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cliffside Market'),
    (select id from sets where short_name = 'opca'),
    '18',
    'common'
) 
 on conflict do nothing;
