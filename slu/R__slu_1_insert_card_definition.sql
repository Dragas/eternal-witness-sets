    insert into mtgcard(name) values ('Verdant Catacombs') on conflict do nothing;
    insert into mtgcard(name) values ('Marsh Flats') on conflict do nothing;
    insert into mtgcard(name) values ('Arid Mesa') on conflict do nothing;
    insert into mtgcard(name) values ('Misty Rainforest') on conflict do nothing;
    insert into mtgcard(name) values ('Scalding Tarn') on conflict do nothing;
