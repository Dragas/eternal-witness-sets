insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Verdant Catacombs'),
    (select id from sets where short_name = 'slu'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marsh Flats'),
    (select id from sets where short_name = 'slu'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arid Mesa'),
    (select id from sets where short_name = 'slu'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Misty Rainforest'),
    (select id from sets where short_name = 'slu'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scalding Tarn'),
    (select id from sets where short_name = 'slu'),
    '2',
    'rare'
) 
 on conflict do nothing;
