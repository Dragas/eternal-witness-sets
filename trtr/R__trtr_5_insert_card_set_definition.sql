insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Bird'),
    (select id from sets where short_name = 'trtr'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhino'),
    (select id from sets where short_name = 'trtr'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurm'),
    (select id from sets where short_name = 'trtr'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'trtr'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight'),
    (select id from sets where short_name = 'trtr'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'trtr'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Centaur'),
    (select id from sets where short_name = 'trtr'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Assassin'),
    (select id from sets where short_name = 'trtr'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'trtr'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ooze'),
    (select id from sets where short_name = 'trtr'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'trtr'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'trtr'),
    '10',
    'common'
) 
 on conflict do nothing;
