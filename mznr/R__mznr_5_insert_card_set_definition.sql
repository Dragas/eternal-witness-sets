insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Roil Royale // Roil Royale'),
    (select id from sets where short_name = 'mznr'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Booster Sleuth // Booster Sleuth'),
    (select id from sets where short_name = 'mznr'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Booster Blitz'),
    (select id from sets where short_name = 'mznr'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Base Race // Base Race'),
    (select id from sets where short_name = 'mznr'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strictly Better // Strictly Better'),
    (select id from sets where short_name = 'mznr'),
    '5',
    'common'
) 
 on conflict do nothing;
