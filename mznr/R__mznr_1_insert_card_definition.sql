    insert into mtgcard(name) values ('Roil Royale // Roil Royale') on conflict do nothing;
    insert into mtgcard(name) values ('Booster Sleuth // Booster Sleuth') on conflict do nothing;
    insert into mtgcard(name) values ('Booster Blitz') on conflict do nothing;
    insert into mtgcard(name) values ('Base Race // Base Race') on conflict do nothing;
    insert into mtgcard(name) values ('Strictly Better // Strictly Better') on conflict do nothing;
