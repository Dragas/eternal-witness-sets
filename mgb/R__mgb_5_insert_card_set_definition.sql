insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'King Cheetah'),
    (select id from sets where short_name = 'mgb'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viashino Sandstalker'),
    (select id from sets where short_name = 'mgb'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necrosavant'),
    (select id from sets where short_name = 'mgb'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bull Elephant'),
    (select id from sets where short_name = 'mgb'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urborg Mindsucker'),
    (select id from sets where short_name = 'mgb'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ovinomancer'),
    (select id from sets where short_name = 'mgb'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Privilege'),
    (select id from sets where short_name = 'mgb'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampirism'),
    (select id from sets where short_name = 'mgb'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Peace Talks'),
    (select id from sets where short_name = 'mgb'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wicked Reward'),
    (select id from sets where short_name = 'mgb'),
    '7',
    'rare'
) 
 on conflict do nothing;
