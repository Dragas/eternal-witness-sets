insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Floodbringer'),
    (select id from sets where short_name = 'bok'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skullmane Baku'),
    (select id from sets where short_name = 'bok'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirror Gallery'),
    (select id from sets where short_name = 'bok'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eradicate'),
    (select id from sets where short_name = 'bok'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nezumi Shadow-Watcher'),
    (select id from sets where short_name = 'bok'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Split-Tail Miko'),
    (select id from sets where short_name = 'bok'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clash of Realities'),
    (select id from sets where short_name = 'bok'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Toils of Night and Day'),
    (select id from sets where short_name = 'bok'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iwamori of the Open Fist'),
    (select id from sets where short_name = 'bok'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tomorrow, Azami''s Familiar'),
    (select id from sets where short_name = 'bok'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ire of Kaminari'),
    (select id from sets where short_name = 'bok'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Takeno''s Cavalry'),
    (select id from sets where short_name = 'bok'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Toshiro Umezawa'),
    (select id from sets where short_name = 'bok'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forked-Branch Garami'),
    (select id from sets where short_name = 'bok'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Patron of the Kitsune'),
    (select id from sets where short_name = 'bok'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reduce to Dreams'),
    (select id from sets where short_name = 'bok'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silverstorm Samurai'),
    (select id from sets where short_name = 'bok'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aura Barbs'),
    (select id from sets where short_name = 'bok'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Neko-Te'),
    (select id from sets where short_name = 'bok'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Matsu-Tribe Sniper'),
    (select id from sets where short_name = 'bok'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disrupting Shoal'),
    (select id from sets where short_name = 'bok'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Patron of the Akki'),
    (select id from sets where short_name = 'bok'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harbinger of Spring'),
    (select id from sets where short_name = 'bok'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frost Ogre'),
    (select id from sets where short_name = 'bok'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horobi''s Whisper'),
    (select id from sets where short_name = 'bok'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychic Spear'),
    (select id from sets where short_name = 'bok'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cunning Bandit // Azamuki, Treachery Incarnate'),
    (select id from sets where short_name = 'bok'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Twist Allegiance'),
    (select id from sets where short_name = 'bok'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Final Judgment'),
    (select id from sets where short_name = 'bok'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lifegift'),
    (select id from sets where short_name = 'bok'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hired Muscle // Scarmaker'),
    (select id from sets where short_name = 'bok'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goryo''s Vengeance'),
    (select id from sets where short_name = 'bok'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oyobi, Who Split the Heavens'),
    (select id from sets where short_name = 'bok'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Veil of Secrecy'),
    (select id from sets where short_name = 'bok'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Walker of Secret Ways'),
    (select id from sets where short_name = 'bok'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ninja of the Deep Hours'),
    (select id from sets where short_name = 'bok'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ink-Eyes, Servant of Oni'),
    (select id from sets where short_name = 'bok'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaijin of the Vanishing Touch'),
    (select id from sets where short_name = 'bok'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scaled Hulk'),
    (select id from sets where short_name = 'bok'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Umezawa''s Jitte'),
    (select id from sets where short_name = 'bok'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hundred-Talon Strike'),
    (select id from sets where short_name = 'bok'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sosuke''s Summons'),
    (select id from sets where short_name = 'bok'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Three Tragedies'),
    (select id from sets where short_name = 'bok'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kami of False Hope'),
    (select id from sets where short_name = 'bok'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ronin Warclub'),
    (select id from sets where short_name = 'bok'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kami of the Honored Dead'),
    (select id from sets where short_name = 'bok'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fumiko the Lowblood'),
    (select id from sets where short_name = 'bok'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quash'),
    (select id from sets where short_name = 'bok'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stream of Consciousness'),
    (select id from sets where short_name = 'bok'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heartless Hidetsugu'),
    (select id from sets where short_name = 'bok'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nourishing Shoal'),
    (select id from sets where short_name = 'bok'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soratami Mindsweeper'),
    (select id from sets where short_name = 'bok'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pus Kami'),
    (select id from sets where short_name = 'bok'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loam Dweller'),
    (select id from sets where short_name = 'bok'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Minamo''s Meddling'),
    (select id from sets where short_name = 'bok'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frostling'),
    (select id from sets where short_name = 'bok'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yukora, the Prisoner'),
    (select id from sets where short_name = 'bok'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kumano''s Blessing'),
    (select id from sets where short_name = 'bok'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hokori, Dust Drinker'),
    (select id from sets where short_name = 'bok'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mending Hands'),
    (select id from sets where short_name = 'bok'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Splinter'),
    (select id from sets where short_name = 'bok'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mistblade Shinobi'),
    (select id from sets where short_name = 'bok'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gnarled Mass'),
    (select id from sets where short_name = 'bok'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Okiba-Gang Shinobi'),
    (select id from sets where short_name = 'bok'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Patron of the Moon'),
    (select id from sets where short_name = 'bok'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sway of the Stars'),
    (select id from sets where short_name = 'bok'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orb of Dreams'),
    (select id from sets where short_name = 'bok'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Patron of the Nezumi'),
    (select id from sets where short_name = 'bok'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mark of the Oni'),
    (select id from sets where short_name = 'bok'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Roar of Jukai'),
    (select id from sets where short_name = 'bok'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kodama of the Center Tree'),
    (select id from sets where short_name = 'bok'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chisei, Heart of Oceans'),
    (select id from sets where short_name = 'bok'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skullsnatcher'),
    (select id from sets where short_name = 'bok'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Patron of the Orochi'),
    (select id from sets where short_name = 'bok'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Torrent of Stone'),
    (select id from sets where short_name = 'bok'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stir the Grave'),
    (select id from sets where short_name = 'bok'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Budoka Pupil // Ichiga, Who Topples Oaks'),
    (select id from sets where short_name = 'bok'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ward of Piety'),
    (select id from sets where short_name = 'bok'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Higure, the Still Wind'),
    (select id from sets where short_name = 'bok'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faithful Squire // Kaiso, Memory of Loyalty'),
    (select id from sets where short_name = 'bok'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mark of Sakiko'),
    (select id from sets where short_name = 'bok'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sakiko, Mother of Summer'),
    (select id from sets where short_name = 'bok'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blademane Baku'),
    (select id from sets where short_name = 'bok'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shining Shoal'),
    (select id from sets where short_name = 'bok'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Petalmane Baku'),
    (select id from sets where short_name = 'bok'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akki Blizzard-Herder'),
    (select id from sets where short_name = 'bok'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shirei, Shizo''s Caretaker'),
    (select id from sets where short_name = 'bok'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bile Urchin'),
    (select id from sets where short_name = 'bok'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baku Altar'),
    (select id from sets where short_name = 'bok'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Empty-Shrine Kannushi'),
    (select id from sets where short_name = 'bok'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Uproot'),
    (select id from sets where short_name = 'bok'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Cohort'),
    (select id from sets where short_name = 'bok'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crack the Earth'),
    (select id from sets where short_name = 'bok'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blazing Shoal'),
    (select id from sets where short_name = 'bok'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ishi-Ishi, Akki Crackshot'),
    (select id from sets where short_name = 'bok'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shinka Gatekeeper'),
    (select id from sets where short_name = 'bok'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Isao, Enlightened Bushi'),
    (select id from sets where short_name = 'bok'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Genju of the Fens'),
    (select id from sets where short_name = 'bok'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Minamo Sightbender'),
    (select id from sets where short_name = 'bok'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shimmering Glasskite'),
    (select id from sets where short_name = 'bok'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overblaze'),
    (select id from sets where short_name = 'bok'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Threads of Disloyalty'),
    (select id from sets where short_name = 'bok'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Day of Destiny'),
    (select id from sets where short_name = 'bok'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crawling Filth'),
    (select id from sets where short_name = 'bok'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Genju of the Cedars'),
    (select id from sets where short_name = 'bok'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tallowisp'),
    (select id from sets where short_name = 'bok'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shuko'),
    (select id from sets where short_name = 'bok'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terashi''s Grasp'),
    (select id from sets where short_name = 'bok'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flames of the Blood Hand'),
    (select id from sets where short_name = 'bok'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantom Wings'),
    (select id from sets where short_name = 'bok'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scour'),
    (select id from sets where short_name = 'bok'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ogre Recluse'),
    (select id from sets where short_name = 'bok'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enshrined Memories'),
    (select id from sets where short_name = 'bok'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teardrop Kami'),
    (select id from sets where short_name = 'bok'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Springcaller'),
    (select id from sets where short_name = 'bok'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Child of Thorns'),
    (select id from sets where short_name = 'bok'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mannichi, the Fevered Dream'),
    (select id from sets where short_name = 'bok'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scourge of Numai'),
    (select id from sets where short_name = 'bok'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vital Surge'),
    (select id from sets where short_name = 'bok'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ornate Kanzashi'),
    (select id from sets where short_name = 'bok'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Indebted Samurai'),
    (select id from sets where short_name = 'bok'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terashi''s Verdict'),
    (select id from sets where short_name = 'bok'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sowing Salt'),
    (select id from sets where short_name = 'bok'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gods'' Eye, Gate to the Reikai'),
    (select id from sets where short_name = 'bok'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Call for Blood'),
    (select id from sets where short_name = 'bok'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Takenuma Bleeder'),
    (select id from sets where short_name = 'bok'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Genju of the Spires'),
    (select id from sets where short_name = 'bok'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Throat Slitter'),
    (select id from sets where short_name = 'bok'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lifespinner'),
    (select id from sets where short_name = 'bok'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Akki Raider'),
    (select id from sets where short_name = 'bok'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Callow Jushi // Jaraku the Interloper'),
    (select id from sets where short_name = 'bok'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Traproot Kami'),
    (select id from sets where short_name = 'bok'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hero''s Demise'),
    (select id from sets where short_name = 'bok'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tendo Ice Bridge'),
    (select id from sets where short_name = 'bok'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'First Volley'),
    (select id from sets where short_name = 'bok'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unchecked Growth'),
    (select id from sets where short_name = 'bok'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'That Which Was Taken'),
    (select id from sets where short_name = 'bok'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heart of Light'),
    (select id from sets where short_name = 'bok'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sickening Shoal'),
    (select id from sets where short_name = 'bok'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moonlit Strider'),
    (select id from sets where short_name = 'bok'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shizuko, Caller of Autumn'),
    (select id from sets where short_name = 'bok'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kyoki, Sanity''s Eclipse'),
    (select id from sets where short_name = 'bok'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kami of Tattered Shoji'),
    (select id from sets where short_name = 'bok'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kentaro, the Smiling Cat'),
    (select id from sets where short_name = 'bok'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blessing of Leeches'),
    (select id from sets where short_name = 'bok'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashen Monstrosity'),
    (select id from sets where short_name = 'bok'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Opal-Eye, Konda''s Yojimbo'),
    (select id from sets where short_name = 'bok'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Genju of the Fields'),
    (select id from sets where short_name = 'bok'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jetting Glasskite'),
    (select id from sets where short_name = 'bok'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slumbering Tora'),
    (select id from sets where short_name = 'bok'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ribbons of the Reikai'),
    (select id from sets where short_name = 'bok'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Waxmane Baku'),
    (select id from sets where short_name = 'bok'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Genju of the Realm'),
    (select id from sets where short_name = 'bok'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heed the Mists'),
    (select id from sets where short_name = 'bok'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kitsune Palliator'),
    (select id from sets where short_name = 'bok'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shuriken'),
    (select id from sets where short_name = 'bok'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ogre Marauder'),
    (select id from sets where short_name = 'bok'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blinding Powder'),
    (select id from sets where short_name = 'bok'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'In the Web of War'),
    (select id from sets where short_name = 'bok'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Body of Jukai'),
    (select id from sets where short_name = 'bok'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yomiji, Who Bars the Way'),
    (select id from sets where short_name = 'bok'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Genju of the Falls'),
    (select id from sets where short_name = 'bok'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ronin Cliffrider'),
    (select id from sets where short_name = 'bok'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kira, Great Glass-Spinner'),
    (select id from sets where short_name = 'bok'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quillmane Baku'),
    (select id from sets where short_name = 'bok'),
    '48',
    'common'
) 
 on conflict do nothing;
