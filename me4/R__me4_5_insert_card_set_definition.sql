insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Bronze Horse'),
    (select id from sets where short_name = 'me4'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = 'me4'),
    '258a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Shred'),
    (select id from sets where short_name = 'me4'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Radjan Spirit'),
    (select id from sets where short_name = 'me4'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelic Voices'),
    (select id from sets where short_name = 'me4'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Just Fate'),
    (select id from sets where short_name = 'me4'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icy Manipulator'),
    (select id from sets where short_name = 'me4'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reconstruction'),
    (select id from sets where short_name = 'me4'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = 'me4'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = 'me4'),
    '259a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Shrine'),
    (select id from sets where short_name = 'me4'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lim-Dûl''s Cohort'),
    (select id from sets where short_name = 'me4'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Matrix'),
    (select id from sets where short_name = 'me4'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ebony Rhino'),
    (select id from sets where short_name = 'me4'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gloom'),
    (select id from sets where short_name = 'me4'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scrubland'),
    (select id from sets where short_name = 'me4'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Floodwater Dam'),
    (select id from sets where short_name = 'me4'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blue Elemental Blast'),
    (select id from sets where short_name = 'me4'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smoke'),
    (select id from sets where short_name = 'me4'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonic Tutor'),
    (select id from sets where short_name = 'me4'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sleight of Hand'),
    (select id from sets where short_name = 'me4'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'me4'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Firestarter'),
    (select id from sets where short_name = 'me4'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steam Catapult'),
    (select id from sets where short_name = 'me4'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Acid Rain'),
    (select id from sets where short_name = 'me4'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'me4'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rockslide Ambush'),
    (select id from sets where short_name = 'me4'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea Serpent'),
    (select id from sets where short_name = 'me4'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fastbond'),
    (select id from sets where short_name = 'me4'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clay Statue'),
    (select id from sets where short_name = 'me4'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bird Maiden'),
    (select id from sets where short_name = 'me4'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balance'),
    (select id from sets where short_name = 'me4'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alaborn Musketeer'),
    (select id from sets where short_name = 'me4'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grapeshot Catapult'),
    (select id from sets where short_name = 'me4'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Veteran Bodyguard'),
    (select id from sets where short_name = 'me4'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloud Spirit'),
    (select id from sets where short_name = 'me4'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Two-Headed Giant of Foriys'),
    (select id from sets where short_name = 'me4'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warp Artifact'),
    (select id from sets where short_name = 'me4'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Undoing'),
    (select id from sets where short_name = 'me4'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Miter'),
    (select id from sets where short_name = 'me4'),
    '237',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Braingeyser'),
    (select id from sets where short_name = 'me4'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = 'me4'),
    '257d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Engine'),
    (select id from sets where short_name = 'me4'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Last Chance'),
    (select id from sets where short_name = 'me4'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Badlands'),
    (select id from sets where short_name = 'me4'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ali from Cairo'),
    (select id from sets where short_name = 'me4'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Juggernaut'),
    (select id from sets where short_name = 'me4'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Transmute Artifact'),
    (select id from sets where short_name = 'me4'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rock Hydra'),
    (select id from sets where short_name = 'me4'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystic Decree'),
    (select id from sets where short_name = 'me4'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Osai Vultures'),
    (select id from sets where short_name = 'me4'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Citanul Druid'),
    (select id from sets where short_name = 'me4'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Warrens'),
    (select id from sets where short_name = 'me4'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = 'me4'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blaze of Glory'),
    (select id from sets where short_name = 'me4'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Force of Nature'),
    (select id from sets where short_name = 'me4'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Squall'),
    (select id from sets where short_name = 'me4'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = 'me4'),
    '258c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whiptail Wurm'),
    (select id from sets where short_name = 'me4'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'me4'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Griffin'),
    (select id from sets where short_name = 'me4'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Argothian Treefolk'),
    (select id from sets where short_name = 'me4'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Xenic Poltergeist'),
    (select id from sets where short_name = 'me4'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dread Reaper'),
    (select id from sets where short_name = 'me4'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drain Power'),
    (select id from sets where short_name = 'me4'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mightstone'),
    (select id from sets where short_name = 'me4'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Staff of Zegon'),
    (select id from sets where short_name = 'me4'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divine Offering'),
    (select id from sets where short_name = 'me4'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = 'me4'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kormus Bell'),
    (select id from sets where short_name = 'me4'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Martyrs of Korlis'),
    (select id from sets where short_name = 'me4'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crumble'),
    (select id from sets where short_name = 'me4'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scavenger Folk'),
    (select id from sets where short_name = 'me4'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Altar'),
    (select id from sets where short_name = 'me4'),
    '181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ebony Horse'),
    (select id from sets where short_name = 'me4'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tablet of Epityr'),
    (select id from sets where short_name = 'me4'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Southern Elephant'),
    (select id from sets where short_name = 'me4'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Personal Incarnation'),
    (select id from sets where short_name = 'me4'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Detonate'),
    (select id from sets where short_name = 'me4'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Howl from Beyond'),
    (select id from sets where short_name = 'me4'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aladdin'),
    (select id from sets where short_name = 'me4'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jade Monolith'),
    (select id from sets where short_name = 'me4'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tropical Island'),
    (select id from sets where short_name = 'me4'),
    '254',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oasis'),
    (select id from sets where short_name = 'me4'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wheel of Fortune'),
    (select id from sets where short_name = 'me4'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coral Helm'),
    (select id from sets where short_name = 'me4'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Tortoise'),
    (select id from sets where short_name = 'me4'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scarecrow'),
    (select id from sets where short_name = 'me4'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Titania''s Song'),
    (select id from sets where short_name = 'me4'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glasses of Urza'),
    (select id from sets where short_name = 'me4'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'White Knight'),
    (select id from sets where short_name = 'me4'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = 'me4'),
    '257a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gate to Phyrexia'),
    (select id from sets where short_name = 'me4'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weakstone'),
    (select id from sets where short_name = 'me4'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island Sanctuary'),
    (select id from sets where short_name = 'me4'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alluring Scent'),
    (select id from sets where short_name = 'me4'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Amulet of Kroog'),
    (select id from sets where short_name = 'me4'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savannah'),
    (select id from sets where short_name = 'me4'),
    '250',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harsh Justice'),
    (select id from sets where short_name = 'me4'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drop of Honey'),
    (select id from sets where short_name = 'me4'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Theft of Dreams'),
    (select id from sets where short_name = 'me4'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Primitive Justice'),
    (select id from sets where short_name = 'me4'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soldevi Golem'),
    (select id from sets where short_name = 'me4'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Avenger'),
    (select id from sets where short_name = 'me4'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Onulet'),
    (select id from sets where short_name = 'me4'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Candelabra of Tawnos'),
    (select id from sets where short_name = 'me4'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Naked Singularity'),
    (select id from sets where short_name = 'me4'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Junún Efreet'),
    (select id from sets where short_name = 'me4'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ice Cauldron'),
    (select id from sets where short_name = 'me4'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spotted Griffin'),
    (select id from sets where short_name = 'me4'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ogre Taskmaster'),
    (select id from sets where short_name = 'me4'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stasis'),
    (select id from sets where short_name = 'me4'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animate Artifact'),
    (select id from sets where short_name = 'me4'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ebon Dragon'),
    (select id from sets where short_name = 'me4'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lifeforce'),
    (select id from sets where short_name = 'me4'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Channel'),
    (select id from sets where short_name = 'me4'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thunder Dragon'),
    (select id from sets where short_name = 'me4'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Chalice'),
    (select id from sets where short_name = 'me4'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guardian Beast'),
    (select id from sets where short_name = 'me4'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'In the Eye of Chaos'),
    (select id from sets where short_name = 'me4'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloud Dragon'),
    (select id from sets where short_name = 'me4'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Argothian Pixies'),
    (select id from sets where short_name = 'me4'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tundra'),
    (select id from sets where short_name = 'me4'),
    '255',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Bully'),
    (select id from sets where short_name = 'me4'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elite Cat Warrior'),
    (select id from sets where short_name = 'me4'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = 'me4'),
    '257c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pentagram of the Ages'),
    (select id from sets where short_name = 'me4'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Conversion'),
    (select id from sets where short_name = 'me4'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wood Elemental'),
    (select id from sets where short_name = 'me4'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Book of Rass'),
    (select id from sets where short_name = 'me4'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Healing Salve'),
    (select id from sets where short_name = 'me4'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prodigal Sorcerer'),
    (select id from sets where short_name = 'me4'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Foul Spirit'),
    (select id from sets where short_name = 'me4'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Energy Flux'),
    (select id from sets where short_name = 'me4'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scavenging Ghoul'),
    (select id from sets where short_name = 'me4'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sedge Troll'),
    (select id from sets where short_name = 'me4'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Living Lands'),
    (select id from sets where short_name = 'me4'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sinkhole'),
    (select id from sets where short_name = 'me4'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eye for an Eye'),
    (select id from sets where short_name = 'me4'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aesthir Glider'),
    (select id from sets where short_name = 'me4'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cyclopean Tomb'),
    (select id from sets where short_name = 'me4'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scarwood Bandits'),
    (select id from sets where short_name = 'me4'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Triassic Egg'),
    (select id from sets where short_name = 'me4'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Devastation'),
    (select id from sets where short_name = 'me4'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'me4'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'War Mammoth'),
    (select id from sets where short_name = 'me4'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Living Wall'),
    (select id from sets where short_name = 'me4'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vibrating Sphere'),
    (select id from sets where short_name = 'me4'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Underground Sea'),
    (select id from sets where short_name = 'me4'),
    '256',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gorilla War Cry'),
    (select id from sets where short_name = 'me4'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clockwork Avian'),
    (select id from sets where short_name = 'me4'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brass Man'),
    (select id from sets where short_name = 'me4'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diabolic Machine'),
    (select id from sets where short_name = 'me4'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savannah Lions'),
    (select id from sets where short_name = 'me4'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Workshop'),
    (select id from sets where short_name = 'me4'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soldevi Machinist'),
    (select id from sets where short_name = 'me4'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elephant Graveyard'),
    (select id from sets where short_name = 'me4'),
    '244',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bayou'),
    (select id from sets where short_name = 'me4'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Word of Command'),
    (select id from sets where short_name = 'me4'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strip Mine'),
    (select id from sets where short_name = 'me4'),
    '252',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roc of Kher Ridges'),
    (select id from sets where short_name = 'me4'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cyclopean Mummy'),
    (select id from sets where short_name = 'me4'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakalite'),
    (select id from sets where short_name = 'me4'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = 'me4'),
    '258b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Aviary'),
    (select id from sets where short_name = 'me4'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drowned'),
    (select id from sets where short_name = 'me4'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = 'me4'),
    '259d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'me4'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = 'me4'),
    '258d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thing from the Deep'),
    (select id from sets where short_name = 'me4'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathcoil Wurm'),
    (select id from sets where short_name = 'me4'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = 'me4'),
    '259b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Forces'),
    (select id from sets where short_name = 'me4'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lava Flow'),
    (select id from sets where short_name = 'me4'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Artifact Blast'),
    (select id from sets where short_name = 'me4'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Water Elemental'),
    (select id from sets where short_name = 'me4'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Tutor'),
    (select id from sets where short_name = 'me4'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tawnos''s Wand'),
    (select id from sets where short_name = 'me4'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ring of Renewal'),
    (select id from sets where short_name = 'me4'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Library of Alexandria'),
    (select id from sets where short_name = 'me4'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gravebind'),
    (select id from sets where short_name = 'me4'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alaborn Trooper'),
    (select id from sets where short_name = 'me4'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = 'me4'),
    '259c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leeches'),
    (select id from sets where short_name = 'me4'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Instill Energy'),
    (select id from sets where short_name = 'me4'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clockwork Swarm'),
    (select id from sets where short_name = 'me4'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Island'),
    (select id from sets where short_name = 'me4'),
    '260',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armageddon Clock'),
    (select id from sets where short_name = 'me4'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bottle of Suleiman'),
    (select id from sets where short_name = 'me4'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Mechanics'),
    (select id from sets where short_name = 'me4'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wicked Pact'),
    (select id from sets where short_name = 'me4'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hasran Ogress'),
    (select id from sets where short_name = 'me4'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Atog'),
    (select id from sets where short_name = 'me4'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Tempest'),
    (select id from sets where short_name = 'me4'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Time Vault'),
    (select id from sets where short_name = 'me4'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fire Imp'),
    (select id from sets where short_name = 'me4'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Ox'),
    (select id from sets where short_name = 'me4'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'False Summoning'),
    (select id from sets where short_name = 'me4'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mijae Djinn'),
    (select id from sets where short_name = 'me4'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tawnos''s Weaponry'),
    (select id from sets where short_name = 'me4'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flying Carpet'),
    (select id from sets where short_name = 'me4'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Regrowth'),
    (select id from sets where short_name = 'me4'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = 'me4'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dread Wight'),
    (select id from sets where short_name = 'me4'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Caves'),
    (select id from sets where short_name = 'me4'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mahamoti Djinn'),
    (select id from sets where short_name = 'me4'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestial Sword'),
    (select id from sets where short_name = 'me4'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Horn of Deafening'),
    (select id from sets where short_name = 'me4'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Taiga'),
    (select id from sets where short_name = 'me4'),
    '253',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prowling Nightstalker'),
    (select id from sets where short_name = 'me4'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tetravus'),
    (select id from sets where short_name = 'me4'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Minion of Tevesh Szat'),
    (select id from sets where short_name = 'me4'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zombie Master'),
    (select id from sets where short_name = 'me4'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Library of Leng'),
    (select id from sets where short_name = 'me4'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kudzu'),
    (select id from sets where short_name = 'me4'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plateau'),
    (select id from sets where short_name = 'me4'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Symbol of Unsummoning'),
    (select id from sets where short_name = 'me4'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fog'),
    (select id from sets where short_name = 'me4'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = 'me4'),
    '257b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'me4'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Artifact'),
    (select id from sets where short_name = 'me4'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Planar Gate'),
    (select id from sets where short_name = 'me4'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ironhoof Ox'),
    (select id from sets where short_name = 'me4'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Martyr''s Cry'),
    (select id from sets where short_name = 'me4'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Cavaliers'),
    (select id from sets where short_name = 'me4'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Control Magic'),
    (select id from sets where short_name = 'me4'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = 'me4'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primal Clay'),
    (select id from sets where short_name = 'me4'),
    '222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yotian Soldier'),
    (select id from sets where short_name = 'me4'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Copy Artifact'),
    (select id from sets where short_name = 'me4'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Weakness'),
    (select id from sets where short_name = 'me4'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Colossus of Sardia'),
    (select id from sets where short_name = 'me4'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maze of Ith'),
    (select id from sets where short_name = 'me4'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lich'),
    (select id from sets where short_name = 'me4'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Basalt Monolith'),
    (select id from sets where short_name = 'me4'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin General'),
    (select id from sets where short_name = 'me4'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Red Elemental Blast'),
    (select id from sets where short_name = 'me4'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Knight'),
    (select id from sets where short_name = 'me4'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gauntlet of Might'),
    (select id from sets where short_name = 'me4'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonic Hordes'),
    (select id from sets where short_name = 'me4'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dust to Dust'),
    (select id from sets where short_name = 'me4'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alchor''s Tomb'),
    (select id from sets where short_name = 'me4'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fork'),
    (select id from sets where short_name = 'me4'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cyclone'),
    (select id from sets where short_name = 'me4'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple Acolyte'),
    (select id from sets where short_name = 'me4'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Owl Familiar'),
    (select id from sets where short_name = 'me4'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Terrain'),
    (select id from sets where short_name = 'me4'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Al-abara''s Carpet'),
    (select id from sets where short_name = 'me4'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Righteous Charge'),
    (select id from sets where short_name = 'me4'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serendib Djinn'),
    (select id from sets where short_name = 'me4'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Champion Lancer'),
    (select id from sets where short_name = 'me4'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bee Sting'),
    (select id from sets where short_name = 'me4'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shapeshifter'),
    (select id from sets where short_name = 'me4'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Argivian Blacksmith'),
    (select id from sets where short_name = 'me4'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'me4'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'me4'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kismet'),
    (select id from sets where short_name = 'me4'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rain of Daggers'),
    (select id from sets where short_name = 'me4'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clockwork Gnomes'),
    (select id from sets where short_name = 'me4'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tsunami'),
    (select id from sets where short_name = 'me4'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sandstorm'),
    (select id from sets where short_name = 'me4'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Vault'),
    (select id from sets where short_name = 'me4'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overwhelming Forces'),
    (select id from sets where short_name = 'me4'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathgrip'),
    (select id from sets where short_name = 'me4'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Bestiary'),
    (select id from sets where short_name = 'me4'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talas Researcher'),
    (select id from sets where short_name = 'me4'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Aesthir'),
    (select id from sets where short_name = 'me4'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dakmor Plague'),
    (select id from sets where short_name = 'me4'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Obsianus Golem'),
    (select id from sets where short_name = 'me4'),
    '218',
    'common'
) 
 on conflict do nothing;
