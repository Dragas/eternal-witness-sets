insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Inexorable Tide'),
    (select id from sets where short_name = 'ha2'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terravore'),
    (select id from sets where short_name = 'ha2'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pack Rat'),
    (select id from sets where short_name = 'ha2'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bojuka Bog'),
    (select id from sets where short_name = 'ha2'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sigil of the Empty Throne'),
    (select id from sets where short_name = 'ha2'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ranger of Eos'),
    (select id from sets where short_name = 'ha2'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Virulent Plague'),
    (select id from sets where short_name = 'ha2'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brain Maggot'),
    (select id from sets where short_name = 'ha2'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meddling Mage'),
    (select id from sets where short_name = 'ha2'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonmaster Outcast'),
    (select id from sets where short_name = 'ha2'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Waste Not'),
    (select id from sets where short_name = 'ha2'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forgotten Cave'),
    (select id from sets where short_name = 'ha2'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nyx-Fleece Ram'),
    (select id from sets where short_name = 'ha2'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Pulse'),
    (select id from sets where short_name = 'ha2'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thalia, Guardian of Thraben'),
    (select id from sets where short_name = 'ha2'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancestral Mask'),
    (select id from sets where short_name = 'ha2'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghost Quarter'),
    (select id from sets where short_name = 'ha2'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lonely Sandbar'),
    (select id from sets where short_name = 'ha2'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tranquil Thicket'),
    (select id from sets where short_name = 'ha2'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of the Reliquary'),
    (select id from sets where short_name = 'ha2'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barren Moor'),
    (select id from sets where short_name = 'ha2'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Platinum Angel'),
    (select id from sets where short_name = 'ha2'),
    '18',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Merrow Reejerey'),
    (select id from sets where short_name = 'ha2'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Ruinblaster'),
    (select id from sets where short_name = 'ha2'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Secluded Steppe'),
    (select id from sets where short_name = 'ha2'),
    '24',
    'common'
) 
 on conflict do nothing;
