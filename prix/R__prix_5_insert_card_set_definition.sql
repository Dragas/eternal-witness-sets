insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Silverclad Ferocidons'),
    (select id from sets where short_name = 'prix'),
    '115p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dead Man''s Chest'),
    (select id from sets where short_name = 'prix'),
    '66s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tetzimoc, Primal Death'),
    (select id from sets where short_name = 'prix'),
    '86s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Release to the Wind'),
    (select id from sets where short_name = 'prix'),
    '46s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghalta, Primal Hunger'),
    (select id from sets where short_name = 'prix'),
    '130s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wayward Swordtooth'),
    (select id from sets where short_name = 'prix'),
    '150p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deeproot Elite'),
    (select id from sets where short_name = 'prix'),
    '127p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azor, the Lawbringer'),
    (select id from sets where short_name = 'prix'),
    '154s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blood Sun'),
    (select id from sets where short_name = 'prix'),
    '92p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Etali, Primal Storm'),
    (select id from sets where short_name = 'prix'),
    '100s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Admiral''s Order'),
    (select id from sets where short_name = 'prix'),
    '31p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghalta, Primal Hunger'),
    (select id from sets where short_name = 'prix'),
    '130p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Siegehorn Ceratops'),
    (select id from sets where short_name = 'prix'),
    '171p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tendershoot Dryad'),
    (select id from sets where short_name = 'prix'),
    '147p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tetzimoc, Primal Death'),
    (select id from sets where short_name = 'prix'),
    '86p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tilonalli''s Summoner'),
    (select id from sets where short_name = 'prix'),
    '121s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jadelight Ranger'),
    (select id from sets where short_name = 'prix'),
    '136p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zacama, Primal Calamity'),
    (select id from sets where short_name = 'prix'),
    '174p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Arch of Orazca'),
    (select id from sets where short_name = 'prix'),
    '185s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rekindling Phoenix'),
    (select id from sets where short_name = 'prix'),
    '111p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nezahal, Primal Tide'),
    (select id from sets where short_name = 'prix'),
    '45s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nezahal, Primal Tide'),
    (select id from sets where short_name = 'prix'),
    '45p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Paladin of Atonement'),
    (select id from sets where short_name = 'prix'),
    '16s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azor''s Gateway // Sanctum of the Sun'),
    (select id from sets where short_name = 'prix'),
    '176s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Zetalpa, Primal Dawn'),
    (select id from sets where short_name = 'prix'),
    '30s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silverclad Ferocidons'),
    (select id from sets where short_name = 'prix'),
    '115s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Polyraptor'),
    (select id from sets where short_name = 'prix'),
    '144s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'prix'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Immortal Sun'),
    (select id from sets where short_name = 'prix'),
    '180p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blood Sun'),
    (select id from sets where short_name = 'prix'),
    '92s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rekindling Phoenix'),
    (select id from sets where short_name = 'prix'),
    '111s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tomb Robber'),
    (select id from sets where short_name = 'prix'),
    '87s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Form of the Dinosaur'),
    (select id from sets where short_name = 'prix'),
    '103s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Admiral''s Order'),
    (select id from sets where short_name = 'prix'),
    '31s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silent Gravestone'),
    (select id from sets where short_name = 'prix'),
    '182p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golden Guardian // Gold-Forge Garrison'),
    (select id from sets where short_name = 'prix'),
    '179s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Radiant Destiny'),
    (select id from sets where short_name = 'prix'),
    '18p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trapjaw Tyrant'),
    (select id from sets where short_name = 'prix'),
    '29s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vona''s Hunger'),
    (select id from sets where short_name = 'prix'),
    '90p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Profane Procession // Tomb of the Dusk Rose'),
    (select id from sets where short_name = 'prix'),
    '166s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Journey to Eternity // Atzal, Cave of Eternity'),
    (select id from sets where short_name = 'prix'),
    '160s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mastermind''s Acquisition'),
    (select id from sets where short_name = 'prix'),
    '77p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm the Vault // Vault of Catlacan'),
    (select id from sets where short_name = 'prix'),
    '173s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dire Fleet Poisoner'),
    (select id from sets where short_name = 'prix'),
    '68s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Immortal Sun'),
    (select id from sets where short_name = 'prix'),
    '180s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Angrath, the Flame-Chained'),
    (select id from sets where short_name = 'prix'),
    '152s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'World Shaper'),
    (select id from sets where short_name = 'prix'),
    '151p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphinx''s Decree'),
    (select id from sets where short_name = 'prix'),
    '24s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Path of Mettle // Metzali, Tower of Triumph'),
    (select id from sets where short_name = 'prix'),
    '165s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twilight Prophet'),
    (select id from sets where short_name = 'prix'),
    '88p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hadana''s Climb // Winged Temple of Orazca'),
    (select id from sets where short_name = 'prix'),
    '158s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silvergill Adept'),
    (select id from sets where short_name = 'prix'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arch of Orazca'),
    (select id from sets where short_name = 'prix'),
    '185p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seafloor Oracle'),
    (select id from sets where short_name = 'prix'),
    '51p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kumena''s Awakening'),
    (select id from sets where short_name = 'prix'),
    '42s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twilight Prophet'),
    (select id from sets where short_name = 'prix'),
    '88s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dire Fleet Daredevil'),
    (select id from sets where short_name = 'prix'),
    '99s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple Altisaur'),
    (select id from sets where short_name = 'prix'),
    '28s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warkite Marauder'),
    (select id from sets where short_name = 'prix'),
    '60p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slaughter the Strong'),
    (select id from sets where short_name = 'prix'),
    '22s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Champion of Dusk'),
    (select id from sets where short_name = 'prix'),
    '64s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Timestream Navigator'),
    (select id from sets where short_name = 'prix'),
    '59s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Path of Discovery'),
    (select id from sets where short_name = 'prix'),
    '142p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wayward Swordtooth'),
    (select id from sets where short_name = 'prix'),
    '150s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crafty Cutpurse'),
    (select id from sets where short_name = 'prix'),
    '33s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zacama, Primal Calamity'),
    (select id from sets where short_name = 'prix'),
    '174s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Path of Discovery'),
    (select id from sets where short_name = 'prix'),
    '142s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brass''s Bounty'),
    (select id from sets where short_name = 'prix'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'World Shaper'),
    (select id from sets where short_name = 'prix'),
    '151s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tilonalli''s Summoner'),
    (select id from sets where short_name = 'prix'),
    '121p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zetalpa, Primal Dawn'),
    (select id from sets where short_name = 'prix'),
    '30p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elenda, the Dusk Rose'),
    (select id from sets where short_name = 'prix'),
    '157s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Siegehorn Ceratops'),
    (select id from sets where short_name = 'prix'),
    '171s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dire Fleet Poisoner'),
    (select id from sets where short_name = 'prix'),
    '68p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghalta, Primal Hunger'),
    (select id from sets where short_name = 'prix'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Radiant Destiny'),
    (select id from sets where short_name = 'prix'),
    '18s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brass''s Bounty'),
    (select id from sets where short_name = 'prix'),
    '94s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warkite Marauder'),
    (select id from sets where short_name = 'prix'),
    '60s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Captain''s Hook'),
    (select id from sets where short_name = 'prix'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tendershoot Dryad'),
    (select id from sets where short_name = 'prix'),
    '147s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kumena, Tyrant of Orazca'),
    (select id from sets where short_name = 'prix'),
    '162s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Protean Raider'),
    (select id from sets where short_name = 'prix'),
    '167s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vona''s Hunger'),
    (select id from sets where short_name = 'prix'),
    '90s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Induced Amnesia'),
    (select id from sets where short_name = 'prix'),
    '40s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Huatli, Radiant Champion'),
    (select id from sets where short_name = 'prix'),
    '159s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Captain''s Hook'),
    (select id from sets where short_name = 'prix'),
    '177s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seafloor Oracle'),
    (select id from sets where short_name = 'prix'),
    '51s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slaughter the Strong'),
    (select id from sets where short_name = 'prix'),
    '22p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mastermind''s Acquisition'),
    (select id from sets where short_name = 'prix'),
    '77s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Awakened Amalgam'),
    (select id from sets where short_name = 'prix'),
    '175s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jadelight Ranger'),
    (select id from sets where short_name = 'prix'),
    '136s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silent Gravestone'),
    (select id from sets where short_name = 'prix'),
    '182s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bishop of Binding'),
    (select id from sets where short_name = 'prix'),
    '2s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deeproot Elite'),
    (select id from sets where short_name = 'prix'),
    '127s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Etali, Primal Storm'),
    (select id from sets where short_name = 'prix'),
    '100p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dire Fleet Daredevil'),
    (select id from sets where short_name = 'prix'),
    '99p',
    'rare'
) 
 on conflict do nothing;
