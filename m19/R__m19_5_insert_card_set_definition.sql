insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Novice Knight'),
    (select id from sets where short_name = 'm19'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pelakka Wurm'),
    (select id from sets where short_name = 'm19'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Detection Tower'),
    (select id from sets where short_name = 'm19'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Riddlemaster Sphinx'),
    (select id from sets where short_name = 'm19'),
    '287',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aegis of the Heavens'),
    (select id from sets where short_name = 'm19'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bogstomper'),
    (select id from sets where short_name = 'm19'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Totally Lost'),
    (select id from sets where short_name = 'm19'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghirapur Guide'),
    (select id from sets where short_name = 'm19'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Timber Gorge'),
    (select id from sets where short_name = 'm19'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sarkhan''s Whelp'),
    (select id from sets where short_name = 'm19'),
    '299',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thorn Lieutenant'),
    (select id from sets where short_name = 'm19'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Strike'),
    (select id from sets where short_name = 'm19'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bone Dragon'),
    (select id from sets where short_name = 'm19'),
    '88',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm19'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oakenform'),
    (select id from sets where short_name = 'm19'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pegasus Courser'),
    (select id from sets where short_name = 'm19'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcane Encyclopedia'),
    (select id from sets where short_name = 'm19'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oreskos Swiftclaw'),
    (select id from sets where short_name = 'm19'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Departed Deckhand'),
    (select id from sets where short_name = 'm19'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thud'),
    (select id from sets where short_name = 'm19'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volley Veteran'),
    (select id from sets where short_name = 'm19'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampire Neonate'),
    (select id from sets where short_name = 'm19'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scapeshift'),
    (select id from sets where short_name = 'm19'),
    '201',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'One with the Machine'),
    (select id from sets where short_name = 'm19'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm19'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sai, Master Thopterist'),
    (select id from sets where short_name = 'm19'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = 'm19'),
    '308',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hieromancer''s Cage'),
    (select id from sets where short_name = 'm19'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Druid of Horns'),
    (select id from sets where short_name = 'm19'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra''s Guardian'),
    (select id from sets where short_name = 'm19'),
    '284',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Last Stand'),
    (select id from sets where short_name = 'm19'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blanchwood Armor'),
    (select id from sets where short_name = 'm19'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Transmogrifying Wand'),
    (select id from sets where short_name = 'm19'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aven Wind Mage'),
    (select id from sets where short_name = 'm19'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Archaeologist'),
    (select id from sets where short_name = 'm19'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meteor Golem'),
    (select id from sets where short_name = 'm19'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rogue''s Gloves'),
    (select id from sets where short_name = 'm19'),
    '243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Befuddle'),
    (select id from sets where short_name = 'm19'),
    '309',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gearsmith Guardian'),
    (select id from sets where short_name = 'm19'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sure Strike'),
    (select id from sets where short_name = 'm19'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcades, the Strategist'),
    (select id from sets where short_name = 'm19'),
    '212',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Knight of the Tusk'),
    (select id from sets where short_name = 'm19'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Patient Rebuilding'),
    (select id from sets where short_name = 'm19'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aethershield Artificer'),
    (select id from sets where short_name = 'm19'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alpine Moon'),
    (select id from sets where short_name = 'm19'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fountain of Renewal'),
    (select id from sets where short_name = 'm19'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tezzeret, Cruel Machinist'),
    (select id from sets where short_name = 'm19'),
    '286',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Djinn of Wishes'),
    (select id from sets where short_name = 'm19'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rhox Oracle'),
    (select id from sets where short_name = 'm19'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychic Corrosion'),
    (select id from sets where short_name = 'm19'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reclamation Sage'),
    (select id from sets where short_name = 'm19'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stitcher''s Supplier'),
    (select id from sets where short_name = 'm19'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Trashmaster'),
    (select id from sets where short_name = 'm19'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm19'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skymarch Bloodletter'),
    (select id from sets where short_name = 'm19'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Mare'),
    (select id from sets where short_name = 'm19'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Two-Headed Zombie'),
    (select id from sets where short_name = 'm19'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tolarian Scholar'),
    (select id from sets where short_name = 'm19'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greenwood Sentinel'),
    (select id from sets where short_name = 'm19'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Havoc Devils'),
    (select id from sets where short_name = 'm19'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snapping Drake'),
    (select id from sets where short_name = 'm19'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Pridemate'),
    (select id from sets where short_name = 'm19'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disperse'),
    (select id from sets where short_name = 'm19'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lena, Selfless Champion'),
    (select id from sets where short_name = 'm19'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strangling Spores'),
    (select id from sets where short_name = 'm19'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dryad Greenseeker'),
    (select id from sets where short_name = 'm19'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woodland Stream'),
    (select id from sets where short_name = 'm19'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychic Symbiont'),
    (select id from sets where short_name = 'm19'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tectonic Rift'),
    (select id from sets where short_name = 'm19'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demanding Dragon'),
    (select id from sets where short_name = 'm19'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aggressive Mammoth'),
    (select id from sets where short_name = 'm19'),
    '302',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boggart Brute'),
    (select id from sets where short_name = 'm19'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lich''s Caress'),
    (select id from sets where short_name = 'm19'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm19'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heroic Reinforcements'),
    (select id from sets where short_name = 'm19'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Walking Corpse'),
    (select id from sets where short_name = 'm19'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kargan Dragonrider'),
    (select id from sets where short_name = 'm19'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghostform'),
    (select id from sets where short_name = 'm19'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vigilant Baloth'),
    (select id from sets where short_name = 'm19'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sarkhan, Fireblood'),
    (select id from sets where short_name = 'm19'),
    '154',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Suncleanser'),
    (select id from sets where short_name = 'm19'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm19'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knightly Valor'),
    (select id from sets where short_name = 'm19'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silverbeak Griffin'),
    (select id from sets where short_name = 'm19'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goreclaw, Terror of Qal Sisma'),
    (select id from sets where short_name = 'm19'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gargoyle Sentinel'),
    (select id from sets where short_name = 'm19'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Child of Night'),
    (select id from sets where short_name = 'm19'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brawl-Bash Ogre'),
    (select id from sets where short_name = 'm19'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = 'm19'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Salvager of Secrets'),
    (select id from sets where short_name = 'm19'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'm19'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Isolate'),
    (select id from sets where short_name = 'm19'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiery Finish'),
    (select id from sets where short_name = 'm19'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Epicure of Blood'),
    (select id from sets where short_name = 'm19'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm19'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm19'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Militia Bugler'),
    (select id from sets where short_name = 'm19'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Herald of Faith'),
    (select id from sets where short_name = 'm19'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Murder'),
    (select id from sets where short_name = 'm19'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bone to Ash'),
    (select id from sets where short_name = 'm19'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desecrated Tomb'),
    (select id from sets where short_name = 'm19'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sift'),
    (select id from sets where short_name = 'm19'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Palladia-Mors, the Ruiner'),
    (select id from sets where short_name = 'm19'),
    '219',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tormenting Voice'),
    (select id from sets where short_name = 'm19'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sigiled Sword of Valeron'),
    (select id from sets where short_name = 'm19'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Make a Stand'),
    (select id from sets where short_name = 'm19'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ajani, Adversary of Tyrants'),
    (select id from sets where short_name = 'm19'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Liliana, the Necromancer'),
    (select id from sets where short_name = 'm19'),
    '291',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Elvish Clancaller'),
    (select id from sets where short_name = 'm19'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Graveyard Marshal'),
    (select id from sets where short_name = 'm19'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infernal Reckoning'),
    (select id from sets where short_name = 'm19'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Highland Game'),
    (select id from sets where short_name = 'm19'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Mist'),
    (select id from sets where short_name = 'm19'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chromium, the Mutable'),
    (select id from sets where short_name = 'm19'),
    '214',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aerial Engineer'),
    (select id from sets where short_name = 'm19'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sovereign''s Bite'),
    (select id from sets where short_name = 'm19'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lathliss, Dragon Queen'),
    (select id from sets where short_name = 'm19'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm19'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Waterknot'),
    (select id from sets where short_name = 'm19'),
    '311',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mist-Cloaked Herald'),
    (select id from sets where short_name = 'm19'),
    '310',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tezzeret''s Gatebreaker'),
    (select id from sets where short_name = 'm19'),
    '289',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Resplendent Angel'),
    (select id from sets where short_name = 'm19'),
    '34',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Skilled Animator'),
    (select id from sets where short_name = 'm19'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sarkhan, Dragonsoul'),
    (select id from sets where short_name = 'm19'),
    '296',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Banefire'),
    (select id from sets where short_name = 'm19'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirror Image'),
    (select id from sets where short_name = 'm19'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon''s Hoard'),
    (select id from sets where short_name = 'm19'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cleansing Nova'),
    (select id from sets where short_name = 'm19'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sun Sentinel'),
    (select id from sets where short_name = 'm19'),
    '307',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diregraf Ghoul'),
    (select id from sets where short_name = 'm19'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aether Tunnel'),
    (select id from sets where short_name = 'm19'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Supreme Phantom'),
    (select id from sets where short_name = 'm19'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Poison-Tip Archer'),
    (select id from sets where short_name = 'm19'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mentor of the Meek'),
    (select id from sets where short_name = 'm19'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Explosive Apparatus'),
    (select id from sets where short_name = 'm19'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Electrify'),
    (select id from sets where short_name = 'm19'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cavalry Drillmaster'),
    (select id from sets where short_name = 'm19'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chaos Wand'),
    (select id from sets where short_name = 'm19'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm19'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyrider Patrol'),
    (select id from sets where short_name = 'm19'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Regal Bloodlord'),
    (select id from sets where short_name = 'm19'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Take Vengeance'),
    (select id from sets where short_name = 'm19'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravenous Harpy'),
    (select id from sets where short_name = 'm19'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vivien Reid'),
    (select id from sets where short_name = 'm19'),
    '208',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dark-Dweller Oracle'),
    (select id from sets where short_name = 'm19'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tattered Mummy'),
    (select id from sets where short_name = 'm19'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Valiant Knight'),
    (select id from sets where short_name = 'm19'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foul Orchard'),
    (select id from sets where short_name = 'm19'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enigma Drake'),
    (select id from sets where short_name = 'm19'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blood Divination'),
    (select id from sets where short_name = 'm19'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm19'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Vanguard'),
    (select id from sets where short_name = 'm19'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Vines'),
    (select id from sets where short_name = 'm19'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight''s Pledge'),
    (select id from sets where short_name = 'm19'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'm19'),
    '314',
    'common'
) ,
(
    (select id from mtgcard where name = 'Isareth the Awakener'),
    (select id from sets where short_name = 'm19'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Contract'),
    (select id from sets where short_name = 'm19'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hired Blade'),
    (select id from sets where short_name = 'm19'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Root Snare'),
    (select id from sets where short_name = 'm19'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Druid of the Cowl'),
    (select id from sets where short_name = 'm19'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gearsmith Prodigy'),
    (select id from sets where short_name = 'm19'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Declare Dominance'),
    (select id from sets where short_name = 'm19'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vivien''s Invocation'),
    (select id from sets where short_name = 'm19'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plummet'),
    (select id from sets where short_name = 'm19'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tranquil Expanse'),
    (select id from sets where short_name = 'm19'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pendulum of Patterns'),
    (select id from sets where short_name = 'm19'),
    '288',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, the Ravager // Nicol Bolas, the Arisen'),
    (select id from sets where short_name = 'm19'),
    '218',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Daggerback Basilisk'),
    (select id from sets where short_name = 'm19'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Invoke the Divine'),
    (select id from sets where short_name = 'm19'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Priest'),
    (select id from sets where short_name = 'm19'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horizon Scholar'),
    (select id from sets where short_name = 'm19'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = 'm19'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Colossal Dreadmaw'),
    (select id from sets where short_name = 'm19'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inspired Charge'),
    (select id from sets where short_name = 'm19'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reliquary Tower'),
    (select id from sets where short_name = 'm19'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trusty Packbeast'),
    (select id from sets where short_name = 'm19'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hungering Hydra'),
    (select id from sets where short_name = 'm19'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = 'm19'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm19'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivien of the Arkbow'),
    (select id from sets where short_name = 'm19'),
    '301',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tezzeret''s Strider'),
    (select id from sets where short_name = 'm19'),
    '290',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Uncomfortable Chill'),
    (select id from sets where short_name = 'm19'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skeleton Archer'),
    (select id from sets where short_name = 'm19'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Surge Mare'),
    (select id from sets where short_name = 'm19'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Instigator'),
    (select id from sets where short_name = 'm19'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Act of Treason'),
    (select id from sets where short_name = 'm19'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm19'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loxodon Line Breaker'),
    (select id from sets where short_name = 'm19'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Radiating Lightning'),
    (select id from sets where short_name = 'm19'),
    '313',
    'common'
) ,
(
    (select id from mtgcard where name = 'Colossal Majesty'),
    (select id from sets where short_name = 'm19'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Omenspeaker'),
    (select id from sets where short_name = 'm19'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Motivator'),
    (select id from sets where short_name = 'm19'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spit Flame'),
    (select id from sets where short_name = 'm19'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Remorseful Cleric'),
    (select id from sets where short_name = 'm19'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Essence Scatter'),
    (select id from sets where short_name = 'm19'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plague Mare'),
    (select id from sets where short_name = 'm19'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bristling Boar'),
    (select id from sets where short_name = 'm19'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smelt'),
    (select id from sets where short_name = 'm19'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inferno Hellion'),
    (select id from sets where short_name = 'm19'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Open the Graves'),
    (select id from sets where short_name = 'm19'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infernal Scarring'),
    (select id from sets where short_name = 'm19'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cancel'),
    (select id from sets where short_name = 'm19'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Switcheroo'),
    (select id from sets where short_name = 'm19'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Luminous Bonds'),
    (select id from sets where short_name = 'm19'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manalith'),
    (select id from sets where short_name = 'm19'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Onakke Ogre'),
    (select id from sets where short_name = 'm19'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grasping Scoundrel'),
    (select id from sets where short_name = 'm19'),
    '312',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talons of Wildwood'),
    (select id from sets where short_name = 'm19'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anticipate'),
    (select id from sets where short_name = 'm19'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dismissive Pyromancer'),
    (select id from sets where short_name = 'm19'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Doomed Dissenter'),
    (select id from sets where short_name = 'm19'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marauder''s Axe'),
    (select id from sets where short_name = 'm19'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Draconic Disciple'),
    (select id from sets where short_name = 'm19'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mistcaller'),
    (select id from sets where short_name = 'm19'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reassembling Skeleton'),
    (select id from sets where short_name = 'm19'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Windreader Sphinx'),
    (select id from sets where short_name = 'm19'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phylactery Lich'),
    (select id from sets where short_name = 'm19'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana, Untouched by Death'),
    (select id from sets where short_name = 'm19'),
    '106',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Viashino Pyromancer'),
    (select id from sets where short_name = 'm19'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crash Through'),
    (select id from sets where short_name = 'm19'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hostile Minotaur'),
    (select id from sets where short_name = 'm19'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Dragon'),
    (select id from sets where short_name = 'm19'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm19'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Warleader'),
    (select id from sets where short_name = 'm19'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forsaken Sanctuary'),
    (select id from sets where short_name = 'm19'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'm19'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runic Armasaur'),
    (select id from sets where short_name = 'm19'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani, Wise Counselor'),
    (select id from sets where short_name = 'm19'),
    '281',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm19'),
    '277',
    'common'
) ,
(
    (select id from mtgcard where name = 'Highland Lake'),
    (select id from sets where short_name = 'm19'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rupture Spire'),
    (select id from sets where short_name = 'm19'),
    '255',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sarkhan''s Dragonfire'),
    (select id from sets where short_name = 'm19'),
    '298',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vine Mare'),
    (select id from sets where short_name = 'm19'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prodigious Growth'),
    (select id from sets where short_name = 'm19'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone Quarry'),
    (select id from sets where short_name = 'm19'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Centaur Courser'),
    (select id from sets where short_name = 'm19'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm19'),
    '276',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guttersnipe'),
    (select id from sets where short_name = 'm19'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rabid Bite'),
    (select id from sets where short_name = 'm19'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sparktongue Dragon'),
    (select id from sets where short_name = 'm19'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rise from the Grave'),
    (select id from sets where short_name = 'm19'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyscanner'),
    (select id from sets where short_name = 'm19'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doublecast'),
    (select id from sets where short_name = 'm19'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'm19'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gift of Paradise'),
    (select id from sets where short_name = 'm19'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gigantosaurus'),
    (select id from sets where short_name = 'm19'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm19'),
    '278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Millstone'),
    (select id from sets where short_name = 'm19'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daybreak Chaplain'),
    (select id from sets where short_name = 'm19'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lava Axe'),
    (select id from sets where short_name = 'm19'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Star-Crowned Stag'),
    (select id from sets where short_name = 'm19'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rustwing Falcon'),
    (select id from sets where short_name = 'm19'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trumpet Blast'),
    (select id from sets where short_name = 'm19'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Apex of Power'),
    (select id from sets where short_name = 'm19'),
    '129',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Welcome'),
    (select id from sets where short_name = 'm19'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death Baron'),
    (select id from sets where short_name = 'm19'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diamond Mare'),
    (select id from sets where short_name = 'm19'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dwindle'),
    (select id from sets where short_name = 'm19'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divination'),
    (select id from sets where short_name = 'm19'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scholar of Stars'),
    (select id from sets where short_name = 'm19'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exclusion Mage'),
    (select id from sets where short_name = 'm19'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Suspicious Bookcase'),
    (select id from sets where short_name = 'm19'),
    '246',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Rejuvenator'),
    (select id from sets where short_name = 'm19'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infectious Horror'),
    (select id from sets where short_name = 'm19'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vaevictis Asmadi, the Dire'),
    (select id from sets where short_name = 'm19'),
    '225',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shield Mare'),
    (select id from sets where short_name = 'm19'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Egg'),
    (select id from sets where short_name = 'm19'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = 'm19'),
    '300',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Influence'),
    (select id from sets where short_name = 'm19'),
    '282',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Amulet of Safekeeping'),
    (select id from sets where short_name = 'm19'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghastbark Twins'),
    (select id from sets where short_name = 'm19'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vivien''s Jaguar'),
    (select id from sets where short_name = 'm19'),
    '305',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demon of Catastrophes'),
    (select id from sets where short_name = 'm19'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cinder Barrens'),
    (select id from sets where short_name = 'm19'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Omniscience'),
    (select id from sets where short_name = 'm19'),
    '65',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Court Cleric'),
    (select id from sets where short_name = 'm19'),
    '283',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mighty Leap'),
    (select id from sets where short_name = 'm19'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abnormal Endurance'),
    (select id from sets where short_name = 'm19'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Submerged Boneyard'),
    (select id from sets where short_name = 'm19'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Revitalize'),
    (select id from sets where short_name = 'm19'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frilled Sea Serpent'),
    (select id from sets where short_name = 'm19'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sleep'),
    (select id from sets where short_name = 'm19'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Field Creeper'),
    (select id from sets where short_name = 'm19'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Satyr Enchanter'),
    (select id from sets where short_name = 'm19'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gallant Cavalry'),
    (select id from sets where short_name = 'm19'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arisen Gorgon'),
    (select id from sets where short_name = 'm19'),
    '292',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm19'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tezzeret, Artifice Master'),
    (select id from sets where short_name = 'm19'),
    '79',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ursine Champion'),
    (select id from sets where short_name = 'm19'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aviation Pioneer'),
    (select id from sets where short_name = 'm19'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Catalyst Elemental'),
    (select id from sets where short_name = 'm19'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fell Specter'),
    (select id from sets where short_name = 'm19'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Spoils'),
    (select id from sets where short_name = 'm19'),
    '294',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm19'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crucible of Worlds'),
    (select id from sets where short_name = 'm19'),
    '229',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Magistrate''s Scepter'),
    (select id from sets where short_name = 'm19'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Siegebreaker Giant'),
    (select id from sets where short_name = 'm19'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sarkhan''s Unsealing'),
    (select id from sets where short_name = 'm19'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampire Sovereign'),
    (select id from sets where short_name = 'm19'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gravewaker'),
    (select id from sets where short_name = 'm19'),
    '293',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Metamorphic Alteration'),
    (select id from sets where short_name = 'm19'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fraying Omnipotence'),
    (select id from sets where short_name = 'm19'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Recollect'),
    (select id from sets where short_name = 'm19'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightmare''s Thirst'),
    (select id from sets where short_name = 'm19'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nexus of Fate'),
    (select id from sets where short_name = 'm19'),
    '306',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Skalla Wolf'),
    (select id from sets where short_name = 'm19'),
    '303',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meandering River'),
    (select id from sets where short_name = 'm19'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Macabre Waltz'),
    (select id from sets where short_name = 'm19'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of the Dawn'),
    (select id from sets where short_name = 'm19'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thornhide Wolves'),
    (select id from sets where short_name = 'm19'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Elemental'),
    (select id from sets where short_name = 'm19'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm19'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Titanic Growth'),
    (select id from sets where short_name = 'm19'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm19'),
    '275',
    'common'
) 
 on conflict do nothing;
