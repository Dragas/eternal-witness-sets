insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Air Servant'),
    (select id from sets where short_name = 'w16'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oakenform'),
    (select id from sets where short_name = 'w16'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = 'w16'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Borderland Marauder'),
    (select id from sets where short_name = 'w16'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aegis Angel'),
    (select id from sets where short_name = 'w16'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = 'w16'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'w16'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Walking Corpse'),
    (select id from sets where short_name = 'w16'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cone of Flame'),
    (select id from sets where short_name = 'w16'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = 'w16'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disperse'),
    (select id from sets where short_name = 'w16'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = 'w16'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul of the Harvest'),
    (select id from sets where short_name = 'w16'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphinx of Magosi'),
    (select id from sets where short_name = 'w16'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Incremental Growth'),
    (select id from sets where short_name = 'w16'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marked by Honor'),
    (select id from sets where short_name = 'w16'),
    '2',
    'common'
) 
 on conflict do nothing;
