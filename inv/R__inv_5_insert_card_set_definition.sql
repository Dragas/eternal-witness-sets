insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Tsabo''s Assassin'),
    (select id from sets where short_name = 'inv'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Spring'),
    (select id from sets where short_name = 'inv'),
    '319',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Care'),
    (select id from sets where short_name = 'inv'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Might Weaver'),
    (select id from sets where short_name = 'inv'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rewards of Diversity'),
    (select id from sets where short_name = 'inv'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tectonic Instability'),
    (select id from sets where short_name = 'inv'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sky Weaver'),
    (select id from sets where short_name = 'inv'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ordered Migration'),
    (select id from sets where short_name = 'inv'),
    '258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spite // Malice'),
    (select id from sets where short_name = 'inv'),
    '293',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Do or Die'),
    (select id from sets where short_name = 'inv'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chaotic Strike'),
    (select id from sets where short_name = 'inv'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Devouring Strossus'),
    (select id from sets where short_name = 'inv'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scarred Puma'),
    (select id from sets where short_name = 'inv'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slinking Serpent'),
    (select id from sets where short_name = 'inv'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Samite Archer'),
    (select id from sets where short_name = 'inv'),
    '269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'inv'),
    '349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tsabo''s Decree'),
    (select id from sets where short_name = 'inv'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seer''s Vision'),
    (select id from sets where short_name = 'inv'),
    '270',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plague Spores'),
    (select id from sets where short_name = 'inv'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Maze'),
    (select id from sets where short_name = 'inv'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prohibit'),
    (select id from sets where short_name = 'inv'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tsabo''s Web'),
    (select id from sets where short_name = 'inv'),
    '317',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Assault // Battery'),
    (select id from sets where short_name = 'inv'),
    '295',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whip Silk'),
    (select id from sets where short_name = 'inv'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cremate'),
    (select id from sets where short_name = 'inv'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Restock'),
    (select id from sets where short_name = 'inv'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Galina''s Knight'),
    (select id from sets where short_name = 'inv'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rooting Kavu'),
    (select id from sets where short_name = 'inv'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heroes'' Reunion'),
    (select id from sets where short_name = 'inv'),
    '250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Benalish Heralds'),
    (select id from sets where short_name = 'inv'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firescreamer'),
    (select id from sets where short_name = 'inv'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frenzied Tilling'),
    (select id from sets where short_name = 'inv'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Kavu'),
    (select id from sets where short_name = 'inv'),
    '291',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wax // Wane'),
    (select id from sets where short_name = 'inv'),
    '296',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Strength of Unity'),
    (select id from sets where short_name = 'inv'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trench Wurm'),
    (select id from sets where short_name = 'inv'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Breaking Wave'),
    (select id from sets where short_name = 'inv'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scouting Trek'),
    (select id from sets where short_name = 'inv'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Well-Laid Plans'),
    (select id from sets where short_name = 'inv'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tolarian Emissary'),
    (select id from sets where short_name = 'inv'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stormscape Apprentice'),
    (select id from sets where short_name = 'inv'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alloy Golem'),
    (select id from sets where short_name = 'inv'),
    '297',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kavu Climber'),
    (select id from sets where short_name = 'inv'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Cavalry'),
    (select id from sets where short_name = 'inv'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Infiltrator'),
    (select id from sets where short_name = 'inv'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thunderscape Master'),
    (select id from sets where short_name = 'inv'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sulfur Vent'),
    (select id from sets where short_name = 'inv'),
    '328',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pincer Spider'),
    (select id from sets where short_name = 'inv'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undermine'),
    (select id from sets where short_name = 'inv'),
    '282',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reckless Spite'),
    (select id from sets where short_name = 'inv'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Planar Portal'),
    (select id from sets where short_name = 'inv'),
    '308',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'inv'),
    '348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dromar''s Attendant'),
    (select id from sets where short_name = 'inv'),
    '303',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Altar'),
    (select id from sets where short_name = 'inv'),
    '306',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dream Thrush'),
    (select id from sets where short_name = 'inv'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tigereye Cameo'),
    (select id from sets where short_name = 'inv'),
    '314',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raging Kavu'),
    (select id from sets where short_name = 'inv'),
    '262',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Divine Presence'),
    (select id from sets where short_name = 'inv'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'inv'),
    '339',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scorching Lava'),
    (select id from sets where short_name = 'inv'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Spy'),
    (select id from sets where short_name = 'inv'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Saproling Symbiosis'),
    (select id from sets where short_name = 'inv'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kavu Runner'),
    (select id from sets where short_name = 'inv'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kavu Titan'),
    (select id from sets where short_name = 'inv'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Canopy Surge'),
    (select id from sets where short_name = 'inv'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tainted Well'),
    (select id from sets where short_name = 'inv'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'inv'),
    '336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quirion Sentinel'),
    (select id from sets where short_name = 'inv'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'inv'),
    '342',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vicious Kavu'),
    (select id from sets where short_name = 'inv'),
    '284',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Callous Giant'),
    (select id from sets where short_name = 'inv'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Global Ruin'),
    (select id from sets where short_name = 'inv'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jade Leech'),
    (select id from sets where short_name = 'inv'),
    '190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sleeper''s Robe'),
    (select id from sets where short_name = 'inv'),
    '273',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harsh Judgment'),
    (select id from sets where short_name = 'inv'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savage Offensive'),
    (select id from sets where short_name = 'inv'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shackles'),
    (select id from sets where short_name = 'inv'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archaeological Dig'),
    (select id from sets where short_name = 'inv'),
    '320',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meteor Storm'),
    (select id from sets where short_name = 'inv'),
    '256',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Halam Djinn'),
    (select id from sets where short_name = 'inv'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aggressive Urge'),
    (select id from sets where short_name = 'inv'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aura Mutation'),
    (select id from sets where short_name = 'inv'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dismantling Blow'),
    (select id from sets where short_name = 'inv'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urborg Phantom'),
    (select id from sets where short_name = 'inv'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duskwalker'),
    (select id from sets where short_name = 'inv'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ardent Soldier'),
    (select id from sets where short_name = 'inv'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Benalish Lancer'),
    (select id from sets where short_name = 'inv'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Juntu Stakes'),
    (select id from sets where short_name = 'inv'),
    '304',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kangee, Aerie Keeper'),
    (select id from sets where short_name = 'inv'),
    '253',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nomadic Elf'),
    (select id from sets where short_name = 'inv'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blazing Specter'),
    (select id from sets where short_name = 'inv'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twilight''s Call'),
    (select id from sets where short_name = 'inv'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maniacal Rage'),
    (select id from sets where short_name = 'inv'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elite'),
    (select id from sets where short_name = 'inv'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Noble Panther'),
    (select id from sets where short_name = 'inv'),
    '257',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sterling Grove'),
    (select id from sets where short_name = 'inv'),
    '278',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overabundance'),
    (select id from sets where short_name = 'inv'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Troll-Horn Cameo'),
    (select id from sets where short_name = 'inv'),
    '316',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prison Barricade'),
    (select id from sets where short_name = 'inv'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wallop'),
    (select id from sets where short_name = 'inv'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crosis''s Attendant'),
    (select id from sets where short_name = 'inv'),
    '300',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mourning'),
    (select id from sets where short_name = 'inv'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fight or Flight'),
    (select id from sets where short_name = 'inv'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reviving Vapors'),
    (select id from sets where short_name = 'inv'),
    '265',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Harvest'),
    (select id from sets where short_name = 'inv'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Breath of Darigaaz'),
    (select id from sets where short_name = 'inv'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Annihilate'),
    (select id from sets where short_name = 'inv'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drake-Skull Cameo'),
    (select id from sets where short_name = 'inv'),
    '302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viashino Grappler'),
    (select id from sets where short_name = 'inv'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blurred Mongoose'),
    (select id from sets where short_name = 'inv'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faerie Squadron'),
    (select id from sets where short_name = 'inv'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barrin''s Unmaking'),
    (select id from sets where short_name = 'inv'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scavenged Weaponry'),
    (select id from sets where short_name = 'inv'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treva, the Renewer'),
    (select id from sets where short_name = 'inv'),
    '280',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keldon Necropolis'),
    (select id from sets where short_name = 'inv'),
    '325',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Searing Rays'),
    (select id from sets where short_name = 'inv'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Metathran Transport'),
    (select id from sets where short_name = 'inv'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thornscape Apprentice'),
    (select id from sets where short_name = 'inv'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yawgmoth''s Agenda'),
    (select id from sets where short_name = 'inv'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crypt Angel'),
    (select id from sets where short_name = 'inv'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dueling Grounds'),
    (select id from sets where short_name = 'inv'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Agonizing Demise'),
    (select id from sets where short_name = 'inv'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hooded Kavu'),
    (select id from sets where short_name = 'inv'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Dart'),
    (select id from sets where short_name = 'inv'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darigaaz, the Igniter'),
    (select id from sets where short_name = 'inv'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orim''s Touch'),
    (select id from sets where short_name = 'inv'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Verduran Emissary'),
    (select id from sets where short_name = 'inv'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zanam Djinn'),
    (select id from sets where short_name = 'inv'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wayfaring Giant'),
    (select id from sets where short_name = 'inv'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Charging Troll'),
    (select id from sets where short_name = 'inv'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Metathran Aerostat'),
    (select id from sets where short_name = 'inv'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crown of Flames'),
    (select id from sets where short_name = 'inv'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Shield'),
    (select id from sets where short_name = 'inv'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel of Mercy'),
    (select id from sets where short_name = 'inv'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'inv'),
    '331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manipulate Fate'),
    (select id from sets where short_name = 'inv'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blind Seer'),
    (select id from sets where short_name = 'inv'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Absorb'),
    (select id from sets where short_name = 'inv'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Slayer'),
    (select id from sets where short_name = 'inv'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit of Resistance'),
    (select id from sets where short_name = 'inv'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stand or Fall'),
    (select id from sets where short_name = 'inv'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Rage'),
    (select id from sets where short_name = 'inv'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rogue Kavu'),
    (select id from sets where short_name = 'inv'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Void'),
    (select id from sets where short_name = 'inv'),
    '287',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elfhame Palace'),
    (select id from sets where short_name = 'inv'),
    '322',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Metathran Zombie'),
    (select id from sets where short_name = 'inv'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Filter'),
    (select id from sets where short_name = 'inv'),
    '318',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Empress Galina'),
    (select id from sets where short_name = 'inv'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dredge'),
    (select id from sets where short_name = 'inv'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'inv'),
    '345',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spinal Embrace'),
    (select id from sets where short_name = 'inv'),
    '276',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aether Rift'),
    (select id from sets where short_name = 'inv'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serpentine Kavu'),
    (select id from sets where short_name = 'inv'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desperate Research'),
    (select id from sets where short_name = 'inv'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elfhame Sanctuary'),
    (select id from sets where short_name = 'inv'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tangle'),
    (select id from sets where short_name = 'inv'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Benalish Emissary'),
    (select id from sets where short_name = 'inv'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyre Zombie'),
    (select id from sets where short_name = 'inv'),
    '261',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barrin''s Spite'),
    (select id from sets where short_name = 'inv'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Andradite Leech'),
    (select id from sets where short_name = 'inv'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wandering Stream'),
    (select id from sets where short_name = 'inv'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armored Guardian'),
    (select id from sets where short_name = 'inv'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urborg Skeleton'),
    (select id from sets where short_name = 'inv'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Lens'),
    (select id from sets where short_name = 'inv'),
    '307',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darigaaz''s Attendant'),
    (select id from sets where short_name = 'inv'),
    '301',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Burn'),
    (select id from sets where short_name = 'inv'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Utopia Tree'),
    (select id from sets where short_name = 'inv'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vodalian Hypnotist'),
    (select id from sets where short_name = 'inv'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Verdeloth the Ancient'),
    (select id from sets where short_name = 'inv'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Llanowar Vanguard'),
    (select id from sets where short_name = 'inv'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treefolk Healer'),
    (select id from sets where short_name = 'inv'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightscape Apprentice'),
    (select id from sets where short_name = 'inv'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temporal Distortion'),
    (select id from sets where short_name = 'inv'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zap'),
    (select id from sets where short_name = 'inv'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molimo, Maro-Sorcerer'),
    (select id from sets where short_name = 'inv'),
    '199',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rampant Elephant'),
    (select id from sets where short_name = 'inv'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunderscape Apprentice'),
    (select id from sets where short_name = 'inv'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wash Out'),
    (select id from sets where short_name = 'inv'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Recover'),
    (select id from sets where short_name = 'inv'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghitu Fire'),
    (select id from sets where short_name = 'inv'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fertile Ground'),
    (select id from sets where short_name = 'inv'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sulam Djinn'),
    (select id from sets where short_name = 'inv'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'inv'),
    '340',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riptide Crab'),
    (select id from sets where short_name = 'inv'),
    '266',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rainbow Crow'),
    (select id from sets where short_name = 'inv'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pouncing Kavu'),
    (select id from sets where short_name = 'inv'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'inv'),
    '346',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thornscape Master'),
    (select id from sets where short_name = 'inv'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vile Consumption'),
    (select id from sets where short_name = 'inv'),
    '285',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fact or Fiction'),
    (select id from sets where short_name = 'inv'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Razorfoot Griffin'),
    (select id from sets where short_name = 'inv'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pain // Suffering'),
    (select id from sets where short_name = 'inv'),
    '294',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death or Glory'),
    (select id from sets where short_name = 'inv'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hate Weaver'),
    (select id from sets where short_name = 'inv'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sabertooth Nishoba'),
    (select id from sets where short_name = 'inv'),
    '268',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hanna, Ship''s Navigator'),
    (select id from sets where short_name = 'inv'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simoon'),
    (select id from sets where short_name = 'inv'),
    '272',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Saproling Infestation'),
    (select id from sets where short_name = 'inv'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Obsidian Acolyte'),
    (select id from sets where short_name = 'inv'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychic Battle'),
    (select id from sets where short_name = 'inv'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crystal Spray'),
    (select id from sets where short_name = 'inv'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'inv'),
    '333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blinding Light'),
    (select id from sets where short_name = 'inv'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plague Spitter'),
    (select id from sets where short_name = 'inv'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hunting Kavu'),
    (select id from sets where short_name = 'inv'),
    '252',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bog Initiate'),
    (select id from sets where short_name = 'inv'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravenous Rats'),
    (select id from sets where short_name = 'inv'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'inv'),
    '335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reviving Dose'),
    (select id from sets where short_name = 'inv'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cauldron Dance'),
    (select id from sets where short_name = 'inv'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Barbarian'),
    (select id from sets where short_name = 'inv'),
    '290',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slimy Kavu'),
    (select id from sets where short_name = 'inv'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exotic Curse'),
    (select id from sets where short_name = 'inv'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Worldly Counsel'),
    (select id from sets where short_name = 'inv'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Atalya, Samite Master'),
    (select id from sets where short_name = 'inv'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Essence Leak'),
    (select id from sets where short_name = 'inv'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spirit Weaver'),
    (select id from sets where short_name = 'inv'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Turf Wound'),
    (select id from sets where short_name = 'inv'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chromatic Sphere'),
    (select id from sets where short_name = 'inv'),
    '299',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vodalian Serpent'),
    (select id from sets where short_name = 'inv'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Emissary'),
    (select id from sets where short_name = 'inv'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rout'),
    (select id from sets where short_name = 'inv'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Opt'),
    (select id from sets where short_name = 'inv'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shimmering Wings'),
    (select id from sets where short_name = 'inv'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liberate'),
    (select id from sets where short_name = 'inv'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fires of Yavimaya'),
    (select id from sets where short_name = 'inv'),
    '246',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquility'),
    (select id from sets where short_name = 'inv'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'inv'),
    '344',
    'common'
) ,
(
    (select id from mtgcard where name = 'Traveler''s Cloak'),
    (select id from sets where short_name = 'inv'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Irrigation Ditch'),
    (select id from sets where short_name = 'inv'),
    '324',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urborg Drake'),
    (select id from sets where short_name = 'inv'),
    '283',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Oasis'),
    (select id from sets where short_name = 'inv'),
    '327',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treva''s Attendant'),
    (select id from sets where short_name = 'inv'),
    '315',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tinder Farm'),
    (select id from sets where short_name = 'inv'),
    '329',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'inv'),
    '341',
    'common'
) ,
(
    (select id from mtgcard where name = 'Repulse'),
    (select id from sets where short_name = 'inv'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tribal Flames'),
    (select id from sets where short_name = 'inv'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormscape Master'),
    (select id from sets where short_name = 'inv'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armadillo Cloak'),
    (select id from sets where short_name = 'inv'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voracious Cobra'),
    (select id from sets where short_name = 'inv'),
    '288',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Moat'),
    (select id from sets where short_name = 'inv'),
    '279',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seashell Cameo'),
    (select id from sets where short_name = 'inv'),
    '311',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rith''s Attendant'),
    (select id from sets where short_name = 'inv'),
    '310',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kavu Aggressor'),
    (select id from sets where short_name = 'inv'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horned Cheetah'),
    (select id from sets where short_name = 'inv'),
    '251',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Probe'),
    (select id from sets where short_name = 'inv'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruham Djinn'),
    (select id from sets where short_name = 'inv'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crusading Knight'),
    (select id from sets where short_name = 'inv'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rage Weaver'),
    (select id from sets where short_name = 'inv'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tidal Visionary'),
    (select id from sets where short_name = 'inv'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Cloud'),
    (select id from sets where short_name = 'inv'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunscape Apprentice'),
    (select id from sets where short_name = 'inv'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Holy Day'),
    (select id from sets where short_name = 'inv'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spreading Plague'),
    (select id from sets where short_name = 'inv'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exclude'),
    (select id from sets where short_name = 'inv'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tower Drake'),
    (select id from sets where short_name = 'inv'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vigorous Charge'),
    (select id from sets where short_name = 'inv'),
    '222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Samite Ministration'),
    (select id from sets where short_name = 'inv'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'inv'),
    '334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Recoil'),
    (select id from sets where short_name = 'inv'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bend or Break'),
    (select id from sets where short_name = 'inv'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cinder Shade'),
    (select id from sets where short_name = 'inv'),
    '240',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'inv'),
    '343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pure Reflection'),
    (select id from sets where short_name = 'inv'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'inv'),
    '332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wings of Hope'),
    (select id from sets where short_name = 'inv'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruby Leech'),
    (select id from sets where short_name = 'inv'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skittish Kavu'),
    (select id from sets where short_name = 'inv'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stand // Deliver'),
    (select id from sets where short_name = 'inv'),
    '292',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disrupt'),
    (select id from sets where short_name = 'inv'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crimson Acolyte'),
    (select id from sets where short_name = 'inv'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Champion'),
    (select id from sets where short_name = 'inv'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vodalian Merchant'),
    (select id from sets where short_name = 'inv'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tek'),
    (select id from sets where short_name = 'inv'),
    '313',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urborg Skeleton'),
    (select id from sets where short_name = 'inv'),
    '134s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quirion Trailblazer'),
    (select id from sets where short_name = 'inv'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rith, the Awakener'),
    (select id from sets where short_name = 'inv'),
    '267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mages'' Contest'),
    (select id from sets where short_name = 'inv'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quirion Elves'),
    (select id from sets where short_name = 'inv'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urborg Volcano'),
    (select id from sets where short_name = 'inv'),
    '330',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alabaster Leech'),
    (select id from sets where short_name = 'inv'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Kavu'),
    (select id from sets where short_name = 'inv'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overload'),
    (select id from sets where short_name = 'inv'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kavu Scout'),
    (select id from sets where short_name = 'inv'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Collapsing Borders'),
    (select id from sets where short_name = 'inv'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tsabo Tavoc'),
    (select id from sets where short_name = 'inv'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Reaper'),
    (select id from sets where short_name = 'inv'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obliterate'),
    (select id from sets where short_name = 'inv'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marauding Knight'),
    (select id from sets where short_name = 'inv'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Distorting Wake'),
    (select id from sets where short_name = 'inv'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bind'),
    (select id from sets where short_name = 'inv'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'inv'),
    '350',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dromar, the Banisher'),
    (select id from sets where short_name = 'inv'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'inv'),
    '338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coastal Tower'),
    (select id from sets where short_name = 'inv'),
    '321',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stun'),
    (select id from sets where short_name = 'inv'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Captain Sisay'),
    (select id from sets where short_name = 'inv'),
    '237',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Artifact Mutation'),
    (select id from sets where short_name = 'inv'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winnow'),
    (select id from sets where short_name = 'inv'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goham Djinn'),
    (select id from sets where short_name = 'inv'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pledge of Loyalty'),
    (select id from sets where short_name = 'inv'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Collective Restraint'),
    (select id from sets where short_name = 'inv'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sparring Golem'),
    (select id from sets where short_name = 'inv'),
    '312',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightscape Master'),
    (select id from sets where short_name = 'inv'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shoreline Raider'),
    (select id from sets where short_name = 'inv'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Battleflies'),
    (select id from sets where short_name = 'inv'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Backlash'),
    (select id from sets where short_name = 'inv'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pulse of Llanowar'),
    (select id from sets where short_name = 'inv'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Defiling Tears'),
    (select id from sets where short_name = 'inv'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skizzik'),
    (select id from sets where short_name = 'inv'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coalition Victory'),
    (select id from sets where short_name = 'inv'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lotus Guardian'),
    (select id from sets where short_name = 'inv'),
    '305',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aura Shards'),
    (select id from sets where short_name = 'inv'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reya Dawnbringer'),
    (select id from sets where short_name = 'inv'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benalish Trapper'),
    (select id from sets where short_name = 'inv'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crosis, the Purger'),
    (select id from sets where short_name = 'inv'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Delver'),
    (select id from sets where short_name = 'inv'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sapphire Leech'),
    (select id from sets where short_name = 'inv'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urborg Emissary'),
    (select id from sets where short_name = 'inv'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llanowar Knight'),
    (select id from sets where short_name = 'inv'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Geothermal Crevice'),
    (select id from sets where short_name = 'inv'),
    '323',
    'common'
) ,
(
    (select id from mtgcard where name = 'Addle'),
    (select id from sets where short_name = 'inv'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Terrain'),
    (select id from sets where short_name = 'inv'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kavu Monarch'),
    (select id from sets where short_name = 'inv'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Explosive Growth'),
    (select id from sets where short_name = 'inv'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urborg Shambler'),
    (select id from sets where short_name = 'inv'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunscape Master'),
    (select id from sets where short_name = 'inv'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Capashen Unicorn'),
    (select id from sets where short_name = 'inv'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harrow'),
    (select id from sets where short_name = 'inv'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Armor'),
    (select id from sets where short_name = 'inv'),
    '309',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Salt Marsh'),
    (select id from sets where short_name = 'inv'),
    '326',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firebrand Ranger'),
    (select id from sets where short_name = 'inv'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Protective Sphere'),
    (select id from sets where short_name = 'inv'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glimmering Angel'),
    (select id from sets where short_name = 'inv'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vodalian Zombie'),
    (select id from sets where short_name = 'inv'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Response'),
    (select id from sets where short_name = 'inv'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kavu Lair'),
    (select id from sets where short_name = 'inv'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shivan Zombie'),
    (select id from sets where short_name = 'inv'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Restrain'),
    (select id from sets where short_name = 'inv'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kavu Chameleon'),
    (select id from sets where short_name = 'inv'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thicket Elemental'),
    (select id from sets where short_name = 'inv'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'inv'),
    '347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'inv'),
    '337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodstone Cameo'),
    (select id from sets where short_name = 'inv'),
    '298',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loafing Giant'),
    (select id from sets where short_name = 'inv'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cursed Flesh'),
    (select id from sets where short_name = 'inv'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smoldering Tar'),
    (select id from sets where short_name = 'inv'),
    '275',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reckless Assault'),
    (select id from sets where short_name = 'inv'),
    '263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lobotomy'),
    (select id from sets where short_name = 'inv'),
    '255',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sway of Illusion'),
    (select id from sets where short_name = 'inv'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stalking Assassin'),
    (select id from sets where short_name = 'inv'),
    '277',
    'rare'
) 
 on conflict do nothing;
