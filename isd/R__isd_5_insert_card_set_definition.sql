insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Invisible Stalker'),
    (select id from sets where short_name = 'isd'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Parallel Lives'),
    (select id from sets where short_name = 'isd'),
    '199',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smite the Monstrous'),
    (select id from sets where short_name = 'isd'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grasp of Phantoms'),
    (select id from sets where short_name = 'isd'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reckless Waif // Merciless Predator'),
    (select id from sets where short_name = 'isd'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Full Moon''s Rise'),
    (select id from sets where short_name = 'isd'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rolling Temblor'),
    (select id from sets where short_name = 'isd'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avacynian Priest'),
    (select id from sets where short_name = 'isd'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skirsdag High Priest'),
    (select id from sets where short_name = 'isd'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silverchase Fox'),
    (select id from sets where short_name = 'isd'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ambush Viper'),
    (select id from sets where short_name = 'isd'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disciple of Griselbrand'),
    (select id from sets where short_name = 'isd'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Intangible Virtue'),
    (select id from sets where short_name = 'isd'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Runechanter''s Pike'),
    (select id from sets where short_name = 'isd'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curse of Stalked Prey'),
    (select id from sets where short_name = 'isd'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Delver of Secrets // Insectile Aberration'),
    (select id from sets where short_name = 'isd'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hamlet Captain'),
    (select id from sets where short_name = 'isd'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battleground Geist'),
    (select id from sets where short_name = 'isd'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodline Keeper // Lord of Lineage'),
    (select id from sets where short_name = 'isd'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heartless Summoning'),
    (select id from sets where short_name = 'isd'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gallows Warden'),
    (select id from sets where short_name = 'isd'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thraben Purebloods'),
    (select id from sets where short_name = 'isd'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'isd'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curse of Death''s Hold'),
    (select id from sets where short_name = 'isd'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'isd'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Graveyard Shovel'),
    (select id from sets where short_name = 'isd'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unbreathing Horde'),
    (select id from sets where short_name = 'isd'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Geistflame'),
    (select id from sets where short_name = 'isd'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slayer of the Wicked'),
    (select id from sets where short_name = 'isd'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghost Quarter'),
    (select id from sets where short_name = 'isd'),
    '240',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moorland Haunt'),
    (select id from sets where short_name = 'isd'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lost in the Mist'),
    (select id from sets where short_name = 'isd'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trepanation Blade'),
    (select id from sets where short_name = 'isd'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelic Overseer'),
    (select id from sets where short_name = 'isd'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = 'isd'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Past in Flames'),
    (select id from sets where short_name = 'isd'),
    '155',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cobbled Wings'),
    (select id from sets where short_name = 'isd'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daybreak Ranger // Nightfall Predator'),
    (select id from sets where short_name = 'isd'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Army of the Damned'),
    (select id from sets where short_name = 'isd'),
    '87',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Memory''s Journey'),
    (select id from sets where short_name = 'isd'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rebuke'),
    (select id from sets where short_name = 'isd'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unburial Rites'),
    (select id from sets where short_name = 'isd'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Markov Patrician'),
    (select id from sets where short_name = 'isd'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corpse Lunge'),
    (select id from sets where short_name = 'isd'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hanweir Watchkeep // Bane of Hanweir'),
    (select id from sets where short_name = 'isd'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghoulcaller''s Chant'),
    (select id from sets where short_name = 'isd'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desperate Ravings'),
    (select id from sets where short_name = 'isd'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mausoleum Guard'),
    (select id from sets where short_name = 'isd'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'isd'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inquisitor''s Flail'),
    (select id from sets where short_name = 'isd'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woodland Sleuth'),
    (select id from sets where short_name = 'isd'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Festerhide Boar'),
    (select id from sets where short_name = 'isd'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kessig Wolf'),
    (select id from sets where short_name = 'isd'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elder of Laurels'),
    (select id from sets where short_name = 'isd'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Geist of Saint Traft'),
    (select id from sets where short_name = 'isd'),
    '213',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Splinterfright'),
    (select id from sets where short_name = 'isd'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frightful Delusion'),
    (select id from sets where short_name = 'isd'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'isd'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evil Twin'),
    (select id from sets where short_name = 'isd'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moan of the Unhallowed'),
    (select id from sets where short_name = 'isd'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crossway Vampire'),
    (select id from sets where short_name = 'isd'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paraselene'),
    (select id from sets where short_name = 'isd'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brain Weevil'),
    (select id from sets where short_name = 'isd'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gnaw to the Bone'),
    (select id from sets where short_name = 'isd'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Somberwald Spider'),
    (select id from sets where short_name = 'isd'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divine Reckoning'),
    (select id from sets where short_name = 'isd'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Typhoid Rats'),
    (select id from sets where short_name = 'isd'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skaab Ruinator'),
    (select id from sets where short_name = 'isd'),
    '77',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sensory Deprivation'),
    (select id from sets where short_name = 'isd'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Interloper'),
    (select id from sets where short_name = 'isd'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'isd'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unruly Mob'),
    (select id from sets where short_name = 'isd'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kindercatch'),
    (select id from sets where short_name = 'isd'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sulfur Falls'),
    (select id from sets where short_name = 'isd'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curse of the Nightly Hunt'),
    (select id from sets where short_name = 'isd'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kessig Wolf Run'),
    (select id from sets where short_name = 'isd'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakish Heir'),
    (select id from sets where short_name = 'isd'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grizzled Outcasts // Krallenhorde Wantons'),
    (select id from sets where short_name = 'isd'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sharpened Pitchfork'),
    (select id from sets where short_name = 'isd'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harvest Pyre'),
    (select id from sets where short_name = 'isd'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Altar''s Reap'),
    (select id from sets where short_name = 'isd'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'isd'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blazing Torch'),
    (select id from sets where short_name = 'isd'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diregraf Ghoul'),
    (select id from sets where short_name = 'isd'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mask of Avacyn'),
    (select id from sets where short_name = 'isd'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Night Terrors'),
    (select id from sets where short_name = 'isd'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blasphemous Act'),
    (select id from sets where short_name = 'isd'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rally the Peasants'),
    (select id from sets where short_name = 'isd'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urgent Exorcism'),
    (select id from sets where short_name = 'isd'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mulch'),
    (select id from sets where short_name = 'isd'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghoulraiser'),
    (select id from sets where short_name = 'isd'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Witchbane Orb'),
    (select id from sets where short_name = 'isd'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Think Twice'),
    (select id from sets where short_name = 'isd'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runic Repetition'),
    (select id from sets where short_name = 'isd'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gruesome Deformity'),
    (select id from sets where short_name = 'isd'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moonmist'),
    (select id from sets where short_name = 'isd'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gatstaf Shepherd // Gatstaf Howler'),
    (select id from sets where short_name = 'isd'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thraben Sentry // Thraben Militia'),
    (select id from sets where short_name = 'isd'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abattoir Ghoul'),
    (select id from sets where short_name = 'isd'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selfless Cathar'),
    (select id from sets where short_name = 'isd'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hysterical Blindness'),
    (select id from sets where short_name = 'isd'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bump in the Night'),
    (select id from sets where short_name = 'isd'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sever the Bloodline'),
    (select id from sets where short_name = 'isd'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Into the Maw of Hell'),
    (select id from sets where short_name = 'isd'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'One-Eyed Scarecrow'),
    (select id from sets where short_name = 'isd'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Geist-Honored Monk'),
    (select id from sets where short_name = 'isd'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dissipate'),
    (select id from sets where short_name = 'isd'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Endless Ranks of the Dead'),
    (select id from sets where short_name = 'isd'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bramblecrush'),
    (select id from sets where short_name = 'isd'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tormented Pariah // Rampaging Werewolf'),
    (select id from sets where short_name = 'isd'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doomed Traveler'),
    (select id from sets where short_name = 'isd'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woodland Cemetery'),
    (select id from sets where short_name = 'isd'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nevermore'),
    (select id from sets where short_name = 'isd'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodcrazed Neonate'),
    (select id from sets where short_name = 'isd'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brimstone Volley'),
    (select id from sets where short_name = 'isd'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manor Gargoyle'),
    (select id from sets where short_name = 'isd'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lumberknot'),
    (select id from sets where short_name = 'isd'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hollowhenge Scavenger'),
    (select id from sets where short_name = 'isd'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Curse of the Pierced Heart'),
    (select id from sets where short_name = 'isd'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undead Alchemist'),
    (select id from sets where short_name = 'isd'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hinterland Harbor'),
    (select id from sets where short_name = 'isd'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boneyard Wurm'),
    (select id from sets where short_name = 'isd'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wreath of Geists'),
    (select id from sets where short_name = 'isd'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Creepy Doll'),
    (select id from sets where short_name = 'isd'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feral Ridgewolf'),
    (select id from sets where short_name = 'isd'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chapel Geist'),
    (select id from sets where short_name = 'isd'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dead Weight'),
    (select id from sets where short_name = 'isd'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Civilized Scholar // Homicidal Brute'),
    (select id from sets where short_name = 'isd'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skaab Goliath'),
    (select id from sets where short_name = 'isd'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Purify the Grave'),
    (select id from sets where short_name = 'isd'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stitcher''s Apprentice'),
    (select id from sets where short_name = 'isd'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wooden Stake'),
    (select id from sets where short_name = 'isd'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avacyn''s Pilgrim'),
    (select id from sets where short_name = 'isd'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Laboratory Maniac'),
    (select id from sets where short_name = 'isd'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Traitorous Blood'),
    (select id from sets where short_name = 'isd'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ulvenwald Mystics // Ulvenwald Primordials'),
    (select id from sets where short_name = 'isd'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feeling of Dread'),
    (select id from sets where short_name = 'isd'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghoulcaller''s Bell'),
    (select id from sets where short_name = 'isd'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fortress Crab'),
    (select id from sets where short_name = 'isd'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Screeching Bat // Stalking Vampire'),
    (select id from sets where short_name = 'isd'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cackling Counterpart'),
    (select id from sets where short_name = 'isd'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grave Bramble'),
    (select id from sets where short_name = 'isd'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'isd'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Flight Alabaster'),
    (select id from sets where short_name = 'isd'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Walking Corpse'),
    (select id from sets where short_name = 'isd'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashmouth Hound'),
    (select id from sets where short_name = 'isd'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maw of the Mire'),
    (select id from sets where short_name = 'isd'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cellar Door'),
    (select id from sets where short_name = 'isd'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Caravan Vigil'),
    (select id from sets where short_name = 'isd'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stromkirk Noble'),
    (select id from sets where short_name = 'isd'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snapcaster Mage'),
    (select id from sets where short_name = 'isd'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silent Departure'),
    (select id from sets where short_name = 'isd'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moon Heron'),
    (select id from sets where short_name = 'isd'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shimmering Grotto'),
    (select id from sets where short_name = 'isd'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deranged Assistant'),
    (select id from sets where short_name = 'isd'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Village Cannibals'),
    (select id from sets where short_name = 'isd'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'isd'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Traveler''s Amulet'),
    (select id from sets where short_name = 'isd'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grimgrin, Corpse-Born'),
    (select id from sets where short_name = 'isd'),
    '214',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Charmbreaker Devils'),
    (select id from sets where short_name = 'isd'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gutter Grime'),
    (select id from sets where short_name = 'isd'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonmail Hauberk'),
    (select id from sets where short_name = 'isd'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orchard Spirit'),
    (select id from sets where short_name = 'isd'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rooftop Storm'),
    (select id from sets where short_name = 'isd'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mindshrieker'),
    (select id from sets where short_name = 'isd'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana of the Veil'),
    (select id from sets where short_name = 'isd'),
    '105',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stitched Drake'),
    (select id from sets where short_name = 'isd'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selhoff Occultist'),
    (select id from sets where short_name = 'isd'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balefire Dragon'),
    (select id from sets where short_name = 'isd'),
    '129',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mirror-Mad Phantasm'),
    (select id from sets where short_name = 'isd'),
    '68',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Curiosity'),
    (select id from sets where short_name = 'isd'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Isolated Chapel'),
    (select id from sets where short_name = 'isd'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infernal Plunge'),
    (select id from sets where short_name = 'isd'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk Relentless // Garruk, the Veil-Cursed'),
    (select id from sets where short_name = 'isd'),
    '181',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Spectral Rider'),
    (select id from sets where short_name = 'isd'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Back from the Brink'),
    (select id from sets where short_name = 'isd'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lantern Spirit'),
    (select id from sets where short_name = 'isd'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightbird''s Clutches'),
    (select id from sets where short_name = 'isd'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gavony Township'),
    (select id from sets where short_name = 'isd'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'isd'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'isd'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prey Upon'),
    (select id from sets where short_name = 'isd'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Claustrophobia'),
    (select id from sets where short_name = 'isd'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Creeping Renaissance'),
    (select id from sets where short_name = 'isd'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skeletal Grimace'),
    (select id from sets where short_name = 'isd'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Falkenrath Marauders'),
    (select id from sets where short_name = 'isd'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moment of Heroism'),
    (select id from sets where short_name = 'isd'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Instigator Gang // Wildblood Pack'),
    (select id from sets where short_name = 'isd'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mentor of the Meek'),
    (select id from sets where short_name = 'isd'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abbey Griffin'),
    (select id from sets where short_name = 'isd'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curse of Oblivion'),
    (select id from sets where short_name = 'isd'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elder Cathar'),
    (select id from sets where short_name = 'isd'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rotting Fensnake'),
    (select id from sets where short_name = 'isd'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mikaeus, the Lunarch'),
    (select id from sets where short_name = 'isd'),
    '23',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ghostly Possession'),
    (select id from sets where short_name = 'isd'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sturmgeist'),
    (select id from sets where short_name = 'isd'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tree of Redemption'),
    (select id from sets where short_name = 'isd'),
    '207',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Curse of the Bloody Tome'),
    (select id from sets where short_name = 'isd'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Devil''s Play'),
    (select id from sets where short_name = 'isd'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ranger''s Guile'),
    (select id from sets where short_name = 'isd'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elite Inquisitor'),
    (select id from sets where short_name = 'isd'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Village Bell-Ringer'),
    (select id from sets where short_name = 'isd'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stensia Bloodhall'),
    (select id from sets where short_name = 'isd'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bonds of Faith'),
    (select id from sets where short_name = 'isd'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spidery Grasp'),
    (select id from sets where short_name = 'isd'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riot Devils'),
    (select id from sets where short_name = 'isd'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Village Ironsmith // Ironfang'),
    (select id from sets where short_name = 'isd'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armored Skaab'),
    (select id from sets where short_name = 'isd'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'isd'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bitterheart Witch'),
    (select id from sets where short_name = 'isd'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Geistcatcher''s Rig'),
    (select id from sets where short_name = 'isd'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dearly Departed'),
    (select id from sets where short_name = 'isd'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spare from Evil'),
    (select id from sets where short_name = 'isd'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Make a Wish'),
    (select id from sets where short_name = 'isd'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Galvanic Juggernaut'),
    (select id from sets where short_name = 'isd'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'isd'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voiceless Spirit'),
    (select id from sets where short_name = 'isd'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grimoire of the Dead'),
    (select id from sets where short_name = 'isd'),
    '226',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Heretic''s Punishment'),
    (select id from sets where short_name = 'isd'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pitchburn Devils'),
    (select id from sets where short_name = 'isd'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'isd'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dream Twist'),
    (select id from sets where short_name = 'isd'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stromkirk Patrol'),
    (select id from sets where short_name = 'isd'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Makeshift Mauler'),
    (select id from sets where short_name = 'isd'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mayor of Avabruck // Howlpack Alpha'),
    (select id from sets where short_name = 'isd'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampiric Fury'),
    (select id from sets where short_name = 'isd'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Essence of the Wild'),
    (select id from sets where short_name = 'isd'),
    '178',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scourge of Geier Reach'),
    (select id from sets where short_name = 'isd'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Butcher''s Cleaver'),
    (select id from sets where short_name = 'isd'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Falkenrath Noble'),
    (select id from sets where short_name = 'isd'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Night Revelers'),
    (select id from sets where short_name = 'isd'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Villagers of Estwald // Howlpack of Estwald'),
    (select id from sets where short_name = 'isd'),
    '209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancient Grudge'),
    (select id from sets where short_name = 'isd'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skirsdag Cultist'),
    (select id from sets where short_name = 'isd'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Manor Skeleton'),
    (select id from sets where short_name = 'isd'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Travel Preparations'),
    (select id from sets where short_name = 'isd'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forbidden Alchemy'),
    (select id from sets where short_name = 'isd'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ludevic''s Test Subject // Ludevic''s Abomination'),
    (select id from sets where short_name = 'isd'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kruin Outlaw // Terror of Kruin Pass'),
    (select id from sets where short_name = 'isd'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Olivia Voldaren'),
    (select id from sets where short_name = 'isd'),
    '215',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Victim of Night'),
    (select id from sets where short_name = 'isd'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nephalia Drownyard'),
    (select id from sets where short_name = 'isd'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reaper from the Abyss'),
    (select id from sets where short_name = 'isd'),
    '112',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rage Thrower'),
    (select id from sets where short_name = 'isd'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fiend Hunter'),
    (select id from sets where short_name = 'isd'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodgift Demon'),
    (select id from sets where short_name = 'isd'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moldgraf Monstrosity'),
    (select id from sets where short_name = 'isd'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stony Silence'),
    (select id from sets where short_name = 'isd'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spectral Flight'),
    (select id from sets where short_name = 'isd'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Champion of the Parish'),
    (select id from sets where short_name = 'isd'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tribute to Hunger'),
    (select id from sets where short_name = 'isd'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silver-Inlaid Dagger'),
    (select id from sets where short_name = 'isd'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cloistered Youth // Unholy Fiend'),
    (select id from sets where short_name = 'isd'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Murder of Crows'),
    (select id from sets where short_name = 'isd'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spider Spawning'),
    (select id from sets where short_name = 'isd'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kessig Cagebreakers'),
    (select id from sets where short_name = 'isd'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'isd'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'isd'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burning Vengeance'),
    (select id from sets where short_name = 'isd'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clifftop Retreat'),
    (select id from sets where short_name = 'isd'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Midnight Haunting'),
    (select id from sets where short_name = 'isd'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darkthicket Wolf'),
    (select id from sets where short_name = 'isd'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Furor of the Bitten'),
    (select id from sets where short_name = 'isd'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Morkrut Banshee'),
    (select id from sets where short_name = 'isd'),
    '110',
    'uncommon'
) 
 on conflict do nothing;
