insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Rite of the Serpent'),
    (select id from sets where short_name = 'ktk'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Secret Plans'),
    (select id from sets where short_name = 'ktk'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Grip'),
    (select id from sets where short_name = 'ktk'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mardu Ascendancy'),
    (select id from sets where short_name = 'ktk'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Surrak Dragonclaw'),
    (select id from sets where short_name = 'ktk'),
    '206',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Warden of the Eye'),
    (select id from sets where short_name = 'ktk'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Villainous Wealth'),
    (select id from sets where short_name = 'ktk'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodfire Expert'),
    (select id from sets where short_name = 'ktk'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scoured Barrens'),
    (select id from sets where short_name = 'ktk'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hooting Mandrills'),
    (select id from sets where short_name = 'ktk'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necropolis Fiend'),
    (select id from sets where short_name = 'ktk'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heir of the Wilds'),
    (select id from sets where short_name = 'ktk'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Debilitating Injury'),
    (select id from sets where short_name = 'ktk'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ktk'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ktk'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Retribution of the Ancients'),
    (select id from sets where short_name = 'ktk'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rush of Battle'),
    (select id from sets where short_name = 'ktk'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kheru Spellsnatcher'),
    (select id from sets where short_name = 'ktk'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leaping Master'),
    (select id from sets where short_name = 'ktk'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tranquil Cove'),
    (select id from sets where short_name = 'ktk'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ainok Tracker'),
    (select id from sets where short_name = 'ktk'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temur Ascendancy'),
    (select id from sets where short_name = 'ktk'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cranial Archive'),
    (select id from sets where short_name = 'ktk'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mardu Warshrieker'),
    (select id from sets where short_name = 'ktk'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zurgo Helmsmasher'),
    (select id from sets where short_name = 'ktk'),
    '214',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Smite the Monstrous'),
    (select id from sets where short_name = 'ktk'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windstorm'),
    (select id from sets where short_name = 'ktk'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rotting Mastodon'),
    (select id from sets where short_name = 'ktk'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodfell Caves'),
    (select id from sets where short_name = 'ktk'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sultai Banner'),
    (select id from sets where short_name = 'ktk'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disdainful Stroke'),
    (select id from sets where short_name = 'ktk'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smoke Teller'),
    (select id from sets where short_name = 'ktk'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ponyback Brigade'),
    (select id from sets where short_name = 'ktk'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ktk'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abzan Guide'),
    (select id from sets where short_name = 'ktk'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ktk'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opulent Palace'),
    (select id from sets where short_name = 'ktk'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abzan Falconer'),
    (select id from sets where short_name = 'ktk'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Venerable Lammasu'),
    (select id from sets where short_name = 'ktk'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ainok Bond-Kin'),
    (select id from sets where short_name = 'ktk'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mardu Hordechief'),
    (select id from sets where short_name = 'ktk'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thousand Winds'),
    (select id from sets where short_name = 'ktk'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ktk'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tormenting Voice'),
    (select id from sets where short_name = 'ktk'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wingmate Roc'),
    (select id from sets where short_name = 'ktk'),
    '31',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chief of the Edge'),
    (select id from sets where short_name = 'ktk'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sage of the Inward Eye'),
    (select id from sets where short_name = 'ktk'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trap Essence'),
    (select id from sets where short_name = 'ktk'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = 'ktk'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Salt Road Patrol'),
    (select id from sets where short_name = 'ktk'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sultai Scavenger'),
    (select id from sets where short_name = 'ktk'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tusked Colossodon'),
    (select id from sets where short_name = 'ktk'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disowned Ancestor'),
    (select id from sets where short_name = 'ktk'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swift Kick'),
    (select id from sets where short_name = 'ktk'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sandsteppe Citadel'),
    (select id from sets where short_name = 'ktk'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barrage of Boulders'),
    (select id from sets where short_name = 'ktk'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deflecting Palm'),
    (select id from sets where short_name = 'ktk'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ktk'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sultai Ascendancy'),
    (select id from sets where short_name = 'ktk'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodsoaked Champion'),
    (select id from sets where short_name = 'ktk'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arc Lightning'),
    (select id from sets where short_name = 'ktk'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'See the Unwritten'),
    (select id from sets where short_name = 'ktk'),
    '149',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Alpine Grizzly'),
    (select id from sets where short_name = 'ktk'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Utter End'),
    (select id from sets where short_name = 'ktk'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Defiant Strike'),
    (select id from sets where short_name = 'ktk'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sagu Archer'),
    (select id from sets where short_name = 'ktk'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wind-Scarred Crag'),
    (select id from sets where short_name = 'ktk'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ride Down'),
    (select id from sets where short_name = 'ktk'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seeker of the Way'),
    (select id from sets where short_name = 'ktk'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lens of Clarity'),
    (select id from sets where short_name = 'ktk'),
    '223',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sage-Eye Harrier'),
    (select id from sets where short_name = 'ktk'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ktk'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ktk'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avalanche Tusker'),
    (select id from sets where short_name = 'ktk'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shambling Attendants'),
    (select id from sets where short_name = 'ktk'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heart-Piercer Bow'),
    (select id from sets where short_name = 'ktk'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodfire Mentor'),
    (select id from sets where short_name = 'ktk'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashcloud Phoenix'),
    (select id from sets where short_name = 'ktk'),
    '99',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sagu Mauler'),
    (select id from sets where short_name = 'ktk'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Butcher of the Horde'),
    (select id from sets where short_name = 'ktk'),
    '168',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savage Knuckleblade'),
    (select id from sets where short_name = 'ktk'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ktk'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rattleclaw Mystic'),
    (select id from sets where short_name = 'ktk'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rugged Highlands'),
    (select id from sets where short_name = 'ktk'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ktk'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whirlwind Adept'),
    (select id from sets where short_name = 'ktk'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feed the Clan'),
    (select id from sets where short_name = 'ktk'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horde Ambusher'),
    (select id from sets where short_name = 'ktk'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mindswipe'),
    (select id from sets where short_name = 'ktk'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krumar Bond-Kin'),
    (select id from sets where short_name = 'ktk'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragonscale Boon'),
    (select id from sets where short_name = 'ktk'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dig Through Time'),
    (select id from sets where short_name = 'ktk'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Howl of the Horde'),
    (select id from sets where short_name = 'ktk'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quiet Contemplation'),
    (select id from sets where short_name = 'ktk'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Become Immense'),
    (select id from sets where short_name = 'ktk'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scion of Glaciers'),
    (select id from sets where short_name = 'ktk'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Highspire Mantis'),
    (select id from sets where short_name = 'ktk'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trail of Mystery'),
    (select id from sets where short_name = 'ktk'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dismal Backwater'),
    (select id from sets where short_name = 'ktk'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Despise'),
    (select id from sets where short_name = 'ktk'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon-Style Twins'),
    (select id from sets where short_name = 'ktk'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archers'' Parapet'),
    (select id from sets where short_name = 'ktk'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'War Behemoth'),
    (select id from sets where short_name = 'ktk'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jeskai Windscout'),
    (select id from sets where short_name = 'ktk'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Singing Bell Strike'),
    (select id from sets where short_name = 'ktk'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jeskai Banner'),
    (select id from sets where short_name = 'ktk'),
    '222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ankle Shanker'),
    (select id from sets where short_name = 'ktk'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savage Punch'),
    (select id from sets where short_name = 'ktk'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stubborn Denial'),
    (select id from sets where short_name = 'ktk'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Master the Way'),
    (select id from sets where short_name = 'ktk'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghostfire Blade'),
    (select id from sets where short_name = 'ktk'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ktk'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jeering Instigator'),
    (select id from sets where short_name = 'ktk'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ivorytusk Fortress'),
    (select id from sets where short_name = 'ktk'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Witness of the Ages'),
    (select id from sets where short_name = 'ktk'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ktk'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abzan Banner'),
    (select id from sets where short_name = 'ktk'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure Cruise'),
    (select id from sets where short_name = 'ktk'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic of the Hidden Way'),
    (select id from sets where short_name = 'ktk'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hardened Scales'),
    (select id from sets where short_name = 'ktk'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abomination of Gudul'),
    (select id from sets where short_name = 'ktk'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sultai Flayer'),
    (select id from sets where short_name = 'ktk'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodstained Mire'),
    (select id from sets where short_name = 'ktk'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Briber''s Purse'),
    (select id from sets where short_name = 'ktk'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Highland Game'),
    (select id from sets where short_name = 'ktk'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistfire Weaver'),
    (select id from sets where short_name = 'ktk'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ktk'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Awaken the Bear'),
    (select id from sets where short_name = 'ktk'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'End Hostilities'),
    (select id from sets where short_name = 'ktk'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scaldkin'),
    (select id from sets where short_name = 'ktk'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mantis Rider'),
    (select id from sets where short_name = 'ktk'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roar of Challenge'),
    (select id from sets where short_name = 'ktk'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Valley Dasher'),
    (select id from sets where short_name = 'ktk'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Taigam''s Scheming'),
    (select id from sets where short_name = 'ktk'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glacial Stalker'),
    (select id from sets where short_name = 'ktk'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Embodiment of Spring'),
    (select id from sets where short_name = 'ktk'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Efreet Weaponmaster'),
    (select id from sets where short_name = 'ktk'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flooded Strand'),
    (select id from sets where short_name = 'ktk'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Altar of the Brood'),
    (select id from sets where short_name = 'ktk'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abzan Battle Priest'),
    (select id from sets where short_name = 'ktk'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abzan Charm'),
    (select id from sets where short_name = 'ktk'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jeskai Student'),
    (select id from sets where short_name = 'ktk'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Siege Rhino'),
    (select id from sets where short_name = 'ktk'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death Frenzy'),
    (select id from sets where short_name = 'ktk'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kin-Tree Invocation'),
    (select id from sets where short_name = 'ktk'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pine Walker'),
    (select id from sets where short_name = 'ktk'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ugin''s Nexus'),
    (select id from sets where short_name = 'ktk'),
    '227',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Crater''s Claws'),
    (select id from sets where short_name = 'ktk'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sultai Charm'),
    (select id from sets where short_name = 'ktk'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Temur Banner'),
    (select id from sets where short_name = 'ktk'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abzan Ascendancy'),
    (select id from sets where short_name = 'ktk'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blinding Spray'),
    (select id from sets where short_name = 'ktk'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bring Low'),
    (select id from sets where short_name = 'ktk'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ktk'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hooded Hydra'),
    (select id from sets where short_name = 'ktk'),
    '136',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Waterwhirl'),
    (select id from sets where short_name = 'ktk'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thornwood Falls'),
    (select id from sets where short_name = 'ktk'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cancel'),
    (select id from sets where short_name = 'ktk'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon''s Eye Savants'),
    (select id from sets where short_name = 'ktk'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bellowing Saddlebrute'),
    (select id from sets where short_name = 'ktk'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chief of the Scale'),
    (select id from sets where short_name = 'ktk'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unyielding Krumar'),
    (select id from sets where short_name = 'ktk'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blossoming Sands'),
    (select id from sets where short_name = 'ktk'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ktk'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weave Fate'),
    (select id from sets where short_name = 'ktk'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ktk'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hordeling Outburst'),
    (select id from sets where short_name = 'ktk'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armament Corps'),
    (select id from sets where short_name = 'ktk'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ktk'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gurmag Swiftwing'),
    (select id from sets where short_name = 'ktk'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wooded Foothills'),
    (select id from sets where short_name = 'ktk'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Watcher of the Roost'),
    (select id from sets where short_name = 'ktk'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woolly Loxodon'),
    (select id from sets where short_name = 'ktk'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meandering Towershell'),
    (select id from sets where short_name = 'ktk'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblinslide'),
    (select id from sets where short_name = 'ktk'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakshasa Deathdealer'),
    (select id from sets where short_name = 'ktk'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crippling Chill'),
    (select id from sets where short_name = 'ktk'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alabaster Kirin'),
    (select id from sets where short_name = 'ktk'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sultai Soothsayer'),
    (select id from sets where short_name = 'ktk'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakshasa''s Secret'),
    (select id from sets where short_name = 'ktk'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icy Blast'),
    (select id from sets where short_name = 'ktk'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = 'ktk'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murderous Cut'),
    (select id from sets where short_name = 'ktk'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tuskguard Captain'),
    (select id from sets where short_name = 'ktk'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feat of Resistance'),
    (select id from sets where short_name = 'ktk'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Throttle'),
    (select id from sets where short_name = 'ktk'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ktk'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molting Snakeskin'),
    (select id from sets where short_name = 'ktk'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Monastery Swiftspear'),
    (select id from sets where short_name = 'ktk'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winterflame'),
    (select id from sets where short_name = 'ktk'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Suspension Field'),
    (select id from sets where short_name = 'ktk'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mystic Monastery'),
    (select id from sets where short_name = 'ktk'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kheru Bloodsucker'),
    (select id from sets where short_name = 'ktk'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Incremental Growth'),
    (select id from sets where short_name = 'ktk'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jeskai Elder'),
    (select id from sets where short_name = 'ktk'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Riverwheel Aerialists'),
    (select id from sets where short_name = 'ktk'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mardu Charm'),
    (select id from sets where short_name = 'ktk'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arrow Storm'),
    (select id from sets where short_name = 'ktk'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sarkhan, the Dragonspeaker'),
    (select id from sets where short_name = 'ktk'),
    '119',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Anafenza, the Foremost'),
    (select id from sets where short_name = 'ktk'),
    '163',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ruthless Ripper'),
    (select id from sets where short_name = 'ktk'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jeskai Charm'),
    (select id from sets where short_name = 'ktk'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jeskai Ascendancy'),
    (select id from sets where short_name = 'ktk'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakshasa Vizier'),
    (select id from sets where short_name = 'ktk'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swarm of Bloodflies'),
    (select id from sets where short_name = 'ktk'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mardu Blazebringer'),
    (select id from sets where short_name = 'ktk'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ktk'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raiders'' Spoils'),
    (select id from sets where short_name = 'ktk'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clever Impersonator'),
    (select id from sets where short_name = 'ktk'),
    '34',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Set Adrift'),
    (select id from sets where short_name = 'ktk'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dazzling Ramparts'),
    (select id from sets where short_name = 'ktk'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grim Haruspex'),
    (select id from sets where short_name = 'ktk'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duneblast'),
    (select id from sets where short_name = 'ktk'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mardu Heart-Piercer'),
    (select id from sets where short_name = 'ktk'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Erase'),
    (select id from sets where short_name = 'ktk'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Throne of Tarkir'),
    (select id from sets where short_name = 'ktk'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seek the Horizon'),
    (select id from sets where short_name = 'ktk'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dutiful Return'),
    (select id from sets where short_name = 'ktk'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mardu Roughrider'),
    (select id from sets where short_name = 'ktk'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bear''s Companion'),
    (select id from sets where short_name = 'ktk'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sorin, Solemn Visitor'),
    (select id from sets where short_name = 'ktk'),
    '202',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'High Sentinels of Arashin'),
    (select id from sets where short_name = 'ktk'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Timely Hordemate'),
    (select id from sets where short_name = 'ktk'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mardu Skullhunter'),
    (select id from sets where short_name = 'ktk'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Longshot Squad'),
    (select id from sets where short_name = 'ktk'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Force Away'),
    (select id from sets where short_name = 'ktk'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kin-Tree Warden'),
    (select id from sets where short_name = 'ktk'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Act of Treason'),
    (select id from sets where short_name = 'ktk'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temur Charm'),
    (select id from sets where short_name = 'ktk'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bitter Revelation'),
    (select id from sets where short_name = 'ktk'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frontier Bivouac'),
    (select id from sets where short_name = 'ktk'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Monastery Flock'),
    (select id from sets where short_name = 'ktk'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sidisi''s Pet'),
    (select id from sets where short_name = 'ktk'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swiftwater Cliffs'),
    (select id from sets where short_name = 'ktk'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windswept Heath'),
    (select id from sets where short_name = 'ktk'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kheru Lich Lord'),
    (select id from sets where short_name = 'ktk'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Master of Pearls'),
    (select id from sets where short_name = 'ktk'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kheru Dreadmaw'),
    (select id from sets where short_name = 'ktk'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Herald of Anafenza'),
    (select id from sets where short_name = 'ktk'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'War-Name Aspirant'),
    (select id from sets where short_name = 'ktk'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Empty the Pits'),
    (select id from sets where short_name = 'ktk'),
    '72',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scout the Borders'),
    (select id from sets where short_name = 'ktk'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tomb of the Spirit Dragon'),
    (select id from sets where short_name = 'ktk'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trumpet Blast'),
    (select id from sets where short_name = 'ktk'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icefeather Aven'),
    (select id from sets where short_name = 'ktk'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firehoof Cavalry'),
    (select id from sets where short_name = 'ktk'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Canyon Lurkers'),
    (select id from sets where short_name = 'ktk'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brave the Sands'),
    (select id from sets where short_name = 'ktk'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flying Crane Technique'),
    (select id from sets where short_name = 'ktk'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mardu Hateblade'),
    (select id from sets where short_name = 'ktk'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Narset, Enlightened Master'),
    (select id from sets where short_name = 'ktk'),
    '190',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ktk'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kill Shot'),
    (select id from sets where short_name = 'ktk'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burn Away'),
    (select id from sets where short_name = 'ktk'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snowhorn Rider'),
    (select id from sets where short_name = 'ktk'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wetland Sambar'),
    (select id from sets where short_name = 'ktk'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temur Charger'),
    (select id from sets where short_name = 'ktk'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Summit Prowler'),
    (select id from sets where short_name = 'ktk'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crackling Doom'),
    (select id from sets where short_name = 'ktk'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nomad Outpost'),
    (select id from sets where short_name = 'ktk'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Siegecraft'),
    (select id from sets where short_name = 'ktk'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pearl Lake Ancient'),
    (select id from sets where short_name = 'ktk'),
    '49',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Take Up Arms'),
    (select id from sets where short_name = 'ktk'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mardu Banner'),
    (select id from sets where short_name = 'ktk'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mer-Ek Nightblade'),
    (select id from sets where short_name = 'ktk'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jungle Hollow'),
    (select id from sets where short_name = 'ktk'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dead Drop'),
    (select id from sets where short_name = 'ktk'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Polluted Delta'),
    (select id from sets where short_name = 'ktk'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sidisi, Brood Tyrant'),
    (select id from sets where short_name = 'ktk'),
    '199',
    'mythic'
) 
 on conflict do nothing;
