insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = 'plgm'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'plgm'),
    '1',
    'rare'
) 
 on conflict do nothing;
