    insert into mtgcard(name) values ('Tuktuk the Returned') on conflict do nothing;
    insert into mtgcard(name) values ('Eldrazi Spawn') on conflict do nothing;
    insert into mtgcard(name) values ('Hellion') on conflict do nothing;
    insert into mtgcard(name) values ('Ooze') on conflict do nothing;
    insert into mtgcard(name) values ('Elemental') on conflict do nothing;
