insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Tuktuk the Returned'),
    (select id from sets where short_name = 'troe'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Spawn'),
    (select id from sets where short_name = 'troe'),
    '1b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hellion'),
    (select id from sets where short_name = 'troe'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Spawn'),
    (select id from sets where short_name = 'troe'),
    '1c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ooze'),
    (select id from sets where short_name = 'troe'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Spawn'),
    (select id from sets where short_name = 'troe'),
    '1a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'troe'),
    '2',
    'common'
) 
 on conflict do nothing;
