insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Beast Attack'),
    (select id from sets where short_name = 'dds'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Relentless Hunter'),
    (select id from sets where short_name = 'dds'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sage-Eye Avengers'),
    (select id from sets where short_name = 'dds'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'dds'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'dds'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boldwyr Intimidator'),
    (select id from sets where short_name = 'dds'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reach Through Mists'),
    (select id from sets where short_name = 'dds'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind''s Desire'),
    (select id from sets where short_name = 'dds'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snap'),
    (select id from sets where short_name = 'dds'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beacon of Tomorrows'),
    (select id from sets where short_name = 'dds'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = 'dds'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beacon of Destruction'),
    (select id from sets where short_name = 'dds'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desperate Ritual'),
    (select id from sets where short_name = 'dds'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burning-Tree Emissary'),
    (select id from sets where short_name = 'dds'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jhoira of the Ghitu'),
    (select id from sets where short_name = 'dds'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dds'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'dds'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skarrgan Pit-Skulk'),
    (select id from sets where short_name = 'dds'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Vision'),
    (select id from sets where short_name = 'dds'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jori En, Ruin Diver'),
    (select id from sets where short_name = 'dds'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Empty the Warrens'),
    (select id from sets where short_name = 'dds'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'dds'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coat of Arms'),
    (select id from sets where short_name = 'dds'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roar of the Wurm'),
    (select id from sets where short_name = 'dds'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dds'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'dds'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Firemind''s Foresight'),
    (select id from sets where short_name = 'dds'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firebolt'),
    (select id from sets where short_name = 'dds'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Radha, Heir to Keld'),
    (select id from sets where short_name = 'dds'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gorehorn Minotaurs'),
    (select id from sets where short_name = 'dds'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Young Pyromancer'),
    (select id from sets where short_name = 'dds'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dds'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kamahl, Pit Fighter'),
    (select id from sets where short_name = 'dds'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shivan Meteor'),
    (select id from sets where short_name = 'dds'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Call of the Herd'),
    (select id from sets where short_name = 'dds'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rugged Highlands'),
    (select id from sets where short_name = 'dds'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudcrown Oak'),
    (select id from sets where short_name = 'dds'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talara''s Battalion'),
    (select id from sets where short_name = 'dds'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dds'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spellheart Chimera'),
    (select id from sets where short_name = 'dds'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grapeshot'),
    (select id from sets where short_name = 'dds'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guttural Response'),
    (select id from sets where short_name = 'dds'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ambassador Oak'),
    (select id from sets where short_name = 'dds'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lovisa Coldeyes'),
    (select id from sets where short_name = 'dds'),
    '34',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goblin Electromancer'),
    (select id from sets where short_name = 'dds'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Might'),
    (select id from sets where short_name = 'dds'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sift Through Sands'),
    (select id from sets where short_name = 'dds'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Increasing Savagery'),
    (select id from sets where short_name = 'dds'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harmonize'),
    (select id from sets where short_name = 'dds'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Temporal Fissure'),
    (select id from sets where short_name = 'dds'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dds'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dds'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Peer Through Depths'),
    (select id from sets where short_name = 'dds'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deep-Sea Kraken'),
    (select id from sets where short_name = 'dds'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'dds'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quicken'),
    (select id from sets where short_name = 'dds'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zo-Zu the Punisher'),
    (select id from sets where short_name = 'dds'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Unspeakable'),
    (select id from sets where short_name = 'dds'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swiftwater Cliffs'),
    (select id from sets where short_name = 'dds'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rubblebelt Raiders'),
    (select id from sets where short_name = 'dds'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nivix Cyclops'),
    (select id from sets where short_name = 'dds'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kruin Striker'),
    (select id from sets where short_name = 'dds'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nucklavee'),
    (select id from sets where short_name = 'dds'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Talrand, Sky Summoner'),
    (select id from sets where short_name = 'dds'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rift Bolt'),
    (select id from sets where short_name = 'dds'),
    '17',
    'common'
) 
 on conflict do nothing;
