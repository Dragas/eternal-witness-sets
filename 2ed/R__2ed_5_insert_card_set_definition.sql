insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '2ed'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crystal Rod'),
    (select id from sets where short_name = '2ed'),
    '240',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Taiga'),
    (select id from sets where short_name = '2ed'),
    '283',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Granite Gargoyle'),
    (select id from sets where short_name = '2ed'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Timetwister'),
    (select id from sets where short_name = '2ed'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '2ed'),
    '288',
    'common'
) ,
(
    (select id from mtgcard where name = 'Red Ward'),
    (select id from sets where short_name = '2ed'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Swords'),
    (select id from sets where short_name = '2ed'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steal Artifact'),
    (select id from sets where short_name = '2ed'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Animate Wall'),
    (select id from sets where short_name = '2ed'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '2ed'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'Control Magic'),
    (select id from sets where short_name = '2ed'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burrowing'),
    (select id from sets where short_name = '2ed'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scathe Zombies'),
    (select id from sets where short_name = '2ed'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Camouflage'),
    (select id from sets where short_name = '2ed'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = '2ed'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time Vault'),
    (select id from sets where short_name = '2ed'),
    '275',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disrupting Scepter'),
    (select id from sets where short_name = '2ed'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Siren''s Call'),
    (select id from sets where short_name = '2ed'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Juggernaut'),
    (select id from sets where short_name = '2ed'),
    '256',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tundra'),
    (select id from sets where short_name = '2ed'),
    '285',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Timber Wolves'),
    (select id from sets where short_name = '2ed'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mons''s Goblin Raiders'),
    (select id from sets where short_name = '2ed'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ironclaw Orcs'),
    (select id from sets where short_name = '2ed'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '2ed'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rod of Ruin'),
    (select id from sets where short_name = '2ed'),
    '269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Net'),
    (select id from sets where short_name = '2ed'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Archers'),
    (select id from sets where short_name = '2ed'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animate Dead'),
    (select id from sets where short_name = '2ed'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Green'),
    (select id from sets where short_name = '2ed'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Warriors'),
    (select id from sets where short_name = '2ed'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scavenging Ghoul'),
    (select id from sets where short_name = '2ed'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blue Ward'),
    (select id from sets where short_name = '2ed'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nether Shadow'),
    (select id from sets where short_name = '2ed'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Personal Incarnation'),
    (select id from sets where short_name = '2ed'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = '2ed'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin King'),
    (select id from sets where short_name = '2ed'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evil Presence'),
    (select id from sets where short_name = '2ed'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fungusaur'),
    (select id from sets where short_name = '2ed'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Instill Energy'),
    (select id from sets where short_name = '2ed'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Red Elemental Blast'),
    (select id from sets where short_name = '2ed'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = '2ed'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Black'),
    (select id from sets where short_name = '2ed'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Creature Bond'),
    (select id from sets where short_name = '2ed'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = '2ed'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mox Ruby'),
    (select id from sets where short_name = '2ed'),
    '265',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hurricane'),
    (select id from sets where short_name = '2ed'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Earth Elemental'),
    (select id from sets where short_name = '2ed'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forcefield'),
    (select id from sets where short_name = '2ed'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Word of Command'),
    (select id from sets where short_name = '2ed'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Force of Nature'),
    (select id from sets where short_name = '2ed'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jump'),
    (select id from sets where short_name = '2ed'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Balloon Brigade'),
    (select id from sets where short_name = '2ed'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sacrifice'),
    (select id from sets where short_name = '2ed'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Stone'),
    (select id from sets where short_name = '2ed'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Icy Manipulator'),
    (select id from sets where short_name = '2ed'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pestilence'),
    (select id from sets where short_name = '2ed'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '2ed'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meekstone'),
    (select id from sets where short_name = '2ed'),
    '261',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blaze of Glory'),
    (select id from sets where short_name = '2ed'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Regrowth'),
    (select id from sets where short_name = '2ed'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancestral Recall'),
    (select id from sets where short_name = '2ed'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Castle'),
    (select id from sets where short_name = '2ed'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ice Storm'),
    (select id from sets where short_name = '2ed'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Wall'),
    (select id from sets where short_name = '2ed'),
    '259',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Northern Paladin'),
    (select id from sets where short_name = '2ed'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uthden Troll'),
    (select id from sets where short_name = '2ed'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ley Druid'),
    (select id from sets where short_name = '2ed'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Natural Selection'),
    (select id from sets where short_name = '2ed'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thicket Basilisk'),
    (select id from sets where short_name = '2ed'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Knight'),
    (select id from sets where short_name = '2ed'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savannah'),
    (select id from sets where short_name = '2ed'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = '2ed'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lord of the Pit'),
    (select id from sets where short_name = '2ed'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Manabarbs'),
    (select id from sets where short_name = '2ed'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Iron Star'),
    (select id from sets where short_name = '2ed'),
    '251',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Farmstead'),
    (select id from sets where short_name = '2ed'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savannah Lions'),
    (select id from sets where short_name = '2ed'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Braingeyser'),
    (select id from sets where short_name = '2ed'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wanderlust'),
    (select id from sets where short_name = '2ed'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blessing'),
    (select id from sets where short_name = '2ed'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gauntlet of Might'),
    (select id from sets where short_name = '2ed'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vesuvan Doppelganger'),
    (select id from sets where short_name = '2ed'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blue Elemental Blast'),
    (select id from sets where short_name = '2ed'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Leak'),
    (select id from sets where short_name = '2ed'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wheel of Fortune'),
    (select id from sets where short_name = '2ed'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lifelace'),
    (select id from sets where short_name = '2ed'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conversion'),
    (select id from sets where short_name = '2ed'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Artifact'),
    (select id from sets where short_name = '2ed'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frozen Shade'),
    (select id from sets where short_name = '2ed'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sinkhole'),
    (select id from sets where short_name = '2ed'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Lotus'),
    (select id from sets where short_name = '2ed'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crusade'),
    (select id from sets where short_name = '2ed'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = '2ed'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prodigal Sorcerer'),
    (select id from sets where short_name = '2ed'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smoke'),
    (select id from sets where short_name = '2ed'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = '2ed'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drudge Skeletons'),
    (select id from sets where short_name = '2ed'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demonic Attorney'),
    (select id from sets where short_name = '2ed'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Library of Leng'),
    (select id from sets where short_name = '2ed'),
    '258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feedback'),
    (select id from sets where short_name = '2ed'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = '2ed'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Vise'),
    (select id from sets where short_name = '2ed'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Contract from Below'),
    (select id from sets where short_name = '2ed'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flight'),
    (select id from sets where short_name = '2ed'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Verduran Enchantress'),
    (select id from sets where short_name = '2ed'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Badlands'),
    (select id from sets where short_name = '2ed'),
    '278',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lifeforce'),
    (select id from sets where short_name = '2ed'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stasis'),
    (select id from sets where short_name = '2ed'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Throne of Bone'),
    (select id from sets where short_name = '2ed'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sedge Troll'),
    (select id from sets where short_name = '2ed'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Basalt Monolith'),
    (select id from sets where short_name = '2ed'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aspect of Wolf'),
    (select id from sets where short_name = '2ed'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Brambles'),
    (select id from sets where short_name = '2ed'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lich'),
    (select id from sets where short_name = '2ed'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Royal Assassin'),
    (select id from sets where short_name = '2ed'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Liege'),
    (select id from sets where short_name = '2ed'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Resurrection'),
    (select id from sets where short_name = '2ed'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drain Power'),
    (select id from sets where short_name = '2ed'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lifetap'),
    (select id from sets where short_name = '2ed'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fastbond'),
    (select id from sets where short_name = '2ed'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone Giant'),
    (select id from sets where short_name = '2ed'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mahamoti Djinn'),
    (select id from sets where short_name = '2ed'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Two-Headed Giant of Foriys'),
    (select id from sets where short_name = '2ed'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '2ed'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '2ed'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cursed Land'),
    (select id from sets where short_name = '2ed'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Channel'),
    (select id from sets where short_name = '2ed'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volcanic Eruption'),
    (select id from sets where short_name = '2ed'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Web'),
    (select id from sets where short_name = '2ed'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mox Jet'),
    (select id from sets where short_name = '2ed'),
    '263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stream of Life'),
    (select id from sets where short_name = '2ed'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '2ed'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unholy Strength'),
    (select id from sets where short_name = '2ed'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Samite Healer'),
    (select id from sets where short_name = '2ed'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = '2ed'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kormus Bell'),
    (select id from sets where short_name = '2ed'),
    '257',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Will-o''-the-Wisp'),
    (select id from sets where short_name = '2ed'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Green Ward'),
    (select id from sets where short_name = '2ed'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Short'),
    (select id from sets where short_name = '2ed'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conservator'),
    (select id from sets where short_name = '2ed'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = '2ed'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Fire'),
    (select id from sets where short_name = '2ed'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Roc of Kher Ridges'),
    (select id from sets where short_name = '2ed'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ivory Cup'),
    (select id from sets where short_name = '2ed'),
    '252',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Vault'),
    (select id from sets where short_name = '2ed'),
    '260',
    'rare'
) ,
(
    (select id from mtgcard where name = 'White Ward'),
    (select id from sets where short_name = '2ed'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Howl from Beyond'),
    (select id from sets where short_name = '2ed'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '2ed'),
    '302',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chaos Orb'),
    (select id from sets where short_name = '2ed'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magical Hack'),
    (select id from sets where short_name = '2ed'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Whelp'),
    (select id from sets where short_name = '2ed'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = '2ed'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scrubland'),
    (select id from sets where short_name = '2ed'),
    '282',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raging River'),
    (select id from sets where short_name = '2ed'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reverse Damage'),
    (select id from sets where short_name = '2ed'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mesa Pegasus'),
    (select id from sets where short_name = '2ed'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ankh of Mishra'),
    (select id from sets where short_name = '2ed'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = '2ed'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demonic Hordes'),
    (select id from sets where short_name = '2ed'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balance'),
    (select id from sets where short_name = '2ed'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cockatrice'),
    (select id from sets where short_name = '2ed'),
    '190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = '2ed'),
    '267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tropical Island'),
    (select id from sets where short_name = '2ed'),
    '284',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bad Moon'),
    (select id from sets where short_name = '2ed'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jade Monolith'),
    (select id from sets where short_name = '2ed'),
    '253',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '2ed'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earthbind'),
    (select id from sets where short_name = '2ed'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psionic Blast'),
    (select id from sets where short_name = '2ed'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hurloon Minotaur'),
    (select id from sets where short_name = '2ed'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Veteran Bodyguard'),
    (select id from sets where short_name = '2ed'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'White Knight'),
    (select id from sets where short_name = '2ed'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pearled Unicorn'),
    (select id from sets where short_name = '2ed'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '2ed'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darkpact'),
    (select id from sets where short_name = '2ed'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benalish Hero'),
    (select id from sets where short_name = '2ed'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Wraith'),
    (select id from sets where short_name = '2ed'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Obsianus Golem'),
    (select id from sets where short_name = '2ed'),
    '268',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nettling Imp'),
    (select id from sets where short_name = '2ed'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '2ed'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: White'),
    (select id from sets where short_name = '2ed'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Surge'),
    (select id from sets where short_name = '2ed'),
    '168',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = '2ed'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Holy Armor'),
    (select id from sets where short_name = '2ed'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Island'),
    (select id from sets where short_name = '2ed'),
    '287',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spell Blast'),
    (select id from sets where short_name = '2ed'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Consecrate Land'),
    (select id from sets where short_name = '2ed'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tunnel'),
    (select id from sets where short_name = '2ed'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Lands'),
    (select id from sets where short_name = '2ed'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cyclopean Tomb'),
    (select id from sets where short_name = '2ed'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Illusionary Mask'),
    (select id from sets where short_name = '2ed'),
    '250',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Healing Salve'),
    (select id from sets where short_name = '2ed'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mox Emerald'),
    (select id from sets where short_name = '2ed'),
    '262',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jade Statue'),
    (select id from sets where short_name = '2ed'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'False Orders'),
    (select id from sets where short_name = '2ed'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Craw Wurm'),
    (select id from sets where short_name = '2ed'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Demolition Team'),
    (select id from sets where short_name = '2ed'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dingus Egg'),
    (select id from sets where short_name = '2ed'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simulacrum'),
    (select id from sets where short_name = '2ed'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fork'),
    (select id from sets where short_name = '2ed'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = '2ed'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rock Hydra'),
    (select id from sets where short_name = '2ed'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merfolk of the Pearl Trident'),
    (select id from sets where short_name = '2ed'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time Walk'),
    (select id from sets where short_name = '2ed'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea Serpent'),
    (select id from sets where short_name = '2ed'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Hive'),
    (select id from sets where short_name = '2ed'),
    '273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Helm of Chatzuk'),
    (select id from sets where short_name = '2ed'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Weakness'),
    (select id from sets where short_name = '2ed'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shanodin Dryads'),
    (select id from sets where short_name = '2ed'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychic Venom'),
    (select id from sets where short_name = '2ed'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Regeneration'),
    (select id from sets where short_name = '2ed'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Wood'),
    (select id from sets where short_name = '2ed'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Growth'),
    (select id from sets where short_name = '2ed'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tsunami'),
    (select id from sets where short_name = '2ed'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thoughtlace'),
    (select id from sets where short_name = '2ed'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunglasses of Urza'),
    (select id from sets where short_name = '2ed'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twiddle'),
    (select id from sets where short_name = '2ed'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fog'),
    (select id from sets where short_name = '2ed'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Underground Sea'),
    (select id from sets where short_name = '2ed'),
    '286',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clone'),
    (select id from sets where short_name = '2ed'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Purelace'),
    (select id from sets where short_name = '2ed'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fire Elemental'),
    (select id from sets where short_name = '2ed'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Flare'),
    (select id from sets where short_name = '2ed'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Copper Tablet'),
    (select id from sets where short_name = '2ed'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Blue'),
    (select id from sets where short_name = '2ed'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flashfires'),
    (select id from sets where short_name = '2ed'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guardian Angel'),
    (select id from sets where short_name = '2ed'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathlace'),
    (select id from sets where short_name = '2ed'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Water Elemental'),
    (select id from sets where short_name = '2ed'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = '2ed'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Terrain'),
    (select id from sets where short_name = '2ed'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howling Mine'),
    (select id from sets where short_name = '2ed'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clockwork Beast'),
    (select id from sets where short_name = '2ed'),
    '237',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '2ed'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mox Pearl'),
    (select id from sets where short_name = '2ed'),
    '264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glasses of Urza'),
    (select id from sets where short_name = '2ed'),
    '246',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demonic Tutor'),
    (select id from sets where short_name = '2ed'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karma'),
    (select id from sets where short_name = '2ed'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'War Mammoth'),
    (select id from sets where short_name = '2ed'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Animate Artifact'),
    (select id from sets where short_name = '2ed'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Celestial Prism'),
    (select id from sets where short_name = '2ed'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '2ed'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Monster'),
    (select id from sets where short_name = '2ed'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = '2ed'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warp Artifact'),
    (select id from sets where short_name = '2ed'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sleight of Mind'),
    (select id from sets where short_name = '2ed'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island Sanctuary'),
    (select id from sets where short_name = '2ed'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drain Life'),
    (select id from sets where short_name = '2ed'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pirate Ship'),
    (select id from sets where short_name = '2ed'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bayou'),
    (select id from sets where short_name = '2ed'),
    '279',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Air'),
    (select id from sets where short_name = '2ed'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grizzly Bears'),
    (select id from sets where short_name = '2ed'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Ward'),
    (select id from sets where short_name = '2ed'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Ice'),
    (select id from sets where short_name = '2ed'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zombie Master'),
    (select id from sets where short_name = '2ed'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Artillery'),
    (select id from sets where short_name = '2ed'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firebreathing'),
    (select id from sets where short_name = '2ed'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Righteousness'),
    (select id from sets where short_name = '2ed'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Copy Artifact'),
    (select id from sets where short_name = '2ed'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plateau'),
    (select id from sets where short_name = '2ed'),
    '280',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Invisibility'),
    (select id from sets where short_name = '2ed'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Oriflamme'),
    (select id from sets where short_name = '2ed'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hill Giant'),
    (select id from sets where short_name = '2ed'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = '2ed'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = '2ed'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = '2ed'),
    '270',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Specter'),
    (select id from sets where short_name = '2ed'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Paralyze'),
    (select id from sets where short_name = '2ed'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gray Ogre'),
    (select id from sets where short_name = '2ed'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jayemdae Tome'),
    (select id from sets where short_name = '2ed'),
    '255',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fear'),
    (select id from sets where short_name = '2ed'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = '2ed'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lance'),
    (select id from sets where short_name = '2ed'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Forces'),
    (select id from sets where short_name = '2ed'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scryb Sprites'),
    (select id from sets where short_name = '2ed'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Bone'),
    (select id from sets where short_name = '2ed'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '2ed'),
    '290',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winter Orb'),
    (select id from sets where short_name = '2ed'),
    '276',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = '2ed'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Power Sink'),
    (select id from sets where short_name = '2ed'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Ward'),
    (select id from sets where short_name = '2ed'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Holy Strength'),
    (select id from sets where short_name = '2ed'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chaoslace'),
    (select id from sets where short_name = '2ed'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plague Rats'),
    (select id from sets where short_name = '2ed'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keldon Warlord'),
    (select id from sets where short_name = '2ed'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Twist'),
    (select id from sets where short_name = '2ed'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gloom'),
    (select id from sets where short_name = '2ed'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lure'),
    (select id from sets where short_name = '2ed'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mox Sapphire'),
    (select id from sets where short_name = '2ed'),
    '266',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ironroot Treefolk'),
    (select id from sets where short_name = '2ed'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kudzu'),
    (select id from sets where short_name = '2ed'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disintegrate'),
    (select id from sets where short_name = '2ed'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Water'),
    (select id from sets where short_name = '2ed'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lord of Atlantis'),
    (select id from sets where short_name = '2ed'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wooden Sphere'),
    (select id from sets where short_name = '2ed'),
    '277',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquility'),
    (select id from sets where short_name = '2ed'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Berserk'),
    (select id from sets where short_name = '2ed'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = '2ed'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathgrip'),
    (select id from sets where short_name = '2ed'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = '2ed'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = '2ed'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = '2ed'),
    '128',
    'uncommon'
) 
 on conflict do nothing;
