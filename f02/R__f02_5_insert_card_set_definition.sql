insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Aura of Silence'),
    (select id from sets where short_name = 'f02'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dissipate'),
    (select id from sets where short_name = 'f02'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forbid'),
    (select id from sets where short_name = 'f02'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soltari Priest'),
    (select id from sets where short_name = 'f02'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drain Life'),
    (select id from sets where short_name = 'f02'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Blossoms'),
    (select id from sets where short_name = 'f02'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Albino Troll'),
    (select id from sets where short_name = 'f02'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Black Knight'),
    (select id from sets where short_name = 'f02'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spike Feeder'),
    (select id from sets where short_name = 'f02'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fireslinger'),
    (select id from sets where short_name = 'f02'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mogg Fanatic'),
    (select id from sets where short_name = 'f02'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'White Knight'),
    (select id from sets where short_name = 'f02'),
    '12',
    'rare'
) 
 on conflict do nothing;
