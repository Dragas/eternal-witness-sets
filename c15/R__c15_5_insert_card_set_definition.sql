insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Mulch'),
    (select id from sets where short_name = 'c15'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Monk Idealist'),
    (select id from sets where short_name = 'c15'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mycoloth'),
    (select id from sets where short_name = 'c15'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jungle Hollow'),
    (select id from sets where short_name = 'c15'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blatant Thievery'),
    (select id from sets where short_name = 'c15'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Epic Experiment'),
    (select id from sets where short_name = 'c15'),
    '216',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ancient Amphitheater'),
    (select id from sets where short_name = 'c15'),
    '276',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orim''s Thunder'),
    (select id from sets where short_name = 'c15'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivid Creek'),
    (select id from sets where short_name = 'c15'),
    '317',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Elder'),
    (select id from sets where short_name = 'c15'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghostblade Eidolon'),
    (select id from sets where short_name = 'c15'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llanowar Reborn'),
    (select id from sets where short_name = 'c15'),
    '293',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Banshee of the Dread Choir'),
    (select id from sets where short_name = 'c15'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fumiko the Lowblood'),
    (select id from sets where short_name = 'c15'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Charm'),
    (select id from sets where short_name = 'c15'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Guildgate'),
    (select id from sets where short_name = 'c15'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jarad, Golgari Lich Lord'),
    (select id from sets where short_name = 'c15'),
    '223',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rise from the Grave'),
    (select id from sets where short_name = 'c15'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aethersnatch'),
    (select id from sets where short_name = 'c15'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mesa Enchantress'),
    (select id from sets where short_name = 'c15'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Solemn Simulacrum'),
    (select id from sets where short_name = 'c15'),
    '269',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirror Match'),
    (select id from sets where short_name = 'c15'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c15'),
    '341',
    'common'
) ,
(
    (select id from mtgcard where name = 'Caller of the Pack'),
    (select id from sets where short_name = 'c15'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Simic Guildgate'),
    (select id from sets where short_name = 'c15'),
    '306',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loaming Shaman'),
    (select id from sets where short_name = 'c15'),
    '190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Command Beacon'),
    (select id from sets where short_name = 'c15'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blustersquall'),
    (select id from sets where short_name = 'c15'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crystal Chimes'),
    (select id from sets where short_name = 'c15'),
    '250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sword of Vengeance'),
    (select id from sets where short_name = 'c15'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Korozda Guildmage'),
    (select id from sets where short_name = 'c15'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diabolic Servitude'),
    (select id from sets where short_name = 'c15'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Melek, Izzet Paragon'),
    (select id from sets where short_name = 'c15'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magma Giant'),
    (select id from sets where short_name = 'c15'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arbiter of Knollridge'),
    (select id from sets where short_name = 'c15'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swiftwater Cliffs'),
    (select id from sets where short_name = 'c15'),
    '310',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viridian Shaman'),
    (select id from sets where short_name = 'c15'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Signet'),
    (select id from sets where short_name = 'c15'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fate Unraveler'),
    (select id from sets where short_name = 'c15'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viridian Zealot'),
    (select id from sets where short_name = 'c15'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aetherize'),
    (select id from sets where short_name = 'c15'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Indrik Stomphowler'),
    (select id from sets where short_name = 'c15'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Izzet Guildgate'),
    (select id from sets where short_name = 'c15'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ninja of the Deep Hours'),
    (select id from sets where short_name = 'c15'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c15'),
    '336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Boilerworks'),
    (select id from sets where short_name = 'c15'),
    '290',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief, the Vastwood'),
    (select id from sets where short_name = 'c15'),
    '297',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gigantoplasm'),
    (select id from sets where short_name = 'c15'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c15'),
    '330',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chain Reaction'),
    (select id from sets where short_name = 'c15'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Basilica'),
    (select id from sets where short_name = 'c15'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Novijen, Heart of Progress'),
    (select id from sets where short_name = 'c15'),
    '296',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Call the Skybreaker'),
    (select id from sets where short_name = 'c15'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of the False God'),
    (select id from sets where short_name = 'c15'),
    '313',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barter in Blood'),
    (select id from sets where short_name = 'c15'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grave Peril'),
    (select id from sets where short_name = 'c15'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cage of Hands'),
    (select id from sets where short_name = 'c15'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burnished Hart'),
    (select id from sets where short_name = 'c15'),
    '248',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ohran Viper'),
    (select id from sets where short_name = 'c15'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Reclamation'),
    (select id from sets where short_name = 'c15'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chameleon Colossus'),
    (select id from sets where short_name = 'c15'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hammerfist Giant'),
    (select id from sets where short_name = 'c15'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c15'),
    '339',
    'common'
) ,
(
    (select id from mtgcard where name = 'Underworld Connections'),
    (select id from sets where short_name = 'c15'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primal Growth'),
    (select id from sets where short_name = 'c15'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orzhov Cluestone'),
    (select id from sets where short_name = 'c15'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Day of the Dragons'),
    (select id from sets where short_name = 'c15'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Acidic Slime'),
    (select id from sets where short_name = 'c15'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scoured Barrens'),
    (select id from sets where short_name = 'c15'),
    '303',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'c15'),
    '268',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beastmaster Ascension'),
    (select id from sets where short_name = 'c15'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Borderland Behemoth'),
    (select id from sets where short_name = 'c15'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teysa, Envoy of Ghosts'),
    (select id from sets where short_name = 'c15'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necromancer''s Covenant'),
    (select id from sets where short_name = 'c15'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stoneshock Giant'),
    (select id from sets where short_name = 'c15'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firemind''s Foresight'),
    (select id from sets where short_name = 'c15'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Charmbreaker Devils'),
    (select id from sets where short_name = 'c15'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Chosen'),
    (select id from sets where short_name = 'c15'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corpse Augur'),
    (select id from sets where short_name = 'c15'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steam Augury'),
    (select id from sets where short_name = 'c15'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jace''s Archivist'),
    (select id from sets where short_name = 'c15'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Underworld Coinsmith'),
    (select id from sets where short_name = 'c15'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viridian Emissary'),
    (select id from sets where short_name = 'c15'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lotleth Troll'),
    (select id from sets where short_name = 'c15'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Putrefy'),
    (select id from sets where short_name = 'c15'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grasp of Fate'),
    (select id from sets where short_name = 'c15'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desperate Ravings'),
    (select id from sets where short_name = 'c15'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thought Reflection'),
    (select id from sets where short_name = 'c15'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Electromancer'),
    (select id from sets where short_name = 'c15'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lorescale Coatl'),
    (select id from sets where short_name = 'c15'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spinerock Knoll'),
    (select id from sets where short_name = 'c15'),
    '309',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scytheclaw'),
    (select id from sets where short_name = 'c15'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Daxos the Returned'),
    (select id from sets where short_name = 'c15'),
    '43',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Arjun, the Shifting Flame'),
    (select id from sets where short_name = 'c15'),
    '42',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ezuri''s Predation'),
    (select id from sets where short_name = 'c15'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sever the Bloodline'),
    (select id from sets where short_name = 'c15'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snakeform'),
    (select id from sets where short_name = 'c15'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Market'),
    (select id from sets where short_name = 'c15'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shielded by Faith'),
    (select id from sets where short_name = 'c15'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nighthowler'),
    (select id from sets where short_name = 'c15'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Signet'),
    (select id from sets where short_name = 'c15'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sigil of the Empty Throne'),
    (select id from sets where short_name = 'c15'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fall of the Hammer'),
    (select id from sets where short_name = 'c15'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smoldering Crater'),
    (select id from sets where short_name = 'c15'),
    '308',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c15'),
    '323',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illusory Ambusher'),
    (select id from sets where short_name = 'c15'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Counterflux'),
    (select id from sets where short_name = 'c15'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thelonite Hermit'),
    (select id from sets where short_name = 'c15'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Growth Chamber'),
    (select id from sets where short_name = 'c15'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vandalblast'),
    (select id from sets where short_name = 'c15'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Satyr Wayfinder'),
    (select id from sets where short_name = 'c15'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reins of Power'),
    (select id from sets where short_name = 'c15'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cold-Eyed Selkie'),
    (select id from sets where short_name = 'c15'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desert Twister'),
    (select id from sets where short_name = 'c15'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bident of Thassa'),
    (select id from sets where short_name = 'c15'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stolen Goods'),
    (select id from sets where short_name = 'c15'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Victimize'),
    (select id from sets where short_name = 'c15'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Arena'),
    (select id from sets where short_name = 'c15'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Centaur Vinecrasher'),
    (select id from sets where short_name = 'c15'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Cluestone'),
    (select id from sets where short_name = 'c15'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mazirek, Kraul Death Priest'),
    (select id from sets where short_name = 'c15'),
    '48',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Awaken the Sky Tyrant'),
    (select id from sets where short_name = 'c15'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghost Quarter'),
    (select id from sets where short_name = 'c15'),
    '285',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lone Revenant'),
    (select id from sets where short_name = 'c15'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orochi Hatchery'),
    (select id from sets where short_name = 'c15'),
    '260',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = 'c15'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stroke of Genius'),
    (select id from sets where short_name = 'c15'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wood Elves'),
    (select id from sets where short_name = 'c15'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eternal Witness'),
    (select id from sets where short_name = 'c15'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dawn to Dusk'),
    (select id from sets where short_name = 'c15'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Garrison'),
    (select id from sets where short_name = 'c15'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overwhelming Stampede'),
    (select id from sets where short_name = 'c15'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anya, Merciless Angel'),
    (select id from sets where short_name = 'c15'),
    '41',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rite of Replication'),
    (select id from sets where short_name = 'c15'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c15'),
    '340',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orzhov Guildgate'),
    (select id from sets where short_name = 'c15'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Synthetic Destiny'),
    (select id from sets where short_name = 'c15'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reliquary Tower'),
    (select id from sets where short_name = 'c15'),
    '301',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mizzix''s Mastery'),
    (select id from sets where short_name = 'c15'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Open the Vaults'),
    (select id from sets where short_name = 'c15'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thief of Blood'),
    (select id from sets where short_name = 'c15'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Simic Signet'),
    (select id from sets where short_name = 'c15'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blade of Selves'),
    (select id from sets where short_name = 'c15'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vow of Malice'),
    (select id from sets where short_name = 'c15'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c15'),
    '325',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windfall'),
    (select id from sets where short_name = 'c15'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sleep'),
    (select id from sets where short_name = 'c15'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terastodon'),
    (select id from sets where short_name = 'c15'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Visionary'),
    (select id from sets where short_name = 'c15'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wretched Confluence'),
    (select id from sets where short_name = 'c15'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fallen Ideal'),
    (select id from sets where short_name = 'c15'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bastion Protector'),
    (select id from sets where short_name = 'c15'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c15'),
    '334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Preordain'),
    (select id from sets where short_name = 'c15'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arachnogenesis'),
    (select id from sets where short_name = 'c15'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Altar''s Reap'),
    (select id from sets where short_name = 'c15'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seer''s Sundial'),
    (select id from sets where short_name = 'c15'),
    '264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiery Confluence'),
    (select id from sets where short_name = 'c15'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prime Speaker Zegana'),
    (select id from sets where short_name = 'c15'),
    '230',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Slippery Karst'),
    (select id from sets where short_name = 'c15'),
    '307',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mosswort Bridge'),
    (select id from sets where short_name = 'c15'),
    '294',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death Grasp'),
    (select id from sets where short_name = 'c15'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darksteel Ingot'),
    (select id from sets where short_name = 'c15'),
    '251',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mystic Confluence'),
    (select id from sets where short_name = 'c15'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aura of Silence'),
    (select id from sets where short_name = 'c15'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blue Sun''s Zenith'),
    (select id from sets where short_name = 'c15'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drifting Meadow'),
    (select id from sets where short_name = 'c15'),
    '282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Polluted Mire'),
    (select id from sets where short_name = 'c15'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Word of Seizing'),
    (select id from sets where short_name = 'c15'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kor Sanctifiers'),
    (select id from sets where short_name = 'c15'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grisly Salvage'),
    (select id from sets where short_name = 'c15'),
    '222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talrand, Sky Summoner'),
    (select id from sets where short_name = 'c15'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mizzium Mortars'),
    (select id from sets where short_name = 'c15'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloudthresher'),
    (select id from sets where short_name = 'c15'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wayfarer''s Bauble'),
    (select id from sets where short_name = 'c15'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c15'),
    '332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arbor Colossus'),
    (select id from sets where short_name = 'c15'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Daxos''s Torment'),
    (select id from sets where short_name = 'c15'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestial Archon'),
    (select id from sets where short_name = 'c15'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Extractor Demon'),
    (select id from sets where short_name = 'c15'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crib Swap'),
    (select id from sets where short_name = 'c15'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dream Pillager'),
    (select id from sets where short_name = 'c15'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Herald of the Host'),
    (select id from sets where short_name = 'c15'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Experiment One'),
    (select id from sets where short_name = 'c15'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c15'),
    '327',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karlov of the Ghost Council'),
    (select id from sets where short_name = 'c15'),
    '46',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Act of Aggression'),
    (select id from sets where short_name = 'c15'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Biomantic Mastery'),
    (select id from sets where short_name = 'c15'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hamletback Goliath'),
    (select id from sets where short_name = 'c15'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Rage'),
    (select id from sets where short_name = 'c15'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Comet Storm'),
    (select id from sets where short_name = 'c15'),
    '146',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'c15'),
    '314',
    'common'
) ,
(
    (select id from mtgcard where name = 'Caller of the Claw'),
    (select id from sets where short_name = 'c15'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'c15'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bonehoard'),
    (select id from sets where short_name = 'c15'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivid Marsh'),
    (select id from sets where short_name = 'c15'),
    '319',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dreamstone Hedron'),
    (select id from sets where short_name = 'c15'),
    '252',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Banishing Light'),
    (select id from sets where short_name = 'c15'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vivid Meadow'),
    (select id from sets where short_name = 'c15'),
    '320',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kalemne''s Captain'),
    (select id from sets where short_name = 'c15'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treasury Thrull'),
    (select id from sets where short_name = 'c15'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hostility'),
    (select id from sets where short_name = 'c15'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silent Sentinel'),
    (select id from sets where short_name = 'c15'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunrise Sovereign'),
    (select id from sets where short_name = 'c15'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Plaguelord'),
    (select id from sets where short_name = 'c15'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ambition''s Cost'),
    (select id from sets where short_name = 'c15'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thornwood Falls'),
    (select id from sets where short_name = 'c15'),
    '315',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krosan Grip'),
    (select id from sets where short_name = 'c15'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c15'),
    '326',
    'common'
) ,
(
    (select id from mtgcard where name = 'Celestial Ancient'),
    (select id from sets where short_name = 'c15'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wind-Scarred Crag'),
    (select id from sets where short_name = 'c15'),
    '321',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stinkdrinker Daredevil'),
    (select id from sets where short_name = 'c15'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gisela, Blade of Goldnight'),
    (select id from sets where short_name = 'c15'),
    '219',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Orzhov Signet'),
    (select id from sets where short_name = 'c15'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shriekmaw'),
    (select id from sets where short_name = 'c15'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warstorm Surge'),
    (select id from sets where short_name = 'c15'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cobra Trap'),
    (select id from sets where short_name = 'c15'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c15'),
    '333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rogue''s Passage'),
    (select id from sets where short_name = 'c15'),
    '302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forgotten Cave'),
    (select id from sets where short_name = 'c15'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c15'),
    '329',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coiling Oracle'),
    (select id from sets where short_name = 'c15'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prophetic Bolt'),
    (select id from sets where short_name = 'c15'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystic Retrieval'),
    (select id from sets where short_name = 'c15'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c15'),
    '331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barren Moor'),
    (select id from sets where short_name = 'c15'),
    '277',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dawnglare Invoker'),
    (select id from sets where short_name = 'c15'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seal of Cleansing'),
    (select id from sets where short_name = 'c15'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dread Summons'),
    (select id from sets where short_name = 'c15'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kessig Cagebreakers'),
    (select id from sets where short_name = 'c15'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stingerfling Spider'),
    (select id from sets where short_name = 'c15'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c15'),
    '335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seal of the Guildpact'),
    (select id from sets where short_name = 'c15'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meteor Blast'),
    (select id from sets where short_name = 'c15'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swiftfoot Boots'),
    (select id from sets where short_name = 'c15'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c15'),
    '328',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = 'c15'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Worn Powerstone'),
    (select id from sets where short_name = 'c15'),
    '275',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Etherium-Horn Sorcerer'),
    (select id from sets where short_name = 'c15'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warchief Giant'),
    (select id from sets where short_name = 'c15'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deadly Tempest'),
    (select id from sets where short_name = 'c15'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dominate'),
    (select id from sets where short_name = 'c15'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Curse of the Nightly Hunt'),
    (select id from sets where short_name = 'c15'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loxodon Warhammer'),
    (select id from sets where short_name = 'c15'),
    '258',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Blossoms'),
    (select id from sets where short_name = 'c15'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Simic Keyrune'),
    (select id from sets where short_name = 'c15'),
    '265',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel of Serenity'),
    (select id from sets where short_name = 'c15'),
    '58',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Great Oak Guardian'),
    (select id from sets where short_name = 'c15'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kalemne, Disciple of Iroas'),
    (select id from sets where short_name = 'c15'),
    '45',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Breath of Darigaaz'),
    (select id from sets where short_name = 'c15'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Secluded Steppe'),
    (select id from sets where short_name = 'c15'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trygon Predator'),
    (select id from sets where short_name = 'c15'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coldsteel Heart'),
    (select id from sets where short_name = 'c15'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zoetic Cavern'),
    (select id from sets where short_name = 'c15'),
    '322',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disaster Radius'),
    (select id from sets where short_name = 'c15'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tainted Wood'),
    (select id from sets where short_name = 'c15'),
    '312',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sandstone Oracle'),
    (select id from sets where short_name = 'c15'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Craving'),
    (select id from sets where short_name = 'c15'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mulldrifter'),
    (select id from sets where short_name = 'c15'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spider Spawning'),
    (select id from sets where short_name = 'c15'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Rot Farm'),
    (select id from sets where short_name = 'c15'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivid Grove'),
    (select id from sets where short_name = 'c15'),
    '318',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inferno Titan'),
    (select id from sets where short_name = 'c15'),
    '160',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blood Bairn'),
    (select id from sets where short_name = 'c15'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Righteous Confluence'),
    (select id from sets where short_name = 'c15'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skullwinder'),
    (select id from sets where short_name = 'c15'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Faithless Looting'),
    (select id from sets where short_name = 'c15'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Monument'),
    (select id from sets where short_name = 'c15'),
    '253',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c15'),
    '337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mizzix of the Izmagnus'),
    (select id from sets where short_name = 'c15'),
    '50',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lightning Greaves'),
    (select id from sets where short_name = 'c15'),
    '257',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Taurean Mauler'),
    (select id from sets where short_name = 'c15'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Doomwake Giant'),
    (select id from sets where short_name = 'c15'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Command Tower'),
    (select id from sets where short_name = 'c15'),
    '281',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sun Titan'),
    (select id from sets where short_name = 'c15'),
    '82',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rite of the Raging Storm'),
    (select id from sets where short_name = 'c15'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blasted Landscape'),
    (select id from sets where short_name = 'c15'),
    '278',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Patagia Viper'),
    (select id from sets where short_name = 'c15'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Noble Quarry'),
    (select id from sets where short_name = 'c15'),
    '193',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Psychosis Crawler'),
    (select id from sets where short_name = 'c15'),
    '263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skullclamp'),
    (select id from sets where short_name = 'c15'),
    '267',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'High Market'),
    (select id from sets where short_name = 'c15'),
    '289',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vow of Duty'),
    (select id from sets where short_name = 'c15'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Champion of Stray Souls'),
    (select id from sets where short_name = 'c15'),
    '119',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Victory''s Herald'),
    (select id from sets where short_name = 'c15'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forgotten Ancient'),
    (select id from sets where short_name = 'c15'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tribute to the Wild'),
    (select id from sets where short_name = 'c15'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Verdant Confluence'),
    (select id from sets where short_name = 'c15'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seal of Doom'),
    (select id from sets where short_name = 'c15'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Repeal'),
    (select id from sets where short_name = 'c15'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grim Backwoods'),
    (select id from sets where short_name = 'c15'),
    '288',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magus of the Wheel'),
    (select id from sets where short_name = 'c15'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c15'),
    '324',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thought Vessel'),
    (select id from sets where short_name = 'c15'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vulturous Zombie'),
    (select id from sets where short_name = 'c15'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magmaquake'),
    (select id from sets where short_name = 'c15'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Mage'),
    (select id from sets where short_name = 'c15'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Echoing Truth'),
    (select id from sets where short_name = 'c15'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jareth, Leonine Titan'),
    (select id from sets where short_name = 'c15'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thundercloud Shaman'),
    (select id from sets where short_name = 'c15'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dawnbreak Reclaimer'),
    (select id from sets where short_name = 'c15'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Rager'),
    (select id from sets where short_name = 'c15'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c15'),
    '338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basalt Monolith'),
    (select id from sets where short_name = 'c15'),
    '244',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mystic Snake'),
    (select id from sets where short_name = 'c15'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fact or Fiction'),
    (select id from sets where short_name = 'c15'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'c15'),
    '283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pathbreaker Ibex'),
    (select id from sets where short_name = 'c15'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eater of Hope'),
    (select id from sets where short_name = 'c15'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overrun'),
    (select id from sets where short_name = 'c15'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rapid Hybridization'),
    (select id from sets where short_name = 'c15'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karmic Justice'),
    (select id from sets where short_name = 'c15'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gild'),
    (select id from sets where short_name = 'c15'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Broodbirth Viper'),
    (select id from sets where short_name = 'c15'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bane of Progress'),
    (select id from sets where short_name = 'c15'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivid Crag'),
    (select id from sets where short_name = 'c15'),
    '316',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kaseto, Orochi Archmage'),
    (select id from sets where short_name = 'c15'),
    '47',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Boros Guildgate'),
    (select id from sets where short_name = 'c15'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Stone'),
    (select id from sets where short_name = 'c15'),
    '259',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ezuri, Claw of Progress'),
    (select id from sets where short_name = 'c15'),
    '44',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'New Benalia'),
    (select id from sets where short_name = 'c15'),
    '295',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c15'),
    '342',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodspore Thrinax'),
    (select id from sets where short_name = 'c15'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oreskos Explorer'),
    (select id from sets where short_name = 'c15'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meren of Clan Nel Toth'),
    (select id from sets where short_name = 'c15'),
    '49',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hunted Dragon'),
    (select id from sets where short_name = 'c15'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Izzet Signet'),
    (select id from sets where short_name = 'c15'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kodama''s Reach'),
    (select id from sets where short_name = 'c15'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plaxmanta'),
    (select id from sets where short_name = 'c15'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Butcher of Malakir'),
    (select id from sets where short_name = 'c15'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verdant Force'),
    (select id from sets where short_name = 'c15'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dictate of Heliod'),
    (select id from sets where short_name = 'c15'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desolation Giant'),
    (select id from sets where short_name = 'c15'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marshal''s Anthem'),
    (select id from sets where short_name = 'c15'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tainted Field'),
    (select id from sets where short_name = 'c15'),
    '311',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Incubator'),
    (select id from sets where short_name = 'c15'),
    '273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fellwar Stone'),
    (select id from sets where short_name = 'c15'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wistful Selkie'),
    (select id from sets where short_name = 'c15'),
    '243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scourge of Nel Toth'),
    (select id from sets where short_name = 'c15'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreadbringer Lampads'),
    (select id from sets where short_name = 'c15'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faith''s Fetters'),
    (select id from sets where short_name = 'c15'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Staff of Nin'),
    (select id from sets where short_name = 'c15'),
    '270',
    'rare'
) 
 on conflict do nothing;
