insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Monk'),
    (select id from sets where short_name = 'tfrf'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warrior'),
    (select id from sets where short_name = 'tfrf'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tfrf'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manifest'),
    (select id from sets where short_name = 'tfrf'),
    '4',
    'common'
) 
 on conflict do nothing;
