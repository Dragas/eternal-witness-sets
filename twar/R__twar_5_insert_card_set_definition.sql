insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Zombie Army'),
    (select id from sets where short_name = 'twar'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'twar'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Warrior'),
    (select id from sets where short_name = 'twar'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Army'),
    (select id from sets where short_name = 'twar'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voja, Friend to Elves'),
    (select id from sets where short_name = 'twar'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Army'),
    (select id from sets where short_name = 'twar'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'twar'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nissa, Who Shakes the World Emblem'),
    (select id from sets where short_name = 'twar'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Devil'),
    (select id from sets where short_name = 'twar'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Servo'),
    (select id from sets where short_name = 'twar'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wizard'),
    (select id from sets where short_name = 'twar'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'twar'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'twar'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'twar'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Assassin'),
    (select id from sets where short_name = 'twar'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'twar'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'twar'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Citizen'),
    (select id from sets where short_name = 'twar'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall'),
    (select id from sets where short_name = 'twar'),
    '4',
    'common'
) 
 on conflict do nothing;
