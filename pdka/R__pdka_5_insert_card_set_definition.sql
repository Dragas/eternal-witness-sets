insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Gravecrawler'),
    (select id from sets where short_name = 'pdka'),
    '*64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zombie Apocalypse'),
    (select id from sets where short_name = 'pdka'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mondronen Shaman // Tovolar''s Magehunter'),
    (select id from sets where short_name = 'pdka'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ravenous Demon // Archdemon of Greed'),
    (select id from sets where short_name = 'pdka'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strangleroot Geist'),
    (select id from sets where short_name = 'pdka'),
    '127',
    'uncommon'
) 
 on conflict do nothing;
