    insert into mtgcard(name) values ('Gravecrawler') on conflict do nothing;
    insert into mtgcard(name) values ('Zombie Apocalypse') on conflict do nothing;
    insert into mtgcard(name) values ('Mondronen Shaman // Tovolar''s Magehunter') on conflict do nothing;
    insert into mtgcard(name) values ('Ravenous Demon // Archdemon of Greed') on conflict do nothing;
    insert into mtgcard(name) values ('Strangleroot Geist') on conflict do nothing;
