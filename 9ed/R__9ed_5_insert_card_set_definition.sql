insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Coral Eel'),
    (select id from sets where short_name = '9ed'),
    'S3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Festering Goblin'),
    (select id from sets where short_name = '9ed'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spined Wurm'),
    (select id from sets where short_name = '9ed'),
    'S10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Index'),
    (select id from sets where short_name = '9ed'),
    'S5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Megrim'),
    (select id from sets where short_name = '9ed'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blaze'),
    (select id from sets where short_name = '9ed'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Storm Crow'),
    (select id from sets where short_name = '9ed'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Skyhunter'),
    (select id from sets where short_name = '9ed'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fugitive Wizard'),
    (select id from sets where short_name = '9ed'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blackmail'),
    (select id from sets where short_name = '9ed'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Balloon Brigade'),
    (select id from sets where short_name = '9ed'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Samite Healer'),
    (select id from sets where short_name = '9ed'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sudden Impact'),
    (select id from sets where short_name = '9ed'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = '9ed'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eager Cadet'),
    (select id from sets where short_name = '9ed'),
    'S1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = '9ed'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rootwalla'),
    (select id from sets where short_name = '9ed'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Confiscate'),
    (select id from sets where short_name = '9ed'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brushland'),
    (select id from sets where short_name = '9ed'),
    '319',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rod of Ruin'),
    (select id from sets where short_name = '9ed'),
    '307',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pegasus Charger'),
    (select id from sets where short_name = '9ed'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Berserker'),
    (select id from sets where short_name = '9ed'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temporal Adept'),
    (select id from sets where short_name = '9ed'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seething Song'),
    (select id from sets where short_name = '9ed'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baleful Stare'),
    (select id from sets where short_name = '9ed'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oracle''s Attendants'),
    (select id from sets where short_name = '9ed'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azure Drake'),
    (select id from sets where short_name = '9ed'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Chariot'),
    (select id from sets where short_name = '9ed'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reflexes'),
    (select id from sets where short_name = '9ed'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Artillery'),
    (select id from sets where short_name = '9ed'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Greater Good'),
    (select id from sets where short_name = '9ed'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '9ed'),
    '349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctum Guardian'),
    (select id from sets where short_name = '9ed'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zodiac Monkey'),
    (select id from sets where short_name = '9ed'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Remove Soul'),
    (select id from sets where short_name = '9ed'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Biorhythm'),
    (select id from sets where short_name = '9ed'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Enchantress'),
    (select id from sets where short_name = '9ed'),
    '284',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Gargantua'),
    (select id from sets where short_name = '9ed'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blood Moon'),
    (select id from sets where short_name = '9ed'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viashino Sandstalker'),
    (select id from sets where short_name = '9ed'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Traumatize'),
    (select id from sets where short_name = '9ed'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Craw Wurm'),
    (select id from sets where short_name = '9ed'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Booby Trap'),
    (select id from sets where short_name = '9ed'),
    '289',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demolish'),
    (select id from sets where short_name = '9ed'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hunted Wumpus'),
    (select id from sets where short_name = '9ed'),
    '248',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Highway Robber'),
    (select id from sets where short_name = '9ed'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Feast'),
    (select id from sets where short_name = '9ed'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thought Courier'),
    (select id from sets where short_name = '9ed'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Form of the Dragon'),
    (select id from sets where short_name = '9ed'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aven Windreader'),
    (select id from sets where short_name = '9ed'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Specter'),
    (select id from sets where short_name = '9ed'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Honor Guard'),
    (select id from sets where short_name = '9ed'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viridian Shaman'),
    (select id from sets where short_name = '9ed'),
    '280',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Banishing'),
    (select id from sets where short_name = '9ed'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Early Harvest'),
    (select id from sets where short_name = '9ed'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blanchwood Armor'),
    (select id from sets where short_name = '9ed'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Counsel of the Soratami'),
    (select id from sets where short_name = '9ed'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foul Imp'),
    (select id from sets where short_name = '9ed'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scaled Wurm'),
    (select id from sets where short_name = '9ed'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Cockroach'),
    (select id from sets where short_name = '9ed'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kird Ape'),
    (select id from sets where short_name = '9ed'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flashfires'),
    (select id from sets where short_name = '9ed'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vulshok Morningstar'),
    (select id from sets where short_name = '9ed'),
    '315',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grave Pact'),
    (select id from sets where short_name = '9ed'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Elemental'),
    (select id from sets where short_name = '9ed'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '9ed'),
    '344',
    'common'
) ,
(
    (select id from mtgcard where name = 'Storage Matrix'),
    (select id from sets where short_name = '9ed'),
    '310',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '9ed'),
    '348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Warrior'),
    (select id from sets where short_name = '9ed'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slate of Ancestry'),
    (select id from sets where short_name = '9ed'),
    '308',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jester''s Cap'),
    (select id from sets where short_name = '9ed'),
    '301',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archivist'),
    (select id from sets where short_name = '9ed'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enrage'),
    (select id from sets where short_name = '9ed'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Regeneration'),
    (select id from sets where short_name = '9ed'),
    '265',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bog Wraith'),
    (select id from sets where short_name = '9ed'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Order of the Sacred Bell'),
    (select id from sets where short_name = '9ed'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Veteran Cavalier'),
    (select id from sets where short_name = '9ed'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = '9ed'),
    '328',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vengeance'),
    (select id from sets where short_name = '9ed'),
    'S2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Threaten'),
    (select id from sets where short_name = '9ed'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drudge Skeletons'),
    (select id from sets where short_name = '9ed'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Piker'),
    (select id from sets where short_name = '9ed'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coat of Arms'),
    (select id from sets where short_name = '9ed'),
    '291',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mortivore'),
    (select id from sets where short_name = '9ed'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stream of Life'),
    (select id from sets where short_name = '9ed'),
    '272',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin King'),
    (select id from sets where short_name = '9ed'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glorious Anthem'),
    (select id from sets where short_name = '9ed'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loxodon Warhammer'),
    (select id from sets where short_name = '9ed'),
    '303',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spirit Link'),
    (select id from sets where short_name = '9ed'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thran Golem'),
    (select id from sets where short_name = '9ed'),
    '313',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spineless Thug'),
    (select id from sets where short_name = '9ed'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Creeping Mold'),
    (select id from sets where short_name = '9ed'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trained Armodon'),
    (select id from sets where short_name = '9ed'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cruel Edict'),
    (select id from sets where short_name = '9ed'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unholy Strength'),
    (select id from sets where short_name = '9ed'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infantry Veteran'),
    (select id from sets where short_name = '9ed'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Suntail Hawk'),
    (select id from sets where short_name = '9ed'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aladdin''s Ring'),
    (select id from sets where short_name = '9ed'),
    '286',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Adarkar Wastes'),
    (select id from sets where short_name = '9ed'),
    '317',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Puzzle Box'),
    (select id from sets where short_name = '9ed'),
    '312',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kami of Old Stone'),
    (select id from sets where short_name = '9ed'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Groundskeeper'),
    (select id from sets where short_name = '9ed'),
    '247',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Peace of Mind'),
    (select id from sets where short_name = '9ed'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Imaginary Pet'),
    (select id from sets where short_name = '9ed'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '9ed'),
    '341',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tempest of Light'),
    (select id from sets where short_name = '9ed'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aven Flock'),
    (select id from sets where short_name = '9ed'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Looming Shade'),
    (select id from sets where short_name = '9ed'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tree Monkey'),
    (select id from sets where short_name = '9ed'),
    '275',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel''s Feather'),
    (select id from sets where short_name = '9ed'),
    '287',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rewind'),
    (select id from sets where short_name = '9ed'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Howling Mine'),
    (select id from sets where short_name = '9ed'),
    '298',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Contaminated Bond'),
    (select id from sets where short_name = '9ed'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Natural Affinity'),
    (select id from sets where short_name = '9ed'),
    '256',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angelic Blessing'),
    (select id from sets where short_name = '9ed'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Warden'),
    (select id from sets where short_name = '9ed'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glory Seeker'),
    (select id from sets where short_name = '9ed'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Utopia Tree'),
    (select id from sets where short_name = '9ed'),
    '277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Worship'),
    (select id from sets where short_name = '9ed'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enfeeblement'),
    (select id from sets where short_name = '9ed'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ley Druid'),
    (select id from sets where short_name = '9ed'),
    '251',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '9ed'),
    '340',
    'common'
) ,
(
    (select id from mtgcard where name = 'Norwood Ranger'),
    (select id from sets where short_name = '9ed'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '9ed'),
    '339',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '9ed'),
    '347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ur-Golem''s Eye'),
    (select id from sets where short_name = '9ed'),
    '314',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = '9ed'),
    '329',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'River Bear'),
    (select id from sets where short_name = '9ed'),
    '266',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hollow Dogs'),
    (select id from sets where short_name = '9ed'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treetop Bracers'),
    (select id from sets where short_name = '9ed'),
    '276',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seasoned Marshal'),
    (select id from sets where short_name = '9ed'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mending Hands'),
    (select id from sets where short_name = '9ed'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Arena'),
    (select id from sets where short_name = '9ed'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scathe Zombies'),
    (select id from sets where short_name = '9ed'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Persecute'),
    (select id from sets where short_name = '9ed'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disrupting Scepter'),
    (select id from sets where short_name = '9ed'),
    '295',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mogg Sentry'),
    (select id from sets where short_name = '9ed'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wanderguard Sentry'),
    (select id from sets where short_name = '9ed'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fleeting Image'),
    (select id from sets where short_name = '9ed'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tidal Kraken'),
    (select id from sets where short_name = '9ed'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Bend'),
    (select id from sets where short_name = '9ed'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trade Routes'),
    (select id from sets where short_name = '9ed'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quicksand'),
    (select id from sets where short_name = '9ed'),
    '323',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = '9ed'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Will-o''-the-Wisp'),
    (select id from sets where short_name = '9ed'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fellwar Stone'),
    (select id from sets where short_name = '9ed'),
    '297',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Holy Day'),
    (select id from sets where short_name = '9ed'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Underworld Dreams'),
    (select id from sets where short_name = '9ed'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ballista Squad'),
    (select id from sets where short_name = '9ed'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = '9ed'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inspirit'),
    (select id from sets where short_name = '9ed'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spellbook'),
    (select id from sets where short_name = '9ed'),
    '309',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warrior''s Honor'),
    (select id from sets where short_name = '9ed'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reverse Damage'),
    (select id from sets where short_name = '9ed'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Leak'),
    (select id from sets where short_name = '9ed'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silklash Spider'),
    (select id from sets where short_name = '9ed'),
    '271',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra''s Blessing'),
    (select id from sets where short_name = '9ed'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Master Decoy'),
    (select id from sets where short_name = '9ed'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Fisher'),
    (select id from sets where short_name = '9ed'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Hulk'),
    (select id from sets where short_name = '9ed'),
    '306',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flight'),
    (select id from sets where short_name = '9ed'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flowstone Shambler'),
    (select id from sets where short_name = '9ed'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sift'),
    (select id from sets where short_name = '9ed'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flowstone Slide'),
    (select id from sets where short_name = '9ed'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death Pits of Rath'),
    (select id from sets where short_name = '9ed'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cowardice'),
    (select id from sets where short_name = '9ed'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dehydration'),
    (select id from sets where short_name = '9ed'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enormous Baloth'),
    (select id from sets where short_name = '9ed'),
    'S9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thieving Magpie'),
    (select id from sets where short_name = '9ed'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fear'),
    (select id from sets where short_name = '9ed'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blinking Spirit'),
    (select id from sets where short_name = '9ed'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yawgmoth Demon'),
    (select id from sets where short_name = '9ed'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demystify'),
    (select id from sets where short_name = '9ed'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karplusan Yeti'),
    (select id from sets where short_name = '9ed'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Millstone'),
    (select id from sets where short_name = '9ed'),
    '304',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coercion'),
    (select id from sets where short_name = '9ed'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blessed Orator'),
    (select id from sets where short_name = '9ed'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daring Apprentice'),
    (select id from sets where short_name = '9ed'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea Monster'),
    (select id from sets where short_name = '9ed'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wind Drake'),
    (select id from sets where short_name = '9ed'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thundermare'),
    (select id from sets where short_name = '9ed'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = '9ed'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Final Punishment'),
    (select id from sets where short_name = '9ed'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zur''s Weirding'),
    (select id from sets where short_name = '9ed'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '9ed'),
    '334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '9ed'),
    '331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zealous Inquisitor'),
    (select id from sets where short_name = '9ed'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deathgazer'),
    (select id from sets where short_name = '9ed'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gluttonous Zombie'),
    (select id from sets where short_name = '9ed'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Holy Strength'),
    (select id from sets where short_name = '9ed'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chastise'),
    (select id from sets where short_name = '9ed'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Force of Nature'),
    (select id from sets where short_name = '9ed'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = '9ed'),
    '327',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anaba Shaman'),
    (select id from sets where short_name = '9ed'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plague Wind'),
    (select id from sets where short_name = '9ed'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sandstone Warrior'),
    (select id from sets where short_name = '9ed'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whip Sergeant'),
    (select id from sets where short_name = '9ed'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mahamoti Djinn'),
    (select id from sets where short_name = '9ed'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plagiarize'),
    (select id from sets where short_name = '9ed'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Silverback'),
    (select id from sets where short_name = '9ed'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '9ed'),
    '332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wildfire'),
    (select id from sets where short_name = '9ed'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea''s Claim'),
    (select id from sets where short_name = '9ed'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'King Cheetah'),
    (select id from sets where short_name = '9ed'),
    '250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bog Imp'),
    (select id from sets where short_name = '9ed'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Righteousness'),
    (select id from sets where short_name = '9ed'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Sky Raider'),
    (select id from sets where short_name = '9ed'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Brigand'),
    (select id from sets where short_name = '9ed'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clone'),
    (select id from sets where short_name = '9ed'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '9ed'),
    '333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Mountaineer'),
    (select id from sets where short_name = '9ed'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Execute'),
    (select id from sets where short_name = '9ed'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weird Harvest'),
    (select id from sets where short_name = '9ed'),
    '282',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emperor Crocodile'),
    (select id from sets where short_name = '9ed'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rogue Kavu'),
    (select id from sets where short_name = '9ed'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nantuko Husk'),
    (select id from sets where short_name = '9ed'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Web'),
    (select id from sets where short_name = '9ed'),
    '281',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = '9ed'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maro'),
    (select id from sets where short_name = '9ed'),
    '254',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ornithopter'),
    (select id from sets where short_name = '9ed'),
    '305',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Panic Attack'),
    (select id from sets where short_name = '9ed'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Razortooth Rats'),
    (select id from sets where short_name = '9ed'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = '9ed'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '9ed'),
    '346',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rathi Dragon'),
    (select id from sets where short_name = '9ed'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ogre Taskmaster'),
    (select id from sets where short_name = '9ed'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guerrilla Tactics'),
    (select id from sets where short_name = '9ed'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demon''s Horn'),
    (select id from sets where short_name = '9ed'),
    '294',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vizzerdrix'),
    (select id from sets where short_name = '9ed'),
    'S7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gift of Estates'),
    (select id from sets where short_name = '9ed'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Might of Oaks'),
    (select id from sets where short_name = '9ed'),
    '255',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '9ed'),
    '336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Hammer'),
    (select id from sets where short_name = '9ed'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Coast'),
    (select id from sets where short_name = '9ed'),
    '330',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Champion'),
    (select id from sets where short_name = '9ed'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anaconda'),
    (select id from sets where short_name = '9ed'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = '9ed'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diabolic Tutor'),
    (select id from sets where short_name = '9ed'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grizzly Bears'),
    (select id from sets where short_name = '9ed'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Octopus'),
    (select id from sets where short_name = '9ed'),
    'S4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = '9ed'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Polymorph'),
    (select id from sets where short_name = '9ed'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Llanowar Wastes'),
    (select id from sets where short_name = '9ed'),
    '322',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Time Ebb'),
    (select id from sets where short_name = '9ed'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravenous Rats'),
    (select id from sets where short_name = '9ed'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lumengrid Warden'),
    (select id from sets where short_name = '9ed'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reclaim'),
    (select id from sets where short_name = '9ed'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombify'),
    (select id from sets where short_name = '9ed'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magnivore'),
    (select id from sets where short_name = '9ed'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wurm''s Tooth'),
    (select id from sets where short_name = '9ed'),
    '316',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rootbreaker Wurm'),
    (select id from sets where short_name = '9ed'),
    '267',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Exhaustion'),
    (select id from sets where short_name = '9ed'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jade Statue'),
    (select id from sets where short_name = '9ed'),
    '300',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sage Aven'),
    (select id from sets where short_name = '9ed'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slay'),
    (select id from sets where short_name = '9ed'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ivory Mask'),
    (select id from sets where short_name = '9ed'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Annex'),
    (select id from sets where short_name = '9ed'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Natural Spring'),
    (select id from sets where short_name = '9ed'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Levitation'),
    (select id from sets where short_name = '9ed'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Story Circle'),
    (select id from sets where short_name = '9ed'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relentless Assault'),
    (select id from sets where short_name = '9ed'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '9ed'),
    '343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raging Goblin'),
    (select id from sets where short_name = '9ed'),
    '209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hell''s Caretaker'),
    (select id from sets where short_name = '9ed'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = '9ed'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bottle Gnomes'),
    (select id from sets where short_name = '9ed'),
    '290',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Telepathy'),
    (select id from sets where short_name = '9ed'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swarm of Rats'),
    (select id from sets where short_name = '9ed'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Foot Soldiers'),
    (select id from sets where short_name = '9ed'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sacred Nectar'),
    (select id from sets where short_name = '9ed'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Clash'),
    (select id from sets where short_name = '9ed'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Underground River'),
    (select id from sets where short_name = '9ed'),
    '326',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = '9ed'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Warrior'),
    (select id from sets where short_name = '9ed'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '9ed'),
    '338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mindslicer'),
    (select id from sets where short_name = '9ed'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fishliver Oil'),
    (select id from sets where short_name = '9ed'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crafty Pathmage'),
    (select id from sets where short_name = '9ed'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = '9ed'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Master Healer'),
    (select id from sets where short_name = '9ed'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Venerable Monk'),
    (select id from sets where short_name = '9ed'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battle of Wits'),
    (select id from sets where short_name = '9ed'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verduran Enchantress'),
    (select id from sets where short_name = '9ed'),
    '279',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Puppeteer'),
    (select id from sets where short_name = '9ed'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hill Giant'),
    (select id from sets where short_name = '9ed'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balduvian Barbarians'),
    (select id from sets where short_name = '9ed'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evacuation'),
    (select id from sets where short_name = '9ed'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seedborn Muse'),
    (select id from sets where short_name = '9ed'),
    '270',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firebreathing'),
    (select id from sets where short_name = '9ed'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Piper'),
    (select id from sets where short_name = '9ed'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overgrowth'),
    (select id from sets where short_name = '9ed'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon''s Claw'),
    (select id from sets where short_name = '9ed'),
    '296',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flame Wave'),
    (select id from sets where short_name = '9ed'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Summer Bloom'),
    (select id from sets where short_name = '9ed'),
    '273',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boiling Seas'),
    (select id from sets where short_name = '9ed'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plague Beetle'),
    (select id from sets where short_name = '9ed'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sleight of Hand'),
    (select id from sets where short_name = '9ed'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = '9ed'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '9ed'),
    '345',
    'common'
) ,
(
    (select id from mtgcard where name = 'Royal Assassin'),
    (select id from sets where short_name = '9ed'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Battlefield Forge'),
    (select id from sets where short_name = '9ed'),
    '318',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verdant Force'),
    (select id from sets where short_name = '9ed'),
    '278',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sulfurous Springs'),
    (select id from sets where short_name = '9ed'),
    '325',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '9ed'),
    '342',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sacred Ground'),
    (select id from sets where short_name = '9ed'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = '9ed'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '9ed'),
    '337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paladin en-Vec'),
    (select id from sets where short_name = '9ed'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Horned Turtle'),
    (select id from sets where short_name = '9ed'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crossbow Infantry'),
    (select id from sets where short_name = '9ed'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savannah Lions'),
    (select id from sets where short_name = '9ed'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lord of the Undead'),
    (select id from sets where short_name = '9ed'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blinding Angel'),
    (select id from sets where short_name = '9ed'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '9ed'),
    '335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kavu Climber'),
    (select id from sets where short_name = '9ed'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Furnace of Rath'),
    (select id from sets where short_name = '9ed'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aven Cloudchaser'),
    (select id from sets where short_name = '9ed'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast of Burden'),
    (select id from sets where short_name = '9ed'),
    '288',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tidings'),
    (select id from sets where short_name = '9ed'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Bard'),
    (select id from sets where short_name = '9ed'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Icy Manipulator'),
    (select id from sets where short_name = '9ed'),
    '299',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marble Titan'),
    (select id from sets where short_name = '9ed'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rukh Egg'),
    (select id from sets where short_name = '9ed'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shard Phoenix'),
    (select id from sets where short_name = '9ed'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Caves of Koilos'),
    (select id from sets where short_name = '9ed'),
    '320',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = '9ed'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '9ed'),
    '350',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kraken''s Eye'),
    (select id from sets where short_name = '9ed'),
    '302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyroclasm'),
    (select id from sets where short_name = '9ed'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boomerang'),
    (select id from sets where short_name = '9ed'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karplusan Forest'),
    (select id from sets where short_name = '9ed'),
    '321',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dream Prowler'),
    (select id from sets where short_name = '9ed'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tanglebloom'),
    (select id from sets where short_name = '9ed'),
    '311',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Consume Spirit'),
    (select id from sets where short_name = '9ed'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = '9ed'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Black'),
    (select id from sets where short_name = '9ed'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dancing Scimitar'),
    (select id from sets where short_name = '9ed'),
    '292',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Reef'),
    (select id from sets where short_name = '9ed'),
    '324',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = '9ed'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Raider'),
    (select id from sets where short_name = '9ed'),
    'S8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure Trove'),
    (select id from sets where short_name = '9ed'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Withering Gaze'),
    (select id from sets where short_name = '9ed'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Needle Storm'),
    (select id from sets where short_name = '9ed'),
    '259',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = '9ed'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Behemoth'),
    (select id from sets where short_name = '9ed'),
    '252',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anarchist'),
    (select id from sets where short_name = '9ed'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wood Elves'),
    (select id from sets where short_name = '9ed'),
    '283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serpent Warrior'),
    (select id from sets where short_name = '9ed'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = '9ed'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Prowler'),
    (select id from sets where short_name = '9ed'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Mercy'),
    (select id from sets where short_name = '9ed'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Defense Grid'),
    (select id from sets where short_name = '9ed'),
    '293',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reminisce'),
    (select id from sets where short_name = '9ed'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lava Axe'),
    (select id from sets where short_name = '9ed'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nekrataal'),
    (select id from sets where short_name = '9ed'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Horror of Horrors'),
    (select id from sets where short_name = '9ed'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flowstone Crusher'),
    (select id from sets where short_name = '9ed'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodfire Colossus'),
    (select id from sets where short_name = '9ed'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Weathered Wayfarer'),
    (select id from sets where short_name = '9ed'),
    '54',
    'rare'
) 
 on conflict do nothing;
