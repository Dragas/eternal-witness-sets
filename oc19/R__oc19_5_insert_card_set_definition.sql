insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Anje Falkenrath'),
    (select id from sets where short_name = 'oc19'),
    '37',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sevinne, the Chronoclasm'),
    (select id from sets where short_name = 'oc19'),
    '49',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kadena, Slinking Sorcerer'),
    (select id from sets where short_name = 'oc19'),
    '45',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ghired, Conclave Exile'),
    (select id from sets where short_name = 'oc19'),
    '42',
    'mythic'
) 
 on conflict do nothing;
