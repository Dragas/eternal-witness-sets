insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tddd'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tddd'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elephant'),
    (select id from sets where short_name = 'tddd'),
    '3',
    'common'
) 
 on conflict do nothing;
