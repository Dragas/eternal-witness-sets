insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tc16'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird'),
    (select id from sets where short_name = 'tc16'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tc16'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daretti, Scrap Savant Emblem'),
    (select id from sets where short_name = 'tc16'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horror'),
    (select id from sets where short_name = 'tc16'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tc16'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tc16'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tc16'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ogre'),
    (select id from sets where short_name = 'tc16'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tc16'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter'),
    (select id from sets where short_name = 'tc16'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tc16'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr'),
    (select id from sets where short_name = 'tc16'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tc16'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elf Warrior'),
    (select id from sets where short_name = 'tc16'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goat'),
    (select id from sets where short_name = 'tc16'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tc16'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squid'),
    (select id from sets where short_name = 'tc16'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird'),
    (select id from sets where short_name = 'tc16'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Germ'),
    (select id from sets where short_name = 'tc16'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Worm'),
    (select id from sets where short_name = 'tc16'),
    '18',
    'common'
) 
 on conflict do nothing;
