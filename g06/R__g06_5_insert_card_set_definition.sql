insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Grim Lavamancer'),
    (select id from sets where short_name = 'g06'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exalted Angel'),
    (select id from sets where short_name = 'g06'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pernicious Deed'),
    (select id from sets where short_name = 'g06'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meddling Mage'),
    (select id from sets where short_name = 'g06'),
    '3',
    'rare'
) 
 on conflict do nothing;
