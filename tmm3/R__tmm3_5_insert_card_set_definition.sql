insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tmm3'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tmm3'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tmm3'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Warrior'),
    (select id from sets where short_name = 'tmm3'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tmm3'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tmm3'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ooze'),
    (select id from sets where short_name = 'tmm3'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird'),
    (select id from sets where short_name = 'tmm3'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurm'),
    (select id from sets where short_name = 'tmm3'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spider'),
    (select id from sets where short_name = 'tmm3'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golem'),
    (select id from sets where short_name = 'tmm3'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tmm3'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'tmm3'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tmm3'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Domri Rade Emblem'),
    (select id from sets where short_name = 'tmm3'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tmm3'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Warrior'),
    (select id from sets where short_name = 'tmm3'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tmm3'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tmm3'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Centaur'),
    (select id from sets where short_name = 'tmm3'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elephant'),
    (select id from sets where short_name = 'tmm3'),
    '12',
    'common'
) 
 on conflict do nothing;
