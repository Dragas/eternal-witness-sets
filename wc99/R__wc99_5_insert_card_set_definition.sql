insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Sphere of Resistance'),
    (select id from sets where short_name = 'wc99'),
    'js139sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc99'),
    'mlp346b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hammer of Bogardan'),
    (select id from sets where short_name = 'wc99'),
    'mlp188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jakub Šlemr Decklist (1999)'),
    (select id from sets where short_name = 'wc99'),
    'js0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avalanche Riders'),
    (select id from sets where short_name = 'wc99'),
    'mlp74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stromgald Cabal'),
    (select id from sets where short_name = 'wc99'),
    'js157sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cursed Scroll'),
    (select id from sets where short_name = 'wc99'),
    'mlp281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Albino Troll'),
    (select id from sets where short_name = 'wc99'),
    'ml231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Helix'),
    (select id from sets where short_name = 'wc99'),
    'kb302sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karn, Silver Golem'),
    (select id from sets where short_name = 'wc99'),
    'kb298',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evincar''s Justice'),
    (select id from sets where short_name = 'wc99'),
    'js134sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blank Card'),
    (select id from sets where short_name = 'wc99'),
    '00b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thran Dynamo'),
    (select id from sets where short_name = 'wc99'),
    'kb139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Tomb'),
    (select id from sets where short_name = 'wc99'),
    'kb315',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overrun'),
    (select id from sets where short_name = 'wc99'),
    'ml243sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc99'),
    'kb346',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Negator'),
    (select id from sets where short_name = 'wc99'),
    'js65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ticking Gnomes'),
    (select id from sets where short_name = 'wc99'),
    'js136b',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Uktabi Orangutan'),
    (select id from sets where short_name = 'wc99'),
    'ml260',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cursed Scroll'),
    (select id from sets where short_name = 'wc99'),
    'kb281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = 'wc99'),
    'mlp209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Matt Linde Bio'),
    (select id from sets where short_name = 'wc99'),
    'ml0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'River Boa'),
    (select id from sets where short_name = 'wc99'),
    'ml249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diabolic Edict'),
    (select id from sets where short_name = 'wc99'),
    'js128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yawgmoth''s Will'),
    (select id from sets where short_name = 'wc99'),
    'js171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc99'),
    'kb344',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thran Foundry'),
    (select id from sets where short_name = 'wc99'),
    'ml140sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mark Le Pine Bio'),
    (select id from sets where short_name = 'wc99'),
    'mlp0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Masticore'),
    (select id from sets where short_name = 'wc99'),
    'mlp134sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pouncing Jaguar'),
    (select id from sets where short_name = 'wc99'),
    'ml269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mark Le Pine Decklist'),
    (select id from sets where short_name = 'wc99'),
    'mlp0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cursed Scroll'),
    (select id from sets where short_name = 'wc99'),
    'ml281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treetop Village'),
    (select id from sets where short_name = 'wc99'),
    'ml143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shattering Pulse'),
    (select id from sets where short_name = 'wc99'),
    'mlp102sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hurricane'),
    (select id from sets where short_name = 'wc99'),
    'ml237sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cursed Scroll'),
    (select id from sets where short_name = 'wc99'),
    'js281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carrion Beetles'),
    (select id from sets where short_name = 'wc99'),
    'js122sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bottle Gnomes'),
    (select id from sets where short_name = 'wc99'),
    'js278',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Choke'),
    (select id from sets where short_name = 'wc99'),
    'ml219sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shattering Pulse'),
    (select id from sets where short_name = 'wc99'),
    'kb102sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireslinger'),
    (select id from sets where short_name = 'wc99'),
    'mlp173sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancient Tomb'),
    (select id from sets where short_name = 'wc99'),
    'mlp315',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc99'),
    'kb343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Powder Keg'),
    (select id from sets where short_name = 'wc99'),
    'js136a',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rancor'),
    (select id from sets where short_name = 'wc99'),
    'ml110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Matt Linde Decklist'),
    (select id from sets where short_name = 'wc99'),
    'ml0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Perish'),
    (select id from sets where short_name = 'wc99'),
    'js147sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arc Lightning'),
    (select id from sets where short_name = 'wc99'),
    'mlp174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wildfire'),
    (select id from sets where short_name = 'wc99'),
    'kb228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampiric Tutor'),
    (select id from sets where short_name = 'wc99'),
    'js161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temporal Aperture'),
    (select id from sets where short_name = 'wc99'),
    'kb310',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boil'),
    (select id from sets where short_name = 'wc99'),
    'kb165sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spawning Pool'),
    (select id from sets where short_name = 'wc99'),
    'js142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arc Lightning'),
    (select id from sets where short_name = 'wc99'),
    'mlp174sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Processor'),
    (select id from sets where short_name = 'wc99'),
    'kb306sb',
    'rare'
) ,
(
    (select id from mtgcard where name = '1999 World Championships Ad'),
    (select id from sets where short_name = 'wc99'),
    '0',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravenous Rats'),
    (select id from sets where short_name = 'wc99'),
    'js68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jackal Pup'),
    (select id from sets where short_name = 'wc99'),
    'mlp183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scald'),
    (select id from sets where short_name = 'wc99'),
    'mlp211sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'wc99'),
    'js127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'wc99'),
    'js132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Uktabi Orangutan'),
    (select id from sets where short_name = 'wc99'),
    'ml260sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Lyrist'),
    (select id from sets where short_name = 'wc99'),
    'ml248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wasteland'),
    (select id from sets where short_name = 'wc99'),
    'mlp330',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'wc99'),
    'ml239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jakub Šlemr Bio (1999)'),
    (select id from sets where short_name = 'wc99'),
    'js0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thran Foundry'),
    (select id from sets where short_name = 'wc99'),
    'mlp140sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rapid Decay'),
    (select id from sets where short_name = 'wc99'),
    'js67sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fireslinger'),
    (select id from sets where short_name = 'wc99'),
    'mlp173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mogg Fanatic'),
    (select id from sets where short_name = 'wc99'),
    'mlp190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kai Budde Bio'),
    (select id from sets where short_name = 'wc99'),
    'kb0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc99'),
    'ml347b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc99'),
    'js340a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc99'),
    'mlp346a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'wc99'),
    'mlp98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc99'),
    'js339',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grim Monolith'),
    (select id from sets where short_name = 'wc99'),
    'kb126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Constant Mists'),
    (select id from sets where short_name = 'wc99'),
    'ml104sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volrath''s Stronghold'),
    (select id from sets where short_name = 'wc99'),
    'js143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'City of Traitors'),
    (select id from sets where short_name = 'wc99'),
    'kb143a',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Persecute'),
    (select id from sets where short_name = 'wc99'),
    'js146sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kai Budde Decklist'),
    (select id from sets where short_name = 'wc99'),
    'kb0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Masticore'),
    (select id from sets where short_name = 'wc99'),
    'kb143b',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flowstone Flood'),
    (select id from sets where short_name = 'wc99'),
    'mlp83sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc99'),
    'js340b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Cradle'),
    (select id from sets where short_name = 'wc99'),
    'ml321',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wasteland'),
    (select id from sets where short_name = 'wc99'),
    'js330',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bottle Gnomes'),
    (select id from sets where short_name = 'wc99'),
    'js278sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'wc99'),
    'ml233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pillage'),
    (select id from sets where short_name = 'wc99'),
    'mlp198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rapid Decay'),
    (select id from sets where short_name = 'wc99'),
    'js67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wild Dogs'),
    (select id from sets where short_name = 'wc99'),
    'ml284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rack and Ruin'),
    (select id from sets where short_name = 'wc99'),
    'kb89sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Helix'),
    (select id from sets where short_name = 'wc99'),
    'kb302',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Weatherseed Treefolk'),
    (select id from sets where short_name = 'wc99'),
    'ml116sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Negator'),
    (select id from sets where short_name = 'wc99'),
    'js65sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corpse Dance'),
    (select id from sets where short_name = 'wc99'),
    'js116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stupor'),
    (select id from sets where short_name = 'wc99'),
    'js158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Plaguelord'),
    (select id from sets where short_name = 'wc99'),
    'js62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc99'),
    'ml349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = 'wc99'),
    'kb173sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fire Diamond'),
    (select id from sets where short_name = 'wc99'),
    'kb284',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc99'),
    'mlp344',
    'common'
) ,
(
    (select id from mtgcard where name = 'Worn Powerstone'),
    (select id from sets where short_name = 'wc99'),
    'kb318',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spellshock'),
    (select id from sets where short_name = 'wc99'),
    'kb104sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blank Card'),
    (select id from sets where short_name = 'wc99'),
    '00a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc99'),
    'ml347a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hatred'),
    (select id from sets where short_name = 'wc99'),
    'js64sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghitu Encampment'),
    (select id from sets where short_name = 'wc99'),
    'mlp141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Voltaic Key'),
    (select id from sets where short_name = 'wc99'),
    'kb314',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Covetous Dragon'),
    (select id from sets where short_name = 'wc99'),
    'kb80',
    'rare'
) 
 on conflict do nothing;
