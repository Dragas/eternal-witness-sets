    insert into mtgcard(name) values ('Sisters of Stone Death Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Fallen Angel Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Oni of Wild Places Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Dakkon Blackblade Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Sakashima the Impostor Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Ink-Eyes, Servant of Oni Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Prodigal Sorcerer Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Bosh, Iron Golem Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Rith, the Awakener Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Ashling, the Extinguisher Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Maro Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Maelstrom Archangel Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Mayael the Anima Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Seshiro the Anointed Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Arcbound Overseer Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Diamond Faerie Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Momir Vig, Simic Visionary Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Two-Headed Giant of Foriys Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Frenetic Efreet Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Master of the Wild Hunt Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Eladamri, Lord of Leaves Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Loxodon Hierarch Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Birds of Paradise Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Phage the Untouchable Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Dauntless Escort Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Serra Angel Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Mirror Entity Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Akroma, Angel of Wrath Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Murderous Redcap Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Haakon, Stromgald Scourge Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Reaper King Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Teysa, Orzhov Scion Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Jaya Ballard Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Vampire Nocturnus Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Warchief Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Morinfen Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Tradewind Rider Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Grinning Demon Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Kresh the Bloodbraided Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Maralen of the Mornsong Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Hell''s Caretaker Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Stuffy Doll Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Braids, Conjurer Adept Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Platinum Angel Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Sliver Queen Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Figure of Destiny Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Hermit Druid Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Etched Oracle Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Squee, Goblin Nabob Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Royal Assassin Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Elvish Champion Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Erhnam Djinn Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Ashling the Pilgrim Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Mirri the Cursed Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Heartwood Storyteller Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Nekrataal Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Eight-and-a-Half-Tails Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Enigma Sphinx Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Higure, the Still Wind Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Chronatog Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Flametongue Kavu Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Jhoira of the Ghitu Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Orcish Squatters Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Stalking Tiger Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Stonehewer Giant Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Lyzolda, the Blood Witch Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Raksha Golden Cub Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Karona, False God Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Viridian Zealot Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Arcanis, the Omnipotent Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Malfegor Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Necropotence Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Peacekeeper Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Rumbling Slum Avatar') on conflict do nothing;
