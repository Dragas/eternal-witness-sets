insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Sisters of Stone Death Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fallen Angel Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oni of Wild Places Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dakkon Blackblade Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sakashima the Impostor Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ink-Eyes, Servant of Oni Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prodigal Sorcerer Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bosh, Iron Golem Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rith, the Awakener Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rith, the Awakener Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashling, the Extinguisher Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maro Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Archangel Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mayael the Anima Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seshiro the Anointed Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcbound Overseer Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diamond Faerie Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Momir Vig, Simic Visionary Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '61a',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Two-Headed Giant of Foriys Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frenetic Efreet Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Master of the Wild Hunt Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eladamri, Lord of Leaves Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loxodon Hierarch Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phage the Untouchable Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dauntless Escort Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Angel Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirror Entity Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akroma, Angel of Wrath Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Murderous Redcap Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Haakon, Stromgald Scourge Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reaper King Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teysa, Orzhov Scion Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jaya Ballard Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampire Nocturnus Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Warchief Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Morinfen Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tradewind Rider Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grinning Demon Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kresh the Bloodbraided Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maralen of the Mornsong Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hell''s Caretaker Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stuffy Doll Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Braids, Conjurer Adept Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Platinum Angel Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sliver Queen Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Figure of Destiny Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hermit Druid Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Etched Oracle Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prodigal Sorcerer Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Warchief Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Squee, Goblin Nabob Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Royal Assassin Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tradewind Rider Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Champion Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Angel Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Erhnam Djinn Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashling the Pilgrim Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirri the Cursed Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heartwood Storyteller Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nekrataal Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eight-and-a-Half-Tails Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enigma Sphinx Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Higure, the Still Wind Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chronatog Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Platinum Angel Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jhoira of the Ghitu Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Squatters Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stalking Tiger Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stonehewer Giant Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lyzolda, the Blood Witch Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Royal Assassin Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raksha Golden Cub Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Erhnam Djinn Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karona, False God Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viridian Zealot Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grinning Demon Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcanis, the Omnipotent Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Malfegor Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Momir Vig, Simic Visionary Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necropotence Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Peacekeeper Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rumbling Slum Avatar'),
    (select id from sets where short_name = 'pmoa'),
    '56',
    'rare'
) 
 on conflict do nothing;
