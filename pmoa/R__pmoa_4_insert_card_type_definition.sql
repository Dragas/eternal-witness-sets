insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Sisters of Stone Death Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fallen Angel Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Oni of Wild Places Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dakkon Blackblade Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sakashima the Impostor Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ink-Eyes, Servant of Oni Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Prodigal Sorcerer Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bosh, Iron Golem Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rith, the Awakener Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rith, the Awakener Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ashling, the Extinguisher Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Maro Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Maelstrom Archangel Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mayael the Anima Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Seshiro the Anointed Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Arcbound Overseer Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Diamond Faerie Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Momir Vig, Simic Visionary Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Two-Headed Giant of Foriys Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Frenetic Efreet Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Master of the Wild Hunt Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eladamri, Lord of Leaves Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Loxodon Hierarch Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Birds of Paradise Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Phage the Untouchable Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dauntless Escort Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Serra Angel Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mirror Entity Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Akroma, Angel of Wrath Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Murderous Redcap Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Haakon, Stromgald Scourge Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Reaper King Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Teysa, Orzhov Scion Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jaya Ballard Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vampire Nocturnus Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goblin Warchief Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Morinfen Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tradewind Rider Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grinning Demon Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kresh the Bloodbraided Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Maralen of the Mornsong Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hell''s Caretaker Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stuffy Doll Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Braids, Conjurer Adept Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Platinum Angel Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sliver Queen Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Figure of Destiny Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hermit Druid Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Etched Oracle Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Prodigal Sorcerer Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Birds of Paradise Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goblin Warchief Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Squee, Goblin Nabob Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Royal Assassin Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tradewind Rider Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elvish Champion Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Serra Angel Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Erhnam Djinn Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ashling the Pilgrim Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mirri the Cursed Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Heartwood Storyteller Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nekrataal Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eight-and-a-Half-Tails Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Enigma Sphinx Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Higure, the Still Wind Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chronatog Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Platinum Angel Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Flametongue Kavu Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jhoira of the Ghitu Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Orcish Squatters Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stalking Tiger Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stonehewer Giant Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lyzolda, the Blood Witch Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Royal Assassin Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Raksha Golden Cub Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Erhnam Djinn Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Karona, False God Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Viridian Zealot Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grinning Demon Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Arcanis, the Omnipotent Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Malfegor Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Momir Vig, Simic Visionary Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Necropotence Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Peacekeeper Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rumbling Slum Avatar'),
        (select types.id from types where name = 'Vanguard')
    ) 
 on conflict do nothing;
