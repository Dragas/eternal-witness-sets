    insert into mtgcard(name) values ('Unmake') on conflict do nothing;
    insert into mtgcard(name) values ('Negate') on conflict do nothing;
    insert into mtgcard(name) values ('Flame Javelin') on conflict do nothing;
    insert into mtgcard(name) values ('Cryptic Command') on conflict do nothing;
    insert into mtgcard(name) values ('Rampant Growth') on conflict do nothing;
    insert into mtgcard(name) values ('Remove Soul') on conflict do nothing;
    insert into mtgcard(name) values ('Blightning') on conflict do nothing;
    insert into mtgcard(name) values ('Nameless Inversion') on conflict do nothing;
    insert into mtgcard(name) values ('Terminate') on conflict do nothing;
