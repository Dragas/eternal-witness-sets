insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Unmake'),
    (select id from sets where short_name = 'p09'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'p09'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flame Javelin'),
    (select id from sets where short_name = 'p09'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cryptic Command'),
    (select id from sets where short_name = 'p09'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = 'p09'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Remove Soul'),
    (select id from sets where short_name = 'p09'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blightning'),
    (select id from sets where short_name = 'p09'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nameless Inversion'),
    (select id from sets where short_name = 'p09'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terminate'),
    (select id from sets where short_name = 'p09'),
    '9',
    'rare'
) 
 on conflict do nothing;
