insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Tinker'),
    (select id from sets where short_name = 'v09'),
    '14',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Skullclamp'),
    (select id from sets where short_name = 'v09'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Strip Mine'),
    (select id from sets where short_name = 'v09'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Berserk'),
    (select id from sets where short_name = 'v09'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Serendib Efreet'),
    (select id from sets where short_name = 'v09'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gifts Ungiven'),
    (select id from sets where short_name = 'v09'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kird Ape'),
    (select id from sets where short_name = 'v09'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Trinisphere'),
    (select id from sets where short_name = 'v09'),
    '15',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Necropotence'),
    (select id from sets where short_name = 'v09'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Balance'),
    (select id from sets where short_name = 'v09'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lotus Petal'),
    (select id from sets where short_name = 'v09'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goblin Lackey'),
    (select id from sets where short_name = 'v09'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Channel'),
    (select id from sets where short_name = 'v09'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sensei''s Divining Top'),
    (select id from sets where short_name = 'v09'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mystical Tutor'),
    (select id from sets where short_name = 'v09'),
    '8',
    'mythic'
) 
 on conflict do nothing;
