insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Nezumi Cutthroat'),
    (select id from sets where short_name = 'phuk'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swarm of Rats'),
    (select id from sets where short_name = 'phuk'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dirge of Dread'),
    (select id from sets where short_name = 'phuk'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nezumi Graverobber // Nighteyes the Desecrator'),
    (select id from sets where short_name = 'phuk'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Larceny'),
    (select id from sets where short_name = 'phuk'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dirty Wererat'),
    (select id from sets where short_name = 'phuk'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rats'' Feast'),
    (select id from sets where short_name = 'phuk'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nocturnal Raid'),
    (select id from sets where short_name = 'phuk'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diabolic Tutor'),
    (select id from sets where short_name = 'phuk'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skullsnatcher'),
    (select id from sets where short_name = 'phuk'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chittering Rats'),
    (select id from sets where short_name = 'phuk'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravestorm'),
    (select id from sets where short_name = 'phuk'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nezumi Cutthroat'),
    (select id from sets where short_name = 'phuk'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swarm of Rats'),
    (select id from sets where short_name = 'phuk'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skullsnatcher'),
    (select id from sets where short_name = 'phuk'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crypt Rats'),
    (select id from sets where short_name = 'phuk'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crippling Fatigue'),
    (select id from sets where short_name = 'phuk'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nezumi Bone-Reader'),
    (select id from sets where short_name = 'phuk'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Patron of the Nezumi'),
    (select id from sets where short_name = 'phuk'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nezumi Graverobber // Nighteyes the Desecrator'),
    (select id from sets where short_name = 'phuk'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skullsnatcher'),
    (select id from sets where short_name = 'phuk'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marrow-Gnawer'),
    (select id from sets where short_name = 'phuk'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Suppress'),
    (select id from sets where short_name = 'phuk'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rats'' Feast'),
    (select id from sets where short_name = 'phuk'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swarm of Rats'),
    (select id from sets where short_name = 'phuk'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chittering Rats'),
    (select id from sets where short_name = 'phuk'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sever Soul'),
    (select id from sets where short_name = 'phuk'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carrion Rats'),
    (select id from sets where short_name = 'phuk'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chittering Rats'),
    (select id from sets where short_name = 'phuk'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carrion Rats'),
    (select id from sets where short_name = 'phuk'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crippling Fatigue'),
    (select id from sets where short_name = 'phuk'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Betrayal of Flesh'),
    (select id from sets where short_name = 'phuk'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dirty Wererat'),
    (select id from sets where short_name = 'phuk'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infernal Contract'),
    (select id from sets where short_name = 'phuk'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sever Soul'),
    (select id from sets where short_name = 'phuk'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carrion Rats'),
    (select id from sets where short_name = 'phuk'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'phuk'),
    '8',
    'common'
) 
 on conflict do nothing;
