insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Spell Blast'),
    (select id from sets where short_name = '4ed'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pikemen'),
    (select id from sets where short_name = '4ed'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rag Man'),
    (select id from sets where short_name = '4ed'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fire Elemental'),
    (select id from sets where short_name = '4ed'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zephyr Falcon'),
    (select id from sets where short_name = '4ed'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jade Monolith'),
    (select id from sets where short_name = '4ed'),
    '329',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Web'),
    (select id from sets where short_name = '4ed'),
    '287',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relic Bind'),
    (select id from sets where short_name = '4ed'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Colossus of Sardia'),
    (select id from sets where short_name = '4ed'),
    '308',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = '4ed'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cosmic Horror'),
    (select id from sets where short_name = '4ed'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Triskelion'),
    (select id from sets where short_name = '4ed'),
    '354',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Northern Paladin'),
    (select id from sets where short_name = '4ed'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pit Scorpion'),
    (select id from sets where short_name = '4ed'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Personal Incarnation'),
    (select id from sets where short_name = '4ed'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Power Surge'),
    (select id from sets where short_name = '4ed'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shapeshifter'),
    (select id from sets where short_name = '4ed'),
    '345',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Battle Gear'),
    (select id from sets where short_name = '4ed'),
    '296',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Forces'),
    (select id from sets where short_name = '4ed'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '4ed'),
    '373',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tundra Wolves'),
    (select id from sets where short_name = '4ed'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Terrain'),
    (select id from sets where short_name = '4ed'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pirate Ship'),
    (select id from sets where short_name = '4ed'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nafs Asp'),
    (select id from sets where short_name = '4ed'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clay Statue'),
    (select id from sets where short_name = '4ed'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Instill Energy'),
    (select id from sets where short_name = '4ed'),
    '252',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mishra''s War Machine'),
    (select id from sets where short_name = '4ed'),
    '337',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '4ed'),
    '374',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '4ed'),
    '376',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gray Ogre'),
    (select id from sets where short_name = '4ed'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fear'),
    (select id from sets where short_name = '4ed'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Land Tax'),
    (select id from sets where short_name = '4ed'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Psychic Venom'),
    (select id from sets where short_name = '4ed'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ivory Cup'),
    (select id from sets where short_name = '4ed'),
    '327',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marsh Viper'),
    (select id from sets where short_name = '4ed'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thoughtlace'),
    (select id from sets where short_name = '4ed'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flight'),
    (select id from sets where short_name = '4ed'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Swords'),
    (select id from sets where short_name = '4ed'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Artillery'),
    (select id from sets where short_name = '4ed'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Green'),
    (select id from sets where short_name = '4ed'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Vise'),
    (select id from sets where short_name = '4ed'),
    '299',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Titania''s Song'),
    (select id from sets where short_name = '4ed'),
    '276',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = '4ed'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nether Shadow'),
    (select id from sets where short_name = '4ed'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Control Magic'),
    (select id from sets where short_name = '4ed'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oasis'),
    (select id from sets where short_name = '4ed'),
    '362',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spirit Shackle'),
    (select id from sets where short_name = '4ed'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Rack'),
    (select id from sets where short_name = '4ed'),
    '352',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = '4ed'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'War Mammoth'),
    (select id from sets where short_name = '4ed'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Ward'),
    (select id from sets where short_name = '4ed'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunken City'),
    (select id from sets where short_name = '4ed'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tsunami'),
    (select id from sets where short_name = '4ed'),
    '278',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Segovian Leviathan'),
    (select id from sets where short_name = '4ed'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pradesh Gypsies'),
    (select id from sets where short_name = '4ed'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathlace'),
    (select id from sets where short_name = '4ed'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Air'),
    (select id from sets where short_name = '4ed'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diabolic Machine'),
    (select id from sets where short_name = '4ed'),
    '314',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cursed Land'),
    (select id from sets where short_name = '4ed'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Rock Sled'),
    (select id from sets where short_name = '4ed'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Giant'),
    (select id from sets where short_name = '4ed'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Detonate'),
    (select id from sets where short_name = '4ed'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rebirth'),
    (select id from sets where short_name = '4ed'),
    '267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Amrou Kithkin'),
    (select id from sets where short_name = '4ed'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scryb Sprites'),
    (select id from sets where short_name = '4ed'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cursed Rack'),
    (select id from sets where short_name = '4ed'),
    '312',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Uthden Troll'),
    (select id from sets where short_name = '4ed'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashes to Ashes'),
    (select id from sets where short_name = '4ed'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firebreathing'),
    (select id from sets where short_name = '4ed'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karma'),
    (select id from sets where short_name = '4ed'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Bomb'),
    (select id from sets where short_name = '4ed'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ornithopter'),
    (select id from sets where short_name = '4ed'),
    '341',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Artifact'),
    (select id from sets where short_name = '4ed'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'White Ward'),
    (select id from sets where short_name = '4ed'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = '4ed'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'White Knight'),
    (select id from sets where short_name = '4ed'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armageddon Clock'),
    (select id from sets where short_name = '4ed'),
    '295',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flying Carpet'),
    (select id from sets where short_name = '4ed'),
    '320',
    'rare'
) ,
(
    (select id from mtgcard where name = 'El-Hajjâj'),
    (select id from sets where short_name = '4ed'),
    '134†',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = '4ed'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '4ed'),
    '369',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cockatrice'),
    (select id from sets where short_name = '4ed'),
    '237',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = '4ed'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Whelp'),
    (select id from sets where short_name = '4ed'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dwarven Warriors'),
    (select id from sets where short_name = '4ed'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Bone'),
    (select id from sets where short_name = '4ed'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Lands'),
    (select id from sets where short_name = '4ed'),
    '260',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grizzly Bears'),
    (select id from sets where short_name = '4ed'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Junún Efreet'),
    (select id from sets where short_name = '4ed'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Wood'),
    (select id from sets where short_name = '4ed'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Holy Armor'),
    (select id from sets where short_name = '4ed'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghost Ship'),
    (select id from sets where short_name = '4ed'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kormus Bell'),
    (select id from sets where short_name = '4ed'),
    '332',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Vault'),
    (select id from sets where short_name = '4ed'),
    '334',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winter Blast'),
    (select id from sets where short_name = '4ed'),
    '290',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inferno'),
    (select id from sets where short_name = '4ed'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Amulet of Kroog'),
    (select id from sets where short_name = '4ed'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Righteousness'),
    (select id from sets where short_name = '4ed'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jayemdae Tome'),
    (select id from sets where short_name = '4ed'),
    '331',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clockwork Beast'),
    (select id from sets where short_name = '4ed'),
    '307',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magical Hack'),
    (select id from sets where short_name = '4ed'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Lust'),
    (select id from sets where short_name = '4ed'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aladdin''s Lamp'),
    (select id from sets where short_name = '4ed'),
    '291',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ankh of Mishra'),
    (select id from sets where short_name = '4ed'),
    '294',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = '4ed'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Erg Raiders'),
    (select id from sets where short_name = '4ed'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tawnos''s Weaponry'),
    (select id from sets where short_name = '4ed'),
    '349',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chaoslace'),
    (select id from sets where short_name = '4ed'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Black Knight'),
    (select id from sets where short_name = '4ed'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tetravus'),
    (select id from sets where short_name = '4ed'),
    '350',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clockwork Avian'),
    (select id from sets where short_name = '4ed'),
    '306',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wild Growth'),
    (select id from sets where short_name = '4ed'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Brute'),
    (select id from sets where short_name = '4ed'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Blue'),
    (select id from sets where short_name = '4ed'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glasses of Urza'),
    (select id from sets where short_name = '4ed'),
    '321',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Liege'),
    (select id from sets where short_name = '4ed'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '4ed'),
    '375',
    'common'
) ,
(
    (select id from mtgcard where name = 'Radjan Spirit'),
    (select id from sets where short_name = '4ed'),
    '266',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Siren''s Call'),
    (select id from sets where short_name = '4ed'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alabaster Potion'),
    (select id from sets where short_name = '4ed'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Red Ward'),
    (select id from sets where short_name = '4ed'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weakness'),
    (select id from sets where short_name = '4ed'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desert Twister'),
    (select id from sets where short_name = '4ed'),
    '240',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Regeneration'),
    (select id from sets where short_name = '4ed'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Oriflamme'),
    (select id from sets where short_name = '4ed'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Purelace'),
    (select id from sets where short_name = '4ed'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Craw Wurm'),
    (select id from sets where short_name = '4ed'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Eruption'),
    (select id from sets where short_name = '4ed'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Will-o''-the-Wisp'),
    (select id from sets where short_name = '4ed'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spirit Link'),
    (select id from sets where short_name = '4ed'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aspect of Wolf'),
    (select id from sets where short_name = '4ed'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blight'),
    (select id from sets where short_name = '4ed'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hurloon Minotaur'),
    (select id from sets where short_name = '4ed'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evil Presence'),
    (select id from sets where short_name = '4ed'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Howl from Beyond'),
    (select id from sets where short_name = '4ed'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conversion'),
    (select id from sets where short_name = '4ed'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyrotechnics'),
    (select id from sets where short_name = '4ed'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disintegrate'),
    (select id from sets where short_name = '4ed'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coral Helm'),
    (select id from sets where short_name = '4ed'),
    '310',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mesa Pegasus'),
    (select id from sets where short_name = '4ed'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lifetap'),
    (select id from sets where short_name = '4ed'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '4ed'),
    '364',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psionic Entity'),
    (select id from sets where short_name = '4ed'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brothers of Fire'),
    (select id from sets where short_name = '4ed'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lost Soul'),
    (select id from sets where short_name = '4ed'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Helm of Chatzuk'),
    (select id from sets where short_name = '4ed'),
    '324',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunglasses of Urza'),
    (select id from sets where short_name = '4ed'),
    '347',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wooden Sphere'),
    (select id from sets where short_name = '4ed'),
    '359',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hill Giant'),
    (select id from sets where short_name = '4ed'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pearled Unicorn'),
    (select id from sets where short_name = '4ed'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ball Lightning'),
    (select id from sets where short_name = '4ed'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Artifacts'),
    (select id from sets where short_name = '4ed'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '4ed'),
    '368',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carnivorous Plant'),
    (select id from sets where short_name = '4ed'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fellwar Stone'),
    (select id from sets where short_name = '4ed'),
    '319',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seeker'),
    (select id from sets where short_name = '4ed'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = '4ed'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Word of Binding'),
    (select id from sets where short_name = '4ed'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Creature Bond'),
    (select id from sets where short_name = '4ed'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mahamoti Djinn'),
    (select id from sets where short_name = '4ed'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Land Leeches'),
    (select id from sets where short_name = '4ed'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathgrip'),
    (select id from sets where short_name = '4ed'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = '4ed'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fissure'),
    (select id from sets where short_name = '4ed'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greed'),
    (select id from sets where short_name = '4ed'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winds of Change'),
    (select id from sets where short_name = '4ed'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Murk Dwellers'),
    (select id from sets where short_name = '4ed'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Holy Strength'),
    (select id from sets where short_name = '4ed'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Water'),
    (select id from sets where short_name = '4ed'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brass Man'),
    (select id from sets where short_name = '4ed'),
    '302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lure'),
    (select id from sets where short_name = '4ed'),
    '262',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '4ed'),
    '372',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jump'),
    (select id from sets where short_name = '4ed'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simulacrum'),
    (select id from sets where short_name = '4ed'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fungusaur'),
    (select id from sets where short_name = '4ed'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = '4ed'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Engine'),
    (select id from sets where short_name = '4ed'),
    '317',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cyclopean Mummy'),
    (select id from sets where short_name = '4ed'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obsianus Golem'),
    (select id from sets where short_name = '4ed'),
    '339',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unstable Mutation'),
    (select id from sets where short_name = '4ed'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hurricane'),
    (select id from sets where short_name = '4ed'),
    '251',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '4ed'),
    '370',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lord of Atlantis'),
    (select id from sets where short_name = '4ed'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island Fish Jasconius'),
    (select id from sets where short_name = '4ed'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Balloon Brigade'),
    (select id from sets where short_name = '4ed'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crumble'),
    (select id from sets where short_name = '4ed'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Castle'),
    (select id from sets where short_name = '4ed'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rod of Ruin'),
    (select id from sets where short_name = '4ed'),
    '344',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = '4ed'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Stone'),
    (select id from sets where short_name = '4ed'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Net'),
    (select id from sets where short_name = '4ed'),
    '346',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shanodin Dryads'),
    (select id from sets where short_name = '4ed'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erosion'),
    (select id from sets where short_name = '4ed'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fortified Area'),
    (select id from sets where short_name = '4ed'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plague Rats'),
    (select id from sets where short_name = '4ed'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elven Riders'),
    (select id from sets where short_name = '4ed'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pestilence'),
    (select id from sets where short_name = '4ed'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brainwash'),
    (select id from sets where short_name = '4ed'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ali Baba'),
    (select id from sets where short_name = '4ed'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ley Druid'),
    (select id from sets where short_name = '4ed'),
    '256',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin King'),
    (select id from sets where short_name = '4ed'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: White'),
    (select id from sets where short_name = '4ed'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk of the Pearl Trident'),
    (select id from sets where short_name = '4ed'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burrowing'),
    (select id from sets where short_name = '4ed'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tunnel'),
    (select id from sets where short_name = '4ed'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = '4ed'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frozen Shade'),
    (select id from sets where short_name = '4ed'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time Elemental'),
    (select id from sets where short_name = '4ed'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lord of the Pit'),
    (select id from sets where short_name = '4ed'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verduran Enchantress'),
    (select id from sets where short_name = '4ed'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Avenger'),
    (select id from sets where short_name = '4ed'),
    '355',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blue Elemental Blast'),
    (select id from sets where short_name = '4ed'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Throne of Bone'),
    (select id from sets where short_name = '4ed'),
    '353',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magnetic Mountain'),
    (select id from sets where short_name = '4ed'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Black'),
    (select id from sets where short_name = '4ed'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cave People'),
    (select id from sets where short_name = '4ed'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Power Leak'),
    (select id from sets where short_name = '4ed'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sindbad'),
    (select id from sets where short_name = '4ed'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eternal Warrior'),
    (select id from sets where short_name = '4ed'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = '4ed'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mons''s Goblin Raiders'),
    (select id from sets where short_name = '4ed'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Energy Tap'),
    (select id from sets where short_name = '4ed'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Clash'),
    (select id from sets where short_name = '4ed'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fog'),
    (select id from sets where short_name = '4ed'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angry Mob'),
    (select id from sets where short_name = '4ed'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savannah Lions'),
    (select id from sets where short_name = '4ed'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abomination'),
    (select id from sets where short_name = '4ed'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sisters of the Flame'),
    (select id from sets where short_name = '4ed'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Xenic Poltergeist'),
    (select id from sets where short_name = '4ed'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Howling Mine'),
    (select id from sets where short_name = '4ed'),
    '325',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Red Elemental Blast'),
    (select id from sets where short_name = '4ed'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strip Mine'),
    (select id from sets where short_name = '4ed'),
    '363',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Earth Elemental'),
    (select id from sets where short_name = '4ed'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Mana Battery'),
    (select id from sets where short_name = '4ed'),
    '298',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = '4ed'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yotian Soldier'),
    (select id from sets where short_name = '4ed'),
    '360',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jandor''s Saddlebags'),
    (select id from sets where short_name = '4ed'),
    '330',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bad Moon'),
    (select id from sets where short_name = '4ed'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eye for an Eye'),
    (select id from sets where short_name = '4ed'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '4ed'),
    '366',
    'common'
) ,
(
    (select id from mtgcard where name = 'Backfire'),
    (select id from sets where short_name = '4ed'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Spears'),
    (select id from sets where short_name = '4ed'),
    '356',
    'common'
) ,
(
    (select id from mtgcard where name = 'Healing Salve'),
    (select id from sets where short_name = '4ed'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = '4ed'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = '4ed'),
    '361',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '4ed'),
    '367',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whirling Dervish'),
    (select id from sets where short_name = '4ed'),
    '288',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Visions'),
    (select id from sets where short_name = '4ed'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elder Land Wurm'),
    (select id from sets where short_name = '4ed'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keldon Warlord'),
    (select id from sets where short_name = '4ed'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Library'),
    (select id from sets where short_name = '4ed'),
    '273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tranquility'),
    (select id from sets where short_name = '4ed'),
    '277',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Dust'),
    (select id from sets where short_name = '4ed'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Royal Assassin'),
    (select id from sets where short_name = '4ed'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Samite Healer'),
    (select id from sets where short_name = '4ed'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Morale'),
    (select id from sets where short_name = '4ed'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Specter'),
    (select id from sets where short_name = '4ed'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'White Mana Battery'),
    (select id from sets where short_name = '4ed'),
    '357',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ebony Horse'),
    (select id from sets where short_name = '4ed'),
    '318',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animate Wall'),
    (select id from sets where short_name = '4ed'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Green Ward'),
    (select id from sets where short_name = '4ed'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '4ed'),
    '378',
    'common'
) ,
(
    (select id from mtgcard where name = 'Library of Leng'),
    (select id from sets where short_name = '4ed'),
    '333',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Iron Star'),
    (select id from sets where short_name = '4ed'),
    '326',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = '4ed'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = '4ed'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dingus Egg'),
    (select id from sets where short_name = '4ed'),
    '315',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drain Life'),
    (select id from sets where short_name = '4ed'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flood'),
    (select id from sets where short_name = '4ed'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Twist'),
    (select id from sets where short_name = '4ed'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = '4ed'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Brambles'),
    (select id from sets where short_name = '4ed'),
    '282',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hurkyl''s Recall'),
    (select id from sets where short_name = '4ed'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lifelace'),
    (select id from sets where short_name = '4ed'),
    '258',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crystal Rod'),
    (select id from sets where short_name = '4ed'),
    '311',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dancing Scimitar'),
    (select id from sets where short_name = '4ed'),
    '313',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island Sanctuary'),
    (select id from sets where short_name = '4ed'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hurr Jackal'),
    (select id from sets where short_name = '4ed'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smoke'),
    (select id from sets where short_name = '4ed'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tempest Efreet'),
    (select id from sets where short_name = '4ed'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bog Imp'),
    (select id from sets where short_name = '4ed'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thicket Basilisk'),
    (select id from sets where short_name = '4ed'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Onulet'),
    (select id from sets where short_name = '4ed'),
    '340',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ironroot Treefolk'),
    (select id from sets where short_name = '4ed'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = '4ed'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Venom'),
    (select id from sets where short_name = '4ed'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marsh Gas'),
    (select id from sets where short_name = '4ed'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meekstone'),
    (select id from sets where short_name = '4ed'),
    '335',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Apprentice Wizard'),
    (select id from sets where short_name = '4ed'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stream of Life'),
    (select id from sets where short_name = '4ed'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sandstorm'),
    (select id from sets where short_name = '4ed'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crimson Manticore'),
    (select id from sets where short_name = '4ed'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bronze Tablet'),
    (select id from sets where short_name = '4ed'),
    '303',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uncle Istvan'),
    (select id from sets where short_name = '4ed'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Animate Dead'),
    (select id from sets where short_name = '4ed'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Primal Clay'),
    (select id from sets where short_name = '4ed'),
    '342',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Fire'),
    (select id from sets where short_name = '4ed'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Force of Nature'),
    (select id from sets where short_name = '4ed'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sleight of Mind'),
    (select id from sets where short_name = '4ed'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blue Mana Battery'),
    (select id from sets where short_name = '4ed'),
    '300',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reverse Damage'),
    (select id from sets where short_name = '4ed'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'El-Hajjâj'),
    (select id from sets where short_name = '4ed'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaseous Form'),
    (select id from sets where short_name = '4ed'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Green Mana Battery'),
    (select id from sets where short_name = '4ed'),
    '323',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Paralyze'),
    (select id from sets where short_name = '4ed'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grapeshot Catapult'),
    (select id from sets where short_name = '4ed'),
    '322',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '4ed'),
    '365',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bottle of Suleiman'),
    (select id from sets where short_name = '4ed'),
    '301',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampire Bats'),
    (select id from sets where short_name = '4ed'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drudge Skeletons'),
    (select id from sets where short_name = '4ed'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = '4ed'),
    '338',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Piety'),
    (select id from sets where short_name = '4ed'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tawnos''s Wand'),
    (select id from sets where short_name = '4ed'),
    '348',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winter Orb'),
    (select id from sets where short_name = '4ed'),
    '358',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Ice'),
    (select id from sets where short_name = '4ed'),
    '283',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zombie Master'),
    (select id from sets where short_name = '4ed'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ironclaw Orcs'),
    (select id from sets where short_name = '4ed'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Timber Wolves'),
    (select id from sets where short_name = '4ed'),
    '275',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aladdin''s Ring'),
    (select id from sets where short_name = '4ed'),
    '292',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Osai Vultures'),
    (select id from sets where short_name = '4ed'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantom Monster'),
    (select id from sets where short_name = '4ed'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Water Elemental'),
    (select id from sets where short_name = '4ed'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = '4ed'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balance'),
    (select id from sets where short_name = '4ed'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twiddle'),
    (select id from sets where short_name = '4ed'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scathe Zombies'),
    (select id from sets where short_name = '4ed'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prodigal Sorcerer'),
    (select id from sets where short_name = '4ed'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steal Artifact'),
    (select id from sets where short_name = '4ed'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Short'),
    (select id from sets where short_name = '4ed'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '4ed'),
    '371',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '4ed'),
    '377',
    'common'
) ,
(
    (select id from mtgcard where name = 'Untamed Wilds'),
    (select id from sets where short_name = '4ed'),
    '279',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blue Ward'),
    (select id from sets where short_name = '4ed'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = '4ed'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scavenging Ghoul'),
    (select id from sets where short_name = '4ed'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lifeforce'),
    (select id from sets where short_name = '4ed'),
    '257',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drain Power'),
    (select id from sets where short_name = '4ed'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Channel'),
    (select id from sets where short_name = '4ed'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sea Serpent'),
    (select id from sets where short_name = '4ed'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sorceress Queen'),
    (select id from sets where short_name = '4ed'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Red Mana Battery'),
    (select id from sets where short_name = '4ed'),
    '343',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Killer Bees'),
    (select id from sets where short_name = '4ed'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = '4ed'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Flare'),
    (select id from sets where short_name = '4ed'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestial Prism'),
    (select id from sets where short_name = '4ed'),
    '304',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = '4ed'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Durkwood Boars'),
    (select id from sets where short_name = '4ed'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Sink'),
    (select id from sets where short_name = '4ed'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Ward'),
    (select id from sets where short_name = '4ed'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Animate Artifact'),
    (select id from sets where short_name = '4ed'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feedback'),
    (select id from sets where short_name = '4ed'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battering Ram'),
    (select id from sets where short_name = '4ed'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flashfires'),
    (select id from sets where short_name = '4ed'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warp Artifact'),
    (select id from sets where short_name = '4ed'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blessing'),
    (select id from sets where short_name = '4ed'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Archers'),
    (select id from sets where short_name = '4ed'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ivory Tower'),
    (select id from sets where short_name = '4ed'),
    '328',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Millstone'),
    (select id from sets where short_name = '4ed'),
    '336',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Strength'),
    (select id from sets where short_name = '4ed'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kismet'),
    (select id from sets where short_name = '4ed'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = '4ed'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stasis'),
    (select id from sets where short_name = '4ed'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wanderlust'),
    (select id from sets where short_name = '4ed'),
    '285',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Tortoise'),
    (select id from sets where short_name = '4ed'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unholy Strength'),
    (select id from sets where short_name = '4ed'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divine Transformation'),
    (select id from sets where short_name = '4ed'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gloom'),
    (select id from sets where short_name = '4ed'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Carrion Ants'),
    (select id from sets where short_name = '4ed'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Conservator'),
    (select id from sets where short_name = '4ed'),
    '309',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Hive'),
    (select id from sets where short_name = '4ed'),
    '351',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Immolation'),
    (select id from sets where short_name = '4ed'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crusade'),
    (select id from sets where short_name = '4ed'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disrupting Scepter'),
    (select id from sets where short_name = '4ed'),
    '316',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leviathan'),
    (select id from sets where short_name = '4ed'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bog Wraith'),
    (select id from sets where short_name = '4ed'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Energy Flux'),
    (select id from sets where short_name = '4ed'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bird Maiden'),
    (select id from sets where short_name = '4ed'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Benalish Hero'),
    (select id from sets where short_name = '4ed'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manabarbs'),
    (select id from sets where short_name = '4ed'),
    '212',
    'rare'
) 
 on conflict do nothing;
