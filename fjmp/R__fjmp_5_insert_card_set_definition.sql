insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Basri'),
    (select id from sets where short_name = 'fjmp'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Reanimated'),
    (select id from sets where short_name = 'fjmp'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minions'),
    (select id from sets where short_name = 'fjmp'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk'),
    (select id from sets where short_name = 'fjmp'),
    '16',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Feathered Friends'),
    (select id from sets where short_name = 'fjmp'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wizards'),
    (select id from sets where short_name = 'fjmp'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragons'),
    (select id from sets where short_name = 'fjmp'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enchanted'),
    (select id from sets where short_name = 'fjmp'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Above the Clouds'),
    (select id from sets where short_name = 'fjmp'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seismic'),
    (select id from sets where short_name = 'fjmp'),
    '33',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goblins'),
    (select id from sets where short_name = 'fjmp'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minotaurs'),
    (select id from sets where short_name = 'fjmp'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spirits'),
    (select id from sets where short_name = 'fjmp'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smashing'),
    (select id from sets where short_name = 'fjmp'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tree-Hugging'),
    (select id from sets where short_name = 'fjmp'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Well-Read'),
    (select id from sets where short_name = 'fjmp'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dinosaurs'),
    (select id from sets where short_name = 'fjmp'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unicorns'),
    (select id from sets where short_name = 'fjmp'),
    '41',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Angels'),
    (select id from sets where short_name = 'fjmp'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archaeology'),
    (select id from sets where short_name = 'fjmp'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rainbow'),
    (select id from sets where short_name = 'fjmp'),
    '30',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pirates'),
    (select id from sets where short_name = 'fjmp'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Predatory'),
    (select id from sets where short_name = 'fjmp'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning'),
    (select id from sets where short_name = 'fjmp'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dogs'),
    (select id from sets where short_name = 'fjmp'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spooky'),
    (select id from sets where short_name = 'fjmp'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra'),
    (select id from sets where short_name = 'fjmp'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Doctor'),
    (select id from sets where short_name = 'fjmp'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elves'),
    (select id from sets where short_name = 'fjmp'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Devilish'),
    (select id from sets where short_name = 'fjmp'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heavily Armored'),
    (select id from sets where short_name = 'fjmp'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Walls'),
    (select id from sets where short_name = 'fjmp'),
    '43',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Teferi'),
    (select id from sets where short_name = 'fjmp'),
    '38',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Milling'),
    (select id from sets where short_name = 'fjmp'),
    '23',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Legion'),
    (select id from sets where short_name = 'fjmp'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Discarding'),
    (select id from sets where short_name = 'fjmp'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rogues'),
    (select id from sets where short_name = 'fjmp'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spellcasting'),
    (select id from sets where short_name = 'fjmp'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian'),
    (select id from sets where short_name = 'fjmp'),
    '26',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lands'),
    (select id from sets where short_name = 'fjmp'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampires'),
    (select id from sets where short_name = 'fjmp'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Under the Sea'),
    (select id from sets where short_name = 'fjmp'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plus One'),
    (select id from sets where short_name = 'fjmp'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cats'),
    (select id from sets where short_name = 'fjmp'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana'),
    (select id from sets where short_name = 'fjmp'),
    '22',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Witchcraft'),
    (select id from sets where short_name = 'fjmp'),
    '45',
    'rare'
) 
 on conflict do nothing;
