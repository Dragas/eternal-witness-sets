insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Basri'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Reanimated'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Minions'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Garruk'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Feathered Friends'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wizards'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dragons'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Enchanted'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Above the Clouds'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Seismic'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goblins'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Minotaurs'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spirits'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Smashing'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tree-Hugging'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Well-Read'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dinosaurs'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Unicorns'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Angels'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Archaeology'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rainbow'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pirates'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Predatory'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lightning'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dogs'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spooky'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chandra'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Doctor'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elves'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Devilish'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Heavily Armored'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Walls'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Teferi'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Milling'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Legion'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Discarding'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rogues'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spellcasting'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Phyrexian'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lands'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vampires'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Under the Sea'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plus One'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cats'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Liliana'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Witchcraft'),
        (select types.id from types where name = 'Card')
    ) 
 on conflict do nothing;
