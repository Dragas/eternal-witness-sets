    insert into mtgcard(name) values ('Will Kenrith Emblem') on conflict do nothing;
    insert into mtgcard(name) values ('Rowan Kenrith Emblem') on conflict do nothing;
    insert into mtgcard(name) values ('Spirit') on conflict do nothing;
    insert into mtgcard(name) values ('Zombie Giant') on conflict do nothing;
    insert into mtgcard(name) values ('Warrior') on conflict do nothing;
    insert into mtgcard(name) values ('Beast') on conflict do nothing;
    insert into mtgcard(name) values ('Zombie') on conflict do nothing;
    insert into mtgcard(name) values ('Myr') on conflict do nothing;
