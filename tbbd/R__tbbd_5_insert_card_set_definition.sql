insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Will Kenrith Emblem'),
    (select id from sets where short_name = 'tbbd'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rowan Kenrith Emblem'),
    (select id from sets where short_name = 'tbbd'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tbbd'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Giant'),
    (select id from sets where short_name = 'tbbd'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warrior'),
    (select id from sets where short_name = 'tbbd'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tbbd'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tbbd'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr'),
    (select id from sets where short_name = 'tbbd'),
    '6',
    'common'
) 
 on conflict do nothing;
