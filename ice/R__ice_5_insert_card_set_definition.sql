insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Thermokarst'),
    (select id from sets where short_name = 'ice'),
    '268',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Healer'),
    (select id from sets where short_name = 'ice'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gorilla Pack'),
    (select id from sets where short_name = 'ice'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fyndhorn Brownie'),
    (select id from sets where short_name = 'ice'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nacre Talisman'),
    (select id from sets where short_name = 'ice'),
    '329',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glacial Wall'),
    (select id from sets where short_name = 'ice'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Game of Chaos'),
    (select id from sets where short_name = 'ice'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snow Hound'),
    (select id from sets where short_name = 'ice'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gangrenous Zombies'),
    (select id from sets where short_name = 'ice'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time Bomb'),
    (select id from sets where short_name = 'ice'),
    '342',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Adarkar Wastes'),
    (select id from sets where short_name = 'ice'),
    '351',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bone Shaman'),
    (select id from sets where short_name = 'ice'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Adarkar Unicorn'),
    (select id from sets where short_name = 'ice'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Folk of the Pines'),
    (select id from sets where short_name = 'ice'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vexing Arcanix'),
    (select id from sets where short_name = 'ice'),
    '344',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lost Order of Jarkeld'),
    (select id from sets where short_name = 'ice'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Green Scarab'),
    (select id from sets where short_name = 'ice'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deflection'),
    (select id from sets where short_name = 'ice'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Essence Flare'),
    (select id from sets where short_name = 'ice'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icequake'),
    (select id from sets where short_name = 'ice'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blinking Spirit'),
    (select id from sets where short_name = 'ice'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zuran Spellcaster'),
    (select id from sets where short_name = 'ice'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brushland'),
    (select id from sets where short_name = 'ice'),
    '352',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ice'),
    '378',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illusions of Grandeur'),
    (select id from sets where short_name = 'ice'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ice Cauldron'),
    (select id from sets where short_name = 'ice'),
    '321',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reality Twist'),
    (select id from sets where short_name = 'ice'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snow Fortress'),
    (select id from sets where short_name = 'ice'),
    '337',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hot Springs'),
    (select id from sets where short_name = 'ice'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrath of Marit Lage'),
    (select id from sets where short_name = 'ice'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Regeneration'),
    (select id from sets where short_name = 'ice'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burnt Offering'),
    (select id from sets where short_name = 'ice'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Caribou Range'),
    (select id from sets where short_name = 'ice'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diabolic Vision'),
    (select id from sets where short_name = 'ice'),
    '284',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Icy Manipulator'),
    (select id from sets where short_name = 'ice'),
    '322',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soldevi Machinist'),
    (select id from sets where short_name = 'ice'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elder Druid'),
    (select id from sets where short_name = 'ice'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cooperation'),
    (select id from sets where short_name = 'ice'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Lumberjack'),
    (select id from sets where short_name = 'ice'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infuse'),
    (select id from sets where short_name = 'ice'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Force Void'),
    (select id from sets where short_name = 'ice'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Word of Undoing'),
    (select id from sets where short_name = 'ice'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arnjlot''s Ascent'),
    (select id from sets where short_name = 'ice'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Monsoon'),
    (select id from sets where short_name = 'ice'),
    '298',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chromatic Armor'),
    (select id from sets where short_name = 'ice'),
    '283',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skeleton Ship'),
    (select id from sets where short_name = 'ice'),
    '301',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glacial Crevasses'),
    (select id from sets where short_name = 'ice'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ice'),
    '366',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anarchy'),
    (select id from sets where short_name = 'ice'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tinder Wall'),
    (select id from sets where short_name = 'ice'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fanatical Fever'),
    (select id from sets where short_name = 'ice'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight of Stromgald'),
    (select id from sets where short_name = 'ice'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Celestial Sword'),
    (select id from sets where short_name = 'ice'),
    '314',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snow Devil'),
    (select id from sets where short_name = 'ice'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ice'),
    '377',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Frostbeast'),
    (select id from sets where short_name = 'ice'),
    '296',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sulfurous Springs'),
    (select id from sets where short_name = 'ice'),
    '360',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vertigo'),
    (select id from sets where short_name = 'ice'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shield Bearer'),
    (select id from sets where short_name = 'ice'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earthlore'),
    (select id from sets where short_name = 'ice'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jeweled Amulet'),
    (select id from sets where short_name = 'ice'),
    '326',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Norritt'),
    (select id from sets where short_name = 'ice'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'ice'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Energy Storm'),
    (select id from sets where short_name = 'ice'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Icy Prison'),
    (select id from sets where short_name = 'ice'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zuran Orb'),
    (select id from sets where short_name = 'ice'),
    '350',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fyndhorn Elves'),
    (select id from sets where short_name = 'ice'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jokulhaups'),
    (select id from sets where short_name = 'ice'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merieke Ri Berit'),
    (select id from sets where short_name = 'ice'),
    '297',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunstone'),
    (select id from sets where short_name = 'ice'),
    '341',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fylgja'),
    (select id from sets where short_name = 'ice'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lim-Dûl''s Cohort'),
    (select id from sets where short_name = 'ice'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glacial Chasm'),
    (select id from sets where short_name = 'ice'),
    '353',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Freyalise''s Charm'),
    (select id from sets where short_name = 'ice'),
    '240',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fyndhorn Pollen'),
    (select id from sets where short_name = 'ice'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Whip'),
    (select id from sets where short_name = 'ice'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Kiss'),
    (select id from sets where short_name = 'ice'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'ice'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clairvoyance'),
    (select id from sets where short_name = 'ice'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ice'),
    '373',
    'common'
) ,
(
    (select id from mtgcard where name = 'Word of Blasting'),
    (select id from sets where short_name = 'ice'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sibilant Spirit'),
    (select id from sets where short_name = 'ice'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gravebind'),
    (select id from sets where short_name = 'ice'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jester''s Cap'),
    (select id from sets where short_name = 'ice'),
    '324',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaze of Pain'),
    (select id from sets where short_name = 'ice'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Green'),
    (select id from sets where short_name = 'ice'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: White'),
    (select id from sets where short_name = 'ice'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Touch of Vitae'),
    (select id from sets where short_name = 'ice'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vibrating Sphere'),
    (select id from sets where short_name = 'ice'),
    '345',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mole Worms'),
    (select id from sets where short_name = 'ice'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elkin Bottle'),
    (select id from sets where short_name = 'ice'),
    '317',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scaled Wurm'),
    (select id from sets where short_name = 'ice'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hallowed Ground'),
    (select id from sets where short_name = 'ice'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whalebone Glider'),
    (select id from sets where short_name = 'ice'),
    '349',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Forest'),
    (select id from sets where short_name = 'ice'),
    '383',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stench of Evil'),
    (select id from sets where short_name = 'ice'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stunted Growth'),
    (select id from sets where short_name = 'ice'),
    '266',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wind Spirit'),
    (select id from sets where short_name = 'ice'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leshrac''s Rite'),
    (select id from sets where short_name = 'ice'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'ice'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ice'),
    '376',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistfolk'),
    (select id from sets where short_name = 'ice'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunder Wall'),
    (select id from sets where short_name = 'ice'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meteor Shower'),
    (select id from sets where short_name = 'ice'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maddening Wind'),
    (select id from sets where short_name = 'ice'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Despotic Scepter'),
    (select id from sets where short_name = 'ice'),
    '316',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necropotence'),
    (select id from sets where short_name = 'ice'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moor Fiend'),
    (select id from sets where short_name = 'ice'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fear'),
    (select id from sets where short_name = 'ice'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = 'ice'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Bauble'),
    (select id from sets where short_name = 'ice'),
    '343',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mystic Remora'),
    (select id from sets where short_name = 'ice'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hipparion'),
    (select id from sets where short_name = 'ice'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = 'ice'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ray of Erasure'),
    (select id from sets where short_name = 'ice'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Freyalise Supplicant'),
    (select id from sets where short_name = 'ice'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balduvian Conjurer'),
    (select id from sets where short_name = 'ice'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Snowman'),
    (select id from sets where short_name = 'ice'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Scarab'),
    (select id from sets where short_name = 'ice'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ritual of Subdual'),
    (select id from sets where short_name = 'ice'),
    '261',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Staff of the Ages'),
    (select id from sets where short_name = 'ice'),
    '340',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aegis of the Meek'),
    (select id from sets where short_name = 'ice'),
    '307',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Pine Needles'),
    (select id from sets where short_name = 'ice'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lava Tubes'),
    (select id from sets where short_name = 'ice'),
    '358',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snowblind'),
    (select id from sets where short_name = 'ice'),
    '264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forbidden Lore'),
    (select id from sets where short_name = 'ice'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Total War'),
    (select id from sets where short_name = 'ice'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Minion of Leshrac'),
    (select id from sets where short_name = 'ice'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Errant Minion'),
    (select id from sets where short_name = 'ice'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ice'),
    '369',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Ravel'),
    (select id from sets where short_name = 'ice'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hymn of Rebirth'),
    (select id from sets where short_name = 'ice'),
    '295',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elemental Augury'),
    (select id from sets where short_name = 'ice'),
    '286',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghostly Flame'),
    (select id from sets where short_name = 'ice'),
    '292',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Shields'),
    (select id from sets where short_name = 'ice'),
    '347',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ray of Command'),
    (select id from sets where short_name = 'ice'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Malachite Talisman'),
    (select id from sets where short_name = 'ice'),
    '328',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Illusionary Wall'),
    (select id from sets where short_name = 'ice'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Johtull Wurm'),
    (select id from sets where short_name = 'ice'),
    '250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soldevi Golem'),
    (select id from sets where short_name = 'ice'),
    '338',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Swamp'),
    (select id from sets where short_name = 'ice'),
    '372',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mudslide'),
    (select id from sets where short_name = 'ice'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dread Wight'),
    (select id from sets where short_name = 'ice'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fyndhorn Bow'),
    (select id from sets where short_name = 'ice'),
    '318',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Royal Guard'),
    (select id from sets where short_name = 'ice'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nature''s Lore'),
    (select id from sets where short_name = 'ice'),
    '255',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silver Erne'),
    (select id from sets where short_name = 'ice'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flow of Maggots'),
    (select id from sets where short_name = 'ice'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sabretooth Tiger'),
    (select id from sets where short_name = 'ice'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illusionary Terrain'),
    (select id from sets where short_name = 'ice'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Curse of Marit Lage'),
    (select id from sets where short_name = 'ice'),
    '181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fire Covenant'),
    (select id from sets where short_name = 'ice'),
    '289',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'General Jarkeld'),
    (select id from sets where short_name = 'ice'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ice'),
    '374',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stromgald Cabal'),
    (select id from sets where short_name = 'ice'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Binding Grasp'),
    (select id from sets where short_name = 'ice'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blessed Wine'),
    (select id from sets where short_name = 'ice'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashen Ghoul'),
    (select id from sets where short_name = 'ice'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Songs of the Damned'),
    (select id from sets where short_name = 'ice'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Mutant'),
    (select id from sets where short_name = 'ice'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prismatic Ward'),
    (select id from sets where short_name = 'ice'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyknite'),
    (select id from sets where short_name = 'ice'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krovikan Vampire'),
    (select id from sets where short_name = 'ice'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zuran Enchanter'),
    (select id from sets where short_name = 'ice'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hoar Shade'),
    (select id from sets where short_name = 'ice'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcum''s Whistle'),
    (select id from sets where short_name = 'ice'),
    '311',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shield of the Ages'),
    (select id from sets where short_name = 'ice'),
    '335',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balduvian Shaman'),
    (select id from sets where short_name = 'ice'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Mount'),
    (select id from sets where short_name = 'ice'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Legions of Lim-Dûl'),
    (select id from sets where short_name = 'ice'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Altar of Bone'),
    (select id from sets where short_name = 'ice'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Amulet of Quoz'),
    (select id from sets where short_name = 'ice'),
    '308',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ice'),
    '382',
    'common'
) ,
(
    (select id from mtgcard where name = 'Formation'),
    (select id from sets where short_name = 'ice'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Márton Stromgald'),
    (select id from sets where short_name = 'ice'),
    '199',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snowfall'),
    (select id from sets where short_name = 'ice'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Spirit'),
    (select id from sets where short_name = 'ice'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lava Burst'),
    (select id from sets where short_name = 'ice'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Storm Spirit'),
    (select id from sets where short_name = 'ice'),
    '303',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rime Dryad'),
    (select id from sets where short_name = 'ice'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mercenaries'),
    (select id from sets where short_name = 'ice'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ice'),
    '364',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Armory'),
    (select id from sets where short_name = 'ice'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karplusan Yeti'),
    (select id from sets where short_name = 'ice'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aurochs'),
    (select id from sets where short_name = 'ice'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = 'ice'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tor Giant'),
    (select id from sets where short_name = 'ice'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pale Bears'),
    (select id from sets where short_name = 'ice'),
    '256',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spectral Shield'),
    (select id from sets where short_name = 'ice'),
    '302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Healer'),
    (select id from sets where short_name = 'ice'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cold Snap'),
    (select id from sets where short_name = 'ice'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ice'),
    '370',
    'common'
) ,
(
    (select id from mtgcard where name = 'Errantry'),
    (select id from sets where short_name = 'ice'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hematite Talisman'),
    (select id from sets where short_name = 'ice'),
    '320',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kelsinko Ranger'),
    (select id from sets where short_name = 'ice'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krovikan Fetish'),
    (select id from sets where short_name = 'ice'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Sink'),
    (select id from sets where short_name = 'ice'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ice Floe'),
    (select id from sets where short_name = 'ice'),
    '355',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magus of the Unseen'),
    (select id from sets where short_name = 'ice'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lure'),
    (select id from sets where short_name = 'ice'),
    '253',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Timberline Ridge'),
    (select id from sets where short_name = 'ice'),
    '361',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyroclasm'),
    (select id from sets where short_name = 'ice'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shambling Strider'),
    (select id from sets where short_name = 'ice'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pygmy Allosaurus'),
    (select id from sets where short_name = 'ice'),
    '257',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foxfire'),
    (select id from sets where short_name = 'ice'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fiery Justice'),
    (select id from sets where short_name = 'ice'),
    '288',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Trap Door Spider'),
    (select id from sets where short_name = 'ice'),
    '293',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Foul Familiar'),
    (select id from sets where short_name = 'ice'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baton of Morale'),
    (select id from sets where short_name = 'ice'),
    '313',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'ice'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demonic Consultation'),
    (select id from sets where short_name = 'ice'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Melting'),
    (select id from sets where short_name = 'ice'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Librarian'),
    (select id from sets where short_name = 'ice'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Venomous Breath'),
    (select id from sets where short_name = 'ice'),
    '273',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hecatomb'),
    (select id from sets where short_name = 'ice'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trailblazer'),
    (select id from sets where short_name = 'ice'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Underground River'),
    (select id from sets where short_name = 'ice'),
    '362',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Order of the White Shield'),
    (select id from sets where short_name = 'ice'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dreams of the Dead'),
    (select id from sets where short_name = 'ice'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sleight of Mind'),
    (select id from sets where short_name = 'ice'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Lyre'),
    (select id from sets where short_name = 'ice'),
    '319',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grizzled Wolverine'),
    (select id from sets where short_name = 'ice'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drought'),
    (select id from sets where short_name = 'ice'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Veldt'),
    (select id from sets where short_name = 'ice'),
    '363',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arctic Foxes'),
    (select id from sets where short_name = 'ice'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shyft'),
    (select id from sets where short_name = 'ice'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Justice'),
    (select id from sets where short_name = 'ice'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Red Scarab'),
    (select id from sets where short_name = 'ice'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Barrier'),
    (select id from sets where short_name = 'ice'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Essence Filter'),
    (select id from sets where short_name = 'ice'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = 'ice'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krovikan Sorcerer'),
    (select id from sets where short_name = 'ice'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Gnats'),
    (select id from sets where short_name = 'ice'),
    '280',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcum''s Weathervane'),
    (select id from sets where short_name = 'ice'),
    '310',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heal'),
    (select id from sets where short_name = 'ice'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wiitigo'),
    (select id from sets where short_name = 'ice'),
    '276',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Musician'),
    (select id from sets where short_name = 'ice'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soldevi Simulacrum'),
    (select id from sets where short_name = 'ice'),
    '339',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Iceberg'),
    (select id from sets where short_name = 'ice'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Cannoneers'),
    (select id from sets where short_name = 'ice'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Touch of Death'),
    (select id from sets where short_name = 'ice'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dire Wolves'),
    (select id from sets where short_name = 'ice'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Elite Guard'),
    (select id from sets where short_name = 'ice'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winter''s Chill'),
    (select id from sets where short_name = 'ice'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Earthlink'),
    (select id from sets where short_name = 'ice'),
    '285',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Battle Frenzy'),
    (select id from sets where short_name = 'ice'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whiteout'),
    (select id from sets where short_name = 'ice'),
    '275',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Onyx Talisman'),
    (select id from sets where short_name = 'ice'),
    '331',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hurricane'),
    (select id from sets where short_name = 'ice'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pox'),
    (select id from sets where short_name = 'ice'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Call to Arms'),
    (select id from sets where short_name = 'ice'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Panic'),
    (select id from sets where short_name = 'ice'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illusionary Presence'),
    (select id from sets where short_name = 'ice'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chaos Moon'),
    (select id from sets where short_name = 'ice'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Illusionary Forces'),
    (select id from sets where short_name = 'ice'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seraph'),
    (select id from sets where short_name = 'ice'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Withering Wisps'),
    (select id from sets where short_name = 'ice'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leshrac''s Sigil'),
    (select id from sets where short_name = 'ice'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Island'),
    (select id from sets where short_name = 'ice'),
    '371',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flame Spirit'),
    (select id from sets where short_name = 'ice'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mesmeric Trance'),
    (select id from sets where short_name = 'ice'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conquer'),
    (select id from sets where short_name = 'ice'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'ice'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Polar Kraken'),
    (select id from sets where short_name = 'ice'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lim-Dûl''s Hex'),
    (select id from sets where short_name = 'ice'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Freyalise''s Winds'),
    (select id from sets where short_name = 'ice'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Melee'),
    (select id from sets where short_name = 'ice'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tarpan'),
    (select id from sets where short_name = 'ice'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ice'),
    '381',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spoils of War'),
    (select id from sets where short_name = 'ice'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blue Scarab'),
    (select id from sets where short_name = 'ice'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'ice'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infernal Denizen'),
    (select id from sets where short_name = 'ice'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Phalanx'),
    (select id from sets where short_name = 'ice'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chub Toad'),
    (select id from sets where short_name = 'ice'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glaciers'),
    (select id from sets where short_name = 'ice'),
    '294',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pentagram of the Ages'),
    (select id from sets where short_name = 'ice'),
    '332',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Halls of Mist'),
    (select id from sets where short_name = 'ice'),
    '354',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Lava'),
    (select id from sets where short_name = 'ice'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karplusan Forest'),
    (select id from sets where short_name = 'ice'),
    '356',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krovikan Elementalist'),
    (select id from sets where short_name = 'ice'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stampede'),
    (select id from sets where short_name = 'ice'),
    '265',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain Titan'),
    (select id from sets where short_name = 'ice'),
    '299',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Guard'),
    (select id from sets where short_name = 'ice'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enduring Renewal'),
    (select id from sets where short_name = 'ice'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pestilence Rats'),
    (select id from sets where short_name = 'ice'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormbind'),
    (select id from sets where short_name = 'ice'),
    '304',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyroblast'),
    (select id from sets where short_name = 'ice'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blizzard'),
    (select id from sets where short_name = 'ice'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Balduvian Hydra'),
    (select id from sets where short_name = 'ice'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Land Cap'),
    (select id from sets where short_name = 'ice'),
    '357',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enervate'),
    (select id from sets where short_name = 'ice'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karplusan Giant'),
    (select id from sets where short_name = 'ice'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battle Cry'),
    (select id from sets where short_name = 'ice'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'White Scarab'),
    (select id from sets where short_name = 'ice'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pit Trap'),
    (select id from sets where short_name = 'ice'),
    '333',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death Ward'),
    (select id from sets where short_name = 'ice'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runed Arch'),
    (select id from sets where short_name = 'ice'),
    '334',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reclamation'),
    (select id from sets where short_name = 'ice'),
    '300',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Howl from Beyond'),
    (select id from sets where short_name = 'ice'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naked Singularity'),
    (select id from sets where short_name = 'ice'),
    '330',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Juniper Order Druid'),
    (select id from sets where short_name = 'ice'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Plains'),
    (select id from sets where short_name = 'ice'),
    '367',
    'common'
) ,
(
    (select id from mtgcard where name = 'Walking Wall'),
    (select id from sets where short_name = 'ice'),
    '346',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Imposing Visage'),
    (select id from sets where short_name = 'ice'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warning'),
    (select id from sets where short_name = 'ice'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Order of the Sacred Torch'),
    (select id from sets where short_name = 'ice'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snow-Covered Mountain'),
    (select id from sets where short_name = 'ice'),
    '379',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain Goat'),
    (select id from sets where short_name = 'ice'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lapis Lazuli Talisman'),
    (select id from sets where short_name = 'ice'),
    '327',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Warrior'),
    (select id from sets where short_name = 'ice'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brand of Ill Omen'),
    (select id from sets where short_name = 'ice'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Skycaptain'),
    (select id from sets where short_name = 'ice'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'War Chariot'),
    (select id from sets where short_name = 'ice'),
    '348',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Blow'),
    (select id from sets where short_name = 'ice'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Updraft'),
    (select id from sets where short_name = 'ice'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stonehands'),
    (select id from sets where short_name = 'ice'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hyalopterous Lemure'),
    (select id from sets where short_name = 'ice'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avalanche'),
    (select id from sets where short_name = 'ice'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Dead'),
    (select id from sets where short_name = 'ice'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Black'),
    (select id from sets where short_name = 'ice'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flare'),
    (select id from sets where short_name = 'ice'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Knight'),
    (select id from sets where short_name = 'ice'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sacred Boon'),
    (select id from sets where short_name = 'ice'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drift of the Dead'),
    (select id from sets where short_name = 'ice'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thoughtleech'),
    (select id from sets where short_name = 'ice'),
    '269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seizures'),
    (select id from sets where short_name = 'ice'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jester''s Mask'),
    (select id from sets where short_name = 'ice'),
    '325',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloak of Confusion'),
    (select id from sets where short_name = 'ice'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ice'),
    '375',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Warp'),
    (select id from sets where short_name = 'ice'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Sappers'),
    (select id from sets where short_name = 'ice'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flooded Woodlands'),
    (select id from sets where short_name = 'ice'),
    '290',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ice'),
    '365',
    'common'
) ,
(
    (select id from mtgcard where name = 'Portent'),
    (select id from sets where short_name = 'ice'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Blue'),
    (select id from sets where short_name = 'ice'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woolly Spider'),
    (select id from sets where short_name = 'ice'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rally'),
    (select id from sets where short_name = 'ice'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zur''s Weirding'),
    (select id from sets where short_name = 'ice'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brown Ouphe'),
    (select id from sets where short_name = 'ice'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Centaur Archer'),
    (select id from sets where short_name = 'ice'),
    '282',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mystic Might'),
    (select id from sets where short_name = 'ice'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Breath of Dreams'),
    (select id from sets where short_name = 'ice'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Conscripts'),
    (select id from sets where short_name = 'ice'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dance of the Dead'),
    (select id from sets where short_name = 'ice'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcum''s Sleigh'),
    (select id from sets where short_name = 'ice'),
    '309',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'River Delta'),
    (select id from sets where short_name = 'ice'),
    '359',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Minion of Tevesh Szat'),
    (select id from sets where short_name = 'ice'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woolly Mammoths'),
    (select id from sets where short_name = 'ice'),
    '278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Burn'),
    (select id from sets where short_name = 'ice'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skull Catapult'),
    (select id from sets where short_name = 'ice'),
    '336',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barbed Sextant'),
    (select id from sets where short_name = 'ice'),
    '312',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chaos Lord'),
    (select id from sets where short_name = 'ice'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wings of Aesthir'),
    (select id from sets where short_name = 'ice'),
    '305',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ice'),
    '368',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balduvian Bears'),
    (select id from sets where short_name = 'ice'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fumarole'),
    (select id from sets where short_name = 'ice'),
    '291',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Farmer'),
    (select id from sets where short_name = 'ice'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balduvian Barbarians'),
    (select id from sets where short_name = 'ice'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oath of Lim-Dûl'),
    (select id from sets where short_name = 'ice'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea Spirit'),
    (select id from sets where short_name = 'ice'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Infernal Darkness'),
    (select id from sets where short_name = 'ice'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Squatters'),
    (select id from sets where short_name = 'ice'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aggression'),
    (select id from sets where short_name = 'ice'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brine Shaman'),
    (select id from sets where short_name = 'ice'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hydroblast'),
    (select id from sets where short_name = 'ice'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Skyknight'),
    (select id from sets where short_name = 'ice'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Banishing'),
    (select id from sets where short_name = 'ice'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Growth'),
    (select id from sets where short_name = 'ice'),
    '277',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arenson''s Aura'),
    (select id from sets where short_name = 'ice'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infinite Hourglass'),
    (select id from sets where short_name = 'ice'),
    '323',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forgotten Lore'),
    (select id from sets where short_name = 'ice'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ice'),
    '380',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abyssal Specter'),
    (select id from sets where short_name = 'ice'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fyndhorn Elder'),
    (select id from sets where short_name = 'ice'),
    '243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armor of Faith'),
    (select id from sets where short_name = 'ice'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crown of the Ages'),
    (select id from sets where short_name = 'ice'),
    '315',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spoils of Evil'),
    (select id from sets where short_name = 'ice'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Ski Patrol'),
    (select id from sets where short_name = 'ice'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Essence Vortex'),
    (select id from sets where short_name = 'ice'),
    '287',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barbarian Guides'),
    (select id from sets where short_name = 'ice'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lhurgoyf'),
    (select id from sets where short_name = 'ice'),
    '252',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Adarkar Sentinel'),
    (select id from sets where short_name = 'ice'),
    '306',
    'uncommon'
) 
 on conflict do nothing;
