    insert into mtgcard(name) values ('Thermokarst') on conflict do nothing;
    insert into mtgcard(name) values ('Elvish Healer') on conflict do nothing;
    insert into mtgcard(name) values ('Gorilla Pack') on conflict do nothing;
    insert into mtgcard(name) values ('Fyndhorn Brownie') on conflict do nothing;
    insert into mtgcard(name) values ('Nacre Talisman') on conflict do nothing;
    insert into mtgcard(name) values ('Glacial Wall') on conflict do nothing;
    insert into mtgcard(name) values ('Game of Chaos') on conflict do nothing;
    insert into mtgcard(name) values ('Snow Hound') on conflict do nothing;
    insert into mtgcard(name) values ('Gangrenous Zombies') on conflict do nothing;
    insert into mtgcard(name) values ('Time Bomb') on conflict do nothing;
    insert into mtgcard(name) values ('Adarkar Wastes') on conflict do nothing;
    insert into mtgcard(name) values ('Bone Shaman') on conflict do nothing;
    insert into mtgcard(name) values ('Adarkar Unicorn') on conflict do nothing;
    insert into mtgcard(name) values ('Folk of the Pines') on conflict do nothing;
    insert into mtgcard(name) values ('Vexing Arcanix') on conflict do nothing;
    insert into mtgcard(name) values ('Lost Order of Jarkeld') on conflict do nothing;
    insert into mtgcard(name) values ('Green Scarab') on conflict do nothing;
    insert into mtgcard(name) values ('Deflection') on conflict do nothing;
    insert into mtgcard(name) values ('Essence Flare') on conflict do nothing;
    insert into mtgcard(name) values ('Icequake') on conflict do nothing;
    insert into mtgcard(name) values ('Blinking Spirit') on conflict do nothing;
    insert into mtgcard(name) values ('Zuran Spellcaster') on conflict do nothing;
    insert into mtgcard(name) values ('Brushland') on conflict do nothing;
    insert into mtgcard(name) values ('Mountain') on conflict do nothing;
    insert into mtgcard(name) values ('Illusions of Grandeur') on conflict do nothing;
    insert into mtgcard(name) values ('Ice Cauldron') on conflict do nothing;
    insert into mtgcard(name) values ('Reality Twist') on conflict do nothing;
    insert into mtgcard(name) values ('Snow Fortress') on conflict do nothing;
    insert into mtgcard(name) values ('Hot Springs') on conflict do nothing;
    insert into mtgcard(name) values ('Wrath of Marit Lage') on conflict do nothing;
    insert into mtgcard(name) values ('Regeneration') on conflict do nothing;
    insert into mtgcard(name) values ('Burnt Offering') on conflict do nothing;
    insert into mtgcard(name) values ('Caribou Range') on conflict do nothing;
    insert into mtgcard(name) values ('Diabolic Vision') on conflict do nothing;
    insert into mtgcard(name) values ('Icy Manipulator') on conflict do nothing;
    insert into mtgcard(name) values ('Soldevi Machinist') on conflict do nothing;
    insert into mtgcard(name) values ('Elder Druid') on conflict do nothing;
    insert into mtgcard(name) values ('Cooperation') on conflict do nothing;
    insert into mtgcard(name) values ('Orcish Lumberjack') on conflict do nothing;
    insert into mtgcard(name) values ('Infuse') on conflict do nothing;
    insert into mtgcard(name) values ('Force Void') on conflict do nothing;
    insert into mtgcard(name) values ('Word of Undoing') on conflict do nothing;
    insert into mtgcard(name) values ('Arnjlot''s Ascent') on conflict do nothing;
    insert into mtgcard(name) values ('Monsoon') on conflict do nothing;
    insert into mtgcard(name) values ('Chromatic Armor') on conflict do nothing;
    insert into mtgcard(name) values ('Skeleton Ship') on conflict do nothing;
    insert into mtgcard(name) values ('Glacial Crevasses') on conflict do nothing;
    insert into mtgcard(name) values ('Plains') on conflict do nothing;
    insert into mtgcard(name) values ('Anarchy') on conflict do nothing;
    insert into mtgcard(name) values ('Tinder Wall') on conflict do nothing;
    insert into mtgcard(name) values ('Fanatical Fever') on conflict do nothing;
    insert into mtgcard(name) values ('Knight of Stromgald') on conflict do nothing;
    insert into mtgcard(name) values ('Celestial Sword') on conflict do nothing;
    insert into mtgcard(name) values ('Snow Devil') on conflict do nothing;
    insert into mtgcard(name) values ('Kjeldoran Frostbeast') on conflict do nothing;
    insert into mtgcard(name) values ('Sulfurous Springs') on conflict do nothing;
    insert into mtgcard(name) values ('Vertigo') on conflict do nothing;
    insert into mtgcard(name) values ('Shield Bearer') on conflict do nothing;
    insert into mtgcard(name) values ('Earthlore') on conflict do nothing;
    insert into mtgcard(name) values ('Jeweled Amulet') on conflict do nothing;
    insert into mtgcard(name) values ('Norritt') on conflict do nothing;
    insert into mtgcard(name) values ('Swords to Plowshares') on conflict do nothing;
    insert into mtgcard(name) values ('Energy Storm') on conflict do nothing;
    insert into mtgcard(name) values ('Icy Prison') on conflict do nothing;
    insert into mtgcard(name) values ('Zuran Orb') on conflict do nothing;
    insert into mtgcard(name) values ('Fyndhorn Elves') on conflict do nothing;
    insert into mtgcard(name) values ('Jokulhaups') on conflict do nothing;
    insert into mtgcard(name) values ('Merieke Ri Berit') on conflict do nothing;
    insert into mtgcard(name) values ('Sunstone') on conflict do nothing;
    insert into mtgcard(name) values ('Fylgja') on conflict do nothing;
    insert into mtgcard(name) values ('Lim-Dûl''s Cohort') on conflict do nothing;
    insert into mtgcard(name) values ('Glacial Chasm') on conflict do nothing;
    insert into mtgcard(name) values ('Freyalise''s Charm') on conflict do nothing;
    insert into mtgcard(name) values ('Fyndhorn Pollen') on conflict do nothing;
    insert into mtgcard(name) values ('Mind Whip') on conflict do nothing;
    insert into mtgcard(name) values ('Soul Kiss') on conflict do nothing;
    insert into mtgcard(name) values ('Giant Growth') on conflict do nothing;
    insert into mtgcard(name) values ('Clairvoyance') on conflict do nothing;
    insert into mtgcard(name) values ('Swamp') on conflict do nothing;
    insert into mtgcard(name) values ('Word of Blasting') on conflict do nothing;
    insert into mtgcard(name) values ('Sibilant Spirit') on conflict do nothing;
    insert into mtgcard(name) values ('Gravebind') on conflict do nothing;
    insert into mtgcard(name) values ('Jester''s Cap') on conflict do nothing;
    insert into mtgcard(name) values ('Gaze of Pain') on conflict do nothing;
    insert into mtgcard(name) values ('Circle of Protection: Green') on conflict do nothing;
    insert into mtgcard(name) values ('Circle of Protection: White') on conflict do nothing;
    insert into mtgcard(name) values ('Touch of Vitae') on conflict do nothing;
    insert into mtgcard(name) values ('Vibrating Sphere') on conflict do nothing;
    insert into mtgcard(name) values ('Mole Worms') on conflict do nothing;
    insert into mtgcard(name) values ('Elkin Bottle') on conflict do nothing;
    insert into mtgcard(name) values ('Scaled Wurm') on conflict do nothing;
    insert into mtgcard(name) values ('Hallowed Ground') on conflict do nothing;
    insert into mtgcard(name) values ('Whalebone Glider') on conflict do nothing;
    insert into mtgcard(name) values ('Snow-Covered Forest') on conflict do nothing;
    insert into mtgcard(name) values ('Stench of Evil') on conflict do nothing;
    insert into mtgcard(name) values ('Stunted Growth') on conflict do nothing;
    insert into mtgcard(name) values ('Wind Spirit') on conflict do nothing;
    insert into mtgcard(name) values ('Leshrac''s Rite') on conflict do nothing;
    insert into mtgcard(name) values ('Dark Ritual') on conflict do nothing;
    insert into mtgcard(name) values ('Mistfolk') on conflict do nothing;
    insert into mtgcard(name) values ('Thunder Wall') on conflict do nothing;
    insert into mtgcard(name) values ('Meteor Shower') on conflict do nothing;
    insert into mtgcard(name) values ('Maddening Wind') on conflict do nothing;
    insert into mtgcard(name) values ('Despotic Scepter') on conflict do nothing;
    insert into mtgcard(name) values ('Necropotence') on conflict do nothing;
    insert into mtgcard(name) values ('Moor Fiend') on conflict do nothing;
    insert into mtgcard(name) values ('Fear') on conflict do nothing;
    insert into mtgcard(name) values ('Circle of Protection: Red') on conflict do nothing;
    insert into mtgcard(name) values ('Urza''s Bauble') on conflict do nothing;
    insert into mtgcard(name) values ('Mystic Remora') on conflict do nothing;
    insert into mtgcard(name) values ('Hipparion') on conflict do nothing;
    insert into mtgcard(name) values ('Stone Rain') on conflict do nothing;
    insert into mtgcard(name) values ('Ray of Erasure') on conflict do nothing;
    insert into mtgcard(name) values ('Freyalise Supplicant') on conflict do nothing;
    insert into mtgcard(name) values ('Balduvian Conjurer') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Snowman') on conflict do nothing;
    insert into mtgcard(name) values ('Black Scarab') on conflict do nothing;
    insert into mtgcard(name) values ('Ritual of Subdual') on conflict do nothing;
    insert into mtgcard(name) values ('Staff of the Ages') on conflict do nothing;
    insert into mtgcard(name) values ('Aegis of the Meek') on conflict do nothing;
    insert into mtgcard(name) values ('Wall of Pine Needles') on conflict do nothing;
    insert into mtgcard(name) values ('Lava Tubes') on conflict do nothing;
    insert into mtgcard(name) values ('Snowblind') on conflict do nothing;
    insert into mtgcard(name) values ('Forbidden Lore') on conflict do nothing;
    insert into mtgcard(name) values ('Total War') on conflict do nothing;
    insert into mtgcard(name) values ('Minion of Leshrac') on conflict do nothing;
    insert into mtgcard(name) values ('Errant Minion') on conflict do nothing;
    insert into mtgcard(name) values ('Island') on conflict do nothing;
    insert into mtgcard(name) values ('Mind Ravel') on conflict do nothing;
    insert into mtgcard(name) values ('Hymn of Rebirth') on conflict do nothing;
    insert into mtgcard(name) values ('Elemental Augury') on conflict do nothing;
    insert into mtgcard(name) values ('Ghostly Flame') on conflict do nothing;
    insert into mtgcard(name) values ('Wall of Shields') on conflict do nothing;
    insert into mtgcard(name) values ('Ray of Command') on conflict do nothing;
    insert into mtgcard(name) values ('Malachite Talisman') on conflict do nothing;
    insert into mtgcard(name) values ('Illusionary Wall') on conflict do nothing;
    insert into mtgcard(name) values ('Johtull Wurm') on conflict do nothing;
    insert into mtgcard(name) values ('Soldevi Golem') on conflict do nothing;
    insert into mtgcard(name) values ('Snow-Covered Swamp') on conflict do nothing;
    insert into mtgcard(name) values ('Mudslide') on conflict do nothing;
    insert into mtgcard(name) values ('Dread Wight') on conflict do nothing;
    insert into mtgcard(name) values ('Fyndhorn Bow') on conflict do nothing;
    insert into mtgcard(name) values ('Kjeldoran Royal Guard') on conflict do nothing;
    insert into mtgcard(name) values ('Nature''s Lore') on conflict do nothing;
    insert into mtgcard(name) values ('Silver Erne') on conflict do nothing;
    insert into mtgcard(name) values ('Flow of Maggots') on conflict do nothing;
    insert into mtgcard(name) values ('Sabretooth Tiger') on conflict do nothing;
    insert into mtgcard(name) values ('Illusionary Terrain') on conflict do nothing;
    insert into mtgcard(name) values ('Curse of Marit Lage') on conflict do nothing;
    insert into mtgcard(name) values ('Fire Covenant') on conflict do nothing;
    insert into mtgcard(name) values ('General Jarkeld') on conflict do nothing;
    insert into mtgcard(name) values ('Stromgald Cabal') on conflict do nothing;
    insert into mtgcard(name) values ('Binding Grasp') on conflict do nothing;
    insert into mtgcard(name) values ('Blessed Wine') on conflict do nothing;
    insert into mtgcard(name) values ('Ashen Ghoul') on conflict do nothing;
    insert into mtgcard(name) values ('Songs of the Damned') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Mutant') on conflict do nothing;
    insert into mtgcard(name) values ('Prismatic Ward') on conflict do nothing;
    insert into mtgcard(name) values ('Pyknite') on conflict do nothing;
    insert into mtgcard(name) values ('Krovikan Vampire') on conflict do nothing;
    insert into mtgcard(name) values ('Zuran Enchanter') on conflict do nothing;
    insert into mtgcard(name) values ('Hoar Shade') on conflict do nothing;
    insert into mtgcard(name) values ('Arcum''s Whistle') on conflict do nothing;
    insert into mtgcard(name) values ('Shield of the Ages') on conflict do nothing;
    insert into mtgcard(name) values ('Balduvian Shaman') on conflict do nothing;
    insert into mtgcard(name) values ('Phantasmal Mount') on conflict do nothing;
    insert into mtgcard(name) values ('Legions of Lim-Dûl') on conflict do nothing;
    insert into mtgcard(name) values ('Altar of Bone') on conflict do nothing;
    insert into mtgcard(name) values ('Amulet of Quoz') on conflict do nothing;
    insert into mtgcard(name) values ('Forest') on conflict do nothing;
    insert into mtgcard(name) values ('Formation') on conflict do nothing;
    insert into mtgcard(name) values ('Márton Stromgald') on conflict do nothing;
    insert into mtgcard(name) values ('Snowfall') on conflict do nothing;
    insert into mtgcard(name) values ('Stone Spirit') on conflict do nothing;
    insert into mtgcard(name) values ('Lava Burst') on conflict do nothing;
    insert into mtgcard(name) values ('Storm Spirit') on conflict do nothing;
    insert into mtgcard(name) values ('Rime Dryad') on conflict do nothing;
    insert into mtgcard(name) values ('Mercenaries') on conflict do nothing;
    insert into mtgcard(name) values ('Dwarven Armory') on conflict do nothing;
    insert into mtgcard(name) values ('Karplusan Yeti') on conflict do nothing;
    insert into mtgcard(name) values ('Aurochs') on conflict do nothing;
    insert into mtgcard(name) values ('Shatter') on conflict do nothing;
    insert into mtgcard(name) values ('Tor Giant') on conflict do nothing;
    insert into mtgcard(name) values ('Pale Bears') on conflict do nothing;
    insert into mtgcard(name) values ('Spectral Shield') on conflict do nothing;
    insert into mtgcard(name) values ('Orcish Healer') on conflict do nothing;
    insert into mtgcard(name) values ('Cold Snap') on conflict do nothing;
    insert into mtgcard(name) values ('Errantry') on conflict do nothing;
    insert into mtgcard(name) values ('Hematite Talisman') on conflict do nothing;
    insert into mtgcard(name) values ('Kelsinko Ranger') on conflict do nothing;
    insert into mtgcard(name) values ('Krovikan Fetish') on conflict do nothing;
    insert into mtgcard(name) values ('Power Sink') on conflict do nothing;
    insert into mtgcard(name) values ('Ice Floe') on conflict do nothing;
    insert into mtgcard(name) values ('Magus of the Unseen') on conflict do nothing;
    insert into mtgcard(name) values ('Lure') on conflict do nothing;
    insert into mtgcard(name) values ('Timberline Ridge') on conflict do nothing;
    insert into mtgcard(name) values ('Pyroclasm') on conflict do nothing;
    insert into mtgcard(name) values ('Shambling Strider') on conflict do nothing;
    insert into mtgcard(name) values ('Pygmy Allosaurus') on conflict do nothing;
    insert into mtgcard(name) values ('Foxfire') on conflict do nothing;
    insert into mtgcard(name) values ('Fiery Justice') on conflict do nothing;
    insert into mtgcard(name) values ('Giant Trap Door Spider') on conflict do nothing;
    insert into mtgcard(name) values ('Foul Familiar') on conflict do nothing;
    insert into mtgcard(name) values ('Baton of Morale') on conflict do nothing;
    insert into mtgcard(name) values ('Brainstorm') on conflict do nothing;
    insert into mtgcard(name) values ('Demonic Consultation') on conflict do nothing;
    insert into mtgcard(name) values ('Melting') on conflict do nothing;
    insert into mtgcard(name) values ('Orcish Librarian') on conflict do nothing;
    insert into mtgcard(name) values ('Venomous Breath') on conflict do nothing;
    insert into mtgcard(name) values ('Hecatomb') on conflict do nothing;
    insert into mtgcard(name) values ('Trailblazer') on conflict do nothing;
    insert into mtgcard(name) values ('Underground River') on conflict do nothing;
    insert into mtgcard(name) values ('Order of the White Shield') on conflict do nothing;
    insert into mtgcard(name) values ('Dreams of the Dead') on conflict do nothing;
    insert into mtgcard(name) values ('Sleight of Mind') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Lyre') on conflict do nothing;
    insert into mtgcard(name) values ('Grizzled Wolverine') on conflict do nothing;
    insert into mtgcard(name) values ('Drought') on conflict do nothing;
    insert into mtgcard(name) values ('Veldt') on conflict do nothing;
    insert into mtgcard(name) values ('Arctic Foxes') on conflict do nothing;
    insert into mtgcard(name) values ('Shyft') on conflict do nothing;
    insert into mtgcard(name) values ('Justice') on conflict do nothing;
    insert into mtgcard(name) values ('Red Scarab') on conflict do nothing;
    insert into mtgcard(name) values ('Soul Barrier') on conflict do nothing;
    insert into mtgcard(name) values ('Essence Filter') on conflict do nothing;
    insert into mtgcard(name) values ('Incinerate') on conflict do nothing;
    insert into mtgcard(name) values ('Krovikan Sorcerer') on conflict do nothing;
    insert into mtgcard(name) values ('Yavimaya Gnats') on conflict do nothing;
    insert into mtgcard(name) values ('Arcum''s Weathervane') on conflict do nothing;
    insert into mtgcard(name) values ('Heal') on conflict do nothing;
    insert into mtgcard(name) values ('Wiitigo') on conflict do nothing;
    insert into mtgcard(name) values ('Musician') on conflict do nothing;
    insert into mtgcard(name) values ('Soldevi Simulacrum') on conflict do nothing;
    insert into mtgcard(name) values ('Iceberg') on conflict do nothing;
    insert into mtgcard(name) values ('Orcish Cannoneers') on conflict do nothing;
    insert into mtgcard(name) values ('Touch of Death') on conflict do nothing;
    insert into mtgcard(name) values ('Dire Wolves') on conflict do nothing;
    insert into mtgcard(name) values ('Kjeldoran Elite Guard') on conflict do nothing;
    insert into mtgcard(name) values ('Winter''s Chill') on conflict do nothing;
    insert into mtgcard(name) values ('Earthlink') on conflict do nothing;
    insert into mtgcard(name) values ('Battle Frenzy') on conflict do nothing;
    insert into mtgcard(name) values ('Whiteout') on conflict do nothing;
    insert into mtgcard(name) values ('Onyx Talisman') on conflict do nothing;
    insert into mtgcard(name) values ('Hurricane') on conflict do nothing;
    insert into mtgcard(name) values ('Pox') on conflict do nothing;
    insert into mtgcard(name) values ('Call to Arms') on conflict do nothing;
    insert into mtgcard(name) values ('Panic') on conflict do nothing;
    insert into mtgcard(name) values ('Illusionary Presence') on conflict do nothing;
    insert into mtgcard(name) values ('Chaos Moon') on conflict do nothing;
    insert into mtgcard(name) values ('Illusionary Forces') on conflict do nothing;
    insert into mtgcard(name) values ('Seraph') on conflict do nothing;
    insert into mtgcard(name) values ('Withering Wisps') on conflict do nothing;
    insert into mtgcard(name) values ('Leshrac''s Sigil') on conflict do nothing;
    insert into mtgcard(name) values ('Snow-Covered Island') on conflict do nothing;
    insert into mtgcard(name) values ('Flame Spirit') on conflict do nothing;
    insert into mtgcard(name) values ('Mesmeric Trance') on conflict do nothing;
    insert into mtgcard(name) values ('Conquer') on conflict do nothing;
    insert into mtgcard(name) values ('Counterspell') on conflict do nothing;
    insert into mtgcard(name) values ('Polar Kraken') on conflict do nothing;
    insert into mtgcard(name) values ('Lim-Dûl''s Hex') on conflict do nothing;
    insert into mtgcard(name) values ('Freyalise''s Winds') on conflict do nothing;
    insert into mtgcard(name) values ('Melee') on conflict do nothing;
    insert into mtgcard(name) values ('Tarpan') on conflict do nothing;
    insert into mtgcard(name) values ('Spoils of War') on conflict do nothing;
    insert into mtgcard(name) values ('Blue Scarab') on conflict do nothing;
    insert into mtgcard(name) values ('Disenchant') on conflict do nothing;
    insert into mtgcard(name) values ('Infernal Denizen') on conflict do nothing;
    insert into mtgcard(name) values ('Kjeldoran Phalanx') on conflict do nothing;
    insert into mtgcard(name) values ('Chub Toad') on conflict do nothing;
    insert into mtgcard(name) values ('Glaciers') on conflict do nothing;
    insert into mtgcard(name) values ('Pentagram of the Ages') on conflict do nothing;
    insert into mtgcard(name) values ('Halls of Mist') on conflict do nothing;
    insert into mtgcard(name) values ('Wall of Lava') on conflict do nothing;
    insert into mtgcard(name) values ('Karplusan Forest') on conflict do nothing;
    insert into mtgcard(name) values ('Krovikan Elementalist') on conflict do nothing;
    insert into mtgcard(name) values ('Stampede') on conflict do nothing;
    insert into mtgcard(name) values ('Mountain Titan') on conflict do nothing;
    insert into mtgcard(name) values ('Kjeldoran Guard') on conflict do nothing;
    insert into mtgcard(name) values ('Enduring Renewal') on conflict do nothing;
    insert into mtgcard(name) values ('Pestilence Rats') on conflict do nothing;
    insert into mtgcard(name) values ('Stormbind') on conflict do nothing;
    insert into mtgcard(name) values ('Pyroblast') on conflict do nothing;
    insert into mtgcard(name) values ('Blizzard') on conflict do nothing;
    insert into mtgcard(name) values ('Balduvian Hydra') on conflict do nothing;
    insert into mtgcard(name) values ('Land Cap') on conflict do nothing;
    insert into mtgcard(name) values ('Enervate') on conflict do nothing;
    insert into mtgcard(name) values ('Karplusan Giant') on conflict do nothing;
    insert into mtgcard(name) values ('Battle Cry') on conflict do nothing;
    insert into mtgcard(name) values ('White Scarab') on conflict do nothing;
    insert into mtgcard(name) values ('Pit Trap') on conflict do nothing;
    insert into mtgcard(name) values ('Death Ward') on conflict do nothing;
    insert into mtgcard(name) values ('Runed Arch') on conflict do nothing;
    insert into mtgcard(name) values ('Reclamation') on conflict do nothing;
    insert into mtgcard(name) values ('Howl from Beyond') on conflict do nothing;
    insert into mtgcard(name) values ('Naked Singularity') on conflict do nothing;
    insert into mtgcard(name) values ('Juniper Order Druid') on conflict do nothing;
    insert into mtgcard(name) values ('Snow-Covered Plains') on conflict do nothing;
    insert into mtgcard(name) values ('Walking Wall') on conflict do nothing;
    insert into mtgcard(name) values ('Imposing Visage') on conflict do nothing;
    insert into mtgcard(name) values ('Warning') on conflict do nothing;
    insert into mtgcard(name) values ('Order of the Sacred Torch') on conflict do nothing;
    insert into mtgcard(name) values ('Snow-Covered Mountain') on conflict do nothing;
    insert into mtgcard(name) values ('Mountain Goat') on conflict do nothing;
    insert into mtgcard(name) values ('Lapis Lazuli Talisman') on conflict do nothing;
    insert into mtgcard(name) values ('Kjeldoran Warrior') on conflict do nothing;
    insert into mtgcard(name) values ('Brand of Ill Omen') on conflict do nothing;
    insert into mtgcard(name) values ('Kjeldoran Skycaptain') on conflict do nothing;
    insert into mtgcard(name) values ('War Chariot') on conflict do nothing;
    insert into mtgcard(name) values ('Lightning Blow') on conflict do nothing;
    insert into mtgcard(name) values ('Updraft') on conflict do nothing;
    insert into mtgcard(name) values ('Stonehands') on conflict do nothing;
    insert into mtgcard(name) values ('Hyalopterous Lemure') on conflict do nothing;
    insert into mtgcard(name) values ('Avalanche') on conflict do nothing;
    insert into mtgcard(name) values ('Kjeldoran Dead') on conflict do nothing;
    insert into mtgcard(name) values ('Circle of Protection: Black') on conflict do nothing;
    insert into mtgcard(name) values ('Flare') on conflict do nothing;
    insert into mtgcard(name) values ('Kjeldoran Knight') on conflict do nothing;
    insert into mtgcard(name) values ('Sacred Boon') on conflict do nothing;
    insert into mtgcard(name) values ('Drift of the Dead') on conflict do nothing;
    insert into mtgcard(name) values ('Thoughtleech') on conflict do nothing;
    insert into mtgcard(name) values ('Seizures') on conflict do nothing;
    insert into mtgcard(name) values ('Jester''s Mask') on conflict do nothing;
    insert into mtgcard(name) values ('Cloak of Confusion') on conflict do nothing;
    insert into mtgcard(name) values ('Mind Warp') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Sappers') on conflict do nothing;
    insert into mtgcard(name) values ('Flooded Woodlands') on conflict do nothing;
    insert into mtgcard(name) values ('Portent') on conflict do nothing;
    insert into mtgcard(name) values ('Circle of Protection: Blue') on conflict do nothing;
    insert into mtgcard(name) values ('Woolly Spider') on conflict do nothing;
    insert into mtgcard(name) values ('Rally') on conflict do nothing;
    insert into mtgcard(name) values ('Zur''s Weirding') on conflict do nothing;
    insert into mtgcard(name) values ('Brown Ouphe') on conflict do nothing;
    insert into mtgcard(name) values ('Centaur Archer') on conflict do nothing;
    insert into mtgcard(name) values ('Mystic Might') on conflict do nothing;
    insert into mtgcard(name) values ('Breath of Dreams') on conflict do nothing;
    insert into mtgcard(name) values ('Orcish Conscripts') on conflict do nothing;
    insert into mtgcard(name) values ('Dance of the Dead') on conflict do nothing;
    insert into mtgcard(name) values ('Arcum''s Sleigh') on conflict do nothing;
    insert into mtgcard(name) values ('River Delta') on conflict do nothing;
    insert into mtgcard(name) values ('Minion of Tevesh Szat') on conflict do nothing;
    insert into mtgcard(name) values ('Woolly Mammoths') on conflict do nothing;
    insert into mtgcard(name) values ('Soul Burn') on conflict do nothing;
    insert into mtgcard(name) values ('Skull Catapult') on conflict do nothing;
    insert into mtgcard(name) values ('Barbed Sextant') on conflict do nothing;
    insert into mtgcard(name) values ('Chaos Lord') on conflict do nothing;
    insert into mtgcard(name) values ('Wings of Aesthir') on conflict do nothing;
    insert into mtgcard(name) values ('Balduvian Bears') on conflict do nothing;
    insert into mtgcard(name) values ('Fumarole') on conflict do nothing;
    insert into mtgcard(name) values ('Orcish Farmer') on conflict do nothing;
    insert into mtgcard(name) values ('Balduvian Barbarians') on conflict do nothing;
    insert into mtgcard(name) values ('Oath of Lim-Dûl') on conflict do nothing;
    insert into mtgcard(name) values ('Sea Spirit') on conflict do nothing;
    insert into mtgcard(name) values ('Infernal Darkness') on conflict do nothing;
    insert into mtgcard(name) values ('Orcish Squatters') on conflict do nothing;
    insert into mtgcard(name) values ('Aggression') on conflict do nothing;
    insert into mtgcard(name) values ('Brine Shaman') on conflict do nothing;
    insert into mtgcard(name) values ('Hydroblast') on conflict do nothing;
    insert into mtgcard(name) values ('Kjeldoran Skyknight') on conflict do nothing;
    insert into mtgcard(name) values ('Dark Banishing') on conflict do nothing;
    insert into mtgcard(name) values ('Wild Growth') on conflict do nothing;
    insert into mtgcard(name) values ('Arenson''s Aura') on conflict do nothing;
    insert into mtgcard(name) values ('Infinite Hourglass') on conflict do nothing;
    insert into mtgcard(name) values ('Forgotten Lore') on conflict do nothing;
    insert into mtgcard(name) values ('Abyssal Specter') on conflict do nothing;
    insert into mtgcard(name) values ('Fyndhorn Elder') on conflict do nothing;
    insert into mtgcard(name) values ('Armor of Faith') on conflict do nothing;
    insert into mtgcard(name) values ('Crown of the Ages') on conflict do nothing;
    insert into mtgcard(name) values ('Spoils of Evil') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Ski Patrol') on conflict do nothing;
    insert into mtgcard(name) values ('Essence Vortex') on conflict do nothing;
    insert into mtgcard(name) values ('Barbarian Guides') on conflict do nothing;
    insert into mtgcard(name) values ('Lhurgoyf') on conflict do nothing;
    insert into mtgcard(name) values ('Adarkar Sentinel') on conflict do nothing;
