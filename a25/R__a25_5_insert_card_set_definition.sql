insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Sundering Titan'),
    (select id from sets where short_name = 'a25'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Niv-Mizzet, the Firemind'),
    (select id from sets where short_name = 'a25'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Epic Confrontation'),
    (select id from sets where short_name = 'a25'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coalition Relic'),
    (select id from sets where short_name = 'a25'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chartooth Cougar'),
    (select id from sets where short_name = 'a25'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Echoing Courage'),
    (select id from sets where short_name = 'a25'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Will-o''-the-Wisp'),
    (select id from sets where short_name = 'a25'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fierce Empath'),
    (select id from sets where short_name = 'a25'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampire Lacerator'),
    (select id from sets where short_name = 'a25'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lunarch Mantle'),
    (select id from sets where short_name = 'a25'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Griffin Protector'),
    (select id from sets where short_name = 'a25'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Laquatus''s Champion'),
    (select id from sets where short_name = 'a25'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coralhelm Guide'),
    (select id from sets where short_name = 'a25'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baloth Null'),
    (select id from sets where short_name = 'a25'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Uncaged Fury'),
    (select id from sets where short_name = 'a25'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudshift'),
    (select id from sets where short_name = 'a25'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Choking Tethers'),
    (select id from sets where short_name = 'a25'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Utopia Sprawl'),
    (select id from sets where short_name = 'a25'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vindicate'),
    (select id from sets where short_name = 'a25'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zoetic Cavern'),
    (select id from sets where short_name = 'a25'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eidolon of the Great Revel'),
    (select id from sets where short_name = 'a25'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woolly Loxodon'),
    (select id from sets where short_name = 'a25'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simian Spirit Guide'),
    (select id from sets where short_name = 'a25'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Piper'),
    (select id from sets where short_name = 'a25'),
    '168',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Haunted Fengraf'),
    (select id from sets where short_name = 'a25'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pillory of the Sleepless'),
    (select id from sets where short_name = 'a25'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ambassador Oak'),
    (select id from sets where short_name = 'a25'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Imperial Recruiter'),
    (select id from sets where short_name = 'a25'),
    '136',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Animar, Soul of Elements'),
    (select id from sets where short_name = 'a25'),
    '196',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Primal Clay'),
    (select id from sets where short_name = 'a25'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chalice of the Void'),
    (select id from sets where short_name = 'a25'),
    '222',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jalira, Master Polymorphist'),
    (select id from sets where short_name = 'a25'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Craving'),
    (select id from sets where short_name = 'a25'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = 'a25'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skirk Commando'),
    (select id from sets where short_name = 'a25'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Borrowing 100,000 Arrows'),
    (select id from sets where short_name = 'a25'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whitemane Lion'),
    (select id from sets where short_name = 'a25'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karona''s Zealot'),
    (select id from sets where short_name = 'a25'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mystic Snake'),
    (select id from sets where short_name = 'a25'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Act of Treason'),
    (select id from sets where short_name = 'a25'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'a25'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Protean Hulk'),
    (select id from sets where short_name = 'a25'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blue Elemental Blast'),
    (select id from sets where short_name = 'a25'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cascade Bluffs'),
    (select id from sets where short_name = 'a25'),
    '237',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shadowmage Infiltrator'),
    (select id from sets where short_name = 'a25'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Akroma, Angel of Fury'),
    (select id from sets where short_name = 'a25'),
    '119',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Perilous Myr'),
    (select id from sets where short_name = 'a25'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stampede Driver'),
    (select id from sets where short_name = 'a25'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Undead Gladiator'),
    (select id from sets where short_name = 'a25'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Master of the Wild Hunt'),
    (select id from sets where short_name = 'a25'),
    '181',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Strionic Resonator'),
    (select id from sets where short_name = 'a25'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Auramancer'),
    (select id from sets where short_name = 'a25'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nettle Sentinel'),
    (select id from sets where short_name = 'a25'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krosan Colossus'),
    (select id from sets where short_name = 'a25'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Path of Peace'),
    (select id from sets where short_name = 'a25'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death''s-Head Buzzard'),
    (select id from sets where short_name = 'a25'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eladamri''s Call'),
    (select id from sets where short_name = 'a25'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystic of the Hidden Way'),
    (select id from sets where short_name = 'a25'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Summoner''s Pact'),
    (select id from sets where short_name = 'a25'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cultivate'),
    (select id from sets where short_name = 'a25'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Renewed Faith'),
    (select id from sets where short_name = 'a25'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fencing Ace'),
    (select id from sets where short_name = 'a25'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arbor Elf'),
    (select id from sets where short_name = 'a25'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gisela, Blade of Goldnight'),
    (select id from sets where short_name = 'a25'),
    '204',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blue Sun''s Zenith'),
    (select id from sets where short_name = 'a25'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spikeshot Goblin'),
    (select id from sets where short_name = 'a25'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frenzied Goblin'),
    (select id from sets where short_name = 'a25'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'a25'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sift'),
    (select id from sets where short_name = 'a25'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crimson Mage'),
    (select id from sets where short_name = 'a25'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mogg Flunkies'),
    (select id from sets where short_name = 'a25'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bident of Thassa'),
    (select id from sets where short_name = 'a25'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruric Thar, the Unbowed'),
    (select id from sets where short_name = 'a25'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloudblazer'),
    (select id from sets where short_name = 'a25'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disfigure'),
    (select id from sets where short_name = 'a25'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Page'),
    (select id from sets where short_name = 'a25'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dauntless Cathar'),
    (select id from sets where short_name = 'a25'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Timberpack Wolf'),
    (select id from sets where short_name = 'a25'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murder of Crows'),
    (select id from sets where short_name = 'a25'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Twisted Image'),
    (select id from sets where short_name = 'a25'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enthralling Victor'),
    (select id from sets where short_name = 'a25'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magus of the Wheel'),
    (select id from sets where short_name = 'a25'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ball Lightning'),
    (select id from sets where short_name = 'a25'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Humble Defector'),
    (select id from sets where short_name = 'a25'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cinder Storm'),
    (select id from sets where short_name = 'a25'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Watchwolf'),
    (select id from sets where short_name = 'a25'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Murder'),
    (select id from sets where short_name = 'a25'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doomsday'),
    (select id from sets where short_name = 'a25'),
    '88',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Diabolic Edict'),
    (select id from sets where short_name = 'a25'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'a25'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sai of the Shinobi'),
    (select id from sets where short_name = 'a25'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin War Drums'),
    (select id from sets where short_name = 'a25'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grenzo, Dungeon Warden'),
    (select id from sets where short_name = 'a25'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trumpet Blast'),
    (select id from sets where short_name = 'a25'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadly Designs'),
    (select id from sets where short_name = 'a25'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Geist of the Moors'),
    (select id from sets where short_name = 'a25'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Outrage'),
    (select id from sets where short_name = 'a25'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Red Elemental Blast'),
    (select id from sets where short_name = 'a25'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kongming, "Sleeping Dragon"'),
    (select id from sets where short_name = 'a25'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plague Wind'),
    (select id from sets where short_name = 'a25'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fathom Seer'),
    (select id from sets where short_name = 'a25'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relentless Rats'),
    (select id from sets where short_name = 'a25'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Genju of the Spires'),
    (select id from sets where short_name = 'a25'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = 'a25'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vesuvan Shapeshifter'),
    (select id from sets where short_name = 'a25'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Retraction Helix'),
    (select id from sets where short_name = 'a25'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Promise of Bunrei'),
    (select id from sets where short_name = 'a25'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'a25'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Browbeat'),
    (select id from sets where short_name = 'a25'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Twilight Mire'),
    (select id from sets where short_name = 'a25'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Triskaidekaphobia'),
    (select id from sets where short_name = 'a25'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heavy Arbalest'),
    (select id from sets where short_name = 'a25'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Noble Templar'),
    (select id from sets where short_name = 'a25'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conflux'),
    (select id from sets where short_name = 'a25'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akroma''s Vengeance'),
    (select id from sets where short_name = 'a25'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hell''s Caretaker'),
    (select id from sets where short_name = 'a25'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treasure Keeper'),
    (select id from sets where short_name = 'a25'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mesmeric Fiend'),
    (select id from sets where short_name = 'a25'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pernicious Deed'),
    (select id from sets where short_name = 'a25'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Courser of Kruphix'),
    (select id from sets where short_name = 'a25'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Congregate'),
    (select id from sets where short_name = 'a25'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urbis Protector'),
    (select id from sets where short_name = 'a25'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loyal Sentry'),
    (select id from sets where short_name = 'a25'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prossh, Skyraider of Kher'),
    (select id from sets where short_name = 'a25'),
    '214',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Assembly-Worker'),
    (select id from sets where short_name = 'a25'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rancor'),
    (select id from sets where short_name = 'a25'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'a25'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nezumi Cutthroat'),
    (select id from sets where short_name = 'a25'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lull'),
    (select id from sets where short_name = 'a25'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plummet'),
    (select id from sets where short_name = 'a25'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyroclasm'),
    (select id from sets where short_name = 'a25'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merfolk Looter'),
    (select id from sets where short_name = 'a25'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kor Firewalker'),
    (select id from sets where short_name = 'a25'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Death'),
    (select id from sets where short_name = 'a25'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas'),
    (select id from sets where short_name = 'a25'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Horseshoe Crab'),
    (select id from sets where short_name = 'a25'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Caustic Tar'),
    (select id from sets where short_name = 'a25'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Stirrings'),
    (select id from sets where short_name = 'a25'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Presence of Gond'),
    (select id from sets where short_name = 'a25'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flooded Grove'),
    (select id from sets where short_name = 'a25'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soulbright Flamekin'),
    (select id from sets where short_name = 'a25'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gods Willing'),
    (select id from sets where short_name = 'a25'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'a25'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Invigorate'),
    (select id from sets where short_name = 'a25'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Act of Heroism'),
    (select id from sets where short_name = 'a25'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Totally Lost'),
    (select id from sets where short_name = 'a25'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thalia, Guardian of Thraben'),
    (select id from sets where short_name = 'a25'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zombify'),
    (select id from sets where short_name = 'a25'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dusk Legion Zealot'),
    (select id from sets where short_name = 'a25'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iwamori of the Open Fist'),
    (select id from sets where short_name = 'a25'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcane Denial'),
    (select id from sets where short_name = 'a25'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vessel of Nascency'),
    (select id from sets where short_name = 'a25'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jackal Pup'),
    (select id from sets where short_name = 'a25'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squadron Hawk'),
    (select id from sets where short_name = 'a25'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rest in Peace'),
    (select id from sets where short_name = 'a25'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Man-o''-War'),
    (select id from sets where short_name = 'a25'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ash Barrens'),
    (select id from sets where short_name = 'a25'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Exclude'),
    (select id from sets where short_name = 'a25'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krosan Tusker'),
    (select id from sets where short_name = 'a25'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stangg'),
    (select id from sets where short_name = 'a25'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Myriad Landscape'),
    (select id from sets where short_name = 'a25'),
    '243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quicksilver Dagger'),
    (select id from sets where short_name = 'a25'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brine Elemental'),
    (select id from sets where short_name = 'a25'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Charm'),
    (select id from sets where short_name = 'a25'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savannah Lions'),
    (select id from sets where short_name = 'a25'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ainok Survivalist'),
    (select id from sets where short_name = 'a25'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shoreline Ranger'),
    (select id from sets where short_name = 'a25'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swiftfoot Boots'),
    (select id from sets where short_name = 'a25'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lorescale Coatl'),
    (select id from sets where short_name = 'a25'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Obliterator'),
    (select id from sets where short_name = 'a25'),
    '101',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pendelhaven'),
    (select id from sets where short_name = 'a25'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Accumulated Knowledge'),
    (select id from sets where short_name = 'a25'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nyx-Fleece Ram'),
    (select id from sets where short_name = 'a25'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Returned Phalanx'),
    (select id from sets where short_name = 'a25'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prophetic Prism'),
    (select id from sets where short_name = 'a25'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brion Stoutarm'),
    (select id from sets where short_name = 'a25'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Erg Raiders'),
    (select id from sets where short_name = 'a25'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ember Weaver'),
    (select id from sets where short_name = 'a25'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Luminarch Ascension'),
    (select id from sets where short_name = 'a25'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zulaport Cutthroat'),
    (select id from sets where short_name = 'a25'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rugged Prairie'),
    (select id from sets where short_name = 'a25'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skeletonize'),
    (select id from sets where short_name = 'a25'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Broodhatch Nantuko'),
    (select id from sets where short_name = 'a25'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mikokoro, Center of the Sea'),
    (select id from sets where short_name = 'a25'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fortune Thief'),
    (select id from sets where short_name = 'a25'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zada, Hedron Grinder'),
    (select id from sets where short_name = 'a25'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azusa, Lost but Seeking'),
    (select id from sets where short_name = 'a25'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruthless Ripper'),
    (select id from sets where short_name = 'a25'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodhunter Bat'),
    (select id from sets where short_name = 'a25'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Notion Thief'),
    (select id from sets where short_name = 'a25'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thresher Lizard'),
    (select id from sets where short_name = 'a25'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace, the Mind Sculptor'),
    (select id from sets where short_name = 'a25'),
    '62',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wildheart Invoker'),
    (select id from sets where short_name = 'a25'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Decree of Justice'),
    (select id from sets where short_name = 'a25'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twisted Abomination'),
    (select id from sets where short_name = 'a25'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Freed from the Real'),
    (select id from sets where short_name = 'a25'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Genju of the Falls'),
    (select id from sets where short_name = 'a25'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Ghoul'),
    (select id from sets where short_name = 'a25'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Street Wraith'),
    (select id from sets where short_name = 'a25'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight of the Skyward Eye'),
    (select id from sets where short_name = 'a25'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'a25'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blightning'),
    (select id from sets where short_name = 'a25'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vendilion Clique'),
    (select id from sets where short_name = 'a25'),
    '76',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ensnaring Bridge'),
    (select id from sets where short_name = 'a25'),
    '224',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pyre Hound'),
    (select id from sets where short_name = 'a25'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quicksand'),
    (select id from sets where short_name = 'a25'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Akroma, Angel of Wrath'),
    (select id from sets where short_name = 'a25'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Izzet Chemister'),
    (select id from sets where short_name = 'a25'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reef Worm'),
    (select id from sets where short_name = 'a25'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rishadan Port'),
    (select id from sets where short_name = 'a25'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cursecatcher'),
    (select id from sets where short_name = 'a25'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Bear'),
    (select id from sets where short_name = 'a25'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flash'),
    (select id from sets where short_name = 'a25'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fallen Angel'),
    (select id from sets where short_name = 'a25'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Wish'),
    (select id from sets where short_name = 'a25'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Moon'),
    (select id from sets where short_name = 'a25'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dirge of Dread'),
    (select id from sets where short_name = 'a25'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Self-Assembler'),
    (select id from sets where short_name = 'a25'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kavu Climber'),
    (select id from sets where short_name = 'a25'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pact of Negation'),
    (select id from sets where short_name = 'a25'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ordeal of Heliod'),
    (select id from sets where short_name = 'a25'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pillage'),
    (select id from sets where short_name = 'a25'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Supernatural Stamina'),
    (select id from sets where short_name = 'a25'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fiend Hunter'),
    (select id from sets where short_name = 'a25'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fetid Heath'),
    (select id from sets where short_name = 'a25'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'a25'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tree of Redemption'),
    (select id from sets where short_name = 'a25'),
    '191',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hordeling Outburst'),
    (select id from sets where short_name = 'a25'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nihil Spellbomb'),
    (select id from sets where short_name = 'a25'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Willbender'),
    (select id from sets where short_name = 'a25'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghost Ship'),
    (select id from sets where short_name = 'a25'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kavu Predator'),
    (select id from sets where short_name = 'a25'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ratcatcher'),
    (select id from sets where short_name = 'a25'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ire Shaman'),
    (select id from sets where short_name = 'a25'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unearth'),
    (select id from sets where short_name = 'a25'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darien, King of Kjeldor'),
    (select id from sets where short_name = 'a25'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Valor in Akros'),
    (select id from sets where short_name = 'a25'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Aberration'),
    (select id from sets where short_name = 'a25'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon''s Eye Savants'),
    (select id from sets where short_name = 'a25'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Court Hussar'),
    (select id from sets where short_name = 'a25'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravenous Chupacabra'),
    (select id from sets where short_name = 'a25'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kindle'),
    (select id from sets where short_name = 'a25'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Regrowth'),
    (select id from sets where short_name = 'a25'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Horror of the Broken Lands'),
    (select id from sets where short_name = 'a25'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curiosity'),
    (select id from sets where short_name = 'a25'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Colossal Dreadmaw'),
    (select id from sets where short_name = 'a25'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balduvian Horde'),
    (select id from sets where short_name = 'a25'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hanna, Ship''s Navigator'),
    (select id from sets where short_name = 'a25'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ihsan''s Shade'),
    (select id from sets where short_name = 'a25'),
    '94',
    'uncommon'
) 
 on conflict do nothing;
