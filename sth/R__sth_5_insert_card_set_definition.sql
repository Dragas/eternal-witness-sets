insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Gliding Licid'),
    (select id from sets where short_name = 'sth'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silver Wyvern'),
    (select id from sets where short_name = 'sth'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brush with Death'),
    (select id from sets where short_name = 'sth'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crystalline Sliver'),
    (select id from sets where short_name = 'sth'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leap'),
    (select id from sets where short_name = 'sth'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mask of the Mimic'),
    (select id from sets where short_name = 'sth'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Razors'),
    (select id from sets where short_name = 'sth'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Revenant'),
    (select id from sets where short_name = 'sth'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heartstone'),
    (select id from sets where short_name = 'sth'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spined Wurm'),
    (select id from sets where short_name = 'sth'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dream Prowler'),
    (select id from sets where short_name = 'sth'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smite'),
    (select id from sets where short_name = 'sth'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Calming Licid'),
    (select id from sets where short_name = 'sth'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mogg Bombers'),
    (select id from sets where short_name = 'sth'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warrior en-Kor'),
    (select id from sets where short_name = 'sth'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tortured Existence'),
    (select id from sets where short_name = 'sth'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conviction'),
    (select id from sets where short_name = 'sth'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mogg Flunkies'),
    (select id from sets where short_name = 'sth'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jinxed Ring'),
    (select id from sets where short_name = 'sth'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evacuation'),
    (select id from sets where short_name = 'sth'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scapegoat'),
    (select id from sets where short_name = 'sth'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reins of Power'),
    (select id from sets where short_name = 'sth'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sliver Queen'),
    (select id from sets where short_name = 'sth'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volrath''s Laboratory'),
    (select id from sets where short_name = 'sth'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Morgue Thrull'),
    (select id from sets where short_name = 'sth'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spike Worker'),
    (select id from sets where short_name = 'sth'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ensnaring Bridge'),
    (select id from sets where short_name = 'sth'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mox Diamond'),
    (select id from sets where short_name = 'sth'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crossbow Ambush'),
    (select id from sets where short_name = 'sth'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Endangered Armodon'),
    (select id from sets where short_name = 'sth'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Acidic Sliver'),
    (select id from sets where short_name = 'sth'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Essence'),
    (select id from sets where short_name = 'sth'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lab Rats'),
    (select id from sets where short_name = 'sth'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Honor Guard'),
    (select id from sets where short_name = 'sth'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spike Feeder'),
    (select id from sets where short_name = 'sth'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sacred Ground'),
    (select id from sets where short_name = 'sth'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spitting Hydra'),
    (select id from sets where short_name = 'sth'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Samite Blessing'),
    (select id from sets where short_name = 'sth'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hermit Druid'),
    (select id from sets where short_name = 'sth'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword of the Chosen'),
    (select id from sets where short_name = 'sth'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volrath''s Stronghold'),
    (select id from sets where short_name = 'sth'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lowland Basilisk'),
    (select id from sets where short_name = 'sth'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Tears'),
    (select id from sets where short_name = 'sth'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Archer'),
    (select id from sets where short_name = 'sth'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shifting Wall'),
    (select id from sets where short_name = 'sth'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mulch'),
    (select id from sets where short_name = 'sth'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Games'),
    (select id from sets where short_name = 'sth'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Megrim'),
    (select id from sets where short_name = 'sth'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hornet Cannon'),
    (select id from sets where short_name = 'sth'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skeleton Scavengers'),
    (select id from sets where short_name = 'sth'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Awakening'),
    (select id from sets where short_name = 'sth'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mogg Infestation'),
    (select id from sets where short_name = 'sth'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hidden Retreat'),
    (select id from sets where short_name = 'sth'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Amok'),
    (select id from sets where short_name = 'sth'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Victual Sliver'),
    (select id from sets where short_name = 'sth'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stronghold Taskmaster'),
    (select id from sets where short_name = 'sth'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flowstone Hellion'),
    (select id from sets where short_name = 'sth'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dungeon Shade'),
    (select id from sets where short_name = 'sth'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fling'),
    (select id from sets where short_name = 'sth'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dauthi Trapper'),
    (select id from sets where short_name = 'sth'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flowstone Shambler'),
    (select id from sets where short_name = 'sth'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Venerable Monk'),
    (select id from sets where short_name = 'sth'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Verdant Touch'),
    (select id from sets where short_name = 'sth'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death Stroke'),
    (select id from sets where short_name = 'sth'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fanning the Flames'),
    (select id from sets where short_name = 'sth'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bandage'),
    (select id from sets where short_name = 'sth'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volrath''s Gardens'),
    (select id from sets where short_name = 'sth'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Intruder Alarm'),
    (select id from sets where short_name = 'sth'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Blossoms'),
    (select id from sets where short_name = 'sth'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warrior Angel'),
    (select id from sets where short_name = 'sth'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Contemplation'),
    (select id from sets where short_name = 'sth'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hammerhead Shark'),
    (select id from sets where short_name = 'sth'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mortuary'),
    (select id from sets where short_name = 'sth'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tidal Surge'),
    (select id from sets where short_name = 'sth'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crovax the Cursed'),
    (select id from sets where short_name = 'sth'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heat of Battle'),
    (select id from sets where short_name = 'sth'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duct Crawler'),
    (select id from sets where short_name = 'sth'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bottomless Pit'),
    (select id from sets where short_name = 'sth'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Peel'),
    (select id from sets where short_name = 'sth'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cloud Spirit'),
    (select id from sets where short_name = 'sth'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grave Pact'),
    (select id from sets where short_name = 'sth'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hibernation Sliver'),
    (select id from sets where short_name = 'sth'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Horn of Greed'),
    (select id from sets where short_name = 'sth'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Constant Mists'),
    (select id from sets where short_name = 'sth'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spined Sliver'),
    (select id from sets where short_name = 'sth'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soltari Champion'),
    (select id from sets where short_name = 'sth'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flowstone Mauler'),
    (select id from sets where short_name = 'sth'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Portcullis'),
    (select id from sets where short_name = 'sth'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spike Colony'),
    (select id from sets where short_name = 'sth'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shaman en-Kor'),
    (select id from sets where short_name = 'sth'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spike Soldier'),
    (select id from sets where short_name = 'sth'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serpent Warrior'),
    (select id from sets where short_name = 'sth'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tidal Warrior'),
    (select id from sets where short_name = 'sth'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flowstone Blade'),
    (select id from sets where short_name = 'sth'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Leak'),
    (select id from sets where short_name = 'sth'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carnassid'),
    (select id from sets where short_name = 'sth'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thalakos Deceiver'),
    (select id from sets where short_name = 'sth'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Change of Heart'),
    (select id from sets where short_name = 'sth'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Falcon'),
    (select id from sets where short_name = 'sth'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nomads en-Kor'),
    (select id from sets where short_name = 'sth'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ransack'),
    (select id from sets where short_name = 'sth'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Furnace Spirit'),
    (select id from sets where short_name = 'sth'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mob Justice'),
    (select id from sets where short_name = 'sth'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rebound'),
    (select id from sets where short_name = 'sth'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overgrowth'),
    (select id from sets where short_name = 'sth'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Souls'),
    (select id from sets where short_name = 'sth'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Walking Dream'),
    (select id from sets where short_name = 'sth'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sift'),
    (select id from sets where short_name = 'sth'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cannibalize'),
    (select id from sets where short_name = 'sth'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corrupting Licid'),
    (select id from sets where short_name = 'sth'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Contempt'),
    (select id from sets where short_name = 'sth'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spindrift Drake'),
    (select id from sets where short_name = 'sth'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stronghold Assassin'),
    (select id from sets where short_name = 'sth'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruination'),
    (select id from sets where short_name = 'sth'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Troopers'),
    (select id from sets where short_name = 'sth'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Convulsing Licid'),
    (select id from sets where short_name = 'sth'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tempting Licid'),
    (select id from sets where short_name = 'sth'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elven Rite'),
    (select id from sets where short_name = 'sth'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rolling Stones'),
    (select id from sets where short_name = 'sth'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mogg Maniac'),
    (select id from sets where short_name = 'sth'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flame Wave'),
    (select id from sets where short_name = 'sth'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Foul Imp'),
    (select id from sets where short_name = 'sth'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hesitation'),
    (select id from sets where short_name = 'sth'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Temper'),
    (select id from sets where short_name = 'sth'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seething Anger'),
    (select id from sets where short_name = 'sth'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primal Rage'),
    (select id from sets where short_name = 'sth'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lancers en-Kor'),
    (select id from sets where short_name = 'sth'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Craven Giant'),
    (select id from sets where short_name = 'sth'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pursuit of Knowledge'),
    (select id from sets where short_name = 'sth'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mindwarper'),
    (select id from sets where short_name = 'sth'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volrath''s Shapeshifter'),
    (select id from sets where short_name = 'sth'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rabid Rats'),
    (select id from sets where short_name = 'sth'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Invasion Plans'),
    (select id from sets where short_name = 'sth'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spike Breeder'),
    (select id from sets where short_name = 'sth'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spirit en-Kor'),
    (select id from sets where short_name = 'sth'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'sth'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Provoke'),
    (select id from sets where short_name = 'sth'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burgeoning'),
    (select id from sets where short_name = 'sth'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Youthful Knight'),
    (select id from sets where short_name = 'sth'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shard Phoenix'),
    (select id from sets where short_name = 'sth'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Torment'),
    (select id from sets where short_name = 'sth'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bullwhip'),
    (select id from sets where short_name = 'sth'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dream Halls'),
    (select id from sets where short_name = 'sth'),
    '28',
    'rare'
) 
 on conflict do nothing;
