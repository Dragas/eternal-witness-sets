insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Season of the Witch'),
    (select id from sets where short_name = 'drk'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Safe Haven'),
    (select id from sets where short_name = 'drk'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Touch'),
    (select id from sets where short_name = 'drk'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Bomb'),
    (select id from sets where short_name = 'drk'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tormod''s Crypt'),
    (select id from sets where short_name = 'drk'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marsh Viper'),
    (select id from sets where short_name = 'drk'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dance of Many'),
    (select id from sets where short_name = 'drk'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Apprentice Wizard'),
    (select id from sets where short_name = 'drk'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exorcist'),
    (select id from sets where short_name = 'drk'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coal Golem'),
    (select id from sets where short_name = 'drk'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Preacher'),
    (select id from sets where short_name = 'drk'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood of the Martyr'),
    (select id from sets where short_name = 'drk'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fountain of Youth'),
    (select id from sets where short_name = 'drk'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wormwood Treefolk'),
    (select id from sets where short_name = 'drk'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'People of the Woods'),
    (select id from sets where short_name = 'drk'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nameless Race'),
    (select id from sets where short_name = 'drk'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Miracle Worker'),
    (select id from sets where short_name = 'drk'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tracker'),
    (select id from sets where short_name = 'drk'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merfolk Assassin'),
    (select id from sets where short_name = 'drk'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Squire'),
    (select id from sets where short_name = 'drk'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savaen Elves'),
    (select id from sets where short_name = 'drk'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Living Armor'),
    (select id from sets where short_name = 'drk'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maze of Ith'),
    (select id from sets where short_name = 'drk'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scarwood Bandits'),
    (select id from sets where short_name = 'drk'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tangle Kelp'),
    (select id from sets where short_name = 'drk'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Witch Hunter'),
    (select id from sets where short_name = 'drk'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Shark'),
    (select id from sets where short_name = 'drk'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scarwood Goblins'),
    (select id from sets where short_name = 'drk'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sisters of the Flame'),
    (select id from sets where short_name = 'drk'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Book of Rass'),
    (select id from sets where short_name = 'drk'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inquisition'),
    (select id from sets where short_name = 'drk'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erosion'),
    (select id from sets where short_name = 'drk'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bone Flute'),
    (select id from sets where short_name = 'drk'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tower of Coireall'),
    (select id from sets where short_name = 'drk'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orc General'),
    (select id from sets where short_name = 'drk'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Worms of the Earth'),
    (select id from sets where short_name = 'drk'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Banshee'),
    (select id from sets where short_name = 'drk'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barl''s Cage'),
    (select id from sets where short_name = 'drk'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carnivorous Plant'),
    (select id from sets where short_name = 'drk'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Standing Stones'),
    (select id from sets where short_name = 'drk'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brainwash'),
    (select id from sets where short_name = 'drk'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sorrow''s Path'),
    (select id from sets where short_name = 'drk'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spitting Slug'),
    (select id from sets where short_name = 'drk'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Hero'),
    (select id from sets where short_name = 'drk'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cave People'),
    (select id from sets where short_name = 'drk'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Clash'),
    (select id from sets where short_name = 'drk'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Sphere'),
    (select id from sets where short_name = 'drk'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Runesword'),
    (select id from sets where short_name = 'drk'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'City of Shadows'),
    (select id from sets where short_name = 'drk'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Moon'),
    (select id from sets where short_name = 'drk'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wand of Ith'),
    (select id from sets where short_name = 'drk'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashes to Ashes'),
    (select id from sets where short_name = 'drk'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Uncle Istvan'),
    (select id from sets where short_name = 'drk'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necropolis'),
    (select id from sets where short_name = 'drk'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Digging Team'),
    (select id from sets where short_name = 'drk'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marsh Goblins'),
    (select id from sets where short_name = 'drk'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tivadar''s Crusade'),
    (select id from sets where short_name = 'drk'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fasting'),
    (select id from sets where short_name = 'drk'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scavenger Folk'),
    (select id from sets where short_name = 'drk'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frankenstein''s Monster'),
    (select id from sets where short_name = 'drk'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'War Barge'),
    (select id from sets where short_name = 'drk'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Rock Sled'),
    (select id from sets where short_name = 'drk'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scarecrow'),
    (select id from sets where short_name = 'drk'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drowned'),
    (select id from sets where short_name = 'drk'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Wizard'),
    (select id from sets where short_name = 'drk'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Niall Silvain'),
    (select id from sets where short_name = 'drk'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deep Water'),
    (select id from sets where short_name = 'drk'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angry Mob'),
    (select id from sets where short_name = 'drk'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Curse Artifact'),
    (select id from sets where short_name = 'drk'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grave Robbers'),
    (select id from sets where short_name = 'drk'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Murk Dwellers'),
    (select id from sets where short_name = 'drk'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Calendar'),
    (select id from sets where short_name = 'drk'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inferno'),
    (select id from sets where short_name = 'drk'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skull of Orm'),
    (select id from sets where short_name = 'drk'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fissure'),
    (select id from sets where short_name = 'drk'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scarwood Hag'),
    (select id from sets where short_name = 'drk'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dust to Dust'),
    (select id from sets where short_name = 'drk'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knights of Thorn'),
    (select id from sets where short_name = 'drk'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Riptide'),
    (select id from sets where short_name = 'drk'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lurker'),
    (select id from sets where short_name = 'drk'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leviathan'),
    (select id from sets where short_name = 'drk'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Touch'),
    (select id from sets where short_name = 'drk'),
    '77†',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marsh Gas'),
    (select id from sets where short_name = 'drk'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Electric Eel'),
    (select id from sets where short_name = 'drk'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brothers of Fire'),
    (select id from sets where short_name = 'drk'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Venom'),
    (select id from sets where short_name = 'drk'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runesword'),
    (select id from sets where short_name = 'drk'),
    '107†',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pikemen'),
    (select id from sets where short_name = 'drk'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ball Lightning'),
    (select id from sets where short_name = 'drk'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diabolic Machine'),
    (select id from sets where short_name = 'drk'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Holy Light'),
    (select id from sets where short_name = 'drk'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Morale'),
    (select id from sets where short_name = 'drk'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Caves'),
    (select id from sets where short_name = 'drk'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hidden Path'),
    (select id from sets where short_name = 'drk'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Shrine'),
    (select id from sets where short_name = 'drk'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Drake'),
    (select id from sets where short_name = 'drk'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fire and Brimstone'),
    (select id from sets where short_name = 'drk'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reflecting Mirror'),
    (select id from sets where short_name = 'drk'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fountain of Youth'),
    (select id from sets where short_name = 'drk'),
    '103†',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eternal Flame'),
    (select id from sets where short_name = 'drk'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bog Rats'),
    (select id from sets where short_name = 'drk'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eater of the Dead'),
    (select id from sets where short_name = 'drk'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghost Ship'),
    (select id from sets where short_name = 'drk'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fellwar Stone'),
    (select id from sets where short_name = 'drk'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Amnesia'),
    (select id from sets where short_name = 'drk'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Martyr''s Cry'),
    (select id from sets where short_name = 'drk'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bog Imp'),
    (select id from sets where short_name = 'drk'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Heart of the Wood'),
    (select id from sets where short_name = 'drk'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Water Wurm'),
    (select id from sets where short_name = 'drk'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whippoorwill'),
    (select id from sets where short_name = 'drk'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Festival'),
    (select id from sets where short_name = 'drk'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Word of Binding'),
    (select id from sets where short_name = 'drk'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunken City'),
    (select id from sets where short_name = 'drk'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elves of Deep Shadow'),
    (select id from sets where short_name = 'drk'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Fallen'),
    (select id from sets where short_name = 'drk'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rag Man'),
    (select id from sets where short_name = 'drk'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Vortex'),
    (select id from sets where short_name = 'drk'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flood'),
    (select id from sets where short_name = 'drk'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cleansing'),
    (select id from sets where short_name = 'drk'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblins of the Flarg'),
    (select id from sets where short_name = 'drk'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychic Allergy'),
    (select id from sets where short_name = 'drk'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Land Leeches'),
    (select id from sets where short_name = 'drk'),
    '79',
    'common'
) 
 on conflict do nothing;
