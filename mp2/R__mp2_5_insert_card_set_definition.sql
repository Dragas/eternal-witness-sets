insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Divert'),
    (select id from sets where short_name = 'mp2'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Force of Will'),
    (select id from sets where short_name = 'mp2'),
    '14',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Opposition'),
    (select id from sets where short_name = 'mp2'),
    '35',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Doomsday'),
    (select id from sets where short_name = 'mp2'),
    '42',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Capsize'),
    (select id from sets where short_name = 'mp2'),
    '32',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forbid'),
    (select id from sets where short_name = 'mp2'),
    '33',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chain Lightning'),
    (select id from sets where short_name = 'mp2'),
    '26',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'mp2'),
    '21',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sunder'),
    (select id from sets where short_name = 'mp2'),
    '36',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'mp2'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Avatar of Woe'),
    (select id from sets where short_name = 'mp2'),
    '38',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pact of Negation'),
    (select id from sets where short_name = 'mp2'),
    '16',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Counterbalance'),
    (select id from sets where short_name = 'mp2'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'mp2'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rhonas the Indomitable'),
    (select id from sets where short_name = 'mp2'),
    '28',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'The Scarab God'),
    (select id from sets where short_name = 'mp2'),
    '53',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Entomb'),
    (select id from sets where short_name = 'mp2'),
    '23',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blood Moon'),
    (select id from sets where short_name = 'mp2'),
    '46',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bontu the Glorified'),
    (select id from sets where short_name = 'mp2'),
    '20',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aven Mindcensor'),
    (select id from sets where short_name = 'mp2'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Omniscience'),
    (select id from sets where short_name = 'mp2'),
    '34',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mind Twist'),
    (select id from sets where short_name = 'mp2'),
    '24',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = 'mp2'),
    '31',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Diabolic Intent'),
    (select id from sets where short_name = 'mp2'),
    '22',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aggravated Assault'),
    (select id from sets where short_name = 'mp2'),
    '25',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Loyal Retainers'),
    (select id from sets where short_name = 'mp2'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Through the Breach'),
    (select id from sets where short_name = 'mp2'),
    '49',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Desolation Angel'),
    (select id from sets where short_name = 'mp2'),
    '40',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shatterstorm'),
    (select id from sets where short_name = 'mp2'),
    '48',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Attrition'),
    (select id from sets where short_name = 'mp2'),
    '19',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'No Mercy'),
    (select id from sets where short_name = 'mp2'),
    '43',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vindicate'),
    (select id from sets where short_name = 'mp2'),
    '30',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cryptic Command'),
    (select id from sets where short_name = 'mp2'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Daze'),
    (select id from sets where short_name = 'mp2'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Oketra the True'),
    (select id from sets where short_name = 'mp2'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Slaughter Pact'),
    (select id from sets where short_name = 'mp2'),
    '44',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Containment Priest'),
    (select id from sets where short_name = 'mp2'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lord of Extinction'),
    (select id from sets where short_name = 'mp2'),
    '52',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Damnation'),
    (select id from sets where short_name = 'mp2'),
    '39',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'The Locust God'),
    (select id from sets where short_name = 'mp2'),
    '51',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Worship'),
    (select id from sets where short_name = 'mp2'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Threads of Disloyalty'),
    (select id from sets where short_name = 'mp2'),
    '37',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Consecrated Sphinx'),
    (select id from sets where short_name = 'mp2'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Austere Command'),
    (select id from sets where short_name = 'mp2'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Pulse'),
    (select id from sets where short_name = 'mp2'),
    '29',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stifle'),
    (select id from sets where short_name = 'mp2'),
    '18',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thoughtseize'),
    (select id from sets where short_name = 'mp2'),
    '45',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Spell Pierce'),
    (select id from sets where short_name = 'mp2'),
    '17',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kefnet the Mindful'),
    (select id from sets where short_name = 'mp2'),
    '15',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'The Scorpion God'),
    (select id from sets where short_name = 'mp2'),
    '54',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Choke'),
    (select id from sets where short_name = 'mp2'),
    '50',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hazoret the Fervent'),
    (select id from sets where short_name = 'mp2'),
    '27',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Diabolic Edict'),
    (select id from sets where short_name = 'mp2'),
    '41',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Boil'),
    (select id from sets where short_name = 'mp2'),
    '47',
    'mythic'
) 
 on conflict do nothing;
