insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Jan Tomcani Bio'),
    (select id from sets where short_name = 'wc01'),
    'jt0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc01'),
    'jt348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kavu Chameleon'),
    (select id from sets where short_name = 'wc01'),
    'jt191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fire // Ice'),
    (select id from sets where short_name = 'wc01'),
    'ar128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prodigal Sorcerer'),
    (select id from sets where short_name = 'wc01'),
    'ab94sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Persecute'),
    (select id from sets where short_name = 'wc01'),
    'tvdl154sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sulfurous Springs'),
    (select id from sets where short_name = 'wc01'),
    'tvdl345',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karplusan Forest'),
    (select id from sets where short_name = 'wc01'),
    'jt336',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc01'),
    'ar336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'wc01'),
    'ab69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc01'),
    'jt349a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc01'),
    'ab335a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc01'),
    'ab336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc01'),
    'jt343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sulfurous Springs'),
    (select id from sets where short_name = 'wc01'),
    'jt345',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vendetta'),
    (select id from sets where short_name = 'wc01'),
    'tvdl170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc01'),
    'ab338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fires of Yavimaya'),
    (select id from sets where short_name = 'wc01'),
    'jt246',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc01'),
    'tvdl341',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc01'),
    'ab336a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc01'),
    'jt348a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plague Spitter'),
    (select id from sets where short_name = 'wc01'),
    'tvdl119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crypt Angel'),
    (select id from sets where short_name = 'wc01'),
    'tvdl97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Maze'),
    (select id from sets where short_name = 'wc01'),
    'ab59sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nether Spirit'),
    (select id from sets where short_name = 'wc01'),
    'ar149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc01'),
    'jt328',
    'common'
) ,
(
    (select id from mtgcard where name = '2001 World Championships Ad'),
    (select id from sets where short_name = 'wc01'),
    '0',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Rage'),
    (select id from sets where short_name = 'wc01'),
    'jt178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc01'),
    'ab335b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyroclasm'),
    (select id from sets where short_name = 'wc01'),
    'tvdl209sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alex Borteh Bio'),
    (select id from sets where short_name = 'wc01'),
    'ab0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opposition'),
    (select id from sets where short_name = 'wc01'),
    'ab92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tsabo''s Assassin'),
    (select id from sets where short_name = 'wc01'),
    'ar128sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Salt Marsh'),
    (select id from sets where short_name = 'wc01'),
    'ar326',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'wc01'),
    'ar67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc01'),
    'tvdl348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terminate'),
    (select id from sets where short_name = 'wc01'),
    'tvdl128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lobotomy'),
    (select id from sets where short_name = 'wc01'),
    'ar255sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merfolk of the Pearl Trident'),
    (select id from sets where short_name = 'wc01'),
    'ab90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc01'),
    'tvdl339a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc01'),
    'ar337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc01'),
    'ar335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Misdirection'),
    (select id from sets where short_name = 'wc01'),
    'ab87sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Scuta'),
    (select id from sets where short_name = 'wc01'),
    'tvdl51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'wc01'),
    'ab67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urborg Volcano'),
    (select id from sets where short_name = 'wc01'),
    'tvdl330',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sulfurous Springs'),
    (select id from sets where short_name = 'wc01'),
    'ar345',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu'),
    (select id from sets where short_name = 'wc01'),
    'tvdl60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc01'),
    'ab333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu'),
    (select id from sets where short_name = 'wc01'),
    'jt60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rishadan Port'),
    (select id from sets where short_name = 'wc01'),
    'tvdl324',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wash Out'),
    (select id from sets where short_name = 'wc01'),
    'ab87bsb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc01'),
    'jt343a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undermine'),
    (select id from sets where short_name = 'wc01'),
    'ar282',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tom van de Logt Bio (2001)'),
    (select id from sets where short_name = 'wc01'),
    'tvdl0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rishadan Port'),
    (select id from sets where short_name = 'wc01'),
    'jt324',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thunderscape Battlemage'),
    (select id from sets where short_name = 'wc01'),
    'jt75sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merfolk Looter'),
    (select id from sets where short_name = 'wc01'),
    'ab89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Opt'),
    (select id from sets where short_name = 'wc01'),
    'ar64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc01'),
    'ab334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc01'),
    'ar338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Wastes'),
    (select id from sets where short_name = 'wc01'),
    'jt141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc01'),
    'tvdl347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tsabo''s Web'),
    (select id from sets where short_name = 'wc01'),
    'ar317',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Obliterate'),
    (select id from sets where short_name = 'wc01'),
    'jt156sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = 'wc01'),
    'jt231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Antoine Ruel Decklist'),
    (select id from sets where short_name = 'wc01'),
    'ar0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scoria Cat'),
    (select id from sets where short_name = 'wc01'),
    'tvdl101sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc01'),
    'jt329',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blastoderm'),
    (select id from sets where short_name = 'wc01'),
    'jt102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Underground River'),
    (select id from sets where short_name = 'wc01'),
    'ar350',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc01'),
    'tvdl343b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc01'),
    'tvdl337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc01'),
    'jt337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire // Ice'),
    (select id from sets where short_name = 'wc01'),
    'jt128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Static Orb'),
    (select id from sets where short_name = 'wc01'),
    'ab319',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crosis''s Charm'),
    (select id from sets where short_name = 'wc01'),
    'ar99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thornscape Battlemage'),
    (select id from sets where short_name = 'wc01'),
    'jt94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc01'),
    'ar336a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urborg Volcano'),
    (select id from sets where short_name = 'wc01'),
    'ar330',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc01'),
    'jt349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc01'),
    'jt347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vodalian Merchant'),
    (select id from sets where short_name = 'wc01'),
    'ab85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lord of Atlantis'),
    (select id from sets where short_name = 'wc01'),
    'ab83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saproling Burst'),
    (select id from sets where short_name = 'wc01'),
    'jt113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alex Borteh Decklist'),
    (select id from sets where short_name = 'wc01'),
    'ab0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Reef'),
    (select id from sets where short_name = 'wc01'),
    'ar142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darting Merfolk'),
    (select id from sets where short_name = 'wc01'),
    'ab72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tom van de Logt Decklist (2001)'),
    (select id from sets where short_name = 'wc01'),
    'tvdl0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Response'),
    (select id from sets where short_name = 'wc01'),
    'ab78sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Arena'),
    (select id from sets where short_name = 'wc01'),
    'tvdl47sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc01'),
    'ab337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc01'),
    'ar334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc01'),
    'ab337a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc01'),
    'ab332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyroclasm'),
    (select id from sets where short_name = 'wc01'),
    'ar209sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blazing Specter'),
    (select id from sets where short_name = 'wc01'),
    'tvdl236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Response'),
    (select id from sets where short_name = 'wc01'),
    'ar78sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skizzik'),
    (select id from sets where short_name = 'wc01'),
    'tvdl169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rushing River'),
    (select id from sets where short_name = 'wc01'),
    'ab30sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Rage'),
    (select id from sets where short_name = 'wc01'),
    'tvdl178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'wc01'),
    'jt327',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'wc01'),
    'ar131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gush'),
    (select id from sets where short_name = 'wc01'),
    'ab82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Oath'),
    (select id from sets where short_name = 'wc01'),
    'jt177sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu'),
    (select id from sets where short_name = 'wc01'),
    'tvdl60sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'wc01'),
    'ar131sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc01'),
    'tvdl343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spiritmonger'),
    (select id from sets where short_name = 'wc01'),
    'jt121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fact or Fiction'),
    (select id from sets where short_name = 'wc01'),
    'ar57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Rage'),
    (select id from sets where short_name = 'wc01'),
    'ar178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc01'),
    'tvdl342',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'wc01'),
    'jt253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crypt Angel'),
    (select id from sets where short_name = 'wc01'),
    'tvdl97sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blank Card'),
    (select id from sets where short_name = 'wc01'),
    '00',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tangle'),
    (select id from sets where short_name = 'wc01'),
    'jt213sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'wc01'),
    'tvdl131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Accumulated Knowledge'),
    (select id from sets where short_name = 'wc01'),
    'ar26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hibernation'),
    (select id from sets where short_name = 'wc01'),
    'ab79sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Addle'),
    (select id from sets where short_name = 'wc01'),
    'tvdl91sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kavu Chameleon'),
    (select id from sets where short_name = 'wc01'),
    'jt191sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc01'),
    'ar335b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc01'),
    'ab338a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jan Tomcani Decklist'),
    (select id from sets where short_name = 'wc01'),
    'jt0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Engineered Plague'),
    (select id from sets where short_name = 'wc01'),
    'ar133sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spite // Malice'),
    (select id from sets where short_name = 'wc01'),
    'ar293',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Waterfront Bouncer'),
    (select id from sets where short_name = 'wc01'),
    'ab114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tsabo''s Decree'),
    (select id from sets where short_name = 'wc01'),
    'ar129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'wc01'),
    'ar69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc01'),
    'jt347a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thwart'),
    (select id from sets where short_name = 'wc01'),
    'ab108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc01'),
    'ab335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meekstone'),
    (select id from sets where short_name = 'wc01'),
    'ar307sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc01'),
    'ar335a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'wc01'),
    'tvdl129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boil'),
    (select id from sets where short_name = 'wc01'),
    'jt177sba',
    'common'
) ,
(
    (select id from mtgcard where name = 'Antoine Ruel Bio'),
    (select id from sets where short_name = 'wc01'),
    'ar0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc01'),
    'tvdl339',
    'common'
) 
 on conflict do nothing;
