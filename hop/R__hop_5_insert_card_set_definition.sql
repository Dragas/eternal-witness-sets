insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Relentless Assault'),
    (select id from sets where short_name = 'hop'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Undead Warchief'),
    (select id from sets where short_name = 'hop'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Swiftblade'),
    (select id from sets where short_name = 'hop'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'hop'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Ghoul'),
    (select id from sets where short_name = 'hop'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcbound Crusher'),
    (select id from sets where short_name = 'hop'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Explosive Vegetation'),
    (select id from sets where short_name = 'hop'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rorix Bladewing'),
    (select id from sets where short_name = 'hop'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'hop'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serum Tank'),
    (select id from sets where short_name = 'hop'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'hop'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Copper Myr'),
    (select id from sets where short_name = 'hop'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Charge'),
    (select id from sets where short_name = 'hop'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relic of Progenitus'),
    (select id from sets where short_name = 'hop'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cerodon Yearling'),
    (select id from sets where short_name = 'hop'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seat of the Synod'),
    (select id from sets where short_name = 'hop'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunhome, Fortress of the Legion'),
    (select id from sets where short_name = 'hop'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hull Breach'),
    (select id from sets where short_name = 'hop'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Door to Nothingness'),
    (select id from sets where short_name = 'hop'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arsenal Thresher'),
    (select id from sets where short_name = 'hop'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cabal Coffers'),
    (select id from sets where short_name = 'hop'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savage Twister'),
    (select id from sets where short_name = 'hop'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Great Furnace'),
    (select id from sets where short_name = 'hop'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Captain''s Maneuver'),
    (select id from sets where short_name = 'hop'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bull Cerodon'),
    (select id from sets where short_name = 'hop'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gold Myr'),
    (select id from sets where short_name = 'hop'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fertilid'),
    (select id from sets where short_name = 'hop'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ascendant Evincar'),
    (select id from sets where short_name = 'hop'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Withered Wretch'),
    (select id from sets where short_name = 'hop'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'hop'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flamekin Harbinger'),
    (select id from sets where short_name = 'hop'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Broodstar'),
    (select id from sets where short_name = 'hop'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nefashu'),
    (select id from sets where short_name = 'hop'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kor Sanctifiers'),
    (select id from sets where short_name = 'hop'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'hop'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'hop'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fertile Ground'),
    (select id from sets where short_name = 'hop'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rotting Rats'),
    (select id from sets where short_name = 'hop'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corpse Harvester'),
    (select id from sets where short_name = 'hop'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Consume Spirit'),
    (select id from sets where short_name = 'hop'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leechridden Swamp'),
    (select id from sets where short_name = 'hop'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fires of Yavimaya'),
    (select id from sets where short_name = 'hop'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Arena'),
    (select id from sets where short_name = 'hop'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Briarhorn'),
    (select id from sets where short_name = 'hop'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soulless One'),
    (select id from sets where short_name = 'hop'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mage Slayer'),
    (select id from sets where short_name = 'hop'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Signet'),
    (select id from sets where short_name = 'hop'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fabricate'),
    (select id from sets where short_name = 'hop'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyrotechnics'),
    (select id from sets where short_name = 'hop'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcbound Slith'),
    (select id from sets where short_name = 'hop'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Double Cleave'),
    (select id from sets where short_name = 'hop'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whiplash Trap'),
    (select id from sets where short_name = 'hop'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Suntouched Myr'),
    (select id from sets where short_name = 'hop'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Helldozer'),
    (select id from sets where short_name = 'hop'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Syphon Mind'),
    (select id from sets where short_name = 'hop'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'hop'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lodestone Myr'),
    (select id from sets where short_name = 'hop'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Living Hive'),
    (select id from sets where short_name = 'hop'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Insurrection'),
    (select id from sets where short_name = 'hop'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Order // Chaos'),
    (select id from sets where short_name = 'hop'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cinder Elemental'),
    (select id from sets where short_name = 'hop'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Branching Bolt'),
    (select id from sets where short_name = 'hop'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forgotten Ancient'),
    (select id from sets where short_name = 'hop'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = 'hop'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darksteel Forge'),
    (select id from sets where short_name = 'hop'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'hop'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'hop'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Congregate'),
    (select id from sets where short_name = 'hop'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mask of Memory'),
    (select id from sets where short_name = 'hop'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bogardan Firefiend'),
    (select id from sets where short_name = 'hop'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blaze'),
    (select id from sets where short_name = 'hop'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bosh, Iron Golem'),
    (select id from sets where short_name = 'hop'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vault of Whispers'),
    (select id from sets where short_name = 'hop'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancient Den'),
    (select id from sets where short_name = 'hop'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cadaverous Knight'),
    (select id from sets where short_name = 'hop'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shepherd of Rot'),
    (select id from sets where short_name = 'hop'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cranial Plating'),
    (select id from sets where short_name = 'hop'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bogardan Rager'),
    (select id from sets where short_name = 'hop'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balefire Liege'),
    (select id from sets where short_name = 'hop'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'hop'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grave Pact'),
    (select id from sets where short_name = 'hop'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noxious Ghoul'),
    (select id from sets where short_name = 'hop'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'hop'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Razia, Boros Archangel'),
    (select id from sets where short_name = 'hop'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'hop'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cruel Revival'),
    (select id from sets where short_name = 'hop'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Festering Goblin'),
    (select id from sets where short_name = 'hop'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dregscape Zombie'),
    (select id from sets where short_name = 'hop'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sludge Strider'),
    (select id from sets where short_name = 'hop'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pentad Prism'),
    (select id from sets where short_name = 'hop'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Menacing Ogre'),
    (select id from sets where short_name = 'hop'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Qumulox'),
    (select id from sets where short_name = 'hop'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Innocent Blood'),
    (select id from sets where short_name = 'hop'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'hop'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'hop'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'hop'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Profane Command'),
    (select id from sets where short_name = 'hop'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myr Enforcer'),
    (select id from sets where short_name = 'hop'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rumbling Slum'),
    (select id from sets where short_name = 'hop'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'hop'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'hop'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tornado Elemental'),
    (select id from sets where short_name = 'hop'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hearthfire Hobgoblin'),
    (select id from sets where short_name = 'hop'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'hop'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duergar Hedge-Mage'),
    (select id from sets where short_name = 'hop'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tree of Tales'),
    (select id from sets where short_name = 'hop'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boros Garrison'),
    (select id from sets where short_name = 'hop'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rolling Thunder'),
    (select id from sets where short_name = 'hop'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beacon of Unrest'),
    (select id from sets where short_name = 'hop'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Warden'),
    (select id from sets where short_name = 'hop'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iron Myr'),
    (select id from sets where short_name = 'hop'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loxodon Warhammer'),
    (select id from sets where short_name = 'hop'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hideous End'),
    (select id from sets where short_name = 'hop'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'hop'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Offensive'),
    (select id from sets where short_name = 'hop'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Guildmage'),
    (select id from sets where short_name = 'hop'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu'),
    (select id from sets where short_name = 'hop'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thirst for Knowledge'),
    (select id from sets where short_name = 'hop'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Akroma''s Vengeance'),
    (select id from sets where short_name = 'hop'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verdant Force'),
    (select id from sets where short_name = 'hop'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Search for Tomorrow'),
    (select id from sets where short_name = 'hop'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nuisance Engine'),
    (select id from sets where short_name = 'hop'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Incremental Blight'),
    (select id from sets where short_name = 'hop'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Etched Oracle'),
    (select id from sets where short_name = 'hop'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'hop'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'hop'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Syphon Soul'),
    (select id from sets where short_name = 'hop'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battlegate Mimic'),
    (select id from sets where short_name = 'hop'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'hop'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Browbeat'),
    (select id from sets where short_name = 'hop'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'hop'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cone of Flame'),
    (select id from sets where short_name = 'hop'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Master of Etherium'),
    (select id from sets where short_name = 'hop'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'hop'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smokebraider'),
    (select id from sets where short_name = 'hop'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'hop'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'hop'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rockslide Elemental'),
    (select id from sets where short_name = 'hop'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arc Lightning'),
    (select id from sets where short_name = 'hop'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Furnace of Rath'),
    (select id from sets where short_name = 'hop'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beast Hunt'),
    (select id from sets where short_name = 'hop'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orim''s Thunder'),
    (select id from sets where short_name = 'hop'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Taurean Mauler'),
    (select id from sets where short_name = 'hop'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'hop'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silver Myr'),
    (select id from sets where short_name = 'hop'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sarcomite Myr'),
    (select id from sets where short_name = 'hop'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'hop'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pentavus'),
    (select id from sets where short_name = 'hop'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keep Watch'),
    (select id from sets where short_name = 'hop'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'hop'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Baron'),
    (select id from sets where short_name = 'hop'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Helix'),
    (select id from sets where short_name = 'hop'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leaden Myr'),
    (select id from sets where short_name = 'hop'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ivy Elemental'),
    (select id from sets where short_name = 'hop'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glory of Warfare'),
    (select id from sets where short_name = 'hop'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wizard Replica'),
    (select id from sets where short_name = 'hop'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skeleton Shard'),
    (select id from sets where short_name = 'hop'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silverglade Elemental'),
    (select id from sets where short_name = 'hop'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prison Term'),
    (select id from sets where short_name = 'hop'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tribal Unity'),
    (select id from sets where short_name = 'hop'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Oasis'),
    (select id from sets where short_name = 'hop'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beseech the Queen'),
    (select id from sets where short_name = 'hop'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gruul Turf'),
    (select id from sets where short_name = 'hop'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oblivion Ring'),
    (select id from sets where short_name = 'hop'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'hop'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'hop'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keldon Champion'),
    (select id from sets where short_name = 'hop'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Assault // Battery'),
    (select id from sets where short_name = 'hop'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vedalken Engineer'),
    (select id from sets where short_name = 'hop'),
    '15',
    'common'
) 
 on conflict do nothing;
