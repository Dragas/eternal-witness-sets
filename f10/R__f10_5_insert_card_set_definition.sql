insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Gatekeeper of Malakir'),
    (select id from sets where short_name = 'f10'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghostly Prison'),
    (select id from sets where short_name = 'f10'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rift Bolt'),
    (select id from sets where short_name = 'f10'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Visionary'),
    (select id from sets where short_name = 'f10'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloudpost'),
    (select id from sets where short_name = 'f10'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Ziggurat'),
    (select id from sets where short_name = 'f10'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tidehollow Sculler'),
    (select id from sets where short_name = 'f10'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anathemancer'),
    (select id from sets where short_name = 'f10'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Grip'),
    (select id from sets where short_name = 'f10'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wild Nacatl'),
    (select id from sets where short_name = 'f10'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodbraid Elf'),
    (select id from sets where short_name = 'f10'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Qasali Pridemage'),
    (select id from sets where short_name = 'f10'),
    '9',
    'rare'
) 
 on conflict do nothing;
