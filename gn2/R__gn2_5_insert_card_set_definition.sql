insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Relief Captain'),
    (select id from sets where short_name = 'gn2'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Claustrophobia'),
    (select id from sets where short_name = 'gn2'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ranging Raptors'),
    (select id from sets where short_name = 'gn2'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fiend Binder'),
    (select id from sets where short_name = 'gn2'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lord of the Accursed'),
    (select id from sets where short_name = 'gn2'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sphinx of Enlightenment'),
    (select id from sets where short_name = 'gn2'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lightning Strike'),
    (select id from sets where short_name = 'gn2'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zetalpa, Primal Dawn'),
    (select id from sets where short_name = 'gn2'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Calculating Lich'),
    (select id from sets where short_name = 'gn2'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gavony Unhallowed'),
    (select id from sets where short_name = 'gn2'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crow of Dark Tidings'),
    (select id from sets where short_name = 'gn2'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spidery Grasp'),
    (select id from sets where short_name = 'gn2'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zealot of the God-Pharaoh'),
    (select id from sets where short_name = 'gn2'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'gn2'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'gn2'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Wind Mage'),
    (select id from sets where short_name = 'gn2'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dramatic Reversal'),
    (select id from sets where short_name = 'gn2'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'gn2'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crested Herdcaller'),
    (select id from sets where short_name = 'gn2'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Carrion Screecher'),
    (select id from sets where short_name = 'gn2'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'gn2'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghalta, Primal Hunger'),
    (select id from sets where short_name = 'gn2'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tattered Mummy'),
    (select id from sets where short_name = 'gn2'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'River''s Rebuke'),
    (select id from sets where short_name = 'gn2'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lathliss, Dragon Queen'),
    (select id from sets where short_name = 'gn2'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Egg'),
    (select id from sets where short_name = 'gn2'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Howling Golem'),
    (select id from sets where short_name = 'gn2'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Consul''s Lieutenant'),
    (select id from sets where short_name = 'gn2'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'gn2'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brute Strength'),
    (select id from sets where short_name = 'gn2'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Accursed Horde'),
    (select id from sets where short_name = 'gn2'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thunderherd Migration'),
    (select id from sets where short_name = 'gn2'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'gn2'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steppe Glider'),
    (select id from sets where short_name = 'gn2'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'gn2'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rise from the Grave'),
    (select id from sets where short_name = 'gn2'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fiendish Duo'),
    (select id from sets where short_name = 'gn2'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Grasp of Darkness'),
    (select id from sets where short_name = 'gn2'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'gn2'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kargan Dragonrider'),
    (select id from sets where short_name = 'gn2'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Patron of the Valiant'),
    (select id from sets where short_name = 'gn2'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thundering Spineback'),
    (select id from sets where short_name = 'gn2'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Take Vengeance'),
    (select id from sets where short_name = 'gn2'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'gn2'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grazing Whiptail'),
    (select id from sets where short_name = 'gn2'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kytheon''s Irregulars'),
    (select id from sets where short_name = 'gn2'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Galestrike'),
    (select id from sets where short_name = 'gn2'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cryptic Serpent'),
    (select id from sets where short_name = 'gn2'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sparktongue Dragon'),
    (select id from sets where short_name = 'gn2'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Salvager of Secrets'),
    (select id from sets where short_name = 'gn2'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Engulf the Shore'),
    (select id from sets where short_name = 'gn2'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rise from the Tides'),
    (select id from sets where short_name = 'gn2'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Earthshaker Giant'),
    (select id from sets where short_name = 'gn2'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Destructive Tampering'),
    (select id from sets where short_name = 'gn2'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Decision Paralysis'),
    (select id from sets where short_name = 'gn2'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Highcliff Felidar'),
    (select id from sets where short_name = 'gn2'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'gn2'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Torgaar, Famine Incarnate'),
    (select id from sets where short_name = 'gn2'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mighty Leap'),
    (select id from sets where short_name = 'gn2'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Mastery'),
    (select id from sets where short_name = 'gn2'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Topan Freeblade'),
    (select id from sets where short_name = 'gn2'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ripjaw Raptor'),
    (select id from sets where short_name = 'gn2'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akoum Hellkite'),
    (select id from sets where short_name = 'gn2'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voldaren Duelist'),
    (select id from sets where short_name = 'gn2'),
    '44',
    'common'
) 
 on conflict do nothing;
