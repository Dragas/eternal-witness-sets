    insert into mtgcard(name) values ('Relief Captain') on conflict do nothing;
    insert into mtgcard(name) values ('Claustrophobia') on conflict do nothing;
    insert into mtgcard(name) values ('Ranging Raptors') on conflict do nothing;
    insert into mtgcard(name) values ('Fiend Binder') on conflict do nothing;
    insert into mtgcard(name) values ('Lord of the Accursed') on conflict do nothing;
    insert into mtgcard(name) values ('Sphinx of Enlightenment') on conflict do nothing;
    insert into mtgcard(name) values ('Lightning Strike') on conflict do nothing;
    insert into mtgcard(name) values ('Zetalpa, Primal Dawn') on conflict do nothing;
    insert into mtgcard(name) values ('Calculating Lich') on conflict do nothing;
    insert into mtgcard(name) values ('Gavony Unhallowed') on conflict do nothing;
    insert into mtgcard(name) values ('Crow of Dark Tidings') on conflict do nothing;
    insert into mtgcard(name) values ('Spidery Grasp') on conflict do nothing;
    insert into mtgcard(name) values ('Zealot of the God-Pharaoh') on conflict do nothing;
    insert into mtgcard(name) values ('Forest') on conflict do nothing;
    insert into mtgcard(name) values ('Island') on conflict do nothing;
    insert into mtgcard(name) values ('Aven Wind Mage') on conflict do nothing;
    insert into mtgcard(name) values ('Dramatic Reversal') on conflict do nothing;
    insert into mtgcard(name) values ('Mountain') on conflict do nothing;
    insert into mtgcard(name) values ('Crested Herdcaller') on conflict do nothing;
    insert into mtgcard(name) values ('Carrion Screecher') on conflict do nothing;
    insert into mtgcard(name) values ('Swamp') on conflict do nothing;
    insert into mtgcard(name) values ('Ghalta, Primal Hunger') on conflict do nothing;
    insert into mtgcard(name) values ('Tattered Mummy') on conflict do nothing;
    insert into mtgcard(name) values ('River''s Rebuke') on conflict do nothing;
    insert into mtgcard(name) values ('Lathliss, Dragon Queen') on conflict do nothing;
    insert into mtgcard(name) values ('Dragon Egg') on conflict do nothing;
    insert into mtgcard(name) values ('Howling Golem') on conflict do nothing;
    insert into mtgcard(name) values ('Consul''s Lieutenant') on conflict do nothing;
    insert into mtgcard(name) values ('Brute Strength') on conflict do nothing;
    insert into mtgcard(name) values ('Accursed Horde') on conflict do nothing;
    insert into mtgcard(name) values ('Thunderherd Migration') on conflict do nothing;
    insert into mtgcard(name) values ('Steppe Glider') on conflict do nothing;
    insert into mtgcard(name) values ('Rise from the Grave') on conflict do nothing;
    insert into mtgcard(name) values ('Fiendish Duo') on conflict do nothing;
    insert into mtgcard(name) values ('Grasp of Darkness') on conflict do nothing;
    insert into mtgcard(name) values ('Plains') on conflict do nothing;
    insert into mtgcard(name) values ('Kargan Dragonrider') on conflict do nothing;
    insert into mtgcard(name) values ('Patron of the Valiant') on conflict do nothing;
    insert into mtgcard(name) values ('Thundering Spineback') on conflict do nothing;
    insert into mtgcard(name) values ('Take Vengeance') on conflict do nothing;
    insert into mtgcard(name) values ('Grazing Whiptail') on conflict do nothing;
    insert into mtgcard(name) values ('Kytheon''s Irregulars') on conflict do nothing;
    insert into mtgcard(name) values ('Galestrike') on conflict do nothing;
    insert into mtgcard(name) values ('Cryptic Serpent') on conflict do nothing;
    insert into mtgcard(name) values ('Sparktongue Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Salvager of Secrets') on conflict do nothing;
    insert into mtgcard(name) values ('Engulf the Shore') on conflict do nothing;
    insert into mtgcard(name) values ('Rise from the Tides') on conflict do nothing;
    insert into mtgcard(name) values ('Earthshaker Giant') on conflict do nothing;
    insert into mtgcard(name) values ('Destructive Tampering') on conflict do nothing;
    insert into mtgcard(name) values ('Decision Paralysis') on conflict do nothing;
    insert into mtgcard(name) values ('Highcliff Felidar') on conflict do nothing;
    insert into mtgcard(name) values ('Torgaar, Famine Incarnate') on conflict do nothing;
    insert into mtgcard(name) values ('Mighty Leap') on conflict do nothing;
    insert into mtgcard(name) values ('Liliana''s Mastery') on conflict do nothing;
    insert into mtgcard(name) values ('Topan Freeblade') on conflict do nothing;
    insert into mtgcard(name) values ('Ripjaw Raptor') on conflict do nothing;
    insert into mtgcard(name) values ('Akoum Hellkite') on conflict do nothing;
    insert into mtgcard(name) values ('Voldaren Duelist') on conflict do nothing;
