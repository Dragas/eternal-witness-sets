insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tm20'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tm20'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure'),
    (select id from sets where short_name = 'tm20'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra, Awakened Inferno Emblem'),
    (select id from sets where short_name = 'tm20'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golem'),
    (select id from sets where short_name = 'tm20'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tm20'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tm20'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental Bird'),
    (select id from sets where short_name = 'tm20'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Pridemate'),
    (select id from sets where short_name = 'tm20'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'tm20'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mu Yanling, Sky Dancer Emblem'),
    (select id from sets where short_name = 'tm20'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon'),
    (select id from sets where short_name = 'tm20'),
    '5',
    'common'
) 
 on conflict do nothing;
