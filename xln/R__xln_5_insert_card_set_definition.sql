insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Siren Lookout'),
    (select id from sets where short_name = 'xln'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Walk the Plank'),
    (select id from sets where short_name = 'xln'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grim Captain''s Call'),
    (select id from sets where short_name = 'xln'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'xln'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gilded Sentinel'),
    (select id from sets where short_name = 'xln'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swashbuckling'),
    (select id from sets where short_name = 'xln'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Star of Extinction'),
    (select id from sets where short_name = 'xln'),
    '161',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shaper Apprentice'),
    (select id from sets where short_name = 'xln'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conqueror''s Galleon // Conqueror''s Foothold'),
    (select id from sets where short_name = 'xln'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Priest of the Wakening Sun'),
    (select id from sets where short_name = 'xln'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Nourishment'),
    (select id from sets where short_name = 'xln'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Legion''s Landing // Adanto, the First Fort'),
    (select id from sets where short_name = 'xln'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emissary of Sunrise'),
    (select id from sets where short_name = 'xln'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Old-Growth Dryads'),
    (select id from sets where short_name = 'xln'),
    '199',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone Quarry'),
    (select id from sets where short_name = 'xln'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skittering Heartstopper'),
    (select id from sets where short_name = 'xln'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snapping Sailback'),
    (select id from sets where short_name = 'xln'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dual Shot'),
    (select id from sets where short_name = 'xln'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sentinel Totem'),
    (select id from sets where short_name = 'xln'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Watertrap Weaver'),
    (select id from sets where short_name = 'xln'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunpetal Grove'),
    (select id from sets where short_name = 'xln'),
    '257',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = 'xln'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dire Fleet Hoarder'),
    (select id from sets where short_name = 'xln'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shore Keeper'),
    (select id from sets where short_name = 'xln'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burning Sun''s Avatar'),
    (select id from sets where short_name = 'xln'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Entrancing Melody'),
    (select id from sets where short_name = 'xln'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raging Swordtooth'),
    (select id from sets where short_name = 'xln'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'New Horizons'),
    (select id from sets where short_name = 'xln'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'xln'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'xln'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sky Terror'),
    (select id from sets where short_name = 'xln'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Herald of Secret Streams'),
    (select id from sets where short_name = 'xln'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steadfast Armasaur'),
    (select id from sets where short_name = 'xln'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tishana, Voice of Thunder'),
    (select id from sets where short_name = 'xln'),
    '230',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Search for Azcanta // Azcanta, the Sunken Ruin'),
    (select id from sets where short_name = 'xln'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'xln'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruthless Knave'),
    (select id from sets where short_name = 'xln'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunbird''s Invocation'),
    (select id from sets where short_name = 'xln'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skymarch Bloodletter'),
    (select id from sets where short_name = 'xln'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crash the Ramparts'),
    (select id from sets where short_name = 'xln'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Adanto Vanguard'),
    (select id from sets where short_name = 'xln'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deeproot Waters'),
    (select id from sets where short_name = 'xln'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sword-Point Diplomacy'),
    (select id from sets where short_name = 'xln'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fell Flagship'),
    (select id from sets where short_name = 'xln'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thrash of Raptors'),
    (select id from sets where short_name = 'xln'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'xln'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vicious Conquistador'),
    (select id from sets where short_name = 'xln'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Headwater Sentries'),
    (select id from sets where short_name = 'xln'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pounce'),
    (select id from sets where short_name = 'xln'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kinjalli''s Sunwing'),
    (select id from sets where short_name = 'xln'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'River Sneak'),
    (select id from sets where short_name = 'xln'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'One With the Wind'),
    (select id from sets where short_name = 'xln'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk Branchwalker'),
    (select id from sets where short_name = 'xln'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raptor Companion'),
    (select id from sets where short_name = 'xln'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravenous Daggertooth'),
    (select id from sets where short_name = 'xln'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inspiring Cleric'),
    (select id from sets where short_name = 'xln'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bright Reprisal'),
    (select id from sets where short_name = 'xln'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kopala, Warden of Waves'),
    (select id from sets where short_name = 'xln'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Queen''s Agent'),
    (select id from sets where short_name = 'xln'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thundering Spineback'),
    (select id from sets where short_name = 'xln'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boneyard Parley'),
    (select id from sets where short_name = 'xln'),
    '94',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fathom Fleet Captain'),
    (select id from sets where short_name = 'xln'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bishop of the Bloodstained'),
    (select id from sets where short_name = 'xln'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Legion''s Judgment'),
    (select id from sets where short_name = 'xln'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thaumatic Compass // Spires of Orazca'),
    (select id from sets where short_name = 'xln'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Brontodon'),
    (select id from sets where short_name = 'xln'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'xln'),
    '278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'xln'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Belligerent Brontodon'),
    (select id from sets where short_name = 'xln'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rampaging Ferocidon'),
    (select id from sets where short_name = 'xln'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cobbled Wings'),
    (select id from sets where short_name = 'xln'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Axis of Mortality'),
    (select id from sets where short_name = 'xln'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blight Keeper'),
    (select id from sets where short_name = 'xln'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carnage Tyrant'),
    (select id from sets where short_name = 'xln'),
    '179',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Marauding Looter'),
    (select id from sets where short_name = 'xln'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sun-Crowned Hunters'),
    (select id from sets where short_name = 'xln'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bishop of Rebirth'),
    (select id from sets where short_name = 'xln'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'March of the Drowned'),
    (select id from sets where short_name = 'xln'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Atzocan Archer'),
    (select id from sets where short_name = 'xln'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodcrazed Paladin'),
    (select id from sets where short_name = 'xln'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'xln'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spike-Tailed Ceratops'),
    (select id from sets where short_name = 'xln'),
    '209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pterodon Knight'),
    (select id from sets where short_name = 'xln'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slice in Twain'),
    (select id from sets where short_name = 'xln'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ruin Raider'),
    (select id from sets where short_name = 'xln'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pirate''s Prize'),
    (select id from sets where short_name = 'xln'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elaborate Firecannon'),
    (select id from sets where short_name = 'xln'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'xln'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashes of the Abhorrent'),
    (select id from sets where short_name = 'xln'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prosperous Pirates'),
    (select id from sets where short_name = 'xln'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dowsing Dagger // Lost Vale'),
    (select id from sets where short_name = 'xln'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unknown Shores'),
    (select id from sets where short_name = 'xln'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hijack'),
    (select id from sets where short_name = 'xln'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drover of the Mighty'),
    (select id from sets where short_name = 'xln'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'xln'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fathom Fleet Firebrand'),
    (select id from sets where short_name = 'xln'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wildgrowth Walker'),
    (select id from sets where short_name = 'xln'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kumena''s Speaker'),
    (select id from sets where short_name = 'xln'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sure Strike'),
    (select id from sets where short_name = 'xln'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dire Fleet Ravager'),
    (select id from sets where short_name = 'xln'),
    '104',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fire Shrine Keeper'),
    (select id from sets where short_name = 'xln'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreamcaller Siren'),
    (select id from sets where short_name = 'xln'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mark of the Vampire'),
    (select id from sets where short_name = 'xln'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nest Robber'),
    (select id from sets where short_name = 'xln'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daring Saboteur'),
    (select id from sets where short_name = 'xln'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Paladin of the Bloodstained'),
    (select id from sets where short_name = 'xln'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Captain Lannery Storm'),
    (select id from sets where short_name = 'xln'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deadeye Quartermaster'),
    (select id from sets where short_name = 'xln'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ixalan''s Binding'),
    (select id from sets where short_name = 'xln'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vanquisher''s Banner'),
    (select id from sets where short_name = 'xln'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wind Strider'),
    (select id from sets where short_name = 'xln'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kinjalli''s Caller'),
    (select id from sets where short_name = 'xln'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Depths of Desire'),
    (select id from sets where short_name = 'xln'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadeye Plunderers'),
    (select id from sets where short_name = 'xln'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tempest Caller'),
    (select id from sets where short_name = 'xln'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Makeshift Munitions'),
    (select id from sets where short_name = 'xln'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fiery Cannonade'),
    (select id from sets where short_name = 'xln'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Headstrong Brute'),
    (select id from sets where short_name = 'xln'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blossom Dryad'),
    (select id from sets where short_name = 'xln'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opt'),
    (select id from sets where short_name = 'xln'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wanted Scoundrels'),
    (select id from sets where short_name = 'xln'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pious Interdiction'),
    (select id from sets where short_name = 'xln'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyblade of the Legion'),
    (select id from sets where short_name = 'xln'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sailor of Means'),
    (select id from sets where short_name = 'xln'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ritual of Rejuvenation'),
    (select id from sets where short_name = 'xln'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Huatli, Dinosaur Knight'),
    (select id from sets where short_name = 'xln'),
    '285',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Spell Pierce'),
    (select id from sets where short_name = 'xln'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spell Swindle'),
    (select id from sets where short_name = 'xln'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hierophant''s Chalice'),
    (select id from sets where short_name = 'xln'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sun-Blessed Mount'),
    (select id from sets where short_name = 'xln'),
    '288',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Encampment Keeper'),
    (select id from sets where short_name = 'xln'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunrise Seeker'),
    (select id from sets where short_name = 'xln'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctum Seeker'),
    (select id from sets where short_name = 'xln'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Siren Stormtamer'),
    (select id from sets where short_name = 'xln'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gishath, Sun''s Avatar'),
    (select id from sets where short_name = 'xln'),
    '222',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Woodland Stream'),
    (select id from sets where short_name = 'xln'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wakening Sun''s Avatar'),
    (select id from sets where short_name = 'xln'),
    '44',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Grazing Whiptail'),
    (select id from sets where short_name = 'xln'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sleek Schooner'),
    (select id from sets where short_name = 'xln'),
    '247',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Colossal Dreadmaw'),
    (select id from sets where short_name = 'xln'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Revel in Riches'),
    (select id from sets where short_name = 'xln'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unfriendly Fire'),
    (select id from sets where short_name = 'xln'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shapers of Nature'),
    (select id from sets where short_name = 'xln'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deeproot Warrior'),
    (select id from sets where short_name = 'xln'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ripjaw Raptor'),
    (select id from sets where short_name = 'xln'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imperial Aerosaur'),
    (select id from sets where short_name = 'xln'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Waker of the Wilds'),
    (select id from sets where short_name = 'xln'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imperial Lancer'),
    (select id from sets where short_name = 'xln'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fleet Swallower'),
    (select id from sets where short_name = 'xln'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'xln'),
    '276',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rile'),
    (select id from sets where short_name = 'xln'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raptor Hatchling'),
    (select id from sets where short_name = 'xln'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dire Fleet Captain'),
    (select id from sets where short_name = 'xln'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goring Ceratops'),
    (select id from sets where short_name = 'xln'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rummaging Goblin'),
    (select id from sets where short_name = 'xln'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadeye Tracker'),
    (select id from sets where short_name = 'xln'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sorcerous Spyglass'),
    (select id from sets where short_name = 'xln'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampire''s Zeal'),
    (select id from sets where short_name = 'xln'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shipwreck Looter'),
    (select id from sets where short_name = 'xln'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathless Ancient'),
    (select id from sets where short_name = 'xln'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Storm Fleet Arsonist'),
    (select id from sets where short_name = 'xln'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Emergent Growth'),
    (select id from sets where short_name = 'xln'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shining Aerosaur'),
    (select id from sets where short_name = 'xln'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drowned Catacomb'),
    (select id from sets where short_name = 'xln'),
    '253',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Siren''s Ruse'),
    (select id from sets where short_name = 'xln'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace''s Sentinel'),
    (select id from sets where short_name = 'xln'),
    '283',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragonskull Summit'),
    (select id from sets where short_name = 'xln'),
    '252',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shapers'' Sanctuary'),
    (select id from sets where short_name = 'xln'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm Sculptor'),
    (select id from sets where short_name = 'xln'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'xln'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vraska, Relic Seeker'),
    (select id from sets where short_name = 'xln'),
    '232',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chart a Course'),
    (select id from sets where short_name = 'xln'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Admiral Beckett Brass'),
    (select id from sets where short_name = 'xln'),
    '217',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Desperate Castaways'),
    (select id from sets where short_name = 'xln'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Verdant Rebirth'),
    (select id from sets where short_name = 'xln'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anointed Deacon'),
    (select id from sets where short_name = 'xln'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brazen Buccaneers'),
    (select id from sets where short_name = 'xln'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Settle the Wreckage'),
    (select id from sets where short_name = 'xln'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verdant Sun''s Avatar'),
    (select id from sets where short_name = 'xln'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm Fleet Aerialist'),
    (select id from sets where short_name = 'xln'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vance''s Blasting Cannons // Spitfire Bastion'),
    (select id from sets where short_name = 'xln'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ranging Raptors'),
    (select id from sets where short_name = 'xln'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Run Aground'),
    (select id from sets where short_name = 'xln'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'xln'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Huatli, Warrior Poet'),
    (select id from sets where short_name = 'xln'),
    '224',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Deathgorge Scavenger'),
    (select id from sets where short_name = 'xln'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wily Goblin'),
    (select id from sets where short_name = 'xln'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Huatli''s Spurring'),
    (select id from sets where short_name = 'xln'),
    '287',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dinosaur Stampede'),
    (select id from sets where short_name = 'xln'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tocatli Honor Guard'),
    (select id from sets where short_name = 'xln'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bonded Horncrest'),
    (select id from sets where short_name = 'xln'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crushing Canopy'),
    (select id from sets where short_name = 'xln'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frenzied Raptor'),
    (select id from sets where short_name = 'xln'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slash of Talons'),
    (select id from sets where short_name = 'xln'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tilonalli''s Skinshifter'),
    (select id from sets where short_name = 'xln'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glorifier of Dusk'),
    (select id from sets where short_name = 'xln'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Storm Fleet Pyromancer'),
    (select id from sets where short_name = 'xln'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'xln'),
    '277',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lurking Chupacabra'),
    (select id from sets where short_name = 'xln'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jade Guardian'),
    (select id from sets where short_name = 'xln'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prying Blade'),
    (select id from sets where short_name = 'xln'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Favorable Winds'),
    (select id from sets where short_name = 'xln'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dusk Legion Dreadnought'),
    (select id from sets where short_name = 'xln'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tishana''s Wayfinder'),
    (select id from sets where short_name = 'xln'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Territorial Hammerskull'),
    (select id from sets where short_name = 'xln'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sheltering Light'),
    (select id from sets where short_name = 'xln'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firecannon Blast'),
    (select id from sets where short_name = 'xln'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ixalli''s Diviner'),
    (select id from sets where short_name = 'xln'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tilonalli''s Knight'),
    (select id from sets where short_name = 'xln'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shadowed Caravel'),
    (select id from sets where short_name = 'xln'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dive Down'),
    (select id from sets where short_name = 'xln'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Castaway''s Despair'),
    (select id from sets where short_name = 'xln'),
    '281',
    'common'
) ,
(
    (select id from mtgcard where name = 'Growing Rites of Itlimoc // Itlimoc, Cradle of the Sun'),
    (select id from sets where short_name = 'xln'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pirate''s Cutlass'),
    (select id from sets where short_name = 'xln'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rootbound Crag'),
    (select id from sets where short_name = 'xln'),
    '256',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jace, Cunning Castaway'),
    (select id from sets where short_name = 'xln'),
    '60',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Skulduggery'),
    (select id from sets where short_name = 'xln'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Repeating Barrage'),
    (select id from sets where short_name = 'xln'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duskborne Skymarcher'),
    (select id from sets where short_name = 'xln'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blinding Fog'),
    (select id from sets where short_name = 'xln'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vraska''s Contempt'),
    (select id from sets where short_name = 'xln'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rowdy Crew'),
    (select id from sets where short_name = 'xln'),
    '159',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jungle Delver'),
    (select id from sets where short_name = 'xln'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcane Adaptation'),
    (select id from sets where short_name = 'xln'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treasure Map // Treasure Cove'),
    (select id from sets where short_name = 'xln'),
    '250',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vona, Butcher of Magan'),
    (select id from sets where short_name = 'xln'),
    '231',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Otepec Huntmaster'),
    (select id from sets where short_name = 'xln'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bellowing Aegisaur'),
    (select id from sets where short_name = 'xln'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Costly Plunder'),
    (select id from sets where short_name = 'xln'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vineshaper Mystic'),
    (select id from sets where short_name = 'xln'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arguel''s Blood Fast // Temple of Aclazotz'),
    (select id from sets where short_name = 'xln'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heartless Pillage'),
    (select id from sets where short_name = 'xln'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Captivating Crew'),
    (select id from sets where short_name = 'xln'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Perilous Voyage'),
    (select id from sets where short_name = 'xln'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spreading Rot'),
    (select id from sets where short_name = 'xln'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savage Stomp'),
    (select id from sets where short_name = 'xln'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Queen''s Bay Soldier'),
    (select id from sets where short_name = 'xln'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glacial Fortress'),
    (select id from sets where short_name = 'xln'),
    '255',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'xln'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cancel'),
    (select id from sets where short_name = 'xln'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Looming Altisaur'),
    (select id from sets where short_name = 'xln'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rallying Roar'),
    (select id from sets where short_name = 'xln'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Contract Killing'),
    (select id from sets where short_name = 'xln'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadeye Tormentor'),
    (select id from sets where short_name = 'xln'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demolish'),
    (select id from sets where short_name = 'xln'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primal Amulet // Primal Wellspring'),
    (select id from sets where short_name = 'xln'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Charging Monstrosaur'),
    (select id from sets where short_name = 'xln'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Regisaur Alpha'),
    (select id from sets where short_name = 'xln'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Field of Ruin'),
    (select id from sets where short_name = 'xln'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bishop''s Soldier'),
    (select id from sets where short_name = 'xln'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'xln'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'xln'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Commune with Dinosaurs'),
    (select id from sets where short_name = 'xln'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'xln'),
    '275',
    'common'
) ,
(
    (select id from mtgcard where name = 'Huatli''s Snubhorn'),
    (select id from sets where short_name = 'xln'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dire Fleet Interloper'),
    (select id from sets where short_name = 'xln'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'River Heralds'' Boon'),
    (select id from sets where short_name = 'xln'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seekers'' Squire'),
    (select id from sets where short_name = 'xln'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demystify'),
    (select id from sets where short_name = 'xln'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace, Ingenious Mind-Mage'),
    (select id from sets where short_name = 'xln'),
    '280',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vanquish the Weak'),
    (select id from sets where short_name = 'xln'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ixalli''s Keeper'),
    (select id from sets where short_name = 'xln'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grasping Current'),
    (select id from sets where short_name = 'xln'),
    '282',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fathom Fleet Cutthroat'),
    (select id from sets where short_name = 'xln'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hostage Taker'),
    (select id from sets where short_name = 'xln'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deeproot Champion'),
    (select id from sets where short_name = 'xln'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Navigator''s Ruin'),
    (select id from sets where short_name = 'xln'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning-Rig Crew'),
    (select id from sets where short_name = 'xln'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rigging Runner'),
    (select id from sets where short_name = 'xln'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'xln'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Queen''s Commission'),
    (select id from sets where short_name = 'xln'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angrath''s Marauders'),
    (select id from sets where short_name = 'xln'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Strike'),
    (select id from sets where short_name = 'xln'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'xln'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emperor''s Vanguard'),
    (select id from sets where short_name = 'xln'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Legion Conquistador'),
    (select id from sets where short_name = 'xln'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lookout''s Dispersal'),
    (select id from sets where short_name = 'xln'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kitesail Freebooter'),
    (select id from sets where short_name = 'xln'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overflowing Insight'),
    (select id from sets where short_name = 'xln'),
    '66',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Call to the Feast'),
    (select id from sets where short_name = 'xln'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mavren Fein, Dusk Apostle'),
    (select id from sets where short_name = 'xln'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm Fleet Spy'),
    (select id from sets where short_name = 'xln'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pillar of Origins'),
    (select id from sets where short_name = 'xln'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sanguine Sacrament'),
    (select id from sets where short_name = 'xln'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trove of Temptation'),
    (select id from sets where short_name = 'xln'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'xln'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raiders'' Wake'),
    (select id from sets where short_name = 'xln'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'River''s Rebuke'),
    (select id from sets where short_name = 'xln'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unclaimed Territory'),
    (select id from sets where short_name = 'xln'),
    '258',
    'uncommon'
) 
 on conflict do nothing;
