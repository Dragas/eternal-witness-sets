insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Lord of Shatterskull Pass'),
    (select id from sets where short_name = 'proe'),
    '★156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Staggershock'),
    (select id from sets where short_name = 'proe'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathless Angel'),
    (select id from sets where short_name = 'proe'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emrakul, the Aeons Torn'),
    (select id from sets where short_name = 'proe'),
    '★4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Guul Draz Assassin'),
    (select id from sets where short_name = 'proe'),
    '*112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pestilence Demon'),
    (select id from sets where short_name = 'proe'),
    '*124',
    'rare'
) 
 on conflict do nothing;
