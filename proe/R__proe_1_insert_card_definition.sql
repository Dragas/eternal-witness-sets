    insert into mtgcard(name) values ('Lord of Shatterskull Pass') on conflict do nothing;
    insert into mtgcard(name) values ('Staggershock') on conflict do nothing;
    insert into mtgcard(name) values ('Deathless Angel') on conflict do nothing;
    insert into mtgcard(name) values ('Emrakul, the Aeons Torn') on conflict do nothing;
    insert into mtgcard(name) values ('Guul Draz Assassin') on conflict do nothing;
    insert into mtgcard(name) values ('Pestilence Demon') on conflict do nothing;
