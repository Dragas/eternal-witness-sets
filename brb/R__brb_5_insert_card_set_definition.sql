insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'brb'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thalakos Lowlands'),
    (select id from sets where short_name = 'brb'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Catastrophe'),
    (select id from sets where short_name = 'brb'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Opportunity'),
    (select id from sets where short_name = 'brb'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Curfew'),
    (select id from sets where short_name = 'brb'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spike Worker'),
    (select id from sets where short_name = 'brb'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spike Colony'),
    (select id from sets where short_name = 'brb'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'brb'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'brb'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pestilence'),
    (select id from sets where short_name = 'brb'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'brb'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'brb'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'brb'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fertile Ground'),
    (select id from sets where short_name = 'brb'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Ants'),
    (select id from sets where short_name = 'brb'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'brb'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fecundity'),
    (select id from sets where short_name = 'brb'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sadistic Glee'),
    (select id from sets where short_name = 'brb'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Village Elder'),
    (select id from sets where short_name = 'brb'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cinder Marsh'),
    (select id from sets where short_name = 'brb'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abyssal Specter'),
    (select id from sets where short_name = 'brb'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heat Ray'),
    (select id from sets where short_name = 'brb'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soltari Foot Soldier'),
    (select id from sets where short_name = 'brb'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'brb'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tranquility'),
    (select id from sets where short_name = 'brb'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Ghoul'),
    (select id from sets where short_name = 'brb'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spike Feeder'),
    (select id from sets where short_name = 'brb'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelic Page'),
    (select id from sets where short_name = 'brb'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'brb'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exhume'),
    (select id from sets where short_name = 'brb'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Remote Isle'),
    (select id from sets where short_name = 'brb'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sun Clasp'),
    (select id from sets where short_name = 'brb'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shower of Sparks'),
    (select id from sets where short_name = 'brb'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'brb'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Argothian Elder'),
    (select id from sets where short_name = 'brb'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Land Tax'),
    (select id from sets where short_name = 'brb'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'brb'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Control Magic'),
    (select id from sets where short_name = 'brb'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sanctum Custodian'),
    (select id from sets where short_name = 'brb'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'brb'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scryb Sprites'),
    (select id from sets where short_name = 'brb'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'brb'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Broken Fall'),
    (select id from sets where short_name = 'brb'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Syphon Soul'),
    (select id from sets where short_name = 'brb'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infantry Veteran'),
    (select id from sets where short_name = 'brb'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'brb'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scaled Wurm'),
    (select id from sets where short_name = 'brb'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'brb'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cackling Fiend'),
    (select id from sets where short_name = 'brb'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'brb'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disruptive Student'),
    (select id from sets where short_name = 'brb'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slippery Karst'),
    (select id from sets where short_name = 'brb'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'brb'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'brb'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plated Rootwalla'),
    (select id from sets where short_name = 'brb'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Polluted Mire'),
    (select id from sets where short_name = 'brb'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wind Drake'),
    (select id from sets where short_name = 'brb'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'brb'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'brb'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Living Death'),
    (select id from sets where short_name = 'brb'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pincher Beetles'),
    (select id from sets where short_name = 'brb'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lhurgoyf'),
    (select id from sets where short_name = 'brb'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'brb'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nekrataal'),
    (select id from sets where short_name = 'brb'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Leak'),
    (select id from sets where short_name = 'brb'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hurricane'),
    (select id from sets where short_name = 'brb'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mogg Hollows'),
    (select id from sets where short_name = 'brb'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seasoned Marshal'),
    (select id from sets where short_name = 'brb'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'brb'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prodigal Sorcerer'),
    (select id from sets where short_name = 'brb'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vec Townships'),
    (select id from sets where short_name = 'brb'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'brb'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maniacal Rage'),
    (select id from sets where short_name = 'brb'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'brb'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'brb'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'River Boa'),
    (select id from sets where short_name = 'brb'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Man-o''-War'),
    (select id from sets where short_name = 'brb'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drifting Meadow'),
    (select id from sets where short_name = 'brb'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ray of Command'),
    (select id from sets where short_name = 'brb'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armored Pegasus'),
    (select id from sets where short_name = 'brb'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'brb'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'brb'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Uthden Troll'),
    (select id from sets where short_name = 'brb'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Lyrist'),
    (select id from sets where short_name = 'brb'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trumpeting Armodon'),
    (select id from sets where short_name = 'brb'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Heat'),
    (select id from sets where short_name = 'brb'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'brb'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'brb'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weakness'),
    (select id from sets where short_name = 'brb'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raging Goblin'),
    (select id from sets where short_name = 'brb'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'brb'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sandstorm'),
    (select id from sets where short_name = 'brb'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'brb'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = 'brb'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Healing Salve'),
    (select id from sets where short_name = 'brb'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windfall'),
    (select id from sets where short_name = 'brb'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'brb'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'brb'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unnerve'),
    (select id from sets where short_name = 'brb'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'brb'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steam Blast'),
    (select id from sets where short_name = 'brb'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'brb'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crazed Skirge'),
    (select id from sets where short_name = 'brb'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sewer Rats'),
    (select id from sets where short_name = 'brb'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gorilla Warrior'),
    (select id from sets where short_name = 'brb'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Elite'),
    (select id from sets where short_name = 'brb'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'brb'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'brb'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seeker of Skybreak'),
    (select id from sets where short_name = 'brb'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spike Weaver'),
    (select id from sets where short_name = 'brb'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rolling Thunder'),
    (select id from sets where short_name = 'brb'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Advance Scout'),
    (select id from sets where short_name = 'brb'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'brb'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wildfire Emissary'),
    (select id from sets where short_name = 'brb'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Master Decoy'),
    (select id from sets where short_name = 'brb'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'brb'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'brb'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = 'brb'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flood'),
    (select id from sets where short_name = 'brb'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Songstitcher'),
    (select id from sets where short_name = 'brb'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reanimate'),
    (select id from sets where short_name = 'brb'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azure Drake'),
    (select id from sets where short_name = 'brb'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Warden'),
    (select id from sets where short_name = 'brb'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Elemental'),
    (select id from sets where short_name = 'brb'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dirtcowl Wurm'),
    (select id from sets where short_name = 'brb'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Manta Riders'),
    (select id from sets where short_name = 'brb'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blinking Spirit'),
    (select id from sets where short_name = 'brb'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = 'brb'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'brb'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arc Lightning'),
    (select id from sets where short_name = 'brb'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'brb'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'brb'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctum Guardian'),
    (select id from sets where short_name = 'brb'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'brb'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Subversion'),
    (select id from sets where short_name = 'brb'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Symbiosis'),
    (select id from sets where short_name = 'brb'),
    '85',
    'common'
) 
 on conflict do nothing;
