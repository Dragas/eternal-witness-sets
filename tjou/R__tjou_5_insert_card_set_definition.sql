insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Snake'),
    (select id from sets where short_name = 'tjou'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minotaur'),
    (select id from sets where short_name = 'tjou'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spider'),
    (select id from sets where short_name = 'tjou'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tjou'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hydra'),
    (select id from sets where short_name = 'tjou'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx'),
    (select id from sets where short_name = 'tjou'),
    '1',
    'common'
) 
 on conflict do nothing;
