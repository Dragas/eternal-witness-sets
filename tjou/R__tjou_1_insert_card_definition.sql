    insert into mtgcard(name) values ('Snake') on conflict do nothing;
    insert into mtgcard(name) values ('Minotaur') on conflict do nothing;
    insert into mtgcard(name) values ('Spider') on conflict do nothing;
    insert into mtgcard(name) values ('Zombie') on conflict do nothing;
    insert into mtgcard(name) values ('Hydra') on conflict do nothing;
    insert into mtgcard(name) values ('Sphinx') on conflict do nothing;
