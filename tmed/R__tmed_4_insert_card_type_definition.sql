insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Zombie'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Garruk, Apex Predator Emblem'),
        (select types.id from types where name = 'Emblem')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Garruk, Apex Predator Emblem'),
        (select types.id from types where name = 'Garruk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ral, Izzet Viceroy Emblem'),
        (select types.id from types where name = 'Emblem')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ral, Izzet Viceroy Emblem'),
        (select types.id from types where name = 'Ral')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Construct'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Construct'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Construct'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Construct'),
        (select types.id from types where name = 'Construct')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Beast'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Beast'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Beast'),
        (select types.id from types where name = 'Beast')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Teferi, Hero of Dominaria Emblem'),
        (select types.id from types where name = 'Emblem')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Teferi, Hero of Dominaria Emblem'),
        (select types.id from types where name = 'Teferi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Construct'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Construct'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Construct'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Construct'),
        (select types.id from types where name = 'Construct')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Liliana, the Last Hope Emblem'),
        (select types.id from types where name = 'Emblem')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Liliana, the Last Hope Emblem'),
        (select types.id from types where name = 'Liliana')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dragon'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dragon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dragon'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elspeth, Knight-Errant Emblem'),
        (select types.id from types where name = 'Emblem')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elspeth, Knight-Errant Emblem'),
        (select types.id from types where name = 'Elspeth')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vraska, Golgari Queen Emblem'),
        (select types.id from types where name = 'Emblem')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vraska, Golgari Queen Emblem'),
        (select types.id from types where name = 'Vraska')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jaya Ballard Emblem'),
        (select types.id from types where name = 'Emblem')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jaya Ballard Emblem'),
        (select types.id from types where name = 'Jaya')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tamiyo, the Moon Sage Emblem'),
        (select types.id from types where name = 'Emblem')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tamiyo, the Moon Sage Emblem'),
        (select types.id from types where name = 'Tamiyo')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Domri, Chaos Bringer Emblem'),
        (select types.id from types where name = 'Emblem')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Domri, Chaos Bringer Emblem'),
        (select types.id from types where name = 'Domri')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dack Fayden Emblem'),
        (select types.id from types where name = 'Emblem')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dack Fayden Emblem'),
        (select types.id from types where name = 'Dack')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soldier'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soldier'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soldier'),
        (select types.id from types where name = 'Soldier')
    ) 
 on conflict do nothing;
