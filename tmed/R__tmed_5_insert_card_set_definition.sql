insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tmed'),
    'G2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk, Apex Predator Emblem'),
    (select id from sets where short_name = 'tmed'),
    'W3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ral, Izzet Viceroy Emblem'),
    (select id from sets where short_name = 'tmed'),
    'G6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Construct'),
    (select id from sets where short_name = 'tmed'),
    'G3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tmed'),
    'W1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teferi, Hero of Dominaria Emblem'),
    (select id from sets where short_name = 'tmed'),
    'G7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Construct'),
    (select id from sets where short_name = 'tmed'),
    'R1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana, the Last Hope Emblem'),
    (select id from sets where short_name = 'tmed'),
    'G5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tmed'),
    'W2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elspeth, Knight-Errant Emblem'),
    (select id from sets where short_name = 'tmed'),
    'G4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vraska, Golgari Queen Emblem'),
    (select id from sets where short_name = 'tmed'),
    'G8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jaya Ballard Emblem'),
    (select id from sets where short_name = 'tmed'),
    'R4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tamiyo, the Moon Sage Emblem'),
    (select id from sets where short_name = 'tmed'),
    'R5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Domri, Chaos Bringer Emblem'),
    (select id from sets where short_name = 'tmed'),
    'R3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dack Fayden Emblem'),
    (select id from sets where short_name = 'tmed'),
    'R2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tmed'),
    'G1',
    'common'
) 
 on conflict do nothing;
