insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Mardu Heart-Piercer'),
    (select id from sets where short_name = 'ddn'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Traumatic Visions'),
    (select id from sets where short_name = 'ddn'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Deathraiders'),
    (select id from sets where short_name = 'ddn'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddn'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ogre Battledriver'),
    (select id from sets where short_name = 'ddn'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddn'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddn'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sparkmage Apprentice'),
    (select id from sets where short_name = 'ddn'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddn'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stonecloaker'),
    (select id from sets where short_name = 'ddn'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'ddn'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krenko''s Command'),
    (select id from sets where short_name = 'ddn'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zurgo Helmsmasher'),
    (select id from sets where short_name = 'ddn'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Arc Trail'),
    (select id from sets where short_name = 'ddn'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Warchief'),
    (select id from sets where short_name = 'ddn'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nomad Outpost'),
    (select id from sets where short_name = 'ddn'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Angel'),
    (select id from sets where short_name = 'ddn'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beetleback Chief'),
    (select id from sets where short_name = 'ddn'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reckless Abandon'),
    (select id from sets where short_name = 'ddn'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Act of Treason'),
    (select id from sets where short_name = 'ddn'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Cannonade'),
    (select id from sets where short_name = 'ddn'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddn'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Repeal'),
    (select id from sets where short_name = 'ddn'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frenzied Goblin'),
    (select id from sets where short_name = 'ddn'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fury of the Horde'),
    (select id from sets where short_name = 'ddn'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faerie Invaders'),
    (select id from sets where short_name = 'ddn'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Monastery'),
    (select id from sets where short_name = 'ddn'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krenko, Mob Boss'),
    (select id from sets where short_name = 'ddn'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddn'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shambling Remains'),
    (select id from sets where short_name = 'ddn'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddn'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scourge Devil'),
    (select id from sets where short_name = 'ddn'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oni of Wild Places'),
    (select id from sets where short_name = 'ddn'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aquamorph Entity'),
    (select id from sets where short_name = 'ddn'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steam Augury'),
    (select id from sets where short_name = 'ddn'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddn'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'ddn'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddn'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stave Off'),
    (select id from sets where short_name = 'ddn'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kathari Bomber'),
    (select id from sets where short_name = 'ddn'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddn'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inferno Trap'),
    (select id from sets where short_name = 'ddn'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'ddn'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Leak'),
    (select id from sets where short_name = 'ddn'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Helix'),
    (select id from sets where short_name = 'ddn'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddn'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Snarecaster'),
    (select id from sets where short_name = 'ddn'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fathom Seer'),
    (select id from sets where short_name = 'ddn'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thousand Winds'),
    (select id from sets where short_name = 'ddn'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddn'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coral Trickster'),
    (select id from sets where short_name = 'ddn'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddn'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whiplash Trap'),
    (select id from sets where short_name = 'ddn'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fiery Fall'),
    (select id from sets where short_name = 'ddn'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Impulse'),
    (select id from sets where short_name = 'ddn'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lone Missionary'),
    (select id from sets where short_name = 'ddn'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'ddn'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghitu Encampment'),
    (select id from sets where short_name = 'ddn'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arrow Volley Trap'),
    (select id from sets where short_name = 'ddn'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hell''s Thunder'),
    (select id from sets where short_name = 'ddn'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fleshbag Marauder'),
    (select id from sets where short_name = 'ddn'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hellraiser Goblin'),
    (select id from sets where short_name = 'ddn'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fleeting Distraction'),
    (select id from sets where short_name = 'ddn'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddn'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Impostor'),
    (select id from sets where short_name = 'ddn'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kor Hookmaster'),
    (select id from sets where short_name = 'ddn'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hold the Line'),
    (select id from sets where short_name = 'ddn'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bone Splinters'),
    (select id from sets where short_name = 'ddn'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Master Decoy'),
    (select id from sets where short_name = 'ddn'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Willbender'),
    (select id from sets where short_name = 'ddn'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Banefire'),
    (select id from sets where short_name = 'ddn'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swift Justice'),
    (select id from sets where short_name = 'ddn'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx of Uthuun'),
    (select id from sets where short_name = 'ddn'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcanis the Omnipotent'),
    (select id from sets where short_name = 'ddn'),
    '42',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Echo Tracer'),
    (select id from sets where short_name = 'ddn'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jeskai Elder'),
    (select id from sets where short_name = 'ddn'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Bombardment'),
    (select id from sets where short_name = 'ddn'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Infantry Veteran'),
    (select id from sets where short_name = 'ddn'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flame-Kin Zealot'),
    (select id from sets where short_name = 'ddn'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dauntless Onslaught'),
    (select id from sets where short_name = 'ddn'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dregscape Zombie'),
    (select id from sets where short_name = 'ddn'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hussar Patrol'),
    (select id from sets where short_name = 'ddn'),
    '55',
    'common'
) 
 on conflict do nothing;
