insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Giant Warrior'),
    (select id from sets where short_name = 'tmor'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Rogue'),
    (select id from sets where short_name = 'tmor'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treefolk Shaman'),
    (select id from sets where short_name = 'tmor'),
    '3',
    'common'
) 
 on conflict do nothing;
