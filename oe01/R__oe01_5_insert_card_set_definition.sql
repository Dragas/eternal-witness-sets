insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Every Dream a Nightmare'),
    (select id from sets where short_name = 'oe01'),
    '6★',
    'common'
) ,
(
    (select id from mtgcard where name = 'There Is No Refuge'),
    (select id from sets where short_name = 'oe01'),
    '17★',
    'common'
) ,
(
    (select id from mtgcard where name = 'A Reckoning Approaches'),
    (select id from sets where short_name = 'oe01'),
    '16★',
    'common'
) ,
(
    (select id from mtgcard where name = 'When Will You Learn?'),
    (select id from sets where short_name = 'oe01'),
    '20★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Without Equal'),
    (select id from sets where short_name = 'oe01'),
    '15★',
    'common'
) ,
(
    (select id from mtgcard where name = 'For Each of You, a Gift'),
    (select id from sets where short_name = 'oe01'),
    '7★',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Mighty Will Fall'),
    (select id from sets where short_name = 'oe01'),
    '10★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Behold My Grandeur'),
    (select id from sets where short_name = 'oe01'),
    '2★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bow to My Command'),
    (select id from sets where short_name = 'oe01'),
    '3★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Delight in the Hunt'),
    (select id from sets where short_name = 'oe01'),
    '5★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pay Tribute to Me'),
    (select id from sets where short_name = 'oe01'),
    '14★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Because I Have Willed It'),
    (select id from sets where short_name = 'oe01'),
    '1★',
    'common'
) ,
(
    (select id from mtgcard where name = 'What''s Yours Is Now Mine'),
    (select id from sets where short_name = 'oe01'),
    '19★',
    'common'
) ,
(
    (select id from mtgcard where name = 'This World Belongs to Me'),
    (select id from sets where short_name = 'oe01'),
    '18★',
    'common'
) ,
(
    (select id from mtgcard where name = 'No One Will Hear Your Cries'),
    (select id from sets where short_name = 'oe01'),
    '13★',
    'common'
) ,
(
    (select id from mtgcard where name = 'My Laughter Echoes'),
    (select id from sets where short_name = 'oe01'),
    '12★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Know Evil'),
    (select id from sets where short_name = 'oe01'),
    '8★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Make Yourself Useful'),
    (select id from sets where short_name = 'oe01'),
    '9★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Choose Your Demise'),
    (select id from sets where short_name = 'oe01'),
    '4★',
    'common'
) ,
(
    (select id from mtgcard where name = 'My Forces Are Innumerable'),
    (select id from sets where short_name = 'oe01'),
    '11★',
    'common'
) 
 on conflict do nothing;
