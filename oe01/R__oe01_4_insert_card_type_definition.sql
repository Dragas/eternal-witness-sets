insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Every Dream a Nightmare'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'There Is No Refuge'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'A Reckoning Approaches'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'When Will You Learn?'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Power Without Equal'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'For Each of You, a Gift'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Mighty Will Fall'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Behold My Grandeur'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bow to My Command'),
        (select types.id from types where name = 'Ongoing')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bow to My Command'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Delight in the Hunt'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pay Tribute to Me'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Because I Have Willed It'),
        (select types.id from types where name = 'Ongoing')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Because I Have Willed It'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'What''s Yours Is Now Mine'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'This World Belongs to Me'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'No One Will Hear Your Cries'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'My Laughter Echoes'),
        (select types.id from types where name = 'Ongoing')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'My Laughter Echoes'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Know Evil'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Make Yourself Useful'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Choose Your Demise'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'My Forces Are Innumerable'),
        (select types.id from types where name = 'Ongoing')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'My Forces Are Innumerable'),
        (select types.id from types where name = 'Scheme')
    ) 
 on conflict do nothing;
