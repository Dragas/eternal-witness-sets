insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Clearwater Pathway // Clearwater Pathway'),
    (select id from sets where short_name = 'aznr'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wasteland // Wasteland'),
    (select id from sets where short_name = 'aznr'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Expedition Diviner // Expedition Diviner'),
    (select id from sets where short_name = 'aznr'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nissa of Shadowed Boughs 2 // Nissa of Shadowed Boughs 2'),
    (select id from sets where short_name = 'aznr'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orah, Skyclave Hierophant // Orah, Skyclave Hierophant'),
    (select id from sets where short_name = 'aznr'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace, Mirror Mage 1 // Jace, Mirror Mage 1'),
    (select id from sets where short_name = 'aznr'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyclave Shade // Skyclave Shade'),
    (select id from sets where short_name = 'aznr'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest 3 // Forest 3'),
    (select id from sets where short_name = 'aznr'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace, Mirror Mage 2 // Jace, Mirror Mage 2'),
    (select id from sets where short_name = 'aznr'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Celebrant // Kor Celebrant'),
    (select id from sets where short_name = 'aznr'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scion of the Swarm // Scion of the Swarm'),
    (select id from sets where short_name = 'aznr'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blackbloom Rogue // Blackbloom Rogue'),
    (select id from sets where short_name = 'aznr'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Needleverge Pathway // Needleverge Pathway'),
    (select id from sets where short_name = 'aznr'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest 2 // Forest 2'),
    (select id from sets where short_name = 'aznr'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brightclimb Pathway // Brightclimb Pathway'),
    (select id from sets where short_name = 'aznr'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Highborn Vampire // Highborn Vampire'),
    (select id from sets where short_name = 'aznr'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nighthawk Scavenger // Nighthawk Scavenger'),
    (select id from sets where short_name = 'aznr'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grimclimb Pathway // Grimclimb Pathway'),
    (select id from sets where short_name = 'aznr'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island 1 // Island 1'),
    (select id from sets where short_name = 'aznr'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murkwater Pathway // Murkwater Pathway'),
    (select id from sets where short_name = 'aznr'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boulderloft Pathway // Boulderloft Pathway'),
    (select id from sets where short_name = 'aznr'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains 1 // Plains 1'),
    (select id from sets where short_name = 'aznr'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp 1 // Swamp 1'),
    (select id from sets where short_name = 'aznr'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Malakir Blood-Priest // Malakir Blood-Priest'),
    (select id from sets where short_name = 'aznr'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp 3 // Swamp 3'),
    (select id from sets where short_name = 'aznr'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Linvala, Shield of Sea Gate // Linvala, Shield of Sea Gate'),
    (select id from sets where short_name = 'aznr'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains 3 // Plains 3'),
    (select id from sets where short_name = 'aznr'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yasharn, Implacable Earth // Yasharn, Implacable Earth'),
    (select id from sets where short_name = 'aznr'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tazri, Beacon of Unity // Tazri, Beacon of Unity'),
    (select id from sets where short_name = 'aznr'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Umara Wizard // Umara Wizard'),
    (select id from sets where short_name = 'aznr'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Morphic Pool // Morphic Pool'),
    (select id from sets where short_name = 'aznr'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Destiny // Angel of Destiny'),
    (select id from sets where short_name = 'aznr'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Omnath, Locus of Creation // Omnath, Locus of Creation'),
    (select id from sets where short_name = 'aznr'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moraug, Fury of Akoum // Moraug, Fury of Akoum'),
    (select id from sets where short_name = 'aznr'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maddening Cacophony // Maddening Cacophony'),
    (select id from sets where short_name = 'aznr'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nissa of Shadowed Boughs 1 // Nissa of Shadowed Boughs 1'),
    (select id from sets where short_name = 'aznr'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nahiri''s Binding // Nahiri''s Binding'),
    (select id from sets where short_name = 'aznr'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nahiri, Heir of the Ancients 1 // Nahiri, Heir of the Ancients 1'),
    (select id from sets where short_name = 'aznr'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Canopy Baloth // Canopy Baloth'),
    (select id from sets where short_name = 'aznr'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grove of the Burnwillows // Grove of the Burnwillows'),
    (select id from sets where short_name = 'aznr'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seachrome Coast // Seachrome Coast'),
    (select id from sets where short_name = 'aznr'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thwart the Grave // Thwart the Grave'),
    (select id from sets where short_name = 'aznr'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teeterpeak Ambusher // Teeterpeak Ambusher'),
    (select id from sets where short_name = 'aznr'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Celestial Colonnade // Celestial Colonnade'),
    (select id from sets where short_name = 'aznr'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain 1 // Mountain 1'),
    (select id from sets where short_name = 'aznr'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Umara Mystic // Umara Mystic'),
    (select id from sets where short_name = 'aznr'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magmatic Channeler // Magmatic Channeler'),
    (select id from sets where short_name = 'aznr'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Taborax, Hope''s Demise // Taborax, Hope''s Demise'),
    (select id from sets where short_name = 'aznr'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Stormcaller // Sea Gate Stormcaller'),
    (select id from sets where short_name = 'aznr'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windswept Heath // Windswept Heath'),
    (select id from sets where short_name = 'aznr'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anowon, the Ruin Thief // Anowon, the Ruin Thief'),
    (select id from sets where short_name = 'aznr'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flooded Strand // Flooded Strand'),
    (select id from sets where short_name = 'aznr'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain 3 // Mountain 3'),
    (select id from sets where short_name = 'aznr'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tazeem Roilmage // Tazeem Roilmage'),
    (select id from sets where short_name = 'aznr'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest 1 // Forest 1'),
    (select id from sets where short_name = 'aznr'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Carver // Mind Carver'),
    (select id from sets where short_name = 'aznr'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lotus Cobra // Lotus Cobra'),
    (select id from sets where short_name = 'aznr'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Shatter // Soul Shatter'),
    (select id from sets where short_name = 'aznr'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reclaim the Wastes // Reclaim the Wastes'),
    (select id from sets where short_name = 'aznr'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island 2 // Island 2'),
    (select id from sets where short_name = 'aznr'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kitesail Cleric // Kitesail Cleric'),
    (select id from sets where short_name = 'aznr'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Expedition Healer // Expedition Healer'),
    (select id from sets where short_name = 'aznr'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain 2 // Mountain 2'),
    (select id from sets where short_name = 'aznr'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nahiri, Heir of the Ancients 2 // Nahiri, Heir of the Ancients 2'),
    (select id from sets where short_name = 'aznr'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akiri, Fearless Voyager // Akiri, Fearless Voyager'),
    (select id from sets where short_name = 'aznr'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swarm Shambler // Swarm Shambler'),
    (select id from sets where short_name = 'aznr'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cliffhaven Sell-Sword // Cliffhaven Sell-Sword'),
    (select id from sets where short_name = 'aznr'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drana, the Last Bloodchief // Drana, the Last Bloodchief'),
    (select id from sets where short_name = 'aznr'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashaya, Soul of the Wild // Ashaya, Soul of the Wild'),
    (select id from sets where short_name = 'aznr'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp 2 // Swamp 2'),
    (select id from sets where short_name = 'aznr'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pillarverge Pathway // Pillarverge Pathway'),
    (select id from sets where short_name = 'aznr'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Banneret // Sea Gate Banneret'),
    (select id from sets where short_name = 'aznr'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyclave Basilica // Skyclave Basilica'),
    (select id from sets where short_name = 'aznr'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prismatic Vista // Prismatic Vista'),
    (select id from sets where short_name = 'aznr'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scourge of the Skyclaves // Scourge of the Skyclaves'),
    (select id from sets where short_name = 'aznr'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island 3 // Island 3'),
    (select id from sets where short_name = 'aznr'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains 2 // Plains 2'),
    (select id from sets where short_name = 'aznr'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anticognition // Anticognition'),
    (select id from sets where short_name = 'aznr'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thundering Rebuke // Thundering Rebuke'),
    (select id from sets where short_name = 'aznr'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grakmaw, Skyclave Ravager // Grakmaw, Skyclave Ravager'),
    (select id from sets where short_name = 'aznr'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Restoration // Sea Gate Restoration'),
    (select id from sets where short_name = 'aznr'),
    '49',
    'common'
) 
 on conflict do nothing;
