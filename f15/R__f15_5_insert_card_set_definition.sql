insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ultimate Price'),
    (select id from sets where short_name = 'f15'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disdainful Stroke'),
    (select id from sets where short_name = 'f15'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hordeling Outburst'),
    (select id from sets where short_name = 'f15'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frost Walker'),
    (select id from sets where short_name = 'f15'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abzan Beastmaster'),
    (select id from sets where short_name = 'f15'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serum Visions'),
    (select id from sets where short_name = 'f15'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anticipate'),
    (select id from sets where short_name = 'f15'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frenzied Goblin'),
    (select id from sets where short_name = 'f15'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orator of Ojutai'),
    (select id from sets where short_name = 'f15'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Path to Exile'),
    (select id from sets where short_name = 'f15'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Suspension Field'),
    (select id from sets where short_name = 'f15'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roast'),
    (select id from sets where short_name = 'f15'),
    '11',
    'rare'
) 
 on conflict do nothing;
