insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Knight'),
    (select id from sets where short_name = 'tcma'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'tcma'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tcma'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treefolk'),
    (select id from sets where short_name = 'tcma'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tcma'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kithkin Soldier'),
    (select id from sets where short_name = 'tcma'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tcma'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elf Druid'),
    (select id from sets where short_name = 'tcma'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'tcma'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Experience'),
    (select id from sets where short_name = 'tcma'),
    '0',
    'common'
) ,
(
    (select id from mtgcard where name = 'Germ'),
    (select id from sets where short_name = 'tcma'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spider'),
    (select id from sets where short_name = 'tcma'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elf Warrior'),
    (select id from sets where short_name = 'tcma'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drake'),
    (select id from sets where short_name = 'tcma'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tcma'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tcma'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elephant'),
    (select id from sets where short_name = 'tcma'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tcma'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tcma'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gargoyle'),
    (select id from sets where short_name = 'tcma'),
    '19',
    'common'
) 
 on conflict do nothing;
