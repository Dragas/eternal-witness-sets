insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Bat'),
    (select id from sets where short_name = 'tc17'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Spawn'),
    (select id from sets where short_name = 'tc17'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat Warrior'),
    (select id from sets where short_name = 'tc17'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire'),
    (select id from sets where short_name = 'tc17'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat Dragon'),
    (select id from sets where short_name = 'tc17'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tc17'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gold'),
    (select id from sets where short_name = 'tc17'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rat'),
    (select id from sets where short_name = 'tc17'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tc17'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tc17'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat'),
    (select id from sets where short_name = 'tc17'),
    '1',
    'common'
) 
 on conflict do nothing;
