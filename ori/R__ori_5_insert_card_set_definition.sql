insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Deep-Sea Terror'),
    (select id from sets where short_name = 'ori'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shadows of the Past'),
    (select id from sets where short_name = 'ori'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jace, Vryn''s Prodigy // Jace, Telepath Unbound'),
    (select id from sets where short_name = 'ori'),
    '60',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Battlefield Forge'),
    (select id from sets where short_name = 'ori'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kothophed, Soul Hoarder'),
    (select id from sets where short_name = 'ori'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ori'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of the White Orchid'),
    (select id from sets where short_name = 'ori'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Despoiler of Souls'),
    (select id from sets where short_name = 'ori'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ori'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weave Fate'),
    (select id from sets where short_name = 'ori'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runed Servitor'),
    (select id from sets where short_name = 'ori'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mighty Leap'),
    (select id from sets where short_name = 'ori'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murder Investigation'),
    (select id from sets where short_name = 'ori'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ramroller'),
    (select id from sets where short_name = 'ori'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sentinel of the Eternal Watch'),
    (select id from sets where short_name = 'ori'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nivix Barrier'),
    (select id from sets where short_name = 'ori'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Read the Bones'),
    (select id from sets where short_name = 'ori'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pia and Kiran Nalaar'),
    (select id from sets where short_name = 'ori'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Titan''s Strength'),
    (select id from sets where short_name = 'ori'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ori'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whirler Rogue'),
    (select id from sets where short_name = 'ori'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maritime Guard'),
    (select id from sets where short_name = 'ori'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mahamoti Djinn'),
    (select id from sets where short_name = 'ori'),
    '278',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Returned Centaur'),
    (select id from sets where short_name = 'ori'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fiery Conclusion'),
    (select id from sets where short_name = 'ori'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyromancer''s Goggles'),
    (select id from sets where short_name = 'ori'),
    '236',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Priest of the Blood Rite'),
    (select id from sets where short_name = 'ori'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ori'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anchor to the Aether'),
    (select id from sets where short_name = 'ori'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blessed Spirits'),
    (select id from sets where short_name = 'ori'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tragic Arrogance'),
    (select id from sets where short_name = 'ori'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Acolyte of the Inferno'),
    (select id from sets where short_name = 'ori'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = 'ori'),
    '282',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Managorger Hydra'),
    (select id from sets where short_name = 'ori'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Day''s Undoing'),
    (select id from sets where short_name = 'ori'),
    '51',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mage-Ring Bully'),
    (select id from sets where short_name = 'ori'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ringwarden Owl'),
    (select id from sets where short_name = 'ori'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aspiring Aeronaut'),
    (select id from sets where short_name = 'ori'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archangel of Tithes'),
    (select id from sets where short_name = 'ori'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Patron of the Valiant'),
    (select id from sets where short_name = 'ori'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Helm of the Gods'),
    (select id from sets where short_name = 'ori'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orchard Spirit'),
    (select id from sets where short_name = 'ori'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Miscreant'),
    (select id from sets where short_name = 'ori'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Outland Colossus'),
    (select id from sets where short_name = 'ori'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ori'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knightly Valor'),
    (select id from sets where short_name = 'ori'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Valor in Akros'),
    (select id from sets where short_name = 'ori'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Into the Void'),
    (select id from sets where short_name = 'ori'),
    '277',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meteorite'),
    (select id from sets where short_name = 'ori'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Willbreaker'),
    (select id from sets where short_name = 'ori'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Titanic Growth'),
    (select id from sets where short_name = 'ori'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alchemist''s Vial'),
    (select id from sets where short_name = 'ori'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bellows Lizard'),
    (select id from sets where short_name = 'ori'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aegis Angel'),
    (select id from sets where short_name = 'ori'),
    '273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ori'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadbridge Shaman'),
    (select id from sets where short_name = 'ori'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heavy Infantry'),
    (select id from sets where short_name = 'ori'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Calculated Dismissal'),
    (select id from sets where short_name = 'ori'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghirapur Aether Grid'),
    (select id from sets where short_name = 'ori'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guardians of Meletis'),
    (select id from sets where short_name = 'ori'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gideon''s Phalanx'),
    (select id from sets where short_name = 'ori'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Consul''s Lieutenant'),
    (select id from sets where short_name = 'ori'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shaman of the Pack'),
    (select id from sets where short_name = 'ori'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Akroan Sergeant'),
    (select id from sets where short_name = 'ori'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prickleboar'),
    (select id from sets where short_name = 'ori'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thornbow Archer'),
    (select id from sets where short_name = 'ori'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blazing Hellhound'),
    (select id from sets where short_name = 'ori'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jhessian Thief'),
    (select id from sets where short_name = 'ori'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avaricious Dragon'),
    (select id from sets where short_name = 'ori'),
    '131',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Enthralling Victor'),
    (select id from sets where short_name = 'ori'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llanowar Empath'),
    (select id from sets where short_name = 'ori'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Visionary'),
    (select id from sets where short_name = 'ori'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skaab Goliath'),
    (select id from sets where short_name = 'ori'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Call of the Full Moon'),
    (select id from sets where short_name = 'ori'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Undead Servant'),
    (select id from sets where short_name = 'ori'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anointer of Champions'),
    (select id from sets where short_name = 'ori'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Conclave Naturalists'),
    (select id from sets where short_name = 'ori'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sigil of the Empty Throne'),
    (select id from sets where short_name = 'ori'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scab-Clan Berserker'),
    (select id from sets where short_name = 'ori'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nantuko Husk'),
    (select id from sets where short_name = 'ori'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightsnare'),
    (select id from sets where short_name = 'ori'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necromantic Summons'),
    (select id from sets where short_name = 'ori'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mantle of Webs'),
    (select id from sets where short_name = 'ori'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Subterranean Scout'),
    (select id from sets where short_name = 'ori'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disciple of the Ring'),
    (select id from sets where short_name = 'ori'),
    '53',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'War Oracle'),
    (select id from sets where short_name = 'ori'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Consecrated by Blood'),
    (select id from sets where short_name = 'ori'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prism Ring'),
    (select id from sets where short_name = 'ori'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alhammarret, High Arbiter'),
    (select id from sets where short_name = 'ori'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exquisite Firecraft'),
    (select id from sets where short_name = 'ori'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yoked Ox'),
    (select id from sets where short_name = 'ori'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bonded Construct'),
    (select id from sets where short_name = 'ori'),
    '223',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kytheon''s Irregulars'),
    (select id from sets where short_name = 'ori'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'War Horn'),
    (select id from sets where short_name = 'ori'),
    '243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Somberwald Alpha'),
    (select id from sets where short_name = 'ori'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mage-Ring Network'),
    (select id from sets where short_name = 'ori'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Piledriver'),
    (select id from sets where short_name = 'ori'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Artificer''s Epiphany'),
    (select id from sets where short_name = 'ori'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Claustrophobia'),
    (select id from sets where short_name = 'ori'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx''s Tutelage'),
    (select id from sets where short_name = 'ori'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flesh to Dust'),
    (select id from sets where short_name = 'ori'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prized Unicorn'),
    (select id from sets where short_name = 'ori'),
    '287',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yeva''s Forcemage'),
    (select id from sets where short_name = 'ori'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Revenge'),
    (select id from sets where short_name = 'ori'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jayemdae Tome'),
    (select id from sets where short_name = 'ori'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reclusive Artificer'),
    (select id from sets where short_name = 'ori'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firefiend Elemental'),
    (select id from sets where short_name = 'ori'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter Spy Network'),
    (select id from sets where short_name = 'ori'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elemental Bond'),
    (select id from sets where short_name = 'ori'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Divine Verdict'),
    (select id from sets where short_name = 'ori'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Act of Treason'),
    (select id from sets where short_name = 'ori'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Totem-Guide Hartebeest'),
    (select id from sets where short_name = 'ori'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kytheon, Hero of Akros // Gideon, Battle-Forged'),
    (select id from sets where short_name = 'ori'),
    '23',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wild Instincts'),
    (select id from sets where short_name = 'ori'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Auramancer'),
    (select id from sets where short_name = 'ori'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unholy Hunger'),
    (select id from sets where short_name = 'ori'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fiery Impulse'),
    (select id from sets where short_name = 'ori'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravaging Blaze'),
    (select id from sets where short_name = 'ori'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Throwing Knife'),
    (select id from sets where short_name = 'ori'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Relic Seeker'),
    (select id from sets where short_name = 'ori'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Watercourser'),
    (select id from sets where short_name = 'ori'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood-Cursed Knight'),
    (select id from sets where short_name = 'ori'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rhox Maulers'),
    (select id from sets where short_name = 'ori'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terra Stomper'),
    (select id from sets where short_name = 'ori'),
    '288',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smash to Smithereens'),
    (select id from sets where short_name = 'ori'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Joraga Invocation'),
    (select id from sets where short_name = 'ori'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bounding Krasis'),
    (select id from sets where short_name = 'ori'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Embermaw Hellion'),
    (select id from sets where short_name = 'ori'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clash of Wills'),
    (select id from sets where short_name = 'ori'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Psychic Rebuttal'),
    (select id from sets where short_name = 'ori'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grasp of the Hieromancer'),
    (select id from sets where short_name = 'ori'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tormented Thoughts'),
    (select id from sets where short_name = 'ori'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jace''s Sanctum'),
    (select id from sets where short_name = 'ori'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Possessed Skaab'),
    (select id from sets where short_name = 'ori'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Malakir Cullblade'),
    (select id from sets where short_name = 'ori'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alhammarret''s Archive'),
    (select id from sets where short_name = 'ori'),
    '221',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nissa''s Revelation'),
    (select id from sets where short_name = 'ori'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infinite Obliteration'),
    (select id from sets where short_name = 'ori'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vastwood Gorger'),
    (select id from sets where short_name = 'ori'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rabid Bloodsucker'),
    (select id from sets where short_name = 'ori'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foundry of the Consuls'),
    (select id from sets where short_name = 'ori'),
    '247',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mizzium Meddler'),
    (select id from sets where short_name = 'ori'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ori'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enlightened Ascetic'),
    (select id from sets where short_name = 'ori'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Honored Hierarch'),
    (select id from sets where short_name = 'ori'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ori'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hangarback Walker'),
    (select id from sets where short_name = 'ori'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Coast'),
    (select id from sets where short_name = 'ori'),
    '252',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mage-Ring Responder'),
    (select id from sets where short_name = 'ori'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Ignition'),
    (select id from sets where short_name = 'ori'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guardian Automaton'),
    (select id from sets where short_name = 'ori'),
    '227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eyeblight Assassin'),
    (select id from sets where short_name = 'ori'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Rambler'),
    (select id from sets where short_name = 'ori'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nissa''s Pilgrimage'),
    (select id from sets where short_name = 'ori'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ampryn Tactician'),
    (select id from sets where short_name = 'ori'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demonic Pact'),
    (select id from sets where short_name = 'ori'),
    '92',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Graveblade Marauder'),
    (select id from sets where short_name = 'ori'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Starfield of Nyx'),
    (select id from sets where short_name = 'ori'),
    '33',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Undercity Troll'),
    (select id from sets where short_name = 'ori'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reclaim'),
    (select id from sets where short_name = 'ori'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ori'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fetid Imp'),
    (select id from sets where short_name = 'ori'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = 'ori'),
    '283',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fiery Hellhound'),
    (select id from sets where short_name = 'ori'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Celestial Flare'),
    (select id from sets where short_name = 'ori'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eagle of the Watch'),
    (select id from sets where short_name = 'ori'),
    '275',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akroan Jailer'),
    (select id from sets where short_name = 'ori'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swift Reckoning'),
    (select id from sets where short_name = 'ori'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dwynen, Gilt-Leaf Daen'),
    (select id from sets where short_name = 'ori'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Llanowar Wastes'),
    (select id from sets where short_name = 'ori'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stratus Walk'),
    (select id from sets where short_name = 'ori'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harbinger of the Tides'),
    (select id from sets where short_name = 'ori'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hallowed Moonlight'),
    (select id from sets where short_name = 'ori'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Messenger'),
    (select id from sets where short_name = 'ori'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leaf Gilder'),
    (select id from sets where short_name = 'ori'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Healing Hands'),
    (select id from sets where short_name = 'ori'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gather the Pack'),
    (select id from sets where short_name = 'ori'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Citadel Castellan'),
    (select id from sets where short_name = 'ori'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Veteran''s Sidearm'),
    (select id from sets where short_name = 'ori'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boggart Brute'),
    (select id from sets where short_name = 'ori'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = 'ori'),
    '281',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fleshbag Marauder'),
    (select id from sets where short_name = 'ori'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana, Heretical Healer // Liliana, Defiant Necromancer'),
    (select id from sets where short_name = 'ori'),
    '106',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vryn Wingmare'),
    (select id from sets where short_name = 'ori'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thopter Engineer'),
    (select id from sets where short_name = 'ori'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seismic Elemental'),
    (select id from sets where short_name = 'ori'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ori'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woodland Bellower'),
    (select id from sets where short_name = 'ori'),
    '207',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cruel Revival'),
    (select id from sets where short_name = 'ori'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Screeching Skaab'),
    (select id from sets where short_name = 'ori'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Send to Sleep'),
    (select id from sets where short_name = 'ori'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Displacement Wave'),
    (select id from sets where short_name = 'ori'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demolish'),
    (select id from sets where short_name = 'ori'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Battle Priest'),
    (select id from sets where short_name = 'ori'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kytheon''s Tactics'),
    (select id from sets where short_name = 'ori'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enshrouding Mist'),
    (select id from sets where short_name = 'ori'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra, Fire of Kaladesh // Chandra, Roaring Flame'),
    (select id from sets where short_name = 'ori'),
    '135',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'ori'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Fodder'),
    (select id from sets where short_name = 'ori'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Great Aurora'),
    (select id from sets where short_name = 'ori'),
    '179',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dark Dabbling'),
    (select id from sets where short_name = 'ori'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flameshadow Conjuring'),
    (select id from sets where short_name = 'ori'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ori'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ori'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ori'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = 'ori'),
    '285',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blightcaster'),
    (select id from sets where short_name = 'ori'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Macabre Waltz'),
    (select id from sets where short_name = 'ori'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ori'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of the Pilgrim''s Road'),
    (select id from sets where short_name = 'ori'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Topan Freeblade'),
    (select id from sets where short_name = 'ori'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stalwart Aven'),
    (select id from sets where short_name = 'ori'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hitchclaw Recluse'),
    (select id from sets where short_name = 'ori'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Touch of Moonglove'),
    (select id from sets where short_name = 'ori'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iroas''s Champion'),
    (select id from sets where short_name = 'ori'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Javelin'),
    (select id from sets where short_name = 'ori'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwynen''s Elite'),
    (select id from sets where short_name = 'ori'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evolutionary Leap'),
    (select id from sets where short_name = 'ori'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orbs of Warding'),
    (select id from sets where short_name = 'ori'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Might of the Masses'),
    (select id from sets where short_name = 'ori'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreadwaters'),
    (select id from sets where short_name = 'ori'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aerial Volley'),
    (select id from sets where short_name = 'ori'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talent of the Telepath'),
    (select id from sets where short_name = 'ori'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyraker Giant'),
    (select id from sets where short_name = 'ori'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ori'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tower Geist'),
    (select id from sets where short_name = 'ori'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Revenant'),
    (select id from sets where short_name = 'ori'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gilt-Leaf Winnower'),
    (select id from sets where short_name = 'ori'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gold-Forged Sentinel'),
    (select id from sets where short_name = 'ori'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Caves of Koilos'),
    (select id from sets where short_name = 'ori'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abbot of Keral Keep'),
    (select id from sets where short_name = 'ori'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tainted Remedy'),
    (select id from sets where short_name = 'ori'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cleric of the Forward Order'),
    (select id from sets where short_name = 'ori'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hixus, Prison Warden'),
    (select id from sets where short_name = 'ori'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infernal Scarring'),
    (select id from sets where short_name = 'ori'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vine Snare'),
    (select id from sets where short_name = 'ori'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Fury'),
    (select id from sets where short_name = 'ori'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Catacomb Slug'),
    (select id from sets where short_name = 'ori'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Suppression Bonds'),
    (select id from sets where short_name = 'ori'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Languish'),
    (select id from sets where short_name = 'ori'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ori'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sigiled Starfish'),
    (select id from sets where short_name = 'ori'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eyeblight Massacre'),
    (select id from sets where short_name = 'ori'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'ori'),
    '276',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Charging Griffin'),
    (select id from sets where short_name = 'ori'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molten Vortex'),
    (select id from sets where short_name = 'ori'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brawler''s Plate'),
    (select id from sets where short_name = 'ori'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reave Soul'),
    (select id from sets where short_name = 'ori'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bone to Ash'),
    (select id from sets where short_name = 'ori'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunderclap Wyvern'),
    (select id from sets where short_name = 'ori'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gnarlroot Trapper'),
    (select id from sets where short_name = 'ori'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ori'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Glory Chaser'),
    (select id from sets where short_name = 'ori'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soulblade Djinn'),
    (select id from sets where short_name = 'ori'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Weight of the Underworld'),
    (select id from sets where short_name = 'ori'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Animist''s Awakening'),
    (select id from sets where short_name = 'ori'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Separatist Voidmage'),
    (select id from sets where short_name = 'ori'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Valeron Wardens'),
    (select id from sets where short_name = 'ori'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ori'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'ori'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rogue''s Passage'),
    (select id from sets where short_name = 'ori'),
    '250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Petition'),
    (select id from sets where short_name = 'ori'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zendikar Incarnate'),
    (select id from sets where short_name = 'ori'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ori'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infectious Bloodlust'),
    (select id from sets where short_name = 'ori'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hydrolash'),
    (select id from sets where short_name = 'ori'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magmatic Insight'),
    (select id from sets where short_name = 'ori'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Caustic Caterpillar'),
    (select id from sets where short_name = 'ori'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghirapur Gearcrafter'),
    (select id from sets where short_name = 'ori'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pharika''s Disciple'),
    (select id from sets where short_name = 'ori'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sigil of Valor'),
    (select id from sets where short_name = 'ori'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Turn to Frog'),
    (select id from sets where short_name = 'ori'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skysnare Spider'),
    (select id from sets where short_name = 'ori'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Reef'),
    (select id from sets where short_name = 'ori'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ori'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chief of the Foundry'),
    (select id from sets where short_name = 'ori'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Timberpack Wolf'),
    (select id from sets where short_name = 'ori'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sword of the Animist'),
    (select id from sets where short_name = 'ori'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Erebos''s Titan'),
    (select id from sets where short_name = 'ori'),
    '94',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scrapskin Drake'),
    (select id from sets where short_name = 'ori'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zendikar''s Roil'),
    (select id from sets where short_name = 'ori'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disperse'),
    (select id from sets where short_name = 'ori'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plummet'),
    (select id from sets where short_name = 'ori'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shambling Ghoul'),
    (select id from sets where short_name = 'ori'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nissa, Vastwood Seer // Nissa, Sage Animist'),
    (select id from sets where short_name = 'ori'),
    '189',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cobblebrute'),
    (select id from sets where short_name = 'ori'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Herald of the Pantheon'),
    (select id from sets where short_name = 'ori'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel''s Tomb'),
    (select id from sets where short_name = 'ori'),
    '222',
    'uncommon'
) 
 on conflict do nothing;
