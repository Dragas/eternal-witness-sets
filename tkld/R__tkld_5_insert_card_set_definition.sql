insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tkld'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra, Torch of Defiance Emblem'),
    (select id from sets where short_name = 'tkld'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nissa, Vital Force Emblem'),
    (select id from sets where short_name = 'tkld'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Servo'),
    (select id from sets where short_name = 'tkld'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Construct'),
    (select id from sets where short_name = 'tkld'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter'),
    (select id from sets where short_name = 'tkld'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter'),
    (select id from sets where short_name = 'tkld'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Energy Reserve'),
    (select id from sets where short_name = 'tkld'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Construct'),
    (select id from sets where short_name = 'tkld'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dovin Baan Emblem'),
    (select id from sets where short_name = 'tkld'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Servo'),
    (select id from sets where short_name = 'tkld'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Servo'),
    (select id from sets where short_name = 'tkld'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter'),
    (select id from sets where short_name = 'tkld'),
    '7',
    'common'
) 
 on conflict do nothing;
