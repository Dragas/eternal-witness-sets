    insert into mtgcard(name) values ('Beast') on conflict do nothing;
    insert into mtgcard(name) values ('Chandra, Torch of Defiance Emblem') on conflict do nothing;
    insert into mtgcard(name) values ('Nissa, Vital Force Emblem') on conflict do nothing;
    insert into mtgcard(name) values ('Servo') on conflict do nothing;
    insert into mtgcard(name) values ('Construct') on conflict do nothing;
    insert into mtgcard(name) values ('Thopter') on conflict do nothing;
    insert into mtgcard(name) values ('Energy Reserve') on conflict do nothing;
    insert into mtgcard(name) values ('Dovin Baan Emblem') on conflict do nothing;
