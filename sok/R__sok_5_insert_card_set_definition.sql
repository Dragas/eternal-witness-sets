insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Celestial Kirin'),
    (select id from sets where short_name = 'sok'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evermind'),
    (select id from sets where short_name = 'sok'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barrel Down Sokenzan'),
    (select id from sets where short_name = 'sok'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Secretkeeper'),
    (select id from sets where short_name = 'sok'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghost-Lit Warder'),
    (select id from sets where short_name = 'sok'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Path of Anger''s Flame'),
    (select id from sets where short_name = 'sok'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Choice of Damnations'),
    (select id from sets where short_name = 'sok'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cut the Earthly Bond'),
    (select id from sets where short_name = 'sok'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akuta, Born of Ash'),
    (select id from sets where short_name = 'sok'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathknell Kami'),
    (select id from sets where short_name = 'sok'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erayo, Soratami Ascendant // Erayo''s Essence'),
    (select id from sets where short_name = 'sok'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cowed by Wisdom'),
    (select id from sets where short_name = 'sok'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shifting Borders'),
    (select id from sets where short_name = 'sok'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghost-Lit Stalker'),
    (select id from sets where short_name = 'sok'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skull Collector'),
    (select id from sets where short_name = 'sok'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Akki Underling'),
    (select id from sets where short_name = 'sok'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plow Through Reito'),
    (select id from sets where short_name = 'sok'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Promise of Bunrei'),
    (select id from sets where short_name = 'sok'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twincast'),
    (select id from sets where short_name = 'sok'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ideas Unbound'),
    (select id from sets where short_name = 'sok'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bounteous Kirin'),
    (select id from sets where short_name = 'sok'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kuro''s Taken'),
    (select id from sets where short_name = 'sok'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sokenzan Renegade'),
    (select id from sets where short_name = 'sok'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feral Lightning'),
    (select id from sets where short_name = 'sok'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'O-Naginata'),
    (select id from sets where short_name = 'sok'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maga, Traitor to Mortals'),
    (select id from sets where short_name = 'sok'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soratami Cloud Chariot'),
    (select id from sets where short_name = 'sok'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arashi, the Sky Asunder'),
    (select id from sets where short_name = 'sok'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Clock'),
    (select id from sets where short_name = 'sok'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thoughts of Ruin'),
    (select id from sets where short_name = 'sok'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Masumaro, First to Live'),
    (select id from sets where short_name = 'sok'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyfire Kirin'),
    (select id from sets where short_name = 'sok'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hidetsugu''s Second Rite'),
    (select id from sets where short_name = 'sok'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Okina Nightwatch'),
    (select id from sets where short_name = 'sok'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kami of Empty Graves'),
    (select id from sets where short_name = 'sok'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eternal Dominion'),
    (select id from sets where short_name = 'sok'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rending Vines'),
    (select id from sets where short_name = 'sok'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Measure of Wickedness'),
    (select id from sets where short_name = 'sok'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Descendant of Kiyomaro'),
    (select id from sets where short_name = 'sok'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scroll of Origins'),
    (select id from sets where short_name = 'sok'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kiku''s Shadow'),
    (select id from sets where short_name = 'sok'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ebony Owl Netsuke'),
    (select id from sets where short_name = 'sok'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kiyomaro, First to Stand'),
    (select id from sets where short_name = 'sok'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ronin Cavekeeper'),
    (select id from sets where short_name = 'sok'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Neverending Torment'),
    (select id from sets where short_name = 'sok'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inner Calm, Outer Strength'),
    (select id from sets where short_name = 'sok'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kuon, Ogre Ascendant // Kuon''s Essence'),
    (select id from sets where short_name = 'sok'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghost-Lit Raider'),
    (select id from sets where short_name = 'sok'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cloudhoof Kirin'),
    (select id from sets where short_name = 'sok'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rune-Tail, Kitsune Ascendant // Rune-Tail''s Essence'),
    (select id from sets where short_name = 'sok'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaze of Adamaro'),
    (select id from sets where short_name = 'sok'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunder from Within'),
    (select id from sets where short_name = 'sok'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stampeding Serow'),
    (select id from sets where short_name = 'sok'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seek the Horizon'),
    (select id from sets where short_name = 'sok'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inner-Chamber Guard'),
    (select id from sets where short_name = 'sok'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reverence'),
    (select id from sets where short_name = 'sok'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exile into Darkness'),
    (select id from sets where short_name = 'sok'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Iname as One'),
    (select id from sets where short_name = 'sok'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kemuri-Onna'),
    (select id from sets where short_name = 'sok'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kagemaro''s Clutch'),
    (select id from sets where short_name = 'sok'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sokenzan Spellblade'),
    (select id from sets where short_name = 'sok'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rushing-Tide Zubera'),
    (select id from sets where short_name = 'sok'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elder Pine of Jukai'),
    (select id from sets where short_name = 'sok'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Miren, the Moaning Well'),
    (select id from sets where short_name = 'sok'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Captive Flame'),
    (select id from sets where short_name = 'sok'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Locust Miser'),
    (select id from sets where short_name = 'sok'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightsoil Kami'),
    (select id from sets where short_name = 'sok'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'One with Nothing'),
    (select id from sets where short_name = 'sok'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wine of Blood and Iron'),
    (select id from sets where short_name = 'sok'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Undying Flames'),
    (select id from sets where short_name = 'sok'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sakashima the Impostor'),
    (select id from sets where short_name = 'sok'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yuki-Onna'),
    (select id from sets where short_name = 'sok'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shinen of Stars'' Light'),
    (select id from sets where short_name = 'sok'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shinen of Fury''s Fire'),
    (select id from sets where short_name = 'sok'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shinen of Flight''s Wings'),
    (select id from sets where short_name = 'sok'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jiwari, the Earth Aflame'),
    (select id from sets where short_name = 'sok'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pain''s Reward'),
    (select id from sets where short_name = 'sok'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oni of Wild Places'),
    (select id from sets where short_name = 'sok'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meishin, the Mind Cage'),
    (select id from sets where short_name = 'sok'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spiraling Embers'),
    (select id from sets where short_name = 'sok'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathmask Nezumi'),
    (select id from sets where short_name = 'sok'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kagemaro, First to Suffer'),
    (select id from sets where short_name = 'sok'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shinen of Fear''s Chill'),
    (select id from sets where short_name = 'sok'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pithing Needle'),
    (select id from sets where short_name = 'sok'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Promised Kannushi'),
    (select id from sets where short_name = 'sok'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enduring Ideal'),
    (select id from sets where short_name = 'sok'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mikokoro, Center of the Sea'),
    (select id from sets where short_name = 'sok'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akki Drillmaster'),
    (select id from sets where short_name = 'sok'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Shockwave'),
    (select id from sets where short_name = 'sok'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Homura, Human Ascendant // Homura''s Essence'),
    (select id from sets where short_name = 'sok'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reki, the History of Kamigawa'),
    (select id from sets where short_name = 'sok'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Briarknit Kami'),
    (select id from sets where short_name = 'sok'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death Denied'),
    (select id from sets where short_name = 'sok'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tomb of Urami'),
    (select id from sets where short_name = 'sok'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Razorjaw Oni'),
    (select id from sets where short_name = 'sok'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sekki, Seasons'' Guide'),
    (select id from sets where short_name = 'sok'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Michiko Konda, Truth Seeker'),
    (select id from sets where short_name = 'sok'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Manriki-Gusari'),
    (select id from sets where short_name = 'sok'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Scout'),
    (select id from sets where short_name = 'sok'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iizuka the Ruthless'),
    (select id from sets where short_name = 'sok'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dense Canopy'),
    (select id from sets where short_name = 'sok'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oboro Envoy'),
    (select id from sets where short_name = 'sok'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glitterfang'),
    (select id from sets where short_name = 'sok'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death of a Thousand Stings'),
    (select id from sets where short_name = 'sok'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oboro Breezecaller'),
    (select id from sets where short_name = 'sok'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raving Oni-Slave'),
    (select id from sets where short_name = 'sok'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kataki, War''s Wage'),
    (select id from sets where short_name = 'sok'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shinen of Life''s Roar'),
    (select id from sets where short_name = 'sok'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kitsune Bonesetter'),
    (select id from sets where short_name = 'sok'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murmurs from Beyond'),
    (select id from sets where short_name = 'sok'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghost-Lit Redeemer'),
    (select id from sets where short_name = 'sok'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oppressive Will'),
    (select id from sets where short_name = 'sok'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burning-Eye Zubera'),
    (select id from sets where short_name = 'sok'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kitsune Dawnblade'),
    (select id from sets where short_name = 'sok'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inner Fire'),
    (select id from sets where short_name = 'sok'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pure Intentions'),
    (select id from sets where short_name = 'sok'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rally the Horde'),
    (select id from sets where short_name = 'sok'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Minamo Scrollkeeper'),
    (select id from sets where short_name = 'sok'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Descendant of Soramaro'),
    (select id from sets where short_name = 'sok'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dosan''s Oldest Chant'),
    (select id from sets where short_name = 'sok'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Descendant of Masumaro'),
    (select id from sets where short_name = 'sok'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Torii Watchward'),
    (select id from sets where short_name = 'sok'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreamcatcher'),
    (select id from sets where short_name = 'sok'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nikko-Onna'),
    (select id from sets where short_name = 'sok'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kami of the Tended Garden'),
    (select id from sets where short_name = 'sok'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kitsune Loreweaver'),
    (select id from sets where short_name = 'sok'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hail of Arrows'),
    (select id from sets where short_name = 'sok'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spiritual Visit'),
    (select id from sets where short_name = 'sok'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infernal Kirin'),
    (select id from sets where short_name = 'sok'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Into the Fray'),
    (select id from sets where short_name = 'sok'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Footsteps of the Goryo'),
    (select id from sets where short_name = 'sok'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overwhelming Intellect'),
    (select id from sets where short_name = 'sok'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Haru-Onna'),
    (select id from sets where short_name = 'sok'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kashi-Tribe Elite'),
    (select id from sets where short_name = 'sok'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sasaya, Orochi Ascendant // Sasaya''s Essence'),
    (select id from sets where short_name = 'sok'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Adamaro, First to Desire'),
    (select id from sets where short_name = 'sok'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sink into Takenuma'),
    (select id from sets where short_name = 'sok'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashes of the Fallen'),
    (select id from sets where short_name = 'sok'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaho, Minamo Historian'),
    (select id from sets where short_name = 'sok'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shape Stealer'),
    (select id from sets where short_name = 'sok'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kiri-Onna'),
    (select id from sets where short_name = 'sok'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fiddlehead Kami'),
    (select id from sets where short_name = 'sok'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hand of Honor'),
    (select id from sets where short_name = 'sok'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ivory Crane Netsuke'),
    (select id from sets where short_name = 'sok'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soramaro, First to Dream'),
    (select id from sets where short_name = 'sok'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Freed from the Real'),
    (select id from sets where short_name = 'sok'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seed the Land'),
    (select id from sets where short_name = 'sok'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hand of Cruelty'),
    (select id from sets where short_name = 'sok'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trusted Advisor'),
    (select id from sets where short_name = 'sok'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kami of the Crescent Moon'),
    (select id from sets where short_name = 'sok'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ayumi, the Last Visitor'),
    (select id from sets where short_name = 'sok'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Molting Skin'),
    (select id from sets where short_name = 'sok'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Matsu-Tribe Birdstalker'),
    (select id from sets where short_name = 'sok'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gnat Miser'),
    (select id from sets where short_name = 'sok'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moonbow Illusionist'),
    (select id from sets where short_name = 'sok'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghost-Lit Nourisher'),
    (select id from sets where short_name = 'sok'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moonwing Moth'),
    (select id from sets where short_name = 'sok'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Endless Swarm'),
    (select id from sets where short_name = 'sok'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Presence of the Wise'),
    (select id from sets where short_name = 'sok'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Araba Mothrider'),
    (select id from sets where short_name = 'sok'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Godo''s Irregulars'),
    (select id from sets where short_name = 'sok'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Curtain of Light'),
    (select id from sets where short_name = 'sok'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charge Across the Araba'),
    (select id from sets where short_name = 'sok'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eiganjo Free-Riders'),
    (select id from sets where short_name = 'sok'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oboro, Palace in the Clouds'),
    (select id from sets where short_name = 'sok'),
    '164',
    'rare'
) 
 on conflict do nothing;
