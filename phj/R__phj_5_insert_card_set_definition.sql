insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Surge of Strength'),
    (select id from sets where short_name = 'phj'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Mutant'),
    (select id from sets where short_name = 'phj'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ihsan''s Shade'),
    (select id from sets where short_name = 'phj'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krovikan Vampire'),
    (select id from sets where short_name = 'phj'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Ants'),
    (select id from sets where short_name = 'phj'),
    '5',
    'rare'
) 
 on conflict do nothing;
