    insert into types (name) values('Instant') on conflict do nothing;
    insert into types (name) values('Creature') on conflict do nothing;
    insert into types (name) values('Goblin') on conflict do nothing;
    insert into types (name) values('Mutant') on conflict do nothing;
    insert into types (name) values('Legendary') on conflict do nothing;
    insert into types (name) values('Creature') on conflict do nothing;
    insert into types (name) values('Shade') on conflict do nothing;
    insert into types (name) values('Knight') on conflict do nothing;
    insert into types (name) values('Creature') on conflict do nothing;
    insert into types (name) values('Vampire') on conflict do nothing;
    insert into types (name) values('Creature') on conflict do nothing;
    insert into types (name) values('Insect') on conflict do nothing;
