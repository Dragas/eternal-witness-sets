    insert into mtgcard(name) values ('Surge of Strength') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Mutant') on conflict do nothing;
    insert into mtgcard(name) values ('Ihsan''s Shade') on conflict do nothing;
    insert into mtgcard(name) values ('Krovikan Vampire') on conflict do nothing;
    insert into mtgcard(name) values ('Yavimaya Ants') on conflict do nothing;
