insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Spiteful Returned'),
    (select id from sets where short_name = 'bng'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dawn to Dusk'),
    (select id from sets where short_name = 'bng'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Peregrination'),
    (select id from sets where short_name = 'bng'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Setessan Starbreaker'),
    (select id from sets where short_name = 'bng'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forsaken Drifters'),
    (select id from sets where short_name = 'bng'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oreskos Sun Guide'),
    (select id from sets where short_name = 'bng'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aerie Worshippers'),
    (select id from sets where short_name = 'bng'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snake of the Golden Grove'),
    (select id from sets where short_name = 'bng'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marshmist Titan'),
    (select id from sets where short_name = 'bng'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reap What Is Sown'),
    (select id from sets where short_name = 'bng'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kraken of the Straits'),
    (select id from sets where short_name = 'bng'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mortal''s Resolve'),
    (select id from sets where short_name = 'bng'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archetype of Aggression'),
    (select id from sets where short_name = 'bng'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Satyr Wayfinder'),
    (select id from sets where short_name = 'bng'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swordwise Centaur'),
    (select id from sets where short_name = 'bng'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silent Sentinel'),
    (select id from sets where short_name = 'bng'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reckless Reveler'),
    (select id from sets where short_name = 'bng'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aspect of Hydra'),
    (select id from sets where short_name = 'bng'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forlorn Pseudamma'),
    (select id from sets where short_name = 'bng'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shrike Harpy'),
    (select id from sets where short_name = 'bng'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plea for Guidance'),
    (select id from sets where short_name = 'bng'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ragemonger'),
    (select id from sets where short_name = 'bng'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scouring Sands'),
    (select id from sets where short_name = 'bng'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drown in Sorrow'),
    (select id from sets where short_name = 'bng'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mogis, God of Slaughter'),
    (select id from sets where short_name = 'bng'),
    '151',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Archetype of Endurance'),
    (select id from sets where short_name = 'bng'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Xenagos, God of Revels'),
    (select id from sets where short_name = 'bng'),
    '156',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Arbiter of the Ideal'),
    (select id from sets where short_name = 'bng'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Champion of Stray Souls'),
    (select id from sets where short_name = 'bng'),
    '63',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fanatic of Xenagos'),
    (select id from sets where short_name = 'bng'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Satyr Nyx-Smith'),
    (select id from sets where short_name = 'bng'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bile Blight'),
    (select id from sets where short_name = 'bng'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Archetype of Imagination'),
    (select id from sets where short_name = 'bng'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyreaping'),
    (select id from sets where short_name = 'bng'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Graverobber Spider'),
    (select id from sets where short_name = 'bng'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stratus Walk'),
    (select id from sets where short_name = 'bng'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Felhide Brawler'),
    (select id from sets where short_name = 'bng'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deepwater Hypnotist'),
    (select id from sets where short_name = 'bng'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eidolon of Countless Battles'),
    (select id from sets where short_name = 'bng'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bolt of Keranos'),
    (select id from sets where short_name = 'bng'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grisly Transformation'),
    (select id from sets where short_name = 'bng'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forgestoker Dragon'),
    (select id from sets where short_name = 'bng'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Retraction Helix'),
    (select id from sets where short_name = 'bng'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple of Malice'),
    (select id from sets where short_name = 'bng'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oracle''s Insight'),
    (select id from sets where short_name = 'bng'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unravel the Aether'),
    (select id from sets where short_name = 'bng'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kragma Butcher'),
    (select id from sets where short_name = 'bng'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Revoke Existence'),
    (select id from sets where short_name = 'bng'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Siren of the Fanged Coast'),
    (select id from sets where short_name = 'bng'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Springleaf Drum'),
    (select id from sets where short_name = 'bng'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vortex Elemental'),
    (select id from sets where short_name = 'bng'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Charging Badger'),
    (select id from sets where short_name = 'bng'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Oak of Odunos'),
    (select id from sets where short_name = 'bng'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fated Intervention'),
    (select id from sets where short_name = 'bng'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nyxborn Shieldmate'),
    (select id from sets where short_name = 'bng'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nessian Wilds Ravager'),
    (select id from sets where short_name = 'bng'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nyxborn Triton'),
    (select id from sets where short_name = 'bng'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divination'),
    (select id from sets where short_name = 'bng'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fated Conflagration'),
    (select id from sets where short_name = 'bng'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ephara, God of the Polis'),
    (select id from sets where short_name = 'bng'),
    '145',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tromokratis'),
    (select id from sets where short_name = 'bng'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Herald of Torment'),
    (select id from sets where short_name = 'bng'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sudden Storm'),
    (select id from sets where short_name = 'bng'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ephara''s Enlightenment'),
    (select id from sets where short_name = 'bng'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chorus of the Tides'),
    (select id from sets where short_name = 'bng'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Great Hart'),
    (select id from sets where short_name = 'bng'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cyclops of One-Eyed Pass'),
    (select id from sets where short_name = 'bng'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghostblade Eidolon'),
    (select id from sets where short_name = 'bng'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karametra, God of Harvests'),
    (select id from sets where short_name = 'bng'),
    '148',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Warchanter of Mogis'),
    (select id from sets where short_name = 'bng'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nyxborn Eidolon'),
    (select id from sets where short_name = 'bng'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hero of Iroas'),
    (select id from sets where short_name = 'bng'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Floodtide Serpent'),
    (select id from sets where short_name = 'bng'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Excoriate'),
    (select id from sets where short_name = 'bng'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Satyr Firedancer'),
    (select id from sets where short_name = 'bng'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pheres-Band Raiders'),
    (select id from sets where short_name = 'bng'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fated Retribution'),
    (select id from sets where short_name = 'bng'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thassa''s Rebuff'),
    (select id from sets where short_name = 'bng'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashiok''s Adept'),
    (select id from sets where short_name = 'bng'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mindreaver'),
    (select id from sets where short_name = 'bng'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Asphyxiate'),
    (select id from sets where short_name = 'bng'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gild'),
    (select id from sets where short_name = 'bng'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pain Seer'),
    (select id from sets where short_name = 'bng'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chromanticore'),
    (select id from sets where short_name = 'bng'),
    '144',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Phenax, God of Deception'),
    (select id from sets where short_name = 'bng'),
    '152',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Archetype of Courage'),
    (select id from sets where short_name = 'bng'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nyxborn Wolf'),
    (select id from sets where short_name = 'bng'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eye Gouge'),
    (select id from sets where short_name = 'bng'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elite Skirmisher'),
    (select id from sets where short_name = 'bng'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'God-Favored General'),
    (select id from sets where short_name = 'bng'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Siren of the Silent Song'),
    (select id from sets where short_name = 'bng'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flitterstep Eidolon'),
    (select id from sets where short_name = 'bng'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mischief and Mayhem'),
    (select id from sets where short_name = 'bng'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mortal''s Ardor'),
    (select id from sets where short_name = 'bng'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fated Infatuation'),
    (select id from sets where short_name = 'bng'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pharagax Giant'),
    (select id from sets where short_name = 'bng'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunbond'),
    (select id from sets where short_name = 'bng'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Everflame Eidolon'),
    (select id from sets where short_name = 'bng'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necrobite'),
    (select id from sets where short_name = 'bng'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pheres-Band Tromper'),
    (select id from sets where short_name = 'bng'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flame-Wreathed Phoenix'),
    (select id from sets where short_name = 'bng'),
    '97',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Heroes'' Podium'),
    (select id from sets where short_name = 'bng'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Claim of Erebos'),
    (select id from sets where short_name = 'bng'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fated Return'),
    (select id from sets where short_name = 'bng'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Odunos River Trawler'),
    (select id from sets where short_name = 'bng'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Astral Cornucopia'),
    (select id from sets where short_name = 'bng'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whims of the Fates'),
    (select id from sets where short_name = 'bng'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nullify'),
    (select id from sets where short_name = 'bng'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weight of the Underworld'),
    (select id from sets where short_name = 'bng'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vanguard of Brimaz'),
    (select id from sets where short_name = 'bng'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meletis Astronomer'),
    (select id from sets where short_name = 'bng'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Temple of Plenty'),
    (select id from sets where short_name = 'bng'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nyxborn Rollicker'),
    (select id from sets where short_name = 'bng'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pinnacle of Rage'),
    (select id from sets where short_name = 'bng'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Volley'),
    (select id from sets where short_name = 'bng'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Griffin Dreamfinder'),
    (select id from sets where short_name = 'bng'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx''s Disciple'),
    (select id from sets where short_name = 'bng'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scourge of Skola Vale'),
    (select id from sets where short_name = 'bng'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Searing Blood'),
    (select id from sets where short_name = 'bng'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Felhide Spiritbinder'),
    (select id from sets where short_name = 'bng'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karametra''s Favor'),
    (select id from sets where short_name = 'bng'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raised by Wolves'),
    (select id from sets where short_name = 'bng'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crypsis'),
    (select id from sets where short_name = 'bng'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunder Brute'),
    (select id from sets where short_name = 'bng'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rise to the Challenge'),
    (select id from sets where short_name = 'bng'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple of Enlightenment'),
    (select id from sets where short_name = 'bng'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fearsome Temper'),
    (select id from sets where short_name = 'bng'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Noble Quarry'),
    (select id from sets where short_name = 'bng'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Acolyte''s Reward'),
    (select id from sets where short_name = 'bng'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hunter''s Prowess'),
    (select id from sets where short_name = 'bng'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Siren Song Lyre'),
    (select id from sets where short_name = 'bng'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thunderous Might'),
    (select id from sets where short_name = 'bng'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evanescent Intellect'),
    (select id from sets where short_name = 'bng'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormcaller of Keranos'),
    (select id from sets where short_name = 'bng'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Culling Mark'),
    (select id from sets where short_name = 'bng'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akroan Skyguard'),
    (select id from sets where short_name = 'bng'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gorgon''s Head'),
    (select id from sets where short_name = 'bng'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Courser of Kruphix'),
    (select id from sets where short_name = 'bng'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Epiphany Storm'),
    (select id from sets where short_name = 'bng'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oracle of Bones'),
    (select id from sets where short_name = 'bng'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akroan Conscriptor'),
    (select id from sets where short_name = 'bng'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Servant of Tymaret'),
    (select id from sets where short_name = 'bng'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ephara''s Radiance'),
    (select id from sets where short_name = 'bng'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pillar of War'),
    (select id from sets where short_name = 'bng'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Setessan Oathsworn'),
    (select id from sets where short_name = 'bng'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loyal Pegasus'),
    (select id from sets where short_name = 'bng'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Impetuous Sunchaser'),
    (select id from sets where short_name = 'bng'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kiora''s Follower'),
    (select id from sets where short_name = 'bng'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fall of the Hammer'),
    (select id from sets where short_name = 'bng'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nessian Demolok'),
    (select id from sets where short_name = 'bng'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hero of Leina Tower'),
    (select id from sets where short_name = 'bng'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fate Unraveler'),
    (select id from sets where short_name = 'bng'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glimpse the Sun God'),
    (select id from sets where short_name = 'bng'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Archetype of Finality'),
    (select id from sets where short_name = 'bng'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spirit of the Labyrinth'),
    (select id from sets where short_name = 'bng'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brimaz, King of Oreskos'),
    (select id from sets where short_name = 'bng'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Eater of Hope'),
    (select id from sets where short_name = 'bng'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kiora, the Crashing Wave'),
    (select id from sets where short_name = 'bng'),
    '149',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ornitharch'),
    (select id from sets where short_name = 'bng'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Perplexing Chimera'),
    (select id from sets where short_name = 'bng'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eternity Snare'),
    (select id from sets where short_name = 'bng'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sanguimancy'),
    (select id from sets where short_name = 'bng'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whelming Wave'),
    (select id from sets where short_name = 'bng'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akroan Phalanx'),
    (select id from sets where short_name = 'bng'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hold at Bay'),
    (select id from sets where short_name = 'bng'),
    '18',
    'common'
) 
 on conflict do nothing;
