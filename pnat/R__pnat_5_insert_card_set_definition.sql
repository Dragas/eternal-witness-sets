insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Flooded Strand'),
    (select id from sets where short_name = 'pnat'),
    '2018',
    'rare'
) 
 on conflict do nothing;
