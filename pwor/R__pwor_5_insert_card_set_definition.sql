insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Crucible of Worlds'),
    (select id from sets where short_name = 'pwor'),
    '2019',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Balduvian Horde'),
    (select id from sets where short_name = 'pwor'),
    '1',
    'rare'
) 
 on conflict do nothing;
