insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'fbb'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flying Carpet'),
    (select id from sets where short_name = 'fbb'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'fbb'),
    '302',
    'common'
) ,
(
    (select id from mtgcard where name = 'Firebreathing'),
    (select id from sets where short_name = 'fbb'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Knight'),
    (select id from sets where short_name = 'fbb'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ebony Horse'),
    (select id from sets where short_name = 'fbb'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Oriflamme'),
    (select id from sets where short_name = 'fbb'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'fbb'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warp Artifact'),
    (select id from sets where short_name = 'fbb'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Veteran Bodyguard'),
    (select id from sets where short_name = 'fbb'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonic Attorney'),
    (select id from sets where short_name = 'fbb'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clone'),
    (select id from sets where short_name = 'fbb'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rock Hydra'),
    (select id from sets where short_name = 'fbb'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sleight of Mind'),
    (select id from sets where short_name = 'fbb'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Living Wall'),
    (select id from sets where short_name = 'fbb'),
    '262',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Contract from Below'),
    (select id from sets where short_name = 'fbb'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jump'),
    (select id from sets where short_name = 'fbb'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aladdin''s Ring'),
    (select id from sets where short_name = 'fbb'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'fbb'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tundra'),
    (select id from sets where short_name = 'fbb'),
    '289',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conversion'),
    (select id from sets where short_name = 'fbb'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fire Elemental'),
    (select id from sets where short_name = 'fbb'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deathgrip'),
    (select id from sets where short_name = 'fbb'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fork'),
    (select id from sets where short_name = 'fbb'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Stone'),
    (select id from sets where short_name = 'fbb'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ivory Cup'),
    (select id from sets where short_name = 'fbb'),
    '253',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hurloon Minotaur'),
    (select id from sets where short_name = 'fbb'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jandor''s Ring'),
    (select id from sets where short_name = 'fbb'),
    '256',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nettling Imp'),
    (select id from sets where short_name = 'fbb'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Taiga'),
    (select id from sets where short_name = 'fbb'),
    '287',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin King'),
    (select id from sets where short_name = 'fbb'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winter Orb'),
    (select id from sets where short_name = 'fbb'),
    '280',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steal Artifact'),
    (select id from sets where short_name = 'fbb'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Fire'),
    (select id from sets where short_name = 'fbb'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grizzly Bears'),
    (select id from sets where short_name = 'fbb'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spell Blast'),
    (select id from sets where short_name = 'fbb'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = 'fbb'),
    '267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ankh of Mishra'),
    (select id from sets where short_name = 'fbb'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Black'),
    (select id from sets where short_name = 'fbb'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crystal Rod'),
    (select id from sets where short_name = 'fbb'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = 'fbb'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scavenging Ghoul'),
    (select id from sets where short_name = 'fbb'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'fbb'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'fbb'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earth Elemental'),
    (select id from sets where short_name = 'fbb'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Ward'),
    (select id from sets where short_name = 'fbb'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nether Shadow'),
    (select id from sets where short_name = 'fbb'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desert Twister'),
    (select id from sets where short_name = 'fbb'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = 'fbb'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Holy Armor'),
    (select id from sets where short_name = 'fbb'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Terrain'),
    (select id from sets where short_name = 'fbb'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'fbb'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Badlands'),
    (select id from sets where short_name = 'fbb'),
    '282',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mijae Djinn'),
    (select id from sets where short_name = 'fbb'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Northern Paladin'),
    (select id from sets where short_name = 'fbb'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Water Elemental'),
    (select id from sets where short_name = 'fbb'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kird Ape'),
    (select id from sets where short_name = 'fbb'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'fbb'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Animate Artifact'),
    (select id from sets where short_name = 'fbb'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scryb Sprites'),
    (select id from sets where short_name = 'fbb'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'fbb'),
    '306a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drudge Skeletons'),
    (select id from sets where short_name = 'fbb'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ivory Tower'),
    (select id from sets where short_name = 'fbb'),
    '254',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wanderlust'),
    (select id from sets where short_name = 'fbb'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rod of Ruin'),
    (select id from sets where short_name = 'fbb'),
    '273',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crusade'),
    (select id from sets where short_name = 'fbb'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = 'fbb'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aladdin''s Lamp'),
    (select id from sets where short_name = 'fbb'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reverse Polarity'),
    (select id from sets where short_name = 'fbb'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island Sanctuary'),
    (select id from sets where short_name = 'fbb'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Energy Flux'),
    (select id from sets where short_name = 'fbb'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gloom'),
    (select id from sets where short_name = 'fbb'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sacrifice'),
    (select id from sets where short_name = 'fbb'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lance'),
    (select id from sets where short_name = 'fbb'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Resurrection'),
    (select id from sets where short_name = 'fbb'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Net'),
    (select id from sets where short_name = 'fbb'),
    '275',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disrupting Scepter'),
    (select id from sets where short_name = 'fbb'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Howl from Beyond'),
    (select id from sets where short_name = 'fbb'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lifetap'),
    (select id from sets where short_name = 'fbb'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Psychic Venom'),
    (select id from sets where short_name = 'fbb'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evil Presence'),
    (select id from sets where short_name = 'fbb'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Green'),
    (select id from sets where short_name = 'fbb'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rocket Launcher'),
    (select id from sets where short_name = 'fbb'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Power Sink'),
    (select id from sets where short_name = 'fbb'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lord of the Pit'),
    (select id from sets where short_name = 'fbb'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dwarven Warriors'),
    (select id from sets where short_name = 'fbb'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Twist'),
    (select id from sets where short_name = 'fbb'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Forces'),
    (select id from sets where short_name = 'fbb'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hurkyl''s Recall'),
    (select id from sets where short_name = 'fbb'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = 'fbb'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bottle of Suleiman'),
    (select id from sets where short_name = 'fbb'),
    '237',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brass Man'),
    (select id from sets where short_name = 'fbb'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Erg Raiders'),
    (select id from sets where short_name = 'fbb'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Engine'),
    (select id from sets where short_name = 'fbb'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'fbb'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = 'fbb'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Personal Incarnation'),
    (select id from sets where short_name = 'fbb'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mesa Pegasus'),
    (select id from sets where short_name = 'fbb'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hill Giant'),
    (select id from sets where short_name = 'fbb'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kormus Bell'),
    (select id from sets where short_name = 'fbb'),
    '260',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zombie Master'),
    (select id from sets where short_name = 'fbb'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Manabarbs'),
    (select id from sets where short_name = 'fbb'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Helm of Chatzuk'),
    (select id from sets where short_name = 'fbb'),
    '250',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'fbb'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blue Ward'),
    (select id from sets where short_name = 'fbb'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shanodin Dryads'),
    (select id from sets where short_name = 'fbb'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Animate Wall'),
    (select id from sets where short_name = 'fbb'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Archers'),
    (select id from sets where short_name = 'fbb'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conservator'),
    (select id from sets where short_name = 'fbb'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gray Ogre'),
    (select id from sets where short_name = 'fbb'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glasses of Urza'),
    (select id from sets where short_name = 'fbb'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'fbb'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Weaponsmith'),
    (select id from sets where short_name = 'fbb'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Water'),
    (select id from sets where short_name = 'fbb'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Red Ward'),
    (select id from sets where short_name = 'fbb'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merfolk of the Pearl Trident'),
    (select id from sets where short_name = 'fbb'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Righteousness'),
    (select id from sets where short_name = 'fbb'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathlace'),
    (select id from sets where short_name = 'fbb'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = 'fbb'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Giant'),
    (select id from sets where short_name = 'fbb'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Verduran Enchantress'),
    (select id from sets where short_name = 'fbb'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'fbb'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Castle'),
    (select id from sets where short_name = 'fbb'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armageddon Clock'),
    (select id from sets where short_name = 'fbb'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Island'),
    (select id from sets where short_name = 'fbb'),
    '291',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jandor''s Saddlebags'),
    (select id from sets where short_name = 'fbb'),
    '257',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clockwork Beast'),
    (select id from sets where short_name = 'fbb'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stasis'),
    (select id from sets where short_name = 'fbb'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simulacrum'),
    (select id from sets where short_name = 'fbb'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weakness'),
    (select id from sets where short_name = 'fbb'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tranquility'),
    (select id from sets where short_name = 'fbb'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savannah Lions'),
    (select id from sets where short_name = 'fbb'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Force of Nature'),
    (select id from sets where short_name = 'fbb'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dancing Scimitar'),
    (select id from sets where short_name = 'fbb'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Control Magic'),
    (select id from sets where short_name = 'fbb'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fastbond'),
    (select id from sets where short_name = 'fbb'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serendib Efreet'),
    (select id from sets where short_name = 'fbb'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death Ward'),
    (select id from sets where short_name = 'fbb'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'fbb'),
    '306',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thoughtlace'),
    (select id from sets where short_name = 'fbb'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Living Lands'),
    (select id from sets where short_name = 'fbb'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flashfires'),
    (select id from sets where short_name = 'fbb'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Atog'),
    (select id from sets where short_name = 'fbb'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Monster'),
    (select id from sets where short_name = 'fbb'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Artifact'),
    (select id from sets where short_name = 'fbb'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uthden Troll'),
    (select id from sets where short_name = 'fbb'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volcanic Eruption'),
    (select id from sets where short_name = 'fbb'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Howling Mine'),
    (select id from sets where short_name = 'fbb'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roc of Kher Ridges'),
    (select id from sets where short_name = 'fbb'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magnetic Mountain'),
    (select id from sets where short_name = 'fbb'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magical Hack'),
    (select id from sets where short_name = 'fbb'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bog Wraith'),
    (select id from sets where short_name = 'fbb'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Instill Energy'),
    (select id from sets where short_name = 'fbb'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'El-Hajjâj'),
    (select id from sets where short_name = 'fbb'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Black Vise'),
    (select id from sets where short_name = 'fbb'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blue Elemental Blast'),
    (select id from sets where short_name = 'fbb'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meekstone'),
    (select id from sets where short_name = 'fbb'),
    '264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Air'),
    (select id from sets where short_name = 'fbb'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Wood'),
    (select id from sets where short_name = 'fbb'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jayemdae Tome'),
    (select id from sets where short_name = 'fbb'),
    '258',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = 'fbb'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Library of Leng'),
    (select id from sets where short_name = 'fbb'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disintegrate'),
    (select id from sets where short_name = 'fbb'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Siren''s Call'),
    (select id from sets where short_name = 'fbb'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Underground Sea'),
    (select id from sets where short_name = 'fbb'),
    '290',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sedge Troll'),
    (select id from sets where short_name = 'fbb'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Timber Wolves'),
    (select id from sets where short_name = 'fbb'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reconstruction'),
    (select id from sets where short_name = 'fbb'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Healing Salve'),
    (select id from sets where short_name = 'fbb'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stream of Life'),
    (select id from sets where short_name = 'fbb'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bayou'),
    (select id from sets where short_name = 'fbb'),
    '283',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chaoslace'),
    (select id from sets where short_name = 'fbb'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = 'fbb'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cockatrice'),
    (select id from sets where short_name = 'fbb'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tunnel'),
    (select id from sets where short_name = 'fbb'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scathe Zombies'),
    (select id from sets where short_name = 'fbb'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obsianus Golem'),
    (select id from sets where short_name = 'fbb'),
    '268',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Paralyze'),
    (select id from sets where short_name = 'fbb'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Ice'),
    (select id from sets where short_name = 'fbb'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demonic Tutor'),
    (select id from sets where short_name = 'fbb'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lifeforce'),
    (select id from sets where short_name = 'fbb'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guardian Angel'),
    (select id from sets where short_name = 'fbb'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Titania''s Song'),
    (select id from sets where short_name = 'fbb'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lure'),
    (select id from sets where short_name = 'fbb'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Onulet'),
    (select id from sets where short_name = 'fbb'),
    '269',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crumble'),
    (select id from sets where short_name = 'fbb'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Samite Healer'),
    (select id from sets where short_name = 'fbb'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Vault'),
    (select id from sets where short_name = 'fbb'),
    '263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'fbb'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunglasses of Urza'),
    (select id from sets where short_name = 'fbb'),
    '276',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Web'),
    (select id from sets where short_name = 'fbb'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cursed Land'),
    (select id from sets where short_name = 'fbb'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'White Knight'),
    (select id from sets where short_name = 'fbb'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Red Elemental Blast'),
    (select id from sets where short_name = 'fbb'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Liege'),
    (select id from sets where short_name = 'fbb'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'fbb'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: White'),
    (select id from sets where short_name = 'fbb'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Copy Artifact'),
    (select id from sets where short_name = 'fbb'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'fbb'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Balance'),
    (select id from sets where short_name = 'fbb'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Specter'),
    (select id from sets where short_name = 'fbb'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lifelace'),
    (select id from sets where short_name = 'fbb'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'War Mammoth'),
    (select id from sets where short_name = 'fbb'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Short'),
    (select id from sets where short_name = 'fbb'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plague Rats'),
    (select id from sets where short_name = 'fbb'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Royal Assassin'),
    (select id from sets where short_name = 'fbb'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'fbb'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prodigal Sorcerer'),
    (select id from sets where short_name = 'fbb'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Flare'),
    (select id from sets where short_name = 'fbb'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatterstorm'),
    (select id from sets where short_name = 'fbb'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pestilence'),
    (select id from sets where short_name = 'fbb'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dingus Egg'),
    (select id from sets where short_name = 'fbb'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darkpact'),
    (select id from sets where short_name = 'fbb'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mons''s Goblin Raiders'),
    (select id from sets where short_name = 'fbb'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Brambles'),
    (select id from sets where short_name = 'fbb'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Channel'),
    (select id from sets where short_name = 'fbb'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Primal Clay'),
    (select id from sets where short_name = 'fbb'),
    '271',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = 'fbb'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ley Druid'),
    (select id from sets where short_name = 'fbb'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'fbb'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tropical Island'),
    (select id from sets where short_name = 'fbb'),
    '288',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Craw Wurm'),
    (select id from sets where short_name = 'fbb'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vesuvan Doppelganger'),
    (select id from sets where short_name = 'fbb'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Green Ward'),
    (select id from sets where short_name = 'fbb'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Holy Strength'),
    (select id from sets where short_name = 'fbb'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basalt Monolith'),
    (select id from sets where short_name = 'fbb'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island Fish Jasconius'),
    (select id from sets where short_name = 'fbb'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'fbb'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Hive'),
    (select id from sets where short_name = 'fbb'),
    '277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'fbb'),
    '303',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sorceress Queen'),
    (select id from sets where short_name = 'fbb'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unholy Strength'),
    (select id from sets where short_name = 'fbb'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Regrowth'),
    (select id from sets where short_name = 'fbb'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Earthbind'),
    (select id from sets where short_name = 'fbb'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smoke'),
    (select id from sets where short_name = 'fbb'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Will-o''-the-Wisp'),
    (select id from sets where short_name = 'fbb'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'fbb'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wooden Sphere'),
    (select id from sets where short_name = 'fbb'),
    '281',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tsunami'),
    (select id from sets where short_name = 'fbb'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'fbb'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savannah'),
    (select id from sets where short_name = 'fbb'),
    '285',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Blue'),
    (select id from sets where short_name = 'fbb'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Leak'),
    (select id from sets where short_name = 'fbb'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karma'),
    (select id from sets where short_name = 'fbb'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pirate Ship'),
    (select id from sets where short_name = 'fbb'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Balloon Brigade'),
    (select id from sets where short_name = 'fbb'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Swords'),
    (select id from sets where short_name = 'fbb'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aspect of Wolf'),
    (select id from sets where short_name = 'fbb'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Granite Gargoyle'),
    (select id from sets where short_name = 'fbb'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wild Growth'),
    (select id from sets where short_name = 'fbb'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Juggernaut'),
    (select id from sets where short_name = 'fbb'),
    '259',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bad Moon'),
    (select id from sets where short_name = 'fbb'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lord of Atlantis'),
    (select id from sets where short_name = 'fbb'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonic Hordes'),
    (select id from sets where short_name = 'fbb'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea Serpent'),
    (select id from sets where short_name = 'fbb'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = 'fbb'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Braingeyser'),
    (select id from sets where short_name = 'fbb'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Iron Star'),
    (select id from sets where short_name = 'fbb'),
    '252',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = 'fbb'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pearled Unicorn'),
    (select id from sets where short_name = 'fbb'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'White Ward'),
    (select id from sets where short_name = 'fbb'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Purelace'),
    (select id from sets where short_name = 'fbb'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestial Prism'),
    (select id from sets where short_name = 'fbb'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burrowing'),
    (select id from sets where short_name = 'fbb'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flight'),
    (select id from sets where short_name = 'fbb'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Millstone'),
    (select id from sets where short_name = 'fbb'),
    '265',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fog'),
    (select id from sets where short_name = 'fbb'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scrubland'),
    (select id from sets where short_name = 'fbb'),
    '286',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Power Surge'),
    (select id from sets where short_name = 'fbb'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keldon Warlord'),
    (select id from sets where short_name = 'fbb'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = 'fbb'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Benalish Hero'),
    (select id from sets where short_name = 'fbb'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frozen Shade'),
    (select id from sets where short_name = 'fbb'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Regeneration'),
    (select id from sets where short_name = 'fbb'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ornithopter'),
    (select id from sets where short_name = 'fbb'),
    '270',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fear'),
    (select id from sets where short_name = 'fbb'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wheel of Fortune'),
    (select id from sets where short_name = 'fbb'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drain Life'),
    (select id from sets where short_name = 'fbb'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Throne of Bone'),
    (select id from sets where short_name = 'fbb'),
    '279',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Artillery'),
    (select id from sets where short_name = 'fbb'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Bone'),
    (select id from sets where short_name = 'fbb'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = 'fbb'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = 'fbb'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Whelp'),
    (select id from sets where short_name = 'fbb'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eye for an Eye'),
    (select id from sets where short_name = 'fbb'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fungusaur'),
    (select id from sets where short_name = 'fbb'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thicket Basilisk'),
    (select id from sets where short_name = 'fbb'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unstable Mutation'),
    (select id from sets where short_name = 'fbb'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drain Power'),
    (select id from sets where short_name = 'fbb'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Rack'),
    (select id from sets where short_name = 'fbb'),
    '278',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plateau'),
    (select id from sets where short_name = 'fbb'),
    '284',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reverse Damage'),
    (select id from sets where short_name = 'fbb'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hurricane'),
    (select id from sets where short_name = 'fbb'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kudzu'),
    (select id from sets where short_name = 'fbb'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mahamoti Djinn'),
    (select id from sets where short_name = 'fbb'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Farmstead'),
    (select id from sets where short_name = 'fbb'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ironroot Treefolk'),
    (select id from sets where short_name = 'fbb'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'fbb'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'fbb'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blessing'),
    (select id from sets where short_name = 'fbb'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Creature Bond'),
    (select id from sets where short_name = 'fbb'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mishra''s War Machine'),
    (select id from sets where short_name = 'fbb'),
    '266',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Feedback'),
    (select id from sets where short_name = 'fbb'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Animate Dead'),
    (select id from sets where short_name = 'fbb'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'fbb'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'fbb'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jade Monolith'),
    (select id from sets where short_name = 'fbb'),
    '255',
    'rare'
) 
 on conflict do nothing;
