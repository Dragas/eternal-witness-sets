insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Sprouting Thrinax'),
    (select id from sets where short_name = 'pwpn'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woolly Thoctar'),
    (select id from sets where short_name = 'pwpn'),
    '22',
    'rare'
) 
 on conflict do nothing;
