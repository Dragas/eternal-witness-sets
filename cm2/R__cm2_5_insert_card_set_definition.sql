insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'cm2'),
    '288',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windfall'),
    (select id from sets where short_name = 'cm2'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Minds Aglow'),
    (select id from sets where short_name = 'cm2'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hardened Scales'),
    (select id from sets where short_name = 'cm2'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anya, Merciless Angel'),
    (select id from sets where short_name = 'cm2'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Opulent Palace'),
    (select id from sets where short_name = 'cm2'),
    '260',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elite Scaleguard'),
    (select id from sets where short_name = 'cm2'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Reclamation'),
    (select id from sets where short_name = 'cm2'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crystalline Crawler'),
    (select id from sets where short_name = 'cm2'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ichor Wellspring'),
    (select id from sets where short_name = 'cm2'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Secluded Steppe'),
    (select id from sets where short_name = 'cm2'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Artisan of Kozilek'),
    (select id from sets where short_name = 'cm2'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tyrant''s Familiar'),
    (select id from sets where short_name = 'cm2'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sewer Nemesis'),
    (select id from sets where short_name = 'cm2'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brave the Sands'),
    (select id from sets where short_name = 'cm2'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'cm2'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tezzeret''s Gambit'),
    (select id from sets where short_name = 'cm2'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Festercreep'),
    (select id from sets where short_name = 'cm2'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grip of Phyresis'),
    (select id from sets where short_name = 'cm2'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flamekin Village'),
    (select id from sets where short_name = 'cm2'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghave, Guru of Spores'),
    (select id from sets where short_name = 'cm2'),
    '157',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Inferno Titan'),
    (select id from sets where short_name = 'cm2'),
    '109',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Junk Diver'),
    (select id from sets where short_name = 'cm2'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivid Meadow'),
    (select id from sets where short_name = 'cm2'),
    '277',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Simic Signet'),
    (select id from sets where short_name = 'cm2'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghost Quarter'),
    (select id from sets where short_name = 'cm2'),
    '253',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Juniper Order Ranger'),
    (select id from sets where short_name = 'cm2'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Buried Ruin'),
    (select id from sets where short_name = 'cm2'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feldon of the Third Path'),
    (select id from sets where short_name = 'cm2'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thundercloud Shaman'),
    (select id from sets where short_name = 'cm2'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Welder'),
    (select id from sets where short_name = 'cm2'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Buried Alive'),
    (select id from sets where short_name = 'cm2'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Astral Cornucopia'),
    (select id from sets where short_name = 'cm2'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tranquil Thicket'),
    (select id from sets where short_name = 'cm2'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duelist''s Heritage'),
    (select id from sets where short_name = 'cm2'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thought Vessel'),
    (select id from sets where short_name = 'cm2'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simic Signet'),
    (select id from sets where short_name = 'cm2'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trading Post'),
    (select id from sets where short_name = 'cm2'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jwar Isle Refuge'),
    (select id from sets where short_name = 'cm2'),
    '256',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loxodon Warhammer'),
    (select id from sets where short_name = 'cm2'),
    '199',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirrorweave'),
    (select id from sets where short_name = 'cm2'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inspiring Call'),
    (select id from sets where short_name = 'cm2'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desecrator Hag'),
    (select id from sets where short_name = 'cm2'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fellwar Stone'),
    (select id from sets where short_name = 'cm2'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dreadship Reef'),
    (select id from sets where short_name = 'cm2'),
    '247',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Worn Powerstone'),
    (select id from sets where short_name = 'cm2'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necroplasm'),
    (select id from sets where short_name = 'cm2'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arbiter of Knollridge'),
    (select id from sets where short_name = 'cm2'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seaside Citadel'),
    (select id from sets where short_name = 'cm2'),
    '265',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rupture Spire'),
    (select id from sets where short_name = 'cm2'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Word of Seizing'),
    (select id from sets where short_name = 'cm2'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cm2'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orim''s Thunder'),
    (select id from sets where short_name = 'cm2'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Solemn Simulacrum'),
    (select id from sets where short_name = 'cm2'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Signet'),
    (select id from sets where short_name = 'cm2'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Acidic Slime'),
    (select id from sets where short_name = 'cm2'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bitter Feud'),
    (select id from sets where short_name = 'cm2'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cathars'' Crusade'),
    (select id from sets where short_name = 'cm2'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reliquary Tower'),
    (select id from sets where short_name = 'cm2'),
    '262',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Corpsejack Menace'),
    (select id from sets where short_name = 'cm2'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sungrass Prairie'),
    (select id from sets where short_name = 'cm2'),
    '269',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curse of the Nightly Hunt'),
    (select id from sets where short_name = 'cm2'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sandstone Oracle'),
    (select id from sets where short_name = 'cm2'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cm2'),
    '311',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duneblast'),
    (select id from sets where short_name = 'cm2'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Stone'),
    (select id from sets where short_name = 'cm2'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ishai, Ojutai Dragonspeaker'),
    (select id from sets where short_name = 'cm2'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'cm2'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bane of the Living'),
    (select id from sets where short_name = 'cm2'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Cluestone'),
    (select id from sets where short_name = 'cm2'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rise from the Grave'),
    (select id from sets where short_name = 'cm2'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sublime Exhalation'),
    (select id from sets where short_name = 'cm2'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beetleback Chief'),
    (select id from sets where short_name = 'cm2'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deepglow Skate'),
    (select id from sets where short_name = 'cm2'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fumiko the Lowblood'),
    (select id from sets where short_name = 'cm2'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Master Biomancer'),
    (select id from sets where short_name = 'cm2'),
    '159',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Victory''s Herald'),
    (select id from sets where short_name = 'cm2'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pristine Talisman'),
    (select id from sets where short_name = 'cm2'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcane Sanctum'),
    (select id from sets where short_name = 'cm2'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chaos Warp'),
    (select id from sets where short_name = 'cm2'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fathom Mage'),
    (select id from sets where short_name = 'cm2'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cm2'),
    '307',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple of the False God'),
    (select id from sets where short_name = 'cm2'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hunted Dragon'),
    (select id from sets where short_name = 'cm2'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vorosh, the Hunter'),
    (select id from sets where short_name = 'cm2'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Incite Rebellion'),
    (select id from sets where short_name = 'cm2'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gisela, Blade of Goldnight'),
    (select id from sets where short_name = 'cm2'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Myr Sire'),
    (select id from sets where short_name = 'cm2'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boros Guildgate'),
    (select id from sets where short_name = 'cm2'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple of the False God'),
    (select id from sets where short_name = 'cm2'),
    '272',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Aqueduct'),
    (select id from sets where short_name = 'cm2'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cm2'),
    '310',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kalemne, Disciple of Iroas'),
    (select id from sets where short_name = 'cm2'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kalemne''s Captain'),
    (select id from sets where short_name = 'cm2'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'cm2'),
    '302',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daretti, Scrap Savant'),
    (select id from sets where short_name = 'cm2'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dimir Signet'),
    (select id from sets where short_name = 'cm2'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bogardan Hellkite'),
    (select id from sets where short_name = 'cm2'),
    '86',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Syphon Flesh'),
    (select id from sets where short_name = 'cm2'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drifting Meadow'),
    (select id from sets where short_name = 'cm2'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Diamond'),
    (select id from sets where short_name = 'cm2'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Troll Ascetic'),
    (select id from sets where short_name = 'cm2'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Excavation'),
    (select id from sets where short_name = 'cm2'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treasure Cruise'),
    (select id from sets where short_name = 'cm2'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coldsteel Heart'),
    (select id from sets where short_name = 'cm2'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Basalt Monolith'),
    (select id from sets where short_name = 'cm2'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vorel of the Hull Clade'),
    (select id from sets where short_name = 'cm2'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spitebellows'),
    (select id from sets where short_name = 'cm2'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warchief Giant'),
    (select id from sets where short_name = 'cm2'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cm2'),
    '282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurmcoil Engine'),
    (select id from sets where short_name = 'cm2'),
    '231',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Volcanic Offering'),
    (select id from sets where short_name = 'cm2'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Palladium Myr'),
    (select id from sets where short_name = 'cm2'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brawn'),
    (select id from sets where short_name = 'cm2'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cm2'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Staff of Nin'),
    (select id from sets where short_name = 'cm2'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cathodion'),
    (select id from sets where short_name = 'cm2'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hammerfist Giant'),
    (select id from sets where short_name = 'cm2'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spell Crumple'),
    (select id from sets where short_name = 'cm2'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Great Furnace'),
    (select id from sets where short_name = 'cm2'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cm2'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orzhov Signet'),
    (select id from sets where short_name = 'cm2'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blasted Landscape'),
    (select id from sets where short_name = 'cm2'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Commander''s Sphere'),
    (select id from sets where short_name = 'cm2'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magma Giant'),
    (select id from sets where short_name = 'cm2'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blade of Selves'),
    (select id from sets where short_name = 'cm2'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Herald of the Host'),
    (select id from sets where short_name = 'cm2'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Riddlekeeper'),
    (select id from sets where short_name = 'cm2'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Command Tower'),
    (select id from sets where short_name = 'cm2'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smoldering Crater'),
    (select id from sets where short_name = 'cm2'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shared Trauma'),
    (select id from sets where short_name = 'cm2'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scythe Specter'),
    (select id from sets where short_name = 'cm2'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crib Swap'),
    (select id from sets where short_name = 'cm2'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Solemn Simulacrum'),
    (select id from sets where short_name = 'cm2'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disdainful Stroke'),
    (select id from sets where short_name = 'cm2'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darksteel Ingot'),
    (select id from sets where short_name = 'cm2'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barren Moor'),
    (select id from sets where short_name = 'cm2'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sign in Blood'),
    (select id from sets where short_name = 'cm2'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Patron of the Nezumi'),
    (select id from sets where short_name = 'cm2'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oreskos Explorer'),
    (select id from sets where short_name = 'cm2'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magmaquake'),
    (select id from sets where short_name = 'cm2'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wind-Scarred Crag'),
    (select id from sets where short_name = 'cm2'),
    '278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mortify'),
    (select id from sets where short_name = 'cm2'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cm2'),
    '283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warstorm Surge'),
    (select id from sets where short_name = 'cm2'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slipstream Eel'),
    (select id from sets where short_name = 'cm2'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skullbriar, the Walking Grave'),
    (select id from sets where short_name = 'cm2'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cm2'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mulldrifter'),
    (select id from sets where short_name = 'cm2'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golgari Signet'),
    (select id from sets where short_name = 'cm2'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fall of the Hammer'),
    (select id from sets where short_name = 'cm2'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faithless Looting'),
    (select id from sets where short_name = 'cm2'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Epochrasite'),
    (select id from sets where short_name = 'cm2'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Panic Spellbomb'),
    (select id from sets where short_name = 'cm2'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Retriever'),
    (select id from sets where short_name = 'cm2'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Custodi Soulbinders'),
    (select id from sets where short_name = 'cm2'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'cm2'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Taurean Mauler'),
    (select id from sets where short_name = 'cm2'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seer''s Sundial'),
    (select id from sets where short_name = 'cm2'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unstable Obelisk'),
    (select id from sets where short_name = 'cm2'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lonely Sandbar'),
    (select id from sets where short_name = 'cm2'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sandsteppe Citadel'),
    (select id from sets where short_name = 'cm2'),
    '264',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cm2'),
    '312',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = 'cm2'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tuskguard Captain'),
    (select id from sets where short_name = 'cm2'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bosh, Iron Golem'),
    (select id from sets where short_name = 'cm2'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avatar of Woe'),
    (select id from sets where short_name = 'cm2'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vulturous Zombie'),
    (select id from sets where short_name = 'cm2'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Living Death'),
    (select id from sets where short_name = 'cm2'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiery Confluence'),
    (select id from sets where short_name = 'cm2'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whipflare'),
    (select id from sets where short_name = 'cm2'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thrummingbird'),
    (select id from sets where short_name = 'cm2'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magus of the Wheel'),
    (select id from sets where short_name = 'cm2'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ingot Chewer'),
    (select id from sets where short_name = 'cm2'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreamborn Muse'),
    (select id from sets where short_name = 'cm2'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cultivate'),
    (select id from sets where short_name = 'cm2'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dormant Volcano'),
    (select id from sets where short_name = 'cm2'),
    '246',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hoard-Smelter Dragon'),
    (select id from sets where short_name = 'cm2'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel of Serenity'),
    (select id from sets where short_name = 'cm2'),
    '16',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Citadel Siege'),
    (select id from sets where short_name = 'cm2'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Murmuring Bosk'),
    (select id from sets where short_name = 'cm2'),
    '258',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Triskelavus'),
    (select id from sets where short_name = 'cm2'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rite of the Raging Storm'),
    (select id from sets where short_name = 'cm2'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sun Titan'),
    (select id from sets where short_name = 'cm2'),
    '37',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dreamstone Hedron'),
    (select id from sets where short_name = 'cm2'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reyhan, Last of the Abzan'),
    (select id from sets where short_name = 'cm2'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cm2'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spine of Ish Sah'),
    (select id from sets where short_name = 'cm2'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'cm2'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrexial, the Risen Deep'),
    (select id from sets where short_name = 'cm2'),
    '171',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ikra Shidiqi, the Usurper'),
    (select id from sets where short_name = 'cm2'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Steel Hellkite'),
    (select id from sets where short_name = 'cm2'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cm2'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unnerve'),
    (select id from sets where short_name = 'cm2'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Languish'),
    (select id from sets where short_name = 'cm2'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cm2'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enduring Scalelord'),
    (select id from sets where short_name = 'cm2'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Incubator'),
    (select id from sets where short_name = 'cm2'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reveillark'),
    (select id from sets where short_name = 'cm2'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cm2'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fact or Fiction'),
    (select id from sets where short_name = 'cm2'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cm2'),
    '306',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wayfarer''s Bauble'),
    (select id from sets where short_name = 'cm2'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Underground River'),
    (select id from sets where short_name = 'cm2'),
    '275',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hostility'),
    (select id from sets where short_name = 'cm2'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'cm2'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relic Crush'),
    (select id from sets where short_name = 'cm2'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Putrefy'),
    (select id from sets where short_name = 'cm2'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Opal Palace'),
    (select id from sets where short_name = 'cm2'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'cm2'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vow of Flight'),
    (select id from sets where short_name = 'cm2'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunrise Sovereign'),
    (select id from sets where short_name = 'cm2'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nezumi Graverobber // Nighteyes the Desecrator'),
    (select id from sets where short_name = 'cm2'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Starstorm'),
    (select id from sets where short_name = 'cm2'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kalonian Hydra'),
    (select id from sets where short_name = 'cm2'),
    '140',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Loreseeker''s Stone'),
    (select id from sets where short_name = 'cm2'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merciless Eviction'),
    (select id from sets where short_name = 'cm2'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ash Barrens'),
    (select id from sets where short_name = 'cm2'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cm2'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexia''s Core'),
    (select id from sets where short_name = 'cm2'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oblivion Stone'),
    (select id from sets where short_name = 'cm2'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wonder'),
    (select id from sets where short_name = 'cm2'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dream Pillager'),
    (select id from sets where short_name = 'cm2'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pentavus'),
    (select id from sets where short_name = 'cm2'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruby Medallion'),
    (select id from sets where short_name = 'cm2'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Garrison'),
    (select id from sets where short_name = 'cm2'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Mimeoplasm'),
    (select id from sets where short_name = 'cm2'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Champion of Lambholt'),
    (select id from sets where short_name = 'cm2'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cm2'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'cm2'),
    '303',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'cm2'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'Everflowing Chalice'),
    (select id from sets where short_name = 'cm2'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Atraxa, Praetors'' Voice'),
    (select id from sets where short_name = 'cm2'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Darkwater Catacombs'),
    (select id from sets where short_name = 'cm2'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Growth Chamber'),
    (select id from sets where short_name = 'cm2'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abzan Falconer'),
    (select id from sets where short_name = 'cm2'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Syphon Mind'),
    (select id from sets where short_name = 'cm2'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dawnbreak Reclaimer'),
    (select id from sets where short_name = 'cm2'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desolation Giant'),
    (select id from sets where short_name = 'cm2'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Solidarity of Heroes'),
    (select id from sets where short_name = 'cm2'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stitch Together'),
    (select id from sets where short_name = 'cm2'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'cm2'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grave Pact'),
    (select id from sets where short_name = 'cm2'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disaster Radius'),
    (select id from sets where short_name = 'cm2'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cm2'),
    '281',
    'common'
) ,
(
    (select id from mtgcard where name = 'Migratory Route'),
    (select id from sets where short_name = 'cm2'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darksteel Citadel'),
    (select id from sets where short_name = 'cm2'),
    '243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tuktuk the Explorer'),
    (select id from sets where short_name = 'cm2'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scrap Mastery'),
    (select id from sets where short_name = 'cm2'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stinkdrinker Daredevil'),
    (select id from sets where short_name = 'cm2'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Hatchling'),
    (select id from sets where short_name = 'cm2'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mortivore'),
    (select id from sets where short_name = 'cm2'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jareth, Leonine Titan'),
    (select id from sets where short_name = 'cm2'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Rot Farm'),
    (select id from sets where short_name = 'cm2'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hamletback Goliath'),
    (select id from sets where short_name = 'cm2'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vow of Malice'),
    (select id from sets where short_name = 'cm2'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scavenging Ooze'),
    (select id from sets where short_name = 'cm2'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forgotten Ancient'),
    (select id from sets where short_name = 'cm2'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Amphitheater'),
    (select id from sets where short_name = 'cm2'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dawnglare Invoker'),
    (select id from sets where short_name = 'cm2'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Extractor Demon'),
    (select id from sets where short_name = 'cm2'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myr Battlesphere'),
    (select id from sets where short_name = 'cm2'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azorius Chancery'),
    (select id from sets where short_name = 'cm2'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fleshbag Marauder'),
    (select id from sets where short_name = 'cm2'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Breath of Darigaaz'),
    (select id from sets where short_name = 'cm2'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cauldron of Souls'),
    (select id from sets where short_name = 'cm2'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Impact Resonance'),
    (select id from sets where short_name = 'cm2'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'cm2'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bred for the Hunt'),
    (select id from sets where short_name = 'cm2'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'cm2'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cm2'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vow of Wildness'),
    (select id from sets where short_name = 'cm2'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vivid Crag'),
    (select id from sets where short_name = 'cm2'),
    '276',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu'),
    (select id from sets where short_name = 'cm2'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Szadek, Lord of Secrets'),
    (select id from sets where short_name = 'cm2'),
    '168',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Butcher of Malakir'),
    (select id from sets where short_name = 'cm2'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meteor Blast'),
    (select id from sets where short_name = 'cm2'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Caged Sun'),
    (select id from sets where short_name = 'cm2'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Signet'),
    (select id from sets where short_name = 'cm2'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cm2'),
    '309',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swiftfoot Boots'),
    (select id from sets where short_name = 'cm2'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orzhov Advokist'),
    (select id from sets where short_name = 'cm2'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Banishing Light'),
    (select id from sets where short_name = 'cm2'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Borderland Behemoth'),
    (select id from sets where short_name = 'cm2'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warmonger Hellkite'),
    (select id from sets where short_name = 'cm2'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'cm2'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spitting Image'),
    (select id from sets where short_name = 'cm2'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Manifold Insights'),
    (select id from sets where short_name = 'cm2'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lhurgoyf'),
    (select id from sets where short_name = 'cm2'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eternal Witness'),
    (select id from sets where short_name = 'cm2'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'cm2'),
    '290',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cm2'),
    '308',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tribute to the Wild'),
    (select id from sets where short_name = 'cm2'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pilgrim''s Eye'),
    (select id from sets where short_name = 'cm2'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Greaves'),
    (select id from sets where short_name = 'cm2'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Memory Erosion'),
    (select id from sets where short_name = 'cm2'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Elder'),
    (select id from sets where short_name = 'cm2'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'cm2'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mycosynth Wellspring'),
    (select id from sets where short_name = 'cm2'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liquimetal Coating'),
    (select id from sets where short_name = 'cm2'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'cm2'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Exotic Orchard'),
    (select id from sets where short_name = 'cm2'),
    '250',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Damia, Sage of Stone'),
    (select id from sets where short_name = 'cm2'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bottle Gnomes'),
    (select id from sets where short_name = 'cm2'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcane Lighthouse'),
    (select id from sets where short_name = 'cm2'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Faith''s Fetters'),
    (select id from sets where short_name = 'cm2'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jalum Tome'),
    (select id from sets where short_name = 'cm2'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dualcaster Mage'),
    (select id from sets where short_name = 'cm2'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blasphemous Act'),
    (select id from sets where short_name = 'cm2'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forgotten Cave'),
    (select id from sets where short_name = 'cm2'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stoneshock Giant'),
    (select id from sets where short_name = 'cm2'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Svogthos, the Restless Tomb'),
    (select id from sets where short_name = 'cm2'),
    '270',
    'uncommon'
) 
 on conflict do nothing;
