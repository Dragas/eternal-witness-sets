insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Domri Rade Emblem'),
    (select id from sets where short_name = 'tgtc'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'tgtc'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horror'),
    (select id from sets where short_name = 'tgtc'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tgtc'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cleric'),
    (select id from sets where short_name = 'tgtc'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tgtc'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frog Lizard'),
    (select id from sets where short_name = 'tgtc'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rat'),
    (select id from sets where short_name = 'tgtc'),
    '2',
    'common'
) 
 on conflict do nothing;
