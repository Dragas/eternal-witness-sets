    insert into mtgcard(name) values ('Domri Rade Emblem') on conflict do nothing;
    insert into mtgcard(name) values ('Angel') on conflict do nothing;
    insert into mtgcard(name) values ('Horror') on conflict do nothing;
    insert into mtgcard(name) values ('Spirit') on conflict do nothing;
    insert into mtgcard(name) values ('Cleric') on conflict do nothing;
    insert into mtgcard(name) values ('Soldier') on conflict do nothing;
    insert into mtgcard(name) values ('Frog Lizard') on conflict do nothing;
    insert into mtgcard(name) values ('Rat') on conflict do nothing;
