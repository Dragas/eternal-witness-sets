insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Nimble Pilferer'),
    (select id from sets where short_name = 'xana'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seismic Rupture'),
    (select id from sets where short_name = 'xana'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blinding Radiance'),
    (select id from sets where short_name = 'xana'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doublecast'),
    (select id from sets where short_name = 'xana'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Grenade'),
    (select id from sets where short_name = 'xana'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ambition''s Cost'),
    (select id from sets where short_name = 'xana'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loxodon Line Breaker'),
    (select id from sets where short_name = 'xana'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overflowing Insight'),
    (select id from sets where short_name = 'xana'),
    '16',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Volcanic Dragon'),
    (select id from sets where short_name = 'xana'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ogre Painbringer'),
    (select id from sets where short_name = 'xana'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Altar''s Reap'),
    (select id from sets where short_name = 'xana'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Gang Leader'),
    (select id from sets where short_name = 'xana'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soulhunter Rakshasa'),
    (select id from sets where short_name = 'xana'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Bruiser'),
    (select id from sets where short_name = 'xana'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rumbling Baloth'),
    (select id from sets where short_name = 'xana'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Miasmic Mummy'),
    (select id from sets where short_name = 'xana'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Waterknot'),
    (select id from sets where short_name = 'xana'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Renegade Demon'),
    (select id from sets where short_name = 'xana'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chaos Maw'),
    (select id from sets where short_name = 'xana'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Divination'),
    (select id from sets where short_name = 'xana'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Take Vengeance'),
    (select id from sets where short_name = 'xana'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Titanic Pelagosaur'),
    (select id from sets where short_name = 'xana'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cruel Cut'),
    (select id from sets where short_name = 'xana'),
    '26',
    'common'
) 
 on conflict do nothing;
