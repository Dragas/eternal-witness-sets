insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ragavan'),
    (select id from sets where short_name = 'taer'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tezzeret the Schemer Emblem'),
    (select id from sets where short_name = 'taer'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Etherium Cell'),
    (select id from sets where short_name = 'taer'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gremlin'),
    (select id from sets where short_name = 'taer'),
    '1',
    'common'
) 
 on conflict do nothing;
