insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Makindi Griffin'),
    (select id from sets where short_name = 'ddp'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grazing Gladehart'),
    (select id from sets where short_name = 'ddp'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Read the Bones'),
    (select id from sets where short_name = 'ddp'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daggerback Basilisk'),
    (select id from sets where short_name = 'ddp'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Stone'),
    (select id from sets where short_name = 'ddp'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Corpsehatch'),
    (select id from sets where short_name = 'ddp'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forerunner of Slaughter'),
    (select id from sets where short_name = 'ddp'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddp'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Butcher of Malakir'),
    (select id from sets where short_name = 'ddp'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Explorer''s Scope'),
    (select id from sets where short_name = 'ddp'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kabira Vindicator'),
    (select id from sets where short_name = 'ddp'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Torch Slinger'),
    (select id from sets where short_name = 'ddp'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Repel the Darkness'),
    (select id from sets where short_name = 'ddp'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akoum Refuge'),
    (select id from sets where short_name = 'ddp'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harrow'),
    (select id from sets where short_name = 'ddp'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frontier Guide'),
    (select id from sets where short_name = 'ddp'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddp'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hellion Eruption'),
    (select id from sets where short_name = 'ddp'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stonework Puma'),
    (select id from sets where short_name = 'ddp'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emrakul''s Hatcher'),
    (select id from sets where short_name = 'ddp'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Spawn'),
    (select id from sets where short_name = 'ddp'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forked Bolt'),
    (select id from sets where short_name = 'ddp'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magmaw'),
    (select id from sets where short_name = 'ddp'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddp'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddp'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Retreat to Kazandu'),
    (select id from sets where short_name = 'ddp'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tajuru Archer'),
    (select id from sets where short_name = 'ddp'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Veteran Warleader'),
    (select id from sets where short_name = 'ddp'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'It That Betrays'),
    (select id from sets where short_name = 'ddp'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avenger of Zendikar'),
    (select id from sets where short_name = 'ddp'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddp'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'ddp'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Spawn'),
    (select id from sets where short_name = 'ddp'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Caravan Escort'),
    (select id from sets where short_name = 'ddp'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Groundswell'),
    (select id from sets where short_name = 'ddp'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rocky Tar Pit'),
    (select id from sets where short_name = 'ddp'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Primal Command'),
    (select id from sets where short_name = 'ddp'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seer''s Sundial'),
    (select id from sets where short_name = 'ddp'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pawn of Ulamog'),
    (select id from sets where short_name = 'ddp'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampire Nighthawk'),
    (select id from sets where short_name = 'ddp'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beastbreaker of Bala Ged'),
    (select id from sets where short_name = 'ddp'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodrite Invoker'),
    (select id from sets where short_name = 'ddp'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marsh Casualties'),
    (select id from sets where short_name = 'ddp'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight of Cliffhaven'),
    (select id from sets where short_name = 'ddp'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Territorial Baloth'),
    (select id from sets where short_name = 'ddp'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oblivion Sower'),
    (select id from sets where short_name = 'ddp'),
    '41',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Turntimber Basilisk'),
    (select id from sets where short_name = 'ddp'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stirring Wildwood'),
    (select id from sets where short_name = 'ddp'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cadaver Imp'),
    (select id from sets where short_name = 'ddp'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddp'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddp'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddp'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heartstabber Mosquito'),
    (select id from sets where short_name = 'ddp'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Affa Guard Hound'),
    (select id from sets where short_name = 'ddp'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddp'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Joraga Bard'),
    (select id from sets where short_name = 'ddp'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Graypelt Hunter'),
    (select id from sets where short_name = 'ddp'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Induce Despair'),
    (select id from sets where short_name = 'ddp'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ulamog''s Crusher'),
    (select id from sets where short_name = 'ddp'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Temple'),
    (select id from sets where short_name = 'ddp'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Graypelt Refuge'),
    (select id from sets where short_name = 'ddp'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddp'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ondu Giant'),
    (select id from sets where short_name = 'ddp'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Artisan of Kozilek'),
    (select id from sets where short_name = 'ddp'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Consume the Meek'),
    (select id from sets where short_name = 'ddp'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sheer Drop'),
    (select id from sets where short_name = 'ddp'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smother'),
    (select id from sets where short_name = 'ddp'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Turntimber Grove'),
    (select id from sets where short_name = 'ddp'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plant'),
    (select id from sets where short_name = 'ddp'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dominator Drone'),
    (select id from sets where short_name = 'ddp'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hellion'),
    (select id from sets where short_name = 'ddp'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scute Mob'),
    (select id from sets where short_name = 'ddp'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Khalni Heart Expedition'),
    (select id from sets where short_name = 'ddp'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runed Servitor'),
    (select id from sets where short_name = 'ddp'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wildheart Invoker'),
    (select id from sets where short_name = 'ddp'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodthrone Vampire'),
    (select id from sets where short_name = 'ddp'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Spawn'),
    (select id from sets where short_name = 'ddp'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddp'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddp'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oust'),
    (select id from sets where short_name = 'ddp'),
    '7',
    'uncommon'
) 
 on conflict do nothing;
