insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Reverberate'),
    (select id from sets where short_name = 'pd2'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pd2'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Servant'),
    (select id from sets where short_name = 'pd2'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pd2'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireblast'),
    (select id from sets where short_name = 'pd2'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grim Lavamancer'),
    (select id from sets where short_name = 'pd2'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jaya Ballard, Task Mage'),
    (select id from sets where short_name = 'pd2'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ball Lightning'),
    (select id from sets where short_name = 'pd2'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pd2'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pd2'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pillage'),
    (select id from sets where short_name = 'pd2'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jackal Pup'),
    (select id from sets where short_name = 'pd2'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'pd2'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hammer of Bogardan'),
    (select id from sets where short_name = 'pd2'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Price of Progress'),
    (select id from sets where short_name = 'pd2'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mogg Fanatic'),
    (select id from sets where short_name = 'pd2'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hellspark Elemental'),
    (select id from sets where short_name = 'pd2'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Figure of Destiny'),
    (select id from sets where short_name = 'pd2'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chain Lightning'),
    (select id from sets where short_name = 'pd2'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Browbeat'),
    (select id from sets where short_name = 'pd2'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keldon Champion'),
    (select id from sets where short_name = 'pd2'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boggart Ram-Gang'),
    (select id from sets where short_name = 'pd2'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barbarian Ring'),
    (select id from sets where short_name = 'pd2'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sudden Impact'),
    (select id from sets where short_name = 'pd2'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cinder Pyromancer'),
    (select id from sets where short_name = 'pd2'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flames of the Blood Hand'),
    (select id from sets where short_name = 'pd2'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghitu Encampment'),
    (select id from sets where short_name = 'pd2'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'pd2'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teetering Peaks'),
    (select id from sets where short_name = 'pd2'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keldon Marauders'),
    (select id from sets where short_name = 'pd2'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vulshok Sorcerer'),
    (select id from sets where short_name = 'pd2'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunderbolt'),
    (select id from sets where short_name = 'pd2'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mogg Flunkies'),
    (select id from sets where short_name = 'pd2'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spark Elemental'),
    (select id from sets where short_name = 'pd2'),
    '4',
    'uncommon'
) 
 on conflict do nothing;
