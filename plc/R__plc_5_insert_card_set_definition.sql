insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Volcano Hellion'),
    (select id from sets where short_name = 'plc'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyrohemia'),
    (select id from sets where short_name = 'plc'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serendib Sorcerer'),
    (select id from sets where short_name = 'plc'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rough // Tumble'),
    (select id from sets where short_name = 'plc'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Life and Limb'),
    (select id from sets where short_name = 'plc'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Braids, Conjurer Adept'),
    (select id from sets where short_name = 'plc'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Knight'),
    (select id from sets where short_name = 'plc'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Retether'),
    (select id from sets where short_name = 'plc'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shivan Meteor'),
    (select id from sets where short_name = 'plc'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Dustwasp'),
    (select id from sets where short_name = 'plc'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dust Elemental'),
    (select id from sets where short_name = 'plc'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jedit Ojanen of Efrava'),
    (select id from sets where short_name = 'plc'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reckless Wurm'),
    (select id from sets where short_name = 'plc'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mire Boa'),
    (select id from sets where short_name = 'plc'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantasmagorian'),
    (select id from sets where short_name = 'plc'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urborg, Tomb of Yawgmoth'),
    (select id from sets where short_name = 'plc'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Torchling'),
    (select id from sets where short_name = 'plc'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Affliction'),
    (select id from sets where short_name = 'plc'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Porphyry Nodes'),
    (select id from sets where short_name = 'plc'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treacherous Urge'),
    (select id from sets where short_name = 'plc'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Imp''s Mischief'),
    (select id from sets where short_name = 'plc'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fury Charm'),
    (select id from sets where short_name = 'plc'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Extirpate'),
    (select id from sets where short_name = 'plc'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Damnation'),
    (select id from sets where short_name = 'plc'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bog Serpent'),
    (select id from sets where short_name = 'plc'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hedge Troll'),
    (select id from sets where short_name = 'plc'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shade of Trokair'),
    (select id from sets where short_name = 'plc'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primal Plasma'),
    (select id from sets where short_name = 'plc'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Timbermare'),
    (select id from sets where short_name = 'plc'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Detritivore'),
    (select id from sets where short_name = 'plc'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aeon Chronicler'),
    (select id from sets where short_name = 'plc'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gossamer Phantasm'),
    (select id from sets where short_name = 'plc'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Midnight Charm'),
    (select id from sets where short_name = 'plc'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darkheart Sliver'),
    (select id from sets where short_name = 'plc'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dismal Failure'),
    (select id from sets where short_name = 'plc'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Akroma, Angel of Fury'),
    (select id from sets where short_name = 'plc'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wild Pair'),
    (select id from sets where short_name = 'plc'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Intet, the Dreamer'),
    (select id from sets where short_name = 'plc'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pallid Mycoderm'),
    (select id from sets where short_name = 'plc'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cradle to Grave'),
    (select id from sets where short_name = 'plc'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mesa Enchantress'),
    (select id from sets where short_name = 'plc'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hunting Wilds'),
    (select id from sets where short_name = 'plc'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keen Sense'),
    (select id from sets where short_name = 'plc'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frenetic Sliver'),
    (select id from sets where short_name = 'plc'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Benalish Commander'),
    (select id from sets where short_name = 'plc'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necrotic Sliver'),
    (select id from sets where short_name = 'plc'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunlance'),
    (select id from sets where short_name = 'plc'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skirk Shaman'),
    (select id from sets where short_name = 'plc'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sulfur Elemental'),
    (select id from sets where short_name = 'plc'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blightspeaker'),
    (select id from sets where short_name = 'plc'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stingscourger'),
    (select id from sets where short_name = 'plc'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harmonize'),
    (select id from sets where short_name = 'plc'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Riptide Pilferer'),
    (select id from sets where short_name = 'plc'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Veiling Oddity'),
    (select id from sets where short_name = 'plc'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fatal Frenzy'),
    (select id from sets where short_name = 'plc'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ridged Kusite'),
    (select id from sets where short_name = 'plc'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fungal Behemoth'),
    (select id from sets where short_name = 'plc'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghost Tactician'),
    (select id from sets where short_name = 'plc'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mycologist'),
    (select id from sets where short_name = 'plc'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sophic Centaur'),
    (select id from sets where short_name = 'plc'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Voidstone Gargoyle'),
    (select id from sets where short_name = 'plc'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Piracy Charm'),
    (select id from sets where short_name = 'plc'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Venarian Glimmer'),
    (select id from sets where short_name = 'plc'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Simian Spirit Guide'),
    (select id from sets where short_name = 'plc'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saltfield Recluse'),
    (select id from sets where short_name = 'plc'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dichotomancy'),
    (select id from sets where short_name = 'plc'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Timebender'),
    (select id from sets where short_name = 'plc'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crovax, Ascendant Hero'),
    (select id from sets where short_name = 'plc'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreamscape Artist'),
    (select id from sets where short_name = 'plc'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keldon Marauders'),
    (select id from sets where short_name = 'plc'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadwood Treefolk'),
    (select id from sets where short_name = 'plc'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Needlepeak Spider'),
    (select id from sets where short_name = 'plc'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fa''adiyah Seer'),
    (select id from sets where short_name = 'plc'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enslave'),
    (select id from sets where short_name = 'plc'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jodah''s Avenger'),
    (select id from sets where short_name = 'plc'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sinew Sliver'),
    (select id from sets where short_name = 'plc'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirri the Cursed'),
    (select id from sets where short_name = 'plc'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Malach of the Dawn'),
    (select id from sets where short_name = 'plc'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aether Membrane'),
    (select id from sets where short_name = 'plc'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hammerheim Deadeye'),
    (select id from sets where short_name = 'plc'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Wumpus'),
    (select id from sets where short_name = 'plc'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boom // Bust'),
    (select id from sets where short_name = 'plc'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Tithe'),
    (select id from sets where short_name = 'plc'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Radha, Heir to Keld'),
    (select id from sets where short_name = 'plc'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dash Hopes'),
    (select id from sets where short_name = 'plc'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brute Force'),
    (select id from sets where short_name = 'plc'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temporal Extortion'),
    (select id from sets where short_name = 'plc'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magus of the Library'),
    (select id from sets where short_name = 'plc'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra''s Boon'),
    (select id from sets where short_name = 'plc'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reality Acid'),
    (select id from sets where short_name = 'plc'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pouncing Wurm'),
    (select id from sets where short_name = 'plc'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Uktabi Drake'),
    (select id from sets where short_name = 'plc'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reflex Sliver'),
    (select id from sets where short_name = 'plc'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cautery Sliver'),
    (select id from sets where short_name = 'plc'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tidewalker'),
    (select id from sets where short_name = 'plc'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kavu Predator'),
    (select id from sets where short_name = 'plc'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dead // Gone'),
    (select id from sets where short_name = 'plc'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prodigal Pyromancer'),
    (select id from sets where short_name = 'plc'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aquamorph Entity'),
    (select id from sets where short_name = 'plc'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heroes Remembered'),
    (select id from sets where short_name = 'plc'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dunerider Outlaw'),
    (select id from sets where short_name = 'plc'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mantle of Leadership'),
    (select id from sets where short_name = 'plc'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shrouded Lore'),
    (select id from sets where short_name = 'plc'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dawn Charm'),
    (select id from sets where short_name = 'plc'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Big Game Hunter'),
    (select id from sets where short_name = 'plc'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firefright Mage'),
    (select id from sets where short_name = 'plc'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chronozoa'),
    (select id from sets where short_name = 'plc'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magus of the Bazaar'),
    (select id from sets where short_name = 'plc'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ovinize'),
    (select id from sets where short_name = 'plc'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampiric Link'),
    (select id from sets where short_name = 'plc'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brain Gorgers'),
    (select id from sets where short_name = 'plc'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Waning Wurm'),
    (select id from sets where short_name = 'plc'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rathi Trapper'),
    (select id from sets where short_name = 'plc'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seal of Primordium'),
    (select id from sets where short_name = 'plc'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dormant Sliver'),
    (select id from sets where short_name = 'plc'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kor Dirge'),
    (select id from sets where short_name = 'plc'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dust Corona'),
    (select id from sets where short_name = 'plc'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Synchronous Sliver'),
    (select id from sets where short_name = 'plc'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oros, the Avenger'),
    (select id from sets where short_name = 'plc'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Auramancer''s Guise'),
    (select id from sets where short_name = 'plc'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Null Profusion'),
    (select id from sets where short_name = 'plc'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vitaspore Thallid'),
    (select id from sets where short_name = 'plc'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whitemane Lion'),
    (select id from sets where short_name = 'plc'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pongify'),
    (select id from sets where short_name = 'plc'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spitting Sliver'),
    (select id from sets where short_name = 'plc'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadly Grub'),
    (select id from sets where short_name = 'plc'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magus of the Coffers'),
    (select id from sets where short_name = 'plc'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Erratic Mutation'),
    (select id from sets where short_name = 'plc'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battering Sliver'),
    (select id from sets where short_name = 'plc'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Numot, the Devastator'),
    (select id from sets where short_name = 'plc'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magus of the Arena'),
    (select id from sets where short_name = 'plc'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wistful Thinking'),
    (select id from sets where short_name = 'plc'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riftmarked Knight'),
    (select id from sets where short_name = 'plc'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Body Double'),
    (select id from sets where short_name = 'plc'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lavacore Elemental'),
    (select id from sets where short_name = 'plc'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Healing Leaves'),
    (select id from sets where short_name = 'plc'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shaper Parasite'),
    (select id from sets where short_name = 'plc'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Utopia Vow'),
    (select id from sets where short_name = 'plc'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Essence Warden'),
    (select id from sets where short_name = 'plc'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vorosh, the Hunter'),
    (select id from sets where short_name = 'plc'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ana Battlemage'),
    (select id from sets where short_name = 'plc'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Citanul Woodreaders'),
    (select id from sets where short_name = 'plc'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magus of the Tabernacle'),
    (select id from sets where short_name = 'plc'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Molten Firebird'),
    (select id from sets where short_name = 'plc'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stormfront Riders'),
    (select id from sets where short_name = 'plc'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Psychotrope Thallid'),
    (select id from sets where short_name = 'plc'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evolution Charm'),
    (select id from sets where short_name = 'plc'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Muck Drubb'),
    (select id from sets where short_name = 'plc'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frozen Aether'),
    (select id from sets where short_name = 'plc'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Anthem'),
    (select id from sets where short_name = 'plc'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saltblast'),
    (select id from sets where short_name = 'plc'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merfolk Thaumaturgist'),
    (select id from sets where short_name = 'plc'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Calciderm'),
    (select id from sets where short_name = 'plc'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Poultice Sliver'),
    (select id from sets where short_name = 'plc'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teneb, the Harvester'),
    (select id from sets where short_name = 'plc'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Groundbreaker'),
    (select id from sets where short_name = 'plc'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spellshift'),
    (select id from sets where short_name = 'plc'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aven Riftwatcher'),
    (select id from sets where short_name = 'plc'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stonecloaker'),
    (select id from sets where short_name = 'plc'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Roiling Horror'),
    (select id from sets where short_name = 'plc'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Sphinx'),
    (select id from sets where short_name = 'plc'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Revered Dead'),
    (select id from sets where short_name = 'plc'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rebuff the Wicked'),
    (select id from sets where short_name = 'plc'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Melancholy'),
    (select id from sets where short_name = 'plc'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Timecrafting'),
    (select id from sets where short_name = 'plc'),
    '109',
    'uncommon'
) 
 on conflict do nothing;
