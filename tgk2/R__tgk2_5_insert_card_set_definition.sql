insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Wurm'),
    (select id from sets where short_name = 'tgk2'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tgk2'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tgk2'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cleric'),
    (select id from sets where short_name = 'tgk2'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ooze'),
    (select id from sets where short_name = 'tgk2'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bat'),
    (select id from sets where short_name = 'tgk2'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird'),
    (select id from sets where short_name = 'tgk2'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tgk2'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tgk2'),
    '2',
    'common'
) 
 on conflict do nothing;
