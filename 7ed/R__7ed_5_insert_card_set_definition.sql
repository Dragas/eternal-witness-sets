insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Unholy Strength'),
    (select id from sets where short_name = '7ed'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Telepathy'),
    (select id from sets where short_name = '7ed'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vengeance'),
    (select id from sets where short_name = '7ed'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '7ed'),
    '338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steal Artifact'),
    (select id from sets where short_name = '7ed'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elite Archers'),
    (select id from sets where short_name = '7ed'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bull Hippo'),
    (select id from sets where short_name = '7ed'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reclaim'),
    (select id from sets where short_name = '7ed'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blessed Reversal'),
    (select id from sets where short_name = '7ed'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghitu Fire-Eater'),
    (select id from sets where short_name = '7ed'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pariah'),
    (select id from sets where short_name = '7ed'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grapeshot Catapult'),
    (select id from sets where short_name = '7ed'),
    '299',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meekstone'),
    (select id from sets where short_name = '7ed'),
    '307',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm Cauldron'),
    (select id from sets where short_name = '7ed'),
    '320',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spineless Thug'),
    (select id from sets where short_name = '7ed'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin King'),
    (select id from sets where short_name = '7ed'),
    '190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Purify'),
    (select id from sets where short_name = '7ed'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = '7ed'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spitting Earth'),
    (select id from sets where short_name = '7ed'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Archers'),
    (select id from sets where short_name = '7ed'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancestral Memories'),
    (select id from sets where short_name = '7ed'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Impatience'),
    (select id from sets where short_name = '7ed'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trained Orgg'),
    (select id from sets where short_name = '7ed'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Horned Turtle'),
    (select id from sets where short_name = '7ed'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = '7ed'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '7ed'),
    '330',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sleight of Hand'),
    (select id from sets where short_name = '7ed'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feroz''s Ban'),
    (select id from sets where short_name = '7ed'),
    '295',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benthic Behemoth'),
    (select id from sets where short_name = '7ed'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Samite Healer'),
    (select id from sets where short_name = '7ed'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Diamond'),
    (select id from sets where short_name = '7ed'),
    '296',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '7ed'),
    '343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sisay''s Ring'),
    (select id from sets where short_name = '7ed'),
    '315',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Telepathic Spies'),
    (select id from sets where short_name = '7ed'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Underground River'),
    (select id from sets where short_name = '7ed'),
    '350',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Force Spike'),
    (select id from sets where short_name = '7ed'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '7ed'),
    '342',
    'common'
) ,
(
    (select id from mtgcard where name = 'Millstone'),
    (select id from sets where short_name = '7ed'),
    '308',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knighthood'),
    (select id from sets where short_name = '7ed'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reprocess'),
    (select id from sets where short_name = '7ed'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '7ed'),
    '329',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evacuation'),
    (select id from sets where short_name = '7ed'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Canopy Spider'),
    (select id from sets where short_name = '7ed'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crystal Rod'),
    (select id from sets where short_name = '7ed'),
    '291',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquility'),
    (select id from sets where short_name = '7ed'),
    '276',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Hulk'),
    (select id from sets where short_name = '7ed'),
    '312',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = '7ed'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea Monster'),
    (select id from sets where short_name = '7ed'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Elemental'),
    (select id from sets where short_name = '7ed'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wood Elves'),
    (select id from sets where short_name = '7ed'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blaze'),
    (select id from sets where short_name = '7ed'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bedlam'),
    (select id from sets where short_name = '7ed'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nature''s Resurgence'),
    (select id from sets where short_name = '7ed'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '7ed'),
    '333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = '7ed'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = '7ed'),
    '327',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bellowing Fiend'),
    (select id from sets where short_name = '7ed'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Megrim'),
    (select id from sets where short_name = '7ed'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Memory Lapse'),
    (select id from sets where short_name = '7ed'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Royal Guard'),
    (select id from sets where short_name = '7ed'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Staunch Defenders'),
    (select id from sets where short_name = '7ed'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Howl from Beyond'),
    (select id from sets where short_name = '7ed'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plague Beetle'),
    (select id from sets where short_name = '7ed'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wind Dancer'),
    (select id from sets where short_name = '7ed'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Raider'),
    (select id from sets where short_name = '7ed'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Okk'),
    (select id from sets where short_name = '7ed'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Fire'),
    (select id from sets where short_name = '7ed'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fleeting Image'),
    (select id from sets where short_name = '7ed'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reprisal'),
    (select id from sets where short_name = '7ed'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodshot Cyclops'),
    (select id from sets where short_name = '7ed'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flight'),
    (select id from sets where short_name = '7ed'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Venerable Monk'),
    (select id from sets where short_name = '7ed'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jayemdae Tome'),
    (select id from sets where short_name = '7ed'),
    '305',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Throne of Bone'),
    (select id from sets where short_name = '7ed'),
    '322',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Silverback'),
    (select id from sets where short_name = '7ed'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Elite Infantry'),
    (select id from sets where short_name = '7ed'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tolarian Winds'),
    (select id from sets where short_name = '7ed'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grafted Skullcap'),
    (select id from sets where short_name = '7ed'),
    '298',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = '7ed'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Lyrist'),
    (select id from sets where short_name = '7ed'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pygmy Pyrosaur'),
    (select id from sets where short_name = '7ed'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '7ed'),
    '344',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stream of Life'),
    (select id from sets where short_name = '7ed'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serpent Warrior'),
    (select id from sets where short_name = '7ed'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Oriflamme'),
    (select id from sets where short_name = '7ed'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Squall'),
    (select id from sets where short_name = '7ed'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eager Cadet'),
    (select id from sets where short_name = '7ed'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mawcor'),
    (select id from sets where short_name = '7ed'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Intrepid Hero'),
    (select id from sets where short_name = '7ed'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sabretooth Tiger'),
    (select id from sets where short_name = '7ed'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treefolk Seedlings'),
    (select id from sets where short_name = '7ed'),
    '277',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karplusan Forest'),
    (select id from sets where short_name = '7ed'),
    '336',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyrotechnics'),
    (select id from sets where short_name = '7ed'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Howling Mine'),
    (select id from sets where short_name = '7ed'),
    '300',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merfolk Looter'),
    (select id from sets where short_name = '7ed'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '7ed'),
    '341',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Growth'),
    (select id from sets where short_name = '7ed'),
    '282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boil'),
    (select id from sets where short_name = '7ed'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fervor'),
    (select id from sets where short_name = '7ed'),
    '181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crimson Hellkite'),
    (select id from sets where short_name = '7ed'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bog Wraith'),
    (select id from sets where short_name = '7ed'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = '7ed'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corrupt'),
    (select id from sets where short_name = '7ed'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Warrior'),
    (select id from sets where short_name = '7ed'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Worship'),
    (select id from sets where short_name = '7ed'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deflection'),
    (select id from sets where short_name = '7ed'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Octopus'),
    (select id from sets where short_name = '7ed'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grizzly Bears'),
    (select id from sets where short_name = '7ed'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vigilant Drake'),
    (select id from sets where short_name = '7ed'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Spears'),
    (select id from sets where short_name = '7ed'),
    '323',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mahamoti Djinn'),
    (select id from sets where short_name = '7ed'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anaconda'),
    (select id from sets where short_name = '7ed'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Northern Paladin'),
    (select id from sets where short_name = '7ed'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Femeref Archers'),
    (select id from sets where short_name = '7ed'),
    '244',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fallen Angel'),
    (select id from sets where short_name = '7ed'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Champion'),
    (select id from sets where short_name = '7ed'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fog'),
    (select id from sets where short_name = '7ed'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Net'),
    (select id from sets where short_name = '7ed'),
    '317',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raging Goblin'),
    (select id from sets where short_name = '7ed'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pride of Lions'),
    (select id from sets where short_name = '7ed'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dakmor Lancer'),
    (select id from sets where short_name = '7ed'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beast of Burden'),
    (select id from sets where short_name = '7ed'),
    '287',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lone Wolf'),
    (select id from sets where short_name = '7ed'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Razortooth Rats'),
    (select id from sets where short_name = '7ed'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rod of Ruin'),
    (select id from sets where short_name = '7ed'),
    '314',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ogre Taskmaster'),
    (select id from sets where short_name = '7ed'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '7ed'),
    '347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Master Healer'),
    (select id from sets where short_name = '7ed'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Regeneration'),
    (select id from sets where short_name = '7ed'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iron Star'),
    (select id from sets where short_name = '7ed'),
    '301',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Elemental'),
    (select id from sets where short_name = '7ed'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Levitation'),
    (select id from sets where short_name = '7ed'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Uktabi Wildcats'),
    (select id from sets where short_name = '7ed'),
    '278',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Charcoal Diamond'),
    (select id from sets where short_name = '7ed'),
    '289',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Clash'),
    (select id from sets where short_name = '7ed'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fighting Drake'),
    (select id from sets where short_name = '7ed'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Looming Shade'),
    (select id from sets where short_name = '7ed'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nocturnal Raid'),
    (select id from sets where short_name = '7ed'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aether Flash'),
    (select id from sets where short_name = '7ed'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thieving Magpie'),
    (select id from sets where short_name = '7ed'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Swords'),
    (select id from sets where short_name = '7ed'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyroclasm'),
    (select id from sets where short_name = '7ed'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fyndhorn Elder'),
    (select id from sets where short_name = '7ed'),
    '246',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wind Drake'),
    (select id from sets where short_name = '7ed'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baleful Stare'),
    (select id from sets where short_name = '7ed'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Befoul'),
    (select id from sets where short_name = '7ed'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daring Apprentice'),
    (select id from sets where short_name = '7ed'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Early Harvest'),
    (select id from sets where short_name = '7ed'),
    '237',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Puzzle Box'),
    (select id from sets where short_name = '7ed'),
    '321',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Storm Crow'),
    (select id from sets where short_name = '7ed'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flying Carpet'),
    (select id from sets where short_name = '7ed'),
    '297',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vernal Bloom'),
    (select id from sets where short_name = '7ed'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bog Imp'),
    (select id from sets where short_name = '7ed'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = '7ed'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scavenger Folk'),
    (select id from sets where short_name = '7ed'),
    '267',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Digging Team'),
    (select id from sets where short_name = '7ed'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lava Axe'),
    (select id from sets where short_name = '7ed'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coral Merfolk'),
    (select id from sets where short_name = '7ed'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudchaser Eagle'),
    (select id from sets where short_name = '7ed'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wildfire'),
    (select id from sets where short_name = '7ed'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tainted Aether'),
    (select id from sets where short_name = '7ed'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Glider'),
    (select id from sets where short_name = '7ed'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Blast'),
    (select id from sets where short_name = '7ed'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunweb'),
    (select id from sets where short_name = '7ed'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dingus Egg'),
    (select id from sets where short_name = '7ed'),
    '292',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Matron'),
    (select id from sets where short_name = '7ed'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Static Orb'),
    (select id from sets where short_name = '7ed'),
    '319',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Caltrops'),
    (select id from sets where short_name = '7ed'),
    '288',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Colossus'),
    (select id from sets where short_name = '7ed'),
    '311',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Piper'),
    (select id from sets where short_name = '7ed'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Opposition'),
    (select id from sets where short_name = '7ed'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Longbow Archer'),
    (select id from sets where short_name = '7ed'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inspiration'),
    (select id from sets where short_name = '7ed'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heavy Ballista'),
    (select id from sets where short_name = '7ed'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brushland'),
    (select id from sets where short_name = '7ed'),
    '326',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Standing Troops'),
    (select id from sets where short_name = '7ed'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wooden Sphere'),
    (select id from sets where short_name = '7ed'),
    '324',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rag Man'),
    (select id from sets where short_name = '7ed'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archivist'),
    (select id from sets where short_name = '7ed'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = '7ed'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Bone'),
    (select id from sets where short_name = '7ed'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fear'),
    (select id from sets where short_name = '7ed'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Patagia Golem'),
    (select id from sets where short_name = '7ed'),
    '310',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Granite Grip'),
    (select id from sets where short_name = '7ed'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Revenant'),
    (select id from sets where short_name = '7ed'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shanodin Dryads'),
    (select id from sets where short_name = '7ed'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Cockroach'),
    (select id from sets where short_name = '7ed'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Compost'),
    (select id from sets where short_name = '7ed'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = '7ed'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = '7ed'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hollow Dogs'),
    (select id from sets where short_name = '7ed'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure Trove'),
    (select id from sets where short_name = '7ed'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thoughtleech'),
    (select id from sets where short_name = '7ed'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = '7ed'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coat of Arms'),
    (select id from sets where short_name = '7ed'),
    '290',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Healing Salve'),
    (select id from sets where short_name = '7ed'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Agonizing Memories'),
    (select id from sets where short_name = '7ed'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pillage'),
    (select id from sets where short_name = '7ed'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ensnaring Bridge'),
    (select id from sets where short_name = '7ed'),
    '294',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blanchwood Armor'),
    (select id from sets where short_name = '7ed'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rolling Stones'),
    (select id from sets where short_name = '7ed'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = '7ed'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = '7ed'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moss Diamond'),
    (select id from sets where short_name = '7ed'),
    '309',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Adarkar Wastes'),
    (select id from sets where short_name = '7ed'),
    '325',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Redwood Treefolk'),
    (select id from sets where short_name = '7ed'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Banishing'),
    (select id from sets where short_name = '7ed'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necrologia'),
    (select id from sets where short_name = '7ed'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gorilla Chieftain'),
    (select id from sets where short_name = '7ed'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Gardener'),
    (select id from sets where short_name = '7ed'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Starlight'),
    (select id from sets where short_name = '7ed'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blood Pet'),
    (select id from sets where short_name = '7ed'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = '7ed'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Short'),
    (select id from sets where short_name = '7ed'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relentless Assault'),
    (select id from sets where short_name = '7ed'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jalum Tome'),
    (select id from sets where short_name = '7ed'),
    '303',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Familiar Ground'),
    (select id from sets where short_name = '7ed'),
    '243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Feast'),
    (select id from sets where short_name = '7ed'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = '7ed'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merfolk of the Pearl Trident'),
    (select id from sets where short_name = '7ed'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glacial Wall'),
    (select id from sets where short_name = '7ed'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Spelunkers'),
    (select id from sets where short_name = '7ed'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Advocate'),
    (select id from sets where short_name = '7ed'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Falcon'),
    (select id from sets where short_name = '7ed'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '7ed'),
    '332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctimony'),
    (select id from sets where short_name = '7ed'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '7ed'),
    '331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scathe Zombies'),
    (select id from sets where short_name = '7ed'),
    '161s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abyssal Specter'),
    (select id from sets where short_name = '7ed'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '7ed'),
    '328',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hibernation'),
    (select id from sets where short_name = '7ed'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marble Diamond'),
    (select id from sets where short_name = '7ed'),
    '306',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Western Paladin'),
    (select id from sets where short_name = '7ed'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eastern Paladin'),
    (select id from sets where short_name = '7ed'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elder Druid'),
    (select id from sets where short_name = '7ed'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Delusions of Mediocrity'),
    (select id from sets where short_name = '7ed'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ostracize'),
    (select id from sets where short_name = '7ed'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oppression'),
    (select id from sets where short_name = '7ed'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Air'),
    (select id from sets where short_name = '7ed'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '7ed'),
    '339',
    'common'
) ,
(
    (select id from mtgcard where name = 'Storm Shaman'),
    (select id from sets where short_name = '7ed'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darkest Hour'),
    (select id from sets where short_name = '7ed'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reverse Damage'),
    (select id from sets where short_name = '7ed'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Honor Guard'),
    (select id from sets where short_name = '7ed'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Embermage'),
    (select id from sets where short_name = '7ed'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Might of Oaks'),
    (select id from sets where short_name = '7ed'),
    '257',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin War Drums'),
    (select id from sets where short_name = '7ed'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tremor'),
    (select id from sets where short_name = '7ed'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Southern Paladin'),
    (select id from sets where short_name = '7ed'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angelic Page'),
    (select id from sets where short_name = '7ed'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sky Diamond'),
    (select id from sets where short_name = '7ed'),
    '316',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Maro'),
    (select id from sets where short_name = '7ed'),
    '256',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: White'),
    (select id from sets where short_name = '7ed'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Engineered Plague'),
    (select id from sets where short_name = '7ed'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disorder'),
    (select id from sets where short_name = '7ed'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pit Trap'),
    (select id from sets where short_name = '7ed'),
    '313',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nausea'),
    (select id from sets where short_name = '7ed'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Black'),
    (select id from sets where short_name = '7ed'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = '7ed'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spirit Link'),
    (select id from sets where short_name = '7ed'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Untamed Wilds'),
    (select id from sets where short_name = '7ed'),
    '279',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Blue'),
    (select id from sets where short_name = '7ed'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rowen'),
    (select id from sets where short_name = '7ed'),
    '266',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trained Armodon'),
    (select id from sets where short_name = '7ed'),
    '275',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drudge Skeletons'),
    (select id from sets where short_name = '7ed'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fugue'),
    (select id from sets where short_name = '7ed'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = '7ed'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vizzerdrix'),
    (select id from sets where short_name = '7ed'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = '7ed'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = '7ed'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seasoned Marshal'),
    (select id from sets where short_name = '7ed'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drudge Skeletons'),
    (select id from sets where short_name = '7ed'),
    '130s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dregs of Sorrow'),
    (select id from sets where short_name = '7ed'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ardent Militia'),
    (select id from sets where short_name = '7ed'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '7ed'),
    '348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Razorfoot Griffin'),
    (select id from sets where short_name = '7ed'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abyssal Horror'),
    (select id from sets where short_name = '7ed'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gang of Elk'),
    (select id from sets where short_name = '7ed'),
    '247',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Holy Strength'),
    (select id from sets where short_name = '7ed'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bereavement'),
    (select id from sets where short_name = '7ed'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '7ed'),
    '349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opportunity'),
    (select id from sets where short_name = '7ed'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '7ed'),
    '334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seeker of Skybreak'),
    (select id from sets where short_name = '7ed'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prodigal Sorcerer'),
    (select id from sets where short_name = '7ed'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sulfurous Springs'),
    (select id from sets where short_name = '7ed'),
    '345',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wing Snare'),
    (select id from sets where short_name = '7ed'),
    '283',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seismic Assault'),
    (select id from sets where short_name = '7ed'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sacred Ground'),
    (select id from sets where short_name = '7ed'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shield Wall'),
    (select id from sets where short_name = '7ed'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jandor''s Saddlebags'),
    (select id from sets where short_name = '7ed'),
    '304',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crossbow Infantry'),
    (select id from sets where short_name = '7ed'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foul Imp'),
    (select id from sets where short_name = '7ed'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Confiscate'),
    (select id from sets where short_name = '7ed'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight Errant'),
    (select id from sets where short_name = '7ed'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strands of Night'),
    (select id from sets where short_name = '7ed'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = '7ed'),
    '157s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Twiddle'),
    (select id from sets where short_name = '7ed'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Verduran Enchantress'),
    (select id from sets where short_name = '7ed'),
    '280',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sustainer of the Realm'),
    (select id from sets where short_name = '7ed'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Persecute'),
    (select id from sets where short_name = '7ed'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spined Wurm'),
    (select id from sets where short_name = '7ed'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '7ed'),
    '346',
    'common'
) ,
(
    (select id from mtgcard where name = 'Equilibrium'),
    (select id from sets where short_name = '7ed'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = '7ed'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thorn Elemental'),
    (select id from sets where short_name = '7ed'),
    '273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nature''s Revolt'),
    (select id from sets where short_name = '7ed'),
    '260',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lure'),
    (select id from sets where short_name = '7ed'),
    '255',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balduvian Barbarians'),
    (select id from sets where short_name = '7ed'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ivory Cup'),
    (select id from sets where short_name = '7ed'),
    '302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Creeping Mold'),
    (select id from sets where short_name = '7ed'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Breach'),
    (select id from sets where short_name = '7ed'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inferno'),
    (select id from sets where short_name = '7ed'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Enchantress'),
    (select id from sets where short_name = '7ed'),
    '285',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gerrard''s Wisdom'),
    (select id from sets where short_name = '7ed'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leshrac''s Rite'),
    (select id from sets where short_name = '7ed'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Artillery'),
    (select id from sets where short_name = '7ed'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glorious Anthem'),
    (select id from sets where short_name = '7ed'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hill Giant'),
    (select id from sets where short_name = '7ed'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Wonder'),
    (select id from sets where short_name = '7ed'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = '7ed'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charcoal Diamond'),
    (select id from sets where short_name = '7ed'),
    '289s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Monstrous Growth'),
    (select id from sets where short_name = '7ed'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sage Owl'),
    (select id from sets where short_name = '7ed'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aladdin''s Ring'),
    (select id from sets where short_name = '7ed'),
    '286',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scathe Zombies'),
    (select id from sets where short_name = '7ed'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = '7ed'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = '7ed'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boomerang'),
    (select id from sets where short_name = '7ed'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temporal Adept'),
    (select id from sets where short_name = '7ed'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sudden Impact'),
    (select id from sets where short_name = '7ed'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hurricane'),
    (select id from sets where short_name = '7ed'),
    '252',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Greed'),
    (select id from sets where short_name = '7ed'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Remove Soul'),
    (select id from sets where short_name = '7ed'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sacred Nectar'),
    (select id from sets where short_name = '7ed'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stronghold Assassin'),
    (select id from sets where short_name = '7ed'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infernal Contract'),
    (select id from sets where short_name = '7ed'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yawgmoth''s Edict'),
    (select id from sets where short_name = '7ed'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Chariot'),
    (select id from sets where short_name = '7ed'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Green'),
    (select id from sets where short_name = '7ed'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Hammer'),
    (select id from sets where short_name = '7ed'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disrupting Scepter'),
    (select id from sets where short_name = '7ed'),
    '293',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crypt Rats'),
    (select id from sets where short_name = '7ed'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcane Laboratory'),
    (select id from sets where short_name = '7ed'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spellbook'),
    (select id from sets where short_name = '7ed'),
    '318',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Final Fortune'),
    (select id from sets where short_name = '7ed'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lord of Atlantis'),
    (select id from sets where short_name = '7ed'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reflexes'),
    (select id from sets where short_name = '7ed'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '7ed'),
    '337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Breath of Life'),
    (select id from sets where short_name = '7ed'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra''s Embrace'),
    (select id from sets where short_name = '7ed'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '7ed'),
    '335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '7ed'),
    '340',
    'common'
) ,
(
    (select id from mtgcard where name = 'Castle'),
    (select id from sets where short_name = '7ed'),
    '5',
    'uncommon'
) 
 on conflict do nothing;
