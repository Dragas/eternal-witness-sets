insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Banewhip Punisher'),
    (select id from sets where short_name = 'hou'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hour of Eternity'),
    (select id from sets where short_name = 'hou'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Torment of Scarabs'),
    (select id from sets where short_name = 'hou'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dune Diviner'),
    (select id from sets where short_name = 'hou'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thorned Moloch'),
    (select id from sets where short_name = 'hou'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dauntless Aven'),
    (select id from sets where short_name = 'hou'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven of Enduring Hope'),
    (select id from sets where short_name = 'hou'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Resilient Khenra'),
    (select id from sets where short_name = 'hou'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grisly Survivor'),
    (select id from sets where short_name = 'hou'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grind // Dust'),
    (select id from sets where short_name = 'hou'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eternal of Harsh Truths'),
    (select id from sets where short_name = 'hou'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Djeru''s Renunciation'),
    (select id from sets where short_name = 'hou'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avid Reclaimer'),
    (select id from sets where short_name = 'hou'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desert of the Glorified'),
    (select id from sets where short_name = 'hou'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Refuse // Cooperate'),
    (select id from sets where short_name = 'hou'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oketra''s Avenger'),
    (select id from sets where short_name = 'hou'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frilled Sandwalla'),
    (select id from sets where short_name = 'hou'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'hou'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'hou'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Samut, the Tested'),
    (select id from sets where short_name = 'hou'),
    '144',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'hou'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Solemnity'),
    (select id from sets where short_name = 'hou'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ifnir Deadlands'),
    (select id from sets where short_name = 'hou'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inferno Jet'),
    (select id from sets where short_name = 'hou'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Earthshaker Khenra'),
    (select id from sets where short_name = 'hou'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woodland Stream'),
    (select id from sets where short_name = 'hou'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'hou'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sandblast'),
    (select id from sets where short_name = 'hou'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Defeat'),
    (select id from sets where short_name = 'hou'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merciless Eternal'),
    (select id from sets where short_name = 'hou'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Claim // Fame'),
    (select id from sets where short_name = 'hou'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doomfall'),
    (select id from sets where short_name = 'hou'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zealot of the God-Pharaoh'),
    (select id from sets where short_name = 'hou'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Traveler''s Amulet'),
    (select id from sets where short_name = 'hou'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ammit Eternal'),
    (select id from sets where short_name = 'hou'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bontu''s Last Reckoning'),
    (select id from sets where short_name = 'hou'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Consign // Oblivion'),
    (select id from sets where short_name = 'hou'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brambleweft Behemoth'),
    (select id from sets where short_name = 'hou'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reason // Believe'),
    (select id from sets where short_name = 'hou'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ipnu Rivulet'),
    (select id from sets where short_name = 'hou'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Djeru, With Eyes Open'),
    (select id from sets where short_name = 'hou'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ominous Sphinx'),
    (select id from sets where short_name = 'hou'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hostile Desert'),
    (select id from sets where short_name = 'hou'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nimble Obstructionist'),
    (select id from sets where short_name = 'hou'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirage Mirror'),
    (select id from sets where short_name = 'hou'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa, Genesis Mage'),
    (select id from sets where short_name = 'hou'),
    '200',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Appeal // Authority'),
    (select id from sets where short_name = 'hou'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Torment of Venom'),
    (select id from sets where short_name = 'hou'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cunning Survivor'),
    (select id from sets where short_name = 'hou'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dunes of the Dead'),
    (select id from sets where short_name = 'hou'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunset Pyramid'),
    (select id from sets where short_name = 'hou'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Apocalypse Demon'),
    (select id from sets where short_name = 'hou'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gift of Strength'),
    (select id from sets where short_name = 'hou'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hashep Oasis'),
    (select id from sets where short_name = 'hou'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scrounger of Souls'),
    (select id from sets where short_name = 'hou'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tenacious Hunter'),
    (select id from sets where short_name = 'hou'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hollow One'),
    (select id from sets where short_name = 'hou'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'hou'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Open Fire'),
    (select id from sets where short_name = 'hou'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wasp of the Bitter End'),
    (select id from sets where short_name = 'hou'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gideon''s Defeat'),
    (select id from sets where short_name = 'hou'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodwater Entity'),
    (select id from sets where short_name = 'hou'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crash Through'),
    (select id from sets where short_name = 'hou'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampaging Hippo'),
    (select id from sets where short_name = 'hou'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kefnet''s Last Word'),
    (select id from sets where short_name = 'hou'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Locust God'),
    (select id from sets where short_name = 'hou'),
    '139',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Imaginary Threats'),
    (select id from sets where short_name = 'hou'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unconventional Tactics'),
    (select id from sets where short_name = 'hou'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burning-Fist Minotaur'),
    (select id from sets where short_name = 'hou'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Neheb, the Eternal'),
    (select id from sets where short_name = 'hou'),
    '104',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Carrion Screecher'),
    (select id from sets where short_name = 'hou'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'River Hoopoe'),
    (select id from sets where short_name = 'hou'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = 'hou'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreamstealer'),
    (select id from sets where short_name = 'hou'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sifter Wurm'),
    (select id from sets where short_name = 'hou'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Life Goes On'),
    (select id from sets where short_name = 'hou'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shefet Dunes'),
    (select id from sets where short_name = 'hou'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Granitic Titan'),
    (select id from sets where short_name = 'hou'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Scarab God'),
    (select id from sets where short_name = 'hou'),
    '145',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Defiant Khenra'),
    (select id from sets where short_name = 'hou'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vizier of the Anointed'),
    (select id from sets where short_name = 'hou'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Accursed Horde'),
    (select id from sets where short_name = 'hou'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quarry Beetle'),
    (select id from sets where short_name = 'hou'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'hou'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scavenger Grounds'),
    (select id from sets where short_name = 'hou'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firebrand Archer'),
    (select id from sets where short_name = 'hou'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steadfast Sentinel'),
    (select id from sets where short_name = 'hou'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Resolute Survivors'),
    (select id from sets where short_name = 'hou'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vile Manifestation'),
    (select id from sets where short_name = 'hou'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crested Sunmare'),
    (select id from sets where short_name = 'hou'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, the Deceiver'),
    (select id from sets where short_name = 'hou'),
    '205',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dutiful Servants'),
    (select id from sets where short_name = 'hou'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Struggle // Survive'),
    (select id from sets where short_name = 'hou'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magmaroth'),
    (select id from sets where short_name = 'hou'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Driven // Despair'),
    (select id from sets where short_name = 'hou'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Majestic Myriarch'),
    (select id from sets where short_name = 'hou'),
    '122',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Crook of Condemnation'),
    (select id from sets where short_name = 'hou'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fraying Sanity'),
    (select id from sets where short_name = 'hou'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desert of the Indomitable'),
    (select id from sets where short_name = 'hou'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seer of the Last Tomorrow'),
    (select id from sets where short_name = 'hou'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hazoret''s Undying Fury'),
    (select id from sets where short_name = 'hou'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desert''s Hold'),
    (select id from sets where short_name = 'hou'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chaos Maw'),
    (select id from sets where short_name = 'hou'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abrade'),
    (select id from sets where short_name = 'hou'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nissa''s Defeat'),
    (select id from sets where short_name = 'hou'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Adorned Pouncer'),
    (select id from sets where short_name = 'hou'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overwhelming Splendor'),
    (select id from sets where short_name = 'hou'),
    '19',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Graven Abomination'),
    (select id from sets where short_name = 'hou'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hour of Revelation'),
    (select id from sets where short_name = 'hou'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Solitary Camel'),
    (select id from sets where short_name = 'hou'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frontline Devastator'),
    (select id from sets where short_name = 'hou'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riddleform'),
    (select id from sets where short_name = 'hou'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bitterbow Sharpshooters'),
    (select id from sets where short_name = 'hou'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ramunap Excavator'),
    (select id from sets where short_name = 'hou'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel of Condemnation'),
    (select id from sets where short_name = 'hou'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruin Rat'),
    (select id from sets where short_name = 'hou'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desert of the Mindful'),
    (select id from sets where short_name = 'hou'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ramunap Hydra'),
    (select id from sets where short_name = 'hou'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strategic Planning'),
    (select id from sets where short_name = 'hou'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Devotee of Strength'),
    (select id from sets where short_name = 'hou'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, God-Pharaoh'),
    (select id from sets where short_name = 'hou'),
    '140',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rhonas''s Stalwart'),
    (select id from sets where short_name = 'hou'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saving Grace'),
    (select id from sets where short_name = 'hou'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'God-Pharaoh''s Faithful'),
    (select id from sets where short_name = 'hou'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Endless Sands'),
    (select id from sets where short_name = 'hou'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lurching Rotbeast'),
    (select id from sets where short_name = 'hou'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hope Tender'),
    (select id from sets where short_name = 'hou'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tragic Lesson'),
    (select id from sets where short_name = 'hou'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oasis Ritualist'),
    (select id from sets where short_name = 'hou'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unquenchable Thirst'),
    (select id from sets where short_name = 'hou'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nissa''s Encouragement'),
    (select id from sets where short_name = 'hou'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imminent Doom'),
    (select id from sets where short_name = 'hou'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spellweaver Eternal'),
    (select id from sets where short_name = 'hou'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'hou'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Khenra Eternal'),
    (select id from sets where short_name = 'hou'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dagger of the Worthy'),
    (select id from sets where short_name = 'hou'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel of the God-Pharaoh'),
    (select id from sets where short_name = 'hou'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disposal Mummy'),
    (select id from sets where short_name = 'hou'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obelisk Spider'),
    (select id from sets where short_name = 'hou'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ramunap Ruins'),
    (select id from sets where short_name = 'hou'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Manticore Eternal'),
    (select id from sets where short_name = 'hou'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'hou'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'hou'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'God-Pharaoh''s Gift'),
    (select id from sets where short_name = 'hou'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blur of Blades'),
    (select id from sets where short_name = 'hou'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Supreme Will'),
    (select id from sets where short_name = 'hou'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aerial Guide'),
    (select id from sets where short_name = 'hou'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feral Prowler'),
    (select id from sets where short_name = 'hou'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Striped Riverwinder'),
    (select id from sets where short_name = 'hou'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Uncage the Menagerie'),
    (select id from sets where short_name = 'hou'),
    '137',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Harrier Naga'),
    (select id from sets where short_name = 'hou'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Act of Heroism'),
    (select id from sets where short_name = 'hou'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vizier of the True'),
    (select id from sets where short_name = 'hou'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abandoned Sarcophagus'),
    (select id from sets where short_name = 'hou'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'hou'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swarm Intelligence'),
    (select id from sets where short_name = 'hou'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'hou'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sidewinder Naga'),
    (select id from sets where short_name = 'hou'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oketra''s Last Mercy'),
    (select id from sets where short_name = 'hou'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'hou'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hour of Promise'),
    (select id from sets where short_name = 'hou'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jace''s Defeat'),
    (select id from sets where short_name = 'hou'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Survivors'' Encampment'),
    (select id from sets where short_name = 'hou'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crypt of the Eternals'),
    (select id from sets where short_name = 'hou'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cinder Barrens'),
    (select id from sets where short_name = 'hou'),
    '209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wretched Camel'),
    (select id from sets where short_name = 'hou'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'hou'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ambuscade'),
    (select id from sets where short_name = 'hou'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kindled Fury'),
    (select id from sets where short_name = 'hou'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overcome'),
    (select id from sets where short_name = 'hou'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Forgotten Pharaohs'),
    (select id from sets where short_name = 'hou'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hour of Devastation'),
    (select id from sets where short_name = 'hou'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Manalith'),
    (select id from sets where short_name = 'hou'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Without Weakness'),
    (select id from sets where short_name = 'hou'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Champion of Wits'),
    (select id from sets where short_name = 'hou'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Puncturing Blow'),
    (select id from sets where short_name = 'hou'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Proven Combatant'),
    (select id from sets where short_name = 'hou'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Farm // Market'),
    (select id from sets where short_name = 'hou'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Razaketh''s Rite'),
    (select id from sets where short_name = 'hou'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pride Sovereign'),
    (select id from sets where short_name = 'hou'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sand Strangler'),
    (select id from sets where short_name = 'hou'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marauding Boneslasher'),
    (select id from sets where short_name = 'hou'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Countervailing Winds'),
    (select id from sets where short_name = 'hou'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Scorpion God'),
    (select id from sets where short_name = 'hou'),
    '146',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Razaketh, the Foulblooded'),
    (select id from sets where short_name = 'hou'),
    '73',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Desert of the True'),
    (select id from sets where short_name = 'hou'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fervent Paincaster'),
    (select id from sets where short_name = 'hou'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Khenra Scrapper'),
    (select id from sets where short_name = 'hou'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mummy Paramount'),
    (select id from sets where short_name = 'hou'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beneath the Sands'),
    (select id from sets where short_name = 'hou'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhonas''s Last Stand'),
    (select id from sets where short_name = 'hou'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'hou'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lethal Sting'),
    (select id from sets where short_name = 'hou'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sinuous Striker'),
    (select id from sets where short_name = 'hou'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steward of Solidarity'),
    (select id from sets where short_name = 'hou'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moaning Wall'),
    (select id from sets where short_name = 'hou'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'hou'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunscourge Champion'),
    (select id from sets where short_name = 'hou'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aven Reedstalker'),
    (select id from sets where short_name = 'hou'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Visage of Bolas'),
    (select id from sets where short_name = 'hou'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hour of Glory'),
    (select id from sets where short_name = 'hou'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Defeat'),
    (select id from sets where short_name = 'hou'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desert of the Fervent'),
    (select id from sets where short_name = 'hou'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gilded Cerodon'),
    (select id from sets where short_name = 'hou'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Torment of Hailfire'),
    (select id from sets where short_name = 'hou'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leave // Chance'),
    (select id from sets where short_name = 'hou'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wildfire Eternal'),
    (select id from sets where short_name = 'hou'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unesh, Criosphinx Sovereign'),
    (select id from sets where short_name = 'hou'),
    '52',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Unraveling Mummy'),
    (select id from sets where short_name = 'hou'),
    '147',
    'uncommon'
) 
 on conflict do nothing;
