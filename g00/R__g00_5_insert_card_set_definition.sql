insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Vampiric Tutor'),
    (select id from sets where short_name = 'g00'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'g00'),
    '1',
    'rare'
) 
 on conflict do nothing;
