insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Fiery Hellhound'),
    (select id from sets where short_name = 'm11'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk Wildspeaker'),
    (select id from sets where short_name = 'm11'),
    '175',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Water Servant'),
    (select id from sets where short_name = 'm11'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm11'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Berserkers of Blood Ridge'),
    (select id from sets where short_name = 'm11'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Day of Judgment'),
    (select id from sets where short_name = 'm11'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frost Titan'),
    (select id from sets where short_name = 'm11'),
    '55',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Prodigal Pyromancer'),
    (select id from sets where short_name = 'm11'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Mantra'),
    (select id from sets where short_name = 'm11'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm11'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Adept'),
    (select id from sets where short_name = 'm11'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Knight'),
    (select id from sets where short_name = 'm11'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Caress'),
    (select id from sets where short_name = 'm11'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wurm''s Tooth'),
    (select id from sets where short_name = 'm11'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stormtide Leviathan'),
    (select id from sets where short_name = 'm11'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Air Servant'),
    (select id from sets where short_name = 'm11'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragonskull Summit'),
    (select id from sets where short_name = 'm11'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mass Polymorph'),
    (select id from sets where short_name = 'm11'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Time Reversal'),
    (select id from sets where short_name = 'm11'),
    '75',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mystifying Maze'),
    (select id from sets where short_name = 'm11'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm11'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conundrum Sphinx'),
    (select id from sets where short_name = 'm11'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Destructive Force'),
    (select id from sets where short_name = 'm11'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'm11'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runeclaw Bear'),
    (select id from sets where short_name = 'm11'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fling'),
    (select id from sets where short_name = 'm11'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overwhelming Stampede'),
    (select id from sets where short_name = 'm11'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roc Egg'),
    (select id from sets where short_name = 'm11'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyretic Ritual'),
    (select id from sets where short_name = 'm11'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Ascendant'),
    (select id from sets where short_name = 'm11'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hoarding Dragon'),
    (select id from sets where short_name = 'm11'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thunder Strike'),
    (select id from sets where short_name = 'm11'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Vines'),
    (select id from sets where short_name = 'm11'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm11'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quag Sickness'),
    (select id from sets where short_name = 'm11'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vulshok Berserker'),
    (select id from sets where short_name = 'm11'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Balloon Brigade'),
    (select id from sets where short_name = 'm11'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blinding Mage'),
    (select id from sets where short_name = 'm11'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leyline of Sanctity'),
    (select id from sets where short_name = 'm11'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Siege Mastodon'),
    (select id from sets where short_name = 'm11'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diminish'),
    (select id from sets where short_name = 'm11'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Raiders'),
    (select id from sets where short_name = 'm11'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm11'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight Exemplar'),
    (select id from sets where short_name = 'm11'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani Goldmane'),
    (select id from sets where short_name = 'm11'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Necrotic Plague'),
    (select id from sets where short_name = 'm11'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cultivate'),
    (select id from sets where short_name = 'm11'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunters'' Feast'),
    (select id from sets where short_name = 'm11'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm11'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = 'm11'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arc Runner'),
    (select id from sets where short_name = 'm11'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Specter'),
    (select id from sets where short_name = 'm11'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ornithopter'),
    (select id from sets where short_name = 'm11'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'm11'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace''s Erasure'),
    (select id from sets where short_name = 'm11'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sign in Blood'),
    (select id from sets where short_name = 'm11'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silence'),
    (select id from sets where short_name = 'm11'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone Golem'),
    (select id from sets where short_name = 'm11'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Honor of the Pure'),
    (select id from sets where short_name = 'm11'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shiv''s Embrace'),
    (select id from sets where short_name = 'm11'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prized Unicorn'),
    (select id from sets where short_name = 'm11'),
    '193',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Primal Cocoon'),
    (select id from sets where short_name = 'm11'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'm11'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Triskelion'),
    (select id from sets where short_name = 'm11'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'm11'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warlord''s Axe'),
    (select id from sets where short_name = 'm11'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Solemn Offering'),
    (select id from sets where short_name = 'm11'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Augury Owl'),
    (select id from sets where short_name = 'm11'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace Beleren'),
    (select id from sets where short_name = 'm11'),
    '58',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm11'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Wurm'),
    (select id from sets where short_name = 'm11'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm11'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greater Basilisk'),
    (select id from sets where short_name = 'm11'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Control'),
    (select id from sets where short_name = 'm11'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disentomb'),
    (select id from sets where short_name = 'm11'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm11'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flashfreeze'),
    (select id from sets where short_name = 'm11'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leyline of Punishment'),
    (select id from sets where short_name = 'm11'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Palace Guard'),
    (select id from sets where short_name = 'm11'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unholy Strength'),
    (select id from sets where short_name = 'm11'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sorcerer''s Strongbox'),
    (select id from sets where short_name = 'm11'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armored Cancrix'),
    (select id from sets where short_name = 'm11'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm11'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Juggernaut'),
    (select id from sets where short_name = 'm11'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armored Ascension'),
    (select id from sets where short_name = 'm11'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crystal Ball'),
    (select id from sets where short_name = 'm11'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantom Beast'),
    (select id from sets where short_name = 'm11'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon of Death''s Gate'),
    (select id from sets where short_name = 'm11'),
    '92',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Plummet'),
    (select id from sets where short_name = 'm11'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Haunting Echoes'),
    (select id from sets where short_name = 'm11'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ice Cage'),
    (select id from sets where short_name = 'm11'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = 'm11'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Ranger'),
    (select id from sets where short_name = 'm11'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grave Titan'),
    (select id from sets where short_name = 'm11'),
    '97',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Earth Servant'),
    (select id from sets where short_name = 'm11'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Redirect'),
    (select id from sets where short_name = 'm11'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viscera Seer'),
    (select id from sets where short_name = 'm11'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Acidic Slime'),
    (select id from sets where short_name = 'm11'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm11'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tome Scour'),
    (select id from sets where short_name = 'm11'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azure Drake'),
    (select id from sets where short_name = 'm11'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obstinate Baloth'),
    (select id from sets where short_name = 'm11'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'm11'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doom Blade'),
    (select id from sets where short_name = 'm11'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Revenge'),
    (select id from sets where short_name = 'm11'),
    '174',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Act of Treason'),
    (select id from sets where short_name = 'm11'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cancel'),
    (select id from sets where short_name = 'm11'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duskdale Wurm'),
    (select id from sets where short_name = 'm11'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gargoyle Sentinel'),
    (select id from sets where short_name = 'm11'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Incite'),
    (select id from sets where short_name = 'm11'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voltaic Key'),
    (select id from sets where short_name = 'm11'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Assassinate'),
    (select id from sets where short_name = 'm11'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scroll Thief'),
    (select id from sets where short_name = 'm11'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Arbiter'),
    (select id from sets where short_name = 'm11'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cudgel Troll'),
    (select id from sets where short_name = 'm11'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jinxed Idol'),
    (select id from sets where short_name = 'm11'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathmark'),
    (select id from sets where short_name = 'm11'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Chieftain'),
    (select id from sets where short_name = 'm11'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lava Axe'),
    (select id from sets where short_name = 'm11'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'm11'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Autumn''s Veil'),
    (select id from sets where short_name = 'm11'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'm11'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm11'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloud Crusader'),
    (select id from sets where short_name = 'm11'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Spitfire'),
    (select id from sets where short_name = 'm11'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Piker'),
    (select id from sets where short_name = 'm11'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk''s Companion'),
    (select id from sets where short_name = 'm11'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barony Vampire'),
    (select id from sets where short_name = 'm11'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elite Vanguard'),
    (select id from sets where short_name = 'm11'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stabbing Pain'),
    (select id from sets where short_name = 'm11'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana Vess'),
    (select id from sets where short_name = 'm11'),
    '102',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Excommunicate'),
    (select id from sets where short_name = 'm11'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'm11'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple Bell'),
    (select id from sets where short_name = 'm11'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demolish'),
    (select id from sets where short_name = 'm11'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goldenglow Moth'),
    (select id from sets where short_name = 'm11'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rise from the Grave'),
    (select id from sets where short_name = 'm11'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sleep'),
    (select id from sets where short_name = 'm11'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demon''s Horn'),
    (select id from sets where short_name = 'm11'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mitotic Slime'),
    (select id from sets where short_name = 'm11'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Hellkite'),
    (select id from sets where short_name = 'm11'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword of Vengeance'),
    (select id from sets where short_name = 'm11'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inspired Charge'),
    (select id from sets where short_name = 'm11'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diabolic Tutor'),
    (select id from sets where short_name = 'm11'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Corrupt'),
    (select id from sets where short_name = 'm11'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elixir of Immortality'),
    (select id from sets where short_name = 'm11'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Traumatize'),
    (select id from sets where short_name = 'm11'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Outrage'),
    (select id from sets where short_name = 'm11'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phylactery Lich'),
    (select id from sets where short_name = 'm11'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Tutelage'),
    (select id from sets where short_name = 'm11'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm11'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'White Knight'),
    (select id from sets where short_name = 'm11'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sacred Wolf'),
    (select id from sets where short_name = 'm11'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reassembling Skeleton'),
    (select id from sets where short_name = 'm11'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Tunneler'),
    (select id from sets where short_name = 'm11'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manic Vandal'),
    (select id from sets where short_name = 'm11'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vengeful Archon'),
    (select id from sets where short_name = 'm11'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Leak'),
    (select id from sets where short_name = 'm11'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Call to Mind'),
    (select id from sets where short_name = 'm11'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm11'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Platinum Angel'),
    (select id from sets where short_name = 'm11'),
    '212',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Harbor Serpent'),
    (select id from sets where short_name = 'm11'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rootbound Crag'),
    (select id from sets where short_name = 'm11'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Holy Strength'),
    (select id from sets where short_name = 'm11'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra Nalaar'),
    (select id from sets where short_name = 'm11'),
    '127',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'm11'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon''s Claw'),
    (select id from sets where short_name = 'm11'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hornet Sting'),
    (select id from sets where short_name = 'm11'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nature''s Spiral'),
    (select id from sets where short_name = 'm11'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brittle Effigy'),
    (select id from sets where short_name = 'm11'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'm11'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magma Phoenix'),
    (select id from sets where short_name = 'm11'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wild Evocation'),
    (select id from sets where short_name = 'm11'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merfolk Spy'),
    (select id from sets where short_name = 'm11'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Canyon Minotaur'),
    (select id from sets where short_name = 'm11'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cyclops Gladiator'),
    (select id from sets where short_name = 'm11'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jace''s Ingenuity'),
    (select id from sets where short_name = 'm11'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glacial Fortress'),
    (select id from sets where short_name = 'm11'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steel Overseer'),
    (select id from sets where short_name = 'm11'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightwing Shade'),
    (select id from sets where short_name = 'm11'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nantuko Shade'),
    (select id from sets where short_name = 'm11'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Strength'),
    (select id from sets where short_name = 'm11'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squadron Hawk'),
    (select id from sets where short_name = 'm11'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mighty Leap'),
    (select id from sets where short_name = 'm11'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kraken''s Eye'),
    (select id from sets where short_name = 'm11'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rotting Legion'),
    (select id from sets where short_name = 'm11'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Royal Assassin'),
    (select id from sets where short_name = 'm11'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stormfront Pegasus'),
    (select id from sets where short_name = 'm11'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infantry Veteran'),
    (select id from sets where short_name = 'm11'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nether Horror'),
    (select id from sets where short_name = 'm11'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dryad''s Favor'),
    (select id from sets where short_name = 'm11'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'm11'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spined Wurm'),
    (select id from sets where short_name = 'm11'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Frost'),
    (select id from sets where short_name = 'm11'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fauna Shaman'),
    (select id from sets where short_name = 'm11'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestial Purge'),
    (select id from sets where short_name = 'm11'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tireless Missionaries'),
    (select id from sets where short_name = 'm11'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = 'm11'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Tithe'),
    (select id from sets where short_name = 'm11'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Combust'),
    (select id from sets where short_name = 'm11'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clone'),
    (select id from sets where short_name = 'm11'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Condemn'),
    (select id from sets where short_name = 'm11'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = 'm11'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Safe Passage'),
    (select id from sets where short_name = 'm11'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leyline of Anticipation'),
    (select id from sets where short_name = 'm11'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whispersilk Cloak'),
    (select id from sets where short_name = 'm11'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sun Titan'),
    (select id from sets where short_name = 'm11'),
    '35',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ember Hauler'),
    (select id from sets where short_name = 'm11'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'War Priest of Thune'),
    (select id from sets where short_name = 'm11'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reverberate'),
    (select id from sets where short_name = 'm11'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'm11'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drowned Catacomb'),
    (select id from sets where short_name = 'm11'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alluring Siren'),
    (select id from sets where short_name = 'm11'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm11'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk''s Packleader'),
    (select id from sets where short_name = 'm11'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leyline of the Void'),
    (select id from sets where short_name = 'm11'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Howling Banshee'),
    (select id from sets where short_name = 'm11'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'm11'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silvercoat Lion'),
    (select id from sets where short_name = 'm11'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Preordain'),
    (select id from sets where short_name = 'm11'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Awakener Druid'),
    (select id from sets where short_name = 'm11'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodthrone Vampire'),
    (select id from sets where short_name = 'm11'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'm11'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Back to Nature'),
    (select id from sets where short_name = 'm11'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyroclasm'),
    (select id from sets where short_name = 'm11'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merfolk Sovereign'),
    (select id from sets where short_name = 'm11'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunpetal Grove'),
    (select id from sets where short_name = 'm11'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = 'm11'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel''s Feather'),
    (select id from sets where short_name = 'm11'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Child of Night'),
    (select id from sets where short_name = 'm11'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inferno Titan'),
    (select id from sets where short_name = 'm11'),
    '146',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Brindle Boar'),
    (select id from sets where short_name = 'm11'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Griffin'),
    (select id from sets where short_name = 'm11'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maritime Guard'),
    (select id from sets where short_name = 'm11'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Servant'),
    (select id from sets where short_name = 'm11'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodcrazed Goblin'),
    (select id from sets where short_name = 'm11'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leyline of Vitality'),
    (select id from sets where short_name = 'm11'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fog'),
    (select id from sets where short_name = 'm11'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Protean Hydra'),
    (select id from sets where short_name = 'm11'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Pridemate'),
    (select id from sets where short_name = 'm11'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Assault Griffin'),
    (select id from sets where short_name = 'm11'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'm11'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relentless Rats'),
    (select id from sets where short_name = 'm11'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Captivating Vampire'),
    (select id from sets where short_name = 'm11'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Archdruid'),
    (select id from sets where short_name = 'm11'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foresee'),
    (select id from sets where short_name = 'm11'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloud Elemental'),
    (select id from sets where short_name = 'm11'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primeval Titan'),
    (select id from sets where short_name = 'm11'),
    '192',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Baneslayer Angel'),
    (select id from sets where short_name = 'm11'),
    '7',
    'mythic'
) 
 on conflict do nothing;
