insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Gemrazer'),
    (select id from sets where short_name = 'iko'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lava Serpent'),
    (select id from sets where short_name = 'iko'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Hollow'),
    (select id from sets where short_name = 'iko'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archipelagore'),
    (select id from sets where short_name = 'iko'),
    '283',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raugrin Triome'),
    (select id from sets where short_name = 'iko'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primal Empathy'),
    (select id from sets where short_name = 'iko'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Neutralize'),
    (select id from sets where short_name = 'iko'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vulpikeet'),
    (select id from sets where short_name = 'iko'),
    '282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Luminous Broodmoth'),
    (select id from sets where short_name = 'iko'),
    '316',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Huntmaster Liger'),
    (select id from sets where short_name = 'iko'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trumpeting Gnarr'),
    (select id from sets where short_name = 'iko'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glowstone Recluse'),
    (select id from sets where short_name = 'iko'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vulpikeet'),
    (select id from sets where short_name = 'iko'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thieving Otter'),
    (select id from sets where short_name = 'iko'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Solid Footing'),
    (select id from sets where short_name = 'iko'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whisper Squad'),
    (select id from sets where short_name = 'iko'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mythos of Snapdax'),
    (select id from sets where short_name = 'iko'),
    '317',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fully Grown'),
    (select id from sets where short_name = 'iko'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'iko'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodfell Caves'),
    (select id from sets where short_name = 'iko'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gyruda, Doom of Depths'),
    (select id from sets where short_name = 'iko'),
    '351',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boon of the Wish-Giver'),
    (select id from sets where short_name = 'iko'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zenith Flare'),
    (select id from sets where short_name = 'iko'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Umori, the Collector'),
    (select id from sets where short_name = 'iko'),
    '358',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death''s Oasis'),
    (select id from sets where short_name = 'iko'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crystalline Giant'),
    (select id from sets where short_name = 'iko'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wingfold Pteron'),
    (select id from sets where short_name = 'iko'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keruga, the Macrosage'),
    (select id from sets where short_name = 'iko'),
    '354',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coordinated Charge'),
    (select id from sets where short_name = 'iko'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lutri, the Spellchaser'),
    (select id from sets where short_name = 'iko'),
    '356',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sanctuary Lockdown'),
    (select id from sets where short_name = 'iko'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Voracious Greatshark'),
    (select id from sets where short_name = 'iko'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alert Heedbonder'),
    (select id from sets where short_name = 'iko'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'iko'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cubwarden'),
    (select id from sets where short_name = 'iko'),
    '279',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Daysquad Marshal'),
    (select id from sets where short_name = 'iko'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flycatcher Giraffid'),
    (select id from sets where short_name = 'iko'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zirda, the Dawnwaker'),
    (select id from sets where short_name = 'iko'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Void Beckoner'),
    (select id from sets where short_name = 'iko'),
    '373',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jegantha, the Wellspring'),
    (select id from sets where short_name = 'iko'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shark Typhoon'),
    (select id from sets where short_name = 'iko'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Migratory Greathorn'),
    (select id from sets where short_name = 'iko'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chevill, Bane of Monsters'),
    (select id from sets where short_name = 'iko'),
    '330',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gyruda, Doom of Depths'),
    (select id from sets where short_name = 'iko'),
    '384',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Huntmaster Liger'),
    (select id from sets where short_name = 'iko'),
    '280',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scoured Barrens'),
    (select id from sets where short_name = 'iko'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frillscare Mentor'),
    (select id from sets where short_name = 'iko'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mythos of Snapdax'),
    (select id from sets where short_name = 'iko'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yorion, Sky Nomad'),
    (select id from sets where short_name = 'iko'),
    '232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sprite Dragon'),
    (select id from sets where short_name = 'iko'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Void Beckoner'),
    (select id from sets where short_name = 'iko'),
    '373A',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Perimeter Sergeant'),
    (select id from sets where short_name = 'iko'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lukka, Coppercoat Outcast'),
    (select id from sets where short_name = 'iko'),
    '276',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stormwild Capridor'),
    (select id from sets where short_name = 'iko'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Indatha Triome'),
    (select id from sets where short_name = 'iko'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mythos of Vadrok'),
    (select id from sets where short_name = 'iko'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'General Kudro of Drannith'),
    (select id from sets where short_name = 'iko'),
    '187',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lurrus of the Dream-Den'),
    (select id from sets where short_name = 'iko'),
    '355',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreamtail Heron'),
    (select id from sets where short_name = 'iko'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Escape Protocol'),
    (select id from sets where short_name = 'iko'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bastion of Remembrance'),
    (select id from sets where short_name = 'iko'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boneyard Lurker'),
    (select id from sets where short_name = 'iko'),
    '298',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necropanther'),
    (select id from sets where short_name = 'iko'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skull Prophet'),
    (select id from sets where short_name = 'iko'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trumpeting Gnarr'),
    (select id from sets where short_name = 'iko'),
    '307',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Adventurous Impulse'),
    (select id from sets where short_name = 'iko'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sleeper Dart'),
    (select id from sets where short_name = 'iko'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Easy Prey'),
    (select id from sets where short_name = 'iko'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crystacean'),
    (select id from sets where short_name = 'iko'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skycat Sovereign'),
    (select id from sets where short_name = 'iko'),
    '344',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Momentum Rumbler'),
    (select id from sets where short_name = 'iko'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blade Banish'),
    (select id from sets where short_name = 'iko'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Migration Path'),
    (select id from sets where short_name = 'iko'),
    '368',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquil Cove'),
    (select id from sets where short_name = 'iko'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sudden Spinnerets'),
    (select id from sets where short_name = 'iko'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Springjaw Trap'),
    (select id from sets where short_name = 'iko'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Call of the Death-Dweller'),
    (select id from sets where short_name = 'iko'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boneyard Lurker'),
    (select id from sets where short_name = 'iko'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lavabrink Venturer'),
    (select id from sets where short_name = 'iko'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Everquill Phoenix'),
    (select id from sets where short_name = 'iko'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quartzwood Crasher'),
    (select id from sets where short_name = 'iko'),
    '341',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unpredictable Cyclone'),
    (select id from sets where short_name = 'iko'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Excavation Mole'),
    (select id from sets where short_name = 'iko'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emergent Ultimatum'),
    (select id from sets where short_name = 'iko'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eerie Ultimatum'),
    (select id from sets where short_name = 'iko'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blazing Volley'),
    (select id from sets where short_name = 'iko'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Memory Leak'),
    (select id from sets where short_name = 'iko'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Capture Sphere'),
    (select id from sets where short_name = 'iko'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kogla, the Titan Ape'),
    (select id from sets where short_name = 'iko'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Extinction Event'),
    (select id from sets where short_name = 'iko'),
    '321',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Genesis Ultimatum'),
    (select id from sets where short_name = 'iko'),
    '336',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Majestic Auricorn'),
    (select id from sets where short_name = 'iko'),
    '281',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cloudpiercer'),
    (select id from sets where short_name = 'iko'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spontaneous Flight'),
    (select id from sets where short_name = 'iko'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivien, Monsters'' Advocate'),
    (select id from sets where short_name = 'iko'),
    '277',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cloudpiercer'),
    (select id from sets where short_name = 'iko'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gloom Pangolin'),
    (select id from sets where short_name = 'iko'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Back for More'),
    (select id from sets where short_name = 'iko'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Convolute'),
    (select id from sets where short_name = 'iko'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emergent Ultimatum'),
    (select id from sets where short_name = 'iko'),
    '333',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Curdle'),
    (select id from sets where short_name = 'iko'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lurking Deadeye'),
    (select id from sets where short_name = 'iko'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cathartic Reunion'),
    (select id from sets where short_name = 'iko'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obosh, the Preypiercer'),
    (select id from sets where short_name = 'iko'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'iko'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anticipate'),
    (select id from sets where short_name = 'iko'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'iko'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Checkpoint Officer'),
    (select id from sets where short_name = 'iko'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'iko'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charge of the Forever-Beast'),
    (select id from sets where short_name = 'iko'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fire Prophecy'),
    (select id from sets where short_name = 'iko'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brokkos, Apex of Forever'),
    (select id from sets where short_name = 'iko'),
    '378',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mythos of Nethroi'),
    (select id from sets where short_name = 'iko'),
    '323',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Luminous Broodmoth'),
    (select id from sets where short_name = 'iko'),
    '371',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blisterspit Gremlin'),
    (select id from sets where short_name = 'iko'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heightened Reflexes'),
    (select id from sets where short_name = 'iko'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bonders'' Enclave'),
    (select id from sets where short_name = 'iko'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivien, Monsters'' Advocate'),
    (select id from sets where short_name = 'iko'),
    '175',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ruinous Ultimatum'),
    (select id from sets where short_name = 'iko'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snapdax, Apex of the Hunt'),
    (select id from sets where short_name = 'iko'),
    '209',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Survivors'' Bond'),
    (select id from sets where short_name = 'iko'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kinnan, Bonder Prodigy'),
    (select id from sets where short_name = 'iko'),
    '338',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Splendor Mare'),
    (select id from sets where short_name = 'iko'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Farfinder'),
    (select id from sets where short_name = 'iko'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pouncing Shoreshark'),
    (select id from sets where short_name = 'iko'),
    '285',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glimmerbell'),
    (select id from sets where short_name = 'iko'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Genesis Ultimatum'),
    (select id from sets where short_name = 'iko'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wilt'),
    (select id from sets where short_name = 'iko'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lukka, Coppercoat Outcast'),
    (select id from sets where short_name = 'iko'),
    '125',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sonorous Howlbonder'),
    (select id from sets where short_name = 'iko'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dirge Bat'),
    (select id from sets where short_name = 'iko'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ferocious Tigorilla'),
    (select id from sets where short_name = 'iko'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maned Serval'),
    (select id from sets where short_name = 'iko'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lurrus of the Dream-Den'),
    (select id from sets where short_name = 'iko'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Divine Arrow'),
    (select id from sets where short_name = 'iko'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tentative Connection'),
    (select id from sets where short_name = 'iko'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voracious Greatshark'),
    (select id from sets where short_name = 'iko'),
    '320',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fight as One'),
    (select id from sets where short_name = 'iko'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Illuna, Apex of Wishes'),
    (select id from sets where short_name = 'iko'),
    '300',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Flourishing Fox'),
    (select id from sets where short_name = 'iko'),
    '365',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winota, Joiner of Forces'),
    (select id from sets where short_name = 'iko'),
    '216',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shark Typhoon'),
    (select id from sets where short_name = 'iko'),
    '319',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brokkos, Apex of Forever'),
    (select id from sets where short_name = 'iko'),
    '299',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rielle, the Everwise'),
    (select id from sets where short_name = 'iko'),
    '203',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hunted Nightmare'),
    (select id from sets where short_name = 'iko'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snare Tactician'),
    (select id from sets where short_name = 'iko'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Essence Scatter'),
    (select id from sets where short_name = 'iko'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hampering Snare'),
    (select id from sets where short_name = 'iko'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frenzied Raptor'),
    (select id from sets where short_name = 'iko'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Parcelbeast'),
    (select id from sets where short_name = 'iko'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drannith Stinger'),
    (select id from sets where short_name = 'iko'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skycat Sovereign'),
    (select id from sets where short_name = 'iko'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystic Subdual'),
    (select id from sets where short_name = 'iko'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kinnan, Bonder Prodigy'),
    (select id from sets where short_name = 'iko'),
    '192',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cavern Whisperer'),
    (select id from sets where short_name = 'iko'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fiend Artisan'),
    (select id from sets where short_name = 'iko'),
    '350',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vadrok, Apex of Thunder'),
    (select id from sets where short_name = 'iko'),
    '308',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Suffocating Fumes'),
    (select id from sets where short_name = 'iko'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Go for Blood'),
    (select id from sets where short_name = 'iko'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snapdax, Apex of the Hunt'),
    (select id from sets where short_name = 'iko'),
    '381',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Weaponize the Monsters'),
    (select id from sets where short_name = 'iko'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savai Crystal'),
    (select id from sets where short_name = 'iko'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Migratory Greathorn'),
    (select id from sets where short_name = 'iko'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mythos of Nethroi'),
    (select id from sets where short_name = 'iko'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'iko'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rielle, the Everwise'),
    (select id from sets where short_name = 'iko'),
    '342',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fiend Artisan'),
    (select id from sets where short_name = 'iko'),
    '220',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Raking Claws'),
    (select id from sets where short_name = 'iko'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Bargain'),
    (select id from sets where short_name = 'iko'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Colossification'),
    (select id from sets where short_name = 'iko'),
    '364',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Insatiable Hemophage'),
    (select id from sets where short_name = 'iko'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swiftwater Cliffs'),
    (select id from sets where short_name = 'iko'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sprite Dragon'),
    (select id from sets where short_name = 'iko'),
    '369',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Almighty Brushwagg'),
    (select id from sets where short_name = 'iko'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savai Thundermane'),
    (select id from sets where short_name = 'iko'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savai Triome'),
    (select id from sets where short_name = 'iko'),
    '253',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winota, Joiner of Forces'),
    (select id from sets where short_name = 'iko'),
    '349',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Umori, the Collector'),
    (select id from sets where short_name = 'iko'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Titans'' Nest'),
    (select id from sets where short_name = 'iko'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pollywog Symbiote'),
    (select id from sets where short_name = 'iko'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yorion, Sky Nomad'),
    (select id from sets where short_name = 'iko'),
    '359',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Offspring''s Revenge'),
    (select id from sets where short_name = 'iko'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reptilian Reflection'),
    (select id from sets where short_name = 'iko'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flourishing Fox'),
    (select id from sets where short_name = 'iko'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death''s Oasis'),
    (select id from sets where short_name = 'iko'),
    '331',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Colossification'),
    (select id from sets where short_name = 'iko'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blitz Leech'),
    (select id from sets where short_name = 'iko'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ketria Crystal'),
    (select id from sets where short_name = 'iko'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Song of Creation'),
    (select id from sets where short_name = 'iko'),
    '346',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jubilant Skybonder'),
    (select id from sets where short_name = 'iko'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Everquill Phoenix'),
    (select id from sets where short_name = 'iko'),
    '292',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightsquad Commando'),
    (select id from sets where short_name = 'iko'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slitherwisp'),
    (select id from sets where short_name = 'iko'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grimdancer'),
    (select id from sets where short_name = 'iko'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barrier Breach'),
    (select id from sets where short_name = 'iko'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blossoming Sands'),
    (select id from sets where short_name = 'iko'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lore Drakkis'),
    (select id from sets where short_name = 'iko'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unpredictable Cyclone'),
    (select id from sets where short_name = 'iko'),
    '325',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Honey Mammoth'),
    (select id from sets where short_name = 'iko'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Porcuparrot'),
    (select id from sets where short_name = 'iko'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mutual Destruction'),
    (select id from sets where short_name = 'iko'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wind-Scarred Crag'),
    (select id from sets where short_name = 'iko'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Extinction Event'),
    (select id from sets where short_name = 'iko'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phase Dolphin'),
    (select id from sets where short_name = 'iko'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Channeled Force'),
    (select id from sets where short_name = 'iko'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wingspan Mentor'),
    (select id from sets where short_name = 'iko'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Monstrous Step'),
    (select id from sets where short_name = 'iko'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Titans'' Nest'),
    (select id from sets where short_name = 'iko'),
    '347',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'iko'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruinous Ultimatum'),
    (select id from sets where short_name = 'iko'),
    '343',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shredded Sails'),
    (select id from sets where short_name = 'iko'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yidaro, Wandering Monster'),
    (select id from sets where short_name = 'iko'),
    '375',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exuberant Wolfbear'),
    (select id from sets where short_name = 'iko'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nethroi, Apex of Death'),
    (select id from sets where short_name = 'iko'),
    '197',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thornwood Falls'),
    (select id from sets where short_name = 'iko'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Essence Symbiote'),
    (select id from sets where short_name = 'iko'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Majestic Auricorn'),
    (select id from sets where short_name = 'iko'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'iko'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crystalline Giant'),
    (select id from sets where short_name = 'iko'),
    '361',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ivy Elemental'),
    (select id from sets where short_name = 'iko'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Humble Naturalist'),
    (select id from sets where short_name = 'iko'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Helica Glider'),
    (select id from sets where short_name = 'iko'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Colossification'),
    (select id from sets where short_name = 'iko'),
    '327',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zagoth Crystal'),
    (select id from sets where short_name = 'iko'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Narset of the Ancient Way'),
    (select id from sets where short_name = 'iko'),
    '278',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Heartless Act'),
    (select id from sets where short_name = 'iko'),
    '366',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Ozolith'),
    (select id from sets where short_name = 'iko'),
    '237',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sprite Dragon'),
    (select id from sets where short_name = 'iko'),
    '382',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yidaro, Wandering Monster'),
    (select id from sets where short_name = 'iko'),
    '326',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Everquill Phoenix'),
    (select id from sets where short_name = 'iko'),
    '374',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rumbling Rockslide'),
    (select id from sets where short_name = 'iko'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gyruda, Doom of Depths'),
    (select id from sets where short_name = 'iko'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glowstone Recluse'),
    (select id from sets where short_name = 'iko'),
    '296',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drannith Magistrate'),
    (select id from sets where short_name = 'iko'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zilortha, Strength Incarnate'),
    (select id from sets where short_name = 'iko'),
    '275',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rugged Highlands'),
    (select id from sets where short_name = 'iko'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Indatha Triome'),
    (select id from sets where short_name = 'iko'),
    '309',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'iko'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fertilid'),
    (select id from sets where short_name = 'iko'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obosh, the Preypiercer'),
    (select id from sets where short_name = 'iko'),
    '357',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Titanoth Rex'),
    (select id from sets where short_name = 'iko'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dismal Backwater'),
    (select id from sets where short_name = 'iko'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lead the Stampede'),
    (select id from sets where short_name = 'iko'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Illuna, Apex of Wishes'),
    (select id from sets where short_name = 'iko'),
    '190',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blitz of the Thunder-Raptor'),
    (select id from sets where short_name = 'iko'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keensight Mentor'),
    (select id from sets where short_name = 'iko'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Durable Coilbug'),
    (select id from sets where short_name = 'iko'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Startling Development'),
    (select id from sets where short_name = 'iko'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Void Beckoner'),
    (select id from sets where short_name = 'iko'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kogla, the Titan Ape'),
    (select id from sets where short_name = 'iko'),
    '328',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corpse Churn'),
    (select id from sets where short_name = 'iko'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ketria Triome'),
    (select id from sets where short_name = 'iko'),
    '250',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Valiant Rescuer'),
    (select id from sets where short_name = 'iko'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Auspicious Starrix'),
    (select id from sets where short_name = 'iko'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spelleater Wolverine'),
    (select id from sets where short_name = 'iko'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea-Dasher Octopus'),
    (select id from sets where short_name = 'iko'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thwart the Enemy'),
    (select id from sets where short_name = 'iko'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necropanther'),
    (select id from sets where short_name = 'iko'),
    '302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keruga, the Macrosage'),
    (select id from sets where short_name = 'iko'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unexpected Fangs'),
    (select id from sets where short_name = 'iko'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Will of the All-Hunter'),
    (select id from sets where short_name = 'iko'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frondland Felidar'),
    (select id from sets where short_name = 'iko'),
    '186',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ram Through'),
    (select id from sets where short_name = 'iko'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insatiable Hemophage'),
    (select id from sets where short_name = 'iko'),
    '290',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drannith Magistrate'),
    (select id from sets where short_name = 'iko'),
    '314',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cunning Nightbonder'),
    (select id from sets where short_name = 'iko'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forbidden Friendship'),
    (select id from sets where short_name = 'iko'),
    '367',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'iko'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reconnaissance Mission'),
    (select id from sets where short_name = 'iko'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crystalline Giant'),
    (select id from sets where short_name = 'iko'),
    '387',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Facet Reader'),
    (select id from sets where short_name = 'iko'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flame Spill'),
    (select id from sets where short_name = 'iko'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Titanoth Rex'),
    (select id from sets where short_name = 'iko'),
    '377',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yidaro, Wandering Monster'),
    (select id from sets where short_name = 'iko'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whirlwind of Thought'),
    (select id from sets where short_name = 'iko'),
    '348',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imposing Vantasaur'),
    (select id from sets where short_name = 'iko'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Regal Leosaur'),
    (select id from sets where short_name = 'iko'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Song of Creation'),
    (select id from sets where short_name = 'iko'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bonders'' Enclave'),
    (select id from sets where short_name = 'iko'),
    '363',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nethroi, Apex of Death'),
    (select id from sets where short_name = 'iko'),
    '380',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nethroi, Apex of Death'),
    (select id from sets where short_name = 'iko'),
    '303',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Unlikely Aid'),
    (select id from sets where short_name = 'iko'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prickly Marmoset'),
    (select id from sets where short_name = 'iko'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Luminous Broodmoth'),
    (select id from sets where short_name = 'iko'),
    '21',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Serrated Scorpion'),
    (select id from sets where short_name = 'iko'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Light of Hope'),
    (select id from sets where short_name = 'iko'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea-Dasher Octopus'),
    (select id from sets where short_name = 'iko'),
    '286',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frondland Felidar'),
    (select id from sets where short_name = 'iko'),
    '334',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Indatha Crystal'),
    (select id from sets where short_name = 'iko'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'iko'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Patagia Tiger'),
    (select id from sets where short_name = 'iko'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dirge Bat'),
    (select id from sets where short_name = 'iko'),
    '289',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Illuna, Apex of Wishes'),
    (select id from sets where short_name = 'iko'),
    '379',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mosscoat Goriak'),
    (select id from sets where short_name = 'iko'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raugrin Triome'),
    (select id from sets where short_name = 'iko'),
    '311',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swallow Whole'),
    (select id from sets where short_name = 'iko'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bristling Boar'),
    (select id from sets where short_name = 'iko'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inspired Ultimatum'),
    (select id from sets where short_name = 'iko'),
    '337',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raugrin Crystal'),
    (select id from sets where short_name = 'iko'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Huntmaster Liger'),
    (select id from sets where short_name = 'iko'),
    '370',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Labyrinth Raptor'),
    (select id from sets where short_name = 'iko'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gemrazer'),
    (select id from sets where short_name = 'iko'),
    '376',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pouncing Shoreshark'),
    (select id from sets where short_name = 'iko'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quartzwood Crasher'),
    (select id from sets where short_name = 'iko'),
    '201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Of One Mind'),
    (select id from sets where short_name = 'iko'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unbreakable Bond'),
    (select id from sets where short_name = 'iko'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brokkos, Apex of Forever'),
    (select id from sets where short_name = 'iko'),
    '179',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Clash of Titans'),
    (select id from sets where short_name = 'iko'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dreamtail Heron'),
    (select id from sets where short_name = 'iko'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bushmeat Poacher'),
    (select id from sets where short_name = 'iko'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rooting Moloch'),
    (select id from sets where short_name = 'iko'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cubwarden'),
    (select id from sets where short_name = 'iko'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zagoth Triome'),
    (select id from sets where short_name = 'iko'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sanctuary Smasher'),
    (select id from sets where short_name = 'iko'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Regal Leosaur'),
    (select id from sets where short_name = 'iko'),
    '305',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mysterious Egg'),
    (select id from sets where short_name = 'iko'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mythos of Vadrok'),
    (select id from sets where short_name = 'iko'),
    '324',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ominous Seas'),
    (select id from sets where short_name = 'iko'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zagoth Mamba'),
    (select id from sets where short_name = 'iko'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snapdax, Apex of the Hunt'),
    (select id from sets where short_name = 'iko'),
    '306',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Boot Nipper'),
    (select id from sets where short_name = 'iko'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whirlwind of Thought'),
    (select id from sets where short_name = 'iko'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savai Triome'),
    (select id from sets where short_name = 'iko'),
    '312',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slitherwisp'),
    (select id from sets where short_name = 'iko'),
    '345',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vadrok, Apex of Thunder'),
    (select id from sets where short_name = 'iko'),
    '383',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Migration Path'),
    (select id from sets where short_name = 'iko'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savai Sabertooth'),
    (select id from sets where short_name = 'iko'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hornbash Mentor'),
    (select id from sets where short_name = 'iko'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kaheera, the Orphanguard'),
    (select id from sets where short_name = 'iko'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chittering Harvester'),
    (select id from sets where short_name = 'iko'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jegantha, the Wellspring'),
    (select id from sets where short_name = 'iko'),
    '352',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gemrazer'),
    (select id from sets where short_name = 'iko'),
    '295',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mythos of Brokkos'),
    (select id from sets where short_name = 'iko'),
    '329',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plummet'),
    (select id from sets where short_name = 'iko'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Adaptive Shimmerer'),
    (select id from sets where short_name = 'iko'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greater Sandwurm'),
    (select id from sets where short_name = 'iko'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chittering Harvester'),
    (select id from sets where short_name = 'iko'),
    '288',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Offspring''s Revenge'),
    (select id from sets where short_name = 'iko'),
    '340',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pollywog Symbiote'),
    (select id from sets where short_name = 'iko'),
    '372',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inspired Ultimatum'),
    (select id from sets where short_name = 'iko'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frostveil Ambush'),
    (select id from sets where short_name = 'iko'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frost Lynx'),
    (select id from sets where short_name = 'iko'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dire Tactics'),
    (select id from sets where short_name = 'iko'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'iko'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aegis Turtle'),
    (select id from sets where short_name = 'iko'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heartless Act'),
    (select id from sets where short_name = 'iko'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'iko'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forbidden Friendship'),
    (select id from sets where short_name = 'iko'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Porcuparrot'),
    (select id from sets where short_name = 'iko'),
    '293',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Labyrinth Raptor'),
    (select id from sets where short_name = 'iko'),
    '339',
    'rare'
) ,
(
    (select id from mtgcard where name = 'General Kudro of Drannith'),
    (select id from sets where short_name = 'iko'),
    '335',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Garrison Cat'),
    (select id from sets where short_name = 'iko'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'iko'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunted Nightmare'),
    (select id from sets where short_name = 'iko'),
    '322',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lore Drakkis'),
    (select id from sets where short_name = 'iko'),
    '301',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'iko'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'iko'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dead Weight'),
    (select id from sets where short_name = 'iko'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Ozolith'),
    (select id from sets where short_name = 'iko'),
    '362',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vadrok, Apex of Thunder'),
    (select id from sets where short_name = 'iko'),
    '214',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mythos of Illuna'),
    (select id from sets where short_name = 'iko'),
    '318',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mythos of Illuna'),
    (select id from sets where short_name = 'iko'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Footfall Crater'),
    (select id from sets where short_name = 'iko'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Archipelagore'),
    (select id from sets where short_name = 'iko'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lavabrink Venturer'),
    (select id from sets where short_name = 'iko'),
    '315',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avian Oddity'),
    (select id from sets where short_name = 'iko'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mysterious Egg'),
    (select id from sets where short_name = 'iko'),
    '385',
    'common'
) ,
(
    (select id from mtgcard where name = 'Proud Wildbonder'),
    (select id from sets where short_name = 'iko'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Parcelbeast'),
    (select id from sets where short_name = 'iko'),
    '304',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ketria Triome'),
    (select id from sets where short_name = 'iko'),
    '310',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyroceratops'),
    (select id from sets where short_name = 'iko'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'General''s Enforcer'),
    (select id from sets where short_name = 'iko'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Auspicious Starrix'),
    (select id from sets where short_name = 'iko'),
    '294',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'iko'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zagoth Triome'),
    (select id from sets where short_name = 'iko'),
    '313',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mythos of Brokkos'),
    (select id from sets where short_name = 'iko'),
    '168',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Narset of the Ancient Way'),
    (select id from sets where short_name = 'iko'),
    '195',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gust of Wind'),
    (select id from sets where short_name = 'iko'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lutri, the Spellchaser'),
    (select id from sets where short_name = 'iko'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duskfang Mentor'),
    (select id from sets where short_name = 'iko'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dirge Bat'),
    (select id from sets where short_name = 'iko'),
    '386',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eerie Ultimatum'),
    (select id from sets where short_name = 'iko'),
    '332',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaheera, the Orphanguard'),
    (select id from sets where short_name = 'iko'),
    '353',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keep Safe'),
    (select id from sets where short_name = 'iko'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cavern Whisperer'),
    (select id from sets where short_name = 'iko'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chevill, Bane of Monsters'),
    (select id from sets where short_name = 'iko'),
    '181',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Drannith Healer'),
    (select id from sets where short_name = 'iko'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zirda, the Dawnwaker'),
    (select id from sets where short_name = 'iko'),
    '360',
    'rare'
) 
 on conflict do nothing;
