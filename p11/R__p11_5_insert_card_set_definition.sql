insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Doom Blade'),
    (select id from sets where short_name = 'p11'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treasure Hunt'),
    (select id from sets where short_name = 'p11'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brave the Elements'),
    (select id from sets where short_name = 'p11'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Searing Blaze'),
    (select id from sets where short_name = 'p11'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Day of Judgment'),
    (select id from sets where short_name = 'p11'),
    '1',
    'rare'
) 
 on conflict do nothing;
