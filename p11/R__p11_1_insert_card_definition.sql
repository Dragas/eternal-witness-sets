    insert into mtgcard(name) values ('Doom Blade') on conflict do nothing;
    insert into mtgcard(name) values ('Treasure Hunt') on conflict do nothing;
    insert into mtgcard(name) values ('Brave the Elements') on conflict do nothing;
    insert into mtgcard(name) values ('Searing Blaze') on conflict do nothing;
    insert into mtgcard(name) values ('Day of Judgment') on conflict do nothing;
