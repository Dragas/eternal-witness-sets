insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Rumbling Aftershocks'),
    (select id from sets where short_name = 'wwk'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Horizon Drake'),
    (select id from sets where short_name = 'wwk'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Akoum Battlesinger'),
    (select id from sets where short_name = 'wwk'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Permafrost Trap'),
    (select id from sets where short_name = 'wwk'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kazuul, Tyrant of the Cliffs'),
    (select id from sets where short_name = 'wwk'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smother'),
    (select id from sets where short_name = 'wwk'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marsh Threader'),
    (select id from sets where short_name = 'wwk'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brink of Disaster'),
    (select id from sets where short_name = 'wwk'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corrupted Zendikon'),
    (select id from sets where short_name = 'wwk'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace, the Mind Sculptor'),
    (select id from sets where short_name = 'wwk'),
    '31',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Creeping Tar Pit'),
    (select id from sets where short_name = 'wwk'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skitter of Lizards'),
    (select id from sets where short_name = 'wwk'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death''s Shadow'),
    (select id from sets where short_name = 'wwk'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fledgling Griffin'),
    (select id from sets where short_name = 'wwk'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shoreline Salvager'),
    (select id from sets where short_name = 'wwk'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mysteries of the Deep'),
    (select id from sets where short_name = 'wwk'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stoneforge Mystic'),
    (select id from sets where short_name = 'wwk'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Khalni Garden'),
    (select id from sets where short_name = 'wwk'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruthless Cullblade'),
    (select id from sets where short_name = 'wwk'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archon of Redemption'),
    (select id from sets where short_name = 'wwk'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Everflowing Chalice'),
    (select id from sets where short_name = 'wwk'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nemesis Trap'),
    (select id from sets where short_name = 'wwk'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feral Contest'),
    (select id from sets where short_name = 'wwk'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kitesail'),
    (select id from sets where short_name = 'wwk'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hada Freeblade'),
    (select id from sets where short_name = 'wwk'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Twitch'),
    (select id from sets where short_name = 'wwk'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battle Hurda'),
    (select id from sets where short_name = 'wwk'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Searing Blaze'),
    (select id from sets where short_name = 'wwk'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hammer of Ruin'),
    (select id from sets where short_name = 'wwk'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seer''s Sundial'),
    (select id from sets where short_name = 'wwk'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bojuka Brigand'),
    (select id from sets where short_name = 'wwk'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolfbriar Elemental'),
    (select id from sets where short_name = 'wwk'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Summit Apes'),
    (select id from sets where short_name = 'wwk'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thada Adel, Acquisitor'),
    (select id from sets where short_name = 'wwk'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Razor Boomerang'),
    (select id from sets where short_name = 'wwk'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scrib Nibblers'),
    (select id from sets where short_name = 'wwk'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quest for Renewal'),
    (select id from sets where short_name = 'wwk'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bestial Menace'),
    (select id from sets where short_name = 'wwk'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stone Idol Trap'),
    (select id from sets where short_name = 'wwk'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pulse Tracker'),
    (select id from sets where short_name = 'wwk'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spell Contortion'),
    (select id from sets where short_name = 'wwk'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Groundswell'),
    (select id from sets where short_name = 'wwk'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quicksand'),
    (select id from sets where short_name = 'wwk'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Perimeter Captain'),
    (select id from sets where short_name = 'wwk'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loam Lion'),
    (select id from sets where short_name = 'wwk'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Talus Paladin'),
    (select id from sets where short_name = 'wwk'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Canopy Cover'),
    (select id from sets where short_name = 'wwk'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bojuka Bog'),
    (select id from sets where short_name = 'wwk'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Walking Atlas'),
    (select id from sets where short_name = 'wwk'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selective Memory'),
    (select id from sets where short_name = 'wwk'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Basilisk Collar'),
    (select id from sets where short_name = 'wwk'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leatherback Baloth'),
    (select id from sets where short_name = 'wwk'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jwari Shapeshifter'),
    (select id from sets where short_name = 'wwk'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Comet Storm'),
    (select id from sets where short_name = 'wwk'),
    '76',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Enclave Elite'),
    (select id from sets where short_name = 'wwk'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodhusk Ritualist'),
    (select id from sets where short_name = 'wwk'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ricochet Trap'),
    (select id from sets where short_name = 'wwk'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Admonition Angel'),
    (select id from sets where short_name = 'wwk'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chain Reaction'),
    (select id from sets where short_name = 'wwk'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Novablast Wurm'),
    (select id from sets where short_name = 'wwk'),
    '119',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nature''s Claim'),
    (select id from sets where short_name = 'wwk'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Joraga Warcaller'),
    (select id from sets where short_name = 'wwk'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sejiri Steppe'),
    (select id from sets where short_name = 'wwk'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dread Statuary'),
    (select id from sets where short_name = 'wwk'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gnarlid Pack'),
    (select id from sets where short_name = 'wwk'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Omnath, Locus of Mana'),
    (select id from sets where short_name = 'wwk'),
    '109',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cunning Sparkmage'),
    (select id from sets where short_name = 'wwk'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sejiri Merfolk'),
    (select id from sets where short_name = 'wwk'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Iona''s Judgment'),
    (select id from sets where short_name = 'wwk'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arbor Elf'),
    (select id from sets where short_name = 'wwk'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voyager Drake'),
    (select id from sets where short_name = 'wwk'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slavering Nulls'),
    (select id from sets where short_name = 'wwk'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Butcher of Malakir'),
    (select id from sets where short_name = 'wwk'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cosi''s Ravager'),
    (select id from sets where short_name = 'wwk'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rest for the Weary'),
    (select id from sets where short_name = 'wwk'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quest for Ula''s Temple'),
    (select id from sets where short_name = 'wwk'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Veteran''s Reflexes'),
    (select id from sets where short_name = 'wwk'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harabaz Druid'),
    (select id from sets where short_name = 'wwk'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightkeeper of Emeria'),
    (select id from sets where short_name = 'wwk'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anowon, the Ruin Sage'),
    (select id from sets where short_name = 'wwk'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Caustic Crawler'),
    (select id from sets where short_name = 'wwk'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goliath Sphinx'),
    (select id from sets where short_name = 'wwk'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roiling Terrain'),
    (select id from sets where short_name = 'wwk'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snapping Creeper'),
    (select id from sets where short_name = 'wwk'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kitesail Apprentice'),
    (select id from sets where short_name = 'wwk'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Surrakar Banisher'),
    (select id from sets where short_name = 'wwk'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bazaar Trader'),
    (select id from sets where short_name = 'wwk'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grotag Thrasher'),
    (select id from sets where short_name = 'wwk'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Amulet of Vigor'),
    (select id from sets where short_name = 'wwk'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aether Tradewinds'),
    (select id from sets where short_name = 'wwk'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terra Eternal'),
    (select id from sets where short_name = 'wwk'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lodestone Golem'),
    (select id from sets where short_name = 'wwk'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vastwood Zendikon'),
    (select id from sets where short_name = 'wwk'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tomb Hex'),
    (select id from sets where short_name = 'wwk'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mire''s Toll'),
    (select id from sets where short_name = 'wwk'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bull Rush'),
    (select id from sets where short_name = 'wwk'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dead Reckoning'),
    (select id from sets where short_name = 'wwk'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smoldering Spires'),
    (select id from sets where short_name = 'wwk'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Calcite Snapper'),
    (select id from sets where short_name = 'wwk'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abyssal Persecutor'),
    (select id from sets where short_name = 'wwk'),
    '47',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Urge to Feed'),
    (select id from sets where short_name = 'wwk'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Strength of the Tajuru'),
    (select id from sets where short_name = 'wwk'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Apex Hawks'),
    (select id from sets where short_name = 'wwk'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wind Zendikon'),
    (select id from sets where short_name = 'wwk'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Firewalker'),
    (select id from sets where short_name = 'wwk'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Refraction Trap'),
    (select id from sets where short_name = 'wwk'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jagwasp Swarm'),
    (select id from sets where short_name = 'wwk'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vapor Snare'),
    (select id from sets where short_name = 'wwk'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hedron Rover'),
    (select id from sets where short_name = 'wwk'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragonmaster Outcast'),
    (select id from sets where short_name = 'wwk'),
    '81',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Eye of Ugin'),
    (select id from sets where short_name = 'wwk'),
    '136',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Halimar Depths'),
    (select id from sets where short_name = 'wwk'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Agadeem Occultist'),
    (select id from sets where short_name = 'wwk'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quest for the Nihil Stone'),
    (select id from sets where short_name = 'wwk'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lavaclaw Reaches'),
    (select id from sets where short_name = 'wwk'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Claws of Valakut'),
    (select id from sets where short_name = 'wwk'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mordant Dragon'),
    (select id from sets where short_name = 'wwk'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raging Ravine'),
    (select id from sets where short_name = 'wwk'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathforge Shaman'),
    (select id from sets where short_name = 'wwk'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pilgrim''s Eye'),
    (select id from sets where short_name = 'wwk'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure Hunt'),
    (select id from sets where short_name = 'wwk'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Halimar Excavator'),
    (select id from sets where short_name = 'wwk'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tuktuk Scrapper'),
    (select id from sets where short_name = 'wwk'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stirring Wildwood'),
    (select id from sets where short_name = 'wwk'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vastwood Animist'),
    (select id from sets where short_name = 'wwk'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quag Vampires'),
    (select id from sets where short_name = 'wwk'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Explore'),
    (select id from sets where short_name = 'wwk'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guardian Zendikon'),
    (select id from sets where short_name = 'wwk'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avenger of Zendikar'),
    (select id from sets where short_name = 'wwk'),
    '96',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wrexial, the Risen Deep'),
    (select id from sets where short_name = 'wwk'),
    '120',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Slingbow Trap'),
    (select id from sets where short_name = 'wwk'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grappler Spider'),
    (select id from sets where short_name = 'wwk'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quest for the Goblin Lord'),
    (select id from sets where short_name = 'wwk'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Join the Ranks'),
    (select id from sets where short_name = 'wwk'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terastodon'),
    (select id from sets where short_name = 'wwk'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Roughrider'),
    (select id from sets where short_name = 'wwk'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Graypelt Hunter'),
    (select id from sets where short_name = 'wwk'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruin Ghost'),
    (select id from sets where short_name = 'wwk'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dispel'),
    (select id from sets where short_name = 'wwk'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kalastria Highborn'),
    (select id from sets where short_name = 'wwk'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tideforce Elemental'),
    (select id from sets where short_name = 'wwk'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marshal''s Anthem'),
    (select id from sets where short_name = 'wwk'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestial Colonnade'),
    (select id from sets where short_name = 'wwk'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crusher Zendikon'),
    (select id from sets where short_name = 'wwk'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tectonic Edge'),
    (select id from sets where short_name = 'wwk'),
    '145',
    'uncommon'
) 
 on conflict do nothing;
