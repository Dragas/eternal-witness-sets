    insert into mtgcard(name) values ('Gideon of the Trials') on conflict do nothing;
    insert into mtgcard(name) values ('Chandra, Torch of Defiance') on conflict do nothing;
    insert into mtgcard(name) values ('Liliana, Untouched by Death') on conflict do nothing;
    insert into mtgcard(name) values ('Nissa, Vital Force') on conflict do nothing;
    insert into mtgcard(name) values ('Jace, Cunning Castaway') on conflict do nothing;
