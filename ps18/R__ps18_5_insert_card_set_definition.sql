insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Gideon of the Trials'),
    (select id from sets where short_name = 'ps18'),
    '14',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chandra, Torch of Defiance'),
    (select id from sets where short_name = 'ps18'),
    '110',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Liliana, Untouched by Death'),
    (select id from sets where short_name = 'ps18'),
    '106',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nissa, Vital Force'),
    (select id from sets where short_name = 'ps18'),
    '163',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jace, Cunning Castaway'),
    (select id from sets where short_name = 'ps18'),
    '60',
    'mythic'
) 
 on conflict do nothing;
