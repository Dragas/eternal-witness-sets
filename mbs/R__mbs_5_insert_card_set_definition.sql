insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Flensermite'),
    (select id from sets where short_name = 'mbs'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyresis'),
    (select id from sets where short_name = 'mbs'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Green Sun''s Zenith'),
    (select id from sets where short_name = 'mbs'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Black Sun''s Zenith'),
    (select id from sets where short_name = 'mbs'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Master''s Call'),
    (select id from sets where short_name = 'mbs'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Core Prowler'),
    (select id from sets where short_name = 'mbs'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Creeping Corrosion'),
    (select id from sets where short_name = 'mbs'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vedalken Anatomist'),
    (select id from sets where short_name = 'mbs'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viridian Corrupter'),
    (select id from sets where short_name = 'mbs'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'mbs'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tezzeret, Agent of Bolas'),
    (select id from sets where short_name = 'mbs'),
    '97',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Viridian Emissary'),
    (select id from sets where short_name = 'mbs'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quilled Slagwurm'),
    (select id from sets where short_name = 'mbs'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Distant Memories'),
    (select id from sets where short_name = 'mbs'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cryptoplasm'),
    (select id from sets where short_name = 'mbs'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kuldotha Flamefiend'),
    (select id from sets where short_name = 'mbs'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pistus Strike'),
    (select id from sets where short_name = 'mbs'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hero of Oxid Ridge'),
    (select id from sets where short_name = 'mbs'),
    '66',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Choking Fumes'),
    (select id from sets where short_name = 'mbs'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Go for the Throat'),
    (select id from sets where short_name = 'mbs'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vedalken Infuser'),
    (select id from sets where short_name = 'mbs'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Razorfield Rhino'),
    (select id from sets where short_name = 'mbs'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viridian Claw'),
    (select id from sets where short_name = 'mbs'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blightwidow'),
    (select id from sets where short_name = 'mbs'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unnatural Predation'),
    (select id from sets where short_name = 'mbs'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gnathosaur'),
    (select id from sets where short_name = 'mbs'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horrifying Revelation'),
    (select id from sets where short_name = 'mbs'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Caustic Hound'),
    (select id from sets where short_name = 'mbs'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mitotic Manipulation'),
    (select id from sets where short_name = 'mbs'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Banishment Decree'),
    (select id from sets where short_name = 'mbs'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ichor Wellspring'),
    (select id from sets where short_name = 'mbs'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Wardriver'),
    (select id from sets where short_name = 'mbs'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kemba''s Legion'),
    (select id from sets where short_name = 'mbs'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'mbs'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Crusader'),
    (select id from sets where short_name = 'mbs'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Rebirth'),
    (select id from sets where short_name = 'mbs'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Red Sun''s Zenith'),
    (select id from sets where short_name = 'mbs'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'mbs'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brass Squire'),
    (select id from sets where short_name = 'mbs'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knowledge Pool'),
    (select id from sets where short_name = 'mbs'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'mbs'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kuldotha Ringleader'),
    (select id from sets where short_name = 'mbs'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruesome Encore'),
    (select id from sets where short_name = 'mbs'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fuel for the Cause'),
    (select id from sets where short_name = 'mbs'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Concussive Bolt'),
    (select id from sets where short_name = 'mbs'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'mbs'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lumengrid Gargoyle'),
    (select id from sets where short_name = 'mbs'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glissa''s Courier'),
    (select id from sets where short_name = 'mbs'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scourge Servant'),
    (select id from sets where short_name = 'mbs'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shimmer Myr'),
    (select id from sets where short_name = 'mbs'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'mbs'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hexplate Golem'),
    (select id from sets where short_name = 'mbs'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychosis Crawler'),
    (select id from sets where short_name = 'mbs'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myr Sire'),
    (select id from sets where short_name = 'mbs'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirran Spy'),
    (select id from sets where short_name = 'mbs'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plaguemaw Beast'),
    (select id from sets where short_name = 'mbs'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Peace Strider'),
    (select id from sets where short_name = 'mbs'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dross Ripper'),
    (select id from sets where short_name = 'mbs'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sword of Feast and Famine'),
    (select id from sets where short_name = 'mbs'),
    '138',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Spine of Ish Sah'),
    (select id from sets where short_name = 'mbs'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gust-Skimmer'),
    (select id from sets where short_name = 'mbs'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrun, the Last Troll'),
    (select id from sets where short_name = 'mbs'),
    '92',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Magnetic Mine'),
    (select id from sets where short_name = 'mbs'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ogre Resister'),
    (select id from sets where short_name = 'mbs'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Copper Carapace'),
    (select id from sets where short_name = 'mbs'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter Assembly'),
    (select id from sets where short_name = 'mbs'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lead the Stampede'),
    (select id from sets where short_name = 'mbs'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oculus'),
    (select id from sets where short_name = 'mbs'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivisection'),
    (select id from sets where short_name = 'mbs'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bladed Sentinel'),
    (select id from sets where short_name = 'mbs'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ardent Recruit'),
    (select id from sets where short_name = 'mbs'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flesh-Eater Imp'),
    (select id from sets where short_name = 'mbs'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treasure Mage'),
    (select id from sets where short_name = 'mbs'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Myr Turbine'),
    (select id from sets where short_name = 'mbs'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corrupted Conscience'),
    (select id from sets where short_name = 'mbs'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glissa, the Traitor'),
    (select id from sets where short_name = 'mbs'),
    '96',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blightsteel Colossus'),
    (select id from sets where short_name = 'mbs'),
    '99',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sphere of the Suns'),
    (select id from sets where short_name = 'mbs'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Titan Forge'),
    (select id from sets where short_name = 'mbs'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rot Wolf'),
    (select id from sets where short_name = 'mbs'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Virulent Wound'),
    (select id from sets where short_name = 'mbs'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Revoker'),
    (select id from sets where short_name = 'mbs'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spiraling Duelist'),
    (select id from sets where short_name = 'mbs'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hellkite Igniter'),
    (select id from sets where short_name = 'mbs'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tine Shrike'),
    (select id from sets where short_name = 'mbs'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pierce Strider'),
    (select id from sets where short_name = 'mbs'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hero of Bladehold'),
    (select id from sets where short_name = 'mbs'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Training Drone'),
    (select id from sets where short_name = 'mbs'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Septic Rats'),
    (select id from sets where short_name = 'mbs'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tangle Hulk'),
    (select id from sets where short_name = 'mbs'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rally the Forces'),
    (select id from sets where short_name = 'mbs'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plague Myr'),
    (select id from sets where short_name = 'mbs'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Koth''s Courier'),
    (select id from sets where short_name = 'mbs'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spire Serpent'),
    (select id from sets where short_name = 'mbs'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sangromancer'),
    (select id from sets where short_name = 'mbs'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Into the Core'),
    (select id from sets where short_name = 'mbs'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slagstorm'),
    (select id from sets where short_name = 'mbs'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crush'),
    (select id from sets where short_name = 'mbs'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loxodon Partisan'),
    (select id from sets where short_name = 'mbs'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blue Sun''s Zenith'),
    (select id from sets where short_name = 'mbs'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'White Sun''s Zenith'),
    (select id from sets where short_name = 'mbs'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Priests of Norn'),
    (select id from sets where short_name = 'mbs'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Juggernaut'),
    (select id from sets where short_name = 'mbs'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Neurok Commando'),
    (select id from sets where short_name = 'mbs'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mirran Crusader'),
    (select id from sets where short_name = 'mbs'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Victory''s Herald'),
    (select id from sets where short_name = 'mbs'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frantic Salvage'),
    (select id from sets where short_name = 'mbs'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burn the Impure'),
    (select id from sets where short_name = 'mbs'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Praetor''s Counsel'),
    (select id from sets where short_name = 'mbs'),
    '88',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Consecrated Sphinx'),
    (select id from sets where short_name = 'mbs'),
    '21',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Metallic Mastery'),
    (select id from sets where short_name = 'mbs'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darksteel Plate'),
    (select id from sets where short_name = 'mbs'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steel Sabotage'),
    (select id from sets where short_name = 'mbs'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shriekhorn'),
    (select id from sets where short_name = 'mbs'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Morbid Plunder'),
    (select id from sets where short_name = 'mbs'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Turn the Tide'),
    (select id from sets where short_name = 'mbs'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Signal Pest'),
    (select id from sets where short_name = 'mbs'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Contested War Zone'),
    (select id from sets where short_name = 'mbs'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nested Ghoul'),
    (select id from sets where short_name = 'mbs'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tangle Mantis'),
    (select id from sets where short_name = 'mbs'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gore Vassal'),
    (select id from sets where short_name = 'mbs'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'mbs'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'mbs'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'mbs'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirrorworks'),
    (select id from sets where short_name = 'mbs'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Hydra'),
    (select id from sets where short_name = 'mbs'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flayer Husk'),
    (select id from sets where short_name = 'mbs'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Massacre Wurm'),
    (select id from sets where short_name = 'mbs'),
    '46',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Piston Sledge'),
    (select id from sets where short_name = 'mbs'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Strandwalker'),
    (select id from sets where short_name = 'mbs'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skinwing'),
    (select id from sets where short_name = 'mbs'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silverskin Armor'),
    (select id from sets where short_name = 'mbs'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Melira''s Keepers'),
    (select id from sets where short_name = 'mbs'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leonin Relic-Warder'),
    (select id from sets where short_name = 'mbs'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spread the Sickness'),
    (select id from sets where short_name = 'mbs'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Accorder Paladin'),
    (select id from sets where short_name = 'mbs'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bonehoard'),
    (select id from sets where short_name = 'mbs'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'mbs'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inkmoth Nexus'),
    (select id from sets where short_name = 'mbs'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirran Mettle'),
    (select id from sets where short_name = 'mbs'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Welder'),
    (select id from sets where short_name = 'mbs'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Vatmother'),
    (select id from sets where short_name = 'mbs'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Rager'),
    (select id from sets where short_name = 'mbs'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serum Raker'),
    (select id from sets where short_name = 'mbs'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rusted Slasher'),
    (select id from sets where short_name = 'mbs'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fangren Marauder'),
    (select id from sets where short_name = 'mbs'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Skyhunter'),
    (select id from sets where short_name = 'mbs'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blisterstick Shaman'),
    (select id from sets where short_name = 'mbs'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Digester'),
    (select id from sets where short_name = 'mbs'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Decimator Web'),
    (select id from sets where short_name = 'mbs'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quicksilver Geyser'),
    (select id from sets where short_name = 'mbs'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mortarpod'),
    (select id from sets where short_name = 'mbs'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Galvanoth'),
    (select id from sets where short_name = 'mbs'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spin Engine'),
    (select id from sets where short_name = 'mbs'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divine Offering'),
    (select id from sets where short_name = 'mbs'),
    '5',
    'common'
) 
 on conflict do nothing;
