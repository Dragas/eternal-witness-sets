insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Luxury Suite'),
    (select id from sets where short_name = 'zne'),
    '18',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wooded Foothills'),
    (select id from sets where short_name = 'zne'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Prismatic Vista'),
    (select id from sets where short_name = 'zne'),
    '27',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Arid Mesa'),
    (select id from sets where short_name = 'zne'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Celestial Colonnade'),
    (select id from sets where short_name = 'zne'),
    '23',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Creeping Tar Pit'),
    (select id from sets where short_name = 'zne'),
    '24',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scalding Tarn'),
    (select id from sets where short_name = 'zne'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cavern of Souls'),
    (select id from sets where short_name = 'zne'),
    '22',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Spire Garden'),
    (select id from sets where short_name = 'zne'),
    '19',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Windswept Heath'),
    (select id from sets where short_name = 'zne'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Copperline Gorge'),
    (select id from sets where short_name = 'zne'),
    '14',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Horizon Canopy'),
    (select id from sets where short_name = 'zne'),
    '26',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Misty Rainforest'),
    (select id from sets where short_name = 'zne'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bountiful Promenade'),
    (select id from sets where short_name = 'zne'),
    '20',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Grove of the Burnwillows'),
    (select id from sets where short_name = 'zne'),
    '25',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ancient Tomb'),
    (select id from sets where short_name = 'zne'),
    '21',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Marsh Flats'),
    (select id from sets where short_name = 'zne'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wasteland'),
    (select id from sets where short_name = 'zne'),
    '30',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Flooded Strand'),
    (select id from sets where short_name = 'zne'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sea of Clouds'),
    (select id from sets where short_name = 'zne'),
    '16',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Morphic Pool'),
    (select id from sets where short_name = 'zne'),
    '17',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Valakut, the Molten Pinnacle'),
    (select id from sets where short_name = 'zne'),
    '29',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Polluted Delta'),
    (select id from sets where short_name = 'zne'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Razorverge Thicket'),
    (select id from sets where short_name = 'zne'),
    '15',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Darkslick Shores'),
    (select id from sets where short_name = 'zne'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Strip Mine'),
    (select id from sets where short_name = 'zne'),
    '28',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bloodstained Mire'),
    (select id from sets where short_name = 'zne'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Verdant Catacombs'),
    (select id from sets where short_name = 'zne'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blackcleave Cliffs'),
    (select id from sets where short_name = 'zne'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Seachrome Coast'),
    (select id from sets where short_name = 'zne'),
    '11',
    'mythic'
) 
 on conflict do nothing;
