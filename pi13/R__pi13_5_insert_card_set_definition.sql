insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Gaze of Granite'),
    (select id from sets where short_name = 'pi13'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ogre Arsonist'),
    (select id from sets where short_name = 'pi13'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corrupt'),
    (select id from sets where short_name = 'pi13'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voidmage Husher'),
    (select id from sets where short_name = 'pi13'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'High Tide'),
    (select id from sets where short_name = 'pi13'),
    '13',
    'rare'
) 
 on conflict do nothing;
