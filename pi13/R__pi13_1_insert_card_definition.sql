    insert into mtgcard(name) values ('Gaze of Granite') on conflict do nothing;
    insert into mtgcard(name) values ('Ogre Arsonist') on conflict do nothing;
    insert into mtgcard(name) values ('Corrupt') on conflict do nothing;
    insert into mtgcard(name) values ('Voidmage Husher') on conflict do nothing;
    insert into mtgcard(name) values ('High Tide') on conflict do nothing;
