insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Rhystic Study'),
    (select id from sets where short_name = 'cm1'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Decree of Pain'),
    (select id from sets where short_name = 'cm1'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diaochan, Artful Beauty'),
    (select id from sets where short_name = 'cm1'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirari''s Wake'),
    (select id from sets where short_name = 'cm1'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaalia of the Vast'),
    (select id from sets where short_name = 'cm1'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sylvan Library'),
    (select id from sets where short_name = 'cm1'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desertion'),
    (select id from sets where short_name = 'cm1'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scroll Rack'),
    (select id from sets where short_name = 'cm1'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loyal Retainers'),
    (select id from sets where short_name = 'cm1'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duplicant'),
    (select id from sets where short_name = 'cm1'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Wanderer'),
    (select id from sets where short_name = 'cm1'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dragonlair Spider'),
    (select id from sets where short_name = 'cm1'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Mimeoplasm'),
    (select id from sets where short_name = 'cm1'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vela the Night-Clad'),
    (select id from sets where short_name = 'cm1'),
    '18',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Command Tower'),
    (select id from sets where short_name = 'cm1'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Edric, Spymaster of Trest'),
    (select id from sets where short_name = 'cm1'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chaos Warp'),
    (select id from sets where short_name = 'cm1'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind''s Eye'),
    (select id from sets where short_name = 'cm1'),
    '13',
    'rare'
) 
 on conflict do nothing;
