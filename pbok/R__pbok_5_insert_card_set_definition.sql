insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Jace Beleren'),
    (select id from sets where short_name = 'pbok'),
    '1',
    'mythic'
) 
 on conflict do nothing;
