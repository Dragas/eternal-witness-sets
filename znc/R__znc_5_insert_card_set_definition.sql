insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Lazav, Dimir Mastermind'),
    (select id from sets where short_name = 'znc'),
    '92',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Faerie Vandal'),
    (select id from sets where short_name = 'znc'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slither Blade'),
    (select id from sets where short_name = 'znc'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armorcraft Judge'),
    (select id from sets where short_name = 'znc'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Omnath, Locus of Rage'),
    (select id from sets where short_name = 'znc'),
    '97',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dimir Locket'),
    (select id from sets where short_name = 'znc'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murder'),
    (select id from sets where short_name = 'znc'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Changeling Outcast'),
    (select id from sets where short_name = 'znc'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Together Forever'),
    (select id from sets where short_name = 'znc'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Invisible Stalker'),
    (select id from sets where short_name = 'znc'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sygg, River Cutthroat'),
    (select id from sets where short_name = 'znc'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beanstalk Giant // Fertile Footsteps'),
    (select id from sets where short_name = 'znc'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tuskguard Captain'),
    (select id from sets where short_name = 'znc'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cryptic Caves'),
    (select id from sets where short_name = 'znc'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Keyrune'),
    (select id from sets where short_name = 'znc'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Price of Fame'),
    (select id from sets where short_name = 'znc'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nissa''s Renewal'),
    (select id from sets where short_name = 'znc'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Military Intelligence'),
    (select id from sets where short_name = 'znc'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Geode Rager'),
    (select id from sets where short_name = 'znc'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruul Turf'),
    (select id from sets where short_name = 'znc'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Guildgate'),
    (select id from sets where short_name = 'znc'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Acidic Slime'),
    (select id from sets where short_name = 'znc'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whispersteel Dagger'),
    (select id from sets where short_name = 'znc'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sepulchral Primordial'),
    (select id from sets where short_name = 'znc'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Embodiment of Insight'),
    (select id from sets where short_name = 'znc'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trove Warden'),
    (select id from sets where short_name = 'znc'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enigma Thief'),
    (select id from sets where short_name = 'znc'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harmonize'),
    (select id from sets where short_name = 'znc'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mina and Denn, Wildborn'),
    (select id from sets where short_name = 'znc'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Obuun, Mul Daya Ancestor'),
    (select id from sets where short_name = 'znc'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Keeper of Fables'),
    (select id from sets where short_name = 'znc'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Waker of the Wilds'),
    (select id from sets where short_name = 'znc'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abzan Falconer'),
    (select id from sets where short_name = 'znc'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fertilid'),
    (select id from sets where short_name = 'znc'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oona''s Blackguard'),
    (select id from sets where short_name = 'znc'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'znc'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marang River Prowler'),
    (select id from sets where short_name = 'znc'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Emeria Angel'),
    (select id from sets where short_name = 'znc'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heirloom Blade'),
    (select id from sets where short_name = 'znc'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Naya Panorama'),
    (select id from sets where short_name = 'znc'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blighted Woodland'),
    (select id from sets where short_name = 'znc'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Command Tower'),
    (select id from sets where short_name = 'znc'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ground Assault'),
    (select id from sets where short_name = 'znc'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jungle Shrine'),
    (select id from sets where short_name = 'znc'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gonti, Lord of Luxury'),
    (select id from sets where short_name = 'znc'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Planar Outburst'),
    (select id from sets where short_name = 'znc'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fated Return'),
    (select id from sets where short_name = 'znc'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Needle Spires'),
    (select id from sets where short_name = 'znc'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treacherous Terrain'),
    (select id from sets where short_name = 'znc'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zendikar''s Roil'),
    (select id from sets where short_name = 'znc'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circuitous Route'),
    (select id from sets where short_name = 'znc'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inspiring Call'),
    (select id from sets where short_name = 'znc'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Guildgate'),
    (select id from sets where short_name = 'znc'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Marsh Flitter'),
    (select id from sets where short_name = 'znc'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'znc'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Distant Melody'),
    (select id from sets where short_name = 'znc'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Syr Konrad, the Grim'),
    (select id from sets where short_name = 'znc'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightveil Sprite'),
    (select id from sets where short_name = 'znc'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silumgar''s Command'),
    (select id from sets where short_name = 'znc'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jwar Isle Refuge'),
    (select id from sets where short_name = 'znc'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bonehoard'),
    (select id from sets where short_name = 'znc'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Master Thief'),
    (select id from sets where short_name = 'znc'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Mending of Dominaria'),
    (select id from sets where short_name = 'znc'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Consuming Aberration'),
    (select id from sets where short_name = 'znc'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abundance'),
    (select id from sets where short_name = 'znc'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Springbloom Druid'),
    (select id from sets where short_name = 'znc'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Reclamation'),
    (select id from sets where short_name = 'znc'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zulaport Cutthroat'),
    (select id from sets where short_name = 'znc'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rampaging Baloths'),
    (select id from sets where short_name = 'znc'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Advocate'),
    (select id from sets where short_name = 'znc'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rogue''s Passage'),
    (select id from sets where short_name = 'znc'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seer''s Sundial'),
    (select id from sets where short_name = 'znc'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elite Scaleguard'),
    (select id from sets where short_name = 'znc'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Obuun, Mul Daya Ancestor'),
    (select id from sets where short_name = 'znc'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scaretiller'),
    (select id from sets where short_name = 'znc'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Satyr Wayfinder'),
    (select id from sets where short_name = 'znc'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Elder'),
    (select id from sets where short_name = 'znc'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Multani, Yavimaya''s Avatar'),
    (select id from sets where short_name = 'znc'),
    '75',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Arcane Signet'),
    (select id from sets where short_name = 'znc'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'znc'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spinal Embrace'),
    (select id from sets where short_name = 'znc'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Submerged Boneyard'),
    (select id from sets where short_name = 'znc'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Manipulation'),
    (select id from sets where short_name = 'znc'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sporemound'),
    (select id from sets where short_name = 'znc'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krosan Verge'),
    (select id from sets where short_name = 'znc'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Naya Charm'),
    (select id from sets where short_name = 'znc'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Signet'),
    (select id from sets where short_name = 'znc'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Struggle // Survive'),
    (select id from sets where short_name = 'znc'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evolution Sage'),
    (select id from sets where short_name = 'znc'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kor Cartographer'),
    (select id from sets where short_name = 'znc'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myriad Landscape'),
    (select id from sets where short_name = 'znc'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Garrison'),
    (select id from sets where short_name = 'znc'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Retreat to Emeria'),
    (select id from sets where short_name = 'znc'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aetherize'),
    (select id from sets where short_name = 'znc'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necromantic Selection'),
    (select id from sets where short_name = 'znc'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Retreat to Kazandu'),
    (select id from sets where short_name = 'znc'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stolen Identity'),
    (select id from sets where short_name = 'znc'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Admonition Angel'),
    (select id from sets where short_name = 'znc'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Open into Wonder'),
    (select id from sets where short_name = 'znc'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Emeria Shepherd'),
    (select id from sets where short_name = 'znc'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blackblade Reforged'),
    (select id from sets where short_name = 'znc'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rites of Flourishing'),
    (select id from sets where short_name = 'znc'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ogre Slumlord'),
    (select id from sets where short_name = 'znc'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Condemn'),
    (select id from sets where short_name = 'znc'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Endless Obedience'),
    (select id from sets where short_name = 'znc'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crush Contraband'),
    (select id from sets where short_name = 'znc'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anowon, the Ruin Thief'),
    (select id from sets where short_name = 'znc'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Frogtosser Banneret'),
    (select id from sets where short_name = 'znc'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Urd'),
    (select id from sets where short_name = 'znc'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'In Garruk''s Wake'),
    (select id from sets where short_name = 'znc'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selesnya Sanctuary'),
    (select id from sets where short_name = 'znc'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scourge of Fleets'),
    (select id from sets where short_name = 'znc'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Notorious Throng'),
    (select id from sets where short_name = 'znc'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Return of the Wildspeaker'),
    (select id from sets where short_name = 'znc'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Extract from Darkness'),
    (select id from sets where short_name = 'znc'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kodama''s Reach'),
    (select id from sets where short_name = 'znc'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Living Twister'),
    (select id from sets where short_name = 'znc'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Banishing Light'),
    (select id from sets where short_name = 'znc'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Stone'),
    (select id from sets where short_name = 'znc'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fact or Fiction'),
    (select id from sets where short_name = 'znc'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Commander''s Sphere'),
    (select id from sets where short_name = 'znc'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Triton Shorestalker'),
    (select id from sets where short_name = 'znc'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Far Wanderings'),
    (select id from sets where short_name = 'znc'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Rejuvenator'),
    (select id from sets where short_name = 'znc'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stinkdrinker Bandit'),
    (select id from sets where short_name = 'znc'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hour of Revelation'),
    (select id from sets where short_name = 'znc'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dimir Aqueduct'),
    (select id from sets where short_name = 'znc'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dismal Backwater'),
    (select id from sets where short_name = 'znc'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Latchkey Faerie'),
    (select id from sets where short_name = 'znc'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul Guildgate'),
    (select id from sets where short_name = 'znc'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sandstone Oracle'),
    (select id from sets where short_name = 'znc'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selesnya Guildgate'),
    (select id from sets where short_name = 'znc'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whirler Rogue'),
    (select id from sets where short_name = 'znc'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sun Titan'),
    (select id from sets where short_name = 'znc'),
    '21',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rise from the Grave'),
    (select id from sets where short_name = 'znc'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nighthowler'),
    (select id from sets where short_name = 'znc'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Notion Thief'),
    (select id from sets where short_name = 'znc'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harrow'),
    (select id from sets where short_name = 'znc'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oona, Queen of the Fae'),
    (select id from sets where short_name = 'znc'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Khalni Heart Expedition'),
    (select id from sets where short_name = 'znc'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scytheclaw'),
    (select id from sets where short_name = 'znc'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anowon, the Ruin Thief'),
    (select id from sets where short_name = 'znc'),
    '7',
    'mythic'
) 
 on conflict do nothing;
