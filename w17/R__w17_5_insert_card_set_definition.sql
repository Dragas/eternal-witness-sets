insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Stealer of Secrets'),
    (select id from sets where short_name = 'w17'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = 'w17'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stalking Tiger'),
    (select id from sets where short_name = 'w17'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = 'w17'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glory Seeker'),
    (select id from sets where short_name = 'w17'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'w17'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodhunter Bat'),
    (select id from sets where short_name = 'w17'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sleep Paralysis'),
    (select id from sets where short_name = 'w17'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = 'w17'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coral Merfolk'),
    (select id from sets where short_name = 'w17'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Standing Troops'),
    (select id from sets where short_name = 'w17'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drag Under'),
    (select id from sets where short_name = 'w17'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inspiration'),
    (select id from sets where short_name = 'w17'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk''s Horde'),
    (select id from sets where short_name = 'w17'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Victory''s Herald'),
    (select id from sets where short_name = 'w17'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sphinx of Magosi'),
    (select id from sets where short_name = 'w17'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wing Snare'),
    (select id from sets where short_name = 'w17'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Certain Death'),
    (select id from sets where short_name = 'w17'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oakenform'),
    (select id from sets where short_name = 'w17'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormfront Pegasus'),
    (select id from sets where short_name = 'w17'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Untamed Hunger'),
    (select id from sets where short_name = 'w17'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = 'w17'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stampeding Rhino'),
    (select id from sets where short_name = 'w17'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divine Verdict'),
    (select id from sets where short_name = 'w17'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Falkenrath Reaver'),
    (select id from sets where short_name = 'w17'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = 'w17'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thundering Giant'),
    (select id from sets where short_name = 'w17'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rootwalla'),
    (select id from sets where short_name = 'w17'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tricks of the Trade'),
    (select id from sets where short_name = 'w17'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rabid Bite'),
    (select id from sets where short_name = 'w17'),
    '26',
    'common'
) 
 on conflict do nothing;
