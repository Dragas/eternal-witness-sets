insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Manabarbs'),
    (select id from sets where short_name = '10e'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Colossus of Sardia'),
    (select id from sets where short_name = '10e'),
    '317★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Lore'),
    (select id from sets where short_name = '10e'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drudge Skeletons'),
    (select id from sets where short_name = '10e'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spineless Thug'),
    (select id from sets where short_name = '10e'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure Hunter'),
    (select id from sets where short_name = '10e'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scathe Zombies'),
    (select id from sets where short_name = '10e'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Hellkite'),
    (select id from sets where short_name = '10e'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ravenous Rats'),
    (select id from sets where short_name = '10e'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snapping Drake'),
    (select id from sets where short_name = '10e'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nomad Mythmaker'),
    (select id from sets where short_name = '10e'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wurm''s Tooth'),
    (select id from sets where short_name = '10e'),
    '346',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flowstone Slide'),
    (select id from sets where short_name = '10e'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steadfast Guard'),
    (select id from sets where short_name = '10e'),
    '48★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clone'),
    (select id from sets where short_name = '10e'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twincast'),
    (select id from sets where short_name = '10e'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warrior''s Honor'),
    (select id from sets where short_name = '10e'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rootwater Matriarch'),
    (select id from sets where short_name = '10e'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wild Griffin'),
    (select id from sets where short_name = '10e'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spark Elemental'),
    (select id from sets where short_name = '10e'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lumengrid Warden'),
    (select id from sets where short_name = '10e'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghitu Encampment'),
    (select id from sets where short_name = '10e'),
    '353',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyroclasm'),
    (select id from sets where short_name = '10e'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cruel Edict'),
    (select id from sets where short_name = '10e'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = '10e'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Elemental'),
    (select id from sets where short_name = '10e'),
    '217★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcanis the Omnipotent'),
    (select id from sets where short_name = '10e'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '10e'),
    '375',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stun'),
    (select id from sets where short_name = '10e'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit Link'),
    (select id from sets where short_name = '10e'),
    '45★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viashino Sandscout'),
    (select id from sets where short_name = '10e'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Robe of Mirrors'),
    (select id from sets where short_name = '10e'),
    '101★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nomad Mythmaker'),
    (select id from sets where short_name = '10e'),
    '30★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampire Bats'),
    (select id from sets where short_name = '10e'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Counsel of the Soratami'),
    (select id from sets where short_name = '10e'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Megrim'),
    (select id from sets where short_name = '10e'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seismic Assault'),
    (select id from sets where short_name = '10e'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tempest of Light'),
    (select id from sets where short_name = '10e'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Swords'),
    (select id from sets where short_name = '10e'),
    '57★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hill Giant'),
    (select id from sets where short_name = '10e'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Wraith'),
    (select id from sets where short_name = '10e'),
    '130★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rule of Law'),
    (select id from sets where short_name = '10e'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Telepathy'),
    (select id from sets where short_name = '10e'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reviving Dose'),
    (select id from sets where short_name = '10e'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spiketail Hatchling'),
    (select id from sets where short_name = '10e'),
    '111★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tundra Wolves'),
    (select id from sets where short_name = '10e'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treetop Village'),
    (select id from sets where short_name = '10e'),
    '361★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raging Goblin'),
    (select id from sets where short_name = '10e'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cancel'),
    (select id from sets where short_name = '10e'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mogg Fanatic'),
    (select id from sets where short_name = '10e'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Wood'),
    (select id from sets where short_name = '10e'),
    '309',
    'common'
) ,
(
    (select id from mtgcard where name = 'Suntail Hawk'),
    (select id from sets where short_name = '10e'),
    '50★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Uncontrollable Anger'),
    (select id from sets where short_name = '10e'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mantis Engine'),
    (select id from sets where short_name = '10e'),
    '333★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Juggernaut'),
    (select id from sets where short_name = '10e'),
    '328',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lava Axe'),
    (select id from sets where short_name = '10e'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'True Believer'),
    (select id from sets where short_name = '10e'),
    '53★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spellbook'),
    (select id from sets where short_name = '10e'),
    '343',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Royal Assassin'),
    (select id from sets where short_name = '10e'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fugitive Wizard'),
    (select id from sets where short_name = '10e'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ascendant Evincar'),
    (select id from sets where short_name = '10e'),
    '127★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rootwater Commando'),
    (select id from sets where short_name = '10e'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Roost'),
    (select id from sets where short_name = '10e'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Field Marshal'),
    (select id from sets where short_name = '10e'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Razormane Masticore'),
    (select id from sets where short_name = '10e'),
    '340',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Distress'),
    (select id from sets where short_name = '10e'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sage Owl'),
    (select id from sets where short_name = '10e'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beacon of Unrest'),
    (select id from sets where short_name = '10e'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghitu Encampment'),
    (select id from sets where short_name = '10e'),
    '353★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thundering Giant'),
    (select id from sets where short_name = '10e'),
    '243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diabolic Tutor'),
    (select id from sets where short_name = '10e'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pincher Beetles'),
    (select id from sets where short_name = '10e'),
    '285★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Afflict'),
    (select id from sets where short_name = '10e'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Specter'),
    (select id from sets where short_name = '10e'),
    '151★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blaze'),
    (select id from sets where short_name = '10e'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aven Fisher'),
    (select id from sets where short_name = '10e'),
    '68★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blanchwood Armor'),
    (select id from sets where short_name = '10e'),
    '253★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spirit Link'),
    (select id from sets where short_name = '10e'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viashino Sandscout'),
    (select id from sets where short_name = '10e'),
    '246★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stronghold Discipline'),
    (select id from sets where short_name = '10e'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '10e'),
    '377',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coat of Arms'),
    (select id from sets where short_name = '10e'),
    '316',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firebreathing'),
    (select id from sets where short_name = '10e'),
    '200★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thieving Magpie'),
    (select id from sets where short_name = '10e'),
    '115★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spirit Weaver'),
    (select id from sets where short_name = '10e'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Agonizing Memories'),
    (select id from sets where short_name = '10e'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merfolk Looter'),
    (select id from sets where short_name = '10e'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Conclave'),
    (select id from sets where short_name = '10e'),
    '351★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra''s Embrace'),
    (select id from sets where short_name = '10e'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mirri, Cat Warrior'),
    (select id from sets where short_name = '10e'),
    '279',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Canopy Spider'),
    (select id from sets where short_name = '10e'),
    '254★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghost Warden'),
    (select id from sets where short_name = '10e'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrull Surgeon'),
    (select id from sets where short_name = '10e'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kamahl, Pit Fighter'),
    (select id from sets where short_name = '10e'),
    '214★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overrun'),
    (select id from sets where short_name = '10e'),
    '284★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glorious Anthem'),
    (select id from sets where short_name = '10e'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rushwood Dryad'),
    (select id from sets where short_name = '10e'),
    '294★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rock Badger'),
    (select id from sets where short_name = '10e'),
    '226★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chromatic Star'),
    (select id from sets where short_name = '10e'),
    '314',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plague Wind'),
    (select id from sets where short_name = '10e'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = '10e'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirri, Cat Warrior'),
    (select id from sets where short_name = '10e'),
    '279★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Severed Legion'),
    (select id from sets where short_name = '10e'),
    '177★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = '10e'),
    '39★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Remove Soul'),
    (select id from sets where short_name = '10e'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sage Owl'),
    (select id from sets where short_name = '10e'),
    '104★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hurricane'),
    (select id from sets where short_name = '10e'),
    '270',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Recollect'),
    (select id from sets where short_name = '10e'),
    '289',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sea Monster'),
    (select id from sets where short_name = '10e'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doomed Necromancer'),
    (select id from sets where short_name = '10e'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '10e'),
    '378',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tidings'),
    (select id from sets where short_name = '10e'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seedborn Muse'),
    (select id from sets where short_name = '10e'),
    '296',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aven Windreader'),
    (select id from sets where short_name = '10e'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Persuasion'),
    (select id from sets where short_name = '10e'),
    '95★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Threaten'),
    (select id from sets where short_name = '10e'),
    '242★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Denizen of the Deep'),
    (select id from sets where short_name = '10e'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rod of Ruin'),
    (select id from sets where short_name = '10e'),
    '341',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Icy Manipulator'),
    (select id from sets where short_name = '10e'),
    '326',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Time Stretch'),
    (select id from sets where short_name = '10e'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Telling Time'),
    (select id from sets where short_name = '10e'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = '10e'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Graveborn Muse'),
    (select id from sets where short_name = '10e'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Doubling Cube'),
    (select id from sets where short_name = '10e'),
    '321',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rushwood Dryad'),
    (select id from sets where short_name = '10e'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodfire Colossus'),
    (select id from sets where short_name = '10e'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Air'),
    (select id from sets where short_name = '10e'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scoria Wurm'),
    (select id from sets where short_name = '10e'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aven Cloudchaser'),
    (select id from sets where short_name = '10e'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grizzly Bears'),
    (select id from sets where short_name = '10e'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smash'),
    (select id from sets where short_name = '10e'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '10e'),
    '383',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sculpting Steel'),
    (select id from sets where short_name = '10e'),
    '342',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Hive'),
    (select id from sets where short_name = '10e'),
    '324',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcane Teachings'),
    (select id from sets where short_name = '10e'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Midnight Ritual'),
    (select id from sets where short_name = '10e'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steadfast Guard'),
    (select id from sets where short_name = '10e'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time Stop'),
    (select id from sets where short_name = '10e'),
    '117★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = '10e'),
    '252',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dehydration'),
    (select id from sets where short_name = '10e'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heart of Light'),
    (select id from sets where short_name = '10e'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abundance'),
    (select id from sets where short_name = '10e'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spitting Earth'),
    (select id from sets where short_name = '10e'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pincher Beetles'),
    (select id from sets where short_name = '10e'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spawning Pool'),
    (select id from sets where short_name = '10e'),
    '358★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rock Badger'),
    (select id from sets where short_name = '10e'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Rager'),
    (select id from sets where short_name = '10e'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Field Marshal'),
    (select id from sets where short_name = '10e'),
    '15★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin King'),
    (select id from sets where short_name = '10e'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reya Dawnbringer'),
    (select id from sets where short_name = '10e'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Regeneration'),
    (select id from sets where short_name = '10e'),
    '290',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Furnace Whelp'),
    (select id from sets where short_name = '10e'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Upwelling'),
    (select id from sets where short_name = '10e'),
    '306★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rootwalla'),
    (select id from sets where short_name = '10e'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mass of Ghouls'),
    (select id from sets where short_name = '10e'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sky Weaver'),
    (select id from sets where short_name = '10e'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Time Stop'),
    (select id from sets where short_name = '10e'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Stone'),
    (select id from sets where short_name = '10e'),
    '335',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Prowler'),
    (select id from sets where short_name = '10e'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreamborn Muse'),
    (select id from sets where short_name = '10e'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aura Graft'),
    (select id from sets where short_name = '10e'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lord of the Pit'),
    (select id from sets where short_name = '10e'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rage Weaver'),
    (select id from sets where short_name = '10e'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Threaten'),
    (select id from sets where short_name = '10e'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Sky Raider'),
    (select id from sets where short_name = '10e'),
    '210★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rain of Tears'),
    (select id from sets where short_name = '10e'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raging Goblin'),
    (select id from sets where short_name = '10e'),
    '224★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hail of Arrows'),
    (select id from sets where short_name = '10e'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mahamoti Djinn'),
    (select id from sets where short_name = '10e'),
    '90★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = '10e'),
    '164★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Feast'),
    (select id from sets where short_name = '10e'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Furnace of Rath'),
    (select id from sets where short_name = '10e'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fear'),
    (select id from sets where short_name = '10e'),
    '142★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tangle Spider'),
    (select id from sets where short_name = '10e'),
    '303★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Citanul Flute'),
    (select id from sets where short_name = '10e'),
    '315',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shunt'),
    (select id from sets where short_name = '10e'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forbidding Watchtower'),
    (select id from sets where short_name = '10e'),
    '352',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deathmark'),
    (select id from sets where short_name = '10e'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fists of the Anvil'),
    (select id from sets where short_name = '10e'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Warden'),
    (select id from sets where short_name = '10e'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sleeper Agent'),
    (select id from sets where short_name = '10e'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Artillery'),
    (select id from sets where short_name = '10e'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = '10e'),
    '230★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whispersilk Cloak'),
    (select id from sets where short_name = '10e'),
    '345',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Joiner Adept'),
    (select id from sets where short_name = '10e'),
    '271',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Story Circle'),
    (select id from sets where short_name = '10e'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demystify'),
    (select id from sets where short_name = '10e'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Femeref Archers'),
    (select id from sets where short_name = '10e'),
    '264',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Samite Healer'),
    (select id from sets where short_name = '10e'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Warrior'),
    (select id from sets where short_name = '10e'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra''s Embrace'),
    (select id from sets where short_name = '10e'),
    '40★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = '10e'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'No Rest for the Wicked'),
    (select id from sets where short_name = '10e'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shimmering Wings'),
    (select id from sets where short_name = '10e'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overgrowth'),
    (select id from sets where short_name = '10e'),
    '283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Cloudchaser'),
    (select id from sets where short_name = '10e'),
    '7★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Contaminated Bond'),
    (select id from sets where short_name = '10e'),
    '132★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beacon of Destruction'),
    (select id from sets where short_name = '10e'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = '10e'),
    '360',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = '10e'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windborn Muse'),
    (select id from sets where short_name = '10e'),
    '60★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = '10e'),
    '252★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Bend'),
    (select id from sets where short_name = '10e'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin King'),
    (select id from sets where short_name = '10e'),
    '207★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Platinum Angel'),
    (select id from sets where short_name = '10e'),
    '339',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faerie Conclave'),
    (select id from sets where short_name = '10e'),
    '351',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Fire'),
    (select id from sets where short_name = '10e'),
    '247',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Millstone'),
    (select id from sets where short_name = '10e'),
    '334',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rhox'),
    (select id from sets where short_name = '10e'),
    '291',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ornithopter'),
    (select id from sets where short_name = '10e'),
    '336★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Paladin en-Vec'),
    (select id from sets where short_name = '10e'),
    '32★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flashfreeze'),
    (select id from sets where short_name = '10e'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Underworld Dreams'),
    (select id from sets where short_name = '10e'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angelic Wall'),
    (select id from sets where short_name = '10e'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relentless Rats'),
    (select id from sets where short_name = '10e'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anaba Bodyguard'),
    (select id from sets where short_name = '10e'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brushland'),
    (select id from sets where short_name = '10e'),
    '349',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Icatian Priest'),
    (select id from sets where short_name = '10e'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = '10e'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treetop Bracers'),
    (select id from sets where short_name = '10e'),
    '304★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Root Maze'),
    (select id from sets where short_name = '10e'),
    '292',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stampeding Wildebeests'),
    (select id from sets where short_name = '10e'),
    '300★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Might of Oaks'),
    (select id from sets where short_name = '10e'),
    '277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Head Games'),
    (select id from sets where short_name = '10e'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lavaborn Muse'),
    (select id from sets where short_name = '10e'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel''s Feather'),
    (select id from sets where short_name = '10e'),
    '311',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Civic Wayfinder'),
    (select id from sets where short_name = '10e'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Berserker'),
    (select id from sets where short_name = '10e'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lord of the Undead'),
    (select id from sets where short_name = '10e'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pithing Needle'),
    (select id from sets where short_name = '10e'),
    '338',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancestor''s Chosen'),
    (select id from sets where short_name = '10e'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mahamoti Djinn'),
    (select id from sets where short_name = '10e'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nantuko Husk'),
    (select id from sets where short_name = '10e'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Molimo, Maro-Sorcerer'),
    (select id from sets where short_name = '10e'),
    '280',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firebreathing'),
    (select id from sets where short_name = '10e'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '10e'),
    '364',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Coast'),
    (select id from sets where short_name = '10e'),
    '363',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Hive'),
    (select id from sets where short_name = '10e'),
    '324★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anaba Bodyguard'),
    (select id from sets where short_name = '10e'),
    '187★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '10e'),
    '366',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tundra Wolves'),
    (select id from sets where short_name = '10e'),
    '54★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancestor''s Chosen'),
    (select id from sets where short_name = '10e'),
    '1★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ascendant Evincar'),
    (select id from sets where short_name = '10e'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Severed Legion'),
    (select id from sets where short_name = '10e'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cephalid Constable'),
    (select id from sets where short_name = '10e'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soulblast'),
    (select id from sets where short_name = '10e'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uncontrollable Anger'),
    (select id from sets where short_name = '10e'),
    '244★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loxodon Mystic'),
    (select id from sets where short_name = '10e'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = '10e'),
    '31★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fountain of Youth'),
    (select id from sets where short_name = '10e'),
    '323',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scion of the Wild'),
    (select id from sets where short_name = '10e'),
    '295',
    'rare'
) ,
(
    (select id from mtgcard where name = 'True Believer'),
    (select id from sets where short_name = '10e'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mantis Engine'),
    (select id from sets where short_name = '10e'),
    '333',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Peek'),
    (select id from sets where short_name = '10e'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molimo, Maro-Sorcerer'),
    (select id from sets where short_name = '10e'),
    '280★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stalking Tiger'),
    (select id from sets where short_name = '10e'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Roost'),
    (select id from sets where short_name = '10e'),
    '197★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Skirmisher'),
    (select id from sets where short_name = '10e'),
    '43★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightmare'),
    (select id from sets where short_name = '10e'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twitch'),
    (select id from sets where short_name = '10e'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steel Golem'),
    (select id from sets where short_name = '10e'),
    '344',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Herald'),
    (select id from sets where short_name = '10e'),
    '265',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '10e'),
    '369',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quicksand'),
    (select id from sets where short_name = '10e'),
    '356',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viashino Runner'),
    (select id from sets where short_name = '10e'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unholy Strength'),
    (select id from sets where short_name = '10e'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Essence Drain'),
    (select id from sets where short_name = '10e'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = '10e'),
    '282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Discombobulate'),
    (select id from sets where short_name = '10e'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pariah'),
    (select id from sets where short_name = '10e'),
    '33★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Underground River'),
    (select id from sets where short_name = '10e'),
    '362',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Regeneration'),
    (select id from sets where short_name = '10e'),
    '290★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mortivore'),
    (select id from sets where short_name = '10e'),
    '161★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spiketail Hatchling'),
    (select id from sets where short_name = '10e'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ornithopter'),
    (select id from sets where short_name = '10e'),
    '336',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coat of Arms'),
    (select id from sets where short_name = '10e'),
    '316★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tangle Spider'),
    (select id from sets where short_name = '10e'),
    '303',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Champion'),
    (select id from sets where short_name = '10e'),
    '261★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plague Beetle'),
    (select id from sets where short_name = '10e'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Mercy'),
    (select id from sets where short_name = '10e'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phage the Untouchable'),
    (select id from sets where short_name = '10e'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spark Elemental'),
    (select id from sets where short_name = '10e'),
    '237★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spawning Pool'),
    (select id from sets where short_name = '10e'),
    '358',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sift'),
    (select id from sets where short_name = '10e'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '10e'),
    '379',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overrun'),
    (select id from sets where short_name = '10e'),
    '284',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fog Elemental'),
    (select id from sets where short_name = '10e'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thieving Magpie'),
    (select id from sets where short_name = '10e'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Natural Spring'),
    (select id from sets where short_name = '10e'),
    '281',
    'common'
) ,
(
    (select id from mtgcard where name = 'Robe of Mirrors'),
    (select id from sets where short_name = '10e'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Ranger'),
    (select id from sets where short_name = '10e'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Elemental'),
    (select id from sets where short_name = '10e'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Siege-Gang Commander'),
    (select id from sets where short_name = '10e'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guerrilla Tactics'),
    (select id from sets where short_name = '10e'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Honor Guard'),
    (select id from sets where short_name = '10e'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chimeric Staff'),
    (select id from sets where short_name = '10e'),
    '313',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flamewave Invoker'),
    (select id from sets where short_name = '10e'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snapping Drake'),
    (select id from sets where short_name = '10e'),
    '110★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '10e'),
    '374',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '10e'),
    '382',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lure'),
    (select id from sets where short_name = '10e'),
    '276★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = '10e'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angelic Blessing'),
    (select id from sets where short_name = '10e'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horseshoe Crab'),
    (select id from sets where short_name = '10e'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elven Riders'),
    (select id from sets where short_name = '10e'),
    '259',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ballista Squad'),
    (select id from sets where short_name = '10e'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Contaminated Bond'),
    (select id from sets where short_name = '10e'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kraken''s Eye'),
    (select id from sets where short_name = '10e'),
    '329',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sudden Impact'),
    (select id from sets where short_name = '10e'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hurkyl''s Recall'),
    (select id from sets where short_name = '10e'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = '10e'),
    '376',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Scimitar'),
    (select id from sets where short_name = '10e'),
    '331',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jayemdae Tome'),
    (select id from sets where short_name = '10e'),
    '327',
    'rare'
) ,
(
    (select id from mtgcard where name = 'March of the Machines'),
    (select id from sets where short_name = '10e'),
    '91★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evacuation'),
    (select id from sets where short_name = '10e'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Patrol'),
    (select id from sets where short_name = '10e'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '10e'),
    '370',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Elite Infantry'),
    (select id from sets where short_name = '10e'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Composite Golem'),
    (select id from sets where short_name = '10e'),
    '318',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Looming Shade'),
    (select id from sets where short_name = '10e'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earth Elemental'),
    (select id from sets where short_name = '10e'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sengir Vampire'),
    (select id from sets where short_name = '10e'),
    '176★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Caves of Koilos'),
    (select id from sets where short_name = '10e'),
    '350',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scalpelexis'),
    (select id from sets where short_name = '10e'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Champion'),
    (select id from sets where short_name = '10e'),
    '261',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shimmering Wings'),
    (select id from sets where short_name = '10e'),
    '107★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Legacy Weapon'),
    (select id from sets where short_name = '10e'),
    '330',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aura of Silence'),
    (select id from sets where short_name = '10e'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Paladin en-Vec'),
    (select id from sets where short_name = '10e'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bottle Gnomes'),
    (select id from sets where short_name = '10e'),
    '312',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Howling Mine'),
    (select id from sets where short_name = '10e'),
    '325',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel of Mercy'),
    (select id from sets where short_name = '10e'),
    '2★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = '10e'),
    '64★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Puppeteer'),
    (select id from sets where short_name = '10e'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stampeding Wildebeests'),
    (select id from sets where short_name = '10e'),
    '300',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mortivore'),
    (select id from sets where short_name = '10e'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ambassador Laquatus'),
    (select id from sets where short_name = '10e'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kavu Climber'),
    (select id from sets where short_name = '10e'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Venerable Monk'),
    (select id from sets where short_name = '10e'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dehydration'),
    (select id from sets where short_name = '10e'),
    '78★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Suntail Hawk'),
    (select id from sets where short_name = '10e'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Youthful Knight'),
    (select id from sets where short_name = '10e'),
    '62★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windborn Muse'),
    (select id from sets where short_name = '10e'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hate Weaver'),
    (select id from sets where short_name = '10e'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Enchantress'),
    (select id from sets where short_name = '10e'),
    '310',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lord of the Pit'),
    (select id from sets where short_name = '10e'),
    '154★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Academy Researchers'),
    (select id from sets where short_name = '10e'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Wood'),
    (select id from sets where short_name = '10e'),
    '309★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '10e'),
    '381',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '10e'),
    '368',
    'common'
) ,
(
    (select id from mtgcard where name = 'Commune with Nature'),
    (select id from sets where short_name = '10e'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'High Ground'),
    (select id from sets where short_name = '10e'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dusk Imp'),
    (select id from sets where short_name = '10e'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Consume Spirit'),
    (select id from sets where short_name = '10e'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treetop Bracers'),
    (select id from sets where short_name = '10e'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mobilization'),
    (select id from sets where short_name = '10e'),
    '29★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rootwater Commando'),
    (select id from sets where short_name = '10e'),
    '102★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Holy Strength'),
    (select id from sets where short_name = '10e'),
    '22★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = '10e'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Highway Robber'),
    (select id from sets where short_name = '10e'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deluge'),
    (select id from sets where short_name = '10e'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aven Fisher'),
    (select id from sets where short_name = '10e'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Verdant Force'),
    (select id from sets where short_name = '10e'),
    '307',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voice of All'),
    (select id from sets where short_name = '10e'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mobilization'),
    (select id from sets where short_name = '10e'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benalish Knight'),
    (select id from sets where short_name = '10e'),
    '11★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squee, Goblin Nabob'),
    (select id from sets where short_name = '10e'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Llanowar Sentinel'),
    (select id from sets where short_name = '10e'),
    '275',
    'common'
) ,
(
    (select id from mtgcard where name = 'Righteousness'),
    (select id from sets where short_name = '10e'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = '10e'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cone of Flame'),
    (select id from sets where short_name = '10e'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loyal Sentry'),
    (select id from sets where short_name = '10e'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aggressive Urge'),
    (select id from sets where short_name = '10e'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grave Pact'),
    (select id from sets where short_name = '10e'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warp World'),
    (select id from sets where short_name = '10e'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Furnace Whelp'),
    (select id from sets where short_name = '10e'),
    '205★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Skirmisher'),
    (select id from sets where short_name = '10e'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcane Teachings'),
    (select id from sets where short_name = '10e'),
    '188★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Primal Rage'),
    (select id from sets where short_name = '10e'),
    '286★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sky Weaver'),
    (select id from sets where short_name = '10e'),
    '109★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Air'),
    (select id from sets where short_name = '10e'),
    '124★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Creeping Mold'),
    (select id from sets where short_name = '10e'),
    '258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cloud Elemental'),
    (select id from sets where short_name = '10e'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bogardan Firefiend'),
    (select id from sets where short_name = '10e'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = '10e'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viridian Shaman'),
    (select id from sets where short_name = '10e'),
    '308',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scalpelexis'),
    (select id from sets where short_name = '10e'),
    '105★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '10e'),
    '365',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bandage'),
    (select id from sets where short_name = '10e'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = '10e'),
    '267★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Canopy Spider'),
    (select id from sets where short_name = '10e'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avatar of Might'),
    (select id from sets where short_name = '10e'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sulfurous Springs'),
    (select id from sets where short_name = '10e'),
    '359',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloud Elemental'),
    (select id from sets where short_name = '10e'),
    '74★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Troll Ascetic'),
    (select id from sets where short_name = '10e'),
    '305★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fog Elemental'),
    (select id from sets where short_name = '10e'),
    '85★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Piker'),
    (select id from sets where short_name = '10e'),
    '209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloud Sprite'),
    (select id from sets where short_name = '10e'),
    '75★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cryoclasm'),
    (select id from sets where short_name = '10e'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karplusan Strider'),
    (select id from sets where short_name = '10e'),
    '272',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Relentless Assault'),
    (select id from sets where short_name = '10e'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Specter'),
    (select id from sets where short_name = '10e'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benalish Knight'),
    (select id from sets where short_name = '10e'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Wraith'),
    (select id from sets where short_name = '10e'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nekrataal'),
    (select id from sets where short_name = '10e'),
    '163★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Troll Ascetic'),
    (select id from sets where short_name = '10e'),
    '305',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = '10e'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karplusan Forest'),
    (select id from sets where short_name = '10e'),
    '354',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plagiarize'),
    (select id from sets where short_name = '10e'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Upwelling'),
    (select id from sets where short_name = '10e'),
    '306',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shivan Reef'),
    (select id from sets where short_name = '10e'),
    '357',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knight of Dusk'),
    (select id from sets where short_name = '10e'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Fire'),
    (select id from sets where short_name = '10e'),
    '247★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '10e'),
    '373',
    'common'
) ,
(
    (select id from mtgcard where name = 'Assassinate'),
    (select id from sets where short_name = '10e'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Youthful Knight'),
    (select id from sets where short_name = '10e'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lure'),
    (select id from sets where short_name = '10e'),
    '276',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Basilisk'),
    (select id from sets where short_name = '10e'),
    '301',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Starlight Invoker'),
    (select id from sets where short_name = '10e'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fear'),
    (select id from sets where short_name = '10e'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drudge Skeletons'),
    (select id from sets where short_name = '10e'),
    '139★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duct Crawler'),
    (select id from sets where short_name = '10e'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = '10e'),
    '380',
    'common'
) ,
(
    (select id from mtgcard where name = 'Razormane Masticore'),
    (select id from sets where short_name = '10e'),
    '340★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = '10e'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = '10e'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Craw Wurm'),
    (select id from sets where short_name = '10e'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Vault'),
    (select id from sets where short_name = '10e'),
    '337',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'March of the Machines'),
    (select id from sets where short_name = '10e'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voice of All'),
    (select id from sets where short_name = '10e'),
    '56★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Bend'),
    (select id from sets where short_name = '10e'),
    '93★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Might Weaver'),
    (select id from sets where short_name = '10e'),
    '278',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thundering Giant'),
    (select id from sets where short_name = '10e'),
    '243★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blanchwood Armor'),
    (select id from sets where short_name = '10e'),
    '253',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treetop Village'),
    (select id from sets where short_name = '10e'),
    '361',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Traumatize'),
    (select id from sets where short_name = '10e'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quirion Dryad'),
    (select id from sets where short_name = '10e'),
    '287',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Might Weaver'),
    (select id from sets where short_name = '10e'),
    '278★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battlefield Forge'),
    (select id from sets where short_name = '10e'),
    '348',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Holy Strength'),
    (select id from sets where short_name = '10e'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dusk Imp'),
    (select id from sets where short_name = '10e'),
    '140★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Luminesce'),
    (select id from sets where short_name = '10e'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodrock Cyclops'),
    (select id from sets where short_name = '10e'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whispersilk Cloak'),
    (select id from sets where short_name = '10e'),
    '345★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Piper'),
    (select id from sets where short_name = '10e'),
    '262',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prodigal Pyromancer'),
    (select id from sets where short_name = '10e'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Swords'),
    (select id from sets where short_name = '10e'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelic Wall'),
    (select id from sets where short_name = '10e'),
    '5★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = '10e'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Scimitar'),
    (select id from sets where short_name = '10e'),
    '331★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spined Wurm'),
    (select id from sets where short_name = '10e'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Recover'),
    (select id from sets where short_name = '10e'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Blessing'),
    (select id from sets where short_name = '10e'),
    '3★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Persuasion'),
    (select id from sets where short_name = '10e'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon''s Claw'),
    (select id from sets where short_name = '10e'),
    '322',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Hellkite'),
    (select id from sets where short_name = '10e'),
    '231★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vedalken Mastermind'),
    (select id from sets where short_name = '10e'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Royal Guard'),
    (select id from sets where short_name = '10e'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Condemn'),
    (select id from sets where short_name = '10e'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avatar of Might'),
    (select id from sets where short_name = '10e'),
    '251★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pariah'),
    (select id from sets where short_name = '10e'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Sky Raider'),
    (select id from sets where short_name = '10e'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plague Beetle'),
    (select id from sets where short_name = '10e'),
    '168★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unsummon'),
    (select id from sets where short_name = '10e'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primal Rage'),
    (select id from sets where short_name = '10e'),
    '286',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kamahl, Pit Fighter'),
    (select id from sets where short_name = '10e'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overgrowth'),
    (select id from sets where short_name = '10e'),
    '283★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon''s Horn'),
    (select id from sets where short_name = '10e'),
    '320',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = '10e'),
    '288',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dross Crocodile'),
    (select id from sets where short_name = '10e'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = '10e'),
    '367',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beacon of Immortality'),
    (select id from sets where short_name = '10e'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rhox'),
    (select id from sets where short_name = '10e'),
    '291★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunken Hope'),
    (select id from sets where short_name = '10e'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rage Weaver'),
    (select id from sets where short_name = '10e'),
    '223★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crafty Pathmage'),
    (select id from sets where short_name = '10e'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mortal Combat'),
    (select id from sets where short_name = '10e'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Platinum Angel'),
    (select id from sets where short_name = '10e'),
    '339★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Adarkar Wastes'),
    (select id from sets where short_name = '10e'),
    '347',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demolish'),
    (select id from sets where short_name = '10e'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Holy Day'),
    (select id from sets where short_name = '10e'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = '10e'),
    '371',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boomerang'),
    (select id from sets where short_name = '10e'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reminisce'),
    (select id from sets where short_name = '10e'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hidden Horror'),
    (select id from sets where short_name = '10e'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampire Bats'),
    (select id from sets where short_name = '10e'),
    '186★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heart of Light'),
    (select id from sets where short_name = '10e'),
    '19★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loxodon Warhammer'),
    (select id from sets where short_name = '10e'),
    '332',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = '10e'),
    '372',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enormous Baloth'),
    (select id from sets where short_name = '10e'),
    '263',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hunted Wumpus'),
    (select id from sets where short_name = '10e'),
    '269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shatterstorm'),
    (select id from sets where short_name = '10e'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelic Chorus'),
    (select id from sets where short_name = '10e'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reya Dawnbringer'),
    (select id from sets where short_name = '10e'),
    '35★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Patrol'),
    (select id from sets where short_name = '10e'),
    '41★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Wastes'),
    (select id from sets where short_name = '10e'),
    '355',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Prowler'),
    (select id from sets where short_name = '10e'),
    '42★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Scrying'),
    (select id from sets where short_name = '10e'),
    '302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unholy Strength'),
    (select id from sets where short_name = '10e'),
    '185★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Windreader'),
    (select id from sets where short_name = '10e'),
    '69★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nekrataal'),
    (select id from sets where short_name = '10e'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crucible of Worlds'),
    (select id from sets where short_name = '10e'),
    '319',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Festering Goblin'),
    (select id from sets where short_name = '10e'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Colossus of Sardia'),
    (select id from sets where short_name = '10e'),
    '317',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loxodon Warhammer'),
    (select id from sets where short_name = '10e'),
    '332★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cho-Manno, Revolutionary'),
    (select id from sets where short_name = '10e'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloud Sprite'),
    (select id from sets where short_name = '10e'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Griffin'),
    (select id from sets where short_name = '10e'),
    '59★',
    'common'
) 
 on conflict do nothing;
