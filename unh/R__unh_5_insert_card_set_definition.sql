insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Mise'),
    (select id from sets where short_name = 'unh'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eye to Eye'),
    (select id from sets where short_name = 'unh'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Six-y Beast'),
    (select id from sets where short_name = 'unh'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aesthetic Consultation'),
    (select id from sets where short_name = 'unh'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Togglodyte'),
    (select id from sets where short_name = 'unh'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moniker Mage'),
    (select id from sets where short_name = 'unh'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Monkey Monkey Monkey'),
    (select id from sets where short_name = 'unh'),
    '104★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Richard Garfield, Ph.D.'),
    (select id from sets where short_name = 'unh'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smart Ass'),
    (select id from sets where short_name = 'unh'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rare-B-Gone'),
    (select id from sets where short_name = 'unh'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Standing Army'),
    (select id from sets where short_name = 'unh'),
    '20★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'unh'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Standing Army'),
    (select id from sets where short_name = 'unh'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Artful Looter'),
    (select id from sets where short_name = 'unh'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aesthetic Consultation'),
    (select id from sets where short_name = 'unh'),
    '48★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Creature Guy'),
    (select id from sets where short_name = 'unh'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'unh'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Land Aid ''04'),
    (select id from sets where short_name = 'unh'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bursting Beebles'),
    (select id from sets where short_name = 'unh'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'unh'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Letter Bomb'),
    (select id from sets where short_name = 'unh'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'When Fluffy Bunnies Attack'),
    (select id from sets where short_name = 'unh'),
    '67★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Secret Agent'),
    (select id from sets where short_name = 'unh'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Form of the Squirrel'),
    (select id from sets where short_name = 'unh'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gleemax'),
    (select id from sets where short_name = 'unh'),
    '121★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keeper of the Sacred Word'),
    (select id from sets where short_name = 'unh'),
    '101★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bad Ass'),
    (select id from sets where short_name = 'unh'),
    '49★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ladies'' Knight'),
    (select id from sets where short_name = 'unh'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Water Gun Balloon Game'),
    (select id from sets where short_name = 'unh'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vile Bile'),
    (select id from sets where short_name = 'unh'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'My First Tome'),
    (select id from sets where short_name = 'unh'),
    '125★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bad Ass'),
    (select id from sets where short_name = 'unh'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Librarian'),
    (select id from sets where short_name = 'unh'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Monkey Monkey Monkey'),
    (select id from sets where short_name = 'unh'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time Machine'),
    (select id from sets where short_name = 'unh'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Atinlay Igpay'),
    (select id from sets where short_name = 'unh'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mother of Goons'),
    (select id from sets where short_name = 'unh'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Touch and Go'),
    (select id from sets where short_name = 'unh'),
    '90★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infernal Spawn of Infernal Spawn of Evil'),
    (select id from sets where short_name = 'unh'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cardpecker'),
    (select id from sets where short_name = 'unh'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flaccify'),
    (select id from sets where short_name = 'unh'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'unh'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wordmail'),
    (select id from sets where short_name = 'unh'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cheap Ass'),
    (select id from sets where short_name = 'unh'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Super Secret Tech'),
    (select id from sets where short_name = 'unh'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Farewell to Arms'),
    (select id from sets where short_name = 'unh'),
    '56★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Richard Garfield, Ph.D.'),
    (select id from sets where short_name = 'unh'),
    '44★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Number Crunch'),
    (select id from sets where short_name = 'unh'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kill! Destroy!'),
    (select id from sets where short_name = 'unh'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frazzled Editor'),
    (select id from sets where short_name = 'unh'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Flair'),
    (select id from sets where short_name = 'unh'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drawn Together'),
    (select id from sets where short_name = 'unh'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Mime'),
    (select id from sets where short_name = 'unh'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Laughing Hyena'),
    (select id from sets where short_name = 'unh'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Double Header'),
    (select id from sets where short_name = 'unh'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodletter'),
    (select id from sets where short_name = 'unh'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pointy Finger of Doom'),
    (select id from sets where short_name = 'unh'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Time Machine'),
    (select id from sets where short_name = 'unh'),
    '128★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Paratroopers'),
    (select id from sets where short_name = 'unh'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = '_____'),
    (select id from sets where short_name = 'unh'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Persecute Artist'),
    (select id from sets where short_name = 'unh'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frankie Peanuts'),
    (select id from sets where short_name = 'unh'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Our Market Research Shows That Players Like Really Long Card Names So We Made this Card to Have the Absolute Longest Card Name Ever Elemental'),
    (select id from sets where short_name = 'unh'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meddling Kids'),
    (select id from sets where short_name = 'unh'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blast from the Past'),
    (select id from sets where short_name = 'unh'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Touch and Go'),
    (select id from sets where short_name = 'unh'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Face to Face'),
    (select id from sets where short_name = 'unh'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magical Hacker'),
    (select id from sets where short_name = 'unh'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Laughing Hyena'),
    (select id from sets where short_name = 'unh'),
    '103★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Screw'),
    (select id from sets where short_name = 'unh'),
    '123★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ambiguity'),
    (select id from sets where short_name = 'unh'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brushstroke Paintermage'),
    (select id from sets where short_name = 'unh'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Farewell to Arms'),
    (select id from sets where short_name = 'unh'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'City of Ass'),
    (select id from sets where short_name = 'unh'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mons''s Goblin Waiters'),
    (select id from sets where short_name = 'unh'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Question Elemental?'),
    (select id from sets where short_name = 'unh'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shoe Tree'),
    (select id from sets where short_name = 'unh'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gleemax'),
    (select id from sets where short_name = 'unh'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Punctuate'),
    (select id from sets where short_name = 'unh'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dumb Ass'),
    (select id from sets where short_name = 'unh'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Collector Protector'),
    (select id from sets where short_name = 'unh'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Hot Tub'),
    (select id from sets where short_name = 'unh'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'unh'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = '"Ach! Hans, Run!"'),
    (select id from sets where short_name = 'unh'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sauté'),
    (select id from sets where short_name = 'unh'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fraction Jackson'),
    (select id from sets where short_name = 'unh'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Who // What // When // Where // Why'),
    (select id from sets where short_name = 'unh'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frazzled Editor'),
    (select id from sets where short_name = 'unh'),
    '77★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shoe Tree'),
    (select id from sets where short_name = 'unh'),
    '109★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avatar of Me'),
    (select id from sets where short_name = 'unh'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Red-Hot Hottie'),
    (select id from sets where short_name = 'unh'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fat Ass'),
    (select id from sets where short_name = 'unh'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Little Girl'),
    (select id from sets where short_name = 'unh'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin S.W.A.T. Team'),
    (select id from sets where short_name = 'unh'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tainted Monkey'),
    (select id from sets where short_name = 'unh'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Staying Power'),
    (select id from sets where short_name = 'unh'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wet Willie of the Damned'),
    (select id from sets where short_name = 'unh'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Symbol Status'),
    (select id from sets where short_name = 'unh'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Granny''s Payback'),
    (select id from sets where short_name = 'unh'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Man of Measure'),
    (select id from sets where short_name = 'unh'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Now I Know My ABC''s'),
    (select id from sets where short_name = 'unh'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zombie Fanboy'),
    (select id from sets where short_name = 'unh'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enter the Dungeon'),
    (select id from sets where short_name = 'unh'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Remodel'),
    (select id from sets where short_name = 'unh'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Assquatch'),
    (select id from sets where short_name = 'unh'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'R&D''s Secret Lair'),
    (select id from sets where short_name = 'unh'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Name Dropping'),
    (select id from sets where short_name = 'unh'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cardpecker'),
    (select id from sets where short_name = 'unh'),
    '4★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fascist Art Director'),
    (select id from sets where short_name = 'unh'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'When Fluffy Bunnies Attack'),
    (select id from sets where short_name = 'unh'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Side to Side'),
    (select id from sets where short_name = 'unh'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Head to Head'),
    (select id from sets where short_name = 'unh'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blast from the Past'),
    (select id from sets where short_name = 'unh'),
    '72★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gluetius Maximus'),
    (select id from sets where short_name = 'unh'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cheatyface'),
    (select id from sets where short_name = 'unh'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Old Fogey'),
    (select id from sets where short_name = 'unh'),
    '106★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mons''s Goblin Waiters'),
    (select id from sets where short_name = 'unh'),
    '82★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mouth to Mouth'),
    (select id from sets where short_name = 'unh'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pygmy Giant'),
    (select id from sets where short_name = 'unh'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mox Lotus'),
    (select id from sets where short_name = 'unh'),
    '124★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loose Lips'),
    (select id from sets where short_name = 'unh'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Fallen Apart'),
    (select id from sets where short_name = 'unh'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Working Stiff'),
    (select id from sets where short_name = 'unh'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'First Come, First Served'),
    (select id from sets where short_name = 'unh'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Graphic Violence'),
    (select id from sets where short_name = 'unh'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Save Life'),
    (select id from sets where short_name = 'unh'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Uktabi Kong'),
    (select id from sets where short_name = 'unh'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curse of the Fire Penguin // Curse of the Fire Penguin Creature'),
    (select id from sets where short_name = 'unh'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Look at Me, I''m R&D'),
    (select id from sets where short_name = 'unh'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Booster Tutor'),
    (select id from sets where short_name = 'unh'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Framed!'),
    (select id from sets where short_name = 'unh'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Topsy Turvy'),
    (select id from sets where short_name = 'unh'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Supersize'),
    (select id from sets where short_name = 'unh'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rod of Spanking'),
    (select id from sets where short_name = 'unh'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stone-Cold Basilisk'),
    (select id from sets where short_name = 'unh'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zzzyxas''s Abyss'),
    (select id from sets where short_name = 'unh'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Letter Bomb'),
    (select id from sets where short_name = 'unh'),
    '122★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Greater Morphling'),
    (select id from sets where short_name = 'unh'),
    '34★',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Greater Morphling'),
    (select id from sets where short_name = 'unh'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emcee'),
    (select id from sets where short_name = 'unh'),
    '9★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Mime'),
    (select id from sets where short_name = 'unh'),
    '78★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carnivorous Death-Parrot'),
    (select id from sets where short_name = 'unh'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'B-I-N-G-O'),
    (select id from sets where short_name = 'unh'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emcee'),
    (select id from sets where short_name = 'unh'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deal Damage'),
    (select id from sets where short_name = 'unh'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'AWOL'),
    (select id from sets where short_name = 'unh'),
    '2★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stop That'),
    (select id from sets where short_name = 'unh'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spell Counter'),
    (select id from sets where short_name = 'unh'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rocket-Powered Turbo Slug'),
    (select id from sets where short_name = 'unh'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'My First Tome'),
    (select id from sets where short_name = 'unh'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Old Fogey'),
    (select id from sets where short_name = 'unh'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yet Another Aether Vortex'),
    (select id from sets where short_name = 'unh'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Question Elemental?'),
    (select id from sets where short_name = 'unh'),
    '43★',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keeper of the Sacred Word'),
    (select id from sets where short_name = 'unh'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mox Lotus'),
    (select id from sets where short_name = 'unh'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necro-Impotence'),
    (select id from sets where short_name = 'unh'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Art'),
    (select id from sets where short_name = 'unh'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Johnny, Combo Player'),
    (select id from sets where short_name = 'unh'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Toy Boat'),
    (select id from sets where short_name = 'unh'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Erase (Not the Urza''s Legacy One)'),
    (select id from sets where short_name = 'unh'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish House Party'),
    (select id from sets where short_name = 'unh'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'S.N.O.T.'),
    (select id from sets where short_name = 'unh'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bosom Buddy'),
    (select id from sets where short_name = 'unh'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duh'),
    (select id from sets where short_name = 'unh'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ass Whuppin'''),
    (select id from sets where short_name = 'unh'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'AWOL'),
    (select id from sets where short_name = 'unh'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'World-Bottling Kit'),
    (select id from sets where short_name = 'unh'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Screw'),
    (select id from sets where short_name = 'unh'),
    '123',
    'uncommon'
) 
 on conflict do nothing;
