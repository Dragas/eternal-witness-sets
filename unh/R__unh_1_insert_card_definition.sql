    insert into mtgcard(name) values ('Mise') on conflict do nothing;
    insert into mtgcard(name) values ('Eye to Eye') on conflict do nothing;
    insert into mtgcard(name) values ('Six-y Beast') on conflict do nothing;
    insert into mtgcard(name) values ('Aesthetic Consultation') on conflict do nothing;
    insert into mtgcard(name) values ('Togglodyte') on conflict do nothing;
    insert into mtgcard(name) values ('Moniker Mage') on conflict do nothing;
    insert into mtgcard(name) values ('Monkey Monkey Monkey') on conflict do nothing;
    insert into mtgcard(name) values ('Richard Garfield, Ph.D.') on conflict do nothing;
    insert into mtgcard(name) values ('Smart Ass') on conflict do nothing;
    insert into mtgcard(name) values ('Rare-B-Gone') on conflict do nothing;
    insert into mtgcard(name) values ('Standing Army') on conflict do nothing;
    insert into mtgcard(name) values ('Island') on conflict do nothing;
    insert into mtgcard(name) values ('Artful Looter') on conflict do nothing;
    insert into mtgcard(name) values ('Creature Guy') on conflict do nothing;
    insert into mtgcard(name) values ('Forest') on conflict do nothing;
    insert into mtgcard(name) values ('Land Aid ''04') on conflict do nothing;
    insert into mtgcard(name) values ('Bursting Beebles') on conflict do nothing;
    insert into mtgcard(name) values ('Plains') on conflict do nothing;
    insert into mtgcard(name) values ('Letter Bomb') on conflict do nothing;
    insert into mtgcard(name) values ('When Fluffy Bunnies Attack') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Secret Agent') on conflict do nothing;
    insert into mtgcard(name) values ('Form of the Squirrel') on conflict do nothing;
    insert into mtgcard(name) values ('Gleemax') on conflict do nothing;
    insert into mtgcard(name) values ('Keeper of the Sacred Word') on conflict do nothing;
    insert into mtgcard(name) values ('Bad Ass') on conflict do nothing;
    insert into mtgcard(name) values ('Ladies'' Knight') on conflict do nothing;
    insert into mtgcard(name) values ('Water Gun Balloon Game') on conflict do nothing;
    insert into mtgcard(name) values ('Vile Bile') on conflict do nothing;
    insert into mtgcard(name) values ('My First Tome') on conflict do nothing;
    insert into mtgcard(name) values ('Phyrexian Librarian') on conflict do nothing;
    insert into mtgcard(name) values ('Time Machine') on conflict do nothing;
    insert into mtgcard(name) values ('Atinlay Igpay') on conflict do nothing;
    insert into mtgcard(name) values ('Mother of Goons') on conflict do nothing;
    insert into mtgcard(name) values ('Touch and Go') on conflict do nothing;
    insert into mtgcard(name) values ('Infernal Spawn of Infernal Spawn of Evil') on conflict do nothing;
    insert into mtgcard(name) values ('Cardpecker') on conflict do nothing;
    insert into mtgcard(name) values ('Flaccify') on conflict do nothing;
    insert into mtgcard(name) values ('Mountain') on conflict do nothing;
    insert into mtgcard(name) values ('Wordmail') on conflict do nothing;
    insert into mtgcard(name) values ('Cheap Ass') on conflict do nothing;
    insert into mtgcard(name) values ('Super Secret Tech') on conflict do nothing;
    insert into mtgcard(name) values ('Farewell to Arms') on conflict do nothing;
    insert into mtgcard(name) values ('Number Crunch') on conflict do nothing;
    insert into mtgcard(name) values ('Kill! Destroy!') on conflict do nothing;
    insert into mtgcard(name) values ('Frazzled Editor') on conflict do nothing;
    insert into mtgcard(name) values ('Mana Flair') on conflict do nothing;
    insert into mtgcard(name) values ('Drawn Together') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Mime') on conflict do nothing;
    insert into mtgcard(name) values ('Laughing Hyena') on conflict do nothing;
    insert into mtgcard(name) values ('Double Header') on conflict do nothing;
    insert into mtgcard(name) values ('Bloodletter') on conflict do nothing;
    insert into mtgcard(name) values ('Pointy Finger of Doom') on conflict do nothing;
    insert into mtgcard(name) values ('Orcish Paratroopers') on conflict do nothing;
    insert into mtgcard(name) values ('_____') on conflict do nothing;
    insert into mtgcard(name) values ('Persecute Artist') on conflict do nothing;
    insert into mtgcard(name) values ('Frankie Peanuts') on conflict do nothing;
    insert into mtgcard(name) values ('Our Market Research Shows That Players Like Really Long Card Names So We Made this Card to Have the Absolute Longest Card Name Ever Elemental') on conflict do nothing;
    insert into mtgcard(name) values ('Meddling Kids') on conflict do nothing;
    insert into mtgcard(name) values ('Blast from the Past') on conflict do nothing;
    insert into mtgcard(name) values ('Face to Face') on conflict do nothing;
    insert into mtgcard(name) values ('Magical Hacker') on conflict do nothing;
    insert into mtgcard(name) values ('Mana Screw') on conflict do nothing;
    insert into mtgcard(name) values ('Ambiguity') on conflict do nothing;
    insert into mtgcard(name) values ('Brushstroke Paintermage') on conflict do nothing;
    insert into mtgcard(name) values ('City of Ass') on conflict do nothing;
    insert into mtgcard(name) values ('Mons''s Goblin Waiters') on conflict do nothing;
    insert into mtgcard(name) values ('Question Elemental?') on conflict do nothing;
    insert into mtgcard(name) values ('Shoe Tree') on conflict do nothing;
    insert into mtgcard(name) values ('Punctuate') on conflict do nothing;
    insert into mtgcard(name) values ('Dumb Ass') on conflict do nothing;
    insert into mtgcard(name) values ('Collector Protector') on conflict do nothing;
    insert into mtgcard(name) values ('Urza''s Hot Tub') on conflict do nothing;
    insert into mtgcard(name) values ('Swamp') on conflict do nothing;
    insert into mtgcard(name) values ('"Ach! Hans, Run!"') on conflict do nothing;
    insert into mtgcard(name) values ('Sauté') on conflict do nothing;
    insert into mtgcard(name) values ('Fraction Jackson') on conflict do nothing;
    insert into mtgcard(name) values ('Who // What // When // Where // Why') on conflict do nothing;
    insert into mtgcard(name) values ('Avatar of Me') on conflict do nothing;
    insert into mtgcard(name) values ('Red-Hot Hottie') on conflict do nothing;
    insert into mtgcard(name) values ('Fat Ass') on conflict do nothing;
    insert into mtgcard(name) values ('Little Girl') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin S.W.A.T. Team') on conflict do nothing;
    insert into mtgcard(name) values ('Tainted Monkey') on conflict do nothing;
    insert into mtgcard(name) values ('Staying Power') on conflict do nothing;
    insert into mtgcard(name) values ('Wet Willie of the Damned') on conflict do nothing;
    insert into mtgcard(name) values ('Symbol Status') on conflict do nothing;
    insert into mtgcard(name) values ('Granny''s Payback') on conflict do nothing;
    insert into mtgcard(name) values ('Man of Measure') on conflict do nothing;
    insert into mtgcard(name) values ('Now I Know My ABC''s') on conflict do nothing;
    insert into mtgcard(name) values ('Zombie Fanboy') on conflict do nothing;
    insert into mtgcard(name) values ('Enter the Dungeon') on conflict do nothing;
    insert into mtgcard(name) values ('Remodel') on conflict do nothing;
    insert into mtgcard(name) values ('Assquatch') on conflict do nothing;
    insert into mtgcard(name) values ('R&D''s Secret Lair') on conflict do nothing;
    insert into mtgcard(name) values ('Name Dropping') on conflict do nothing;
    insert into mtgcard(name) values ('Fascist Art Director') on conflict do nothing;
    insert into mtgcard(name) values ('Side to Side') on conflict do nothing;
    insert into mtgcard(name) values ('Head to Head') on conflict do nothing;
    insert into mtgcard(name) values ('Gluetius Maximus') on conflict do nothing;
    insert into mtgcard(name) values ('Cheatyface') on conflict do nothing;
    insert into mtgcard(name) values ('Old Fogey') on conflict do nothing;
    insert into mtgcard(name) values ('Mouth to Mouth') on conflict do nothing;
    insert into mtgcard(name) values ('Pygmy Giant') on conflict do nothing;
    insert into mtgcard(name) values ('Mox Lotus') on conflict do nothing;
    insert into mtgcard(name) values ('Loose Lips') on conflict do nothing;
    insert into mtgcard(name) values ('The Fallen Apart') on conflict do nothing;
    insert into mtgcard(name) values ('Working Stiff') on conflict do nothing;
    insert into mtgcard(name) values ('First Come, First Served') on conflict do nothing;
    insert into mtgcard(name) values ('Graphic Violence') on conflict do nothing;
    insert into mtgcard(name) values ('Save Life') on conflict do nothing;
    insert into mtgcard(name) values ('Uktabi Kong') on conflict do nothing;
    insert into mtgcard(name) values ('Curse of the Fire Penguin // Curse of the Fire Penguin Creature') on conflict do nothing;
    insert into mtgcard(name) values ('Look at Me, I''m R&D') on conflict do nothing;
    insert into mtgcard(name) values ('Booster Tutor') on conflict do nothing;
    insert into mtgcard(name) values ('Framed!') on conflict do nothing;
    insert into mtgcard(name) values ('Topsy Turvy') on conflict do nothing;
    insert into mtgcard(name) values ('Supersize') on conflict do nothing;
    insert into mtgcard(name) values ('Rod of Spanking') on conflict do nothing;
    insert into mtgcard(name) values ('Stone-Cold Basilisk') on conflict do nothing;
    insert into mtgcard(name) values ('Zzzyxas''s Abyss') on conflict do nothing;
    insert into mtgcard(name) values ('Greater Morphling') on conflict do nothing;
    insert into mtgcard(name) values ('Emcee') on conflict do nothing;
    insert into mtgcard(name) values ('Carnivorous Death-Parrot') on conflict do nothing;
    insert into mtgcard(name) values ('B-I-N-G-O') on conflict do nothing;
    insert into mtgcard(name) values ('Deal Damage') on conflict do nothing;
    insert into mtgcard(name) values ('AWOL') on conflict do nothing;
    insert into mtgcard(name) values ('Stop That') on conflict do nothing;
    insert into mtgcard(name) values ('Spell Counter') on conflict do nothing;
    insert into mtgcard(name) values ('Rocket-Powered Turbo Slug') on conflict do nothing;
    insert into mtgcard(name) values ('Yet Another Aether Vortex') on conflict do nothing;
    insert into mtgcard(name) values ('Necro-Impotence') on conflict do nothing;
    insert into mtgcard(name) values ('Circle of Protection: Art') on conflict do nothing;
    insert into mtgcard(name) values ('Johnny, Combo Player') on conflict do nothing;
    insert into mtgcard(name) values ('Toy Boat') on conflict do nothing;
    insert into mtgcard(name) values ('Erase (Not the Urza''s Legacy One)') on conflict do nothing;
    insert into mtgcard(name) values ('Elvish House Party') on conflict do nothing;
    insert into mtgcard(name) values ('S.N.O.T.') on conflict do nothing;
    insert into mtgcard(name) values ('Bosom Buddy') on conflict do nothing;
    insert into mtgcard(name) values ('Duh') on conflict do nothing;
    insert into mtgcard(name) values ('Ass Whuppin''') on conflict do nothing;
    insert into mtgcard(name) values ('World-Bottling Kit') on conflict do nothing;
