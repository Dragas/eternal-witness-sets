insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Entomb'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Swamp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crystal Vein'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Verdant Force'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Verdant Force'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Polluted Mire'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Swamp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dread Return'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crosis, the Purger'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crosis, the Purger'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crosis, the Purger'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Inkwell Leviathan'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Inkwell Leviathan'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Inkwell Leviathan'),
        (select types.id from types where name = 'Leviathan')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Animate Dead'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Animate Dead'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sickening Dreams'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Reanimate'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Buried Alive'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hidden Horror'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hidden Horror'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Exhume'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Faceless Butcher'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Faceless Butcher'),
        (select types.id from types where name = 'Nightmare')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Faceless Butcher'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Terastodon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Terastodon'),
        (select types.id from types where name = 'Elephant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ebon Stronghold'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Blazing Archon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Blazing Archon'),
        (select types.id from types where name = 'Archon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Duress'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cabal Therapy'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sphinx of the Steel Wind'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sphinx of the Steel Wind'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sphinx of the Steel Wind'),
        (select types.id from types where name = 'Sphinx')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Swamp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Avatar of Woe'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Avatar of Woe'),
        (select types.id from types where name = 'Avatar')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Last Rites'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie Infestation'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Putrid Imp'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Putrid Imp'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Putrid Imp'),
        (select types.id from types where name = 'Imp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Diabolic Servitude'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Twisted Abomination'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Twisted Abomination'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Twisted Abomination'),
        (select types.id from types where name = 'Mutant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Swamp')
    ) 
 on conflict do nothing;
