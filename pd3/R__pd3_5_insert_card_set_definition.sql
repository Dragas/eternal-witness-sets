insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Entomb'),
    (select id from sets where short_name = 'pd3'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pd3'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crystal Vein'),
    (select id from sets where short_name = 'pd3'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Verdant Force'),
    (select id from sets where short_name = 'pd3'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Polluted Mire'),
    (select id from sets where short_name = 'pd3'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pd3'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dread Return'),
    (select id from sets where short_name = 'pd3'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crosis, the Purger'),
    (select id from sets where short_name = 'pd3'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inkwell Leviathan'),
    (select id from sets where short_name = 'pd3'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animate Dead'),
    (select id from sets where short_name = 'pd3'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sickening Dreams'),
    (select id from sets where short_name = 'pd3'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reanimate'),
    (select id from sets where short_name = 'pd3'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Buried Alive'),
    (select id from sets where short_name = 'pd3'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hidden Horror'),
    (select id from sets where short_name = 'pd3'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Exhume'),
    (select id from sets where short_name = 'pd3'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faceless Butcher'),
    (select id from sets where short_name = 'pd3'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terastodon'),
    (select id from sets where short_name = 'pd3'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ebon Stronghold'),
    (select id from sets where short_name = 'pd3'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blazing Archon'),
    (select id from sets where short_name = 'pd3'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'pd3'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cabal Therapy'),
    (select id from sets where short_name = 'pd3'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sphinx of the Steel Wind'),
    (select id from sets where short_name = 'pd3'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pd3'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avatar of Woe'),
    (select id from sets where short_name = 'pd3'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Last Rites'),
    (select id from sets where short_name = 'pd3'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Infestation'),
    (select id from sets where short_name = 'pd3'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Putrid Imp'),
    (select id from sets where short_name = 'pd3'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diabolic Servitude'),
    (select id from sets where short_name = 'pd3'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Twisted Abomination'),
    (select id from sets where short_name = 'pd3'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pd3'),
    '27',
    'common'
) 
 on conflict do nothing;
