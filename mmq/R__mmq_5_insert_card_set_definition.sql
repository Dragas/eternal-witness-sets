insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Saprazzan Skerry'),
    (select id from sets where short_name = 'mmq'),
    '328',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mercadian Atlas'),
    (select id from sets where short_name = 'mmq'),
    '305',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tiger Claws'),
    (select id from sets where short_name = 'mmq'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Henge Guardian'),
    (select id from sets where short_name = 'mmq'),
    '297',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pangosaur'),
    (select id from sets where short_name = 'mmq'),
    '261',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Puppet''s Verdict'),
    (select id from sets where short_name = 'mmq'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Henge of Ramos'),
    (select id from sets where short_name = 'mmq'),
    '318',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chameleon Spirit'),
    (select id from sets where short_name = 'mmq'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Market'),
    (select id from sets where short_name = 'mmq'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snake Pit'),
    (select id from sets where short_name = 'mmq'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Customs Depot'),
    (select id from sets where short_name = 'mmq'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karn''s Touch'),
    (select id from sets where short_name = 'mmq'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Monkey Cage'),
    (select id from sets where short_name = 'mmq'),
    '307',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Charm Peddler'),
    (select id from sets where short_name = 'mmq'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightwind Glider'),
    (select id from sets where short_name = 'mmq'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lava Runner'),
    (select id from sets where short_name = 'mmq'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liability'),
    (select id from sets where short_name = 'mmq'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wave of Reckoning'),
    (select id from sets where short_name = 'mmq'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kyren Glider'),
    (select id from sets where short_name = 'mmq'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kyren Negotiations'),
    (select id from sets where short_name = 'mmq'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cornered Market'),
    (select id from sets where short_name = 'mmq'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saprazzan Heir'),
    (select id from sets where short_name = 'mmq'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Midnight Ritual'),
    (select id from sets where short_name = 'mmq'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ramosian Captain'),
    (select id from sets where short_name = 'mmq'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'mmq'),
    '341',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scandalmonger'),
    (select id from sets where short_name = 'mmq'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rappelling Scouts'),
    (select id from sets where short_name = 'mmq'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Remote Farm'),
    (select id from sets where short_name = 'mmq'),
    '323',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancestral Mask'),
    (select id from sets where short_name = 'mmq'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sailmonger'),
    (select id from sets where short_name = 'mmq'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cho-Arrim Legate'),
    (select id from sets where short_name = 'mmq'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mercadia''s Downfall'),
    (select id from sets where short_name = 'mmq'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overtaker'),
    (select id from sets where short_name = 'mmq'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ramosian Sky Marshal'),
    (select id from sets where short_name = 'mmq'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Assembly Hall'),
    (select id from sets where short_name = 'mmq'),
    '286',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skulking Fugitive'),
    (select id from sets where short_name = 'mmq'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coastal Piracy'),
    (select id from sets where short_name = 'mmq'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Task Force'),
    (select id from sets where short_name = 'mmq'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flaming Sword'),
    (select id from sets where short_name = 'mmq'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ogre Taskmaster'),
    (select id from sets where short_name = 'mmq'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Food Chain'),
    (select id from sets where short_name = 'mmq'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snuff Out'),
    (select id from sets where short_name = 'mmq'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'mmq'),
    '345',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'mmq'),
    '343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Credit Voucher'),
    (select id from sets where short_name = 'mmq'),
    '289',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Close Quarters'),
    (select id from sets where short_name = 'mmq'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Intimidation'),
    (select id from sets where short_name = 'mmq'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Game Preserve'),
    (select id from sets where short_name = 'mmq'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armistice'),
    (select id from sets where short_name = 'mmq'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moonlit Wake'),
    (select id from sets where short_name = 'mmq'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crag Saurian'),
    (select id from sets where short_name = 'mmq'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strongarm Thug'),
    (select id from sets where short_name = 'mmq'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nether Spirit'),
    (select id from sets where short_name = 'mmq'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Notorious Assassin'),
    (select id from sets where short_name = 'mmq'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cho-Arrim Bruiser'),
    (select id from sets where short_name = 'mmq'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Furious Assault'),
    (select id from sets where short_name = 'mmq'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deepwood Ghoul'),
    (select id from sets where short_name = 'mmq'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = 'mmq'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rishadan Pawnshop'),
    (select id from sets where short_name = 'mmq'),
    '311',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dehydration'),
    (select id from sets where short_name = 'mmq'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pretender''s Claim'),
    (select id from sets where short_name = 'mmq'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dawnstrider'),
    (select id from sets where short_name = 'mmq'),
    '237',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cateran Slaver'),
    (select id from sets where short_name = 'mmq'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cave Sense'),
    (select id from sets where short_name = 'mmq'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unmask'),
    (select id from sets where short_name = 'mmq'),
    '168',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saprazzan Breaker'),
    (select id from sets where short_name = 'mmq'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Instigator'),
    (select id from sets where short_name = 'mmq'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'mmq'),
    '331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magistrate''s Veto'),
    (select id from sets where short_name = 'mmq'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ballista Squad'),
    (select id from sets where short_name = 'mmq'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Groundskeeper'),
    (select id from sets where short_name = 'mmq'),
    '250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tonic Peddler'),
    (select id from sets where short_name = 'mmq'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tidal Kraken'),
    (select id from sets where short_name = 'mmq'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ivory Mask'),
    (select id from sets where short_name = 'mmq'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'mmq'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forced March'),
    (select id from sets where short_name = 'mmq'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cateran Kidnappers'),
    (select id from sets where short_name = 'mmq'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battle Squadron'),
    (select id from sets where short_name = 'mmq'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Hounds'),
    (select id from sets where short_name = 'mmq'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mercadian Lift'),
    (select id from sets where short_name = 'mmq'),
    '306',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unnatural Hunger'),
    (select id from sets where short_name = 'mmq'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Larceny'),
    (select id from sets where short_name = 'mmq'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'mmq'),
    '334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arrest'),
    (select id from sets where short_name = 'mmq'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Last Breath'),
    (select id from sets where short_name = 'mmq'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vernal Equinox'),
    (select id from sets where short_name = 'mmq'),
    '283',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Haunted Crossroads'),
    (select id from sets where short_name = 'mmq'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volcanic Wind'),
    (select id from sets where short_name = 'mmq'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quagmire Lamprey'),
    (select id from sets where short_name = 'mmq'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cloud Sprite'),
    (select id from sets where short_name = 'mmq'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cho-Manno, Revolutionary'),
    (select id from sets where short_name = 'mmq'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brawl'),
    (select id from sets where short_name = 'mmq'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moment of Silence'),
    (select id from sets where short_name = 'mmq'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Embargo'),
    (select id from sets where short_name = 'mmq'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Worry Beads'),
    (select id from sets where short_name = 'mmq'),
    '315',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Invigorate'),
    (select id from sets where short_name = 'mmq'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Two-Headed Dragon'),
    (select id from sets where short_name = 'mmq'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conspiracy'),
    (select id from sets where short_name = 'mmq'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fountain of Cho'),
    (select id from sets where short_name = 'mmq'),
    '317',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Iron Lance'),
    (select id from sets where short_name = 'mmq'),
    '300',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'mmq'),
    '346',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cho-Arrim Alchemist'),
    (select id from sets where short_name = 'mmq'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kris Mage'),
    (select id from sets where short_name = 'mmq'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darting Merfolk'),
    (select id from sets where short_name = 'mmq'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cave-In'),
    (select id from sets where short_name = 'mmq'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'mmq'),
    '332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadly Insect'),
    (select id from sets where short_name = 'mmq'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rishadan Port'),
    (select id from sets where short_name = 'mmq'),
    '324',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Venomous Breath'),
    (select id from sets where short_name = 'mmq'),
    '281',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Venomous Dragonfly'),
    (select id from sets where short_name = 'mmq'),
    '282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reverent Mantra'),
    (select id from sets where short_name = 'mmq'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'False Demise'),
    (select id from sets where short_name = 'mmq'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Devout Witness'),
    (select id from sets where short_name = 'mmq'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rishadan Footpad'),
    (select id from sets where short_name = 'mmq'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'mmq'),
    '342',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ceremonial Guard'),
    (select id from sets where short_name = 'mmq'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ferocity'),
    (select id from sets where short_name = 'mmq'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squee, Goblin Nabob'),
    (select id from sets where short_name = 'mmq'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'mmq'),
    '344',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kyren Sniper'),
    (select id from sets where short_name = 'mmq'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sand Squid'),
    (select id from sets where short_name = 'mmq'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Territorial Dispute'),
    (select id from sets where short_name = 'mmq'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rushwood Elemental'),
    (select id from sets where short_name = 'mmq'),
    '264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saprazzan Cove'),
    (select id from sets where short_name = 'mmq'),
    '327',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deepwood Legate'),
    (select id from sets where short_name = 'mmq'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rushwood Dryad'),
    (select id from sets where short_name = 'mmq'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warmonger'),
    (select id from sets where short_name = 'mmq'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Channeling'),
    (select id from sets where short_name = 'mmq'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Misdirection'),
    (select id from sets where short_name = 'mmq'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Caustic Wasps'),
    (select id from sets where short_name = 'mmq'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blockade Runner'),
    (select id from sets where short_name = 'mmq'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wishmonger'),
    (select id from sets where short_name = 'mmq'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sustenance'),
    (select id from sets where short_name = 'mmq'),
    '278',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Security Detail'),
    (select id from sets where short_name = 'mmq'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'mmq'),
    '335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cho-Manno''s Blessing'),
    (select id from sets where short_name = 'mmq'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corrupt Official'),
    (select id from sets where short_name = 'mmq'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primeval Shambler'),
    (select id from sets where short_name = 'mmq'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lumbering Satyr'),
    (select id from sets where short_name = 'mmq'),
    '257',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rishadan Airship'),
    (select id from sets where short_name = 'mmq'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kyren Archive'),
    (select id from sets where short_name = 'mmq'),
    '302',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saprazzan Raider'),
    (select id from sets where short_name = 'mmq'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charisma'),
    (select id from sets where short_name = 'mmq'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steadfast Guard'),
    (select id from sets where short_name = 'mmq'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Jhovall'),
    (select id from sets where short_name = 'mmq'),
    '227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drake Hatchling'),
    (select id from sets where short_name = 'mmq'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Putrefaction'),
    (select id from sets where short_name = 'mmq'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Charmed Griffin'),
    (select id from sets where short_name = 'mmq'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Righteous Aura'),
    (select id from sets where short_name = 'mmq'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Story Circle'),
    (select id from sets where short_name = 'mmq'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ignoble Soldier'),
    (select id from sets where short_name = 'mmq'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vendetta'),
    (select id from sets where short_name = 'mmq'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fountain Watch'),
    (select id from sets where short_name = 'mmq'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'mmq'),
    '347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Natural Affinity'),
    (select id from sets where short_name = 'mmq'),
    '260',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghoul''s Feast'),
    (select id from sets where short_name = 'mmq'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bog Witch'),
    (select id from sets where short_name = 'mmq'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maggot Therapy'),
    (select id from sets where short_name = 'mmq'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horn of Ramos'),
    (select id from sets where short_name = 'mmq'),
    '299',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flailing Manticore'),
    (select id from sets where short_name = 'mmq'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kyren Legate'),
    (select id from sets where short_name = 'mmq'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mercadian Bazaar'),
    (select id from sets where short_name = 'mmq'),
    '321',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Honor the Fallen'),
    (select id from sets where short_name = 'mmq'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glowing Anemone'),
    (select id from sets where short_name = 'mmq'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'mmq'),
    '339',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cateran Brute'),
    (select id from sets where short_name = 'mmq'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Uphill Battle'),
    (select id from sets where short_name = 'mmq'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Howling Wolf'),
    (select id from sets where short_name = 'mmq'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crossbow Infantry'),
    (select id from sets where short_name = 'mmq'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orim''s Cure'),
    (select id from sets where short_name = 'mmq'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dust Bowl'),
    (select id from sets where short_name = 'mmq'),
    '316',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Statecraft'),
    (select id from sets where short_name = 'mmq'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Toymaker'),
    (select id from sets where short_name = 'mmq'),
    '314',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eye of Ramos'),
    (select id from sets where short_name = 'mmq'),
    '294',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crash'),
    (select id from sets where short_name = 'mmq'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kyren Toy'),
    (select id from sets where short_name = 'mmq'),
    '303',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Revive'),
    (select id from sets where short_name = 'mmq'),
    '262',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'mmq'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desert Twister'),
    (select id from sets where short_name = 'mmq'),
    '243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Robber Fly'),
    (select id from sets where short_name = 'mmq'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jhovall Rider'),
    (select id from sets where short_name = 'mmq'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Horned Troll'),
    (select id from sets where short_name = 'mmq'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ramosian Rally'),
    (select id from sets where short_name = 'mmq'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blaster Mage'),
    (select id from sets where short_name = 'mmq'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cinder Elemental'),
    (select id from sets where short_name = 'mmq'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rushwood Legate'),
    (select id from sets where short_name = 'mmq'),
    '266',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sandstone Needle'),
    (select id from sets where short_name = 'mmq'),
    '326',
    'common'
) ,
(
    (select id from mtgcard where name = 'Puffer Extract'),
    (select id from sets where short_name = 'mmq'),
    '310',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquility'),
    (select id from sets where short_name = 'mmq'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spiritual Focus'),
    (select id from sets where short_name = 'mmq'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tremor'),
    (select id from sets where short_name = 'mmq'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rain of Tears'),
    (select id from sets where short_name = 'mmq'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bargaining Table'),
    (select id from sets where short_name = 'mmq'),
    '288',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chambered Nautilus'),
    (select id from sets where short_name = 'mmq'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ramosian Commander'),
    (select id from sets where short_name = 'mmq'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thrashing Wumpus'),
    (select id from sets where short_name = 'mmq'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Panacea'),
    (select id from sets where short_name = 'mmq'),
    '308',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lure'),
    (select id from sets where short_name = 'mmq'),
    '258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gerrard''s Irregulars'),
    (select id from sets where short_name = 'mmq'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Muzzle'),
    (select id from sets where short_name = 'mmq'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampart Crawler'),
    (select id from sets where short_name = 'mmq'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Renounce'),
    (select id from sets where short_name = 'mmq'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hoodwink'),
    (select id from sets where short_name = 'mmq'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Waterfront Bouncer'),
    (select id from sets where short_name = 'mmq'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Misstep'),
    (select id from sets where short_name = 'mmq'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Afterlife'),
    (select id from sets where short_name = 'mmq'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Collective Unconscious'),
    (select id from sets where short_name = 'mmq'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Timid Drake'),
    (select id from sets where short_name = 'mmq'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diplomatic Escort'),
    (select id from sets where short_name = 'mmq'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rishadan Cutpurse'),
    (select id from sets where short_name = 'mmq'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seismic Mage'),
    (select id from sets where short_name = 'mmq'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soothing Balm'),
    (select id from sets where short_name = 'mmq'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'mmq'),
    '350',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lithophage'),
    (select id from sets where short_name = 'mmq'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Extravagant Spirit'),
    (select id from sets where short_name = 'mmq'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'mmq'),
    '336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tooth of Ramos'),
    (select id from sets where short_name = 'mmq'),
    '313',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rushwood Herbalist'),
    (select id from sets where short_name = 'mmq'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silverglade Pathfinder'),
    (select id from sets where short_name = 'mmq'),
    '270',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alabaster Wall'),
    (select id from sets where short_name = 'mmq'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tectonic Break'),
    (select id from sets where short_name = 'mmq'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cateran Enforcer'),
    (select id from sets where short_name = 'mmq'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spidersilk Armor'),
    (select id from sets where short_name = 'mmq'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inviolability'),
    (select id from sets where short_name = 'mmq'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'mmq'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heart of Ramos'),
    (select id from sets where short_name = 'mmq'),
    '296',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'mmq'),
    '348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Misshapen Fiend'),
    (select id from sets where short_name = 'mmq'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Matrix'),
    (select id from sets where short_name = 'mmq'),
    '309',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Extortion'),
    (select id from sets where short_name = 'mmq'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'mmq'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rishadan Brigand'),
    (select id from sets where short_name = 'mmq'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crackdown'),
    (select id from sets where short_name = 'mmq'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Energy Flux'),
    (select id from sets where short_name = 'mmq'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cateran Persuader'),
    (select id from sets where short_name = 'mmq'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saprazzan Bailiff'),
    (select id from sets where short_name = 'mmq'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shoving Match'),
    (select id from sets where short_name = 'mmq'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Distorting Lens'),
    (select id from sets where short_name = 'mmq'),
    '293',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deepwood Drummer'),
    (select id from sets where short_name = 'mmq'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rouse'),
    (select id from sets where short_name = 'mmq'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Noble Purpose'),
    (select id from sets where short_name = 'mmq'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aerial Caravan'),
    (select id from sets where short_name = 'mmq'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cateran Summons'),
    (select id from sets where short_name = 'mmq'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hickory Woodlot'),
    (select id from sets where short_name = 'mmq'),
    '319',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pulverize'),
    (select id from sets where short_name = 'mmq'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Revered Elder'),
    (select id from sets where short_name = 'mmq'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Buoyancy'),
    (select id from sets where short_name = 'mmq'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunted Wumpus'),
    (select id from sets where short_name = 'mmq'),
    '253',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Saprazzan Outrigger'),
    (select id from sets where short_name = 'mmq'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'mmq'),
    '337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunderclap'),
    (select id from sets where short_name = 'mmq'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thieves'' Auction'),
    (select id from sets where short_name = 'mmq'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Briar Patch'),
    (select id from sets where short_name = 'mmq'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clear the Land'),
    (select id from sets where short_name = 'mmq'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jhovall Queen'),
    (select id from sets where short_name = 'mmq'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hammer Mage'),
    (select id from sets where short_name = 'mmq'),
    '193',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silent Assassin'),
    (select id from sets where short_name = 'mmq'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Hound'),
    (select id from sets where short_name = 'mmq'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Squeeze'),
    (select id from sets where short_name = 'mmq'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pious Warrior'),
    (select id from sets where short_name = 'mmq'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Peat Bog'),
    (select id from sets where short_name = 'mmq'),
    '322',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cavern Crawler'),
    (select id from sets where short_name = 'mmq'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Caterpillar'),
    (select id from sets where short_name = 'mmq'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foster'),
    (select id from sets where short_name = 'mmq'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barbed Wire'),
    (select id from sets where short_name = 'mmq'),
    '287',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deepwood Tantiv'),
    (select id from sets where short_name = 'mmq'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Caller of the Hunt'),
    (select id from sets where short_name = 'mmq'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thwart'),
    (select id from sets where short_name = 'mmq'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'mmq'),
    '349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Righteous Indignation'),
    (select id from sets where short_name = 'mmq'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bog Smugglers'),
    (select id from sets where short_name = 'mmq'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'mmq'),
    '340',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sever Soul'),
    (select id from sets where short_name = 'mmq'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rushwood Grove'),
    (select id from sets where short_name = 'mmq'),
    '325',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balloon Peddler'),
    (select id from sets where short_name = 'mmq'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Word of Blasting'),
    (select id from sets where short_name = 'mmq'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Megatherium'),
    (select id from sets where short_name = 'mmq'),
    '259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Squallmonger'),
    (select id from sets where short_name = 'mmq'),
    '276',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stinging Barrier'),
    (select id from sets where short_name = 'mmq'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spontaneous Generation'),
    (select id from sets where short_name = 'mmq'),
    '274',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cackling Witch'),
    (select id from sets where short_name = 'mmq'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alley Grifters'),
    (select id from sets where short_name = 'mmq'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathgazer'),
    (select id from sets where short_name = 'mmq'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Specter''s Wail'),
    (select id from sets where short_name = 'mmq'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insubordination'),
    (select id from sets where short_name = 'mmq'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deepwood Elder'),
    (select id from sets where short_name = 'mmq'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Distortion'),
    (select id from sets where short_name = 'mmq'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cowardice'),
    (select id from sets where short_name = 'mmq'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magistrate''s Scepter'),
    (select id from sets where short_name = 'mmq'),
    '304',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Horn of Plenty'),
    (select id from sets where short_name = 'mmq'),
    '298',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Land Grant'),
    (select id from sets where short_name = 'mmq'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crenellated Wall'),
    (select id from sets where short_name = 'mmq'),
    '290',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crumbling Sanctuary'),
    (select id from sets where short_name = 'mmq'),
    '292',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Delraich'),
    (select id from sets where short_name = 'mmq'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Indentured Djinn'),
    (select id from sets where short_name = 'mmq'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arms Dealer'),
    (select id from sets where short_name = 'mmq'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deepwood Wolverine'),
    (select id from sets where short_name = 'mmq'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saprazzan Legate'),
    (select id from sets where short_name = 'mmq'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hired Giant'),
    (select id from sets where short_name = 'mmq'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Molting Harpy'),
    (select id from sets where short_name = 'mmq'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ramosian Lieutenant'),
    (select id from sets where short_name = 'mmq'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bifurcate'),
    (select id from sets where short_name = 'mmq'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soothsaying'),
    (select id from sets where short_name = 'mmq'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bribery'),
    (select id from sets where short_name = 'mmq'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rock Badger'),
    (select id from sets where short_name = 'mmq'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enslaved Horror'),
    (select id from sets where short_name = 'mmq'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'War Cadence'),
    (select id from sets where short_name = 'mmq'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warpath'),
    (select id from sets where short_name = 'mmq'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fresh Volunteers'),
    (select id from sets where short_name = 'mmq'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'mmq'),
    '338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'mmq'),
    '333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flailing Ogre'),
    (select id from sets where short_name = 'mmq'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Squall'),
    (select id from sets where short_name = 'mmq'),
    '275',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snorting Gahr'),
    (select id from sets where short_name = 'mmq'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vine Trellis'),
    (select id from sets where short_name = 'mmq'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gush'),
    (select id from sets where short_name = 'mmq'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shock Troops'),
    (select id from sets where short_name = 'mmq'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'War Tax'),
    (select id from sets where short_name = 'mmq'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cateran Overlord'),
    (select id from sets where short_name = 'mmq'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saber Ants'),
    (select id from sets where short_name = 'mmq'),
    '267',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sacred Prey'),
    (select id from sets where short_name = 'mmq'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lunge'),
    (select id from sets where short_name = 'mmq'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jeweled Torque'),
    (select id from sets where short_name = 'mmq'),
    '301',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Common Cause'),
    (select id from sets where short_name = 'mmq'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trap Runner'),
    (select id from sets where short_name = 'mmq'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Erithizon'),
    (select id from sets where short_name = 'mmq'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stamina'),
    (select id from sets where short_name = 'mmq'),
    '277',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Subterranean Hangar'),
    (select id from sets where short_name = 'mmq'),
    '329',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tower of the Magistrate'),
    (select id from sets where short_name = 'mmq'),
    '330',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trade Routes'),
    (select id from sets where short_name = 'mmq'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Port Inspector'),
    (select id from sets where short_name = 'mmq'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ramosian Sergeant'),
    (select id from sets where short_name = 'mmq'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skull of Ramos'),
    (select id from sets where short_name = 'mmq'),
    '312',
    'rare'
) ,
(
    (select id from mtgcard where name = 'High Seas'),
    (select id from sets where short_name = 'mmq'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blood Oath'),
    (select id from sets where short_name = 'mmq'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sizzle'),
    (select id from sets where short_name = 'mmq'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silverglade Elemental'),
    (select id from sets where short_name = 'mmq'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battle Rampart'),
    (select id from sets where short_name = 'mmq'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'High Market'),
    (select id from sets where short_name = 'mmq'),
    '320',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Undertaker'),
    (select id from sets where short_name = 'mmq'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tidal Bore'),
    (select id from sets where short_name = 'mmq'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boa Constrictor'),
    (select id from sets where short_name = 'mmq'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ley Line'),
    (select id from sets where short_name = 'mmq'),
    '256',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diplomatic Immunity'),
    (select id from sets where short_name = 'mmq'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flailing Soldier'),
    (select id from sets where short_name = 'mmq'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'General''s Regalia'),
    (select id from sets where short_name = 'mmq'),
    '295',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Highway Robber'),
    (select id from sets where short_name = 'mmq'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vine Dryad'),
    (select id from sets where short_name = 'mmq'),
    '284',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crooked Scales'),
    (select id from sets where short_name = 'mmq'),
    '291',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thermal Glider'),
    (select id from sets where short_name = 'mmq'),
    '53',
    'common'
) 
 on conflict do nothing;
