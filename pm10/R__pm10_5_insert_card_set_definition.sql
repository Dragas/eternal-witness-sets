insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Naya Sojourners'),
    (select id from sets where short_name = 'pm10'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampire Nocturnus'),
    (select id from sets where short_name = 'pm10'),
    '*118',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mycoid Shepherd'),
    (select id from sets where short_name = 'pm10'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ant Queen'),
    (select id from sets where short_name = 'pm10'),
    '*166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Honor of the Pure'),
    (select id from sets where short_name = 'pm10'),
    '*16',
    'rare'
) 
 on conflict do nothing;
