insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Grisly Salvage'),
    (select id from sets where short_name = 'f13'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Cackler'),
    (select id from sets where short_name = 'f13'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sin Collector'),
    (select id from sets where short_name = 'f13'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reliquary Tower'),
    (select id from sets where short_name = 'f13'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghor-Clan Rampager'),
    (select id from sets where short_name = 'f13'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Call of the Conclave'),
    (select id from sets where short_name = 'f13'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Farseek'),
    (select id from sets where short_name = 'f13'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dimir Charm'),
    (select id from sets where short_name = 'f13'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Judge''s Familiar'),
    (select id from sets where short_name = 'f13'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Experiment One'),
    (select id from sets where short_name = 'f13'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Searing Spear'),
    (select id from sets where short_name = 'f13'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Izzet Charm'),
    (select id from sets where short_name = 'f13'),
    '6',
    'rare'
) 
 on conflict do nothing;
