insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Jaya Ballard'),
    (select id from sets where short_name = 'pdom'),
    '132s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sulfur Falls'),
    (select id from sets where short_name = 'pdom'),
    '247p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Isolated Chapel'),
    (select id from sets where short_name = 'pdom'),
    '241p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mox Amber'),
    (select id from sets where short_name = 'pdom'),
    '224s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goblin Chainwhirler'),
    (select id from sets where short_name = 'pdom'),
    '129s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yargle, Glutton of Urborg'),
    (select id from sets where short_name = 'pdom'),
    '113s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steel Leaf Champion'),
    (select id from sets where short_name = 'pdom'),
    '182p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gilded Lotus'),
    (select id from sets where short_name = 'pdom'),
    '215s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hallar, the Firefletcher'),
    (select id from sets where short_name = 'pdom'),
    '196s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teshar, Ancestor''s Apostle'),
    (select id from sets where short_name = 'pdom'),
    '36s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Helm of the Host'),
    (select id from sets where short_name = 'pdom'),
    '217p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Traxos, Scourge of Kroog'),
    (select id from sets where short_name = 'pdom'),
    '234s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jhoira, Weatherlight Captain'),
    (select id from sets where short_name = 'pdom'),
    '197s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Haphazard Bombardment'),
    (select id from sets where short_name = 'pdom'),
    '131s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kamahl''s Druidic Vow'),
    (select id from sets where short_name = 'pdom'),
    '166s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Adeliz, the Cinder Wind'),
    (select id from sets where short_name = 'pdom'),
    '190s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Scriptures'),
    (select id from sets where short_name = 'pdom'),
    '100s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'pdom'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shanna, Sisay''s Legacy'),
    (select id from sets where short_name = 'pdom'),
    '204s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steel Leaf Champion'),
    (select id from sets where short_name = 'pdom'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shanna, Sisay''s Legacy'),
    (select id from sets where short_name = 'pdom'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tetsuko Umezawa, Fugitive'),
    (select id from sets where short_name = 'pdom'),
    '69s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Danitha Capashen, Paragon'),
    (select id from sets where short_name = 'pdom'),
    '12s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clifftop Retreat'),
    (select id from sets where short_name = 'pdom'),
    '239p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lich''s Mastery'),
    (select id from sets where short_name = 'pdom'),
    '98s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cabal Stronghold'),
    (select id from sets where short_name = 'pdom'),
    '238p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Siege-Gang Commander'),
    (select id from sets where short_name = 'pdom'),
    '143s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Multani, Yavimaya''s Avatar'),
    (select id from sets where short_name = 'pdom'),
    '174s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Karn''s Temporal Sundering'),
    (select id from sets where short_name = 'pdom'),
    '55s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yawgmoth''s Vile Offering'),
    (select id from sets where short_name = 'pdom'),
    '114s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The First Eruption'),
    (select id from sets where short_name = 'pdom'),
    '122s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slinn Voda, the Rising Deep'),
    (select id from sets where short_name = 'pdom'),
    '66s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Siege-Gang Commander'),
    (select id from sets where short_name = 'pdom'),
    '143p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sulfur Falls'),
    (select id from sets where short_name = 'pdom'),
    '247s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tempest Djinn'),
    (select id from sets where short_name = 'pdom'),
    '68p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Two-Headed Giant'),
    (select id from sets where short_name = 'pdom'),
    '147s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Self-Replicator'),
    (select id from sets where short_name = 'pdom'),
    '223s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verix Bladewing'),
    (select id from sets where short_name = 'pdom'),
    '149s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lyra Dawnbringer'),
    (select id from sets where short_name = 'pdom'),
    '26s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jaya''s Immolating Inferno'),
    (select id from sets where short_name = 'pdom'),
    '133s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kwende, Pride of Femeref'),
    (select id from sets where short_name = 'pdom'),
    '25s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steel Leaf Champion'),
    (select id from sets where short_name = 'pdom'),
    '182s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hinterland Harbor'),
    (select id from sets where short_name = 'pdom'),
    '240p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonlord Belzenlok'),
    (select id from sets where short_name = 'pdom'),
    '86s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Helm of the Host'),
    (select id from sets where short_name = 'pdom'),
    '217s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zahid, Djinn of the Lamp'),
    (select id from sets where short_name = 'pdom'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Garna, the Bloodflame'),
    (select id from sets where short_name = 'pdom'),
    '194s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zhalfirin Void'),
    (select id from sets where short_name = 'pdom'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Precognition Field'),
    (select id from sets where short_name = 'pdom'),
    '61s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marwyn, the Nurturer'),
    (select id from sets where short_name = 'pdom'),
    '172p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forebear''s Blade'),
    (select id from sets where short_name = 'pdom'),
    '214p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rona, Disciple of Gix'),
    (select id from sets where short_name = 'pdom'),
    '203s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karn, Scion of Urza'),
    (select id from sets where short_name = 'pdom'),
    '1s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Clifftop Retreat'),
    (select id from sets where short_name = 'pdom'),
    '239s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oath of Teferi'),
    (select id from sets where short_name = 'pdom'),
    '200s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verdant Force'),
    (select id from sets where short_name = 'pdom'),
    '187s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karn, Scion of Urza'),
    (select id from sets where short_name = 'pdom'),
    '1p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goblin Chainwhirler'),
    (select id from sets where short_name = 'pdom'),
    '129p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Baird, Steward of Argive'),
    (select id from sets where short_name = 'pdom'),
    '4s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marwyn, the Nurturer'),
    (select id from sets where short_name = 'pdom'),
    '172s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grand Warlord Radha'),
    (select id from sets where short_name = 'pdom'),
    '195s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zahid, Djinn of the Lamp'),
    (select id from sets where short_name = 'pdom'),
    '76s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Mirari Conjecture'),
    (select id from sets where short_name = 'pdom'),
    '57s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tatyova, Benthic Druid'),
    (select id from sets where short_name = 'pdom'),
    '206s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daring Archaeologist'),
    (select id from sets where short_name = 'pdom'),
    '13s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urgoros, the Empty One'),
    (select id from sets where short_name = 'pdom'),
    '109s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'History of Benalia'),
    (select id from sets where short_name = 'pdom'),
    '21p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Primevals'' Glorious Rebirth'),
    (select id from sets where short_name = 'pdom'),
    '201s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benalish Marshal'),
    (select id from sets where short_name = 'pdom'),
    '6p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kazarov, Sengir Pureblood'),
    (select id from sets where short_name = 'pdom'),
    '96s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jodah, Archmage Eternal'),
    (select id from sets where short_name = 'pdom'),
    '198s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Antiquities War'),
    (select id from sets where short_name = 'pdom'),
    '42p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Muldrotha, the Gravetide'),
    (select id from sets where short_name = 'pdom'),
    '199s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Weatherlight'),
    (select id from sets where short_name = 'pdom'),
    '237s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tempest Djinn'),
    (select id from sets where short_name = 'pdom'),
    '68s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Naban, Dean of Iteration'),
    (select id from sets where short_name = 'pdom'),
    '58s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evra, Halcyon Witness'),
    (select id from sets where short_name = 'pdom'),
    '16s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Mending of Dominaria'),
    (select id from sets where short_name = 'pdom'),
    '173s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cast Down'),
    (select id from sets where short_name = 'pdom'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teferi, Hero of Dominaria'),
    (select id from sets where short_name = 'pdom'),
    '207s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thran Temporal Gateway'),
    (select id from sets where short_name = 'pdom'),
    '233s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woodland Cemetery'),
    (select id from sets where short_name = 'pdom'),
    '248p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lyra Dawnbringer'),
    (select id from sets where short_name = 'pdom'),
    '26p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Slimefoot, the Stowaway'),
    (select id from sets where short_name = 'pdom'),
    '205s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Squee, the Immortal'),
    (select id from sets where short_name = 'pdom'),
    '146s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Antiquities War'),
    (select id from sets where short_name = 'pdom'),
    '42s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arvad the Cursed'),
    (select id from sets where short_name = 'pdom'),
    '191s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Opt'),
    (select id from sets where short_name = 'pdom'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forebear''s Blade'),
    (select id from sets where short_name = 'pdom'),
    '214s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gilded Lotus'),
    (select id from sets where short_name = 'pdom'),
    '215p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'History of Benalia'),
    (select id from sets where short_name = 'pdom'),
    '21s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Valduk, Keeper of the Flame'),
    (select id from sets where short_name = 'pdom'),
    '148s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shalai, Voice of Plenty'),
    (select id from sets where short_name = 'pdom'),
    '35s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whisper, Blood Liturgist'),
    (select id from sets where short_name = 'pdom'),
    '111s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blackblade Reforged'),
    (select id from sets where short_name = 'pdom'),
    '211s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dread Shade'),
    (select id from sets where short_name = 'pdom'),
    '88s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fall of the Thran'),
    (select id from sets where short_name = 'pdom'),
    '18s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Territorial Allosaurus'),
    (select id from sets where short_name = 'pdom'),
    '184s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benalish Marshal'),
    (select id from sets where short_name = 'pdom'),
    '6s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Awakening'),
    (select id from sets where short_name = 'pdom'),
    '183s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Naru Meha, Master Wizard'),
    (select id from sets where short_name = 'pdom'),
    '59s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Isolated Chapel'),
    (select id from sets where short_name = 'pdom'),
    '241s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shalai, Voice of Plenty'),
    (select id from sets where short_name = 'pdom'),
    '35p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Ruinous Blast'),
    (select id from sets where short_name = 'pdom'),
    '39s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Torgaar, Famine Incarnate'),
    (select id from sets where short_name = 'pdom'),
    '108s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rite of Belzenlok'),
    (select id from sets where short_name = 'pdom'),
    '102s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Josu Vess, Lich Knight'),
    (select id from sets where short_name = 'pdom'),
    '95s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darigaaz Reincarnated'),
    (select id from sets where short_name = 'pdom'),
    '193s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hinterland Harbor'),
    (select id from sets where short_name = 'pdom'),
    '240s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cabal Stronghold'),
    (select id from sets where short_name = 'pdom'),
    '238s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'pdom'),
    '33c',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aryel, Knight of Windgrace'),
    (select id from sets where short_name = 'pdom'),
    '192s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woodland Cemetery'),
    (select id from sets where short_name = 'pdom'),
    '248s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raff Capashen, Ship''s Mage'),
    (select id from sets where short_name = 'pdom'),
    '202s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Squee, the Immortal'),
    (select id from sets where short_name = 'pdom'),
    '146p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grunn, the Lonely King'),
    (select id from sets where short_name = 'pdom'),
    '165s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blackblade Reforged'),
    (select id from sets where short_name = 'pdom'),
    '211p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tiana, Ship''s Caretaker'),
    (select id from sets where short_name = 'pdom'),
    '208s',
    'uncommon'
) 
 on conflict do nothing;
