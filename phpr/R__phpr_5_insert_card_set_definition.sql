insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Mana Crypt'),
    (select id from sets where short_name = 'phpr'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arena'),
    (select id from sets where short_name = 'phpr'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Badger'),
    (select id from sets where short_name = 'phpr'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sewers of Estark'),
    (select id from sets where short_name = 'phpr'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Windseeker Centaur'),
    (select id from sets where short_name = 'phpr'),
    '3',
    'rare'
) 
 on conflict do nothing;
