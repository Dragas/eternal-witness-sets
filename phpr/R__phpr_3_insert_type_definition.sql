    insert into types (name) values('Artifact') on conflict do nothing;
    insert into types (name) values('Land') on conflict do nothing;
    insert into types (name) values('Creature') on conflict do nothing;
    insert into types (name) values('Badger') on conflict do nothing;
    insert into types (name) values('Instant') on conflict do nothing;
    insert into types (name) values('Creature') on conflict do nothing;
    insert into types (name) values('Centaur') on conflict do nothing;
