    insert into mtgcard(name) values ('Mana Crypt') on conflict do nothing;
    insert into mtgcard(name) values ('Arena') on conflict do nothing;
    insert into mtgcard(name) values ('Giant Badger') on conflict do nothing;
    insert into mtgcard(name) values ('Sewers of Estark') on conflict do nothing;
    insert into mtgcard(name) values ('Windseeker Centaur') on conflict do nothing;
