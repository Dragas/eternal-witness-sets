insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Sliver Legion'),
    (select id from sets where short_name = 'j19'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mox Opal'),
    (select id from sets where short_name = 'j19'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Monastery Mentor'),
    (select id from sets where short_name = 'j19'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Reflecting Pool'),
    (select id from sets where short_name = 'j19'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Isolated Watchtower'),
    (select id from sets where short_name = 'j19'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Yuriko, the Tiger''s Shadow'),
    (select id from sets where short_name = 'j19'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirri''s Guile'),
    (select id from sets where short_name = 'j19'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chalice of the Void'),
    (select id from sets where short_name = 'j19'),
    '7',
    'rare'
) 
 on conflict do nothing;
