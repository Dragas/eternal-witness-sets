    insert into mtgcard(name) values ('Sliver Legion') on conflict do nothing;
    insert into mtgcard(name) values ('Mox Opal') on conflict do nothing;
    insert into mtgcard(name) values ('Monastery Mentor') on conflict do nothing;
    insert into mtgcard(name) values ('Reflecting Pool') on conflict do nothing;
    insert into mtgcard(name) values ('Isolated Watchtower') on conflict do nothing;
    insert into mtgcard(name) values ('Yuriko, the Tiger''s Shadow') on conflict do nothing;
    insert into mtgcard(name) values ('Mirri''s Guile') on conflict do nothing;
    insert into mtgcard(name) values ('Chalice of the Void') on conflict do nothing;
