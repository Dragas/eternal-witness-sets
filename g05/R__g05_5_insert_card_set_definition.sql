insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'g05'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = 'g05'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gemstone Mine'),
    (select id from sets where short_name = 'g05'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Regrowth'),
    (select id from sets where short_name = 'g05'),
    '2',
    'rare'
) 
 on conflict do nothing;
