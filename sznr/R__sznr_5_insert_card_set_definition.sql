insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Double-Faced Substitute Card'),
    (select id from sets where short_name = 'sznr'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Double-Faced Substitute Card'),
    (select id from sets where short_name = 'sznr'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Double-Faced Substitute Card'),
    (select id from sets where short_name = 'sznr'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Double-Faced Substitute Card'),
    (select id from sets where short_name = 'sznr'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Double-Faced Substitute Card'),
    (select id from sets where short_name = 'sznr'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Double-Faced Substitute Card'),
    (select id from sets where short_name = 'sznr'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Double-Faced Substitute Card'),
    (select id from sets where short_name = 'sznr'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Double-Faced Substitute Card'),
    (select id from sets where short_name = 'sznr'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Double-Faced Substitute Card'),
    (select id from sets where short_name = 'sznr'),
    '4',
    'common'
) 
 on conflict do nothing;
