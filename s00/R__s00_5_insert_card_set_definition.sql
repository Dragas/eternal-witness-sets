insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Royal Falcon'),
    (select id from sets where short_name = 's00'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eager Cadet'),
    (select id from sets where short_name = 's00'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lava Axe'),
    (select id from sets where short_name = 's00'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Durkwood Boars'),
    (select id from sets where short_name = 's00'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moon Sprite'),
    (select id from sets where short_name = 's00'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vizzerdrix'),
    (select id from sets where short_name = 's00'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mons''s Goblin Raiders'),
    (select id from sets where short_name = 's00'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Griffin'),
    (select id from sets where short_name = 's00'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhox'),
    (select id from sets where short_name = 's00'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angelic Blessing'),
    (select id from sets where short_name = 's00'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Monstrous Growth'),
    (select id from sets where short_name = 's00'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Octopus'),
    (select id from sets where short_name = 's00'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time Ebb'),
    (select id from sets where short_name = 's00'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight Errant'),
    (select id from sets where short_name = 's00'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea Eagle'),
    (select id from sets where short_name = 's00'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Willow Elf'),
    (select id from sets where short_name = 's00'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ogre Warrior'),
    (select id from sets where short_name = 's00'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trained Orgg'),
    (select id from sets where short_name = 's00'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hand of Death'),
    (select id from sets where short_name = 's00'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Breath of Life'),
    (select id from sets where short_name = 's00'),
    '3',
    'uncommon'
) 
 on conflict do nothing;
