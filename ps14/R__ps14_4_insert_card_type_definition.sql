insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Chandra, Pyromaster'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chandra, Pyromaster'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chandra, Pyromaster'),
        (select types.id from types where name = 'Chandra')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nissa, Worldwaker'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nissa, Worldwaker'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nissa, Worldwaker'),
        (select types.id from types where name = 'Nissa')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ajani Steadfast'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ajani Steadfast'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ajani Steadfast'),
        (select types.id from types where name = 'Ajani')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Garruk, Apex Predator'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Garruk, Apex Predator'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Garruk, Apex Predator'),
        (select types.id from types where name = 'Garruk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Liliana Vess'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Liliana Vess'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Liliana Vess'),
        (select types.id from types where name = 'Liliana')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jace, the Living Guildpact'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jace, the Living Guildpact'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jace, the Living Guildpact'),
        (select types.id from types where name = 'Jace')
    ) 
 on conflict do nothing;
