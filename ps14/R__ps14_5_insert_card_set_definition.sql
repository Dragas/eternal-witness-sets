insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Chandra, Pyromaster'),
    (select id from sets where short_name = 'ps14'),
    '134',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nissa, Worldwaker'),
    (select id from sets where short_name = 'ps14'),
    '187',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ajani Steadfast'),
    (select id from sets where short_name = 'ps14'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Garruk, Apex Predator'),
    (select id from sets where short_name = 'ps14'),
    '210',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Liliana Vess'),
    (select id from sets where short_name = 'ps14'),
    '103',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jace, the Living Guildpact'),
    (select id from sets where short_name = 'ps14'),
    '62',
    'mythic'
) 
 on conflict do nothing;
