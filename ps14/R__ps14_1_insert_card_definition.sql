    insert into mtgcard(name) values ('Chandra, Pyromaster') on conflict do nothing;
    insert into mtgcard(name) values ('Nissa, Worldwaker') on conflict do nothing;
    insert into mtgcard(name) values ('Ajani Steadfast') on conflict do nothing;
    insert into mtgcard(name) values ('Garruk, Apex Predator') on conflict do nothing;
    insert into mtgcard(name) values ('Liliana Vess') on conflict do nothing;
    insert into mtgcard(name) values ('Jace, the Living Guildpact') on conflict do nothing;
