insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'gk2'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakdos Charm'),
    (select id from sets where short_name = 'gk2'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ulasht, the Hate Seed'),
    (select id from sets where short_name = 'gk2'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burning-Tree Emissary'),
    (select id from sets where short_name = 'gk2'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'gk2'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windreaver'),
    (select id from sets where short_name = 'gk2'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'gk2'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sky Hussar'),
    (select id from sets where short_name = 'gk2'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Isperia the Inscrutable'),
    (select id from sets where short_name = 'gk2'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scab-Clan Mauler'),
    (select id from sets where short_name = 'gk2'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakdos Signet'),
    (select id from sets where short_name = 'gk2'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos, Lord of Riots'),
    (select id from sets where short_name = 'gk2'),
    '52',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Experiment Kraj'),
    (select id from sets where short_name = 'gk2'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'gk2'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pit Fight'),
    (select id from sets where short_name = 'gk2'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plaxcaster Frogling'),
    (select id from sets where short_name = 'gk2'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Court Hussar'),
    (select id from sets where short_name = 'gk2'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urban Evolution'),
    (select id from sets where short_name = 'gk2'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treasury Thrull'),
    (select id from sets where short_name = 'gk2'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archon of the Triumvirate'),
    (select id from sets where short_name = 'gk2'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Borborygmos'),
    (select id from sets where short_name = 'gk2'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruul Charm'),
    (select id from sets where short_name = 'gk2'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wasteland Viper'),
    (select id from sets where short_name = 'gk2'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stab Wound'),
    (select id from sets where short_name = 'gk2'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azorius Signet'),
    (select id from sets where short_name = 'gk2'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vorel of the Hull Clade'),
    (select id from sets where short_name = 'gk2'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azorius Guildmage'),
    (select id from sets where short_name = 'gk2'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Omnibian'),
    (select id from sets where short_name = 'gk2'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dramatic Rescue'),
    (select id from sets where short_name = 'gk2'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teysa, Orzhov Scion'),
    (select id from sets where short_name = 'gk2'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pride of the Clouds'),
    (select id from sets where short_name = 'gk2'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Signet'),
    (select id from sets where short_name = 'gk2'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakdos Cackler'),
    (select id from sets where short_name = 'gk2'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pontiff of Blight'),
    (select id from sets where short_name = 'gk2'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jagged Poppet'),
    (select id from sets where short_name = 'gk2'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keening Banshee'),
    (select id from sets where short_name = 'gk2'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demonfire'),
    (select id from sets where short_name = 'gk2'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thrill-Kill Assassin'),
    (select id from sets where short_name = 'gk2'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azorius Herald'),
    (select id from sets where short_name = 'gk2'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wurmweaver Coil'),
    (select id from sets where short_name = 'gk2'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Splatter Thug'),
    (select id from sets where short_name = 'gk2'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Despair'),
    (select id from sets where short_name = 'gk2'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carnival Hellsteed'),
    (select id from sets where short_name = 'gk2'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ultimate Price'),
    (select id from sets where short_name = 'gk2'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = 'gk2'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fathom Mage'),
    (select id from sets where short_name = 'gk2'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stoic Ephemera'),
    (select id from sets where short_name = 'gk2'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skymark Roc'),
    (select id from sets where short_name = 'gk2'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dreadbore'),
    (select id from sets where short_name = 'gk2'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Pontiff'),
    (select id from sets where short_name = 'gk2'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloudfin Raptor'),
    (select id from sets where short_name = 'gk2'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zhur-Taa Druid'),
    (select id from sets where short_name = 'gk2'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avatar of Discord'),
    (select id from sets where short_name = 'gk2'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghost Council of Orzhova'),
    (select id from sets where short_name = 'gk2'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Miming Slime'),
    (select id from sets where short_name = 'gk2'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coiling Oracle'),
    (select id from sets where short_name = 'gk2'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakdos Carnarium'),
    (select id from sets where short_name = 'gk2'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Master of Cruelties'),
    (select id from sets where short_name = 'gk2'),
    '66',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Plagued Rusalka'),
    (select id from sets where short_name = 'gk2'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savageborn Hydra'),
    (select id from sets where short_name = 'gk2'),
    '98',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Elusive Krasis'),
    (select id from sets where short_name = 'gk2'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Progenitor Mimic'),
    (select id from sets where short_name = 'gk2'),
    '123',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Orzhov Charm'),
    (select id from sets where short_name = 'gk2'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Detention Sphere'),
    (select id from sets where short_name = 'gk2'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zhur-Taa Swine'),
    (select id from sets where short_name = 'gk2'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burning-Tree Shaman'),
    (select id from sets where short_name = 'gk2'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'gk2'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voidslime'),
    (select id from sets where short_name = 'gk2'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruric Thar, the Unbowed'),
    (select id from sets where short_name = 'gk2'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghor-Clan Rampager'),
    (select id from sets where short_name = 'gk2'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lyzolda, the Blood Witch'),
    (select id from sets where short_name = 'gk2'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'gk2'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orzhov Basilica'),
    (select id from sets where short_name = 'gk2'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rapid Hybridization'),
    (select id from sets where short_name = 'gk2'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gyre Sage'),
    (select id from sets where short_name = 'gk2'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Experiment One'),
    (select id from sets where short_name = 'gk2'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Auger Spree'),
    (select id from sets where short_name = 'gk2'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'gk2'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simic Sky Swallower'),
    (select id from sets where short_name = 'gk2'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Riot Spikes'),
    (select id from sets where short_name = 'gk2'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crypt Champion'),
    (select id from sets where short_name = 'gk2'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vizkopa Guildmage'),
    (select id from sets where short_name = 'gk2'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savage Twister'),
    (select id from sets where short_name = 'gk2'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azorius Charm'),
    (select id from sets where short_name = 'gk2'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lyev Skyknight'),
    (select id from sets where short_name = 'gk2'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zegana, Utopian Speaker'),
    (select id from sets where short_name = 'gk2'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rubblebelt Raiders'),
    (select id from sets where short_name = 'gk2'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rubblehulk'),
    (select id from sets where short_name = 'gk2'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azorius Justiciar'),
    (select id from sets where short_name = 'gk2'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Debtors'' Knell'),
    (select id from sets where short_name = 'gk2'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'gk2'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skeletal Vampire'),
    (select id from sets where short_name = 'gk2'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Judge''s Familiar'),
    (select id from sets where short_name = 'gk2'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sin Collector'),
    (select id from sets where short_name = 'gk2'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cytoplast Root-Kin'),
    (select id from sets where short_name = 'gk2'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Growth Chamber'),
    (select id from sets where short_name = 'gk2'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'One Thousand Lashes'),
    (select id from sets where short_name = 'gk2'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Martyred Rusalka'),
    (select id from sets where short_name = 'gk2'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'gk2'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zameck Guildmage'),
    (select id from sets where short_name = 'gk2'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hover Barrier'),
    (select id from sets where short_name = 'gk2'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pillory of the Sleepless'),
    (select id from sets where short_name = 'gk2'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skarrgan Firebird'),
    (select id from sets where short_name = 'gk2'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos''s Return'),
    (select id from sets where short_name = 'gk2'),
    '72',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nimbus Swimmer'),
    (select id from sets where short_name = 'gk2'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Guildmage'),
    (select id from sets where short_name = 'gk2'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sphinx''s Revelation'),
    (select id from sets where short_name = 'gk2'),
    '21',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'gk2'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lavinia of the Tenth'),
    (select id from sets where short_name = 'gk2'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Signet'),
    (select id from sets where short_name = 'gk2'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Solifuge'),
    (select id from sets where short_name = 'gk2'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrecking Ball'),
    (select id from sets where short_name = 'gk2'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Render Silent'),
    (select id from sets where short_name = 'gk2'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azorius Keyrune'),
    (select id from sets where short_name = 'gk2'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos the Defiler'),
    (select id from sets where short_name = 'gk2'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathpact Angel'),
    (select id from sets where short_name = 'gk2'),
    '38',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Protean Hulk'),
    (select id from sets where short_name = 'gk2'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Momir Vig, Simic Visionary'),
    (select id from sets where short_name = 'gk2'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruul Turf'),
    (select id from sets where short_name = 'gk2'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trygon Predator'),
    (select id from sets where short_name = 'gk2'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dovescape'),
    (select id from sets where short_name = 'gk2'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Shred-Freak'),
    (select id from sets where short_name = 'gk2'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cackling Flames'),
    (select id from sets where short_name = 'gk2'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul Signet'),
    (select id from sets where short_name = 'gk2'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skarrgan Pit-Skulk'),
    (select id from sets where short_name = 'gk2'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azorius Chancery'),
    (select id from sets where short_name = 'gk2'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vinelasher Kudzu'),
    (select id from sets where short_name = 'gk2'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Utvara Hellkite'),
    (select id from sets where short_name = 'gk2'),
    '59',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Belfry Spirit'),
    (select id from sets where short_name = 'gk2'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rumbling Slum'),
    (select id from sets where short_name = 'gk2'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Keyrune'),
    (select id from sets where short_name = 'gk2'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Pit Dragon'),
    (select id from sets where short_name = 'gk2'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Isperia, Supreme Judge'),
    (select id from sets where short_name = 'gk2'),
    '1',
    'mythic'
) 
 on conflict do nothing;
