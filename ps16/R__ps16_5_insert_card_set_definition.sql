insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Jace, Unraveler of Secrets'),
    (select id from sets where short_name = 'ps16'),
    '69',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nissa, Voice of Zendikar'),
    (select id from sets where short_name = 'ps16'),
    '138',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gideon, Ally of Zendikar'),
    (select id from sets where short_name = 'ps16'),
    '29',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chandra, Flamecaller'),
    (select id from sets where short_name = 'ps16'),
    '104',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Liliana, the Last Hope'),
    (select id from sets where short_name = 'ps16'),
    '93',
    'mythic'
) 
 on conflict do nothing;
