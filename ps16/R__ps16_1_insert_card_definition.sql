    insert into mtgcard(name) values ('Jace, Unraveler of Secrets') on conflict do nothing;
    insert into mtgcard(name) values ('Nissa, Voice of Zendikar') on conflict do nothing;
    insert into mtgcard(name) values ('Gideon, Ally of Zendikar') on conflict do nothing;
    insert into mtgcard(name) values ('Chandra, Flamecaller') on conflict do nothing;
    insert into mtgcard(name) values ('Liliana, the Last Hope') on conflict do nothing;
