insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Tempt with Discovery'),
    (select id from sets where short_name = 'c13'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foster'),
    (select id from sets where short_name = 'c13'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flickerform'),
    (select id from sets where short_name = 'c13'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Tusker'),
    (select id from sets where short_name = 'c13'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Shrine'),
    (select id from sets where short_name = 'c13'),
    '299',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kirtar''s Wrath'),
    (select id from sets where short_name = 'c13'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Springjack Pasture'),
    (select id from sets where short_name = 'c13'),
    '326',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thornwind Faeries'),
    (select id from sets where short_name = 'c13'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Battlesphere'),
    (select id from sets where short_name = 'c13'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serene Master'),
    (select id from sets where short_name = 'c13'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c13'),
    '350',
    'common'
) ,
(
    (select id from mtgcard where name = 'Surveyor''s Scope'),
    (select id from sets where short_name = 'c13'),
    '262',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guard Gomazoa'),
    (select id from sets where short_name = 'c13'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azorius Guildgate'),
    (select id from sets where short_name = 'c13'),
    '275',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sek''Kuar, Deathkeeper'),
    (select id from sets where short_name = 'c13'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akoum Refuge'),
    (select id from sets where short_name = 'c13'),
    '272',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krosan Warchief'),
    (select id from sets where short_name = 'c13'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c13'),
    '345',
    'common'
) ,
(
    (select id from mtgcard where name = 'Homeward Path'),
    (select id from sets where short_name = 'c13'),
    '295',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Order of Succession'),
    (select id from sets where short_name = 'c13'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Army of the Damned'),
    (select id from sets where short_name = 'c13'),
    '69',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Terra Ravager'),
    (select id from sets where short_name = 'c13'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Conjurer''s Closet'),
    (select id from sets where short_name = 'c13'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Augur of Bolas'),
    (select id from sets where short_name = 'c13'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tidal Force'),
    (select id from sets where short_name = 'c13'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sudden Spoiling'),
    (select id from sets where short_name = 'c13'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hooded Horror'),
    (select id from sets where short_name = 'c13'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tidehollow Strix'),
    (select id from sets where short_name = 'c13'),
    '222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molten Disaster'),
    (select id from sets where short_name = 'c13'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tempt with Glory'),
    (select id from sets where short_name = 'c13'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fog Bank'),
    (select id from sets where short_name = 'c13'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c13'),
    '338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thraximundar'),
    (select id from sets where short_name = 'c13'),
    '221',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Serra Avatar'),
    (select id from sets where short_name = 'c13'),
    '21',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Leafdrake Roost'),
    (select id from sets where short_name = 'c13'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'c13'),
    '259',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Charnelhoard Wurm'),
    (select id from sets where short_name = 'c13'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Behemoth Sledge'),
    (select id from sets where short_name = 'c13'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c13'),
    '348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Witch Hunt'),
    (select id from sets where short_name = 'c13'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Guildgate'),
    (select id from sets where short_name = 'c13'),
    '312',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stronghold Assassin'),
    (select id from sets where short_name = 'c13'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blue Sun''s Zenith'),
    (select id from sets where short_name = 'c13'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Survival Cache'),
    (select id from sets where short_name = 'c13'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Jund'),
    (select id from sets where short_name = 'c13'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Reborn'),
    (select id from sets where short_name = 'c13'),
    '304',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grixis Charm'),
    (select id from sets where short_name = 'c13'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death Grasp'),
    (select id from sets where short_name = 'c13'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c13'),
    '340',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aerie Mystics'),
    (select id from sets where short_name = 'c13'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Pridemate'),
    (select id from sets where short_name = 'c13'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darksteel Ingot'),
    (select id from sets where short_name = 'c13'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Presence of Gond'),
    (select id from sets where short_name = 'c13'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azorius Keyrune'),
    (select id from sets where short_name = 'c13'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mirror Entity'),
    (select id from sets where short_name = 'c13'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eternal Dragon'),
    (select id from sets where short_name = 'c13'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Charm'),
    (select id from sets where short_name = 'c13'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thousand-Year Elixir'),
    (select id from sets where short_name = 'c13'),
    '266',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aethermage''s Touch'),
    (select id from sets where short_name = 'c13'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nihil Spellbomb'),
    (select id from sets where short_name = 'c13'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divinity of Pride'),
    (select id from sets where short_name = 'c13'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slice in Twain'),
    (select id from sets where short_name = 'c13'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'New Benalia'),
    (select id from sets where short_name = 'c13'),
    '309',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bojuka Bog'),
    (select id from sets where short_name = 'c13'),
    '278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mold Shambler'),
    (select id from sets where short_name = 'c13'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mayael the Anima'),
    (select id from sets where short_name = 'c13'),
    '199',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Disciple of Griselbrand'),
    (select id from sets where short_name = 'c13'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reckless Spite'),
    (select id from sets where short_name = 'c13'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savage Lands'),
    (select id from sets where short_name = 'c13'),
    '317',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drumhunter'),
    (select id from sets where short_name = 'c13'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spawning Grounds'),
    (select id from sets where short_name = 'c13'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'War Cadence'),
    (select id from sets where short_name = 'c13'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Delver'),
    (select id from sets where short_name = 'c13'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Well of Lost Dreams'),
    (select id from sets where short_name = 'c13'),
    '271',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strategic Planning'),
    (select id from sets where short_name = 'c13'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viseling'),
    (select id from sets where short_name = 'c13'),
    '269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c13'),
    '343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Walker of the Grove'),
    (select id from sets where short_name = 'c13'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sharding Sphinx'),
    (select id from sets where short_name = 'c13'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jade Mage'),
    (select id from sets where short_name = 'c13'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Derevi, Empyrial Tactician'),
    (select id from sets where short_name = 'c13'),
    '186',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Baleful Force'),
    (select id from sets where short_name = 'c13'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Molten Slagheap'),
    (select id from sets where short_name = 'c13'),
    '306',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selesnya Sanctuary'),
    (select id from sets where short_name = 'c13'),
    '322',
    'common'
) ,
(
    (select id from mtgcard where name = 'Razor Hippogriff'),
    (select id from sets where short_name = 'c13'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nekusar, the Mindrazer'),
    (select id from sets where short_name = 'c13'),
    '201',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Urza''s Factory'),
    (select id from sets where short_name = 'c13'),
    '331',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyward Eye Prophets'),
    (select id from sets where short_name = 'c13'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Starstorm'),
    (select id from sets where short_name = 'c13'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inferno Titan'),
    (select id from sets where short_name = 'c13'),
    '114',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Selesnya Guildgate'),
    (select id from sets where short_name = 'c13'),
    '321',
    'common'
) ,
(
    (select id from mtgcard where name = 'One Dozen Eyes'),
    (select id from sets where short_name = 'c13'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'From the Ashes'),
    (select id from sets where short_name = 'c13'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Filigree Angel'),
    (select id from sets where short_name = 'c13'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'c13'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Baleful Strix'),
    (select id from sets where short_name = 'c13'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tempt with Vengeance'),
    (select id from sets where short_name = 'c13'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Capricious Efreet'),
    (select id from sets where short_name = 'c13'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Endrek Sahr, Master Breeder'),
    (select id from sets where short_name = 'c13'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crumbling Necropolis'),
    (select id from sets where short_name = 'c13'),
    '283',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thunderstaff'),
    (select id from sets where short_name = 'c13'),
    '267',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fecundity'),
    (select id from sets where short_name = 'c13'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c13'),
    '339',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warstorm Surge'),
    (select id from sets where short_name = 'c13'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seer''s Sundial'),
    (select id from sets where short_name = 'c13'),
    '256',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Sharpshooter'),
    (select id from sets where short_name = 'c13'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brilliant Plan'),
    (select id from sets where short_name = 'c13'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Reclamation'),
    (select id from sets where short_name = 'c13'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c13'),
    '337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Uyo, Silent Prophet'),
    (select id from sets where short_name = 'c13'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grixis Panorama'),
    (select id from sets where short_name = 'c13'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Night Soil'),
    (select id from sets where short_name = 'c13'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Acidic Slime'),
    (select id from sets where short_name = 'c13'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viscera Seer'),
    (select id from sets where short_name = 'c13'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lu Xun, Scholar General'),
    (select id from sets where short_name = 'c13'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kongming, "Sleeping Dragon"'),
    (select id from sets where short_name = 'c13'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Djinn of Infinite Deceits'),
    (select id from sets where short_name = 'c13'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Khalni Garden'),
    (select id from sets where short_name = 'c13'),
    '302',
    'common'
) ,
(
    (select id from mtgcard where name = 'Toxic Deluge'),
    (select id from sets where short_name = 'c13'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Gargantua'),
    (select id from sets where short_name = 'c13'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gahiji, Honored One'),
    (select id from sets where short_name = 'c13'),
    '191',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Contested Cliffs'),
    (select id from sets where short_name = 'c13'),
    '282',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marath, Will of the Wild'),
    (select id from sets where short_name = 'c13'),
    '198',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Annihilate'),
    (select id from sets where short_name = 'c13'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selesnya Charm'),
    (select id from sets where short_name = 'c13'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forgotten Cave'),
    (select id from sets where short_name = 'c13'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple of the False God'),
    (select id from sets where short_name = 'c13'),
    '327',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Propaganda'),
    (select id from sets where short_name = 'c13'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Farhaven Elf'),
    (select id from sets where short_name = 'c13'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple Bell'),
    (select id from sets where short_name = 'c13'),
    '265',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Signet'),
    (select id from sets where short_name = 'c13'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golgari Rot Farm'),
    (select id from sets where short_name = 'c13'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curse of the Forsaken'),
    (select id from sets where short_name = 'c13'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Curse of Inertia'),
    (select id from sets where short_name = 'c13'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rupture Spire'),
    (select id from sets where short_name = 'c13'),
    '315',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanguine Bond'),
    (select id from sets where short_name = 'c13'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diviner Spirit'),
    (select id from sets where short_name = 'c13'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sun Droplet'),
    (select id from sets where short_name = 'c13'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Izzet Guildgate'),
    (select id from sets where short_name = 'c13'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sejiri Refuge'),
    (select id from sets where short_name = 'c13'),
    '320',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Elder'),
    (select id from sets where short_name = 'c13'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c13'),
    '352',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ophiomancer'),
    (select id from sets where short_name = 'c13'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sharuum the Hegemon'),
    (select id from sets where short_name = 'c13'),
    '212',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Deadwood Treefolk'),
    (select id from sets where short_name = 'c13'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prosperity'),
    (select id from sets where short_name = 'c13'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dromar''s Charm'),
    (select id from sets where short_name = 'c13'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drifting Meadow'),
    (select id from sets where short_name = 'c13'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dungeon Geists'),
    (select id from sets where short_name = 'c13'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rampaging Baloths'),
    (select id from sets where short_name = 'c13'),
    '164',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Simic Guildgate'),
    (select id from sets where short_name = 'c13'),
    '323',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unexpectedly Absent'),
    (select id from sets where short_name = 'c13'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hull Breach'),
    (select id from sets where short_name = 'c13'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Bladetrap'),
    (select id from sets where short_name = 'c13'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Esper'),
    (select id from sets where short_name = 'c13'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tranquil Thicket'),
    (select id from sets where short_name = 'c13'),
    '329',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sudden Demise'),
    (select id from sets where short_name = 'c13'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivid Creek'),
    (select id from sets where short_name = 'c13'),
    '334',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jar of Eyeballs'),
    (select id from sets where short_name = 'c13'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cruel Ultimatum'),
    (select id from sets where short_name = 'c13'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword of the Paruns'),
    (select id from sets where short_name = 'c13'),
    '264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grim Backwoods'),
    (select id from sets where short_name = 'c13'),
    '292',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tower of Fortunes'),
    (select id from sets where short_name = 'c13'),
    '268',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightscape Familiar'),
    (select id from sets where short_name = 'c13'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basalt Monolith'),
    (select id from sets where short_name = 'c13'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cultivate'),
    (select id from sets where short_name = 'c13'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karmic Guide'),
    (select id from sets where short_name = 'c13'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Command Tower'),
    (select id from sets where short_name = 'c13'),
    '281',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krosan Grip'),
    (select id from sets where short_name = 'c13'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mirari'),
    (select id from sets where short_name = 'c13'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reincarnation'),
    (select id from sets where short_name = 'c13'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azorius Chancery'),
    (select id from sets where short_name = 'c13'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golgari Guildgate'),
    (select id from sets where short_name = 'c13'),
    '290',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barren Moor'),
    (select id from sets where short_name = 'c13'),
    '277',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azorius Herald'),
    (select id from sets where short_name = 'c13'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fires of Yavimaya'),
    (select id from sets where short_name = 'c13'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harmonize'),
    (select id from sets where short_name = 'c13'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Naya Panorama'),
    (select id from sets where short_name = 'c13'),
    '308',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jeleva, Nephalia''s Scourge'),
    (select id from sets where short_name = 'c13'),
    '194',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Curse of Shallow Graves'),
    (select id from sets where short_name = 'c13'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Archangel'),
    (select id from sets where short_name = 'c13'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Saltcrusted Steppe'),
    (select id from sets where short_name = 'c13'),
    '316',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jace''s Archivist'),
    (select id from sets where short_name = 'c13'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Decree of Pain'),
    (select id from sets where short_name = 'c13'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plague Boiler'),
    (select id from sets where short_name = 'c13'),
    '254',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spine of Ish Sah'),
    (select id from sets where short_name = 'c13'),
    '260',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rough // Tumble'),
    (select id from sets where short_name = 'c13'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampire Nighthawk'),
    (select id from sets where short_name = 'c13'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crater Hellion'),
    (select id from sets where short_name = 'c13'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Guildmage'),
    (select id from sets where short_name = 'c13'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blood Rites'),
    (select id from sets where short_name = 'c13'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spoils of Victory'),
    (select id from sets where short_name = 'c13'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phthisis'),
    (select id from sets where short_name = 'c13'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raven Familiar'),
    (select id from sets where short_name = 'c13'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Echo Mage'),
    (select id from sets where short_name = 'c13'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hua Tuo, Honored Physician'),
    (select id from sets where short_name = 'c13'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scarland Thrinax'),
    (select id from sets where short_name = 'c13'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Borrowing 100,000 Arrows'),
    (select id from sets where short_name = 'c13'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rain of Thorns'),
    (select id from sets where short_name = 'c13'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ravenous Baloth'),
    (select id from sets where short_name = 'c13'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magus of the Arena'),
    (select id from sets where short_name = 'c13'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vile Requiem'),
    (select id from sets where short_name = 'c13'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Incendiary Command'),
    (select id from sets where short_name = 'c13'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Street Spasm'),
    (select id from sets where short_name = 'c13'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nivix Guildmage'),
    (select id from sets where short_name = 'c13'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kazandu Refuge'),
    (select id from sets where short_name = 'c13'),
    '301',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c13'),
    '342',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deep Analysis'),
    (select id from sets where short_name = 'c13'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mass Mutiny'),
    (select id from sets where short_name = 'c13'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wonder'),
    (select id from sets where short_name = 'c13'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c13'),
    '356',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crawlspace'),
    (select id from sets where short_name = 'c13'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Price of Knowledge'),
    (select id from sets where short_name = 'c13'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wild Ricochet'),
    (select id from sets where short_name = 'c13'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savage Twister'),
    (select id from sets where short_name = 'c13'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flickerwisp'),
    (select id from sets where short_name = 'c13'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slippery Karst'),
    (select id from sets where short_name = 'c13'),
    '324',
    'common'
) ,
(
    (select id from mtgcard where name = 'Furnace Celebration'),
    (select id from sets where short_name = 'c13'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winged Coatl'),
    (select id from sets where short_name = 'c13'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcane Melee'),
    (select id from sets where short_name = 'c13'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcane Sanctum'),
    (select id from sets where short_name = 'c13'),
    '273',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Transguild Promenade'),
    (select id from sets where short_name = 'c13'),
    '330',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c13'),
    '347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illusionist''s Gambit'),
    (select id from sets where short_name = 'c13'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakeclaw Gargantuan'),
    (select id from sets where short_name = 'c13'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shattergang Brothers'),
    (select id from sets where short_name = 'c13'),
    '213',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Marrow Bats'),
    (select id from sets where short_name = 'c13'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tower Gargoyle'),
    (select id from sets where short_name = 'c13'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lonely Sandbar'),
    (select id from sets where short_name = 'c13'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c13'),
    '351',
    'common'
) ,
(
    (select id from mtgcard where name = 'Famine'),
    (select id from sets where short_name = 'c13'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crosis''s Charm'),
    (select id from sets where short_name = 'c13'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c13'),
    '344',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vizkopa Guildmage'),
    (select id from sets where short_name = 'c13'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Restore'),
    (select id from sets where short_name = 'c13'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armillary Sphere'),
    (select id from sets where short_name = 'c13'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hada Spy Patrol'),
    (select id from sets where short_name = 'c13'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dirge of Dread'),
    (select id from sets where short_name = 'c13'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistmeadow Witch'),
    (select id from sets where short_name = 'c13'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sydri, Galvanic Genius'),
    (select id from sets where short_name = 'c13'),
    '220',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Augury Adept'),
    (select id from sets where short_name = 'c13'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcane Denial'),
    (select id from sets where short_name = 'c13'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fiery Justice'),
    (select id from sets where short_name = 'c13'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Charmbreaker Devils'),
    (select id from sets where short_name = 'c13'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Where Ancients Tread'),
    (select id from sets where short_name = 'c13'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oloro, Ageless Ascetic'),
    (select id from sets where short_name = 'c13'),
    '203',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stalking Vengeance'),
    (select id from sets where short_name = 'c13'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pilgrim''s Eye'),
    (select id from sets where short_name = 'c13'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bant Panorama'),
    (select id from sets where short_name = 'c13'),
    '276',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter Foundry'),
    (select id from sets where short_name = 'c13'),
    '234',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fell Shepherd'),
    (select id from sets where short_name = 'c13'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sprouting Vines'),
    (select id from sets where short_name = 'c13'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vitu-Ghazi, the City-Tree'),
    (select id from sets where short_name = 'c13'),
    '332',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brooding Saurian'),
    (select id from sets where short_name = 'c13'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eye of Doom'),
    (select id from sets where short_name = 'c13'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vision Skeins'),
    (select id from sets where short_name = 'c13'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mosswort Bridge'),
    (select id from sets where short_name = 'c13'),
    '307',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyscribing'),
    (select id from sets where short_name = 'c13'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spellbreaker Behemoth'),
    (select id from sets where short_name = 'c13'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'c13'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deceiver Exarch'),
    (select id from sets where short_name = 'c13'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guttersnipe'),
    (select id from sets where short_name = 'c13'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Opportunity'),
    (select id from sets where short_name = 'c13'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slice and Dice'),
    (select id from sets where short_name = 'c13'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel of Finality'),
    (select id from sets where short_name = 'c13'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Secluded Steppe'),
    (select id from sets where short_name = 'c13'),
    '319',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wight of Precinct Six'),
    (select id from sets where short_name = 'c13'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Esper Panorama'),
    (select id from sets where short_name = 'c13'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Nantuko'),
    (select id from sets where short_name = 'c13'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiend Hunter'),
    (select id from sets where short_name = 'c13'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Carnage Altar'),
    (select id from sets where short_name = 'c13'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wayfarer''s Bauble'),
    (select id from sets where short_name = 'c13'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fissure Vent'),
    (select id from sets where short_name = 'c13'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx of the Steel Wind'),
    (select id from sets where short_name = 'c13'),
    '217',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Greed'),
    (select id from sets where short_name = 'c13'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Basilica'),
    (select id from sets where short_name = 'c13'),
    '311',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boros Garrison'),
    (select id from sets where short_name = 'c13'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tempt with Immortality'),
    (select id from sets where short_name = 'c13'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mnemonic Wall'),
    (select id from sets where short_name = 'c13'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tooth and Claw'),
    (select id from sets where short_name = 'c13'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c13'),
    '341',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azami, Lady of Scrolls'),
    (select id from sets where short_name = 'c13'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bane of Progress'),
    (select id from sets where short_name = 'c13'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quagmire Druid'),
    (select id from sets where short_name = 'c13'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boros Guildgate'),
    (select id from sets where short_name = 'c13'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lim-Dûl''s Vault'),
    (select id from sets where short_name = 'c13'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Bombardment'),
    (select id from sets where short_name = 'c13'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Valley Rannet'),
    (select id from sets where short_name = 'c13'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunted Troll'),
    (select id from sets where short_name = 'c13'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spitebellows'),
    (select id from sets where short_name = 'c13'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Murkfiend Liege'),
    (select id from sets where short_name = 'c13'),
    '231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curse of Predation'),
    (select id from sets where short_name = 'c13'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = 'c13'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seaside Citadel'),
    (select id from sets where short_name = 'c13'),
    '318',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c13'),
    '354',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jund Panorama'),
    (select id from sets where short_name = 'c13'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kazandu Tuskcaller'),
    (select id from sets where short_name = 'c13'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c13'),
    '346',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul Guildgate'),
    (select id from sets where short_name = 'c13'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivid Grove'),
    (select id from sets where short_name = 'c13'),
    '335',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dismiss'),
    (select id from sets where short_name = 'c13'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stonecloaker'),
    (select id from sets where short_name = 'c13'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jwar Isle Refuge'),
    (select id from sets where short_name = 'c13'),
    '300',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vivid Crag'),
    (select id from sets where short_name = 'c13'),
    '333',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Izzet Boilerworks'),
    (select id from sets where short_name = 'c13'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deepfire Elemental'),
    (select id from sets where short_name = 'c13'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Control Magic'),
    (select id from sets where short_name = 'c13'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Baloth Woodcrasher'),
    (select id from sets where short_name = 'c13'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'c13'),
    '328',
    'common'
) ,
(
    (select id from mtgcard where name = 'Widespread Panic'),
    (select id from sets where short_name = 'c13'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c13'),
    '355',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swiftfoot Boots'),
    (select id from sets where short_name = 'c13'),
    '263',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selesnya Signet'),
    (select id from sets where short_name = 'c13'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c13'),
    '349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primal Vigor'),
    (select id from sets where short_name = 'c13'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rubinia Soulsinger'),
    (select id from sets where short_name = 'c13'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Reverence'),
    (select id from sets where short_name = 'c13'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wash Out'),
    (select id from sets where short_name = 'c13'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Endless Cockroaches'),
    (select id from sets where short_name = 'c13'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dimir Guildgate'),
    (select id from sets where short_name = 'c13'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infest'),
    (select id from sets where short_name = 'c13'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darksteel Mutation'),
    (select id from sets where short_name = 'c13'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jund Charm'),
    (select id from sets where short_name = 'c13'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Roon of the Hidden Realm'),
    (select id from sets where short_name = 'c13'),
    '206',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c13'),
    '353',
    'common'
) ,
(
    (select id from mtgcard where name = 'Act of Authority'),
    (select id from sets where short_name = 'c13'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cradle of Vitality'),
    (select id from sets where short_name = 'c13'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spiteful Visions'),
    (select id from sets where short_name = 'c13'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivid Marsh'),
    (select id from sets where short_name = 'c13'),
    '336',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Grixis'),
    (select id from sets where short_name = 'c13'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avenger of Zendikar'),
    (select id from sets where short_name = 'c13'),
    '135',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rakdos Guildgate'),
    (select id from sets where short_name = 'c13'),
    '314',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opal Palace'),
    (select id from sets where short_name = 'c13'),
    '310',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormscape Battlemage'),
    (select id from sets where short_name = 'c13'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'True-Name Nemesis'),
    (select id from sets where short_name = 'c13'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Carnarium'),
    (select id from sets where short_name = 'c13'),
    '313',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tempt with Reflections'),
    (select id from sets where short_name = 'c13'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pristine Talisman'),
    (select id from sets where short_name = 'c13'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kher Keep'),
    (select id from sets where short_name = 'c13'),
    '303',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smoldering Crater'),
    (select id from sets where short_name = 'c13'),
    '325',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'c13'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naya Soulbeast'),
    (select id from sets where short_name = 'c13'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silklash Spider'),
    (select id from sets where short_name = 'c13'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spinal Embrace'),
    (select id from sets where short_name = 'c13'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selesnya Guildmage'),
    (select id from sets where short_name = 'c13'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Druidic Satchel'),
    (select id from sets where short_name = 'c13'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faerie Conclave'),
    (select id from sets where short_name = 'c13'),
    '288',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sprouting Thrinax'),
    (select id from sets where short_name = 'c13'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Naya Charm'),
    (select id from sets where short_name = 'c13'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grazing Gladehart'),
    (select id from sets where short_name = 'c13'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Manipulation'),
    (select id from sets where short_name = 'c13'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prossh, Skyraider of Kher'),
    (select id from sets where short_name = 'c13'),
    '204',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Curse of Chaos'),
    (select id from sets where short_name = 'c13'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deathbringer Thoctar'),
    (select id from sets where short_name = 'c13'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Skysweeper'),
    (select id from sets where short_name = 'c13'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Barrier'),
    (select id from sets where short_name = 'c13'),
    '18',
    'rare'
) 
 on conflict do nothing;
