insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Mardu Shadowspear'),
    (select id from sets where short_name = 'pfrf'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mastery of the Unseen'),
    (select id from sets where short_name = 'pfrf'),
    '19s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Supplant Form'),
    (select id from sets where short_name = 'pfrf'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archfiend of Depravity'),
    (select id from sets where short_name = 'pfrf'),
    '62s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flamerush Rider'),
    (select id from sets where short_name = 'pfrf'),
    '99s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kolaghan, the Storm''s Fury'),
    (select id from sets where short_name = 'pfrf'),
    '155s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mardu Strike Leader'),
    (select id from sets where short_name = 'pfrf'),
    '75s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crux of Fate'),
    (select id from sets where short_name = 'pfrf'),
    '65p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soulfire Grand Master'),
    (select id from sets where short_name = 'pfrf'),
    '27s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Soulflayer'),
    (select id from sets where short_name = 'pfrf'),
    '84s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flamewake Phoenix'),
    (select id from sets where short_name = 'pfrf'),
    '100s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Torrent Elemental'),
    (select id from sets where short_name = 'pfrf'),
    '56s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Atarka, World Render'),
    (select id from sets where short_name = 'pfrf'),
    '149s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Supplant Form'),
    (select id from sets where short_name = 'pfrf'),
    '54s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Atarka, World Render'),
    (select id from sets where short_name = 'pfrf'),
    '149p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sage-Eye Avengers'),
    (select id from sets where short_name = 'pfrf'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silumgar, the Drifting Death'),
    (select id from sets where short_name = 'pfrf'),
    '157s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonscale General'),
    (select id from sets where short_name = 'pfrf'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sage-Eye Avengers'),
    (select id from sets where short_name = 'pfrf'),
    '50s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alesha, Who Smiles at Death'),
    (select id from sets where short_name = 'pfrf'),
    '90s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jeskai Infiltrator'),
    (select id from sets where short_name = 'pfrf'),
    '36s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dromoka, the Eternal'),
    (select id from sets where short_name = 'pfrf'),
    '151s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sandsteppe Mastodon'),
    (select id from sets where short_name = 'pfrf'),
    '137s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rally the Ancestors'),
    (select id from sets where short_name = 'pfrf'),
    '22s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ojutai, Soul of Winter'),
    (select id from sets where short_name = 'pfrf'),
    '156s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Daghatar the Adamant'),
    (select id from sets where short_name = 'pfrf'),
    '9s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brutal Hordechief'),
    (select id from sets where short_name = 'pfrf'),
    '64s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Arcbond'),
    (select id from sets where short_name = 'pfrf'),
    '91s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warden of the First Tree'),
    (select id from sets where short_name = 'pfrf'),
    '143s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shaman of the Great Hunt'),
    (select id from sets where short_name = 'pfrf'),
    '113s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Archfiend of Depravity'),
    (select id from sets where short_name = 'pfrf'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sandsteppe Mastodon'),
    (select id from sets where short_name = 'pfrf'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shamanic Revelation'),
    (select id from sets where short_name = 'pfrf'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shamanic Revelation'),
    (select id from sets where short_name = 'pfrf'),
    '138p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temur War Shaman'),
    (select id from sets where short_name = 'pfrf'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shamanic Revelation'),
    (select id from sets where short_name = 'pfrf'),
    '138s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tasigur, the Golden Fang'),
    (select id from sets where short_name = 'pfrf'),
    '87s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yasova Dragonclaw'),
    (select id from sets where short_name = 'pfrf'),
    '148s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonscale General'),
    (select id from sets where short_name = 'pfrf'),
    '11s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wildcall'),
    (select id from sets where short_name = 'pfrf'),
    '146s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flamerush Rider'),
    (select id from sets where short_name = 'pfrf'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shu Yun, the Silent Tempest'),
    (select id from sets where short_name = 'pfrf'),
    '52s',
    'rare'
) 
 on conflict do nothing;
