    insert into mtgcard(name) values ('Memnite') on conflict do nothing;
    insert into mtgcard(name) values ('Steel Hellkite') on conflict do nothing;
    insert into mtgcard(name) values ('Wurmcoil Engine') on conflict do nothing;
    insert into mtgcard(name) values ('Tempered Steel') on conflict do nothing;
    insert into mtgcard(name) values ('Memoricide') on conflict do nothing;
