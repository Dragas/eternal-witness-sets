insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Memnite'),
    (select id from sets where short_name = 'psom'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steel Hellkite'),
    (select id from sets where short_name = 'psom'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wurmcoil Engine'),
    (select id from sets where short_name = 'psom'),
    '223',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tempered Steel'),
    (select id from sets where short_name = 'psom'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Memoricide'),
    (select id from sets where short_name = 'psom'),
    '*69',
    'rare'
) 
 on conflict do nothing;
