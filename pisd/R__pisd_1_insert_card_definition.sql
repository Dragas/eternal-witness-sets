    insert into mtgcard(name) values ('Ludevic''s Test Subject // Ludevic''s Abomination') on conflict do nothing;
    insert into mtgcard(name) values ('Mayor of Avabruck // Howlpack Alpha') on conflict do nothing;
    insert into mtgcard(name) values ('Elite Inquisitor') on conflict do nothing;
    insert into mtgcard(name) values ('Diregraf Ghoul') on conflict do nothing;
    insert into mtgcard(name) values ('Devil''s Play') on conflict do nothing;
