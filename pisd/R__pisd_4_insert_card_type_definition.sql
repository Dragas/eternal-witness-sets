insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Ludevic''s Test Subject // Ludevic''s Abomination'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ludevic''s Test Subject // Ludevic''s Abomination'),
        (select types.id from types where name = 'Lizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ludevic''s Test Subject // Ludevic''s Abomination'),
        (select types.id from types where name = 'Egg')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ludevic''s Test Subject // Ludevic''s Abomination'),
        (select types.id from types where name = '//')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ludevic''s Test Subject // Ludevic''s Abomination'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ludevic''s Test Subject // Ludevic''s Abomination'),
        (select types.id from types where name = 'Lizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ludevic''s Test Subject // Ludevic''s Abomination'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mayor of Avabruck // Howlpack Alpha'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mayor of Avabruck // Howlpack Alpha'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mayor of Avabruck // Howlpack Alpha'),
        (select types.id from types where name = 'Advisor')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mayor of Avabruck // Howlpack Alpha'),
        (select types.id from types where name = 'Werewolf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mayor of Avabruck // Howlpack Alpha'),
        (select types.id from types where name = '//')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mayor of Avabruck // Howlpack Alpha'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mayor of Avabruck // Howlpack Alpha'),
        (select types.id from types where name = 'Werewolf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elite Inquisitor'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elite Inquisitor'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elite Inquisitor'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Diregraf Ghoul'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Diregraf Ghoul'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Devil''s Play'),
        (select types.id from types where name = 'Sorcery')
    ) 
 on conflict do nothing;
