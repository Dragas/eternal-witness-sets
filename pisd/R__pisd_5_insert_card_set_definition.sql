insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ludevic''s Test Subject // Ludevic''s Abomination'),
    (select id from sets where short_name = 'pisd'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mayor of Avabruck // Howlpack Alpha'),
    (select id from sets where short_name = 'pisd'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elite Inquisitor'),
    (select id from sets where short_name = 'pisd'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diregraf Ghoul'),
    (select id from sets where short_name = 'pisd'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Devil''s Play'),
    (select id from sets where short_name = 'pisd'),
    '140',
    'rare'
) 
 on conflict do nothing;
