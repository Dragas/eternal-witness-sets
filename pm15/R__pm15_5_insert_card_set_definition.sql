insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Siege Dragon'),
    (select id from sets where short_name = 'pm15'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phytotitan'),
    (select id from sets where short_name = 'pm15'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'In Garruk''s Wake'),
    (select id from sets where short_name = 'pm15'),
    '100p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mercurial Pretender'),
    (select id from sets where short_name = 'pm15'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Rabblemaster'),
    (select id from sets where short_name = 'pm15'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul of New Phyrexia'),
    (select id from sets where short_name = 'pm15'),
    '231p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'In Garruk''s Wake'),
    (select id from sets where short_name = 'pm15'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Indulgent Tormentor'),
    (select id from sets where short_name = 'pm15'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis, Unshackled'),
    (select id from sets where short_name = 'pm15'),
    '110p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reclamation Sage'),
    (select id from sets where short_name = 'pm15'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chief Engineer'),
    (select id from sets where short_name = 'pm15'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Resolute Archangel'),
    (select id from sets where short_name = 'pm15'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chasm Skulker'),
    (select id from sets where short_name = 'pm15'),
    '46p',
    'rare'
) 
 on conflict do nothing;
