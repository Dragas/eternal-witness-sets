    insert into mtgcard(name) values ('Nissa, Vastwood Seer // Nissa, Sage Animist') on conflict do nothing;
    insert into mtgcard(name) values ('Kytheon, Hero of Akros // Gideon, Battle-Forged') on conflict do nothing;
    insert into mtgcard(name) values ('Chandra, Fire of Kaladesh // Chandra, Roaring Flame') on conflict do nothing;
    insert into mtgcard(name) values ('Liliana, Heretical Healer // Liliana, Defiant Necromancer') on conflict do nothing;
    insert into mtgcard(name) values ('Jace, Vryn''s Prodigy // Jace, Telepath Unbound') on conflict do nothing;
