insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Nissa, Vastwood Seer // Nissa, Sage Animist'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nissa, Vastwood Seer // Nissa, Sage Animist'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nissa, Vastwood Seer // Nissa, Sage Animist'),
        (select types.id from types where name = 'Elf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nissa, Vastwood Seer // Nissa, Sage Animist'),
        (select types.id from types where name = 'Scout')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nissa, Vastwood Seer // Nissa, Sage Animist'),
        (select types.id from types where name = '//')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nissa, Vastwood Seer // Nissa, Sage Animist'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nissa, Vastwood Seer // Nissa, Sage Animist'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nissa, Vastwood Seer // Nissa, Sage Animist'),
        (select types.id from types where name = 'Nissa')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kytheon, Hero of Akros // Gideon, Battle-Forged'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kytheon, Hero of Akros // Gideon, Battle-Forged'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kytheon, Hero of Akros // Gideon, Battle-Forged'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kytheon, Hero of Akros // Gideon, Battle-Forged'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kytheon, Hero of Akros // Gideon, Battle-Forged'),
        (select types.id from types where name = '//')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kytheon, Hero of Akros // Gideon, Battle-Forged'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kytheon, Hero of Akros // Gideon, Battle-Forged'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kytheon, Hero of Akros // Gideon, Battle-Forged'),
        (select types.id from types where name = 'Gideon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chandra, Fire of Kaladesh // Chandra, Roaring Flame'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chandra, Fire of Kaladesh // Chandra, Roaring Flame'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chandra, Fire of Kaladesh // Chandra, Roaring Flame'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chandra, Fire of Kaladesh // Chandra, Roaring Flame'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chandra, Fire of Kaladesh // Chandra, Roaring Flame'),
        (select types.id from types where name = '//')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chandra, Fire of Kaladesh // Chandra, Roaring Flame'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chandra, Fire of Kaladesh // Chandra, Roaring Flame'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chandra, Fire of Kaladesh // Chandra, Roaring Flame'),
        (select types.id from types where name = 'Chandra')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Liliana, Heretical Healer // Liliana, Defiant Necromancer'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Liliana, Heretical Healer // Liliana, Defiant Necromancer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Liliana, Heretical Healer // Liliana, Defiant Necromancer'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Liliana, Heretical Healer // Liliana, Defiant Necromancer'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Liliana, Heretical Healer // Liliana, Defiant Necromancer'),
        (select types.id from types where name = '//')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Liliana, Heretical Healer // Liliana, Defiant Necromancer'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Liliana, Heretical Healer // Liliana, Defiant Necromancer'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Liliana, Heretical Healer // Liliana, Defiant Necromancer'),
        (select types.id from types where name = 'Liliana')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jace, Vryn''s Prodigy // Jace, Telepath Unbound'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jace, Vryn''s Prodigy // Jace, Telepath Unbound'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jace, Vryn''s Prodigy // Jace, Telepath Unbound'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jace, Vryn''s Prodigy // Jace, Telepath Unbound'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jace, Vryn''s Prodigy // Jace, Telepath Unbound'),
        (select types.id from types where name = '//')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jace, Vryn''s Prodigy // Jace, Telepath Unbound'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jace, Vryn''s Prodigy // Jace, Telepath Unbound'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jace, Vryn''s Prodigy // Jace, Telepath Unbound'),
        (select types.id from types where name = 'Jace')
    ) 
 on conflict do nothing;
