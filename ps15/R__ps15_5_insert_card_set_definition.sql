insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Nissa, Vastwood Seer // Nissa, Sage Animist'),
    (select id from sets where short_name = 'ps15'),
    '189',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kytheon, Hero of Akros // Gideon, Battle-Forged'),
    (select id from sets where short_name = 'ps15'),
    '23',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chandra, Fire of Kaladesh // Chandra, Roaring Flame'),
    (select id from sets where short_name = 'ps15'),
    '135',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Liliana, Heretical Healer // Liliana, Defiant Necromancer'),
    (select id from sets where short_name = 'ps15'),
    '106',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jace, Vryn''s Prodigy // Jace, Telepath Unbound'),
    (select id from sets where short_name = 'ps15'),
    '60',
    'mythic'
) 
 on conflict do nothing;
