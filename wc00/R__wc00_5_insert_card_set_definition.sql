insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Priest of Titania'),
    (select id from sets where short_name = 'wc00'),
    'jk270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Creeping Mold'),
    (select id from sets where short_name = 'wc00'),
    'jk220sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = 'wc00'),
    'nl217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'wc00'),
    'tvdl61b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daze'),
    (select id from sets where short_name = 'wc00'),
    'tvdl30sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grim Monolith'),
    (select id from sets where short_name = 'wc00'),
    'jf126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Attunement'),
    (select id from sets where short_name = 'wc00'),
    'tvdl61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tom van de Logt Decklist (2000)'),
    (select id from sets where short_name = 'wc00'),
    'tvdl0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'wc00'),
    'nl239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Metalworker'),
    (select id from sets where short_name = 'wc00'),
    'jf135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seal of Cleansing'),
    (select id from sets where short_name = 'wc00'),
    'nl18sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erase'),
    (select id from sets where short_name = 'wc00'),
    'tvdl7sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'wc00'),
    'tvdl331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Janosch Kühn Decklist (2000)'),
    (select id from sets where short_name = 'wc00'),
    'jk0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc00'),
    'jk347',
    'common'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'wc00'),
    'nl321',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Opalescence'),
    (select id from sets where short_name = 'wc00'),
    'tvdl13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Splinter'),
    (select id from sets where short_name = 'wc00'),
    'jk121sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Masticore'),
    (select id from sets where short_name = 'wc00'),
    'jk134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boil'),
    (select id from sets where short_name = 'wc00'),
    'jk169sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cursed Totem'),
    (select id from sets where short_name = 'wc00'),
    'tvdl278sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'wc00'),
    'tvdl54sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Academy Rector'),
    (select id from sets where short_name = 'wc00'),
    'nl1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blank Card'),
    (select id from sets where short_name = 'wc00'),
    '00',
    'common'
) ,
(
    (select id from mtgcard where name = 'Worship'),
    (select id from sets where short_name = 'wc00'),
    'nl57sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chill'),
    (select id from sets where short_name = 'wc00'),
    'jf60sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nicolas Labarre Bio'),
    (select id from sets where short_name = 'wc00'),
    'nl0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voltaic Key'),
    (select id from sets where short_name = 'wc00'),
    'jf314',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pattern of Rebirth'),
    (select id from sets where short_name = 'wc00'),
    'nl115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'wc00'),
    'tvdl54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Parallax Wave'),
    (select id from sets where short_name = 'wc00'),
    'nl17sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Creeping Mold'),
    (select id from sets where short_name = 'wc00'),
    'jk220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = 'wc00'),
    'jk209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yawgmoth''s Will'),
    (select id from sets where short_name = 'wc00'),
    'nl171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Processor'),
    (select id from sets where short_name = 'wc00'),
    'jk306sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thran Dynamo'),
    (select id from sets where short_name = 'wc00'),
    'jf139b',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'wc00'),
    'jk239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Helix'),
    (select id from sets where short_name = 'wc00'),
    'jf302',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fecundity'),
    (select id from sets where short_name = 'wc00'),
    'nl251',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karplusan Forest'),
    (select id from sets where short_name = 'wc00'),
    'jk326',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tom van de Logt Bio (2000)'),
    (select id from sets where short_name = 'wc00'),
    'tvdl0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc00'),
    'tvdl335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Black'),
    (select id from sets where short_name = 'wc00'),
    'tvdl8sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avalanche Riders'),
    (select id from sets where short_name = 'wc00'),
    'jk74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc00'),
    'nl347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thran Quarry'),
    (select id from sets where short_name = 'wc00'),
    'nl329',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Processor'),
    (select id from sets where short_name = 'wc00'),
    'jk306',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Tower'),
    (select id from sets where short_name = 'wc00'),
    'nl322',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chill'),
    (select id from sets where short_name = 'wc00'),
    'tvdl60sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = 'wc00'),
    'jk217',
    'rare'
) ,
(
    (select id from mtgcard where name = '2000 World Championships Ad'),
    (select id from sets where short_name = 'wc00'),
    '0',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blaze'),
    (select id from sets where short_name = 'wc00'),
    'nl168sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Annul'),
    (select id from sets where short_name = 'wc00'),
    'jf59sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling Burst'),
    (select id from sets where short_name = 'wc00'),
    'jk113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saproling Cluster'),
    (select id from sets where short_name = 'wc00'),
    'nl114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Replenish'),
    (select id from sets where short_name = 'wc00'),
    'tvdl15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Processor'),
    (select id from sets where short_name = 'wc00'),
    'jf306b',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Energy Field'),
    (select id from sets where short_name = 'wc00'),
    'tvdl73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saprazzan Skerry'),
    (select id from sets where short_name = 'wc00'),
    'jf328',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc00'),
    'jk343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meekstone'),
    (select id from sets where short_name = 'wc00'),
    'nl299sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rack and Ruin'),
    (select id from sets where short_name = 'wc00'),
    'jk89sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frantic Search'),
    (select id from sets where short_name = 'wc00'),
    'tvdl32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = 'wc00'),
    'nl4sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enlightened Tutor'),
    (select id from sets where short_name = 'wc00'),
    'nl19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blastoderm'),
    (select id from sets where short_name = 'wc00'),
    'jk102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Defense Grid'),
    (select id from sets where short_name = 'wc00'),
    'nl125sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rishadan Port'),
    (select id from sets where short_name = 'wc00'),
    'jk324',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seal of Cleansing'),
    (select id from sets where short_name = 'wc00'),
    'tvdl18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dust Bowl'),
    (select id from sets where short_name = 'wc00'),
    'jk316',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Absolute Law'),
    (select id from sets where short_name = 'wc00'),
    'nl2sba',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Masticore'),
    (select id from sets where short_name = 'wc00'),
    'jf134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nicolas Labarre Decklist'),
    (select id from sets where short_name = 'wc00'),
    'nl0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enlightened Tutor'),
    (select id from sets where short_name = 'wc00'),
    'tvdl19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heart of Ramos'),
    (select id from sets where short_name = 'wc00'),
    'nl296sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rishadan Port'),
    (select id from sets where short_name = 'wc00'),
    'jf324',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystical Tutor'),
    (select id from sets where short_name = 'wc00'),
    'tvdl83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seal of Cleansing'),
    (select id from sets where short_name = 'wc00'),
    'tvdl18sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arc Lightning'),
    (select id from sets where short_name = 'wc00'),
    'jk174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Confiscate'),
    (select id from sets where short_name = 'wc00'),
    'nl66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Priest of Titania'),
    (select id from sets where short_name = 'wc00'),
    'nl270',
    'common'
) ,
(
    (select id from mtgcard where name = 'High Market'),
    (select id from sets where short_name = 'wc00'),
    'nl320b',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jon Finkel Decklist'),
    (select id from sets where short_name = 'wc00'),
    'jf0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Parallax Tide'),
    (select id from sets where short_name = 'wc00'),
    'tvdl37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Avatar'),
    (select id from sets where short_name = 'wc00'),
    'nl45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Submerge'),
    (select id from sets where short_name = 'wc00'),
    'tvdl48sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tangle Wire'),
    (select id from sets where short_name = 'wc00'),
    'jf139a',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc00'),
    'jf335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whetstone'),
    (select id from sets where short_name = 'wc00'),
    'nl316',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Light of Day'),
    (select id from sets where short_name = 'wc00'),
    'nl29sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Processor'),
    (select id from sets where short_name = 'wc00'),
    'nl306sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flameshot'),
    (select id from sets where short_name = 'wc00'),
    'jk90sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plow Under'),
    (select id from sets where short_name = 'wc00'),
    'jk117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crumbling Sanctuary'),
    (select id from sets where short_name = 'wc00'),
    'jf292',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lilting Refrain'),
    (select id from sets where short_name = 'wc00'),
    'tvdl83sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'wc00'),
    'jf61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Helix'),
    (select id from sets where short_name = 'wc00'),
    'jf302sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Masticore'),
    (select id from sets where short_name = 'wc00'),
    'jk134sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rising Waters'),
    (select id from sets where short_name = 'wc00'),
    'jf38sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brushland'),
    (select id from sets where short_name = 'wc00'),
    'nl320',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crystal Vein'),
    (select id from sets where short_name = 'wc00'),
    'jf322',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Energy Flux'),
    (select id from sets where short_name = 'wc00'),
    'nl78sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seal of Removal'),
    (select id from sets where short_name = 'wc00'),
    'tvdl42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jon Finkel Bio'),
    (select id from sets where short_name = 'wc00'),
    'jf0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Uktabi Orangutan'),
    (select id from sets where short_name = 'wc00'),
    'jk260sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yawgmoth''s Bargain'),
    (select id from sets where short_name = 'wc00'),
    'nl75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Miscalculation'),
    (select id from sets where short_name = 'wc00'),
    'jf36sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Adarkar Wastes'),
    (select id from sets where short_name = 'wc00'),
    'tvdl319',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aura Fracture'),
    (select id from sets where short_name = 'wc00'),
    'nl2sbb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snake Basket'),
    (select id from sets where short_name = 'wc00'),
    'nl312',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tinker'),
    (select id from sets where short_name = 'wc00'),
    'jf45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rishadan Port'),
    (select id from sets where short_name = 'wc00'),
    'tvdl324',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Parallax Wave'),
    (select id from sets where short_name = 'wc00'),
    'tvdl17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sky Diamond'),
    (select id from sets where short_name = 'wc00'),
    'tvdl311',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Altar'),
    (select id from sets where short_name = 'wc00'),
    'nl274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Colossus'),
    (select id from sets where short_name = 'wc00'),
    'jf305',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Janosch Kühn Bio (2000)'),
    (select id from sets where short_name = 'wc00'),
    'jk0a',
    'common'
) 
 on conflict do nothing;
