insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Snake'),
    (select id from sets where short_name = 'tktk'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tktk'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire'),
    (select id from sets where short_name = 'tktk'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sorin, Solemn Visitor Emblem'),
    (select id from sets where short_name = 'tktk'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sarkhan, the Dragonspeaker Emblem'),
    (select id from sets where short_name = 'tktk'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird'),
    (select id from sets where short_name = 'tktk'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warrior'),
    (select id from sets where short_name = 'tktk'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tktk'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Morph'),
    (select id from sets where short_name = 'tktk'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bear'),
    (select id from sets where short_name = 'tktk'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tktk'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warrior'),
    (select id from sets where short_name = 'tktk'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit Warrior'),
    (select id from sets where short_name = 'tktk'),
    '10',
    'common'
) 
 on conflict do nothing;
