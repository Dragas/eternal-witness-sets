insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Osai Vultures'),
    (select id from sets where short_name = 'ren'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yotian Soldier'),
    (select id from sets where short_name = 'ren'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elder Land Wurm'),
    (select id from sets where short_name = 'ren'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Visions'),
    (select id from sets where short_name = 'ren'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cave People'),
    (select id from sets where short_name = 'ren'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blight'),
    (select id from sets where short_name = 'ren'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rebirth'),
    (select id from sets where short_name = 'ren'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tempest Efreet'),
    (select id from sets where short_name = 'ren'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyrotechnics'),
    (select id from sets where short_name = 'ren'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whirling Dervish'),
    (select id from sets where short_name = 'ren'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tawnos''s Wand'),
    (select id from sets where short_name = 'ren'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'White Mana Battery'),
    (select id from sets where short_name = 'ren'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seeker'),
    (select id from sets where short_name = 'ren'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Durkwood Boars'),
    (select id from sets where short_name = 'ren'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit Shackle'),
    (select id from sets where short_name = 'ren'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fortified Area'),
    (select id from sets where short_name = 'ren'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cyclopean Mummy'),
    (select id from sets where short_name = 'ren'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tetravus'),
    (select id from sets where short_name = 'ren'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elven Riders'),
    (select id from sets where short_name = 'ren'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inferno'),
    (select id from sets where short_name = 'ren'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Immolation'),
    (select id from sets where short_name = 'ren'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ali Baba'),
    (select id from sets where short_name = 'ren'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Strength'),
    (select id from sets where short_name = 'ren'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cosmic Horror'),
    (select id from sets where short_name = 'ren'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Dust'),
    (select id from sets where short_name = 'ren'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Colossus of Sardia'),
    (select id from sets where short_name = 'ren'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Spears'),
    (select id from sets where short_name = 'ren'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bog Imp'),
    (select id from sets where short_name = 'ren'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relic Bind'),
    (select id from sets where short_name = 'ren'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nafs Asp'),
    (select id from sets where short_name = 'ren'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird Maiden'),
    (select id from sets where short_name = 'ren'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shapeshifter'),
    (select id from sets where short_name = 'ren'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Killer Bees'),
    (select id from sets where short_name = 'ren'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Morale'),
    (select id from sets where short_name = 'ren'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Bats'),
    (select id from sets where short_name = 'ren'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brothers of Fire'),
    (select id from sets where short_name = 'ren'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winter Blast'),
    (select id from sets where short_name = 'ren'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sandstorm'),
    (select id from sets where short_name = 'ren'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ironclaw Orcs'),
    (select id from sets where short_name = 'ren'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Brute'),
    (select id from sets where short_name = 'ren'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eternal Warrior'),
    (select id from sets where short_name = 'ren'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blood Lust'),
    (select id from sets where short_name = 'ren'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blue Mana Battery'),
    (select id from sets where short_name = 'ren'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Backfire'),
    (select id from sets where short_name = 'ren'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Greed'),
    (select id from sets where short_name = 'ren'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abomination'),
    (select id from sets where short_name = 'ren'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Erosion'),
    (select id from sets where short_name = 'ren'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clockwork Avian'),
    (select id from sets where short_name = 'ren'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Bomb'),
    (select id from sets where short_name = 'ren'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Triskelion'),
    (select id from sets where short_name = 'ren'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sisters of the Flame'),
    (select id from sets where short_name = 'ren'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gaseous Form'),
    (select id from sets where short_name = 'ren'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sindbad'),
    (select id from sets where short_name = 'ren'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Twiddle'),
    (select id from sets where short_name = 'ren'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Uncle Istvan'),
    (select id from sets where short_name = 'ren'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghost Ship'),
    (select id from sets where short_name = 'ren'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Segovian Leviathan'),
    (select id from sets where short_name = 'ren'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tawnos''s Weaponry'),
    (select id from sets where short_name = 'ren'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Time Elemental'),
    (select id from sets where short_name = 'ren'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diabolic Machine'),
    (select id from sets where short_name = 'ren'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tundra Wolves'),
    (select id from sets where short_name = 'ren'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Library'),
    (select id from sets where short_name = 'ren'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marsh Viper'),
    (select id from sets where short_name = 'ren'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brainwash'),
    (select id from sets where short_name = 'ren'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murk Dwellers'),
    (select id from sets where short_name = 'ren'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hurr Jackal'),
    (select id from sets where short_name = 'ren'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coral Helm'),
    (select id from sets where short_name = 'ren'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Rock Sled'),
    (select id from sets where short_name = 'ren'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divine Transformation'),
    (select id from sets where short_name = 'ren'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Carnivorous Plant'),
    (select id from sets where short_name = 'ren'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lost Soul'),
    (select id from sets where short_name = 'ren'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pikemen'),
    (select id from sets where short_name = 'ren'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pit Scorpion'),
    (select id from sets where short_name = 'ren'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Tortoise'),
    (select id from sets where short_name = 'ren'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Land Leeches'),
    (select id from sets where short_name = 'ren'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Venom'),
    (select id from sets where short_name = 'ren'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psionic Entity'),
    (select id from sets where short_name = 'ren'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunken City'),
    (select id from sets where short_name = 'ren'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Red Mana Battery'),
    (select id from sets where short_name = 'ren'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pradesh Gypsies'),
    (select id from sets where short_name = 'ren'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Xenic Poltergeist'),
    (select id from sets where short_name = 'ren'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crimson Manticore'),
    (select id from sets where short_name = 'ren'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Apprentice Wizard'),
    (select id from sets where short_name = 'ren'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Radjan Spirit'),
    (select id from sets where short_name = 'ren'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Junún Efreet'),
    (select id from sets where short_name = 'ren'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clay Statue'),
    (select id from sets where short_name = 'ren'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fellwar Stone'),
    (select id from sets where short_name = 'ren'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kismet'),
    (select id from sets where short_name = 'ren'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bronze Tablet'),
    (select id from sets where short_name = 'ren'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marsh Gas'),
    (select id from sets where short_name = 'ren'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cursed Rack'),
    (select id from sets where short_name = 'ren'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battering Ram'),
    (select id from sets where short_name = 'ren'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Avenger'),
    (select id from sets where short_name = 'ren'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zephyr Falcon'),
    (select id from sets where short_name = 'ren'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Word of Binding'),
    (select id from sets where short_name = 'ren'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winds of Change'),
    (select id from sets where short_name = 'ren'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spirit Link'),
    (select id from sets where short_name = 'ren'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Clash'),
    (select id from sets where short_name = 'ren'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Black Mana Battery'),
    (select id from sets where short_name = 'ren'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fissure'),
    (select id from sets where short_name = 'ren'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Artifacts'),
    (select id from sets where short_name = 'ren'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Energy Tap'),
    (select id from sets where short_name = 'ren'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rag Man'),
    (select id from sets where short_name = 'ren'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Piety'),
    (select id from sets where short_name = 'ren'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Green Mana Battery'),
    (select id from sets where short_name = 'ren'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = 'ren'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Untamed Wilds'),
    (select id from sets where short_name = 'ren'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oasis'),
    (select id from sets where short_name = 'ren'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Amulet of Kroog'),
    (select id from sets where short_name = 'ren'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strip Mine'),
    (select id from sets where short_name = 'ren'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Detonate'),
    (select id from sets where short_name = 'ren'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashes to Ashes'),
    (select id from sets where short_name = 'ren'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carrion Ants'),
    (select id from sets where short_name = 'ren'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angry Mob'),
    (select id from sets where short_name = 'ren'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leviathan'),
    (select id from sets where short_name = 'ren'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alabaster Potion'),
    (select id from sets where short_name = 'ren'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Battle Gear'),
    (select id from sets where short_name = 'ren'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grapeshot Catapult'),
    (select id from sets where short_name = 'ren'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flood'),
    (select id from sets where short_name = 'ren'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ball Lightning'),
    (select id from sets where short_name = 'ren'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Amrou Kithkin'),
    (select id from sets where short_name = 'ren'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Land Tax'),
    (select id from sets where short_name = 'ren'),
    '13',
    'uncommon'
) 
 on conflict do nothing;
