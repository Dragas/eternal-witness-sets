insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'M''Odo, the Gnarled Oracle'),
    (select id from sets where short_name = 'htr17'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Diabolical Salvation'),
    (select id from sets where short_name = 'htr17'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Inzerva, Master of Insights'),
    (select id from sets where short_name = 'htr17'),
    '2',
    'mythic'
) 
 on conflict do nothing;
