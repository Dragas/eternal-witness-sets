insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Maro''s Gone Nuts'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Weaponized Scrap'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Weaponized Scrap'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Weaponized Scrap'),
        (select types.id from types where name = 'Construct')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Form of the Mulldrifter'),
        (select types.id from types where name = 'Tribal')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Form of the Mulldrifter'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Form of the Mulldrifter'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aggressive Crag'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kaya, Ghost Haunter'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kaya, Ghost Haunter'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kaya, Ghost Haunter'),
        (select types.id from types where name = 'Kaya')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Transcantation'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Five Kids in a Trenchcoat'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Five Kids in a Trenchcoat'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Five Kids in a Trenchcoat'),
        (select types.id from types where name = 'Citizen')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Frontier Explorer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Frontier Explorer'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Frontier Explorer'),
        (select types.id from types where name = 'Scout')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Everlasting Lich'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Everlasting Lich'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'How to Keep an Izzet Mage Busy'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ruff, Underdog Champ'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ruff, Underdog Champ'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ruff, Underdog Champ'),
        (select types.id from types where name = 'Dog')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ruff, Underdog Champ'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Xyru Specter'),
        (select types.id from types where name = 'Summon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Xyru Specter'),
        (select types.id from types where name = 'Specter')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Evil Boros Charm'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Smelt // Herd // Saw'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Smelt // Herd // Saw'),
        (select types.id from types where name = '//')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Smelt // Herd // Saw'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Smelt // Herd // Saw'),
        (select types.id from types where name = '//')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Smelt // Herd // Saw'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'High Troller'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'High Troller'),
        (select types.id from types where name = 'Troll')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'High Troller'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Taiga Stadium'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Khod, Etlan Shiis Envoy'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Khod, Etlan Shiis Envoy'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Khod, Etlan Shiis Envoy'),
        (select types.id from types where name = 'Homarid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Khod, Etlan Shiis Envoy'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Memory Bank'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bucket List'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mirrored Lotus'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swarm of Locus'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swarm of Locus'),
        (select types.id from types where name = 'Insect')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swarm of Locus'),
        (select types.id from types where name = 'Locus')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bear with Set''s Mechanic'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bear with Set''s Mechanic'),
        (select types.id from types where name = 'Bear')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sliv-Mizzet, Hivemind'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sliv-Mizzet, Hivemind'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sliv-Mizzet, Hivemind'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sliv-Mizzet, Hivemind'),
        (select types.id from types where name = 'Sliver')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Graveyard Dig'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Trial and Error'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Trial and Error'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Trial and Error'),
        (select types.id from types where name = 'Fire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Imaginary Friends'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Imaginary Friends'),
        (select types.id from types where name = 'Arcane')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Queue of Beetles'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Queue of Beetles'),
        (select types.id from types where name = 'Insect')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Noxious Bayou'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sunimret'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gunk Slug'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gunk Slug'),
        (select types.id from types where name = 'Slug')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Barry''s Land'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Barry''s Land'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Barry''s Land'),
        (select types.id from types where name = 'Cloud')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spellmorph Raise Dead'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gold Mine'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lantern of Undersight'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vazal, the Compleat'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vazal, the Compleat'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vazal, the Compleat'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vazal, the Compleat'),
        (select types.id from types where name = 'Phyrexian')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Geometric Weird'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Geometric Weird'),
        (select types.id from types where name = 'Weird')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Scaled Destruction'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Krosan Adaptation'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Krosan Adaptation'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tibalt the Chaotic'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tibalt the Chaotic'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tibalt the Chaotic'),
        (select types.id from types where name = 'Tibalt')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'You''re in Command'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Growth Charm'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Underdark Beholder'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Underdark Beholder'),
        (select types.id from types where name = 'Beholder')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Truth or Dare'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Domesticated Watercourse'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Priority Avenger'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Priority Avenger'),
        (select types.id from types where name = 'Bird')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Priority Avenger'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Waste Land'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Slivdrazi Monstrosity'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Slivdrazi Monstrosity'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Slivdrazi Monstrosity'),
        (select types.id from types where name = 'Sliver')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Slivdrazi Monstrosity'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'One with Death'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Enroll in the Coalition'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pithing Spyglass'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Patient Turtle'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Patient Turtle'),
        (select types.id from types where name = 'Turtle')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chronobot'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chronobot'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chronobot'),
        (select types.id from types where name = 'Construct')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Whammy Burn'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Enchantmentize'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Enchantmentize'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Grand Tour'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Yawgmoth''s Testament'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Largepox'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Buried Ogre'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Buried Ogre'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Buried Ogre'),
        (select types.id from types where name = 'Ogre')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Biting Remark'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Biting Remark'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Banding Sliver'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Banding Sliver'),
        (select types.id from types where name = 'Sliver')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Seasoned Weaponsmith'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Seasoned Weaponsmith'),
        (select types.id from types where name = 'Bird')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Seasoned Weaponsmith'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Impatient Iguana'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Impatient Iguana'),
        (select types.id from types where name = 'Lizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Impatient Iguana'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Siege Elemental'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Siege Elemental'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Frenemy of the Guildpact'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Frenemy of the Guildpact'),
        (select types.id from types where name = 'Spirit')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stack of Paperwork'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Problematic Volcano'),
        (select types.id from types where name = 'World')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Problematic Volcano'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chimney Goyf'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chimney Goyf'),
        (select types.id from types where name = 'Lhurgoyf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chimney Goyf'),
        (select types.id from types where name = 'Imp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Red Herring'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Red Herring'),
        (select types.id from types where name = 'Fish')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Loopy Lobster'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Loopy Lobster'),
        (select types.id from types where name = 'Lobster')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Planequake'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Celestine Cave Witch'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Celestine Cave Witch'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Celestine Cave Witch'),
        (select types.id from types where name = 'Warlock')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wrath of Sod'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jasconian Isle'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jasconian Isle'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jasconian Isle'),
        (select types.id from types where name = 'Island')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jasconian Isle'),
        (select types.id from types where name = 'Fish')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mana Abundance'),
        (select types.id from types where name = 'World')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mana Abundance'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Enchanted Prairie'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Enchanted Prairie'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Golgari Death Swarm'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Golgari Death Swarm'),
        (select types.id from types where name = 'Bat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Golgari Death Swarm'),
        (select types.id from types where name = 'Fungus')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Golgari Death Swarm'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Abian, Luvion Usurper'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Abian, Luvion Usurper'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Abian, Luvion Usurper'),
        (select types.id from types where name = 'Abian')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wizened Arbiter'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wizened Arbiter'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wizened Arbiter'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wizened Arbiter'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Domesticated Mammoth'),
        (select types.id from types where name = 'Snow')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Domesticated Mammoth'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Domesticated Mammoth'),
        (select types.id from types where name = 'Mammoth')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Recycla-bird'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Recycla-bird'),
        (select types.id from types where name = 'Bird')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Corrupted Key'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Corrupted Key'),
        (select types.id from types where name = 'Key')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Seek Bolas''s Counsel'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Animate Spell'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Animate Spell'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Interplanar Brushwagg'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Interplanar Brushwagg'),
        (select types.id from types where name = 'Brushwagg')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Visitor from Planet Q'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Visitor from Planet Q'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Visitor from Planet Q'),
        (select types.id from types where name = 'Alien')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ral''s Vanguard'),
        (select types.id from types where name = 'Vanguard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Blood Poet'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Blood Poet'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Blood Poet'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Metagamer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Metagamer'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Metagamer'),
        (select types.id from types where name = 'Advisor')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Unicycle'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Unicycle'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Unicycle'),
        (select types.id from types where name = 'Vehicle')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Do-Over'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soulmates'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soulmates'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Personal Decoy'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Personal Decoy'),
        (select types.id from types where name = 'Duck')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lazier Goblin'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lazier Goblin'),
        (select types.id from types where name = 'Goblin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Frogkin Kidnapper'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Frogkin Kidnapper'),
        (select types.id from types where name = 'Frog')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Frogkin Kidnapper'),
        (select types.id from types where name = 'Rogue')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zyym, Mesmeric Lord'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zyym, Mesmeric Lord'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zyym, Mesmeric Lord'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zyym, Mesmeric Lord'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Learned Learner'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Learned Learner'),
        (select types.id from types where name = 'Cephalid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Learned Learner'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Throat Wolf'),
        (select types.id from types where name = 'Summon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Throat Wolf'),
        (select types.id from types where name = 'Wolf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Puresteel Angel'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Puresteel Angel'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Puresteel Angel'),
        (select types.id from types where name = 'Angel')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lightning Colt'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lightning Colt'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lightning Colt'),
        (select types.id from types where name = 'Horse')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Baneslayer Aspirant'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Baneslayer Aspirant'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Baneslayer Aspirant'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gorilla Tactics'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Witty Demon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Witty Demon'),
        (select types.id from types where name = 'Demon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Command the Chaff'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Generated Horizons'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Control Win Condition'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Control Win Condition'),
        (select types.id from types where name = 'Whale')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Inspirational Antelope'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Inspirational Antelope'),
        (select types.id from types where name = 'Antelope')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cyclopean Titan'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cyclopean Titan'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cyclopean Titan'),
        (select types.id from types where name = 'Giant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bind // Liberate'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bind // Liberate'),
        (select types.id from types where name = '//')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bind // Liberate'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Experiment Five'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Experiment Five'),
        (select types.id from types where name = 'Bear')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Experiment Five'),
        (select types.id from types where name = 'Snake')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Experiment Five'),
        (select types.id from types where name = 'Mutant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Innocuous Insect'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Innocuous Insect'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Innocuous Insect'),
        (select types.id from types where name = 'Insect')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Time Sidewalk'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'A Good Thing'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Start // Fire'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Start // Fire'),
        (select types.id from types where name = '//')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Start // Fire'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Louvaq, the Aberrant'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Louvaq, the Aberrant'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Louvaq, the Aberrant'),
        (select types.id from types where name = 'Mutant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Squidnapper'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Squidnapper'),
        (select types.id from types where name = 'Squid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Squidnapper'),
        (select types.id from types where name = 'Pirate')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sarah''s Wings'),
        (select types.id from types where name = 'Tribal')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sarah''s Wings'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sarah''s Wings'),
        (select types.id from types where name = 'Angel')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rift'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rift'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bombardment'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pick Your Poison'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bone Rattler'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bone Rattler'),
        (select types.id from types where name = 'Skeleton')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plane-Merge Elf'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plane-Merge Elf'),
        (select types.id from types where name = 'Elf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plane-Merge Elf'),
        (select types.id from types where name = 'Warrior')
    ) 
 on conflict do nothing;
