insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Maro''s Gone Nuts'),
    (select id from sets where short_name = 'cmb1'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Weaponized Scrap'),
    (select id from sets where short_name = 'cmb1'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Form of the Mulldrifter'),
    (select id from sets where short_name = 'cmb1'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aggressive Crag'),
    (select id from sets where short_name = 'cmb1'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaya, Ghost Haunter'),
    (select id from sets where short_name = 'cmb1'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Transcantation'),
    (select id from sets where short_name = 'cmb1'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Five Kids in a Trenchcoat'),
    (select id from sets where short_name = 'cmb1'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frontier Explorer'),
    (select id from sets where short_name = 'cmb1'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Everlasting Lich'),
    (select id from sets where short_name = 'cmb1'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'How to Keep an Izzet Mage Busy'),
    (select id from sets where short_name = 'cmb1'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruff, Underdog Champ'),
    (select id from sets where short_name = 'cmb1'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Xyru Specter'),
    (select id from sets where short_name = 'cmb1'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evil Boros Charm'),
    (select id from sets where short_name = 'cmb1'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smelt // Herd // Saw'),
    (select id from sets where short_name = 'cmb1'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'High Troller'),
    (select id from sets where short_name = 'cmb1'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Taiga Stadium'),
    (select id from sets where short_name = 'cmb1'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Khod, Etlan Shiis Envoy'),
    (select id from sets where short_name = 'cmb1'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Memory Bank'),
    (select id from sets where short_name = 'cmb1'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bucket List'),
    (select id from sets where short_name = 'cmb1'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirrored Lotus'),
    (select id from sets where short_name = 'cmb1'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swarm of Locus'),
    (select id from sets where short_name = 'cmb1'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bear with Set''s Mechanic'),
    (select id from sets where short_name = 'cmb1'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sliv-Mizzet, Hivemind'),
    (select id from sets where short_name = 'cmb1'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Graveyard Dig'),
    (select id from sets where short_name = 'cmb1'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trial and Error'),
    (select id from sets where short_name = 'cmb1'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imaginary Friends'),
    (select id from sets where short_name = 'cmb1'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Queue of Beetles'),
    (select id from sets where short_name = 'cmb1'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noxious Bayou'),
    (select id from sets where short_name = 'cmb1'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunimret'),
    (select id from sets where short_name = 'cmb1'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gunk Slug'),
    (select id from sets where short_name = 'cmb1'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barry''s Land'),
    (select id from sets where short_name = 'cmb1'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spellmorph Raise Dead'),
    (select id from sets where short_name = 'cmb1'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gold Mine'),
    (select id from sets where short_name = 'cmb1'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lantern of Undersight'),
    (select id from sets where short_name = 'cmb1'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vazal, the Compleat'),
    (select id from sets where short_name = 'cmb1'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Geometric Weird'),
    (select id from sets where short_name = 'cmb1'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scaled Destruction'),
    (select id from sets where short_name = 'cmb1'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Adaptation'),
    (select id from sets where short_name = 'cmb1'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tibalt the Chaotic'),
    (select id from sets where short_name = 'cmb1'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'You''re in Command'),
    (select id from sets where short_name = 'cmb1'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Growth Charm'),
    (select id from sets where short_name = 'cmb1'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Underdark Beholder'),
    (select id from sets where short_name = 'cmb1'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Truth or Dare'),
    (select id from sets where short_name = 'cmb1'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Domesticated Watercourse'),
    (select id from sets where short_name = 'cmb1'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Priority Avenger'),
    (select id from sets where short_name = 'cmb1'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Waste Land'),
    (select id from sets where short_name = 'cmb1'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slivdrazi Monstrosity'),
    (select id from sets where short_name = 'cmb1'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'One with Death'),
    (select id from sets where short_name = 'cmb1'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enroll in the Coalition'),
    (select id from sets where short_name = 'cmb1'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pithing Spyglass'),
    (select id from sets where short_name = 'cmb1'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Patient Turtle'),
    (select id from sets where short_name = 'cmb1'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chronobot'),
    (select id from sets where short_name = 'cmb1'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whammy Burn'),
    (select id from sets where short_name = 'cmb1'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enchantmentize'),
    (select id from sets where short_name = 'cmb1'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Grand Tour'),
    (select id from sets where short_name = 'cmb1'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yawgmoth''s Testament'),
    (select id from sets where short_name = 'cmb1'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Largepox'),
    (select id from sets where short_name = 'cmb1'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Buried Ogre'),
    (select id from sets where short_name = 'cmb1'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Biting Remark'),
    (select id from sets where short_name = 'cmb1'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Banding Sliver'),
    (select id from sets where short_name = 'cmb1'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seasoned Weaponsmith'),
    (select id from sets where short_name = 'cmb1'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Impatient Iguana'),
    (select id from sets where short_name = 'cmb1'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Siege Elemental'),
    (select id from sets where short_name = 'cmb1'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frenemy of the Guildpact'),
    (select id from sets where short_name = 'cmb1'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stack of Paperwork'),
    (select id from sets where short_name = 'cmb1'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Problematic Volcano'),
    (select id from sets where short_name = 'cmb1'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chimney Goyf'),
    (select id from sets where short_name = 'cmb1'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Red Herring'),
    (select id from sets where short_name = 'cmb1'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loopy Lobster'),
    (select id from sets where short_name = 'cmb1'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Planequake'),
    (select id from sets where short_name = 'cmb1'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestine Cave Witch'),
    (select id from sets where short_name = 'cmb1'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrath of Sod'),
    (select id from sets where short_name = 'cmb1'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jasconian Isle'),
    (select id from sets where short_name = 'cmb1'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Abundance'),
    (select id from sets where short_name = 'cmb1'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enchanted Prairie'),
    (select id from sets where short_name = 'cmb1'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Death Swarm'),
    (select id from sets where short_name = 'cmb1'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abian, Luvion Usurper'),
    (select id from sets where short_name = 'cmb1'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wizened Arbiter'),
    (select id from sets where short_name = 'cmb1'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Domesticated Mammoth'),
    (select id from sets where short_name = 'cmb1'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Recycla-bird'),
    (select id from sets where short_name = 'cmb1'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corrupted Key'),
    (select id from sets where short_name = 'cmb1'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seek Bolas''s Counsel'),
    (select id from sets where short_name = 'cmb1'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animate Spell'),
    (select id from sets where short_name = 'cmb1'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Interplanar Brushwagg'),
    (select id from sets where short_name = 'cmb1'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Visitor from Planet Q'),
    (select id from sets where short_name = 'cmb1'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ral''s Vanguard'),
    (select id from sets where short_name = 'cmb1'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Poet'),
    (select id from sets where short_name = 'cmb1'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Metagamer'),
    (select id from sets where short_name = 'cmb1'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unicycle'),
    (select id from sets where short_name = 'cmb1'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Do-Over'),
    (select id from sets where short_name = 'cmb1'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soulmates'),
    (select id from sets where short_name = 'cmb1'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Personal Decoy'),
    (select id from sets where short_name = 'cmb1'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lazier Goblin'),
    (select id from sets where short_name = 'cmb1'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frogkin Kidnapper'),
    (select id from sets where short_name = 'cmb1'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zyym, Mesmeric Lord'),
    (select id from sets where short_name = 'cmb1'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Learned Learner'),
    (select id from sets where short_name = 'cmb1'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Throat Wolf'),
    (select id from sets where short_name = 'cmb1'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Puresteel Angel'),
    (select id from sets where short_name = 'cmb1'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Colt'),
    (select id from sets where short_name = 'cmb1'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Baneslayer Aspirant'),
    (select id from sets where short_name = 'cmb1'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gorilla Tactics'),
    (select id from sets where short_name = 'cmb1'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Witty Demon'),
    (select id from sets where short_name = 'cmb1'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Command the Chaff'),
    (select id from sets where short_name = 'cmb1'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Generated Horizons'),
    (select id from sets where short_name = 'cmb1'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Control Win Condition'),
    (select id from sets where short_name = 'cmb1'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inspirational Antelope'),
    (select id from sets where short_name = 'cmb1'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cyclopean Titan'),
    (select id from sets where short_name = 'cmb1'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bind // Liberate'),
    (select id from sets where short_name = 'cmb1'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Experiment Five'),
    (select id from sets where short_name = 'cmb1'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Innocuous Insect'),
    (select id from sets where short_name = 'cmb1'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Time Sidewalk'),
    (select id from sets where short_name = 'cmb1'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'A Good Thing'),
    (select id from sets where short_name = 'cmb1'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Start // Fire'),
    (select id from sets where short_name = 'cmb1'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Louvaq, the Aberrant'),
    (select id from sets where short_name = 'cmb1'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Squidnapper'),
    (select id from sets where short_name = 'cmb1'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sarah''s Wings'),
    (select id from sets where short_name = 'cmb1'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rift'),
    (select id from sets where short_name = 'cmb1'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bombardment'),
    (select id from sets where short_name = 'cmb1'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pick Your Poison'),
    (select id from sets where short_name = 'cmb1'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bone Rattler'),
    (select id from sets where short_name = 'cmb1'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plane-Merge Elf'),
    (select id from sets where short_name = 'cmb1'),
    '83',
    'rare'
) 
 on conflict do nothing;
