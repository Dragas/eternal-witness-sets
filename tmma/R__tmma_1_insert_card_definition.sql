    insert into mtgcard(name) values ('Elspeth, Knight-Errant Emblem') on conflict do nothing;
    insert into mtgcard(name) values ('Elemental') on conflict do nothing;
    insert into mtgcard(name) values ('Saproling') on conflict do nothing;
    insert into mtgcard(name) values ('Kithkin Soldier') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin') on conflict do nothing;
    insert into mtgcard(name) values ('Spider') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Rogue') on conflict do nothing;
    insert into mtgcard(name) values ('Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Zombie') on conflict do nothing;
    insert into mtgcard(name) values ('Worm') on conflict do nothing;
    insert into mtgcard(name) values ('Bat') on conflict do nothing;
    insert into mtgcard(name) values ('Illusion') on conflict do nothing;
    insert into mtgcard(name) values ('Giant Warrior') on conflict do nothing;
    insert into mtgcard(name) values ('Soldier') on conflict do nothing;
    insert into mtgcard(name) values ('Treefolk Shaman') on conflict do nothing;
    insert into mtgcard(name) values ('Faerie Rogue') on conflict do nothing;
