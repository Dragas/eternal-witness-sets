insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Elspeth, Knight-Errant Emblem'),
    (select id from sets where short_name = 'tmma'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tmma'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tmma'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kithkin Soldier'),
    (select id from sets where short_name = 'tmma'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tmma'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spider'),
    (select id from sets where short_name = 'tmma'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Rogue'),
    (select id from sets where short_name = 'tmma'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tmma'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tmma'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Worm'),
    (select id from sets where short_name = 'tmma'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bat'),
    (select id from sets where short_name = 'tmma'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illusion'),
    (select id from sets where short_name = 'tmma'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Warrior'),
    (select id from sets where short_name = 'tmma'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tmma'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treefolk Shaman'),
    (select id from sets where short_name = 'tmma'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faerie Rogue'),
    (select id from sets where short_name = 'tmma'),
    '14',
    'common'
) 
 on conflict do nothing;
