insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'ta25'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'ta25'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skeleton'),
    (select id from sets where short_name = 'ta25'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'ta25'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insect'),
    (select id from sets where short_name = 'ta25'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'ta25'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kraken'),
    (select id from sets where short_name = 'ta25'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'ta25'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fish // Kraken'),
    (select id from sets where short_name = 'ta25'),
    '5★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elf Warrior'),
    (select id from sets where short_name = 'ta25'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fish'),
    (select id from sets where short_name = 'ta25'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whale'),
    (select id from sets where short_name = 'ta25'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Morph'),
    (select id from sets where short_name = 'ta25'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kobolds of Kher Keep'),
    (select id from sets where short_name = 'ta25'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stangg Twin'),
    (select id from sets where short_name = 'ta25'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'ta25'),
    '1',
    'common'
) 
 on conflict do nothing;
