insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Might Sliver'),
    (select id from sets where short_name = 'h09'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barbed Sliver'),
    (select id from sets where short_name = 'h09'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vivid Grove'),
    (select id from sets where short_name = 'h09'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Distant Melody'),
    (select id from sets where short_name = 'h09'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crystalline Sliver'),
    (select id from sets where short_name = 'h09'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Ziggurat'),
    (select id from sets where short_name = 'h09'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'h09'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Homing Sliver'),
    (select id from sets where short_name = 'h09'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sliver Overlord'),
    (select id from sets where short_name = 'h09'),
    '24',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gemhide Sliver'),
    (select id from sets where short_name = 'h09'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clot Sliver'),
    (select id from sets where short_name = 'h09'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heartstone'),
    (select id from sets where short_name = 'h09'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spined Sliver'),
    (select id from sets where short_name = 'h09'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Metallic Sliver'),
    (select id from sets where short_name = 'h09'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'h09'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aphetto Dredging'),
    (select id from sets where short_name = 'h09'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rootbound Crag'),
    (select id from sets where short_name = 'h09'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wild Pair'),
    (select id from sets where short_name = 'h09'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Muscle Sliver'),
    (select id from sets where short_name = 'h09'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'h09'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quick Sliver'),
    (select id from sets where short_name = 'h09'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hibernation Sliver'),
    (select id from sets where short_name = 'h09'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Amoeboid Changeling'),
    (select id from sets where short_name = 'h09'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necrotic Sliver'),
    (select id from sets where short_name = 'h09'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'h09'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frenzy Sliver'),
    (select id from sets where short_name = 'h09'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Victual Sliver'),
    (select id from sets where short_name = 'h09'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heart Sliver'),
    (select id from sets where short_name = 'h09'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'h09'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivid Creek'),
    (select id from sets where short_name = 'h09'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Virulent Sliver'),
    (select id from sets where short_name = 'h09'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fungus Sliver'),
    (select id from sets where short_name = 'h09'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rupture Spire'),
    (select id from sets where short_name = 'h09'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spectral Sliver'),
    (select id from sets where short_name = 'h09'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'h09'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fury Sliver'),
    (select id from sets where short_name = 'h09'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armor Sliver'),
    (select id from sets where short_name = 'h09'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Acidic Sliver'),
    (select id from sets where short_name = 'h09'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winged Sliver'),
    (select id from sets where short_name = 'h09'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brood Sliver'),
    (select id from sets where short_name = 'h09'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coat of Arms'),
    (select id from sets where short_name = 'h09'),
    '29',
    'rare'
) 
 on conflict do nothing;
