insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Chainer''s Edict'),
    (select id from sets where short_name = 'v13'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'v13'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cruel Ultimatum'),
    (select id from sets where short_name = 'v13'),
    '17',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jace, the Mind Sculptor'),
    (select id from sets where short_name = 'v13'),
    '18',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Impulse'),
    (select id from sets where short_name = 'v13'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kessig Wolf Run'),
    (select id from sets where short_name = 'v13'),
    '20',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wall of Blossoms'),
    (select id from sets where short_name = 'v13'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Akroma''s Vengeance'),
    (select id from sets where short_name = 'v13'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chameleon Colossus'),
    (select id from sets where short_name = 'v13'),
    '16',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tangle Wire'),
    (select id from sets where short_name = 'v13'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Green Sun''s Zenith'),
    (select id from sets where short_name = 'v13'),
    '19',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fact or Fiction'),
    (select id from sets where short_name = 'v13'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fyndhorn Elves'),
    (select id from sets where short_name = 'v13'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Venser, Shaper Savant'),
    (select id from sets where short_name = 'v13'),
    '15',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gilded Lotus'),
    (select id from sets where short_name = 'v13'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'v13'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Char'),
    (select id from sets where short_name = 'v13'),
    '14',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ink-Eyes, Servant of Oni'),
    (select id from sets where short_name = 'v13'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thran Dynamo'),
    (select id from sets where short_name = 'v13'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hymn to Tourach'),
    (select id from sets where short_name = 'v13'),
    '3',
    'mythic'
) 
 on conflict do nothing;
