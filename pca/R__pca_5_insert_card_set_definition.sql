insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Sigil of the Empty Throne'),
    (select id from sets where short_name = 'pca'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Blossoms'),
    (select id from sets where short_name = 'pca'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snake Umbra'),
    (select id from sets where short_name = 'pca'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skullsnatcher'),
    (select id from sets where short_name = 'pca'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elderwood Scion'),
    (select id from sets where short_name = 'pca'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hellkite Hatchling'),
    (select id from sets where short_name = 'pca'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cage of Hands'),
    (select id from sets where short_name = 'pca'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whirlpool Warrior'),
    (select id from sets where short_name = 'pca'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Concentrate'),
    (select id from sets where short_name = 'pca'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inkfathom Witch'),
    (select id from sets where short_name = 'pca'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ondu Giant'),
    (select id from sets where short_name = 'pca'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erratic Explosion'),
    (select id from sets where short_name = 'pca'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Predatory Urge'),
    (select id from sets where short_name = 'pca'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pca'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul Turf'),
    (select id from sets where short_name = 'pca'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enlisted Wurm'),
    (select id from sets where short_name = 'pca'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armored Griffin'),
    (select id from sets where short_name = 'pca'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fiery Fall'),
    (select id from sets where short_name = 'pca'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thran Golem'),
    (select id from sets where short_name = 'pca'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Assassinate'),
    (select id from sets where short_name = 'pca'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pca'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mark of Mutiny'),
    (select id from sets where short_name = 'pca'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pca'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rivals'' Duel'),
    (select id from sets where short_name = 'pca'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thromok the Insatiable'),
    (select id from sets where short_name = 'pca'),
    '106',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pca'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vitu-Ghazi, the City-Tree'),
    (select id from sets where short_name = 'pca'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pca'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mycoloth'),
    (select id from sets where short_name = 'pca'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nest Invader'),
    (select id from sets where short_name = 'pca'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dowsing Shaman'),
    (select id from sets where short_name = 'pca'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skarrg, the Rage Pits'),
    (select id from sets where short_name = 'pca'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodbraid Elf'),
    (select id from sets where short_name = 'pca'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fractured Powerstone'),
    (select id from sets where short_name = 'pca'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cadaver Imp'),
    (select id from sets where short_name = 'pca'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Indrik Umbra'),
    (select id from sets where short_name = 'pca'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Okiba-Gang Shinobi'),
    (select id from sets where short_name = 'pca'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Khalni Garden'),
    (select id from sets where short_name = 'pca'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Sanctuary'),
    (select id from sets where short_name = 'pca'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brutalizer Exarch'),
    (select id from sets where short_name = 'pca'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Preyseizer Dragon'),
    (select id from sets where short_name = 'pca'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pca'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Farsight Mask'),
    (select id from sets where short_name = 'pca'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jwar Isle Refuge'),
    (select id from sets where short_name = 'pca'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Exotic Orchard'),
    (select id from sets where short_name = 'pca'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kathari Remnant'),
    (select id from sets where short_name = 'pca'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pca'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rupture Spire'),
    (select id from sets where short_name = 'pca'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Three Dreams'),
    (select id from sets where short_name = 'pca'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Augury Owl'),
    (select id from sets where short_name = 'pca'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guard Gomazoa'),
    (select id from sets where short_name = 'pca'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Specter'),
    (select id from sets where short_name = 'pca'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fusion Elemental'),
    (select id from sets where short_name = 'pca'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mass Mutiny'),
    (select id from sets where short_name = 'pca'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ink-Eyes, Servant of Oni'),
    (select id from sets where short_name = 'pca'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arc Trail'),
    (select id from sets where short_name = 'pca'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Auratouched Mage'),
    (select id from sets where short_name = 'pca'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bituminous Blast'),
    (select id from sets where short_name = 'pca'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Peregrine Drake'),
    (select id from sets where short_name = 'pca'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whispersilk Cloak'),
    (select id from sets where short_name = 'pca'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viridian Emissary'),
    (select id from sets where short_name = 'pca'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pca'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glen Elendra Liege'),
    (select id from sets where short_name = 'pca'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rancor'),
    (select id from sets where short_name = 'pca'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tormented Soul'),
    (select id from sets where short_name = 'pca'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sai of the Shinobi'),
    (select id from sets where short_name = 'pca'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Illusory Angel'),
    (select id from sets where short_name = 'pca'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mudbutton Torchrunner'),
    (select id from sets where short_name = 'pca'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'See Beyond'),
    (select id from sets where short_name = 'pca'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fires of Yavimaya'),
    (select id from sets where short_name = 'pca'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pca'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tainted Isle'),
    (select id from sets where short_name = 'pca'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunken Hope'),
    (select id from sets where short_name = 'pca'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'pca'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivid Creek'),
    (select id from sets where short_name = 'pca'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quietus Spike'),
    (select id from sets where short_name = 'pca'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pca'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Walker of Secret Ways'),
    (select id from sets where short_name = 'pca'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pca'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Etherium-Horn Sorcerer'),
    (select id from sets where short_name = 'pca'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pca'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lumberknot'),
    (select id from sets where short_name = 'pca'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pca'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast Within'),
    (select id from sets where short_name = 'pca'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krosan Verge'),
    (select id from sets where short_name = 'pca'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pca'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overrun'),
    (select id from sets where short_name = 'pca'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Penumbra Spider'),
    (select id from sets where short_name = 'pca'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shimmering Grotto'),
    (select id from sets where short_name = 'pca'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Throat Slitter'),
    (select id from sets where short_name = 'pca'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Frost'),
    (select id from sets where short_name = 'pca'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silhana Ledgewalker'),
    (select id from sets where short_name = 'pca'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boar Umbra'),
    (select id from sets where short_name = 'pca'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mammoth Umbra'),
    (select id from sets where short_name = 'pca'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hellion Eruption'),
    (select id from sets where short_name = 'pca'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Graypelt Refuge'),
    (select id from sets where short_name = 'pca'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sakashima''s Student'),
    (select id from sets where short_name = 'pca'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pca'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunder-Thrash Elder'),
    (select id from sets where short_name = 'pca'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pca'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aura Gnarlid'),
    (select id from sets where short_name = 'pca'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreampod Druid'),
    (select id from sets where short_name = 'pca'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kazandu Refuge'),
    (select id from sets where short_name = 'pca'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fiery Conclusion'),
    (select id from sets where short_name = 'pca'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Wanderer'),
    (select id from sets where short_name = 'pca'),
    '101',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pca'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fling'),
    (select id from sets where short_name = 'pca'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pca'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brindle Shoat'),
    (select id from sets where short_name = 'pca'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mitotic Slime'),
    (select id from sets where short_name = 'pca'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Felidar Umbra'),
    (select id from sets where short_name = 'pca'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pca'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krond the Dawn-Clad'),
    (select id from sets where short_name = 'pca'),
    '99',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gluttonous Slime'),
    (select id from sets where short_name = 'pca'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beetleback Chief'),
    (select id from sets where short_name = 'pca'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragonlair Spider'),
    (select id from sets where short_name = 'pca'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noggle Ransacker'),
    (select id from sets where short_name = 'pca'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hissing Iguanar'),
    (select id from sets where short_name = 'pca'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pca'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thorn-Thrash Viashino'),
    (select id from sets where short_name = 'pca'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enigma Sphinx'),
    (select id from sets where short_name = 'pca'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spirit Mantle'),
    (select id from sets where short_name = 'pca'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Auramancer'),
    (select id from sets where short_name = 'pca'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shardless Agent'),
    (select id from sets where short_name = 'pca'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hyena Umbra'),
    (select id from sets where short_name = 'pca'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Celestial Ancient'),
    (select id from sets where short_name = 'pca'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dimir Infiltrator'),
    (select id from sets where short_name = 'pca'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ninja of the Deep Hours'),
    (select id from sets where short_name = 'pca'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Awakening Zone'),
    (select id from sets where short_name = 'pca'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pca'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dimir Aqueduct'),
    (select id from sets where short_name = 'pca'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bramble Elemental'),
    (select id from sets where short_name = 'pca'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cancel'),
    (select id from sets where short_name = 'pca'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vela the Night-Clad'),
    (select id from sets where short_name = 'pca'),
    '107',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Quiet Disrepair'),
    (select id from sets where short_name = 'pca'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silent-Blade Oni'),
    (select id from sets where short_name = 'pca'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Last Stand'),
    (select id from sets where short_name = 'pca'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pca'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pca'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pollenbright Wings'),
    (select id from sets where short_name = 'pca'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Baleful Strix'),
    (select id from sets where short_name = 'pca'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cultivate'),
    (select id from sets where short_name = 'pca'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warstorm Surge'),
    (select id from sets where short_name = 'pca'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tukatongue Thallid'),
    (select id from sets where short_name = 'pca'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armillary Sphere'),
    (select id from sets where short_name = 'pca'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Hatchling'),
    (select id from sets where short_name = 'pca'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pca'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pca'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flayer Husk'),
    (select id from sets where short_name = 'pca'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistblade Shinobi'),
    (select id from sets where short_name = 'pca'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primal Plasma'),
    (select id from sets where short_name = 'pca'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Higure, the Still Wind'),
    (select id from sets where short_name = 'pca'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nullmage Advocate'),
    (select id from sets where short_name = 'pca'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deny Reality'),
    (select id from sets where short_name = 'pca'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Spiritdancer'),
    (select id from sets where short_name = 'pca'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghostly Prison'),
    (select id from sets where short_name = 'pca'),
    '7',
    'uncommon'
) 
 on conflict do nothing;
