insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tdom'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Knight'),
    (select id from sets where short_name = 'tdom'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightmare Horror'),
    (select id from sets where short_name = 'tdom'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tdom'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon'),
    (select id from sets where short_name = 'tdom'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cleric'),
    (select id from sets where short_name = 'tdom'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karox Bladewing'),
    (select id from sets where short_name = 'tdom'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jaya Ballard Emblem'),
    (select id from sets where short_name = 'tdom'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tdom'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tdom'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teferi, Hero of Dominaria Emblem'),
    (select id from sets where short_name = 'tdom'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tdom'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Construct'),
    (select id from sets where short_name = 'tdom'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight'),
    (select id from sets where short_name = 'tdom'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight'),
    (select id from sets where short_name = 'tdom'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tdom'),
    '3',
    'common'
) 
 on conflict do nothing;
