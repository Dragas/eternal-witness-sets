insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'pwos'),
    '1',
    'rare'
) 
 on conflict do nothing;
