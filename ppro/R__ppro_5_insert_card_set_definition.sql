insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Mirari''s Wake'),
    (select id from sets where short_name = 'ppro'),
    '2008',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vraska, Golgari Queen'),
    (select id from sets where short_name = 'ppro'),
    'p2019-3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Liliana of the Veil'),
    (select id from sets where short_name = 'ppro'),
    '2015',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Arcbound Ravager'),
    (select id from sets where short_name = 'ppro'),
    '2019',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Surgical Extraction'),
    (select id from sets where short_name = 'ppro'),
    '2020-2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cryptic Command'),
    (select id from sets where short_name = 'ppro'),
    '2020-1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treva, the Renewer'),
    (select id from sets where short_name = 'ppro'),
    '2009',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana, Dreadhorde General'),
    (select id from sets where short_name = 'ppro'),
    'p2020-3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Emrakul, the Aeons Torn'),
    (select id from sets where short_name = 'ppro'),
    '2017',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Eternal Dragon'),
    (select id from sets where short_name = 'ppro'),
    '2007',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Experimental Frenzy'),
    (select id from sets where short_name = 'ppro'),
    'p2019-2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avatar of Woe'),
    (select id from sets where short_name = 'ppro'),
    '2010',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aether Vial'),
    (select id from sets where short_name = 'ppro'),
    '2020-3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa, Who Shakes the World'),
    (select id from sets where short_name = 'ppro'),
    'p2020-2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Narset, Parter of Veils'),
    (select id from sets where short_name = 'ppro'),
    'p2020-1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Snapcaster Mage'),
    (select id from sets where short_name = 'ppro'),
    '2016',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pteramander'),
    (select id from sets where short_name = 'ppro'),
    'p2019-1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ajani Goldmane'),
    (select id from sets where short_name = 'ppro'),
    '2011',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Noble Hierarch'),
    (select id from sets where short_name = 'ppro'),
    '2018',
    'rare'
) 
 on conflict do nothing;
