insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Frost Titan'),
    (select id from sets where short_name = 'pdp12'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Inferno Titan'),
    (select id from sets where short_name = 'pdp12'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Grave Titan'),
    (select id from sets where short_name = 'pdp12'),
    '2',
    'mythic'
) 
 on conflict do nothing;
