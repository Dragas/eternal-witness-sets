insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Humble'),
    (select id from sets where short_name = 'usg'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bravado'),
    (select id from sets where short_name = 'usg'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pariah'),
    (select id from sets where short_name = 'usg'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hidden Stag'),
    (select id from sets where short_name = 'usg'),
    '265',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lingering Mirage'),
    (select id from sets where short_name = 'usg'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Expunge'),
    (select id from sets where short_name = 'usg'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Redeem'),
    (select id from sets where short_name = 'usg'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Acridian'),
    (select id from sets where short_name = 'usg'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bereavement'),
    (select id from sets where short_name = 'usg'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viashino Sandswimmer'),
    (select id from sets where short_name = 'usg'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fertile Ground'),
    (select id from sets where short_name = 'usg'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Zealot'),
    (select id from sets where short_name = 'usg'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'usg'),
    '350',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arc Lightning'),
    (select id from sets where short_name = 'usg'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diabolic Servitude'),
    (select id from sets where short_name = 'usg'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sanctum Guardian'),
    (select id from sets where short_name = 'usg'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gamble'),
    (select id from sets where short_name = 'usg'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Purging Scythe'),
    (select id from sets where short_name = 'usg'),
    '308',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Intrepid Hero'),
    (select id from sets where short_name = 'usg'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unworthy Dead'),
    (select id from sets where short_name = 'usg'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Raptor'),
    (select id from sets where short_name = 'usg'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'usg'),
    '334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greater Good'),
    (select id from sets where short_name = 'usg'),
    '257',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Power Taint'),
    (select id from sets where short_name = 'usg'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steam Blast'),
    (select id from sets where short_name = 'usg'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra''s Embrace'),
    (select id from sets where short_name = 'usg'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Gorge'),
    (select id from sets where short_name = 'usg'),
    '326',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viashino Runner'),
    (select id from sets where short_name = 'usg'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Noetic Scales'),
    (select id from sets where short_name = 'usg'),
    '304',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imaginary Pet'),
    (select id from sets where short_name = 'usg'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voltaic Key'),
    (select id from sets where short_name = 'usg'),
    '314',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hidden Guerrillas'),
    (select id from sets where short_name = 'usg'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darkest Hour'),
    (select id from sets where short_name = 'usg'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Colossus'),
    (select id from sets where short_name = 'usg'),
    '305',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anaconda'),
    (select id from sets where short_name = 'usg'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Lyrist'),
    (select id from sets where short_name = 'usg'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rain of Filth'),
    (select id from sets where short_name = 'usg'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bull Hippo'),
    (select id from sets where short_name = 'usg'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'usg'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treefolk Seedlings'),
    (select id from sets where short_name = 'usg'),
    '278',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spreading Algae'),
    (select id from sets where short_name = 'usg'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hidden Herd'),
    (select id from sets where short_name = 'usg'),
    '262',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Worn Powerstone'),
    (select id from sets where short_name = 'usg'),
    '318',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Herald of Serra'),
    (select id from sets where short_name = 'usg'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crater Hellion'),
    (select id from sets where short_name = 'usg'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Veiled Serpent'),
    (select id from sets where short_name = 'usg'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enchantment Alteration'),
    (select id from sets where short_name = 'usg'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Cradle'),
    (select id from sets where short_name = 'usg'),
    '321',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viashino Outrider'),
    (select id from sets where short_name = 'usg'),
    '223',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctum Custodian'),
    (select id from sets where short_name = 'usg'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hidden Predators'),
    (select id from sets where short_name = 'usg'),
    '263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Monk Idealist'),
    (select id from sets where short_name = 'usg'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra Avatar'),
    (select id from sets where short_name = 'usg'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cathodion'),
    (select id from sets where short_name = 'usg'),
    '287',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rune of Protection: Red'),
    (select id from sets where short_name = 'usg'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Catastrophe'),
    (select id from sets where short_name = 'usg'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Umbilicus'),
    (select id from sets where short_name = 'usg'),
    '312',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'usg'),
    '346',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shimmering Barrier'),
    (select id from sets where short_name = 'usg'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Windfall'),
    (select id from sets where short_name = 'usg'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vile Requiem'),
    (select id from sets where short_name = 'usg'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Exploration'),
    (select id from sets where short_name = 'usg'),
    '250',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pestilence'),
    (select id from sets where short_name = 'usg'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcane Laboratory'),
    (select id from sets where short_name = 'usg'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Catalog'),
    (select id from sets where short_name = 'usg'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Corrupt'),
    (select id from sets where short_name = 'usg'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treetop Rangers'),
    (select id from sets where short_name = 'usg'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Dragon'),
    (select id from sets where short_name = 'usg'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pendrell Flux'),
    (select id from sets where short_name = 'usg'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hush'),
    (select id from sets where short_name = 'usg'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Raiders'),
    (select id from sets where short_name = 'usg'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opal Titan'),
    (select id from sets where short_name = 'usg'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reclusive Wight'),
    (select id from sets where short_name = 'usg'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'War Dance'),
    (select id from sets where short_name = 'usg'),
    '282',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vug Lizard'),
    (select id from sets where short_name = 'usg'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Opal Caryatid'),
    (select id from sets where short_name = 'usg'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grafted Skullcap'),
    (select id from sets where short_name = 'usg'),
    '296',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bog Raiders'),
    (select id from sets where short_name = 'usg'),
    '119s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Herder'),
    (select id from sets where short_name = 'usg'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brilliant Halo'),
    (select id from sets where short_name = 'usg'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'usg'),
    '345',
    'common'
) ,
(
    (select id from mtgcard where name = 'Looming Shade'),
    (select id from sets where short_name = 'usg'),
    '139s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scrap'),
    (select id from sets where short_name = 'usg'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'usg'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yawgmoth''s Edict'),
    (select id from sets where short_name = 'usg'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rune of Protection: Black'),
    (select id from sets where short_name = 'usg'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Annul'),
    (select id from sets where short_name = 'usg'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disorder'),
    (select id from sets where short_name = 'usg'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lifeline'),
    (select id from sets where short_name = 'usg'),
    '299',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scoria Wurm'),
    (select id from sets where short_name = 'usg'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'usg'),
    '338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Argothian Elder'),
    (select id from sets where short_name = 'usg'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hidden Ancients'),
    (select id from sets where short_name = 'usg'),
    '260',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lotus Blossom'),
    (select id from sets where short_name = 'usg'),
    '300',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carrion Beetles'),
    (select id from sets where short_name = 'usg'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lay Waste'),
    (select id from sets where short_name = 'usg'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hopping Automaton'),
    (select id from sets where short_name = 'usg'),
    '297',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rain of Salt'),
    (select id from sets where short_name = 'usg'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Destructive Urge'),
    (select id from sets where short_name = 'usg'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Parasitic Bond'),
    (select id from sets where short_name = 'usg'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Ghoul'),
    (select id from sets where short_name = 'usg'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jagged Lightning'),
    (select id from sets where short_name = 'usg'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Peregrine Drake'),
    (select id from sets where short_name = 'usg'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wizard Mentor'),
    (select id from sets where short_name = 'usg'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curfew'),
    (select id from sets where short_name = 'usg'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Outmaneuver'),
    (select id from sets where short_name = 'usg'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thran Quarry'),
    (select id from sets where short_name = 'usg'),
    '329',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Show and Tell'),
    (select id from sets where short_name = 'usg'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pit Trap'),
    (select id from sets where short_name = 'usg'),
    '307',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rune of Protection: Lands'),
    (select id from sets where short_name = 'usg'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Persecute'),
    (select id from sets where short_name = 'usg'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Tower'),
    (select id from sets where short_name = 'usg'),
    '322',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Telepathy'),
    (select id from sets where short_name = 'usg'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Turnabout'),
    (select id from sets where short_name = 'usg'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Looming Shade'),
    (select id from sets where short_name = 'usg'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Endless Wurm'),
    (select id from sets where short_name = 'usg'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shower of Sparks'),
    (select id from sets where short_name = 'usg'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fecundity'),
    (select id from sets where short_name = 'usg'),
    '251',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'usg'),
    '333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Order of Yawgmoth'),
    (select id from sets where short_name = 'usg'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Remembrance'),
    (select id from sets where short_name = 'usg'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Veil of Birds'),
    (select id from sets where short_name = 'usg'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greener Pastures'),
    (select id from sets where short_name = 'usg'),
    '258',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reprocess'),
    (select id from sets where short_name = 'usg'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raze'),
    (select id from sets where short_name = 'usg'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silent Attendant'),
    (select id from sets where short_name = 'usg'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Great Whale'),
    (select id from sets where short_name = 'usg'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Albino Troll'),
    (select id from sets where short_name = 'usg'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rescind'),
    (select id from sets where short_name = 'usg'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Launch'),
    (select id from sets where short_name = 'usg'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Absolute Law'),
    (select id from sets where short_name = 'usg'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Citanul Centaurs'),
    (select id from sets where short_name = 'usg'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temporal Aperture'),
    (select id from sets where short_name = 'usg'),
    '310',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tolarian Winds'),
    (select id from sets where short_name = 'usg'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanguine Guard'),
    (select id from sets where short_name = 'usg'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Helix'),
    (select id from sets where short_name = 'usg'),
    '302',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Symbiosis'),
    (select id from sets where short_name = 'usg'),
    '275',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faith Healer'),
    (select id from sets where short_name = 'usg'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stroke of Genius'),
    (select id from sets where short_name = 'usg'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mobile Fort'),
    (select id from sets where short_name = 'usg'),
    '303',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Okk'),
    (select id from sets where short_name = 'usg'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scald'),
    (select id from sets where short_name = 'usg'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Cadets'),
    (select id from sets where short_name = 'usg'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reflexes'),
    (select id from sets where short_name = 'usg'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glorious Anthem'),
    (select id from sets where short_name = 'usg'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whetstone'),
    (select id from sets where short_name = 'usg'),
    '316',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wirecat'),
    (select id from sets where short_name = 'usg'),
    '317',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rune of Protection: White'),
    (select id from sets where short_name = 'usg'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Priest of Gix'),
    (select id from sets where short_name = 'usg'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sandbar Merfolk'),
    (select id from sets where short_name = 'usg'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dromosaur'),
    (select id from sets where short_name = 'usg'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Power Sink'),
    (select id from sets where short_name = 'usg'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exhaustion'),
    (select id from sets where short_name = 'usg'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Bounty'),
    (select id from sets where short_name = 'usg'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Processor'),
    (select id from sets where short_name = 'usg'),
    '306',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hibernation'),
    (select id from sets where short_name = 'usg'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heat Ray'),
    (select id from sets where short_name = 'usg'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smokestack'),
    (select id from sets where short_name = 'usg'),
    '309',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fog Bank'),
    (select id from sets where short_name = 'usg'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Antagonism'),
    (select id from sets where short_name = 'usg'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Torch Song'),
    (select id from sets where short_name = 'usg'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Academy Researchers'),
    (select id from sets where short_name = 'usg'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slippery Karst'),
    (select id from sets where short_name = 'usg'),
    '327',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yawgmoth''s Will'),
    (select id from sets where short_name = 'usg'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elite Archers'),
    (select id from sets where short_name = 'usg'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hollow Dogs'),
    (select id from sets where short_name = 'usg'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stern Proctor'),
    (select id from sets where short_name = 'usg'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wildfire'),
    (select id from sets where short_name = 'usg'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voice of Grace'),
    (select id from sets where short_name = 'usg'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra''s Hymn'),
    (select id from sets where short_name = 'usg'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gorilla Warrior'),
    (select id from sets where short_name = 'usg'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Claws of Gix'),
    (select id from sets where short_name = 'usg'),
    '290',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Acidic Soil'),
    (select id from sets where short_name = 'usg'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thran Turbine'),
    (select id from sets where short_name = 'usg'),
    '311',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disciple of Law'),
    (select id from sets where short_name = 'usg'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Retaliation'),
    (select id from sets where short_name = 'usg'),
    '272',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Monk Realist'),
    (select id from sets where short_name = 'usg'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Dogs'),
    (select id from sets where short_name = 'usg'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shiv''s Embrace'),
    (select id from sets where short_name = 'usg'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Confiscate'),
    (select id from sets where short_name = 'usg'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Spelunkers'),
    (select id from sets where short_name = 'usg'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Vassal'),
    (select id from sets where short_name = 'usg'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vernal Bloom'),
    (select id from sets where short_name = 'usg'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Congregate'),
    (select id from sets where short_name = 'usg'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whirlwind'),
    (select id from sets where short_name = 'usg'),
    '283',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karn, Silver Golem'),
    (select id from sets where short_name = 'usg'),
    '298',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Morphling'),
    (select id from sets where short_name = 'usg'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crazed Skirge'),
    (select id from sets where short_name = 'usg'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Energy Field'),
    (select id from sets where short_name = 'usg'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ill-Gotten Gains'),
    (select id from sets where short_name = 'usg'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hermetic Study'),
    (select id from sets where short_name = 'usg'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coral Merfolk'),
    (select id from sets where short_name = 'usg'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opal Acrolith'),
    (select id from sets where short_name = 'usg'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sporogenesis'),
    (select id from sets where short_name = 'usg'),
    '273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disciple of Grace'),
    (select id from sets where short_name = 'usg'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hawkeater Moth'),
    (select id from sets where short_name = 'usg'),
    '259',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Presence of the Master'),
    (select id from sets where short_name = 'usg'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Electryte'),
    (select id from sets where short_name = 'usg'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rune of Protection: Blue'),
    (select id from sets where short_name = 'usg'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Contamination'),
    (select id from sets where short_name = 'usg'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Waylay'),
    (select id from sets where short_name = 'usg'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Hatchling'),
    (select id from sets where short_name = 'usg'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oppression'),
    (select id from sets where short_name = 'usg'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampiric Embrace'),
    (select id from sets where short_name = 'usg'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Healing Salve'),
    (select id from sets where short_name = 'usg'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spined Fluke'),
    (select id from sets where short_name = 'usg'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Leech'),
    (select id from sets where short_name = 'usg'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'usg'),
    '340',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cradle Guard'),
    (select id from sets where short_name = 'usg'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Hellkite'),
    (select id from sets where short_name = 'usg'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gilded Drake'),
    (select id from sets where short_name = 'usg'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'usg'),
    '335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Veiled Apparition'),
    (select id from sets where short_name = 'usg'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Citanul Hierophants'),
    (select id from sets where short_name = 'usg'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rune of Protection: Green'),
    (select id from sets where short_name = 'usg'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Chorus'),
    (select id from sets where short_name = 'usg'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Worship'),
    (select id from sets where short_name = 'usg'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fluctuator'),
    (select id from sets where short_name = 'usg'),
    '295',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'usg'),
    '337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Argothian Wurm'),
    (select id from sets where short_name = 'usg'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skirge Familiar'),
    (select id from sets where short_name = 'usg'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Douse'),
    (select id from sets where short_name = 'usg'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skittering Skirge'),
    (select id from sets where short_name = 'usg'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carpet of Flowers'),
    (select id from sets where short_name = 'usg'),
    '240',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abyssal Horror'),
    (select id from sets where short_name = 'usg'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Recantation'),
    (select id from sets where short_name = 'usg'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Priest of Titania'),
    (select id from sets where short_name = 'usg'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drifting Djinn'),
    (select id from sets where short_name = 'usg'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pendrell Drake'),
    (select id from sets where short_name = 'usg'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thundering Giant'),
    (select id from sets where short_name = 'usg'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barrin''s Codex'),
    (select id from sets where short_name = 'usg'),
    '286',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Argothian Enchantress'),
    (select id from sets where short_name = 'usg'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Horseshoe Crab'),
    (select id from sets where short_name = 'usg'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blanchwood Armor'),
    (select id from sets where short_name = 'usg'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Lackey'),
    (select id from sets where short_name = 'usg'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Veiled Crocodile'),
    (select id from sets where short_name = 'usg'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seasoned Marshal'),
    (select id from sets where short_name = 'usg'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rewind'),
    (select id from sets where short_name = 'usg'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meltdown'),
    (select id from sets where short_name = 'usg'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Matron'),
    (select id from sets where short_name = 'usg'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Headlong Rush'),
    (select id from sets where short_name = 'usg'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bulwark'),
    (select id from sets where short_name = 'usg'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chimeric Staff'),
    (select id from sets where short_name = 'usg'),
    '288',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'usg'),
    '332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bedlam'),
    (select id from sets where short_name = 'usg'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Opal Archangel'),
    (select id from sets where short_name = 'usg'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Child of Gaea'),
    (select id from sets where short_name = 'usg'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exhume'),
    (select id from sets where short_name = 'usg'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'usg'),
    '349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opal Gargoyle'),
    (select id from sets where short_name = 'usg'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drifting Meadow'),
    (select id from sets where short_name = 'usg'),
    '320',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sicken'),
    (select id from sets where short_name = 'usg'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hidden Spider'),
    (select id from sets where short_name = 'usg'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blasted Landscape'),
    (select id from sets where short_name = 'usg'),
    '319',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tolarian Academy'),
    (select id from sets where short_name = 'usg'),
    '330',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fire Ants'),
    (select id from sets where short_name = 'usg'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cackling Fiend'),
    (select id from sets where short_name = 'usg'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Path of Peace'),
    (select id from sets where short_name = 'usg'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Argothian Swine'),
    (select id from sets where short_name = 'usg'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Planar Void'),
    (select id from sets where short_name = 'usg'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sleeper Agent'),
    (select id from sets where short_name = 'usg'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'usg'),
    '347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sandbar Serpent'),
    (select id from sets where short_name = 'usg'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Midsummer Revel'),
    (select id from sets where short_name = 'usg'),
    '268',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra''s Liturgy'),
    (select id from sets where short_name = 'usg'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Titania''s Boon'),
    (select id from sets where short_name = 'usg'),
    '276',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'usg'),
    '331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brand'),
    (select id from sets where short_name = 'usg'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'usg'),
    '336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'usg'),
    '344',
    'common'
) ,
(
    (select id from mtgcard where name = 'Citanul Flute'),
    (select id from sets where short_name = 'usg'),
    '289',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'usg'),
    '339',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'usg'),
    '348',
    'common'
) ,
(
    (select id from mtgcard where name = 'No Rest for the Wicked'),
    (select id from sets where short_name = 'usg'),
    '142s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Somnophore'),
    (select id from sets where short_name = 'usg'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiery Mantle'),
    (select id from sets where short_name = 'usg'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Western Paladin'),
    (select id from sets where short_name = 'usg'),
    '168',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fire Ants'),
    (select id from sets where short_name = 'usg'),
    '187s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spire Owl'),
    (select id from sets where short_name = 'usg'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cave Tiger'),
    (select id from sets where short_name = 'usg'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Planar Birth'),
    (select id from sets where short_name = 'usg'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clear'),
    (select id from sets where short_name = 'usg'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'usg'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Victimize'),
    (select id from sets where short_name = 'usg'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fault Line'),
    (select id from sets where short_name = 'usg'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Embrace'),
    (select id from sets where short_name = 'usg'),
    '255',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Junk'),
    (select id from sets where short_name = 'usg'),
    '315',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunder'),
    (select id from sets where short_name = 'usg'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Endoskeleton'),
    (select id from sets where short_name = 'usg'),
    '294',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Polluted Mire'),
    (select id from sets where short_name = 'usg'),
    '323',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sneak Attack'),
    (select id from sets where short_name = 'usg'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ravenous Skirge'),
    (select id from sets where short_name = 'usg'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Patrol'),
    (select id from sets where short_name = 'usg'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tainted Aether'),
    (select id from sets where short_name = 'usg'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin War Buggy'),
    (select id from sets where short_name = 'usg'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diabolic Servitude'),
    (select id from sets where short_name = 'usg'),
    '130s',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pouncing Jaguar'),
    (select id from sets where short_name = 'usg'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Discordant Dirge'),
    (select id from sets where short_name = 'usg'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fortitude'),
    (select id from sets where short_name = 'usg'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vebulid'),
    (select id from sets where short_name = 'usg'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crystal Chimes'),
    (select id from sets where short_name = 'usg'),
    '292',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Copper Gnomes'),
    (select id from sets where short_name = 'usg'),
    '291',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guma'),
    (select id from sets where short_name = 'usg'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pegasus Charger'),
    (select id from sets where short_name = 'usg'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Metrognome'),
    (select id from sets where short_name = 'usg'),
    '301',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Titania''s Chosen'),
    (select id from sets where short_name = 'usg'),
    '277',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'No Rest for the Wicked'),
    (select id from sets where short_name = 'usg'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Voice of Law'),
    (select id from sets where short_name = 'usg'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'usg'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloak of Mists'),
    (select id from sets where short_name = 'usg'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rumbling Crescendo'),
    (select id from sets where short_name = 'usg'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Veiled Sentry'),
    (select id from sets where short_name = 'usg'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viashino Weaponsmith'),
    (select id from sets where short_name = 'usg'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Armor'),
    (select id from sets where short_name = 'usg'),
    '313',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zephid'),
    (select id from sets where short_name = 'usg'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sulfuric Vapors'),
    (select id from sets where short_name = 'usg'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Raider'),
    (select id from sets where short_name = 'usg'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rune of Protection: Artifacts'),
    (select id from sets where short_name = 'usg'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Falter'),
    (select id from sets where short_name = 'usg'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flesh Reaver'),
    (select id from sets where short_name = 'usg'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Remote Isle'),
    (select id from sets where short_name = 'usg'),
    '324',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Offensive'),
    (select id from sets where short_name = 'usg'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smoldering Crater'),
    (select id from sets where short_name = 'usg'),
    '328',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'usg'),
    '343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Retromancer'),
    (select id from sets where short_name = 'usg'),
    '209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Breach'),
    (select id from sets where short_name = 'usg'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barrin, Master Wizard'),
    (select id from sets where short_name = 'usg'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winding Wurm'),
    (select id from sets where short_name = 'usg'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disruptive Student'),
    (select id from sets where short_name = 'usg'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lull'),
    (select id from sets where short_name = 'usg'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lilting Refrain'),
    (select id from sets where short_name = 'usg'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Despondency'),
    (select id from sets where short_name = 'usg'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Witch Engine'),
    (select id from sets where short_name = 'usg'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eastern Paladin'),
    (select id from sets where short_name = 'usg'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crosswinds'),
    (select id from sets where short_name = 'usg'),
    '246',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Time Spiral'),
    (select id from sets where short_name = 'usg'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angelic Page'),
    (select id from sets where short_name = 'usg'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Venomous Fangs'),
    (select id from sets where short_name = 'usg'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lurking Evil'),
    (select id from sets where short_name = 'usg'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Attunement'),
    (select id from sets where short_name = 'usg'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abundance'),
    (select id from sets where short_name = 'usg'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unnerve'),
    (select id from sets where short_name = 'usg'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'usg'),
    '341',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra''s Sanctum'),
    (select id from sets where short_name = 'usg'),
    '325',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'usg'),
    '342',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blanchwood Treefolk'),
    (select id from sets where short_name = 'usg'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Befoul'),
    (select id from sets where short_name = 'usg'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Songstitcher'),
    (select id from sets where short_name = 'usg'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Back to Basics'),
    (select id from sets where short_name = 'usg'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zephid''s Embrace'),
    (select id from sets where short_name = 'usg'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unworthy Dead'),
    (select id from sets where short_name = 'usg'),
    '163s',
    'common'
) ,
(
    (select id from mtgcard where name = 'Defensive Formation'),
    (select id from sets where short_name = 'usg'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rejuvenate'),
    (select id from sets where short_name = 'usg'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Sculptor'),
    (select id from sets where short_name = 'usg'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Absolute Grace'),
    (select id from sets where short_name = 'usg'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Blood'),
    (select id from sets where short_name = 'usg'),
    '293',
    'uncommon'
) 
 on conflict do nothing;
