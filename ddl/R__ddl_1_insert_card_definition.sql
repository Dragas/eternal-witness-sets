    insert into mtgcard(name) values ('Terrifying Presence') on conflict do nothing;
    insert into mtgcard(name) values ('Forest') on conflict do nothing;
    insert into mtgcard(name) values ('Boros Guildgate') on conflict do nothing;
    insert into mtgcard(name) values ('Plains') on conflict do nothing;
    insert into mtgcard(name) values ('Daily Regimen') on conflict do nothing;
    insert into mtgcard(name) values ('Ordeal of Purphoros') on conflict do nothing;
    insert into mtgcard(name) values ('Fencing Ace') on conflict do nothing;
    insert into mtgcard(name) values ('Condemn') on conflict do nothing;
    insert into mtgcard(name) values ('Stun Sniper') on conflict do nothing;
    insert into mtgcard(name) values ('Undying Rage') on conflict do nothing;
    insert into mtgcard(name) values ('Freewind Equenaut') on conflict do nothing;
    insert into mtgcard(name) values ('Armory Guard') on conflict do nothing;
    insert into mtgcard(name) values ('Llanowar Reborn') on conflict do nothing;
    insert into mtgcard(name) values ('Orcish Lumberjack') on conflict do nothing;
    insert into mtgcard(name) values ('Crowned Ceratok') on conflict do nothing;
    insert into mtgcard(name) values ('Nobilis of War') on conflict do nothing;
    insert into mtgcard(name) values ('Winds of Rath') on conflict do nothing;
    insert into mtgcard(name) values ('Skarrgan Firebird') on conflict do nothing;
    insert into mtgcard(name) values ('Pay No Heed') on conflict do nothing;
    insert into mtgcard(name) values ('Somberwald Vigilante') on conflict do nothing;
    insert into mtgcard(name) values ('Ghor-Clan Savage') on conflict do nothing;
    insert into mtgcard(name) values ('Mountain') on conflict do nothing;
    insert into mtgcard(name) values ('Satyr Hedonist') on conflict do nothing;
    insert into mtgcard(name) values ('Fires of Yavimaya') on conflict do nothing;
    insert into mtgcard(name) values ('Dawnstrike Paladin') on conflict do nothing;
    insert into mtgcard(name) values ('Gustcloak Sentinel') on conflict do nothing;
    insert into mtgcard(name) values ('Troll Ascetic') on conflict do nothing;
    insert into mtgcard(name) values ('Pyrokinesis') on conflict do nothing;
    insert into mtgcard(name) values ('Kazandu Refuge') on conflict do nothing;
    insert into mtgcard(name) values ('Figure of Destiny') on conflict do nothing;
    insert into mtgcard(name) values ('Cavalry Pegasus') on conflict do nothing;
    insert into mtgcard(name) values ('Anax and Cymede') on conflict do nothing;
    insert into mtgcard(name) values ('Righteousness') on conflict do nothing;
    insert into mtgcard(name) values ('Miraculous Recovery') on conflict do nothing;
    insert into mtgcard(name) values ('Beast Within') on conflict do nothing;
    insert into mtgcard(name) values ('Prey Upon') on conflict do nothing;
    insert into mtgcard(name) values ('Shower of Sparks') on conflict do nothing;
    insert into mtgcard(name) values ('Deadly Recluse') on conflict do nothing;
    insert into mtgcard(name) values ('Polukranos, World Eater') on conflict do nothing;
    insert into mtgcard(name) values ('Stand Firm') on conflict do nothing;
    insert into mtgcard(name) values ('Volt Charge') on conflict do nothing;
    insert into mtgcard(name) values ('Smite the Monstrous') on conflict do nothing;
    insert into mtgcard(name) values ('Conquering Manticore') on conflict do nothing;
    insert into mtgcard(name) values ('Moment of Heroism') on conflict do nothing;
    insert into mtgcard(name) values ('Crater Hellion') on conflict do nothing;
    insert into mtgcard(name) values ('Auramancer') on conflict do nothing;
    insert into mtgcard(name) values ('Krosan Tusker') on conflict do nothing;
    insert into mtgcard(name) values ('Bonds of Faith') on conflict do nothing;
    insert into mtgcard(name) values ('Deus of Calamity') on conflict do nothing;
    insert into mtgcard(name) values ('Truefire Paladin') on conflict do nothing;
    insert into mtgcard(name) values ('Kamahl, Pit Fighter') on conflict do nothing;
    insert into mtgcard(name) values ('Thraben Valiant') on conflict do nothing;
    insert into mtgcard(name) values ('Magma Jet') on conflict do nothing;
    insert into mtgcard(name) values ('Destructive Revelry') on conflict do nothing;
    insert into mtgcard(name) values ('Kavu Predator') on conflict do nothing;
    insert into mtgcard(name) values ('Pyroclasm') on conflict do nothing;
    insert into mtgcard(name) values ('Zhur-Taa Druid') on conflict do nothing;
    insert into mtgcard(name) values ('Gorehorn Minotaurs') on conflict do nothing;
    insert into mtgcard(name) values ('Skarrgan Skybreaker') on conflict do nothing;
    insert into mtgcard(name) values ('Battle Mastery') on conflict do nothing;
    insert into mtgcard(name) values ('Dragon Blood') on conflict do nothing;
    insert into mtgcard(name) values ('New Benalia') on conflict do nothing;
    insert into mtgcard(name) values ('Griffin Guide') on conflict do nothing;
    insert into mtgcard(name) values ('Regrowth') on conflict do nothing;
    insert into mtgcard(name) values ('Blood Ogre') on conflict do nothing;
    insert into mtgcard(name) values ('Valley Rannet') on conflict do nothing;
    insert into mtgcard(name) values ('Sun Titan') on conflict do nothing;
    insert into mtgcard(name) values ('Skarrg, the Rage Pits') on conflict do nothing;
