insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Terrifying Presence'),
    (select id from sets where short_name = 'ddl'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddl'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boros Guildgate'),
    (select id from sets where short_name = 'ddl'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddl'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daily Regimen'),
    (select id from sets where short_name = 'ddl'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ordeal of Purphoros'),
    (select id from sets where short_name = 'ddl'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fencing Ace'),
    (select id from sets where short_name = 'ddl'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Condemn'),
    (select id from sets where short_name = 'ddl'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stun Sniper'),
    (select id from sets where short_name = 'ddl'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Undying Rage'),
    (select id from sets where short_name = 'ddl'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Freewind Equenaut'),
    (select id from sets where short_name = 'ddl'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armory Guard'),
    (select id from sets where short_name = 'ddl'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Reborn'),
    (select id from sets where short_name = 'ddl'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orcish Lumberjack'),
    (select id from sets where short_name = 'ddl'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crowned Ceratok'),
    (select id from sets where short_name = 'ddl'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nobilis of War'),
    (select id from sets where short_name = 'ddl'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winds of Rath'),
    (select id from sets where short_name = 'ddl'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skarrgan Firebird'),
    (select id from sets where short_name = 'ddl'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pay No Heed'),
    (select id from sets where short_name = 'ddl'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Somberwald Vigilante'),
    (select id from sets where short_name = 'ddl'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghor-Clan Savage'),
    (select id from sets where short_name = 'ddl'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddl'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Satyr Hedonist'),
    (select id from sets where short_name = 'ddl'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fires of Yavimaya'),
    (select id from sets where short_name = 'ddl'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dawnstrike Paladin'),
    (select id from sets where short_name = 'ddl'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gustcloak Sentinel'),
    (select id from sets where short_name = 'ddl'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddl'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Troll Ascetic'),
    (select id from sets where short_name = 'ddl'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyrokinesis'),
    (select id from sets where short_name = 'ddl'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kazandu Refuge'),
    (select id from sets where short_name = 'ddl'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Figure of Destiny'),
    (select id from sets where short_name = 'ddl'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cavalry Pegasus'),
    (select id from sets where short_name = 'ddl'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddl'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anax and Cymede'),
    (select id from sets where short_name = 'ddl'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Righteousness'),
    (select id from sets where short_name = 'ddl'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Miraculous Recovery'),
    (select id from sets where short_name = 'ddl'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beast Within'),
    (select id from sets where short_name = 'ddl'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prey Upon'),
    (select id from sets where short_name = 'ddl'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shower of Sparks'),
    (select id from sets where short_name = 'ddl'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadly Recluse'),
    (select id from sets where short_name = 'ddl'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddl'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Polukranos, World Eater'),
    (select id from sets where short_name = 'ddl'),
    '43',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stand Firm'),
    (select id from sets where short_name = 'ddl'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volt Charge'),
    (select id from sets where short_name = 'ddl'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddl'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smite the Monstrous'),
    (select id from sets where short_name = 'ddl'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conquering Manticore'),
    (select id from sets where short_name = 'ddl'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moment of Heroism'),
    (select id from sets where short_name = 'ddl'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crater Hellion'),
    (select id from sets where short_name = 'ddl'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Auramancer'),
    (select id from sets where short_name = 'ddl'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krosan Tusker'),
    (select id from sets where short_name = 'ddl'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bonds of Faith'),
    (select id from sets where short_name = 'ddl'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deus of Calamity'),
    (select id from sets where short_name = 'ddl'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Truefire Paladin'),
    (select id from sets where short_name = 'ddl'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kamahl, Pit Fighter'),
    (select id from sets where short_name = 'ddl'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thraben Valiant'),
    (select id from sets where short_name = 'ddl'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddl'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddl'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddl'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magma Jet'),
    (select id from sets where short_name = 'ddl'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddl'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Destructive Revelry'),
    (select id from sets where short_name = 'ddl'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddl'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kavu Predator'),
    (select id from sets where short_name = 'ddl'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddl'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyroclasm'),
    (select id from sets where short_name = 'ddl'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zhur-Taa Druid'),
    (select id from sets where short_name = 'ddl'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gorehorn Minotaurs'),
    (select id from sets where short_name = 'ddl'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skarrgan Skybreaker'),
    (select id from sets where short_name = 'ddl'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battle Mastery'),
    (select id from sets where short_name = 'ddl'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Blood'),
    (select id from sets where short_name = 'ddl'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'New Benalia'),
    (select id from sets where short_name = 'ddl'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddl'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Griffin Guide'),
    (select id from sets where short_name = 'ddl'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Regrowth'),
    (select id from sets where short_name = 'ddl'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blood Ogre'),
    (select id from sets where short_name = 'ddl'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Valley Rannet'),
    (select id from sets where short_name = 'ddl'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sun Titan'),
    (select id from sets where short_name = 'ddl'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddl'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddl'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skarrg, the Rage Pits'),
    (select id from sets where short_name = 'ddl'),
    '73',
    'uncommon'
) 
 on conflict do nothing;
