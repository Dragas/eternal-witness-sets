insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Timid Drake'),
    (select id from sets where short_name = 'wth'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Redwood Treefolk'),
    (select id from sets where short_name = 'wth'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fatal Blow'),
    (select id from sets where short_name = 'wth'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mischievous Poltergeist'),
    (select id from sets where short_name = 'wth'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancestral Knowledge'),
    (select id from sets where short_name = 'wth'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dingus Staff'),
    (select id from sets where short_name = 'wth'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gemstone Mine'),
    (select id from sets where short_name = 'wth'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fit of Rage'),
    (select id from sets where short_name = 'wth'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Striped Bears'),
    (select id from sets where short_name = 'wth'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bubble Matrix'),
    (select id from sets where short_name = 'wth'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foriysian Brigade'),
    (select id from sets where short_name = 'wth'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ophidian'),
    (select id from sets where short_name = 'wth'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shattered Crypt'),
    (select id from sets where short_name = 'wth'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abeyance'),
    (select id from sets where short_name = 'wth'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Stone'),
    (select id from sets where short_name = 'wth'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abyssal Gatekeeper'),
    (select id from sets where short_name = 'wth'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fledgling Djinn'),
    (select id from sets where short_name = 'wth'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rogue Elephant'),
    (select id from sets where short_name = 'wth'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fallow Wurm'),
    (select id from sets where short_name = 'wth'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heat Stroke'),
    (select id from sets where short_name = 'wth'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bone Dancer'),
    (select id from sets where short_name = 'wth'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inner Sanctum'),
    (select id from sets where short_name = 'wth'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tariff'),
    (select id from sets where short_name = 'wth'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roc Hatchling'),
    (select id from sets where short_name = 'wth'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra''s Blessing'),
    (select id from sets where short_name = 'wth'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fog Elemental'),
    (select id from sets where short_name = 'wth'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Flash'),
    (select id from sets where short_name = 'wth'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Southern Paladin'),
    (select id from sets where short_name = 'wth'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Web'),
    (select id from sets where short_name = 'wth'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urborg Stalker'),
    (select id from sets where short_name = 'wth'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Festering Evil'),
    (select id from sets where short_name = 'wth'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nature''s Resurgence'),
    (select id from sets where short_name = 'wth'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jangling Automaton'),
    (select id from sets where short_name = 'wth'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lotus Vale'),
    (select id from sets where short_name = 'wth'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zombie Scavengers'),
    (select id from sets where short_name = 'wth'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blossoming Wreath'),
    (select id from sets where short_name = 'wth'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ertai''s Familiar'),
    (select id from sets where short_name = 'wth'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uktabi Efreet'),
    (select id from sets where short_name = 'wth'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flux'),
    (select id from sets where short_name = 'wth'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bösium Strip'),
    (select id from sets where short_name = 'wth'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kithkin Armor'),
    (select id from sets where short_name = 'wth'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urborg Justice'),
    (select id from sets where short_name = 'wth'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alabaster Dragon'),
    (select id from sets where short_name = 'wth'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Odylic Wraith'),
    (select id from sets where short_name = 'wth'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Psychic Vortex'),
    (select id from sets where short_name = 'wth'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jabari''s Banner'),
    (select id from sets where short_name = 'wth'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llanowar Behemoth'),
    (select id from sets where short_name = 'wth'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fire Whip'),
    (select id from sets where short_name = 'wth'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sage Owl'),
    (select id from sets where short_name = 'wth'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boiling Blood'),
    (select id from sets where short_name = 'wth'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Hierophant'),
    (select id from sets where short_name = 'wth'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fungus Elemental'),
    (select id from sets where short_name = 'wth'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Straw Golem'),
    (select id from sets where short_name = 'wth'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sawtooth Ogre'),
    (select id from sets where short_name = 'wth'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heart of Bogardan'),
    (select id from sets where short_name = 'wth'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coils of the Medusa'),
    (select id from sets where short_name = 'wth'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Well of Knowledge'),
    (select id from sets where short_name = 'wth'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volunteer Reserves'),
    (select id from sets where short_name = 'wth'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Empyrial Armor'),
    (select id from sets where short_name = 'wth'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infernal Tribute'),
    (select id from sets where short_name = 'wth'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Buried Alive'),
    (select id from sets where short_name = 'wth'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cone of Flame'),
    (select id from sets where short_name = 'wth'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aura of Silence'),
    (select id from sets where short_name = 'wth'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spinning Darkness'),
    (select id from sets where short_name = 'wth'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Peacekeeper'),
    (select id from sets where short_name = 'wth'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maraxus of Keld'),
    (select id from sets where short_name = 'wth'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Grenadiers'),
    (select id from sets where short_name = 'wth'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doomsday'),
    (select id from sets where short_name = 'wth'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shadow Rider'),
    (select id from sets where short_name = 'wth'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodrock Cyclops'),
    (select id from sets where short_name = 'wth'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Betrothed of Fire'),
    (select id from sets where short_name = 'wth'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lava Storm'),
    (select id from sets where short_name = 'wth'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thran Tome'),
    (select id from sets where short_name = 'wth'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nature''s Kiss'),
    (select id from sets where short_name = 'wth'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Briar Shield'),
    (select id from sets where short_name = 'wth'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cinder Wall'),
    (select id from sets where short_name = 'wth'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Choking Vines'),
    (select id from sets where short_name = 'wth'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Sentinel'),
    (select id from sets where short_name = 'wth'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hurloon Shaman'),
    (select id from sets where short_name = 'wth'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scorched Ruins'),
    (select id from sets where short_name = 'wth'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Chains'),
    (select id from sets where short_name = 'wth'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Berserker'),
    (select id from sets where short_name = 'wth'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Renewal'),
    (select id from sets where short_name = 'wth'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manta Ray'),
    (select id from sets where short_name = 'wth'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bogardan Firefiend'),
    (select id from sets where short_name = 'wth'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strands of Night'),
    (select id from sets where short_name = 'wth'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hidden Horror'),
    (select id from sets where short_name = 'wth'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lava Hounds'),
    (select id from sets where short_name = 'wth'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Argivian Find'),
    (select id from sets where short_name = 'wth'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Xanthic Statue'),
    (select id from sets where short_name = 'wth'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Revered Unicorn'),
    (select id from sets where short_name = 'wth'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aboroth'),
    (select id from sets where short_name = 'wth'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circling Vultures'),
    (select id from sets where short_name = 'wth'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gallowbraid'),
    (select id from sets where short_name = 'wth'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dwarven Thaumaturgist'),
    (select id from sets where short_name = 'wth'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mistmoon Griffin'),
    (select id from sets where short_name = 'wth'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Relearn'),
    (select id from sets where short_name = 'wth'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tolarian Serpent'),
    (select id from sets where short_name = 'wth'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Touchstone'),
    (select id from sets where short_name = 'wth'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Haunting Misery'),
    (select id from sets where short_name = 'wth'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alms'),
    (select id from sets where short_name = 'wth'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Bomb'),
    (select id from sets where short_name = 'wth'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steel Golem'),
    (select id from sets where short_name = 'wth'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vodalian Illusionist'),
    (select id from sets where short_name = 'wth'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harvest Wurm'),
    (select id from sets where short_name = 'wth'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Argivian Restoration'),
    (select id from sets where short_name = 'wth'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thunderbolt'),
    (select id from sets where short_name = 'wth'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Wings'),
    (select id from sets where short_name = 'wth'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Call of the Wild'),
    (select id from sets where short_name = 'wth'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avizoa'),
    (select id from sets where short_name = 'wth'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mwonvuli Ooze'),
    (select id from sets where short_name = 'wth'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Downdraft'),
    (select id from sets where short_name = 'wth'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abduction'),
    (select id from sets where short_name = 'wth'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Master of Arms'),
    (select id from sets where short_name = 'wth'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Apathy'),
    (select id from sets where short_name = 'wth'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Razortooth Rats'),
    (select id from sets where short_name = 'wth'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Warrior'),
    (select id from sets where short_name = 'wth'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fervor'),
    (select id from sets where short_name = 'wth'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Morinfen'),
    (select id from sets where short_name = 'wth'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tendrils of Despair'),
    (select id from sets where short_name = 'wth'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arctic Wolves'),
    (select id from sets where short_name = 'wth'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Vandal'),
    (select id from sets where short_name = 'wth'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pendrell Mists'),
    (select id from sets where short_name = 'wth'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thran Forge'),
    (select id from sets where short_name = 'wth'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ardent Militia'),
    (select id from sets where short_name = 'wth'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Null Rod'),
    (select id from sets where short_name = 'wth'),
    '154',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noble Benefactor'),
    (select id from sets where short_name = 'wth'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Veteran Explorer'),
    (select id from sets where short_name = 'wth'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heavy Ballista'),
    (select id from sets where short_name = 'wth'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Agonizing Memories'),
    (select id from sets where short_name = 'wth'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duskrider Falcon'),
    (select id from sets where short_name = 'wth'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tolarian Entrancer'),
    (select id from sets where short_name = 'wth'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benalish Knight'),
    (select id from sets where short_name = 'wth'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serrated Biskelion'),
    (select id from sets where short_name = 'wth'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquil Grove'),
    (select id from sets where short_name = 'wth'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dense Foliage'),
    (select id from sets where short_name = 'wth'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disrupt'),
    (select id from sets where short_name = 'wth'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guided Strike'),
    (select id from sets where short_name = 'wth'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloud Djinn'),
    (select id from sets where short_name = 'wth'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Veil'),
    (select id from sets where short_name = 'wth'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chimeric Sphere'),
    (select id from sets where short_name = 'wth'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Debt of Loyalty'),
    (select id from sets where short_name = 'wth'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wave of Terror'),
    (select id from sets where short_name = 'wth'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Settlers'),
    (select id from sets where short_name = 'wth'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vitalize'),
    (select id from sets where short_name = 'wth'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serenity'),
    (select id from sets where short_name = 'wth'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cinder Giant'),
    (select id from sets where short_name = 'wth'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liege of the Hollows'),
    (select id from sets where short_name = 'wth'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tolarian Drake'),
    (select id from sets where short_name = 'wth'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paradigm Shift'),
    (select id from sets where short_name = 'wth'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firestorm'),
    (select id from sets where short_name = 'wth'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benalish Infantry'),
    (select id from sets where short_name = 'wth'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thundermare'),
    (select id from sets where short_name = 'wth'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Furnace'),
    (select id from sets where short_name = 'wth'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Benalish Missionary'),
    (select id from sets where short_name = 'wth'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk Traders'),
    (select id from sets where short_name = 'wth'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Blessing'),
    (select id from sets where short_name = 'wth'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barishi'),
    (select id from sets where short_name = 'wth'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winding Canyons'),
    (select id from sets where short_name = 'wth'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desperate Gambit'),
    (select id from sets where short_name = 'wth'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Shepherd'),
    (select id from sets where short_name = 'wth'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barrow Ghoul'),
    (select id from sets where short_name = 'wth'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gerrard''s Wisdom'),
    (select id from sets where short_name = 'wth'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Familiar Ground'),
    (select id from sets where short_name = 'wth'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necratog'),
    (select id from sets where short_name = 'wth'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abjure'),
    (select id from sets where short_name = 'wth'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Druid'),
    (select id from sets where short_name = 'wth'),
    '133',
    'common'
) 
 on conflict do nothing;
