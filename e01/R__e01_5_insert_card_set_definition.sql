insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Baleful Strix'),
    (select id from sets where short_name = 'e01'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'e01'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archfiend of Depravity'),
    (select id from sets where short_name = 'e01'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vastwood Zendikon'),
    (select id from sets where short_name = 'e01'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aerial Responder'),
    (select id from sets where short_name = 'e01'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Phoenix'),
    (select id from sets where short_name = 'e01'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Grixis'),
    (select id from sets where short_name = 'e01'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grasp of the Hieromancer'),
    (select id from sets where short_name = 'e01'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Nighthawk'),
    (select id from sets where short_name = 'e01'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Talisman of Indulgence'),
    (select id from sets where short_name = 'e01'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ior Ruin Expedition'),
    (select id from sets where short_name = 'e01'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu'),
    (select id from sets where short_name = 'e01'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fertilid'),
    (select id from sets where short_name = 'e01'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreadbore'),
    (select id from sets where short_name = 'e01'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drowned Catacomb'),
    (select id from sets where short_name = 'e01'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hammerhand'),
    (select id from sets where short_name = 'e01'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Scholar'),
    (select id from sets where short_name = 'e01'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anointer of Champions'),
    (select id from sets where short_name = 'e01'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grixis Panorama'),
    (select id from sets where short_name = 'e01'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'e01'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'e01'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Compulsive Research'),
    (select id from sets where short_name = 'e01'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aegis Angel'),
    (select id from sets where short_name = 'e01'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'e01'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Turntimber Basilisk'),
    (select id from sets where short_name = 'e01'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blood Ogre'),
    (select id from sets where short_name = 'e01'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormblood Berserker'),
    (select id from sets where short_name = 'e01'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inferno Titan'),
    (select id from sets where short_name = 'e01'),
    '53',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Talisman of Dominance'),
    (select id from sets where short_name = 'e01'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Searing Spear'),
    (select id from sets where short_name = 'e01'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Khalni Heart Expedition'),
    (select id from sets where short_name = 'e01'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obsidian Fireheart'),
    (select id from sets where short_name = 'e01'),
    '55',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nissa, Worldwaker'),
    (select id from sets where short_name = 'e01'),
    '68',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Harvester of Souls'),
    (select id from sets where short_name = 'e01'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Bounty'),
    (select id from sets where short_name = 'e01'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'e01'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cultivate'),
    (select id from sets where short_name = 'e01'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Spite'),
    (select id from sets where short_name = 'e01'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fencing Ace'),
    (select id from sets where short_name = 'e01'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sphinx of Jwar Isle'),
    (select id from sets where short_name = 'e01'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coordinated Assault'),
    (select id from sets where short_name = 'e01'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Extract from Darkness'),
    (select id from sets where short_name = 'e01'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fiendslayer Paladin'),
    (select id from sets where short_name = 'e01'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'e01'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vines of the Recluse'),
    (select id from sets where short_name = 'e01'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightscape Familiar'),
    (select id from sets where short_name = 'e01'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flickerwisp'),
    (select id from sets where short_name = 'e01'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woodborn Behemoth'),
    (select id from sets where short_name = 'e01'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blood Tyrant'),
    (select id from sets where short_name = 'e01'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Explore'),
    (select id from sets where short_name = 'e01'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gideon''s Lawkeeper'),
    (select id from sets where short_name = 'e01'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dualcaster Mage'),
    (select id from sets where short_name = 'e01'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vision Skeins'),
    (select id from sets where short_name = 'e01'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relief Captain'),
    (select id from sets where short_name = 'e01'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chandra, Pyromaster'),
    (select id from sets where short_name = 'e01'),
    '42',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dragonskull Summit'),
    (select id from sets where short_name = 'e01'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'e01'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathbringer Regent'),
    (select id from sets where short_name = 'e01'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prognostic Sphinx'),
    (select id from sets where short_name = 'e01'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gorehorn Minotaurs'),
    (select id from sets where short_name = 'e01'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windrider Eel'),
    (select id from sets where short_name = 'e01'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hunter''s Prowess'),
    (select id from sets where short_name = 'e01'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sudden Demise'),
    (select id from sets where short_name = 'e01'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Outrage'),
    (select id from sets where short_name = 'e01'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crumbling Necropolis'),
    (select id from sets where short_name = 'e01'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'e01'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Geyser'),
    (select id from sets where short_name = 'e01'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forgotten Ancient'),
    (select id from sets where short_name = 'e01'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shoulder to Shoulder'),
    (select id from sets where short_name = 'e01'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doomed Traveler'),
    (select id from sets where short_name = 'e01'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, Planeswalker'),
    (select id from sets where short_name = 'e01'),
    '85',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Odric, Master Tactician'),
    (select id from sets where short_name = 'e01'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Torchling'),
    (select id from sets where short_name = 'e01'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Battle-Rattle Shaman'),
    (select id from sets where short_name = 'e01'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Press the Advantage'),
    (select id from sets where short_name = 'e01'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sword of the Animist'),
    (select id from sets where short_name = 'e01'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Icefall Regent'),
    (select id from sets where short_name = 'e01'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slave of Bolas'),
    (select id from sets where short_name = 'e01'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Retreat to Kazandu'),
    (select id from sets where short_name = 'e01'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grand Abolisher'),
    (select id from sets where short_name = 'e01'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightwielder Paladin'),
    (select id from sets where short_name = 'e01'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Youthful Knight'),
    (select id from sets where short_name = 'e01'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Ransom'),
    (select id from sets where short_name = 'e01'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skarrgan Firebird'),
    (select id from sets where short_name = 'e01'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Excoriate'),
    (select id from sets where short_name = 'e01'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'e01'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampaging Baloths'),
    (select id from sets where short_name = 'e01'),
    '71',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scute Mob'),
    (select id from sets where short_name = 'e01'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overseer of the Damned'),
    (select id from sets where short_name = 'e01'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Expedition Raptor'),
    (select id from sets where short_name = 'e01'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fiery Fall'),
    (select id from sets where short_name = 'e01'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mentor of the Meek'),
    (select id from sets where short_name = 'e01'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'e01'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sun Titan'),
    (select id from sets where short_name = 'e01'),
    '21',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cruel Ultimatum'),
    (select id from sets where short_name = 'e01'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Doom Blade'),
    (select id from sets where short_name = 'e01'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smoldering Spires'),
    (select id from sets where short_name = 'e01'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grim Lavamancer'),
    (select id from sets where short_name = 'e01'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thragtusk'),
    (select id from sets where short_name = 'e01'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moment of Heroism'),
    (select id from sets where short_name = 'e01'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avatar of Fury'),
    (select id from sets where short_name = 'e01'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'e01'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gideon Jura'),
    (select id from sets where short_name = 'e01'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Precinct Captain'),
    (select id from sets where short_name = 'e01'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guttersnipe'),
    (select id from sets where short_name = 'e01'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief Hydra'),
    (select id from sets where short_name = 'e01'),
    '69',
    'rare'
) 
 on conflict do nothing;
