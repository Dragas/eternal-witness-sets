insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Phoberos Reaver'),
    (select id from sets where short_name = 'tbth'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minotaur Younghorn'),
    (select id from sets where short_name = 'tbth'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minotaur Goreseeker'),
    (select id from sets where short_name = 'tbth'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mogis''s Chosen'),
    (select id from sets where short_name = 'tbth'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Intervention of Keranos'),
    (select id from sets where short_name = 'tbth'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vitality Salve'),
    (select id from sets where short_name = 'tbth'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Consuming Rage'),
    (select id from sets where short_name = 'tbth'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plundered Statue'),
    (select id from sets where short_name = 'tbth'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Minotaur'),
    (select id from sets where short_name = 'tbth'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Descend on the Prey'),
    (select id from sets where short_name = 'tbth'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Massacre Totem'),
    (select id from sets where short_name = 'tbth'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Refreshing Elixir'),
    (select id from sets where short_name = 'tbth'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Touch of the Horned God'),
    (select id from sets where short_name = 'tbth'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unquenchable Fury'),
    (select id from sets where short_name = 'tbth'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Altar of Mogis'),
    (select id from sets where short_name = 'tbth'),
    '11',
    'common'
) 
 on conflict do nothing;
