insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Pia Nalaar'),
    (select id from sets where short_name = 'pkld'),
    '124s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Metallurgic Summonings'),
    (select id from sets where short_name = 'pkld'),
    '56s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blooming Marsh'),
    (select id from sets where short_name = 'pkld'),
    '243s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spirebluff Canal'),
    (select id from sets where short_name = 'pkld'),
    '249s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fumigate'),
    (select id from sets where short_name = 'pkld'),
    '15p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bristling Hydra'),
    (select id from sets where short_name = 'pkld'),
    '147s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Combustible Gearhulk'),
    (select id from sets where short_name = 'pkld'),
    '112p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Inspiring Vantage'),
    (select id from sets where short_name = 'pkld'),
    '246s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Padeem, Consul of Innovation'),
    (select id from sets where short_name = 'pkld'),
    '59s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Authority of the Consuls'),
    (select id from sets where short_name = 'pkld'),
    '5s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghirapur Orrery'),
    (select id from sets where short_name = 'pkld'),
    '216s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aetherflux Reservoir'),
    (select id from sets where short_name = 'pkld'),
    '192s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cataclysmic Gearhulk'),
    (select id from sets where short_name = 'pkld'),
    '9s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dovin Baan'),
    (select id from sets where short_name = 'pkld'),
    '179s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Deadlock Trap'),
    (select id from sets where short_name = 'pkld'),
    '204s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Key to the City'),
    (select id from sets where short_name = 'pkld'),
    '220s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chief of the Foundry'),
    (select id from sets where short_name = 'pkld'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dynavolt Tower'),
    (select id from sets where short_name = 'pkld'),
    '208s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Architect of the Untamed'),
    (select id from sets where short_name = 'pkld'),
    '143s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oviya Pashiri, Sage Lifecrafter'),
    (select id from sets where short_name = 'pkld'),
    '165s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Midnight Oil'),
    (select id from sets where short_name = 'pkld'),
    '92s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wildest Dreams'),
    (select id from sets where short_name = 'pkld'),
    '174s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dubious Challenge'),
    (select id from sets where short_name = 'pkld'),
    '152s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fumigate'),
    (select id from sets where short_name = 'pkld'),
    '15s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aetherworks Marvel'),
    (select id from sets where short_name = 'pkld'),
    '193s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cultivator''s Caravan'),
    (select id from sets where short_name = 'pkld'),
    '203s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saheeli''s Artistry'),
    (select id from sets where short_name = 'pkld'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inventors'' Fair'),
    (select id from sets where short_name = 'pkld'),
    '247s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fleetwheel Cruiser'),
    (select id from sets where short_name = 'pkld'),
    '214s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyship Stalker'),
    (select id from sets where short_name = 'pkld'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Master Trinketeer'),
    (select id from sets where short_name = 'pkld'),
    '21s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skysovereign, Consul Flagship'),
    (select id from sets where short_name = 'pkld'),
    '234s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Combustible Gearhulk'),
    (select id from sets where short_name = 'pkld'),
    '112s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kambal, Consul of Allocation'),
    (select id from sets where short_name = 'pkld'),
    '183s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Depala, Pilot Exemplar'),
    (select id from sets where short_name = 'pkld'),
    '178s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Insidious Will'),
    (select id from sets where short_name = 'pkld'),
    '52s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Toolcraft Exemplar'),
    (select id from sets where short_name = 'pkld'),
    '32s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eliminate the Competition'),
    (select id from sets where short_name = 'pkld'),
    '78s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noxious Gearhulk'),
    (select id from sets where short_name = 'pkld'),
    '96s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Angel of Invention'),
    (select id from sets where short_name = 'pkld'),
    '4s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lost Legacy'),
    (select id from sets where short_name = 'pkld'),
    '88s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghirapur Orrery'),
    (select id from sets where short_name = 'pkld'),
    '216p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gonti, Lord of Luxury'),
    (select id from sets where short_name = 'pkld'),
    '84p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Padeem, Consul of Innovation'),
    (select id from sets where short_name = 'pkld'),
    '59p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saheeli Rai'),
    (select id from sets where short_name = 'pkld'),
    '186s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aetherstorm Roc'),
    (select id from sets where short_name = 'pkld'),
    '3s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Botanical Sanctum'),
    (select id from sets where short_name = 'pkld'),
    '244s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marionette Master'),
    (select id from sets where short_name = 'pkld'),
    '90s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bomat Courier'),
    (select id from sets where short_name = 'pkld'),
    '199s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Syndicate Trafficker'),
    (select id from sets where short_name = 'pkld'),
    '101s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyship Stalker'),
    (select id from sets where short_name = 'pkld'),
    '130s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Concealed Courtyard'),
    (select id from sets where short_name = 'pkld'),
    '245s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Captured by the Consulate'),
    (select id from sets where short_name = 'pkld'),
    '8s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aethersquall Ancient'),
    (select id from sets where short_name = 'pkld'),
    '39s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Multiform Wonder'),
    (select id from sets where short_name = 'pkld'),
    '223s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Paradoxical Outcome'),
    (select id from sets where short_name = 'pkld'),
    '60s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Confiscation Coup'),
    (select id from sets where short_name = 'pkld'),
    '41s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Madcap Experiment'),
    (select id from sets where short_name = 'pkld'),
    '122s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rashmi, Eternities Crafter'),
    (select id from sets where short_name = 'pkld'),
    '184p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scrapheap Scrounger'),
    (select id from sets where short_name = 'pkld'),
    '231s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animation Module'),
    (select id from sets where short_name = 'pkld'),
    '194s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Metalwork Colossus'),
    (select id from sets where short_name = 'pkld'),
    '222s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa, Vital Force'),
    (select id from sets where short_name = 'pkld'),
    '163p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gonti, Lord of Luxury'),
    (select id from sets where short_name = 'pkld'),
    '84s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smuggler''s Copter'),
    (select id from sets where short_name = 'pkld'),
    '235s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verdurous Gearhulk'),
    (select id from sets where short_name = 'pkld'),
    '172s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Demon of Dark Schemes'),
    (select id from sets where short_name = 'pkld'),
    '73s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lathnu Hellion'),
    (select id from sets where short_name = 'pkld'),
    '121s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noxious Gearhulk'),
    (select id from sets where short_name = 'pkld'),
    '96p',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Authority of the Consuls'),
    (select id from sets where short_name = 'pkld'),
    '5p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Essence Extraction'),
    (select id from sets where short_name = 'pkld'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cultivator of Blades'),
    (select id from sets where short_name = 'pkld'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa, Vital Force'),
    (select id from sets where short_name = 'pkld'),
    '163s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Torrential Gearhulk'),
    (select id from sets where short_name = 'pkld'),
    '67s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rashmi, Eternities Crafter'),
    (select id from sets where short_name = 'pkld'),
    '184s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Territorial Gorger'),
    (select id from sets where short_name = 'pkld'),
    '136s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cultivator of Blades'),
    (select id from sets where short_name = 'pkld'),
    '151s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Panharmonicon'),
    (select id from sets where short_name = 'pkld'),
    '226s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fateful Showdown'),
    (select id from sets where short_name = 'pkld'),
    '114s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra, Torch of Defiance'),
    (select id from sets where short_name = 'pkld'),
    '110s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Electrostatic Pummeler'),
    (select id from sets where short_name = 'pkld'),
    '210s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saheeli''s Artistry'),
    (select id from sets where short_name = 'pkld'),
    '62s',
    'rare'
) 
 on conflict do nothing;
