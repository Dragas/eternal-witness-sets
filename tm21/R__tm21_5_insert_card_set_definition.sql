insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Knight'),
    (select id from sets where short_name = 'tm21'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basri Ket Emblem'),
    (select id from sets where short_name = 'tm21'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tm21'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird'),
    (select id from sets where short_name = 'tm21'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weird'),
    (select id from sets where short_name = 'tm21'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tm21'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Construct'),
    (select id from sets where short_name = 'tm21'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure'),
    (select id from sets where short_name = 'tm21'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dog'),
    (select id from sets where short_name = 'tm21'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tm21'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana, Waker of the Dead Emblem'),
    (select id from sets where short_name = 'tm21'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'tm21'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk, Unleashed Emblem'),
    (select id from sets where short_name = 'tm21'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tm21'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pirate'),
    (select id from sets where short_name = 'tm21'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Griffin'),
    (select id from sets where short_name = 'tm21'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon'),
    (select id from sets where short_name = 'tm21'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat'),
    (select id from sets where short_name = 'tm21'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat'),
    (select id from sets where short_name = 'tm21'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Wizard'),
    (select id from sets where short_name = 'tm21'),
    '8',
    'common'
) 
 on conflict do nothing;
