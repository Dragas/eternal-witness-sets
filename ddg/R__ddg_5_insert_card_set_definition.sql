insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Dragon Fodder'),
    (select id from sets where short_name = 'ddg'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bogardan Rager'),
    (select id from sets where short_name = 'ddg'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reciprocate'),
    (select id from sets where short_name = 'ddg'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lionheart Maverick'),
    (select id from sets where short_name = 'ddg'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddg'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Punishing Fire'),
    (select id from sets where short_name = 'ddg'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Breath of Darigaaz'),
    (select id from sets where short_name = 'ddg'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight Exemplar'),
    (select id from sets where short_name = 'ddg'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seismic Strike'),
    (select id from sets where short_name = 'ddg'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Captive Flame'),
    (select id from sets where short_name = 'ddg'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leonin Skyhunter'),
    (select id from sets where short_name = 'ddg'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddg'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cinder Wall'),
    (select id from sets where short_name = 'ddg'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Claws of Valakut'),
    (select id from sets where short_name = 'ddg'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steward of Valeron'),
    (select id from sets where short_name = 'ddg'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mighty Leap'),
    (select id from sets where short_name = 'ddg'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silver Knight'),
    (select id from sets where short_name = 'ddg'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shiv''s Embrace'),
    (select id from sets where short_name = 'ddg'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grasslands'),
    (select id from sets where short_name = 'ddg'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oblivion Ring'),
    (select id from sets where short_name = 'ddg'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of the White Orchid'),
    (select id from sets where short_name = 'ddg'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Paladin of Prahv'),
    (select id from sets where short_name = 'ddg'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddg'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sigil Blessing'),
    (select id from sets where short_name = 'ddg'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddg'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jaws of Stone'),
    (select id from sets where short_name = 'ddg'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddg'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddg'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knotvine Paladin'),
    (select id from sets where short_name = 'ddg'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kabira Vindicator'),
    (select id from sets where short_name = 'ddg'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Hellkite'),
    (select id from sets where short_name = 'ddg'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Test of Faith'),
    (select id from sets where short_name = 'ddg'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Juniper Order Ranger'),
    (select id from sets where short_name = 'ddg'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight of Meadowgrain'),
    (select id from sets where short_name = 'ddg'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heroes'' Reunion'),
    (select id from sets where short_name = 'ddg'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight of the Reliquary'),
    (select id from sets where short_name = 'ddg'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddg'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'White Knight'),
    (select id from sets where short_name = 'ddg'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyhunter Patrol'),
    (select id from sets where short_name = 'ddg'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kilnmouth Dragon'),
    (select id from sets where short_name = 'ddg'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Edge of Autumn'),
    (select id from sets where short_name = 'ddg'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Whelp'),
    (select id from sets where short_name = 'ddg'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghostfire'),
    (select id from sets where short_name = 'ddg'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodmark Mentor'),
    (select id from sets where short_name = 'ddg'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight of Cliffhaven'),
    (select id from sets where short_name = 'ddg'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddg'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire-Belly Changeling'),
    (select id from sets where short_name = 'ddg'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Benalish Lancer'),
    (select id from sets where short_name = 'ddg'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alaborn Cavalier'),
    (select id from sets where short_name = 'ddg'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragonspeaker Shaman'),
    (select id from sets where short_name = 'ddg'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spitting Earth'),
    (select id from sets where short_name = 'ddg'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mudbutton Torchrunner'),
    (select id from sets where short_name = 'ddg'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reprisal'),
    (select id from sets where short_name = 'ddg'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thunder Dragon'),
    (select id from sets where short_name = 'ddg'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harm''s Way'),
    (select id from sets where short_name = 'ddg'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ddg'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mordant Dragon'),
    (select id from sets where short_name = 'ddg'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treetop Village'),
    (select id from sets where short_name = 'ddg'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fiery Fall'),
    (select id from sets where short_name = 'ddg'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Griffin Guide'),
    (select id from sets where short_name = 'ddg'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddg'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kinsbaile Cavalier'),
    (select id from sets where short_name = 'ddg'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wilt-Leaf Cavaliers'),
    (select id from sets where short_name = 'ddg'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddg'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cone of Flame'),
    (select id from sets where short_name = 'ddg'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skirk Prospector'),
    (select id from sets where short_name = 'ddg'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temporary Insanity'),
    (select id from sets where short_name = 'ddg'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sejiri Steppe'),
    (select id from sets where short_name = 'ddg'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Sanctuary'),
    (select id from sets where short_name = 'ddg'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plover Knights'),
    (select id from sets where short_name = 'ddg'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seething Song'),
    (select id from sets where short_name = 'ddg'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zhalfirin Commander'),
    (select id from sets where short_name = 'ddg'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spidersilk Armor'),
    (select id from sets where short_name = 'ddg'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armillary Sphere'),
    (select id from sets where short_name = 'ddg'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loxodon Warhammer'),
    (select id from sets where short_name = 'ddg'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddg'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Henge Guardian'),
    (select id from sets where short_name = 'ddg'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Caravan Escort'),
    (select id from sets where short_name = 'ddg'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voracious Dragon'),
    (select id from sets where short_name = 'ddg'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bogardan Hellkite'),
    (select id from sets where short_name = 'ddg'),
    '47',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dragon''s Claw'),
    (select id from sets where short_name = 'ddg'),
    '63',
    'uncommon'
) 
 on conflict do nothing;
