insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Aeronaut Tinkerer'),
    (select id from sets where short_name = 'purl'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steward of Valeron'),
    (select id from sets where short_name = 'purl'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relentless Rats'),
    (select id from sets where short_name = 'purl'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stealer of Secrets'),
    (select id from sets where short_name = 'purl'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merfolk Mesmerist'),
    (select id from sets where short_name = 'purl'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodthrone Vampire'),
    (select id from sets where short_name = 'purl'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kor Skyfisher'),
    (select id from sets where short_name = 'purl'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shepherd of the Lost'),
    (select id from sets where short_name = 'purl'),
    '34*',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Fury'),
    (select id from sets where short_name = 'purl'),
    '5',
    'rare'
) 
 on conflict do nothing;
