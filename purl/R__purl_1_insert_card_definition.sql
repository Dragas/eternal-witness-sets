    insert into mtgcard(name) values ('Aeronaut Tinkerer') on conflict do nothing;
    insert into mtgcard(name) values ('Steward of Valeron') on conflict do nothing;
    insert into mtgcard(name) values ('Relentless Rats') on conflict do nothing;
    insert into mtgcard(name) values ('Stealer of Secrets') on conflict do nothing;
    insert into mtgcard(name) values ('Merfolk Mesmerist') on conflict do nothing;
    insert into mtgcard(name) values ('Bloodthrone Vampire') on conflict do nothing;
    insert into mtgcard(name) values ('Kor Skyfisher') on conflict do nothing;
    insert into mtgcard(name) values ('Shepherd of the Lost') on conflict do nothing;
    insert into mtgcard(name) values ('Chandra''s Fury') on conflict do nothing;
