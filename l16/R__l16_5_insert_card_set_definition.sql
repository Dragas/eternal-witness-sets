insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Servo // Thopter'),
    (select id from sets where short_name = 'l16'),
    '5',
    'common'
) 
 on conflict do nothing;
