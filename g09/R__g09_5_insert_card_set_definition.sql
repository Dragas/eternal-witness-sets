insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Flooded Strand'),
    (select id from sets where short_name = 'g09'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Polluted Delta'),
    (select id from sets where short_name = 'g09'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maze of Ith'),
    (select id from sets where short_name = 'g09'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Windswept Heath'),
    (select id from sets where short_name = 'g09'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'g09'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodstained Mire'),
    (select id from sets where short_name = 'g09'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wooded Foothills'),
    (select id from sets where short_name = 'g09'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Survival of the Fittest'),
    (select id from sets where short_name = 'g09'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burning Wish'),
    (select id from sets where short_name = 'g09'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stifle'),
    (select id from sets where short_name = 'g09'),
    '3',
    'rare'
) 
 on conflict do nothing;
