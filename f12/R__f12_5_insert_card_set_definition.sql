insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Avacyn''s Pilgrim'),
    (select id from sets where short_name = 'f12'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lingering Souls'),
    (select id from sets where short_name = 'f12'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glistener Elf'),
    (select id from sets where short_name = 'f12'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Human // Wolf'),
    (select id from sets where short_name = 'f12'),
    '1a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dismember'),
    (select id from sets where short_name = 'f12'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Acidic Slime'),
    (select id from sets where short_name = 'f12'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gitaxian Probe'),
    (select id from sets where short_name = 'f12'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pillar of Flame'),
    (select id from sets where short_name = 'f12'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Despise'),
    (select id from sets where short_name = 'f12'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Grudge'),
    (select id from sets where short_name = 'f12'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forbidden Alchemy'),
    (select id from sets where short_name = 'f12'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tectonic Edge'),
    (select id from sets where short_name = 'f12'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'f12'),
    '10',
    'rare'
) 
 on conflict do nothing;
