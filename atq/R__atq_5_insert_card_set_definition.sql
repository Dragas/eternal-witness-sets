insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Yawgmoth Demon'),
    (select id from sets where short_name = 'atq'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Colossus of Sardia'),
    (select id from sets where short_name = 'atq'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Engine'),
    (select id from sets where short_name = 'atq'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Weaponsmith'),
    (select id from sets where short_name = 'atq'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shatterstorm'),
    (select id from sets where short_name = 'atq'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakalite'),
    (select id from sets where short_name = 'atq'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Millstone'),
    (select id from sets where short_name = 'atq'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Damping Field'),
    (select id from sets where short_name = 'atq'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Workshop'),
    (select id from sets where short_name = 'atq'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Artifact Blast'),
    (select id from sets where short_name = 'atq'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reconstruction'),
    (select id from sets where short_name = 'atq'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Undoing'),
    (select id from sets where short_name = 'atq'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clockwork Avian'),
    (select id from sets where short_name = 'atq'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gate to Phyrexia'),
    (select id from sets where short_name = 'atq'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Gremlins'),
    (select id from sets where short_name = 'atq'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Atog'),
    (select id from sets where short_name = 'atq'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Artifacts'),
    (select id from sets where short_name = 'atq'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tetravus'),
    (select id from sets where short_name = 'atq'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = 'atq'),
    '83b',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yotian Soldier'),
    (select id from sets where short_name = 'atq'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Transmogrant'),
    (select id from sets where short_name = 'atq'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tawnos''s Weaponry'),
    (select id from sets where short_name = 'atq'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Candelabra of Tawnos'),
    (select id from sets where short_name = 'atq'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Avenger'),
    (select id from sets where short_name = 'atq'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = 'atq'),
    '80b',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Miter'),
    (select id from sets where short_name = 'atq'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Avenger'),
    (select id from sets where short_name = 'atq'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armageddon Clock'),
    (select id from sets where short_name = 'atq'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weakstone'),
    (select id from sets where short_name = 'atq'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = 'atq'),
    '85c',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drafna''s Restoration'),
    (select id from sets where short_name = 'atq'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grapeshot Catapult'),
    (select id from sets where short_name = 'atq'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Xenic Poltergeist'),
    (select id from sets where short_name = 'atq'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Argothian Pixies'),
    (select id from sets where short_name = 'atq'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Titania''s Song'),
    (select id from sets where short_name = 'atq'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Artifact Possession'),
    (select id from sets where short_name = 'atq'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ornithopter'),
    (select id from sets where short_name = 'atq'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jalum Tome'),
    (select id from sets where short_name = 'atq'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = 'atq'),
    '84c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Mechanics'),
    (select id from sets where short_name = 'atq'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Argivian Blacksmith'),
    (select id from sets where short_name = 'atq'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clay Statue'),
    (select id from sets where short_name = 'atq'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Artisans'),
    (select id from sets where short_name = 'atq'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = 'atq'),
    '85b',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coral Helm'),
    (select id from sets where short_name = 'atq'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tablet of Epityr'),
    (select id from sets where short_name = 'atq'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Transmute Artifact'),
    (select id from sets where short_name = 'atq'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bronze Tablet'),
    (select id from sets where short_name = 'atq'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cursed Rack'),
    (select id from sets where short_name = 'atq'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgothian Sylex'),
    (select id from sets where short_name = 'atq'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Argothian Treefolk'),
    (select id from sets where short_name = 'atq'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = 'atq'),
    '85a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mishra''s War Machine'),
    (select id from sets where short_name = 'atq'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strip Mine'),
    (select id from sets where short_name = 'atq'),
    '82c',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = 'atq'),
    '84a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tawnos''s Wand'),
    (select id from sets where short_name = 'atq'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Haunting Wind'),
    (select id from sets where short_name = 'atq'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Strip Mine'),
    (select id from sets where short_name = 'atq'),
    '82b',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ivory Tower'),
    (select id from sets where short_name = 'atq'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Su-Chi'),
    (select id from sets where short_name = 'atq'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = 'atq'),
    '80a',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Staff of Zegon'),
    (select id from sets where short_name = 'atq'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Triskelion'),
    (select id from sets where short_name = 'atq'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primal Clay'),
    (select id from sets where short_name = 'atq'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = 'atq'),
    '80d',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Powerleech'),
    (select id from sets where short_name = 'atq'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Battle Gear'),
    (select id from sets where short_name = 'atq'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Amulet of Kroog'),
    (select id from sets where short_name = 'atq'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = 'atq'),
    '84d',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Spears'),
    (select id from sets where short_name = 'atq'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mightstone'),
    (select id from sets where short_name = 'atq'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Artifact Ward'),
    (select id from sets where short_name = 'atq'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sage of Lat-Nam'),
    (select id from sets where short_name = 'atq'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feldon''s Cane'),
    (select id from sets where short_name = 'atq'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Energy Flux'),
    (select id from sets where short_name = 'atq'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Martyrs of Korlis'),
    (select id from sets where short_name = 'atq'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tawnos''s Coffin'),
    (select id from sets where short_name = 'atq'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = 'atq'),
    '80c',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Priest of Yawgmoth'),
    (select id from sets where short_name = 'atq'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shapeshifter'),
    (select id from sets where short_name = 'atq'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = 'atq'),
    '85d',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tawnos''s Weaponry'),
    (select id from sets where short_name = 'atq'),
    '70†',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = 'atq'),
    '83d',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Altar'),
    (select id from sets where short_name = 'atq'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Argivian Archaeologist'),
    (select id from sets where short_name = 'atq'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crumble'),
    (select id from sets where short_name = 'atq'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rocket Launcher'),
    (select id from sets where short_name = 'atq'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Onulet'),
    (select id from sets where short_name = 'atq'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = 'atq'),
    '83c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reverse Polarity'),
    (select id from sets where short_name = 'atq'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = 'atq'),
    '83a',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Strip Mine'),
    (select id from sets where short_name = 'atq'),
    '82d',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = 'atq'),
    '84b',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Power Artifact'),
    (select id from sets where short_name = 'atq'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Strip Mine'),
    (select id from sets where short_name = 'atq'),
    '82a',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Rack'),
    (select id from sets where short_name = 'atq'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hurkyl''s Recall'),
    (select id from sets where short_name = 'atq'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Chalice'),
    (select id from sets where short_name = 'atq'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battering Ram'),
    (select id from sets where short_name = 'atq'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Citanul Druid'),
    (select id from sets where short_name = 'atq'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Detonate'),
    (select id from sets where short_name = 'atq'),
    '24',
    'uncommon'
) 
 on conflict do nothing;
