insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Archon of the Triumvirate'),
    (select id from sets where short_name = 'prtr'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Supreme Verdict'),
    (select id from sets where short_name = 'prtr'),
    '*201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grove of the Guardian'),
    (select id from sets where short_name = 'prtr'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corpsejack Menace'),
    (select id from sets where short_name = 'prtr'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hypersonic Dragon'),
    (select id from sets where short_name = 'prtr'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cryptborn Horror'),
    (select id from sets where short_name = 'prtr'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dryad Militant'),
    (select id from sets where short_name = 'prtr'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dreg Mangler'),
    (select id from sets where short_name = 'prtr'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deadbridge Goliath'),
    (select id from sets where short_name = 'prtr'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carnival Hellsteed'),
    (select id from sets where short_name = 'prtr'),
    '147',
    'rare'
) 
 on conflict do nothing;
