insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Drench the Soil in Their Blood'),
    (select id from sets where short_name = 'parc'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plots That Span Centuries'),
    (select id from sets where short_name = 'parc'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Your Inescapable Doom'),
    (select id from sets where short_name = 'parc'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Perhaps You''ve Met My Cohort'),
    (select id from sets where short_name = 'parc'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imprison This Insolent Wretch'),
    (select id from sets where short_name = 'parc'),
    '58',
    'rare'
) 
 on conflict do nothing;
