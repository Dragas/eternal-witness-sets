insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Drench the Soil in Their Blood'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plots That Span Centuries'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Your Inescapable Doom'),
        (select types.id from types where name = 'Ongoing')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Your Inescapable Doom'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Perhaps You''ve Met My Cohort'),
        (select types.id from types where name = 'Scheme')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Imprison This Insolent Wretch'),
        (select types.id from types where name = 'Ongoing')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Imprison This Insolent Wretch'),
        (select types.id from types where name = 'Scheme')
    ) 
 on conflict do nothing;
