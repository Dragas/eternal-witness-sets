    insert into mtgcard(name) values ('Drench the Soil in Their Blood') on conflict do nothing;
    insert into mtgcard(name) values ('Plots That Span Centuries') on conflict do nothing;
    insert into mtgcard(name) values ('Your Inescapable Doom') on conflict do nothing;
    insert into mtgcard(name) values ('Perhaps You''ve Met My Cohort') on conflict do nothing;
    insert into mtgcard(name) values ('Imprison This Insolent Wretch') on conflict do nothing;
