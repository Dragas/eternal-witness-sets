insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'jgp'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Cradle'),
    (select id from sets where short_name = 'jgp'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stroke of Genius'),
    (select id from sets where short_name = 'jgp'),
    '2',
    'rare'
) 
 on conflict do nothing;
