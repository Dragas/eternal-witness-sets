insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Elemental Shaman'),
    (select id from sets where short_name = 'tjvc'),
    '4',
    'common'
) 
 on conflict do nothing;
