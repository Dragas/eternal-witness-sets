insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Griselbrand'),
    (select id from sets where short_name = 'phel'),
    '106',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Avacyn, Angel of Hope'),
    (select id from sets where short_name = 'phel'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Angel // Demon'),
    (select id from sets where short_name = 'phel'),
    '1★',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sigarda, Host of Herons'),
    (select id from sets where short_name = 'phel'),
    '210',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gisela, Blade of Goldnight'),
    (select id from sets where short_name = 'phel'),
    '209',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bruna, Light of Alabaster'),
    (select id from sets where short_name = 'phel'),
    '208',
    'mythic'
) 
 on conflict do nothing;
