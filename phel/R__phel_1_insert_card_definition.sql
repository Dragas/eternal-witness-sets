    insert into mtgcard(name) values ('Griselbrand') on conflict do nothing;
    insert into mtgcard(name) values ('Avacyn, Angel of Hope') on conflict do nothing;
    insert into mtgcard(name) values ('Angel // Demon') on conflict do nothing;
    insert into mtgcard(name) values ('Sigarda, Host of Herons') on conflict do nothing;
    insert into mtgcard(name) values ('Gisela, Blade of Goldnight') on conflict do nothing;
    insert into mtgcard(name) values ('Bruna, Light of Alabaster') on conflict do nothing;
