insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Astral Drift'),
        (select types.id from types where name = 'Enchantment')
    ) 
 on conflict do nothing;
