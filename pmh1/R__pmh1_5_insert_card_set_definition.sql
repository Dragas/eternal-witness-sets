insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Astral Drift'),
    (select id from sets where short_name = 'pmh1'),
    '3',
    'rare'
) 
 on conflict do nothing;
