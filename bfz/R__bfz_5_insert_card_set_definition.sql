insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Kor Entanglers'),
    (select id from sets where short_name = 'bfz'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Touch of the Void'),
    (select id from sets where short_name = 'bfz'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grove Rumbler'),
    (select id from sets where short_name = 'bfz'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Noyan Dar, Roil Shaper'),
    (select id from sets where short_name = 'bfz'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Felidar Sovereign'),
    (select id from sets where short_name = 'bfz'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Adverse Conditions'),
    (select id from sets where short_name = 'bfz'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'March from the Tomb'),
    (select id from sets where short_name = 'bfz'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Devastator'),
    (select id from sets where short_name = 'bfz'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sure Strike'),
    (select id from sets where short_name = 'bfz'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ondu Rising'),
    (select id from sets where short_name = 'bfz'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barrage Tyrant'),
    (select id from sets where short_name = 'bfz'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief Invoker'),
    (select id from sets where short_name = 'bfz'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'bfz'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inspired Charge'),
    (select id from sets where short_name = 'bfz'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Territorial Baloth'),
    (select id from sets where short_name = 'bfz'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'bfz'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathless Behemoth'),
    (select id from sets where short_name = 'bfz'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunken Hollow'),
    (select id from sets where short_name = 'bfz'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grave Birthing'),
    (select id from sets where short_name = 'bfz'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infuse with the Elements'),
    (select id from sets where short_name = 'bfz'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel of Renewal'),
    (select id from sets where short_name = 'bfz'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drowner of Hope'),
    (select id from sets where short_name = 'bfz'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ulamog, the Ceaseless Hunger'),
    (select id from sets where short_name = 'bfz'),
    '15',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wave-Wing Elemental'),
    (select id from sets where short_name = 'bfz'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'bfz'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dominator Drone'),
    (select id from sets where short_name = 'bfz'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murk Strider'),
    (select id from sets where short_name = 'bfz'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'bfz'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kalastria Nightwatch'),
    (select id from sets where short_name = 'bfz'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fertile Thicket'),
    (select id from sets where short_name = 'bfz'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pathway Arrows'),
    (select id from sets where short_name = 'bfz'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Valakut Invoker'),
    (select id from sets where short_name = 'bfz'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloud Manta'),
    (select id from sets where short_name = 'bfz'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gideon, Ally of Zendikar'),
    (select id from sets where short_name = 'bfz'),
    '29',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Reclaiming Vines'),
    (select id from sets where short_name = 'bfz'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slab Hammer'),
    (select id from sets where short_name = 'bfz'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firemantle Mage'),
    (select id from sets where short_name = 'bfz'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rot Shambler'),
    (select id from sets where short_name = 'bfz'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Culling Drone'),
    (select id from sets where short_name = 'bfz'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'bfz'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vile Aggregate'),
    (select id from sets where short_name = 'bfz'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Void Attendant'),
    (select id from sets where short_name = 'bfz'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brilliant Spectrum'),
    (select id from sets where short_name = 'bfz'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'bfz'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oblivion Sower'),
    (select id from sets where short_name = 'bfz'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dust Stalker'),
    (select id from sets where short_name = 'bfz'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lavastep Raider'),
    (select id from sets where short_name = 'bfz'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hedron Archive'),
    (select id from sets where short_name = 'bfz'),
    '223',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ulamog''s Reclaimer'),
    (select id from sets where short_name = 'bfz'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyline Cascade'),
    (select id from sets where short_name = 'bfz'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blighted Fen'),
    (select id from sets where short_name = 'bfz'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Herald of Kozilek'),
    (select id from sets where short_name = 'bfz'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oracle of Dust'),
    (select id from sets where short_name = 'bfz'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nirkana Assassin'),
    (select id from sets where short_name = 'bfz'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'bfz'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brood Butcher'),
    (select id from sets where short_name = 'bfz'),
    '199',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akoum Firebird'),
    (select id from sets where short_name = 'bfz'),
    '138',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shambling Vent'),
    (select id from sets where short_name = 'bfz'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quarantine Field'),
    (select id from sets where short_name = 'bfz'),
    '43',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'bfz'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragonmaster Outcast'),
    (select id from sets where short_name = 'bfz'),
    '144',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Coralhelm Guide'),
    (select id from sets where short_name = 'bfz'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampiric Rites'),
    (select id from sets where short_name = 'bfz'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tajuru Beastmaster'),
    (select id from sets where short_name = 'bfz'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smoldering Marsh'),
    (select id from sets where short_name = 'bfz'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bane of Bala Ged'),
    (select id from sets where short_name = 'bfz'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'bfz'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Haven Medic'),
    (select id from sets where short_name = 'bfz'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Titan''s Presence'),
    (select id from sets where short_name = 'bfz'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'bfz'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'bfz'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clutch of Currents'),
    (select id from sets where short_name = 'bfz'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruesome Slaughter'),
    (select id from sets where short_name = 'bfz'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Molten Nursery'),
    (select id from sets where short_name = 'bfz'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'bfz'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'bfz'),
    '252a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Expedition Envoy'),
    (select id from sets where short_name = 'bfz'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kalastria Healer'),
    (select id from sets where short_name = 'bfz'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gideon''s Reproach'),
    (select id from sets where short_name = 'bfz'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Painful Truths'),
    (select id from sets where short_name = 'bfz'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Veteran Warleader'),
    (select id from sets where short_name = 'bfz'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'bfz'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyrider Elf'),
    (select id from sets where short_name = 'bfz'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blight Herder'),
    (select id from sets where short_name = 'bfz'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Retreat to Hagra'),
    (select id from sets where short_name = 'bfz'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'bfz'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Gift'),
    (select id from sets where short_name = 'bfz'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dampening Pulse'),
    (select id from sets where short_name = 'bfz'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Windrider Patrol'),
    (select id from sets where short_name = 'bfz'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kiora, Master of the Depths'),
    (select id from sets where short_name = 'bfz'),
    '213',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Retreat to Emeria'),
    (select id from sets where short_name = 'bfz'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Salvage Drone'),
    (select id from sets where short_name = 'bfz'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conduit of Ruin'),
    (select id from sets where short_name = 'bfz'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tide Drifter'),
    (select id from sets where short_name = 'bfz'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hero of Goma Fada'),
    (select id from sets where short_name = 'bfz'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Horribly Awry'),
    (select id from sets where short_name = 'bfz'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desolation Twin'),
    (select id from sets where short_name = 'bfz'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Courier Griffin'),
    (select id from sets where short_name = 'bfz'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eyeless Watcher'),
    (select id from sets where short_name = 'bfz'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Turn Against'),
    (select id from sets where short_name = 'bfz'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Malakir Familiar'),
    (select id from sets where short_name = 'bfz'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rising Miasma'),
    (select id from sets where short_name = 'bfz'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fortified Rampart'),
    (select id from sets where short_name = 'bfz'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'bfz'),
    '271a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serpentine Spike'),
    (select id from sets where short_name = 'bfz'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unified Front'),
    (select id from sets where short_name = 'bfz'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Retreat to Kazandu'),
    (select id from sets where short_name = 'bfz'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis Reignited'),
    (select id from sets where short_name = 'bfz'),
    '119',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'bfz'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ulamog''s Despoiler'),
    (select id from sets where short_name = 'bfz'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dutiful Return'),
    (select id from sets where short_name = 'bfz'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Encircling Fissure'),
    (select id from sets where short_name = 'bfz'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'bfz'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Omnath, Locus of Rage'),
    (select id from sets where short_name = 'bfz'),
    '217',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Processor Assault'),
    (select id from sets where short_name = 'bfz'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'bfz'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greenwarden of Murasa'),
    (select id from sets where short_name = 'bfz'),
    '174',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Guardian of Tazeem'),
    (select id from sets where short_name = 'bfz'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'bfz'),
    '266a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grovetender Druids'),
    (select id from sets where short_name = 'bfz'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zulaport Cutthroat'),
    (select id from sets where short_name = 'bfz'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'bfz'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Call the Scions'),
    (select id from sets where short_name = 'bfz'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rolling Thunder'),
    (select id from sets where short_name = 'bfz'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wasteland Strangler'),
    (select id from sets where short_name = 'bfz'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Planar Outburst'),
    (select id from sets where short_name = 'bfz'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plummet'),
    (select id from sets where short_name = 'bfz'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'bfz'),
    '268a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'bfz'),
    '267a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crumble to Dust'),
    (select id from sets where short_name = 'bfz'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'bfz'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Endless One'),
    (select id from sets where short_name = 'bfz'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Retreat to Coralhelm'),
    (select id from sets where short_name = 'bfz'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kitesail Scout'),
    (select id from sets where short_name = 'bfz'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'bfz'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tandem Tactics'),
    (select id from sets where short_name = 'bfz'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stonefury'),
    (select id from sets where short_name = 'bfz'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Catacomb Sifter'),
    (select id from sets where short_name = 'bfz'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prism Array'),
    (select id from sets where short_name = 'bfz'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scour from Existence'),
    (select id from sets where short_name = 'bfz'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'bfz'),
    '269a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serene Steward'),
    (select id from sets where short_name = 'bfz'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ondu Champion'),
    (select id from sets where short_name = 'bfz'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruinous Path'),
    (select id from sets where short_name = 'bfz'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Radiant Flames'),
    (select id from sets where short_name = 'bfz'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'bfz'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voracious Null'),
    (select id from sets where short_name = 'bfz'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bone Splinters'),
    (select id from sets where short_name = 'bfz'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief Hydra'),
    (select id from sets where short_name = 'bfz'),
    '181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blighted Woodland'),
    (select id from sets where short_name = 'bfz'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sheer Drop'),
    (select id from sets where short_name = 'bfz'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tajuru Warcaller'),
    (select id from sets where short_name = 'bfz'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'bfz'),
    '272a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smite the Monstrous'),
    (select id from sets where short_name = 'bfz'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Cohort'),
    (select id from sets where short_name = 'bfz'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Breaker of Armies'),
    (select id from sets where short_name = 'bfz'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kozilek''s Sentinel'),
    (select id from sets where short_name = 'bfz'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guul Draz Overseer'),
    (select id from sets where short_name = 'bfz'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emeria Shepherd'),
    (select id from sets where short_name = 'bfz'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Complete Disregard'),
    (select id from sets where short_name = 'bfz'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'bfz'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seek the Wilds'),
    (select id from sets where short_name = 'bfz'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unnatural Aggression'),
    (select id from sets where short_name = 'bfz'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snapping Gnarlid'),
    (select id from sets where short_name = 'bfz'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swarm Surge'),
    (select id from sets where short_name = 'bfz'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Part the Waterveil'),
    (select id from sets where short_name = 'bfz'),
    '80',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'bfz'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'bfz'),
    '250a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grip of Desolation'),
    (select id from sets where short_name = 'bfz'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sanctum of Ugin'),
    (select id from sets where short_name = 'bfz'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anticipate'),
    (select id from sets where short_name = 'bfz'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hedron Blade'),
    (select id from sets where short_name = 'bfz'),
    '224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Valakut Predator'),
    (select id from sets where short_name = 'bfz'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blighted Gorge'),
    (select id from sets where short_name = 'bfz'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blighted Cataract'),
    (select id from sets where short_name = 'bfz'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scythe Leopard'),
    (select id from sets where short_name = 'bfz'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'bfz'),
    '258a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brutal Expulsion'),
    (select id from sets where short_name = 'bfz'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'bfz'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cliffside Lookout'),
    (select id from sets where short_name = 'bfz'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Void Winnower'),
    (select id from sets where short_name = 'bfz'),
    '17',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'bfz'),
    '257a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Upheaval'),
    (select id from sets where short_name = 'bfz'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'bfz'),
    '259a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nissa''s Renewal'),
    (select id from sets where short_name = 'bfz'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ondu Greathorn'),
    (select id from sets where short_name = 'bfz'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prairie Stream'),
    (select id from sets where short_name = 'bfz'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sludge Crawler'),
    (select id from sets where short_name = 'bfz'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Mantis'),
    (select id from sets where short_name = 'bfz'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drana''s Emissary'),
    (select id from sets where short_name = 'bfz'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silent Skimmer'),
    (select id from sets where short_name = 'bfz'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'bfz'),
    '264a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Skyspawner'),
    (select id from sets where short_name = 'bfz'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shrine of the Forsaken Gods'),
    (select id from sets where short_name = 'bfz'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Natural Connection'),
    (select id from sets where short_name = 'bfz'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Makindi Sliderunner'),
    (select id from sets where short_name = 'bfz'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pilgrim''s Eye'),
    (select id from sets where short_name = 'bfz'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tightening Coils'),
    (select id from sets where short_name = 'bfz'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'bfz'),
    '260a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'bfz'),
    '251a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Canopy Vista'),
    (select id from sets where short_name = 'bfz'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lifespring Druid'),
    (select id from sets where short_name = 'bfz'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forerunner of Slaughter'),
    (select id from sets where short_name = 'bfz'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Defiant Bloodlord'),
    (select id from sets where short_name = 'bfz'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'bfz'),
    '254a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Raker'),
    (select id from sets where short_name = 'bfz'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Belligerent Whiptail'),
    (select id from sets where short_name = 'bfz'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vestige of Emrakul'),
    (select id from sets where short_name = 'bfz'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hagra Sharpshooter'),
    (select id from sets where short_name = 'bfz'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'bfz'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jaddi Offshoot'),
    (select id from sets where short_name = 'bfz'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ulamog''s Nullifier'),
    (select id from sets where short_name = 'bfz'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Makindi Patrol'),
    (select id from sets where short_name = 'bfz'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swell of Growth'),
    (select id from sets where short_name = 'bfz'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fathom Feeder'),
    (select id from sets where short_name = 'bfz'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'bfz'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'bfz'),
    '273a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Outnumber'),
    (select id from sets where short_name = 'bfz'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shadow Glider'),
    (select id from sets where short_name = 'bfz'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin War Paint'),
    (select id from sets where short_name = 'bfz'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Looming Spires'),
    (select id from sets where short_name = 'bfz'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ally Encampment'),
    (select id from sets where short_name = 'bfz'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dispel'),
    (select id from sets where short_name = 'bfz'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carrier Thrall'),
    (select id from sets where short_name = 'bfz'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Resolute Blademaster'),
    (select id from sets where short_name = 'bfz'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Roil''s Retribution'),
    (select id from sets where short_name = 'bfz'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aligned Hedron Network'),
    (select id from sets where short_name = 'bfz'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Broodhunter Wurm'),
    (select id from sets where short_name = 'bfz'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coastal Discovery'),
    (select id from sets where short_name = 'bfz'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mire''s Malice'),
    (select id from sets where short_name = 'bfz'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exert Influence'),
    (select id from sets where short_name = 'bfz'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'bfz'),
    '261a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murasa Ranger'),
    (select id from sets where short_name = 'bfz'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kozilek''s Channeler'),
    (select id from sets where short_name = 'bfz'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mortuary Mire'),
    (select id from sets where short_name = 'bfz'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'bfz'),
    '253a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sandstone Bridge'),
    (select id from sets where short_name = 'bfz'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shatterskull Recruit'),
    (select id from sets where short_name = 'bfz'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akoum Stonewaker'),
    (select id from sets where short_name = 'bfz'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodbond Vampire'),
    (select id from sets where short_name = 'bfz'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woodland Wanderer'),
    (select id from sets where short_name = 'bfz'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beastcaller Savant'),
    (select id from sets where short_name = 'bfz'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plated Crusher'),
    (select id from sets where short_name = 'bfz'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smothering Abomination'),
    (select id from sets where short_name = 'bfz'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boiling Earth'),
    (select id from sets where short_name = 'bfz'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akoum Hellkite'),
    (select id from sets where short_name = 'bfz'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cinder Glade'),
    (select id from sets where short_name = 'bfz'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Scrying'),
    (select id from sets where short_name = 'bfz'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Roilmage''s Trick'),
    (select id from sets where short_name = 'bfz'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bring to Light'),
    (select id from sets where short_name = 'bfz'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drana, Liberator of Malakir'),
    (select id from sets where short_name = 'bfz'),
    '109',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tunneling Geopede'),
    (select id from sets where short_name = 'bfz'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Altar''s Reap'),
    (select id from sets where short_name = 'bfz'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Geyserfield Stalker'),
    (select id from sets where short_name = 'bfz'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undergrowth Champion'),
    (select id from sets where short_name = 'bfz'),
    '197',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sire of Stagnation'),
    (select id from sets where short_name = 'bfz'),
    '206',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scatter to the Winds'),
    (select id from sets where short_name = 'bfz'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Munda, Ambush Leader'),
    (select id from sets where short_name = 'bfz'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruin Processor'),
    (select id from sets where short_name = 'bfz'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earthen Arms'),
    (select id from sets where short_name = 'bfz'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Halimar Tidecaller'),
    (select id from sets where short_name = 'bfz'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lithomancer''s Focus'),
    (select id from sets where short_name = 'bfz'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghostly Sentinel'),
    (select id from sets where short_name = 'bfz'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Incubator Drone'),
    (select id from sets where short_name = 'bfz'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'bfz'),
    '265a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blisterpod'),
    (select id from sets where short_name = 'bfz'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Bladewhirl'),
    (select id from sets where short_name = 'bfz'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spawning Bed'),
    (select id from sets where short_name = 'bfz'),
    '248',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skitterskin'),
    (select id from sets where short_name = 'bfz'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nettle Drone'),
    (select id from sets where short_name = 'bfz'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Felidar Cub'),
    (select id from sets where short_name = 'bfz'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brood Monitor'),
    (select id from sets where short_name = 'bfz'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'bfz'),
    '274a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'bfz'),
    '262a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'bfz'),
    '256a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Roil Spout'),
    (select id from sets where short_name = 'bfz'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelic Captain'),
    (select id from sets where short_name = 'bfz'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spell Shrivel'),
    (select id from sets where short_name = 'bfz'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruination Guide'),
    (select id from sets where short_name = 'bfz'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lumbering Falls'),
    (select id from sets where short_name = 'bfz'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ugin''s Insight'),
    (select id from sets where short_name = 'bfz'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Benthic Infiltrator'),
    (select id from sets where short_name = 'bfz'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blighted Steppe'),
    (select id from sets where short_name = 'bfz'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kor Castigator'),
    (select id from sets where short_name = 'bfz'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'bfz'),
    '255a',
    'common'
) ,
(
    (select id from mtgcard where name = 'From Beyond'),
    (select id from sets where short_name = 'bfz'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Transgress the Mind'),
    (select id from sets where short_name = 'bfz'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Retreat to Valakut'),
    (select id from sets where short_name = 'bfz'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'bfz'),
    '270a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mist Intruder'),
    (select id from sets where short_name = 'bfz'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tajuru Stalwart'),
    (select id from sets where short_name = 'bfz'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rush of Ice'),
    (select id from sets where short_name = 'bfz'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'bfz'),
    '263a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chasm Guide'),
    (select id from sets where short_name = 'bfz'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zada, Hedron Grinder'),
    (select id from sets where short_name = 'bfz'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lantern Scout'),
    (select id from sets where short_name = 'bfz'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cryptic Cruiser'),
    (select id from sets where short_name = 'bfz'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stasis Snare'),
    (select id from sets where short_name = 'bfz'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demon''s Grasp'),
    (select id from sets where short_name = 'bfz'),
    '108',
    'common'
) 
 on conflict do nothing;
