insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Maze of Ith'),
    (select id from sets where short_name = 'v12'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Windbrisk Heights'),
    (select id from sets where short_name = 'v12'),
    '15',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ancient Tomb'),
    (select id from sets where short_name = 'v12'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Grove of the Burnwillows'),
    (select id from sets where short_name = 'v12'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Urborg, Tomb of Yawgmoth'),
    (select id from sets where short_name = 'v12'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cephalid Coliseum'),
    (select id from sets where short_name = 'v12'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dryad Arbor'),
    (select id from sets where short_name = 'v12'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vesuva'),
    (select id from sets where short_name = 'v12'),
    '14',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Glacial Chasm'),
    (select id from sets where short_name = 'v12'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Murmuring Bosk'),
    (select id from sets where short_name = 'v12'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'High Market'),
    (select id from sets where short_name = 'v12'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Desert'),
    (select id from sets where short_name = 'v12'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Boseiju, Who Shelters All'),
    (select id from sets where short_name = 'v12'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shivan Gorge'),
    (select id from sets where short_name = 'v12'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forbidden Orchard'),
    (select id from sets where short_name = 'v12'),
    '6',
    'mythic'
) 
 on conflict do nothing;
