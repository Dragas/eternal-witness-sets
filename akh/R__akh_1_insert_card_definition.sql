    insert into mtgcard(name) values ('Initiate''s Companion') on conflict do nothing;
    insert into mtgcard(name) values ('Plains') on conflict do nothing;
    insert into mtgcard(name) values ('Winged Shepherd') on conflict do nothing;
    insert into mtgcard(name) values ('Soulstinger') on conflict do nothing;
    insert into mtgcard(name) values ('Cruel Reality') on conflict do nothing;
    insert into mtgcard(name) values ('Commit // Memory') on conflict do nothing;
    insert into mtgcard(name) values ('Sacred Cat') on conflict do nothing;
    insert into mtgcard(name) values ('Renewed Faith') on conflict do nothing;
    insert into mtgcard(name) values ('Manglehorn') on conflict do nothing;
    insert into mtgcard(name) values ('Spidery Grasp') on conflict do nothing;
    insert into mtgcard(name) values ('Stir the Sands') on conflict do nothing;
    insert into mtgcard(name) values ('Open into Wonder') on conflict do nothing;
    insert into mtgcard(name) values ('As Foretold') on conflict do nothing;
    insert into mtgcard(name) values ('Oketra''s Monument') on conflict do nothing;
    insert into mtgcard(name) values ('Compulsory Rest') on conflict do nothing;
    insert into mtgcard(name) values ('Rags // Riches') on conflict do nothing;
    insert into mtgcard(name) values ('Mighty Leap') on conflict do nothing;
    insert into mtgcard(name) values ('Forest') on conflict do nothing;
    insert into mtgcard(name) values ('Protection of the Hekma') on conflict do nothing;
    insert into mtgcard(name) values ('Curator of Mysteries') on conflict do nothing;
    insert into mtgcard(name) values ('Labyrinth Guardian') on conflict do nothing;
    insert into mtgcard(name) values ('Hapatra''s Mark') on conflict do nothing;
    insert into mtgcard(name) values ('Brute Strength') on conflict do nothing;
    insert into mtgcard(name) values ('Quarry Hauler') on conflict do nothing;
    insert into mtgcard(name) values ('Hooded Brawler') on conflict do nothing;
    insert into mtgcard(name) values ('Sacred Excavation') on conflict do nothing;
    insert into mtgcard(name) values ('Desert Cerodon') on conflict do nothing;
    insert into mtgcard(name) values ('Island') on conflict do nothing;
    insert into mtgcard(name) values ('Honored Hydra') on conflict do nothing;
    insert into mtgcard(name) values ('Electrify') on conflict do nothing;
    insert into mtgcard(name) values ('Desiccated Naga') on conflict do nothing;
    insert into mtgcard(name) values ('Stone Quarry') on conflict do nothing;
    insert into mtgcard(name) values ('Pitiless Vizier') on conflict do nothing;
    insert into mtgcard(name) values ('Glory-Bound Initiate') on conflict do nothing;
    insert into mtgcard(name) values ('Vizier of Deferment') on conflict do nothing;
    insert into mtgcard(name) values ('Shefet Monitor') on conflict do nothing;
    insert into mtgcard(name) values ('Plague Belcher') on conflict do nothing;
    insert into mtgcard(name) values ('Combat Celebrant') on conflict do nothing;
    insert into mtgcard(name) values ('Liliana''s Influence') on conflict do nothing;
    insert into mtgcard(name) values ('Glorious End') on conflict do nothing;
    insert into mtgcard(name) values ('Wasteland Scorpion') on conflict do nothing;
    insert into mtgcard(name) values ('Gideon, Martial Paragon') on conflict do nothing;
    insert into mtgcard(name) values ('Ahn-Crop Champion') on conflict do nothing;
    insert into mtgcard(name) values ('Mountain') on conflict do nothing;
    insert into mtgcard(name) values ('Crocodile of the Crossing') on conflict do nothing;
    insert into mtgcard(name) values ('Tormenting Voice') on conflict do nothing;
    insert into mtgcard(name) values ('Unburden') on conflict do nothing;
    insert into mtgcard(name) values ('Watchers of the Dead') on conflict do nothing;
    insert into mtgcard(name) values ('Glorybringer') on conflict do nothing;
    insert into mtgcard(name) values ('Archfiend of Ifnir') on conflict do nothing;
    insert into mtgcard(name) values ('Dread Wanderer') on conflict do nothing;
    insert into mtgcard(name) values ('Scribe of the Mindful') on conflict do nothing;
    insert into mtgcard(name) values ('Horror of the Broken Lands') on conflict do nothing;
    insert into mtgcard(name) values ('Vizier of Remedies') on conflict do nothing;
    insert into mtgcard(name) values ('Hazoret the Fervent') on conflict do nothing;
    insert into mtgcard(name) values ('Lord of the Accursed') on conflict do nothing;
    insert into mtgcard(name) values ('Floodwaters') on conflict do nothing;
    insert into mtgcard(name) values ('Faith of the Devoted') on conflict do nothing;
    insert into mtgcard(name) values ('Oashra Cultivator') on conflict do nothing;
    insert into mtgcard(name) values ('Rhet-Crop Spearmaster') on conflict do nothing;
    insert into mtgcard(name) values ('Aven Mindcensor') on conflict do nothing;
    insert into mtgcard(name) values ('Sunscorched Desert') on conflict do nothing;
    insert into mtgcard(name) values ('Liliana, Death''s Majesty') on conflict do nothing;
    insert into mtgcard(name) values ('Aven Wind Guide') on conflict do nothing;
    insert into mtgcard(name) values ('Cradle of the Accursed') on conflict do nothing;
    insert into mtgcard(name) values ('Drake Haven') on conflict do nothing;
    insert into mtgcard(name) values ('Cryptic Serpent') on conflict do nothing;
    insert into mtgcard(name) values ('Oracle''s Vault') on conflict do nothing;
    insert into mtgcard(name) values ('Zenith Seeker') on conflict do nothing;
    insert into mtgcard(name) values ('Miasmic Mummy') on conflict do nothing;
    insert into mtgcard(name) values ('Gust Walker') on conflict do nothing;
    insert into mtgcard(name) values ('Violent Impact') on conflict do nothing;
    insert into mtgcard(name) values ('Bontu the Glorified') on conflict do nothing;
    insert into mtgcard(name) values ('Censor') on conflict do nothing;
    insert into mtgcard(name) values ('Pathmaker Initiate') on conflict do nothing;
    insert into mtgcard(name) values ('Irrigated Farmland') on conflict do nothing;
    insert into mtgcard(name) values ('Dispossess') on conflict do nothing;
    insert into mtgcard(name) values ('Anointer Priest') on conflict do nothing;
    insert into mtgcard(name) values ('Rhonas the Indomitable') on conflict do nothing;
    insert into mtgcard(name) values ('Onward // Victory') on conflict do nothing;
    insert into mtgcard(name) values ('Rhonas''s Monument') on conflict do nothing;
    insert into mtgcard(name) values ('Trial of Zeal') on conflict do nothing;
    insert into mtgcard(name) values ('Dissenter''s Deliverance') on conflict do nothing;
    insert into mtgcard(name) values ('Stinging Shot') on conflict do nothing;
    insert into mtgcard(name) values ('Hapatra, Vizier of Poisons') on conflict do nothing;
    insert into mtgcard(name) values ('Bounty of the Luxa') on conflict do nothing;
    insert into mtgcard(name) values ('Splendid Agony') on conflict do nothing;
    insert into mtgcard(name) values ('Scaled Behemoth') on conflict do nothing;
    insert into mtgcard(name) values ('Liliana''s Mastery') on conflict do nothing;
    insert into mtgcard(name) values ('Warfire Javelineer') on conflict do nothing;
    insert into mtgcard(name) values ('Destined // Lead') on conflict do nothing;
    insert into mtgcard(name) values ('Nimble-Blade Khenra') on conflict do nothing;
    insert into mtgcard(name) values ('Oketra''s Attendant') on conflict do nothing;
    insert into mtgcard(name) values ('Swamp') on conflict do nothing;
    insert into mtgcard(name) values ('Samut, Voice of Dissent') on conflict do nothing;
    insert into mtgcard(name) values ('Aven Initiate') on conflict do nothing;
    insert into mtgcard(name) values ('Trueheart Duelist') on conflict do nothing;
    insert into mtgcard(name) values ('Companion of the Trials') on conflict do nothing;
    insert into mtgcard(name) values ('Slither Blade') on conflict do nothing;
    insert into mtgcard(name) values ('Greater Sandwurm') on conflict do nothing;
    insert into mtgcard(name) values ('Spring // Mind') on conflict do nothing;
    insert into mtgcard(name) values ('Enigma Drake') on conflict do nothing;
    insert into mtgcard(name) values ('Angel of Sanctions') on conflict do nothing;
    insert into mtgcard(name) values ('By Force') on conflict do nothing;
    insert into mtgcard(name) values ('Edifice of Authority') on conflict do nothing;
    insert into mtgcard(name) values ('Temmet, Vizier of Naktamun') on conflict do nothing;
    insert into mtgcard(name) values ('Evolving Wilds') on conflict do nothing;
    insert into mtgcard(name) values ('Decimator Beetle') on conflict do nothing;
    insert into mtgcard(name) values ('Gideon''s Intervention') on conflict do nothing;
    insert into mtgcard(name) values ('Galestrike') on conflict do nothing;
    insert into mtgcard(name) values ('Cancel') on conflict do nothing;
    insert into mtgcard(name) values ('Meandering River') on conflict do nothing;
    insert into mtgcard(name) values ('Scarab Feast') on conflict do nothing;
    insert into mtgcard(name) values ('Neheb, the Worthy') on conflict do nothing;
    insert into mtgcard(name) values ('Colossapede') on conflict do nothing;
    insert into mtgcard(name) values ('Cast Out') on conflict do nothing;
    insert into mtgcard(name) values ('Minotaur Sureshot') on conflict do nothing;
    insert into mtgcard(name) values ('Cascading Cataracts') on conflict do nothing;
    insert into mtgcard(name) values ('Hazoret''s Monument') on conflict do nothing;
    insert into mtgcard(name) values ('Highland Lake') on conflict do nothing;
    insert into mtgcard(name) values ('Forsaken Sanctuary') on conflict do nothing;
    insert into mtgcard(name) values ('Embalmer''s Tools') on conflict do nothing;
    insert into mtgcard(name) values ('Ancient Crab') on conflict do nothing;
    insert into mtgcard(name) values ('Compelling Argument') on conflict do nothing;
    insert into mtgcard(name) values ('Seeker of Insight') on conflict do nothing;
    insert into mtgcard(name) values ('Tattered Mummy') on conflict do nothing;
    insert into mtgcard(name) values ('Foul Orchard') on conflict do nothing;
    insert into mtgcard(name) values ('Shed Weakness') on conflict do nothing;
    insert into mtgcard(name) values ('Emberhorn Minotaur') on conflict do nothing;
    insert into mtgcard(name) values ('Manticore of the Gauntlet') on conflict do nothing;
    insert into mtgcard(name) values ('Seraph of the Suns') on conflict do nothing;
    insert into mtgcard(name) values ('Trial of Knowledge') on conflict do nothing;
    insert into mtgcard(name) values ('Harsh Mentor') on conflict do nothing;
    insert into mtgcard(name) values ('Unwavering Initiate') on conflict do nothing;
    insert into mtgcard(name) values ('Khenra Charioteer') on conflict do nothing;
    insert into mtgcard(name) values ('Painted Bluffs') on conflict do nothing;
    insert into mtgcard(name) values ('Hazoret''s Favor') on conflict do nothing;
    insert into mtgcard(name) values ('Gate to the Afterlife') on conflict do nothing;
    insert into mtgcard(name) values ('Ornery Kudu') on conflict do nothing;
    insert into mtgcard(name) values ('Giant Spider') on conflict do nothing;
    insert into mtgcard(name) values ('Supernatural Stamina') on conflict do nothing;
    insert into mtgcard(name) values ('Canyon Slough') on conflict do nothing;
    insert into mtgcard(name) values ('Cartouche of Zeal') on conflict do nothing;
    insert into mtgcard(name) values ('Blighted Bat') on conflict do nothing;
    insert into mtgcard(name) values ('Final Reward') on conflict do nothing;
    insert into mtgcard(name) values ('Naga Oracle') on conflict do nothing;
    insert into mtgcard(name) values ('Festering Mummy') on conflict do nothing;
    insert into mtgcard(name) values ('Cartouche of Solidarity') on conflict do nothing;
    insert into mtgcard(name) values ('Dune Beetle') on conflict do nothing;
    insert into mtgcard(name) values ('Prowling Serpopard') on conflict do nothing;
    insert into mtgcard(name) values ('Dusk // Dawn') on conflict do nothing;
    insert into mtgcard(name) values ('Gideon of the Trials') on conflict do nothing;
    insert into mtgcard(name) values ('Limits of Solidarity') on conflict do nothing;
    insert into mtgcard(name) values ('Gravedigger') on conflict do nothing;
    insert into mtgcard(name) values ('Thresher Lizard') on conflict do nothing;
    insert into mtgcard(name) values ('Nef-Crop Entangler') on conflict do nothing;
    insert into mtgcard(name) values ('Nest of Scarabs') on conflict do nothing;
    insert into mtgcard(name) values ('Grim Strider') on conflict do nothing;
    insert into mtgcard(name) values ('Magma Spray') on conflict do nothing;
    insert into mtgcard(name) values ('Deem Worthy') on conflict do nothing;
    insert into mtgcard(name) values ('Anointed Procession') on conflict do nothing;
    insert into mtgcard(name) values ('Shadow of the Grave') on conflict do nothing;
    insert into mtgcard(name) values ('Start // Finish') on conflict do nothing;
    insert into mtgcard(name) values ('Luxa River Shrine') on conflict do nothing;
    insert into mtgcard(name) values ('Doomed Dissenter') on conflict do nothing;
    insert into mtgcard(name) values ('Trial of Solidarity') on conflict do nothing;
    insert into mtgcard(name) values ('Glyph Keeper') on conflict do nothing;
    insert into mtgcard(name) values ('Gideon''s Resolve') on conflict do nothing;
    insert into mtgcard(name) values ('Cursed Minotaur') on conflict do nothing;
    insert into mtgcard(name) values ('Mouth // Feed') on conflict do nothing;
    insert into mtgcard(name) values ('Never // Return') on conflict do nothing;
    insert into mtgcard(name) values ('Illusory Wrappings') on conflict do nothing;
    insert into mtgcard(name) values ('Soul-Scar Mage') on conflict do nothing;
    insert into mtgcard(name) values ('River Serpent') on conflict do nothing;
    insert into mtgcard(name) values ('Grasping Dunes') on conflict do nothing;
    insert into mtgcard(name) values ('Impeccable Timing') on conflict do nothing;
    insert into mtgcard(name) values ('Tah-Crop Skirmisher') on conflict do nothing;
    insert into mtgcard(name) values ('Trueheart Twins') on conflict do nothing;
    insert into mtgcard(name) values ('Liliana, Death Wielder') on conflict do nothing;
    insert into mtgcard(name) values ('Trial of Strength') on conflict do nothing;
    insert into mtgcard(name) values ('Submerged Boneyard') on conflict do nothing;
    insert into mtgcard(name) values ('Djeru''s Resolve') on conflict do nothing;
    insert into mtgcard(name) values ('Fetid Pools') on conflict do nothing;
    insert into mtgcard(name) values ('Wander in Death') on conflict do nothing;
    insert into mtgcard(name) values ('Watchful Naga') on conflict do nothing;
    insert into mtgcard(name) values ('Champion of Rhonas') on conflict do nothing;
    insert into mtgcard(name) values ('Honored Crop-Captain') on conflict do nothing;
    insert into mtgcard(name) values ('Bloodlust Inciter') on conflict do nothing;
    insert into mtgcard(name) values ('Vizier of Many Faces') on conflict do nothing;
    insert into mtgcard(name) values ('Graceful Cat') on conflict do nothing;
    insert into mtgcard(name) values ('Those Who Serve') on conflict do nothing;
    insert into mtgcard(name) values ('Time to Reflect') on conflict do nothing;
    insert into mtgcard(name) values ('Haze of Pollen') on conflict do nothing;
    insert into mtgcard(name) values ('Defiant Greatmaw') on conflict do nothing;
    insert into mtgcard(name) values ('Failure // Comply') on conflict do nothing;
    insert into mtgcard(name) values ('Blazing Volley') on conflict do nothing;
    insert into mtgcard(name) values ('Harvest Season') on conflict do nothing;
    insert into mtgcard(name) values ('Bone Picker') on conflict do nothing;
    insert into mtgcard(name) values ('Painful Lesson') on conflict do nothing;
    insert into mtgcard(name) values ('Channeler Initiate') on conflict do nothing;
    insert into mtgcard(name) values ('Gift of Paradise') on conflict do nothing;
    insert into mtgcard(name) values ('New Perspectives') on conflict do nothing;
    insert into mtgcard(name) values ('Hieroglyphic Illumination') on conflict do nothing;
    insert into mtgcard(name) values ('Cut // Ribbons') on conflict do nothing;
    insert into mtgcard(name) values ('Nissa, Steward of Elements') on conflict do nothing;
    insert into mtgcard(name) values ('Honed Khopesh') on conflict do nothing;
    insert into mtgcard(name) values ('Timber Gorge') on conflict do nothing;
    insert into mtgcard(name) values ('Decision Paralysis') on conflict do nothing;
    insert into mtgcard(name) values ('Hekma Sentinels') on conflict do nothing;
    insert into mtgcard(name) values ('Vizier of the Menagerie') on conflict do nothing;
    insert into mtgcard(name) values ('Kefnet the Mindful') on conflict do nothing;
    insert into mtgcard(name) values ('Lay Claim') on conflict do nothing;
    insert into mtgcard(name) values ('Forsake the Worldly') on conflict do nothing;
    insert into mtgcard(name) values ('Scattered Groves') on conflict do nothing;
    insert into mtgcard(name) values ('In Oketra''s Name') on conflict do nothing;
    insert into mtgcard(name) values ('Regal Caracal') on conflict do nothing;
    insert into mtgcard(name) values ('Vizier of Tumbling Sands') on conflict do nothing;
    insert into mtgcard(name) values ('Tah-Crop Elite') on conflict do nothing;
    insert into mtgcard(name) values ('Naga Vitalist') on conflict do nothing;
    insert into mtgcard(name) values ('Pyramid of the Pantheon') on conflict do nothing;
    insert into mtgcard(name) values ('Angler Drake') on conflict do nothing;
    insert into mtgcard(name) values ('Sheltered Thicket') on conflict do nothing;
    insert into mtgcard(name) values ('Supply Caravan') on conflict do nothing;
    insert into mtgcard(name) values ('Reduce // Rubble') on conflict do nothing;
    insert into mtgcard(name) values ('Exemplar of Strength') on conflict do nothing;
    insert into mtgcard(name) values ('Devoted Crop-Mate') on conflict do nothing;
    insert into mtgcard(name) values ('Lay Bare the Heart') on conflict do nothing;
    insert into mtgcard(name) values ('Consuming Fervor') on conflict do nothing;
    insert into mtgcard(name) values ('Cartouche of Strength') on conflict do nothing;
    insert into mtgcard(name) values ('Cartouche of Knowledge') on conflict do nothing;
    insert into mtgcard(name) values ('Ruthless Sniper') on conflict do nothing;
    insert into mtgcard(name) values ('Weaver of Currents') on conflict do nothing;
    insert into mtgcard(name) values ('Trial of Ambition') on conflict do nothing;
    insert into mtgcard(name) values ('Benefaction of Rhonas') on conflict do nothing;
    insert into mtgcard(name) values ('Bitterblade Warrior') on conflict do nothing;
    insert into mtgcard(name) values ('Ahn-Crop Crasher') on conflict do nothing;
    insert into mtgcard(name) values ('Heart-Piercer Manticore') on conflict do nothing;
    insert into mtgcard(name) values ('Cinder Barrens') on conflict do nothing;
    insert into mtgcard(name) values ('Essence Scatter') on conflict do nothing;
    insert into mtgcard(name) values ('Sandwurm Convergence') on conflict do nothing;
    insert into mtgcard(name) values ('Sparring Mummy') on conflict do nothing;
    insert into mtgcard(name) values ('Sixth Sense') on conflict do nothing;
    insert into mtgcard(name) values ('Bloodrage Brawler') on conflict do nothing;
    insert into mtgcard(name) values ('Cartouche of Ambition') on conflict do nothing;
    insert into mtgcard(name) values ('Bontu''s Monument') on conflict do nothing;
    insert into mtgcard(name) values ('Wayward Servant') on conflict do nothing;
    insert into mtgcard(name) values ('Shadowstorm Vizier') on conflict do nothing;
    insert into mtgcard(name) values ('Shimmerscale Drake') on conflict do nothing;
    insert into mtgcard(name) values ('Merciless Javelineer') on conflict do nothing;
    insert into mtgcard(name) values ('Insult // Injury') on conflict do nothing;
    insert into mtgcard(name) values ('Fling') on conflict do nothing;
    insert into mtgcard(name) values ('Battlefield Scavenger') on conflict do nothing;
    insert into mtgcard(name) values ('Sweltering Suns') on conflict do nothing;
    insert into mtgcard(name) values ('Pull from Tomorrow') on conflict do nothing;
    insert into mtgcard(name) values ('Winds of Rebuke') on conflict do nothing;
    insert into mtgcard(name) values ('Oketra the True') on conflict do nothing;
    insert into mtgcard(name) values ('Pursue Glory') on conflict do nothing;
    insert into mtgcard(name) values ('Prepare // Fight') on conflict do nothing;
    insert into mtgcard(name) values ('Kefnet''s Monument') on conflict do nothing;
    insert into mtgcard(name) values ('Tranquil Expanse') on conflict do nothing;
    insert into mtgcard(name) values ('Fan Bearer') on conflict do nothing;
    insert into mtgcard(name) values ('Trespasser''s Curse') on conflict do nothing;
    insert into mtgcard(name) values ('Woodland Stream') on conflict do nothing;
    insert into mtgcard(name) values ('Flameblade Adept') on conflict do nothing;
    insert into mtgcard(name) values ('Hyena Pack') on conflict do nothing;
    insert into mtgcard(name) values ('Binding Mummy') on conflict do nothing;
    insert into mtgcard(name) values ('Baleful Ammit') on conflict do nothing;
    insert into mtgcard(name) values ('Pouncing Cheetah') on conflict do nothing;
    insert into mtgcard(name) values ('Synchronized Strike') on conflict do nothing;
    insert into mtgcard(name) values ('Approach of the Second Sun') on conflict do nothing;
    insert into mtgcard(name) values ('Heaven // Earth') on conflict do nothing;
    insert into mtgcard(name) values ('Throne of the God-Pharaoh') on conflict do nothing;
