insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Initiate''s Companion'),
    (select id from sets where short_name = 'akh'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'akh'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winged Shepherd'),
    (select id from sets where short_name = 'akh'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soulstinger'),
    (select id from sets where short_name = 'akh'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cruel Reality'),
    (select id from sets where short_name = 'akh'),
    '84',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Commit // Memory'),
    (select id from sets where short_name = 'akh'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sacred Cat'),
    (select id from sets where short_name = 'akh'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Renewed Faith'),
    (select id from sets where short_name = 'akh'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Manglehorn'),
    (select id from sets where short_name = 'akh'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spidery Grasp'),
    (select id from sets where short_name = 'akh'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stir the Sands'),
    (select id from sets where short_name = 'akh'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Open into Wonder'),
    (select id from sets where short_name = 'akh'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'As Foretold'),
    (select id from sets where short_name = 'akh'),
    '42',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Oketra''s Monument'),
    (select id from sets where short_name = 'akh'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Compulsory Rest'),
    (select id from sets where short_name = 'akh'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rags // Riches'),
    (select id from sets where short_name = 'akh'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mighty Leap'),
    (select id from sets where short_name = 'akh'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'akh'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Protection of the Hekma'),
    (select id from sets where short_name = 'akh'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Curator of Mysteries'),
    (select id from sets where short_name = 'akh'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Labyrinth Guardian'),
    (select id from sets where short_name = 'akh'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'akh'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hapatra''s Mark'),
    (select id from sets where short_name = 'akh'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brute Strength'),
    (select id from sets where short_name = 'akh'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quarry Hauler'),
    (select id from sets where short_name = 'akh'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hooded Brawler'),
    (select id from sets where short_name = 'akh'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sacred Excavation'),
    (select id from sets where short_name = 'akh'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desert Cerodon'),
    (select id from sets where short_name = 'akh'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'akh'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Honored Hydra'),
    (select id from sets where short_name = 'akh'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Electrify'),
    (select id from sets where short_name = 'akh'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desiccated Naga'),
    (select id from sets where short_name = 'akh'),
    '276',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stone Quarry'),
    (select id from sets where short_name = 'akh'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pitiless Vizier'),
    (select id from sets where short_name = 'akh'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glory-Bound Initiate'),
    (select id from sets where short_name = 'akh'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vizier of Deferment'),
    (select id from sets where short_name = 'akh'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shefet Monitor'),
    (select id from sets where short_name = 'akh'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plague Belcher'),
    (select id from sets where short_name = 'akh'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Combat Celebrant'),
    (select id from sets where short_name = 'akh'),
    '125',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Influence'),
    (select id from sets where short_name = 'akh'),
    '277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glorious End'),
    (select id from sets where short_name = 'akh'),
    '133',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wasteland Scorpion'),
    (select id from sets where short_name = 'akh'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gideon, Martial Paragon'),
    (select id from sets where short_name = 'akh'),
    '270',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'akh'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ahn-Crop Champion'),
    (select id from sets where short_name = 'akh'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'akh'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crocodile of the Crossing'),
    (select id from sets where short_name = 'akh'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tormenting Voice'),
    (select id from sets where short_name = 'akh'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unburden'),
    (select id from sets where short_name = 'akh'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Watchers of the Dead'),
    (select id from sets where short_name = 'akh'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glorybringer'),
    (select id from sets where short_name = 'akh'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archfiend of Ifnir'),
    (select id from sets where short_name = 'akh'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dread Wanderer'),
    (select id from sets where short_name = 'akh'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scribe of the Mindful'),
    (select id from sets where short_name = 'akh'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horror of the Broken Lands'),
    (select id from sets where short_name = 'akh'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vizier of Remedies'),
    (select id from sets where short_name = 'akh'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hazoret the Fervent'),
    (select id from sets where short_name = 'akh'),
    '136',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'akh'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lord of the Accursed'),
    (select id from sets where short_name = 'akh'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Floodwaters'),
    (select id from sets where short_name = 'akh'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Faith of the Devoted'),
    (select id from sets where short_name = 'akh'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oashra Cultivator'),
    (select id from sets where short_name = 'akh'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhet-Crop Spearmaster'),
    (select id from sets where short_name = 'akh'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Mindcensor'),
    (select id from sets where short_name = 'akh'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunscorched Desert'),
    (select id from sets where short_name = 'akh'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana, Death''s Majesty'),
    (select id from sets where short_name = 'akh'),
    '97',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aven Wind Guide'),
    (select id from sets where short_name = 'akh'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cradle of the Accursed'),
    (select id from sets where short_name = 'akh'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drake Haven'),
    (select id from sets where short_name = 'akh'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cryptic Serpent'),
    (select id from sets where short_name = 'akh'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oracle''s Vault'),
    (select id from sets where short_name = 'akh'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zenith Seeker'),
    (select id from sets where short_name = 'akh'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Miasmic Mummy'),
    (select id from sets where short_name = 'akh'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gust Walker'),
    (select id from sets where short_name = 'akh'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Violent Impact'),
    (select id from sets where short_name = 'akh'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bontu the Glorified'),
    (select id from sets where short_name = 'akh'),
    '82',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Censor'),
    (select id from sets where short_name = 'akh'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pathmaker Initiate'),
    (select id from sets where short_name = 'akh'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Irrigated Farmland'),
    (select id from sets where short_name = 'akh'),
    '245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dispossess'),
    (select id from sets where short_name = 'akh'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anointer Priest'),
    (select id from sets where short_name = 'akh'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhonas the Indomitable'),
    (select id from sets where short_name = 'akh'),
    '182',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Onward // Victory'),
    (select id from sets where short_name = 'akh'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rhonas''s Monument'),
    (select id from sets where short_name = 'akh'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trial of Zeal'),
    (select id from sets where short_name = 'akh'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dissenter''s Deliverance'),
    (select id from sets where short_name = 'akh'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stinging Shot'),
    (select id from sets where short_name = 'akh'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hapatra, Vizier of Poisons'),
    (select id from sets where short_name = 'akh'),
    '199',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bounty of the Luxa'),
    (select id from sets where short_name = 'akh'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Splendid Agony'),
    (select id from sets where short_name = 'akh'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scaled Behemoth'),
    (select id from sets where short_name = 'akh'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Mastery'),
    (select id from sets where short_name = 'akh'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warfire Javelineer'),
    (select id from sets where short_name = 'akh'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Destined // Lead'),
    (select id from sets where short_name = 'akh'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nimble-Blade Khenra'),
    (select id from sets where short_name = 'akh'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oketra''s Attendant'),
    (select id from sets where short_name = 'akh'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'akh'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Samut, Voice of Dissent'),
    (select id from sets where short_name = 'akh'),
    '205',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aven Initiate'),
    (select id from sets where short_name = 'akh'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trueheart Duelist'),
    (select id from sets where short_name = 'akh'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'akh'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Companion of the Trials'),
    (select id from sets where short_name = 'akh'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slither Blade'),
    (select id from sets where short_name = 'akh'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greater Sandwurm'),
    (select id from sets where short_name = 'akh'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spring // Mind'),
    (select id from sets where short_name = 'akh'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enigma Drake'),
    (select id from sets where short_name = 'akh'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel of Sanctions'),
    (select id from sets where short_name = 'akh'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'By Force'),
    (select id from sets where short_name = 'akh'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Edifice of Authority'),
    (select id from sets where short_name = 'akh'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Temmet, Vizier of Naktamun'),
    (select id from sets where short_name = 'akh'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'akh'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Decimator Beetle'),
    (select id from sets where short_name = 'akh'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'akh'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gideon''s Intervention'),
    (select id from sets where short_name = 'akh'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Galestrike'),
    (select id from sets where short_name = 'akh'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cancel'),
    (select id from sets where short_name = 'akh'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Meandering River'),
    (select id from sets where short_name = 'akh'),
    '283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scarab Feast'),
    (select id from sets where short_name = 'akh'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Neheb, the Worthy'),
    (select id from sets where short_name = 'akh'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Colossapede'),
    (select id from sets where short_name = 'akh'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cast Out'),
    (select id from sets where short_name = 'akh'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Minotaur Sureshot'),
    (select id from sets where short_name = 'akh'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cascading Cataracts'),
    (select id from sets where short_name = 'akh'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hazoret''s Monument'),
    (select id from sets where short_name = 'akh'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'akh'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Highland Lake'),
    (select id from sets where short_name = 'akh'),
    '282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forsaken Sanctuary'),
    (select id from sets where short_name = 'akh'),
    '281',
    'common'
) ,
(
    (select id from mtgcard where name = 'Embalmer''s Tools'),
    (select id from sets where short_name = 'akh'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Crab'),
    (select id from sets where short_name = 'akh'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Compelling Argument'),
    (select id from sets where short_name = 'akh'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seeker of Insight'),
    (select id from sets where short_name = 'akh'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tattered Mummy'),
    (select id from sets where short_name = 'akh'),
    '278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foul Orchard'),
    (select id from sets where short_name = 'akh'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shed Weakness'),
    (select id from sets where short_name = 'akh'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emberhorn Minotaur'),
    (select id from sets where short_name = 'akh'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Manticore of the Gauntlet'),
    (select id from sets where short_name = 'akh'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seraph of the Suns'),
    (select id from sets where short_name = 'akh'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trial of Knowledge'),
    (select id from sets where short_name = 'akh'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harsh Mentor'),
    (select id from sets where short_name = 'akh'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unwavering Initiate'),
    (select id from sets where short_name = 'akh'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Khenra Charioteer'),
    (select id from sets where short_name = 'akh'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Painted Bluffs'),
    (select id from sets where short_name = 'akh'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hazoret''s Favor'),
    (select id from sets where short_name = 'akh'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gate to the Afterlife'),
    (select id from sets where short_name = 'akh'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ornery Kudu'),
    (select id from sets where short_name = 'akh'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Spider'),
    (select id from sets where short_name = 'akh'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Supernatural Stamina'),
    (select id from sets where short_name = 'akh'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Canyon Slough'),
    (select id from sets where short_name = 'akh'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cartouche of Zeal'),
    (select id from sets where short_name = 'akh'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blighted Bat'),
    (select id from sets where short_name = 'akh'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Final Reward'),
    (select id from sets where short_name = 'akh'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naga Oracle'),
    (select id from sets where short_name = 'akh'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Festering Mummy'),
    (select id from sets where short_name = 'akh'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cartouche of Solidarity'),
    (select id from sets where short_name = 'akh'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dune Beetle'),
    (select id from sets where short_name = 'akh'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prowling Serpopard'),
    (select id from sets where short_name = 'akh'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dusk // Dawn'),
    (select id from sets where short_name = 'akh'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gideon of the Trials'),
    (select id from sets where short_name = 'akh'),
    '14',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Limits of Solidarity'),
    (select id from sets where short_name = 'akh'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'akh'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thresher Lizard'),
    (select id from sets where short_name = 'akh'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nef-Crop Entangler'),
    (select id from sets where short_name = 'akh'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nest of Scarabs'),
    (select id from sets where short_name = 'akh'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grim Strider'),
    (select id from sets where short_name = 'akh'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magma Spray'),
    (select id from sets where short_name = 'akh'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deem Worthy'),
    (select id from sets where short_name = 'akh'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anointed Procession'),
    (select id from sets where short_name = 'akh'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shadow of the Grave'),
    (select id from sets where short_name = 'akh'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Start // Finish'),
    (select id from sets where short_name = 'akh'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'akh'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Luxa River Shrine'),
    (select id from sets where short_name = 'akh'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doomed Dissenter'),
    (select id from sets where short_name = 'akh'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trial of Solidarity'),
    (select id from sets where short_name = 'akh'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glyph Keeper'),
    (select id from sets where short_name = 'akh'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gideon''s Resolve'),
    (select id from sets where short_name = 'akh'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cursed Minotaur'),
    (select id from sets where short_name = 'akh'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mouth // Feed'),
    (select id from sets where short_name = 'akh'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Never // Return'),
    (select id from sets where short_name = 'akh'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Illusory Wrappings'),
    (select id from sets where short_name = 'akh'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul-Scar Mage'),
    (select id from sets where short_name = 'akh'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'River Serpent'),
    (select id from sets where short_name = 'akh'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grasping Dunes'),
    (select id from sets where short_name = 'akh'),
    '244',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Impeccable Timing'),
    (select id from sets where short_name = 'akh'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tah-Crop Skirmisher'),
    (select id from sets where short_name = 'akh'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trueheart Twins'),
    (select id from sets where short_name = 'akh'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana, Death Wielder'),
    (select id from sets where short_name = 'akh'),
    '275',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Trial of Strength'),
    (select id from sets where short_name = 'akh'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Submerged Boneyard'),
    (select id from sets where short_name = 'akh'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Djeru''s Resolve'),
    (select id from sets where short_name = 'akh'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fetid Pools'),
    (select id from sets where short_name = 'akh'),
    '243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wander in Death'),
    (select id from sets where short_name = 'akh'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Watchful Naga'),
    (select id from sets where short_name = 'akh'),
    '193',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Champion of Rhonas'),
    (select id from sets where short_name = 'akh'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Honored Crop-Captain'),
    (select id from sets where short_name = 'akh'),
    '200',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodlust Inciter'),
    (select id from sets where short_name = 'akh'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vizier of Many Faces'),
    (select id from sets where short_name = 'akh'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Graceful Cat'),
    (select id from sets where short_name = 'akh'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Those Who Serve'),
    (select id from sets where short_name = 'akh'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time to Reflect'),
    (select id from sets where short_name = 'akh'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Haze of Pollen'),
    (select id from sets where short_name = 'akh'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'akh'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Defiant Greatmaw'),
    (select id from sets where short_name = 'akh'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Failure // Comply'),
    (select id from sets where short_name = 'akh'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blazing Volley'),
    (select id from sets where short_name = 'akh'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'akh'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harvest Season'),
    (select id from sets where short_name = 'akh'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bone Picker'),
    (select id from sets where short_name = 'akh'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'akh'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Painful Lesson'),
    (select id from sets where short_name = 'akh'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Channeler Initiate'),
    (select id from sets where short_name = 'akh'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gift of Paradise'),
    (select id from sets where short_name = 'akh'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'akh'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'New Perspectives'),
    (select id from sets where short_name = 'akh'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hieroglyphic Illumination'),
    (select id from sets where short_name = 'akh'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cut // Ribbons'),
    (select id from sets where short_name = 'akh'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'akh'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nissa, Steward of Elements'),
    (select id from sets where short_name = 'akh'),
    '204',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Honed Khopesh'),
    (select id from sets where short_name = 'akh'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Timber Gorge'),
    (select id from sets where short_name = 'akh'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Decision Paralysis'),
    (select id from sets where short_name = 'akh'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hekma Sentinels'),
    (select id from sets where short_name = 'akh'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vizier of the Menagerie'),
    (select id from sets where short_name = 'akh'),
    '192',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kefnet the Mindful'),
    (select id from sets where short_name = 'akh'),
    '59',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lay Claim'),
    (select id from sets where short_name = 'akh'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forsake the Worldly'),
    (select id from sets where short_name = 'akh'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scattered Groves'),
    (select id from sets where short_name = 'akh'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'In Oketra''s Name'),
    (select id from sets where short_name = 'akh'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Regal Caracal'),
    (select id from sets where short_name = 'akh'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vizier of Tumbling Sands'),
    (select id from sets where short_name = 'akh'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tah-Crop Elite'),
    (select id from sets where short_name = 'akh'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naga Vitalist'),
    (select id from sets where short_name = 'akh'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyramid of the Pantheon'),
    (select id from sets where short_name = 'akh'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angler Drake'),
    (select id from sets where short_name = 'akh'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sheltered Thicket'),
    (select id from sets where short_name = 'akh'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Supply Caravan'),
    (select id from sets where short_name = 'akh'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reduce // Rubble'),
    (select id from sets where short_name = 'akh'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Exemplar of Strength'),
    (select id from sets where short_name = 'akh'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'akh'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Devoted Crop-Mate'),
    (select id from sets where short_name = 'akh'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lay Bare the Heart'),
    (select id from sets where short_name = 'akh'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Consuming Fervor'),
    (select id from sets where short_name = 'akh'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'akh'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cartouche of Strength'),
    (select id from sets where short_name = 'akh'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cartouche of Knowledge'),
    (select id from sets where short_name = 'akh'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruthless Sniper'),
    (select id from sets where short_name = 'akh'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weaver of Currents'),
    (select id from sets where short_name = 'akh'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trial of Ambition'),
    (select id from sets where short_name = 'akh'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Benefaction of Rhonas'),
    (select id from sets where short_name = 'akh'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bitterblade Warrior'),
    (select id from sets where short_name = 'akh'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ahn-Crop Crasher'),
    (select id from sets where short_name = 'akh'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heart-Piercer Manticore'),
    (select id from sets where short_name = 'akh'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cinder Barrens'),
    (select id from sets where short_name = 'akh'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Essence Scatter'),
    (select id from sets where short_name = 'akh'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'akh'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sandwurm Convergence'),
    (select id from sets where short_name = 'akh'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sparring Mummy'),
    (select id from sets where short_name = 'akh'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sixth Sense'),
    (select id from sets where short_name = 'akh'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodrage Brawler'),
    (select id from sets where short_name = 'akh'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cartouche of Ambition'),
    (select id from sets where short_name = 'akh'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bontu''s Monument'),
    (select id from sets where short_name = 'akh'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wayward Servant'),
    (select id from sets where short_name = 'akh'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shadowstorm Vizier'),
    (select id from sets where short_name = 'akh'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shimmerscale Drake'),
    (select id from sets where short_name = 'akh'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merciless Javelineer'),
    (select id from sets where short_name = 'akh'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Insult // Injury'),
    (select id from sets where short_name = 'akh'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fling'),
    (select id from sets where short_name = 'akh'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battlefield Scavenger'),
    (select id from sets where short_name = 'akh'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sweltering Suns'),
    (select id from sets where short_name = 'akh'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pull from Tomorrow'),
    (select id from sets where short_name = 'akh'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winds of Rebuke'),
    (select id from sets where short_name = 'akh'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oketra the True'),
    (select id from sets where short_name = 'akh'),
    '21',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pursue Glory'),
    (select id from sets where short_name = 'akh'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prepare // Fight'),
    (select id from sets where short_name = 'akh'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kefnet''s Monument'),
    (select id from sets where short_name = 'akh'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquil Expanse'),
    (select id from sets where short_name = 'akh'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fan Bearer'),
    (select id from sets where short_name = 'akh'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trespasser''s Curse'),
    (select id from sets where short_name = 'akh'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woodland Stream'),
    (select id from sets where short_name = 'akh'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flameblade Adept'),
    (select id from sets where short_name = 'akh'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hyena Pack'),
    (select id from sets where short_name = 'akh'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Binding Mummy'),
    (select id from sets where short_name = 'akh'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baleful Ammit'),
    (select id from sets where short_name = 'akh'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pouncing Cheetah'),
    (select id from sets where short_name = 'akh'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Synchronized Strike'),
    (select id from sets where short_name = 'akh'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Approach of the Second Sun'),
    (select id from sets where short_name = 'akh'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heaven // Earth'),
    (select id from sets where short_name = 'akh'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Throne of the God-Pharaoh'),
    (select id from sets where short_name = 'akh'),
    '237',
    'rare'
) 
 on conflict do nothing;
