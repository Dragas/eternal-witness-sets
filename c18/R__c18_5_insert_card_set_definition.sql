insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Whiptongue Hydra'),
    (select id from sets where short_name = 'c18'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hunting Wilds'),
    (select id from sets where short_name = 'c18'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krosan Verge'),
    (select id from sets where short_name = 'c18'),
    '263',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cloudform'),
    (select id from sets where short_name = 'c18'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hellkite Igniter'),
    (select id from sets where short_name = 'c18'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saheeli''s Artistry'),
    (select id from sets where short_name = 'c18'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archetype of Imagination'),
    (select id from sets where short_name = 'c18'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vedalken Humiliator'),
    (select id from sets where short_name = 'c18'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Estrid''s Invocation'),
    (select id from sets where short_name = 'c18'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Worn Powerstone'),
    (select id from sets where short_name = 'c18'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skull Storm'),
    (select id from sets where short_name = 'c18'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Geode Golem'),
    (select id from sets where short_name = 'c18'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Genesis Storm'),
    (select id from sets where short_name = 'c18'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inkwell Leviathan'),
    (select id from sets where short_name = 'c18'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savage Twister'),
    (select id from sets where short_name = 'c18'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azorius Chancery'),
    (select id from sets where short_name = 'c18'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jungle Hollow'),
    (select id from sets where short_name = 'c18'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Entreat the Angels'),
    (select id from sets where short_name = 'c18'),
    '67',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Foundry of the Consuls'),
    (select id from sets where short_name = 'c18'),
    '248',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Putrefy'),
    (select id from sets where short_name = 'c18'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eidolon of Blossoms'),
    (select id from sets where short_name = 'c18'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reclamation Sage'),
    (select id from sets where short_name = 'c18'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Akroma''s Vengeance'),
    (select id from sets where short_name = 'c18'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Growth Chamber'),
    (select id from sets where short_name = 'c18'),
    '282',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Charnelhoard Wurm'),
    (select id from sets where short_name = 'c18'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fury Storm'),
    (select id from sets where short_name = 'c18'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tranquil Expanse'),
    (select id from sets where short_name = 'c18'),
    '289',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dawn''s Reflection'),
    (select id from sets where short_name = 'c18'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chief of the Foundry'),
    (select id from sets where short_name = 'c18'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hydra Omnivore'),
    (select id from sets where short_name = 'c18'),
    '153',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aminatou, the Fateshifter'),
    (select id from sets where short_name = 'c18'),
    '37',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Izzet Boilerworks'),
    (select id from sets where short_name = 'c18'),
    '256',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Growth'),
    (select id from sets where short_name = 'c18'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swiftfoot Boots'),
    (select id from sets where short_name = 'c18'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bant Charm'),
    (select id from sets where short_name = 'c18'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loyal Drake'),
    (select id from sets where short_name = 'c18'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sphinx of Uthuun'),
    (select id from sets where short_name = 'c18'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of the False God'),
    (select id from sets where short_name = 'c18'),
    '285',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loyal Guardian'),
    (select id from sets where short_name = 'c18'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seat of the Synod'),
    (select id from sets where short_name = 'c18'),
    '278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magnifying Glass'),
    (select id from sets where short_name = 'c18'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azorius Signet'),
    (select id from sets where short_name = 'c18'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Far Wanderings'),
    (select id from sets where short_name = 'c18'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lord Windgrace'),
    (select id from sets where short_name = 'c18'),
    '43',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Darksteel Juggernaut'),
    (select id from sets where short_name = 'c18'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rampaging Baloths'),
    (select id from sets where short_name = 'c18'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forgotten Cave'),
    (select id from sets where short_name = 'c18'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c18'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scute Mob'),
    (select id from sets where short_name = 'c18'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maverick Thopterist'),
    (select id from sets where short_name = 'c18'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Conundrum Sphinx'),
    (select id from sets where short_name = 'c18'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Halimar Depths'),
    (select id from sets where short_name = 'c18'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avenger of Zendikar'),
    (select id from sets where short_name = 'c18'),
    '129',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sejiri Refuge'),
    (select id from sets where short_name = 'c18'),
    '280',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Devastation Tide'),
    (select id from sets where short_name = 'c18'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grapple with the Past'),
    (select id from sets where short_name = 'c18'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Khalni Heart Expedition'),
    (select id from sets where short_name = 'c18'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Entreat the Dead'),
    (select id from sets where short_name = 'c18'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Estrid, the Masked'),
    (select id from sets where short_name = 'c18'),
    '40',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Reality Scramble'),
    (select id from sets where short_name = 'c18'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Predict'),
    (select id from sets where short_name = 'c18'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Worm Harvest'),
    (select id from sets where short_name = 'c18'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blinkmoth Urn'),
    (select id from sets where short_name = 'c18'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yuriko, the Tiger''s Shadow'),
    (select id from sets where short_name = 'c18'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warped Landscape'),
    (select id from sets where short_name = 'c18'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c18'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Etherium Sculptor'),
    (select id from sets where short_name = 'c18'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Khalni Garden'),
    (select id from sets where short_name = 'c18'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enigma Sphinx'),
    (select id from sets where short_name = 'c18'),
    '178',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Stone'),
    (select id from sets where short_name = 'c18'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Sanctuary'),
    (select id from sets where short_name = 'c18'),
    '281',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mortify'),
    (select id from sets where short_name = 'c18'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Submerged Boneyard'),
    (select id from sets where short_name = 'c18'),
    '283',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spawning Grounds'),
    (select id from sets where short_name = 'c18'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boon Satyr'),
    (select id from sets where short_name = 'c18'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saheeli''s Directive'),
    (select id from sets where short_name = 'c18'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Meandering River'),
    (select id from sets where short_name = 'c18'),
    '265',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jeskai Infiltrator'),
    (select id from sets where short_name = 'c18'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ravenous Slime'),
    (select id from sets where short_name = 'c18'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Echo Storm'),
    (select id from sets where short_name = 'c18'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Consign to Dust'),
    (select id from sets where short_name = 'c18'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steel Hellkite'),
    (select id from sets where short_name = 'c18'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harrow'),
    (select id from sets where short_name = 'c18'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eel Umbra'),
    (select id from sets where short_name = 'c18'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Budoka Gardener // Dokai, Weaver of Life'),
    (select id from sets where short_name = 'c18'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bruna, Light of Alabaster'),
    (select id from sets where short_name = 'c18'),
    '170',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vow of Wildness'),
    (select id from sets where short_name = 'c18'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Baloth Woodcrasher'),
    (select id from sets where short_name = 'c18'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boreas Charger'),
    (select id from sets where short_name = 'c18'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nylea''s Colossus'),
    (select id from sets where short_name = 'c18'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Retrofitter Foundry'),
    (select id from sets where short_name = 'c18'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fertile Ground'),
    (select id from sets where short_name = 'c18'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Return to Dust'),
    (select id from sets where short_name = 'c18'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ninja of the Deep Hours'),
    (select id from sets where short_name = 'c18'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Esper Charm'),
    (select id from sets where short_name = 'c18'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquil Thicket'),
    (select id from sets where short_name = 'c18'),
    '290',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hedron Archive'),
    (select id from sets where short_name = 'c18'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Decimate'),
    (select id from sets where short_name = 'c18'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moonlight Bargain'),
    (select id from sets where short_name = 'c18'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mulldrifter'),
    (select id from sets where short_name = 'c18'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aethermage''s Touch'),
    (select id from sets where short_name = 'c18'),
    '168',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c18'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cold-Eyed Selkie'),
    (select id from sets where short_name = 'c18'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Elder'),
    (select id from sets where short_name = 'c18'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seaside Citadel'),
    (select id from sets where short_name = 'c18'),
    '277',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sigiled Starfish'),
    (select id from sets where short_name = 'c18'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tidings'),
    (select id from sets where short_name = 'c18'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treasure Nabber'),
    (select id from sets where short_name = 'c18'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blighted Woodland'),
    (select id from sets where short_name = 'c18'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Akoum Refuge'),
    (select id from sets where short_name = 'c18'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Command Tower'),
    (select id from sets where short_name = 'c18'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kestia, the Cultivator'),
    (select id from sets where short_name = 'c18'),
    '42',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Explosive Vegetation'),
    (select id from sets where short_name = 'c18'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dismantling Blow'),
    (select id from sets where short_name = 'c18'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orzhov Guildgate'),
    (select id from sets where short_name = 'c18'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crib Swap'),
    (select id from sets where short_name = 'c18'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra Avatar'),
    (select id from sets where short_name = 'c18'),
    '73',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Creeping Renaissance'),
    (select id from sets where short_name = 'c18'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Varina, Lich Queen'),
    (select id from sets where short_name = 'c18'),
    '48',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Rebirth'),
    (select id from sets where short_name = 'c18'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sharding Sphinx'),
    (select id from sets where short_name = 'c18'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silent Sentinel'),
    (select id from sets where short_name = 'c18'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whirler Rogue'),
    (select id from sets where short_name = 'c18'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Epic Proportions'),
    (select id from sets where short_name = 'c18'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treasure Hunt'),
    (select id from sets where short_name = 'c18'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c18'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruinous Path'),
    (select id from sets where short_name = 'c18'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Izzet Guildgate'),
    (select id from sets where short_name = 'c18'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter Engineer'),
    (select id from sets where short_name = 'c18'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Farhaven Elf'),
    (select id from sets where short_name = 'c18'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Varchild, Betrayer of Kjeldor'),
    (select id from sets where short_name = 'c18'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scuttling Doom Engine'),
    (select id from sets where short_name = 'c18'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Psychosis Crawler'),
    (select id from sets where short_name = 'c18'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unstable Obelisk'),
    (select id from sets where short_name = 'c18'),
    '227',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loyal Unicorn'),
    (select id from sets where short_name = 'c18'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c18'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gaze of Granite'),
    (select id from sets where short_name = 'c18'),
    '181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vow of Flight'),
    (select id from sets where short_name = 'c18'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aura Gnarlid'),
    (select id from sets where short_name = 'c18'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chaos Warp'),
    (select id from sets where short_name = 'c18'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Into the Roil'),
    (select id from sets where short_name = 'c18'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'c18'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forge of Heroes'),
    (select id from sets where short_name = 'c18'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightform'),
    (select id from sets where short_name = 'c18'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'c18'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Chosen'),
    (select id from sets where short_name = 'c18'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c18'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rocky Tar Pit'),
    (select id from sets where short_name = 'c18'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stitch Together'),
    (select id from sets where short_name = 'c18'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orzhov Basilica'),
    (select id from sets where short_name = 'c18'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heavenly Blademaster'),
    (select id from sets where short_name = 'c18'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myr Battlesphere'),
    (select id from sets where short_name = 'c18'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Xantcha, Sleeper Agent'),
    (select id from sets where short_name = 'c18'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nesting Dragon'),
    (select id from sets where short_name = 'c18'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grisly Salvage'),
    (select id from sets where short_name = 'c18'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Commander''s Sphere'),
    (select id from sets where short_name = 'c18'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Herald of the Pantheon'),
    (select id from sets where short_name = 'c18'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathreap Ritual'),
    (select id from sets where short_name = 'c18'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jwar Isle Refuge'),
    (select id from sets where short_name = 'c18'),
    '260',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zendikar Incarnate'),
    (select id from sets where short_name = 'c18'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blossoming Sands'),
    (select id from sets where short_name = 'c18'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Righteous Authority'),
    (select id from sets where short_name = 'c18'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magmaquake'),
    (select id from sets where short_name = 'c18'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ponder'),
    (select id from sets where short_name = 'c18'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gyrus, Waker of Corpses'),
    (select id from sets where short_name = 'c18'),
    '41',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thantis, the Warweaver'),
    (select id from sets where short_name = 'c18'),
    '46',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bojuka Bog'),
    (select id from sets where short_name = 'c18'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c18'),
    '302',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enchantress''s Presence'),
    (select id from sets where short_name = 'c18'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain Valley'),
    (select id from sets where short_name = 'c18'),
    '268',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scoured Barrens'),
    (select id from sets where short_name = 'c18'),
    '276',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pilgrim''s Eye'),
    (select id from sets where short_name = 'c18'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Utter End'),
    (select id from sets where short_name = 'c18'),
    '193',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Haunted Fengraf'),
    (select id from sets where short_name = 'c18'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c18'),
    '303',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirrorworks'),
    (select id from sets where short_name = 'c18'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Signet'),
    (select id from sets where short_name = 'c18'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Army of the Damned'),
    (select id from sets where short_name = 'c18'),
    '113',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Seer''s Lantern'),
    (select id from sets where short_name = 'c18'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azorius Guildgate'),
    (select id from sets where short_name = 'c18'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duskmantle Seer'),
    (select id from sets where short_name = 'c18'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Isolated Watchtower'),
    (select id from sets where short_name = 'c18'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Octopus Umbra'),
    (select id from sets where short_name = 'c18'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Telling Time'),
    (select id from sets where short_name = 'c18'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tawnos, Urza''s Apprentice'),
    (select id from sets where short_name = 'c18'),
    '45',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Blasphemous Act'),
    (select id from sets where short_name = 'c18'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'New Benalia'),
    (select id from sets where short_name = 'c18'),
    '270',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scrabbling Claws'),
    (select id from sets where short_name = 'c18'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sphinx of Jwar Isle'),
    (select id from sets where short_name = 'c18'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tranquil Cove'),
    (select id from sets where short_name = 'c18'),
    '288',
    'common'
) ,
(
    (select id from mtgcard where name = 'Djinn of Wishes'),
    (select id from sets where short_name = 'c18'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c18'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seer''s Sundial'),
    (select id from sets where short_name = 'c18'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Rot Farm'),
    (select id from sets where short_name = 'c18'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dictate of Kruphix'),
    (select id from sets where short_name = 'c18'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Banishing Stroke'),
    (select id from sets where short_name = 'c18'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Acidic Slime'),
    (select id from sets where short_name = 'c18'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Finest Hour'),
    (select id from sets where short_name = 'c18'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barren Moor'),
    (select id from sets where short_name = 'c18'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sower of Discord'),
    (select id from sets where short_name = 'c18'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'High Priest of Penance'),
    (select id from sets where short_name = 'c18'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moldgraf Monstrosity'),
    (select id from sets where short_name = 'c18'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'c18'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lonely Sandbar'),
    (select id from sets where short_name = 'c18'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mosswort Bridge'),
    (select id from sets where short_name = 'c18'),
    '267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Loyal Apprentice'),
    (select id from sets where short_name = 'c18'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ever-Watching Threshold'),
    (select id from sets where short_name = 'c18'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thopter Assembly'),
    (select id from sets where short_name = 'c18'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kazandu Refuge'),
    (select id from sets where short_name = 'c18'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Myth Unbound'),
    (select id from sets where short_name = 'c18'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Carnarium'),
    (select id from sets where short_name = 'c18'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Borderland Explorer'),
    (select id from sets where short_name = 'c18'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emissary of Grudges'),
    (select id from sets where short_name = 'c18'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vessel of Endless Rest'),
    (select id from sets where short_name = 'c18'),
    '229',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grim Backwoods'),
    (select id from sets where short_name = 'c18'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Secluded Steppe'),
    (select id from sets where short_name = 'c18'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lavalanche'),
    (select id from sets where short_name = 'c18'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jund Panorama'),
    (select id from sets where short_name = 'c18'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul of New Phyrexia'),
    (select id from sets where short_name = 'c18'),
    '223',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Enchanter''s Bane'),
    (select id from sets where short_name = 'c18'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tuvasa the Sunlit'),
    (select id from sets where short_name = 'c18'),
    '47',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Reverse Engineer'),
    (select id from sets where short_name = 'c18'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Izzet Signet'),
    (select id from sets where short_name = 'c18'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prismatic Lens'),
    (select id from sets where short_name = 'c18'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elderwood Scion'),
    (select id from sets where short_name = 'c18'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Martial Coup'),
    (select id from sets where short_name = 'c18'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savage Lands'),
    (select id from sets where short_name = 'c18'),
    '275',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul of Innistrad'),
    (select id from sets where short_name = 'c18'),
    '118',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Unwinding Clock'),
    (select id from sets where short_name = 'c18'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestial Archon'),
    (select id from sets where short_name = 'c18'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ground Seal'),
    (select id from sets where short_name = 'c18'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woodland Stream'),
    (select id from sets where short_name = 'c18'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mortuary Mire'),
    (select id from sets where short_name = 'c18'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flameblast Dragon'),
    (select id from sets where short_name = 'c18'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Night Incarnate'),
    (select id from sets where short_name = 'c18'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arixmethes, Slumbering Isle'),
    (select id from sets where short_name = 'c18'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terminus'),
    (select id from sets where short_name = 'c18'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Retreat to Hagra'),
    (select id from sets where short_name = 'c18'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crash of Rhino Beetles'),
    (select id from sets where short_name = 'c18'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brudiclad, Telchor Engineer'),
    (select id from sets where short_name = 'c18'),
    '39',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Saheeli, the Gifted'),
    (select id from sets where short_name = 'c18'),
    '44',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Unquestioned Authority'),
    (select id from sets where short_name = 'c18'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodtracker'),
    (select id from sets where short_name = 'c18'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'c18'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chain Reaction'),
    (select id from sets where short_name = 'c18'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arcane Sanctum'),
    (select id from sets where short_name = 'c18'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Great Furnace'),
    (select id from sets where short_name = 'c18'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Turntimber Sower'),
    (select id from sets where short_name = 'c18'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dimir Signet'),
    (select id from sets where short_name = 'c18'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Highland Lake'),
    (select id from sets where short_name = 'c18'),
    '255',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mimic Vat'),
    (select id from sets where short_name = 'c18'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sage''s Reverie'),
    (select id from sets where short_name = 'c18'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Endless Atlas'),
    (select id from sets where short_name = 'c18'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crystal Ball'),
    (select id from sets where short_name = 'c18'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Stone Idol'),
    (select id from sets where short_name = 'c18'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'c18'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kruphix''s Insight'),
    (select id from sets where short_name = 'c18'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darksteel Citadel'),
    (select id from sets where short_name = 'c18'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cultivate'),
    (select id from sets where short_name = 'c18'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Elder'),
    (select id from sets where short_name = 'c18'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dream Cache'),
    (select id from sets where short_name = 'c18'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windgrace''s Judgment'),
    (select id from sets where short_name = 'c18'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magus of the Balance'),
    (select id from sets where short_name = 'c18'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dimir Aqueduct'),
    (select id from sets where short_name = 'c18'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Empyrial Storm'),
    (select id from sets where short_name = 'c18'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Delver'),
    (select id from sets where short_name = 'c18'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreamstone Hedron'),
    (select id from sets where short_name = 'c18'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Portent'),
    (select id from sets where short_name = 'c18'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prototype Portal'),
    (select id from sets where short_name = 'c18'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overgrowth'),
    (select id from sets where short_name = 'c18'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul Turf'),
    (select id from sets where short_name = 'c18'),
    '252',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rubblehulk'),
    (select id from sets where short_name = 'c18'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myriad Landscape'),
    (select id from sets where short_name = 'c18'),
    '269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c18'),
    '307',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primordial Mist'),
    (select id from sets where short_name = 'c18'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Enchantress'),
    (select id from sets where short_name = 'c18'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'c18'),
    '306',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aether Gale'),
    (select id from sets where short_name = 'c18'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duplicant'),
    (select id from sets where short_name = 'c18'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dismal Backwater'),
    (select id from sets where short_name = 'c18'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whitewater Naiads'),
    (select id from sets where short_name = 'c18'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daxos of Meletis'),
    (select id from sets where short_name = 'c18'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thopter Spy Network'),
    (select id from sets where short_name = 'c18'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aminatou''s Augury'),
    (select id from sets where short_name = 'c18'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Centaur Vinecrasher'),
    (select id from sets where short_name = 'c18'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snake Umbra'),
    (select id from sets where short_name = 'c18'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silent-Blade Oni'),
    (select id from sets where short_name = 'c18'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bear Umbra'),
    (select id from sets where short_name = 'c18'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swiftwater Cliffs'),
    (select id from sets where short_name = 'c18'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'c18'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winds of Rath'),
    (select id from sets where short_name = 'c18'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forsaken Sanctuary'),
    (select id from sets where short_name = 'c18'),
    '247',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loyal Subordinate'),
    (select id from sets where short_name = 'c18'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Guildgate'),
    (select id from sets where short_name = 'c18'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Snare'),
    (select id from sets where short_name = 'c18'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coveted Jewel'),
    (select id from sets where short_name = 'c18'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'c18'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thirst for Knowledge'),
    (select id from sets where short_name = 'c18'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Explore'),
    (select id from sets where short_name = 'c18'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yennett, Cryptic Sovereign'),
    (select id from sets where short_name = 'c18'),
    '51',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bosh, Iron Golem'),
    (select id from sets where short_name = 'c18'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'c18'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Adarkar Valkyrie'),
    (select id from sets where short_name = 'c18'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thornwood Falls'),
    (select id from sets where short_name = 'c18'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sigil of the Empty Throne'),
    (select id from sets where short_name = 'c18'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Buried Ruin'),
    (select id from sets where short_name = 'c18'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unflinching Courage'),
    (select id from sets where short_name = 'c18'),
    '192',
    'uncommon'
) 
 on conflict do nothing;
