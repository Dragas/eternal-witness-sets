insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tddg'),
    '1',
    'common'
) 
 on conflict do nothing;
