insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Lightkeeper of Emeria'),
    (select id from sets where short_name = 'cmd'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Death Mutation'),
    (select id from sets where short_name = 'cmd'),
    '192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquil Thicket'),
    (select id from sets where short_name = 'cmd'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'Firespout'),
    (select id from sets where short_name = 'cmd'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chaos Warp'),
    (select id from sets where short_name = 'cmd'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slipstream Eel'),
    (select id from sets where short_name = 'cmd'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chromeshell Crab'),
    (select id from sets where short_name = 'cmd'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fierce Empath'),
    (select id from sets where short_name = 'cmd'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyscribing'),
    (select id from sets where short_name = 'cmd'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Garruk Wildspeaker'),
    (select id from sets where short_name = 'cmd'),
    '157',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nantuko Husk'),
    (select id from sets where short_name = 'cmd'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mother of Runes'),
    (select id from sets where short_name = 'cmd'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana-Charged Dragon'),
    (select id from sets where short_name = 'cmd'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diabolic Tutor'),
    (select id from sets where short_name = 'cmd'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Celestial Force'),
    (select id from sets where short_name = 'cmd'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evincar''s Justice'),
    (select id from sets where short_name = 'cmd'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lhurgoyf'),
    (select id from sets where short_name = 'cmd'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sulfurous Blast'),
    (select id from sets where short_name = 'cmd'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Squallmonger'),
    (select id from sets where short_name = 'cmd'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bathe in Light'),
    (select id from sets where short_name = 'cmd'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grave Pact'),
    (select id from sets where short_name = 'cmd'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eternal Witness'),
    (select id from sets where short_name = 'cmd'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stranglehold'),
    (select id from sets where short_name = 'cmd'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Invigorate'),
    (select id from sets where short_name = 'cmd'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cmd'),
    '318',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cmd'),
    '308',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivid Crag'),
    (select id from sets where short_name = 'cmd'),
    '293',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Footbottom Feast'),
    (select id from sets where short_name = 'cmd'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spike Feeder'),
    (select id from sets where short_name = 'cmd'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selesnya Signet'),
    (select id from sets where short_name = 'cmd'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fog Bank'),
    (select id from sets where short_name = 'cmd'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Simic Signet'),
    (select id from sets where short_name = 'cmd'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duergar Hedge-Mage'),
    (select id from sets where short_name = 'cmd'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spawnwrithe'),
    (select id from sets where short_name = 'cmd'),
    '171',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avatar of Woe'),
    (select id from sets where short_name = 'cmd'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cmd'),
    '301',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flusterstorm'),
    (select id from sets where short_name = 'cmd'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akroma''s Vengeance'),
    (select id from sets where short_name = 'cmd'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Buried Alive'),
    (select id from sets where short_name = 'cmd'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scattering Stroke'),
    (select id from sets where short_name = 'cmd'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'cmd'),
    '313',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Mimeoplasm'),
    (select id from sets where short_name = 'cmd'),
    '210',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Brion Stoutarm'),
    (select id from sets where short_name = 'cmd'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spell Crumple'),
    (select id from sets where short_name = 'cmd'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Perilous Research'),
    (select id from sets where short_name = 'cmd'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fire // Ice'),
    (select id from sets where short_name = 'cmd'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reins of Power'),
    (select id from sets where short_name = 'cmd'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Furnace Whelp'),
    (select id from sets where short_name = 'cmd'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Howling Mine'),
    (select id from sets where short_name = 'cmd'),
    '251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Solemn Simulacrum'),
    (select id from sets where short_name = 'cmd'),
    '262',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bojuka Bog'),
    (select id from sets where short_name = 'cmd'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rise from the Grave'),
    (select id from sets where short_name = 'cmd'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Izzet Chronarch'),
    (select id from sets where short_name = 'cmd'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azorius Guildmage'),
    (select id from sets where short_name = 'cmd'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whirlpool Whelm'),
    (select id from sets where short_name = 'cmd'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Acidic Slime'),
    (select id from sets where short_name = 'cmd'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Return to Dust'),
    (select id from sets where short_name = 'cmd'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dreamborn Muse'),
    (select id from sets where short_name = 'cmd'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Insurrection'),
    (select id from sets where short_name = 'cmd'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Whelp'),
    (select id from sets where short_name = 'cmd'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Martyr''s Bond'),
    (select id from sets where short_name = 'cmd'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death by Dragons'),
    (select id from sets where short_name = 'cmd'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Veteran Explorer'),
    (select id from sets where short_name = 'cmd'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Malfegor'),
    (select id from sets where short_name = 'cmd'),
    '208',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cmd'),
    '317',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Evangel'),
    (select id from sets where short_name = 'cmd'),
    '223',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Despair'),
    (select id from sets where short_name = 'cmd'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = 'cmd'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hex'),
    (select id from sets where short_name = 'cmd'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scavenging Ooze'),
    (select id from sets where short_name = 'cmd'),
    '170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Molten Slagheap'),
    (select id from sets where short_name = 'cmd'),
    '282',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chartooth Cougar'),
    (select id from sets where short_name = 'cmd'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'cmd'),
    '272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Afterlife'),
    (select id from sets where short_name = 'cmd'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bladewing the Risen'),
    (select id from sets where short_name = 'cmd'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Troll Ascetic'),
    (select id from sets where short_name = 'cmd'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Living Death'),
    (select id from sets where short_name = 'cmd'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Greaves'),
    (select id from sets where short_name = 'cmd'),
    '253',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'cmd'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golgari Rot Farm'),
    (select id from sets where short_name = 'cmd'),
    '275',
    'common'
) ,
(
    (select id from mtgcard where name = 'Austere Command'),
    (select id from sets where short_name = 'cmd'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cmd'),
    '310',
    'common'
) ,
(
    (select id from mtgcard where name = 'Syphon Flesh'),
    (select id from sets where short_name = 'cmd'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Guildmage'),
    (select id from sets where short_name = 'cmd'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Journey to Nowhere'),
    (select id from sets where short_name = 'cmd'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Command Tower'),
    (select id from sets where short_name = 'cmd'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boros Signet'),
    (select id from sets where short_name = 'cmd'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cmd'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hour of Reckoning'),
    (select id from sets where short_name = 'cmd'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Propaganda'),
    (select id from sets where short_name = 'cmd'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vulturous Zombie'),
    (select id from sets where short_name = 'cmd'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Signet'),
    (select id from sets where short_name = 'cmd'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orzhov Signet'),
    (select id from sets where short_name = 'cmd'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rupture Spire'),
    (select id from sets where short_name = 'cmd'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Omens'),
    (select id from sets where short_name = 'cmd'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kaalia of the Vast'),
    (select id from sets where short_name = 'cmd'),
    '206',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Akoum Refuge'),
    (select id from sets where short_name = 'cmd'),
    '264',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gomazoa'),
    (select id from sets where short_name = 'cmd'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vow of Malice'),
    (select id from sets where short_name = 'cmd'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cmd'),
    '302',
    'common'
) ,
(
    (select id from mtgcard where name = 'Master Warcraft'),
    (select id from sets where short_name = 'cmd'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archangel of Strife'),
    (select id from sets where short_name = 'cmd'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreadship Reef'),
    (select id from sets where short_name = 'cmd'),
    '271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Syphon Mind'),
    (select id from sets where short_name = 'cmd'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simic Sky Swallower'),
    (select id from sets where short_name = 'cmd'),
    '226',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stitch Together'),
    (select id from sets where short_name = 'cmd'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cmd'),
    '309',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oni of Wild Places'),
    (select id from sets where short_name = 'cmd'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Carnarium'),
    (select id from sets where short_name = 'cmd'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cmd'),
    '315',
    'common'
) ,
(
    (select id from mtgcard where name = 'Monk Realist'),
    (select id from sets where short_name = 'cmd'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vivid Grove'),
    (select id from sets where short_name = 'cmd'),
    '295',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trade Secrets'),
    (select id from sets where short_name = 'cmd'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivid Meadow'),
    (select id from sets where short_name = 'cmd'),
    '297',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savage Twister'),
    (select id from sets where short_name = 'cmd'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Ricochet'),
    (select id from sets where short_name = 'cmd'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pollen Lullaby'),
    (select id from sets where short_name = 'cmd'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magmatic Force'),
    (select id from sets where short_name = 'cmd'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'cmd'),
    '305',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vow of Lightning'),
    (select id from sets where short_name = 'cmd'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'cmd'),
    '312',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fallen Angel'),
    (select id from sets where short_name = 'cmd'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angelic Arbiter'),
    (select id from sets where short_name = 'cmd'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Valley Rannet'),
    (select id from sets where short_name = 'cmd'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Edric, Spymaster of Trest'),
    (select id from sets where short_name = 'cmd'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deadly Recluse'),
    (select id from sets where short_name = 'cmd'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forgotten Cave'),
    (select id from sets where short_name = 'cmd'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Collective Voyage'),
    (select id from sets where short_name = 'cmd'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magus of the Vineyard'),
    (select id from sets where short_name = 'cmd'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruination'),
    (select id from sets where short_name = 'cmd'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shriekmaw'),
    (select id from sets where short_name = 'cmd'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Baloth Woodcrasher'),
    (select id from sets where short_name = 'cmd'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gwyllion Hedge-Mage'),
    (select id from sets where short_name = 'cmd'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skullbriar, the Walking Grave'),
    (select id from sets where short_name = 'cmd'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Svogthos, the Restless Tomb'),
    (select id from sets where short_name = 'cmd'),
    '289',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cobra Trap'),
    (select id from sets where short_name = 'cmd'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Righteous Cause'),
    (select id from sets where short_name = 'cmd'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plumeveil'),
    (select id from sets where short_name = 'cmd'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brawn'),
    (select id from sets where short_name = 'cmd'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Triskelavus'),
    (select id from sets where short_name = 'cmd'),
    '263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Penumbra Spider'),
    (select id from sets where short_name = 'cmd'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mortivore'),
    (select id from sets where short_name = 'cmd'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Colossal Might'),
    (select id from sets where short_name = 'cmd'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ray of Command'),
    (select id from sets where short_name = 'cmd'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Path to Exile'),
    (select id from sets where short_name = 'cmd'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'cmd'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hornet Queen'),
    (select id from sets where short_name = 'cmd'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Riku of Two Reflections'),
    (select id from sets where short_name = 'cmd'),
    '220',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vivid Marsh'),
    (select id from sets where short_name = 'cmd'),
    '296',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hull Breach'),
    (select id from sets where short_name = 'cmd'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Cadets'),
    (select id from sets where short_name = 'cmd'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orim''s Thunder'),
    (select id from sets where short_name = 'cmd'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Memory Erosion'),
    (select id from sets where short_name = 'cmd'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Windborn Muse'),
    (select id from sets where short_name = 'cmd'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oros, the Avenger'),
    (select id from sets where short_name = 'cmd'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Izzet Boilerworks'),
    (select id from sets where short_name = 'cmd'),
    '278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fleshbag Marauder'),
    (select id from sets where short_name = 'cmd'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyrohemia'),
    (select id from sets where short_name = 'cmd'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Guildmage'),
    (select id from sets where short_name = 'cmd'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fists of Ironwood'),
    (select id from sets where short_name = 'cmd'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruhan of the Fomori'),
    (select id from sets where short_name = 'cmd'),
    '221',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Breath of Darigaaz'),
    (select id from sets where short_name = 'cmd'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reiver Demon'),
    (select id from sets where short_name = 'cmd'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Acorn Catapult'),
    (select id from sets where short_name = 'cmd'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bestial Menace'),
    (select id from sets where short_name = 'cmd'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shared Trauma'),
    (select id from sets where short_name = 'cmd'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vedalken Plotter'),
    (select id from sets where short_name = 'cmd'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gruul Turf'),
    (select id from sets where short_name = 'cmd'),
    '276',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'cmd'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Aberration'),
    (select id from sets where short_name = 'cmd'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oblivion Ring'),
    (select id from sets where short_name = 'cmd'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Razorjaw Oni'),
    (select id from sets where short_name = 'cmd'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sewer Nemesis'),
    (select id from sets where short_name = 'cmd'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guard Gomazoa'),
    (select id from sets where short_name = 'cmd'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Signet'),
    (select id from sets where short_name = 'cmd'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windfall'),
    (select id from sets where short_name = 'cmd'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Call the Skybreaker'),
    (select id from sets where short_name = 'cmd'),
    '188',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Elder'),
    (select id from sets where short_name = 'cmd'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadwood Treefolk'),
    (select id from sets where short_name = 'cmd'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teneb, the Harvester'),
    (select id from sets where short_name = 'cmd'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selesnya Sanctuary'),
    (select id from sets where short_name = 'cmd'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghave, Guru of Spores'),
    (select id from sets where short_name = 'cmd'),
    '200',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dominus of Fealty'),
    (select id from sets where short_name = 'cmd'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jötun Grunt'),
    (select id from sets where short_name = 'cmd'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vision Skeins'),
    (select id from sets where short_name = 'cmd'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riddlekeeper'),
    (select id from sets where short_name = 'cmd'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vorosh, the Hunter'),
    (select id from sets where short_name = 'cmd'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Secluded Steppe'),
    (select id from sets where short_name = 'cmd'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spitebellows'),
    (select id from sets where short_name = 'cmd'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hunting Pack'),
    (select id from sets where short_name = 'cmd'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fact or Fiction'),
    (select id from sets where short_name = 'cmd'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Intet, the Dreamer'),
    (select id from sets where short_name = 'cmd'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azorius Chancery'),
    (select id from sets where short_name = 'cmd'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spurnmage Advocate'),
    (select id from sets where short_name = 'cmd'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'cmd'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Damia, Sage of Stone'),
    (select id from sets where short_name = 'cmd'),
    '191',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Prophetic Bolt'),
    (select id from sets where short_name = 'cmd'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nemesis Trap'),
    (select id from sets where short_name = 'cmd'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu'),
    (select id from sets where short_name = 'cmd'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kazandu Refuge'),
    (select id from sets where short_name = 'cmd'),
    '280',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darksteel Ingot'),
    (select id from sets where short_name = 'cmd'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Nighthawk'),
    (select id from sets where short_name = 'cmd'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arbiter of Knollridge'),
    (select id from sets where short_name = 'cmd'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vish Kal, Blood Arbiter'),
    (select id from sets where short_name = 'cmd'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Symbiotic Wurm'),
    (select id from sets where short_name = 'cmd'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aura Shards'),
    (select id from sets where short_name = 'cmd'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gruul Signet'),
    (select id from sets where short_name = 'cmd'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Guildmage'),
    (select id from sets where short_name = 'cmd'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vengeful Rebirth'),
    (select id from sets where short_name = 'cmd'),
    '233',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lonely Sandbar'),
    (select id from sets where short_name = 'cmd'),
    '281',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anger'),
    (select id from sets where short_name = 'cmd'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nucklavee'),
    (select id from sets where short_name = 'cmd'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crescendo of War'),
    (select id from sets where short_name = 'cmd'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Butcher of Malakir'),
    (select id from sets where short_name = 'cmd'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'cmd'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Comet Storm'),
    (select id from sets where short_name = 'cmd'),
    '117',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aquastrand Spider'),
    (select id from sets where short_name = 'cmd'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Snare'),
    (select id from sets where short_name = 'cmd'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vivid Creek'),
    (select id from sets where short_name = 'cmd'),
    '294',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Repulse'),
    (select id from sets where short_name = 'cmd'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Patron of the Nezumi'),
    (select id from sets where short_name = 'cmd'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Punishing Fire'),
    (select id from sets where short_name = 'cmd'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barren Moor'),
    (select id from sets where short_name = 'cmd'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Awakening Zone'),
    (select id from sets where short_name = 'cmd'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Minds Aglow'),
    (select id from sets where short_name = 'cmd'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghostly Prison'),
    (select id from sets where short_name = 'cmd'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Homeward Path'),
    (select id from sets where short_name = 'cmd'),
    '277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trench Gorger'),
    (select id from sets where short_name = 'cmd'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faultgrinder'),
    (select id from sets where short_name = 'cmd'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrexial, the Risen Deep'),
    (select id from sets where short_name = 'cmd'),
    '239',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jwar Isle Refuge'),
    (select id from sets where short_name = 'cmd'),
    '279',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sigil Captain'),
    (select id from sets where short_name = 'cmd'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prison Term'),
    (select id from sets where short_name = 'cmd'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Elder'),
    (select id from sets where short_name = 'cmd'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skullclamp'),
    (select id from sets where short_name = 'cmd'),
    '260',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dreamstone Hedron'),
    (select id from sets where short_name = 'cmd'),
    '247',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Signet'),
    (select id from sets where short_name = 'cmd'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dimir Aqueduct'),
    (select id from sets where short_name = 'cmd'),
    '270',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vow of Flight'),
    (select id from sets where short_name = 'cmd'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rapacious One'),
    (select id from sets where short_name = 'cmd'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'cmd'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Szadek, Lord of Secrets'),
    (select id from sets where short_name = 'cmd'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scythe Specter'),
    (select id from sets where short_name = 'cmd'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tariel, Reckoner of Souls'),
    (select id from sets where short_name = 'cmd'),
    '229',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Voice of All'),
    (select id from sets where short_name = 'cmd'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'cmd'),
    '316',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kodama''s Reach'),
    (select id from sets where short_name = 'cmd'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Necrogenesis'),
    (select id from sets where short_name = 'cmd'),
    '212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mortify'),
    (select id from sets where short_name = 'cmd'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vow of Wildness'),
    (select id from sets where short_name = 'cmd'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unnerve'),
    (select id from sets where short_name = 'cmd'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hydra Omnivore'),
    (select id from sets where short_name = 'cmd'),
    '161',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karador, Ghost Chieftain'),
    (select id from sets where short_name = 'cmd'),
    '207',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Animar, Soul of Elements'),
    (select id from sets where short_name = 'cmd'),
    '181',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Numot, the Devastator'),
    (select id from sets where short_name = 'cmd'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desecrator Hag'),
    (select id from sets where short_name = 'cmd'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sign in Blood'),
    (select id from sets where short_name = 'cmd'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prophetic Prism'),
    (select id from sets where short_name = 'cmd'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Hatchling'),
    (select id from sets where short_name = 'cmd'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fertilid'),
    (select id from sets where short_name = 'cmd'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrecking Ball'),
    (select id from sets where short_name = 'cmd'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lash Out'),
    (select id from sets where short_name = 'cmd'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oblation'),
    (select id from sets where short_name = 'cmd'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oblivion Stone'),
    (select id from sets where short_name = 'cmd'),
    '254',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wonder'),
    (select id from sets where short_name = 'cmd'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mulldrifter'),
    (select id from sets where short_name = 'cmd'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'cmd'),
    '303',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murmurs from Beyond'),
    (select id from sets where short_name = 'cmd'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orzhov Basilica'),
    (select id from sets where short_name = 'cmd'),
    '283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Champion''s Helm'),
    (select id from sets where short_name = 'cmd'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shattered Angel'),
    (select id from sets where short_name = 'cmd'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Extractor Demon'),
    (select id from sets where short_name = 'cmd'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vow of Duty'),
    (select id from sets where short_name = 'cmd'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aethersnipe'),
    (select id from sets where short_name = 'cmd'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Basandra, Battle Seraph'),
    (select id from sets where short_name = 'cmd'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Guildmage'),
    (select id from sets where short_name = 'cmd'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Electrolyze'),
    (select id from sets where short_name = 'cmd'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Artisan of Kozilek'),
    (select id from sets where short_name = 'cmd'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'cmd'),
    '314',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple of the False God'),
    (select id from sets where short_name = 'cmd'),
    '290',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nin, the Pain Artist'),
    (select id from sets where short_name = 'cmd'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Court Hussar'),
    (select id from sets where short_name = 'cmd'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Denial'),
    (select id from sets where short_name = 'cmd'),
    '237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zoetic Cavern'),
    (select id from sets where short_name = 'cmd'),
    '298',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Akroma, Angel of Fury'),
    (select id from sets where short_name = 'cmd'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cleansing Beam'),
    (select id from sets where short_name = 'cmd'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'cmd'),
    '307',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tribute to the Wild'),
    (select id from sets where short_name = 'cmd'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disaster Radius'),
    (select id from sets where short_name = 'cmd'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relic Crush'),
    (select id from sets where short_name = 'cmd'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dread Cacodemon'),
    (select id from sets where short_name = 'cmd'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chain Reaction'),
    (select id from sets where short_name = 'cmd'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Alliance of Arms'),
    (select id from sets where short_name = 'cmd'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Conundrum Sphinx'),
    (select id from sets where short_name = 'cmd'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avatar of Slaughter'),
    (select id from sets where short_name = 'cmd'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Tusker'),
    (select id from sets where short_name = 'cmd'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'False Prophet'),
    (select id from sets where short_name = 'cmd'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fungal Reaches'),
    (select id from sets where short_name = 'cmd'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Storm Herd'),
    (select id from sets where short_name = 'cmd'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'cmd'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simic Growth Chamber'),
    (select id from sets where short_name = 'cmd'),
    '288',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chorus of the Conclave'),
    (select id from sets where short_name = 'cmd'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Explosive Vegetation'),
    (select id from sets where short_name = 'cmd'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cultivate'),
    (select id from sets where short_name = 'cmd'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zedruu the Greathearted'),
    (select id from sets where short_name = 'cmd'),
    '240',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Doom Blade'),
    (select id from sets where short_name = 'cmd'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Attrition'),
    (select id from sets where short_name = 'cmd'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fellwar Stone'),
    (select id from sets where short_name = 'cmd'),
    '248',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avatar of Fury'),
    (select id from sets where short_name = 'cmd'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armillary Sphere'),
    (select id from sets where short_name = 'cmd'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terminate'),
    (select id from sets where short_name = 'cmd'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boros Garrison'),
    (select id from sets where short_name = 'cmd'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Congregate'),
    (select id from sets where short_name = 'cmd'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nezumi Graverobber // Nighteyes the Desecrator'),
    (select id from sets where short_name = 'cmd'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harmonize'),
    (select id from sets where short_name = 'cmd'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Izzet Signet'),
    (select id from sets where short_name = 'cmd'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'cmd'),
    '311',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'cmd'),
    '306',
    'common'
) 
 on conflict do nothing;
