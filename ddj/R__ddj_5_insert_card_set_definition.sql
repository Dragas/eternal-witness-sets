insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Golgari Grave-Troll'),
    (select id from sets where short_name = 'ddj'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reassembling Skeleton'),
    (select id from sets where short_name = 'ddj'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddj'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gleancrawler'),
    (select id from sets where short_name = 'ddj'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Call to Heel'),
    (select id from sets where short_name = 'ddj'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddj'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Guildmage'),
    (select id from sets where short_name = 'ddj'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddj'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prophetic Bolt'),
    (select id from sets where short_name = 'ddj'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ogre Savant'),
    (select id from sets where short_name = 'ddj'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stinkweed Imp'),
    (select id from sets where short_name = 'ddj'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nightmare Void'),
    (select id from sets where short_name = 'ddj'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shambling Shell'),
    (select id from sets where short_name = 'ddj'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eternal Witness'),
    (select id from sets where short_name = 'ddj'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Galvanoth'),
    (select id from sets where short_name = 'ddj'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Life // Death'),
    (select id from sets where short_name = 'ddj'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddj'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Putrefy'),
    (select id from sets where short_name = 'ddj'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Germination'),
    (select id from sets where short_name = 'ddj'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vacuumelt'),
    (select id from sets where short_name = 'ddj'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stingerfling Spider'),
    (select id from sets where short_name = 'ddj'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddj'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dakmor Salvage'),
    (select id from sets where short_name = 'ddj'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tranquil Thicket'),
    (select id from sets where short_name = 'ddj'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barren Moor'),
    (select id from sets where short_name = 'ddj'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Boilerworks'),
    (select id from sets where short_name = 'ddj'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vigor Mortis'),
    (select id from sets where short_name = 'ddj'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elves of Deep Shadow'),
    (select id from sets where short_name = 'ddj'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Charm'),
    (select id from sets where short_name = 'ddj'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddj'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plagued Rusalka'),
    (select id from sets where short_name = 'ddj'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Electromancer'),
    (select id from sets where short_name = 'ddj'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reminisce'),
    (select id from sets where short_name = 'ddj'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Djinn Illuminatus'),
    (select id from sets where short_name = 'ddj'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wee Dragonauts'),
    (select id from sets where short_name = 'ddj'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddj'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golgari Rotwurm'),
    (select id from sets where short_name = 'ddj'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doomgape'),
    (select id from sets where short_name = 'ddj'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Rot Farm'),
    (select id from sets where short_name = 'ddj'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddj'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyromatics'),
    (select id from sets where short_name = 'ddj'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'ddj'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddj'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yoke of the Damned'),
    (select id from sets where short_name = 'ddj'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boneyard Wurm'),
    (select id from sets where short_name = 'ddj'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Greater Mossdog'),
    (select id from sets where short_name = 'ddj'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feast or Famine'),
    (select id from sets where short_name = 'ddj'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overwhelming Intellect'),
    (select id from sets where short_name = 'ddj'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Force Spike'),
    (select id from sets where short_name = 'ddj'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golgari Thug'),
    (select id from sets where short_name = 'ddj'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddj'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kiln Fiend'),
    (select id from sets where short_name = 'ddj'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreg Mangler'),
    (select id from sets where short_name = 'ddj'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nivix, Aerie of the Firemind'),
    (select id from sets where short_name = 'ddj'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Signet'),
    (select id from sets where short_name = 'ddj'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghoul''s Feast'),
    (select id from sets where short_name = 'ddj'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gelectrode'),
    (select id from sets where short_name = 'ddj'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Steamcore Weird'),
    (select id from sets where short_name = 'ddj'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lonely Sandbar'),
    (select id from sets where short_name = 'ddj'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Twilight''s Call'),
    (select id from sets where short_name = 'ddj'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Train of Thought'),
    (select id from sets where short_name = 'ddj'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quicksilver Dagger'),
    (select id from sets where short_name = 'ddj'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grim Flowering'),
    (select id from sets where short_name = 'ddj'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fire // Ice'),
    (select id from sets where short_name = 'ddj'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Svogthos, the Restless Tomb'),
    (select id from sets where short_name = 'ddj'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddj'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Chronarch'),
    (select id from sets where short_name = 'ddj'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Signet'),
    (select id from sets where short_name = 'ddj'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunderheads'),
    (select id from sets where short_name = 'ddj'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brain Weevil'),
    (select id from sets where short_name = 'ddj'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Niv-Mizzet, the Firemind'),
    (select id from sets where short_name = 'ddj'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ddj'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddj'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Isochron Scepter'),
    (select id from sets where short_name = 'ddj'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Putrid Leech'),
    (select id from sets where short_name = 'ddj'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forgotten Cave'),
    (select id from sets where short_name = 'ddj'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ddj'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dissipate'),
    (select id from sets where short_name = 'ddj'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Korozda Guildmage'),
    (select id from sets where short_name = 'ddj'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddj'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravenous Rats'),
    (select id from sets where short_name = 'ddj'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx-Bone Wand'),
    (select id from sets where short_name = 'ddj'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Invoke the Firemind'),
    (select id from sets where short_name = 'ddj'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sadistic Hypnotist'),
    (select id from sets where short_name = 'ddj'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Life from the Loam'),
    (select id from sets where short_name = 'ddj'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magma Spray'),
    (select id from sets where short_name = 'ddj'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shrewd Hatchling'),
    (select id from sets where short_name = 'ddj'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddj'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Street Spasm'),
    (select id from sets where short_name = 'ddj'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jarad, Golgari Lich Lord'),
    (select id from sets where short_name = 'ddj'),
    '45',
    'mythic'
) 
 on conflict do nothing;
