insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Forest')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Swamp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Giant Scorpion'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Giant Scorpion'),
        (select types.id from types where name = 'Scorpion')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pestilence Demon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pestilence Demon'),
        (select types.id from types where name = 'Demon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie Giant'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie Giant'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie Giant'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie Giant'),
        (select types.id from types where name = 'Giant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Disfigure'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Briarhorn'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Briarhorn'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Altar''s Reap'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crop Rotation'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Doom Blade'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Quest for the Gravelord'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Saddleback Lagac'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Saddleback Lagac'),
        (select types.id from types where name = 'Lizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Priest of the Blood Rite'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Priest of the Blood Rite'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Priest of the Blood Rite'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ob Nixilis Reignited Emblem'),
        (select types.id from types where name = 'Emblem')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ob Nixilis Reignited Emblem'),
        (select types.id from types where name = 'Nixilis')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Natural Connection'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jaddi Lifestrider'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jaddi Lifestrider'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Swamp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Foul Imp'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Foul Imp'),
        (select types.id from types where name = 'Imp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tendrils of Corruption'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shadows of the Past'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Oran-Rief Hydra'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Oran-Rief Hydra'),
        (select types.id from types where name = 'Hydra')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Swamp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Forest')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Blistergrub'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Blistergrub'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Demon'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Demon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Demon'),
        (select types.id from types where name = 'Demon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Oran-Rief Invoker'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Oran-Rief Invoker'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Oran-Rief Invoker'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cadaver Imp'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cadaver Imp'),
        (select types.id from types where name = 'Imp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Civic Wayfinder'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Civic Wayfinder'),
        (select types.id from types where name = 'Elf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Civic Wayfinder'),
        (select types.id from types where name = 'Druid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Civic Wayfinder'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Seek the Horizon'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Innocent Blood'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Forest')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Squelching Leeches'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Squelching Leeches'),
        (select types.id from types where name = 'Leech')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hideous End'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vines of the Recluse'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ob Nixilis Reignited'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ob Nixilis Reignited'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ob Nixilis Reignited'),
        (select types.id from types where name = 'Nixilis')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nissa, Voice of Zendikar'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nissa, Voice of Zendikar'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nissa, Voice of Zendikar'),
        (select types.id from types where name = 'Nissa')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Smallpox'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Oakgnarl Warrior'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Oakgnarl Warrior'),
        (select types.id from types where name = 'Treefolk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Oakgnarl Warrior'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Swamp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Demon''s Grasp'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nissa''s Chosen'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nissa''s Chosen'),
        (select types.id from types where name = 'Elf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nissa''s Chosen'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fetid Imp'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fetid Imp'),
        (select types.id from types where name = 'Imp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Citanul Woodreaders'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Citanul Woodreaders'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Citanul Woodreaders'),
        (select types.id from types where name = 'Druid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Abundance'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Unhallowed Pact'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Unhallowed Pact'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Despoiler of Souls'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Despoiler of Souls'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Indulgent Tormentor'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Indulgent Tormentor'),
        (select types.id from types where name = 'Demon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eldrazi Scion'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eldrazi Scion'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eldrazi Scion'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eldrazi Scion'),
        (select types.id from types where name = 'Scion')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mire''s Toll'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thornweald Archer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thornweald Archer'),
        (select types.id from types where name = 'Elf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thornweald Archer'),
        (select types.id from types where name = 'Archer')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gaea''s Blessing'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Treetop Village'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gilt-Leaf Seer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gilt-Leaf Seer'),
        (select types.id from types where name = 'Elf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gilt-Leaf Seer'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Walker of the Grove'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Walker of the Grove'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ambition''s Cost'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Renegade Demon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Renegade Demon'),
        (select types.id from types where name = 'Demon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elvish Visionary'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elvish Visionary'),
        (select types.id from types where name = 'Elf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elvish Visionary'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fertile Thicket'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thicket Elemental'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thicket Elemental'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Carrier Thrall'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Carrier Thrall'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Khalni Garden'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Desecration Demon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Desecration Demon'),
        (select types.id from types where name = 'Demon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Scythe Leopard'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Scythe Leopard'),
        (select types.id from types where name = 'Cat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fertilid'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fertilid'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Leechridden Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Leechridden Swamp'),
        (select types.id from types where name = 'Swamp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Forest')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wood Elves'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wood Elves'),
        (select types.id from types where name = 'Elf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wood Elves'),
        (select types.id from types where name = 'Scout')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Forest')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Woodborn Behemoth'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Woodborn Behemoth'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elemental'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elemental'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elemental'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cloudthresher'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cloudthresher'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bala Ged Scorpion'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bala Ged Scorpion'),
        (select types.id from types where name = 'Scorpion')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Swamp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mosswort Bridge'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grim Discovery'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plant'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plant'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plant'),
        (select types.id from types where name = 'Plant')
    ) 
 on conflict do nothing;
