insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddr'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddr'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Scorpion'),
    (select id from sets where short_name = 'ddr'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pestilence Demon'),
    (select id from sets where short_name = 'ddr'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zombie Giant'),
    (select id from sets where short_name = 'ddr'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disfigure'),
    (select id from sets where short_name = 'ddr'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Briarhorn'),
    (select id from sets where short_name = 'ddr'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Altar''s Reap'),
    (select id from sets where short_name = 'ddr'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crop Rotation'),
    (select id from sets where short_name = 'ddr'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doom Blade'),
    (select id from sets where short_name = 'ddr'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quest for the Gravelord'),
    (select id from sets where short_name = 'ddr'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Saddleback Lagac'),
    (select id from sets where short_name = 'ddr'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Priest of the Blood Rite'),
    (select id from sets where short_name = 'ddr'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis Reignited Emblem'),
    (select id from sets where short_name = 'ddr'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Natural Connection'),
    (select id from sets where short_name = 'ddr'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jaddi Lifestrider'),
    (select id from sets where short_name = 'ddr'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddr'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foul Imp'),
    (select id from sets where short_name = 'ddr'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tendrils of Corruption'),
    (select id from sets where short_name = 'ddr'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shadows of the Past'),
    (select id from sets where short_name = 'ddr'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief Hydra'),
    (select id from sets where short_name = 'ddr'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddr'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddr'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blistergrub'),
    (select id from sets where short_name = 'ddr'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon'),
    (select id from sets where short_name = 'ddr'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief Invoker'),
    (select id from sets where short_name = 'ddr'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cadaver Imp'),
    (select id from sets where short_name = 'ddr'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Civic Wayfinder'),
    (select id from sets where short_name = 'ddr'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seek the Horizon'),
    (select id from sets where short_name = 'ddr'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Innocent Blood'),
    (select id from sets where short_name = 'ddr'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddr'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squelching Leeches'),
    (select id from sets where short_name = 'ddr'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hideous End'),
    (select id from sets where short_name = 'ddr'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vines of the Recluse'),
    (select id from sets where short_name = 'ddr'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis Reignited'),
    (select id from sets where short_name = 'ddr'),
    '36',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nissa, Voice of Zendikar'),
    (select id from sets where short_name = 'ddr'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Smallpox'),
    (select id from sets where short_name = 'ddr'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oakgnarl Warrior'),
    (select id from sets where short_name = 'ddr'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddr'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demon''s Grasp'),
    (select id from sets where short_name = 'ddr'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nissa''s Chosen'),
    (select id from sets where short_name = 'ddr'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fetid Imp'),
    (select id from sets where short_name = 'ddr'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Citanul Woodreaders'),
    (select id from sets where short_name = 'ddr'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abundance'),
    (select id from sets where short_name = 'ddr'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unhallowed Pact'),
    (select id from sets where short_name = 'ddr'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Despoiler of Souls'),
    (select id from sets where short_name = 'ddr'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Indulgent Tormentor'),
    (select id from sets where short_name = 'ddr'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Scion'),
    (select id from sets where short_name = 'ddr'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mire''s Toll'),
    (select id from sets where short_name = 'ddr'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thornweald Archer'),
    (select id from sets where short_name = 'ddr'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Blessing'),
    (select id from sets where short_name = 'ddr'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treetop Village'),
    (select id from sets where short_name = 'ddr'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gilt-Leaf Seer'),
    (select id from sets where short_name = 'ddr'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Walker of the Grove'),
    (select id from sets where short_name = 'ddr'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ambition''s Cost'),
    (select id from sets where short_name = 'ddr'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Renegade Demon'),
    (select id from sets where short_name = 'ddr'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Visionary'),
    (select id from sets where short_name = 'ddr'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fertile Thicket'),
    (select id from sets where short_name = 'ddr'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thicket Elemental'),
    (select id from sets where short_name = 'ddr'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carrier Thrall'),
    (select id from sets where short_name = 'ddr'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Khalni Garden'),
    (select id from sets where short_name = 'ddr'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desecration Demon'),
    (select id from sets where short_name = 'ddr'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scythe Leopard'),
    (select id from sets where short_name = 'ddr'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fertilid'),
    (select id from sets where short_name = 'ddr'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leechridden Swamp'),
    (select id from sets where short_name = 'ddr'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddr'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wood Elves'),
    (select id from sets where short_name = 'ddr'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ddr'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Woodborn Behemoth'),
    (select id from sets where short_name = 'ddr'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'ddr'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudthresher'),
    (select id from sets where short_name = 'ddr'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bala Ged Scorpion'),
    (select id from sets where short_name = 'ddr'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ddr'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mosswort Bridge'),
    (select id from sets where short_name = 'ddr'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grim Discovery'),
    (select id from sets where short_name = 'ddr'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plant'),
    (select id from sets where short_name = 'ddr'),
    '75',
    'common'
) 
 on conflict do nothing;
