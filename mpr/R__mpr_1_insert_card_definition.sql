    insert into mtgcard(name) values ('Bear') on conflict do nothing;
    insert into mtgcard(name) values ('Spirit') on conflict do nothing;
    insert into mtgcard(name) values ('Wasteland') on conflict do nothing;
    insert into mtgcard(name) values ('Bird') on conflict do nothing;
    insert into mtgcard(name) values ('Elephant') on conflict do nothing;
    insert into mtgcard(name) values ('Saproling') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Soldier') on conflict do nothing;
    insert into mtgcard(name) values ('Beast') on conflict do nothing;
