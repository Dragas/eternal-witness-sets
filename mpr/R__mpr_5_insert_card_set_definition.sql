insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Bear'),
    (select id from sets where short_name = 'mpr'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'mpr'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wasteland'),
    (select id from sets where short_name = 'mpr'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bird'),
    (select id from sets where short_name = 'mpr'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elephant'),
    (select id from sets where short_name = 'mpr'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'mpr'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Soldier'),
    (select id from sets where short_name = 'mpr'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'mpr'),
    '8',
    'common'
) 
 on conflict do nothing;
