insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Spurnmage Advocate'),
    (select id from sets where short_name = 'jud'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcane Teachings'),
    (select id from sets where short_name = 'jud'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spellgorger Barbarian'),
    (select id from sets where short_name = 'jud'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Surge'),
    (select id from sets where short_name = 'jud'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dwarven Scorcher'),
    (select id from sets where short_name = 'jud'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cabal Therapy'),
    (select id from sets where short_name = 'jud'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serene Sunset'),
    (select id from sets where short_name = 'jud'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Masked Gorgon'),
    (select id from sets where short_name = 'jud'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Epic Struggle'),
    (select id from sets where short_name = 'jud'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pulsemage Advocate'),
    (select id from sets where short_name = 'jud'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Web of Inertia'),
    (select id from sets where short_name = 'jud'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chastise'),
    (select id from sets where short_name = 'jud'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silver Seraph'),
    (select id from sets where short_name = 'jud'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nantuko Tracer'),
    (select id from sets where short_name = 'jud'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forcemage Advocate'),
    (select id from sets where short_name = 'jud'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nullmage Advocate'),
    (select id from sets where short_name = 'jud'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wormfang Behemoth'),
    (select id from sets where short_name = 'jud'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burning Wish'),
    (select id from sets where short_name = 'jud'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jeska, Warrior Adept'),
    (select id from sets where short_name = 'jud'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mental Note'),
    (select id from sets where short_name = 'jud'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Warcraft'),
    (select id from sets where short_name = 'jud'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lead Astray'),
    (select id from sets where short_name = 'jud'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Benevolent Bodyguard'),
    (select id from sets where short_name = 'jud'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hapless Researcher'),
    (select id from sets where short_name = 'jud'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Living Wish'),
    (select id from sets where short_name = 'jud'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scalpelexis'),
    (select id from sets where short_name = 'jud'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cephalid Inkshrouder'),
    (select id from sets where short_name = 'jud'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shieldmage Advocate'),
    (select id from sets where short_name = 'jud'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fledgling Dragon'),
    (select id from sets where short_name = 'jud'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phantom Tiger'),
    (select id from sets where short_name = 'jud'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crush of Wurms'),
    (select id from sets where short_name = 'jud'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anurid Barkripper'),
    (select id from sets where short_name = 'jud'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prismatic Strands'),
    (select id from sets where short_name = 'jud'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Commander Eesha'),
    (select id from sets where short_name = 'jud'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anurid Swarmsnapper'),
    (select id from sets where short_name = 'jud'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Filth'),
    (select id from sets where short_name = 'jud'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spelljack'),
    (select id from sets where short_name = 'jud'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Centaur Rootcaster'),
    (select id from sets where short_name = 'jud'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grip of Amnesia'),
    (select id from sets where short_name = 'jud'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wonder'),
    (select id from sets where short_name = 'jud'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Defy Gravity'),
    (select id from sets where short_name = 'jud'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wormfang Turtle'),
    (select id from sets where short_name = 'jud'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swirling Sandstorm'),
    (select id from sets where short_name = 'jud'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Funeral Pyre'),
    (select id from sets where short_name = 'jud'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seedtime'),
    (select id from sets where short_name = 'jud'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Wayfarer'),
    (select id from sets where short_name = 'jud'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Nishoba'),
    (select id from sets where short_name = 'jud'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Valor'),
    (select id from sets where short_name = 'jud'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hunting Grounds'),
    (select id from sets where short_name = 'jud'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Reclamation'),
    (select id from sets where short_name = 'jud'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Erhnam Djinn'),
    (select id from sets where short_name = 'jud'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Earsplitting Rats'),
    (select id from sets where short_name = 'jud'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Planar Chaos'),
    (select id from sets where short_name = 'jud'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Suntail Hawk'),
    (select id from sets where short_name = 'jud'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Flock'),
    (select id from sets where short_name = 'jud'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantom Nantuko'),
    (select id from sets where short_name = 'jud'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dwarven Driller'),
    (select id from sets where short_name = 'jud'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ember Shot'),
    (select id from sets where short_name = 'jud'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Nomad'),
    (select id from sets where short_name = 'jud'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ray of Revelation'),
    (select id from sets where short_name = 'jud'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cephalid Constable'),
    (select id from sets where short_name = 'jud'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keep Watch'),
    (select id from sets where short_name = 'jud'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quiet Speculation'),
    (select id from sets where short_name = 'jud'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trained Pronghorn'),
    (select id from sets where short_name = 'jud'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cagemail'),
    (select id from sets where short_name = 'jud'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Browbeat'),
    (select id from sets where short_name = 'jud'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Folk Medicine'),
    (select id from sets where short_name = 'jud'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Breaking Point'),
    (select id from sets where short_name = 'jud'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mist of Stagnation'),
    (select id from sets where short_name = 'jud'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glory'),
    (select id from sets where short_name = 'jud'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death Wish'),
    (select id from sets where short_name = 'jud'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elephant Guide'),
    (select id from sets where short_name = 'jud'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Infectious Rage'),
    (select id from sets where short_name = 'jud'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lava Dart'),
    (select id from sets where short_name = 'jud'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soulgorger Orgg'),
    (select id from sets where short_name = 'jud'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Toxic Stench'),
    (select id from sets where short_name = 'jud'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swelter'),
    (select id from sets where short_name = 'jud'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grizzly Fate'),
    (select id from sets where short_name = 'jud'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Riftstone Portal'),
    (select id from sets where short_name = 'jud'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harvester Druid'),
    (select id from sets where short_name = 'jud'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goretusk Firebeast'),
    (select id from sets where short_name = 'jud'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Worldgorger Dragon'),
    (select id from sets where short_name = 'jud'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nomad Mythmaker'),
    (select id from sets where short_name = 'jud'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dwarven Bloodboiler'),
    (select id from sets where short_name = 'jud'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grave Consequences'),
    (select id from sets where short_name = 'jud'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treacherous Vampire'),
    (select id from sets where short_name = 'jud'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unquestioned Authority'),
    (select id from sets where short_name = 'jud'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Border Patrol'),
    (select id from sets where short_name = 'jud'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thriss, Nantuko Primus'),
    (select id from sets where short_name = 'jud'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anurid Brushhopper'),
    (select id from sets where short_name = 'jud'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rats'' Feast'),
    (select id from sets where short_name = 'jud'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soulcatchers'' Aerie'),
    (select id from sets where short_name = 'jud'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Genesis'),
    (select id from sets where short_name = 'jud'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stitch Together'),
    (select id from sets where short_name = 'jud'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Morality Shift'),
    (select id from sets where short_name = 'jud'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirari''s Wake'),
    (select id from sets where short_name = 'jud'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wormfang Drake'),
    (select id from sets where short_name = 'jud'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krosan Verge'),
    (select id from sets where short_name = 'jud'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wormfang Manta'),
    (select id from sets where short_name = 'jud'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ironshell Beetle'),
    (select id from sets where short_name = 'jud'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Book Burning'),
    (select id from sets where short_name = 'jud'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ancestor''s Chosen'),
    (select id from sets where short_name = 'jud'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aven Fogbringer'),
    (select id from sets where short_name = 'jud'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vigilant Sentry'),
    (select id from sets where short_name = 'jud'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battle Screech'),
    (select id from sets where short_name = 'jud'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Warthog'),
    (select id from sets where short_name = 'jud'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Centaur'),
    (select id from sets where short_name = 'jud'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Canopy Claws'),
    (select id from sets where short_name = 'jud'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tunneler Wurm'),
    (select id from sets where short_name = 'jud'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treacherous Werewolf'),
    (select id from sets where short_name = 'jud'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selfless Exorcist'),
    (select id from sets where short_name = 'jud'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cunning Wish'),
    (select id from sets where short_name = 'jud'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Battlewise Aven'),
    (select id from sets where short_name = 'jud'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nantuko Monastery'),
    (select id from sets where short_name = 'jud'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Test of Endurance'),
    (select id from sets where short_name = 'jud'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirror Wall'),
    (select id from sets where short_name = 'jud'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guided Strike'),
    (select id from sets where short_name = 'jud'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cabal Trainee'),
    (select id from sets where short_name = 'jud'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Firecat Blitz'),
    (select id from sets where short_name = 'jud'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Telekinetic Bonds'),
    (select id from sets where short_name = 'jud'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spirit Cairn'),
    (select id from sets where short_name = 'jud'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Venomous Vines'),
    (select id from sets where short_name = 'jud'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golden Wish'),
    (select id from sets where short_name = 'jud'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wormfang Crab'),
    (select id from sets where short_name = 'jud'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wormfang Newt'),
    (select id from sets where short_name = 'jud'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shaman''s Trance'),
    (select id from sets where short_name = 'jud'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exoskeletal Armor'),
    (select id from sets where short_name = 'jud'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liberated Dwarf'),
    (select id from sets where short_name = 'jud'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guiltfeeder'),
    (select id from sets where short_name = 'jud'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Laquatus''s Disdain'),
    (select id from sets where short_name = 'jud'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Barbarian Bully'),
    (select id from sets where short_name = 'jud'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sudden Strength'),
    (select id from sets where short_name = 'jud'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Solitary Confinement'),
    (select id from sets where short_name = 'jud'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Envelop'),
    (select id from sets where short_name = 'jud'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brawn'),
    (select id from sets where short_name = 'jud'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balthor the Defiled'),
    (select id from sets where short_name = 'jud'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flaring Pain'),
    (select id from sets where short_name = 'jud'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sylvan Safekeeper'),
    (select id from sets where short_name = 'jud'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Battlefield Scrounger'),
    (select id from sets where short_name = 'jud'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lost in Thought'),
    (select id from sets where short_name = 'jud'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sutured Ghoul'),
    (select id from sets where short_name = 'jud'),
    '73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anger'),
    (select id from sets where short_name = 'jud'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flash of Insight'),
    (select id from sets where short_name = 'jud'),
    '40',
    'uncommon'
) 
 on conflict do nothing;
