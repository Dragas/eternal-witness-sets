insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Brain Maggot'),
    (select id from sets where short_name = 'f14'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tormented Hero'),
    (select id from sets where short_name = 'f14'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bile Blight'),
    (select id from sets where short_name = 'f14'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fanatic of Xenagos'),
    (select id from sets where short_name = 'f14'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Banishing Light'),
    (select id from sets where short_name = 'f14'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Mystic'),
    (select id from sets where short_name = 'f14'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Banisher Priest'),
    (select id from sets where short_name = 'f14'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magma Spray'),
    (select id from sets where short_name = 'f14'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Encroaching Wastes'),
    (select id from sets where short_name = 'f14'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dissolve'),
    (select id from sets where short_name = 'f14'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stoke the Flames'),
    (select id from sets where short_name = 'f14'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warleader''s Helix'),
    (select id from sets where short_name = 'f14'),
    '1',
    'rare'
) 
 on conflict do nothing;
