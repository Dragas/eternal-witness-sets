insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Soul Separator'),
    (select id from sets where short_name = 'pemn'),
    '199s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ulvenwald Observer'),
    (select id from sets where short_name = 'pemn'),
    '176s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gisa and Geralf'),
    (select id from sets where short_name = 'pemn'),
    '183s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bruna, the Fading Light'),
    (select id from sets where short_name = 'pemn'),
    '15as',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elder Deep-Fiend'),
    (select id from sets where short_name = 'pemn'),
    '5s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eternal Scourge'),
    (select id from sets where short_name = 'pemn'),
    '7s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Impetuous Devils'),
    (select id from sets where short_name = 'pemn'),
    '132s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eldritch Evolution'),
    (select id from sets where short_name = 'pemn'),
    '155s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tamiyo, Field Researcher'),
    (select id from sets where short_name = 'pemn'),
    '190s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sigarda''s Aid'),
    (select id from sets where short_name = 'pemn'),
    '41s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tree of Perdition'),
    (select id from sets where short_name = 'pemn'),
    '109s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stromkirk Occultist'),
    (select id from sets where short_name = 'pemn'),
    '146s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ulrich of the Krallenhorde // Ulrich, Uncontested Alpha'),
    (select id from sets where short_name = 'pemn'),
    '191s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stromkirk Condemned'),
    (select id from sets where short_name = 'pemn'),
    '106s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selfless Spirit'),
    (select id from sets where short_name = 'pemn'),
    '40s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brisela, Voice of Nightmares'),
    (select id from sets where short_name = 'pemn'),
    '15bs',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Thalia, Heretic Cathar'),
    (select id from sets where short_name = 'pemn'),
    '46s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harmless Offering'),
    (select id from sets where short_name = 'pemn'),
    '131s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stitcher''s Graft'),
    (select id from sets where short_name = 'pemn'),
    '200s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Summary Dismissal'),
    (select id from sets where short_name = 'pemn'),
    '75s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thalia, Heretic Cathar'),
    (select id from sets where short_name = 'pemn'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Collective Defiance'),
    (select id from sets where short_name = 'pemn'),
    '123s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zombie // Zombie'),
    (select id from sets where short_name = 'pemn'),
    '1Z',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deploy the Gatewatch'),
    (select id from sets where short_name = 'pemn'),
    '20s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mausoleum Wanderer'),
    (select id from sets where short_name = 'pemn'),
    '69s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emrakul''s Evangel'),
    (select id from sets where short_name = 'pemn'),
    '156s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imprisoned in the Moon'),
    (select id from sets where short_name = 'pemn'),
    '65s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Providence'),
    (select id from sets where short_name = 'pemn'),
    '37s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Decimator of the Provinces'),
    (select id from sets where short_name = 'pemn'),
    '2s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Assembled Alphas'),
    (select id from sets where short_name = 'pemn'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cryptbreaker'),
    (select id from sets where short_name = 'pemn'),
    '86s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hanweir Garrison'),
    (select id from sets where short_name = 'pemn'),
    '130as',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coax from the Blind Eternities'),
    (select id from sets where short_name = 'pemn'),
    '51s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Permeating Mass'),
    (select id from sets where short_name = 'pemn'),
    '165s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sanctifier of Souls'),
    (select id from sets where short_name = 'pemn'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spell Queller'),
    (select id from sets where short_name = 'pemn'),
    '189s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Splendid Reclamation'),
    (select id from sets where short_name = 'pemn'),
    '171s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lupine Prototype'),
    (select id from sets where short_name = 'pemn'),
    '197s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bedlam Reveler'),
    (select id from sets where short_name = 'pemn'),
    '118s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ishkanah, Grafwidow'),
    (select id from sets where short_name = 'pemn'),
    '162s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Noosegraf Mob'),
    (select id from sets where short_name = 'pemn'),
    '98s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unsubstantiate'),
    (select id from sets where short_name = 'pemn'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Identity Thief'),
    (select id from sets where short_name = 'pemn'),
    '64s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nahiri''s Wrath'),
    (select id from sets where short_name = 'pemn'),
    '137s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Noosegraf Mob'),
    (select id from sets where short_name = 'pemn'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Splendid Reclamation'),
    (select id from sets where short_name = 'pemn'),
    '171p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voldaren Pariah // Abolisher of Bloodlines'),
    (select id from sets where short_name = 'pemn'),
    '111s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana, the Last Hope'),
    (select id from sets where short_name = 'pemn'),
    '93s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bloodhall Priest'),
    (select id from sets where short_name = 'pemn'),
    '181s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sigarda''s Aid'),
    (select id from sets where short_name = 'pemn'),
    '41p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Identity Thief'),
    (select id from sets where short_name = 'pemn'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Niblis of Frost'),
    (select id from sets where short_name = 'pemn'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oath of Liliana'),
    (select id from sets where short_name = 'pemn'),
    '99s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hanweir Battlements'),
    (select id from sets where short_name = 'pemn'),
    '204s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ulvenwald Observer'),
    (select id from sets where short_name = 'pemn'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spirit of the Hunt'),
    (select id from sets where short_name = 'pemn'),
    '170s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirrorwing Dragon'),
    (select id from sets where short_name = 'pemn'),
    '136s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dark Salvation'),
    (select id from sets where short_name = 'pemn'),
    '87s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Assembled Alphas'),
    (select id from sets where short_name = 'pemn'),
    '117s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sanctifier of Souls'),
    (select id from sets where short_name = 'pemn'),
    '39s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grim Flayer'),
    (select id from sets where short_name = 'pemn'),
    '184s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Imprisoned in the Moon'),
    (select id from sets where short_name = 'pemn'),
    '65p',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Geier Reach Sanitarium'),
    (select id from sets where short_name = 'pemn'),
    '203s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wharf Infiltrator'),
    (select id from sets where short_name = 'pemn'),
    '80s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heron''s Grace Champion'),
    (select id from sets where short_name = 'pemn'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thalia''s Lancers'),
    (select id from sets where short_name = 'pemn'),
    '47s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emrakul, the Promised End'),
    (select id from sets where short_name = 'pemn'),
    '6s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gisela, the Broken Blade'),
    (select id from sets where short_name = 'pemn'),
    '28s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Heron''s Grace Champion'),
    (select id from sets where short_name = 'pemn'),
    '185s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hanweir, the Writhing Township'),
    (select id from sets where short_name = 'pemn'),
    '130bs',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Distended Mindbender'),
    (select id from sets where short_name = 'pemn'),
    '3s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Collective Effort'),
    (select id from sets where short_name = 'pemn'),
    '17s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Niblis of Frost'),
    (select id from sets where short_name = 'pemn'),
    '72s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Docent of Perfection // Final Iteration'),
    (select id from sets where short_name = 'pemn'),
    '56s',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind''s Dilation'),
    (select id from sets where short_name = 'pemn'),
    '70s',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Collective Brutality'),
    (select id from sets where short_name = 'pemn'),
    '85s',
    'rare'
) 
 on conflict do nothing;
