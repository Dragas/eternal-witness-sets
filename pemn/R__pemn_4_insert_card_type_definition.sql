insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Soul Separator'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ulvenwald Observer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ulvenwald Observer'),
        (select types.id from types where name = 'Treefolk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gisa and Geralf'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gisa and Geralf'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gisa and Geralf'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gisa and Geralf'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bruna, the Fading Light'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bruna, the Fading Light'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bruna, the Fading Light'),
        (select types.id from types where name = 'Angel')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bruna, the Fading Light'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elder Deep-Fiend'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elder Deep-Fiend'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elder Deep-Fiend'),
        (select types.id from types where name = 'Octopus')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eternal Scourge'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eternal Scourge'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eternal Scourge'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Impetuous Devils'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Impetuous Devils'),
        (select types.id from types where name = 'Devil')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eldritch Evolution'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tamiyo, Field Researcher'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tamiyo, Field Researcher'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tamiyo, Field Researcher'),
        (select types.id from types where name = 'Tamiyo')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sigarda''s Aid'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tree of Perdition'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tree of Perdition'),
        (select types.id from types where name = 'Plant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stromkirk Occultist'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stromkirk Occultist'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stromkirk Occultist'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ulrich of the Krallenhorde // Ulrich, Uncontested Alpha'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ulrich of the Krallenhorde // Ulrich, Uncontested Alpha'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ulrich of the Krallenhorde // Ulrich, Uncontested Alpha'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ulrich of the Krallenhorde // Ulrich, Uncontested Alpha'),
        (select types.id from types where name = 'Werewolf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ulrich of the Krallenhorde // Ulrich, Uncontested Alpha'),
        (select types.id from types where name = '//')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ulrich of the Krallenhorde // Ulrich, Uncontested Alpha'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ulrich of the Krallenhorde // Ulrich, Uncontested Alpha'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ulrich of the Krallenhorde // Ulrich, Uncontested Alpha'),
        (select types.id from types where name = 'Werewolf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stromkirk Condemned'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stromkirk Condemned'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stromkirk Condemned'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Selfless Spirit'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Selfless Spirit'),
        (select types.id from types where name = 'Spirit')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Selfless Spirit'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Brisela, Voice of Nightmares'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Brisela, Voice of Nightmares'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Brisela, Voice of Nightmares'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Brisela, Voice of Nightmares'),
        (select types.id from types where name = 'Angel')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thalia, Heretic Cathar'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thalia, Heretic Cathar'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thalia, Heretic Cathar'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thalia, Heretic Cathar'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Harmless Offering'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stitcher''s Graft'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stitcher''s Graft'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Summary Dismissal'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thalia, Heretic Cathar'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thalia, Heretic Cathar'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thalia, Heretic Cathar'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thalia, Heretic Cathar'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Collective Defiance'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie // Zombie'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie // Zombie'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie // Zombie'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie // Zombie'),
        (select types.id from types where name = '//')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie // Zombie'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie // Zombie'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zombie // Zombie'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Deploy the Gatewatch'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mausoleum Wanderer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mausoleum Wanderer'),
        (select types.id from types where name = 'Spirit')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Emrakul''s Evangel'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Emrakul''s Evangel'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Emrakul''s Evangel'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Imprisoned in the Moon'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Imprisoned in the Moon'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Providence'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Decimator of the Provinces'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Decimator of the Provinces'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Decimator of the Provinces'),
        (select types.id from types where name = 'Boar')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Assembled Alphas'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Assembled Alphas'),
        (select types.id from types where name = 'Wolf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cryptbreaker'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cryptbreaker'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hanweir Garrison'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hanweir Garrison'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hanweir Garrison'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Coax from the Blind Eternities'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Permeating Mass'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Permeating Mass'),
        (select types.id from types where name = 'Spirit')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sanctifier of Souls'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sanctifier of Souls'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sanctifier of Souls'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spell Queller'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spell Queller'),
        (select types.id from types where name = 'Spirit')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Splendid Reclamation'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lupine Prototype'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lupine Prototype'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lupine Prototype'),
        (select types.id from types where name = 'Wolf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lupine Prototype'),
        (select types.id from types where name = 'Construct')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bedlam Reveler'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bedlam Reveler'),
        (select types.id from types where name = 'Devil')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bedlam Reveler'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ishkanah, Grafwidow'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ishkanah, Grafwidow'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ishkanah, Grafwidow'),
        (select types.id from types where name = 'Spider')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Noosegraf Mob'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Noosegraf Mob'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Unsubstantiate'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Identity Thief'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Identity Thief'),
        (select types.id from types where name = 'Shapeshifter')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nahiri''s Wrath'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Noosegraf Mob'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Noosegraf Mob'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Splendid Reclamation'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Voldaren Pariah // Abolisher of Bloodlines'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Voldaren Pariah // Abolisher of Bloodlines'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Voldaren Pariah // Abolisher of Bloodlines'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Voldaren Pariah // Abolisher of Bloodlines'),
        (select types.id from types where name = '//')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Voldaren Pariah // Abolisher of Bloodlines'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Voldaren Pariah // Abolisher of Bloodlines'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Voldaren Pariah // Abolisher of Bloodlines'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Liliana, the Last Hope'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Liliana, the Last Hope'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Liliana, the Last Hope'),
        (select types.id from types where name = 'Liliana')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodhall Priest'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodhall Priest'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodhall Priest'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sigarda''s Aid'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Identity Thief'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Identity Thief'),
        (select types.id from types where name = 'Shapeshifter')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Niblis of Frost'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Niblis of Frost'),
        (select types.id from types where name = 'Spirit')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Oath of Liliana'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Oath of Liliana'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hanweir Battlements'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ulvenwald Observer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ulvenwald Observer'),
        (select types.id from types where name = 'Treefolk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spirit of the Hunt'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spirit of the Hunt'),
        (select types.id from types where name = 'Wolf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spirit of the Hunt'),
        (select types.id from types where name = 'Spirit')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mirrorwing Dragon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mirrorwing Dragon'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dark Salvation'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Assembled Alphas'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Assembled Alphas'),
        (select types.id from types where name = 'Wolf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sanctifier of Souls'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sanctifier of Souls'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sanctifier of Souls'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grim Flayer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grim Flayer'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grim Flayer'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Imprisoned in the Moon'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Imprisoned in the Moon'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Geier Reach Sanitarium'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Geier Reach Sanitarium'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wharf Infiltrator'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wharf Infiltrator'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wharf Infiltrator'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Heron''s Grace Champion'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Heron''s Grace Champion'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Heron''s Grace Champion'),
        (select types.id from types where name = 'Knight')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thalia''s Lancers'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thalia''s Lancers'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thalia''s Lancers'),
        (select types.id from types where name = 'Knight')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Emrakul, the Promised End'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Emrakul, the Promised End'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Emrakul, the Promised End'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gisela, the Broken Blade'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gisela, the Broken Blade'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gisela, the Broken Blade'),
        (select types.id from types where name = 'Angel')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gisela, the Broken Blade'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Heron''s Grace Champion'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Heron''s Grace Champion'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Heron''s Grace Champion'),
        (select types.id from types where name = 'Knight')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hanweir, the Writhing Township'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hanweir, the Writhing Township'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hanweir, the Writhing Township'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hanweir, the Writhing Township'),
        (select types.id from types where name = 'Ooze')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Distended Mindbender'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Distended Mindbender'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Distended Mindbender'),
        (select types.id from types where name = 'Insect')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Collective Effort'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Niblis of Frost'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Niblis of Frost'),
        (select types.id from types where name = 'Spirit')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Docent of Perfection // Final Iteration'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Docent of Perfection // Final Iteration'),
        (select types.id from types where name = 'Insect')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Docent of Perfection // Final Iteration'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Docent of Perfection // Final Iteration'),
        (select types.id from types where name = '//')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Docent of Perfection // Final Iteration'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Docent of Perfection // Final Iteration'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Docent of Perfection // Final Iteration'),
        (select types.id from types where name = 'Insect')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mind''s Dilation'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Collective Brutality'),
        (select types.id from types where name = 'Sorcery')
    ) 
 on conflict do nothing;
