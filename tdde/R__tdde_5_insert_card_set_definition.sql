insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Hornet'),
    (select id from sets where short_name = 'tdde'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Minion'),
    (select id from sets where short_name = 'tdde'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Saproling'),
    (select id from sets where short_name = 'tdde'),
    '3',
    'common'
) 
 on conflict do nothing;
