insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Lurking Nightstalker'),
    (select id from sets where short_name = 'p02'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chorus of Woe'),
    (select id from sets where short_name = 'p02'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Earthquake'),
    (select id from sets where short_name = 'p02'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Razorclaw Bear'),
    (select id from sets where short_name = 'p02'),
    '142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Yeti'),
    (select id from sets where short_name = 'p02'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cunning Giant'),
    (select id from sets where short_name = 'p02'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abyssal Nightstalker'),
    (select id from sets where short_name = 'p02'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coastal Wizard'),
    (select id from sets where short_name = 'p02'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raiding Nightstalker'),
    (select id from sets where short_name = 'p02'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barbtooth Wurm'),
    (select id from sets where short_name = 'p02'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jagged Lightning'),
    (select id from sets where short_name = 'p02'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alaborn Grenadier'),
    (select id from sets where short_name = 'p02'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wind Sail'),
    (select id from sets where short_name = 'p02'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talas Researcher'),
    (select id from sets where short_name = 'p02'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel of Fury'),
    (select id from sets where short_name = 'p02'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angelic Blessing'),
    (select id from sets where short_name = 'p02'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Touch of Brilliance'),
    (select id from sets where short_name = 'p02'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Piker'),
    (select id from sets where short_name = 'p02'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'p02'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warrior''s Stand'),
    (select id from sets where short_name = 'p02'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Ox'),
    (select id from sets where short_name = 'p02'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = 'p02'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dakmor Scorpion'),
    (select id from sets where short_name = 'p02'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lynx'),
    (select id from sets where short_name = 'p02'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talas Merchant'),
    (select id from sets where short_name = 'p02'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Denial'),
    (select id from sets where short_name = 'p02'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vengeance'),
    (select id from sets where short_name = 'p02'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Craving'),
    (select id from sets where short_name = 'p02'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Monstrous Growth'),
    (select id from sets where short_name = 'p02'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin War Cry'),
    (select id from sets where short_name = 'p02'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blaze'),
    (select id from sets where short_name = 'p02'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Offering'),
    (select id from sets where short_name = 'p02'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'p02'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'p02'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alaborn Musketeer'),
    (select id from sets where short_name = 'p02'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armored Galleon'),
    (select id from sets where short_name = 'p02'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Talas Scout'),
    (select id from sets where short_name = 'p02'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undo'),
    (select id from sets where short_name = 'p02'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armored Griffin'),
    (select id from sets where short_name = 'p02'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brimstone Dragon'),
    (select id from sets where short_name = 'p02'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dakmor Plague'),
    (select id from sets where short_name = 'p02'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volcanic Hammer'),
    (select id from sets where short_name = 'p02'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tidal Surge'),
    (select id from sets where short_name = 'p02'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rally the Troops'),
    (select id from sets where short_name = 'p02'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightstalker Engine'),
    (select id from sets where short_name = 'p02'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Denizen of the Deep'),
    (select id from sets where short_name = 'p02'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Extinguish'),
    (select id from sets where short_name = 'p02'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hidden Horror'),
    (select id from sets where short_name = 'p02'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foul Spirit'),
    (select id from sets where short_name = 'p02'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Talas Explorer'),
    (select id from sets where short_name = 'p02'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alluring Scent'),
    (select id from sets where short_name = 'p02'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ogre Warrior'),
    (select id from sets where short_name = 'p02'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Raider'),
    (select id from sets where short_name = 'p02'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hurricane'),
    (select id from sets where short_name = 'p02'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Screeching Drake'),
    (select id from sets where short_name = 'p02'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Norwood Warrior'),
    (select id from sets where short_name = 'p02'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin War Strike'),
    (select id from sets where short_name = 'p02'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prowling Nightstalker'),
    (select id from sets where short_name = 'p02'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bee Sting'),
    (select id from sets where short_name = 'p02'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Rot'),
    (select id from sets where short_name = 'p02'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Natural Spring'),
    (select id from sets where short_name = 'p02'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alaborn Veteran'),
    (select id from sets where short_name = 'p02'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Norwood Priestess'),
    (select id from sets where short_name = 'p02'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple Acolyte'),
    (select id from sets where short_name = 'p02'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathcoil Wurm'),
    (select id from sets where short_name = 'p02'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ironhoof Ox'),
    (select id from sets where short_name = 'p02'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Predatory Nightstalker'),
    (select id from sets where short_name = 'p02'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin General'),
    (select id from sets where short_name = 'p02'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lone Wolf'),
    (select id from sets where short_name = 'p02'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Talas Air Ship'),
    (select id from sets where short_name = 'p02'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'p02'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Firestarter'),
    (select id from sets where short_name = 'p02'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ravenous Rats'),
    (select id from sets where short_name = 'p02'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'p02'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'p02'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alaborn Cavalier'),
    (select id from sets where short_name = 'p02'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'p02'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Norwood Riders'),
    (select id from sets where short_name = 'p02'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magma Giant'),
    (select id from sets where short_name = 'p02'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = 'p02'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dakmor Sorceress'),
    (select id from sets where short_name = 'p02'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raise Dead'),
    (select id from sets where short_name = 'p02'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Glider'),
    (select id from sets where short_name = 'p02'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trokin High Guard'),
    (select id from sets where short_name = 'p02'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Apprentice Sorcerer'),
    (select id from sets where short_name = 'p02'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Righteous Charge'),
    (select id from sets where short_name = 'p02'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Breath of Life'),
    (select id from sets where short_name = 'p02'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Griffin'),
    (select id from sets where short_name = 'p02'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alaborn Zealot'),
    (select id from sets where short_name = 'p02'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Just Fate'),
    (select id from sets where short_name = 'p02'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Theft of Dreams'),
    (select id from sets where short_name = 'p02'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hand of Death'),
    (select id from sets where short_name = 'p02'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'p02'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obsidian Giant'),
    (select id from sets where short_name = 'p02'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'p02'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Remove'),
    (select id from sets where short_name = 'p02'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'p02'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Déjà Vu'),
    (select id from sets where short_name = 'p02'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'p02'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'p02'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temporal Manipulation'),
    (select id from sets where short_name = 'p02'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brutal Nightstalker'),
    (select id from sets where short_name = 'p02'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plated Wurm'),
    (select id from sets where short_name = 'p02'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Renewing Touch'),
    (select id from sets where short_name = 'p02'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ogre Arsonist'),
    (select id from sets where short_name = 'p02'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ogre Berserker'),
    (select id from sets where short_name = 'p02'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wildfire'),
    (select id from sets where short_name = 'p02'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'River Bear'),
    (select id from sets where short_name = 'p02'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Basilisk'),
    (select id from sets where short_name = 'p02'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rain of Daggers'),
    (select id from sets where short_name = 'p02'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'p02'),
    '160',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archangel'),
    (select id from sets where short_name = 'p02'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kiss of Death'),
    (select id from sets where short_name = 'p02'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'p02'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Righteous Fury'),
    (select id from sets where short_name = 'p02'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Norwood Archers'),
    (select id from sets where short_name = 'p02'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Norwood Ranger'),
    (select id from sets where short_name = 'p02'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tree Monkey'),
    (select id from sets where short_name = 'p02'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relentless Assault'),
    (select id from sets where short_name = 'p02'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Untamed Wilds'),
    (select id from sets where short_name = 'p02'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Path of Peace'),
    (select id from sets where short_name = 'p02'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Air Elemental'),
    (select id from sets where short_name = 'p02'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'False Summoning'),
    (select id from sets where short_name = 'p02'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steam Catapult'),
    (select id from sets where short_name = 'p02'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moaning Spirit'),
    (select id from sets where short_name = 'p02'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deep Wood'),
    (select id from sets where short_name = 'p02'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ogre Taskmaster'),
    (select id from sets where short_name = 'p02'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bear Cub'),
    (select id from sets where short_name = 'p02'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golden Bear'),
    (select id from sets where short_name = 'p02'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sea Drake'),
    (select id from sets where short_name = 'p02'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel of Mercy'),
    (select id from sets where short_name = 'p02'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Piracy'),
    (select id from sets where short_name = 'p02'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple Elder'),
    (select id from sets where short_name = 'p02'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodcurdling Scream'),
    (select id from sets where short_name = 'p02'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volunteer Militia'),
    (select id from sets where short_name = 'p02'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talas Warrior'),
    (select id from sets where short_name = 'p02'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coercion'),
    (select id from sets where short_name = 'p02'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bargain'),
    (select id from sets where short_name = 'p02'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Muck Rats'),
    (select id from sets where short_name = 'p02'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Wall'),
    (select id from sets where short_name = 'p02'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alaborn Trooper'),
    (select id from sets where short_name = 'p02'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tremor'),
    (select id from sets where short_name = 'p02'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harmony of Nature'),
    (select id from sets where short_name = 'p02'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Town Sentry'),
    (select id from sets where short_name = 'p02'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raging Goblin'),
    (select id from sets where short_name = 'p02'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Return of the Nightstalkers'),
    (select id from sets where short_name = 'p02'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Time Ebb'),
    (select id from sets where short_name = 'p02'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steam Frigate'),
    (select id from sets where short_name = 'p02'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Cavaliers'),
    (select id from sets where short_name = 'p02'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eye Spy'),
    (select id from sets where short_name = 'p02'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swarm of Rats'),
    (select id from sets where short_name = 'p02'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Lore'),
    (select id from sets where short_name = 'p02'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Salvage'),
    (select id from sets where short_name = 'p02'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sleight of Hand'),
    (select id from sets where short_name = 'p02'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nature''s Lore'),
    (select id from sets where short_name = 'p02'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dakmor Bat'),
    (select id from sets where short_name = 'p02'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Festival of Trokin'),
    (select id from sets where short_name = 'p02'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cruel Edict'),
    (select id from sets where short_name = 'p02'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampiric Spirit'),
    (select id from sets where short_name = 'p02'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'p02'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Mountaineer'),
    (select id from sets where short_name = 'p02'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spitting Earth'),
    (select id from sets where short_name = 'p02'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Matron'),
    (select id from sets where short_name = 'p02'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Exhaustion'),
    (select id from sets where short_name = 'p02'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lava Axe'),
    (select id from sets where short_name = 'p02'),
    '107',
    'common'
) 
 on conflict do nothing;
