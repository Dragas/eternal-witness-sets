insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Nira, Hellkite Duelist'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nira, Hellkite Duelist'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nira, Hellkite Duelist'),
        (select types.id from types where name = 'Dragon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dungeon Master'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dungeon Master'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dungeon Master'),
        (select types.id from types where name = 'Dungeon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dungeon Master'),
        (select types.id from types where name = 'Master')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chandra, Gremlin Wrangler'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chandra, Gremlin Wrangler'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Chandra, Gremlin Wrangler'),
        (select types.id from types where name = 'Chandra')
    ) 
 on conflict do nothing;
