insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Nira, Hellkite Duelist'),
    (select id from sets where short_name = 'htr'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dungeon Master'),
    (select id from sets where short_name = 'htr'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chandra, Gremlin Wrangler'),
    (select id from sets where short_name = 'htr'),
    '1',
    'mythic'
) 
 on conflict do nothing;
