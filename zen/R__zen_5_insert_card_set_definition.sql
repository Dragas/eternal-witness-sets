insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Kor Outfitter'),
    (select id from sets where short_name = 'zen'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quest for the Holy Relic'),
    (select id from sets where short_name = 'zen'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Relic Crush'),
    (select id from sets where short_name = 'zen'),
    '179',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beastmaster Ascension'),
    (select id from sets where short_name = 'zen'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kor Sanctifiers'),
    (select id from sets where short_name = 'zen'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'zen'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stonework Puma'),
    (select id from sets where short_name = 'zen'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grazing Gladehart'),
    (select id from sets where short_name = 'zen'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Journey to Nowhere'),
    (select id from sets where short_name = 'zen'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whiplash Trap'),
    (select id from sets where short_name = 'zen'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bala Ged Thief'),
    (select id from sets where short_name = 'zen'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trapfinder''s Trick'),
    (select id from sets where short_name = 'zen'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief Survivalist'),
    (select id from sets where short_name = 'zen'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Ruinblaster'),
    (select id from sets where short_name = 'zen'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quest for Ancient Secrets'),
    (select id from sets where short_name = 'zen'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Tsunami'),
    (select id from sets where short_name = 'zen'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'zen'),
    '233a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Lacerator'),
    (select id from sets where short_name = 'zen'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gomazoa'),
    (select id from sets where short_name = 'zen'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'World Queller'),
    (select id from sets where short_name = 'zen'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bladetusk Boar'),
    (select id from sets where short_name = 'zen'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arid Mesa'),
    (select id from sets where short_name = 'zen'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Devout Lightcaster'),
    (select id from sets where short_name = 'zen'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cliff Threader'),
    (select id from sets where short_name = 'zen'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'zen'),
    '248a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lotus Cobra'),
    (select id from sets where short_name = 'zen'),
    '168',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Feast of Blood'),
    (select id from sets where short_name = 'zen'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Conqueror''s Pledge'),
    (select id from sets where short_name = 'zen'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa Revane'),
    (select id from sets where short_name = 'zen'),
    '170',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Heartstabber Mosquito'),
    (select id from sets where short_name = 'zen'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mire Blight'),
    (select id from sets where short_name = 'zen'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blazing Torch'),
    (select id from sets where short_name = 'zen'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spidersilk Net'),
    (select id from sets where short_name = 'zen'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obsidian Fireheart'),
    (select id from sets where short_name = 'zen'),
    '140',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sejiri Refuge'),
    (select id from sets where short_name = 'zen'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seismic Shudder'),
    (select id from sets where short_name = 'zen'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'zen'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'zen'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primal Bellow'),
    (select id from sets where short_name = 'zen'),
    '176',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Landbind Ritual'),
    (select id from sets where short_name = 'zen'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Misty Rainforest'),
    (select id from sets where short_name = 'zen'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pitfall Trap'),
    (select id from sets where short_name = 'zen'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marsh Casualties'),
    (select id from sets where short_name = 'zen'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyromancer Ascension'),
    (select id from sets where short_name = 'zen'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Surrakar Marauder'),
    (select id from sets where short_name = 'zen'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sorin Markov'),
    (select id from sets where short_name = 'zen'),
    '111',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Day of Judgment'),
    (select id from sets where short_name = 'zen'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ior Ruin Expedition'),
    (select id from sets where short_name = 'zen'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sky Ruin Drake'),
    (select id from sets where short_name = 'zen'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Narrow Escape'),
    (select id from sets where short_name = 'zen'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burst Lightning'),
    (select id from sets where short_name = 'zen'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akoum Refuge'),
    (select id from sets where short_name = 'zen'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hideous End'),
    (select id from sets where short_name = 'zen'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scalding Tarn'),
    (select id from sets where short_name = 'zen'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'zen'),
    '240a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hagra Crocodile'),
    (select id from sets where short_name = 'zen'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'zen'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lethargy Trap'),
    (select id from sets where short_name = 'zen'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Welkin Tern'),
    (select id from sets where short_name = 'zen'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'zen'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seascape Aerialist'),
    (select id from sets where short_name = 'zen'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spreading Seas'),
    (select id from sets where short_name = 'zen'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief Recluse'),
    (select id from sets where short_name = 'zen'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Valakut, the Molten Pinnacle'),
    (select id from sets where short_name = 'zen'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elemental Appeal'),
    (select id from sets where short_name = 'zen'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Adventuring Gear'),
    (select id from sets where short_name = 'zen'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magosi, the Waterveil'),
    (select id from sets where short_name = 'zen'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'zen'),
    '247a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desecrated Earth'),
    (select id from sets where short_name = 'zen'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Bushwhacker'),
    (select id from sets where short_name = 'zen'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin War Paint'),
    (select id from sets where short_name = 'zen'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra Ablaze'),
    (select id from sets where short_name = 'zen'),
    '120',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Scute Mob'),
    (select id from sets where short_name = 'zen'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampire Nighthawk'),
    (select id from sets where short_name = 'zen'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marsh Flats'),
    (select id from sets where short_name = 'zen'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Territorial Baloth'),
    (select id from sets where short_name = 'zen'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rite of Replication'),
    (select id from sets where short_name = 'zen'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'zen'),
    '243a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Celestial Mantle'),
    (select id from sets where short_name = 'zen'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kraken Hatchling'),
    (select id from sets where short_name = 'zen'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast Hunt'),
    (select id from sets where short_name = 'zen'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'zen'),
    '239a',
    'common'
) ,
(
    (select id from mtgcard where name = 'River Boa'),
    (select id from sets where short_name = 'zen'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terra Stomper'),
    (select id from sets where short_name = 'zen'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Paralyzing Grasp'),
    (select id from sets where short_name = 'zen'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'zen'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grim Discovery'),
    (select id from sets where short_name = 'zen'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mindbreak Trap'),
    (select id from sets where short_name = 'zen'),
    '57',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'zen'),
    '242a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Duelist'),
    (select id from sets where short_name = 'zen'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merfolk Seastalkers'),
    (select id from sets where short_name = 'zen'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'zen'),
    '230a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carnage Altar'),
    (select id from sets where short_name = 'zen'),
    '198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'zen'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grappling Hook'),
    (select id from sets where short_name = 'zen'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'zen'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Punishing Fire'),
    (select id from sets where short_name = 'zen'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kabira Evangel'),
    (select id from sets where short_name = 'zen'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mark of Mutiny'),
    (select id from sets where short_name = 'zen'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Loremaster'),
    (select id from sets where short_name = 'zen'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nimbus Wings'),
    (select id from sets where short_name = 'zen'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Shortcutter'),
    (select id from sets where short_name = 'zen'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Into the Roil'),
    (select id from sets where short_name = 'zen'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bold Defense'),
    (select id from sets where short_name = 'zen'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Halo Hunter'),
    (select id from sets where short_name = 'zen'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Umara Raptor'),
    (select id from sets where short_name = 'zen'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Cartographer'),
    (select id from sets where short_name = 'zen'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murasa Pyromancer'),
    (select id from sets where short_name = 'zen'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lorthos, the Tidemaker'),
    (select id from sets where short_name = 'zen'),
    '53',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Iona, Shield of Emeria'),
    (select id from sets where short_name = 'zen'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bloodghast'),
    (select id from sets where short_name = 'zen'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hedron Scrabbler'),
    (select id from sets where short_name = 'zen'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire''s Bite'),
    (select id from sets where short_name = 'zen'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Roil Elemental'),
    (select id from sets where short_name = 'zen'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rampaging Baloths'),
    (select id from sets where short_name = 'zen'),
    '178',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Crypt of Agadeem'),
    (select id from sets where short_name = 'zen'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'zen'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archive Trap'),
    (select id from sets where short_name = 'zen'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'zen'),
    '236a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Hookmaster'),
    (select id from sets where short_name = 'zen'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'zen'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soaring Seacliff'),
    (select id from sets where short_name = 'zen'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demolish'),
    (select id from sets where short_name = 'zen'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guul Draz Specter'),
    (select id from sets where short_name = 'zen'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merfolk Wayfinder'),
    (select id from sets where short_name = 'zen'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gatekeeper of Malakir'),
    (select id from sets where short_name = 'zen'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Summoning Trap'),
    (select id from sets where short_name = 'zen'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aether Figment'),
    (select id from sets where short_name = 'zen'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Greenweaver Druid'),
    (select id from sets where short_name = 'zen'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis, the Fallen'),
    (select id from sets where short_name = 'zen'),
    '107',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Khalni Heart Expedition'),
    (select id from sets where short_name = 'zen'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'zen'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Sludge'),
    (select id from sets where short_name = 'zen'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Verdant Catacombs'),
    (select id from sets where short_name = 'zen'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quest for Pure Flame'),
    (select id from sets where short_name = 'zen'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'zen'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blade of the Bloodchief'),
    (select id from sets where short_name = 'zen'),
    '196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oracle of Mul Daya'),
    (select id from sets where short_name = 'zen'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'zen'),
    '241a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quest for the Gravelord'),
    (select id from sets where short_name = 'zen'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Baloth Woodcrasher'),
    (select id from sets where short_name = 'zen'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'zen'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kalitas, Bloodchief of Ghet'),
    (select id from sets where short_name = 'zen'),
    '99',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Malakir Bloodwitch'),
    (select id from sets where short_name = 'zen'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trusty Machete'),
    (select id from sets where short_name = 'zen'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kor Aeronaut'),
    (select id from sets where short_name = 'zen'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'zen'),
    '231a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cobra Trap'),
    (select id from sets where short_name = 'zen'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kazandu Refuge'),
    (select id from sets where short_name = 'zen'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Emeria Angel'),
    (select id from sets where short_name = 'zen'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Khalni Gem'),
    (select id from sets where short_name = 'zen'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savage Silhouette'),
    (select id from sets where short_name = 'zen'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nimana Sell-Sword'),
    (select id from sets where short_name = 'zen'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Needlebite Trap'),
    (select id from sets where short_name = 'zen'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hellfire Mongrel'),
    (select id from sets where short_name = 'zen'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Stair Expedition'),
    (select id from sets where short_name = 'zen'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mold Shambler'),
    (select id from sets where short_name = 'zen'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Hexmage'),
    (select id from sets where short_name = 'zen'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kazandu Blademaster'),
    (select id from sets where short_name = 'zen'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armament Master'),
    (select id from sets where short_name = 'zen'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Electropotence'),
    (select id from sets where short_name = 'zen'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warren Instigator'),
    (select id from sets where short_name = 'zen'),
    '154',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Crypt Ripper'),
    (select id from sets where short_name = 'zen'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inferno Trap'),
    (select id from sets where short_name = 'zen'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Windborne Charge'),
    (select id from sets where short_name = 'zen'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief, the Vastwood'),
    (select id from sets where short_name = 'zen'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trapmaker''s Snare'),
    (select id from sets where short_name = 'zen'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shatterskull Giant'),
    (select id from sets where short_name = 'zen'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scythe Tiger'),
    (select id from sets where short_name = 'zen'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cancel'),
    (select id from sets where short_name = 'zen'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molten Ravager'),
    (select id from sets where short_name = 'zen'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bog Tatters'),
    (select id from sets where short_name = 'zen'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'zen'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx of Jwar Isle'),
    (select id from sets where short_name = 'zen'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Turntimber Ranger'),
    (select id from sets where short_name = 'zen'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tanglesap'),
    (select id from sets where short_name = 'zen'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shoal Serpent'),
    (select id from sets where short_name = 'zen'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nissa''s Chosen'),
    (select id from sets where short_name = 'zen'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kabira Crossroads'),
    (select id from sets where short_name = 'zen'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eternity Vessel'),
    (select id from sets where short_name = 'zen'),
    '200',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bloodchief Ascension'),
    (select id from sets where short_name = 'zen'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gigantiform'),
    (select id from sets where short_name = 'zen'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Monument'),
    (select id from sets where short_name = 'zen'),
    '199',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'zen'),
    '237a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'zen'),
    '245a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trailblazer''s Boots'),
    (select id from sets where short_name = 'zen'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sunspring Expedition'),
    (select id from sets where short_name = 'zen'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steppe Lynx'),
    (select id from sets where short_name = 'zen'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Guide'),
    (select id from sets where short_name = 'zen'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Disfigure'),
    (select id from sets where short_name = 'zen'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zendikar Farguide'),
    (select id from sets where short_name = 'zen'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'zen'),
    '232a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Joraga Bard'),
    (select id from sets where short_name = 'zen'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Caller of Gales'),
    (select id from sets where short_name = 'zen'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mindless Null'),
    (select id from sets where short_name = 'zen'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Turntimber Basilisk'),
    (select id from sets where short_name = 'zen'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Geyser Glider'),
    (select id from sets where short_name = 'zen'),
    '124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frontier Guide'),
    (select id from sets where short_name = 'zen'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reckless Scholar'),
    (select id from sets where short_name = 'zen'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Skyfisher'),
    (select id from sets where short_name = 'zen'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'zen'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vastwood Gorger'),
    (select id from sets where short_name = 'zen'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'zen'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baloth Cage Trap'),
    (select id from sets where short_name = 'zen'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lavaball Trap'),
    (select id from sets where short_name = 'zen'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brave the Elements'),
    (select id from sets where short_name = 'zen'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Windrider Eel'),
    (select id from sets where short_name = 'zen'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Scorpion'),
    (select id from sets where short_name = 'zen'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hagra Diabolist'),
    (select id from sets where short_name = 'zen'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Graypelt Refuge'),
    (select id from sets where short_name = 'zen'),
    '214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'zen'),
    '234a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Expedition Map'),
    (select id from sets where short_name = 'zen'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'zen'),
    '246a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Tribute'),
    (select id from sets where short_name = 'zen'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slaughter Cry'),
    (select id from sets where short_name = 'zen'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spell Pierce'),
    (select id from sets where short_name = 'zen'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Felidar Sovereign'),
    (select id from sets where short_name = 'zen'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ondu Cleric'),
    (select id from sets where short_name = 'zen'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tempest Owl'),
    (select id from sets where short_name = 'zen'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blood Seeker'),
    (select id from sets where short_name = 'zen'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unstable Footing'),
    (select id from sets where short_name = 'zen'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Timbermaw Larva'),
    (select id from sets where short_name = 'zen'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pillarfield Ox'),
    (select id from sets where short_name = 'zen'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Predatory Urge'),
    (select id from sets where short_name = 'zen'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'zen'),
    '238a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arrow Volley Trap'),
    (select id from sets where short_name = 'zen'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sphinx of Lost Truths'),
    (select id from sets where short_name = 'zen'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kazuul Warlord'),
    (select id from sets where short_name = 'zen'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noble Vestige'),
    (select id from sets where short_name = 'zen'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archmage Ascension'),
    (select id from sets where short_name = 'zen'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jwar Isle Refuge'),
    (select id from sets where short_name = 'zen'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'zen'),
    '244a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cosi''s Trickster'),
    (select id from sets where short_name = 'zen'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Runeflare Trap'),
    (select id from sets where short_name = 'zen'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harrow'),
    (select id from sets where short_name = 'zen'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lullmage Mentor'),
    (select id from sets where short_name = 'zen'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Highland Berserker'),
    (select id from sets where short_name = 'zen'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'zen'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teetering Peaks'),
    (select id from sets where short_name = 'zen'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tuktuk Grunts'),
    (select id from sets where short_name = 'zen'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hellkite Charger'),
    (select id from sets where short_name = 'zen'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Makindi Shieldmate'),
    (select id from sets where short_name = 'zen'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ruinous Minotaur'),
    (select id from sets where short_name = 'zen'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vines of Vastwood'),
    (select id from sets where short_name = 'zen'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'zen'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Luminarch Ascension'),
    (select id from sets where short_name = 'zen'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Piranha Marsh'),
    (select id from sets where short_name = 'zen'),
    '222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quest for the Gemblades'),
    (select id from sets where short_name = 'zen'),
    '177',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Explorer''s Scope'),
    (select id from sets where short_name = 'zen'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tajuru Archer'),
    (select id from sets where short_name = 'zen'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Summoner''s Bane'),
    (select id from sets where short_name = 'zen'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magma Rift'),
    (select id from sets where short_name = 'zen'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'zen'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'zen'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hedron Crab'),
    (select id from sets where short_name = 'zen'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zektar Shrine Expedition'),
    (select id from sets where short_name = 'zen'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'zen'),
    '249a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guul Draz Vampire'),
    (select id from sets where short_name = 'zen'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sadistic Sacrament'),
    (select id from sets where short_name = 'zen'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ravenous Trap'),
    (select id from sets where short_name = 'zen'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Torch Slinger'),
    (select id from sets where short_name = 'zen'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shieldmate''s Blessing'),
    (select id from sets where short_name = 'zen'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'zen'),
    '235a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spire Barrage'),
    (select id from sets where short_name = 'zen'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Turntimber Grove'),
    (select id from sets where short_name = 'zen'),
    '227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plated Geopede'),
    (select id from sets where short_name = 'zen'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emeria, the Sky Ruin'),
    (select id from sets where short_name = 'zen'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shepherd of the Lost'),
    (select id from sets where short_name = 'zen'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Caravan Hurda'),
    (select id from sets where short_name = 'zen'),
    '5',
    'common'
) 
 on conflict do nothing;
