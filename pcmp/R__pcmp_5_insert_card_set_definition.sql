insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Niv-Mizzet, the Firemind'),
    (select id from sets where short_name = 'pcmp'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Groundbreaker'),
    (select id from sets where short_name = 'pcmp'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voidslime'),
    (select id from sets where short_name = 'pcmp'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imperious Perfect'),
    (select id from sets where short_name = 'pcmp'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Guildmage'),
    (select id from sets where short_name = 'pcmp'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Electrolyze'),
    (select id from sets where short_name = 'pcmp'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Knight'),
    (select id from sets where short_name = 'pcmp'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Factory'),
    (select id from sets where short_name = 'pcmp'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mutavault'),
    (select id from sets where short_name = 'pcmp'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bramblewood Paragon'),
    (select id from sets where short_name = 'pcmp'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Avenger'),
    (select id from sets where short_name = 'pcmp'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Doran, the Siege Tower'),
    (select id from sets where short_name = 'pcmp'),
    '10',
    'rare'
) 
 on conflict do nothing;
