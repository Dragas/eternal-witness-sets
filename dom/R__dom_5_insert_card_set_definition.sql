insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Charge'),
    (select id from sets where short_name = 'dom'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tolarian Scholar'),
    (select id from sets where short_name = 'dom'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'dom'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dom'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evra, Halcyon Witness'),
    (select id from sets where short_name = 'dom'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firefist Adept'),
    (select id from sets where short_name = 'dom'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Sentinel'),
    (select id from sets where short_name = 'dom'),
    '273',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dom'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'dom'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'On Serra''s Wings'),
    (select id from sets where short_name = 'dom'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whisper, Blood Liturgist'),
    (select id from sets where short_name = 'dom'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Karn, Scion of Urza'),
    (select id from sets where short_name = 'dom'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rescue'),
    (select id from sets where short_name = 'dom'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Demonic Vigor'),
    (select id from sets where short_name = 'dom'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dom'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcane Flight'),
    (select id from sets where short_name = 'dom'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cabal Stronghold'),
    (select id from sets where short_name = 'dom'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'dom'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Mending of Dominaria'),
    (select id from sets where short_name = 'dom'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warlord''s Fury'),
    (select id from sets where short_name = 'dom'),
    '151',
    'common'
) ,
(
    (select id from mtgcard where name = 'The First Eruption'),
    (select id from sets where short_name = 'dom'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curator''s Ward'),
    (select id from sets where short_name = 'dom'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cast Down'),
    (select id from sets where short_name = 'dom'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tetsuko Umezawa, Fugitive'),
    (select id from sets where short_name = 'dom'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skirk Prospector'),
    (select id from sets where short_name = 'dom'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Verix Bladewing'),
    (select id from sets where short_name = 'dom'),
    '149',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Llanowar Envoy'),
    (select id from sets where short_name = 'dom'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Settle the Score'),
    (select id from sets where short_name = 'dom'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Mirari Conjecture'),
    (select id from sets where short_name = 'dom'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Helm of the Host'),
    (select id from sets where short_name = 'dom'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Verdant Force'),
    (select id from sets where short_name = 'dom'),
    '187',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rite of Belzenlok'),
    (select id from sets where short_name = 'dom'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jodah, Archmage Eternal'),
    (select id from sets where short_name = 'dom'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thallid Soothsayer'),
    (select id from sets where short_name = 'dom'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shivan Fire'),
    (select id from sets where short_name = 'dom'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keldon Warcaller'),
    (select id from sets where short_name = 'dom'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lingering Phantom'),
    (select id from sets where short_name = 'dom'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deathbloom Thallid'),
    (select id from sets where short_name = 'dom'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Blessing'),
    (select id from sets where short_name = 'dom'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'dom'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Steel Leaf Champion'),
    (select id from sets where short_name = 'dom'),
    '182',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Marwyn, the Nurturer'),
    (select id from sets where short_name = 'dom'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Valduk, Keeper of the Flame'),
    (select id from sets where short_name = 'dom'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Saproling Migration'),
    (select id from sets where short_name = 'dom'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aesthir Glider'),
    (select id from sets where short_name = 'dom'),
    '209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opt'),
    (select id from sets where short_name = 'dom'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Voltaic Servant'),
    (select id from sets where short_name = 'dom'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kazarov, Sengir Pureblood'),
    (select id from sets where short_name = 'dom'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cabal Paladin'),
    (select id from sets where short_name = 'dom'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Caligo Skin-Witch'),
    (select id from sets where short_name = 'dom'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dom'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weight of Memory'),
    (select id from sets where short_name = 'dom'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pardic Wanderer'),
    (select id from sets where short_name = 'dom'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zahid, Djinn of the Lamp'),
    (select id from sets where short_name = 'dom'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Daring Archaeologist'),
    (select id from sets where short_name = 'dom'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodstone Goblin'),
    (select id from sets where short_name = 'dom'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Flame of Keld'),
    (select id from sets where short_name = 'dom'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feral Abomination'),
    (select id from sets where short_name = 'dom'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Triumph of Gerrard'),
    (select id from sets where short_name = 'dom'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul Salvage'),
    (select id from sets where short_name = 'dom'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pegasus Courser'),
    (select id from sets where short_name = 'dom'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk Trickster'),
    (select id from sets where short_name = 'dom'),
    '56',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jhoira''s Familiar'),
    (select id from sets where short_name = 'dom'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firesong and Sunspeaker'),
    (select id from sets where short_name = 'dom'),
    '280',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primordial Wurm'),
    (select id from sets where short_name = 'dom'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fall of the Thran'),
    (select id from sets where short_name = 'dom'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tempest Djinn'),
    (select id from sets where short_name = 'dom'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Untamed Kavu'),
    (select id from sets where short_name = 'dom'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Territorial Allosaurus'),
    (select id from sets where short_name = 'dom'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Adamant Will'),
    (select id from sets where short_name = 'dom'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jousting Lance'),
    (select id from sets where short_name = 'dom'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'dom'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rona, Disciple of Gix'),
    (select id from sets where short_name = 'dom'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'dom'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Champion of the Flame'),
    (select id from sets where short_name = 'dom'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dread Shade'),
    (select id from sets where short_name = 'dom'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jaya''s Immolating Inferno'),
    (select id from sets where short_name = 'dom'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Barrage'),
    (select id from sets where short_name = 'dom'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blessed Light'),
    (select id from sets where short_name = 'dom'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rat Colony'),
    (select id from sets where short_name = 'dom'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baloth Gorger'),
    (select id from sets where short_name = 'dom'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deep Freeze'),
    (select id from sets where short_name = 'dom'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grow from the Ashes'),
    (select id from sets where short_name = 'dom'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forebear''s Blade'),
    (select id from sets where short_name = 'dom'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Multani, Yavimaya''s Avatar'),
    (select id from sets where short_name = 'dom'),
    '174',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Memorial to War'),
    (select id from sets where short_name = 'dom'),
    '246',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'dom'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fungal Plots'),
    (select id from sets where short_name = 'dom'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Memorial to Glory'),
    (select id from sets where short_name = 'dom'),
    '244',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Naru Meha, Master Wizard'),
    (select id from sets where short_name = 'dom'),
    '59',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Urza''s Ruinous Blast'),
    (select id from sets where short_name = 'dom'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'dom'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aryel, Knight of Windgrace'),
    (select id from sets where short_name = 'dom'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wizard''s Lightning'),
    (select id from sets where short_name = 'dom'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oath of Teferi'),
    (select id from sets where short_name = 'dom'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Warchief'),
    (select id from sets where short_name = 'dom'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vodalian Arcanist'),
    (select id from sets where short_name = 'dom'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teferi, Hero of Dominaria'),
    (select id from sets where short_name = 'dom'),
    '207',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Elfhame Druid'),
    (select id from sets where short_name = 'dom'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Sapherd'),
    (select id from sets where short_name = 'dom'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodtallow Candle'),
    (select id from sets where short_name = 'dom'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orcish Vandal'),
    (select id from sets where short_name = 'dom'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dom'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Elemental'),
    (select id from sets where short_name = 'dom'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eviscerate'),
    (select id from sets where short_name = 'dom'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yargle, Glutton of Urborg'),
    (select id from sets where short_name = 'dom'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diligent Excavator'),
    (select id from sets where short_name = 'dom'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sporecrown Thallid'),
    (select id from sets where short_name = 'dom'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mox Amber'),
    (select id from sets where short_name = 'dom'),
    '224',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nature''s Spiral'),
    (select id from sets where short_name = 'dom'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raff Capashen, Ship''s Mage'),
    (select id from sets where short_name = 'dom'),
    '202',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fiery Intervention'),
    (select id from sets where short_name = 'dom'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sorcerer''s Wand'),
    (select id from sets where short_name = 'dom'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Navigator''s Compass'),
    (select id from sets where short_name = 'dom'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sulfur Falls'),
    (select id from sets where short_name = 'dom'),
    '247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cloudreader Sphinx'),
    (select id from sets where short_name = 'dom'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Benalish Marshal'),
    (select id from sets where short_name = 'dom'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Josu Vess, Lich Knight'),
    (select id from sets where short_name = 'dom'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Two-Headed Giant'),
    (select id from sets where short_name = 'dom'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Bargain'),
    (select id from sets where short_name = 'dom'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shield of the Realm'),
    (select id from sets where short_name = 'dom'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thran Temporal Gateway'),
    (select id from sets where short_name = 'dom'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fight with Fire'),
    (select id from sets where short_name = 'dom'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Benalish Honor Guard'),
    (select id from sets where short_name = 'dom'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Amaranthine Wall'),
    (select id from sets where short_name = 'dom'),
    '210',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jhoira, Weatherlight Captain'),
    (select id from sets where short_name = 'dom'),
    '197',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Excavation Elephant'),
    (select id from sets where short_name = 'dom'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relic Runner'),
    (select id from sets where short_name = 'dom'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howling Golem'),
    (select id from sets where short_name = 'dom'),
    '218',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skizzik'),
    (select id from sets where short_name = 'dom'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lich''s Mastery'),
    (select id from sets where short_name = 'dom'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Call the Cavalry'),
    (select id from sets where short_name = 'dom'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Timber Gorge'),
    (select id from sets where short_name = 'dom'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gift of Growth'),
    (select id from sets where short_name = 'dom'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karn''s Temporal Sundering'),
    (select id from sets where short_name = 'dom'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gideon''s Reproach'),
    (select id from sets where short_name = 'dom'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of Grace'),
    (select id from sets where short_name = 'dom'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Protector'),
    (select id from sets where short_name = 'dom'),
    '162',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sentinel of the Pearl Trident'),
    (select id from sets where short_name = 'dom'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Scriptures'),
    (select id from sets where short_name = 'dom'),
    '100',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rampaging Cyclops'),
    (select id from sets where short_name = 'dom'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Invoke the Divine'),
    (select id from sets where short_name = 'dom'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primevals'' Glorious Rebirth'),
    (select id from sets where short_name = 'dom'),
    '201',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Disciple'),
    (select id from sets where short_name = 'dom'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blackblade Reforged'),
    (select id from sets where short_name = 'dom'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Befuddle'),
    (select id from sets where short_name = 'dom'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of New Benalia'),
    (select id from sets where short_name = 'dom'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naban, Dean of Iteration'),
    (select id from sets where short_name = 'dom'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Chainwhirler'),
    (select id from sets where short_name = 'dom'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sergeant-at-Arms'),
    (select id from sets where short_name = 'dom'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seal Away'),
    (select id from sets where short_name = 'dom'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'dom'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skittering Surveyor'),
    (select id from sets where short_name = 'dom'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tatyova, Benthic Druid'),
    (select id from sets where short_name = 'dom'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Awakening'),
    (select id from sets where short_name = 'dom'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lyra Dawnbringer'),
    (select id from sets where short_name = 'dom'),
    '26',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Homarid Explorer'),
    (select id from sets where short_name = 'dom'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Radiating Lightning'),
    (select id from sets where short_name = 'dom'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darigaaz Reincarnated'),
    (select id from sets where short_name = 'dom'),
    '193',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Karplusan Hound'),
    (select id from sets where short_name = 'dom'),
    '277',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drudge Sentinel'),
    (select id from sets where short_name = 'dom'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unwind'),
    (select id from sets where short_name = 'dom'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grand Warlord Radha'),
    (select id from sets where short_name = 'dom'),
    '195',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Memorial to Unity'),
    (select id from sets where short_name = 'dom'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thallid Omnivore'),
    (select id from sets where short_name = 'dom'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tiana, Ship''s Caretaker'),
    (select id from sets where short_name = 'dom'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Temporal Machinations'),
    (select id from sets where short_name = 'dom'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dom'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icy Manipulator'),
    (select id from sets where short_name = 'dom'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Board the Weatherlight'),
    (select id from sets where short_name = 'dom'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Isolated Chapel'),
    (select id from sets where short_name = 'dom'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'dom'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squee, the Immortal'),
    (select id from sets where short_name = 'dom'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fungal Infection'),
    (select id from sets where short_name = 'dom'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dauntless Bodyguard'),
    (select id from sets where short_name = 'dom'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Song of Freyalise'),
    (select id from sets where short_name = 'dom'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Academy Journeymage'),
    (select id from sets where short_name = 'dom'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gilded Lotus'),
    (select id from sets where short_name = 'dom'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'dom'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'D''Avenant Trapper'),
    (select id from sets where short_name = 'dom'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Damping Sphere'),
    (select id from sets where short_name = 'dom'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'dom'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctum Spirit'),
    (select id from sets where short_name = 'dom'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vicious Offering'),
    (select id from sets where short_name = 'dom'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stronghold Confessor'),
    (select id from sets where short_name = 'dom'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blink of an Eye'),
    (select id from sets where short_name = 'dom'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'dom'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garna, the Bloodflame'),
    (select id from sets where short_name = 'dom'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spore Swarm'),
    (select id from sets where short_name = 'dom'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Danitha Capashen, Paragon'),
    (select id from sets where short_name = 'dom'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Adeliz, the Cinder Wind'),
    (select id from sets where short_name = 'dom'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight of Malice'),
    (select id from sets where short_name = 'dom'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kwende, Pride of Femeref'),
    (select id from sets where short_name = 'dom'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Broken Bond'),
    (select id from sets where short_name = 'dom'),
    '157',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'dom'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Animus'),
    (select id from sets where short_name = 'dom'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grunn, the Lonely King'),
    (select id from sets where short_name = 'dom'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shanna, Sisay''s Legacy'),
    (select id from sets where short_name = 'dom'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Memorial to Genius'),
    (select id from sets where short_name = 'dom'),
    '243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woodland Cemetery'),
    (select id from sets where short_name = 'dom'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra, Bold Pyromancer'),
    (select id from sets where short_name = 'dom'),
    '275',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sparring Construct'),
    (select id from sets where short_name = 'dom'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wizard''s Retort'),
    (select id from sets where short_name = 'dom'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Guardians of Koilos'),
    (select id from sets where short_name = 'dom'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Antiquities War'),
    (select id from sets where short_name = 'dom'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arbor Armament'),
    (select id from sets where short_name = 'dom'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Sentry'),
    (select id from sets where short_name = 'dom'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kamahl''s Druidic Vow'),
    (select id from sets where short_name = 'dom'),
    '166',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yawgmoth''s Vile Offering'),
    (select id from sets where short_name = 'dom'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Druid'),
    (select id from sets where short_name = 'dom'),
    '167',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghitu Lavarunner'),
    (select id from sets where short_name = 'dom'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Niambi, Faithful Healer'),
    (select id from sets where short_name = 'dom'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Muldrotha, the Gravetide'),
    (select id from sets where short_name = 'dom'),
    '199',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Divest'),
    (select id from sets where short_name = 'dom'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Divination'),
    (select id from sets where short_name = 'dom'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Scout'),
    (select id from sets where short_name = 'dom'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Eldest Reborn'),
    (select id from sets where short_name = 'dom'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hallar, the Firefletcher'),
    (select id from sets where short_name = 'dom'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slinn Voda, the Rising Deep'),
    (select id from sets where short_name = 'dom'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Torgaar, Famine Incarnate'),
    (select id from sets where short_name = 'dom'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Short Sword'),
    (select id from sets where short_name = 'dom'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Precognition Field'),
    (select id from sets where short_name = 'dom'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cold-Water Snapper'),
    (select id from sets where short_name = 'dom'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mesa Unicorn'),
    (select id from sets where short_name = 'dom'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'History of Benalia'),
    (select id from sets where short_name = 'dom'),
    '21',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Corrosive Ooze'),
    (select id from sets where short_name = 'dom'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baird, Steward of Argive'),
    (select id from sets where short_name = 'dom'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cabal Evangel'),
    (select id from sets where short_name = 'dom'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blessing of Belzenlok'),
    (select id from sets where short_name = 'dom'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghitu Chronicler'),
    (select id from sets where short_name = 'dom'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Run Amok'),
    (select id from sets where short_name = 'dom'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weatherlight'),
    (select id from sets where short_name = 'dom'),
    '237',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Meandering River'),
    (select id from sets where short_name = 'dom'),
    '274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Powerstone Shard'),
    (select id from sets where short_name = 'dom'),
    '227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hinterland Harbor'),
    (select id from sets where short_name = 'dom'),
    '240',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tome'),
    (select id from sets where short_name = 'dom'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'In Bolas''s Clutches'),
    (select id from sets where short_name = 'dom'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thorn Elemental'),
    (select id from sets where short_name = 'dom'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Traxos, Scourge of Kroog'),
    (select id from sets where short_name = 'dom'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seismic Shift'),
    (select id from sets where short_name = 'dom'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mammoth Spider'),
    (select id from sets where short_name = 'dom'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shalai, Voice of Plenty'),
    (select id from sets where short_name = 'dom'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'dom'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dub'),
    (select id from sets where short_name = 'dom'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Final Parting'),
    (select id from sets where short_name = 'dom'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Artificer''s Assistant'),
    (select id from sets where short_name = 'dom'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pierce the Sky'),
    (select id from sets where short_name = 'dom'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Juggernaut'),
    (select id from sets where short_name = 'dom'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zhalfirin Void'),
    (select id from sets where short_name = 'dom'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clifftop Retreat'),
    (select id from sets where short_name = 'dom'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghitu Journeymage'),
    (select id from sets where short_name = 'dom'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fervent Strike'),
    (select id from sets where short_name = 'dom'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Haphazard Bombardment'),
    (select id from sets where short_name = 'dom'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Self-Replicator'),
    (select id from sets where short_name = 'dom'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arvad the Cursed'),
    (select id from sets where short_name = 'dom'),
    '191',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urgoros, the Empty One'),
    (select id from sets where short_name = 'dom'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slimefoot, the Stowaway'),
    (select id from sets where short_name = 'dom'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'dom'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teferi, Timebender'),
    (select id from sets where short_name = 'dom'),
    '270',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wild Onslaught'),
    (select id from sets where short_name = 'dom'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Memorial to Folly'),
    (select id from sets where short_name = 'dom'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keldon Overseer'),
    (select id from sets where short_name = 'dom'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time of Ice'),
    (select id from sets where short_name = 'dom'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keldon Raider'),
    (select id from sets where short_name = 'dom'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Outburst'),
    (select id from sets where short_name = 'dom'),
    '276',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Adventurous Impulse'),
    (select id from sets where short_name = 'dom'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chainer''s Torment'),
    (select id from sets where short_name = 'dom'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Siege-Gang Commander'),
    (select id from sets where short_name = 'dom'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyromantic Pilgrim'),
    (select id from sets where short_name = 'dom'),
    '278',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teshar, Ancestor''s Apostle'),
    (select id from sets where short_name = 'dom'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sage of Lat-Nam'),
    (select id from sets where short_name = 'dom'),
    '64',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Healing Grace'),
    (select id from sets where short_name = 'dom'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Syncopate'),
    (select id from sets where short_name = 'dom'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Windgrace Acolyte'),
    (select id from sets where short_name = 'dom'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Academy Drake'),
    (select id from sets where short_name = 'dom'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tragic Poet'),
    (select id from sets where short_name = 'dom'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frenzied Rage'),
    (select id from sets where short_name = 'dom'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jaya Ballard'),
    (select id from sets where short_name = 'dom'),
    '132',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Warcry Phoenix'),
    (select id from sets where short_name = 'dom'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demonlord Belzenlok'),
    (select id from sets where short_name = 'dom'),
    '86',
    'mythic'
) 
 on conflict do nothing;
