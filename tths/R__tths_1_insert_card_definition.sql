    insert into mtgcard(name) values ('Elspeth, Sun''s Champion Emblem') on conflict do nothing;
    insert into mtgcard(name) values ('Harpy') on conflict do nothing;
    insert into mtgcard(name) values ('Boar') on conflict do nothing;
    insert into mtgcard(name) values ('Soldier') on conflict do nothing;
    insert into mtgcard(name) values ('Cleric') on conflict do nothing;
    insert into mtgcard(name) values ('Satyr') on conflict do nothing;
    insert into mtgcard(name) values ('Bird') on conflict do nothing;
    insert into mtgcard(name) values ('Golem') on conflict do nothing;
    insert into mtgcard(name) values ('Elemental') on conflict do nothing;
