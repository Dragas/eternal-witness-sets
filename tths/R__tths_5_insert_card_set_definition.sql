insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Elspeth, Sun''s Champion Emblem'),
    (select id from sets where short_name = 'tths'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harpy'),
    (select id from sets where short_name = 'tths'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boar'),
    (select id from sets where short_name = 'tths'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tths'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tths'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tths'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cleric'),
    (select id from sets where short_name = 'tths'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Satyr'),
    (select id from sets where short_name = 'tths'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bird'),
    (select id from sets where short_name = 'tths'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golem'),
    (select id from sets where short_name = 'tths'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'tths'),
    '5',
    'common'
) 
 on conflict do nothing;
