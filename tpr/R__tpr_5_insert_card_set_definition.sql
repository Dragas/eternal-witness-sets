insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Dauthi Marauder'),
    (select id from sets where short_name = 'tpr'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shaman en-Kor'),
    (select id from sets where short_name = 'tpr'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shatter'),
    (select id from sets where short_name = 'tpr'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aluren'),
    (select id from sets where short_name = 'tpr'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Winged Sliver'),
    (select id from sets where short_name = 'tpr'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gallantry'),
    (select id from sets where short_name = 'tpr'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shadow Rift'),
    (select id from sets where short_name = 'tpr'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dream Prowler'),
    (select id from sets where short_name = 'tpr'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mogg Fanatic'),
    (select id from sets where short_name = 'tpr'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spike Feeder'),
    (select id from sets where short_name = 'tpr'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sliver Queen'),
    (select id from sets where short_name = 'tpr'),
    '211',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Victual Sliver'),
    (select id from sets where short_name = 'tpr'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mirri, Cat Warrior'),
    (select id from sets where short_name = 'tpr'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sandstone Warrior'),
    (select id from sets where short_name = 'tpr'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exalted Dragon'),
    (select id from sets where short_name = 'tpr'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'tpr'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spined Wurm'),
    (select id from sets where short_name = 'tpr'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grindstone'),
    (select id from sets where short_name = 'tpr'),
    '223',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Metallic Sliver'),
    (select id from sets where short_name = 'tpr'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forbid'),
    (select id from sets where short_name = 'tpr'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armor Sliver'),
    (select id from sets where short_name = 'tpr'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'tpr'),
    '263',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barbed Sliver'),
    (select id from sets where short_name = 'tpr'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dark Banishing'),
    (select id from sets where short_name = 'tpr'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thalakos Scout'),
    (select id from sets where short_name = 'tpr'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Killer Whale'),
    (select id from sets where short_name = 'tpr'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meditate'),
    (select id from sets where short_name = 'tpr'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vec Townships'),
    (select id from sets where short_name = 'tpr'),
    '247',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anoint'),
    (select id from sets where short_name = 'tpr'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fanning the Flames'),
    (select id from sets where short_name = 'tpr'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cursed Scroll'),
    (select id from sets where short_name = 'tpr'),
    '220',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Soltari Trooper'),
    (select id from sets where short_name = 'tpr'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'tpr'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avenging Angel'),
    (select id from sets where short_name = 'tpr'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thrull Surgeon'),
    (select id from sets where short_name = 'tpr'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'tpr'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Protector'),
    (select id from sets where short_name = 'tpr'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seething Anger'),
    (select id from sets where short_name = 'tpr'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stronghold Assassin'),
    (select id from sets where short_name = 'tpr'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flowstone Wyvern'),
    (select id from sets where short_name = 'tpr'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Legacy''s Allure'),
    (select id from sets where short_name = 'tpr'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jinxed Idol'),
    (select id from sets where short_name = 'tpr'),
    '224',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spell Blast'),
    (select id from sets where short_name = 'tpr'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kezzerdrix'),
    (select id from sets where short_name = 'tpr'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Revenant'),
    (select id from sets where short_name = 'tpr'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stalking Stones'),
    (select id from sets where short_name = 'tpr'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kor Chant'),
    (select id from sets where short_name = 'tpr'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bottle Gnomes'),
    (select id from sets where short_name = 'tpr'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conviction'),
    (select id from sets where short_name = 'tpr'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lowland Basilisk'),
    (select id from sets where short_name = 'tpr'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dream Halls'),
    (select id from sets where short_name = 'tpr'),
    '46',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lightning Blast'),
    (select id from sets where short_name = 'tpr'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Blossoms'),
    (select id from sets where short_name = 'tpr'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'tpr'),
    '250',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soltari Guerrillas'),
    (select id from sets where short_name = 'tpr'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Hulk'),
    (select id from sets where short_name = 'tpr'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rats of Rath'),
    (select id from sets where short_name = 'tpr'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rolling Thunder'),
    (select id from sets where short_name = 'tpr'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dismiss'),
    (select id from sets where short_name = 'tpr'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hibernation Sliver'),
    (select id from sets where short_name = 'tpr'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'tpr'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Searing Touch'),
    (select id from sets where short_name = 'tpr'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mnemonic Sliver'),
    (select id from sets where short_name = 'tpr'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Recycle'),
    (select id from sets where short_name = 'tpr'),
    '189',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fugue'),
    (select id from sets where short_name = 'tpr'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Standing Troops'),
    (select id from sets where short_name = 'tpr'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dauthi Jackal'),
    (select id from sets where short_name = 'tpr'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overrun'),
    (select id from sets where short_name = 'tpr'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reality Anchor'),
    (select id from sets where short_name = 'tpr'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Endangered Armodon'),
    (select id from sets where short_name = 'tpr'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dauthi Horror'),
    (select id from sets where short_name = 'tpr'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rootwalla'),
    (select id from sets where short_name = 'tpr'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spike Hatcher'),
    (select id from sets where short_name = 'tpr'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dungeon Shade'),
    (select id from sets where short_name = 'tpr'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'tpr'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scabland'),
    (select id from sets where short_name = 'tpr'),
    '243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aftershock'),
    (select id from sets where short_name = 'tpr'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hermit Druid'),
    (select id from sets where short_name = 'tpr'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Armored Pegasus'),
    (select id from sets where short_name = 'tpr'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soltari Priest'),
    (select id from sets where short_name = 'tpr'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pandemonium'),
    (select id from sets where short_name = 'tpr'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mulch'),
    (select id from sets where short_name = 'tpr'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wind Dancer'),
    (select id from sets where short_name = 'tpr'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Carnassid'),
    (select id from sets where short_name = 'tpr'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Fury'),
    (select id from sets where short_name = 'tpr'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crovax the Cursed'),
    (select id from sets where short_name = 'tpr'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hammerhead Shark'),
    (select id from sets where short_name = 'tpr'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nomads en-Kor'),
    (select id from sets where short_name = 'tpr'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kindle'),
    (select id from sets where short_name = 'tpr'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wayward Soul'),
    (select id from sets where short_name = 'tpr'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stun'),
    (select id from sets where short_name = 'tpr'),
    '163',
    'common'
) ,
(
    (select id from mtgcard where name = 'Canyon Wildcat'),
    (select id from sets where short_name = 'tpr'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Diffusion'),
    (select id from sets where short_name = 'tpr'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Survival of the Fittest'),
    (select id from sets where short_name = 'tpr'),
    '199',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'tpr'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thalakos Seer'),
    (select id from sets where short_name = 'tpr'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Humility'),
    (select id from sets where short_name = 'tpr'),
    '16',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'tpr'),
    '256',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whispers of the Muse'),
    (select id from sets where short_name = 'tpr'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Hounds'),
    (select id from sets where short_name = 'tpr'),
    '122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mawcor'),
    (select id from sets where short_name = 'tpr'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spined Sliver'),
    (select id from sets where short_name = 'tpr'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Screeching Harpy'),
    (select id from sets where short_name = 'tpr'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clot Sliver'),
    (select id from sets where short_name = 'tpr'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dracoplasm'),
    (select id from sets where short_name = 'tpr'),
    '208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sarcomancy'),
    (select id from sets where short_name = 'tpr'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'tpr'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Field of Souls'),
    (select id from sets where short_name = 'tpr'),
    '13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thopter Squadron'),
    (select id from sets where short_name = 'tpr'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shard Phoenix'),
    (select id from sets where short_name = 'tpr'),
    '158',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mana Leak'),
    (select id from sets where short_name = 'tpr'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coffin Queen'),
    (select id from sets where short_name = 'tpr'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = 'tpr'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Craven Giant'),
    (select id from sets where short_name = 'tpr'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selenia, Dark Angel'),
    (select id from sets where short_name = 'tpr'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death''s Duet'),
    (select id from sets where short_name = 'tpr'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mage il-Vec'),
    (select id from sets where short_name = 'tpr'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Verdigris'),
    (select id from sets where short_name = 'tpr'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Evincar''s Justice'),
    (select id from sets where short_name = 'tpr'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sea Monster'),
    (select id from sets where short_name = 'tpr'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wasteland'),
    (select id from sets where short_name = 'tpr'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'tpr'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Twitch'),
    (select id from sets where short_name = 'tpr'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krakilin'),
    (select id from sets where short_name = 'tpr'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spirit Mirror'),
    (select id from sets where short_name = 'tpr'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lowland Giant'),
    (select id from sets where short_name = 'tpr'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravedigger'),
    (select id from sets where short_name = 'tpr'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Furnace Brood'),
    (select id from sets where short_name = 'tpr'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paladin en-Vec'),
    (select id from sets where short_name = 'tpr'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Manabond'),
    (select id from sets where short_name = 'tpr'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lab Rats'),
    (select id from sets where short_name = 'tpr'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cataclysm'),
    (select id from sets where short_name = 'tpr'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'City of Traitors'),
    (select id from sets where short_name = 'tpr'),
    '237',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rootwater Depths'),
    (select id from sets where short_name = 'tpr'),
    '241',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angelic Blessing'),
    (select id from sets where short_name = 'tpr'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elven Rite'),
    (select id from sets where short_name = 'tpr'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pegasus Stampede'),
    (select id from sets where short_name = 'tpr'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orim, Samite Healer'),
    (select id from sets where short_name = 'tpr'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spellshock'),
    (select id from sets where short_name = 'tpr'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rootwater Hunter'),
    (select id from sets where short_name = 'tpr'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'tpr'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soltari Monk'),
    (select id from sets where short_name = 'tpr'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winds of Rath'),
    (select id from sets where short_name = 'tpr'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Repentance'),
    (select id from sets where short_name = 'tpr'),
    '25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'tpr'),
    '254',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'tpr'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Renegade Warlord'),
    (select id from sets where short_name = 'tpr'),
    '151',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mounted Archers'),
    (select id from sets where short_name = 'tpr'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gerrard''s Battle Cry'),
    (select id from sets where short_name = 'tpr'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wind Drake'),
    (select id from sets where short_name = 'tpr'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ephemeron'),
    (select id from sets where short_name = 'tpr'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'tpr'),
    '252',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thalakos Lowlands'),
    (select id from sets where short_name = 'tpr'),
    '246',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cursed Flesh'),
    (select id from sets where short_name = 'tpr'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silver Wyvern'),
    (select id from sets where short_name = 'tpr'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carnophage'),
    (select id from sets where short_name = 'tpr'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Master Decoy'),
    (select id from sets where short_name = 'tpr'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bandage'),
    (select id from sets where short_name = 'tpr'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emmessi Tome'),
    (select id from sets where short_name = 'tpr'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spike Breeder'),
    (select id from sets where short_name = 'tpr'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Commander Greven il-Vec'),
    (select id from sets where short_name = 'tpr'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'tpr'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coercion'),
    (select id from sets where short_name = 'tpr'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit en-Kor'),
    (select id from sets where short_name = 'tpr'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cinder Marsh'),
    (select id from sets where short_name = 'tpr'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mindless Automaton'),
    (select id from sets where short_name = 'tpr'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pacifism'),
    (select id from sets where short_name = 'tpr'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Legerdemain'),
    (select id from sets where short_name = 'tpr'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Salt Flats'),
    (select id from sets where short_name = 'tpr'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Essence'),
    (select id from sets where short_name = 'tpr'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anarchist'),
    (select id from sets where short_name = 'tpr'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Living Death'),
    (select id from sets where short_name = 'tpr'),
    '109',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Maniacal Rage'),
    (select id from sets where short_name = 'tpr'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oath of Druids'),
    (select id from sets where short_name = 'tpr'),
    '184',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goblin Bombardment'),
    (select id from sets where short_name = 'tpr'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cannibalize'),
    (select id from sets where short_name = 'tpr'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Provoke'),
    (select id from sets where short_name = 'tpr'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gaseous Form'),
    (select id from sets where short_name = 'tpr'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rathi Dragon'),
    (select id from sets where short_name = 'tpr'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harrow'),
    (select id from sets where short_name = 'tpr'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charging Paladin'),
    (select id from sets where short_name = 'tpr'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Starke of Rath'),
    (select id from sets where short_name = 'tpr'),
    '162',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sabertooth Wyvern'),
    (select id from sets where short_name = 'tpr'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Patchwork Gnomes'),
    (select id from sets where short_name = 'tpr'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spike Colony'),
    (select id from sets where short_name = 'tpr'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wall of Souls'),
    (select id from sets where short_name = 'tpr'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rootbreaker Wurm'),
    (select id from sets where short_name = 'tpr'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vhati il-Dal'),
    (select id from sets where short_name = 'tpr'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'tpr'),
    '258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volrath''s Laboratory'),
    (select id from sets where short_name = 'tpr'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corpse Dance'),
    (select id from sets where short_name = 'tpr'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scrivener'),
    (select id from sets where short_name = 'tpr'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thalakos Drifters'),
    (select id from sets where short_name = 'tpr'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spinal Graft'),
    (select id from sets where short_name = 'tpr'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Pits of Rath'),
    (select id from sets where short_name = 'tpr'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'tpr'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deadshot'),
    (select id from sets where short_name = 'tpr'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Warrior en-Kor'),
    (select id from sets where short_name = 'tpr'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Canopy Spider'),
    (select id from sets where short_name = 'tpr'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pine Barrens'),
    (select id from sets where short_name = 'tpr'),
    '240',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Intuition'),
    (select id from sets where short_name = 'tpr'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soltari Champion'),
    (select id from sets where short_name = 'tpr'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smite'),
    (select id from sets where short_name = 'tpr'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Troll'),
    (select id from sets where short_name = 'tpr'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Recurring Nightmare'),
    (select id from sets where short_name = 'tpr'),
    '113',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Trained Armodon'),
    (select id from sets where short_name = 'tpr'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shackles'),
    (select id from sets where short_name = 'tpr'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'tpr'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horned Sliver'),
    (select id from sets where short_name = 'tpr'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Staunch Defenders'),
    (select id from sets where short_name = 'tpr'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Forest'),
    (select id from sets where short_name = 'tpr'),
    '244',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Verdant Force'),
    (select id from sets where short_name = 'tpr'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wood Sage'),
    (select id from sets where short_name = 'tpr'),
    '216',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flowstone Mauler'),
    (select id from sets where short_name = 'tpr'),
    '133',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mox Diamond'),
    (select id from sets where short_name = 'tpr'),
    '228',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mogg Infestation'),
    (select id from sets where short_name = 'tpr'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyshaper'),
    (select id from sets where short_name = 'tpr'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Time Ebb'),
    (select id from sets where short_name = 'tpr'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Verdant Touch'),
    (select id from sets where short_name = 'tpr'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dauthi Slayer'),
    (select id from sets where short_name = 'tpr'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coiled Tinviper'),
    (select id from sets where short_name = 'tpr'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mogg Conscripts'),
    (select id from sets where short_name = 'tpr'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volrath''s Curse'),
    (select id from sets where short_name = 'tpr'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fighting Drake'),
    (select id from sets where short_name = 'tpr'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'tpr'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Elf'),
    (select id from sets where short_name = 'tpr'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tranquility'),
    (select id from sets where short_name = 'tpr'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mogg Flunkies'),
    (select id from sets where short_name = 'tpr'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Acidic Sliver'),
    (select id from sets where short_name = 'tpr'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sift'),
    (select id from sets where short_name = 'tpr'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crystalline Sliver'),
    (select id from sets where short_name = 'tpr'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necrologia'),
    (select id from sets where short_name = 'tpr'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'tpr'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erratic Portal'),
    (select id from sets where short_name = 'tpr'),
    '222',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dauthi Warlord'),
    (select id from sets where short_name = 'tpr'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Youthful Knight'),
    (select id from sets where short_name = 'tpr'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heartwood Giant'),
    (select id from sets where short_name = 'tpr'),
    '174',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coat of Arms'),
    (select id from sets where short_name = 'tpr'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serpent Warrior'),
    (select id from sets where short_name = 'tpr'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maze of Shadows'),
    (select id from sets where short_name = 'tpr'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magmasaur'),
    (select id from sets where short_name = 'tpr'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Needle Storm'),
    (select id from sets where short_name = 'tpr'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shadowstorm'),
    (select id from sets where short_name = 'tpr'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyshroud Vampire'),
    (select id from sets where short_name = 'tpr'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tradewind Rider'),
    (select id from sets where short_name = 'tpr'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mogg Maniac'),
    (select id from sets where short_name = 'tpr'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crashing Boars'),
    (select id from sets where short_name = 'tpr'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reanimate'),
    (select id from sets where short_name = 'tpr'),
    '112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'tpr'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ogre Shaman'),
    (select id from sets where short_name = 'tpr'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Time Warp'),
    (select id from sets where short_name = 'tpr'),
    '74',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Flowstone Blade'),
    (select id from sets where short_name = 'tpr'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heartwood Dryad'),
    (select id from sets where short_name = 'tpr'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Stroke'),
    (select id from sets where short_name = 'tpr'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mogg Hollows'),
    (select id from sets where short_name = 'tpr'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diabolic Edict'),
    (select id from sets where short_name = 'tpr'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Capsize'),
    (select id from sets where short_name = 'tpr'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Muscle Sliver'),
    (select id from sets where short_name = 'tpr'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volrath''s Stronghold'),
    (select id from sets where short_name = 'tpr'),
    '248',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Flame Wave'),
    (select id from sets where short_name = 'tpr'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Telethopter'),
    (select id from sets where short_name = 'tpr'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Caldera Lake'),
    (select id from sets where short_name = 'tpr'),
    '235',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lotus Petal'),
    (select id from sets where short_name = 'tpr'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Horned Turtle'),
    (select id from sets where short_name = 'tpr'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curiosity'),
    (select id from sets where short_name = 'tpr'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soltari Lancer'),
    (select id from sets where short_name = 'tpr'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'tpr'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk Looter'),
    (select id from sets where short_name = 'tpr'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spitting Hydra'),
    (select id from sets where short_name = 'tpr'),
    '161',
    'rare'
) 
 on conflict do nothing;
