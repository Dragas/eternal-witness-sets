    insert into mtgcard(name) values ('Knight of New Alara') on conflict do nothing;
    insert into mtgcard(name) values ('Figure of Destiny') on conflict do nothing;
    insert into mtgcard(name) values ('Earwig Squad') on conflict do nothing;
    insert into mtgcard(name) values ('Obelisk of Alara') on conflict do nothing;
    insert into mtgcard(name) values ('Magister of Worth') on conflict do nothing;
    insert into mtgcard(name) values ('Vexing Shusher') on conflict do nothing;
