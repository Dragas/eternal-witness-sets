insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Knight of New Alara'),
    (select id from sets where short_name = 'plpa'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Figure of Destiny'),
    (select id from sets where short_name = 'plpa'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Earwig Squad'),
    (select id from sets where short_name = 'plpa'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Alara'),
    (select id from sets where short_name = 'plpa'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magister of Worth'),
    (select id from sets where short_name = 'plpa'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vexing Shusher'),
    (select id from sets where short_name = 'plpa'),
    '2',
    'rare'
) 
 on conflict do nothing;
