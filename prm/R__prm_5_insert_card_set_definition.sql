insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Zodiac Rabbit'),
    (select id from sets where short_name = 'prm'),
    '35086',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zodiac Ox'),
    (select id from sets where short_name = 'prm'),
    '35092',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lake of the Dead'),
    (select id from sets where short_name = 'prm'),
    '23954',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Underworld Dreams'),
    (select id from sets where short_name = 'prm'),
    '35970',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Niv-Mizzet, the Firemind'),
    (select id from sets where short_name = 'prm'),
    '32583',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seeker of the Way'),
    (select id from sets where short_name = 'prm'),
    '57576',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jokulhaups'),
    (select id from sets where short_name = 'prm'),
    '70942',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Soul Collector'),
    (select id from sets where short_name = 'prm'),
    '36268',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dictate of Kruphix'),
    (select id from sets where short_name = 'prm'),
    '52326',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arbiter of the Ideal'),
    (select id from sets where short_name = 'prm'),
    '51912',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Necromaster Dragon'),
    (select id from sets where short_name = 'prm'),
    '57000',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Black Knight'),
    (select id from sets where short_name = 'prm'),
    '35922',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'prm'),
    '73632',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Superion'),
    (select id from sets where short_name = 'prm'),
    '40078',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Searing Spear'),
    (select id from sets where short_name = 'prm'),
    '48003',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lowland Oaf'),
    (select id from sets where short_name = 'prm'),
    '62415',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sun Titan'),
    (select id from sets where short_name = 'prm'),
    '37596',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mulldrifter'),
    (select id from sets where short_name = 'prm'),
    '36190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Celestine Reef'),
    (select id from sets where short_name = 'prm'),
    '44348',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone-Tongue Basilisk'),
    (select id from sets where short_name = 'prm'),
    '36238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Capture of Jingzhou'),
    (select id from sets where short_name = 'prm'),
    '64420',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'prm'),
    '40054',
    'common'
) ,
(
    (select id from mtgcard where name = 'White Knight'),
    (select id from sets where short_name = 'prm'),
    '35950',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pillage'),
    (select id from sets where short_name = 'prm'),
    '35994',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Auramancer'),
    (select id from sets where short_name = 'prm'),
    '41644',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'prm'),
    '277',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fathom Mage'),
    (select id from sets where short_name = 'prm'),
    '48001',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Devil''s Play'),
    (select id from sets where short_name = 'prm'),
    '42868',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elves of Deep Shadow'),
    (select id from sets where short_name = 'prm'),
    '35974',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glacial Ray'),
    (select id from sets where short_name = 'prm'),
    '36286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zodiac Dog'),
    (select id from sets where short_name = 'prm'),
    '35102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armadillo Cloak'),
    (select id from sets where short_name = 'prm'),
    '31455',
    'common'
) ,
(
    (select id from mtgcard where name = 'Morphling'),
    (select id from sets where short_name = 'prm'),
    '26982',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bramblewood Paragon'),
    (select id from sets where short_name = 'prm'),
    '31423',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Baron Sengir'),
    (select id from sets where short_name = 'prm'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cast Down'),
    (select id from sets where short_name = 'prm'),
    '68049',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oona''s Blackguard'),
    (select id from sets where short_name = 'prm'),
    '32535',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wood Elves'),
    (select id from sets where short_name = 'prm'),
    '62527',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dread Return'),
    (select id from sets where short_name = 'prm'),
    '60482',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azusa, Lost but Seeking'),
    (select id from sets where short_name = 'prm'),
    '62505',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystical Tutor'),
    (select id from sets where short_name = 'prm'),
    '70926',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bident of Thassa'),
    (select id from sets where short_name = 'prm'),
    '50114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Blessing'),
    (select id from sets where short_name = 'prm'),
    '36026',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glissa, the Traitor'),
    (select id from sets where short_name = 'prm'),
    '39618',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'prm'),
    '73628',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glory'),
    (select id from sets where short_name = 'prm'),
    '36248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tawnos''s Coffin'),
    (select id from sets where short_name = 'prm'),
    '23946',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tibalt, Rakish Instigator'),
    (select id from sets where short_name = 'prm'),
    '72247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Taiga'),
    (select id from sets where short_name = 'prm'),
    '43618',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'prm'),
    '32557',
    'common'
) ,
(
    (select id from mtgcard where name = 'Resurrection'),
    (select id from sets where short_name = 'prm'),
    '36136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volcanic Fallout'),
    (select id from sets where short_name = 'prm'),
    '43570',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'prm'),
    '261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'prm'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fatal Push'),
    (select id from sets where short_name = 'prm'),
    '64997',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gorilla Shaman'),
    (select id from sets where short_name = 'prm'),
    '61060',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gate Colossus'),
    (select id from sets where short_name = 'prm'),
    '71592',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hermit Druid'),
    (select id from sets where short_name = 'prm'),
    '36080',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Korlash, Heir to Blackblade'),
    (select id from sets where short_name = 'prm'),
    '31961',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pia and Kiran Nalaar'),
    (select id from sets where short_name = 'prm'),
    '57594',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'prm'),
    '58261',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk''s Horde'),
    (select id from sets where short_name = 'prm'),
    '41636',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exquisite Firecraft'),
    (select id from sets where short_name = 'prm'),
    '62503',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Rack'),
    (select id from sets where short_name = 'prm'),
    '62385',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hixus, Prison Warden'),
    (select id from sets where short_name = 'prm'),
    '57584',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grim Monolith'),
    (select id from sets where short_name = 'prm'),
    '61567',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phytotitan'),
    (select id from sets where short_name = 'prm'),
    '53830',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Living Death'),
    (select id from sets where short_name = 'prm'),
    '36042',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burning Wish'),
    (select id from sets where short_name = 'prm'),
    '43542',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Roots'),
    (select id from sets where short_name = 'prm'),
    '35114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'prm'),
    '275',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opt'),
    (select id from sets where short_name = 'prm'),
    '68047',
    'common'
) ,
(
    (select id from mtgcard where name = 'Transmute Artifact'),
    (select id from sets where short_name = 'prm'),
    '65644',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Path to Exile'),
    (select id from sets where short_name = 'prm'),
    '77957',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reanimate'),
    (select id from sets where short_name = 'prm'),
    '36052',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scaleguard Sentinels'),
    (select id from sets where short_name = 'prm'),
    '55882',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mirrored Depths'),
    (select id from sets where short_name = 'prm'),
    '44344',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'prm'),
    '35944',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wee Dragonauts'),
    (select id from sets where short_name = 'prm'),
    '35174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Siege Rhino'),
    (select id from sets where short_name = 'prm'),
    '57602',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wren''s Run Vanquisher'),
    (select id from sets where short_name = 'prm'),
    '36138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kiora''s Follower'),
    (select id from sets where short_name = 'prm'),
    '51926',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burst Lightning'),
    (select id from sets where short_name = 'prm'),
    '37610',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zuran Orb'),
    (select id from sets where short_name = 'prm'),
    '225',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ojutai''s Command'),
    (select id from sets where short_name = 'prm'),
    '55878',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inkmoth Nexus'),
    (select id from sets where short_name = 'prm'),
    '62999',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thran Quarry'),
    (select id from sets where short_name = 'prm'),
    '36128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karador, Ghost Chieftain'),
    (select id from sets where short_name = 'prm'),
    '51534',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'prm'),
    '31485',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leatherback Baloth'),
    (select id from sets where short_name = 'prm'),
    '43576',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unmake'),
    (select id from sets where short_name = 'prm'),
    '32551',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'prm'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Tower'),
    (select id from sets where short_name = 'prm'),
    '69266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blue Elemental Blast'),
    (select id from sets where short_name = 'prm'),
    '70922',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Corrupt'),
    (select id from sets where short_name = 'prm'),
    '35120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Suspension Field'),
    (select id from sets where short_name = 'prm'),
    '55876',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spatial Contortion'),
    (select id from sets where short_name = 'prm'),
    '61555',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eternal Witness'),
    (select id from sets where short_name = 'prm'),
    '31431',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Liliana, Dreadhorde General'),
    (select id from sets where short_name = 'prm'),
    '72283',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dimir Signet'),
    (select id from sets where short_name = 'prm'),
    '46924',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gideon Blackblade'),
    (select id from sets where short_name = 'prm'),
    '72285',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jaya, Venerated Firemage'),
    (select id from sets where short_name = 'prm'),
    '72243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abzan Beastmaster'),
    (select id from sets where short_name = 'prm'),
    '55888',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Imperious Perfect'),
    (select id from sets where short_name = 'prm'),
    '31409',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Xathrid Necromancer'),
    (select id from sets where short_name = 'prm'),
    '52312',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steam Vents'),
    (select id from sets where short_name = 'prm'),
    '72307',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Entomb'),
    (select id from sets where short_name = 'prm'),
    '51532',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aisling Leprechaun'),
    (select id from sets where short_name = 'prm'),
    '35050',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skirk Marauder'),
    (select id from sets where short_name = 'prm'),
    '36260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wren''s Run Packmaster'),
    (select id from sets where short_name = 'prm'),
    '31965',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Umezawa''s Jitte'),
    (select id from sets where short_name = 'prm'),
    '36210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selesnya Signet'),
    (select id from sets where short_name = 'prm'),
    '62437',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karametra''s Acolyte'),
    (select id from sets where short_name = 'prm'),
    '50118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hand of Justice'),
    (select id from sets where short_name = 'prm'),
    '223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Impulse'),
    (select id from sets where short_name = 'prm'),
    '36006',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carnival Hellsteed'),
    (select id from sets where short_name = 'prm'),
    '46885',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sandsteppe Citadel'),
    (select id from sets where short_name = 'prm'),
    '57606',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curse of the Bloody Tome'),
    (select id from sets where short_name = 'prm'),
    '42870',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quirion Ranger'),
    (select id from sets where short_name = 'prm'),
    '62443',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = 'prm'),
    '36232',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Survival of the Fittest'),
    (select id from sets where short_name = 'prm'),
    '43544',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Batterskull'),
    (select id from sets where short_name = 'prm'),
    '51930',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shadowmage Infiltrator'),
    (select id from sets where short_name = 'prm'),
    '55314',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vivien, Champion of the Wilds'),
    (select id from sets where short_name = 'prm'),
    '72279',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rest in Peace'),
    (select id from sets where short_name = 'prm'),
    '77943',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tamiyo, Collector of Tales'),
    (select id from sets where short_name = 'prm'),
    '72273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shrapnel Blast'),
    (select id from sets where short_name = 'prm'),
    '31443',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'prm'),
    '31983',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'prm'),
    '36321',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Contagion Clasp'),
    (select id from sets where short_name = 'prm'),
    '42258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Infest'),
    (select id from sets where short_name = 'prm'),
    '43568',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Emrakul, the Aeons Torn'),
    (select id from sets where short_name = 'prm'),
    '36873',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Elite Inquisitor'),
    (select id from sets where short_name = 'prm'),
    '42880',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Scrying'),
    (select id from sets where short_name = 'prm'),
    '60476',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Show and Tell'),
    (select id from sets where short_name = 'prm'),
    '46930',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'prm'),
    '31977',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu'),
    (select id from sets where short_name = 'prm'),
    '31459',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Accumulated Knowledge'),
    (select id from sets where short_name = 'prm'),
    '36182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carnophage'),
    (select id from sets where short_name = 'prm'),
    '36100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thalia, Heretic Cathar'),
    (select id from sets where short_name = 'prm'),
    '61545',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oracle''s Vault'),
    (select id from sets where short_name = 'prm'),
    '64434',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moonsilver Spear'),
    (select id from sets where short_name = 'prm'),
    '44311',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uktabi Orangutan'),
    (select id from sets where short_name = 'prm'),
    '36016',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stoke the Flames'),
    (select id from sets where short_name = 'prm'),
    '54553',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kaya, Bane of the Dead'),
    (select id from sets where short_name = 'prm'),
    '72229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Engineered Plague'),
    (select id from sets where short_name = 'prm'),
    '32543',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drowner of Hope'),
    (select id from sets where short_name = 'prm'),
    '58245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Warchief'),
    (select id from sets where short_name = 'prm'),
    '32577',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Peace Strider'),
    (select id from sets where short_name = 'prm'),
    '39019',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Melek, Izzet Paragon'),
    (select id from sets where short_name = 'prm'),
    '48578',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Ranger'),
    (select id from sets where short_name = 'prm'),
    '37592',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ugin''s Construct'),
    (select id from sets where short_name = 'prm'),
    '55761',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sign in Blood'),
    (select id from sets where short_name = 'prm'),
    '36226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cenn''s Tactician'),
    (select id from sets where short_name = 'prm'),
    '32541',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghor-Clan Rampager'),
    (select id from sets where short_name = 'prm'),
    '50110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mox Ruby'),
    (select id from sets where short_name = 'prm'),
    '46952',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Izzet Signet'),
    (select id from sets where short_name = 'prm'),
    '62387',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Adorned Pouncer'),
    (select id from sets where short_name = 'prm'),
    '64993',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mastery of the Unseen'),
    (select id from sets where short_name = 'prm'),
    '55705',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'prm'),
    '53879',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crop Rotation'),
    (select id from sets where short_name = 'prm'),
    '62521',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overbeing of Myth'),
    (select id from sets where short_name = 'prm'),
    '31967',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Day of Judgment'),
    (select id from sets where short_name = 'prm'),
    '36220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Crypt'),
    (select id from sets where short_name = 'prm'),
    '72301',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forgestoker Dragon'),
    (select id from sets where short_name = 'prm'),
    '51920',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cultivator of Blades'),
    (select id from sets where short_name = 'prm'),
    '62219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beanstalk Giant // Fertile Footsteps'),
    (select id from sets where short_name = 'prm'),
    '78786',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Windseeker Centaur'),
    (select id from sets where short_name = 'prm'),
    '35966',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hall of Triumph'),
    (select id from sets where short_name = 'prm'),
    '52340',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'prm'),
    '257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forbidden Orchard'),
    (select id from sets where short_name = 'prm'),
    '62409',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hordeling Outburst'),
    (select id from sets where short_name = 'prm'),
    '55783',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rubblehulk'),
    (select id from sets where short_name = 'prm'),
    '47977',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Grudge'),
    (select id from sets where short_name = 'prm'),
    '43590',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reckless Wurm'),
    (select id from sets where short_name = 'prm'),
    '35990',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'prm'),
    '35824',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trostani''s Summoner'),
    (select id from sets where short_name = 'prm'),
    '48574',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zodiac Rooster'),
    (select id from sets where short_name = 'prm'),
    '35082',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hellspark Elemental'),
    (select id from sets where short_name = 'prm'),
    '36172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tidehollow Sculler'),
    (select id from sets where short_name = 'prm'),
    '43560',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heir of the Wilds'),
    (select id from sets where short_name = 'prm'),
    '54555',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'prm'),
    '40100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conclave Naturalists'),
    (select id from sets where short_name = 'prm'),
    '57596',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mindwrack Demon'),
    (select id from sets where short_name = 'prm'),
    '60480',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wheel and Deal'),
    (select id from sets where short_name = 'prm'),
    '62411',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seal of Cleansing'),
    (select id from sets where short_name = 'prm'),
    '35132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Undiscovered Paradise'),
    (select id from sets where short_name = 'prm'),
    '61062',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grove of the Guardian'),
    (select id from sets where short_name = 'prm'),
    '46889',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hydroblast'),
    (select id from sets where short_name = 'prm'),
    '69979',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mortify'),
    (select id from sets where short_name = 'prm'),
    '35046',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Incorrigible Youths'),
    (select id from sets where short_name = 'prm'),
    '60470',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Circle of Flame'),
    (select id from sets where short_name = 'prm'),
    '41648',
    'common'
) ,
(
    (select id from mtgcard where name = 'Firebolt'),
    (select id from sets where short_name = 'prm'),
    '31469',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squadron Hawk'),
    (select id from sets where short_name = 'prm'),
    '39675',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'prm'),
    '40060',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archfiend of Ifnir'),
    (select id from sets where short_name = 'prm'),
    '64422',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ink-Eyes, Servant of Oni'),
    (select id from sets where short_name = 'prm'),
    '32013',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scrap Trawler'),
    (select id from sets where short_name = 'prm'),
    '62997',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'prm'),
    '53875',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fierce Invocation'),
    (select id from sets where short_name = 'prm'),
    '55743',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Rain'),
    (select id from sets where short_name = 'prm'),
    '35946',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'prm'),
    '35048',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spawn of Thraxes'),
    (select id from sets where short_name = 'prm'),
    '52320',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Steel Leaf Champion'),
    (select id from sets where short_name = 'prm'),
    '68045',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Explore'),
    (select id from sets where short_name = 'prm'),
    '59643',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shepherd of the Lost'),
    (select id from sets where short_name = 'prm'),
    '43550',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Specter'),
    (select id from sets where short_name = 'prm'),
    '35040',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Day of Judgment'),
    (select id from sets where short_name = 'prm'),
    '37873',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spike Feeder'),
    (select id from sets where short_name = 'prm'),
    '36082',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Revenant'),
    (select id from sets where short_name = 'prm'),
    '32192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treetop Village'),
    (select id from sets where short_name = 'prm'),
    '31403',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Morphling'),
    (select id from sets where short_name = 'prm'),
    '36847',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Okina Nightwatch'),
    (select id from sets where short_name = 'prm'),
    '36290',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arena'),
    (select id from sets where short_name = 'prm'),
    '35958',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Headless Horseman'),
    (select id from sets where short_name = 'prm'),
    '35052',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vendilion Clique'),
    (select id from sets where short_name = 'prm'),
    '39628',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Cradle'),
    (select id from sets where short_name = 'prm'),
    '59655',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nissa, Voice of Zendikar'),
    (select id from sets where short_name = 'prm'),
    '62511',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Two-Headed Dragon'),
    (select id from sets where short_name = 'prm'),
    '36156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arlinn, Voice of the Pack'),
    (select id from sets where short_name = 'prm'),
    '72239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slave of Bolas'),
    (select id from sets where short_name = 'prm'),
    '36180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Decimate'),
    (select id from sets where short_name = 'prm'),
    '62459',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Force of Nature'),
    (select id from sets where short_name = 'prm'),
    '36294',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vexing Shusher'),
    (select id from sets where short_name = 'prm'),
    '32533',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dismember'),
    (select id from sets where short_name = 'prm'),
    '43588',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gatekeeper of Malakir'),
    (select id from sets where short_name = 'prm'),
    '37853',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Willbender'),
    (select id from sets where short_name = 'prm'),
    '36258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Markov Dreadknight'),
    (select id from sets where short_name = 'prm'),
    '60456',
    'rare'
) ,
(
    (select id from mtgcard where name = 'True-Name Nemesis'),
    (select id from sets where short_name = 'prm'),
    '69268',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Decree of Justice'),
    (select id from sets where short_name = 'prm'),
    '59645',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reclamation Sage'),
    (select id from sets where short_name = 'prm'),
    '53836',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Foe-Razer Regent'),
    (select id from sets where short_name = 'prm'),
    '55890',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wood Elves'),
    (select id from sets where short_name = 'prm'),
    '36094',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greater Good'),
    (select id from sets where short_name = 'prm'),
    '51542',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mogg Fanatic'),
    (select id from sets where short_name = 'prm'),
    '31407',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mercurial Pretender'),
    (select id from sets where short_name = 'prm'),
    '53826',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maul Splicer'),
    (select id from sets where short_name = 'prm'),
    '40084',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treva, the Renewer'),
    (select id from sets where short_name = 'prm'),
    '36158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Killer // Chop Down'),
    (select id from sets where short_name = 'prm'),
    '78794',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eternal Dragon'),
    (select id from sets where short_name = 'prm'),
    '31387',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fated Intervention'),
    (select id from sets where short_name = 'prm'),
    '53818',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gravecrawler'),
    (select id from sets where short_name = 'prm'),
    '43517',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whirling Dervish'),
    (select id from sets where short_name = 'prm'),
    '35056',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Warp'),
    (select id from sets where short_name = 'prm'),
    '35982',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blight Herder'),
    (select id from sets where short_name = 'prm'),
    '58265',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'prm'),
    '299',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fortune''s Favor'),
    (select id from sets where short_name = 'prm'),
    '62987',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chandra, Torch of Defiance'),
    (select id from sets where short_name = 'prm'),
    '70934',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nighthowler'),
    (select id from sets where short_name = 'prm'),
    '50122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'prm'),
    '31991',
    'common'
) ,
(
    (select id from mtgcard where name = 'Honored Hierarch'),
    (select id from sets where short_name = 'prm'),
    '57598',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gideon of the Trials'),
    (select id from sets where short_name = 'prm'),
    '70940',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'prm'),
    '40056',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Phoenix'),
    (select id from sets where short_name = 'prm'),
    '41646',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boggart Ram-Gang'),
    (select id from sets where short_name = 'prm'),
    '31439',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Murder'),
    (select id from sets where short_name = 'prm'),
    '69256',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'prm'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis, the Hate-Twisted'),
    (select id from sets where short_name = 'prm'),
    '72251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mother of Runes'),
    (select id from sets where short_name = 'prm'),
    '36140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Contagion'),
    (select id from sets where short_name = 'prm'),
    '62485',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twilight Mire'),
    (select id from sets where short_name = 'prm'),
    '62455',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rampaging Baloths'),
    (select id from sets where short_name = 'prm'),
    '35140',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lightning Rift'),
    (select id from sets where short_name = 'prm'),
    '32571',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bituminous Blast'),
    (select id from sets where short_name = 'prm'),
    '36857',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dirtcowl Wurm'),
    (select id from sets where short_name = 'prm'),
    '32190',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dauthi Slayer'),
    (select id from sets where short_name = 'prm'),
    '36032',
    'common'
) ,
(
    (select id from mtgcard where name = 'Encroaching Wastes'),
    (select id from sets where short_name = 'prm'),
    '51932',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'prm'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mizzium Meddler'),
    (select id from sets where short_name = 'prm'),
    '57608',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flaxen Intruder // Welcome Home'),
    (select id from sets where short_name = 'prm'),
    '78814',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ajani Vengeant'),
    (select id from sets where short_name = 'prm'),
    '31969',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pyroblast'),
    (select id from sets where short_name = 'prm'),
    '69981',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tinder Wall'),
    (select id from sets where short_name = 'prm'),
    '62483',
    'common'
) ,
(
    (select id from mtgcard where name = 'Everflowing Chalice'),
    (select id from sets where short_name = 'prm'),
    '39620',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hive Stirrings'),
    (select id from sets where short_name = 'prm'),
    '49828',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mutavault'),
    (select id from sets where short_name = 'prm'),
    '31425',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'prm'),
    '48582',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oblivion Sower'),
    (select id from sets where short_name = 'prm'),
    '58893',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chalice of the Void'),
    (select id from sets where short_name = 'prm'),
    '69993',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruul Signet'),
    (select id from sets where short_name = 'prm'),
    '46914',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ajani, the Greathearted'),
    (select id from sets where short_name = 'prm'),
    '72295',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sultai Emissary'),
    (select id from sets where short_name = 'prm'),
    '55753',
    'common'
) ,
(
    (select id from mtgcard where name = 'Threads of Disloyalty'),
    (select id from sets where short_name = 'prm'),
    '70928',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mad Auntie'),
    (select id from sets where short_name = 'prm'),
    '35066',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'prm'),
    '32007',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lord of Shatterskull Pass'),
    (select id from sets where short_name = 'prm'),
    '36871',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Isochron Scepter'),
    (select id from sets where short_name = 'prm'),
    '35064',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shepherd of the Flock // Usher to Safety'),
    (select id from sets where short_name = 'prm'),
    '78802',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fireslinger'),
    (select id from sets where short_name = 'prm'),
    '36038',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kongming, "Sleeping Dragon"'),
    (select id from sets where short_name = 'prm'),
    '33442',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'prm'),
    '32011',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vindicate'),
    (select id from sets where short_name = 'prm'),
    '31391',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Samut, Tyrant Smasher'),
    (select id from sets where short_name = 'prm'),
    '72255',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Prognostic Sphinx'),
    (select id from sets where short_name = 'prm'),
    '53842',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nearheath Stalker'),
    (select id from sets where short_name = 'prm'),
    '43509',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcanis the Omnipotent'),
    (select id from sets where short_name = 'prm'),
    '54547',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Karn, Silver Golem'),
    (select id from sets where short_name = 'prm'),
    '36106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trophy Mage'),
    (select id from sets where short_name = 'prm'),
    '63003',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight of New Alara'),
    (select id from sets where short_name = 'prm'),
    '32555',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Mystic'),
    (select id from sets where short_name = 'prm'),
    '51538',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yixlid Jailer'),
    (select id from sets where short_name = 'prm'),
    '36068',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Breeding Pool'),
    (select id from sets where short_name = 'prm'),
    '72311',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruthless Ripper'),
    (select id from sets where short_name = 'prm'),
    '55767',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Inferno'),
    (select id from sets where short_name = 'prm'),
    '62439',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gifts Ungiven'),
    (select id from sets where short_name = 'prm'),
    '70924',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Wing Shards'),
    (select id from sets where short_name = 'prm'),
    '35156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balduvian Horde'),
    (select id from sets where short_name = 'prm'),
    '35992',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kalastria Highborn'),
    (select id from sets where short_name = 'prm'),
    '43552',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flame Javelin'),
    (select id from sets where short_name = 'prm'),
    '35122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Royal Assassin'),
    (select id from sets where short_name = 'prm'),
    '35160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rout'),
    (select id from sets where short_name = 'prm'),
    '62465',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Elder'),
    (select id from sets where short_name = 'prm'),
    '36200',
    'common'
) ,
(
    (select id from mtgcard where name = 'Diabolic Edict'),
    (select id from sets where short_name = 'prm'),
    '32527',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cancel'),
    (select id from sets where short_name = 'prm'),
    '36222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Whip of Erebos'),
    (select id from sets where short_name = 'prm'),
    '55733',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serrated Arrows'),
    (select id from sets where short_name = 'prm'),
    '35116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'prm'),
    '279',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brion Stoutarm'),
    (select id from sets where short_name = 'prm'),
    '36096',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Specter'),
    (select id from sets where short_name = 'prm'),
    '37602',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vampire Nighthawk'),
    (select id from sets where short_name = 'prm'),
    '36214',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moat'),
    (select id from sets where short_name = 'prm'),
    '46948',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kozilek, the Great Distortion'),
    (select id from sets where short_name = 'prm'),
    '62493',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Valorous Stance'),
    (select id from sets where short_name = 'prm'),
    '57582',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bonesplitter'),
    (select id from sets where short_name = 'prm'),
    '36272',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burgeoning'),
    (select id from sets where short_name = 'prm'),
    '62507',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Decree of Pain'),
    (select id from sets where short_name = 'prm'),
    '47985',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bonecrusher Giant // Stomp'),
    (select id from sets where short_name = 'prm'),
    '78810',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brazen Borrower // Petty Theft'),
    (select id from sets where short_name = 'prm'),
    '78826',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Abrade'),
    (select id from sets where short_name = 'prm'),
    '65001',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'prm'),
    '36224',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'prm'),
    '36184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dryad Militant'),
    (select id from sets where short_name = 'prm'),
    '46267',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Overtaker'),
    (select id from sets where short_name = 'prm'),
    '32202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'prm'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Crypt'),
    (select id from sets where short_name = 'prm'),
    '46908',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flaying Tendrils'),
    (select id from sets where short_name = 'prm'),
    '61561',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Marisi''s Twinclaws'),
    (select id from sets where short_name = 'prm'),
    '36186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Caryatid'),
    (select id from sets where short_name = 'prm'),
    '50120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'prm'),
    '265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grave Titan'),
    (select id from sets where short_name = 'prm'),
    '62403',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Zurgo Helmsmasher'),
    (select id from sets where short_name = 'prm'),
    '54549',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Voidslime'),
    (select id from sets where short_name = 'prm'),
    '36310',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Djinn Illuminatus'),
    (select id from sets where short_name = 'prm'),
    '32029',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lotus Cobra'),
    (select id from sets where short_name = 'prm'),
    '44321',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Steel Hellkite'),
    (select id from sets where short_name = 'prm'),
    '37859',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, Dragon-God'),
    (select id from sets where short_name = 'prm'),
    '72281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thran Golem'),
    (select id from sets where short_name = 'prm'),
    '62433',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fungal Shambler'),
    (select id from sets where short_name = 'prm'),
    '36236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lion''s Eye Diamond'),
    (select id from sets where short_name = 'prm'),
    '51936',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pain Seer'),
    (select id from sets where short_name = 'prm'),
    '51916',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zodiac Goat'),
    (select id from sets where short_name = 'prm'),
    '35098',
    'common'
) ,
(
    (select id from mtgcard where name = 'Triumph of Ferocity'),
    (select id from sets where short_name = 'prm'),
    '62475',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prophet of Kruphix'),
    (select id from sets where short_name = 'prm'),
    '53844',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oloro, Ageless Ascetic'),
    (select id from sets where short_name = 'prm'),
    '52316',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tradewind Rider'),
    (select id from sets where short_name = 'prm'),
    '36048',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lonesome Unicorn // Rider in Need'),
    (select id from sets where short_name = 'prm'),
    '78758',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Dragon'),
    (select id from sets where short_name = 'prm'),
    '32196',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temporal Manipulation'),
    (select id from sets where short_name = 'prm'),
    '55725',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rise from the Grave'),
    (select id from sets where short_name = 'prm'),
    '36300',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Accumulated Knowledge'),
    (select id from sets where short_name = 'prm'),
    '59641',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Alara'),
    (select id from sets where short_name = 'prm'),
    '31973',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lavinia, Azorius Renegade'),
    (select id from sets where short_name = 'prm'),
    '71588',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nahiri, Storm of Stone'),
    (select id from sets where short_name = 'prm'),
    '72291',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elusive Tormentor // Insidious Mist'),
    (select id from sets where short_name = 'prm'),
    '60472',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Power Plant'),
    (select id from sets where short_name = 'prm'),
    '69264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guttersnipe'),
    (select id from sets where short_name = 'prm'),
    '69248',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Megantic Sliver'),
    (select id from sets where short_name = 'prm'),
    '49830',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glistener Elf'),
    (select id from sets where short_name = 'prm'),
    '43067',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sudden Shock'),
    (select id from sets where short_name = 'prm'),
    '36316',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feldon of the Third Path'),
    (select id from sets where short_name = 'prm'),
    '55872',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Char'),
    (select id from sets where short_name = 'prm'),
    '36142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rhox War Monk'),
    (select id from sets where short_name = 'prm'),
    '40088',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gladehart Cavalry'),
    (select id from sets where short_name = 'prm'),
    '59661',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dark Ritual'),
    (select id from sets where short_name = 'prm'),
    '36168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Powder Keg'),
    (select id from sets where short_name = 'prm'),
    '36146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gauntlet of Might'),
    (select id from sets where short_name = 'prm'),
    '65656',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skittering Skirge'),
    (select id from sets where short_name = 'prm'),
    '36126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Path to Exile'),
    (select id from sets where short_name = 'prm'),
    '36174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'prm'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Signet'),
    (select id from sets where short_name = 'prm'),
    '46926',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wheel of Fortune'),
    (select id from sets where short_name = 'prm'),
    '36879',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scrubland'),
    (select id from sets where short_name = 'prm'),
    '43616',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Windfall'),
    (select id from sets where short_name = 'prm'),
    '62529',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hydra Broodmaster'),
    (select id from sets where short_name = 'prm'),
    '53850',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reciprocate'),
    (select id from sets where short_name = 'prm'),
    '35164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sterling Grove'),
    (select id from sets where short_name = 'prm'),
    '62467',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'prm'),
    '40040',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loyal Retainers'),
    (select id from sets where short_name = 'prm'),
    '47973',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ral, Storm Conduit'),
    (select id from sets where short_name = 'prm'),
    '72231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Bombardment'),
    (select id from sets where short_name = 'prm'),
    '36058',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oko, Thief of Crowns'),
    (select id from sets where short_name = 'prm'),
    '78858',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'prm'),
    '263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fling'),
    (select id from sets where short_name = 'prm'),
    '37590',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Summons'),
    (select id from sets where short_name = 'prm'),
    '55751',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Sun''s Zenith'),
    (select id from sets where short_name = 'prm'),
    '39634',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghost-Lit Raider'),
    (select id from sets where short_name = 'prm'),
    '36292',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grisly Salvage'),
    (select id from sets where short_name = 'prm'),
    '50108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magma Spray'),
    (select id from sets where short_name = 'prm'),
    '52338',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancestral Recall'),
    (select id from sets where short_name = 'prm'),
    '46954',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exclude'),
    (select id from sets where short_name = 'prm'),
    '62477',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blastoderm'),
    (select id from sets where short_name = 'prm'),
    '35134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Questing Phelddagrif'),
    (select id from sets where short_name = 'prm'),
    '36234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'prm'),
    '58255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana of the Veil'),
    (select id from sets where short_name = 'prm'),
    '55866',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mirari''s Wake'),
    (select id from sets where short_name = 'prm'),
    '31421',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elesh Norn, Grand Cenobite'),
    (select id from sets where short_name = 'prm'),
    '52306',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Skyship Stalker'),
    (select id from sets where short_name = 'prm'),
    '62215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oath of Druids'),
    (select id from sets where short_name = 'prm'),
    '36092',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hallowed Fountain'),
    (select id from sets where short_name = 'prm'),
    '72297',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nameless Inversion'),
    (select id from sets where short_name = 'prm'),
    '35126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Servo Exhibition'),
    (select id from sets where short_name = 'prm'),
    '64428',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'prm'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Greaves'),
    (select id from sets where short_name = 'prm'),
    '36194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tormod''s Crypt'),
    (select id from sets where short_name = 'prm'),
    '31427',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief Hydra'),
    (select id from sets where short_name = 'prm'),
    '58251',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Murderous Redcap'),
    (select id from sets where short_name = 'prm'),
    '36192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Library'),
    (select id from sets where short_name = 'prm'),
    '43636',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Electrolyze'),
    (select id from sets where short_name = 'prm'),
    '32581',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'prm'),
    '40064',
    'common'
) ,
(
    (select id from mtgcard where name = 'Serum Visions'),
    (select id from sets where short_name = 'prm'),
    '36280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'prm'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alhammarret, High Arbiter'),
    (select id from sets where short_name = 'prm'),
    '57588',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodlord of Vaasgoth'),
    (select id from sets where short_name = 'prm'),
    '41638',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Martyr''s Bond'),
    (select id from sets where short_name = 'prm'),
    '77945',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Razorfin Hunter'),
    (select id from sets where short_name = 'prm'),
    '62407',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coiling Oracle'),
    (select id from sets where short_name = 'prm'),
    '32587',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakdos Signet'),
    (select id from sets where short_name = 'prm'),
    '62389',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vengevine'),
    (select id from sets where short_name = 'prm'),
    '49844',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Spiritmonger'),
    (select id from sets where short_name = 'prm'),
    '35976',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noosegraf Mob'),
    (select id from sets where short_name = 'prm'),
    '61549',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wilt-Leaf Cavaliers'),
    (select id from sets where short_name = 'prm'),
    '31441',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rancor'),
    (select id from sets where short_name = 'prm'),
    '35112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Militia Bugler'),
    (select id from sets where short_name = 'prm'),
    '69260',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doom Blade'),
    (select id from sets where short_name = 'prm'),
    '37855',
    'common'
) ,
(
    (select id from mtgcard where name = 'Helm of Kaldra'),
    (select id from sets where short_name = 'prm'),
    '31989',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golem''s Heart'),
    (select id from sets where short_name = 'prm'),
    '37869',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tyrant of Valakut'),
    (select id from sets where short_name = 'prm'),
    '59657',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treasure Hunt'),
    (select id from sets where short_name = 'prm'),
    '39632',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireblast'),
    (select id from sets where short_name = 'prm'),
    '43598',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maelstrom Pulse'),
    (select id from sets where short_name = 'prm'),
    '37845',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Factory'),
    (select id from sets where short_name = 'prm'),
    '31493',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gruul Signet'),
    (select id from sets where short_name = 'prm'),
    '62401',
    'common'
) ,
(
    (select id from mtgcard where name = 'Metalworker'),
    (select id from sets where short_name = 'prm'),
    '46950',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merchant of the Vale // Haggle'),
    (select id from sets where short_name = 'prm'),
    '78778',
    'common'
) ,
(
    (select id from mtgcard where name = 'Food Chain'),
    (select id from sets where short_name = 'prm'),
    '69947',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Worship'),
    (select id from sets where short_name = 'prm'),
    '77955',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silverflame Squire // On Alert'),
    (select id from sets where short_name = 'prm'),
    '78834',
    'common'
) ,
(
    (select id from mtgcard where name = 'Defiant Bloodlord'),
    (select id from sets where short_name = 'prm'),
    '58247',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hostage Taker'),
    (select id from sets where short_name = 'prm'),
    '69987',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hero of Goma Fada'),
    (select id from sets where short_name = 'prm'),
    '58243',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arc Lightning'),
    (select id from sets where short_name = 'prm'),
    '55791',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Recollect'),
    (select id from sets where short_name = 'prm'),
    '35060',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yahenni''s Expertise'),
    (select id from sets where short_name = 'prm'),
    '62989',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temur War Shaman'),
    (select id from sets where short_name = 'prm'),
    '55715',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Stirrings'),
    (select id from sets where short_name = 'prm'),
    '70944',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rakdos Signet'),
    (select id from sets where short_name = 'prm'),
    '46898',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scythe Leopard'),
    (select id from sets where short_name = 'prm'),
    '58271',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stunted Growth'),
    (select id from sets where short_name = 'prm'),
    '62425',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Utter End'),
    (select id from sets where short_name = 'prm'),
    '54563',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dryad Militant'),
    (select id from sets where short_name = 'prm'),
    '46873',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chittering Rats'),
    (select id from sets where short_name = 'prm'),
    '62445',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace, Cunning Castaway'),
    (select id from sets where short_name = 'prm'),
    '70932',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ambition''s Cost'),
    (select id from sets where short_name = 'prm'),
    '62523',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orator of Ojutai'),
    (select id from sets where short_name = 'prm'),
    '57578',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'False Prophet'),
    (select id from sets where short_name = 'prm'),
    '62469',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brass''s Bounty'),
    (select id from sets where short_name = 'prm'),
    '66886',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nalathni Dragon'),
    (select id from sets where short_name = 'prm'),
    '35956',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spellskite'),
    (select id from sets where short_name = 'prm'),
    '62533',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Piper of the Swarm'),
    (select id from sets where short_name = 'prm'),
    '78862',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'prm'),
    '55880',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stasis'),
    (select id from sets where short_name = 'prm'),
    '65648',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Twist'),
    (select id from sets where short_name = 'prm'),
    '62427',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Remove Soul'),
    (select id from sets where short_name = 'prm'),
    '35130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wildfire Eternal'),
    (select id from sets where short_name = 'prm'),
    '64989',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Enlightened Tutor'),
    (select id from sets where short_name = 'prm'),
    '35998',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Argothian Enchantress'),
    (select id from sets where short_name = 'prm'),
    '36104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hero of Bladehold'),
    (select id from sets where short_name = 'prm'),
    '39646',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Diregraf Ghoul'),
    (select id from sets where short_name = 'prm'),
    '42882',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soltari Priest'),
    (select id from sets where short_name = 'prm'),
    '35054',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mother of Runes'),
    (select id from sets where short_name = 'prm'),
    '59649',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disdainful Stroke'),
    (select id from sets where short_name = 'prm'),
    '55785',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mayor of Avabruck // Howlpack Alpha'),
    (select id from sets where short_name = 'prm'),
    '42866',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Celestial Purge'),
    (select id from sets where short_name = 'prm'),
    '36859',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demon of Catastrophes'),
    (select id from sets where short_name = 'prm'),
    '69246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burning Sun''s Avatar'),
    (select id from sets where short_name = 'prm'),
    '65664',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'prm'),
    '31999',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bile Blight'),
    (select id from sets where short_name = 'prm'),
    '53838',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Formless Nurturing'),
    (select id from sets where short_name = 'prm'),
    '55747',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Broodmother'),
    (select id from sets where short_name = 'prm'),
    '32553',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Judge''s Familiar'),
    (select id from sets where short_name = 'prm'),
    '48194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'prm'),
    '32019',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'prm'),
    '58259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Pair'),
    (select id from sets where short_name = 'prm'),
    '62419',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mind Twist'),
    (select id from sets where short_name = 'prm'),
    '46920',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Xathrid Gorgon'),
    (select id from sets where short_name = 'prm'),
    '45205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = 'prm'),
    '35954',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chief Engineer'),
    (select id from sets where short_name = 'prm'),
    '53824',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Diplomats'),
    (select id from sets where short_name = 'prm'),
    '49832',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'prm'),
    '249',
    'common'
) ,
(
    (select id from mtgcard where name = 'Memory Lapse'),
    (select id from sets where short_name = 'prm'),
    '35986',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tempered Steel'),
    (select id from sets where short_name = 'prm'),
    '37865',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra, Pyromaster'),
    (select id from sets where short_name = 'prm'),
    '65007',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stairs to Infinity'),
    (select id from sets where short_name = 'prm'),
    '44350',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Izzet Charm'),
    (select id from sets where short_name = 'prm'),
    '48192',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grindstone'),
    (select id from sets where short_name = 'prm'),
    '62423',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mwonvuli Beast Tracker'),
    (select id from sets where short_name = 'prm'),
    '45211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Regrowth'),
    (select id from sets where short_name = 'prm'),
    '43612',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sorrow''s Path'),
    (select id from sets where short_name = 'prm'),
    '36889',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa, Vital Force'),
    (select id from sets where short_name = 'prm'),
    '70936',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Karn''s Bastion'),
    (select id from sets where short_name = 'prm'),
    '72223',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Challenger'),
    (select id from sets where short_name = 'prm'),
    '69953',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kor Duelist'),
    (select id from sets where short_name = 'prm'),
    '36212',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Guildmage'),
    (select id from sets where short_name = 'prm'),
    '32589',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Icatian Javelineers'),
    (select id from sets where short_name = 'prm'),
    '59639',
    'common'
) ,
(
    (select id from mtgcard where name = 'Queen of Ice // Rage of Winter'),
    (select id from sets where short_name = 'prm'),
    '78762',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aura of Silence'),
    (select id from sets where short_name = 'prm'),
    '36030',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'prm'),
    '70920',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goblin Warchief'),
    (select id from sets where short_name = 'prm'),
    '31477',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avalanche Tusker'),
    (select id from sets where short_name = 'prm'),
    '54514',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Radiant Flames'),
    (select id from sets where short_name = 'prm'),
    '58277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Future Sight'),
    (select id from sets where short_name = 'prm'),
    '43596',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harbinger of the Hunt'),
    (select id from sets where short_name = 'prm'),
    '57004',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Time Spiral'),
    (select id from sets where short_name = 'prm'),
    '62221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Signet'),
    (select id from sets where short_name = 'prm'),
    '46910',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Noose Constrictor'),
    (select id from sets where short_name = 'prm'),
    '62995',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Storm Entity'),
    (select id from sets where short_name = 'prm'),
    '36020',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Time Wipe'),
    (select id from sets where short_name = 'prm'),
    '72219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Windswept Heath'),
    (select id from sets where short_name = 'prm'),
    '43586',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Font of Fertility'),
    (select id from sets where short_name = 'prm'),
    '53848',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blackblade Reforged'),
    (select id from sets where short_name = 'prm'),
    '77949',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jackal Pup'),
    (select id from sets where short_name = 'prm'),
    '36056',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ajani Goldmane'),
    (select id from sets where short_name = 'prm'),
    '37847',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nissa''s Chosen'),
    (select id from sets where short_name = 'prm'),
    '36216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pouncing Jaguar'),
    (select id from sets where short_name = 'prm'),
    '36110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Maze of Ith'),
    (select id from sets where short_name = 'prm'),
    '36170',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tidings'),
    (select id from sets where short_name = 'prm'),
    '35076',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aether Hub'),
    (select id from sets where short_name = 'prm'),
    '64432',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Garruk, Cursed Huntsman'),
    (select id from sets where short_name = 'prm'),
    '78856',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dromoka, the Eternal'),
    (select id from sets where short_name = 'prm'),
    '57604',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Ringleader'),
    (select id from sets where short_name = 'prm'),
    '35144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oros, the Avenger'),
    (select id from sets where short_name = 'prm'),
    '31959',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wooded Foothills'),
    (select id from sets where short_name = 'prm'),
    '43584',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stroke of Genius'),
    (select id from sets where short_name = 'prm'),
    '36114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lingering Souls'),
    (select id from sets where short_name = 'prm'),
    '45201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'prm'),
    '31397',
    'common'
) ,
(
    (select id from mtgcard where name = 'Staunch Defenders'),
    (select id from sets where short_name = 'prm'),
    '36064',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kamahl, Pit Fighter'),
    (select id from sets where short_name = 'prm'),
    '36088',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reliquary Tower'),
    (select id from sets where short_name = 'prm'),
    '69250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lord of Atlantis'),
    (select id from sets where short_name = 'prm'),
    '35936',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dread Defiler'),
    (select id from sets where short_name = 'prm'),
    '59665',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cunning Wish'),
    (select id from sets where short_name = 'prm'),
    '31393',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'prm'),
    '269',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nissa, Worldwaker'),
    (select id from sets where short_name = 'prm'),
    '65005',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goblin Vandal'),
    (select id from sets where short_name = 'prm'),
    '62383',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exploration'),
    (select id from sets where short_name = 'prm'),
    '62463',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'prm'),
    '73634',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hoodwink'),
    (select id from sets where short_name = 'prm'),
    '62449',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quicksmith Rebel'),
    (select id from sets where short_name = 'prm'),
    '62993',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kird Ape'),
    (select id from sets where short_name = 'prm'),
    '31383',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'prm'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Control'),
    (select id from sets where short_name = 'prm'),
    '36298',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Curious Pair // Treats to Share'),
    (select id from sets where short_name = 'prm'),
    '78754',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bolas''s Citadel'),
    (select id from sets where short_name = 'prm'),
    '72317',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ravenous Baloth'),
    (select id from sets where short_name = 'prm'),
    '31389',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sewers of Estark'),
    (select id from sets where short_name = 'prm'),
    '35964',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword of Feast and Famine'),
    (select id from sets where short_name = 'prm'),
    '52304',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Red Elemental Blast'),
    (select id from sets where short_name = 'prm'),
    '43610',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selkie Hedge-Mage'),
    (select id from sets where short_name = 'prm'),
    '32549',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tropical Island'),
    (select id from sets where short_name = 'prm'),
    '43620',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'prm'),
    '32021',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fated Conflagration'),
    (select id from sets where short_name = 'prm'),
    '51922',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kothophed, Soul Hoarder'),
    (select id from sets where short_name = 'prm'),
    '57590',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Captain''s Hook'),
    (select id from sets where short_name = 'prm'),
    '66884',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hinder'),
    (select id from sets where short_name = 'prm'),
    '35172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sword of Feast and Famine'),
    (select id from sets where short_name = 'prm'),
    '59683',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'prm'),
    '32027',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravenous Bloodseeker'),
    (select id from sets where short_name = 'prm'),
    '60486',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Earwig Squad'),
    (select id from sets where short_name = 'prm'),
    '32539',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thalia, Guardian of Thraben'),
    (select id from sets where short_name = 'prm'),
    '55699',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Lumberjack'),
    (select id from sets where short_name = 'prm'),
    '43638',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cruel Edict'),
    (select id from sets where short_name = 'prm'),
    '35062',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghostly Prison'),
    (select id from sets where short_name = 'prm'),
    '43538',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = 'prm'),
    '31483',
    'common'
) ,
(
    (select id from mtgcard where name = 'Doran, the Siege Tower'),
    (select id from sets where short_name = 'prm'),
    '65658',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'prm'),
    '73626',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shardless Agent'),
    (select id from sets where short_name = 'prm'),
    '55697',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'prm'),
    '31979',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloudpost'),
    (select id from sets where short_name = 'prm'),
    '36869',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rathi Assassin'),
    (select id from sets where short_name = 'prm'),
    '32204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ainok Tracker'),
    (select id from sets where short_name = 'prm'),
    '55779',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mox Jet'),
    (select id from sets where short_name = 'prm'),
    '46894',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emmara, Soul of the Accord'),
    (select id from sets where short_name = 'prm'),
    '69951',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rimrock Knight // Boulder Rush'),
    (select id from sets where short_name = 'prm'),
    '78750',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emrakul, the Aeons Torn'),
    (select id from sets where short_name = 'prm'),
    '63001',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'prm'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blue Elemental Blast'),
    (select id from sets where short_name = 'prm'),
    '35924',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hedge Troll'),
    (select id from sets where short_name = 'prm'),
    '35968',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zameck Guildmage'),
    (select id from sets where short_name = 'prm'),
    '47987',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avenger of Zendikar'),
    (select id from sets where short_name = 'prm'),
    '58895',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Dreadnought'),
    (select id from sets where short_name = 'prm'),
    '43534',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smash to Smithereens'),
    (select id from sets where short_name = 'prm'),
    '59675',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bribery'),
    (select id from sets where short_name = 'prm'),
    '46928',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'prm'),
    '40068',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rhox'),
    (select id from sets where short_name = 'prm'),
    '36204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ravenous Demon // Archdemon of Greed'),
    (select id from sets where short_name = 'prm'),
    '43503',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Quirion Ranger'),
    (select id from sets where short_name = 'prm'),
    '36012',
    'common'
) ,
(
    (select id from mtgcard where name = 'Watchwolf'),
    (select id from sets where short_name = 'prm'),
    '36196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Metamorph'),
    (select id from sets where short_name = 'prm'),
    '40082',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'prm'),
    '31995',
    'common'
) ,
(
    (select id from mtgcard where name = 'Altar of the Brood'),
    (select id from sets where short_name = 'prm'),
    '55787',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Erratic Portal'),
    (select id from sets where short_name = 'prm'),
    '62421',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Icy Manipulator'),
    (select id from sets where short_name = 'prm'),
    '35042',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jori En, Ruin Diver'),
    (select id from sets where short_name = 'prm'),
    '59679',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Memnite'),
    (select id from sets where short_name = 'prm'),
    '37871',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Throne of Tarkir'),
    (select id from sets where short_name = 'prm'),
    '54565',
    'rare'
) ,
(
    (select id from mtgcard where name = 'River Boa'),
    (select id from sets where short_name = 'prm'),
    '36014',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Muscle Sliver'),
    (select id from sets where short_name = 'prm'),
    '36034',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reins of Power'),
    (select id from sets where short_name = 'prm'),
    '62517',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Demonic Tutor'),
    (select id from sets where short_name = 'prm'),
    '35074',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Wanderer'),
    (select id from sets where short_name = 'prm'),
    '72267',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of the False God'),
    (select id from sets where short_name = 'prm'),
    '59653',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Condemn'),
    (select id from sets where short_name = 'prm'),
    '31385',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'prm'),
    '35078',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombify'),
    (select id from sets where short_name = 'prm'),
    '35170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sage of the Inward Eye'),
    (select id from sets where short_name = 'prm'),
    '54518',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Outpost'),
    (select id from sets where short_name = 'prm'),
    '43628',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'prm'),
    '32003',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Piledriver'),
    (select id from sets where short_name = 'prm'),
    '31415',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fire // Ice'),
    (select id from sets where short_name = 'prm'),
    '31497',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abrupt Decay'),
    (select id from sets where short_name = 'prm'),
    '59681',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zombie Apocalypse'),
    (select id from sets where short_name = 'prm'),
    '43515',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Guide'),
    (select id from sets where short_name = 'prm'),
    '44309',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shock'),
    (select id from sets where short_name = 'prm'),
    '32531',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wave of Reckoning'),
    (select id from sets where short_name = 'prm'),
    '62515',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Helix'),
    (select id from sets where short_name = 'prm'),
    '31487',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Necropolis Fiend'),
    (select id from sets where short_name = 'prm'),
    '55729',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pristine Skywise'),
    (select id from sets where short_name = 'prm'),
    '57002',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silverblade Paladin'),
    (select id from sets where short_name = 'prm'),
    '44313',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gideon Jura'),
    (select id from sets where short_name = 'prm'),
    '77947',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kiora, the Crashing Wave'),
    (select id from sets where short_name = 'prm'),
    '59651',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Lake of the Dead'),
    (select id from sets where short_name = 'prm'),
    '43630',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Essence Extraction'),
    (select id from sets where short_name = 'prm'),
    '62211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silver Knight'),
    (select id from sets where short_name = 'prm'),
    '31479',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Comet Storm'),
    (select id from sets where short_name = 'prm'),
    '35822',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Duergar Hedge-Mage'),
    (select id from sets where short_name = 'prm'),
    '32547',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jiang Yanggu, Wildcrafter'),
    (select id from sets where short_name = 'prm'),
    '72275',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stunted Growth'),
    (select id from sets where short_name = 'prm'),
    '43642',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Ziggurat'),
    (select id from sets where short_name = 'prm'),
    '43562',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Elvish Aberration'),
    (select id from sets where short_name = 'prm'),
    '36266',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pierce Strider'),
    (select id from sets where short_name = 'prm'),
    '39017',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scavenging Ooze'),
    (select id from sets where short_name = 'prm'),
    '62451',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fact or Fiction'),
    (select id from sets where short_name = 'prm'),
    '31457',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doomwake Giant'),
    (select id from sets where short_name = 'prm'),
    '52322',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Door of Destinies'),
    (select id from sets where short_name = 'prm'),
    '31963',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Priest of Urabrask'),
    (select id from sets where short_name = 'prm'),
    '40080',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oakhame Ranger // Bring Back'),
    (select id from sets where short_name = 'prm'),
    '78738',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scourge of Fleets'),
    (select id from sets where short_name = 'prm'),
    '52324',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inferno Titan'),
    (select id from sets where short_name = 'prm'),
    '62405',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Krosan Tusker'),
    (select id from sets where short_name = 'prm'),
    '32575',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thirst for Knowledge'),
    (select id from sets where short_name = 'prm'),
    '31445',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'prm'),
    '70930',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dawnbringer Charioteers'),
    (select id from sets where short_name = 'prm'),
    '52332',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra, Fire Artisan'),
    (select id from sets where short_name = 'prm'),
    '72263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Walk the Plank'),
    (select id from sets where short_name = 'prm'),
    '65666',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Giant Badger'),
    (select id from sets where short_name = 'prm'),
    '35960',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boneyard Wurm'),
    (select id from sets where short_name = 'prm'),
    '42860',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tromokratis'),
    (select id from sets where short_name = 'prm'),
    '51914',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirran Crusader'),
    (select id from sets where short_name = 'prm'),
    '39636',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyroclasm'),
    (select id from sets where short_name = 'prm'),
    '35168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul of Theros'),
    (select id from sets where short_name = 'prm'),
    '62471',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shanna, Sisay''s Legacy'),
    (select id from sets where short_name = 'prm'),
    '68051',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smite the Monstrous'),
    (select id from sets where short_name = 'prm'),
    '55769',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avatar of Hope'),
    (select id from sets where short_name = 'prm'),
    '32206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Walking Ballista'),
    (select id from sets where short_name = 'prm'),
    '69991',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Noble Hierarch'),
    (select id from sets where short_name = 'prm'),
    '66894',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sliver Queen'),
    (select id from sets where short_name = 'prm'),
    '26980',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ulvenwald Observer'),
    (select id from sets where short_name = 'prm'),
    '61553',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Staggershock'),
    (select id from sets where short_name = 'prm'),
    '36851',
    'common'
) ,
(
    (select id from mtgcard where name = 'Davriel, Rogue Shadowmage'),
    (select id from sets where short_name = 'prm'),
    '72271',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Briber''s Purse'),
    (select id from sets where short_name = 'prm'),
    '55781',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodstained Mire'),
    (select id from sets where short_name = 'prm'),
    '36254',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warmonger'),
    (select id from sets where short_name = 'prm'),
    '36152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doom Blade'),
    (select id from sets where short_name = 'prm'),
    '62525',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cryptic Command'),
    (select id from sets where short_name = 'prm'),
    '31447',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pathrazer of Ulamog'),
    (select id from sets where short_name = 'prm'),
    '36861',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fae of Wishes // Granted'),
    (select id from sets where short_name = 'prm'),
    '78734',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shrine of Burning Rage'),
    (select id from sets where short_name = 'prm'),
    '40090',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'prm'),
    '32025',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prodigal Sorcerer'),
    (select id from sets where short_name = 'prm'),
    '35938',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul Swallower'),
    (select id from sets where short_name = 'prm'),
    '60460',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kitchen Finks'),
    (select id from sets where short_name = 'prm'),
    '36166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shamanic Revelation'),
    (select id from sets where short_name = 'prm'),
    '55719',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'prm'),
    '40044',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind Stone'),
    (select id from sets where short_name = 'prm'),
    '31405',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Abyss'),
    (select id from sets where short_name = 'prm'),
    '46938',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Land Tax'),
    (select id from sets where short_name = 'prm'),
    '36855',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Assembled Alphas'),
    (select id from sets where short_name = 'prm'),
    '61551',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dismiss'),
    (select id from sets where short_name = 'prm'),
    '36060',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ash Barrens'),
    (select id from sets where short_name = 'prm'),
    '69983',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dictate of the Twin Gods'),
    (select id from sets where short_name = 'prm'),
    '52334',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hada Freeblade'),
    (select id from sets where short_name = 'prm'),
    '43558',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Renegade Rallier'),
    (select id from sets where short_name = 'prm'),
    '64999',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serendib Efreet'),
    (select id from sets where short_name = 'prm'),
    '217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ardenvale Tactician // Dizzying Swoop'),
    (select id from sets where short_name = 'prm'),
    '78790',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mind''s Eye'),
    (select id from sets where short_name = 'prm'),
    '47981',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Withered Wretch'),
    (select id from sets where short_name = 'prm'),
    '36262',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ludevic''s Test Subject // Ludevic''s Abomination'),
    (select id from sets where short_name = 'prm'),
    '42874',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Master''s Call'),
    (select id from sets where short_name = 'prm'),
    '39644',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'prm'),
    '40096',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ant Queen'),
    (select id from sets where short_name = 'prm'),
    '33103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Copy Artifact'),
    (select id from sets where short_name = 'prm'),
    '65650',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warleader''s Helix'),
    (select id from sets where short_name = 'prm'),
    '51540',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firemind''s Research'),
    (select id from sets where short_name = 'prm'),
    '69949',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sin Collector'),
    (select id from sets where short_name = 'prm'),
    '50112',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grim Haruspex'),
    (select id from sets where short_name = 'prm'),
    '55789',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Khabál Ghoul'),
    (select id from sets where short_name = 'prm'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crystalline Sliver'),
    (select id from sets where short_name = 'prm'),
    '36070',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'prm'),
    '40052',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treasure Mage'),
    (select id from sets where short_name = 'prm'),
    '39642',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Legionnaire'),
    (select id from sets where short_name = 'prm'),
    '35146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golgari Signet'),
    (select id from sets where short_name = 'prm'),
    '62397',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shielded by Faith'),
    (select id from sets where short_name = 'prm'),
    '77953',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stifle'),
    (select id from sets where short_name = 'prm'),
    '43540',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ruinous Path'),
    (select id from sets where short_name = 'prm'),
    '58273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death Baron'),
    (select id from sets where short_name = 'prm'),
    '69254',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaea''s Cradle'),
    (select id from sets where short_name = 'prm'),
    '36112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Broodmate Dragon'),
    (select id from sets where short_name = 'prm'),
    '36148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curse of Thirst'),
    (select id from sets where short_name = 'prm'),
    '43499',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whipcorder'),
    (select id from sets where short_name = 'prm'),
    '36250',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raging Kavu'),
    (select id from sets where short_name = 'prm'),
    '36206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rift Bolt'),
    (select id from sets where short_name = 'prm'),
    '37849',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Dark-Dwellers'),
    (select id from sets where short_name = 'prm'),
    '59673',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'prm'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'prm'),
    '40046',
    'common'
) ,
(
    (select id from mtgcard where name = 'Myr Enforcer'),
    (select id from sets where short_name = 'prm'),
    '36274',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strangleroot Geist'),
    (select id from sets where short_name = 'prm'),
    '43513',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'prm'),
    '36323',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zoetic Cavern'),
    (select id from sets where short_name = 'prm'),
    '36066',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Figure of Destiny'),
    (select id from sets where short_name = 'prm'),
    '32545',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fling'),
    (select id from sets where short_name = 'prm'),
    '36076',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gather the Townsfolk'),
    (select id from sets where short_name = 'prm'),
    '43511',
    'common'
) ,
(
    (select id from mtgcard where name = 'Resolute Archangel'),
    (select id from sets where short_name = 'prm'),
    '53820',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rhystic Study'),
    (select id from sets where short_name = 'prm'),
    '47975',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eidolon of Blossoms'),
    (select id from sets where short_name = 'prm'),
    '52318',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragonscale General'),
    (select id from sets where short_name = 'prm'),
    '55707',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Basking Rootwalla'),
    (select id from sets where short_name = 'prm'),
    '35152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Genju of the Spires'),
    (select id from sets where short_name = 'prm'),
    '36284',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anthousa, Setessan Hero'),
    (select id from sets where short_name = 'prm'),
    '50124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Spirit Guide'),
    (select id from sets where short_name = 'prm'),
    '65654',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Memoricide'),
    (select id from sets where short_name = 'prm'),
    '37863',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cabal Coffers'),
    (select id from sets where short_name = 'prm'),
    '31471',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'prm'),
    '46265',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragonscale Boon'),
    (select id from sets where short_name = 'prm'),
    '55793',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cabal Therapy'),
    (select id from sets where short_name = 'prm'),
    '31473',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Foundry Champion'),
    (select id from sets where short_name = 'prm'),
    '47993',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Joraga Warcaller'),
    (select id from sets where short_name = 'prm'),
    '35820',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Valakut, the Molten Pinnacle'),
    (select id from sets where short_name = 'prm'),
    '35142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Endbringer'),
    (select id from sets where short_name = 'prm'),
    '59667',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mitotic Slime'),
    (select id from sets where short_name = 'prm'),
    '37600',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ember Swallower'),
    (select id from sets where short_name = 'prm'),
    '50130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aether Storm'),
    (select id from sets where short_name = 'prm'),
    '62453',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spellstutter Sprite'),
    (select id from sets where short_name = 'prm'),
    '39622',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flameblade Angel'),
    (select id from sets where short_name = 'prm'),
    '60454',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Longbow Archer'),
    (select id from sets where short_name = 'prm'),
    '36008',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ponder'),
    (select id from sets where short_name = 'prm'),
    '35118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hypersonic Dragon'),
    (select id from sets where short_name = 'prm'),
    '46881',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Garruk Wildspeaker'),
    (select id from sets where short_name = 'prm'),
    '36178',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Qasali Pridemage'),
    (select id from sets where short_name = 'prm'),
    '37608',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Growth'),
    (select id from sets where short_name = 'prm'),
    '35930',
    'common'
) ,
(
    (select id from mtgcard where name = 'Exalted Angel'),
    (select id from sets where short_name = 'prm'),
    '31491',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shivan Dragon'),
    (select id from sets where short_name = 'prm'),
    '62535',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harmonize'),
    (select id from sets where short_name = 'prm'),
    '35106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'prm'),
    '40042',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nissa''s Pilgrimage'),
    (select id from sets where short_name = 'prm'),
    '59677',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jaya Ballard, Task Mage'),
    (select id from sets where short_name = 'prm'),
    '36312',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Voidmage Prodigy'),
    (select id from sets where short_name = 'prm'),
    '43646',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Griselbrand'),
    (select id from sets where short_name = 'prm'),
    '55727',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Malfegor'),
    (select id from sets where short_name = 'prm'),
    '31971',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Urza''s Mine'),
    (select id from sets where short_name = 'prm'),
    '69262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Artisan of Kozilek'),
    (select id from sets where short_name = 'prm'),
    '39673',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Wish'),
    (select id from sets where short_name = 'prm'),
    '35068',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pendelhaven'),
    (select id from sets where short_name = 'prm'),
    '65660',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'prm'),
    '32009',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gideon Jura'),
    (select id from sets where short_name = 'prm'),
    '65003',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Celestial Archon'),
    (select id from sets where short_name = 'prm'),
    '50128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Watcher of the Roost'),
    (select id from sets where short_name = 'prm'),
    '55773',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stoneforge Mystic'),
    (select id from sets where short_name = 'prm'),
    '62499',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathbringer Regent'),
    (select id from sets where short_name = 'prm'),
    '55894',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'prm'),
    '58253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tuinvale Treefolk // Oaken Boon'),
    (select id from sets where short_name = 'prm'),
    '78818',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disintegrate'),
    (select id from sets where short_name = 'prm'),
    '62429',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashiok, Dream Render'),
    (select id from sets where short_name = 'prm'),
    '72259',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Doomsday'),
    (select id from sets where short_name = 'prm'),
    '61058',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Siege Dragon'),
    (select id from sets where short_name = 'prm'),
    '53834',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dissipate'),
    (select id from sets where short_name = 'prm'),
    '35996',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Timetwister'),
    (select id from sets where short_name = 'prm'),
    '46918',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Call of the Conclave'),
    (select id from sets where short_name = 'prm'),
    '48190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savage Lands'),
    (select id from sets where short_name = 'prm'),
    '42876',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'prm'),
    '283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reliquary Tower'),
    (select id from sets where short_name = 'prm'),
    '48007',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Epic Struggle'),
    (select id from sets where short_name = 'prm'),
    '62447',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampire Nocturnus'),
    (select id from sets where short_name = 'prm'),
    '33105',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Boros Signet'),
    (select id from sets where short_name = 'prm'),
    '46904',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feral Throwback'),
    (select id from sets where short_name = 'prm'),
    '36256',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fireblast'),
    (select id from sets where short_name = 'prm'),
    '31401',
    'common'
) ,
(
    (select id from mtgcard where name = 'Identity Thief'),
    (select id from sets where short_name = 'prm'),
    '61559',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reverse Engineer'),
    (select id from sets where short_name = 'prm'),
    '64995',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Remand'),
    (select id from sets where short_name = 'prm'),
    '31429',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lovestruck Beast // Heart''s Desire'),
    (select id from sets where short_name = 'prm'),
    '78830',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'prm'),
    '35932',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eureka'),
    (select id from sets where short_name = 'prm'),
    '46946',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Languish'),
    (select id from sets where short_name = 'prm'),
    '57592',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Castigate'),
    (select id from sets where short_name = 'prm'),
    '35176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Old Man of the Sea'),
    (select id from sets where short_name = 'prm'),
    '43644',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, Planeswalker'),
    (select id from sets where short_name = 'prm'),
    '65009',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'prm'),
    '40094',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reality Shift'),
    (select id from sets where short_name = 'prm'),
    '55759',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Charm'),
    (select id from sets where short_name = 'prm'),
    '49840',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Restoration Angel'),
    (select id from sets where short_name = 'prm'),
    '44317',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teyo, the Shieldmage'),
    (select id from sets where short_name = 'prm'),
    '72249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Niblis of Frost'),
    (select id from sets where short_name = 'prm'),
    '61547',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dauntless Dourbark'),
    (select id from sets where short_name = 'prm'),
    '36098',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Hounds'),
    (select id from sets where short_name = 'prm'),
    '36154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scryb Sprites'),
    (select id from sets where short_name = 'prm'),
    '62431',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ifh-Bíff Efreet'),
    (select id from sets where short_name = 'prm'),
    '213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dualcaster Mage'),
    (select id from sets where short_name = 'prm'),
    '55868',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tormented Hero'),
    (select id from sets where short_name = 'prm'),
    '51934',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flusterstorm'),
    (select id from sets where short_name = 'prm'),
    '69985',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flamerush Rider'),
    (select id from sets where short_name = 'prm'),
    '55713',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unlicensed Disintegration'),
    (select id from sets where short_name = 'prm'),
    '64430',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'prm'),
    '40062',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shipbreaker Kraken'),
    (select id from sets where short_name = 'prm'),
    '50106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anguished Unmaking'),
    (select id from sets where short_name = 'prm'),
    '60462',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Colossal Whale'),
    (select id from sets where short_name = 'prm'),
    '49834',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Despise'),
    (select id from sets where short_name = 'prm'),
    '43065',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragonlord''s Servant'),
    (select id from sets where short_name = 'prm'),
    '55884',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Noble Hierarch'),
    (select id from sets where short_name = 'prm'),
    '58239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Braingeyser'),
    (select id from sets where short_name = 'prm'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Angel'),
    (select id from sets where short_name = 'prm'),
    '35942',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fanatic of Xenagos'),
    (select id from sets where short_name = 'prm'),
    '54559',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'prm'),
    '40098',
    'common'
) ,
(
    (select id from mtgcard where name = 'Volcanic Geyser'),
    (select id from sets where short_name = 'prm'),
    '36004',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deathless Angel'),
    (select id from sets where short_name = 'prm'),
    '36853',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magister of Worth'),
    (select id from sets where short_name = 'prm'),
    '52593',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plague Stinger'),
    (select id from sets where short_name = 'prm'),
    '37867',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pillar of Flame'),
    (select id from sets where short_name = 'prm'),
    '46871',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stealer of Secrets'),
    (select id from sets where short_name = 'prm'),
    '52310',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Tabernacle at Pendrell Vale'),
    (select id from sets where short_name = 'prm'),
    '60450',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gemstone Mine'),
    (select id from sets where short_name = 'prm'),
    '36028',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slith Firewalker'),
    (select id from sets where short_name = 'prm'),
    '35158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakshasa Vizier'),
    (select id from sets where short_name = 'prm'),
    '54516',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Avenger'),
    (select id from sets where short_name = 'prm'),
    '36314',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sinkhole'),
    (select id from sets where short_name = 'prm'),
    '43566',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Icatian Javelineers'),
    (select id from sets where short_name = 'prm'),
    '35980',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darksteel Ingot'),
    (select id from sets where short_name = 'prm'),
    '36276',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arashin War Beast'),
    (select id from sets where short_name = 'prm'),
    '55741',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Albino Troll'),
    (select id from sets where short_name = 'prm'),
    '36122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grim Lavamancer'),
    (select id from sets where short_name = 'prm'),
    '31489',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forbidden Alchemy'),
    (select id from sets where short_name = 'prm'),
    '45203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Huatli, the Sun''s Heart'),
    (select id from sets where short_name = 'prm'),
    '72241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Time Walk'),
    (select id from sets where short_name = 'prm'),
    '46912',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blasting Station'),
    (select id from sets where short_name = 'prm'),
    '62473',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flooded Strand'),
    (select id from sets where short_name = 'prm'),
    '69252',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Intuition'),
    (select id from sets where short_name = 'prm'),
    '36046',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Fodder'),
    (select id from sets where short_name = 'prm'),
    '55898',
    'common'
) ,
(
    (select id from mtgcard where name = 'Magmaquake'),
    (select id from sets where short_name = 'prm'),
    '45207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drogskol Cavalry'),
    (select id from sets where short_name = 'prm'),
    '60466',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Signet'),
    (select id from sets where short_name = 'prm'),
    '62391',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter Assembly'),
    (select id from sets where short_name = 'prm'),
    '39638',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merfolk Secretkeeper // Venture Deeper'),
    (select id from sets where short_name = 'prm'),
    '78822',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chrome Mox'),
    (select id from sets where short_name = 'prm'),
    '36160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Trueheart Duelist'),
    (select id from sets where short_name = 'prm'),
    '64424',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forbid'),
    (select id from sets where short_name = 'prm'),
    '36078',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Corpsejack Menace'),
    (select id from sets where short_name = 'prm'),
    '46887',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Damnation'),
    (select id from sets where short_name = 'prm'),
    '55862',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Supplant Form'),
    (select id from sets where short_name = 'prm'),
    '55723',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Regrowth'),
    (select id from sets where short_name = 'prm'),
    '35940',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Misdirection'),
    (select id from sets where short_name = 'prm'),
    '62461',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unsubstantiate'),
    (select id from sets where short_name = 'prm'),
    '61557',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saheeli, Sublime Artificer'),
    (select id from sets where short_name = 'prm'),
    '72287',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Blossoms'),
    (select id from sets where short_name = 'prm'),
    '36084',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Geist of Saint Traft'),
    (select id from sets where short_name = 'prm'),
    '51928',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'prm'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lava Axe'),
    (select id from sets where short_name = 'prm'),
    '32559',
    'common'
) ,
(
    (select id from mtgcard where name = 'Decree of Justice'),
    (select id from sets where short_name = 'prm'),
    '31399',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mardu Shadowspear'),
    (select id from sets where short_name = 'prm'),
    '55717',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tormented Soul'),
    (select id from sets where short_name = 'prm'),
    '41652',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karn, the Great Creator'),
    (select id from sets where short_name = 'prm'),
    '72245',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiery Temper'),
    (select id from sets where short_name = 'prm'),
    '62217',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reya Dawnbringer'),
    (select id from sets where short_name = 'prm'),
    '36086',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azorius Signet'),
    (select id from sets where short_name = 'prm'),
    '46906',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Blue Elemental Blast'),
    (select id from sets where short_name = 'prm'),
    '43606',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zodiac Monkey'),
    (select id from sets where short_name = 'prm'),
    '35084',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avacyn, Angel of Hope'),
    (select id from sets where short_name = 'prm'),
    '64436',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diaochan, Artful Beauty'),
    (select id from sets where short_name = 'prm'),
    '47971',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Farseek'),
    (select id from sets where short_name = 'prm'),
    '48005',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shard Phoenix'),
    (select id from sets where short_name = 'prm'),
    '36308',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hymn to Tourach'),
    (select id from sets where short_name = 'prm'),
    '43634',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Nacatl'),
    (select id from sets where short_name = 'prm'),
    '37851',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deranged Hermit'),
    (select id from sets where short_name = 'prm'),
    '36132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jace''s Ingenuity'),
    (select id from sets where short_name = 'prm'),
    '41642',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rise from the Tides'),
    (select id from sets where short_name = 'prm'),
    '62209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Naya Sojourners'),
    (select id from sets where short_name = 'prm'),
    '36188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darksteel Relic'),
    (select id from sets where short_name = 'prm'),
    '41108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Curse of Wizardry'),
    (select id from sets where short_name = 'prm'),
    '36863',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serum Visions'),
    (select id from sets where short_name = 'prm'),
    '57586',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hanna, Ship''s Navigator'),
    (select id from sets where short_name = 'prm'),
    '52308',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Command Tower'),
    (select id from sets where short_name = 'prm'),
    '47979',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wonder'),
    (select id from sets where short_name = 'prm'),
    '35154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Omens'),
    (select id from sets where short_name = 'prm'),
    '39624',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pernicious Deed'),
    (select id from sets where short_name = 'prm'),
    '36306',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pendelhaven'),
    (select id from sets where short_name = 'prm'),
    '35104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Signet'),
    (select id from sets where short_name = 'prm'),
    '62435',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nether Void'),
    (select id from sets where short_name = 'prm'),
    '46936',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zodiac Horse'),
    (select id from sets where short_name = 'prm'),
    '35090',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serra Avatar'),
    (select id from sets where short_name = 'prm'),
    '26984',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyrokinesis'),
    (select id from sets where short_name = 'prm'),
    '43640',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avatar of Woe'),
    (select id from sets where short_name = 'prm'),
    '36208',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cryptborn Horror'),
    (select id from sets where short_name = 'prm'),
    '46883',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Murderous Rider // Swift End'),
    (select id from sets where short_name = 'prm'),
    '78846',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'prm'),
    '40050',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'prm'),
    '253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tendrils of Agony'),
    (select id from sets where short_name = 'prm'),
    '31433',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'prm'),
    '31975',
    'common'
) ,
(
    (select id from mtgcard where name = 'Domri, Anarch of Bolas'),
    (select id from sets where short_name = 'prm'),
    '72225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'prm'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nessian Wilds Ravager'),
    (select id from sets where short_name = 'prm'),
    '51924',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'prm'),
    '273',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savannah'),
    (select id from sets where short_name = 'prm'),
    '43614',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Incendiary Flow'),
    (select id from sets where short_name = 'prm'),
    '62991',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Argivian Archaeologist'),
    (select id from sets where short_name = 'prm'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sandsteppe Mastodon'),
    (select id from sets where short_name = 'prm'),
    '55721',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'prm'),
    '58257',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'prm'),
    '68039',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krosan Grip'),
    (select id from sets where short_name = 'prm'),
    '37604',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stasis Snare'),
    (select id from sets where short_name = 'prm'),
    '58275',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Narset, Parter of Veils'),
    (select id from sets where short_name = 'prm'),
    '72269',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ultimate Price'),
    (select id from sets where short_name = 'prm'),
    '58269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teferi, Time Raveler'),
    (select id from sets where short_name = 'prm'),
    '72233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'prm'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Firewalker'),
    (select id from sets where short_name = 'prm'),
    '43574',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chainer''s Edict'),
    (select id from sets where short_name = 'prm'),
    '36242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wildcall'),
    (select id from sets where short_name = 'prm'),
    '55749',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snap'),
    (select id from sets where short_name = 'prm'),
    '62441',
    'common'
) ,
(
    (select id from mtgcard where name = 'Celestial Colonnade'),
    (select id from sets where short_name = 'prm'),
    '43572',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Maze''s End'),
    (select id from sets where short_name = 'prm'),
    '48572',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nephalia Moondrakes'),
    (select id from sets where short_name = 'prm'),
    '60458',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deadbridge Goliath'),
    (select id from sets where short_name = 'prm'),
    '46875',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'prm'),
    '31981',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reaper of Night // Harvest Fear'),
    (select id from sets where short_name = 'prm'),
    '78770',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kasmina, Enigmatic Mentor'),
    (select id from sets where short_name = 'prm'),
    '72253',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Banisher Priest'),
    (select id from sets where short_name = 'prm'),
    '51908',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lotus Bloom'),
    (select id from sets where short_name = 'prm'),
    '31495',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Momentary Blink'),
    (select id from sets where short_name = 'prm'),
    '60478',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rukh Egg'),
    (select id from sets where short_name = 'prm'),
    '36270',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Barrage Tyrant'),
    (select id from sets where short_name = 'prm'),
    '58249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Empyrial Armor'),
    (select id from sets where short_name = 'prm'),
    '36024',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mox Pearl'),
    (select id from sets where short_name = 'prm'),
    '46892',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Juggernaut'),
    (select id from sets where short_name = 'prm'),
    '35044',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kiyomaro, First to Stand'),
    (select id from sets where short_name = 'prm'),
    '32015',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Geist of Saint Traft'),
    (select id from sets where short_name = 'prm'),
    '60484',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Faerie Guidemother // Gift of the Fae'),
    (select id from sets where short_name = 'prm'),
    '78850',
    'common'
) ,
(
    (select id from mtgcard where name = 'Retaliator Griffin'),
    (select id from sets where short_name = 'prm'),
    '43546',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anathemancer'),
    (select id from sets where short_name = 'prm'),
    '37606',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zahid, Djinn of the Lamp'),
    (select id from sets where short_name = 'prm'),
    '68041',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guul Draz Assassin'),
    (select id from sets where short_name = 'prm'),
    '36875',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampiric Tutor'),
    (select id from sets where short_name = 'prm'),
    '36018',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hewed Stone Retainers'),
    (select id from sets where short_name = 'prm'),
    '55745',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kjeldoran Outpost'),
    (select id from sets where short_name = 'prm'),
    '23952',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blood Knight'),
    (select id from sets where short_name = 'prm'),
    '35984',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Elder'),
    (select id from sets where short_name = 'prm'),
    '36282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple Garden'),
    (select id from sets where short_name = 'prm'),
    '72303',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avatar of Discord'),
    (select id from sets where short_name = 'prm'),
    '32031',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Preacher'),
    (select id from sets where short_name = 'prm'),
    '23950',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ratchet Bomb'),
    (select id from sets where short_name = 'prm'),
    '49836',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scroll Rack'),
    (select id from sets where short_name = 'prm'),
    '47969',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carrion Feeder'),
    (select id from sets where short_name = 'prm'),
    '36264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lord of Atlantis'),
    (select id from sets where short_name = 'prm'),
    '69945',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orim''s Chant'),
    (select id from sets where short_name = 'prm'),
    '35072',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Woolly Thoctar'),
    (select id from sets where short_name = 'prm'),
    '31449',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shield of Kaldra'),
    (select id from sets where short_name = 'prm'),
    '31985',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circle of Protection: Red'),
    (select id from sets where short_name = 'prm'),
    '31375',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skyknight Legionnaire'),
    (select id from sets where short_name = 'prm'),
    '36296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glorybringer'),
    (select id from sets where short_name = 'prm'),
    '64426',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Damnation'),
    (select id from sets where short_name = 'prm'),
    '31417',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'prm'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arashin Sovereign'),
    (select id from sets where short_name = 'prm'),
    '56996',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thawing Glaciers'),
    (select id from sets where short_name = 'prm'),
    '43564',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flooded Strand'),
    (select id from sets where short_name = 'prm'),
    '43580',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Staff of Nin'),
    (select id from sets where short_name = 'prm'),
    '45213',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Cackler'),
    (select id from sets where short_name = 'prm'),
    '49842',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'prm'),
    '281',
    'common'
) ,
(
    (select id from mtgcard where name = 'Black Lotus'),
    (select id from sets where short_name = 'prm'),
    '46932',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mystic of the Hidden Way'),
    (select id from sets where short_name = 'prm'),
    '55775',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Vault'),
    (select id from sets where short_name = 'prm'),
    '46942',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sultai Ascendancy'),
    (select id from sets where short_name = 'prm'),
    '55701',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Island'),
    (select id from sets where short_name = 'prm'),
    '43626',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bitterblossom'),
    (select id from sets where short_name = 'prm'),
    '39626',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temple of Mystery'),
    (select id from sets where short_name = 'prm'),
    '53846',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harrow'),
    (select id from sets where short_name = 'prm'),
    '37612',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ankle Shanker'),
    (select id from sets where short_name = 'prm'),
    '54512',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ugin, the Ineffable'),
    (select id from sets where short_name = 'prm'),
    '72235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kiora, Behemoth Beckoner'),
    (select id from sets where short_name = 'prm'),
    '72293',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Go for the Throat'),
    (select id from sets where short_name = 'prm'),
    '42878',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plateau'),
    (select id from sets where short_name = 'prm'),
    '43608',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Champion'),
    (select id from sets where short_name = 'prm'),
    '31435',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Firemane Avenger'),
    (select id from sets where short_name = 'prm'),
    '47991',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Order of Midnight // Alter Fate'),
    (select id from sets where short_name = 'prm'),
    '78838',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Sprite // Mesmeric Glare'),
    (select id from sets where short_name = 'prm'),
    '78742',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Demigod of Revenge'),
    (select id from sets where short_name = 'prm'),
    '31437',
    'rare'
) ,
(
    (select id from mtgcard where name = 'True Conviction'),
    (select id from sets where short_name = 'prm'),
    '77951',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Indulgent Tormentor'),
    (select id from sets where short_name = 'prm'),
    '53822',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fiery Temper'),
    (select id from sets where short_name = 'prm'),
    '35978',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Flare'),
    (select id from sets where short_name = 'prm'),
    '58241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sword of Kaldra'),
    (select id from sets where short_name = 'prm'),
    '31987',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treasury Thrull'),
    (select id from sets where short_name = 'prm'),
    '47999',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archon of the Triumvirate'),
    (select id from sets where short_name = 'prm'),
    '46877',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reflecting Pool'),
    (select id from sets where short_name = 'prm'),
    '62487',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Doran, the Siege Tower'),
    (select id from sets where short_name = 'prm'),
    '31411',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bottle Gnomes'),
    (select id from sets where short_name = 'prm'),
    '36050',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'prm'),
    '53883',
    'common'
) ,
(
    (select id from mtgcard where name = 'Allosaurus Rider'),
    (select id from sets where short_name = 'prm'),
    '32033',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Polluted Delta'),
    (select id from sets where short_name = 'prm'),
    '43582',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Render Silent'),
    (select id from sets where short_name = 'prm'),
    '48576',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Overgrown Tomb'),
    (select id from sets where short_name = 'prm'),
    '72315',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mondronen Shaman // Tovolar''s Magehunter'),
    (select id from sets where short_name = 'prm'),
    '43507',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vraska, Swarm''s Eminence'),
    (select id from sets where short_name = 'prm'),
    '72289',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silkwrap'),
    (select id from sets where short_name = 'prm'),
    '62497',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stone Haven Outfitter'),
    (select id from sets where short_name = 'prm'),
    '62495',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Repeal'),
    (select id from sets where short_name = 'prm'),
    '54543',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blightning'),
    (select id from sets where short_name = 'prm'),
    '35124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwynen, Gilt-Leaf Daen'),
    (select id from sets where short_name = 'prm'),
    '57600',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rewind'),
    (select id from sets where short_name = 'prm'),
    '36124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace Beleren'),
    (select id from sets where short_name = 'prm'),
    '36176',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mana Tithe'),
    (select id from sets where short_name = 'prm'),
    '35108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Surgical Extraction'),
    (select id from sets where short_name = 'prm'),
    '40072',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oblivion Ring'),
    (select id from sets where short_name = 'prm'),
    '36202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodbraid Elf'),
    (select id from sets where short_name = 'prm'),
    '36867',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Mongrel'),
    (select id from sets where short_name = 'prm'),
    '36240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Control Magic'),
    (select id from sets where short_name = 'prm'),
    '43594',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rosethorn Acolyte // Seasonal Ritual'),
    (select id from sets where short_name = 'prm'),
    '78842',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scragnoth'),
    (select id from sets where short_name = 'prm'),
    '36054',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rattleclaw Mystic'),
    (select id from sets where short_name = 'prm'),
    '54557',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Immolating Glare'),
    (select id from sets where short_name = 'prm'),
    '59669',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Force of Will'),
    (select id from sets where short_name = 'prm'),
    '43578',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fyndhorn Elves'),
    (select id from sets where short_name = 'prm'),
    '43632',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psionic Blast'),
    (select id from sets where short_name = 'prm'),
    '35058',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodcrazed Neonate'),
    (select id from sets where short_name = 'prm'),
    '42862',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avacyn''s Pilgrim'),
    (select id from sets where short_name = 'prm'),
    '45209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurmcoil Engine'),
    (select id from sets where short_name = 'prm'),
    '37861',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sage-Eye Avengers'),
    (select id from sets where short_name = 'prm'),
    '55709',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'prm'),
    '40048',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunderbreak Regent'),
    (select id from sets where short_name = 'prm'),
    '55892',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = 'prm'),
    '31413',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lobotomy'),
    (select id from sets where short_name = 'prm'),
    '32529',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mutavault'),
    (select id from sets where short_name = 'prm'),
    '66888',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sacred Foundry'),
    (select id from sets where short_name = 'prm'),
    '72309',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ball Lightning'),
    (select id from sets where short_name = 'prm'),
    '35972',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mogg Fanatic'),
    (select id from sets where short_name = 'prm'),
    '36036',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azorius Guildmage'),
    (select id from sets where short_name = 'prm'),
    '32585',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Watery Grave'),
    (select id from sets where short_name = 'prm'),
    '72299',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rhystic Study'),
    (select id from sets where short_name = 'prm'),
    '69943',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fire-Lit Thicket'),
    (select id from sets where short_name = 'prm'),
    '62457',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Titania''s Song'),
    (select id from sets where short_name = 'prm'),
    '78860',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'prm'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elspeth, Sun''s Champion'),
    (select id from sets where short_name = 'prm'),
    '59647',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Abbot of Keral Keep'),
    (select id from sets where short_name = 'prm'),
    '62501',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abhorrent Overlord'),
    (select id from sets where short_name = 'prm'),
    '50126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Priest of Titania'),
    (select id from sets where short_name = 'prm'),
    '36120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squelching Leeches'),
    (select id from sets where short_name = 'prm'),
    '52330',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scent of Cinder'),
    (select id from sets where short_name = 'prm'),
    '36150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ryusei, the Falling Star'),
    (select id from sets where short_name = 'prm'),
    '32001',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ivorytusk Fortress'),
    (select id from sets where short_name = 'prm'),
    '54510',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Emeria Angel'),
    (select id from sets where short_name = 'prm'),
    '36218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Incinerate'),
    (select id from sets where short_name = 'prm'),
    '31419',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctifier of Souls'),
    (select id from sets where short_name = 'prm'),
    '61543',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Volcanic Hammer'),
    (select id from sets where short_name = 'prm'),
    '36102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fork'),
    (select id from sets where short_name = 'prm'),
    '207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reaper of the Wilds'),
    (select id from sets where short_name = 'prm'),
    '55737',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'prm'),
    '251',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gitaxian Probe'),
    (select id from sets where short_name = 'prm'),
    '46869',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sparksmith'),
    (select id from sets where short_name = 'prm'),
    '32569',
    'common'
) ,
(
    (select id from mtgcard where name = 'Riku of Two Reflections'),
    (select id from sets where short_name = 'prm'),
    '51536',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hero''s Downfall'),
    (select id from sets where short_name = 'prm'),
    '55731',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Ascendancy'),
    (select id from sets where short_name = 'prm'),
    '71586',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roast'),
    (select id from sets where short_name = 'prm'),
    '58263',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Munda''s Vanguard'),
    (select id from sets where short_name = 'prm'),
    '59659',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mox Emerald'),
    (select id from sets where short_name = 'prm'),
    '46896',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merchant Scroll'),
    (select id from sets where short_name = 'prm'),
    '68055',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angrath, Captain of Chaos'),
    (select id from sets where short_name = 'prm'),
    '72237',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Groundbreaker'),
    (select id from sets where short_name = 'prm'),
    '36072',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dimir Guildmage'),
    (select id from sets where short_name = 'prm'),
    '32579',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Garenbrig Carver // Shield''s Might'),
    (select id from sets where short_name = 'prm'),
    '78782',
    'common'
) ,
(
    (select id from mtgcard where name = 'Budoka Pupil // Ichiga, Who Topples Oaks'),
    (select id from sets where short_name = 'prm'),
    '36288',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'prm'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fling'),
    (select id from sets where short_name = 'prm'),
    '36074',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravages of War'),
    (select id from sets where short_name = 'prm'),
    '55864',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brain Maggot'),
    (select id from sets where short_name = 'prm'),
    '54551',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jeering Instigator'),
    (select id from sets where short_name = 'prm'),
    '55771',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Embereth Shieldbreaker // Battle Display'),
    (select id from sets where short_name = 'prm'),
    '78774',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crusade'),
    (select id from sets where short_name = 'prm'),
    '35926',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Bombardment'),
    (select id from sets where short_name = 'prm'),
    '54545',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desert'),
    (select id from sets where short_name = 'prm'),
    '35148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Rejuvenator'),
    (select id from sets where short_name = 'prm'),
    '69258',
    'common'
) ,
(
    (select id from mtgcard where name = 'Debilitating Injury'),
    (select id from sets where short_name = 'prm'),
    '55765',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dovin, Hand of Control'),
    (select id from sets where short_name = 'prm'),
    '72227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hammer of Bogardan'),
    (select id from sets where short_name = 'prm'),
    '36000',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Godless Shrine'),
    (select id from sets where short_name = 'prm'),
    '72305',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Signet'),
    (select id from sets where short_name = 'prm'),
    '46900',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balance'),
    (select id from sets where short_name = 'prm'),
    '36278',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teetering Peaks'),
    (select id from sets where short_name = 'prm'),
    '41654',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'prm'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Warchief'),
    (select id from sets where short_name = 'prm'),
    '60452',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Signal Pest'),
    (select id from sets where short_name = 'prm'),
    '39648',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avalanche Riders'),
    (select id from sets where short_name = 'prm'),
    '36134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jeskai Infiltrator'),
    (select id from sets where short_name = 'prm'),
    '55757',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'prm'),
    '31997',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crumbling Vestige'),
    (select id from sets where short_name = 'prm'),
    '61565',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rupture'),
    (select id from sets where short_name = 'prm'),
    '62417',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cultivate'),
    (select id from sets where short_name = 'prm'),
    '41640',
    'common'
) ,
(
    (select id from mtgcard where name = 'Traxos, Scourge of Kroog'),
    (select id from sets where short_name = 'prm'),
    '69242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'prm'),
    '31381',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Consuming Aberration'),
    (select id from sets where short_name = 'prm'),
    '47997',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Firewheeler'),
    (select id from sets where short_name = 'prm'),
    '71590',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grafdigger''s Cage'),
    (select id from sets where short_name = 'prm'),
    '69989',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desertion'),
    (select id from sets where short_name = 'prm'),
    '47967',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pristine Talisman'),
    (select id from sets where short_name = 'prm'),
    '40074',
    'common'
) ,
(
    (select id from mtgcard where name = 'Laquatus''s Champion'),
    (select id from sets where short_name = 'prm'),
    '37875',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Life // Death'),
    (select id from sets where short_name = 'prm'),
    '32591',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wasteland'),
    (select id from sets where short_name = 'prm'),
    '36877',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sultai Charm'),
    (select id from sets where short_name = 'prm'),
    '54561',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Killing Wave'),
    (select id from sets where short_name = 'prm'),
    '44319',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Circular Logic'),
    (select id from sets where short_name = 'prm'),
    '36244',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sedge Troll'),
    (select id from sets where short_name = 'prm'),
    '209',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abu Ja''far'),
    (select id from sets where short_name = 'prm'),
    '62479',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sword of Fire and Ice'),
    (select id from sets where short_name = 'prm'),
    '39650',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scapeshift'),
    (select id from sets where short_name = 'prm'),
    '66898',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Faerie Conclave'),
    (select id from sets where short_name = 'prm'),
    '36090',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Path to Exile'),
    (select id from sets where short_name = 'prm'),
    '57580',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Astral Slide'),
    (select id from sets where short_name = 'prm'),
    '32567',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Natural Order'),
    (select id from sets where short_name = 'prm'),
    '43536',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana, Untouched by Death'),
    (select id from sets where short_name = 'prm'),
    '70938',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tundra'),
    (select id from sets where short_name = 'prm'),
    '43622',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shriekmaw'),
    (select id from sets where short_name = 'prm'),
    '32537',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brainstorm'),
    (select id from sets where short_name = 'prm'),
    '31453',
    'common'
) ,
(
    (select id from mtgcard where name = 'Roar of the Wurm'),
    (select id from sets where short_name = 'prm'),
    '32565',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = 'prm'),
    '37598',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = 'prm'),
    '35948',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'prm'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace, Wielder of Mysteries'),
    (select id from sets where short_name = 'prm'),
    '72265',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dungrove Elder'),
    (select id from sets where short_name = 'prm'),
    '41650',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smitten Swordmaster // Curry Favor'),
    (select id from sets where short_name = 'prm'),
    '78766',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frenzied Goblin'),
    (select id from sets where short_name = 'prm'),
    '55703',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nekusar, the Mindrazer'),
    (select id from sets where short_name = 'prm'),
    '52314',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sorin, Vengeful Bloodlord'),
    (select id from sets where short_name = 'prm'),
    '72277',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drain Life'),
    (select id from sets where short_name = 'prm'),
    '35928',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunscorch Regent'),
    (select id from sets where short_name = 'prm'),
    '55896',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'prm'),
    '53877',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arrogant Wurm'),
    (select id from sets where short_name = 'prm'),
    '36246',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chains of Mephistopheles'),
    (select id from sets where short_name = 'prm'),
    '65646',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karakas'),
    (select id from sets where short_name = 'prm'),
    '43592',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Orzhov Signet'),
    (select id from sets where short_name = 'prm'),
    '62399',
    'common'
) ,
(
    (select id from mtgcard where name = 'Capsize'),
    (select id from sets where short_name = 'prm'),
    '36040',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Workshop'),
    (select id from sets where short_name = 'prm'),
    '46944',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Creeping Mold'),
    (select id from sets where short_name = 'prm'),
    '32525',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'prm'),
    '73630',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychatog'),
    (select id from sets where short_name = 'prm'),
    '31467',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind''s Desire'),
    (select id from sets where short_name = 'prm'),
    '35070',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Pioneer'),
    (select id from sets where short_name = 'prm'),
    '62481',
    'common'
) ,
(
    (select id from mtgcard where name = 'Call the Bloodline'),
    (select id from sets where short_name = 'prm'),
    '62213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'prm'),
    '35934',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zodiac Pig'),
    (select id from sets where short_name = 'prm'),
    '35100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Badlands'),
    (select id from sets where short_name = 'prm'),
    '43600',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruul Guildmage'),
    (select id from sets where short_name = 'prm'),
    '36302',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gaddock Teeg'),
    (select id from sets where short_name = 'prm'),
    '64418',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deepfathom Skulker'),
    (select id from sets where short_name = 'prm'),
    '59663',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Man-o''-War'),
    (select id from sets where short_name = 'prm'),
    '36010',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'prm'),
    '40092',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nissa, Who Shakes the World'),
    (select id from sets where short_name = 'prm'),
    '72261',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kor Skyfisher'),
    (select id from sets where short_name = 'prm'),
    '43548',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis Reignited'),
    (select id from sets where short_name = 'prm'),
    '62509',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Animating Faerie // Bring to Life'),
    (select id from sets where short_name = 'prm'),
    '78746',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crucible of Worlds'),
    (select id from sets where short_name = 'prm'),
    '49846',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terminate'),
    (select id from sets where short_name = 'prm'),
    '35136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Surging Flame'),
    (select id from sets where short_name = 'prm'),
    '35178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Calciderm'),
    (select id from sets where short_name = 'prm'),
    '35988',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merrow Reejerey'),
    (select id from sets where short_name = 'prm'),
    '36164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mox Sapphire'),
    (select id from sets where short_name = 'prm'),
    '46902',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sprouting Thrinax'),
    (select id from sets where short_name = 'prm'),
    '31451',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stomping Ground'),
    (select id from sets where short_name = 'prm'),
    '72313',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Triumph'),
    (select id from sets where short_name = 'prm'),
    '72221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gleancrawler'),
    (select id from sets where short_name = 'prm'),
    '32017',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zodiac Snake'),
    (select id from sets where short_name = 'prm'),
    '35096',
    'common'
) ,
(
    (select id from mtgcard where name = 'Genesis Hydra'),
    (select id from sets where short_name = 'prm'),
    '55870',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ophidian'),
    (select id from sets where short_name = 'prm'),
    '36022',
    'common'
) ,
(
    (select id from mtgcard where name = 'Searing Blaze'),
    (select id from sets where short_name = 'prm'),
    '39630',
    'common'
) ,
(
    (select id from mtgcard where name = 'False Prophet'),
    (select id from sets where short_name = 'prm'),
    '32200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Basalt Monolith'),
    (select id from sets where short_name = 'prm'),
    '43602',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bayou'),
    (select id from sets where short_name = 'prm'),
    '43604',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zodiac Tiger'),
    (select id from sets where short_name = 'prm'),
    '35088',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Relic Seeker'),
    (select id from sets where short_name = 'prm'),
    '57574',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Call of the Herd'),
    (select id from sets where short_name = 'prm'),
    '36108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Force Spike'),
    (select id from sets where short_name = 'prm'),
    '31395',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Monstrous Hound'),
    (select id from sets where short_name = 'prm'),
    '32194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chain of Vapor'),
    (select id from sets where short_name = 'prm'),
    '62513',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vault Skirge'),
    (select id from sets where short_name = 'prm'),
    '40076',
    'common'
) ,
(
    (select id from mtgcard where name = 'Suture Priest'),
    (select id from sets where short_name = 'prm'),
    '40070',
    'common'
) ,
(
    (select id from mtgcard where name = 'Negate'),
    (select id from sets where short_name = 'prm'),
    '35138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tectonic Edge'),
    (select id from sets where short_name = 'prm'),
    '43069',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magma Jet'),
    (select id from sets where short_name = 'prm'),
    '36162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terminate'),
    (select id from sets where short_name = 'prm'),
    '36230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clash of Wills'),
    (select id from sets where short_name = 'prm'),
    '59671',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Negator'),
    (select id from sets where short_name = 'prm'),
    '36144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampiric Tutor'),
    (select id from sets where short_name = 'prm'),
    '68053',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gleemox'),
    (select id from sets where short_name = 'prm'),
    '26584',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cuombajj Witches'),
    (select id from sets where short_name = 'prm'),
    '65652',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stupor'),
    (select id from sets where short_name = 'prm'),
    '36002',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Hellkite'),
    (select id from sets where short_name = 'prm'),
    '37594',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gerrard''s Verdict'),
    (select id from sets where short_name = 'prm'),
    '31461',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Two-Headed Giant of Foriys'),
    (select id from sets where short_name = 'prm'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Banishing Light'),
    (select id from sets where short_name = 'prm'),
    '53840',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tember City'),
    (select id from sets where short_name = 'prm'),
    '44342',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghostfire Blade'),
    (select id from sets where short_name = 'prm'),
    '55777',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'prm'),
    '32023',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silvergill Adept'),
    (select id from sets where short_name = 'prm'),
    '66892',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heroes'' Bane'),
    (select id from sets where short_name = 'prm'),
    '52328',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zodiac Rat'),
    (select id from sets where short_name = 'prm'),
    '35094',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast of Burden'),
    (select id from sets where short_name = 'prm'),
    '32198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Smother'),
    (select id from sets where short_name = 'prm'),
    '31475',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'In Garruk''s Wake'),
    (select id from sets where short_name = 'prm'),
    '53828',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lu Bu, Master-at-Arms'),
    (select id from sets where short_name = 'prm'),
    '36130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tazeem'),
    (select id from sets where short_name = 'prm'),
    '44340',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Horizon Boughs'),
    (select id from sets where short_name = 'prm'),
    '44346',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frost Walker'),
    (select id from sets where short_name = 'prm'),
    '55886',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Acidic Slime'),
    (select id from sets where short_name = 'prm'),
    '45199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hazezon Tamar'),
    (select id from sets where short_name = 'prm'),
    '23956',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Putrefy'),
    (select id from sets where short_name = 'prm'),
    '35166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'prm'),
    '31377',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Vandal'),
    (select id from sets where short_name = 'prm'),
    '46916',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snapcaster Mage'),
    (select id from sets where short_name = 'prm'),
    '59685',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bishop of Rebirth'),
    (select id from sets where short_name = 'prm'),
    '65662',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silent Specter'),
    (select id from sets where short_name = 'prm'),
    '36252',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Library of Alexandria'),
    (select id from sets where short_name = 'prm'),
    '46940',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Experiment One'),
    (select id from sets where short_name = 'prm'),
    '49838',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phalanx Leader'),
    (select id from sets where short_name = 'prm'),
    '50116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heron''s Grace Champion'),
    (select id from sets where short_name = 'prm'),
    '61563',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'prm'),
    '31993',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skarrg Goliath'),
    (select id from sets where short_name = 'prm'),
    '47989',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terra Stomper'),
    (select id from sets where short_name = 'prm'),
    '36849',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rishadan Port'),
    (select id from sets where short_name = 'prm'),
    '55739',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mycoid Shepherd'),
    (select id from sets where short_name = 'prm'),
    '43556',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Duplicant'),
    (select id from sets where short_name = 'prm'),
    '47983',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beacon of Unrest'),
    (select id from sets where short_name = 'prm'),
    '62519',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thunder Spirit'),
    (select id from sets where short_name = 'prm'),
    '229',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ugin, the Spirit Dragon'),
    (select id from sets where short_name = 'prm'),
    '55763',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = 'prm'),
    '35128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yawgmoth''s Will'),
    (select id from sets where short_name = 'prm'),
    '35080',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forked Lightning'),
    (select id from sets where short_name = 'prm'),
    '33440',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Write into Being'),
    (select id from sets where short_name = 'prm'),
    '55755',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Leak'),
    (select id from sets where short_name = 'prm'),
    '31481',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gush'),
    (select id from sets where short_name = 'prm'),
    '61056',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slice and Dice'),
    (select id from sets where short_name = 'prm'),
    '32573',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eater of Hope'),
    (select id from sets where short_name = 'prm'),
    '51918',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'prm'),
    '40058',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Royal Scions'),
    (select id from sets where short_name = 'prm'),
    '78854',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chill'),
    (select id from sets where short_name = 'prm'),
    '36062',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desecrated Tomb'),
    (select id from sets where short_name = 'prm'),
    '69244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sylvan Library'),
    (select id from sets where short_name = 'prm'),
    '23948',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oxidize'),
    (select id from sets where short_name = 'prm'),
    '35162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brave the Elements'),
    (select id from sets where short_name = 'prm'),
    '37857',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Anticipate'),
    (select id from sets where short_name = 'prm'),
    '58267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sheoldred, Whispering One'),
    (select id from sets where short_name = 'prm'),
    '40086',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Zhalfirin Void'),
    (select id from sets where short_name = 'prm'),
    '68043',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Winter Orb'),
    (select id from sets where short_name = 'prm'),
    '46922',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diabolic Edict'),
    (select id from sets where short_name = 'prm'),
    '62413',
    'common'
) ,
(
    (select id from mtgcard where name = 'Candelabra of Tawnos'),
    (select id from sets where short_name = 'prm'),
    '62531',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Breaking // Entering'),
    (select id from sets where short_name = 'prm'),
    '48586',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silent Sentinel'),
    (select id from sets where short_name = 'prm'),
    '51910',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Drain'),
    (select id from sets where short_name = 'prm'),
    '46934',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skyship Weatherlight'),
    (select id from sets where short_name = 'prm'),
    '62381',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archfiend of Depravity'),
    (select id from sets where short_name = 'prm'),
    '55711',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Visionary'),
    (select id from sets where short_name = 'prm'),
    '36865',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azorius Signet'),
    (select id from sets where short_name = 'prm'),
    '62395',
    'common'
) ,
(
    (select id from mtgcard where name = 'Browbeat'),
    (select id from sets where short_name = 'prm'),
    '36198',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Underground Sea'),
    (select id from sets where short_name = 'prm'),
    '43624',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wasteland'),
    (select id from sets where short_name = 'prm'),
    '55874',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'prm'),
    '35952',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plague Myr'),
    (select id from sets where short_name = 'prm'),
    '39640',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dissolve'),
    (select id from sets where short_name = 'prm'),
    '52336',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Latch Seeker'),
    (select id from sets where short_name = 'prm'),
    '44315',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Time Warp'),
    (select id from sets where short_name = 'prm'),
    '36044',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deep Analysis'),
    (select id from sets where short_name = 'prm'),
    '35150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'prm'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'prm'),
    '31379',
    'common'
) ,
(
    (select id from mtgcard where name = 'Primal Command'),
    (select id from sets where short_name = 'prm'),
    '62489',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Rabblemaster'),
    (select id from sets where short_name = 'prm'),
    '53832',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightveil Specter'),
    (select id from sets where short_name = 'prm'),
    '47995',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cathedral of War'),
    (select id from sets where short_name = 'prm'),
    '45215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'prm'),
    '32005',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glorious Anthem'),
    (select id from sets where short_name = 'prm'),
    '35110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'prm'),
    '40066',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Crypt'),
    (select id from sets where short_name = 'prm'),
    '35962',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'prm'),
    '53881',
    'common'
) ,
(
    (select id from mtgcard where name = 'Realm-Cloaked Giant // Cast Off'),
    (select id from sets where short_name = 'prm'),
    '78798',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ghalta, Primal Hunger'),
    (select id from sets where short_name = 'prm'),
    '66896',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ramunap Excavator'),
    (select id from sets where short_name = 'prm'),
    '64991',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Honor of the Pure'),
    (select id from sets where short_name = 'prm'),
    '43554',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Signet'),
    (select id from sets where short_name = 'prm'),
    '62393',
    'common'
) ,
(
    (select id from mtgcard where name = 'Courser of Kruphix'),
    (select id from sets where short_name = 'prm'),
    '55735',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boltwing Marauder'),
    (select id from sets where short_name = 'prm'),
    '56998',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boomerang'),
    (select id from sets where short_name = 'prm'),
    '36228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Supreme Verdict'),
    (select id from sets where short_name = 'prm'),
    '46879',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Helm of Obedience'),
    (select id from sets where short_name = 'prm'),
    '65642',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Elvish Lyrist'),
    (select id from sets where short_name = 'prm'),
    '36118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blighted Fen'),
    (select id from sets where short_name = 'prm'),
    '60464',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foulmire Knight // Profane Insight'),
    (select id from sets where short_name = 'prm'),
    '78806',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arc Lightning'),
    (select id from sets where short_name = 'prm'),
    '36116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Consume the Meek'),
    (select id from sets where short_name = 'prm'),
    '62491',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = 'prm'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Meddling Mage'),
    (select id from sets where short_name = 'prm'),
    '36304',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sarkhan the Masterless'),
    (select id from sets where short_name = 'prm'),
    '72257',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saheeli''s Artistry'),
    (select id from sets where short_name = 'prm'),
    '62207',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angel of Deliverance'),
    (select id from sets where short_name = 'prm'),
    '60474',
    'rare'
) 
 on conflict do nothing;
