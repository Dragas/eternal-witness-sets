insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Gideon Blackblade'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gideon Blackblade'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gideon Blackblade'),
        (select types.id from types where name = 'Gideon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Karn, Scion of Urza'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Karn, Scion of Urza'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Karn, Scion of Urza'),
        (select types.id from types where name = 'Karn')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tamiyo, the Moon Sage'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tamiyo, the Moon Sage'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tamiyo, the Moon Sage'),
        (select types.id from types where name = 'Tamiyo')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Daretti, Ingenious Iconoclast'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Daretti, Ingenious Iconoclast'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Daretti, Ingenious Iconoclast'),
        (select types.id from types where name = 'Daretti')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nicol Bolas, Dragon-God'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nicol Bolas, Dragon-God'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nicol Bolas, Dragon-God'),
        (select types.id from types where name = 'Bolas')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ugin, the Spirit Dragon'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ugin, the Spirit Dragon'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ugin, the Spirit Dragon'),
        (select types.id from types where name = 'Ugin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nahiri, the Harbinger'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nahiri, the Harbinger'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nahiri, the Harbinger'),
        (select types.id from types where name = 'Nahiri')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Domri, Chaos Bringer'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Domri, Chaos Bringer'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Domri, Chaos Bringer'),
        (select types.id from types where name = 'Domri')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dack Fayden'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dack Fayden'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dack Fayden'),
        (select types.id from types where name = 'Dack')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Garruk, Apex Predator'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Garruk, Apex Predator'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Garruk, Apex Predator'),
        (select types.id from types where name = 'Garruk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ajani, Mentor of Heroes'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ajani, Mentor of Heroes'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ajani, Mentor of Heroes'),
        (select types.id from types where name = 'Ajani')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elspeth, Knight-Errant'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elspeth, Knight-Errant'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Elspeth, Knight-Errant'),
        (select types.id from types where name = 'Elspeth')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nicol Bolas, Planeswalker'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nicol Bolas, Planeswalker'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nicol Bolas, Planeswalker'),
        (select types.id from types where name = 'Bolas')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sarkhan Unbroken'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sarkhan Unbroken'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sarkhan Unbroken'),
        (select types.id from types where name = 'Sarkhan')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Liliana, the Last Hope'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Liliana, the Last Hope'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Liliana, the Last Hope'),
        (select types.id from types where name = 'Liliana')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Teferi, Hero of Dominaria'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Teferi, Hero of Dominaria'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Teferi, Hero of Dominaria'),
        (select types.id from types where name = 'Teferi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vraska, Golgari Queen'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vraska, Golgari Queen'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vraska, Golgari Queen'),
        (select types.id from types where name = 'Vraska')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jace, the Mind Sculptor'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jace, the Mind Sculptor'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jace, the Mind Sculptor'),
        (select types.id from types where name = 'Jace')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sorin Markov'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sorin Markov'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sorin Markov'),
        (select types.id from types where name = 'Sorin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tezzeret the Seeker'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tezzeret the Seeker'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tezzeret the Seeker'),
        (select types.id from types where name = 'Tezzeret')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tezzeret, Agent of Bolas'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tezzeret, Agent of Bolas'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tezzeret, Agent of Bolas'),
        (select types.id from types where name = 'Tezzeret')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kaya, Orzhov Usurper'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kaya, Orzhov Usurper'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kaya, Orzhov Usurper'),
        (select types.id from types where name = 'Kaya')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ral, Izzet Viceroy'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ral, Izzet Viceroy'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ral, Izzet Viceroy'),
        (select types.id from types where name = 'Ral')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jaya Ballard'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jaya Ballard'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jaya Ballard'),
        (select types.id from types where name = 'Jaya')
    ) 
 on conflict do nothing;
