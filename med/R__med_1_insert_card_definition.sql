    insert into mtgcard(name) values ('Gideon Blackblade') on conflict do nothing;
    insert into mtgcard(name) values ('Karn, Scion of Urza') on conflict do nothing;
    insert into mtgcard(name) values ('Tamiyo, the Moon Sage') on conflict do nothing;
    insert into mtgcard(name) values ('Daretti, Ingenious Iconoclast') on conflict do nothing;
    insert into mtgcard(name) values ('Nicol Bolas, Dragon-God') on conflict do nothing;
    insert into mtgcard(name) values ('Ugin, the Spirit Dragon') on conflict do nothing;
    insert into mtgcard(name) values ('Nahiri, the Harbinger') on conflict do nothing;
    insert into mtgcard(name) values ('Domri, Chaos Bringer') on conflict do nothing;
    insert into mtgcard(name) values ('Dack Fayden') on conflict do nothing;
    insert into mtgcard(name) values ('Garruk, Apex Predator') on conflict do nothing;
    insert into mtgcard(name) values ('Ajani, Mentor of Heroes') on conflict do nothing;
    insert into mtgcard(name) values ('Elspeth, Knight-Errant') on conflict do nothing;
    insert into mtgcard(name) values ('Nicol Bolas, Planeswalker') on conflict do nothing;
    insert into mtgcard(name) values ('Sarkhan Unbroken') on conflict do nothing;
    insert into mtgcard(name) values ('Liliana, the Last Hope') on conflict do nothing;
    insert into mtgcard(name) values ('Teferi, Hero of Dominaria') on conflict do nothing;
    insert into mtgcard(name) values ('Vraska, Golgari Queen') on conflict do nothing;
    insert into mtgcard(name) values ('Jace, the Mind Sculptor') on conflict do nothing;
    insert into mtgcard(name) values ('Sorin Markov') on conflict do nothing;
    insert into mtgcard(name) values ('Tezzeret the Seeker') on conflict do nothing;
    insert into mtgcard(name) values ('Tezzeret, Agent of Bolas') on conflict do nothing;
    insert into mtgcard(name) values ('Kaya, Orzhov Usurper') on conflict do nothing;
    insert into mtgcard(name) values ('Ral, Izzet Viceroy') on conflict do nothing;
    insert into mtgcard(name) values ('Jaya Ballard') on conflict do nothing;
