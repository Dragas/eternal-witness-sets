insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Gideon Blackblade'),
    (select id from sets where short_name = 'med'),
    'WS2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Karn, Scion of Urza'),
    (select id from sets where short_name = 'med'),
    'RA1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tamiyo, the Moon Sage'),
    (select id from sets where short_name = 'med'),
    'RA2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Daretti, Ingenious Iconoclast'),
    (select id from sets where short_name = 'med'),
    'GR3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, Dragon-God'),
    (select id from sets where short_name = 'med'),
    'WS6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ugin, the Spirit Dragon'),
    (select id from sets where short_name = 'med'),
    'WS1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nahiri, the Harbinger'),
    (select id from sets where short_name = 'med'),
    'WS7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Domri, Chaos Bringer'),
    (select id from sets where short_name = 'med'),
    'RA7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dack Fayden'),
    (select id from sets where short_name = 'med'),
    'RA6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Garruk, Apex Predator'),
    (select id from sets where short_name = 'med'),
    'WS5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ajani, Mentor of Heroes'),
    (select id from sets where short_name = 'med'),
    'RA5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Elspeth, Knight-Errant'),
    (select id from sets where short_name = 'med'),
    'GR1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, Planeswalker'),
    (select id from sets where short_name = 'med'),
    'GR4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sarkhan Unbroken'),
    (select id from sets where short_name = 'med'),
    'WS8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Liliana, the Last Hope'),
    (select id from sets where short_name = 'med'),
    'GR2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Teferi, Hero of Dominaria'),
    (select id from sets where short_name = 'med'),
    'GR6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vraska, Golgari Queen'),
    (select id from sets where short_name = 'med'),
    'GR8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jace, the Mind Sculptor'),
    (select id from sets where short_name = 'med'),
    'WS3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sorin Markov'),
    (select id from sets where short_name = 'med'),
    'RA3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tezzeret the Seeker'),
    (select id from sets where short_name = 'med'),
    'WS4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tezzeret, Agent of Bolas'),
    (select id from sets where short_name = 'med'),
    'GR7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kaya, Orzhov Usurper'),
    (select id from sets where short_name = 'med'),
    'RA8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ral, Izzet Viceroy'),
    (select id from sets where short_name = 'med'),
    'GR5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Jaya Ballard'),
    (select id from sets where short_name = 'med'),
    'RA4',
    'mythic'
) 
 on conflict do nothing;
