insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Browbeat'),
    (select id from sets where short_name = 'f09'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merrow Reejerey'),
    (select id from sets where short_name = 'f09'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Greaves'),
    (select id from sets where short_name = 'f09'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mulldrifter'),
    (select id from sets where short_name = 'f09'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oblivion Ring'),
    (select id from sets where short_name = 'f09'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wren''s Run Vanquisher'),
    (select id from sets where short_name = 'f09'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kitchen Finks'),
    (select id from sets where short_name = 'f09'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Murderous Redcap'),
    (select id from sets where short_name = 'f09'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Watchwolf'),
    (select id from sets where short_name = 'f09'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Elder'),
    (select id from sets where short_name = 'f09'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Myr Enforcer'),
    (select id from sets where short_name = 'f09'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magma Jet'),
    (select id from sets where short_name = 'f09'),
    '1',
    'rare'
) 
 on conflict do nothing;
