insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Zuran Orb'),
    (select id from sets where short_name = 'me1'),
    '174',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thawing Glaciers'),
    (select id from sets where short_name = 'me1'),
    '180',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fissure'),
    (select id from sets where short_name = 'me1'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brothers of Fire'),
    (select id from sets where short_name = 'me1'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Greater Realm of Preservation'),
    (select id from sets where short_name = 'me1'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wanderlust'),
    (select id from sets where short_name = 'me1'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Wizard'),
    (select id from sets where short_name = 'me1'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gargantuan Gorilla'),
    (select id from sets where short_name = 'me1'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lim-Dûl''s Vault'),
    (select id from sets where short_name = 'me1'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Serendib Efreet'),
    (select id from sets where short_name = 'me1'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stone Giant'),
    (select id from sets where short_name = 'me1'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Boon'),
    (select id from sets where short_name = 'me1'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dwarven Soldier'),
    (select id from sets where short_name = 'me1'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Argivian Archaeologist'),
    (select id from sets where short_name = 'me1'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thorn Thallid'),
    (select id from sets where short_name = 'me1'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirror Universe'),
    (select id from sets where short_name = 'me1'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animate Wall'),
    (select id from sets where short_name = 'me1'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nether Shadow'),
    (select id from sets where short_name = 'me1'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'me1'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Tortoise'),
    (select id from sets where short_name = 'me1'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mystic Remora'),
    (select id from sets where short_name = 'me1'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Homarid Spawning Bed'),
    (select id from sets where short_name = 'me1'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island of Wak-Wak'),
    (select id from sets where short_name = 'me1'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Adun Oakenshield'),
    (select id from sets where short_name = 'me1'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Diminishing Returns'),
    (select id from sets where short_name = 'me1'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clockwork Beast'),
    (select id from sets where short_name = 'me1'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'me1'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Grenade'),
    (select id from sets where short_name = 'me1'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thrull Champion'),
    (select id from sets where short_name = 'me1'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lord of Tresserhorn'),
    (select id from sets where short_name = 'me1'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghazbán Ogre'),
    (select id from sets where short_name = 'me1'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Márton Stromgald'),
    (select id from sets where short_name = 'me1'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Illusionary Forces'),
    (select id from sets where short_name = 'me1'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Psychic Purge'),
    (select id from sets where short_name = 'me1'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Granite Gargoyle'),
    (select id from sets where short_name = 'me1'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crusade'),
    (select id from sets where short_name = 'me1'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunken City'),
    (select id from sets where short_name = 'me1'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hydroblast'),
    (select id from sets where short_name = 'me1'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eater of the Dead'),
    (select id from sets where short_name = 'me1'),
    '67',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dakkon Blackblade'),
    (select id from sets where short_name = 'me1'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Amnesia'),
    (select id from sets where short_name = 'me1'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tornado'),
    (select id from sets where short_name = 'me1'),
    '136',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Paralyze'),
    (select id from sets where short_name = 'me1'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cursed Rack'),
    (select id from sets where short_name = 'me1'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Balduvian Horde'),
    (select id from sets where short_name = 'me1'),
    '86',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tawnos''s Coffin'),
    (select id from sets where short_name = 'me1'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Bauble'),
    (select id from sets where short_name = 'me1'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thicket Basilisk'),
    (select id from sets where short_name = 'me1'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Library'),
    (select id from sets where short_name = 'me1'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angry Mob'),
    (select id from sets where short_name = 'me1'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feast or Famine'),
    (select id from sets where short_name = 'me1'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'me1'),
    '181',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Transmogrant'),
    (select id from sets where short_name = 'me1'),
    '152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ankh of Mishra'),
    (select id from sets where short_name = 'me1'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Holy Light'),
    (select id from sets where short_name = 'me1'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elder Land Wurm'),
    (select id from sets where short_name = 'me1'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Breeding Pit'),
    (select id from sets where short_name = 'me1'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diamond Valley'),
    (select id from sets where short_name = 'me1'),
    '175',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Juxtapose'),
    (select id from sets where short_name = 'me1'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wyluli Wolf'),
    (select id from sets where short_name = 'me1'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Righteous Avengers'),
    (select id from sets where short_name = 'me1'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winds of Change'),
    (select id from sets where short_name = 'me1'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Engine'),
    (select id from sets where short_name = 'me1'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Ants'),
    (select id from sets where short_name = 'me1'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Erg Raiders'),
    (select id from sets where short_name = 'me1'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Su-Chi'),
    (select id from sets where short_name = 'me1'),
    '168',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fyndhorn Elves'),
    (select id from sets where short_name = 'me1'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spinal Villain'),
    (select id from sets where short_name = 'me1'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Illusions of Grandeur'),
    (select id from sets where short_name = 'me1'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Mutant'),
    (select id from sets where short_name = 'me1'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Storm Seeker'),
    (select id from sets where short_name = 'me1'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seasinger'),
    (select id from sets where short_name = 'me1'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Fallen'),
    (select id from sets where short_name = 'me1'),
    '69',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phelddagrif'),
    (select id from sets where short_name = 'me1'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Walking Wall'),
    (select id from sets where short_name = 'me1'),
    '172',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cuombajj Witches'),
    (select id from sets where short_name = 'me1'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'me1'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Force of Will'),
    (select id from sets where short_name = 'me1'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ball Lightning'),
    (select id from sets where short_name = 'me1'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chub Toad'),
    (select id from sets where short_name = 'me1'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hymn to Tourach'),
    (select id from sets where short_name = 'me1'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rainbow Vale'),
    (select id from sets where short_name = 'me1'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vesuvan Doppelganger'),
    (select id from sets where short_name = 'me1'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mindstab Thrull'),
    (select id from sets where short_name = 'me1'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyroblast'),
    (select id from sets where short_name = 'me1'),
    '107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Town'),
    (select id from sets where short_name = 'me1'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vodalian Knights'),
    (select id from sets where short_name = 'me1'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nevinyrral''s Disk'),
    (select id from sets where short_name = 'me1'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serpent Generator'),
    (select id from sets where short_name = 'me1'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ivory Tower'),
    (select id from sets where short_name = 'me1'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rabid Wombat'),
    (select id from sets where short_name = 'me1'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ice Storm'),
    (select id from sets where short_name = 'me1'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hyalopterous Lemure'),
    (select id from sets where short_name = 'me1'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian War Beast'),
    (select id from sets where short_name = 'me1'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sea Sprite'),
    (select id from sets where short_name = 'me1'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Chirurgeon'),
    (select id from sets where short_name = 'me1'),
    '94',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'me1'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'me1'),
    '183',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jacques le Vert'),
    (select id from sets where short_name = 'me1'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Time Elemental'),
    (select id from sets where short_name = 'me1'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Primal Order'),
    (select id from sets where short_name = 'me1'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Order of Leitbur'),
    (select id from sets where short_name = 'me1'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Derelor'),
    (select id from sets where short_name = 'me1'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Singing Tree'),
    (select id from sets where short_name = 'me1'),
    '130',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ydwen Efreet'),
    (select id from sets where short_name = 'me1'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Animate Dead'),
    (select id from sets where short_name = 'me1'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shield Sphere'),
    (select id from sets where short_name = 'me1'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'me1'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armageddon'),
    (select id from sets where short_name = 'me1'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Divine Transformation'),
    (select id from sets where short_name = 'me1'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hand of Justice'),
    (select id from sets where short_name = 'me1'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Black Knight'),
    (select id from sets where short_name = 'me1'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mesa Pegasus'),
    (select id from sets where short_name = 'me1'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winter Orb'),
    (select id from sets where short_name = 'me1'),
    '173',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Berserk'),
    (select id from sets where short_name = 'me1'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Centaur Archer'),
    (select id from sets where short_name = 'me1'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'me1'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Monster'),
    (select id from sets where short_name = 'me1'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oubliette'),
    (select id from sets where short_name = 'me1'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Keldon Warlord'),
    (select id from sets where short_name = 'me1'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Order of the Ebon Hand'),
    (select id from sets where short_name = 'me1'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Contagion'),
    (select id from sets where short_name = 'me1'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orcish Mechanics'),
    (select id from sets where short_name = 'me1'),
    '106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Exile'),
    (select id from sets where short_name = 'me1'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'me1'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shambling Strider'),
    (select id from sets where short_name = 'me1'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychic Venom'),
    (select id from sets where short_name = 'me1'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knights of Thorn'),
    (select id from sets where short_name = 'me1'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dust to Dust'),
    (select id from sets where short_name = 'me1'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Khabál Ghoul'),
    (select id from sets where short_name = 'me1'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jokulhaups'),
    (select id from sets where short_name = 'me1'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lake of the Dead'),
    (select id from sets where short_name = 'me1'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Autumn Willow'),
    (select id from sets where short_name = 'me1'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dwarven Catapult'),
    (select id from sets where short_name = 'me1'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = 'me1'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Benalish Hero'),
    (select id from sets where short_name = 'me1'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eureka'),
    (select id from sets where short_name = 'me1'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mana Flare'),
    (select id from sets where short_name = 'me1'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tivadar''s Crusade'),
    (select id from sets where short_name = 'me1'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Carnivorous Plant'),
    (select id from sets where short_name = 'me1'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'me1'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Juzám Djinn'),
    (select id from sets where short_name = 'me1'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'me1'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seraph'),
    (select id from sets where short_name = 'me1'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shield of the Ages'),
    (select id from sets where short_name = 'me1'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain Yeti'),
    (select id from sets where short_name = 'me1'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hymn of Rebirth'),
    (select id from sets where short_name = 'me1'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'me1'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'High Tide'),
    (select id from sets where short_name = 'me1'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Word of Undoing'),
    (select id from sets where short_name = 'me1'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Polar Kraken'),
    (select id from sets where short_name = 'me1'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forcefield'),
    (select id from sets where short_name = 'me1'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblins of the Flarg'),
    (select id from sets where short_name = 'me1'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thrull Retainer'),
    (select id from sets where short_name = 'me1'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hungry Mist'),
    (select id from sets where short_name = 'me1'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Illusionary Wall'),
    (select id from sets where short_name = 'me1'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hecatomb'),
    (select id from sets where short_name = 'me1'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Urza''s Chalice'),
    (select id from sets where short_name = 'me1'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Telekinesis'),
    (select id from sets where short_name = 'me1'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Icatian Lieutenant'),
    (select id from sets where short_name = 'me1'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Winter Blast'),
    (select id from sets where short_name = 'me1'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'me1'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blight'),
    (select id from sets where short_name = 'me1'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Preacher'),
    (select id from sets where short_name = 'me1'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hallowed Ground'),
    (select id from sets where short_name = 'me1'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pox'),
    (select id from sets where short_name = 'me1'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'me1'),
    '190',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Speakers'),
    (select id from sets where short_name = 'me1'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moat'),
    (select id from sets where short_name = 'me1'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bestial Fury'),
    (select id from sets where short_name = 'me1'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spectral Bears'),
    (select id from sets where short_name = 'me1'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Baron Sengir'),
    (select id from sets where short_name = 'me1'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'River Merfolk'),
    (select id from sets where short_name = 'me1'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stone Calendar'),
    (select id from sets where short_name = 'me1'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Petra Sphinx'),
    (select id from sets where short_name = 'me1'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Death Ward'),
    (select id from sets where short_name = 'me1'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Varchild''s War-Riders'),
    (select id from sets where short_name = 'me1'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scryb Sprites'),
    (select id from sets where short_name = 'me1'),
    '128',
    'common'
) ,
(
    (select id from mtgcard where name = 'Artifact Blast'),
    (select id from sets where short_name = 'me1'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'me1'),
    '192',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nature''s Lore'),
    (select id from sets where short_name = 'me1'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arcane Denial'),
    (select id from sets where short_name = 'me1'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Energy Arc'),
    (select id from sets where short_name = 'me1'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'me1'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chains of Mephistopheles'),
    (select id from sets where short_name = 'me1'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ifh-Bíff Efreet'),
    (select id from sets where short_name = 'me1'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Basal Thrull'),
    (select id from sets where short_name = 'me1'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crookshank Kobolds'),
    (select id from sets where short_name = 'me1'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire Covenant'),
    (select id from sets where short_name = 'me1'),
    '145',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thunder Spirit'),
    (select id from sets where short_name = 'me1'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Copper Tablet'),
    (select id from sets where short_name = 'me1'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ring of Ma''rûf'),
    (select id from sets where short_name = 'me1'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roots'),
    (select id from sets where short_name = 'me1'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Onulet'),
    (select id from sets where short_name = 'me1'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Apprentice Wizard'),
    (select id from sets where short_name = 'me1'),
    '30',
    'common'
) 
 on conflict do nothing;
