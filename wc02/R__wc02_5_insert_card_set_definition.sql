insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'rl349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Nishoba'),
    (select id from sets where short_name = 'wc02'),
    'bk140sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Coast'),
    (select id from sets where short_name = 'wc02'),
    'rl143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Werebear'),
    (select id from sets where short_name = 'wc02'),
    'rl282',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anurid Brushhopper'),
    (select id from sets where short_name = 'wc02'),
    'bk137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roar of the Wurm'),
    (select id from sets where short_name = 'wc02'),
    'shh266sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Intrepid Hero'),
    (select id from sets where short_name = 'wc02'),
    'bk22sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'bk348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Call of the Herd'),
    (select id from sets where short_name = 'wc02'),
    'bk231a',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Repulse'),
    (select id from sets where short_name = 'wc02'),
    'rl70sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc02'),
    'bk343',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'rl338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire // Ice'),
    (select id from sets where short_name = 'wc02'),
    'bk128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Upheaval'),
    (select id from sets where short_name = 'wc02'),
    'cr113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Roar of the Wurm'),
    (select id from sets where short_name = 'wc02'),
    'rl266',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coffin Purge'),
    (select id from sets where short_name = 'wc02'),
    'cr124sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simoon'),
    (select id from sets where short_name = 'wc02'),
    'shh272sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ray of Revelation'),
    (select id from sets where short_name = 'wc02'),
    'shh20sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'wc02'),
    'bk327sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Counterspell'),
    (select id from sets where short_name = 'wc02'),
    'cr67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'rl347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Genesis'),
    (select id from sets where short_name = 'wc02'),
    'bk117sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'shh350',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squirrel Nest'),
    (select id from sets where short_name = 'wc02'),
    'shh274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glory'),
    (select id from sets where short_name = 'wc02'),
    'bk11sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'rl336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = 'wc02'),
    'bk231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'rl329',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'rl332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'cr332',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circular Logic'),
    (select id from sets where short_name = 'wc02'),
    'cr33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Repulse'),
    (select id from sets where short_name = 'wc02'),
    'cr70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brian Kibler Bio'),
    (select id from sets where short_name = 'wc02'),
    'bk0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'shh330',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quiet Speculation'),
    (select id from sets where short_name = 'wc02'),
    'shh49sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Genesis'),
    (select id from sets where short_name = 'wc02'),
    'rl117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'shh338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'bk328',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'cr338',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'shh335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'rl328',
    'common'
) ,
(
    (select id from mtgcard where name = 'Slay'),
    (select id from sets where short_name = 'wc02'),
    'cr55sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merfolk Looter'),
    (select id from sets where short_name = 'wc02'),
    'shh89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'wc02'),
    'bk341',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'cr337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'rl333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brushland'),
    (select id from sets where short_name = 'wc02'),
    'bk326',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Careful Study'),
    (select id from sets where short_name = 'wc02'),
    'rl70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rushing River'),
    (select id from sets where short_name = 'wc02'),
    'rl30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'rl348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'wc02'),
    'bk333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gainsay'),
    (select id from sets where short_name = 'wc02'),
    'shh26sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Living Wish'),
    (select id from sets where short_name = 'wc02'),
    'bk124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'cr335b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'wc02'),
    'cr131sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'City of Brass'),
    (select id from sets where short_name = 'wc02'),
    'bk327',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carlos Romão Decklist'),
    (select id from sets where short_name = 'wc02'),
    'cr0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'rl336a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'bk330',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disrupt'),
    (select id from sets where short_name = 'wc02'),
    'rl51sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sylvan Safekeeper'),
    (select id from sets where short_name = 'wc02'),
    'bk133sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'rl337a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Coast'),
    (select id from sets where short_name = 'wc02'),
    'shh143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gainsay'),
    (select id from sets where short_name = 'wc02'),
    'cr26sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darkwater Catacombs'),
    (select id from sets where short_name = 'wc02'),
    'cr319',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sim Han How Bio'),
    (select id from sets where short_name = 'wc02'),
    'shh0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mana Short'),
    (select id from sets where short_name = 'wc02'),
    'cr86sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'rl334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'wc02'),
    'bk253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Recoil'),
    (select id from sets where short_name = 'wc02'),
    'cr264sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karplusan Forest'),
    (select id from sets where short_name = 'wc02'),
    'bk336',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sim Han How Decklist'),
    (select id from sets where short_name = 'wc02'),
    'shh0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rushing River'),
    (select id from sets where short_name = 'wc02'),
    'rl30sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cephalid Coliseum'),
    (select id from sets where short_name = 'wc02'),
    'cr317',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Salt Marsh'),
    (select id from sets where short_name = 'wc02'),
    'cr326',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deep Analysis'),
    (select id from sets where short_name = 'wc02'),
    'shh36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Compost'),
    (select id from sets where short_name = 'wc02'),
    'rl235sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raphael Levy Bio'),
    (select id from sets where short_name = 'wc02'),
    'rl0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu'),
    (select id from sets where short_name = 'wc02'),
    'shh60sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu'),
    (select id from sets where short_name = 'wc02'),
    'shh60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chainer''s Edict'),
    (select id from sets where short_name = 'wc02'),
    'cr57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'bk329',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mental Note'),
    (select id from sets where short_name = 'wc02'),
    'rl46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simoon'),
    (select id from sets where short_name = 'wc02'),
    'bk272sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'rl337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raphael Levy Decklist'),
    (select id from sets where short_name = 'wc02'),
    'rl0b',
    'common'
) ,
(
    (select id from mtgcard where name = '2002 World Championships Ad'),
    (select id from sets where short_name = 'wc02'),
    '0',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hibernation'),
    (select id from sets where short_name = 'wc02'),
    'cr79sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'shh347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Centaur'),
    (select id from sets where short_name = 'wc02'),
    'shh127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'cr336',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'cr337a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'shh329',
    'common'
) ,
(
    (select id from mtgcard where name = 'Birds of Paradise'),
    (select id from sets where short_name = 'wc02'),
    'shh231',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Global Ruin'),
    (select id from sets where short_name = 'wc02'),
    'bk18sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc02'),
    'cr342',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'shh328',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fact or Fiction'),
    (select id from sets where short_name = 'wc02'),
    'cr57a',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Breakthrough'),
    (select id from sets where short_name = 'wc02'),
    'rl26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightscape Familiar'),
    (select id from sets where short_name = 'wc02'),
    'cr48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc02'),
    'cr341',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'cr334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opposition'),
    (select id from sets where short_name = 'wc02'),
    'shh92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'rl331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shivan Reef'),
    (select id from sets where short_name = 'wc02'),
    'shh142',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cunning Wish'),
    (select id from sets where short_name = 'wc02'),
    'cr37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Carlos Romão Bio'),
    (select id from sets where short_name = 'wc02'),
    'cr0a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reprisal'),
    (select id from sets where short_name = 'wc02'),
    'bk33sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'rl330',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Mongrel'),
    (select id from sets where short_name = 'wc02'),
    'shh283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'rl335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Centaur'),
    (select id from sets where short_name = 'wc02'),
    'bk127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghastly Demise'),
    (select id from sets where short_name = 'wc02'),
    'cr139sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phantom Centaur'),
    (select id from sets where short_name = 'wc02'),
    'rl127sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thornscape Battlemage'),
    (select id from sets where short_name = 'wc02'),
    'bk94sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'shh331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'cr335a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karplusan Forest'),
    (select id from sets where short_name = 'wc02'),
    'shh336',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nimble Mongoose'),
    (select id from sets where short_name = 'wc02'),
    'rl258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Response'),
    (select id from sets where short_name = 'wc02'),
    'cr78sb',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'bk331',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu'),
    (select id from sets where short_name = 'wc02'),
    'bk60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glory'),
    (select id from sets where short_name = 'wc02'),
    'bk11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Seton''s Scout'),
    (select id from sets where short_name = 'wc02'),
    'rl138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'bk347',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunderscape Battlemage'),
    (select id from sets where short_name = 'wc02'),
    'bk75sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Psychatog'),
    (select id from sets where short_name = 'wc02'),
    'cr292',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ray of Revelation'),
    (select id from sets where short_name = 'wc02'),
    'rl20sb',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wax // Wane'),
    (select id from sets where short_name = 'wc02'),
    'bk296',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brian Kibler Decklist'),
    (select id from sets where short_name = 'wc02'),
    'bk0b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Memory Lapse'),
    (select id from sets where short_name = 'wc02'),
    'cr88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'shh337',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'rl350',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circular Logic'),
    (select id from sets where short_name = 'wc02'),
    'shh33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Mongrel'),
    (select id from sets where short_name = 'wc02'),
    'bk283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'wc02'),
    'bk346',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'shh334',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'cr333',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'wc02'),
    'cr340',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'wc02'),
    'bk331a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fire // Ice'),
    (select id from sets where short_name = 'wc02'),
    'shh128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'bk349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blank Card'),
    (select id from sets where short_name = 'wc02'),
    '00',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'shh348',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'cr335',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'wc02'),
    'shh336a',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'wc02'),
    'shh253',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wonder'),
    (select id from sets where short_name = 'wc02'),
    'rl54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'wc02'),
    'shh349',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild Mongrel'),
    (select id from sets where short_name = 'wc02'),
    'rl283',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fact or Fiction'),
    (select id from sets where short_name = 'wc02'),
    'cr57sb',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deep Analysis'),
    (select id from sets where short_name = 'wc02'),
    'cr36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Underground River'),
    (select id from sets where short_name = 'wc02'),
    'cr350',
    'rare'
) 
 on conflict do nothing;
