insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Oreskos Swiftclaw'),
    (select id from sets where short_name = 'jou'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gnarled Scarhide'),
    (select id from sets where short_name = 'jou'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hubris'),
    (select id from sets where short_name = 'jou'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bearer of the Heavens'),
    (select id from sets where short_name = 'jou'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Countermand'),
    (select id from sets where short_name = 'jou'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dreadbringer Lampads'),
    (select id from sets where short_name = 'jou'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scourge of Fleets'),
    (select id from sets where short_name = 'jou'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cast into Darkness'),
    (select id from sets where short_name = 'jou'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nyx Weaver'),
    (select id from sets where short_name = 'jou'),
    '153',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rotted Hulk'),
    (select id from sets where short_name = 'jou'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fleetfeather Cockatrice'),
    (select id from sets where short_name = 'jou'),
    '149',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tormented Thoughts'),
    (select id from sets where short_name = 'jou'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Starfall'),
    (select id from sets where short_name = 'jou'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nyx Infusion'),
    (select id from sets where short_name = 'jou'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golden Hind'),
    (select id from sets where short_name = 'jou'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Agent of Erebos'),
    (select id from sets where short_name = 'jou'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ravenous Leucrocota'),
    (select id from sets where short_name = 'jou'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oppressive Rays'),
    (select id from sets where short_name = 'jou'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Twinflame'),
    (select id from sets where short_name = 'jou'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ritual of the Returned'),
    (select id from sets where short_name = 'jou'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kiora''s Dismissal'),
    (select id from sets where short_name = 'jou'),
    '44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Renowned Weaver'),
    (select id from sets where short_name = 'jou'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Font of Fortunes'),
    (select id from sets where short_name = 'jou'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kruphix, God of Horizons'),
    (select id from sets where short_name = 'jou'),
    '152',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Godsend'),
    (select id from sets where short_name = 'jou'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Polymorphous Rush'),
    (select id from sets where short_name = 'jou'),
    '46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nature''s Panoply'),
    (select id from sets where short_name = 'jou'),
    '131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flamespeaker''s Will'),
    (select id from sets where short_name = 'jou'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flurry of Horns'),
    (select id from sets where short_name = 'jou'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reprisal'),
    (select id from sets where short_name = 'jou'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sigiled Skink'),
    (select id from sets where short_name = 'jou'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heroes'' Bane'),
    (select id from sets where short_name = 'jou'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Setessan Tactics'),
    (select id from sets where short_name = 'jou'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lagonna-Band Trailblazer'),
    (select id from sets where short_name = 'jou'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hypnotic Siren'),
    (select id from sets where short_name = 'jou'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harvestguard Alseids'),
    (select id from sets where short_name = 'jou'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rollick of Abandon'),
    (select id from sets where short_name = 'jou'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Whitewater Naiads'),
    (select id from sets where short_name = 'jou'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Font of Ire'),
    (select id from sets where short_name = 'jou'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armament of Nyx'),
    (select id from sets where short_name = 'jou'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desecration Plague'),
    (select id from sets where short_name = 'jou'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cyclops of Eternal Fury'),
    (select id from sets where short_name = 'jou'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battlefield Thaumaturge'),
    (select id from sets where short_name = 'jou'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reviving Melody'),
    (select id from sets where short_name = 'jou'),
    '138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ajani, Mentor of Heroes'),
    (select id from sets where short_name = 'jou'),
    '145',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stonewise Fortifier'),
    (select id from sets where short_name = 'jou'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Triton Cavalry'),
    (select id from sets where short_name = 'jou'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nightmarish End'),
    (select id from sets where short_name = 'jou'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thassa''s Ire'),
    (select id from sets where short_name = 'jou'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Diadem'),
    (select id from sets where short_name = 'jou'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Felhide Petrifier'),
    (select id from sets where short_name = 'jou'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gold-Forged Sentinel'),
    (select id from sets where short_name = 'jou'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mana Confluence'),
    (select id from sets where short_name = 'jou'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nessian Game Warden'),
    (select id from sets where short_name = 'jou'),
    '132',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Athreos, God of Passage'),
    (select id from sets where short_name = 'jou'),
    '146',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Spite of Mogis'),
    (select id from sets where short_name = 'jou'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bladetusk Boar'),
    (select id from sets where short_name = 'jou'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deicide'),
    (select id from sets where short_name = 'jou'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hall of Triumph'),
    (select id from sets where short_name = 'jou'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rouse the Mob'),
    (select id from sets where short_name = 'jou'),
    '109',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mortal Obstinacy'),
    (select id from sets where short_name = 'jou'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Extinguish All Hope'),
    (select id from sets where short_name = 'jou'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Interpret the Signs'),
    (select id from sets where short_name = 'jou'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thoughtrender Lamia'),
    (select id from sets where short_name = 'jou'),
    '85',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dictate of the Twin Gods'),
    (select id from sets where short_name = 'jou'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hydra Broodmaster'),
    (select id from sets where short_name = 'jou'),
    '128',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grim Guardian'),
    (select id from sets where short_name = 'jou'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prophetic Flamespeaker'),
    (select id from sets where short_name = 'jou'),
    '106',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hour of Need'),
    (select id from sets where short_name = 'jou'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pharika, God of Affliction'),
    (select id from sets where short_name = 'jou'),
    '154',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Supply-Line Cranes'),
    (select id from sets where short_name = 'jou'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloaked Siren'),
    (select id from sets where short_name = 'jou'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armory of Iroas'),
    (select id from sets where short_name = 'jou'),
    '158',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knowledge and Power'),
    (select id from sets where short_name = 'jou'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leonin Iconoclast'),
    (select id from sets where short_name = 'jou'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Akroan Mastiff'),
    (select id from sets where short_name = 'jou'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sightless Brawler'),
    (select id from sets where short_name = 'jou'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phalanx Formation'),
    (select id from sets where short_name = 'jou'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pharika''s Chosen'),
    (select id from sets where short_name = 'jou'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pheres-Band Warchief'),
    (select id from sets where short_name = 'jou'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swarmborn Giant'),
    (select id from sets where short_name = 'jou'),
    '144',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sigiled Starfish'),
    (select id from sets where short_name = 'jou'),
    '52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Triton Shorestalker'),
    (select id from sets where short_name = 'jou'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crystalline Nautilus'),
    (select id from sets where short_name = 'jou'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sage of Hours'),
    (select id from sets where short_name = 'jou'),
    '50',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'War-Wing Siren'),
    (select id from sets where short_name = 'jou'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eidolon of Blossoms'),
    (select id from sets where short_name = 'jou'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dakra Mystic'),
    (select id from sets where short_name = 'jou'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Worst Fears'),
    (select id from sets where short_name = 'jou'),
    '87',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Deserter''s Quarters'),
    (select id from sets where short_name = 'jou'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pensive Minotaur'),
    (select id from sets where short_name = 'jou'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skybind'),
    (select id from sets where short_name = 'jou'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kruphix''s Insight'),
    (select id from sets where short_name = 'jou'),
    '129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dictate of Heliod'),
    (select id from sets where short_name = 'jou'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bassara Tower Archer'),
    (select id from sets where short_name = 'jou'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magma Spray'),
    (select id from sets where short_name = 'jou'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nyx-Fleece Ram'),
    (select id from sets where short_name = 'jou'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Riddle of Lightning'),
    (select id from sets where short_name = 'jou'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Solidarity of Heroes'),
    (select id from sets where short_name = 'jou'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harness by Force'),
    (select id from sets where short_name = 'jou'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Returned Reveler'),
    (select id from sets where short_name = 'jou'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dictate of Erebos'),
    (select id from sets where short_name = 'jou'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pull from the Deep'),
    (select id from sets where short_name = 'jou'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pheres-Band Thunderhoof'),
    (select id from sets where short_name = 'jou'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akroan Line Breaker'),
    (select id from sets where short_name = 'jou'),
    '88',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cruel Feeding'),
    (select id from sets where short_name = 'jou'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eidolon of the Great Revel'),
    (select id from sets where short_name = 'jou'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keranos, God of Storms'),
    (select id from sets where short_name = 'jou'),
    '151',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Spirespine'),
    (select id from sets where short_name = 'jou'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Font of Fertility'),
    (select id from sets where short_name = 'jou'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brain Maggot'),
    (select id from sets where short_name = 'jou'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Squelching Leeches'),
    (select id from sets where short_name = 'jou'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Doomwake Giant'),
    (select id from sets where short_name = 'jou'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forgeborn Oreads'),
    (select id from sets where short_name = 'jou'),
    '98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Satyr Hoplite'),
    (select id from sets where short_name = 'jou'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temple of Epiphany'),
    (select id from sets where short_name = 'jou'),
    '164',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Launch the Fleet'),
    (select id from sets where short_name = 'jou'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aspect of Gorgon'),
    (select id from sets where short_name = 'jou'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pin to the Earth'),
    (select id from sets where short_name = 'jou'),
    '45',
    'common'
) ,
(
    (select id from mtgcard where name = 'Font of Return'),
    (select id from sets where short_name = 'jou'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chariot of Victory'),
    (select id from sets where short_name = 'jou'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Market Festival'),
    (select id from sets where short_name = 'jou'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rise of Eagles'),
    (select id from sets where short_name = 'jou'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eidolon of Rhetoric'),
    (select id from sets where short_name = 'jou'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Colossal Heroics'),
    (select id from sets where short_name = 'jou'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Strength from the Fallen'),
    (select id from sets where short_name = 'jou'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skyspear Cavalry'),
    (select id from sets where short_name = 'jou'),
    '26',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daring Thief'),
    (select id from sets where short_name = 'jou'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Iroas, God of Victory'),
    (select id from sets where short_name = 'jou'),
    '150',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goldenhide Ox'),
    (select id from sets where short_name = 'jou'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Satyr Grovedancer'),
    (select id from sets where short_name = 'jou'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thassa''s Devourer'),
    (select id from sets where short_name = 'jou'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Font of Vigor'),
    (select id from sets where short_name = 'jou'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dictate of Karametra'),
    (select id from sets where short_name = 'jou'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Master of the Feast'),
    (select id from sets where short_name = 'jou'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silence the Believers'),
    (select id from sets where short_name = 'jou'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wildfire Cerberus'),
    (select id from sets where short_name = 'jou'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oakheart Dryads'),
    (select id from sets where short_name = 'jou'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tethmos High Priest'),
    (select id from sets where short_name = 'jou'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quarry Colossus'),
    (select id from sets where short_name = 'jou'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Godhunter Octopus'),
    (select id from sets where short_name = 'jou'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stormchaser Chimera'),
    (select id from sets where short_name = 'jou'),
    '156',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eagle of the Watch'),
    (select id from sets where short_name = 'jou'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Feast of Dreams'),
    (select id from sets where short_name = 'jou'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blinding Flare'),
    (select id from sets where short_name = 'jou'),
    '91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spawn of Thraxes'),
    (select id from sets where short_name = 'jou'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Revel of the Fallen God'),
    (select id from sets where short_name = 'jou'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gluttonous Cyclops'),
    (select id from sets where short_name = 'jou'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ajani''s Presence'),
    (select id from sets where short_name = 'jou'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Desperate Stand'),
    (select id from sets where short_name = 'jou'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dictate of Kruphix'),
    (select id from sets where short_name = 'jou'),
    '37',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Riptide Chimera'),
    (select id from sets where short_name = 'jou'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Underworld Coinsmith'),
    (select id from sets where short_name = 'jou'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dawnbringer Charioteers'),
    (select id from sets where short_name = 'jou'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Consign to Dust'),
    (select id from sets where short_name = 'jou'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Humbler of Mortals'),
    (select id from sets where short_name = 'jou'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodcrazed Hoplite'),
    (select id from sets where short_name = 'jou'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aegis of the Gods'),
    (select id from sets where short_name = 'jou'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mogis''s Warhound'),
    (select id from sets where short_name = 'jou'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Temple of Malady'),
    (select id from sets where short_name = 'jou'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spiteful Blow'),
    (select id from sets where short_name = 'jou'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Banishing Light'),
    (select id from sets where short_name = 'jou'),
    '5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Disciple of Deceit'),
    (select id from sets where short_name = 'jou'),
    '148',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aerial Formation'),
    (select id from sets where short_name = 'jou'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'King Macar, the Gold-Cursed'),
    (select id from sets where short_name = 'jou'),
    '74',
    'rare'
) 
 on conflict do nothing;
