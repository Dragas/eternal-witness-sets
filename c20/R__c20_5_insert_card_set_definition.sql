insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Odric, Master Tactician'),
    (select id from sets where short_name = 'c20'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harmonize'),
    (select id from sets where short_name = 'c20'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Alesha, Who Smiles at Death'),
    (select id from sets where short_name = 'c20'),
    '143',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temur Charm'),
    (select id from sets where short_name = 'c20'),
    '230',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nikara, Lair Scavenger'),
    (select id from sets where short_name = 'c20'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bojuka Bog'),
    (select id from sets where short_name = 'c20'),
    '259',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kelsien, the Plague'),
    (select id from sets where short_name = 'c20'),
    '11',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tayam, Luminous Enigma'),
    (select id from sets where short_name = 'c20'),
    '16',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Comet Storm'),
    (select id from sets where short_name = 'c20'),
    '148',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pako, Arcane Retriever'),
    (select id from sets where short_name = 'c20'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Trygon Predator'),
    (select id from sets where short_name = 'c20'),
    '232',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Secluded Steppe'),
    (select id from sets where short_name = 'c20'),
    '307',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Verge Rangers'),
    (select id from sets where short_name = 'c20'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Astral Drift'),
    (select id from sets where short_name = 'c20'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chemister''s Insight'),
    (select id from sets where short_name = 'c20'),
    '108',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shadowblood Ridge'),
    (select id from sets where short_name = 'c20'),
    '309',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shiny Impetus'),
    (select id from sets where short_name = 'c20'),
    '55',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beast Whisperer'),
    (select id from sets where short_name = 'c20'),
    '167',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Capricopian'),
    (select id from sets where short_name = 'c20'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hieroglyphic Illumination'),
    (select id from sets where short_name = 'c20'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Increasing Devotion'),
    (select id from sets where short_name = 'c20'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Whiplash Trap'),
    (select id from sets where short_name = 'c20'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thraben Doomsayer'),
    (select id from sets where short_name = 'c20'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desert of the Fervent'),
    (select id from sets where short_name = 'c20'),
    '266',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vigilante Justice'),
    (select id from sets where short_name = 'c20'),
    '164',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shriekmaw'),
    (select id from sets where short_name = 'c20'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yannik, Scavenging Sentinel'),
    (select id from sets where short_name = 'c20'),
    '19',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Curator of Mysteries'),
    (select id from sets where short_name = 'c20'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Predatory Impetus'),
    (select id from sets where short_name = 'c20'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arcane Signet'),
    (select id from sets where short_name = 'c20'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bounty Agent'),
    (select id from sets where short_name = 'c20'),
    '78',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desert of the Mindful'),
    (select id from sets where short_name = 'c20'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trynn, Champion of Freedom'),
    (select id from sets where short_name = 'c20'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Haldan, Avid Arcanist'),
    (select id from sets where short_name = 'c20'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Deadly Tempest'),
    (select id from sets where short_name = 'c20'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Starstorm'),
    (select id from sets where short_name = 'c20'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evolution Charm'),
    (select id from sets where short_name = 'c20'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terminate'),
    (select id from sets where short_name = 'c20'),
    '231',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cartographer''s Hawk'),
    (select id from sets where short_name = 'c20'),
    '24',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rupture Spire'),
    (select id from sets where short_name = 'c20'),
    '304',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cast Out'),
    (select id from sets where short_name = 'c20'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Izzet Signet'),
    (select id from sets where short_name = 'c20'),
    '243',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Isperia, Supreme Judge'),
    (select id from sets where short_name = 'c20'),
    '217',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Commune with Lava'),
    (select id from sets where short_name = 'c20'),
    '149',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strength of the Tajuru'),
    (select id from sets where short_name = 'c20'),
    '192',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azorius Signet'),
    (select id from sets where short_name = 'c20'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deadbridge Chant'),
    (select id from sets where short_name = 'c20'),
    '207',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Frontier Warmonger'),
    (select id from sets where short_name = 'c20'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akim, the Soaring Wind'),
    (select id from sets where short_name = 'c20'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Abandoned Sarcophagus'),
    (select id from sets where short_name = 'c20'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slice in Twain'),
    (select id from sets where short_name = 'c20'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heroes'' Bane'),
    (select id from sets where short_name = 'c20'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avenging Huntbonder'),
    (select id from sets where short_name = 'c20'),
    '22',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mercurial Chemister'),
    (select id from sets where short_name = 'c20'),
    '221',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Martial Impetus'),
    (select id from sets where short_name = 'c20'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deathsprout'),
    (select id from sets where short_name = 'c20'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Smoldering Crater'),
    (select id from sets where short_name = 'c20'),
    '313',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daring Fiendbonder'),
    (select id from sets where short_name = 'c20'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'New Perspectives'),
    (select id from sets where short_name = 'c20'),
    '119',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sol Ring'),
    (select id from sets where short_name = 'c20'),
    '252',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Agitator Ant'),
    (select id from sets where short_name = 'c20'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Surreal Memoir'),
    (select id from sets where short_name = 'c20'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grim Backwoods'),
    (select id from sets where short_name = 'c20'),
    '278',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Karametra, God of Harvests'),
    (select id from sets where short_name = 'c20'),
    '218',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vitality Hunter'),
    (select id from sets where short_name = 'c20'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mindleecher'),
    (select id from sets where short_name = 'c20'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Izzet Boilerworks'),
    (select id from sets where short_name = 'c20'),
    '283',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Soul of Innistrad'),
    (select id from sets where short_name = 'c20'),
    '137',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mystic Monastery'),
    (select id from sets where short_name = 'c20'),
    '293',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Citywide Bust'),
    (select id from sets where short_name = 'c20'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Windfall'),
    (select id from sets where short_name = 'c20'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Coast'),
    (select id from sets where short_name = 'c20'),
    '322',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darkwater Catacombs'),
    (select id from sets where short_name = 'c20'),
    '265',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Command Tower'),
    (select id from sets where short_name = 'c20'),
    '264',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azorius Chancery'),
    (select id from sets where short_name = 'c20'),
    '256',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Charmbreaker Devils'),
    (select id from sets where short_name = 'c20'),
    '147',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Niv-Mizzet, the Firemind'),
    (select id from sets where short_name = 'c20'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Decoy Gambit'),
    (select id from sets where short_name = 'c20'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beast Within'),
    (select id from sets where short_name = 'c20'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Dark-Dwellers'),
    (select id from sets where short_name = 'c20'),
    '153',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Migratory Route'),
    (select id from sets where short_name = 'c20'),
    '222',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Desert of the True'),
    (select id from sets where short_name = 'c20'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Humble Defector'),
    (select id from sets where short_name = 'c20'),
    '154',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Myriad Landscape'),
    (select id from sets where short_name = 'c20'),
    '292',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magus of the Wheel'),
    (select id from sets where short_name = 'c20'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skycloud Expanse'),
    (select id from sets where short_name = 'c20'),
    '312',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Xyris, the Writhing Storm'),
    (select id from sets where short_name = 'c20'),
    '18',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dimir Aqueduct'),
    (select id from sets where short_name = 'c20'),
    '270',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Remote Isle'),
    (select id from sets where short_name = 'c20'),
    '302',
    'common'
) ,
(
    (select id from mtgcard where name = 'Majestic Myriarch'),
    (select id from sets where short_name = 'c20'),
    '182',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Disciple of Bolas'),
    (select id from sets where short_name = 'c20'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Riders of Gavony'),
    (select id from sets where short_name = 'c20'),
    '98',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crop Rotation'),
    (select id from sets where short_name = 'c20'),
    '169',
    'common'
) ,
(
    (select id from mtgcard where name = 'Despark'),
    (select id from sets where short_name = 'c20'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fireflux Squad'),
    (select id from sets where short_name = 'c20'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Devout Chaplain'),
    (select id from sets where short_name = 'c20'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fierce Guardianship'),
    (select id from sets where short_name = 'c20'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Rot Farm'),
    (select id from sets where short_name = 'c20'),
    '277',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ambition''s Cost'),
    (select id from sets where short_name = 'c20'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cataclysmic Gearhulk'),
    (select id from sets where short_name = 'c20'),
    '80',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Captivating Crew'),
    (select id from sets where short_name = 'c20'),
    '144',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Etali, Primal Storm'),
    (select id from sets where short_name = 'c20'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Illusory Ambusher'),
    (select id from sets where short_name = 'c20'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prophetic Bolt'),
    (select id from sets where short_name = 'c20'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lifecrafter''s Bestiary'),
    (select id from sets where short_name = 'c20'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Irrigated Farmland'),
    (select id from sets where short_name = 'c20'),
    '282',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Netherborn Altar'),
    (select id from sets where short_name = 'c20'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Talrand, Sky Summoner'),
    (select id from sets where short_name = 'c20'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Melek, Izzet Paragon'),
    (select id from sets where short_name = 'c20'),
    '220',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tribute to the Wild'),
    (select id from sets where short_name = 'c20'),
    '193',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zulaport Cutthroat'),
    (select id from sets where short_name = 'c20'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hoofprints of the Stag'),
    (select id from sets where short_name = 'c20'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sawtusk Demolisher'),
    (select id from sets where short_name = 'c20'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swiftfoot Boots'),
    (select id from sets where short_name = 'c20'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampire Nighthawk'),
    (select id from sets where short_name = 'c20'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Villainous Wealth'),
    (select id from sets where short_name = 'c20'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Curious Herd'),
    (select id from sets where short_name = 'c20'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cinder Glade'),
    (select id from sets where short_name = 'c20'),
    '263',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bonder''s Ornament'),
    (select id from sets where short_name = 'c20'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Frantic Search'),
    (select id from sets where short_name = 'c20'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Finality'),
    (select id from sets where short_name = 'c20'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skullwinder'),
    (select id from sets where short_name = 'c20'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nascent Metamorph'),
    (select id from sets where short_name = 'c20'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wydwen, the Biting Gale'),
    (select id from sets where short_name = 'c20'),
    '235',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Signet'),
    (select id from sets where short_name = 'c20'),
    '249',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Caves of Koilos'),
    (select id from sets where short_name = 'c20'),
    '262',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Otrimi, the Ever-Playful'),
    (select id from sets where short_name = 'c20'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Abzan Ascendancy'),
    (select id from sets where short_name = 'c20'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Llanowar Wastes'),
    (select id from sets where short_name = 'c20'),
    '286',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drifting Meadow'),
    (select id from sets where short_name = 'c20'),
    '271',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soaring Seacliff'),
    (select id from sets where short_name = 'c20'),
    '315',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tectonic Reformation'),
    (select id from sets where short_name = 'c20'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crackling Doom'),
    (select id from sets where short_name = 'c20'),
    '205',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Splinterfright'),
    (select id from sets where short_name = 'c20'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Growth Spiral'),
    (select id from sets where short_name = 'c20'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cultivate'),
    (select id from sets where short_name = 'c20'),
    '170',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heirloom Blade'),
    (select id from sets where short_name = 'c20'),
    '242',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mind Spring'),
    (select id from sets where short_name = 'c20'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jirina Kudro'),
    (select id from sets where short_name = 'c20'),
    '8',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Find // Finality'),
    (select id from sets where short_name = 'c20'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Garna, the Bloodflame'),
    (select id from sets where short_name = 'c20'),
    '213',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rashmi, Eternities Crafter'),
    (select id from sets where short_name = 'c20'),
    '229',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dearly Departed'),
    (select id from sets where short_name = 'c20'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magus of the Disk'),
    (select id from sets where short_name = 'c20'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Carnarium'),
    (select id from sets where short_name = 'c20'),
    '300',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace, Architect of Thought'),
    (select id from sets where short_name = 'c20'),
    '114',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gavony Township'),
    (select id from sets where short_name = 'c20'),
    '276',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sandsteppe Citadel'),
    (select id from sets where short_name = 'c20'),
    '305',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Odric, Lunarch Marshal'),
    (select id from sets where short_name = 'c20'),
    '95',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Xathrid Necromancer'),
    (select id from sets where short_name = 'c20'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Surly Badgersaur'),
    (select id from sets where short_name = 'c20'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Djinn Illuminatus'),
    (select id from sets where short_name = 'c20'),
    '210',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reveillark'),
    (select id from sets where short_name = 'c20'),
    '97',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slice and Dice'),
    (select id from sets where short_name = 'c20'),
    '159',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Manascape Refractor'),
    (select id from sets where short_name = 'c20'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mosswort Bridge'),
    (select id from sets where short_name = 'c20'),
    '291',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Archon of Valor''s Reach'),
    (select id from sets where short_name = 'c20'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Garrison'),
    (select id from sets where short_name = 'c20'),
    '260',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cold-Eyed Selkie'),
    (select id from sets where short_name = 'c20'),
    '204',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gavi, Nest Warden'),
    (select id from sets where short_name = 'c20'),
    '7',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rogue''s Passage'),
    (select id from sets where short_name = 'c20'),
    '303',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nimble Obstructionist'),
    (select id from sets where short_name = 'c20'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deflecting Swat'),
    (select id from sets where short_name = 'c20'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Profane Command'),
    (select id from sets where short_name = 'c20'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Halimar Depths'),
    (select id from sets where short_name = 'c20'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Banisher Priest'),
    (select id from sets where short_name = 'c20'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'The Locust God'),
    (select id from sets where short_name = 'c20'),
    '219',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aerial Responder'),
    (select id from sets where short_name = 'c20'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skullclamp'),
    (select id from sets where short_name = 'c20'),
    '251',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glademuse'),
    (select id from sets where short_name = 'c20'),
    '60',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Commander''s Sphere'),
    (select id from sets where short_name = 'c20'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Abzan Charm'),
    (select id from sets where short_name = 'c20'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Together Forever'),
    (select id from sets where short_name = 'c20'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kalemne''s Captain'),
    (select id from sets where short_name = 'c20'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Molten Echoes'),
    (select id from sets where short_name = 'c20'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Desolate Lighthouse'),
    (select id from sets where short_name = 'c20'),
    '269',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spinerock Knoll'),
    (select id from sets where short_name = 'c20'),
    '316',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zetalpa, Primal Dawn'),
    (select id from sets where short_name = 'c20'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nissa, Steward of Elements'),
    (select id from sets where short_name = 'c20'),
    '224',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hostile Desert'),
    (select id from sets where short_name = 'c20'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Dryad'),
    (select id from sets where short_name = 'c20'),
    '197',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crystalline Resonance'),
    (select id from sets where short_name = 'c20'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Psychic Impetus'),
    (select id from sets where short_name = 'c20'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wilderness Reclamation'),
    (select id from sets where short_name = 'c20'),
    '196',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duneblast'),
    (select id from sets where short_name = 'c20'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ajani Unyielding'),
    (select id from sets where short_name = 'c20'),
    '201',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ethereal Forager'),
    (select id from sets where short_name = 'c20'),
    '34',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shivan Reef'),
    (select id from sets where short_name = 'c20'),
    '310',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frontline Medic'),
    (select id from sets where short_name = 'c20'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Silent Arbiter'),
    (select id from sets where short_name = 'c20'),
    '250',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Descend upon the Sinful'),
    (select id from sets where short_name = 'c20'),
    '86',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Shabraz, the Skyshark'),
    (select id from sets where short_name = 'c20'),
    '14',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Spirit Cairn'),
    (select id from sets where short_name = 'c20'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wort, the Raidmother'),
    (select id from sets where short_name = 'c20'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reclamation Sage'),
    (select id from sets where short_name = 'c20'),
    '186',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brallin, Skyshark Rider'),
    (select id from sets where short_name = 'c20'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Cleansing Nova'),
    (select id from sets where short_name = 'c20'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Orzhov Signet'),
    (select id from sets where short_name = 'c20'),
    '247',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thalia''s Lieutenant'),
    (select id from sets where short_name = 'c20'),
    '103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Solemn Simulacrum'),
    (select id from sets where short_name = 'c20'),
    '253',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dismantling Wave'),
    (select id from sets where short_name = 'c20'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unexpectedly Absent'),
    (select id from sets where short_name = 'c20'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mossfire Valley'),
    (select id from sets where short_name = 'c20'),
    '290',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ash Barrens'),
    (select id from sets where short_name = 'c20'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smoldering Marsh'),
    (select id from sets where short_name = 'c20'),
    '314',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harrow'),
    (select id from sets where short_name = 'c20'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dualcaster Mage'),
    (select id from sets where short_name = 'c20'),
    '150',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ever After'),
    (select id from sets where short_name = 'c20'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kodama''s Reach'),
    (select id from sets where short_name = 'c20'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Orzhov Basilica'),
    (select id from sets where short_name = 'c20'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cryptic Trilobite'),
    (select id from sets where short_name = 'c20'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gaze of Granite'),
    (select id from sets where short_name = 'c20'),
    '214',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra, Flamecaller'),
    (select id from sets where short_name = 'c20'),
    '145',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Hunter''s Insight'),
    (select id from sets where short_name = 'c20'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Acidic Slime'),
    (select id from sets where short_name = 'c20'),
    '165',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Satyr Wayfinder'),
    (select id from sets where short_name = 'c20'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Titan of Eternal Fire'),
    (select id from sets where short_name = 'c20'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tidal Barracuda'),
    (select id from sets where short_name = 'c20'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Murmuring Mystic'),
    (select id from sets where short_name = 'c20'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eon Frolicker'),
    (select id from sets where short_name = 'c20'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Outpost Siege'),
    (select id from sets where short_name = 'c20'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selective Adaptation'),
    (select id from sets where short_name = 'c20'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crackling Drake'),
    (select id from sets where short_name = 'c20'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selesnya Sanctuary'),
    (select id from sets where short_name = 'c20'),
    '308',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kessig Wolf Run'),
    (select id from sets where short_name = 'c20'),
    '284',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vizier of Tumbling Sands'),
    (select id from sets where short_name = 'c20'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Artifact Mutation'),
    (select id from sets where short_name = 'c20'),
    '203',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blighted Woodland'),
    (select id from sets where short_name = 'c20'),
    '258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Putrefy'),
    (select id from sets where short_name = 'c20'),
    '228',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Parasitic Impetus'),
    (select id from sets where short_name = 'c20'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knight of the White Orchid'),
    (select id from sets where short_name = 'c20'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frontier Bivouac'),
    (select id from sets where short_name = 'c20'),
    '275',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Temple of the False God'),
    (select id from sets where short_name = 'c20'),
    '319',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hornet Queen'),
    (select id from sets where short_name = 'c20'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Natural Connection'),
    (select id from sets where short_name = 'c20'),
    '184',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunblast Angel'),
    (select id from sets where short_name = 'c20'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oran-Rief, the Vastwood'),
    (select id from sets where short_name = 'c20'),
    '296',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unclaimed Territory'),
    (select id from sets where short_name = 'c20'),
    '320',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prairie Stream'),
    (select id from sets where short_name = 'c20'),
    '299',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Genesis Hydra'),
    (select id from sets where short_name = 'c20'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Species Specialist'),
    (select id from sets where short_name = 'c20'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Painful Truths'),
    (select id from sets where short_name = 'c20'),
    '134',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shared Animosity'),
    (select id from sets where short_name = 'c20'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sunken Hollow'),
    (select id from sets where short_name = 'c20'),
    '318',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akroma, Angel of Wrath'),
    (select id from sets where short_name = 'c20'),
    '73',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Vastwood Hydra'),
    (select id from sets where short_name = 'c20'),
    '194',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fluctuator'),
    (select id from sets where short_name = 'c20'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zaxara, the Exemplary'),
    (select id from sets where short_name = 'c20'),
    '20',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Chaos Warp'),
    (select id from sets where short_name = 'c20'),
    '146',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Obscuring Haze'),
    (select id from sets where short_name = 'c20'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slippery Bogbonder'),
    (select id from sets where short_name = 'c20'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Predator Ooze'),
    (select id from sets where short_name = 'c20'),
    '185',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Herald of the Forgotten'),
    (select id from sets where short_name = 'c20'),
    '27',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ukkima, Stalking Shadow'),
    (select id from sets where short_name = 'c20'),
    '17',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forgotten Cave'),
    (select id from sets where short_name = 'c20'),
    '274',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cavalry Pegasus'),
    (select id from sets where short_name = 'c20'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mortuary Mire'),
    (select id from sets where short_name = 'c20'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Masked Admirers'),
    (select id from sets where short_name = 'c20'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deadly Rollick'),
    (select id from sets where short_name = 'c20'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Signet'),
    (select id from sets where short_name = 'c20'),
    '239',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lightning Rift'),
    (select id from sets where short_name = 'c20'),
    '155',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hunting Pack'),
    (select id from sets where short_name = 'c20'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Elder'),
    (select id from sets where short_name = 'c20'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lunar Mystic'),
    (select id from sets where short_name = 'c20'),
    '115',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flawless Maneuver'),
    (select id from sets where short_name = 'c20'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sanctuary Blade'),
    (select id from sets where short_name = 'c20'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ravenous Gigantotherium'),
    (select id from sets where short_name = 'c20'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Greaves'),
    (select id from sets where short_name = 'c20'),
    '245',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hungering Hydra'),
    (select id from sets where short_name = 'c20'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Opulent Palace'),
    (select id from sets where short_name = 'c20'),
    '295',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Psychosis Crawler'),
    (select id from sets where short_name = 'c20'),
    '248',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swarm Intelligence'),
    (select id from sets where short_name = 'c20'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reliquary Tower'),
    (select id from sets where short_name = 'c20'),
    '301',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nesting Grounds'),
    (select id from sets where short_name = 'c20'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sungrass Prairie'),
    (select id from sets where short_name = 'c20'),
    '317',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Path of Ancestry'),
    (select id from sets where short_name = 'c20'),
    '298',
    'common'
) ,
(
    (select id from mtgcard where name = 'Adriana, Captain of the Guard'),
    (select id from sets where short_name = 'c20'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lavabrink Floodgates'),
    (select id from sets where short_name = 'c20'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fumiko the Lowblood'),
    (select id from sets where short_name = 'c20'),
    '152',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sun Titan'),
    (select id from sets where short_name = 'c20'),
    '101',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Grisly Salvage'),
    (select id from sets where short_name = 'c20'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cazur, Ruthless Stalker'),
    (select id from sets where short_name = 'c20'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Canopy Vista'),
    (select id from sets where short_name = 'c20'),
    '261',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mulldrifter'),
    (select id from sets where short_name = 'c20'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nahiri, the Harbinger'),
    (select id from sets where short_name = 'c20'),
    '223',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nyx Weaver'),
    (select id from sets where short_name = 'c20'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Memorial to Folly'),
    (select id from sets where short_name = 'c20'),
    '288',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Call the Coppercoats'),
    (select id from sets where short_name = 'c20'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soulflayer'),
    (select id from sets where short_name = 'c20'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drake Haven'),
    (select id from sets where short_name = 'c20'),
    '110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Akroma''s Vengeance'),
    (select id from sets where short_name = 'c20'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Unburial Rites'),
    (select id from sets where short_name = 'c20'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eternal Dragon'),
    (select id from sets where short_name = 'c20'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twinning Staff'),
    (select id from sets where short_name = 'c20'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Portal Mage'),
    (select id from sets where short_name = 'c20'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Battlefield Forge'),
    (select id from sets where short_name = 'c20'),
    '257',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Titan Hunter'),
    (select id from sets where short_name = 'c20'),
    '48',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kathril, Aspect Warper'),
    (select id from sets where short_name = 'c20'),
    '10',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Windbrisk Heights'),
    (select id from sets where short_name = 'c20'),
    '321',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Scavenger Grounds'),
    (select id from sets where short_name = 'c20'),
    '306',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Verge'),
    (select id from sets where short_name = 'c20'),
    '285',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gruul Turf'),
    (select id from sets where short_name = 'c20'),
    '279',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vorapede'),
    (select id from sets where short_name = 'c20'),
    '195',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Decree of Justice'),
    (select id from sets where short_name = 'c20'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nomad Outpost'),
    (select id from sets where short_name = 'c20'),
    '294',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Solemn Recruit'),
    (select id from sets where short_name = 'c20'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Endless Sands'),
    (select id from sets where short_name = 'c20'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spellpyre Phoenix'),
    (select id from sets where short_name = 'c20'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Niblis of Frost'),
    (select id from sets where short_name = 'c20'),
    '120',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dredge the Mire'),
    (select id from sets where short_name = 'c20'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Exotic Orchard'),
    (select id from sets where short_name = 'c20'),
    '273',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boneyard Mycodrax'),
    (select id from sets where short_name = 'c20'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Propaganda'),
    (select id from sets where short_name = 'c20'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Souvenir Snatcher'),
    (select id from sets where short_name = 'c20'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Grip'),
    (select id from sets where short_name = 'c20'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cairn Wanderer'),
    (select id from sets where short_name = 'c20'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lonely Sandbar'),
    (select id from sets where short_name = 'c20'),
    '287',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kalamax, the Stormsire'),
    (select id from sets where short_name = 'c20'),
    '9',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mimic Vat'),
    (select id from sets where short_name = 'c20'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Growth Chamber'),
    (select id from sets where short_name = 'c20'),
    '311',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silvar, Devourer of the Free'),
    (select id from sets where short_name = 'c20'),
    '15',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Animist''s Awakening'),
    (select id from sets where short_name = 'c20'),
    '166',
    'rare'
) 
 on conflict do nothing;
