insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Flame Slash'),
    (select id from sets where short_name = 'roe'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Consuming Vapors'),
    (select id from sets where short_name = 'roe'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magmaw'),
    (select id from sets where short_name = 'roe'),
    '158',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grotag Siege-Runner'),
    (select id from sets where short_name = 'roe'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Momentous Fall'),
    (select id from sets where short_name = 'roe'),
    '197',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kor Spiritdancer'),
    (select id from sets where short_name = 'roe'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vendetta'),
    (select id from sets where short_name = 'roe'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jwari Scuttler'),
    (select id from sets where short_name = 'roe'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lay Bare'),
    (select id from sets where short_name = 'roe'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deathless Angel'),
    (select id from sets where short_name = 'roe'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guard Gomazoa'),
    (select id from sets where short_name = 'roe'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forked Bolt'),
    (select id from sets where short_name = 'roe'),
    '146',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kozilek, Butcher of Truth'),
    (select id from sets where short_name = 'roe'),
    '6',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kor Line-Slinger'),
    (select id from sets where short_name = 'roe'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oust'),
    (select id from sets where short_name = 'roe'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vent Sentinel'),
    (select id from sets where short_name = 'roe'),
    '171',
    'common'
) ,
(
    (select id from mtgcard where name = 'Awakening Zone'),
    (select id from sets where short_name = 'roe'),
    '176',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Tunneler'),
    (select id from sets where short_name = 'roe'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emerge Unscathed'),
    (select id from sets where short_name = 'roe'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kiln Fiend'),
    (select id from sets where short_name = 'roe'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aura Finesse'),
    (select id from sets where short_name = 'roe'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brood Birthing'),
    (select id from sets where short_name = 'roe'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Makindi Griffin'),
    (select id from sets where short_name = 'roe'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Halimar Wavewatch'),
    (select id from sets where short_name = 'roe'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Might of the Masses'),
    (select id from sets where short_name = 'roe'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'roe'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Joraga Treespeaker'),
    (select id from sets where short_name = 'roe'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dormant Gomazoa'),
    (select id from sets where short_name = 'roe'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Totem-Guide Hartebeest'),
    (select id from sets where short_name = 'roe'),
    '50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'roe'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Devastating Summons'),
    (select id from sets where short_name = 'roe'),
    '140',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fissure Vent'),
    (select id from sets where short_name = 'roe'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Curse of Wizardry'),
    (select id from sets where short_name = 'roe'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'roe'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk Observer'),
    (select id from sets where short_name = 'roe'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zulaport Enforcer'),
    (select id from sets where short_name = 'roe'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emrakul''s Hatcher'),
    (select id from sets where short_name = 'roe'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Induce Despair'),
    (select id from sets where short_name = 'roe'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cadaver Imp'),
    (select id from sets where short_name = 'roe'),
    '99',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk Skyscout'),
    (select id from sets where short_name = 'roe'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gelatinous Genesis'),
    (select id from sets where short_name = 'roe'),
    '183',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brimstone Mage'),
    (select id from sets where short_name = 'roe'),
    '137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gravity Well'),
    (select id from sets where short_name = 'roe'),
    '185',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enclave Cryptologist'),
    (select id from sets where short_name = 'roe'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bear Umbra'),
    (select id from sets where short_name = 'roe'),
    '177',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jaddi Lifestrider'),
    (select id from sets where short_name = 'roe'),
    '189',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Battle Rampart'),
    (select id from sets where short_name = 'roe'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ulamog, the Infinite Gyre'),
    (select id from sets where short_name = 'roe'),
    '12',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Nest Invader'),
    (select id from sets where short_name = 'roe'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Smite'),
    (select id from sets where short_name = 'roe'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sarkhan the Mad'),
    (select id from sets where short_name = 'roe'),
    '214',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Skittering Invasion'),
    (select id from sets where short_name = 'roe'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Conscription'),
    (select id from sets where short_name = 'roe'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nighthaze'),
    (select id from sets where short_name = 'roe'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Baneful Omen'),
    (select id from sets where short_name = 'roe'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Leaf Arrow'),
    (select id from sets where short_name = 'roe'),
    '194',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spider Umbra'),
    (select id from sets where short_name = 'roe'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deprive'),
    (select id from sets where short_name = 'roe'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Splinter Twin'),
    (select id from sets where short_name = 'roe'),
    '165',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skeletal Wurm'),
    (select id from sets where short_name = 'roe'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lighthouse Chronologist'),
    (select id from sets where short_name = 'roe'),
    '75',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'roe'),
    '238',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Temple'),
    (select id from sets where short_name = 'roe'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Distortion Strike'),
    (select id from sets where short_name = 'roe'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soul''s Attendant'),
    (select id from sets where short_name = 'roe'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nirkana Cutthroat'),
    (select id from sets where short_name = 'roe'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Last Kiss'),
    (select id from sets where short_name = 'roe'),
    '116',
    'common'
) ,
(
    (select id from mtgcard where name = 'It That Betrays'),
    (select id from sets where short_name = 'roe'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Living Destiny'),
    (select id from sets where short_name = 'roe'),
    '195',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dread Drone'),
    (select id from sets where short_name = 'roe'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Artisan of Kozilek'),
    (select id from sets where short_name = 'roe'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Contaminated Ground'),
    (select id from sets where short_name = 'roe'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drake Umbra'),
    (select id from sets where short_name = 'roe'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Surreal Memoir'),
    (select id from sets where short_name = 'roe'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pathrazer of Ulamog'),
    (select id from sets where short_name = 'roe'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crab Umbra'),
    (select id from sets where short_name = 'roe'),
    '58',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantasmal Abomination'),
    (select id from sets where short_name = 'roe'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Survival Cache'),
    (select id from sets where short_name = 'roe'),
    '48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reinforced Bulwark'),
    (select id from sets where short_name = 'roe'),
    '223',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warmonger''s Chariot'),
    (select id from sets where short_name = 'roe'),
    '226',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'roe'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bloodthrone Vampire'),
    (select id from sets where short_name = 'roe'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snake Umbra'),
    (select id from sets where short_name = 'roe'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kozilek''s Predator'),
    (select id from sets where short_name = 'roe'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bala Ged Scorpion'),
    (select id from sets where short_name = 'roe'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'roe'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelheart Vial'),
    (select id from sets where short_name = 'roe'),
    '215',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Perish the Thought'),
    (select id from sets where short_name = 'roe'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coralhelm Commander'),
    (select id from sets where short_name = 'roe'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Luminous Wake'),
    (select id from sets where short_name = 'roe'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'roe'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vengevine'),
    (select id from sets where short_name = 'roe'),
    '212',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Kazandu Tuskcaller'),
    (select id from sets where short_name = 'roe'),
    '191',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightmine Field'),
    (select id from sets where short_name = 'roe'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Not of This World'),
    (select id from sets where short_name = 'roe'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Valakut Fireboar'),
    (select id from sets where short_name = 'roe'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mul Daya Channelers'),
    (select id from sets where short_name = 'roe'),
    '198',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bloodrite Invoker'),
    (select id from sets where short_name = 'roe'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Knight of Cliffhaven'),
    (select id from sets where short_name = 'roe'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Arrogant Bloodlord'),
    (select id from sets where short_name = 'roe'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'roe'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Growth Spasm'),
    (select id from sets where short_name = 'roe'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'All Is Dust'),
    (select id from sets where short_name = 'roe'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Soulbound Guardians'),
    (select id from sets where short_name = 'roe'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hellion Eruption'),
    (select id from sets where short_name = 'roe'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frostwind Invoker'),
    (select id from sets where short_name = 'roe'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Emrakul, the Aeons Torn'),
    (select id from sets where short_name = 'roe'),
    '4',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Inquisition of Kozilek'),
    (select id from sets where short_name = 'roe'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Transcendent Master'),
    (select id from sets where short_name = 'roe'),
    '51',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Unified Will'),
    (select id from sets where short_name = 'roe'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kabira Vindicator'),
    (select id from sets where short_name = 'roe'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wall of Omens'),
    (select id from sets where short_name = 'roe'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Arsonist'),
    (select id from sets where short_name = 'roe'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'roe'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Haze Frog'),
    (select id from sets where short_name = 'roe'),
    '187',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guul Draz Assassin'),
    (select id from sets where short_name = 'roe'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'roe'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Escaped Null'),
    (select id from sets where short_name = 'roe'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Enatu Golem'),
    (select id from sets where short_name = 'roe'),
    '217',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Staggershock'),
    (select id from sets where short_name = 'roe'),
    '166',
    'common'
) ,
(
    (select id from mtgcard where name = 'Champion''s Drake'),
    (select id from sets where short_name = 'roe'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ulamog''s Crusher'),
    (select id from sets where short_name = 'roe'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mammoth Umbra'),
    (select id from sets where short_name = 'roe'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'roe'),
    '247',
    'common'
) ,
(
    (select id from mtgcard where name = 'Near-Death Experience'),
    (select id from sets where short_name = 'roe'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dreamstone Hedron'),
    (select id from sets where short_name = 'roe'),
    '216',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rage Nimbus'),
    (select id from sets where short_name = 'roe'),
    '160',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Khalni Hydra'),
    (select id from sets where short_name = 'roe'),
    '192',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ikiral Outrider'),
    (select id from sets where short_name = 'roe'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heat Ray'),
    (select id from sets where short_name = 'roe'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Linvala, Keeper of Silence'),
    (select id from sets where short_name = 'roe'),
    '33',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Caravan Escort'),
    (select id from sets where short_name = 'roe'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hand of Emrakul'),
    (select id from sets where short_name = 'roe'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Affa Guard Hound'),
    (select id from sets where short_name = 'roe'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pawn of Ulamog'),
    (select id from sets where short_name = 'roe'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stalwart Shield-Bearers'),
    (select id from sets where short_name = 'roe'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Akoum Boulderfoot'),
    (select id from sets where short_name = 'roe'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Realms Uncharted'),
    (select id from sets where short_name = 'roe'),
    '206',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stomper Cub'),
    (select id from sets where short_name = 'roe'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Null Champion'),
    (select id from sets where short_name = 'roe'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boar Umbra'),
    (select id from sets where short_name = 'roe'),
    '179',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pelakka Wurm'),
    (select id from sets where short_name = 'roe'),
    '204',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lavafume Invoker'),
    (select id from sets where short_name = 'roe'),
    '155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hedron Matrix'),
    (select id from sets where short_name = 'roe'),
    '218',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aura Gnarlid'),
    (select id from sets where short_name = 'roe'),
    '175',
    'common'
) ,
(
    (select id from mtgcard where name = 'Repay in Kind'),
    (select id from sets where short_name = 'roe'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Spawning Breath'),
    (select id from sets where short_name = 'roe'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Repel the Darkness'),
    (select id from sets where short_name = 'roe'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Consume the Meek'),
    (select id from sets where short_name = 'roe'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'roe'),
    '246',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disaster Radius'),
    (select id from sets where short_name = 'roe'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ancient Stirrings'),
    (select id from sets where short_name = 'roe'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hellcarver Demon'),
    (select id from sets where short_name = 'roe'),
    '113',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Gloomhunter'),
    (select id from sets where short_name = 'roe'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Surrakar Spellblade'),
    (select id from sets where short_name = 'roe'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'roe'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Raid Bombardment'),
    (select id from sets where short_name = 'roe'),
    '161',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lust for War'),
    (select id from sets where short_name = 'roe'),
    '157',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Runed Servitor'),
    (select id from sets where short_name = 'roe'),
    '224',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Venerated Teacher'),
    (select id from sets where short_name = 'roe'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Student of Warfare'),
    (select id from sets where short_name = 'roe'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Time of Heroes'),
    (select id from sets where short_name = 'roe'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spawnsire of Ulamog'),
    (select id from sets where short_name = 'roe'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mortician Beetle'),
    (select id from sets where short_name = 'roe'),
    '117',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nema Siltlurker'),
    (select id from sets where short_name = 'roe'),
    '200',
    'common'
) ,
(
    (select id from mtgcard where name = 'World at War'),
    (select id from sets where short_name = 'roe'),
    '172',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Umbra Mystic'),
    (select id from sets where short_name = 'roe'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'roe'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pestilence Demon'),
    (select id from sets where short_name = 'roe'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'See Beyond'),
    (select id from sets where short_name = 'roe'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harmless Assault'),
    (select id from sets where short_name = 'roe'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shrivel'),
    (select id from sets where short_name = 'roe'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Narcolepsy'),
    (select id from sets where short_name = 'roe'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battle-Rattle Shaman'),
    (select id from sets where short_name = 'roe'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nirkana Revenant'),
    (select id from sets where short_name = 'roe'),
    '120',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sporecap Spider'),
    (select id from sets where short_name = 'roe'),
    '209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Echo Mage'),
    (select id from sets where short_name = 'roe'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Drana, Kalastria Bloodchief'),
    (select id from sets where short_name = 'roe'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gigantomancer'),
    (select id from sets where short_name = 'roe'),
    '184',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lord of Shatterskull Pass'),
    (select id from sets where short_name = 'roe'),
    '156',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Beastbreaker of Bala Ged'),
    (select id from sets where short_name = 'roe'),
    '178',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hada Spy Patrol'),
    (select id from sets where short_name = 'roe'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Zof Shade'),
    (select id from sets where short_name = 'roe'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lagac Lizard'),
    (select id from sets where short_name = 'roe'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'roe'),
    '248',
    'common'
) ,
(
    (select id from mtgcard where name = 'Virulent Swipe'),
    (select id from sets where short_name = 'roe'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kargan Dragonlord'),
    (select id from sets where short_name = 'roe'),
    '152',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Regress'),
    (select id from sets where short_name = 'roe'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx-Bone Wand'),
    (select id from sets where short_name = 'roe'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Suffer the Past'),
    (select id from sets where short_name = 'roe'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Essence Feed'),
    (select id from sets where short_name = 'roe'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wrap in Flames'),
    (select id from sets where short_name = 'roe'),
    '173',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gravitational Shift'),
    (select id from sets where short_name = 'roe'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Evolving Wilds'),
    (select id from sets where short_name = 'roe'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Death Cultist'),
    (select id from sets where short_name = 'roe'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skywatcher Adept'),
    (select id from sets where short_name = 'roe'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Conquering Manticore'),
    (select id from sets where short_name = 'roe'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'roe'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Naturalize'),
    (select id from sets where short_name = 'roe'),
    '199',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'roe'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hedron-Field Purists'),
    (select id from sets where short_name = 'roe'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tajuru Preserver'),
    (select id from sets where short_name = 'roe'),
    '211',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corpsehatch'),
    (select id from sets where short_name = 'roe'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cast Through Time'),
    (select id from sets where short_name = 'roe'),
    '55',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Demonic Appetite'),
    (select id from sets where short_name = 'roe'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eel Umbra'),
    (select id from sets where short_name = 'roe'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tuktuk the Explorer'),
    (select id from sets where short_name = 'roe'),
    '169',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'roe'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prophetic Prism'),
    (select id from sets where short_name = 'roe'),
    '222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soulsurge Elemental'),
    (select id from sets where short_name = 'roe'),
    '163',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Irresistible Prey'),
    (select id from sets where short_name = 'roe'),
    '188',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Broodwarden'),
    (select id from sets where short_name = 'roe'),
    '181',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Daggerback Basilisk'),
    (select id from sets where short_name = 'roe'),
    '182',
    'common'
) ,
(
    (select id from mtgcard where name = 'Traitorous Instinct'),
    (select id from sets where short_name = 'roe'),
    '168',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ogre Sentry'),
    (select id from sets where short_name = 'roe'),
    '159',
    'common'
) ,
(
    (select id from mtgcard where name = 'Explosive Revelation'),
    (select id from sets where short_name = 'roe'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pennon Blade'),
    (select id from sets where short_name = 'roe'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ondu Giant'),
    (select id from sets where short_name = 'roe'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Recurring Insight'),
    (select id from sets where short_name = 'roe'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glory Seeker'),
    (select id from sets where short_name = 'roe'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wildheart Invoker'),
    (select id from sets where short_name = 'roe'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bramblesnap'),
    (select id from sets where short_name = 'roe'),
    '180',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nomads'' Assembly'),
    (select id from sets where short_name = 'roe'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'roe'),
    '236',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prey''s Vengeance'),
    (select id from sets where short_name = 'roe'),
    '205',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gideon Jura'),
    (select id from sets where short_name = 'roe'),
    '21',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Demystify'),
    (select id from sets where short_name = 'roe'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Domestication'),
    (select id from sets where short_name = 'roe'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dawnglare Invoker'),
    (select id from sets where short_name = 'roe'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Training Grounds'),
    (select id from sets where short_name = 'roe'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'roe'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rapacious One'),
    (select id from sets where short_name = 'roe'),
    '162',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Puncturing Light'),
    (select id from sets where short_name = 'roe'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shared Discovery'),
    (select id from sets where short_name = 'roe'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ogre''s Cleaver'),
    (select id from sets where short_name = 'roe'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reality Spasm'),
    (select id from sets where short_name = 'roe'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fleeting Distraction'),
    (select id from sets where short_name = 'roe'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sphinx of Magosi'),
    (select id from sets where short_name = 'roe'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hyena Umbra'),
    (select id from sets where short_name = 'roe'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overgrown Battlement'),
    (select id from sets where short_name = 'roe'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eland Umbra'),
    (select id from sets where short_name = 'roe'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Renegade Doppelganger'),
    (select id from sets where short_name = 'roe'),
    '84',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keening Stone'),
    (select id from sets where short_name = 'roe'),
    '219',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Guard Duty'),
    (select id from sets where short_name = 'roe'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thought Gorger'),
    (select id from sets where short_name = 'roe'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sea Gate Oracle'),
    (select id from sets where short_name = 'roe'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lone Missionary'),
    (select id from sets where short_name = 'roe'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mnemonic Wall'),
    (select id from sets where short_name = 'roe'),
    '78',
    'common'
) 
 on conflict do nothing;
