insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Flame Slash'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Consuming Vapors'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Magmaw'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Magmaw'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grotag Siege-Runner'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grotag Siege-Runner'),
        (select types.id from types where name = 'Goblin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grotag Siege-Runner'),
        (select types.id from types where name = 'Rogue')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Momentous Fall'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kor Spiritdancer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kor Spiritdancer'),
        (select types.id from types where name = 'Kor')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kor Spiritdancer'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vendetta'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jwari Scuttler'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jwari Scuttler'),
        (select types.id from types where name = 'Crab')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lay Bare'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Deathless Angel'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Deathless Angel'),
        (select types.id from types where name = 'Angel')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Guard Gomazoa'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Guard Gomazoa'),
        (select types.id from types where name = 'Jellyfish')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forked Bolt'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kozilek, Butcher of Truth'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kozilek, Butcher of Truth'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kozilek, Butcher of Truth'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kor Line-Slinger'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kor Line-Slinger'),
        (select types.id from types where name = 'Kor')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kor Line-Slinger'),
        (select types.id from types where name = 'Scout')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Oust'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vent Sentinel'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vent Sentinel'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Awakening Zone'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goblin Tunneler'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goblin Tunneler'),
        (select types.id from types where name = 'Goblin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goblin Tunneler'),
        (select types.id from types where name = 'Rogue')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Emerge Unscathed'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kiln Fiend'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kiln Fiend'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kiln Fiend'),
        (select types.id from types where name = 'Beast')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aura Finesse'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Brood Birthing'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Makindi Griffin'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Makindi Griffin'),
        (select types.id from types where name = 'Griffin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Halimar Wavewatch'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Halimar Wavewatch'),
        (select types.id from types where name = 'Merfolk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Halimar Wavewatch'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Might of the Masses'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Forest')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Joraga Treespeaker'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Joraga Treespeaker'),
        (select types.id from types where name = 'Elf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Joraga Treespeaker'),
        (select types.id from types where name = 'Druid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dormant Gomazoa'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dormant Gomazoa'),
        (select types.id from types where name = 'Jellyfish')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Totem-Guide Hartebeest'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Totem-Guide Hartebeest'),
        (select types.id from types where name = 'Antelope')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Island')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Devastating Summons'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fissure Vent'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Curse of Wizardry'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Swamp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Merfolk Observer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Merfolk Observer'),
        (select types.id from types where name = 'Merfolk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Merfolk Observer'),
        (select types.id from types where name = 'Rogue')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zulaport Enforcer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zulaport Enforcer'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zulaport Enforcer'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Emrakul''s Hatcher'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Emrakul''s Hatcher'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Emrakul''s Hatcher'),
        (select types.id from types where name = 'Drone')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Induce Despair'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cadaver Imp'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cadaver Imp'),
        (select types.id from types where name = 'Imp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Merfolk Skyscout'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Merfolk Skyscout'),
        (select types.id from types where name = 'Merfolk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Merfolk Skyscout'),
        (select types.id from types where name = 'Scout')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gelatinous Genesis'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Brimstone Mage'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Brimstone Mage'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Brimstone Mage'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gravity Well'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Enclave Cryptologist'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Enclave Cryptologist'),
        (select types.id from types where name = 'Merfolk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Enclave Cryptologist'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bear Umbra'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bear Umbra'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jaddi Lifestrider'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Jaddi Lifestrider'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Battle Rampart'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Battle Rampart'),
        (select types.id from types where name = 'Wall')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ulamog, the Infinite Gyre'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ulamog, the Infinite Gyre'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ulamog, the Infinite Gyre'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nest Invader'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nest Invader'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nest Invader'),
        (select types.id from types where name = 'Drone')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Smite'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sarkhan the Mad'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sarkhan the Mad'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sarkhan the Mad'),
        (select types.id from types where name = 'Sarkhan')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skittering Invasion'),
        (select types.id from types where name = 'Tribal')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skittering Invasion'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skittering Invasion'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eldrazi Conscription'),
        (select types.id from types where name = 'Tribal')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eldrazi Conscription'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eldrazi Conscription'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eldrazi Conscription'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nighthaze'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Baneful Omen'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Leaf Arrow'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spider Umbra'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spider Umbra'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Deprive'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Splinter Twin'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Splinter Twin'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skeletal Wurm'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skeletal Wurm'),
        (select types.id from types where name = 'Skeleton')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skeletal Wurm'),
        (select types.id from types where name = 'Wurm')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lighthouse Chronologist'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lighthouse Chronologist'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lighthouse Chronologist'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Swamp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eldrazi Temple'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Distortion Strike'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soul''s Attendant'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soul''s Attendant'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soul''s Attendant'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nirkana Cutthroat'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nirkana Cutthroat'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nirkana Cutthroat'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Last Kiss'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'It That Betrays'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'It That Betrays'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Living Destiny'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dread Drone'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dread Drone'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dread Drone'),
        (select types.id from types where name = 'Drone')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Artisan of Kozilek'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Artisan of Kozilek'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Contaminated Ground'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Contaminated Ground'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Drake Umbra'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Drake Umbra'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Surreal Memoir'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pathrazer of Ulamog'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pathrazer of Ulamog'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crab Umbra'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Crab Umbra'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Phantasmal Abomination'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Phantasmal Abomination'),
        (select types.id from types where name = 'Illusion')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Survival Cache'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Reinforced Bulwark'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Reinforced Bulwark'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Reinforced Bulwark'),
        (select types.id from types where name = 'Wall')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Warmonger''s Chariot'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Warmonger''s Chariot'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Plains')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodthrone Vampire'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodthrone Vampire'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Snake Umbra'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Snake Umbra'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kozilek''s Predator'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kozilek''s Predator'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kozilek''s Predator'),
        (select types.id from types where name = 'Drone')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bala Ged Scorpion'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bala Ged Scorpion'),
        (select types.id from types where name = 'Scorpion')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Island')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Angelheart Vial'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Perish the Thought'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Coralhelm Commander'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Coralhelm Commander'),
        (select types.id from types where name = 'Merfolk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Coralhelm Commander'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Luminous Wake'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Luminous Wake'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Swamp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vengevine'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Vengevine'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kazandu Tuskcaller'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kazandu Tuskcaller'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kazandu Tuskcaller'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lightmine Field'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Not of This World'),
        (select types.id from types where name = 'Tribal')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Not of This World'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Not of This World'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Valakut Fireboar'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Valakut Fireboar'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Valakut Fireboar'),
        (select types.id from types where name = 'Boar')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mul Daya Channelers'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mul Daya Channelers'),
        (select types.id from types where name = 'Elf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mul Daya Channelers'),
        (select types.id from types where name = 'Druid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mul Daya Channelers'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodrite Invoker'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodrite Invoker'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bloodrite Invoker'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Knight of Cliffhaven'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Knight of Cliffhaven'),
        (select types.id from types where name = 'Kor')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Knight of Cliffhaven'),
        (select types.id from types where name = 'Knight')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Arrogant Bloodlord'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Arrogant Bloodlord'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Arrogant Bloodlord'),
        (select types.id from types where name = 'Knight')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swamp'),
        (select types.id from types where name = 'Swamp')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Growth Spasm'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'All Is Dust'),
        (select types.id from types where name = 'Tribal')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'All Is Dust'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'All Is Dust'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soulbound Guardians'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soulbound Guardians'),
        (select types.id from types where name = 'Kor')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soulbound Guardians'),
        (select types.id from types where name = 'Spirit')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hellion Eruption'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Frostwind Invoker'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Frostwind Invoker'),
        (select types.id from types where name = 'Merfolk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Frostwind Invoker'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Emrakul, the Aeons Torn'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Emrakul, the Aeons Torn'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Emrakul, the Aeons Torn'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Inquisition of Kozilek'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Transcendent Master'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Transcendent Master'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Transcendent Master'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Transcendent Master'),
        (select types.id from types where name = 'Avatar')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Unified Will'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kabira Vindicator'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kabira Vindicator'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kabira Vindicator'),
        (select types.id from types where name = 'Knight')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wall of Omens'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wall of Omens'),
        (select types.id from types where name = 'Wall')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goblin Arsonist'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goblin Arsonist'),
        (select types.id from types where name = 'Goblin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Goblin Arsonist'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Plains')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Haze Frog'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Haze Frog'),
        (select types.id from types where name = 'Frog')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Guul Draz Assassin'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Guul Draz Assassin'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Guul Draz Assassin'),
        (select types.id from types where name = 'Assassin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Mountain')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Escaped Null'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Escaped Null'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Escaped Null'),
        (select types.id from types where name = 'Berserker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Enatu Golem'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Enatu Golem'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Enatu Golem'),
        (select types.id from types where name = 'Golem')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Staggershock'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Champion''s Drake'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Champion''s Drake'),
        (select types.id from types where name = 'Drake')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ulamog''s Crusher'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ulamog''s Crusher'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mammoth Umbra'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mammoth Umbra'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Forest')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Near-Death Experience'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dreamstone Hedron'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rage Nimbus'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rage Nimbus'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Khalni Hydra'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Khalni Hydra'),
        (select types.id from types where name = 'Hydra')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ikiral Outrider'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ikiral Outrider'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ikiral Outrider'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Heat Ray'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Linvala, Keeper of Silence'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Linvala, Keeper of Silence'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Linvala, Keeper of Silence'),
        (select types.id from types where name = 'Angel')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Caravan Escort'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Caravan Escort'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Caravan Escort'),
        (select types.id from types where name = 'Knight')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hand of Emrakul'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hand of Emrakul'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Affa Guard Hound'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Affa Guard Hound'),
        (select types.id from types where name = 'Dog')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pawn of Ulamog'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pawn of Ulamog'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pawn of Ulamog'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stalwart Shield-Bearers'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stalwart Shield-Bearers'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stalwart Shield-Bearers'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Akoum Boulderfoot'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Akoum Boulderfoot'),
        (select types.id from types where name = 'Giant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Akoum Boulderfoot'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Realms Uncharted'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stomper Cub'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Stomper Cub'),
        (select types.id from types where name = 'Beast')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Null Champion'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Null Champion'),
        (select types.id from types where name = 'Zombie')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Null Champion'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Boar Umbra'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Boar Umbra'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pelakka Wurm'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pelakka Wurm'),
        (select types.id from types where name = 'Wurm')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lavafume Invoker'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lavafume Invoker'),
        (select types.id from types where name = 'Goblin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lavafume Invoker'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hedron Matrix'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hedron Matrix'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aura Gnarlid'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Aura Gnarlid'),
        (select types.id from types where name = 'Beast')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Repay in Kind'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spawning Breath'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Repel the Darkness'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Consume the Meek'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Forest')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Disaster Radius'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ancient Stirrings'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hellcarver Demon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hellcarver Demon'),
        (select types.id from types where name = 'Demon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gloomhunter'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gloomhunter'),
        (select types.id from types where name = 'Bat')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Surrakar Spellblade'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Surrakar Spellblade'),
        (select types.id from types where name = 'Surrakar')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Mountain')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Raid Bombardment'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lust for War'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lust for War'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Runed Servitor'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Runed Servitor'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Runed Servitor'),
        (select types.id from types where name = 'Construct')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Venerated Teacher'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Venerated Teacher'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Venerated Teacher'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Student of Warfare'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Student of Warfare'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Student of Warfare'),
        (select types.id from types where name = 'Knight')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Time of Heroes'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spawnsire of Ulamog'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spawnsire of Ulamog'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mortician Beetle'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mortician Beetle'),
        (select types.id from types where name = 'Insect')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nema Siltlurker'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nema Siltlurker'),
        (select types.id from types where name = 'Lizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'World at War'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Umbra Mystic'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Umbra Mystic'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Umbra Mystic'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Mountain')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pestilence Demon'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pestilence Demon'),
        (select types.id from types where name = 'Demon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'See Beyond'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Harmless Assault'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shrivel'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Narcolepsy'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Narcolepsy'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Battle-Rattle Shaman'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Battle-Rattle Shaman'),
        (select types.id from types where name = 'Goblin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Battle-Rattle Shaman'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nirkana Revenant'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nirkana Revenant'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nirkana Revenant'),
        (select types.id from types where name = 'Shade')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sporecap Spider'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sporecap Spider'),
        (select types.id from types where name = 'Spider')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Echo Mage'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Echo Mage'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Echo Mage'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Drana, Kalastria Bloodchief'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Drana, Kalastria Bloodchief'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Drana, Kalastria Bloodchief'),
        (select types.id from types where name = 'Vampire')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Drana, Kalastria Bloodchief'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gigantomancer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gigantomancer'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gigantomancer'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lord of Shatterskull Pass'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lord of Shatterskull Pass'),
        (select types.id from types where name = 'Minotaur')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lord of Shatterskull Pass'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Beastbreaker of Bala Ged'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Beastbreaker of Bala Ged'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Beastbreaker of Bala Ged'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hada Spy Patrol'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hada Spy Patrol'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hada Spy Patrol'),
        (select types.id from types where name = 'Rogue')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zof Shade'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Zof Shade'),
        (select types.id from types where name = 'Shade')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lagac Lizard'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lagac Lizard'),
        (select types.id from types where name = 'Lizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Forest'),
        (select types.id from types where name = 'Forest')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Virulent Swipe'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kargan Dragonlord'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kargan Dragonlord'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Kargan Dragonlord'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Regress'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sphinx-Bone Wand'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Suffer the Past'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Essence Feed'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wrap in Flames'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gravitational Shift'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Evolving Wilds'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Death Cultist'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Death Cultist'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Death Cultist'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skywatcher Adept'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skywatcher Adept'),
        (select types.id from types where name = 'Merfolk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Skywatcher Adept'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Conquering Manticore'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Conquering Manticore'),
        (select types.id from types where name = 'Manticore')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Plains')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Naturalize'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Plains'),
        (select types.id from types where name = 'Plains')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hedron-Field Purists'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hedron-Field Purists'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hedron-Field Purists'),
        (select types.id from types where name = 'Cleric')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tajuru Preserver'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tajuru Preserver'),
        (select types.id from types where name = 'Elf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tajuru Preserver'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Corpsehatch'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cast Through Time'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Demonic Appetite'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Demonic Appetite'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eel Umbra'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eel Umbra'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tuktuk the Explorer'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tuktuk the Explorer'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Tuktuk the Explorer'),
        (select types.id from types where name = 'Goblin')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mountain'),
        (select types.id from types where name = 'Mountain')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Prophetic Prism'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soulsurge Elemental'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Soulsurge Elemental'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Irresistible Prey'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Broodwarden'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Broodwarden'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Broodwarden'),
        (select types.id from types where name = 'Drone')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Daggerback Basilisk'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Daggerback Basilisk'),
        (select types.id from types where name = 'Basilisk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Traitorous Instinct'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ogre Sentry'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ogre Sentry'),
        (select types.id from types where name = 'Ogre')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ogre Sentry'),
        (select types.id from types where name = 'Warrior')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Explosive Revelation'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pennon Blade'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Pennon Blade'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ondu Giant'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ondu Giant'),
        (select types.id from types where name = 'Giant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ondu Giant'),
        (select types.id from types where name = 'Druid')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Recurring Insight'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Glory Seeker'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Glory Seeker'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Glory Seeker'),
        (select types.id from types where name = 'Soldier')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wildheart Invoker'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wildheart Invoker'),
        (select types.id from types where name = 'Elf')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Wildheart Invoker'),
        (select types.id from types where name = 'Shaman')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bramblesnap'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bramblesnap'),
        (select types.id from types where name = 'Elemental')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Nomads'' Assembly'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Island')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Prey''s Vengeance'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gideon Jura'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gideon Jura'),
        (select types.id from types where name = 'Planeswalker')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Gideon Jura'),
        (select types.id from types where name = 'Gideon')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Demystify'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Domestication'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Domestication'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dawnglare Invoker'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dawnglare Invoker'),
        (select types.id from types where name = 'Kor')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Dawnglare Invoker'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Training Grounds'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Basic')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Land')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Island'),
        (select types.id from types where name = 'Island')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rapacious One'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rapacious One'),
        (select types.id from types where name = 'Eldrazi')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Rapacious One'),
        (select types.id from types where name = 'Drone')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Puncturing Light'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shared Discovery'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ogre''s Cleaver'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ogre''s Cleaver'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Reality Spasm'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Fleeting Distraction'),
        (select types.id from types where name = 'Instant')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sphinx of Magosi'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sphinx of Magosi'),
        (select types.id from types where name = 'Sphinx')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hyena Umbra'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hyena Umbra'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Overgrown Battlement'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Overgrown Battlement'),
        (select types.id from types where name = 'Wall')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eland Umbra'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Eland Umbra'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Renegade Doppelganger'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Renegade Doppelganger'),
        (select types.id from types where name = 'Shapeshifter')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Keening Stone'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Guard Duty'),
        (select types.id from types where name = 'Enchantment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Guard Duty'),
        (select types.id from types where name = 'Aura')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thought Gorger'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Thought Gorger'),
        (select types.id from types where name = 'Horror')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sea Gate Oracle'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sea Gate Oracle'),
        (select types.id from types where name = 'Human')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Sea Gate Oracle'),
        (select types.id from types where name = 'Wizard')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lone Missionary'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lone Missionary'),
        (select types.id from types where name = 'Kor')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lone Missionary'),
        (select types.id from types where name = 'Monk')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mnemonic Wall'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Mnemonic Wall'),
        (select types.id from types where name = 'Wall')
    ) 
 on conflict do nothing;
