insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Izoni, Thousand-Eyed'),
    (select id from sets where short_name = 'gk1'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Grove of the Guardian'),
    (select id from sets where short_name = 'gk1'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Turn // Burn'),
    (select id from sets where short_name = 'gk1'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Abrupt Decay'),
    (select id from sets where short_name = 'gk1'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lazav, Dimir Mastermind'),
    (select id from sets where short_name = 'gk1'),
    '16',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Boros Swiftblade'),
    (select id from sets where short_name = 'gk1'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Glare of Subdual'),
    (select id from sets where short_name = 'gk1'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Treasured Find'),
    (select id from sets where short_name = 'gk1'),
    '72',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trostani, Selesnya''s Voice'),
    (select id from sets where short_name = 'gk1'),
    '102',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Aurelia, the Warleader'),
    (select id from sets where short_name = 'gk1'),
    '77',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Izzet Boilerworks'),
    (select id from sets where short_name = 'gk1'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scatter the Seeds'),
    (select id from sets where short_name = 'gk1'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dimir Aqueduct'),
    (select id from sets where short_name = 'gk1'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'gk1'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Telling Time'),
    (select id from sets where short_name = 'gk1'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Martial Glory'),
    (select id from sets where short_name = 'gk1'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'gk1'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armada Wurm'),
    (select id from sets where short_name = 'gk1'),
    '108',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Selesnya Charm'),
    (select id from sets where short_name = 'gk1'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grave-Shell Scarab'),
    (select id from sets where short_name = 'gk1'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Warped Physique'),
    (select id from sets where short_name = 'gk1'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Razia, Boros Archangel'),
    (select id from sets where short_name = 'gk1'),
    '93',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Korozda Guildmage'),
    (select id from sets where short_name = 'gk1'),
    '66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Goblin Rally'),
    (select id from sets where short_name = 'gk1'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hour of Reckoning'),
    (select id from sets where short_name = 'gk1'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'gk1'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Evangel'),
    (select id from sets where short_name = 'gk1'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dryad Militant'),
    (select id from sets where short_name = 'gk1'),
    '111',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Frenzied Goblin'),
    (select id from sets where short_name = 'gk1'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Call of the Conclave'),
    (select id from sets where short_name = 'gk1'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stitch in Time'),
    (select id from sets where short_name = 'gk1'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Consuming Aberration'),
    (select id from sets where short_name = 'gk1'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Charm'),
    (select id from sets where short_name = 'gk1'),
    '84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Rot Farm'),
    (select id from sets where short_name = 'gk1'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Char'),
    (select id from sets where short_name = 'gk1'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mirko Vosk, Mind Drinker'),
    (select id from sets where short_name = 'gk1'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'gk1'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stinkweed Imp'),
    (select id from sets where short_name = 'gk1'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'gk1'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tolsimir Wolfblood'),
    (select id from sets where short_name = 'gk1'),
    '121',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Izzet Signet'),
    (select id from sets where short_name = 'gk1'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grisly Salvage'),
    (select id from sets where short_name = 'gk1'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bomber Corps'),
    (select id from sets where short_name = 'gk1'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'gk1'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brightflame'),
    (select id from sets where short_name = 'gk1'),
    '87',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Helix'),
    (select id from sets where short_name = 'gk1'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nivix Guildmage'),
    (select id from sets where short_name = 'gk1'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Electrickery'),
    (select id from sets where short_name = 'gk1'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Boros Elite'),
    (select id from sets where short_name = 'gk1'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Boros Signet'),
    (select id from sets where short_name = 'gk1'),
    '97',
    'common'
) ,
(
    (select id from mtgcard where name = 'Darkblast'),
    (select id from sets where short_name = 'gk1'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Shambling Shell'),
    (select id from sets where short_name = 'gk1'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Etrata, the Silencer'),
    (select id from sets where short_name = 'gk1'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Niv-Mizzet, the Firemind'),
    (select id from sets where short_name = 'gk1'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nightveil Specter'),
    (select id from sets where short_name = 'gk1'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Devouring Light'),
    (select id from sets where short_name = 'gk1'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sisters of Stone Death'),
    (select id from sets where short_name = 'gk1'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shattering Spree'),
    (select id from sets where short_name = 'gk1'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'gk1'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Circu, Dimir Lobotomist'),
    (select id from sets where short_name = 'gk1'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'gk1'),
    '49',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stolen Identity'),
    (select id from sets where short_name = 'gk1'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Reckoner'),
    (select id from sets where short_name = 'gk1'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jarad, Golgari Lich Lord'),
    (select id from sets where short_name = 'gk1'),
    '65',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sunhome, Fortress of the Legion'),
    (select id from sets where short_name = 'gk1'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'gk1'),
    '100',
    'common'
) ,
(
    (select id from mtgcard where name = 'Guttersnipe'),
    (select id from sets where short_name = 'gk1'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Loxodon Hierarch'),
    (select id from sets where short_name = 'gk1'),
    '114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Master Warcraft'),
    (select id from sets where short_name = 'gk1'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Glimpse the Unthinkable'),
    (select id from sets where short_name = 'gk1'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'gk1'),
    '101',
    'common'
) ,
(
    (select id from mtgcard where name = 'Firemane Avenger'),
    (select id from sets where short_name = 'gk1'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selesnya Sanctuary'),
    (select id from sets where short_name = 'gk1'),
    '125',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hypersonic Dragon'),
    (select id from sets where short_name = 'gk1'),
    '39',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ribbons of Night'),
    (select id from sets where short_name = 'gk1'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vigor Mortis'),
    (select id from sets where short_name = 'gk1'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deathrite Shaman'),
    (select id from sets where short_name = 'gk1'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dimir Charm'),
    (select id from sets where short_name = 'gk1'),
    '11',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Firemane Angel'),
    (select id from sets where short_name = 'gk1'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Selesnya Guildmage'),
    (select id from sets where short_name = 'gk1'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Invoke the Firemind'),
    (select id from sets where short_name = 'gk1'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deadbridge Goliath'),
    (select id from sets where short_name = 'gk1'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Garrison'),
    (select id from sets where short_name = 'gk1'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cerebral Vortex'),
    (select id from sets where short_name = 'gk1'),
    '35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Privileged Position'),
    (select id from sets where short_name = 'gk1'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thunderheads'),
    (select id from sets where short_name = 'gk1'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Growing Ranks'),
    (select id from sets where short_name = 'gk1'),
    '113',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Szadek, Lord of Secrets'),
    (select id from sets where short_name = 'gk1'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lotleth Troll'),
    (select id from sets where short_name = 'gk1'),
    '67',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gelectrode'),
    (select id from sets where short_name = 'gk1'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Moroii'),
    (select id from sets where short_name = 'gk1'),
    '18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spark Trooper'),
    (select id from sets where short_name = 'gk1'),
    '94',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Electrolyze'),
    (select id from sets where short_name = 'gk1'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selesnya Signet'),
    (select id from sets where short_name = 'gk1'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Izzet Charm'),
    (select id from sets where short_name = 'gk1'),
    '41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mizzium Mortars'),
    (select id from sets where short_name = 'gk1'),
    '32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dinrova Horror'),
    (select id from sets where short_name = 'gk1'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Djinn Illuminatus'),
    (select id from sets where short_name = 'gk1'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyromatics'),
    (select id from sets where short_name = 'gk1'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golgari Signet'),
    (select id from sets where short_name = 'gk1'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Last Gasp'),
    (select id from sets where short_name = 'gk1'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dimir Doppelganger'),
    (select id from sets where short_name = 'gk1'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Syncopate'),
    (select id from sets where short_name = 'gk1'),
    '3',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gather Courage'),
    (select id from sets where short_name = 'gk1'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sunhome Guildmage'),
    (select id from sets where short_name = 'gk1'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gaze of Granite'),
    (select id from sets where short_name = 'gk1'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slum Reaper'),
    (select id from sets where short_name = 'gk1'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tibor and Lumia'),
    (select id from sets where short_name = 'gk1'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Golgari Charm'),
    (select id from sets where short_name = 'gk1'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sundering Growth'),
    (select id from sets where short_name = 'gk1'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pollenbright Wings'),
    (select id from sets where short_name = 'gk1'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Advent of the Wurm'),
    (select id from sets where short_name = 'gk1'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Agrus Kos, Wojek Veteran'),
    (select id from sets where short_name = 'gk1'),
    '83',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Boros Keyrune'),
    (select id from sets where short_name = 'gk1'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deadbridge Chant'),
    (select id from sets where short_name = 'gk1'),
    '58',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Watchwolf'),
    (select id from sets where short_name = 'gk1'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Guildmage'),
    (select id from sets where short_name = 'gk1'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drown in Filth'),
    (select id from sets where short_name = 'gk1'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Putrefy'),
    (select id from sets where short_name = 'gk1'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Call of the Nightwing'),
    (select id from sets where short_name = 'gk1'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Signet'),
    (select id from sets where short_name = 'gk1'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Netherborn Phalanx'),
    (select id from sets where short_name = 'gk1'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savra, Queen of the Golgari'),
    (select id from sets where short_name = 'gk1'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Centaur Healer'),
    (select id from sets where short_name = 'gk1'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elves of Deep Shadow'),
    (select id from sets where short_name = 'gk1'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daring Skyjek'),
    (select id from sets where short_name = 'gk1'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Legion Loyalist'),
    (select id from sets where short_name = 'gk1'),
    '82',
    'rare'
) 
 on conflict do nothing;
