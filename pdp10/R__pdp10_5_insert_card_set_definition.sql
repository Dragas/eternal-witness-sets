insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Nissa Revane'),
    (select id from sets where short_name = 'pdp10'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Liliana Vess'),
    (select id from sets where short_name = 'pdp10'),
    '1',
    'mythic'
) 
 on conflict do nothing;
