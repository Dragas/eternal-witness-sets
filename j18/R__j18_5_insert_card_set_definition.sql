insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Commander''s Sphere'),
    (select id from sets where short_name = 'j18'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rhystic Study'),
    (select id from sets where short_name = 'j18'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Merchant Scroll'),
    (select id from sets where short_name = 'j18'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Food Chain'),
    (select id from sets where short_name = 'j18'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Teferi''s Protection'),
    (select id from sets where short_name = 'j18'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lord of Atlantis'),
    (select id from sets where short_name = 'j18'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nin, the Pain Artist'),
    (select id from sets where short_name = 'j18'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vampiric Tutor'),
    (select id from sets where short_name = 'j18'),
    '2',
    'rare'
) 
 on conflict do nothing;
