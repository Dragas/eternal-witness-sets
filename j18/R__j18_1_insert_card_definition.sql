    insert into mtgcard(name) values ('Commander''s Sphere') on conflict do nothing;
    insert into mtgcard(name) values ('Rhystic Study') on conflict do nothing;
    insert into mtgcard(name) values ('Merchant Scroll') on conflict do nothing;
    insert into mtgcard(name) values ('Food Chain') on conflict do nothing;
    insert into mtgcard(name) values ('Teferi''s Protection') on conflict do nothing;
    insert into mtgcard(name) values ('Lord of Atlantis') on conflict do nothing;
    insert into mtgcard(name) values ('Nin, the Pain Artist') on conflict do nothing;
    insert into mtgcard(name) values ('Vampiric Tutor') on conflict do nothing;
