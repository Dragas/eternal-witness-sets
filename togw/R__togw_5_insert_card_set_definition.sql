insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Eldrazi Scion'),
    (select id from sets where short_name = 'togw'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Scion'),
    (select id from sets where short_name = 'togw'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plant'),
    (select id from sets where short_name = 'togw'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel'),
    (select id from sets where short_name = 'togw'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Scion'),
    (select id from sets where short_name = 'togw'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Scion'),
    (select id from sets where short_name = 'togw'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'togw'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Scion'),
    (select id from sets where short_name = 'togw'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elemental'),
    (select id from sets where short_name = 'togw'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'togw'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Eldrazi Scion'),
    (select id from sets where short_name = 'togw'),
    '2',
    'common'
) 
 on conflict do nothing;
