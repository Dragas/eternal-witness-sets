    insert into mtgcard(name) values ('Disenchant') on conflict do nothing;
    insert into mtgcard(name) values ('Wrath of God') on conflict do nothing;
    insert into mtgcard(name) values ('Psionic Blast') on conflict do nothing;
    insert into mtgcard(name) values ('Mortify') on conflict do nothing;
    insert into mtgcard(name) values ('Recollect') on conflict do nothing;
    insert into mtgcard(name) values ('Condemn') on conflict do nothing;
    insert into mtgcard(name) values ('Cruel Edict') on conflict do nothing;
