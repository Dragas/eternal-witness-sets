insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Disenchant'),
    (select id from sets where short_name = 'p07'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wrath of God'),
    (select id from sets where short_name = 'p07'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Psionic Blast'),
    (select id from sets where short_name = 'p07'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mortify'),
    (select id from sets where short_name = 'p07'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Recollect'),
    (select id from sets where short_name = 'p07'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Condemn'),
    (select id from sets where short_name = 'p07'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cruel Edict'),
    (select id from sets where short_name = 'p07'),
    '5',
    'rare'
) 
 on conflict do nothing;
