insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Torn Between Heads'),
    (select id from sets where short_name = 'tfth'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snapping Fang Head'),
    (select id from sets where short_name = 'tfth'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Noxious Hydra Breath'),
    (select id from sets where short_name = 'tfth'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Distract the Hydra'),
    (select id from sets where short_name = 'tfth'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hydra Head'),
    (select id from sets where short_name = 'tfth'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Strike the Weak Spot'),
    (select id from sets where short_name = 'tfth'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shrieking Titan Head'),
    (select id from sets where short_name = 'tfth'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Savage Vigor Head'),
    (select id from sets where short_name = 'tfth'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hydra''s Impenetrable Hide'),
    (select id from sets where short_name = 'tfth'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Disorienting Glower'),
    (select id from sets where short_name = 'tfth'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swallow the Hero Whole'),
    (select id from sets where short_name = 'tfth'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ravenous Brute Head'),
    (select id from sets where short_name = 'tfth'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Neck Tangle'),
    (select id from sets where short_name = 'tfth'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unified Lunge'),
    (select id from sets where short_name = 'tfth'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grown from the Stump'),
    (select id from sets where short_name = 'tfth'),
    '8',
    'common'
) 
 on conflict do nothing;
