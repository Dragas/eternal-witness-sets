insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Torn Between Heads'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Snapping Fang Head'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Snapping Fang Head'),
        (select types.id from types where name = 'Elite')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Snapping Fang Head'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Snapping Fang Head'),
        (select types.id from types where name = 'Head')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Noxious Hydra Breath'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Distract the Hydra'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hydra Head'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hydra Head'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hydra Head'),
        (select types.id from types where name = 'Head')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Strike the Weak Spot'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shrieking Titan Head'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shrieking Titan Head'),
        (select types.id from types where name = 'Elite')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shrieking Titan Head'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Shrieking Titan Head'),
        (select types.id from types where name = 'Head')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Savage Vigor Head'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Savage Vigor Head'),
        (select types.id from types where name = 'Elite')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Savage Vigor Head'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Savage Vigor Head'),
        (select types.id from types where name = 'Head')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hydra''s Impenetrable Hide'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Disorienting Glower'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Swallow the Hero Whole'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ravenous Brute Head'),
        (select types.id from types where name = 'Token')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ravenous Brute Head'),
        (select types.id from types where name = 'Elite')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ravenous Brute Head'),
        (select types.id from types where name = 'Creature')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Ravenous Brute Head'),
        (select types.id from types where name = 'Head')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Neck Tangle'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Unified Lunge'),
        (select types.id from types where name = 'Sorcery')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Grown from the Stump'),
        (select types.id from types where name = 'Sorcery')
    ) 
 on conflict do nothing;
