insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Unwavering Initiate'),
    (select id from sets where short_name = 'takh'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Punchcard'),
    (select id from sets where short_name = 'takh'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Initiate'),
    (select id from sets where short_name = 'takh'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hippo'),
    (select id from sets where short_name = 'takh'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insect'),
    (select id from sets where short_name = 'takh'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'takh'),
    '21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warrior'),
    (select id from sets where short_name = 'takh'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wurm'),
    (select id from sets where short_name = 'takh'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vizier of Many Faces'),
    (select id from sets where short_name = 'takh'),
    '15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sacred Cat'),
    (select id from sets where short_name = 'takh'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temmet, Vizier of Naktamun'),
    (select id from sets where short_name = 'takh'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drake'),
    (select id from sets where short_name = 'takh'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Snake'),
    (select id from sets where short_name = 'takh'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tah-Crop Skirmisher'),
    (select id from sets where short_name = 'takh'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Heart-Piercer Manticore'),
    (select id from sets where short_name = 'takh'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Honored Hydra'),
    (select id from sets where short_name = 'takh'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Labyrinth Guardian'),
    (select id from sets where short_name = 'takh'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anointer Priest'),
    (select id from sets where short_name = 'takh'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Punchcard'),
    (select id from sets where short_name = 'takh'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Sanctions'),
    (select id from sets where short_name = 'takh'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aven Wind Guide'),
    (select id from sets where short_name = 'takh'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Oketra''s Attendant'),
    (select id from sets where short_name = 'takh'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cat'),
    (select id from sets where short_name = 'takh'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'takh'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Trueheart Duelist'),
    (select id from sets where short_name = 'takh'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gideon of the Trials Emblem'),
    (select id from sets where short_name = 'takh'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Glyph Keeper'),
    (select id from sets where short_name = 'takh'),
    '5',
    'common'
) 
 on conflict do nothing;
