insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Ajani, the Greathearted'),
    (select id from sets where short_name = 'psld'),
    '520',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Liliana, Dreadhorde General'),
    (select id from sets where short_name = 'psld'),
    '510',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ashiok, Dream Render'),
    (select id from sets where short_name = 'psld'),
    '528',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chandra, Fire Artisan'),
    (select id from sets where short_name = 'psld'),
    '512',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Angrath, Captain of Chaos'),
    (select id from sets where short_name = 'psld'),
    '527',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gideon Blackblade'),
    (select id from sets where short_name = 'psld'),
    '503',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ob Nixilis, the Hate-Twisted'),
    (select id from sets where short_name = 'psld'),
    '511',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kaya, Bane of the Dead'),
    (select id from sets where short_name = 'psld'),
    '531',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Domri, Anarch of Bolas'),
    (select id from sets where short_name = 'psld'),
    '521',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kasmina, Enigmatic Mentor'),
    (select id from sets where short_name = 'psld'),
    '507',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Narset, Parter of Veils'),
    (select id from sets where short_name = 'psld'),
    '508',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nicol Bolas, Dragon-God'),
    (select id from sets where short_name = 'psld'),
    '522',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'The Wanderer'),
    (select id from sets where short_name = 'psld'),
    '505',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teferi, Time Raveler'),
    (select id from sets where short_name = 'psld'),
    '526',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Huatli, the Sun''s Heart'),
    (select id from sets where short_name = 'psld'),
    '530',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Davriel, Rogue Shadowmage'),
    (select id from sets where short_name = 'psld'),
    '509',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tibalt, Rakish Instigator'),
    (select id from sets where short_name = 'psld'),
    '515',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jace, Wielder of Mysteries'),
    (select id from sets where short_name = 'psld'),
    '506',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ral, Storm Conduit'),
    (select id from sets where short_name = 'psld'),
    '523',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dovin, Hand of Control'),
    (select id from sets where short_name = 'psld'),
    '529',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vivien, Champion of the Wilds'),
    (select id from sets where short_name = 'psld'),
    '519',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Samut, Tyrant Smasher'),
    (select id from sets where short_name = 'psld'),
    '535',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jiang Yanggu, Wildcrafter'),
    (select id from sets where short_name = 'psld'),
    '517',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Teyo, the Shieldmage'),
    (select id from sets where short_name = 'psld'),
    '504',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arlinn, Voice of the Pack'),
    (select id from sets where short_name = 'psld'),
    '516',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sorin, Vengeful Bloodlord'),
    (select id from sets where short_name = 'psld'),
    '524',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kiora, Behemoth Beckoner'),
    (select id from sets where short_name = 'psld'),
    '532',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nahiri, Storm of Stone'),
    (select id from sets where short_name = 'psld'),
    '533',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Saheeli, Sublime Artificer'),
    (select id from sets where short_name = 'psld'),
    '534',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nissa, Who Shakes the World'),
    (select id from sets where short_name = 'psld'),
    '518',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sarkhan the Masterless'),
    (select id from sets where short_name = 'psld'),
    '514',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jaya, Venerated Firemage'),
    (select id from sets where short_name = 'psld'),
    '513',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vraska, Swarm''s Eminence'),
    (select id from sets where short_name = 'psld'),
    '536',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tamiyo, Collector of Tales'),
    (select id from sets where short_name = 'psld'),
    '525',
    'rare'
) 
 on conflict do nothing;
