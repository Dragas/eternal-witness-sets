insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Golem'),
    (select id from sets where short_name = 'tmbs'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tmbs'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Horror'),
    (select id from sets where short_name = 'tmbs'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Germ'),
    (select id from sets where short_name = 'tmbs'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thopter'),
    (select id from sets where short_name = 'tmbs'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Poison Counter'),
    (select id from sets where short_name = 'tmbs'),
    '6',
    'common'
) 
 on conflict do nothing;
