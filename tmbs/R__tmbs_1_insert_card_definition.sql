    insert into mtgcard(name) values ('Golem') on conflict do nothing;
    insert into mtgcard(name) values ('Zombie') on conflict do nothing;
    insert into mtgcard(name) values ('Horror') on conflict do nothing;
    insert into mtgcard(name) values ('Germ') on conflict do nothing;
    insert into mtgcard(name) values ('Thopter') on conflict do nothing;
    insert into mtgcard(name) values ('Poison Counter') on conflict do nothing;
