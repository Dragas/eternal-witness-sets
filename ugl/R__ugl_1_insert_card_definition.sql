    insert into mtgcard(name) values ('Goblin Bowling Team') on conflict do nothing;
    insert into mtgcard(name) values ('Organ Harvest') on conflict do nothing;
    insert into mtgcard(name) values ('Cardboard Carapace') on conflict do nothing;
    insert into mtgcard(name) values ('Handcuffs') on conflict do nothing;
    insert into mtgcard(name) values ('Denied!') on conflict do nothing;
    insert into mtgcard(name) values ('Sorry') on conflict do nothing;
    insert into mtgcard(name) values ('Look at Me, I''m the DCI') on conflict do nothing;
    insert into mtgcard(name) values ('Ghazbán Ogress') on conflict do nothing;
    insert into mtgcard(name) values ('Burning Cinder Fury of Crimson Chaos Fire') on conflict do nothing;
    insert into mtgcard(name) values ('Jack-in-the-Mox') on conflict do nothing;
    insert into mtgcard(name) values ('Clam Session') on conflict do nothing;
    insert into mtgcard(name) values ('Giant Fan') on conflict do nothing;
    insert into mtgcard(name) values ('Flock of Rabid Sheep') on conflict do nothing;
    insert into mtgcard(name) values ('Bronze Calendar') on conflict do nothing;
    insert into mtgcard(name) values ('Fowl Play') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Tutor') on conflict do nothing;
    insert into mtgcard(name) values ('Get a Life') on conflict do nothing;
    insert into mtgcard(name) values ('Growth Spurt') on conflict do nothing;
    insert into mtgcard(name) values ('Mirror Mirror') on conflict do nothing;
    insert into mtgcard(name) values ('Clambassadors') on conflict do nothing;
    insert into mtgcard(name) values ('Hungry Hungry Heifer') on conflict do nothing;
    insert into mtgcard(name) values ('Landfill') on conflict do nothing;
    insert into mtgcard(name) values ('Chicken à la King') on conflict do nothing;
    insert into mtgcard(name) values ('Bureaucracy') on conflict do nothing;
    insert into mtgcard(name) values ('Blacker Lotus') on conflict do nothing;
    insert into mtgcard(name) values ('Clay Pigeon') on conflict do nothing;
    insert into mtgcard(name) values ('Island') on conflict do nothing;
    insert into mtgcard(name) values ('Incoming!') on conflict do nothing;
    insert into mtgcard(name) values ('I''m Rubber, You''re Glue') on conflict do nothing;
    insert into mtgcard(name) values ('Rock Lobster') on conflict do nothing;
    insert into mtgcard(name) values ('Psychic Network') on conflict do nothing;
    insert into mtgcard(name) values ('The Ultimate Nightmare of Wizards of the Coast® Customer Service') on conflict do nothing;
    insert into mtgcard(name) values ('Free-for-All') on conflict do nothing;
    insert into mtgcard(name) values ('Chicken Egg') on conflict do nothing;
    insert into mtgcard(name) values ('Swamp') on conflict do nothing;
    insert into mtgcard(name) values ('Paper Tiger') on conflict do nothing;
    insert into mtgcard(name) values ('Jester''s Sombrero') on conflict do nothing;
    insert into mtgcard(name) values ('Double Take') on conflict do nothing;
    insert into mtgcard(name) values ('Miss Demeanor') on conflict do nothing;
    insert into mtgcard(name) values ('Spatula of the Ages') on conflict do nothing;
    insert into mtgcard(name) values ('Prismatic Wardrobe') on conflict do nothing;
    insert into mtgcard(name) values ('Urza''s Science Fair Project') on conflict do nothing;
    insert into mtgcard(name) values ('Mountain') on conflict do nothing;
    insert into mtgcard(name) values ('Elvish Impersonators') on conflict do nothing;
    insert into mtgcard(name) values ('Charm School') on conflict do nothing;
    insert into mtgcard(name) values ('Sex Appeal') on conflict do nothing;
    insert into mtgcard(name) values ('Squirrel Farm') on conflict do nothing;
    insert into mtgcard(name) values ('Infernal Spawn of Evil') on conflict do nothing;
    insert into mtgcard(name) values ('Common Courtesy') on conflict do nothing;
    insert into mtgcard(name) values ('Jalum Grifter') on conflict do nothing;
    insert into mtgcard(name) values ('Jumbo Imp') on conflict do nothing;
    insert into mtgcard(name) values ('Strategy, Schmategy') on conflict do nothing;
    insert into mtgcard(name) values ('Chaos Confetti') on conflict do nothing;
    insert into mtgcard(name) values ('Mine, Mine, Mine!') on conflict do nothing;
    insert into mtgcard(name) values ('B.F.M. (Big Furry Monster)') on conflict do nothing;
    insert into mtgcard(name) values ('Gerrymandering') on conflict do nothing;
    insert into mtgcard(name) values ('Volrath''s Motion Sensor') on conflict do nothing;
    insert into mtgcard(name) values ('Lexivore') on conflict do nothing;
    insert into mtgcard(name) values ('Forest') on conflict do nothing;
    insert into mtgcard(name) values ('Clam-I-Am') on conflict do nothing;
    insert into mtgcard(name) values ('Temp of the Damned') on conflict do nothing;
    insert into mtgcard(name) values ('Team Spirit') on conflict do nothing;
    insert into mtgcard(name) values ('Once More with Feeling') on conflict do nothing;
    insert into mtgcard(name) values ('Knight of the Hokey Pokey') on conflict do nothing;
    insert into mtgcard(name) values ('Poultrygeist') on conflict do nothing;
    insert into mtgcard(name) values ('Scissors Lizard') on conflict do nothing;
    insert into mtgcard(name) values ('Plains') on conflict do nothing;
    insert into mtgcard(name) values ('Checks and Balances') on conflict do nothing;
    insert into mtgcard(name) values ('Deadhead') on conflict do nothing;
    insert into mtgcard(name) values ('Mesa Chicken') on conflict do nothing;
    insert into mtgcard(name) values ('The Cheese Stands Alone') on conflict do nothing;
    insert into mtgcard(name) values ('Ashnod''s Coupon') on conflict do nothing;
    insert into mtgcard(name) values ('Hurloon Wrangler') on conflict do nothing;
    insert into mtgcard(name) values ('Gus') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin Bookie') on conflict do nothing;
    insert into mtgcard(name) values ('Ow') on conflict do nothing;
    insert into mtgcard(name) values ('Censorship') on conflict do nothing;
    insert into mtgcard(name) values ('Double Play') on conflict do nothing;
    insert into mtgcard(name) values ('Timmy, Power Gamer') on conflict do nothing;
    insert into mtgcard(name) values ('Free-Range Chicken') on conflict do nothing;
    insert into mtgcard(name) values ('Double Cross') on conflict do nothing;
    insert into mtgcard(name) values ('Krazy Kow') on conflict do nothing;
    insert into mtgcard(name) values ('Spark Fiend') on conflict do nothing;
    insert into mtgcard(name) values ('Ricochet') on conflict do nothing;
    insert into mtgcard(name) values ('Double Dip') on conflict do nothing;
    insert into mtgcard(name) values ('Double Deal') on conflict do nothing;
    insert into mtgcard(name) values ('Urza''s Contact Lenses') on conflict do nothing;
