insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Goblin Bowling Team'),
    (select id from sets where short_name = 'ugl'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Organ Harvest'),
    (select id from sets where short_name = 'ugl'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cardboard Carapace'),
    (select id from sets where short_name = 'ugl'),
    '54',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Handcuffs'),
    (select id from sets where short_name = 'ugl'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Denied!'),
    (select id from sets where short_name = 'ugl'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sorry'),
    (select id from sets where short_name = 'ugl'),
    '27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Look at Me, I''m the DCI'),
    (select id from sets where short_name = 'ugl'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ghazbán Ogress'),
    (select id from sets where short_name = 'ugl'),
    '60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Burning Cinder Fury of Crimson Chaos Fire'),
    (select id from sets where short_name = 'ugl'),
    '40',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jack-in-the-Mox'),
    (select id from sets where short_name = 'ugl'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clam Session'),
    (select id from sets where short_name = 'ugl'),
    '20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Giant Fan'),
    (select id from sets where short_name = 'ugl'),
    '74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Flock of Rabid Sheep'),
    (select id from sets where short_name = 'ugl'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bronze Calendar'),
    (select id from sets where short_name = 'ugl'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fowl Play'),
    (select id from sets where short_name = 'ugl'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Tutor'),
    (select id from sets where short_name = 'ugl'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Get a Life'),
    (select id from sets where short_name = 'ugl'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Growth Spurt'),
    (select id from sets where short_name = 'ugl'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mirror Mirror'),
    (select id from sets where short_name = 'ugl'),
    '77',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clambassadors'),
    (select id from sets where short_name = 'ugl'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hungry Hungry Heifer'),
    (select id from sets where short_name = 'ugl'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Landfill'),
    (select id from sets where short_name = 'ugl'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chicken à la King'),
    (select id from sets where short_name = 'ugl'),
    '17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bureaucracy'),
    (select id from sets where short_name = 'ugl'),
    '14',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Blacker Lotus'),
    (select id from sets where short_name = 'ugl'),
    '70',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Clay Pigeon'),
    (select id from sets where short_name = 'ugl'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'ugl'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Incoming!'),
    (select id from sets where short_name = 'ugl'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'I''m Rubber, You''re Glue'),
    (select id from sets where short_name = 'ugl'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rock Lobster'),
    (select id from sets where short_name = 'ugl'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Psychic Network'),
    (select id from sets where short_name = 'ugl'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Ultimate Nightmare of Wizards of the Coast® Customer Service'),
    (select id from sets where short_name = 'ugl'),
    '53',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Free-for-All'),
    (select id from sets where short_name = 'ugl'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chicken Egg'),
    (select id from sets where short_name = 'ugl'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'ugl'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paper Tiger'),
    (select id from sets where short_name = 'ugl'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jester''s Sombrero'),
    (select id from sets where short_name = 'ugl'),
    '76',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Double Take'),
    (select id from sets where short_name = 'ugl'),
    '23',
    'common'
) ,
(
    (select id from mtgcard where name = 'Miss Demeanor'),
    (select id from sets where short_name = 'ugl'),
    '10',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spatula of the Ages'),
    (select id from sets where short_name = 'ugl'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Prismatic Wardrobe'),
    (select id from sets where short_name = 'ugl'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Science Fair Project'),
    (select id from sets where short_name = 'ugl'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'ugl'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Impersonators'),
    (select id from sets where short_name = 'ugl'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Charm School'),
    (select id from sets where short_name = 'ugl'),
    '1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sex Appeal'),
    (select id from sets where short_name = 'ugl'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squirrel Farm'),
    (select id from sets where short_name = 'ugl'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Infernal Spawn of Evil'),
    (select id from sets where short_name = 'ugl'),
    '33',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Common Courtesy'),
    (select id from sets where short_name = 'ugl'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Jalum Grifter'),
    (select id from sets where short_name = 'ugl'),
    '47',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jumbo Imp'),
    (select id from sets where short_name = 'ugl'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Strategy, Schmategy'),
    (select id from sets where short_name = 'ugl'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chaos Confetti'),
    (select id from sets where short_name = 'ugl'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mine, Mine, Mine!'),
    (select id from sets where short_name = 'ugl'),
    '65',
    'rare'
) ,
(
    (select id from mtgcard where name = 'B.F.M. (Big Furry Monster)'),
    (select id from sets where short_name = 'ugl'),
    '28',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gerrymandering'),
    (select id from sets where short_name = 'ugl'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volrath''s Motion Sensor'),
    (select id from sets where short_name = 'ugl'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lexivore'),
    (select id from sets where short_name = 'ugl'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'ugl'),
    '88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Clam-I-Am'),
    (select id from sets where short_name = 'ugl'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Temp of the Damned'),
    (select id from sets where short_name = 'ugl'),
    '38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Team Spirit'),
    (select id from sets where short_name = 'ugl'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Once More with Feeling'),
    (select id from sets where short_name = 'ugl'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Knight of the Hokey Pokey'),
    (select id from sets where short_name = 'ugl'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Poultrygeist'),
    (select id from sets where short_name = 'ugl'),
    '37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scissors Lizard'),
    (select id from sets where short_name = 'ugl'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'ugl'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Checks and Balances'),
    (select id from sets where short_name = 'ugl'),
    '16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deadhead'),
    (select id from sets where short_name = 'ugl'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mesa Chicken'),
    (select id from sets where short_name = 'ugl'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Cheese Stands Alone'),
    (select id from sets where short_name = 'ugl'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'B.F.M. (Big Furry Monster)'),
    (select id from sets where short_name = 'ugl'),
    '29',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashnod''s Coupon'),
    (select id from sets where short_name = 'ugl'),
    '69',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hurloon Wrangler'),
    (select id from sets where short_name = 'ugl'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gus'),
    (select id from sets where short_name = 'ugl'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Bookie'),
    (select id from sets where short_name = 'ugl'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ow'),
    (select id from sets where short_name = 'ugl'),
    '36',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Censorship'),
    (select id from sets where short_name = 'ugl'),
    '15',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Double Play'),
    (select id from sets where short_name = 'ugl'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Timmy, Power Gamer'),
    (select id from sets where short_name = 'ugl'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Free-Range Chicken'),
    (select id from sets where short_name = 'ugl'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Double Cross'),
    (select id from sets where short_name = 'ugl'),
    '31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Krazy Kow'),
    (select id from sets where short_name = 'ugl'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spark Fiend'),
    (select id from sets where short_name = 'ugl'),
    '51',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ricochet'),
    (select id from sets where short_name = 'ugl'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Double Dip'),
    (select id from sets where short_name = 'ugl'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Double Deal'),
    (select id from sets where short_name = 'ugl'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urza''s Contact Lenses'),
    (select id from sets where short_name = 'ugl'),
    '82',
    'uncommon'
) 
 on conflict do nothing;
