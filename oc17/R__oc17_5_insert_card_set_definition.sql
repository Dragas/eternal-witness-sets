insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Edgar Markov'),
    (select id from sets where short_name = 'oc17'),
    '36',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Inalla, Archmage Ritualist'),
    (select id from sets where short_name = 'oc17'),
    '38',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'The Ur-Dragon'),
    (select id from sets where short_name = 'oc17'),
    '48',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Arahbo, Roar of the World'),
    (select id from sets where short_name = 'oc17'),
    '35',
    'mythic'
) 
 on conflict do nothing;
