insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Taurean Mauler'),
    (select id from sets where short_name = 'arc'),
    '49',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sorcerer''s Strongbox'),
    (select id from sets where short_name = 'arc'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dreamstone Hedron'),
    (select id from sets where short_name = 'arc'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kaervek the Merciless'),
    (select id from sets where short_name = 'arc'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ethersworn Shieldmage'),
    (select id from sets where short_name = 'arc'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infectious Horror'),
    (select id from sets where short_name = 'arc'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fires of Yavimaya'),
    (select id from sets where short_name = 'arc'),
    '86',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dragon Fodder'),
    (select id from sets where short_name = 'arc'),
    '34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'arc'),
    '148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Breath of Darigaaz'),
    (select id from sets where short_name = 'arc'),
    '31',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Beacon of Unrest'),
    (select id from sets where short_name = 'arc'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shriekmaw'),
    (select id from sets where short_name = 'arc'),
    '24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Heroes'' Reunion'),
    (select id from sets where short_name = 'arc'),
    '87',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Dimir Signet'),
    (select id from sets where short_name = 'arc'),
    '104',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gathan Raiders'),
    (select id from sets where short_name = 'arc'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'arc'),
    '140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Colossal Might'),
    (select id from sets where short_name = 'arc'),
    '83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vitu-Ghazi, the City-Tree'),
    (select id from sets where short_name = 'arc'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kilnmouth Dragon'),
    (select id from sets where short_name = 'arc'),
    '44',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Llanowar Reborn'),
    (select id from sets where short_name = 'arc'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Leaf Gilder'),
    (select id from sets where short_name = 'arc'),
    '63',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skirk Commando'),
    (select id from sets where short_name = 'arc'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inferno Trap'),
    (select id from sets where short_name = 'arc'),
    '43',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Batwing Brume'),
    (select id from sets where short_name = 'arc'),
    '80',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Khalni Garden'),
    (select id from sets where short_name = 'arc'),
    '127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Unbender Tine'),
    (select id from sets where short_name = 'arc'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sun Droplet'),
    (select id from sets where short_name = 'arc'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bituminous Blast'),
    (select id from sets where short_name = 'arc'),
    '81',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Twisted Abomination'),
    (select id from sets where short_name = 'arc'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thunderstaff'),
    (select id from sets where short_name = 'arc'),
    '122',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fierce Empath'),
    (select id from sets where short_name = 'arc'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'March of the Machines'),
    (select id from sets where short_name = 'arc'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Chandra''s Outrage'),
    (select id from sets where short_name = 'arc'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barren Moor'),
    (select id from sets where short_name = 'arc'),
    '124',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dregscape Zombie'),
    (select id from sets where short_name = 'arc'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spider Umbra'),
    (select id from sets where short_name = 'arc'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'arc'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Synod Centurion'),
    (select id from sets where short_name = 'arc'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Verdeloth the Ancient'),
    (select id from sets where short_name = 'arc'),
    '72',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Azorius Signet'),
    (select id from sets where short_name = 'arc'),
    '103',
    'common'
) ,
(
    (select id from mtgcard where name = 'Watchwolf'),
    (select id from sets where short_name = 'arc'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Corpse Connoisseur'),
    (select id from sets where short_name = 'arc'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sign in Blood'),
    (select id from sets where short_name = 'arc'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terminate'),
    (select id from sets where short_name = 'arc'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Juggernaut'),
    (select id from sets where short_name = 'arc'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sundering Titan'),
    (select id from sets where short_name = 'arc'),
    '118',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fog'),
    (select id from sets where short_name = 'arc'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'arc'),
    '150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'arc'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'arc'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Selesnya Guildmage'),
    (select id from sets where short_name = 'arc'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mistvein Borderpost'),
    (select id from sets where short_name = 'arc'),
    '90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'arc'),
    '141',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragonspeaker Shaman'),
    (select id from sets where short_name = 'arc'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harmonize'),
    (select id from sets where short_name = 'arc'),
    '59',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duplicant'),
    (select id from sets where short_name = 'arc'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reassembling Skeleton'),
    (select id from sets where short_name = 'arc'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Elder'),
    (select id from sets where short_name = 'arc'),
    '68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fertilid'),
    (select id from sets where short_name = 'arc'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Battering Craghorn'),
    (select id from sets where short_name = 'arc'),
    '30',
    'common'
) ,
(
    (select id from mtgcard where name = 'Two-Headed Dragon'),
    (select id from sets where short_name = 'arc'),
    '50',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dragon Whelp'),
    (select id from sets where short_name = 'arc'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Obelisk of Esper'),
    (select id from sets where short_name = 'arc'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Architects of Will'),
    (select id from sets where short_name = 'arc'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flameblast Dragon'),
    (select id from sets where short_name = 'arc'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magister Sphinx'),
    (select id from sets where short_name = 'arc'),
    '89',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mosswort Bridge'),
    (select id from sets where short_name = 'arc'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rakdos Guildmage'),
    (select id from sets where short_name = 'arc'),
    '92',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gleeful Sabotage'),
    (select id from sets where short_name = 'arc'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'arc'),
    '139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thelonite Hermit'),
    (select id from sets where short_name = 'arc'),
    '71',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Metallurgeon'),
    (select id from sets where short_name = 'arc'),
    '2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Graypelt Refuge'),
    (select id from sets where short_name = 'arc'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spin into Myth'),
    (select id from sets where short_name = 'arc'),
    '8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unmake'),
    (select id from sets where short_name = 'arc'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Makeshift Mannequin'),
    (select id from sets where short_name = 'arc'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Primal Command'),
    (select id from sets where short_name = 'arc'),
    '66',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'arc'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon Breath'),
    (select id from sets where short_name = 'arc'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'arc'),
    '147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Secluded Steppe'),
    (select id from sets where short_name = 'arc'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Festering Goblin'),
    (select id from sets where short_name = 'arc'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Memnarch'),
    (select id from sets where short_name = 'arc'),
    '112',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Bog Witch'),
    (select id from sets where short_name = 'arc'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chameleon Colossus'),
    (select id from sets where short_name = 'arc'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wall of Roots'),
    (select id from sets where short_name = 'arc'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forgotten Ancient'),
    (select id from sets where short_name = 'arc'),
    '57',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Kamahl, Fist of Krosa'),
    (select id from sets where short_name = 'arc'),
    '61',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'arc'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Leonin Abunas'),
    (select id from sets where short_name = 'arc'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Furnace Whelp'),
    (select id from sets where short_name = 'arc'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Volcanic Fallout'),
    (select id from sets where short_name = 'arc'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Skullcage'),
    (select id from sets where short_name = 'arc'),
    '115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Branching Bolt'),
    (select id from sets where short_name = 'arc'),
    '82',
    'common'
) ,
(
    (select id from mtgcard where name = 'Synod Sanctum'),
    (select id from sets where short_name = 'arc'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wickerbough Elder'),
    (select id from sets where short_name = 'arc'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plummet'),
    (select id from sets where short_name = 'arc'),
    '65',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sanctum Gargoyle'),
    (select id from sets where short_name = 'arc'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Path to Exile'),
    (select id from sets where short_name = 'arc'),
    '4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nantuko Monastery'),
    (select id from sets where short_name = 'arc'),
    '131',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Savage Twister'),
    (select id from sets where short_name = 'arc'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Signet'),
    (select id from sets where short_name = 'arc'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avatar of Discord'),
    (select id from sets where short_name = 'arc'),
    '79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aether Spellbomb'),
    (select id from sets where short_name = 'arc'),
    '102',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skirk Marauder'),
    (select id from sets where short_name = 'arc'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Artisan of Kozilek'),
    (select id from sets where short_name = 'arc'),
    '123',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Feral Hydra'),
    (select id from sets where short_name = 'arc'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Greaves'),
    (select id from sets where short_name = 'arc'),
    '110',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Dryad'),
    (select id from sets where short_name = 'arc'),
    '75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Incremental Blight'),
    (select id from sets where short_name = 'arc'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'arc'),
    '145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hellkite Charger'),
    (select id from sets where short_name = 'arc'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Molimo, Maro-Sorcerer'),
    (select id from sets where short_name = 'arc'),
    '64',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Fieldmist Borderpost'),
    (select id from sets where short_name = 'arc'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Agony Warp'),
    (select id from sets where short_name = 'arc'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reanimate'),
    (select id from sets where short_name = 'arc'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'arc'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fireball'),
    (select id from sets where short_name = 'arc'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampiric Dragon'),
    (select id from sets where short_name = 'arc'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Verge'),
    (select id from sets where short_name = 'arc'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Master Transmuter'),
    (select id from sets where short_name = 'arc'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Zombify'),
    (select id from sets where short_name = 'arc'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rakdos Carnarium'),
    (select id from sets where short_name = 'arc'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Torrent of Souls'),
    (select id from sets where short_name = 'arc'),
    '96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Extractor Demon'),
    (select id from sets where short_name = 'arc'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hunting Moa'),
    (select id from sets where short_name = 'arc'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lodestone Golem'),
    (select id from sets where short_name = 'arc'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Imperial Hellkite'),
    (select id from sets where short_name = 'arc'),
    '42',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avatar of Woe'),
    (select id from sets where short_name = 'arc'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cemetery Reaper'),
    (select id from sets where short_name = 'arc'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shinen of Life''s Roar'),
    (select id from sets where short_name = 'arc'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'arc'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Urborg Syphon-Mage'),
    (select id from sets where short_name = 'arc'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Infest'),
    (select id from sets where short_name = 'arc'),
    '19',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seething Song'),
    (select id from sets where short_name = 'arc'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie Infestation'),
    (select id from sets where short_name = 'arc'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oblivion Ring'),
    (select id from sets where short_name = 'arc'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Armadillo Cloak'),
    (select id from sets where short_name = 'arc'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Everflowing Chalice'),
    (select id from sets where short_name = 'arc'),
    '107',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'arc'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ryusei, the Falling Star'),
    (select id from sets where short_name = 'arc'),
    '45',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tranquil Thicket'),
    (select id from sets where short_name = 'arc'),
    '135',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wax // Wane'),
    (select id from sets where short_name = 'arc'),
    '101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kazandu Refuge'),
    (select id from sets where short_name = 'arc'),
    '126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pale Recluse'),
    (select id from sets where short_name = 'arc'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rancor'),
    (select id from sets where short_name = 'arc'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scion of Darkness'),
    (select id from sets where short_name = 'arc'),
    '23',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Krosan Tusker'),
    (select id from sets where short_name = 'arc'),
    '62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gruul Signet'),
    (select id from sets where short_name = 'arc'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thran Dynamo'),
    (select id from sets where short_name = 'arc'),
    '121',
    'uncommon'
) 
 on conflict do nothing;
