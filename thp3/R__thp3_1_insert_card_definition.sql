    insert into mtgcard(name) values ('Bow of the Hunter') on conflict do nothing;
    insert into mtgcard(name) values ('Lash of the Tyrant') on conflict do nothing;
    insert into mtgcard(name) values ('Hall of Triumph') on conflict do nothing;
    insert into mtgcard(name) values ('The Destined') on conflict do nothing;
    insert into mtgcard(name) values ('Spear of the General') on conflict do nothing;
    insert into mtgcard(name) values ('The Champion') on conflict do nothing;
    insert into mtgcard(name) values ('Enhancement Stickers') on conflict do nothing;
    insert into mtgcard(name) values ('Cloak of the Philosopher') on conflict do nothing;
    insert into mtgcard(name) values ('Axe of the Warmonger') on conflict do nothing;
