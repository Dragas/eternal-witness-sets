insert into mtgcard_type(card_id, type_id) VALUES
    (
        (select mtgcard.id from mtgcard where name = 'Bow of the Hunter'),
        (select types.id from types where name = 'Hero')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bow of the Hunter'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Bow of the Hunter'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lash of the Tyrant'),
        (select types.id from types where name = 'Hero')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lash of the Tyrant'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Lash of the Tyrant'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hall of Triumph'),
        (select types.id from types where name = 'Legendary')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Hall of Triumph'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Destined'),
        (select types.id from types where name = 'Hero')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spear of the General'),
        (select types.id from types where name = 'Hero')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spear of the General'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Spear of the General'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'The Champion'),
        (select types.id from types where name = 'Hero')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Enhancement Stickers'),
        (select types.id from types where name = 'Card')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cloak of the Philosopher'),
        (select types.id from types where name = 'Hero')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cloak of the Philosopher'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Cloak of the Philosopher'),
        (select types.id from types where name = 'Equipment')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Axe of the Warmonger'),
        (select types.id from types where name = 'Hero')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Axe of the Warmonger'),
        (select types.id from types where name = 'Artifact')
    ) ,
    (
        (select mtgcard.id from mtgcard where name = 'Axe of the Warmonger'),
        (select types.id from types where name = 'Equipment')
    ) 
 on conflict do nothing;
