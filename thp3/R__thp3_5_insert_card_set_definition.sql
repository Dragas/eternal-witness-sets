insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Bow of the Hunter'),
    (select id from sets where short_name = 'thp3'),
    '7e',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lash of the Tyrant'),
    (select id from sets where short_name = 'thp3'),
    '7c',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hall of Triumph'),
    (select id from sets where short_name = 'thp3'),
    '162',
    'rare'
) ,
(
    (select id from mtgcard where name = 'The Destined'),
    (select id from sets where short_name = 'thp3'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spear of the General'),
    (select id from sets where short_name = 'thp3'),
    '7a',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Champion'),
    (select id from sets where short_name = 'thp3'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enhancement Stickers'),
    (select id from sets where short_name = 'thp3'),
    '0',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloak of the Philosopher'),
    (select id from sets where short_name = 'thp3'),
    '7b',
    'common'
) ,
(
    (select id from mtgcard where name = 'Axe of the Warmonger'),
    (select id from sets where short_name = 'thp3'),
    '7d',
    'common'
) 
 on conflict do nothing;
