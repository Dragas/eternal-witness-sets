insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Unruly Mob'),
    (select id from sets where short_name = 'soi'),
    '47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inexorable Blob'),
    (select id from sets where short_name = 'soi'),
    '212',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hulking Devil'),
    (select id from sets where short_name = 'soi'),
    '165',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seagraf Skaab'),
    (select id from sets where short_name = 'soi'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Scourge Wolf'),
    (select id from sets where short_name = 'soi'),
    '179',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Open the Armory'),
    (select id from sets where short_name = 'soi'),
    '32',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stoic Builder'),
    (select id from sets where short_name = 'soi'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fork in the Road'),
    (select id from sets where short_name = 'soi'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forsaken Sanctuary'),
    (select id from sets where short_name = 'soi'),
    '273',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avacyn''s Judgment'),
    (select id from sets where short_name = 'soi'),
    '145',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Harvest Hand // Scrounged Scythe'),
    (select id from sets where short_name = 'soi'),
    '256',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silverstrike'),
    (select id from sets where short_name = 'soi'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cathar''s Companion'),
    (select id from sets where short_name = 'soi'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Farbog Revenant'),
    (select id from sets where short_name = 'soi'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hound of the Farbogs'),
    (select id from sets where short_name = 'soi'),
    '117',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avacynian Missionaries // Lunarch Inquisitors'),
    (select id from sets where short_name = 'soi'),
    '6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flameblade Angel'),
    (select id from sets where short_name = 'soi'),
    '157',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Devils'' Playground'),
    (select id from sets where short_name = 'soi'),
    '151',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'soi'),
    '288',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wild-Field Scarecrow'),
    (select id from sets where short_name = 'soi'),
    '269',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Hinterland Logger // Timber Shredder'),
    (select id from sets where short_name = 'soi'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Crow of Dark Tidings'),
    (select id from sets where short_name = 'soi'),
    '105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Warped Landscape'),
    (select id from sets where short_name = 'soi'),
    '280',
    'common'
) ,
(
    (select id from mtgcard where name = 'Deny Existence'),
    (select id from sets where short_name = 'soi'),
    '55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Paranoid Parish-Blade'),
    (select id from sets where short_name = 'soi'),
    '33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Woodland Stream'),
    (select id from sets where short_name = 'soi'),
    '282',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drunau Corpse Trawler'),
    (select id from sets where short_name = 'soi'),
    '57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Geistblast'),
    (select id from sets where short_name = 'soi'),
    '160',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ongoing Investigation'),
    (select id from sets where short_name = 'soi'),
    '77',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'soi'),
    '289',
    'common'
) ,
(
    (select id from mtgcard where name = 'Catalog'),
    (select id from sets where short_name = 'soi'),
    '51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fortified Village'),
    (select id from sets where short_name = 'soi'),
    '274',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Corrupted Grafstone'),
    (select id from sets where short_name = 'soi'),
    '253',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jace, Unraveler of Secrets'),
    (select id from sets where short_name = 'soi'),
    '69',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Game Trail'),
    (select id from sets where short_name = 'soi'),
    '276',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Engulf the Shore'),
    (select id from sets where short_name = 'soi'),
    '58',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Soul Swallower'),
    (select id from sets where short_name = 'soi'),
    '230',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Foul Orchard'),
    (select id from sets where short_name = 'soi'),
    '275',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lambholt Pacifist // Lambholt Butcher'),
    (select id from sets where short_name = 'soi'),
    '215',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tamiyo''s Journal'),
    (select id from sets where short_name = 'soi'),
    '265†d',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Broken Concentration'),
    (select id from sets where short_name = 'soi'),
    '50',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Graf Mole'),
    (select id from sets where short_name = 'soi'),
    '207',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sanitarium Skeleton'),
    (select id from sets where short_name = 'soi'),
    '133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forgotten Creation'),
    (select id from sets where short_name = 'soi'),
    '63',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Cryptolith Rite'),
    (select id from sets where short_name = 'soi'),
    '200',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Relentless Dead'),
    (select id from sets where short_name = 'soi'),
    '131',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Dissension in the Ranks'),
    (select id from sets where short_name = 'soi'),
    '152',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'soi'),
    '296',
    'common'
) ,
(
    (select id from mtgcard where name = 'Welcome to the Fold'),
    (select id from sets where short_name = 'soi'),
    '96',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Byway Courier'),
    (select id from sets where short_name = 'soi'),
    '196',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rabid Bite'),
    (select id from sets where short_name = 'soi'),
    '223',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tormenting Voice'),
    (select id from sets where short_name = 'soi'),
    '186',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skeleton Key'),
    (select id from sets where short_name = 'soi'),
    '263',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rancid Rats'),
    (select id from sets where short_name = 'soi'),
    '130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murderous Compulsion'),
    (select id from sets where short_name = 'soi'),
    '126',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kessig Forgemaster // Flameheart Werewolf'),
    (select id from sets where short_name = 'soi'),
    '169',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Merciless Resolve'),
    (select id from sets where short_name = 'soi'),
    '123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thing in the Ice // Awoken Horror'),
    (select id from sets where short_name = 'soi'),
    '92',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nearheath Chaplain'),
    (select id from sets where short_name = 'soi'),
    '29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drownyard Explorers'),
    (select id from sets where short_name = 'soi'),
    '56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Apothecary Geist'),
    (select id from sets where short_name = 'soi'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Liliana''s Indignation'),
    (select id from sets where short_name = 'soi'),
    '120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Niblis of Dusk'),
    (select id from sets where short_name = 'soi'),
    '76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Foreboding Ruins'),
    (select id from sets where short_name = 'soi'),
    '272',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Twins of Maurer Estate'),
    (select id from sets where short_name = 'soi'),
    '142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jace''s Scrutiny'),
    (select id from sets where short_name = 'soi'),
    '70',
    'common'
) ,
(
    (select id from mtgcard where name = 'From Under the Floorboards'),
    (select id from sets where short_name = 'soi'),
    '111',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Call the Bloodline'),
    (select id from sets where short_name = 'soi'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fevered Visions'),
    (select id from sets where short_name = 'soi'),
    '244',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uncaged Fury'),
    (select id from sets where short_name = 'soi'),
    '188',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tenacity'),
    (select id from sets where short_name = 'soi'),
    '42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'soi'),
    '295',
    'common'
) ,
(
    (select id from mtgcard where name = 'Daring Sleuth // Bearer of Overwhelming Truths'),
    (select id from sets where short_name = 'soi'),
    '54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Village Messenger // Moonrise Intruder'),
    (select id from sets where short_name = 'soi'),
    '190',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tamiyo''s Journal'),
    (select id from sets where short_name = 'soi'),
    '265†a',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Geier Reach Bandit // Vildin-Pack Alpha'),
    (select id from sets where short_name = 'soi'),
    '159',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Inspiring Captain'),
    (select id from sets where short_name = 'soi'),
    '25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Drogskol Cavalry'),
    (select id from sets where short_name = 'soi'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Falkenrath Gorger'),
    (select id from sets where short_name = 'soi'),
    '155',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Highland Lake'),
    (select id from sets where short_name = 'soi'),
    '277',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stensia Masquerade'),
    (select id from sets where short_name = 'soi'),
    '184',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Trail of Evidence'),
    (select id from sets where short_name = 'soi'),
    '93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fleeting Memories'),
    (select id from sets where short_name = 'soi'),
    '62',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Second Harvest'),
    (select id from sets where short_name = 'soi'),
    '227',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Uninvited Geist // Unimpeded Trespasser'),
    (select id from sets where short_name = 'soi'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Incorrigible Youths'),
    (select id from sets where short_name = 'soi'),
    '166',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stern Constable'),
    (select id from sets where short_name = 'soi'),
    '39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gibbering Fiend'),
    (select id from sets where short_name = 'soi'),
    '161',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harness the Storm'),
    (select id from sets where short_name = 'soi'),
    '163',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Strength of Arms'),
    (select id from sets where short_name = 'soi'),
    '40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pick the Brain'),
    (select id from sets where short_name = 'soi'),
    '129',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silent Observer'),
    (select id from sets where short_name = 'soi'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Asylum Visitor'),
    (select id from sets where short_name = 'soi'),
    '99',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'soi'),
    '283',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Gitrog Monster'),
    (select id from sets where short_name = 'soi'),
    '245',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Devilthorn Fox'),
    (select id from sets where short_name = 'soi'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moorland Drifter'),
    (select id from sets where short_name = 'soi'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reaper of Flight Moonsilver'),
    (select id from sets where short_name = 'soi'),
    '36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Spiteful Motives'),
    (select id from sets where short_name = 'soi'),
    '183',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Might Beyond Reason'),
    (select id from sets where short_name = 'soi'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'To the Slaughter'),
    (select id from sets where short_name = 'soi'),
    '139',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Biting Rain'),
    (select id from sets where short_name = 'soi'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Puncturing Light'),
    (select id from sets where short_name = 'soi'),
    '35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Odric, Lunarch Marshal'),
    (select id from sets where short_name = 'soi'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Survive the Night'),
    (select id from sets where short_name = 'soi'),
    '41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Neglected Heirloom // Ashmouth Blade'),
    (select id from sets where short_name = 'soi'),
    '260',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyre Hound'),
    (select id from sets where short_name = 'soi'),
    '174',
    'common'
) ,
(
    (select id from mtgcard where name = 'Press for Answers'),
    (select id from sets where short_name = 'soi'),
    '80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Loam Dryad'),
    (select id from sets where short_name = 'soi'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fiery Temper'),
    (select id from sets where short_name = 'soi'),
    '156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prized Amalgam'),
    (select id from sets where short_name = 'soi'),
    '249',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Essence Flux'),
    (select id from sets where short_name = 'soi'),
    '61',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Erdwal Illuminator'),
    (select id from sets where short_name = 'soi'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'soi'),
    '284',
    'common'
) ,
(
    (select id from mtgcard where name = 'Epiphany at the Drownyard'),
    (select id from sets where short_name = 'soi'),
    '59',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pack Guardian'),
    (select id from sets where short_name = 'soi'),
    '221',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Seasons Past'),
    (select id from sets where short_name = 'soi'),
    '226',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Not Forgotten'),
    (select id from sets where short_name = 'soi'),
    '30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Malevolent Whispers'),
    (select id from sets where short_name = 'soi'),
    '173',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Militant Inquisitor'),
    (select id from sets where short_name = 'soi'),
    '26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pieces of the Puzzle'),
    (select id from sets where short_name = 'soi'),
    '78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Murderer''s Axe'),
    (select id from sets where short_name = 'soi'),
    '259',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gatstaf Arsonists // Gatstaf Ravagers'),
    (select id from sets where short_name = 'soi'),
    '158',
    'common'
) ,
(
    (select id from mtgcard where name = 'Solitary Hunter // One of the Pack'),
    (select id from sets where short_name = 'soi'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Runaway Carriage'),
    (select id from sets where short_name = 'soi'),
    '261',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'soi'),
    '287',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rise from the Tides'),
    (select id from sets where short_name = 'soi'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Invasive Surgery'),
    (select id from sets where short_name = 'soi'),
    '68',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghoulcaller''s Accomplice'),
    (select id from sets where short_name = 'soi'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'soi'),
    '290',
    'common'
) ,
(
    (select id from mtgcard where name = 'Olivia, Mobilized for War'),
    (select id from sets where short_name = 'soi'),
    '248',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Tamiyo''s Journal'),
    (select id from sets where short_name = 'soi'),
    '265†c',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Moldgraf Scavenger'),
    (select id from sets where short_name = 'soi'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Alms of the Vein'),
    (select id from sets where short_name = 'soi'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shard of Broken Glass'),
    (select id from sets where short_name = 'soi'),
    '262',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pious Evangel // Wayward Disciple'),
    (select id from sets where short_name = 'soi'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stallion of Ashmouth'),
    (select id from sets where short_name = 'soi'),
    '136',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bound by Moonsilver'),
    (select id from sets where short_name = 'soi'),
    '7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Humble the Brute'),
    (select id from sets where short_name = 'soi'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bygone Bishop'),
    (select id from sets where short_name = 'soi'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hanweir Militia Captain // Westvale Cult Leader'),
    (select id from sets where short_name = 'soi'),
    '21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crawling Sensation'),
    (select id from sets where short_name = 'soi'),
    '199',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Behind the Scenes'),
    (select id from sets where short_name = 'soi'),
    '100',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Triskaidekaphobia'),
    (select id from sets where short_name = 'soi'),
    '141',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Accursed Witch // Infectious Curse'),
    (select id from sets where short_name = 'soi'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Lamplighter of Selhoff'),
    (select id from sets where short_name = 'soi'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insolent Neonate'),
    (select id from sets where short_name = 'soi'),
    '168',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sinister Concoction'),
    (select id from sets where short_name = 'soi'),
    '135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'True-Faith Censer'),
    (select id from sets where short_name = 'soi'),
    '267',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vessel of Volatility'),
    (select id from sets where short_name = 'soi'),
    '189',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vessel of Malignity'),
    (select id from sets where short_name = 'soi'),
    '144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Always Watching'),
    (select id from sets where short_name = 'soi'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Nahiri, the Harbinger'),
    (select id from sets where short_name = 'soi'),
    '247',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ulrich''s Kindred'),
    (select id from sets where short_name = 'soi'),
    '187',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'soi'),
    '291',
    'common'
) ,
(
    (select id from mtgcard where name = 'Brain in a Jar'),
    (select id from sets where short_name = 'soi'),
    '252',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vessel of Nascency'),
    (select id from sets where short_name = 'soi'),
    '237',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ulvenwald Hydra'),
    (select id from sets where short_name = 'soi'),
    '235',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Creeping Dread'),
    (select id from sets where short_name = 'soi'),
    '104',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pale Rider of Trostad'),
    (select id from sets where short_name = 'soi'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mad Prophet'),
    (select id from sets where short_name = 'soi'),
    '171',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Diregraf Colossus'),
    (select id from sets where short_name = 'soi'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anguished Unmaking'),
    (select id from sets where short_name = 'soi'),
    '242',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ulvenwald Mysteries'),
    (select id from sets where short_name = 'soi'),
    '236',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kessig Dire Swine'),
    (select id from sets where short_name = 'soi'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rottenheart Ghoul'),
    (select id from sets where short_name = 'soi'),
    '132',
    'common'
) ,
(
    (select id from mtgcard where name = 'Epitaph Golem'),
    (select id from sets where short_name = 'soi'),
    '254',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sage of Ancient Lore // Werewolf of Ancient Hunger'),
    (select id from sets where short_name = 'soi'),
    '225',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Throttle'),
    (select id from sets where short_name = 'soi'),
    '138',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Axe'),
    (select id from sets where short_name = 'soi'),
    '170',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Just the Wind'),
    (select id from sets where short_name = 'soi'),
    '71',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aim High'),
    (select id from sets where short_name = 'soi'),
    '193',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ember-Eye Wolf'),
    (select id from sets where short_name = 'soi'),
    '154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Choked Estuary'),
    (select id from sets where short_name = 'soi'),
    '270',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ever After'),
    (select id from sets where short_name = 'soi'),
    '109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Stormrider Spirit'),
    (select id from sets where short_name = 'soi'),
    '91',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thornhide Wolves'),
    (select id from sets where short_name = 'soi'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Senseless Rage'),
    (select id from sets where short_name = 'soi'),
    '180',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gryff''s Boon'),
    (select id from sets where short_name = 'soi'),
    '20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Expose Evil'),
    (select id from sets where short_name = 'soi'),
    '19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howlpack Resurgence'),
    (select id from sets where short_name = 'soi'),
    '211',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silverfur Partisan'),
    (select id from sets where short_name = 'soi'),
    '228',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Reduce to Ashes'),
    (select id from sets where short_name = 'soi'),
    '176',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elusive Tormentor // Insidious Mist'),
    (select id from sets where short_name = 'soi'),
    '108',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Veteran Cathar'),
    (select id from sets where short_name = 'soi'),
    '238',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Drownyard Temple'),
    (select id from sets where short_name = 'soi'),
    '271',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Macabre Waltz'),
    (select id from sets where short_name = 'soi'),
    '121',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hope Against Hope'),
    (select id from sets where short_name = 'soi'),
    '22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rattlechains'),
    (select id from sets where short_name = 'soi'),
    '81',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'soi'),
    '292',
    'common'
) ,
(
    (select id from mtgcard where name = 'Structural Distortion'),
    (select id from sets where short_name = 'soi'),
    '185',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nagging Thoughts'),
    (select id from sets where short_name = 'soi'),
    '74',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nahiri''s Machinations'),
    (select id from sets where short_name = 'soi'),
    '28',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stromkirk Mentor'),
    (select id from sets where short_name = 'soi'),
    '137',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spectral Shepherd'),
    (select id from sets where short_name = 'soi'),
    '38',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sanguinary Mage'),
    (select id from sets where short_name = 'soi'),
    '178',
    'common'
) ,
(
    (select id from mtgcard where name = 'Groundskeeper'),
    (select id from sets where short_name = 'soi'),
    '208',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wicker Witch'),
    (select id from sets where short_name = 'soi'),
    '268',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archangel Avacyn // Avacyn, the Purifier'),
    (select id from sets where short_name = 'soi'),
    '5',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Pore Over the Pages'),
    (select id from sets where short_name = 'soi'),
    '79',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampire Noble'),
    (select id from sets where short_name = 'soi'),
    '143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Obsessive Skinner'),
    (select id from sets where short_name = 'soi'),
    '220',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Arlinn Kord // Arlinn, Embraced by the Moon'),
    (select id from sets where short_name = 'soi'),
    '243',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'soi'),
    '294',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inner Struggle'),
    (select id from sets where short_name = 'soi'),
    '167',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vessel of Paramnesia'),
    (select id from sets where short_name = 'soi'),
    '95',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tamiyo''s Journal'),
    (select id from sets where short_name = 'soi'),
    '265',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Aberrant Researcher // Perfected Form'),
    (select id from sets where short_name = 'soi'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloodmad Vampire'),
    (select id from sets where short_name = 'soi'),
    '146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dual Shot'),
    (select id from sets where short_name = 'soi'),
    '153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weirding Wood'),
    (select id from sets where short_name = 'soi'),
    '240',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mindwrack Demon'),
    (select id from sets where short_name = 'soi'),
    '124',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Port Town'),
    (select id from sets where short_name = 'soi'),
    '278',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Dauntless Cathar'),
    (select id from sets where short_name = 'soi'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Traverse the Ulvenwald'),
    (select id from sets where short_name = 'soi'),
    '234',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Watcher in the Web'),
    (select id from sets where short_name = 'soi'),
    '239',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'soi'),
    '286',
    'common'
) ,
(
    (select id from mtgcard where name = 'Skin Invasion // Skin Shedder'),
    (select id from sets where short_name = 'soi'),
    '182',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stitched Mangler'),
    (select id from sets where short_name = 'soi'),
    '89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Altered Ego'),
    (select id from sets where short_name = 'soi'),
    '241',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Slayer''s Plate'),
    (select id from sets where short_name = 'soi'),
    '264',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Compelling Deterrence'),
    (select id from sets where short_name = 'soi'),
    '52',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Westvale Abbey // Ormendahl, Profane Prince'),
    (select id from sets where short_name = 'soi'),
    '281',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sorin, Grim Nemesis'),
    (select id from sets where short_name = 'soi'),
    '251',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Confront the Unknown'),
    (select id from sets where short_name = 'soi'),
    '198',
    'common'
) ,
(
    (select id from mtgcard where name = 'Invocation of Saint Traft'),
    (select id from sets where short_name = 'soi'),
    '246',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Descend upon the Sinful'),
    (select id from sets where short_name = 'soi'),
    '13',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Goldnight Castigator'),
    (select id from sets where short_name = 'soi'),
    '162',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Ravenous Bloodseeker'),
    (select id from sets where short_name = 'soi'),
    '175',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Emissary of the Sleepless'),
    (select id from sets where short_name = 'soi'),
    '17',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grotesque Mutation'),
    (select id from sets where short_name = 'soi'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sin Prodder'),
    (select id from sets where short_name = 'soi'),
    '181',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Heir of Falkenrath // Heir to the Night'),
    (select id from sets where short_name = 'soi'),
    '116',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Confirm Suspicions'),
    (select id from sets where short_name = 'soi'),
    '53',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rush of Adrenaline'),
    (select id from sets where short_name = 'soi'),
    '177',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thraben Inspector'),
    (select id from sets where short_name = 'soi'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Town Gossipmonger // Incited Rabble'),
    (select id from sets where short_name = 'soi'),
    '46',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cult of the Waxing Moon'),
    (select id from sets where short_name = 'soi'),
    '201',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Voldaren Duelist'),
    (select id from sets where short_name = 'soi'),
    '191',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Purge'),
    (select id from sets where short_name = 'soi'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Autumnal Gloom // Ancient of the Equinox'),
    (select id from sets where short_name = 'soi'),
    '194',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Reckless Scholar'),
    (select id from sets where short_name = 'soi'),
    '82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magmatic Chasm'),
    (select id from sets where short_name = 'soi'),
    '172',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'soi'),
    '293',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hermit of the Natterknolls // Lone Wolf of the Natterknolls'),
    (select id from sets where short_name = 'soi'),
    '209',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silburlind Snapper'),
    (select id from sets where short_name = 'soi'),
    '85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moonlight Hunt'),
    (select id from sets where short_name = 'soi'),
    '219',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Intrepid Provisioner'),
    (select id from sets where short_name = 'soi'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sigarda, Heron''s Grace'),
    (select id from sets where short_name = 'soi'),
    '250',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Eerie Interlude'),
    (select id from sets where short_name = 'soi'),
    '16',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gisa''s Bidding'),
    (select id from sets where short_name = 'soi'),
    '114',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Duskwatch Recruiter // Krallenhorde Howler'),
    (select id from sets where short_name = 'soi'),
    '203',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Kindly Stranger // Demon-Possessed Witch'),
    (select id from sets where short_name = 'soi'),
    '119',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Startled Awake // Persistent Nightmare'),
    (select id from sets where short_name = 'soi'),
    '88',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Stone Quarry'),
    (select id from sets where short_name = 'soi'),
    '279',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Explosive Apparatus'),
    (select id from sets where short_name = 'soi'),
    '255',
    'common'
) ,
(
    (select id from mtgcard where name = 'Howlpack Wolf'),
    (select id from sets where short_name = 'soi'),
    '164',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gone Missing'),
    (select id from sets where short_name = 'soi'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tamiyo''s Journal'),
    (select id from sets where short_name = 'soi'),
    '265†b',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Manic Scribe'),
    (select id from sets where short_name = 'soi'),
    '73',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Topplegeist'),
    (select id from sets where short_name = 'soi'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghoulsteed'),
    (select id from sets where short_name = 'soi'),
    '113',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Gloomwidow'),
    (select id from sets where short_name = 'soi'),
    '206',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'soi'),
    '297',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tireless Tracker'),
    (select id from sets where short_name = 'soi'),
    '233',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Haunted Cloak'),
    (select id from sets where short_name = 'soi'),
    '257',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Geralf''s Masterpiece'),
    (select id from sets where short_name = 'soi'),
    '65',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Sleep Paralysis'),
    (select id from sets where short_name = 'soi'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dead Weight'),
    (select id from sets where short_name = 'soi'),
    '106',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dance with Devils'),
    (select id from sets where short_name = 'soi'),
    '150',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghostly Wings'),
    (select id from sets where short_name = 'soi'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Indulgent Aristocrat'),
    (select id from sets where short_name = 'soi'),
    '118',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thraben Gargoyle // Stonewing Antagonizer'),
    (select id from sets where short_name = 'soi'),
    '266',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Clip Wings'),
    (select id from sets where short_name = 'soi'),
    '197',
    'common'
) ,
(
    (select id from mtgcard where name = 'Furtive Homunculus'),
    (select id from sets where short_name = 'soi'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vessel of Ephemera'),
    (select id from sets where short_name = 'soi'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Convicted Killer // Branded Howler'),
    (select id from sets where short_name = 'soi'),
    '149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Briarbridge Patrol'),
    (select id from sets where short_name = 'soi'),
    '195',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ethereal Guidance'),
    (select id from sets where short_name = 'soi'),
    '18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angel of Deliverance'),
    (select id from sets where short_name = 'soi'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Declaration in Stone'),
    (select id from sets where short_name = 'soi'),
    '12',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shamble Back'),
    (select id from sets where short_name = 'soi'),
    '134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chaplain''s Blessing'),
    (select id from sets where short_name = 'soi'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Nephalia Moondrakes'),
    (select id from sets where short_name = 'soi'),
    '75',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Morkrut Necropod'),
    (select id from sets where short_name = 'soi'),
    '125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tamiyo''s Journal'),
    (select id from sets where short_name = 'soi'),
    '265†e',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Magnifying Glass'),
    (select id from sets where short_name = 'soi'),
    '258',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stitchwing Skaab'),
    (select id from sets where short_name = 'soi'),
    '90',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Burn from Within'),
    (select id from sets where short_name = 'soi'),
    '148',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Equestrian Skill'),
    (select id from sets where short_name = 'soi'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Quilled Wolf'),
    (select id from sets where short_name = 'soi'),
    '222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Markov Dreadknight'),
    (select id from sets where short_name = 'soi'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Deathcap Cultivator'),
    (select id from sets where short_name = 'soi'),
    '202',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'soi'),
    '285',
    'common'
) ,
(
    (select id from mtgcard where name = 'Inquisitor''s Ox'),
    (select id from sets where short_name = 'soi'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Behold the Beyond'),
    (select id from sets where short_name = 'soi'),
    '101',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Olivia''s Bloodsworn'),
    (select id from sets where short_name = 'soi'),
    '127',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thalia''s Lieutenant'),
    (select id from sets where short_name = 'soi'),
    '43',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Breakneck Rider // Neck Breaker'),
    (select id from sets where short_name = 'soi'),
    '147',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tooth Collector'),
    (select id from sets where short_name = 'soi'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wolf of Devil''s Breach'),
    (select id from sets where short_name = 'soi'),
    '192',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Root Out'),
    (select id from sets where short_name = 'soi'),
    '224',
    'common'
) 
 on conflict do nothing;
