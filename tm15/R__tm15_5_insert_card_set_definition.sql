insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Treefolk Warrior'),
    (select id from sets where short_name = 'tm15'),
    '11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Land Mine'),
    (select id from sets where short_name = 'tm15'),
    '12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spirit'),
    (select id from sets where short_name = 'tm15'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Garruk, Apex Predator Emblem'),
    (select id from sets where short_name = 'tm15'),
    '14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Squid'),
    (select id from sets where short_name = 'tm15'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dragon'),
    (select id from sets where short_name = 'tm15'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tm15'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tm15'),
    '9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sliver'),
    (select id from sets where short_name = 'tm15'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insect'),
    (select id from sets where short_name = 'tm15'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ajani Steadfast Emblem'),
    (select id from sets where short_name = 'tm15'),
    '13',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tm15'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tm15'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tm15'),
    '5',
    'common'
) 
 on conflict do nothing;
