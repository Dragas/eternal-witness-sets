insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Underworld Dreams'),
    (select id from sets where short_name = 'p2hg'),
    '1',
    'rare'
) 
 on conflict do nothing;
