insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Treetop Village'),
    (select id from sets where short_name = 'ulg'),
    '143',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Urza''s Blueprints'),
    (select id from sets where short_name = 'ulg'),
    '137',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Giant Cockroach'),
    (select id from sets where short_name = 'ulg'),
    '54',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Granger'),
    (select id from sets where short_name = 'ulg'),
    '118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Erase'),
    (select id from sets where short_name = 'ulg'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast of Burden'),
    (select id from sets where short_name = 'ulg'),
    '122',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anthroplasm'),
    (select id from sets where short_name = 'ulg'),
    '25',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Tethered Skirge'),
    (select id from sets where short_name = 'ulg'),
    '70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ring of Gix'),
    (select id from sets where short_name = 'ulg'),
    '131',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mother of Runes'),
    (select id from sets where short_name = 'ulg'),
    '14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Angel''s Trumpet'),
    (select id from sets where short_name = 'ulg'),
    '121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Defiler'),
    (select id from sets where short_name = 'ulg'),
    '60',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Multani, Maro-Sorcerer'),
    (select id from sets where short_name = 'ulg'),
    '107',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Shivan Phoenix'),
    (select id from sets where short_name = 'ulg'),
    '91',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Viashino Sandscout'),
    (select id from sets where short_name = 'ulg'),
    '96',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghitu Fire-Eater'),
    (select id from sets where short_name = 'ulg'),
    '76',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Viashino Heretic'),
    (select id from sets where short_name = 'ulg'),
    '95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Scrapheap'),
    (select id from sets where short_name = 'ulg'),
    '132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eviscerator'),
    (select id from sets where short_name = 'ulg'),
    '52',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Frantic Search'),
    (select id from sets where short_name = 'ulg'),
    '32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wing Snare'),
    (select id from sets where short_name = 'ulg'),
    '117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Knighthood'),
    (select id from sets where short_name = 'ulg'),
    '12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Thran Lens'),
    (select id from sets where short_name = 'ulg'),
    '133',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Darkwatch Elves'),
    (select id from sets where short_name = 'ulg'),
    '99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Reclamation'),
    (select id from sets where short_name = 'ulg'),
    '63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Broodlings'),
    (select id from sets where short_name = 'ulg'),
    '58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opal Champion'),
    (select id from sets where short_name = 'ulg'),
    '16',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viashino Bey'),
    (select id from sets where short_name = 'ulg'),
    '93',
    'common'
) ,
(
    (select id from mtgcard where name = 'Engineered Plague'),
    (select id from sets where short_name = 'ulg'),
    '51',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Slow Motion'),
    (select id from sets where short_name = 'ulg'),
    '42',
    'common'
) ,
(
    (select id from mtgcard where name = 'Radiant''s Judgment'),
    (select id from sets where short_name = 'ulg'),
    '22',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tragic Poet'),
    (select id from sets where short_name = 'ulg'),
    '24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Last-Ditch Effort'),
    (select id from sets where short_name = 'ulg'),
    '83',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sluggishness'),
    (select id from sets where short_name = 'ulg'),
    '92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Treacherous Link'),
    (select id from sets where short_name = 'ulg'),
    '71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Plaguelord'),
    (select id from sets where short_name = 'ulg'),
    '62',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pyromancy'),
    (select id from sets where short_name = 'ulg'),
    '88',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Vigilant Drake'),
    (select id from sets where short_name = 'ulg'),
    '46',
    'common'
) ,
(
    (select id from mtgcard where name = 'Multani''s Presence'),
    (select id from sets where short_name = 'ulg'),
    '109',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cessation'),
    (select id from sets where short_name = 'ulg'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rivalry'),
    (select id from sets where short_name = 'ulg'),
    '90',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Avalanche Riders'),
    (select id from sets where short_name = 'ulg'),
    '74',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fog of Gnats'),
    (select id from sets where short_name = 'ulg'),
    '53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Blessed Reversal'),
    (select id from sets where short_name = 'ulg'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Burst of Energy'),
    (select id from sets where short_name = 'ulg'),
    '3',
    'common'
) ,
(
    (select id from mtgcard where name = 'Spawning Pool'),
    (select id from sets where short_name = 'ulg'),
    '142',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Impending Disaster'),
    (select id from sets where short_name = 'ulg'),
    '82',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Crawlspace'),
    (select id from sets where short_name = 'ulg'),
    '123',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Intervene'),
    (select id from sets where short_name = 'ulg'),
    '33',
    'common'
) ,
(
    (select id from mtgcard where name = 'Miscalculation'),
    (select id from sets where short_name = 'ulg'),
    '36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Martyr''s Cause'),
    (select id from sets where short_name = 'ulg'),
    '13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rebuild'),
    (select id from sets where short_name = 'ulg'),
    '40',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'No Mercy'),
    (select id from sets where short_name = 'ulg'),
    '56',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Multani''s Acolyte'),
    (select id from sets where short_name = 'ulg'),
    '108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weatherseed Elf'),
    (select id from sets where short_name = 'ulg'),
    '115',
    'common'
) ,
(
    (select id from mtgcard where name = 'Cloud of Faeries'),
    (select id from sets where short_name = 'ulg'),
    '29',
    'common'
) ,
(
    (select id from mtgcard where name = 'Subversion'),
    (select id from sets where short_name = 'ulg'),
    '68',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Purify'),
    (select id from sets where short_name = 'ulg'),
    '19',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Palinchron'),
    (select id from sets where short_name = 'ulg'),
    '38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thran War Machine'),
    (select id from sets where short_name = 'ulg'),
    '134',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rank and File'),
    (select id from sets where short_name = 'ulg'),
    '65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rancor'),
    (select id from sets where short_name = 'ulg'),
    '110',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gang of Elk'),
    (select id from sets where short_name = 'ulg'),
    '102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Harmonic Convergence'),
    (select id from sets where short_name = 'ulg'),
    '103',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Defense Grid'),
    (select id from sets where short_name = 'ulg'),
    '125',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Peace and Quiet'),
    (select id from sets where short_name = 'ulg'),
    '17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Might of Oaks'),
    (select id from sets where short_name = 'ulg'),
    '106',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Thran Weaponry'),
    (select id from sets where short_name = 'ulg'),
    '135',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Goblin Welder'),
    (select id from sets where short_name = 'ulg'),
    '80',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ticking Gnomes'),
    (select id from sets where short_name = 'ulg'),
    '136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Second Chance'),
    (select id from sets where short_name = 'ulg'),
    '41',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Opportunity'),
    (select id from sets where short_name = 'ulg'),
    '37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Crop Rotation'),
    (select id from sets where short_name = 'ulg'),
    '98',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Debaser'),
    (select id from sets where short_name = 'ulg'),
    '59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghitu Slinger'),
    (select id from sets where short_name = 'ulg'),
    '77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Defender of Chaos'),
    (select id from sets where short_name = 'ulg'),
    '75',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Medics'),
    (select id from sets where short_name = 'ulg'),
    '79',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karmic Guide'),
    (select id from sets where short_name = 'ulg'),
    '11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ostracize'),
    (select id from sets where short_name = 'ulg'),
    '57',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hope and Glory'),
    (select id from sets where short_name = 'ulg'),
    '9',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Tinker'),
    (select id from sets where short_name = 'ulg'),
    '45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'About Face'),
    (select id from sets where short_name = 'ulg'),
    '73',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bouncing Beebles'),
    (select id from sets where short_name = 'ulg'),
    '28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sick and Tired'),
    (select id from sets where short_name = 'ulg'),
    '66',
    'common'
) ,
(
    (select id from mtgcard where name = 'Damping Engine'),
    (select id from sets where short_name = 'ulg'),
    '124',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wheel of Torture'),
    (select id from sets where short_name = 'ulg'),
    '138',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Delusions of Mediocrity'),
    (select id from sets where short_name = 'ulg'),
    '30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Radiant''s Dragoons'),
    (select id from sets where short_name = 'ulg'),
    '21',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Weatherseed Faeries'),
    (select id from sets where short_name = 'ulg'),
    '48',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hidden Gibbons'),
    (select id from sets where short_name = 'ulg'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lurking Skirge'),
    (select id from sets where short_name = 'ulg'),
    '55',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swat'),
    (select id from sets where short_name = 'ulg'),
    '69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Silk Net'),
    (select id from sets where short_name = 'ulg'),
    '112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Molten Hydra'),
    (select id from sets where short_name = 'ulg'),
    '85',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Pygmy Pyrosaur'),
    (select id from sets where short_name = 'ulg'),
    '87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forbidding Watchtower'),
    (select id from sets where short_name = 'ulg'),
    '140',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bloated Toad'),
    (select id from sets where short_name = 'ulg'),
    '97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treefolk Mystic'),
    (select id from sets where short_name = 'ulg'),
    '114',
    'common'
) ,
(
    (select id from mtgcard where name = 'Devout Harpist'),
    (select id from sets where short_name = 'ulg'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Archivist'),
    (select id from sets where short_name = 'ulg'),
    '26',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Radiant, Archangel'),
    (select id from sets where short_name = 'ulg'),
    '20',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Defender of Law'),
    (select id from sets where short_name = 'ulg'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Thornwind Faeries'),
    (select id from sets where short_name = 'ulg'),
    '44',
    'common'
) ,
(
    (select id from mtgcard where name = 'Grim Monolith'),
    (select id from sets where short_name = 'ulg'),
    '126',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Defense of the Heart'),
    (select id from sets where short_name = 'ulg'),
    '100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Sleeper''s Guile'),
    (select id from sets where short_name = 'ulg'),
    '67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simian Grunts'),
    (select id from sets where short_name = 'ulg'),
    '113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Memory Jar'),
    (select id from sets where short_name = 'ulg'),
    '129',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Scion'),
    (select id from sets where short_name = 'ulg'),
    '119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rack and Ruin'),
    (select id from sets where short_name = 'ulg'),
    '89',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aura Flux'),
    (select id from sets where short_name = 'ulg'),
    '27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Repopulate'),
    (select id from sets where short_name = 'ulg'),
    '111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Iron Maiden'),
    (select id from sets where short_name = 'ulg'),
    '127',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Faerie Conclave'),
    (select id from sets where short_name = 'ulg'),
    '139',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'King Crab'),
    (select id from sets where short_name = 'ulg'),
    '34',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Raven Familiar'),
    (select id from sets where short_name = 'ulg'),
    '39',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Walking Sponge'),
    (select id from sets where short_name = 'ulg'),
    '47',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Unearth'),
    (select id from sets where short_name = 'ulg'),
    '72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Viashino Cutthroat'),
    (select id from sets where short_name = 'ulg'),
    '94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Iron Will'),
    (select id from sets where short_name = 'ulg'),
    '10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghitu Encampment'),
    (select id from sets where short_name = 'ulg'),
    '141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Deranged Hermit'),
    (select id from sets where short_name = 'ulg'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plague Beetle'),
    (select id from sets where short_name = 'ulg'),
    '64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Angelic Curator'),
    (select id from sets where short_name = 'ulg'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghitu War Cry'),
    (select id from sets where short_name = 'ulg'),
    '78',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Levitation'),
    (select id from sets where short_name = 'ulg'),
    '35',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Sustainer of the Realm'),
    (select id from sets where short_name = 'ulg'),
    '23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Parch'),
    (select id from sets where short_name = 'ulg'),
    '86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Denouncer'),
    (select id from sets where short_name = 'ulg'),
    '61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Wurm'),
    (select id from sets where short_name = 'ulg'),
    '120',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lava Axe'),
    (select id from sets where short_name = 'ulg'),
    '84',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lone Wolf'),
    (select id from sets where short_name = 'ulg'),
    '105',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Bone Shredder'),
    (select id from sets where short_name = 'ulg'),
    '49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Quicksilver Amulet'),
    (select id from sets where short_name = 'ulg'),
    '130',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Jhoira''s Toolbox'),
    (select id from sets where short_name = 'ulg'),
    '128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Planar Collapse'),
    (select id from sets where short_name = 'ulg'),
    '18',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Granite Grip'),
    (select id from sets where short_name = 'ulg'),
    '81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fleeting Image'),
    (select id from sets where short_name = 'ulg'),
    '31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Expendable Troops'),
    (select id from sets where short_name = 'ulg'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weatherseed Treefolk'),
    (select id from sets where short_name = 'ulg'),
    '116',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Snap'),
    (select id from sets where short_name = 'ulg'),
    '43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Opal Avenger'),
    (select id from sets where short_name = 'ulg'),
    '15',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Brink of Madness'),
    (select id from sets where short_name = 'ulg'),
    '50',
    'rare'
) 
 on conflict do nothing;
