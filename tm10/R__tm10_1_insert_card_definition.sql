    insert into mtgcard(name) values ('Wolf') on conflict do nothing;
    insert into mtgcard(name) values ('Avatar') on conflict do nothing;
    insert into mtgcard(name) values ('Goblin') on conflict do nothing;
    insert into mtgcard(name) values ('Beast') on conflict do nothing;
    insert into mtgcard(name) values ('Insect') on conflict do nothing;
    insert into mtgcard(name) values ('Soldier') on conflict do nothing;
    insert into mtgcard(name) values ('Gargoyle') on conflict do nothing;
    insert into mtgcard(name) values ('Zombie') on conflict do nothing;
