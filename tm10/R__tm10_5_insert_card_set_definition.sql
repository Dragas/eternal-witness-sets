insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Wolf'),
    (select id from sets where short_name = 'tm10'),
    '7',
    'common'
) ,
(
    (select id from mtgcard where name = 'Avatar'),
    (select id from sets where short_name = 'tm10'),
    '1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin'),
    (select id from sets where short_name = 'tm10'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Beast'),
    (select id from sets where short_name = 'tm10'),
    '5',
    'common'
) ,
(
    (select id from mtgcard where name = 'Insect'),
    (select id from sets where short_name = 'tm10'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Soldier'),
    (select id from sets where short_name = 'tm10'),
    '2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Gargoyle'),
    (select id from sets where short_name = 'tm10'),
    '8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Zombie'),
    (select id from sets where short_name = 'tm10'),
    '3',
    'common'
) 
 on conflict do nothing;
