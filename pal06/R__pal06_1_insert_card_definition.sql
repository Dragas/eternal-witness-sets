    insert into mtgcard(name) values ('Plains') on conflict do nothing;
    insert into mtgcard(name) values ('Coiling Oracle') on conflict do nothing;
    insert into mtgcard(name) values ('Surging Flame') on conflict do nothing;
    insert into mtgcard(name) values ('Wee Dragonauts') on conflict do nothing;
    insert into mtgcard(name) values ('Castigate') on conflict do nothing;
    insert into mtgcard(name) values ('Island') on conflict do nothing;
    insert into mtgcard(name) values ('Swamp') on conflict do nothing;
    insert into mtgcard(name) values ('Mountain') on conflict do nothing;
    insert into mtgcard(name) values ('Forest') on conflict do nothing;
