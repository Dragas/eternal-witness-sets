insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pal06'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Coiling Oracle'),
    (select id from sets where short_name = 'pal06'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Surging Flame'),
    (select id from sets where short_name = 'pal06'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Wee Dragonauts'),
    (select id from sets where short_name = 'pal06'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Castigate'),
    (select id from sets where short_name = 'pal06'),
    '6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pal06'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pal06'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pal06'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pal06'),
    '5',
    'rare'
) 
 on conflict do nothing;
