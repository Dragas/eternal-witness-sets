insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pana'),
    '242',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pana'),
    '210',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pana'),
    '203',
    'common'
) ,
(
    (select id from mtgcard where name = 'Llanowar Elves'),
    (select id from sets where short_name = 'pana'),
    '4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pana'),
    '212',
    'common'
) ,
(
    (select id from mtgcard where name = 'Vraska, Golgari Queen'),
    (select id from sets where short_name = 'pana'),
    '3',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Bladewing the Risen'),
    (select id from sets where short_name = 'pana'),
    '104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pana'),
    '233',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pana'),
    '208',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pana'),
    '219',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pana'),
    '211',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ral, Izzet Viceroy'),
    (select id from sets where short_name = 'pana'),
    '1',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pana'),
    '217',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pana'),
    '239',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pana'),
    '221',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pana'),
    '223',
    'common'
) ,
(
    (select id from mtgcard where name = 'Firemind''s Research'),
    (select id from sets where short_name = 'pana'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pana'),
    '222',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pana'),
    '236',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pana'),
    '238',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pana'),
    '204',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pana'),
    '235',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pana'),
    '237',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pana'),
    '232',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pana'),
    '245',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pana'),
    '215',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pana'),
    '229',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pana'),
    '214',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pana'),
    '209',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pana'),
    '240',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pana'),
    '207',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pana'),
    '216',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pana'),
    '202',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pana'),
    '218',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pana'),
    '206',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pana'),
    '243',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pana'),
    '234',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pana'),
    '213',
    'common'
) ,
(
    (select id from mtgcard where name = 'Talrand, Sky Summoner'),
    (select id from sets where short_name = 'pana'),
    '102',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pana'),
    '244',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pana'),
    '226',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'pana'),
    '228',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pana'),
    '201',
    'common'
) ,
(
    (select id from mtgcard where name = 'Ghalta, Primal Hunger'),
    (select id from sets where short_name = 'pana'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pana'),
    '231',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pana'),
    '220',
    'common'
) ,
(
    (select id from mtgcard where name = 'Duress'),
    (select id from sets where short_name = 'pana'),
    '6',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pana'),
    '230',
    'common'
) ,
(
    (select id from mtgcard where name = 'Teferi, Hero of Dominaria'),
    (select id from sets where short_name = 'pana'),
    '2',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pana'),
    '205',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'pana'),
    '241',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hanna, Ship''s Navigator'),
    (select id from sets where short_name = 'pana'),
    '105',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'pana'),
    '225',
    'common'
) ,
(
    (select id from mtgcard where name = 'The Gitrog Monster'),
    (select id from sets where short_name = 'pana'),
    '103',
    'mythic'
) ,
(
    (select id from mtgcard where name = 'Rhys the Redeemed'),
    (select id from sets where short_name = 'pana'),
    '101',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'pana'),
    '227',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'pana'),
    '224',
    'common'
) 
 on conflict do nothing;
