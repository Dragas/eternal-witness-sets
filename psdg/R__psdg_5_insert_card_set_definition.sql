insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Tornellan Protector'),
    (select id from sets where short_name = 'psdg'),
    '9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Murgish Cemetery'),
    (select id from sets where short_name = 'psdg'),
    '7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Velican Dragon'),
    (select id from sets where short_name = 'psdg'),
    '10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Saji''s Torrent'),
    (select id from sets where short_name = 'psdg'),
    '8',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lydari Druid'),
    (select id from sets where short_name = 'psdg'),
    '5',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Arden Angel'),
    (select id from sets where short_name = 'psdg'),
    '1',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Camato Scout'),
    (select id from sets where short_name = 'psdg'),
    '3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Hapato''s Might'),
    (select id from sets where short_name = 'psdg'),
    '4',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ashuza''s Breath'),
    (select id from sets where short_name = 'psdg'),
    '2',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lydari Elephant'),
    (select id from sets where short_name = 'psdg'),
    '6',
    'rare'
) 
 on conflict do nothing;
