insert into mtgcard_set(card_id, set_id, card_number, rarity) values
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'td0'),
    'B35',
    'common'
) ,
(
    (select id from mtgcard where name = 'Miren, the Moaning Well'),
    (select id from sets where short_name = 'td0'),
    'A132',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lava Spike'),
    (select id from sets where short_name = 'td0'),
    'B20',
    'common'
) ,
(
    (select id from mtgcard where name = 'Confiscate'),
    (select id from sets where short_name = 'td0'),
    'A20',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Price of Progress'),
    (select id from sets where short_name = 'td0'),
    'B23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'td0'),
    'A142',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Greaves'),
    (select id from sets where short_name = 'td0'),
    'A115',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mishra''s Factory'),
    (select id from sets where short_name = 'td0'),
    'B33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'td0'),
    'A152',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakdos Carnarium'),
    (select id from sets where short_name = 'td0'),
    'A133',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flickerwisp'),
    (select id from sets where short_name = 'td0'),
    'B4',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Stonecloaker'),
    (select id from sets where short_name = 'td0'),
    'A16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Treva''s Ruins'),
    (select id from sets where short_name = 'td0'),
    'A141',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Keldon Marauders'),
    (select id from sets where short_name = 'td0'),
    'B19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Life from the Loam'),
    (select id from sets where short_name = 'td0'),
    'A79',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Terror'),
    (select id from sets where short_name = 'td0'),
    'A50',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sterling Grove'),
    (select id from sets where short_name = 'td0'),
    'A106',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Brawn'),
    (select id from sets where short_name = 'td0'),
    'A63',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chain Lightning'),
    (select id from sets where short_name = 'td0'),
    'A55',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shambling Shell'),
    (select id from sets where short_name = 'td0'),
    'A105',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'td0'),
    'A156',
    'common'
) ,
(
    (select id from mtgcard where name = 'Weathered Wayfarer'),
    (select id from sets where short_name = 'td0'),
    'B13',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'td0'),
    'A147',
    'common'
) ,
(
    (select id from mtgcard where name = 'Prison Term'),
    (select id from sets where short_name = 'td0'),
    'A13',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'td0'),
    'A155',
    'common'
) ,
(
    (select id from mtgcard where name = 'Edge of Autumn'),
    (select id from sets where short_name = 'td0'),
    'A67',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jötun Grunt'),
    (select id from sets where short_name = 'td0'),
    'B5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ashes to Ashes'),
    (select id from sets where short_name = 'td0'),
    'A37',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Relic of Progenitus'),
    (select id from sets where short_name = 'td0'),
    'A117',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'td0'),
    'A145',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kor Firewalker'),
    (select id from sets where short_name = 'td0'),
    'B6',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wirewood Guardian'),
    (select id from sets where short_name = 'td0'),
    'A90',
    'common'
) ,
(
    (select id from mtgcard where name = 'Borderland Ranger'),
    (select id from sets where short_name = 'td0'),
    'A62',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stoneforge Mystic'),
    (select id from sets where short_name = 'td0'),
    'B11',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Umezawa''s Jitte'),
    (select id from sets where short_name = 'td0'),
    'B30',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Oblivion Ring'),
    (select id from sets where short_name = 'td0'),
    'B8',
    'common'
) ,
(
    (select id from mtgcard where name = 'Sakura-Tribe Elder'),
    (select id from sets where short_name = 'td0'),
    'A86',
    'common'
) ,
(
    (select id from mtgcard where name = 'Simic Signet'),
    (select id from sets where short_name = 'td0'),
    'A119',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'td0'),
    'A149',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mulldrifter'),
    (select id from sets where short_name = 'td0'),
    'A28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Tranquil Thicket'),
    (select id from sets where short_name = 'td0'),
    'A140',
    'common'
) ,
(
    (select id from mtgcard where name = 'Anger'),
    (select id from sets where short_name = 'td0'),
    'A54',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Vampiric Dragon'),
    (select id from sets where short_name = 'td0'),
    'A109',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Temporal Spring'),
    (select id from sets where short_name = 'td0'),
    'A107',
    'common'
) ,
(
    (select id from mtgcard where name = 'Massacre'),
    (select id from sets where short_name = 'td0'),
    'A44',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghostly Prison'),
    (select id from sets where short_name = 'td0'),
    'A8',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Darigaaz''s Caldera'),
    (select id from sets where short_name = 'td0'),
    'A125',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Putrefy'),
    (select id from sets where short_name = 'td0'),
    'A102',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Oblivion Ring'),
    (select id from sets where short_name = 'td0'),
    'A12',
    'common'
) ,
(
    (select id from mtgcard where name = 'Concordant Crossroads'),
    (select id from sets where short_name = 'td0'),
    'A65',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Coastal Tower'),
    (select id from sets where short_name = 'td0'),
    'A124',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Auramancer'),
    (select id from sets where short_name = 'td0'),
    'A1',
    'common'
) ,
(
    (select id from mtgcard where name = 'Genesis'),
    (select id from sets where short_name = 'td0'),
    'A73',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Explosive Vegetation'),
    (select id from sets where short_name = 'td0'),
    'A71',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selesnya Signet'),
    (select id from sets where short_name = 'td0'),
    'A118',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bant Panorama'),
    (select id from sets where short_name = 'td0'),
    'A122',
    'common'
) ,
(
    (select id from mtgcard where name = 'Enlightened Tutor'),
    (select id from sets where short_name = 'td0'),
    'A6',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Keldon Vandals'),
    (select id from sets where short_name = 'td0'),
    'A59',
    'common'
) ,
(
    (select id from mtgcard where name = 'Karakas'),
    (select id from sets where short_name = 'td0'),
    'B32',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Serra Avenger'),
    (select id from sets where short_name = 'td0'),
    'B9',
    'rare'
) ,
(
    (select id from mtgcard where name = 'All Hallow''s Eve'),
    (select id from sets where short_name = 'td0'),
    'A35',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Abyssal Gatekeeper'),
    (select id from sets where short_name = 'td0'),
    'A34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rootbreaker Wurm'),
    (select id from sets where short_name = 'td0'),
    'A85',
    'common'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Enchantress'),
    (select id from sets where short_name = 'td0'),
    'A93',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Chain Lightning'),
    (select id from sets where short_name = 'td0'),
    'B14',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hellspark Elemental'),
    (select id from sets where short_name = 'td0'),
    'B18',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mangara of Corondor'),
    (select id from sets where short_name = 'td0'),
    'B7',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Rhystic Study'),
    (select id from sets where short_name = 'td0'),
    'A31',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elfhame Palace'),
    (select id from sets where short_name = 'td0'),
    'A126',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wood Elves'),
    (select id from sets where short_name = 'td0'),
    'A91',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Grave-Troll'),
    (select id from sets where short_name = 'td0'),
    'A74',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Simic Growth Chamber'),
    (select id from sets where short_name = 'td0'),
    'A138',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Resurrection'),
    (select id from sets where short_name = 'td0'),
    'A14',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terramorphic Expanse'),
    (select id from sets where short_name = 'td0'),
    'A139',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rubinia Soulsinger'),
    (select id from sets where short_name = 'td0'),
    'A104',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Anarchist'),
    (select id from sets where short_name = 'td0'),
    'A52',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shoreline Ranger'),
    (select id from sets where short_name = 'td0'),
    'A32',
    'common'
) ,
(
    (select id from mtgcard where name = 'Golgari Rot Farm'),
    (select id from sets where short_name = 'td0'),
    'A129',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seal of Cleansing'),
    (select id from sets where short_name = 'td0'),
    'A15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Island'),
    (select id from sets where short_name = 'td0'),
    'A146',
    'common'
) ,
(
    (select id from mtgcard where name = 'Recollect'),
    (select id from sets where short_name = 'td0'),
    'A84',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Illusionary Mask'),
    (select id from sets where short_name = 'td0'),
    'A114',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Copy Enchantment'),
    (select id from sets where short_name = 'td0'),
    'A21',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Eternal Witness'),
    (select id from sets where short_name = 'td0'),
    'A70',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Terminate'),
    (select id from sets where short_name = 'td0'),
    'A108',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dismantling Blow'),
    (select id from sets where short_name = 'td0'),
    'A4',
    'common'
) ,
(
    (select id from mtgcard where name = 'Noble Templar'),
    (select id from sets where short_name = 'td0'),
    'A11',
    'common'
) ,
(
    (select id from mtgcard where name = 'Willbender'),
    (select id from sets where short_name = 'td0'),
    'A33',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Penumbra Bobcat'),
    (select id from sets where short_name = 'td0'),
    'A81',
    'common'
) ,
(
    (select id from mtgcard where name = 'Coiling Oracle'),
    (select id from sets where short_name = 'td0'),
    'A97',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'td0'),
    'A151',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azorius Chancery'),
    (select id from sets where short_name = 'td0'),
    'A121',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'td0'),
    'A144',
    'common'
) ,
(
    (select id from mtgcard where name = 'Shriekmaw'),
    (select id from sets where short_name = 'td0'),
    'A48',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Silence'),
    (select id from sets where short_name = 'td0'),
    'B10',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruul Turf'),
    (select id from sets where short_name = 'td0'),
    'A130',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elvish Visionary'),
    (select id from sets where short_name = 'td0'),
    'A69',
    'common'
) ,
(
    (select id from mtgcard where name = 'Man-o''-War'),
    (select id from sets where short_name = 'td0'),
    'A26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Xira Arien'),
    (select id from sets where short_name = 'td0'),
    'A110',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Ethersworn Canonist'),
    (select id from sets where short_name = 'td0'),
    'B3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Constant Mists'),
    (select id from sets where short_name = 'td0'),
    'A66',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fact or Fiction'),
    (select id from sets where short_name = 'td0'),
    'A23',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Grim Harvest'),
    (select id from sets where short_name = 'td0'),
    'A43',
    'common'
) ,
(
    (select id from mtgcard where name = 'Court Hussar'),
    (select id from sets where short_name = 'td0'),
    'A22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Magma Jet'),
    (select id from sets where short_name = 'td0'),
    'B22',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Krosan Tusker'),
    (select id from sets where short_name = 'td0'),
    'A78',
    'common'
) ,
(
    (select id from mtgcard where name = 'Wickerbough Elder'),
    (select id from sets where short_name = 'td0'),
    'A88',
    'common'
) ,
(
    (select id from mtgcard where name = 'Reaping the Graves'),
    (select id from sets where short_name = 'td0'),
    'A47',
    'common'
) ,
(
    (select id from mtgcard where name = 'Elven Cache'),
    (select id from sets where short_name = 'td0'),
    'A68',
    'common'
) ,
(
    (select id from mtgcard where name = 'Benevolent Bodyguard'),
    (select id from sets where short_name = 'td0'),
    'B2',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'td0'),
    'B38',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'td0'),
    'A153',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rampant Growth'),
    (select id from sets where short_name = 'td0'),
    'A83',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flagstones of Trokair'),
    (select id from sets where short_name = 'td0'),
    'B31',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Skullclamp'),
    (select id from sets where short_name = 'td0'),
    'A120',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Aether Vial'),
    (select id from sets where short_name = 'td0'),
    'B27',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Signet'),
    (select id from sets where short_name = 'td0'),
    'A112',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jund Panorama'),
    (select id from sets where short_name = 'td0'),
    'A131',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Barrier'),
    (select id from sets where short_name = 'td0'),
    'A99',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'td0'),
    'A143',
    'common'
) ,
(
    (select id from mtgcard where name = 'Werebear'),
    (select id from sets where short_name = 'td0'),
    'A87',
    'common'
) ,
(
    (select id from mtgcard where name = 'Harmonize'),
    (select id from sets where short_name = 'td0'),
    'A75',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Selesnya Sanctuary'),
    (select id from sets where short_name = 'td0'),
    'A137',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'td0'),
    'B41',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'td0'),
    'B40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'td0'),
    'B37',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mistmeadow Witch'),
    (select id from sets where short_name = 'td0'),
    'A101',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ancient Grudge'),
    (select id from sets where short_name = 'td0'),
    'A53',
    'common'
) ,
(
    (select id from mtgcard where name = 'Forest'),
    (select id from sets where short_name = 'td0'),
    'A154',
    'common'
) ,
(
    (select id from mtgcard where name = 'Barren Moor'),
    (select id from sets where short_name = 'td0'),
    'A123',
    'common'
) ,
(
    (select id from mtgcard where name = 'Pyrostatic Pillar'),
    (select id from sets where short_name = 'td0'),
    'B25',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Looter il-Kor'),
    (select id from sets where short_name = 'td0'),
    'A25',
    'common'
) ,
(
    (select id from mtgcard where name = 'Resounding Thunder'),
    (select id from sets where short_name = 'td0'),
    'A61',
    'common'
) ,
(
    (select id from mtgcard where name = 'Mountain'),
    (select id from sets where short_name = 'td0'),
    'B39',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hissing Iguanar'),
    (select id from sets where short_name = 'td0'),
    'A58',
    'common'
) ,
(
    (select id from mtgcard where name = 'Hinder'),
    (select id from sets where short_name = 'td0'),
    'A24',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Ghost Quarter'),
    (select id from sets where short_name = 'td0'),
    'A128',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Yavimaya Elder'),
    (select id from sets where short_name = 'td0'),
    'A92',
    'common'
) ,
(
    (select id from mtgcard where name = 'Bonesplitter'),
    (select id from sets where short_name = 'td0'),
    'B28',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kami of Ancient Law'),
    (select id from sets where short_name = 'td0'),
    'A9',
    'common'
) ,
(
    (select id from mtgcard where name = 'Fertile Ground'),
    (select id from sets where short_name = 'td0'),
    'A72',
    'common'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'td0'),
    'B34',
    'common'
) ,
(
    (select id from mtgcard where name = 'Capsize'),
    (select id from sets where short_name = 'td0'),
    'A18',
    'common'
) ,
(
    (select id from mtgcard where name = 'Merfolk Looter'),
    (select id from sets where short_name = 'td0'),
    'A27',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'td0'),
    'A148',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rakdos Signet'),
    (select id from sets where short_name = 'td0'),
    'A116',
    'common'
) ,
(
    (select id from mtgcard where name = 'Jungle Lion'),
    (select id from sets where short_name = 'td0'),
    'A76',
    'common'
) ,
(
    (select id from mtgcard where name = 'Kodama''s Reach'),
    (select id from sets where short_name = 'td0'),
    'A77',
    'common'
) ,
(
    (select id from mtgcard where name = 'Momentary Blink'),
    (select id from sets where short_name = 'td0'),
    'A10',
    'common'
) ,
(
    (select id from mtgcard where name = 'Overwhelming Intellect'),
    (select id from sets where short_name = 'td0'),
    'A29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Twisted Abomination'),
    (select id from sets where short_name = 'td0'),
    'A51',
    'common'
) ,
(
    (select id from mtgcard where name = 'Stinkweed Imp'),
    (select id from sets where short_name = 'td0'),
    'A49',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fireblast'),
    (select id from sets where short_name = 'td0'),
    'B15',
    'common'
) ,
(
    (select id from mtgcard where name = 'Relic of Progenitus'),
    (select id from sets where short_name = 'td0'),
    'B29',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Azorius Signet'),
    (select id from sets where short_name = 'td0'),
    'A111',
    'common'
) ,
(
    (select id from mtgcard where name = 'Flametongue Kavu'),
    (select id from sets where short_name = 'td0'),
    'A57',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Cauldron Dance'),
    (select id from sets where short_name = 'td0'),
    'A96',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Flames of the Blood Hand'),
    (select id from sets where short_name = 'td0'),
    'B16',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Questing Phelddagrif'),
    (select id from sets where short_name = 'td0'),
    'A103',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Phyrexian Arena'),
    (select id from sets where short_name = 'td0'),
    'A46',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Savage Lands'),
    (select id from sets where short_name = 'td0'),
    'A135',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Faith''s Fetters'),
    (select id from sets where short_name = 'td0'),
    'A7',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Nezumi Graverobber // Nighteyes the Desecrator'),
    (select id from sets where short_name = 'td0'),
    'A45',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Civic Wayfinder'),
    (select id from sets where short_name = 'td0'),
    'A64',
    'common'
) ,
(
    (select id from mtgcard where name = 'Aura of Silence'),
    (select id from sets where short_name = 'td0'),
    'B1',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Animate Dead'),
    (select id from sets where short_name = 'td0'),
    'A36',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Golgari Thug'),
    (select id from sets where short_name = 'td0'),
    'A42',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Fires of Yavimaya'),
    (select id from sets where short_name = 'td0'),
    'A98',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Pyroblast'),
    (select id from sets where short_name = 'td0'),
    'B24',
    'common'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'td0'),
    'B21',
    'common'
) ,
(
    (select id from mtgcard where name = 'Chartooth Cougar'),
    (select id from sets where short_name = 'td0'),
    'A56',
    'common'
) ,
(
    (select id from mtgcard where name = 'Carrion Feeder'),
    (select id from sets where short_name = 'td0'),
    'A40',
    'common'
) ,
(
    (select id from mtgcard where name = 'Decree of Justice'),
    (select id from sets where short_name = 'td0'),
    'A3',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Lightning Bolt'),
    (select id from sets where short_name = 'td0'),
    'A60',
    'common'
) ,
(
    (select id from mtgcard where name = 'Azorius Guildmage'),
    (select id from sets where short_name = 'td0'),
    'A95',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Avatar of Woe'),
    (select id from sets where short_name = 'td0'),
    'A38',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Raven Familiar'),
    (select id from sets where short_name = 'td0'),
    'A30',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Plains'),
    (select id from sets where short_name = 'td0'),
    'B36',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swamp'),
    (select id from sets where short_name = 'td0'),
    'A150',
    'common'
) ,
(
    (select id from mtgcard where name = 'Dread Return'),
    (select id from sets where short_name = 'td0'),
    'A41',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Phantom Centaur'),
    (select id from sets where short_name = 'td0'),
    'A82',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Rupture Spire'),
    (select id from sets where short_name = 'td0'),
    'A134',
    'common'
) ,
(
    (select id from mtgcard where name = 'Goblin Guide'),
    (select id from sets where short_name = 'td0'),
    'B17',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Armadillo Cloak'),
    (select id from sets where short_name = 'td0'),
    'A94',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'td0'),
    'B12',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Empyrial Armor'),
    (select id from sets where short_name = 'td0'),
    'A5',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Forgotten Cave'),
    (select id from sets where short_name = 'td0'),
    'A127',
    'common'
) ,
(
    (select id from mtgcard where name = 'Compulsive Research'),
    (select id from sets where short_name = 'td0'),
    'A19',
    'common'
) ,
(
    (select id from mtgcard where name = 'Moment''s Peace'),
    (select id from sets where short_name = 'td0'),
    'A80',
    'common'
) ,
(
    (select id from mtgcard where name = 'Rift Bolt'),
    (select id from sets where short_name = 'td0'),
    'B26',
    'common'
) ,
(
    (select id from mtgcard where name = 'Swords to Plowshares'),
    (select id from sets where short_name = 'td0'),
    'A17',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Wild Mongrel'),
    (select id from sets where short_name = 'td0'),
    'A89',
    'common'
) ,
(
    (select id from mtgcard where name = 'Condemn'),
    (select id from sets where short_name = 'td0'),
    'A2',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Mirari''s Wake'),
    (select id from sets where short_name = 'td0'),
    'A100',
    'rare'
) ,
(
    (select id from mtgcard where name = 'Gruul Signet'),
    (select id from sets where short_name = 'td0'),
    'A113',
    'common'
) ,
(
    (select id from mtgcard where name = 'Seaside Citadel'),
    (select id from sets where short_name = 'td0'),
    'A136',
    'uncommon'
) ,
(
    (select id from mtgcard where name = 'Buried Alive'),
    (select id from sets where short_name = 'td0'),
    'A39',
    'uncommon'
) 
 on conflict do nothing;
